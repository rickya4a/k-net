var $ = jQuery;

var Frm = {
  validatee : function(){
    $('#qcTcktForm').on('submit', function( e ) {
      var name = $("#name").val();
      var ktp = $("#ktp").val();
      var alamat = $("#alamat").val();
      var phone = $("#phone").val();
      var email = $("#email").val();
      var ticket = $("#ticket").val();
      var qty = $("#quantity").val();
           
      // if(qty == '0') {
      //   e.preventDefault( e );
      //   alert('Please Select Quantity');
      //  }
      //  else if(ticket == '') {
      //   e.preventDefault( e );
      //   alert('Please Select Ticket');
      //  }
      //  else if(phone == '') {
      //   e.preventDefault( e );
      //   alert('Please Fill telephone number');
      //  }
      //  else if(alamat == '') {
      //   e.preventDefault( e );
      //   alert('Please Fill The Address');
      //  }
      //  else if(ktp == '') {
      //   e.preventDefault( e );
      //   alert('Please Fill idno');
      //  }
      //  else if(name == '') {
      //   e.preventDefault( e );
      //   alert('Please Fill The name');
      //  }
      // else{
      //   // post to controller
      // }
      var hasError = false;
      $(this).find('.requiredField').each(function () {
        if ($.trim($(this).val()) == '') {
          var labelText = $(this).prev('label').text();
          $(this).parent().append('<span class="error">Please enter ' + labelText + '</span>');
          $(this).parent().addClass('inputError');
          hasError = true;
        } else if ($(this).hasClass('email')) {
          var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          if (!emailReg.test($.trim($(this).val()))) {
            var labelText = $(this).prev('label').text();
            $(this).parent().append('<span class="error">You have entered an invalid ' + labelText + '</span>');
            $(this).parent().addClass('inputError');
            hasError = true;
          }
        } else if ($(this).hasClass('checkbox')) {
          if (!$(this).is(':checked')) {
            $(this).parent().append('<span class="error">You must agree to the Terms & Conditions</span>');
            $(this).parent().addClass('inputError');
            hasError = true;
          }
        } else if ($(this).hasClass('captcha')) {
          if ($(this).val() != 'red' && $(this).val() != 'Red') {
            $(this).parent().append('<span class="error">You have entered wrong Captcha Value</span>');
            $(this).parent().addClass('inputError');
            hasError = true;
          }
        }
      });
      if (hasError == true) {
        e.preventDefault();
      } else {
        // post to controller
      }
    });
  },
  getPreview : function (){
    All.set_disable_button();
        $.post(All.get_url("ticketing/preview") , $("#qcTcktForm").serialize(), function(hasil)
        {
            $("#qcTcktRegForm").hide();
            $("#resTEs").html(hasil);
        });
  }
}

jQuery(document).ready(function($){
  if($('#qcTcktForm').length) {
    //console.log('ready..');
    Frm.validatee();
  }
});
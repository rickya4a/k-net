var $ = jQuery;

var memberlp = {
	getMemberNameByID : function(idx, setToDiv) {
   	    All.set_disable_button();
		$.ajax({
            url: All.get_url("cart/member/id/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                All.set_enable_button();
                if(data.sukses == true || data.sukses == "true") {
                	$(setToDiv).val(data.arrayData[0].fullnm);
				} else {
					alert("Tidak ada data member dengan ID "+idx.value);
					$(setToDiv).val(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
   },
    
   show_stk : function (idx,setToDiv)
    {   
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/getStockist/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	var stkname = $("#"+idx.id+" :selected").text(); 
            	
            	$(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
					$("#nama_stkarea").val(stkname);
					$("#nama_stockistr1area").val(stkname);
					var arrayData = data.arrayData;
					$(setToDiv).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.loccd+">"+value.loccd+ " - " +value.fullnm+"</option>";
                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
                        
	                });
                	$(setToDiv).append(rowhtml);
                    
				} else {
					alert("Tidak ada data stokis untuk area "+stkname);
					$(setToDiv).html(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    
    show_kota : function (idx, setToDiv)
    {
        All.set_disable_button();
        var txt = $("#"+idx.id+" :selected").text(); 
	    $(setToDiv).attr("disabled", "disabled");
	    if(idx.value == "select") {
	    	alert("Silahkan pilih provinsi terlebih dahulu");
	    } else {
			$.ajax({
	            url: All.get_url("shipping/reg/kota/") +idx.value,
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){
	            	
					
	                $(setToDiv).removeAttr("disabled", "disabled");
	                All.set_enable_button();
	                if(data.response == "true") {
						var arrayData = data.arrayData;
						$("#nama_provinsi").val(txt);
						$(setToDiv).html(null);
		                var rowhtml = "<option value=''>--Select here--</option>";
		                $.each(arrayData, function(key, value) {
		                   
		                    rowhtml += "<option  value="+value.kode_kabupaten+">" +value.kabupaten.toUpperCase()+"</option>";
	                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
	                        
		                });
	                	$(setToDiv).append(rowhtml);
	                	$("#kecamatan").val(null);
	                	$("#stockistref").val(null);
	                	$("#sender_address").val(null);
	                    $("#destination_address").val(null);
					} else {
						$(setToDiv).append(null);
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	     }   
    },
    
    show_kota2 : function (idx, setToDiv)
    {
        All.set_disable_button();
        var txt = $("#"+idx.id+" :selected").text(); 
	    $(setToDiv).attr("disabled", "disabled");
	    if(idx.value == "select") {
	    	alert("Silahkan pilih provinsi terlebih dahulu");
	    } else {
			$.ajax({
	            url: All.get_url("shipping/kota/") +idx.value,
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){
	            	
					
	                $(setToDiv).removeAttr("disabled", "disabled");
	                All.set_enable_button();
	                if(data.response == "true") {
						var arrayData = data.arrayData;
						$("#nama_provinsi1").val(txt);
						$(setToDiv).html(null);
		                var rowhtml = "<option value=''>--Select here--</option>";
		                $.each(arrayData, function(key, value) {
		                   
		                    rowhtml += "<option  value="+value.kode_kabupaten+">" +value.kabupaten.toUpperCase()+"</option>";
	                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
	                        
		                });
	                	$(setToDiv).append(rowhtml);
	                	$("#kecamatan1").val(null);
	                	$("#nama_stockist").val(null);
	                	$("#sender_address").val(null);
	                    $("#destination_address").val(null);
					} else {
						$(setToDiv).append(null);
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	     }   
    },
    
    back_to_cart : function(clear_div, show_div) {
        
        $(clear_div).html(null);
        $(show_div).show();
        var pricecode = $("#pricecode").val();
        if(pricecode == "12E3") {
            $(".westP").css("display", "none");
            $(".eastP").css("display", "block");
			
        } else {
             $(".westP").css("display", "block");
            $(".eastP").css("display", "none");
        }
    },
    
    show_kecamatan : function (idx,setToDiv)
    {
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/kecamatan/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                var txt = $("#"+idx.id+" :selected").text(); 
                $(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
                	$("#nama_kota").val(txt);
					var arrayData = data.arrayData;
					$(setToDiv).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.kode_kecamatan+">" +value.kecamatan.toUpperCase()+"</option>";
                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
                        
	                });
                	$(setToDiv).append(rowhtml);
                	$("#kelurahan").html(null);
                    
				} else {
					$(setToDiv).html(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    show_kecamatan2 : function (idx,setToDiv)
    {
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/kecamatan/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                var txt = $("#"+idx.id+" :selected").text(); 
                $(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
                	$("#nama_kota1").val(txt);
                	
					var arrayData = data.arrayData;
					$(setToDiv).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.kode_kecamatan+">" +value.kecamatan.toUpperCase()+"</option>";
                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
                        
	                });
                	$(setToDiv).append(rowhtml);
                	$("#nama_kecamatan1").html(null);
                	$("#stockist").html(null);
                	$("#nama_stockist").html(null);
                    
				} else {
					$(setToDiv).html(null);
					$("#nama_kecamatan1").html(null);
                	$("#stockist").html(null);
                	$("#nama_stockist").html(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    show_kelurahan  : function (idx,setToDiv)
    {
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/kelurahan/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	var txt = $("#"+idx.id+" :selected").text(); 
                //$(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.listStk != null) {
                	$("#nama_kecamatan").val(txt);
                	$("#destination_address").val(data.arrayData[0].kode_kec_JNE);
					var arrayData = data.listStk;
					$(setToDiv).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.loccd+">"+value.loccd+" - " +value.fullnm.toUpperCase()+" - "+value.state.toUpperCase()+"</option>";
                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
                        
	                });
                	$(setToDiv).append(rowhtml); 
                	$(setToDiv).removeAttr("disabled");
                    
				} else {
					//$(setToDiv).html(null);
					$("#nama_kecamatan").val(txt);
					$(setToDiv).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                
                	$(setToDiv).append(rowhtml); 
                	$(setToDiv).removeAttr("disabled");
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    show_kelurahan2  : function (idx,setToDiv)
    {
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/kelurahan/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	var txt = $("#"+idx.id+" :selected").text(); 
                //$(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.listStk != null) {
                	$("#nama_kecamatan1").val(txt);
                	$("#destination_address").val(null);
					var arrayData = data.listStk;
					$(setToDiv).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.loccd+">"+value.loccd+" - " +value.fullnm.toUpperCase()+" - "+value.state.toUpperCase() +"</option>";
                        //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";
                        
	                });
                	$(setToDiv).append(rowhtml); 
                	$(setToDiv).removeAttr("disabled");
                    
				} else {
					//$(setToDiv).html(null);
					$("#nama_kecamatan1").val(txt);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    chooseKelurahan : function(idx) {
    	var txt = $("#"+idx.id+" :selected").text();
    	var xx = txt.split(" - ");
    	$("#nama_kelurahan").val(xx[1]);
    	$("#kodepos").val(xx[0]);
    },
    
    
    show_pricestkX : function (idx,setToDiv){
    	var kec = $("#kecamatan1").val();
    	//alert(kec);
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/getPricecodeX/") +idx.value +"/" +kec,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                var stkname = $("#"+idx.id+" :selected").text(); 
                $(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
				    $("#nama_stockist").val(stkname);
					$("#nama_stockistr1ref").val(stkname);
					$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
					$("#jne_branch").val(data.arrayData[0].jne_branchcd);
					//console.log(data.arrayData[0].kode_kec_JNE_Origin);
                	$(setToDiv).val(data.arrayData[0].pricecode);
                    if(data.arrayData[0].pricecode == "12E3") {
                        $(".westP").css("display", "none");
                        $(".eastP").css("display", "block");
						
                    } else {
                         $(".westP").css("display", "block");
                        $(".eastP").css("display", "none");
                    }
                    Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
				} else {
					alert("Price code tidak ada");
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    show_pricestkY : function (idx,setToDiv){
    	var kec = $("#kecamatan").val();
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/reg/getPricecodeX/") +idx.value +"/" +kec,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                var stkname = $("#"+idx.id+" :selected").text(); 
                $(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
				    $("#nama_stockist").val(stkname);
					$("#nama_stockistr1ref").val(stkname);
					$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
					$("#jne_branch").val(data.arrayData[0].jne_branchcd);
					//console.log(data.arrayData[0].kode_kec_JNE_Origin);
                	$(setToDiv).val(data.arrayData[0].pricecode);
                    if(data.arrayData[0].pricecode == "12E3") {
                        $(".westP").css("display", "none");
                        $(".eastP").css("display", "block");
						
                    } else {
                         $(".westP").css("display", "block");
                        $(".eastP").css("display", "none");
                    }
                    //Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
				} else {
					alert("Price code tidak ada");
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    show_pricestk : function (idx,setToDiv){
        All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/reg/getPricecode/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                var stkname = $("#"+idx.id+" :selected").text(); 
                $(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
				    $("#nama_stockist").val(stkname);
					$("#nama_stockistr1ref").val(stkname);
					$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
					$("#jne_branch").val(data.arrayData[0].jne_branchcd);
					//console.log(data.arrayData[0].kode_kec_JNE_Origin);
                	$(setToDiv).val(data.arrayData[0].pricecode);
                    if(data.arrayData[0].pricecode == "12E3") {
                        $(".westP").css("display", "none");
                        $(".eastP").css("display", "block");
						
                    } else {
                         $(".westP").css("display", "block");
                        $(".eastP").css("display", "none");
                    }
                    //Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
				} else {
					alert("Price code tidak ada");
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    
    show_pricestk2 : function (idx,setToDiv){
    	All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
            url: All.get_url("shipping/reg/getPricecode/") +idx.value,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                var stkname = $("#"+idx.id+" :selected").text(); 
                var stn = stkname.split(" - ");
                var sdf = stn[0]+" - "+stn[1];
                $(setToDiv).removeAttr("disabled", "disabled");
                All.set_enable_button();
                if(data.response == "true") {
				    $("#nama_stockist").val(sdf);
					$("#nama_stockistr1ref").val(sdf);
					$("#state1").val(stn[2]);
					$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
					$("#jne_branch").val(data.arrayData[0].jne_branchcd);
					//console.log(data.arrayData[0].kode_kec_JNE_Origin);
                	$(setToDiv).val(data.arrayData[0].pricecode);
                    if(data.arrayData[0].pricecode == "12E3") {
                        $(".westP").css("display", "none");
                        $(".eastP").css("display", "block");
						
                    } else {
                         $(".westP").css("display", "block");
                        $(".eastP").css("display", "none");
                    }
                    //Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
				} else {
					alert("Price code tidak ada");
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },	 
    
    getDelChoice : function (nilai){
        if(nilai == "1") {
            $('.diambil').show();
            $('.dikirim').hide();
            
        } else {
            $('.diambil').hide();
            $('.dikirim').show();
        }
    },
    
    addToCart : function(param) {
        All.set_disable_button();
    	var prdcd = $("#prdcd" +param).val();
    	var prdnm = $("#prdnm" +param).val();
    	var bv = $("#bv" +param).val();
    	var qty = $("#qty" +param).val();
    	var westPrice = $("#westPrice" +param).val();
    	var eastPrice = $("#eastPrice" +param).val();
    	var weight = $("#weight" +param).val();
    	$.ajax ({
            url: All.get_url("cart/add"),
            type: 'POST',
			dataType: 'json',
			data: {prdcd: prdcd, prdnm: prdnm, bv: bv, qty: qty, westPrice: westPrice, eastPrice: eastPrice, weight : weight},
            success:
            function(data){  
                All.set_enable_button();
            alert(data.message);
	            if(data.response == "true") {
	            	var maxID = parseInt($("#maxID").val());
			    	var nextMaxID = maxID + 1;
			    	$("#maxID").val(nextMaxID);
	            	$("#showInfoSumCart").html(null);
	            	var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
	            	$("#showInfoSumCart").append(row);
	            }
            
         },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
         });   
    },
    
    delete_rowCart : function (param){
        All.set_disable_button();
         var rowid = $("#rowid" +param).val();
         var qty = parseInt($("#qty" +param).val());
         var totQtyWest = parseInt($("#totQtyWest").val());
         var pricecode = $("#pricecode").val();
         $.ajax({
             dataType: 'json',   
             type: "POST",
             url : All.get_url("cart/remove/"),
             data: {rowid: rowid},
             success: function(data)
             {
                All.set_enable_button();
                alert(data.message);
                if(data.response == "true")
                {
                    /*var newQty = totQtyWest - qty;
                    $("#divTotQtyWest").html(null);
            		$("#divTotQtyEast").html(null);
                    $("#divTotQtyWest").html(All.num(newQty));
                    $("#divTotQtyEast").html(All.num(newQty));
                    $("#totQtyWest").val(newQty);
                    $("#totQtyEast").val(newQty); */
                   
                    Shopping.recalculateItems2(param);
                    $("div#" +param).remove();
                    //Shopping.setNewPrice(param);
                    
                    Shopping.updatePriceAtHeader(pricecode);
                }
                
             }
         });
    },
    
    recalculateItems2 : function(param) {
    	var qty = parseInt($("#qty" +param).val());
    	var westPrice = parseInt($("#amountWest" +param).val());
    	var eastPrice = parseInt($("#amountEast" +param).val());
    	var totBv = parseInt($("#totBv").val());
    	var totQtyWest = parseInt($("#totQtyWest").val());
    	var totalAmountWest = parseInt($("#totalAmountWest").val());
    	var totQtyEast = parseInt($("#totQtyEast").val());
    	var totalAmountEast = parseInt($("#totalAmountEast").val());
    	
    	var selisih_barat = totalAmountWest - westPrice;
    	var selisih_timur = totalAmountEast - eastPrice;
    	var selisih_qty = totQtyWest - qty;
    	//alert("Selisih barat : " +selisih_barat+ ", selisih timur : " +selisih_timur);
    	
    	$("#totalAmountWest").val(selisih_barat);
        $("#totalAmountEast").val(selisih_timur);
        $("#totQtyWest").val(selisih_qty);
        $("#totQtyEast").val(selisih_qty);
        $("#divTotQtyWest").html(null);
        $("#divTotQtyEast").html(null);
        $("#divTotQtyWest").html(selisih_qty);
        $("#divTotQtyEast").html(selisih_qty);
        $("#totAllWestPrice").html(null);
        $("#totAllEastPrice").html(null);
        $("#totAllItemWest").html(null);
        $("#totAllItemEast").html(null);
        $("#totAllWestPrice").text(All.num(selisih_barat));
        $("#totAllEastPrice").text(All.num(selisih_timur));
        $("#totAllItemWest").text(All.num(selisih_qty));
        $("#totAllItemEast").text(All.num(selisih_qty));
    	
    	
    },
    
    recalculateItems : function() {
    	var jmlAmt = $(".amtW").length;
    	//alert(jmlAmt);
    	var totWest = 0;
        var totEast = 0;
        var totQtyWest = 0;
        var totQtyEast = 0;
     
    	for(var i = 1; i <= jmlAmt; i++) {
            totWest += parseInt($("#amountWest" +i).val());
            totEast += parseInt($("#amountEast" +i).val());
            totQtyWest += parseInt($("#qty" +i).val());
            totQtyEast += parseInt($("#qty" +i).val());
        }
        
        $("#totalAmountWest").val(totWest);
            $("#totalAmountEast").val(totEast);
            $("#totQtyWest").val(totQtyWest);
            $("#totQtyEast").val(totQtyEast);
            $("#divTotQtyWest").html(null);
            $("#divTotQtyEast").html(null);
            $("#divTotQtyWest").html(totQtyWest);
            $("#divTotQtyEast").html(totQtyEast);
            $("#totAllWestPrice").html(null);
            $("#totAllEastPrice").html(null);
            $("#totAllItemWest").html(null);
            $("#totAllItemEast").html(null);
            $("#totAllWestPrice").text(All.num(totWest));
            $("#totAllEastPrice").text(All.num(totEast));
            $("#totAllItemWest").text(All.num(totQtyWest));
            $("#totAllItemEast").text(All.num(totQtyEast));
    },
    
    setNewPrice : function(param) {
    	var qty = parseInt($("#qty" +param).val());
    	var nm = /^[0-9]+$/;
        if($("#qty" +param).val().match(nm) && qty > 0)
        {
    	
    	
	    	var westPrice = $("#westPrice" +param).val();
	        var eastPrice = $("#eastPrice" +param).val();
	        $("#amountWest" +param).val(qty * westPrice);
	        $("#amountEast" +param).val(qty * eastPrice);
	        $("#divSubTotWestPrice" +param).html(null);
	        $("#divSubTotEastPrice" +param).html(null);
	        $("#divSubTotWestPrice" +param).text(All.num(qty * westPrice));
	        $("#divSubTotEastPrice" +param).text(All.num(qty * eastPrice));
	        
	        var totQty = 0;
	        
	        var totWest = 0;
	        var totEast = 0;
	        
	        $('input[name="qty[]"]').each(function(){
			totQty += parseInt($(this).val());
			});
			
			$('input[name="amountWest[]"]').each(function(){
			totWest += parseInt($(this).val());
			});
			
			$('input[name="amountEast[]"]').each(function(){
			totEast += parseInt($(this).val());
			});
			//alert("totQty :" +totQty+ " west :" +totWest+ " east :" +totEast);
			
	        $("#totalAmountWest").val(totWest);
	        $("#totalAmountEast").val(totEast);
	        $("#totQtyWest").val(totQty);
	        $("#totQtyEast").val(totQty);
	        $("#divTotQtyWest").html(null);
	        $("#divTotQtyEast").html(null);
	        $("#divTotQtyWest").html(totQty);
	        $("#divTotQtyEast").html(totQty);
	        $("#totAllWestPrice").html(null);
	        $("#totAllEastPrice").html(null);
	        $("#totAllItemWest").html(null);
	        $("#totAllItemEast").html(null);
	        $("#totAllWestPrice").text(All.num(totWest));
	        $("#totAllEastPrice").text(All.num(totEast));
	        $("#totAllItemWest").text(All.num(totQty));
	        $("#totAllItemEast").text(All.num(totQty));
	        
	        var pricecode = $("#pricecode").val();
	        Shopping.updatePriceAtHeader(pricecode); 
	    } else {
	    	alert('Tidak boleh huruf atau angka nol');
            $("#qty" +param).val(1);
            $("#qty" +param).focus();
	    }    
    },
    
    setNewPrice2 : function(param) {
    
        var qty = parseInt($("#qty" +param).val());
        var amountWest = parseInt($("#amountWest" +param).val());
        var westPrice = $("#westPrice" +param).val();
        var eastPrice = $("#eastPrice" +param).val();
        $("#amountWest" +param).val(qty * westPrice);
        $("#amountEast" +param).val(qty * eastPrice);
        $("#divSubTotWestPrice" +param).html(null);
        $("#divSubTotEastPrice" +param).html(null);
        $("#divSubTotWestPrice" +param).text(All.num(qty * westPrice));
        $("#divSubTotEastPrice" +param).text(All.num(qty * eastPrice));
        var jmlAmt = $(".amtW").length;
        var totWest = 0;
        var totEast = 0;
        var totQtyWest = 0;
        var totQtyEast = 0;
        //alert(jmlAmt);
        for(var i = 1; i <= jmlAmt; i++) {
            totWest += parseInt($("#amountWest" +i).val());
            totEast += parseInt($("#amountEast" +i).val());
            totQtyWest += parseInt($("#qty" +i).val());
            totQtyEast += parseInt($("#qty" +i).val());
        }
        if(isNaN(qty)){
            ("#qty" +i).val(1);
            $("#totalAmountWest").val(0);
            $("#totalAmountEast").val(0);
            $("#totQtyWest").val(0);
            $("#totQtyEast").val(0); 
        }else{
            $("#totalAmountWest").val(totWest);
            $("#totalAmountEast").val(totEast);
            $("#totQtyWest").val(totQtyWest);
            $("#totQtyEast").val(totQtyEast);
            $("#divTotQtyWest").html(null);
            $("#divTotQtyEast").html(null);
            $("#divTotQtyWest").html(totQtyWest);
            $("#divTotQtyEast").html(totQtyEast);
            $("#totAllWestPrice").html(null);
            $("#totAllEastPrice").html(null);
            $("#totAllItemWest").html(null);
            $("#totAllItemEast").html(null);
            $("#totAllWestPrice").text(All.num(totWest));
            $("#totAllEastPrice").text(All.num(totEast));
            $("#totAllItemWest").text(All.num(totQtyWest));
            $("#totAllItemEast").text(All.num(totQtyEast));
              
        }
        var pricecode = $("#pricecode").val();
        Shopping.updatePriceAtHeader(pricecode); 
    },
    
    updateCart : function (){
        All.set_disable_button();
         $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("cart/update") , $("#formCart").serialize(), function(data)
            {  
                All.set_enable_button();
                //$("input[type=button]").removeAttr('disabled');
                alert("Shopping Cart telah di update");
                $("#listCartPrd").html(null);
                $("#listCartPrd").html(data);   
            }).fail(function() { 
                alert("Error requesting page"); 
                //All.set_enable_button();
                $("input[type=button]").removeAttr('disabled');
            });  
           
         //alert("pricecode : " +$("#pricecode").val()); 
    },
    
    updateCart2 : function() {
    	All.set_disable_button();
         $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("cart/upd") , $("#formCart").serialize(), function(data)
            {  
                All.set_enable_button();
                //$("input[type=button]").removeAttr('disabled');
                if(data.response == "true") {
                	alert("Shopping Cart telah di update");
                }  
            },"json").fail(function() { 
                alert("Error requesting page"); 
                //All.set_enable_button();
                $("input[type=button]").removeAttr('disabled');
            });  
    },
    
    validateCheckOut : function(form) {
    	var del = $('input[name=delivery]').is(':checked');
        var update = $("#updateCart").val();
        var val = $('input:radio[name=delivery]:checked').val();
        var err = 0;
        if(!del) {
            alert("Silahkan Pilih Tipe Pengiriman");
            //$("#submits").attr("disabled", "disabled");
            $('input[name=delivery]').focus();
            err++;
            //alert("isi err :" +err);	
            return false;
        } else {
             //alert("Pilihan :" +val);
             //return false;	
                
		        if(val == "1") {
		        	var provinsi1 = $("#provinsi1").val();
		        	var kota1 = $("#kota1").val()
		        	var kecamatan1 = $("#kecamatan1").val();
		        	var stockist = $("#stockist").val();
		        	//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
		        	/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
		        		alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
		        		err++;
		        		return false; 
		        	} */
		        	if(provinsi1 == "" || provinsi1 == null || kota1 == "" || 
		        	    kota1 == null || kecamatan1 == "" || kecamatan1 == null ||
		        	    stockist == "" || stockist == null) {
		        		err++;
		        		alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
		        	} 
		        	
		        }
		        
		        else {
		        	var nama_penerima = $("#nama_penerima").val();
		        	//var lastname = $("#lastname").val();
		        	var notlp = $("#notlp").val();
		        	//var stkarea1 = $("#stkarea1").val();
		        	var stockistref = $("#stockistref").val();
		        	var provinsi = $("#provinsi").val();
		        	var kota = $("#kota").val();
		        	var kecamatan = $("#kecamatan").val();
		        	//var kelurahan = $("#kelurahan").val();
		        	var alamat = $("#alamat").val();
		        	
		        	if(nama_penerima == "" || nama_penerima == null || notlp == "" || notlp == null
		        	  || stockistref == "" || stockistref == null || provinsi == "" || provinsi == null
		        	  || kota == "" || kota == null || kecamatan == "" || kecamatan == null
		        	  || alamat == "" || alamat == null) {
		        		err++;
		        	} 
		        	
		         } 
		  }
         
          
          if(err > 0) {
          	alert("Silahkan lengkapi data anda sebelum melanjutkan");
          	return false;
          } else {
          	return true;
          }
    },
    
    checkout : function ()
    {
       /*var del = $('input[name=delivery]').is(':checked');
        var update = $("#updateCart").val();
        
        if(!del)
        {
            alert("Silahkan Pilih Tipe Pengiriman");
            $("#submits").attr("disabled", "disabled");
        }
        else{
        */	
        if(Shopping.validateCheckOut()) {
            /*Shopping.updateCart();
            $("#submits").removeAttr("disabled");
            $.post(All.get_url("cart/checkout") , $("#formCart").serialize(), function(data)
            {  
                $("#submits").removeAttr("disabled");
                
                $("#formCart1").hide();
                $("#divCheckOut").html(null);
                $("#divCheckOut").html(data); 
                  
            }).fail(function() { 
                alert("Error requesting page"); 
                //All.set_enable_button();
                $("#submits").attr("disabled", "disabled");
            });  */
           alert("validasi OK..!!")
         }    
        //}
    },
	
	updatePriceAtHeader : function(pricecode) {
		var totQtyWest = $("#totQtyWest").val();
		var totalAmountEast = $("#totalAmountEast").val();
		var totalAmountWest = $("#totalAmountWest").val();
		$("#showInfoSumCart").html(null);
		var row = "";
		if(pricecode == "12W3") {
	        row += "&nbsp;&nbsp;&nbsp;&nbsp;"+ totQtyWest + "&nbsp;&nbsp; items | Rp. " + All.num(totalAmountWest);       
		} else {
		    row += "&nbsp;&nbsp;&nbsp;&nbsp;"+ totQtyWest + "&nbsp;&nbsp; items | Rp. " + All.num(totalAmountEast);	       
		}
		$("#showInfoSumCart").append(row);
	
	},
    
    getProdByCatHeader : function(param){
        var prdcat = param.id;
        //var offset = param.rel;
        $('#listprd').html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
        All.set_disable_button();
        
        $.ajax({
            url: All.get_url("shop/productHeader/cat/id/") +prdcat,
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
				$('#listprd').html(null);
                $('#listprd').html(data);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        }); 
    },
    
    
    getProdByCat : function (param)
    {
        var prdcat = param.id;
        var offset = param.rel;
        $('#listprd').html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
        All.set_disable_button();
        
        $.ajax({
            url: All.get_url("shop/product/cat/id/") +prdcat,
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
				$('#listprd').html(null);
                $('#listprd').html(data);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        }); 
        
    },
    
    getProdByCat2 : function (param)
    {
        var prdcat = param.id;
        var offset = param.rel;
        $('#listprd').html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
        All.set_disable_button();
        if(offset == "1") {
        	$.ajax({
	            url: All.get_url("shop/product/cat/id/") +prdcat,
	            type: 'GET',
	            success:
	            function(data){
	                All.set_enable_button();
	                
					$('#listprd').html(null);
	                $('#listprd').html(data);
	                
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            }); 
        } else {
        
	        $.ajax({
	            url: All.get_url("shop/product/cat/id/") +prdcat+"/"+offset,
	            type: 'GET',
	            success:
	            function(data){
	                All.set_enable_button();
	                
					$('#listprd').html(null);
	                $('#listprd').html(data);
	                
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        }); 
        }
    },
    
    proceedPayment : function() {
    	    $("#checkout").removeAttr("disabled");
            $.post(All.get_url("payment") , $("#proceedPayment").serialize(), function(data)
            {  
                $("#checkout").removeAttr("disabled");
                
                $("#paymentDiv").hide();
                $("#nextPaymentProcessDIV").html(null);
                $("#nextPaymentProcessDIV").html(data); 
                  
            }).fail(function() { 
                alert("Error requesting page"); 
                //All.set_enable_button();
                $("#checkout").attr("disabled", "disabled");
            });  
    },
    
    getTracking : function(){
        //alert("masuk sini");
        
        $.post(All.get_url("tracking/process") , $("#tracking_awb").serialize(), function(data)
        {  
            var rowhtml = "";
            var obj = $.parseJSON(data);
            //$("#resultAwb").html(obj.error);
              if(obj.status == false) {
                $("#resultAwb").html(obj.error);
              } /*else {
                $.each(obj, function(key, value) {
                    //rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'> ";
                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.+"</div> ";
                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.+"</div> ";
                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.+"</div> ";
                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.+"</div> ";
                    //rowhtml += "</div> ";
                });
                $("#listPricePrd").append(rowhtml);
               
              }*/
         
        });
    },
    
    getAccountPage : function(param) {
    	$('#acc_div').html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
    	$.ajax({
	            url: All.get_url(param),
	            type: 'GET',
	            success:
	            function(data){
	                All.set_enable_button();
	                
					$("#acc_div").html(null);
	                $('#acc_div').html(data);
	                
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            }); 
    	
    },
    
    showProductDetail : function(param) {
    	$.ajax({
	            url: All.get_url("shop/prodDet/") +param,
	            type: 'GET',
	            success:
	            function(data){
	                All.set_enable_button();
	                
					$("#listProductDiv").hide();
					$("#detailProductDiv").html(null);
	                $('#detailProductDiv').html(data);
	                
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            });
    },
    
    backToListProduct: function() {
    	$("#detailProductDiv").html(null);
    	$("#listProductDiv").show();
    },
    
    getMemberInfo : function(nilai, setTo) {
    	/*$("#submits").attr("disabled", "disabled");
    	$.ajax({
	            url: All.get_url("member/id/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                if(data.response === "true") {
	                 $('#' +setTo).val(data.arrayData[0].fullnm);
	                 $("#submits").removeAttr("disabled");
	                } else {
	                	alert("invalid ID member");
	                }
					
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            }); */
         $("#submits").attr("disabled", "disabled");
    	$.ajax({
	            url: All.get_url("member/id/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                if(data.response === "true") {
	                 $('#' +setTo).val(data.sponsorname);
	                 $("#submits").removeAttr("disabled");
	                } else if(data.response == "err"){
	                	alert("invalid ID member");
	                	$('#' +setTo).val(null);
	                } else {
	                	alert(data.message);
	                	$('#' +setTo).val(data.sponsorname);
	                 	$("#submits").removeAttr("disabled");
	                }
					
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            });
    },
    
    validateInputMember : function() {
    	var x = 0;
    	$("#formMember .required").each(function() {
            if(this.value == "" || this.value == null) {               
                x++;            }
        });
        
        if(x > 0) {
        	alert("Masih ada inputan yang kosong..");
        	return false;
        }  else {
        	return true;
        }
    },	
    
    validateStarterKit : function() {
    	var del = $('input[name=delivery]').is(':checked');
        var update = $("#updateCart").val();
        var val = $('input:radio[name=delivery]:checked').val();
        var pilihSK = $("#pilStarterkit").val();
        var err = 0;
        if(!del) {
            alert("Silahkan Pilih Tipe Pengiriman");
            //$("#submits").attr("disabled", "disabled");
            $('input[name=delivery]').focus();
            err++;
            //alert("isi err :" +err);	
            return false;
        } else {
             //alert("Pilihan :" +val);
             //return false;	
                
		        if(val == "1") {
		        	var provinsi1 = $("#provinsi1").val();
		        	var kota1 = $("#kota1").val()
		        	var kecamatan1 = $("#kecamatan1").val();
		        	var stockist = $("#stockist").val();
		        	//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
		        	/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
		        		alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
		        		err++;
		        		return false; 
		        	} */
		        	if(provinsi1 == "" || provinsi1 == null || kota1 == "" || 
		        	    kota1 == null || kecamatan1 == "" || kecamatan1 == null ||
		        	    stockist == "" || stockist == null) {
		        		err++;
		        		//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
		        	} 
		        	
		        }
		        
		        else {
		        	var nama_penerima = $("#nama_penerima").val();
		        	//var lastname = $("#lastname").val();
		        	var notlp = $("#notlp").val();
		        	//var stkarea1 = $("#stkarea1").val();
		        	var stockistref = $("#stockistref").val();
		        	var provinsi = $("#provinsi").val();
		        	var kota = $("#kota").val();
		        	var kecamatan = $("#kecamatan").val();
		        	//var kelurahan = $("#kelurahan").val();
		        	var alamat = $("#alamat").val();
		        	
		        	if(nama_penerima == "" || nama_penerima == null || notlp == "" || notlp == null
		        	  || stockistref == "" || stockistref == null || provinsi == "" || provinsi == null
		        	  || kota == "" || kota == null || kecamatan == "" || kecamatan == null
		        	  || alamat == "" || alamat == null) {
		        		err++;
		        	} 
		        	
		         } 
		  }
         
          if(pilihSK == "" || pilihSK == null) {
	    	//alert("Silahkan Pilih Starterkit");
            //$("#submits").attr("disabled", "disabled");
            //$("#pilStarterkit").focus();
            err++;
	      } 
          
          if(err > 0) {
          	alert("Silahkan lengkapi data anda sebelum melanjutkan");
          	return false;
          } else {
          	return true;
          }
    },
    
    checkDoubleKTP : function(nilai) {
    	$("#submits").attr("disabled", "disabled");
    	$.ajax({
	            url: All.get_url("member/ktp/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                if(data.sukses === "TRUE") {
	                	alert(data.responseMessage);
	                	$("#idno").focus();
	                } else {
	                	$("#submits").removeAttr("disabled");
	                	//$("#membername").focus();
	                }
					
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            });
    },
    
    checkDoubleHP : function(nilai) {
    	$("#submits").attr("disabled", "disabled");
    	$.ajax({
	            url: All.get_url("member/no_hp/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                if(data.sukses === "TRUE") {
	                	alert(data.responseMessage);
	                	$("#tel_hp").focus();
	                } else {
	                	$("#submits").removeAttr("disabled");
	                	
	                }
					
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            });
    },
    
    checkAgeMember : function(nilai) {
    	var chk = memberlp.isValidDate(nilai);
    	if(chk != false) {
    		$("#submits").removeAttr("disabled");
    		var currentTime = new Date();
	    	var res = nilai.split("/");
	    	var selisih = parseInt(currentTime.getFullYear()) - parseInt(res[2]);
	    	//var thn = res[2].parseInt();
	    	if(selisih < 18) {
	    		alert("Minimum umur adalah 18 tahun..");
	    		$("#submits").attr("disabled", "disabled");
	    	} else {
	    		$("#submits").removeAttr("disabled");
	    	}
    	} else {
    		$("#submits").attr("disabled", "disabled");
    	}
    	/*if(memberlp.isValidDate(nilai)) {
    		
	    	var currentTime = new Date();
	    	var res = nilai.split("/");
	    	var selisih = parseInt(currentTime.getFullYear()) - parseInt(res[2]);
	    	//var thn = res[2].parseInt();
	    	if(selisih < 18) {
	    		alert("Minimum umur adalah 18 tahun..");
	    		$("#submits").attr("disabled", "disabled");
	    	} else {
	    		$("#submits").removeAttr("disabled");
	    	}
	    } */
	   
	    	
    },
    
    isValidDate : function(inputText)
	{
	    var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;  
		  // Match the date format through regular expression  
		  if(inputText.match(dateformat))  
		  {  
			  //document.form1.text1.focus();  
			  //Test which seperator is used '/' or '-'  
			  var opera1 = inputText.split('/');  
			 // var opera2 = inputText.split('-');  
			  lopera1 = opera1.length;  
			  //lopera2 = opera2.length;  
			  // Extract the string into month, date and year  
			  if (lopera1>1)  
			  {  
			  	var pdate = inputText.split('/');  
			  }  
			  /*else if (lopera2>1)  
			  {  
			  	var pdate = inputText.split('-');  
			  } */ 
			  
			  var dd = parseInt(pdate[0]);  
			  var mm  = parseInt(pdate[1]);  
			  var yy = parseInt(pdate[2]);  
		     // Create list of days of a month [assume there is no leap year by default]  
			  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  
			  if (mm==1 || mm>2)  
			  {  
			  	if (dd>ListofDays[mm-1])  
				  {  
					  alert('Invalid date format!');  
					  return false;  
				  }  
		  	  }  
		  	  
			  if (mm==2)  
			  {  
			  	var lyear = false;  
		  		if ( (!(yy % 4) && yy % 100) || !(yy % 400))   
				  {  
				    lyear = true;  
				  }  
				  
				  if ((lyear==false) && (dd>=29))  
				  {  
				  	alert('Format tanggal invalid');  
				  	return false;  
				  }  
				  if ((lyear==true) && (dd>29))  
				  {  
					  alert('Format tanggal invalid');  
					  return false;  
				  }  
		      }  
		  }  
		  else  
		  {  
			  alert("Format tanggal invalid");  
			  //document.form1.text1.focus();  
			  return false;  
		  }  
	},
    
    chooseSK : function(nilai) {
    	/*var res = nilai.split("|");
    	$("#infoSK").html(null);
    	if(nilai !== "") {
	    	$("#infoSK").css("display", "block");
	    	var text = "<br />Kode Produk : " + res[0] + "<br />";
	    	text += "Nama Produk  : " + res[1] + "<br />";
	    	text += "Berat Produk : " + res[2] + "<br />";
	    	text += "Harga Barat  : " + res[3] + "<br />";
	    	text += "Harga Timur  : " + res[4] + "<br />";
    		$("#infoSK").html(text);
    	} else {
    		$("#infoSK").css("display", "none");
    	}*/
    	
    	
    	var res = nilai.split("|");
    	$("#infoSK").html(null);
    	if(nilai !== "") {
	    	$("#infoSK").css("display", "block");
	    	var text = "<br />Kode Produk : " + res[0] + "<br />";
	    	text += "Nama Produk  : " + res[1] + "<br />";
	    	text += "Berat Produk : " + res[2] + "<br />";
	    	text += "Harga Barat  : " + res[3] + "<br />";
	    	text += "Harga Timur  : " + res[4] + "<br />";
    		$("#infoSK").html(text);
    		
    		if(res[5] === "0") {
    			alert("Starterkit jenis ini tidak akan dikirim ke alamat,silahkan pilih opsi 'Diambil di stockist' ..");
    			//$("input:radio[name='delivery']").val(2);
    			//$('input[name="delivery"]').val("1");
    			//$(".dikirim").css("display", "none");
    			$("#is_charge").val(res[5]);
    		} else {
    			$("#is_charge").val(1);
    		}
    	} else {
    		$("#infoSK").css("display", "none");
    	}
    }
}
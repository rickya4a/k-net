var $ = jQuery;

var Sales = {

    searchTransaksi : function()
    {
        var tahun = $('#tahun').val();
        /*var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);*/
        if(tahun == ""){
            alert('Field Tahun Bonus Tidak Boleh Kosong');
            //All.set_disable_button();
        }
        /*else if(!objRegthn){
         alert('Format Tahun Harus berupa Angka');
         }*/
        else
        {
            All.set_disable_button();
            $("#hasilPencarian").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("sales/cek/transaksi") , $("#cektransaksi").serialize(), function(hasil)
            {
                All.set_enable_button();

                $("#hasilPencarian").html(null);
                $("#hasilPencarian").html(hasil);
            });
        }

    },


    searchTransaksiKnet : function()
    {
        var tahun = $('#tahun').val();
        /*var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);*/
        if(tahun == ""){
            alert('Field Tahun Bonus Tidak Boleh Kosong');
            //All.set_disable_button();
        }
        /*else if(!objRegthn){
         alert('Format Tahun Harus berupa Angka');
         }*/
        else
        {
            All.set_disable_button();
            $("#hasilPencarian").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("sales/cek/transOnline") , $("#cektransaksi").serialize(), function(hasil)
            {
                All.set_enable_button();
                $("#hasilPencarian").html(null);
                $("#hasilPencarian").html(hasil);
            });
        }

    },

    get_listing_ttp : function(trcd,tost,orderno,bln,thn){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/

        if(orderno == ""){
            orderno = "kosong";
        }
        //alert('isi nya ' +trcd+ '/ ' +orderno+ ' /' +tost);

        $("#formCekTransaksi").hide();
        $("#detail_trans").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("sales/detail/transaksi/" +trcd +"/" +orderno +"/" +tost +"/" +bln +"/" +thn ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detail_trans").html(null);
                    $("#detail_trans").html(data);
                }
        });
    },
    /////aldi klaim///////
    getDetailKlaim : function(trcd){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/


        $("#KlaimHeader").hide();
        $("#detKlaim").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("KlaimBonusPaket/getDetKw/" +trcd  ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detKlaim").html(null);
                    $("#detKlaim").html(data);
                }
        });
    },
    getDetKlaim : function(trcd){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/


        $("#KlaimHeader").hide();
        $("#detKlaim").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("KlaimBonusPaket/getDetKlaim/" +trcd  ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detKlaim").html(null);
                    $("#detKlaim").html(data);
                }
        });
    },
    getStockistKlaim : function(trcd,dfno){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/


        $("#KlaimHeader").hide();
        $("#detKlaim").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("KlaimBonusPaket/getformstockist/" +trcd+"/"+dfno  ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detKlaim").html(null);

                    $("#detKlaim").html(data);

                }
        });
    },
    getProdukKlaim : function(bnsperiod){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/


        $("#KlaimHeader").hide();
        $("#detKlaim").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("KlaimBonusPaket/getProdukKlaim/" +bnsperiod  ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detKlaim").html(null);

                    $("#detKlaim").html(data);

                }
        });
    },

    submitKlaim : function()
    {


        $("#hasilPencarian").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $("input[type=button]").attr('disabled', 'disabled');
        $.post(All.get_url("KlaimBonusPaket/Submit") , $("#formKlaim").serialize(), function(hasil)
        {
            if(hasil){
                if(confirm('Data berhasil disimpan')){
                    window.location.reload();
                }
            }
                else
            alert('Data gagal disimpan');

        });


    },
    backToKlaim : function(){
        $("#KlaimHeader").show();
        $("#detKlaim").html(null);
    },
    ///////aldhi cashback///////
    getDetailCashback : function(trcd){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/


        $("#CBHeader").hide();
        $("#CBdet").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("promo_cashback/getDetKw/" +trcd  ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#CBdet").html(null);
                    $("#CBdet").html(data);
                }
        });
    },
    backToCB : function(){
        $("#CBHeader").show();
        $("#CBdet").html(null);
    },
    ////////

    get_listing_ttpOnline : function(orderno,bln,thn){

        /*var trcd = $("#trcd" +param).val();
         var orderno = $("#orderno" +param).val();
         var tost = $("#xx" +param).val();*/

        if(orderno == ""){
            orderno = "kosong";
        }
        //alert('isi nya ' +trcd+ '/ ' +orderno+ ' /' +tost);

        $("#formCekTransaksi").hide();
        $("#detail_trans").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.ajax({
            url: All.get_url("sales/detail/transaksiOnline/"+orderno +"/" +bln +"/" +thn),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detail_trans").html(null);
                    $("#detail_trans").html(data);
                }
        });
    },

    backToSearch : function(){
        $("#formCekTransaksi").show();
        $("#detail_trans").html(null);
    },

    backToJaringan : function (){
        $("#hasilPencarian1").show();
        $("#detail_downline").html(null);
    },

    searchDonwline : function(){
        /*var tahun = $('#year').val();
         var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);
         if($objRegthn){
         alert('Format Tahun Harus Angka');
         }else{*/
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("sales/direct/downlineInfo") , $("#cekDownline").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);
        });
        //}
    },
    searchRecruit : function(){
        /*var tahun = $('#year').val();
         var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);
         if($objRegthn){
         alert('Format Tahun Harus Angka');
         }else{*/
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("sales/recruiter/bonusinfo") , $("#cekDownline").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);

            //$("#nextFrom1").hide();
            //$("#nextFrom1").html(null);
        });
        //}
    },

    searchDonwline2 : function(idmember,month,year){
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").hide();
        $("#detail_downline").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.ajax({
            url: All.get_url("sales/direct/downlineInfo2/" +idmember +"/" +month +"/" +year ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    All.set_enable_button();

                    $("#detail_downline").html(null);
                    $("#detail_downline").html(data);
                    $("#detail_downline").show();
                }
        });
    },

    searchDonwline3 : function(upline,month,year){
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');


        $("#detail_downline").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.ajax({
            url: All.get_url("sales/back/downlineInfo/" +upline +"/" +month +"/" +year),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    All.set_enable_button();
                    $("#hasilPencarian1").hide();
                    $("#detail_downline").show();
                    $("#detail_downline").html(null);
                    $("#detail_downline").html(data);
                }
        });
    },

    printToPdf : function (idmember,month,year){
        $.ajax({
            url: All.get_url("sales/print/" +idmember +"/" +month +"/" +year ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
                function(data){
                    $("#detail_trans").show();
                    $("#detail_trans").html(null);
                    $("#detail_trans").html(data);
                }
        });
    },

    searchBonusMember : function (){
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#pencarianBonus").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("sales/direct/downlineInfo") , $("#cekBonus").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);
        });
    },
    searchReportCahyono : function(){
        /*var tahun = $('#year').val();
         var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);
         if($objRegthn){
         alert('Format Tahun Harus Angka');
         }else{*/
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("sales/getReportCahyono") , $("#cekBonus").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);
        });
        //}
    },
    searchReportCahyono2 : function(){
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("tes_tampilreport") , $("#cekBonus").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);
        });
        //}
    },
    searchOfficeCahyono : function(){
        /*var tahun = $('#year').val();
         var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);
         if($objRegthn){
         alert('Format Tahun Harus Angka');
         }else{*/
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("sales/getReportCahyono") , $("#formSearch").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);
        });
        //}
    },
    
    cekValidMembForBnsMonth: function() {
    	 $("input[type=button]").attr('disabled', 'disabled');
    	 var month = $("#month").val();
    	 var year = $("#year").val();
    	 var username = $("#username").val();
    	 var pwd_member = $("#pwd_member").val();
    	 
    	 $.ajax({
            url: All.get_url("bonus/cekvalidmember2"),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'POST',
            dataType: "json",
            data: {username: username, pwd_member: pwd_member, month: month, year: year},
            success:
                function(data){
                    $("input[type=button]").removeAttr('disabled');
                    if(data.response == "true") {
                    	$.ajax({
				            url: All.get_url("bonus/action"),
				            //url: All.get_url("sales/detail/transaksi"),
				            type: 'POST',
				            data: {month: month, year: year},
				            success:
				                function(data){
				                    
				                }
				        });
                    } else {
                    	alert(data.message);
                    }
                }
        });
    },

    cashback_hydro : function(){
        console.log('1');
        /*var tahun = $('#year').val();
         var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);
         if($objRegthn){
         alert('Format Tahun Harus Angka');
         }else{*/
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("sales/cashback_hydro/process"), $("#cekCashback").serialize(), function(hasil)
        {
            All.set_enable_button();
            $("#detail_cashback").hide();
            $("#detail_cashback").html(null);
            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);
        });

    }

}
var $ = jQuery;

var Shopping = {
	getListRecruitMember: function () {
		var bln = $("#bln").val();
		var thn = $("#thn").val();
		var recruitid = $("#recruitid").val();

		if (bln === "" || thn === "") {
			alert("Tanggal harus disi..");
			$("#btn_recruit").attr("disabled", "disabled");
		} else {

			$("#btn_recruit").attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("member/recruit/list"),
				type: 'POST',
				data: {
					bln: bln,
					thn: thn,
					recruitid: recruitid
				},
				success: function (data) {
					$("#btn_recruit").removeAttr("disabled");
					$("#listRecruit").html(data);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	getListRecruitMemberIEC: function () {
		var period = $("#period").val();
		var recruitid = $("#recruitid").val();

		$("#btn_recruit").attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("webshop/promo_iec/getListIecRes"),
			type: 'POST',
			data: {
				period: period,
				recruitid: recruitid
			},
			success: function (data) {
				$("#btn_recruit").removeAttr("disabled");
				$("#listRecruit").html(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	getMemberNameByID: function (idx, setToDiv) {
		//All.set_disable_button();
		if (idx.value == '0000999') {
			alert("Tidak ada data member dengan ID " + idx.value);
			$(setToDiv).val(null);
		} else {
			All.set_disable_button();
			$.ajax({
				url: All.get_url("cart/member/id/") + idx.value,
				type: 'GET',
				dataType: 'json',
				success: function (data) {

					All.set_enable_button();
					if (data.sukses == "1" || data.sukses == "true") {
						$(setToDiv).val(data.arrayData[0].fullnm);
					} else {
						alert("Tidak ada data member dengan ID " + idx.value);
						$(setToDiv).val(null);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}

	},

	show_stk: function (idx, setToDiv) {
		All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("shipping/getStockist/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				var stkname = $("#" + idx.id + " :selected").text();

				$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.response == "true") {
					$("#nama_stkarea").val(stkname);
					$("#nama_stockistr1area").val(stkname);
					var arrayData = data.arrayData;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.loccd + ">" + value.loccd + " - " + value.fullnm + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);

				} else {
					alert("Tidak ada data stokis untuk area " + stkname);
					$(setToDiv).html(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},


	show_kota: function (idx, setToDiv) {
		All.set_disable_button();
		var txt = $("#" + idx.id + " :selected").text();
		$(setToDiv).attr("disabled", "disabled");
		if (idx.value == "select") {
			alert("Silahkan pilih provinsi terlebih dahulu");
		} else {
			$.ajax({
				url: All.get_url("shipping/kota/") + idx.value,
				type: 'GET',
				dataType: 'json',
				success: function (data) {


					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						var arrayData = data.arrayData;
						$("#nama_provinsi").val(txt);
						$(setToDiv).html(null);
						var rowhtml = "<option value=''>--Select here--</option>";
						$.each(arrayData, function (key, value) {

							rowhtml += "<option  value=" + value.kode_kabupaten + ">" + value.kabupaten.toUpperCase() + "</option>";
							//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

						});
						$(setToDiv).append(rowhtml);
						$("#kecamatan").val(null);
						$("#stockistref").val(null);
						$("#sender_address").val(null);
						$("#destination_address").val(null);
					} else {
						$(setToDiv).append(null);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	show_kota2: function (idx, setToDiv) {
		All.set_disable_button();
		var txt = $("#" + idx.id + " :selected").text();
		$(setToDiv).attr("disabled", "disabled");
		if (idx.value == "select") {
			alert("Silahkan pilih provinsi terlebih dahulu");
		} else {
			$.ajax({
				url: All.get_url("shipping/kota/") + idx.value,
				type: 'GET',
				dataType: 'json',
				success: function (data) {


					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						var arrayData = data.arrayData;
						$("#nama_provinsi1").val(txt);
						$(setToDiv).html(null);
						var rowhtml = "<option value=''>--Select here--</option>";
						$.each(arrayData, function (key, value) {

							rowhtml += "<option  value=" + value.kode_kabupaten + ">" + value.kabupaten.toUpperCase() + "</option>";
							//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

						});
						$(setToDiv).append(rowhtml);
						$("#kecamatan1").val(null);
						$("#nama_stockist").val(null);
						$("#sender_address").val(null);
						$("#destination_address").val(null);
					} else {
						$(setToDiv).append(null);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	back_to_cart: function (clear_div, show_div) {

		$(clear_div).html(null);
		$(show_div).show();
		var pricecode = $("#pricecode").val();
		if (pricecode == "12E3") {
			$(".westP").css("display", "none");
			$(".eastP").css("display", "block");

		} else {
			$(".westP").css("display", "block");
			$(".eastP").css("display", "none");
		}
	},

	show_kecamatan: function (idx, setToDiv) {
		All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("shipping/kecamatan/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				var txt = $("#" + idx.id + " :selected").text();
				$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.response == "true") {
					$("#nama_kota").val(txt);
					var arrayData = data.arrayData;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.kode_kecamatan + ">" + value.kecamatan.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$("#kelurahan").html(null);

				} else {
					$(setToDiv).html(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	show_kecamatan2: function (idx, setToDiv) {
		All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("shipping/kecamatan/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				var txt = $("#" + idx.id + " :selected").text();
				$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.response == "true") {
					$("#nama_kota1").val(txt);

					var arrayData = data.arrayData;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.kode_kecamatan + ">" + value.kecamatan.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$("#nama_kecamatan1").html(null);
					$("#stockist").html(null);
					$("#nama_stockist").html(null);

				} else {
					$(setToDiv).html(null);
					$("#nama_kecamatan1").html(null);
					$("#stockist").html(null);
					$("#nama_stockist").html(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	show_kelurahan: function (idx, setToDiv) {
		All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("shipping/kelurahan/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				var txt = $("#" + idx.id + " :selected").text();
				//$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.listStk !== null) {
					$("#nama_kecamatan").val(txt);
					$("#destination_address").val(data.arrayData[0].kode_kec_JNE);
					var arrayData = data.listStk;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.loccd + ">" + value.loccd + " - " + value.fullnm.toUpperCase() + " - " + value.state.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$(setToDiv).removeAttr("disabled");

				} else {
					//$(setToDiv).html(null);
					$("#nama_kecamatan").val(txt);
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";

					$(setToDiv).append(rowhtml);
					$(setToDiv).removeAttr("disabled");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	show_kelurahan_nonm_promo: function (idx, setToDiv) {
		All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("shipping/kelurahan/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				var txt = $("#" + idx.id + " :selected").text();
				//$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.listStk !== null) {
					$("#nama_kecamatan").val(txt);
					$("#destination_address").val(data.arrayData[0].kode_kec_JNE);
					var arrayData = data.listStk;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option value=" + value.loccd + ">" + value.loccd + " - " + value.fullnm.toUpperCase() + " - " + value.state.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$(setToDiv).removeAttr("disabled");
					console.log("#" + setToDiv + "set loccd to : " + arrayData[0].loccd);
					$(setToDiv + " option[value=" + arrayData[0].loccd + "]").attr('selected', 'selected');
					Shopping.tampilkanPriceCode();
				} else {
					//$(setToDiv).html(null);
					$("#nama_kecamatan").val(txt);
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";

					$(setToDiv).append(rowhtml);
					$(setToDiv).removeAttr("disabled");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	show_kelurahan2: function (idx, setToDiv) {
		All.set_disable_button();
		$(setToDiv).attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("shipping/kelurahan/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				var txt = $("#" + idx.id + " :selected").text();
				//$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.listStk !== null) {
					$("#nama_kecamatan1").val(txt);
					$("#destination_address").val(null);
					var arrayData = data.listStk;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.loccd + ">" + value.loccd + " - " + value.fullnm.toUpperCase() + " - " + value.state.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$(setToDiv).removeAttr("disabled");

				} else {
					//$(setToDiv).html(null);
					$("#nama_kecamatan1").val(txt);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	chooseKelurahan: function (idx) {
		var txt = $("#" + idx.id + " :selected").text();
		var xx = txt.split(" - ");
		$("#nama_kelurahan").val(xx[1]);
		$("#kodepos").val(xx[0]);
	},


	show_pricestkX: function (idx, setToDiv) {
		if (idx.value !== "") {
			var kec = $("#kecamatan1").val();
			All.set_disable_button();
			$(setToDiv).attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("shipping/getPricecodeX/") + idx.value + "/" + kec,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					var stkname = $("#" + idx.id + " :selected").text();
					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						$("#nama_stockist").val(stkname);
						$("#nama_stockistr1ref").val(stkname);
						$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
						$("#jne_branch").val(data.arrayData[0].jne_branchcd);
						//console.log(data.arrayData[0].kode_kec_JNE_Origin);
						$(setToDiv).val(data.arrayData[0].pricecode);
						if (data.arrayData[0].pricecode == "12E3") {
							$(".westP").css("display", "none");
							$(".eastP").css("display", "block");

						} else {
							$(".westP").css("display", "block");
							$(".eastP").css("display", "none");
						}
						Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
					} else {
						alert("Price code tidak ada");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	show_pricestkY: function (idx, setToDiv) {
		if (idx.value !== "") {
			var kec = $("#kecamatan").val();
			All.set_disable_button();
			$(setToDiv).attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("shipping/getPricecodeX/") + idx.value + "/" + kec,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					var stkname = $("#" + idx.id + " :selected").text();
					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						$("#nama_stockist").val(stkname);
						$("#nama_stockistr1ref").val(stkname);
						$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
						$("#jne_branch").val(data.arrayData[0].jne_branchcd);
						//console.log(data.arrayData[0].kode_kec_JNE_Origin);
						$(setToDiv).val(data.arrayData[0].pricecode);
						if (data.arrayData[0].pricecode == "12E3") {
							$(".westP").css("display", "none");
							$(".eastP").css("display", "block");

						} else {
							$(".westP").css("display", "block");
							$(".eastP").css("display", "none");
						}
						Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
					} else {
						alert("Price code tidak ada");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	show_pricestk: function (idx, setToDiv) {
		if (idx.value !== "") {
			All.set_disable_button();
			$(setToDiv).attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("shipping/getPricecodeX/") + idx.value,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					var stkname = $("#" + idx.id + " :selected").text();
					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						$("#nama_stockist").val(stkname);
						$("#nama_stockistr1ref").val(stkname);
						$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
						//console.log(data.arrayData[0].kode_kec_JNE_Origin);
						$(setToDiv).val(data.arrayData[0].pricecode);
						if (data.arrayData[0].pricecode == "12E3") {
							$(".westP").css("display", "none");
							$(".eastP").css("display", "block");

						} else {
							$(".westP").css("display", "block");
							$(".eastP").css("display", "none");
						}
						Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
					} else {
						alert("Price code tidak ada");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},


	show_pricestk2: function (idx, setToDiv) {
		if (idx.value !== "") {
			var kec = $("#kecamatan1").val();
			All.set_disable_button();
			$(setToDiv).attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("shipping/getPricecodeX/") + idx.value + "/" + kec,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					var stkname = $("#" + idx.id + " :selected").text();
					var stn = stkname.split(" - ");
					var sdf = stn[0] + " - " + stn[1];
					var sdf_vera = stn[0] + "|" + stn[2];
					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						$("#nama_stockist").val(sdf);
						$("#stk").val(sdf_vera); //tambahan Vera
						$("#nama_stockistr1ref").val(sdf);
						$("#state1").val(stn[2]);
						$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
						$("#jne_branch").val(data.arrayData[0].jne_branchcd);
						//console.log(data.arrayData[0].kode_kec_JNE_Origin);
						$(setToDiv).val(data.arrayData[0].pricecode);
						if (data.arrayData[0].pricecode == "12E3") {
							$(".westP").css("display", "none");
							$(".eastP").css("display", "block");

						} else {
							$(".westP").css("display", "block");
							$(".eastP").css("display", "none");
						}
						//Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
					} else {
						alert("Price code tidak ada");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	show_pricestk2Y: function (idx, setToDiv) {
		if (idx.value !== "") {
			var kec = $("#kecamatan").val();
			All.set_disable_button();
			$(setToDiv).attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("shipping/getPricecodeX/") + idx.value + "/" + kec,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					var stkname = $("#" + idx.id + " :selected").text();
					var stn = stkname.split(" - ");
					var sdf = stn[0] + " - " + stn[1];
					$(setToDiv).removeAttr("disabled", "disabled");
					All.set_enable_button();
					if (data.response == "true") {
						$("#nama_stockist").val(sdf);
						$("#nama_stockistr1ref").val(sdf);
						$("#state1").val(stn[2]);
						$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
						$("#jne_branch").val(data.arrayData[0].jne_branchcd);
						//console.log(data.arrayData[0].kode_kec_JNE_Origin);
						$(setToDiv).val(data.arrayData[0].pricecode);
						if (data.arrayData[0].pricecode == "12E3") {
							$(".westP").css("display", "none");
							$(".eastP").css("display", "block");

						} else {
							$(".westP").css("display", "block");
							$(".eastP").css("display", "none");
						}
						//Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
					} else {
						alert("Price code tidak ada");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	getDelChoice: function (nilai) {
		if (nilai == "1") {
			$('.diambil').show();
			$('.dikirim').hide();

		} else {
			$('.diambil').hide();
			$('.dikirim').show();

		}
	},

	chooseSponsorId: function (nilai) {
		if (nilai == "input") {
			$('.input').show();
			$('.nama').show();
			$('.pilih').hide();
			document.getElementById('sponsorname').value = '';

		} else {
			$('.input').hide();
			$('.pilih').show();
			$('.nama').show();
		}
	},
	chooseSponsorId2: function (nilai) {
		if (nilai == "input") {
			$('.input2').show();
			$('.nama2').show();
			$('.pilih2').hide();
			document.getElementById('recuitername').value = '';

		} else {
			$('.input2').hide();
			$('.pilih2').show();
			$('.nama2').show();
		}
	},

	addToCartNeedLogin: function (param) {
		All.set_disable_button();
		var stt = $("#stt" + param).val();
		if (stt == 0) {

		} else {

			var prdcd = $("#prdcd" + param).val();
			var prdnm = $("#prdnm" + param).val();
			var bv = $("#bv" + param).val();
			var qty = $("#qty" + param).val();
			var westPrice = $("#westPrice" + param).val();
			var eastPrice = $("#eastPrice" + param).val();
			var weight = $("#weight" + param).val();
			$.ajax({
				url: All.get_url("cart/add"),
				type: 'POST',
				dataType: 'json',
				data: {
					prdcd: prdcd,
					prdnm: prdnm,
					bv: bv,
					qty: qty,
					westPrice: westPrice,
					eastPrice: eastPrice,
					weight: weight
				},
				success: function (data) {
					All.set_enable_button();
					alert(data.message);
					if (data.response == "true") {
						var maxID = parseInt($("#maxID").val());
						var nextMaxID = maxID + 1;
						$("#maxID").val(nextMaxID);
						$("#showInfoSumCart").html(null);
						var row = "&nbsp;&nbsp;&nbsp;&nbsp;" + data.total_item + " items | Rp. " + All.num(data.total_west_price);
						$("#showInfoSumCart").append(row);
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	addToCart: function (param) {
		All.set_disable_button();
		var prdcd = $("#prdcd" + param).val();
		var prdnm = $("#prdnm" + param).val();
		var bv = $("#bv" + param).val();
		var qty = $("#qty" + param).val();
		var westPrice = $("#westPrice" + param).val();
		var eastPrice = $("#eastPrice" + param).val();
		var eastCPrice = $("#eastCPrice" + param).val();
		var westCPrice = $("#westCPrice" + param).val();

		var weight = $("#weight" + param).val();
		var desc = 0; //tambahan vera
		$.ajax({
			url: All.get_url("cart/add"),
			type: 'POST',
			dataType: 'json',
			data: {
				prdcd: prdcd,
				prdnm: prdnm,
				bv: bv,
				qty: qty,
				westPrice: westPrice,
				eastPrice: eastPrice,
				westCPrice: westCPrice,
				eastCPrice: eastCPrice,
				weight: weight,
				desc: desc
			},
			success: function (data) {
				All.set_enable_button();
				alert(data.message);
				if (data.response == "true") {
					var maxID = parseInt($("#maxID").val());
					var nextMaxID = maxID + 1;
					$("#maxID").val(nextMaxID);
					$("#showInfoSumCart").html(null);
					var row = "&nbsp;&nbsp;&nbsp;&nbsp;" + data.total_item + " items | Rp. " + All.num(data.total_west_price);
					$("#showInfoSumCart").append(row);
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	addToCart2: function (param) {
		All.set_disable_button();
		var prdcd = $("#prdcd" + param).val();
		var prdnm = $("#prdnm" + param).val();
		var bv = $("#bv" + param).val();
		var qty = $("#qty" + param).val();
		var westPrice = $("#westPrice" + param).val();
		var eastPrice = $("#eastPrice" + param).val();
		var eastCPrice = $("#eastCPrice" + param).val();
		var westCPrice = $("#westCPrice" + param).val();

		var weight = $("#weight" + param).val();
		$.ajax({
			url: All.get_url("addtocart"),
			type: 'POST',
			dataType: 'json',
			data: {
				prdcd: prdcd,
				prdnm: prdnm,
				bv: bv,
				qty: qty,
				westPrice: westPrice,
				eastPrice: eastPrice,
				westCPrice: westCPrice,
				eastCPrice: eastCPrice,
				weight: weight
			},
			success: function (data) {
				All.set_enable_button();
				alert(data.message);
				if (data.response == "true") {
					var maxID = parseInt($("#maxID").val());
					var nextMaxID = maxID + 1;
					$("#maxID").val(nextMaxID);
					$("#showInfoSumCart").html(null);
					var row = "&nbsp;&nbsp;&nbsp;&nbsp;" + data.total_item + " items | Rp. " + All.num(data.total_west_price);
					$("#showInfoSumCart").append(row);
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	delete_rowCart: function (param) {
		All.set_disable_button();
		var rowid = $("#rowid" + param).val();
		var qty = parseInt($("#qty" + param).val());
		var totQtyWest = parseInt($("#totQtyWest").val());
		var pricecode = $("#pricecode").val();
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: All.get_url("cart/remove/"),
			data: {
				rowid: rowid
			},
			success: function (data) {
				All.set_enable_button();
				alert(data.message);
				if (data.response == "true") {
					/*var newQty = totQtyWest - qty;
                    $("#divTotQtyWest").html(null);
            		$("#divTotQtyEast").html(null);
                    $("#divTotQtyWest").html(All.num(newQty));
                    $("#divTotQtyEast").html(All.num(newQty));
                    $("#totQtyWest").val(newQty);
                    $("#totQtyEast").val(newQty); */

					Shopping.recalculateItems2(param);
					$("div#" + param).remove();
					//Shopping.setNewPrice(param);

					Shopping.updatePriceAtHeader(pricecode);
				}

			}
		});
	},

	recalculateItems2: function (param) {
		var qty = parseInt($("#qty" + param).val());
		var westPrice = parseInt($("#amountWest" + param).val());
		var weightPrice = parseFloat($("#amountWeight" + param).val());
		var eastPrice = parseInt($("#amountEast" + param).val());
		var totBv = parseInt($("#totBv").val());
		var totQtyWest = parseInt($("#totQtyWest").val());
		var totalAmountWest = parseInt($("#totalAmountWest").val());
		var totQtyEast = parseInt($("#totQtyEast").val());
		var totalAmountEast = parseInt($("#totalAmountEast").val());
		var totalAmountWeight = parseFloat($("#totalAmountWeight").val());
		var amountKIRIM = parseInt($("#amountKIRIM").val());

		console.log(totalAmountWeight);
		console.log(weightPrice);
		var selisih_barat = totalAmountWest - westPrice;
		var selisih_timur = totalAmountEast - eastPrice;
		var selisih_qty = totQtyWest - qty;
		var selisih_weight = totalAmountWeight - weightPrice;
		//alert("Selisih barat : " +selisih_barat+ ", selisih timur : " +selisih_timur);
		selisih_weight = selisih_weight.toPrecision(2);

		$("#totalAmountWest").val(selisih_barat);
		$("#totalAmountWeight").val(selisih_weight);
		$("#totalAmountEast").val(selisih_timur);
		$("#totQtyWest").val(selisih_qty);
		$("#totQtyEast").val(selisih_qty);
		$("#divTotQtyWest").html(null);
		$("#divTotQtyEast").html(null);
		$("#divTotWeight").html(null);
		$("#divTotQtyWest").html(selisih_qty);
		$("#divTotQtyEast").html(selisih_qty);
		$("#divTotWeight").html(selisih_weight);

		$("#totAllWestPrice").html(null);
		$("#totAllEastPrice").html(null);
		$("#totAllItemWest").html(null);
		$("#totAllItemEast").html(null);
		$("#totAllWestPrice").text(All.num(selisih_barat));
		$("#totAllEastPrice").text(All.num(selisih_timur));
		$("#totAllItemWest").text(All.num(selisih_qty));
		$("#totAllItemEast").text(All.num(selisih_qty));


	},

	recalculateItems: function () {
		var jmlAmt = $(".amtW").length;
		//alert(jmlAmt);
		var totWest = 0;
		var totEast = 0;
		var totQtyWest = 0;
		var totQtyEast = 0;

		for (var i = 1; i <= jmlAmt; i++) {
			totWest += parseInt($("#amountWest" + i).val());
			totEast += parseInt($("#amountEast" + i).val());
			totQtyWest += parseInt($("#qty" + i).val());
			totQtyEast += parseInt($("#qty" + i).val());
		}

		$("#totalAmountWest").val(totWest);
		$("#totalAmountEast").val(totEast);
		$("#totQtyWest").val(totQtyWest);
		$("#totQtyEast").val(totQtyEast);
		$("#divTotQtyWest").html(null);
		$("#divTotQtyEast").html(null);
		$("#divTotQtyWest").html(totQtyWest);
		$("#divTotQtyEast").html(totQtyEast);
		$("#totAllWestPrice").html(null);
		$("#totAllEastPrice").html(null);
		$("#totAllItemWest").html(null);
		$("#totAllItemEast").html(null);
		$("#totAllWestPrice").text(All.num(totWest));
		$("#totAllEastPrice").text(All.num(totEast));
		$("#totAllItemWest").text(All.num(totQtyWest));
		$("#totAllItemEast").text(All.num(totQtyEast));
	},


	/*----------------------

    UPDATE FOR PROMO 17 AGUSTUS 2018*/
	setNewPrice: function (param) {
		var qty = parseInt($("#qty" + param).val());
		var nm = /^[0-9]+$/;
		if ($("#qty" + param).val().match(nm) && qty > 0) {

			var westPrice = $("#westPrice" + param).val();

			//var amountKIRIM = $("#amountKIRIM").val();
			//var weightPrice = $("#weightPrice" +param).val();

			var eastPrice = $("#eastPrice" + param).val();
			var bvr = parseInt($("#bv" + param).val());
			$("#amountWest" + param).val(qty * westPrice);
			$("#amountEast" + param).val(qty * eastPrice);
			//$("#amountWeight" +param).val(qty * weightPrice);
			//console.log("bvr : "+bvr+ " qty : "+qty+ " weight : "+(weightPrice) );
			$("#amountBV" + param).val(qty * bvr);
			$("#divSubTotWestPrice" + param).html(null);
			$("#divSubTotEastPrice" + param).html(null);
			//$("#divSubTotWeightPrice" +param).html(null);
			$("#divSubTotWestPrice" + param).text(All.num(qty * westPrice));
			$("#divSubTotEastPrice" + param).text(All.num(qty * eastPrice));
			//$("#divSubTotWeightPrice" +param).text(All.num(qty * weightPrice));
			//$("#divSubTotWeightPrice" +param).text(weightPrice.toFixed(2));

			/*$("#divTotBVWest").html(null);
			$("#divTotBVEast").html(null);
			$("#divTotBVWest" +param).text(All.num(qty * bvr));
			$("#divTotBVEast" +param).text(All.num(qty * bvr));*/

			var totQty = 0;
			var totBV = 0;
			var totWest = 0;
			var totEast = 0;
			var totWeight = 0;


			$('input[name="qty[]"]').each(function () {
				totQty += parseInt($(this).val());
			});
			$('input[name="amountWest[]"]').each(function () {
				totWest += parseInt($(this).val());
			});

			/*$('input[name="amountWeight[]"]').each(function(){
				console.log("aa ");

				console.log(parseFloat($(this).val()));
				totWeight += parseFloat($(this).val());
			}); */

			$('input[name="amountEast[]"]').each(function () {
				totEast += parseInt($(this).val());
			});

			$('input[name="amountBV[]"]').each(function () {
				totBV += parseInt($(this).val());
			});

			//totWest=totWest+amountKIRIM;
			//totEast=totWest+amountKIRIM;

			//alert("totQty :" +totQty+ " west :" +totWest+ " east :" +totEast);

			$("#totalAmountWest").val(totWest);
			$("#totalAmountEast").val(totEast);
			//$("#totalAmountWeight").val(totWeight);
			$("#totQtyWest").val(totQty);
			$("#totQtyEast").val(totQty);
			$("#divTotQtyWest").html(null);
			$("#divTotQtyEast").html(null);
			//$("#divTotWeight").html(null);



			$("#divTotQtyWest").html(totQty);
			$("#divTotQtyEast").html(totQty);
			//$("#divTotWeight").html(totWeight);

			$("#totAllWestPrice").html(null);
			$("#totAllEastPrice").html(null);
			$("#totAllItemWest").html(null);
			$("#totAllItemEast").html(null);
			$("#totAllWestPrice").text(All.num(totWest));
			$("#totAllEastPrice").text(All.num(totEast));
			$("#totAllItemWest").text(All.num(totQty));
			$("#totAllItemEast").text(All.num(totQty));
			$("#divTotBVWest").html(All.num(totBV));
			$("#divTotBVEast").html(All.num(totBV));

			var pricecode = $("#pricecode").val();
			Shopping.updatePriceAtHeader(pricecode);
		} else {
			alert('Tidak boleh huruf atau angka nol');
			$("#qty" + param).val(1);
			$("#qty" + param).focus();
		}
	},

	setNewPrice2: function (param) {

		var qty = parseInt($("#qty" + param).val());
		var amountWest = parseInt($("#amountWest" + param).val());
		var westPrice = $("#westPrice" + param).val();
		var eastPrice = $("#eastPrice" + param).val();
		$("#amountWest" + param).val(qty * westPrice);
		$("#amountEast" + param).val(qty * eastPrice);
		$("#divSubTotWestPrice" + param).html(null);
		$("#divSubTotEastPrice" + param).html(null);
		$("#divSubTotWestPrice" + param).text(All.num(qty * westPrice));
		$("#divSubTotEastPrice" + param).text(All.num(qty * eastPrice));
		var jmlAmt = $(".amtW").length;
		var totWest = 0;
		var totEast = 0;
		var totQtyWest = 0;
		var totQtyEast = 0;
		//alert(jmlAmt);
		for (var i = 1; i <= jmlAmt; i++) {
			totWest += parseInt($("#amountWest" + i).val());
			totEast += parseInt($("#amountEast" + i).val());
			totQtyWest += parseInt($("#qty" + i).val());
			totQtyEast += parseInt($("#qty" + i).val());
		}
		if (isNaN(qty)) {
			("#qty" + i).val(1);
			$("#totalAmountWest").val(0);
			$("#totalAmountEast").val(0);
			$("#totQtyWest").val(0);
			$("#totQtyEast").val(0);
		} else {
			$("#totalAmountWest").val(totWest);
			$("#totalAmountEast").val(totEast);
			$("#totQtyWest").val(totQtyWest);
			$("#totQtyEast").val(totQtyEast);
			$("#divTotQtyWest").html(null);
			$("#divTotQtyEast").html(null);
			$("#divTotQtyWest").html(totQtyWest);
			$("#divTotQtyEast").html(totQtyEast);
			$("#totAllWestPrice").html(null);
			$("#totAllEastPrice").html(null);
			$("#totAllItemWest").html(null);
			$("#totAllItemEast").html(null);
			$("#totAllWestPrice").text(All.num(totWest));
			$("#totAllEastPrice").text(All.num(totEast));
			$("#totAllItemWest").text(All.num(totQtyWest));
			$("#totAllItemEast").text(All.num(totQtyEast));

		}
		var pricecode = $("#pricecode").val();
		Shopping.updatePriceAtHeader(pricecode);
	},

	updateCart: function () {
		All.set_disable_button();
		$("input[type=button]").attr('disabled', 'disabled');
		$.post(All.get_url("cart/update"), $("#formCart").serialize(), function (data) {
			All.set_enable_button();
			//$("input[type=button]").removeAttr('disabled');
			alert("Shopping Cart telah di update");
			$("#listCartPrd").html(null);
			$("#listCartPrd").html(data);
		}).fail(function () {
			alert("Error requesting page");
			//All.set_enable_button();
			$("input[type=button]").removeAttr('disabled');
		});

		//alert("pricecode : " +$("#pricecode").val());
	},

	updateCart2: function () {
		All.set_disable_button();
		$("input[type=button]").attr('disabled', 'disabled');
		$.post(All.get_url("cart/upd"), $("#formCart").serialize(), function (data) {
			All.set_enable_button();
			//$("input[type=button]").removeAttr('disabled');
			if (data.response == "true") {
				alert("Shopping Cart telah di update");
			}
		}, "json").fail(function () {
			alert("Error requesting page");
			//All.set_enable_button();
			$("input[type=button]").removeAttr('disabled');
		});
	},

	validateCheckOut: function () {
		//e.preventDefault();
		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var err = 0;
		if (!del) {
			alert("Silahkan Pilih Tipe Pengiriman");
			//$("#submits").attr("disabled", "disabled");
			$('input[name=delivery]').focus();
			err++;
			//alert("isi err :" +err);
			return false;
		} else {
			//alert("Pilihan :" +val);
			//return false;

			var idmemberx = $("#idmemberx").val();
			var membername = $("#membername").val();

			if (idmemberx === "" || idmemberx === null || membername === "" || membername === null) {
				err++;
			}

			if (val == "1") {
				var provinsi1 = $("#provinsi1").val();
				var kota1 = $("#kota1").val();
				var kecamatan1 = $("#kecamatan1").val();
				var stockist = $("#stockist").val();
				//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
				/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
					alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
					err++;
					return false;
				} */
				if (provinsi1 === "" || provinsi1 === null || kota1 === "" ||
					kota1 === null || kecamatan1 === "" || kecamatan1 === null ||
					stockist === "" || stockist === null) {
					err++;
					//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
				}

			} else {
				var nama_penerima = $("#nama_penerima").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();
				var stockistref = $("#stockistref").val();
				var provinsi = $("#provinsi").val();
				var kota = $("#kota").val();
				var kecamatan = $("#kecamatan").val();
				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
					stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
					kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
					alamat === "" || alamat === null) {
					err++;
				}

			}
		}


		if (err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			return true;
		}
	},

	validateCheckOutBaru: function () {
		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var err = 0;
		if (!del) {
			alert("Silahkan Pilih Tipe Pengiriman");
			//$("#submits").attr("disabled", "disabled");
			$('input[name=delivery]').focus();
			err++;
			//alert("isi err :" +err);
			return false;
		} else {
			//alert("Pilihan :" +val);
			//return false;

			var idmemberx = $("#idmemberx").val();
			var membername = $("#membername").val();

			if (idmemberx === "" || idmemberx === null || membername === "" || membername === null) {
				err++;
			}

			if (val == "1") {
				var provinsi1 = $("#provinsi1").val();
				var kota1 = $("#kota1").val();
				var kecamatan1 = $("#kecamatan1").val();
				var stockist = $("#stockist").val();
				//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
				/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
					alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
					err++;
					return false;
				} */
				if (provinsi1 === "" || provinsi1 === null || kota1 === "" ||
					kota1 === null || kecamatan1 === "" || kecamatan1 === null ||
					stockist === "" || stockist === null) {
					err++;
					//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
				}

			} else {
				var nama_penerima = $("#nama_penerima").val();
				var shipper = $("#shipper").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();
				var stockistref = $("#stockistref").val();
				var provinsi = $("#provinsi").val();
				var kota = $("#kota").val();

				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				if (shipper == 1) {
					var kecamatan = $("#kecamatan").val();
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				} else {
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				}


			}
		}


		if (err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			return true;
		}
	},

	validateCheckOutNonMemberPromo: function () {
		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var err = 0;
		if (!del) {
			alert("Silahkan Pilih Tipe Pengiriman");
			//$("#submits").attr("disabled", "disabled");
			$('input[name=delivery]').focus();
			err++;
			//alert("isi err :" +err);
			return false;
		} else {
			//alert("Pilihan :" +val);
			//return false;

			var idmemberx = $("#idmemberx").val();
			var membername = $("#membername").val();

			if (idmemberx === "" || idmemberx === null || membername === "" || membername === null) {
				err++;
			}

			if (val == "1") {
				var provinsi1 = $("#provinsi1").val();
				var kota1 = $("#kota1").val();
				var kecamatan1 = $("#kecamatan1").val();
				var stockist = $("#stockist").val();
				//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
				/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
					alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
					err++;
					return false;
				} */
				if (provinsi1 === "" || provinsi1 === null || kota1 === "" ||
					kota1 === null || kecamatan1 === "" || kecamatan1 === null ||
					stockist === "" || stockist === null) {
					err++;
					//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
				}

			} else {
				var nama_penerima = $("#nama_penerima").val();
				var shipper = $("#shipper").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();
				var stockistref = $("#stockistref").val();
				var provinsi = $("#provinsi").val();
				var kota = $("#kota").val();

				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				if (shipper == 1) {
					var kecamatan = $("#kecamatan").val();
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				} else if (shipper == "4") {
					var metode = $("#metode").val();
					var alamat = $("#alamat").val();
					if (metode === "" || metode === null || alamat === "" || alamat === null) {
						err++;
						//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
					}

				} else {
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				}


			}
		}


		if (err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			var resHitung = Shopping.hitungTotalCart();
			if (resHitung == false) {
				alert("Minimum pembelanjaan senilai Rp. 350000,-")
				return false;
			} else {
				return true;
			}
			//return resHitung;
		}
	},
	validateCheckOutNonMemberPromoDigital: function () {
		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var err = 0; {
			//alert("Pilihan :" +val);
			//return false;

			var idmemberx = $("#idmemberx").val();
			var membername = $("#membername").val();

			if (idmemberx === "" || idmemberx === null || membername === "" || membername === null) {
				//err++;
			}

			{
				var nama_penerima = $("#nama_penerima").val();
				var shipper = $("#shipper").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();
				//var stockistref = $("#stockistref").val();
				var provinsi = $("#provinsi").val();
				var kota = $("#kota").val();

				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				if (shipper == 1) {
					var kecamatan = $("#kecamatan").val();
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						provinsi === "" || provinsi === null ||
						kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				} else {
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						provinsi === "" || provinsi === null ||
						kota === "" || kota === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				}


			}
		}


		if (err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			//var resHitung = Shopping.hitungTotalCart();
			//if(resHitung == false) {
			//	alert("Minimum pembelanjaan senilai Rp. 350000,-")
			//	return false;
			//} else
			{
				return true;
			}
			//return resHitung;
		}
	},

	hitungTotalCart: function () {
		var pricecode = $("#pricecode").val();
		var harga = 0;
		var total_belanja = 0;
		if (pricecode == "12W3") {
			harga = parseInt($("#totalAmountWest").val());
		} else {
			harga = parseInt($("#totalAmountEast").val());
		}

		if (harga < 350000) {
			return false;
		} else {
			return true;
		}
	},

	checkTotalBelanjaMampirkak: function (param) {
		var mindp = parseInt(param);
		var pricecode = $("#pricecode").val();
		var harga = 0;
		var total_belanja = 0;
		if (pricecode == "12W3") {
			harga = parseInt($("#totalAmountWest").val());
			harga_kirim = parseInt($("#amountKIRIM").val());
			harga_minus_kirim = harga - harga_kirim;
		} else {
			harga = parseInt($("#totalAmountEast").val());
			harga_kirim = parseInt($("#amountKIRIM").val());
			harga_minus_kirim = harga - harga_kirim;
		}
		console.log("Belanja : " + harga_minus_kirim);
		console.log("Min DP : " + mindp);
		if (harga_minus_kirim < mindp) {
			return false;
		} else {
			return true;
		}
	},

	checkout: function () {
		/*var del = $('input[name=delivery]').is(':checked');
		 var update = $("#updateCart").val();

		 if(!del)
		 {
		     alert("Silahkan Pilih Tipe Pengiriman");
		     $("#submits").attr("disabled", "disabled");
		 }
		 else{
		 */
		if (Shopping.validateCheckOut()) {
			/*Shopping.updateCart();
			$("#submits").removeAttr("disabled");
			$.post(All.get_url("cart/checkout") , $("#formCart").serialize(), function(data)
			{
			    $("#submits").removeAttr("disabled");

			    $("#formCart1").hide();
			    $("#divCheckOut").html(null);
			    $("#divCheckOut").html(data);

			}).fail(function() {
			    alert("Error requesting page");
			    //All.set_enable_button();
			    $("#submits").attr("disabled", "disabled");
			});  */
			alert("validasi OK..!!");
		}
		//}
	},

	updatePriceAtHeader: function (pricecode) {
		var totQtyWest = $("#totQtyWest").val();
		var totalAmountEast = $("#totalAmountEast").val();
		var totalAmountWest = $("#totalAmountWest").val();
		$("#showInfoSumCart").html(null);
		var row = "";
		var total_belanja
		if (pricecode == "12W3") {
			row += "&nbsp;&nbsp;&nbsp;&nbsp;" + totQtyWest + "&nbsp;&nbsp; items | Rp. " + All.num(totalAmountWest);
			total_belanja = parseInt(totalAmountWest);
		} else {
			row += "&nbsp;&nbsp;&nbsp;&nbsp;" + totQtyWest + "&nbsp;&nbsp; items | Rp. " + All.num(totalAmountEast);
			total_belanja = parseInt(totalAmountEast);
		}
		$("#showInfoSumCart").append(row);

		//Shopping.checkValidPromo17Agustus(total_belanja);

	},

	getProdByCatHeader: function (param) {
		var prdcat = param.id;
		//var offset = param.rel;
		$('#listprd').html("<center><img src=" + All.get_base() + "/asset/images/ajax-loader.gif ></center>");
		All.set_disable_button();

		$.ajax({
			url: All.get_url("shop/productHeader/cat/id/") + prdcat,
			type: 'GET',
			success: function (data) {
				All.set_enable_button();
				$('#listprd').html(null);
				$('#listprd').html(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},


	getProdByCat: function (param) {
		var prdcat = param.id;
		var offset = param.rel;
		$('#listprd').html("<center><img src=" + All.get_base() + "/asset/images/ajax-loader.gif ></center>");
		All.set_disable_button();

		$.ajax({
			url: All.get_url("shop/product/cat/id/") + prdcat,
			type: 'GET',
			success: function (data) {
				All.set_enable_button();
				$('#listprd').html(null);
				$('#listprd').html(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});

	},

	getProdByCatNonMember: function (param) {
		var prdcat = param.id;
		var offset = param.rel;
		$('#listprd').html("<center><img src=" + All.get_base() + "/asset/images/ajax-loader.gif ></center>");
		All.set_disable_button();

		$.ajax({
			url: All.get_url("mampirkak/product/cat/id/") + prdcat,
			type: 'GET',
			success: function (data) {
				All.set_enable_button();
				$('#listprd').html(null);
				$('#listprd').html(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});

	},

	getProdByCat2: function (param) {
		var prdcat = param.id;
		var offset = param.rel;
		$('#listprd').html("<center><img src=" + All.get_base() + "/asset/images/ajax-loader.gif ></center>");
		All.set_disable_button();
		if (offset == "1") {
			$.ajax({
				url: All.get_url("shop/product/cat/id/") + prdcat,
				type: 'GET',
				success: function (data) {
					All.set_enable_button();

					$('#listprd').html(null);
					$('#listprd').html(data);

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		} else {

			$.ajax({
				url: All.get_url("shop/product/cat/id/") + prdcat + "/" + offset,
				type: 'GET',
				success: function (data) {
					All.set_enable_button();

					$('#listprd').html(null);
					$('#listprd').html(data);

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	proceedPayment: function () {
		$("#checkout").removeAttr("disabled");
		$.post(All.get_url("payment"), $("#proceedPayment").serialize(), function (data) {
			$("#checkout").removeAttr("disabled");

			$("#paymentDiv").hide();
			$("#nextPaymentProcessDIV").html(null);
			$("#nextPaymentProcessDIV").html(data);

		}).fail(function () {
			alert("Error requesting page");
			//All.set_enable_button();
			$("#checkout").attr("disabled", "disabled");
		});
	},

	getTracking: function () {
		//alert("masuk sini");
		var awb = $("#awb").val();

		/* $.ajax({
	            url: All.get_url("tracking/process/") +awb,
	            type: 'GET',
	            //dataType: "json",
	            success:
	            function(data){
	                All.set_enable_button();

					$("#resultAwb").html(data.status);

	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        }); */
		$("#button_track").attr("disabled", "disabled");
		$("#resultAwb").append("<font size=3>Silahkan tunggu sebentar..</font>");
		$.post(All.get_url("tracking/process"), $("#tracking_awb").serialize(), function (data) {
			$("#button_track").removeAttr("disabled");

			if (data.hasOwnProperty('status')) {
				if (data.status === false) {

					$.ajax({
						url: All.get_url("tracking/cnote/") + awb,
						type: 'GET',
						success: function (datax) {
							$("#resultAwb").html(null);
							$("#resultAwb").html(datax);

						},
						error: function (xhr, ajaxOptions, thrownError) {
							alert(thrownError + ':' + xhr.status);
							All.set_enable_button();
						}
					});
				}
			} else {
				var cnote = data.cnote;
				var manifest = data.manifest;
				var rowhtml = "";
				if (manifest === null) {
					rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'><div class='col-lg-12 col-xs-12'><font size=3>Cnote masih dalam proses..</font></div></div>";
				} else {

					$.each(manifest, function (key, value) {
						rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'>";
						rowhtml += "<div class='col-lg-4 col-xs-4'>" + value.manifest_date + "</div>";
						rowhtml += "<div class='col-lg-4 col-xs-4'>" + value.city_name + "</div>";
						rowhtml += "<div class='col-lg-4 col-xs-4'>" + value.keterangan + "</div></div>";
						rowhtml += "</div>";

					});
				}
				$("#resultAwb").html(null);
				$("#resultAwb").append(rowhtml);
			}

			/*if(data.status === false) {
				$("#resultAwb").html(null);
			  $("#resultAwb").html(data.error);
			} else {


			} */

		}, "json");


	},

	getAccountPage: function (param) {

		$('#acc_div').html("<center><img src=" + All.get_base() + "/asset/images/ajax-loader.gif ></center>");
		$.ajax({
			url: All.get_url(param),
			type: 'GET',
			success: function (data) {
				All.set_enable_button();

				$("#acc_div").html(null);
				$('#acc_div').html(data);
				$('#acc_div').find('#new_email').focus();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});

	},

	showProductDetail: function (param) {
		$.ajax({
			url: All.get_url("shop/prodDet/") + param,
			type: 'GET',
			success: function (data) {
				All.set_enable_button();

				$("#listProductDiv").hide();
				$("#detailProductDiv").html(null);
				$('#detailProductDiv').html(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	backToListProduct: function () {
		$("#detailProductDiv").html(null);
		$("#listProductDiv").show();
	},

	getMemberInfo: function (nilai, setTo) {
		if (nilai === undefined || nilai === null || nilai === "") {
			alert("Data sponsor/recruiter harus diisi..");
		} else {

			$("#submits").attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("member/id/") + nilai,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					All.set_enable_button();
					if (data.response === "true") {
						//$('#' +setTo).val(data.arrayData[0].sponsorname);
						$('#' + setTo).val(data.sponsorname);
						$("#submits").removeAttr("disabled");
					} else if (data.response == "err") {
						alert("invalid ID member");
						$('#' + setTo).val(null);
					} else {
						alert(data.message);
						$('#' + setTo).val(data.sponsorname);
						$("#submits").removeAttr("disabled");
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	getMemberInfoDev: function (nilai, setTo) {
		if (nilai === undefined || nilai === null || nilai === "") {
			alert("Data sponsor/recruiter harus diisi..");
		} else {
			$.ajax({
				url: All.get_url("member/id/") + nilai,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					if (data.response === "true") {
						//$('#' +setTo).val(data.arrayData[0].sponsorname);
						// All.set_enable_button();
						// $("#submits").prop("disabled", false);
						$("#idno").attr("readonly", false);
						$('#' + setTo).val(data.sponsorname);
						alert(data.message);
						if (setTo == "sponsorname") {
							$("#errformSponsor").val(0);
						} else if (setTo == "recruitername") {
							$("#errformRecruiter").val(0);
						}

					} else if (data.response == "err") {
						alert("invalid ID member");
						// $("#submits").prop("disabled", true);
						$('#' + setTo).val(null);
						if (setTo == "sponsorname") {
							$("#errformSponsor").val(1);
						} else if (setTo == "recruitername") {
							$("#errformRecruiter").val(1);
						}

					} else {
						alert(data.message);
						// $("#submits").prop("disabled", true);
						// $("#idno").attr("readonly", true).val("");
						// $("#idno").val("");
						$('#' + setTo).val(data.sponsorname);
						if (setTo == "sponsorname") {
							$("#errformSponsor").val(1);
						} else if (setTo == "recruitername") {
							$("#errformRecruiter").val(1);
						}
						//$("#submits").removeAttr("disabled");
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					//  All.set_enable_button();
				}
			});
		}
	},

	getSponsorName: function (nilai, setTo) {
		if (nilai === undefined || nilai === null || nilai === "") {
			alert("Data sponsor/recruiter harus diisi..");
		} else {

			$("#submits").attr("disabled", "disabled");
			$.ajax({
				url: All.get_url("member/id/") + nilai,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					All.set_enable_button();
					if (data.response === "true") {
						//$('#' +setTo).val(data.arrayData[0].sponsorname);
						$('#' + setTo).val(data.sponsorname);
						$("#submits").removeAttr("disabled");

						if (setTo == "sponsorname") {
							$("#errformSponsor").val(0);
						} else if (setTo == "recruitername") {
							$("#errformRecruiter").val(0);
						}

					} else if (data.response == "err") {
						alert("invalid ID member");
						$('#' + setTo).val(null);
						if (setTo == "sponsorname") {
							$("#errformSponsor").val(1);
						} else if (setTo == "recruitername") {
							$("#errformRecruiter").val(1);
						}

					} else {
						alert(data.message);
						$('#' + setTo).val(data.sponsorname);
						if (setTo == "sponsorname") {
							$("#errformSponsor").val(1);
						} else if (setTo == "recruitername") {
							$("#errformRecruiter").val(1);
						}
						//$("#submits").removeAttr("disabled");
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	pilBayar: function (nilai) {
		//alert("test");
		if (nilai == "1") {
			$(".vchcheck").css("display", "block");
		} else {
			$(".vchcheck").css("display", "none");
		}
	},

	checkPilBayar: function () {
		//var pay = $("#pay_tipe").val();
		var pay = $("input[type=radio]:checked").val();
		var x = 0;
		if (pay === "1") {
			$("#formMember .form-list").each(function () {
				if (this.value.length === 0) {
					x++;
				}
			});

			if (x > 0) {
				alert("Masih ada inputan yang kosong..");
				return false;
			} else {
				return true;
			}
		}
		console.log("ISI : " + $("#pay_tipe").val());
	},

	validateInputMember: function () {
		var x = 0;
		$("#formMember .required").each(function () {
			if (this.value.length === 0) {
				x++;
			}
			if (this.value === "--Pilih disini--") {
				x++;
			}
		});

		if (x > 0) {
			alert("Masih ada inputan yang kosong..");
			return false;
		} else {
			return true;
		}
	},

	validateInputMemberDev: function () {
		var x = 0;
		var errformSponsor = parseInt($("#errformSponsor").val());
		var errformRecruiter = parseInt($("#errformRecruiter").val());
		$("#formMember .required").each(function () {
			if (this.value.length === 0) {
				x++;
			}
			if (this.value === "--Pilih disini--") {
				x++;
			}
		});

		if (x > 0) {
			alert("Masih ada inputan yang kosong..");
			return false;
		}
		/*else if(errformSponsor > 0) {
		       	alert("Sponsor harus 1 jaringan dengan anda yang login..");
		       	return false;
		       } else if(errformRecruiter > 0) {
		       	alert("Recruiter harus 1 jaringan dengan anda yang login..");
		       	return false;
		       } */
		else {
			return true;
		}
	},

	validateStarterKit: function () {
		//e.preventDefault();
		/*var del = $('input[name=delivery]').is(':checked');
        var update = $("#updateCart").val();
        var val = $('input:radio[name=delivery]:checked').val();
        var pilihSK = $("#pilStarterkit").val();
        var err = 0;
        if(!del) {
            alert("Silahkan Pilih Tipe Pengiriman");
            $("#submits").attr("disabled", "disabled");
            $('input[name=delivery]').focus();
            err++;
        } else {

	        if(val == "1") {
	        	var provinsi1 = $("#provinsi1").val().length;
	        	var kota1 = $("#kota1").val().length;
	        	var kecamatan1 = $("#kecamatan1").val().length;
	        	var stockist = $("#stockist").val().length;
	        	if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
	        		alert("Silahkan lengkapi data anda sebelum melanjutkan");
	        		err++;
	        	}

	        } else {
	        	var nama_penerima = $("#nama_penerima").val().length;
	        	//var lastname = $("#lastname").val();
	        	var notlp = $("#notlp").val().length;
	        	//var stkarea1 = $("#stkarea1").val();
	        	var stockistref = $("#stockistref").val().length;
	        	var provinsi = $("#provinsi").val().length;
	        	var kota = $("#kota").val().length;
	        	var kecamatan = $("#kecamatan").val().length;
	        	//var kelurahan = $("#kelurahan").val();
	        	var alamat = $("#alamat").val().length;
	        	if(nama_penerima === 0 || notlp === 0
	        		|| stockistref === 0 || provinsi === 0 || kota === 0 || kecamatan === 0
	        		|| alamat === 0) {
	        		alert("Silahkan lengkapi data anda sebelum melanjutkan");
	        		err++;
	        	}


	        }
	    }

	    if(pilihSK == "") {
	    	alert("Silahkan Pilih Starterkit");
            $("#submits").attr("disabled", "disabled");
            $("#pilStarterkit").focus();
            err++;
	    }
          //alert("isi err :" +err);
          if(err > 0) {
          	return false;
          } else {
          	return true;
          }
          */

		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var pilihSK = $("#pilStarterkit").val();
		var err = 0;
		if (!del) {
			alert("Silahkan Pilih Tipe Pengiriman");
			//$("#submits").attr("disabled", "disabled");
			$('input[name=delivery]').focus();
			err++;
			//alert("isi err :" +err);
			return false;
		} else {
			//alert("Pilihan :" +val);
			//return false;

			if (val == "1") {
				var provinsi1 = $("#provinsi1").val();
				var kota1 = $("#kota1").val();
				var kecamatan1 = $("#kecamatan1").val();
				var stockist = $("#stockist").val();
				//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
				/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
					alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
					err++;
					return false;
				} */
				if (provinsi1 === "" || provinsi1 === null || kota1 === "" ||
					kota1 === null || kecamatan1 === "" || kecamatan1 === null ||
					stockist === "" || stockist === null) {
					err++;
					//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
				}

			} else {
				var nama_penerima = $("#nama_penerima").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();
				var stockistref = $("#stockistref").val();
				var provinsi = $("#provinsi").val();
				var kota = $("#kota").val();
				var kecamatan = $("#kecamatan").val();
				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
					stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
					kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
					alamat === "" || alamat === null) {
					err++;
				}

			}
		}

		if (pilihSK === "" || pilihSK === null) {
			//alert("Silahkan Pilih Starterkit");
			//$("#submits").attr("disabled", "disabled");
			//$("#pilStarterkit").focus();
			err++;
		}

		if (err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			return true;
		}
	},

	validateStarterKit_baru: function () {
		//e.preventDefault();
		/*var del = $('input[name=delivery]').is(':checked');
        var update = $("#updateCart").val();
        var val = $('input:radio[name=delivery]:checked').val();
        var pilihSK = $("#pilStarterkit").val();
        var err = 0;
        if(!del) {
            alert("Silahkan Pilih Tipe Pengiriman");
            $("#submits").attr("disabled", "disabled");
            $('input[name=delivery]').focus();
            err++;
        } else {

	        if(val == "1") {
	        	var provinsi1 = $("#provinsi1").val().length;
	        	var kota1 = $("#kota1").val().length;
	        	var kecamatan1 = $("#kecamatan1").val().length;
	        	var stockist = $("#stockist").val().length;
	        	if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
	        		alert("Silahkan lengkapi data anda sebelum melanjutkan");
	        		err++;
	        	}

	        } else {
	        	var nama_penerima = $("#nama_penerima").val().length;
	        	//var lastname = $("#lastname").val();
	        	var notlp = $("#notlp").val().length;
	        	//var stkarea1 = $("#stkarea1").val();
	        	var stockistref = $("#stockistref").val().length;
	        	var provinsi = $("#provinsi").val().length;
	        	var kota = $("#kota").val().length;
	        	var kecamatan = $("#kecamatan").val().length;
	        	//var kelurahan = $("#kelurahan").val();
	        	var alamat = $("#alamat").val().length;
	        	if(nama_penerima === 0 || notlp === 0
	        		|| stockistref === 0 || provinsi === 0 || kota === 0 || kecamatan === 0
	        		|| alamat === 0) {
	        		alert("Silahkan lengkapi data anda sebelum melanjutkan");
	        		err++;
	        	}


	        }
	    }

	    if(pilihSK == "") {
	    	alert("Silahkan Pilih Starterkit");
            $("#submits").attr("disabled", "disabled");
            $("#pilStarterkit").focus();
            err++;
	    }
          //alert("isi err :" +err);
          if(err > 0) {
          	return false;
          } else {
          	return true;
          }
          */

		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var pilihSK = $("#pilStarterkit").val();
		var err = 0;
		if (!del) {
			alert("Silahkan Pilih Tipe Pengiriman");
			//$("#submits").attr("disabled", "disabled");
			$('input[name=delivery]').focus();
			err++;
			//alert("isi err :" +err);
			return false;
		} else {
			//alert("Pilihan :" +val);
			//return false;

			if (val == "1") {
				var provinsi1 = $("#provinsi1").val();
				var kota1 = $("#kota1").val();
				var kecamatan1 = $("#kecamatan1").val();
				var stockist = $("#stockist").val();
				//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
				/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
					alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
					err++;
					return false;
				} */
				if (provinsi1 === "" || provinsi1 === null || kota1 === "" ||
					kota1 === null || kecamatan1 === "" || kecamatan1 === null ||
					stockist === "" || stockist === null) {
					err++;
					//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
				}

			} else {
				var shipper = $("#shipper").val();
				var nama_penerima = $("#nama_penerima").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();
				var stockistref = $("#stockistref").val();
				var provinsi = $("#provinsi").val();
				var kota = $("#kota").val();
				//var kecamatan = $("#kecamatan").val();
				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				/*if(nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
				   stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
				   kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
				   alamat === "" || alamat === null) {
					err++;
				} */

				if (shipper == 1) {
					var kecamatan = $("#kecamatan").val();
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				} else {
					if (nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				}

			}
		}

		if (pilihSK === "" || pilihSK === null) {
			//alert("Silahkan Pilih Starterkit");
			//$("#submits").attr("disabled", "disabled");
			//$("#pilStarterkit").focus();
			err++;
		}

		if (err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			return true;
		}
	},

	checkDoubleKTP: function (nilai) {
		$("#submits").attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("member/ktp/") + nilai,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				All.set_enable_button();
				if (data.sukses === "TRUE") {
					alert(data.responseMessage);
					$("#idno").focus();
					$("#submits").prop("disabled", true);
					$("#errformKTP").val(1);
				} else {
					$("#submits").prop("disabled", false);
					$("#errformKTP").val(0);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	checkDoubleHP: function (nilai) {
		$("#submits").attr("disabled", "disabled");
		$.ajax({
			url: All.get_url("member/no_hp/") + nilai,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				All.set_enable_button();
				if (data.sukses === "TRUE") {
					alert(data.responseMessage);
					$("#submits").prop("disabled", true);
					$("#tel_hp").focus();
					$("#errformHP").val(1);
				} else {
					// alert(data.responseMessage);
					$("#submits").prop("disabled", false);
					$("#errformHP").val(0);
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	checkAgeMember: function (nilai) {
		var chk = Shopping.isValidDate(nilai);
		if (chk != false) {
			$("#submits").removeAttr("disabled");
			var currentTime = new Date();
			var res = nilai.split("/");
			var selisih = parseInt(currentTime.getFullYear()) - parseInt(res[2]);
			//var thn = res[2].parseInt();
			if (selisih < 18) {
				alert("Minimum umur adalah 18 tahun..");
				$("#submits").attr("disabled", "disabled");
			} else {
				$("#submits").removeAttr("disabled");
			}
		} else {
			$("#submits").attr("disabled", "disabled");
		}
	},

	isValidDate: function (inputText) {
		var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;
		// Match the date format through regular expression
		if (inputText.match(dateformat)) {
			//document.form1.text1.focus();
			//Test which seperator is used '/' or '-'
			var opera1 = inputText.split('/');
			// var opera2 = inputText.split('-');
			lopera1 = opera1.length;
			//lopera2 = opera2.length;
			// Extract the string into month, date and year
			if (lopera1 > 1) {
				var pdate = inputText.split('/');
			}
			/*else if (lopera2>1)
			{
				var pdate = inputText.split('-');
			} */

			var dd = parseInt(pdate[0]);
			var mm = parseInt(pdate[1]);
			var yy = parseInt(pdate[2]);
			// Create list of days of a month [assume there is no leap year by default]
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (mm == 1 || mm > 2) {
				if (dd > ListofDays[mm - 1]) {
					alert('Invalid date format!');
					return false;
				}
			}

			if (mm == 2) {
				var lyear = false;
				if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
					lyear = true;
				}

				if ((lyear === false) && (dd >= 29)) {
					alert('Format tanggal invalid');
					return false;
				}
				if ((lyear === true) && (dd > 29)) {
					alert('Format tanggal invalid');
					return false;
				}
			}
		} else {
			alert("Format tanggal invalid");
			//document.form1.text1.focus();
			return false;
		}
	},

	chooseSK: function (nilai) {
		var res = nilai.split("|");
		$("#infoSK").html(null);
		if (nilai !== "") {
			$("#infoSK").css("display", "block");
			var text = "<br />Kode Produk : " + res[0] + "<br />";
			text += "Nama Produk  : " + res[1] + "<br />";
			text += "Berat Produk : " + res[2] + "<br />";
			text += "Harga Barat  : " + res[3] + "<br />";
			text += "Harga Timur  : " + res[4] + "<br />";
			$("#infoSK").html(text);

			if (res[5] === "0") {
				if (res[0] == "SK004AO") {
					alert("PERHATIAN -- Kartu dan product ayu artis oil akan dikirim ke alamat sesuai dengan data member yang diinput");
				} else {
					alert("Starterkit jenis ini tidak akan dikirim ke alamat,silahkan pilih opsi 'Diambil di stockist' ..");
				}

				//$("input:radio[name='delivery']").val(2);
				//$('input[name="delivery"]').val("1");
				//$(".dikirim").css("display", "none");
				$("#is_charge").val(res[5]);
			} else {
				$("#is_charge").val(1);
			}
		} else {
			$("#infoSK").css("display", "none");
		}
	},

	updateSponsorLP: function (param) {
		var memberid = $("#memberid" + param).val();
		var membername = $("#membername" + param).val();
		var idno = $("#idno" + param).val();
		$("#mainDiv").hide();
		$("#divUpd").html(null);
		var htmlx = "<form id='formUpdSponsor'>";

		htmlx += "<span class='title_header'><i class='fa fa-check-square-o'></i> Data Member</span>";
		htmlx += "<br/><table width=70% border=0 cellspacing=1 cellpadding=1>";
		htmlx += "<tr><td width=30%>&nbsp;ID Member</td><td><input type='text' readonly=readonly name='idmemberx' id='idmemberx' style='width:50%;' value='" + memberid + "'/></td></tr>";
		htmlx += "<tr><td>&nbsp;Nama Member</td><td><input type='text' readonly=readonly name='namamemberx' id='namamemberx' style='width:50%;' value='" + membername + "'/></td></tr>";
		htmlx += "<tr><td>&nbsp;ID Sponsor</td><td><input type='text' name='idsponsorx' id='idsponsorx' style='width:50%;' value=''/><input type='button' id='checkIDMember' value='Check ID' class='btn1 btn2 btn-primary1' onclick=Shopping.getMemberNameByIDLP(this.form.idsponsorx,'#sponsornamex') /></td></tr>";
		htmlx += "<tr><td>&nbsp;Nama Sponsor</td><td><input type='text' readonly=readonly name='sponsornamex' id='sponsornamex' style='width:50%;' value=''/></td></tr>";
		htmlx += "<tr><td colspan=2>&nbsp;";
		htmlx += "<input type=button value='Kembali' class='btn1 btn2 btn-primary1' onclick='Shopping.backToListLP()' />";
		htmlx += "&nbsp;<input disabled='disabled' type=button id='btn-update' value='Update' class='btn1 btn2 btn-primary1' onclick='Shopping.updSponsorLP()' />";
		htmlx += "<input type=hidden value='" + param + "' id='paramx' name='paramx' /></td></tr>";
		htmlx += "</table><div id=waitmsg></div>";

		/*  htmlx += "<div class='register-top-grid'>";
		  htmlx += "<div style='width:100%;'>";
		  htmlx += "<span>ID Member<label>*</label></span>";
		  htmlx += "<input type='text' name='idmemberx' id='idmemberx' value='"+memberid+"'/></div>";
		  htmlx += "<div><span>Member Name<label>*</label></span>";
		  htmlx += "<input type='text' name='namamemberx' id='namamemberx' value='"+membername+"'/></div>";

		  htmlx += "<div><span>ID Sponsor<label>*</label></span>";
		  htmlx += "<input type='text' name='idsponsorx' id='idsponsorx' style='width:50%;' value=''/>";
		  htmlx += "<input type='button' id='checkIDMember' value='Check ID' class='btn1 btn2 btn-primary1' onclick='Shopping.getMemberNameByID(this.form.idmemberx,'#membername')' />";
		  htmlx += "</div><div><span>Nama Sponsor<label>*</label></span>";
		  htmlx += "<input readonly='readonly' type='text' name='sponsornamex' id='sponsornamex' value=''/>";
		  </div>
		  */
		htmlx += "</form>";
		$("#divUpd").append(htmlx);
	},

	getMemberNameByIDLP: function (idx, setToDiv) {
		All.set_disable_button();
		$.ajax({
			url: All.get_url("cart/member/id/") + idx.value,
			type: 'GET',
			dataType: 'json',
			success: function (data) {

				All.set_enable_button();
				if (data.sukses == "true") {
					$(setToDiv).val(data.arrayData[0].fullnm);
					$("#btn-update").removeAttr("disabled");
				} else {
					alert("Tidak ada data member dengan ID " + idx.value);
					$(setToDiv).val(null);
					$("#btn-update").attr("disabled", "disabled");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	backToListLP: function () {
		$("#divUpd").html(null);
		$("#mainDiv").show();
	},

	updSponsorLP: function () {
		var paramx = $("#paramx").val();
		$("input[type=button]").attr("disabled", "disabled");
		$("#waitmsg").html("<font color=red>Silahkan tunggu sejenak...</font>");
		$.post(All.get_url("member/reg/sponsor/update"), $("#formUpdSponsor").serialize(), function (data) {
			if (data.response == "true") {
				alert("Data sponsor berhasil di ubah");
				$("#divUpd").html(null);
				$("#mainDiv").show();
				$("#divTblSponsor" + paramx).html(null);
				$("#divTblSponsor" + paramx).html(data.arrayData[0].sponsorid);
			} else {
				alert("Data sponsor gagal di ubah/kode sponsor tidak terdaftar");
			}
			$("#waitmsg").html(null);

		}, "json");
		$("input[type=button]").removeAttr("disabled");
	},

	updtEmailss: function () {
		var new_email = $("#new_email").val();
		var new_hp = $("#new_hp").val();
		$("input[type=button]").attr("disabled", "disabled");
		$.post(All.get_url("update/email"), $("#updt_emailss").serialize(), function (data) {
			if (data.response == "true") {
				alert(data.message);
				//$("#curr_email").val(null);
				$("#new_email").val(null);
				$("#curr_email").val(data.email);
				$("#new_hp").val(null);
				$("#curr_hp").val(data.tel_hp);
				//$("input[type=button]").removeAttr("disabled");
			} else {
				alert(data.message);
				$('#emaills').find('#new_email').focus();
				//$("input[type=button]").removeAttr("disabled");
			}

		}, "json");
		$("input[type=button]").removeAttr("disabled");

	},

	updtEmailMain: function () {
		var new_email = $("#new_email").val();
		$("input[type=button]").attr("disabled", "disabled");
		$.post(All.get_url("updtEmailMain"), $("#updt_emailss").serialize(), function (data) {
			if (data.response == "true") {
				alert(data.message);
				$("#curr_email").val(null);
				$("#new_email").val(null);
				$("#curr_email").val(new_email);
				//$("input[type=button]").removeAttr("disabled");
			} else {
				alert(data.message);
				$('#emaills').find('#new_email').focus();
				//$("input[type=button]").removeAttr("disabled");
			}

		}, "json");
		$("input[type=button]").removeAttr("disabled");

	},

	checkPoinRewards: function () {
		var memberid = $("#idmember").val();
		$.ajax({
			url: All.get_url("promo_rekrut/id/") + memberid,
			type: 'GET',
			success: function (data) {
				$(".result").html(null);
				$(".result").html(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	updPass: function () {
		var new_pass = $("#new_pass").val();
		var conf_pass = $("#conf_pass").val();
		if (new_pass != conf_pass) {
			alert("New Pass dan Confirm Pass harus sama");
		} else {
			$("input[type=button]").attr("disabled", "disabled");
			$.post(All.get_url("update/password"), $("#updt_pass").serialize(), function (data) {
				if (data.response == "true") {
					alert(data.message);
					$("#curr_pass").val(data.password);
					$("#new_pass").val(null);
					$("#conf_pass").val(null);

				} else {
					alert(data.message);
					$('#pwd').find('#new_pass').focus();
				}

			}, "json");
			$("input[type=button]").removeAttr("disabled");
		}

	},

	updBnsStt: function () {
		$("input[type=button]").attr("disabled", "disabled");
		$.post(All.get_url("bnsstt_view/update"), $("#upd_bnsstt_view").serialize(), function (data) {
			alert(data.message);
		}, "json");
		$("input[type=button]").removeAttr("disabled");
	},

	isValidEmailAddress: function (emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	},

	download_file: function (param) {
		if (confirm("Download File ?")) {

			var urlx = All.get_url("download/" + param);
			window.location = urlx;


		}
	},

	checkVoucherBelanja: function (param) {
		var nilai = $("#vch" + param).val();

		if (nilai.length === 0) {
			alert("Kode Voucher masih kosong");
			$("#vch" + param).val(null);
		} else {
			var arrTemp = [];
			timeRepeated = 0;
			var xc = 0;
			$("input[class='chkvch']").each(function () {
				var nlk = $(this).val();
				if (nlk.length !== 0) {
					arrTemp.push(nlk);
					xc++;
					console.log(nlk);
				} else {
					console.log('kosong');
				}
			});

			if (arrTemp.length > 1) {
				arrTemp.pop();
				if ($.inArray(nilai, arrTemp) !== -1) {
					timeRepeated++;
				}
				console.log("arrTemp length : " + arrTemp.length);
				console.log("isi : " + arrTemp);
			}

			arrTemp = null;
			if (timeRepeated > 0) {
				alert("Data voucher " + nilai + " sudah ada dalam inputan..");
				$("#vch" + param).val(null);
			} else {
				$.ajax({
					url: All.get_url("cart/vch/check/") + nilai,
					type: 'GET',
					dataType: 'json',
					success: function (data) {
						if (data.response == "true") {
							var nom = data.arrayData[0].nominal;
							$("#span_nom" + param).text("Rp." + nom);
							$("#nom" + param).val(nom);
						} else {
							alert("Voucher invalid atau sudah pernah di klaim atau expired..");
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + ':' + xhr.status);
						All.set_enable_button();
					}
				});
			}
		}
	},

	listProvinceByCargo: function (idx) {
		All.set_disable_button();
		var txt = $("#" + idx.id + " :selected").text();
		//$(setToDiv).attr("disabled", "disabled");
		if (idx.value == "") {
			alert("Silahkan pilih Ekspedisi/Cargo dahulu");
		} else {
			$.ajax({
				url: All.get_url("cargo/province/list/") + idx.value,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					//$("#provDiv").html(null);
					//$("#kabDiv").html(null);
					//$("#kecDiv").html(null);
					All.set_enable_button();
					if (data.response == "true") {
						var arrayData = data.arrayData;

						if (idx.value == "1") {

							$("#provinsi").css("display", "block");
							$("#kota").css("display", "block");
							$("#kecamatan").css("display", "block");
							$("#stockistref").css("display", "block");

							//$("#provKGBDiv").css("display", "none");
							//$("#kotaKGBDiv").css("display", "none");
							//$("#provDiv").css("display", "block");
							//$("#KabDiv").css("display", "block");
							//$("#KecDiv").css("display", "block");
							$("#provinsi").html(null);
							$("#kota").html(null);
							$("#kecamatan").html(null);
							$("#stockistref").html(null);
							var rowhtml = "<option value=''>--Select Here--</option>";
							//rowhtml += "<span>Provinsi<label><font color=red>&nbsp;*</font></label></span>";
							//rowhtml += "<select class='form-list' name='provinsi' id='provinsi' onchange=Shopping.show_kota(this,'#kota') >";
							//rowhtml += "<option value=''>--Select Here--</option>";
							$.each(arrayData, function (key, value) {
								rowhtml += "<option value=" + value.kode_provinsi + ">" + value.provinsi + "</option>";

							});
							$("#provinsi").append(rowhtml);
							// $("#provinsi").removeAttr("onchange");
							//$("#provinsi").attr("onchange", "Shopping.show_kota(this,'#kota')");

						} else if (idx.value == "2") {
							var rowhtml = "<option value=''>--Select Here--</option>";


							$("#provinsi").html(null);
							$("#kota").html(null);
							$("#kecamatan").html(null);
							$("#stockistref").html(null);
							$("#KecDiv").css("display", "none");
							//$("#provDiv").css("display", "none");
							//$("#KabDiv").css("display", "none");
							//$("#kotaKGBDiv").css("display", "block");
							//$("#provKGBDiv").css("display", "block");
							$.each(arrayData, function (key, value) {
								rowhtml += "<option value='" + value.provinsi + "'>" + value.provinsi + "</option>";

							});
							$("#provinsi").append(rowhtml);
							//$("#provinsi").removeAttr("onchange");
							//$("#provinsi").attr("onchange", "Shopping.show_kota_kgb(this,'#kota')");

						}
					} else {
						//alert(data.message)
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	},

	show_kota_kgb: function (provinsi, setDiv) {
		All.set_disable_button();
		//var txt = $("#"+idx.id+" :selected").text();
		$(setDiv).attr("disabled", "disabled");

		$.ajax({
			url: All.get_url("cargo/kota/list"),
			type: 'POST',
			dataType: 'json',
			data: {
				provinsiNama: provinsi
			},
			success: function (data) {
				var rowhtml = "<option value=''>--Select Here--</option>";
				if (data.response == "true") {
					$(setDiv).removeAttr("disabled");
					$(setDiv).html(null);
					var arrayData = data.arrayData;
					$.each(arrayData, function (key, value) {
						rowhtml += "<option value='" + value.KotaKode + "'>" + value.KotaNama + "</option>";

					});
					//rowhtml += "</select>";
					//rowhtml += "<input type='hidden' id='nama_provinsi' name='nama_provinsi' value='' />";
					$(setDiv).append(rowhtml);
				} else {
					$(setDiv).append(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});

	},

	tampilkanKota: function (setToDiv) {
		All.set_disable_button();

		var shipper = $("#shipper").val();
		var provinsi = $("#provinsi").val();
		$(setToDiv).html(null);
		$(setToDiv).attr("disabled", "disabled");
		if (shipper == "") {
			alert("Silahkan pilih Provinsi dahulu");
		} else {
			var niss = $("#provinsi :selected").text();
			if (shipper == "1") {
				Shopping.show_kotaJNE(provinsi, setToDiv);
			} else {
				Shopping.show_kota_kgb(provinsi, setToDiv);
			}
			$("#nama_provinsi").val(niss);
		}
	},

	tampilkanKecamatan: function () {
		All.set_disable_button();

		var shipper = $("#shipper").val();
		var kota = $("#kota").val();

		if (shipper == "") {
			alert("Silahkan pilih Kota/Kabupaten dahulu");
		} else {
			var niss = $("#kota :selected").text();
			if (shipper == "1") {
				$("#kecamatan").html(null);
				$("#kecamatan").attr("disabled", "disabled");
				Shopping.show_kecamatanJNE(kota, '#kecamatan');
			} else {
				//Shopping.show_kota_kgb(kota, setToDiv);
				$("#stockistref").html(null);
				$("#stockistref").attr("disabled", "disabled");
				Shopping.show_stockistKGB(kota, '#stockistref');
			}
			$("#nama_kota").val(niss);
		}
	},

	show_stockistKGB: function (nilai, setToDiv) {
		$("#destination_address").val(nilai);

		$.ajax({
			url: All.get_url("cargo/stockist/list"),
			type: 'POST',
			dataType: 'json',
			data: {
				kotaKode: nilai
			},
			success: function (data) {
				var rowhtml = "<option value=''>--Select Here--</option>";
				if (data.response == "true") {
					$(setToDiv).removeAttr("disabled");
					$(setToDiv).html(null);
					var arrayData = data.arrayData;
					$.each(arrayData, function (key, value) {
						rowhtml += "<option value='" + value.loccd + "'>" + value.loccd + " - " + value.fullnm + "</option>";

					});
					//rowhtml += "</select>";
					//rowhtml += "<input type='hidden' id='nama_provinsi' name='nama_provinsi' value='' />";
					$(setToDiv).append(rowhtml);
				} else {
					$(setToDiv).append(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	show_kecamatanJNE: function (nilai, setToDiv) {
		$.ajax({
			url: All.get_url("shipping/kecamatan/") + nilai,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				//var txt = $("#"+idx.id+" :selected").text();
				$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.response == "true") {
					//$("#nama_kota").val(txt);
					var arrayData = data.arrayData;
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.kode_kecamatan + ">" + value.kecamatan.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$("#kelurahan").html(null);

				} else {
					$(setToDiv).html(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	show_kotaJNE: function (nilai, setToDiv) {

		$.ajax({
			url: All.get_url("shipping/kota/") + nilai,
			type: 'GET',
			dataType: 'json',
			success: function (data) {


				$(setToDiv).removeAttr("disabled", "disabled");
				All.set_enable_button();
				if (data.response == "true") {
					var arrayData = data.arrayData;
					//$("#nama_provinsi").val(txt);
					$(setToDiv).html(null);
					var rowhtml = "<option value=''>--Select here--</option>";
					$.each(arrayData, function (key, value) {

						rowhtml += "<option  value=" + value.kode_kabupaten + ">" + value.kabupaten.toUpperCase() + "</option>";
						//rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

					});
					$(setToDiv).append(rowhtml);
					$("#kecamatan").val(null);
					$("#stockistref").val(null);
					$("#sender_address").val(null);
					$("#destination_address").val(null);
				} else {
					$(setToDiv).append(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});

	},

	pilFreeProduct: function () {
		var nilai = $("#free_product").val();
		var res = nilai.split("|");
		$("#free_prdcd").val(res[0]);
		$("#free_prdnm").val(res[1]);
		$("#free_qty").val(res[2]);
		$("#free_bv").val(res[3]);
		$("#free_westPrice").val(res[4]);
		$("#free_eastPrice").val(res[5]);
		$("#free_weight").val(res[6]);
	},

	tampilkanPriceCode: function () {
		All.set_disable_button();

		var shipper = $("#shipper").val();
		var stockistref = $("#stockistref").val();
		var kota = $("#kota").val();

		if (shipper == "") {
			alert("Silahkan pilih Stockist dahulu");
		} else {
			//var niss = $("#kota :stockistref").text();
			if (shipper == "1") {
				var kec = $("#kecamatan").val();
				All.set_disable_button();
				$("#pricecode").attr("disabled", "disabled");
				$.ajax({
					url: All.get_url("shipping/getPricecodeX/") + stockistref + "/" + kec,
					type: 'GET',
					dataType: 'json',
					success: function (data) {
						var stkname = $("#stockistref :selected").text();
						$("#pricecode").removeAttr("disabled", "disabled");
						All.set_enable_button();
						if (data.response == "true") {
							$("#nama_stockist").val(stkname);
							$("#nama_stockistr1ref").val(stkname);
							$("#state").val(data.arrayData[0].state);
							$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
							$("#jne_branch").val(data.arrayData[0].jne_branchcd);
							//console.log(data.arrayData[0].kode_kec_JNE_Origin);
							$("#pricecode").val(data.arrayData[0].pricecode);
							if (data.arrayData[0].pricecode == "12E3") {
								$(".westP").css("display", "none");
								$(".eastP").css("display", "block");

							} else {
								$(".westP").css("display", "block");
								$(".eastP").css("display", "none");
							}
							//$("#state").val(data.arrayData[0].pricecode);
							Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
						} else {
							alert("Price code tidak ada");
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + ':' + xhr.status);
						All.set_enable_button();
					}
				});
			} else {
				$.ajax({
					url: All.get_url("cargo/pricecode/") + kota,
					type: 'GET',
					dataType: 'json',
					success: function (data) {

						var stkname = $("#stockistref :selected").text();
						$("#pricecode").removeAttr("disabled", "disabled");
						All.set_enable_button();
						if (data.response == "true") {
							//var arrayData = data.arrayData;
							$("#nama_stockist").val(stkname);
							$("#nama_stockistr1ref").val(stkname);
							$("#pricecode").val(data.arrayData[0].pricecode);
							$("#sender_address").val(null);
							$("#jne_branch").val(null);
							if (data.arrayData[0].pricecode == "12E3") {
								$(".westP").css("display", "none");
								$(".eastP").css("display", "block");

							} else {
								$(".westP").css("display", "block");
								$(".eastP").css("display", "none");
							}
							Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
						} else {
							alert("Price code tidak ada..")
							//$(setToDiv).append(null);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + ':' + xhr.status);
						All.set_enable_button();
					}
				});
			}
			//$("#nama_kota").val(niss);
		}
	},


	tampilkanPriceCode2: function () {
		All.set_disable_button();
		console.log("heyyy");
		var shipper = $("#shipper").val();
		var stockistref = $("#stockistref").val();
		var kota = $("#kota").val();

		if (shipper == "") {
			alert("Silahkan pilih Stockist dahulu");
		} else {
			//var niss = $("#kota :stockistref").text();
			if (shipper == "1") {
				var kec = $("#kecamatan").val();
				All.set_disable_button();
				$("#pricecode").attr("disabled", "disabled");
				$.ajax({
					url: All.get_url("shipping/getPricecodeX/") + stockistref + "/" + kec,
					type: 'GET',
					dataType: 'json',
					success: function (data) {
						var stkname = $("#stockistref :selected").text();
						$("#pricecode").removeAttr("disabled", "disabled");
						All.set_enable_button();
						if (data.response == "true") {
							$("#nama_stockist").val(stkname);
							$("#nama_stockistr1ref").val(stkname);
							$("#state").val(data.arrayData[0].state);
							$("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
							$("#jne_branch").val(data.arrayData[0].jne_branchcd);
							//console.log(data.arrayData[0].kode_kec_JNE_Origin);
							$("#pricecode").val(data.arrayData[0].pricecode);
							if (data.arrayData[0].pricecode == "12E3") {
								$(".westP").css("display", "none");
								$(".eastP").css("display", "block");

							} else {
								$(".westP").css("display", "block");
								$(".eastP").css("display", "none");
							}
							//$("#state").val(data.arrayData[0].pricecode);
							Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);


							var sender = $("#sender_address").val();
							var jbranch = $("#jne_branch").val();
							if (sender != '' && jbranch != '') {
								console.log("berhasiwl");

								$.ajax({
									url: All.get_url("cargo/price/"),
									type: 'POST',
									data: {
										cargo_id: 1,
										dest_code: $("#kecamatan").val(),
										stockist_code: $("#stockistref").val(),
										weight: $("#totalAmountWeight").val()

									},
									dataType: 'json',
									success: function (data) {


										//$("#provDiv").html(null);
										//$("#kabDiv").html(null);
										//$("#kecDiv").html(null);
										All.set_enable_button();
										if (data.response == "true") {
											var arrayData = data.arrayData;
											//alert("hore");
											$("#amountKIRIM").val(arrayData.price);
											//console.log(arrayData);
										} else {
											alert("Area ini tidak ada pricelist nya..");
										}
									},
									error: function (xhr, ajaxOptions, thrownError) {
										alert(thrownError + ':' + xhr.status);
										All.set_enable_button();
									}
								});




							} else {
								console.log("tidaaak");
								console.log("sender " + sender);
								console.log("jbranch " + jbranch);
							}


						} else {
							alert("Price code tidak ada");
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + ':' + xhr.status);
						All.set_enable_button();
					}
				});




			} else {
				$.ajax({
					url: All.get_url("cargo/pricecode/") + kota,
					type: 'GET',
					dataType: 'json',
					success: function (data) {

						var stkname = $("#stockistref :selected").text();
						$("#pricecode").removeAttr("disabled", "disabled");
						All.set_enable_button();
						if (data.response == "true") {
							//var arrayData = data.arrayData;
							$("#nama_stockist").val(stkname);
							$("#nama_stockistr1ref").val(stkname);
							$("#pricecode").val(data.arrayData[0].pricecode);
							$("#sender_address").val(null);
							$("#jne_branch").val(null);
							if (data.arrayData[0].pricecode == "12E3") {
								$(".westP").css("display", "none");
								$(".eastP").css("display", "block");

							} else {
								$(".westP").css("display", "block");
								$(".eastP").css("display", "none");
							}
							Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
						} else {
							alert("Price code tidak ada..")
							//$(setToDiv).append(null);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + ':' + xhr.status);
						All.set_enable_button();
					}
				});
			}
			//$("#nama_kota").val(niss);
		}
	},

	pilihHadiah: function () {
		var nilai = $("#free_product").val();
		var res = nilai.split("|");
		$("#free_prdcd").val(res[0]);
		$("#free_prdnm").val(res[1]);
		$("#free_qty").val(res[2]);
		$("#free_bv").val(res[3]);
		$("#free_westPrice").val(res[4]);
		$("#free_eastPrice").val(res[5]);
		$("#free_weight").val(res[6]);
	},

	listProvinsi: function (idx) {
		All.set_disable_button();
		var txt = $("#" + idx.id + " :selected").text();
		//$(setToDiv).attr("disabled", "disabled");
		console.log('1');
		if (idx.value == "") {
			alert("Silahkan pilih Ekspedisi/Cargo dahulu");
		} else {
			$.ajax({
				url: All.get_url("cargo/province/list/") + idx.value,
				type: 'GET',
				dataType: 'json',
				success: function (data) {


					//$("#provDiv").html(null);
					//$("#kabDiv").html(null);
					//$("#kecDiv").html(null);
					All.set_enable_button();
					if (data.response == "true") {
						var arrayData = data.arrayData;

						if (idx.value == "1") {
							$("#provinsi").html(null);
							//$("#KecDiv").css("display", "block");
							//$("#kota").html(null);
							//$("#kecamatan").html(null);
							//$("#stockistref").html(null);
							var rowhtml = "<option value=''>--Select Here--</option>";
							//rowhtml += "<span>Provinsi<label><font color=red>&nbsp;*</font></label></span>";
							//rowhtml += "<select class='form-list' name='provinsi' id='provinsi' onchange=Shopping.show_kota(this,'#kota') >";
							//rowhtml += "<option value=''>--Select Here--</option>";
							$.each(arrayData, function (key, value) {
								rowhtml += "<option value=" + value.kode_provinsi + ">" + value.provinsi + "</option>";

							});
							$("#provinsi").append(rowhtml);

						} else if (idx.value == "2") {
							var rowhtml = "<option value=''>--Select Here--</option>";
							$("#provinsi").html(null);
							//$("#kota").html(null);
							//$("#kecamatan").html(null);
							//$("#stockistref").html(null);
							$.each(arrayData, function (key, value) {
								rowhtml += "<option value='" + value.provinsi + "'>" + value.provinsi + "</option>";

							});
							$("#provinsi").append(rowhtml);

						}

					} else {
						//alert(data.message)
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
				}
			});
		}
	}

}

//jQuery(document).ready(function($){
//    if($('#updt_emailss').length) {
//        console.log('masuk if..');
//        $('#new_email').blur(function(){
//            console.log('masuk blur..');
//            var strValid = Shopping.isValidEmailAddress($('#new_email').val());
//            if(strValid === false) {
//                alert('Penulisan email salah');
//                $('#new_email').focus();
//            }
//        });
//    }
//
//});
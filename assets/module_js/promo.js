var $ = jQuery;

var Promo = {
	getMemberInfo : function(nilai, setTo) {
    	$("#submits").attr("disabled", "disabled");
    	$('#resultCheck').html(null);
    	var rowhtml = "";
    	rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'>";
        rowhtml += "<div class='col-lg-12 col-md-12 col-xs-12'><font size=6><font size=4>Silahkan tunggu sebentar..</font></font></div>";
        rowhtml += "</div>";
	    $('#resultCheck').append(rowhtml);
    	$.ajax({
	            url: All.get_url("member/id/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                
	                if(data.response === "true") {
	                 //$('#' +setTo).val(data.arrayData[0].sponsorname);
	                 var promotype = $("#promotype").val();
	                 $('#' +setTo).val(data.sponsorname);
	                 Promo.cekPromo500BV(nilai, promotype);
	                } else if(data.response == "err"){
	                	alert("invalid ID member");
	                	$('#' +setTo).val(null);
	                	 $("#btnSavePromo").attr("disabled", "disabled");
	                } else {
	                	alert(data.message);
	                	$("#btnSavePromo").attr("disabled", "disabled");
	                 	$('#' +setTo).val(null);
	                 	$('#resultCheck').html(null);
	                }
					
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            });
    },
    
    cekPromo500BV : function (nilai, promotype) {
    	 $("#submits").attr("disabled", "disabled");
    	$.ajax({
	            url: All.get_url("promo/calendar/check/") +nilai + "/" +promotype,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                 var rowhtml = "";
	                if(data.response == "true") {
	                	 var arrayData = data.arrayData;
	                	 var dataLimit = data.dataLimit;
		              	 rowhtml += "<div class='col-lg-12 col-xs-12 cart_header'>";
			             rowhtml += "<div class='col-lg-3 col-xs-3'>Bonus Period</div>"; 
			             rowhtml += "<div class='col-lg-3 col-xs-3'>Total DP</div>";
			             rowhtml += "<div class='col-lg-3 col-xs-3'>Total BV</div>"; 
			             rowhtml += "<div class='col-lg-3 col-xs-3'>Promo Type</div> ";         	 
			             rowhtml += "</div>";
		              	 $.each(arrayData, function(key, value) {
		              	 	
		                    rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'>";
		                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.bnsperiod+"</div>";
		                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.dp+"</div>";
		                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.bv+"</div>";
		                    rowhtml += "<div class='col-lg-3 col-xs-3'>"+value.promotype+"</div></div>";
	                        rowhtml += "</div>";
	                        
		                 });
		                 $("#promocode").val(dataLimit[0].promocode);
		                 $("#prdcd").val(dataLimit[0].prdcd);
		                 $("#qtyPrd").val(dataLimit[0].qtyord);
		                 $("#totalPay").val(arrayData[0].dp);
		                 $("#totalBV").val(arrayData[0].bv);
		                 $("#pricecode").val(arrayData[0].pricecode);
		                 var stkchoose = arrayData[0].loccd + "|" + arrayData[0].stkname + "|" + arrayData[0].pricecode;
		                 $("#idstk").val(stkchoose);
		                 Promo.setStockist();
		                 $("#btnSavePromo").removeAttr("disabled");
	                } else if(data.response == "double") {
	                	    
		                	 var urlx = All.get_url("promo/calendar/print/") + data.orderno;
		                	 rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'>";
		                     rowhtml += "<div class='col-lg-12 col-md-12 col-xs-12'><font size=5>"+data.message+"</font>";
		                     rowhtml += "<br /><form method=post action = '"+urlx+"' target='_BLANK'>";
		                     rowhtml += "<input type=submit value='Print Nota' /></form>";
		                     rowhtml += "</div>";
		                     rowhtml += "</div>";
                     	
	                } 
	                
	                else {
	                		rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'>";
		                    rowhtml += "<div class='col-lg-12 col-md-12 col-xs-12'><font size=6>"+data.message+"</font></div>";
	                        rowhtml += "</div>";
	                        $("#promocode").val(null);
		                 	$("#prdcd").val(null);
		                 	$("#qtyPrd").val(null);
		                 	$("#totalPay").val(null);
		                    $("#totalBV").val(null);
		                    $("#pricecode").val(null);
	                        $("#btnSavePromo").attr("disabled", "disabled");
	                }
	                $('#resultCheck').html(null);
	                $('#resultCheck').append(rowhtml);	
	                
					
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
            });
    },
    
    setStockist : function() {
    	 var x = $("#idstk").val();
    	 var str = x.split("|");
	  	 $("#idstkx").val(str[0]);
	  	 $("#stkname").val(str[1]);
	  	 $("#pricecode").val(str[2]);
    },
    
    saveDataPromo : function() {
    	 $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("promo/calendar/save") , $("#promoCalendar").serialize(), function(data)
            {  
                $("input[type=button]").removeAttr('disabled');
                if(data.response === "true") {
                	 
                	 Promo.clearFormData();
                	 var rowhtml = "";
                	 var urlx = All.get_url("promo/calendar/print/") + data.arrayData[0].orderno;
                	 rowhtml += "<div class='col-lg-12 col-xs-12 cart_content text-center'>";
                     rowhtml += "<div class='col-lg-12 col-md-12 col-xs-12'><font size=5>"+data.message+"</font>";
                     rowhtml += "<br /><form method=post action = '"+urlx+"' target='_BLANK'>";
                     rowhtml += "<input type=submit value='Print Nota' /></form>";
                     rowhtml += "</div>";
                     rowhtml += "</div>";
                     $('#resultCheck').append(rowhtml);	
                } else {
                	 alert(data.message);
                }
            },"json");
    },
    
    clearFormData : function() {
    	$("#idmember").val(null);
     	$("#membername").val(null);
     	$("#idstk").val(null);
    	$("#promocode").val(null);
     	$("#prdcd").val(null);
     	$("#qtyPrd").val(null);
     	$("#totalPay").val(null);
        $("#totalBV").val(null);
        $("#pricecode").val(null);
        $("#resultCheck").html(null);
        $("#btnSavePromo").attr("disabled", "disabled");
    },
    
    searchIDC : function(){
        var memberid = $("#memberid").val();
        var bnsperiod = $("#bnsperiod").val();
        All.set_disable_button();
        $.ajax({
	            url: All.get_url("be/promo/detail"),
	            type: 'POST',
                data: {memberid:memberid,bnsperiod:bnsperiod},
	            success: function(data){
	                All.set_enable_button();
                    $("#detail-idc").html(null);
                    $("#detail-idc").html(data);
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
        });
        /*$("input[type=button]").attr('disabled', 'disabled');
        $("#detail-idc").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url("promo/idc/detail") , $("#cekPoinIDc").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $("#detail-idc").show();
            $("#detail-idc").html(null);
            $("#detail-idc").html(hasil);   
        });*/
    },
    
    getDetIEC : function(bnsperiod,idmember,flag){
        All.set_disable_button();
        $("input[type=button]").attr('disabled', 'disabled');
        $("#SummIEC").hide();
        $("#detIEC").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.ajax({
            url: All.get_url("be/iec/detail/" +bnsperiod +"/" +idmember +"/" +flag ),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
                
                $("#detIEC").html(null);
                $("#detIEC").html(data);
                $("#detIEC").show();
            }
        });
    },
    
    back_to_form : function (){
            $("#SummIEC").show();
            $("#detIEC").hide();
            $("#detIEC").html(null);
            //$("#detIEC").html(data);
                
    },
    
    getListPromoNovApr2018 : function() {
    	//$("input[type=button]").attr('disabled', 'disabled');
    	var bnsperiod = $("#bnsperiod").val();
    	var searchBy = $("#searchBy").val();
    	var searchValue = $("#searchValue").val();
    	$("#judul").html(null);
            	$("#showDtaPemenang").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
    	$.ajax({
            url: All.get_url("promo_novapr/bns"),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'POST',
            data: {searchBy: searchBy, searchValue: searchValue, bnsperiod: bnsperiod},
            success:
            function(data){
            	
                //$("input[type=button]").removeAttr('disabled');
                $("#judul").html(null);
                var teks = "Daftar Pemenang Periode Bonus" +bnsperiod;
                $("#judul").html(teks);
                $("#showDtaPemenang").html(null);
                $("#showDtaPemenang").html(data);
             
            }
        });
    },
    
    getDetailHadiahPrm: function(param) {
    	var vchno = $("#vch" +param).val();
    	$.ajax({
            url: All.get_url("promo_novapr/detailPrd"),
            //url: All.get_url("sales/detail/transaksi"),
            type: 'POST',
            data: {VoucherNo: vchno},
            success:
            function(data){
            	$("#formUtama").hide();
                //$("input[type=button]").removeAttr('disabled');
                $("#detailProduk").html(null);
                $("#detailProduk").html(data);
             
            }
        });
    },

	getDetailKupon: function(param) {
		var dfno = $("#dfno" +param).val();
		$.ajax({
			url: All.get_url("det_kupon"),
			type: 'POST',
			data: {dfno: dfno},
			success:
				function(data){
					$("#formUtama").hide();
					$("#detailkupon").html(null);
					$("#detailkupon").html(data);
				}
		});
	},


	backToFormLBC : function() {
		$("#detailLBC").html(null);
		$("#formUtama").show();
	},

	backToFormKupon : function() {
		$("#detailkupon").html(null);
		$("#formUtama").show();
	},
    
    backToPrmForm : function() {
    	$("#detailProduk").html(null);
    	$("#formUtama").show();
    },

	getDetailLBC: function(param) {
		var dfno = $("#dfno" +param).val();
		$.ajax({
			url: All.get_url("det_promolbc"),
			type: 'POST',
			data: {dfno: dfno},
			success:
				function(data){
					$("#formUtama").hide();
					$("#detailkupon").html(null);
					$("#detailkupon").html(data);
				}
		});
	}
}
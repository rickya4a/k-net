var $ = jQuery;

var Umroh = {
    
    get_distributor_info : function (idmember){
        if(idmember == ""){
            
            $("#formUmroh input[type='text']").val('');
            $("#fullnm").attr('readonly', false);
        }
        
        if($('input[name^="tipeJamaah"]:checked').val() === '1') {
            // asumsi panjang id member PASTI 10 digit
            $("#submits").removeAttr("disabled");
            $.post(All.get_url("umroh/helper/distInfo/"), {
                "idmember" : idmember
            }, function(res){
                var data = JSON.parse(res);
                if(data.adadata == 'salah') 
                {  
                    $("#submits").attr("disabled", "disabled");
                     alert('ID Sponsor salah atau sudah termination')
                     /("#fullnm").attr('readonly', true);
                     //$("#fullnm").attr('readonly', false);
                     $("#formUmroh input[type='text']").val('');
                     $("#idmember").focus();
                }
                else
                {
                     var tgl = data.birthdt;
                     var loccd = data.loccd;
                     var sfno = data.sfno;
	                 res = tgl.split("-"); 
                     //$("#idno").attr('readonly', true);
                     $("#fullnm").val(data.fullnm);
                     $("#addr1").val(data.addr);
                     $("#tel_hp").val(data.tel_hp);
                     $("#tel_hm").val(data.tel_hm);
                     $("#idno").val(data.noktp).attr('readonly',true);
                     $("#tgllhr").val(res[0]);
					 $("#blnlhr").val(res[1]);
					 $("#thnlhr").val(res[2]);
                     $("#idstk").val(loccd);
                     $("#idsponsor").val(sfno);
                     $("#fullnm").attr('readonly', true);
                     //$("#tel_hp").attr('readonly', true);
                     $("#tel_hp").attr('readonly', false);
                     //$("#idno").attr('readonly', false);
                     $("#submits").removeAttr("disabled");
                }  
            });
        }
    },
    
    get_sponsor_info: function (idsponsor){
        //if(idsponsor.length == 12) {
            // asumsi panjang id member PASTI 10 digit
            $("#submits").removeAttr("disabled");
            $.post(All.get_url("umroh/helper/sponsorInfo/"), {
                "idsponsor" : idsponsor
            }, function(res){
                var data = JSON.parse(res);
                if(data.adadata == 'salah') 
                {  
                    $("#submits").attr("disabled", "disabled");
                     alert('ID Sponsor salah atau sudah termination')
                     /("#nmsponsor").attr('readonly', true);
                     //$("#fullnm").attr('readonly', false);
                     $("#formUmroh input[type='text']").val('');
                     $("#idsponsor").focus();
                }
                else
                {
                     $("#nmsponsor").val(data.fullnm);
                     $("#idno").focus();
                     $("#nmsponsor").attr('readonly', true);
                     $("#submits").removeAttr("disabled");
                }  
            });
       // }
    },
    
    cekNoKtp : function(noktp){
        $("#submits").removeAttr("disabled");
        $.post(All.get_url("umroh/helper/ktpInfo/"), {
            "noktp" : noktp
        }, function(res){
            var data = JSON.parse(res);
            
            //if($('input[name^="tipeJamaah"]:checked').val() === '0')){
	            if(data.adadata == 'salah') 
	            {  
	                $("#submits").attr("disabled", "disabled");
	                 alert('No Ktp Sudah Terdaftar atas nama ' +data.fullnm);
	                 $("#idmember").focus();
	            }
	            else
	            {
	                 $("#sex").focus();
	                 $("#submits").removeAttr("disabled");
	            }
	    	//}  
        });
    },
    
    cekNoHp : function(tel_hp){
        $("#submits").removeAttr("disabled");
        $.post(All.get_url("umroh/helper/hpInfo/"), {
            "tel_hp" : tel_hp
        }, function(res){
            var data = JSON.parse(res);
            if(data.adadata == 'salah') 
            {  
                $("#submits").attr("disabled", "disabled");
                 alert('No Hp Sudah Terdaftar atas nama ' +data.fullnm);
                 $("#tel_hp").focus();
            }
            else
            {
                 $("#tel_hm").focus();
                 $("#submits").removeAttr("disabled");
            }  
        });
    },
    
    addJamaah: function (){
        All.set_disable_button();
        $.post(All.get_url("umroh/add/jamaah") , $("#formUmroh").serialize(), function(hasil)
        {  
            All.set_enable_button();
            if(hasil.response == "false") {
                alert(hasil.message);
            }else{
                alert(hasil.message);
                $("#formUmroh input[type='text']").val('');
            }
        },"json");
    },
    
    getPreview : function (){
        All.set_disable_button();
        $.post(All.get_url("umroh/preview") , $("#formUmroh").serialize(), function(hasil)
        {
            All.set_enable_button();
            //if(hasil == "Semua field harus diisi..!!" || hasil == "KTP atau Passport Sudah Terpakai") {
            if(hasil == "Semua field harus diisi..!!" || hasil == "Ktp Sudah Terdaftar" || hasil == "No Hp Sudah Terdaftar" || hasil == "Umur Kurang dari 18 Tahun" || hasil == "Silahkan Pilih Stockist" || hasil == "Jadwal Keberangkatan Sudah Terpakai" || hasil =="Id Sponsor Tidak Boleh Kosong" || hasil == "No Hp Tidak Boleh Kosong" || hasil == "Silahkan Pilih Stockist Tujuan") {
                alert(hasil);
            }else{
                $("#formJamaah").hide();
                $("#jamaahPreview").html(hasil);
            }
        });
    },
    
    deleteJamaah : function (noArr,noDiv){
        if(confirm("Hapus Calon Jamaah ?"))
         {
             var divs = $("#isi" +noArr).val();
             //alert('isinya '+noArr);
             $.ajax({
                 dataType: 'json',   
                 type: "GET",
                 url : All.get_url("umroh/delete/sessJamaah/" +noArr ),
                 success: function(data)
                 {
                    if(data.response == "true")
                    {
                        alert(data.message);
                        //$("#isi"+noDiv).animate({ opacity: "hide" }, "slow");
                        $("#isi"+noDiv).remove();
                    }
                    else
                    {
                        alert(data.message);
                    }
                    
                 }
             });
        }
    },
    
    backtoForm : function(){
        $("#formJamaah").show();
        $("#formJamaah input[type='text']").val('');
        $("#jamaahPreview").html(null);
    },
    
    gotopayment : function(){
        All.set_disable_button();
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("umroh/payment") , $("#calonJamaah").serialize(), function(hasil)
            {  
                All.set_enable_button();
                
                $("#formJamaah").hide();
                $("#prevsJamaah").hide();
                $("#gotoPayment").show();
                
                $("#gotoPayment").html(null);
                $("#gotoPayment").html(hasil);
            });
    },
    
    backtoPrev : function(){
        $("#gotoPayment").hide();
        $("#formJamaah").hide();
        $("#prevsJamaah").show();
    },
    
    postPayment : function(){
        All.set_disable_button();
            
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("umroh/post/payment") , $("#postPaymentsUmroh").serialize(), function(hasil)
            {  
                if(hasil == "Saldo Tidak Mencukupi") {
                    alert(hasil);
                }else{
                    $("#formJamaah").hide();
                    $("#jamaahPreview").hide();
                    $("#gotoPayment").hide();
                    $("#hasilPayment").html(null);
                    $("#hasilPayment").html(hasil);
                }
            });
    },
    
    getTipeJamaah : function(){
        if($('input[name^="tipeJamaah"]').length) {
            var arrThn = [];
            $('#thnlhr').next('select').find('option').each(function(){
              arrThn.push($(this).val());
            });
            $('#thnlhr').html('');
            for(var i = 18; i < arrThn.length; i++) {
                $('#thnlhr').append('<option value="'+arrThn[i]+'">'+arrThn[i]+'</option>');
            }
            $('#tel_hp,#email').removeAttr('disabled');
            $("#fullnm").attr('readonly', true);
            $("#idno").attr('readonly', true);
            /*$("#idsponsor").attr('readonly', true);
            $("#nmsponsor").attr('readonly', true);*/
            //$("#idstk").attr('disabled', 'disabled');
            $('#idsponsor, #nmsponsor').parent().addClass('hide');
            
            $('input[name^="tipeJamaah"]').change(function(e) {
                $('#thnlhr').html('');
                if($('input[name^="tipeJamaah"]:checked').val() === '2') {
                    for(var i = 0; i <= 17; i++) {
                        $('#thnlhr').append('<option value="'+arrThn[i]+'">'+arrThn[i]+'</option>');
                    }
                    $('#idno').attr('readonly', true);
                    
                    $('#idno').val('');
                    $("#formJamaah input[type='text']").val('');
                    $("#addr1").val('');
                    $("#fullnm").removeAttr('readonly');
                    $("#addr1").removeAttr('readonly');
                    $("#tel_hp").removeAttr('readonly');
                    //$("#idstk").attr('disabled', 'disabled');
                    /*$("#idsponsor").attr('readonly');
                    $("#nmsponsor").attr('readonly');*/
                    $("#idmember").attr('readonly');
                    $('#idsponsor, #nmsponsor').parent().addClass('hide');
                    $("#fullnm").focus();
                   
                } else if($('input[name^="tipeJamaah"]:checked').val() === '1') {
                    for(var i = 18; i < arrThn.length; i++) {
                        $('#thnlhr').append('<option value="'+arrThn[i]+'">'+arrThn[i]+'</option>');
                    }
                    //$('#idno,#email,#idstk').attr('disabled');
                    $("#formJamaah input[type='text']").val('');
                    $("#fullnm").attr('readonly');
                    $("#idno").attr('readonly');
                    $("#addr1").attr('readonly');
                    $("#tel_hp").attr('readonly');
                    $("#addr1").val('');
                    //$("#idstk").attr('disabled', 'disabled');
                    /*$("#idsponsor").attr('readonly');
                    $("#nmsponsor").attr('readonly');*/
                    $('#idsponsor, #nmsponsor').parent().addClass('hide');
                    $("#idmember").focus();
                }else{
                    for(var i = 18; i < arrThn.length; i++) {
                        $('#thnlhr').append('<option value="'+arrThn[i]+'">'+arrThn[i]+'</option>');
                    }
                    $('#idno,#tel_hp,#email,#idstk').removeAttr('disabled');
                    $("#formJamaah input[type='text']").val('');
                    $("#fullnm").removeAttr('readonly');
                    $("#idno").removeAttr('readonly');
                    $("#addr1").removeAttr('readonly');
                    $("#tel_hp").removeAttr('readonly');
                    $("#addr1").val('');
                    $("#idmember").attr('readonly');
                    //$("#idsponsor").removeAttr('readonly');
                    $('#idsponsor, #nmsponsor').parent().removeClass('hide');
                    $("#fullnm").focus();
                }
            });
        }
    },

    isValidEmailAddress: function(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    },
    
    searchMutasi: function(){
        var noreg = $('#noregisters').val();
        var idmember = $('#idmembers').val();
        /*if(noreg == ""){
            alert('Field Register No Harus Diisi');
            
        }else if(idmember == ""){
            alert('Field Id Member Harus Diisi');
        }
        else
        {*/
            All.set_disable_button();
            $("#resPencarianUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("umroh/get/mutasi") , $("#cekmutasi").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $("#resPencarianUmroh").html(null);
                $("#resPencarianUmroh").html(hasil);   
            });
        //}
    },
    
    getRegnoInfo : function(regnos){
        $("#submits").removeAttr("disabled");
                $.ajax({
                 dataType: 'json',   
                 type: "POST",
                 url : All.get_url("umroh/helper/regnoInfo"),
                 data : {regnos: regnos},
                 success: function(data)
                 {
                    var arrayData = data.arrayData;
                    if(data.response == "true") {
                        $("#idmember").val(arrayData[0].dfno);
                         $("#nmmember").val(arrayData[0].fullnm);
                         $("#pktUmroh").val(arrayData[0].departuredesc);
                         $("#saldo").val(arrayData[0].saldo);
                         $("#tipePerjalanan").val(arrayData[0].tipe_perjalanan);
                         $("#amtUmroh").focus();
                         $("#submits").removeAttr("disabled");
                    } else {
                        $("#submits").attr("disabled", "disabled");
                         alert('RegisterNo Salah');
                         $("#idmember").attr('readonly', true);
                         $("#nmmember").attr('readonly', true);
                         $("#pktUmroh").attr('readonly', true);
                         $("#saldo").attr('readonly', true);
                        
                         $("#regnos").focus();
                    }
                } 
            });
    },
    
    getRegnoInfo1 :  function(regnos){
        $("#submits").removeAttr("disabled");
                $.ajax({
                 dataType: 'json',   
                 type: "POST",
                 url : All.get_url("ziarah/helper/regnoInfo"),
                 data : {regnos: regnos},
                 success: function(data)
                 {
                    var arrayData = data.arrayData;
                    if(data.response == "true") {
                        $("#idmember").val(arrayData[0].dfno);
                         $("#nmmember").val(arrayData[0].fullnm);
                         $("#pktUmroh").val(arrayData[0].departuredesc);
                         $("#saldo").val(arrayData[0].saldo);
                         $("#tipePerjalanan").val(arrayData[0].tipe_perjalanan);
                         $("#amtUmroh").focus();
                         $("#submits").removeAttr("disabled");
                    } else {
                        $("#submits").attr("disabled", "disabled");
                         alert('RegisterNo Salah');
                         $("#idmember").attr('readonly', true);
                         $("#nmmember").attr('readonly', true);
                         $("#pktUmroh").attr('readonly', true);
                         $("#saldo").attr('readonly', true);
                        
                         $("#regnos").focus();
                    }
                } 
            });
    },
    
    searchTransaksi : function() 
    {
        var vchno = $('#vchno').val();
        /*var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);*/
        if(vchno == ""){
            alert('Field Voucher No Tidak Boleh Kosong');
            //All.set_disable_button();
        }
        else
        {
            All.set_disable_button();
            $("#hasilPencarian").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("umroh/printVchNo/act") , $("#cekVchnoo").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $("#hasilPencarian").html(null);
                $("#hasilPencarian").html(hasil);   
            });
        }
        
    },
    
    searchRegno : function (){
         var vchno = $('#vchno').val();
        /*var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);*/
        if(vchno == ""){
            alert('Field Register No Tidak Boleh Kosong');
            //All.set_disable_button();
        }
        else
        {
            All.set_disable_button();
            $("#resPencarian").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $("input[type=button]").attr('disabled', 'disabled');
            $.post(All.get_url("umroh/reprint/act") , $("#cekRegno").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $("#resPencarian").html(null);
                $("#resPencarian").html(hasil);   
            });
        }
    },
    
    
    
   /* getFormPays : function(){
        All.set_disable_button();
        $.post(All.get_url("umroh/installments/pay") , $("#formCicilan").serialize(), function(hasil)
        {
            All.set_enable_button();
            //if(hasil == "Semua field harus diisi..!!" || hasil == "KTP atau Passport Sudah Terpakai") {
            if(hasil == "Semua field harus diisi..!!" || hasil == "Ktp Sudah Terdaftar" || hasil == "No Hp Sudah Terdaftar" || hasil == "Umur Kurang dari 18 Tahun" || hasil == "Silahkan Pilih Stockist" || hasil == "Jadwal Keberangkatan Sudah Terpakai") {
                alert(hasil);
            }else{
                $("#formJamaah").hide();
                $("#jamaahPreview").html(hasil);
            }
        });
    },*/
   
}

jQuery(document).ready(function($){
    if($('#formUmroh').length) {
        var thnlhrOri = $('<select class="hide"></select>');
        var thnlhrOpt = $('#thnlhr option').clone();
        thnlhrOri.append(thnlhrOpt).insertAfter('#thnlhr');
        Umroh.getTipeJamaah();
        $('#email').blur(function(){
            var strValid = Umroh.isValidEmailAddress($('#email').val());
            if(strValid === false) {
                alert('Alamat email salah');
                $('#email').focus();
            }
        });
    }
    
    /*pilihTipePerjalanan = function() {
        if ($('input[name="tipePrjlanan"]:checked').val() === 'umr') 
        {
            $('#jdwlbrkt option[value*="Umroh"]').show();
            $('#jdwlbrkt option[value*="Yerusalem"]').hide();
        } else 
        {
            $('#jdwlbrkt option[value*="Umroh"]').hide();
            $('#jdwlbrkt option[value*="Yerusalem"]').show();
        }
        $('#jdwlbrkt').val($('#jdwlbrkt option[style*="block"]:first').val());
       
    };
    
    if($('input[name="tipePrjlanan"]').length) {
         untuk inisialisasi saat document.ready, langsung panggil fungsinya 
        pilihTipePerjalanan();
        $('#jdwlbrkt').val($('#jdwlbrkt option:first').val());
         bind change-event pada input dengan nama tipePrjlanan untuk mengeksekusi fungsinya 
        $('input[name="tipePrjlanan"]').on('change',function() {
        pilihTipePerjalanan();
        });
    }*/
    
});
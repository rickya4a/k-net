if (typeof NetFunnel == "object"){
    NetFunnel.SkinUtil.add('klink',{
        prepareCallback:function(){
            var popBg = document.getElementById('pop_iframe');
            popBg.style.opacity = 0.5;
            popBg.style.backgroundColor = 'black';
            var progress_print = document.getElementById("Progress_Print");
            progress_print.style.width = '0px';
        },
        updateCallback:function(percent,nwait,totwait,timeleft){
            // percnet: 진행률
            // nwait: 대기자수
            // totwait: 후 순위 대기자수
            // timeleft: 대기시간
            var progress_print = document.getElementById("Progress_Print");
            progress_print.style.width = 420 * (percent/100) + 'px';
        },
        htmlStr:'<div id="NetFunnel_Skin_Top" style="background-color:#ffffff; width:460px; height: 240px; border-radius: 10px;"> \
					<div style="position: absolute; top: 10px; left: 420px;">\
                        <img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AAALPAAAEkwAABmIAAAiJ/9sAhAACAgICAgICAgICAwICAgMEAwICAwQFBAQEBAQFBgUFBQUFBQYGBwcIBwcGCQkKCgkJDAwMDAwMDAwMDAwMDAwMAQMDAwUEBQkGBgkNCwkLDQ8ODg4ODw8MDAwMDA8PDAwMDAwMDwwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wgARCAAlAB8DAREAAhEBAxEB/8QAyAAAAwADAAAAAAAAAAAAAAAABQYHAQMEAQADAQEBAAAAAAAAAAAAAAAABQYHAwQQAAIDAAICAgMAAAAAAAAAAAIDAQQFEhMUBgAgESQVEQACAQMDAgIIBwEAAAAAAAABAgMREgQAEwUhIkEyUWGBkcFCUhTwcWJykiMzJBIAAgECAwcFAQAAAAAAAAAAAAERAhLwITEgQVGBIjIjYZGh4QMTEwEAAgICAgEDBQEBAAAAAAABESEAMUFRYXGB8JGxECDB0eHxof/aAAwDAQACEQMRAAABtqtAbmtAXaWAoii7iLXON/kpHLjRHDvLGOZ58dI8caA4ehZa58Zm9AXqTPqEouxFUj1B0B1B/9oACAEBAAEFAs6fZp0ysag5tlvszdOmWtOjRXjo0Rr1N6oVbNolSCh/QotyX6TGUMjP8nLuRTZn+dnj7MOp1aZZttfsoaVONSNIQ9k+WQ9hnRph7JHyuHsHnf/aAAgBAgABBQJvPjA2ZSElEV4f2sXBjVrwoXU4NqaUAxrOA1bHaD7nBqbvNjezj+1CVwf4R5Hb9P/aAAgBAwABBQKh2eQTK4HaFxvbKuGbwCzopO98pgxaHGfXmyJ2dB7M+aZm1LQKF0Ib3yNcjtdwvb1cPp//2gAIAQICBj8Cqt1Mnv45+5SqnmVS8bhqSJFXdhDquwypwTAqLcMdNuEO3U58ym7UqnTEbP8A/9oACAEDAgY/AvI+kmrJF35dnwKETS5e71NLbSxUz95C6SEofHgK1zPE/pdH0LqPJ2meh4uz45ijZ//aAAgBAQEGPwLlo3zb1iD7gMgZR9Ngr2633y7IBJ/oXo/8q64842UTF2qLWAjJHnvFe7WVG011oa4E1QE+Sg1zErcwyxQpKVawi8V6/u6+HjqGPH5DYP3AULIDTccUVbB6QPNrGxZOZVJMNTHNfdWsKiRrPY2soR8nuGOORpQoa9VZa1Y09euYjk4Y2SpKFjuJt69a/T8NIsXGb+5OC5dmADr1VhItCD6tY8svChlzY7sgSEljuAIbfT5B11lW8ZtM8cis6Fr2ULS1x+Q1yrS4o/tV7nZFA/TYademjCcWsG51BTv91Ncc2Pi/0dpVFUbdT57/AKfxTWUzwC5w1WK9tflo2p/tsnMWG9rBkQY5elTS2so8KUr7dTfaZOckfbapgiZKW+lpQvuGv+rJyGGwuwGghWj3pW+yUmtPjqsuRlHGsyO1oYRHX5OolL9PDp7tf//aAAgBAQMBPyESU6aVnmEeCObyjxQEk1ErE8b+MjRArAzoq7SurwR+vgDKSQmP5yUdUEvl0/IwHASi3AkZS8OMhFQUkgUcEhPqXNPcjaKuKKPLrTjjZgjSBMbNyeGQ8JBwqLCKXjnvEyPyLGyeEOzHDjdXgDSkXIwcP3wKgkaUYj/2ObxAUCkq5LaDzv4yBSB9FIgjbaFG7YGXDgRDUNTHvnEqoDD26say+jJiXIYFYJ/01vCmf88BG9bJBUi6wg5a+ET3hF5Nzj//2gAIAQIDAT8hLmaV9cYGtemj/XE4aKve3n3nB++RvocZV9J9e83lV+Pt/OVwRFfdTl4kj79n8YN6g1k0ow/HxmwAx79M1Mifft7z0ZWEQBnwKevfzikJW/fP0YZB5dRPR/b/AP/aAAgBAwMBPyG9YGyZ9QGvoyskHJBPp8eMtzengcz39RnMyuI93zhkmamreefH3xfeC5+JGKkkk2TCYbcI38pPyPrEiwJLncVX2vrGs+a2p+P7rGJY728X2oHuPGFSqyHVn+WPeNbIquCbr73gGmE3/ER9R5yMxYc2T/zKSdq/L6fF5t1138z+3//aAAwDAQACEQMRAAAQRAueksM2Jt//2gAIAQEDAT8QBpnLxo1xRQ0MKlgiY+pIEe+3gxx+RBVASUCVpgcikYDeQaFqIYktj8LyHaFFrEAdZ0SEyWlUAJRW6zEagE11rBPcri4BioCx+lv7NVJgHlmVm0h5Xl0qyBkxzvkw4dGKK19n7RSmZIQpY4faVf8A3REk8drhnDsySLiiAVNso/y5YGRkl4w1jYMVFSlxKKTcc4GAPNsqoF2O2PrTqIcPWCQSlGMpUyI0b6S0gWRFhx7KVsBkIoGgaEyiMnncoBnSQd4z/9oACAECAwE/EAoSaShD4WqTbRzikknQ0xo69j4rJoqJIUdPyNusQAJKxBcFUHNFUy440GJDVz/645Mc7p0kogD0PbGUhdBnTaqf+TknwDAIOTyRPs3xmyNNvMX0cr1hijhpZVMrdTCcYPNyVZUK8xB7jjJ/iLEXog+ldNZHDmawb8TSxMTU7yANJQFfhq9x9mQBQSgpduPOk6wpsRFQIcS07trd/t//2gAIAQMDAT8QivIqb8tPlRUonHQQJLS3oLcElBQn5jTp5K0yxRK0qd1BLiFxIYohsQRJhSwKljhhRJdGKjaYp3FYDMQpFCZYwCVCAcAJgvJH0AhDnAzO/wAjZiI0EFTUzDSkrqArEA6CiqhawAWSAryf9EBEUAtUr8EsRWPlkYgkkHqivLnUZJaScAVziX/S4aFH0mG+ZshzvzkjEYQNh4HX9Q7MESacoUx1NHiA8frX6f/Z"></img>\
                    </div>\
                    <div style="width: 100%; height: 146px; padding:10px; text-align:center;"> \
                        <img style="height:30px;color:black; margin-bottom: 10px; margin-top: 5px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAgCAYAAAASYli2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAa1JREFUeNrMljFIQkEcxu+l4VTQZEMugZMVLiKETi3Z1lBDU9JSszXlaFPttYRB0NLQli1NSiAuUjg52pBTQ5OJ2Cd8J/eeV967JDr4cdz//30f757vns8RN5vPQohlMZnxMiUmPIJgxUi5cye614tjZcZXmMvljHQycAt8gm2LXbq8MnAJTIOYRaDLKwP7ntnPcHm99zBkERjS3cMPzjZbjqkZMvCR84bP0Bg9wwwZWAf3IAAeQMogLEVtgN669x7ugSZYAGVwChxNkMNemdomvSMP9htIgiuuD0FeE5hnT1CbpFd7Ut5BFhxwfQzCSj/MmqAmS8/Yo3cBanwkMko9w1qNGl9n+YlzRKlFPD37l4Pp+ClwlXNLqbU8PePAfZAAHVBS6iXWEtSMDZwDRXDO9QloK/02a4KaIj3awHlQBbtcn4GC5iIK7Alqq/SOBF6CKHgFaXD0zeusz16a2ii9rsA4D3kPrIOKwQ9aobZHb1wNXOM8OOQNH09Jg55hhgycVQR+h/TM6P4COhaBnT85KYPL7v5iy0NvkMVbYjNc3olv2cH3yv/++voSYAD5yV3inlVrEgAAAABJRU5ErkJggg==">  \
                        <div style="text-align: center;"> \
							<!--\
                            <span style="color:rgb(251, 163, 34); font-size:24px;">서비스&nbsp;</span>\
							<span style="color:rgb(0, 166, 81); font-size:24px;">접속대기 중</span>\
							<span style="color:rgb(251, 163, 34); font-size:24px;">입니다.</span>\
							<br>\
                            <span style="color:rgb(118, 118, 118); font-size:16px;">예상대기시간&nbsp;</span> \
                            <span style="color:rgb(102, 102, 102); font-weight: bold; font-size:16px;" id="NetFunnel_Loading_Popup_TimeLeft" class="%02H시&nbsp%02M분&nbsp%02S초^&nbsp^false" ></span>\
							<br>\
							-->\
							<span style="color:rgb(0, 166, 81); font-size:24px;">Waiting</span>\
							<span style="color:rgb(251, 163, 34); font-size:24px;">for the service...&nbsp;</span>\
							<br>\
                            <span style="color:rgb(118, 118, 118); font-size:16px;">Remain&nbsp;</span> \
                            <span style="color:rgb(102, 102, 102); font-weight: bold; font-size:16px;" id="NetFunnel_Loading_Popup_TimeLeft" class="%02Hhour(s)&nbsp%02Mminute(s)&nbsp%2S&nbspsecond(s)^&nbsp^false"></span>\
							<br>\
							<!--\
                            <span style="color:rgb(251, 163, 34); font-size:24px;">Menunggu&nbsp;</span>\
							<span style="color:rgb(0, 166, 81); font-size:24px;">akses</span>\
							<span style="color:rgb(251, 163, 34); font-size:24px;">layanan.</span>\
							<br>\
							<span style="color:rgb(102, 102, 102); font-weight: bold; font-size:16px;" id="NetFunnel_Loading_Popup_TimeLeft" class="%02Hhour(s)&nbsp%02Mminute(s)&nbsp%02Ssecond(s)^&nbsp^false"></span>\
                            <span style="color:rgb(118, 118, 118); font-size:16px;">Waktu tunggu diperkiraken&nbsp;</span> \
							<br>\
							-->\
                        </div> \
                        <div style="width:420px; height: 17px; background-color:rgb(251, 163, 34); margin: 10px; border-radius: 10px;" > \
                            <div style="height: 17px; background-color:rgb(0, 166, 81); border-radius: 10px; " id="Progress_Print"></div>\
                        </div> \
                    </div> \
                    <div style="width: calc(100% - 20px); height: calc(100% - 146px); background-color: rgb(242, 242, 242); border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; padding: 10px;"> \
                        <div style="font-size: 11px; color: rgb(102, 102, 102);">\
                            <span>고객님 앞에&nbsp;&nbsp;</span>\
							<span id="NetFunnel_Loading_Popup_Count" style="color:rgb(237, 32, 36);"></span>\
                            <span>명,</span>\
                            <span>뒤에&nbsp;&nbsp;</span>\
							<span id="NetFunnel_Loading_Popup_NextCnt" style="color:rgb(237, 32, 36);"></span>\
                            <span>명의 대기자가 있습니다.</span>\
							<br>\
                            <span>현재 접속 사용자가 많아 대기중이며, 잠시만 기다리시면 서비스로 자동 접속 됩니다.</span>\
							<br>\
                        </div>\
                        <div style="font-size: 12px; color: rgb(102, 102, 102); margin-top: 20px;">\
                            <span>※ 재 접속하시면 대기시간이 더 길어집니다.&nbsp;</span>\
							<span onclick="NetFunnel.countdown_stop()" style="font-weight: bold; cursor:pointer;">&nbsp;[중지]</span>\
                        </div>\
                    </div>\
                </div>'
    },'normal');
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adm_userconfig_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("admin/adm_userconfig_model");
    }
	
	function getListAllLocationService() {	
		$srvReturn = null;
		try {
			$srvReturn = $this->adm_userconfig_model->getListAllLocation();
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function getListAllAdminUser() {	
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_userconfig_model->getListAllAdminUser();
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function getListAdminUserByID($id) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_userconfig_model->getListAdminUserByID($id);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function saveUserAdmin() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		try {
			if($this->form_validation->run('admUser') == FALSE) {
			   throw new Exception(requiredFieldMessage(), 1);
		    }
			$modelReturn = $this->adm_userconfig_model->saveUserAdmin();
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function getListAllClientUser() {	
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_userconfig_model->getListAllClientUser();
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function getListClientUserByID($id) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_userconfig_model->getListClientUserByID($id);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
		
}
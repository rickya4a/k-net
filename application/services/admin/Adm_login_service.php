<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adm_login_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("admin/adm_login_model");
    }
	
	function handleAdmLoginService() {
		$data = $this->input->post(NULL, TRUE);	
		$mdReturn = null;
		try {
			$checkAuth = $this->adm_login_model->checkAuthLoginAdm($data);
			$insDataLog = $this->adm_login_model->insertAdminDataLog($checkAuth);
			$mdReturn = $this->adm_login_model->fetchingMenuAdm($checkAuth[0]->id);
			$arr = array('usrid_digsign' => $checkAuth[0]->userid,
						 'usrnm_digsign' => $checkAuth[0]->username,
						 'menu_digsign' => $mdReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);			
		}
		return $arr;
	}
	
	
}
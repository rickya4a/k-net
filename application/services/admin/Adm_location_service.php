<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adm_location_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("admin/adm_location_model");
    }
	
	function getListAllLocationService() {	
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_location_model->getListAllLocation();
			$srvReturn = $this->jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function getListLocationByIDService($id) {
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_location_model->getListLocationByID($id);
			$srvReturn = $this->jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function saveLocationService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_location_model->saveLocation();
			$srvReturn = $this->jsonTrueResponse(null, "Save Location success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function updateLocationService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->adm_location_model->updateLocation();
			$srvReturn = $this->jsonTrueResponse(null, "Update Location success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	
}
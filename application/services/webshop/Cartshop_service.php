<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cartshop_service extends MY_Service {

	public function __construct() {
		parent::__construct();
	}

	function getPromo17Agustus() {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getPromo17Agustus();
		if($res != null) {
			$res = jsonTrueResponse($res);
		}
		return $res;
	}

	
	/*
	function getMemberInfo3($idmember){
		$this->load->model('nonmember_promo/M_nonmember_promo', 'm_nonmember_promo');
		$dt['sponsorname'] = $this->m_nonmember_promo->getSponsorName($idmember);
	}*/
	
	function getMemberInfo3($userid) {
		$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->show_idmember($userid);
		if($arr != null) {
			$returnArr = array("sukses" => "true", "arrayData" => $arr);
		} else {
			$returnArr = array("sukses" => "false", "arrayData" => null);
		}

		return $returnArr;
	}

	function addToCart() {
		$arr = array("response" => "false", "message" => "Data product ".$this->input->post('prdnm')." gagal dimasukkan ke dalam Cart");

		$prdcd=$this->input->post('prdcd');
		$prdnm=$this->input->post('prdnm');
		$desc=$this->input->post('desc');

		if((substr($prdcd,0,5) == 'HC050' || substr($prdcd,0,7) == 'HC075NA') AND $desc == 2 ) { //promo paket k-ion
			//$product= substr($prdnm,5);
			$product= $prdnm;
		}else{
			$product= $prdnm;
		}

		$addCart = array(array(
			'id' => $this->input->post('prdcd'),
			'name' => $this->input->post('prdnm'),
			'qty' => 1 ,
			'bv' => $this->input->post('bv'),
			'west_price' => $this->input->post('westPrice'),
			'east_price' => $this->input->post('eastPrice'),
			'west_Cprice' => $this->input->post('westCPrice'),
			'east_Cprice' => $this->input->post('eastCPrice'),
			'weight' => $this->input->post('weight'),
			'css_class' => '',
			'max_order' => $this->input->post('max_order')
		));
		//print_r($addCart);
		$insCart = $this->cart->insert($addCart);
		//print_r($insCart);
		if($insCart) {
			$arr = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"message" => "Terima Kasih, product ".$product." sudah dimasukkan ke dalam cart");
		}

		return $arr;
	}
	
	function addSCartProduct($arr) {
		$res = array("response" => "false", "message" => "Data product ".$arr['name']." gagal dimasukkan ke dalam Cart");
		$addCart = array(array(
			'id' => $arr['id'],
			'name' => $arr['name'],
			'qty' => $arr['qty'] ,
			'bv' => $arr['bv'],
			'west_price' => $arr['west_price'],
			'east_price' => $arr['east_price'],
			'weight' => $arr['weight'],

		));
		//print_r($addCart);
		$insCart = $this->cart->insert($addCart);
		if($insCart) {
			$res = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"message" => "Terima Kasih, product ".$arr['name']." sudah dimasukkan ke dalam cart");
		}

		return $res;
	}

	function addToCartXXX($data) {
		$arr = array("response" => "false", "message" => "Data product ".$data['prdnm']." gagal dimasukkan ke dalam Cart");
		$addCart = array(array(
			'id' => $data['prdcd'],
			'name' => $data['prdnm'],
			'qty' => 1 ,
			'bv' => $data['bv'],
			'west_price' => $data['westPrice'],
			'east_price' => $data['eastPrice'],
			'weight' => $data['weight'],

		));
		//print_r($data);
		$insCart = $this->cart->insert($addCart);
		//print_r($insCart);
		if($insCart) {
			$arr = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"message" => "Terima Kasih, product ".$data['prdnm']." sudah dimasukkan ke dalam cart");
		}

		return $arr;
	}

	function addProductToCart($prdcd) {
		$this->load->model('webshop/Product_model', 'm_product');
		$res = $this->m_product->getDetailPrdcdByID($prdcd);

		$addCart = array(array(
			'id' => $res[0]->prdcd,
			'name' =>  $res[0]->prdnm,
			'qty' => 1 ,
			'bv' =>  $res[0]->bv,
			'west_price' =>  $res[0]->price_cw,
			'east_price' =>  $res[0]->price_ce,
			'west_Cprice' =>  $res[0]->price_cw,
			'east_Cprice' =>  $res[0]->price_ce,
			'weight' =>  $res[0]->weight,
			'max_order' => $res[0]->max_order,

		));
		//print_r($addCart);
		$insCart = $this->cart->insert($addCart);
		//print_r($insCart);
		if($insCart) {
			$arr = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"total_west_Cprice" => $this->cart->total_west_Cprice(),
				"total_east_Cprice" => $this->cart->total_east_Cprice(),
				"message" => "Terima Kasih, product ".$this->input->post('prdnm')." sudah dimasukkan ke dalam cart");
		}

		return $arr;
	}

	function addProductToCartVera($prdcd) {
		$this->load->model('webshop/Product_model_baru', 'm_product');
		$res = $this->m_product->getDetailPrdcdByID($prdcd);

		$addCart = array(array(
			'id' => $res[0]->prdcd,
			'name' =>  $res[0]->prdnm,
			'qty' => 1 ,
			'bv' =>  $res[0]->bv,
			'west_price' =>  $res[0]->price_w,
			'east_price' =>  $res[0]->price_e,
			'west_Cprice' =>  $res[0]->price_cw,
			'east_Cprice' =>  $res[0]->price_ce,
			'weight' =>  $res[0]->weight,
			'max_order' => $res[0]->max_order,

		));
		//print_r($addCart);
		$insCart = $this->cart->insert($addCart);
		//print_r($insCart);
		if($insCart) {
			$arr = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"total_west_Cprice" => $this->cart->total_west_Cprice(),
				"total_east_Cprice" => $this->cart->total_east_Cprice(),
				"message" => "Terima Kasih, product ".$this->input->post('prdnm')." sudah dimasukkan ke dalam cart");
		}

		return $arr;
	}

	function IfDoubleInputCart($prdcd) {
		//$this->load->library('cart');
		$arr = false;
		foreach ($this->cart->contents() as $items) {
			if($items['id'] == $prdcd) {
				$arr = true;
			}
		}
		return $arr;
	}

	function IfDoubleInputCart2($prdcd) {
		//$this->load->library('cart');
		$arr = false;
		foreach ($this->cart->contents() as $items) {
			//echo $items['id']. " - ".$prdcd;
			if($items['id'] == $prdcd) {
				$arr = true;
			}
		}
		return $arr;
	}

	function updateCart($data) {

		$jum = count($data['rowid']);

		$success = 0;
		for($i = 0; $i < $jum; $i++) {
			$xxx = array(
				'rowid' => $data['rowid'][$i],
				'qty'   => $data['qty'][$i],

			);

			if($this->cart->update($xxx)) {
				$success++;
			}
		}

		if($success == $jum) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	function removeCart() {
		// Check rowid value.
		$arr = array("response" => "false");
		$rowid = $this->input->post('rowid');

		// Destroy selected rowid in session.
		$data = array(
			'rowid' => $rowid,
			'qty' => 0
		);
		$res = $this->cart->update($data);
		if($res) {
			$arr = array("response" => "true",  "message" => "Data berhasil di hapus");
		}
		return $arr;
		//redirect('product');
		// Update cart data, after cancel.
	}

	function checkShippingDataBaru($data) {
		if($data['delivery'] == null || $data['delivery'] == "") {
			return false;
		} else {
			if($data['delivery'] == "1") {
				if($data['provinsi1'] == null || $data['provinsi1'] == "") {
					return false;
				} elseif($data['kota1'] == null || $data['kota1'] == "") {
					return false;
				} elseif($data['kecamatan1'] == null || $data['kecamatan1'] == "") {
					return false;
				} elseif($data['stockist'] == null || $data['stockist'] == "") {
					return false;
				} else {
					return true;
				}

			} else {
				 //tambahan Vera
				 if($data['shipper'] == "1") {
					if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
					} elseif($data['notlp'] == null || $data['notlp'] == "") {
						return false;
					} elseif($data['provinsi'] == null || $data['provinsi'] == "") {
						return false;
					} elseif($data['kota'] == null || $data['kota'] == "") {
						return false;
					} elseif($data['kecamatan'] == null || $data['kecamatan'] == "") {
						return false;
					}
					elseif($data['alamat'] == null || $data['alamat'] == "") {
						return false;
					} elseif($data['stockistref'] == null || $data['stockistref'] == "") {
						return false;
					} else {
						return true;
					}
				}else if($data['shipper'] == "4") {
					if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
					} elseif($data['notlp'] == null || $data['notlp'] == "") {
						return false;
					}elseif($data['alamat'] == null || $data['alamat'] == "") {
						return false;
					} elseif($data['stockistref1'] == null || $data['stockistref1'] == "") {
						return false;
					} elseif($data['stockistref3'] == null || $data['stockistref3'] == "") {
						return false;
					}elseif($data['metode'] == null || $data['metode'] == "") {
						return false;
					} else {
						return true;
					}
				} //end shipper 4

				/* //ASLI DION
				 if($data['shipper'] == "1") {
					if($data['kecamatan'] == null || $data['kecamatan'] == "")
					{
						return false;
					}
				}

				if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
					return false;
				} elseif($data['notlp'] == null || $data['notlp'] == "") {
					return false;
				} elseif($data['provinsi'] == null || $data['provinsi'] == "") {
					return false;
				} elseif($data['kota'] == null || $data['kota'] == "") {
					return false;
				} //elseif($data['kecamatan'] == null || $data['kecamatan'] == "") {
					//return false;
				//}
				elseif($data['alamat'] == null || $data['alamat'] == "") {
					return false;
				} elseif($data['stockistref'] == null || $data['stockistref'] == "") {
					return false;
				} else {
					return true;
				}*/
			}
		}
	}
	
	function checkShippingDataJNE($data) {
		if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
		} elseif($data['notlp'] == null || $data['notlp'] == "") {
			return false;
		} elseif($data['provinsi'] == null || $data['provinsi'] == "") {
			return false;
		} elseif($data['kota'] == null || $data['kota'] == "") {
			return false;
		} elseif($data['kecamatan'] == null || $data['kecamatan'] == "") {
			return false;
		}
		elseif($data['alamat'] == null || $data['alamat'] == "") {
			return false;
		} elseif($data['stockistref'] == null || $data['stockistref'] == "") {
			return false;
		} else {
			return true;
		}
	}

	function checkShippingData($data) {
		if($data['delivery'] == null || $data['delivery'] == "") {
			return false;
		} else {
			if($data['delivery'] == "1") {
				if($data['provinsi1'] == null || $data['provinsi1'] == "") {
					return false;
				} elseif($data['kota1'] == null || $data['kota1'] == "") {
					return false;
				} elseif($data['kecamatan1'] == null || $data['kecamatan1'] == "") {
					return false;
				} elseif($data['stockist'] == null || $data['stockist'] == "") {
					return false;
				} else {
					return true;
				}

			} else {
				/*if($data['shipper'] == "1") {
					if($data['kecamatan'] == null || $data['kecamatan'] == "")
					{
						return false;
					}
				}

				if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
					return false;
				} elseif($data['notlp'] == null || $data['notlp'] == "") {
					return false;
				} elseif($data['provinsi'] == null || $data['provinsi'] == "") {
					return false;
				} elseif($data['kota'] == null || $data['kota'] == "") {
					return false;
				} elseif($data['kecamatan'] == null || $data['kecamatan'] == "") {
					return false;
				} elseif($data['alamat'] == null || $data['alamat'] == "") {
					return false;
				} elseif($data['stockistref'] == null || $data['stockistref'] == "") {
					return false;
				} else {
					return true;
				}*/
				//tambahan Vera
				if($data['shipper'] == "1") {
					if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
					} elseif($data['notlp'] == null || $data['notlp'] == "") {
						return false;
					} elseif($data['provinsi'] == null || $data['provinsi'] == "") {
						return false;
					} elseif($data['kota'] == null || $data['kota'] == "") {
						return false;
					} elseif($data['kecamatan'] == null || $data['kecamatan'] == "") {
						return false;
					}
					elseif($data['alamat'] == null || $data['alamat'] == "") {
						return false;
					} elseif($data['stockistref'] == null || $data['stockistref'] == "") {
						return false;
					} else {
						return true;
					}
				}else if($data['shipper'] == "4") {
					if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
					} elseif($data['notlp'] == null || $data['notlp'] == "") {
						return false;
					}elseif($data['alamat'] == null || $data['alamat'] == "") {
						return false;
					} elseif($data['stockistref1'] == null || $data['stockistref1'] == "") {
						return false;
					} elseif($data['stockistref3'] == null || $data['stockistref3'] == "") {
						return false;
					}elseif($data['metode'] == null || $data['metode'] == "") {
						return false;
					} else {
						return true;
					}
				} //end shipper 4
			}
		}
	}

	function getProductShop($listprd, $pricecode) {
		$request = array();
		$valReturn = array();
		//$this->xmlrpc->set_debug(TRUE);
		$this->xmlrpc->method('get.PriceByProd');
		$request = array(array(array(
			"prdcd" => $listprd,
			"pricecode" => $pricecode,
			"idmember" => $this->session->userdata('store_userid'),
			"signature" => array(md5($this->session->userdata('store_userid') . '12345'), 'string')
		), 'struct'
		));
		$this->xmlrpc->request($request);

		if (!$this->xmlrpc->send_request()) {
			echo $this->xmlrpc->display_error();
		} else {
			$arr = $this->xmlrpc->display_response();
			return $valReturn = json_decode($arr['arrayData']);
		}
	}

	function getShippingData($personal_info) {
		$request = array();
		$valReturn = array();


		//$this->xmlrpc->set_debug(TRUE);
		$this->xmlrpc->method('get.getShippingData');
		$request = array(array(array(
			"provinsi" => $personal_info['provinsi'],
			"kota" => $personal_info['kota'],
			"kecamatan" => $personal_info['kecamatan'],
			"idmember" => getUserID(),
			"signature" => array(md5(getUserID() . '12345'), 'string')
		), 'struct'
		));
		//print_r($request);
		$this->xmlrpc->request($request);

		if (!$this->xmlrpc->send_request()) {
			echo $this->xmlrpc->display_error();
		} else {
			$arr = $this->xmlrpc->display_response();
			$setArr = array(
				//"provinsi" => json_decode($arr['provinsi']),
				"kota" => json_decode($arr['kota']),
				"kecamatan" => json_decode($arr['kecamatan']),
				//"kelurahan" => json_decode($arr['kelurahan'])
				"listStk" => json_decode($arr['listStk'])
			);
			//print_r($setArr);
			return $setArr;
		}

	}

	function getShippingData2($personal_info) {

		$listKota = $this->showListKota($personal_info['provinsi']);
		$listKec = $this->showListKecamatan($personal_info['kota']);
		$listStk = $this->showListKelurahan($personal_info['kecamatan']);

		$setArr = array(
			"kota" =>$listKota['arrayData'],
			"kecamatan" => $listKec['arrayData'],
			"listStk" => $listStk['listStk']
		);
		return $setArr;
	}

	function getShippingDataKgb($personal_info) {
		$listKota = $this->showListKotaKgb($personal_info['provinsi']);
		//$listKec = $this->showListKecamatan($personal_info['kota']);
		//$listStk = $this->showListKelurahan($personal_info['kecamatan']);
		$this->load->model('webshop/shared_module', 'shared');
		$listStk = $this->shared->listCargoStockist($personal_info['kota']);
		$setArr = array(
			"kota" =>$listKota['arrayData'],
			"kecamatan" => null,
			"listStk" => $listStk
		);
		return $setArr;
	}

	function setAutomaticPersonalInfo($data) {
		$this->session->unset_userdata('personal_info');
		$this->session->unset_userdata('sender_address');
		$this->session->unset_userdata('destination_address');
		$this->session->unset_userdata('jne_branch');
		$arr = array(
			"delivery" => "2",
			"nama_penerima" => $data[0]->nama_penerima,
			//"lastname" => $data['lastname'],
			"email" => $data[0]->email,
			"notlp" => $data[0]->notlp,
			"stkarea" => NULL,
			"nama_stkarea" => NULL,
			"stockist" => $data[0]->stockist,
			"nama_stockist" => $data[0]->nama_stockist,
			"provinsi" => $data[0]->provinsi,
			"nama_provinsi" => $data[0]->nama_provinsi,
			"kota" => $data[0]->kota,
			"nama_kota" => $data[0]->nama_kota,
			"kecamatan" =>$data[0]->kecamatan,
			"nama_kecamatan" =>$data[0]->nama_kecamatan,
			//"kelurahan" => $data['kelurahan'],
			//"nama_kelurahan" => $data['nama_kelurahan'],
			//"kodepos" => $data['kodepos'],
			"alamat" => $data[0]->alamat,
			"idmemberx" => "",
			"membername" => ""
		);

		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('sender_address', $data[0]->sender_address);
		$this->session->set_userdata('destination_address', $data[0]->destination_address);
		$this->session->set_userdata('jne_branch', $data[0]->jne_branch);
	}

	function registerCustomerNonMember($data) {

		$login = array(
			"userlogin" =>  $data['nama_penerima'],
			"usertelp" =>  $data['notlp'],
			"useremail" =>  $data['email'],
		);
		$this->session->set_userdata('non_member_info', $login);
	}

	function sessionRegPersonalInfo($data) {
		$this->session->unset_userdata('personal_info');
		if($data['delivery'] == "1") {
			$arr = array(
				"delivery" => $data['delivery'],
				/*"stkarea" => $data['stkarea'],
                "nama_stkarea" => $data['nama_stkarea'],
                "stockist" => $data['stockist'],
                "nama_stockist" => $data['nama_stockist'], */
				"provinsi" => $data['provinsi1'],
				"nama_provinsi" => $data['nama_provinsi1'],
				"kota" => $data['kota1'],
				"nama_kota" => $data['nama_kota1'],
				"kecamatan" => $data['kecamatan1'],
				"nama_kecamatan" => $data['nama_kecamatan1'],
				"stockist" => $data['stockist'],
				"nama_stockist" => $data['nama_stockist'],
				"idmemberx" => $data['idmemberx'],
				"membername" => $data['membername'],
				"bnsperiod" => $data['bnsperiod'],
				"id_lp" => $data['id_lp']
			);
		} else {
			$arr = array(
				"delivery" => $data['delivery'],
				"nama_penerima" => $data['nama_penerima'],
				//"lastname" => $data['lastname'],
				"email" => $data['email'],
				"notlp" => $data['notlp'],
				"stkarea" => NULL,
				"nama_stkarea" => NULL,
				"stockist" => $data['stockistref'],
				"nama_stockist" => $data['nama_stockistr1ref'],
				"provinsi" => $data['provinsi'],
				"nama_provinsi" => $data['nama_provinsi'],
				"kota" => $data['kota'],
				"nama_kota" => $data['nama_kota'],
				"kecamatan" => $data['kecamatan'],
				"nama_kecamatan" => $data['nama_kecamatan'],
				//"kelurahan" => $data['kelurahan'],
				//"nama_kelurahan" => $data['nama_kelurahan'],
				//"kodepos" => $data['kodepos'],
				"alamat" => $data['alamat'],
				"idmemberx" => $data['idmemberx'],
				"membername" => $data['membername'],

				"bnsperiod" => $data['bnsperiod'],
				"id_lp" => $data['id_lp']
			);
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "product_shopping");
	}

	function sessionRegPersonalInfoBaru($data) {
		$this->session->unset_userdata('personal_info');
		$idmember = str_replace(' ', '', $data['idmemberx']);
		
		if($data['delivery'] == "1") {
			$arr = array(
				"delivery" => $data['delivery'],
				/*"stkarea" => $data['stkarea'],
                "nama_stkarea" => $data['nama_stkarea'],
                "stockist" => $data['stockist'],
                "nama_stockist" => $data['nama_stockist'], */
				"provinsi" => $data['provinsi1'],
				"nama_provinsi" => $data['nama_provinsi1'],
				"kota" => $data['kota1'],
				"nama_kota" => $data['nama_kota1'],
				"kecamatan" => $data['kecamatan1'],
				"nama_kecamatan" => $data['nama_kecamatan1'],
				"stockist" => $data['stockist'],
				"nama_stockist" => $data['nama_stockist'],
				//"idmemberx" => $data['idmemberx'],
				"idmemberx" => $idmember,
				"membername" => $data['membername'],
				"bnsperiod" => $data['bnsperiod'],
				"id_lp" => $data['id_lp'],
				"whcd" => "", //tambahan vera
				"whnm" => ""
			);
		} else {
			if($data['shipper'] == "1") {
				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $data['stockistref'],
					"nama_stockist" => $data['nama_stockistr1ref'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['provinsi'],
					"nama_provinsi" => $data['nama_provinsi'],
					"kota" => $data['kota'],
					"nama_kota" => $data['nama_kota'],
					"kecamatan" => $data['kecamatan'],
					"nama_kecamatan" => $data['nama_kecamatan'],
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					//"idmemberx" => $data['idmemberx'],
					"idmemberx" => $idmember,
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp'],
					"whcd" => "",
					"whnm" => ""
				);

			} else if($data['shipper'] == "4") { //tambahan Vera
				$metode= $data['metode'];
				$cmetode=substr($metode,0,1);
				if($cmetode == 1){
					$cmetode="Instant";
				}else{
					$cmetode="SameDay";
				}
				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => $data['no_order'],
					"nama_stkarea" => $cmetode,
					"stockist" => $data['stockistref1'],
					"nama_stockist" => $data['stockistref3'], //contoh:BID06 - PT. K-LINK NUSANTARA - JK
					"shipper" => $data['shipper'],
					"provinsi" => NULL,
					/*"nama_provinsi" => NULL,
					"kota" => NULL,
					"nama_kota" => NULL,*/
					
					"nama_provinsi" => $data['long_dest'],
					"kota" => NULL,
					"nama_kota" => $data['lat_dest'],
					
					"kecamatan" => '',
					"nama_kecamatan" => NULL,
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					//"idmemberx" => $data['idmemberx'],
					"idmemberx" => $idmember,
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp'],
					"whcd" =>  $data['whref1'],
					"whnm" =>  $data['whref2']

				);
			}elseif($data['shipper'] == "6") {
				$liststokist= $data['liststockist'];
				$stokist= explode("|", $liststokist);

				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $stokist[0],
					"nama_stockist" => $data['nama_stockist2'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['listprovinsi2'],
					"nama_provinsi" => $data['nama_provinsi3'],
					"kota" => $data['listkota2'],
					"nama_kota" => $data['nama_kota3'],
					"kecamatan" => $data['listkecamatan2'],
					"nama_kecamatan" => $data['nama_kecamatan3'],
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					//"idmemberx" => $data['idmemberx'],
					"idmemberx" => $idmember,
					"membername" => $data['membername'],

					"bnsperiod" => '0000-00-00', //bonus period COD kosong
					"id_lp" => $data['id_lp'],
					"whcd" => "",
					"whnm" => ""
				);

			}else {
				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $data['stockistref'],
					"nama_stockist" => $data['nama_stockistr1ref'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['provinsi'],
					"nama_provinsi" => $data['nama_provinsi'],
					"kota" => $data['kota'],
					"nama_kota" => $data['nama_kota'],
					"kecamatan" => "",
					"nama_kecamatan" => "",
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					//"idmemberx" => $data['idmemberx'],
					"idmemberx" => $idmember,
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp'],
					"whcd" => "",
					"whnm" => ""
				);
			}
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "product_shopping");
	}



	function sessionRegPersonalInfoBaruDigital($data) {
		$this->session->unset_userdata('personal_info');
		$membername="KIV CODE";
		if($data['memberx']!= '0000999'){
			$membername=$data['nama_penerima'];
		}
		{
			{
				$arr = array(
//					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
//					"stkarea" => NULL,
//					"nama_stkarea" => NULL,
//					"stockist" => $data['stockistref'],
//					"nama_stockist" => $data['nama_stockistr1ref'],
//					"shipper" => $data['shipper'],
//					"provinsi" => $data['provinsi'],
//					"nama_provinsi" => $data['nama_provinsi'],
//					"kota" => $data['kota'],
//					"nama_kota" => $data['nama_kota'],
//					"kecamatan" => $data['kecamatan'],
//					"nama_kecamatan" => $data['nama_kecamatan'],
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					"idmemberx" => $data['memberx'],
					"membername" => $membername,

					"bnsperiod" => $data['bnsperiod']
//					"id_lp" => $data['id_lp']
				);
			}
//			$this->session->set_userdata('sender_address', $data['sender_address']);
//			$this->session->set_userdata('destination_address', $data['destination_address']);
//			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
//		$this->session->set_userdata('trxtype', "product_shopping");
	}

	function sessionRegPersonalInfoPromoMember($data) {
		$this->session->unset_userdata('personal_info');
		if($data['delivery'] == "1") {
			$arr = array(
				"delivery" => $data['delivery'],
				/*"stkarea" => $data['stkarea'],
                "nama_stkarea" => $data['nama_stkarea'],
                "stockist" => $data['stockist'],
                "nama_stockist" => $data['nama_stockist'], */
				"provinsi" => $data['provinsi1'],
				"nama_provinsi" => $data['nama_provinsi1'],
				"kota" => $data['kota1'],
				"nama_kota" => $data['nama_kota1'],
				"kecamatan" => $data['kecamatan1'],
				"nama_kecamatan" => $data['nama_kecamatan1'],
				"stockist" => $data['stockist'],
				"nama_stockist" => $data['nama_stockist'],
				"idmemberx" => $data['idmemberx'],
				"membername" => $data['membername'],
				"bnsperiod" => $data['bnsperiod'],
				"id_lp" => $data['id_lp'],
				"state" => $data['state']
			);
		} else {
			if($data['shipper'] == "1") {
				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $data['stockistref'],
					"nama_stockist" => $data['nama_stockistr1ref'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['provinsi'],
					"nama_provinsi" => $data['nama_provinsi'],
					"kota" => $data['kota'],
					"nama_kota" => $data['nama_kota'],
					"kecamatan" => $data['kecamatan'],
					"nama_kecamatan" => $data['nama_kecamatan'],
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					"idmemberx" => $data['idmemberx'],
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp'],
					"state" => $data['state']
				);
			} else {
				/*$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $data['stockistref'],
					"nama_stockist" => $data['nama_stockistr1ref'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['provinsi'],
					"nama_provinsi" => $data['nama_provinsi'],
					"kota" => $data['kota'],
					"nama_kota" => $data['nama_kota'],
					"kecamatan" => "",
					"nama_kecamatan" => "",
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					"idmemberx" => $data['idmemberx'],
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp'],
					"state" => $data['state']
				);*/
				if($data['shipper'] == "1") {
					$arr = array(
						"delivery" => $data['delivery'],
						"nama_penerima" => $data['nama_penerima'],
						//"lastname" => $data['lastname'],
						"email" => $data['email'],
						"notlp" => $data['notlp'],
						"stkarea" => NULL,
						"nama_stkarea" => NULL,
						"stockist" => $data['stockistref'],
						"nama_stockist" => $data['nama_stockistr1ref'],
						"shipper" => $data['shipper'],
						"provinsi" => $data['provinsi'],
						"nama_provinsi" => $data['nama_provinsi'],
						"kota" => $data['kota'],
						"nama_kota" => $data['nama_kota'],
						"kecamatan" => $data['kecamatan'],
						"nama_kecamatan" => $data['nama_kecamatan'],
						//"kelurahan" => $data['kelurahan'],
						//"nama_kelurahan" => $data['nama_kelurahan'],
						//"kodepos" => $data['kodepos'],
						"alamat" => $data['alamat'],
						"idmemberx" => $data['idmemberx'],
						"membername" => $data['membername'],

						"bnsperiod" => $data['bnsperiod'],
						"id_lp" => $data['id_lp'],
						"state" => $data['state']
					);
				} else if($data['shipper'] == "4") { //tambahan Vera
					$metode= $data['metode'];
					$cmetode=substr($metode,0,1);
					if($cmetode == 1){
						$cmetode="Instant";
					}

					if($cmetode == 2){
						$cmetode="SameDay";
					}

					$arr = array(
						"delivery" => $data['delivery'],
						"nama_penerima" => $data['nama_penerima'],
						//"lastname" => $data['lastname'],
						"email" => $data['email'],
						"notlp" => $data['notlp'],
						"stkarea" => $data['price_harga'],
						"nama_stkarea" => $cmetode,
						"stockist" => $data['stockistref1'],
						"nama_stockist" => $data['stockistref3'],
						"shipper" => $data['shipper'],
						"provinsi" => NULL,
						"nama_provinsi" => $data['long_dest'],
						"kota" => NULL,
						"nama_kota" => $data['lat_dest'],
						"kecamatan" => 411,
						"nama_kecamatan" => NULL,
						//"kelurahan" => $data['kelurahan'],
						//"nama_kelurahan" => $data['nama_kelurahan'],
						//"kodepos" => $data['kodepos'],
						"alamat" => $data['alamat'],
						"idmemberx" => $data['idmemberx'],
						"membername" => $data['membername'],

						"bnsperiod" => $data['bnsperiod'],
						"id_lp" => $data['id_lp'],
						"state" => $data['state']

					);
				}else if($data['shipper'] == "6") { //tambahan Vera

					$arr = array(
						"delivery" => $data['delivery'],
						"nama_penerima" => $data['nama_penerima'],
						//"lastname" => $data['lastname'],
						"email" => $data['email'],
						"notlp" => $data['notlp'],
						"stkarea" => $data['price_harga'],
						"nama_stkarea" => '',
						"stockist" => $data['stockistref3'],
						"nama_stockist" => $data['nama_stockist2'],
						"shipper" => $data['shipper'],
						"provinsi" => NULL,
						"nama_provinsi" => $data['nama_provinsi3'],
						"kota" => NULL,
						"nama_kota" => $data['nama_kota3'],
						"kecamatan" => 411,
						"nama_kecamatan" => $data['nama_kecamatan3'],
						//"kelurahan" => $data['kelurahan'],
						//"nama_kelurahan" => $data['nama_kelurahan'],
						//"kodepos" => $data['kodepos'],
						"alamat" => $data['alamat'],
						"idmemberx" => $data['idmemberx'],
						"membername" => $data['membername'],
						"bnsperiod" => $data['bnsperiod'],
						"id_lp" => $data['id_lp'],
						"state" => $data['state']
					);
				}else {
					$arr = array(
						"delivery" => $data['delivery'],
						"nama_penerima" => $data['nama_penerima'],
						//"lastname" => $data['lastname'],
						"email" => $data['email'],
						"notlp" => $data['notlp'],
						"stkarea" => NULL,
						"nama_stkarea" => NULL,
						"stockist" => $data['stockistref'],
						"nama_stockist" => $data['nama_stockistr1ref'],
						"shipper" => $data['shipper'],
						"provinsi" => $data['provinsi'],
						"nama_provinsi" => $data['nama_provinsi'],
						"kota" => $data['kota'],
						"nama_kota" => $data['nama_kota'],
						"kecamatan" => "",
						"nama_kecamatan" => "",
						//"kelurahan" => $data['kelurahan'],
						//"nama_kelurahan" => $data['nama_kelurahan'],
						//"kodepos" => $data['kodepos'],
						"alamat" => $data['alamat'],
						"idmemberx" => $data['idmemberx'],
						"membername" => $data['membername'],

						"bnsperiod" => $data['bnsperiod'],
						"id_lp" => $data['id_lp'],
						"state" => $data['state']
					);
				}
			}
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "product_shopping");
	}

	function setShippingInfo() {
		$personal_info = $this->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
			);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
			return 1;
		} else {
			$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
			$arrx = array();
			if(array_key_exists("status", $dtx)) {
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->set_userdata('shipping_jne_info', $arrx);
				return null;
			} else {
				//if(getUserID() == "IDSPAAA66834") {
				$arrx = $this->setLowestJnePrice($dtx);
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->set_userdata('shipping_jne_info', $arrx);
				return $arrx;

			}
		}


	}

	function setShippingInfoNonMember($berat) {
		$personal_info = $this->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
				"ship_discount" => 0,
				"lat_dest" => "",
				"long_dest" => ""
			);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
		} else {
			/*if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($berat));
//				echo "isi dtc = ";
//				print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}
			} else {
				$dtx = json_decode($this->showListPriceKgb($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}*/
			//tambahan Vera
			if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
//				echo "isi dtc = ";
//				print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}

			}else if($personal_info['shipper'] == "2") {
				$dtx = json_decode($this->showListPriceKgb($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}else if($personal_info['shipper'] == "4") {
				//$dtx = json_decode($this->showListPriceGosend($this->cart->total_weight()));
				//echo "tes11111";
				$metode=$this->input->post("metode");
				$cmetode=substr($metode,0,1);
				$disc=0;
				if($cmetode == 1){
					$cmetode="Instant";
					//$disc= 1000;
				}

				if($cmetode == 2){
					$cmetode="SameDay";
					//$disc= 5000;
				}

				if( $personal_info['idmemberx'] == "IDSPAAA66834" AND $cmetode == 1){
					$disc= 10000;
				}else{
					$disc=0;
				}

				$harga= $this->input->post("price_harga");

				//print_r("metode".$cmetode);

				$nmstk = explode(" - ", $personal_info['nama_stockist']);
				$arrx = array(
					"origin_name" => $nmstk,
					"destination_name" => $personal_info['alamat'],
					"service_code" => "",
					"service_display" => $personal_info['nama_stkarea'],
					"price" => $personal_info['stkarea'],
					"etd_from" => "",
					"etd_thru" => "",
					"times" => "",
					"ship_discount" => $disc,
					"lat_dest" => $personal_info['nama_kota'],
					"long_dest" => $personal_info['nama_provinsi']
				);
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->set_userdata('shipping_jne_info', $arrx);
				return $arrx;

			}
		}
	}

	function setShippingInfoBaru() {
		$personal_info = $this->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
				"ship_discount" => 0,
				"lat_dest" => "", //tambahan Vera
				"long_dest" => ""
			);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
		} else {
				 //tambahan Vera
				if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
//				echo "isi dtc = ";
//				print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}

			}else if($personal_info['shipper'] == "2") {
				$dtx = json_decode($this->showListPriceKgb($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}else if($personal_info['shipper'] == "4") {
				//$dtx = json_decode($this->showListPriceGosend($this->cart->total_weight()));
				//echo "tes11111";
				$metode=$this->input->post("metode");
				$cmetode=substr($metode,0,1);

				$tgl_promo="2018-12-09";
				$today= date("Y-m-d");

				if($cmetode == 1){
					$cmetode="Instant";
				}else{
					$cmetode="SameDay";
				}

				/*$arrx1 = array(
				"full_discount" => $this->input->post("discount")
				);
				$this->session->unset_userdata('promo');
				$this->session->set_userdata('promo', $arrx1);*/

				if($today > $tgl_promo){  //tambahan vera promo diskon gosend
					$arrx1 = array(
						"full_discount" => $this->input->post("discount")
					);
					$this->session->unset_userdata('promo');
					$this->session->set_userdata('promo', $arrx1);
					//return $arrx1;
				}

				$harga=substr($metode,2);
				$charga=str_replace(".","",$harga);
				$charga= $charga * 1;

				$nmstk = explode(" - ", $personal_info['nama_stockist']);
				$arrx = array(
					"origin_name" => $nmstk,
					"destination_name" => $this->input->post("alamat"),
					"service_code" => $this->input->post("no_order"),
					"service_display" => $cmetode,
					"price" => $charga,
					"etd_from" => "",
					"etd_thru" => "",
					"times" => "",
					"ship_discount" => $this->input->post("discount"),
					"lat_dest" => $this->input->post("lat_dest"),
					"long_dest" => $this->input->post("long_dest")
				);
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->set_userdata('shipping_jne_info', $arrx);
				return $arrx;

			}else if($personal_info['shipper'] == "6") {

					$nmstk = explode(" - ", $personal_info['nama_stockist']);
					$arrx = array(
						"origin_name" => $nmstk,
						"destination_name" => $personal_info['alamat'],
						"service_code" => $this->input->post("awb_cod"),
						"service_display" => "COD",
						"price" => $this->input->post("ongkir_cod"),
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" =>'',
						"lat_dest" => '',
						"long_dest" => ''
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}

			/*if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
//				echo "isi dtc = ";
//				print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}
			} else {
				$dtx = json_decode($this->showListPriceKgb($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}*/
		}
	}

	function setShippingInfoBaruNew() {
		$personal_info = $this->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
				"ship_discount" => 0
			);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
		} else {
			if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
//				echo "isi dtc = ";
				//print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$arrx['ship_discount']= 0;
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}
			} else {
				$dtx = json_decode($this->showListPriceKgb($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}
		}
	}

	function setShippingInfoBaru2() {
		$personal_info = $this->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
				"ship_discount" => 0
			);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
		} else {
			if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}
			} else {
				$dtx = json_decode($this->showListPriceKgb2($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					//print_r($arrx);
					return null;
				}
			}
		}
	}

	public function showPriceResult2() {
		$data = $this->input->post(NULL, TRUE);
		$xx = $this->session->userdata('shipping_jne_info');
		$url = "http://api.jne.co.id:8889/tracing/klink/price/";

		$curl_post_data = array(
			"username" => $this->jne_username,
			//"api_key" => "76270305bef5d402220c96d59ac61977",
			"api_key" => $this->jne_api_key,
			//"from" => "CGK10000",
			//"thru" => "BDO10000",
			"from" => $this->session->userdata("sender_address"),
			"thru" => $this->session->userdata("destination_address"),
			/*"from" => $xx['origin_name'],
            "thru" => $xx['destination_name'],*/
			"weight" => $this->cart->total_weight()
		);
		//$curl = curl_init($url);
		$curl = curl_init($this->generate_url_ship_price);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}

	function insertEcommerceTrxSGO($trans_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->insert_temp_trx_sgo($trans_id);
		return $res;
	}

	function insertEcommerceTrxSGOforLP($trans_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->insert_temp_trx_sgo_forLP($trans_id);
		return $res;
	}

	/*function getListCargo() {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getListCargo();
		return $res;
	}*/

	function deleteTempTrxSGO($trans_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->delete_temp_trx_sgo($trans_id);
		return $res;
	}

	function getVoucherValidCheck($vch) {
		$arr = jsonFalseResponse();
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getVoucherValidCheck($vch, getUserID());
		if($res != null) {
			$arr = jsonTrueResponse($res);
		}
		return $arr;
	}

	function getListValidVoucher() {
		$arr = jsonFalseResponse();
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getListValidVoucher(getUserID());
		if($res != null) {
			$arr = jsonTrueResponse($res);
		}
		return $arr;
	}

	function getListValidVoucherPemenang($voucherno) {
		$arr = jsonFalseResponse();
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getListValidVoucherPemenang($voucherno);
		if($res != null) {
			$arr = jsonTrueResponse($res);
		}
		return $arr;
	}

	/*function updateBankCodePayment($temp_id, $arr) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
    	$res = $this->ecomm_trans_model->updateBankCodePayment($temp_id, $arr);
		$res2 = $this->ecomm_trans_model->getDataPaymentSGOByOrderID($temp_id);
		return $res2;
	}
	
	//function updatePayShip($temp_id, $price']);
	
	function getBank() {
        $this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getBank();
		return $res;
      
    }*/

	function getStatusCnoteByID($id) {
		$res = jsonFalseResponse("Conote tidak ada..");
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getStatusCnoteByID($id);
		if($res != null) {
			$res = jsonTrueResponse($res);
		}
		return $res;
	}

	function destroyCartData() {
		$this->session->unset_userdata('personal_info');
		$this->session->unset_userdata('pricecode');
		$this->session->unset_userdata('cart_contents');
		$this->session->unset_userdata('shipping_jne_info');
	}

	function addToCartVera() {
		//$arr = array("response" => "false", "message" => "Data product ".$this->input->post('prdnm')." gagal dimasukkan ke dalam Cart");
		//$product= substr($prdnm,8);
		
		$prdnm=$this->input->post('prdnm');
		//$prdcd="bundle".$this->input->post('prdcd');

		$product= $prdnm;
		$addCart = array(array(
			'id' => $this->input->post('prdcd'),
			'name' => $this->input->post('prdnm'),
			'qty' => 1 ,
			'bv' => $this->input->post('bv'),
			'west_price' => $this->input->post('westPrice'),
			'east_price' => $this->input->post('eastPrice'),
			'west_Cprice' => $this->input->post('westCPrice'),
			'east_Cprice' => $this->input->post('eastCPrice'),
			'weight' => $this->input->post('weight'),
			'css_class' => $this->input->post('css_class'),
			'max_order' => $this->input->post('max_order'),
		));
		//print_r($addCart);
		$insCart = $this->cart->insert($addCart);
		//print_r($insCart);
		if($insCart) {
			$arr = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"message" => "Terima Kasih, product ".$product." sudah terupdate dimasukkan ke dalam cart");
		}

		return $arr;
	}
}
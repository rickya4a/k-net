<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_service extends MY_Service {	
	
	public function __construct() {
	    parent::__construct();
		$this->load->model('webshop/Login_model', 'login_model');
	}
	
	function checkAuthLogin($data) {
       $res = $this->login_model->captchaCheck($data['captcha']);	
       if($res['response']) {	
	        unlink($_SERVER['DOCUMENT_ROOT'] . '/captcha/'.$res['word'].'.jpg');	 
	        
			$form = $this->input->post(NULL, TRUE);
			$res = $this->login_model->checkAuthLoginKnet($form['idmember'], $form['password']);
			return $res;
	   } else {
	   	   //unlink($_SERVER['DOCUMENT_ROOT'] . '/captcha/'.$res['word'].'.jpg');
	   	   return null;
	   }
	   
    }
	
	function checkAuthLoginSSS($data) {
       $res = $this->login_model->captchaCheck($data['captcha']);	
       if($res['response']) {	
	        //unlink($_SERVER['DOCUMENT_ROOT'] . '/captcha/'.$res['word'].'.jpg');	 
	        
			//$form = $this->input->post(NULL, TRUE);
			//print_r($data);
			$res = $this->login_model->checkAuthLoginKnet($data['idmember'], $data['password']);
			return $res;
	   } else {
	   	   //unlink($_SERVER['DOCUMENT_ROOT'] . '/captcha/'.$res['word'].'.jpg');
	   	   return null;
	   }	
	    //return $res;
    }

	function checkListPromo() {
		$res = $this->login_model->checkListPromo();
		$ass = array();
		if($res != null) {
			foreach($res as $dta) {
				$key = $dta->id;
				$ass[$key] = $dta->max_discount;
				$ass['full_discount'] = $dta->full_discount;
			}
		} else {
			$ass = null;
		}
	    return $ass;	
	}

	function createChaptcha() {
		$srvReturn = $this->login_model->createChaptcha();
		return $srvReturn;
	}
	
	function insertLogoutData() {
		$request = array();
	        $valReturn = array();
	        //$this->xmlrpc->set_debug(TRUE);
	        $this->xmlrpc->method('set.logout');
	        $request = array(array(array(
	                    "idmember" => array(getUserID(), 'string'),	                    
	                    "log_status" => array("2", 'string'),
	                    "ip_address" => array($this->input->ip_address(), 'string'),
	                    "signature" => array(md5(getUserID() . '12345'), 'string')
	                ), 'struct'
	        ));
			//print_r($request);
	        $this->xmlrpc->request($request);
	
	        if (!$this->xmlrpc->send_request()) {
	            return $this->xmlrpc->display_error();
	        } else {
	            $arr = $this->xmlrpc->display_response();
				return $valReturn = json_decode($arr['arrayData']);
	        }
	}
	
	
}	
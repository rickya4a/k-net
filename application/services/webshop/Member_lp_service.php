<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_lp_service extends MY_Service {	
	
	public function __construct() {
	    parent::__construct();
	}
	

	/*function getListBank() {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listbank');	
        $request = array(array(array(
        			//"idmemberx" => getUserID(),
        			"idmemberx" => "",
                    //"signature" => array(md5(getUserID() . '12345'), 'string')
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $ss = json_decode($arr['arrayData']);
			$valReturn = array("response" => "true", "arrayData" => $ss);
            return $valReturn;
        }
	}*/

	function checkNoKTP($noktp) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.ktpMember');	
        $request = array(array(array(
        			"noktp" => $noktp,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
		//print_r($request);
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //$ss = $arr;
			//$valReturn = array("response" => $res, "responseMessage" => $msg);
            return $arr;
		}	
	}

	function checkNoHP($nohp) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.noHp');	
        $request = array(array(array(
        			"telhp" => $nohp,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
		//print_r($request);
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            return $arr;
		}	
	}

	function listStarterkit() {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.listStarterkitLP');	
        $request = array(array(array(
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $ss = json_decode($arr['arrayData']);
			$valReturn = array("arrayData" => $ss);
			return $valReturn;
        }
	}
		
	function sessionRegPersonalInfo($data) {
		$this->session->unset_userdata('personal_info');
		$ssx = explode(" - ", $data['nama_stockist']);
		if($data['delivery'] == "1") {
			$arr = array(
			  "delivery" => $data['delivery'],
			  /*"stkarea" => $data['stkarea'],
			  "nama_stkarea" => $data['nama_stkarea'],
			  "stockist" => $data['stockist'],
			  "nama_stockist" => $data['nama_stockist'], */
			  "provinsi" => $data['provinsi1'],
			  "nama_provinsi" => $data['nama_provinsi1'],
			  "kota" => $data['kota1'],
			  "nama_kota" => $data['nama_kota1'],
			  "kecamatan" => $data['kecamatan1'],
			  "nama_kecamatan" => $data['nama_kecamatan1'],
			  "stockist" => $data['stockist'],
			  "nama_stockist" => $data['nama_stockist'],
			  "state" => $ssx[2]
			);
		} else {
			if($data['is_charge'] == "0") {
				$arr = array(
					"delivery" => "1",
					"provinsi" => $data['provinsi'],
				    "nama_provinsi" => $data['nama_provinsi'],
				    "kota" => $data['kota'],
				    "nama_kota" => $data['nama_kota'],
				    "kecamatan" => $data['kecamatan'],
				    "nama_kecamatan" => $data['nama_kecamatan'],
				    "stockist" => $data['stockistref'],
				    "nama_stockist" => $data['nama_stockistr1ref'],
				    "state" => $ssx[2]
			    );
			} else {
				$arr = array(
				  "delivery" => $data['delivery'],
				  "nama_penerima" => $data['nama_penerima'],
				  //"lastname" => $data['lastname'],
				  "email" => $data['email'],
				  "notlp" => $data['notlp'],
				  "stkarea" => NULL,
				  "nama_stkarea" => NULL,
				  "stockist" => $data['stockistref'],
				  "nama_stockist" => $data['nama_stockistr1ref'],
				  "provinsi" => $data['provinsi'],
				  "nama_provinsi" => $data['nama_provinsi'],
				  "kota" => $data['kota'],
				  "nama_kota" => $data['nama_kota'],
				  "kecamatan" => $data['kecamatan'],
				  "nama_kecamatan" => $data['nama_kecamatan'],
				  //"kelurahan" => $data['kelurahan'],
				  //"nama_kelurahan" => $data['nama_kelurahan'],
				  //"kodepos" => $data['kodepos'],
				  "alamat" => $data['alamat'],
				  "state" => $data['state1']
				  
				);
			}
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
			
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "reg_member_lp");
		$sess_sk = $this->session->userdata('personal_info');
		
		return $sess_sk;
	}

	function sessionRegStarterkit($data) {
		$this->session->unset_userdata('starterkit_prd');
		$prod = explode("|", $data['pilStarterkit']);
		if($data['pricecode'] == "12W3") {
			$harga = $prod[3];
		} else {
			$harga = $prod[4];
		}
		$pricecode = $this->session->set_userdata("pricecode", $data['pricecode']);
		$arr = array(
			  "prdcd" => $prod[0],
			  "prdnm" =>  $prod[1],
			  "price" => $harga,
			  "qty" => 1,
			  "weight" => $prod[2]
	    );
		$this->session->set_userdata('starterkit_prd', $arr);
		$sess_sk = $this->session->userdata('starterkit_prd');
		return $sess_sk;
	}
	
	function setShippingInfo() {
		$personal_info = $this->session->userdata('personal_info');	
		if($personal_info['delivery'] == "1") {
			   $arrx = array(
			           "origin_name" => "",
			           "destination_name" => "",
			           "service_display" => "",
			           "price" => 0,
			           "etd_from" => "",
			           "etd_thru" => "",
			           "times" => "",
					);
				$this->session->unset_userdata('shipping_jne_info');		
				$this->session->set_userdata('shipping_jne_info', $arrx);	
			return 1;		
		} else {
			   $dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));		
			   //$dtx = json_decode($this->showPriceResult2());
	    	  //update dion 03/09/2015
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
				//end
				    $arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;	
					/*
					$lowest = $dtx->price[0]->price;
					foreach($dtx->price as $dta) {
						if($dta->service_code == 'REG13') {
			    	  	 	$arrx = array(
					           "service_code" => $dta->service_code,
					           "origin_name" => $dta->origin_name,
					           "destination_name" => $dta->destination_name,
					           "service_display" => $dta->service_display,
					           "price" => $dta->price,
					           "etd_from" => $dta->etd_from,
					           "etd_thru" => $dta->etd_thru,
					           "times" => $dta->times,
							);
							break;
					    } else {
					    	//upd if($dta->price < $lowest) {
							if($dta->price <= $lowest) {
								$lowest = $dta->price;
								$arrx = array(
								  "service_code" => $dta->service_code,
								  "service_display" => $dta->service_display,
								  "origin_name" => $dta->origin_name,
								  "destination_name" => $dta->destination_name,
								  "price" => $dta->price,
								  "etd_from" => $dta->etd_from,
								  "etd_thru" => $dta->etd_thru,
								  "times" => $dta->times,
								);
							}
						}	
					}
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
					*/ 
				}
				
		}	  
				  
		
	}
	
	public function showPriceResult2() {
		$data = $this->input->post(NULL, TRUE);
        $xx = $this->session->userdata('shipping_jne_info');
		$url = "http://api.jne.co.id:8889/tracing/klink/price/";
        
        $curl_post_data = array(
                            "username" => "KLINK",
                            "api_key" => "76270305bef5d402220c96d59ac61977",
                            //"from" => "CGK10000",
                            //"thru" => "BDO10000",
                            "from" => $this->session->userdata("sender_address"),
                            "thru" => $this->session->userdata("destination_address"),
                            /*"from" => $xx['origin_name'],
                            "thru" => $xx['destination_name'],*/
                            "weight" => $this->cart->total_weight()
                            );
        //$curl = curl_init($url);
		$curl = curl_init($this->generate_url_ship_price);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return $curl_response;	
	}

	/*function showListProvinsi() {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listProvinsi');
        $request = array(array(array(
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}*/
	
	function showStockistByArea($userid) {
		$request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.area');
        $request = array(array(array(
                    "idmember" => $userid,
                    "signature" => array(md5($userid . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array("arrayData" => json_decode($arr['arrayData']));
			return $valReturn;
        }
	}
	
	function getListStockistByArea($area) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.stkByArea');
        $request = array(array(array(
        			"area" => $area,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        
		$dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
        
	}
	
	function showPricecodeByStockistID($nilai) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.priceStk');
        $request = array(array(array(
        			"stk" => $nilai,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showPricecodeByStockistIDX($nilai, $kec) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.priceStkX');
        $request = array(array(array(
        			"stk" => $nilai,
        			"kec" => $kec,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	
	function showListKota($prov) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKota');
        $request = array(array(array(
        			"prov" => $prov,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showListKecamatan($kota) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKec');
        $request = array(array(array(
        			"kota" => $kota,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showListKelurahan($kec) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKel');
        $request = array(array(array(
        			"kecamatan" => $kec,
        			"idmember" => "",
                    "signature" => array(md5("" . '12345'), 'string')
                ), 'struct'
        ));
		
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
            if(isset($arr['arrayData']) && isset($arr['listStk'])) {
				$valReturn = array(
				  "arrayData" => json_decode($arr['arrayData']),
				  "listStk" => json_decode($arr['listStk'])
				);
			} else {
				$valReturn = array(
				  "arrayData" => NULL,
				  "listStk" => NULL
				);
			}
			return $valReturn;
        }
        //$dta = setRPCResponse($this->xmlrpc, $request);
        //return $dta;
	}

	
	function getListRecruitLandingPage($recruiterid, $type) {
		$this->load->model('webshop/member_model', 'member_model');
		$listMember = $this->member_model->getListLandingPageRecruit($recruiterid, $type);
            if($listMember > 0){
                $response = array(
                            'arrayData' => $listMember,
                            'sukses' =>TRUE); 
    		    //print_r($parameters);
    			return $response;
            }else{
                $response = array(
                            'arrayData' => null,
                            'sukses' => FALSE); 
                return $response;
            }
	}
	
	/*function updateSponsorLP($data) {
		$valReturn = array();	
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        if($data['sponsornamex']!=null || $data['sponsornamex']!="" || $data['idmemberx'] != $data['idsponsorx'])
		{
			
	        $this->xmlrpc->method('set.updSponsorLP');
			$request = array(array(array(
	        			"recruiterid" => getUserID(),
	        			"idmember" => $data['idmemberx'],
	        			"idsponsor" => $data['idsponsorx'],
	                    "signature" => array(md5(getUserID() . '12345'), 'string')
	                ), 'struct'
	        ));
			
			$this->xmlrpc->request($request);
	
	        if (!$this->xmlrpc->send_request()) {
	            echo $this->xmlrpc->display_error();
	        } else {
	        	$arr = $this->xmlrpc->display_response();
	            if($arr['status'] == "2") {
	            	$valReturn = array(
					  "response" => "false",
					  "msg" => "Hanya boleh mengupdate sponsor dari member dengan rekruiter anda"
					);
	            } else if($arr['status'] == "0") {
	            	$valReturn = array(
					  "response" => "false",
					  "msg" => "Gagal mengupdate data sponsor"
					);
	            } else {
	            	$valReturn = array(
					  "response" => "true",
					  "msg" => "Data sponsor berhasil di update",
					  "arrayData" => json_decode($arr['arrayData'])
					);
	            }
				return $valReturn;
	        }
		}else{
			$valReturn = array("response" => "false",
					  			"msg" => "Sponsor tidak terdaftar."
								);
			return $valReturn;
		}
	} */

	function updateSponsorLP($data) {
		$idmember = $data['idmemberx'];
		$recruiterid = getUserID();
		$idsponsor = $data['idsponsorx'];
		
		$this->load->model('webshop/member_model', 'member_model');
		$check2 = $this->member_model->getDataMember($idsponsor);
		if($check2 != null) {
			$check = $this->member_model->checkSponsorLP($recruiterid, $idmember);
		
			if($check > 0) {
				$insCheckout = "UPDATE db_ecommerce.dbo.ecomm_memb_ok SET sponsorid = '$idsponsor'
				                WHERE memberid = '$idmember'";
	            //echo "checout ".$insCheckout."<br>";          
	            $queryCheckout = $this->db->query($insCheckout);
				if($queryCheckout > 0) {
					
					$listDta = $this->member_model->getDetailMember($idmember);
					
                	$valReturn = array(
					  "response" => "true",
					  "msg" => "Data sponsor berhasil di update",
					  "arrayData" => $listDta
					);
				} else {
					$valReturn = array(
					  "response" => "false",
					  "msg" => "Gagal mengupdate data sponsor"
					);
				}
			} else {
				$valReturn = array(
					  "response" => "false",
					  "msg" => "Hanya boleh mengupdate sponsor dari member dengan rekruiter anda"
					);
			}	
		} else {
			$valReturn = array(
					  "response" => "false",
					  "msg" => "Data sponsor tidak terdaftar"
					);
		}
		    

	      return $valReturn;
	}

	function checkInputMemberLP($data) {
		$srvReturn = jsonTrueResponse();
		$err = 0;	
		$msg2 = ""; $msg3 = "";
		
		$cekKTP = $this->checkNoKTP($data['idno']);
		if($cekKTP['sukses'] == "TRUE") {
			$err++;	
			$msg2 = "<font color=red>* KTP $data[idno] sudah terdaftar</font><br />";
		}
		
		$cekhp = $this->checkNoHP($data['tel_hp']);
		if($cekhp['sukses'] == "TRUE") {
			$err++;
			$msg3 = "<font color=red>* No HP $data[tel_hp] sudah terdaftar</font>";
		}
		
		if($err > 0) {
			$srvReturn = array("response" => "false", "err" => $msg2."".$msg3);
		}
		return $srvReturn;
	}
	
	function checkValidationMemberID($memberid) {
		$srvReturn = false;	
		$this->load->model('webshop/member_model', 'member_model');
		$check = $this->member_model->checkValidationMemberID($memberid);
		if($check > 0) {
			$srvReturn = true;
		}
		return $srvReturn;
	}
	
	function checkMemberValid2($idmember) {
		$srvReturn = false;	
		$this->load->model('webshop/member_model', 'member_model');
		$check = $this->member_model->checkMemberValid2($idmember);
		return $check;
	}
	/*
	function updateBankCodePayment($temp_id, $bankid) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
    	$res = $this->ecomm_trans_model->updateBankCodePayment($temp_id, $bankid);
		$res2 = $this->ecomm_trans_model->getDataPaymentSGOByOrderID($temp_id);
		return $res2;
	}*/


	  
	
}
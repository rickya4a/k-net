<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipping_service extends MY_Service {	
	
	public function __construct() {
	    parent::__construct();
	}
	
	//READY TO DELETE
	/*function getAddressReferenceByID($memberid) {
		$db = $this->load->database('db_ecommerce', true);
		$qry = "SELECT * FROM log_dest_address WHERE memberid = '$memberid'";
		$res = $db->query($qry);
		return $res->result();
	}*/
	
	function getMemberInfo($userid) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.idmember');	
        $request = array(array(array(
        			"idmemberx" => $userid,
                    "signature" => array(md5($userid . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $ss = json_decode($arr['arrayData']);
			$valReturn = array("sukses" => $arr['sukses'], "arrayData" => $ss);
            //print_r($arr);
			//$valReturn = array("sukses" => $arr['sukses'], "idmemb" => $arr['idmemb'], "fullnm" => $arr['fullnm']);
			return $valReturn;
        }
	}
	
	//READY TO DELETE 
	/*
	function getCurrentPeriod() {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.currentperiod');	
        $request = array(array(array(
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $ss = json_decode($arr['arrayData']);
			$valReturn = array("sukses" => $arr['sukses'], "arrayData" => $ss);
            //print_r($arr);
			//$valReturn = array("sukses" => $arr['sukses'], "idmemb" => $arr['idmemb'], "fullnm" => $arr['fullnm']);
			return $valReturn;
        }
	}*/
	
	function showStockistByArea($userid) {
		$request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.area');
        $request = array(array(array(
                    "idmember" => $userid,
                    "signature" => array(md5($userid . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array("arrayData" => json_decode($arr['arrayData']));
			return $valReturn;
        }
	}
	
	function getListStockistByArea($area) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.stkByArea');
        $request = array(array(array(
        			"area" => $area,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        
		$dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
        
	}
	
	function showPricecodeByStockistID($nilai) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.priceStk');
        $request = array(array(array(
        			"stk" => $nilai,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	//READY TO DELETE 
	/*
	function showPricecodeByStockistIDX($nilai, $kec) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.priceStkX');
        $request = array(array(array(
        			"stk" => $nilai,
        			"kec" => $kec,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	
	function showListProvinsi() {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listProvinsi');
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	} 
	
	function showListKota($prov) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKota');
        $request = array(array(array(
        			"prov" => $prov,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showListKecamatan($kota) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKec');
        $request = array(array(array(
        			"kota" => $kota,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showListKelurahan($kec) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKel');
        $request = array(array(array(
        			"kecamatan" => $kec,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
		
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
            if(isset($arr['arrayData']) && isset($arr['listStk'])) {
				$valReturn = array(
				  "arrayData" => json_decode($arr['arrayData']),
				  "listStk" => json_decode($arr['listStk'])
				);
			} else {
				$valReturn = array(
				  "arrayData" => NULL,
				  "listStk" => NULL
				);
			}
			return $valReturn;
        }
        //$dta = setRPCResponse($this->xmlrpc, $request);
        //return $dta;
	}*/
}
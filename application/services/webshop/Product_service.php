<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_service extends MY_Service {	

	public function __construct() {
	    parent::__construct();
		/*if(getUserID() != "ID9999A00251") {
			$this->load->model('webshop/Product_model', 'm_product');
		} else {*/
		    $date = date("Y-m-d");
			if($date >= "2019-03-01") {
				$this->load->model('webshop/Product_model_baru', 'm_product');
			} else {
				$this->load->model('webshop/Product_model', 'm_product');
			}
		//}
	}
	
	/*function getProductShop() {	
        $request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listProduct');
        $request = array(array(array(
                    "idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            return $valReturn = json_decode($arr['arrayData']);
        }
	}*/
	
	function getHeaderBanner() {
		$valReturn = $this->m_product->getHeaderBanner();
		return $valReturn;
	}

    function getHeaderBannerCache() {
        $valReturn = $this->m_product->getHeaderBannerCache();
        return $valReturn;
    }
	
	function getHeaderBannerCat() {
		$valReturn = $this->m_product->getHeaderBannerCat();
		return $valReturn;
	}
	
	
	function getProductShop($promo='0') {
		if($promo == '1'){
        	$valReturn = $this->m_product->getProductShop($promo); //jika promo, maka bundling tidak disertakan
        }else if($promo == '0'){
        	$valReturn = $this->m_product->getProductShop(); //jika reguler, maka bundling tidak disertakan
        }
		//$valReturn = $this->m_product->getProductShop();
		return $valReturn;
	}

    function getProductShopCache($promo='0') {
        if($promo == '1'){
            $valReturn = $this->m_product->getProductShopCache($promo); //jika promo, maka bundling tidak disertakan
        }else if($promo == '0'){
            $valReturn = $this->m_product->getProductShopCache(); //jika reguler, maka bundling tidak disertakan
        }
        //$valReturn = $this->m_product->getProductShop();
        return $valReturn;
    }
	
	function getProductShopByID($id) {
		$valReturn = $this->m_product->getProductShopByID($id);
		return $valReturn;
	}
	
	function getProductCategory() {
		
        $this->load->model('webshop/Product_model', 'm_product');
		$arr = $this->m_product->getListCatProd();
		return $arr;
	}

    function getProductCategoryCache() {
        
        $this->load->model('webshop/Product_model', 'm_product');
        $arr = $this->m_product->getListCatProdCache();
        return $arr;
    }
	
	function getProdByName($name) {
		$arr = $this->m_product->getProdByName(strtoupper($name));
		//$result = $arr['arrayData'];
		return $arr;
	}

    function getProdByNameCache($name) {
        $arr = $this->m_product->getProdByNameCache(strtoupper($name));
        //$result = $arr['arrayData'];
        return $arr;
    }
    
    function getProdByCat($id,$limit,$offset){
        /*$request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.prodByCat');
        $request = array(array(array(
                    "prdCat" => $id,
                    "limit" => $limit,
                    "offset" => $offset
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $valReturn = json_decode($arr['arrayData']);
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			return $returnArr;
        }*/
		
		$arr = $this->m_product->ListProdByCat($id,$limit,$offset);
		if($arr != null) {
		    $res = array("response" => "true", "arrayData" => $arr);
		} else {
			$res = array("response" => "false");
		}	
		//$result = $arr['arrayData'];
		return $res;
    }
    
    function getProdByCatHeader($id){
        /*$request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.prodByCatHeader');
        $request = array(array(array(
                    "prdCat" => $id
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $valReturn = json_decode($arr['arrayData']);
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			return $returnArr;
        }
		*/
		$arr = $this->m_product->ListProdByCatHeader($id);
		if($arr != null) {
		    $res = array("response" => "true", "arrayData" => $arr);
		} else {
			$res = array("response" => "false");
		}	
		//$result = $arr['arrayData'];
		return $res;
    }
    function getProdByCatHeaderCache($id){
        /*$request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.prodByCatHeader');
        $request = array(array(array(
                    "prdCat" => $id
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
            $arr = $this->xmlrpc->display_response();
            $valReturn = json_decode($arr['arrayData']);
            $returnArr = array("response" => "true", "arrayData" => $valReturn);
            return $returnArr;
        }
        */
        $arr = $this->m_product->ListProdByCatHeaderCache($id);
        if($arr != null) {
            $res = array("response" => "true", "arrayData" => $arr);
        } else {
            $res = array("response" => "false");
        }   
        //$result = $arr['arrayData'];
        return $res;
    }

    function getCountProduct(){
        $request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.countProd');
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);
        if ( ! $this->xmlrpc->send_request())
		{
			echo $this->xmlrpc->display_error();
		}
		else
		{
			//echo '<pre>';
			$dt = $this->xmlrpc->display_response();
			//echo '</pre>';
		}
        return $dt;
    }
	
	function getCountProductByID($id) {
		$arr = $this->m_product->getCountProdByID($id);
		return $arr;
	}
    
    function getProductDetails($prdcd){
        $request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.detProd');
        $request = array(array(array(
                    "prdcd" => $prdcd
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $valReturn = json_decode($arr['arrayData']);
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			return $returnArr;
        }
    }
	
	function getListCurrentPromo($year, $month) {
		$arr = $this->m_product->getListCurrentPromo($year, $month);
		return $arr;
	}


    //cahyono

    function getProdByCahyono($id){


        $arr = $this->m_product->ListProdByCahyono($id);
        if($arr != null) {
            $res = array("response" => "true", "arrayData" => $arr);
        } else {
            $res = array("response" => "false");
        }
        //$result = $arr['arrayData'];
        return $res;
    }

    //vera
    function getProductShopVera() {
        $valReturn = $this->m_product->getProductShopVera();
        return $valReturn;
    }

    function getProductShopVera1() {
        $valReturn = $this->m_product->getProductShopVera1();
        return $valReturn;
    }

    function getProductShopVeraD1() {
        $valReturn = $this->m_product->getProductShopVeraD1();
        return $valReturn;
    }

    function getProductShopVeraD2() {
        $valReturn = $this->m_product->getProductShopVeraD2();
        return $valReturn;
    }

    function getProductShopVeraD3() {
        $valReturn = $this->m_product->getProductShopVeraD3();
        return $valReturn;
    }

    function getProductShopVeraP1() {
        $valReturn = $this->m_product->getProductShopVeraP1();
        return $valReturn;
    }

    function getProductShopVeraP2() {
        $valReturn = $this->m_product->getProductShopVeraP2();
        return $valReturn;
    }

    function getProductShopVeraP3() {
        $valReturn = $this->m_product->getProductShopVeraP3();
        return $valReturn;
    }

    function getProductShopVeraP4() {
        $valReturn = $this->m_product->getProductShopVeraP4();
        return $valReturn;
    }

    function getProductShopVeraP5() {
        $valReturn = $this->m_product->getProductShopVeraP5();
        return $valReturn;
    }

    function getProductShopVeraP6() {
        $valReturn = $this->m_product->getProductShopVeraP6();
        return $valReturn;
    }

    function getProductShopVeraP7() {
        $valReturn = $this->m_product->getProductShopVeraP7();
        return $valReturn;
    }

    function getProductShopVeraT1() {
        $valReturn = $this->m_product->getProductShopVeraT1();
        return $valReturn;
    }

    function getProductShopVeraT2() {
        $valReturn = $this->m_product->getProductShopVeraT2();
        return $valReturn;
    }

    function getProdDet($value) {
        $valReturn = $this->m_product->getProdDet($value);
        return $valReturn;
    }

    function getProdDetFlora($value1,$value2,$value3,$value4,$value5,$value6,$value7) {
        $valReturn = $this->m_product->getProdDetFlora($value1,$value2,$value3,$value4,$value5,$value6,$value7);
        return $valReturn;
    }

    function getProdDetFlora2($value1,$value2,$value3,$value4) {
        $valReturn = $this->m_product->getProdDetFlora2($value1,$value2,$value3,$value4);
        return $valReturn;
    }

    function getProdDetAnniv2019($value1,$value2) {
        $valReturn = $this->m_product->getProdDetAnniv2019($value1,$value2);
        return $valReturn;
    }
}
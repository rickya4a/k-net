<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_payment_service extends MY_Service {
		
	public function __construct() {
        parent::__construct();
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
    }
	
	/*----------------------------------
	 * E-COMMERCE MODULE
	 * 
	 *----------------------------------*/
	
	function updateTrxSGO($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		//$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			if($upd > 0) {
				$this->generateCNoteJNE_baru($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	function updateTrxSGO_baru($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			if($upd > 0) {
				$this->generate_konot($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	function getInsertTrxOrderID($order_id) {
		//$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderID($order_id);
		return $arr;
	}
	
	function sendTrxSMS2($data) {
		//$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$upd = $this->ecomm_trans_model->sendTrxSMS2($data);
	}
	
}
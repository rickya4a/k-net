<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_service extends MY_Service {
		
	public function __construct() {
        parent::__construct();
		$this->load->model('webshop/Payment_model', 'payment_model');
		//Veritrans_Config::$serverKey = 'VT-server-qoQ6mGJFLS-zkG16wTg5ufR0';
        //Veritrans_Config::$isProduction = true;
		//Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
        //Veritrans_Config::$isProduction = false;
    }
	
	/*-------------------------------
	 * SGO PAYMENT UPDATE TRX
	 * -----------------------------*/
	function getTrxSGOByOrderID($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getTrxSGOByOrderID($order_id);
		return $arr;
	}
	
	/*function getDataPaymentSGOByOrderID($orderid) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getDataPaymentSGOByOrderID($orderid);
		return $arr;
	}*/
	
	function getInsertTrxOrderID($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderID($order_id);
		return $arr;
	}

	function getInsertTrxOrderIDNonMember($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderIDNonMember($order_id);
		return $arr;
	}
	
	function getInsertTrxOrderNo($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderNo($order_id);
		return $arr;
	}

	function updateTrxSGODev($order_id) {
		$upd = $this->ecomm_trans_model->insertTrxTblEcommDev($order_id, "CNTH1", "1");
	}
	
	function updateTrxSGOCust($order_id) {
		$res['jne'] = "";
		$return = false;
		//$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id); 
		if($arr) {		    
			//$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcommTest($order_id, "IDEC1603000029", $res['jne']);
			/*if($upd > 0) {
				$this->generateCNoteJNE_baru($order_id, $trcd);
				$return = true;
			}*/			
		} 
		return $upd;
	}
	 
	function updateTrxSGO($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			if($upd > 0) {
				//$this->generateCNoteJNE_baru($order_id, $trcd);
				$this->generate_konot($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	function updateTrxSGO_baru($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			if($upd > 0) {
				$this->generate_konot($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	
	
	function insertAddressReference($personal_info) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$memberid = getUserID();
		$check = $this->ecomm_trans_model->checkAddressReferenceByID($memberid);
		if($check < 1) {
			$sender_address = $this->session->userdata("sender_address");
			$destination_address = $this->session->userdata("destination_address");
			$jne_branch = $this->session->userdata("jne_branch");
			
			
			$arr = array(
				"sender_address" => $sender_address,
				"destination_address" => $destination_address,
				"jne_branch" => $jne_branch,
				"memberid" => $memberid
			);
			$ins = $this->ecomm_trans_model->insertAddressReference($personal_info, $arr);
		}
	}
	
	function prepareConote($orderno) {
		$resi =  $this->generateJNENo($orderno);
        $arr = json_decode($resi);
		//print_r($arr);
		$dta['jne'] = $arr->detail[0]->cnote_no;
		$updResi = $this->ecomm_trans_model->updateConotByOrderNo($orderno, $dta['jne']);
		return $dta['jne'];
	}
	
	function updateConotByOrderNo($orderno, $resi) {
		$upd = $this->ecomm_trans_model->updateConotByOrderNo($orderno, $resi);
	}

	function sendTrxSMS($order_no, $total_pay, $secno) {
		$upd = $this->ecomm_trans_model->sendTrxSMS($order_no, $total_pay, $secno);
	}
	
	function sendTrxSMS2($data) {
		$upd = $this->ecomm_trans_model->sendTrxSMS2($data);
	}
	
	function sendTrxSMSNonMember($data) {
		$upd = $this->ecomm_trans_model->sendTrxSMSNonMember($data);
		return $upd;
	}
	
	/*------------------
	 * END
	 -------------------*/ 
	function getItemDetailCart() {
		$item_details = array();
		$arr = $this->cart->contents();
		$pricecode = $this->session->userdata('pricecode');
		
		foreach($arr as $dta) {
			if($pricecode == "12W3") {
				$price = $dta['west_price'];
			} else {
				$price = $dta['east_price'];
			}
			
			$dtaArr = array(
			 "id" => $dta['id'],
			 "price" => $price,
		     "quantity" => $dta['qty'],
		     "name" => $dta['name'],
			 );
			 array_push($item_details, $dtaArr);
		}
		$totPayment = getTotalPayNet();
		//$adminCost = ($totPayment * 0.032) + 2500;
		$adminCost = 2500 + (2500 * 0.1);
		$dtaArr2 = array(
			 "id" => "ADM_COST",
			 "price" => $adminCost,
		     "quantity" => 1,
		     "name" => "ADM_COST",
			 );
			 array_push($item_details, $dtaArr2);
			 
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$shipCost = 0;
		if($shipping_jne_info != null) {
			$shipCost = $shipping_jne_info['price'];
		}
		
		$dtaArr3 = array(
			"id" => "SHIP_COST",
			 "price" => $shipCost,
		     "quantity" => 1,
		     "name" => "SHIP_COST",
		);
		array_push($item_details, $dtaArr3);
		return $item_details;
	}
	
	function creditCard($token_id) {
		
        
        if (empty($token_id)) {
            die('Empty token_id!');
        }

		// Uncomment for production environment
		// Veritrans_Config::$isProduction = true;
		// Uncomment to enable sanitization
		// Veritrans_Config::$isSanitized = true;
		
		$item_details = $this->getItemDetailCart();
		
		$billing_address = array(
            
        );
		
		$shipping_address = array(
            
        );

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => getTotalPayment("cc")
        );
		
		$customer_details = array(
            'first_name'    => getUsername(),
            'last_name'     => "", // Optional
            'email'         => getUserEmail(),
            'phone'         => getUserPhone(),
            //'billing_address' => $billing_address,
            //'shipping_address' => $shipping_address
        );
        
		// Transaction data to be sent
        $transaction_data = array(
                'payment_type' => 'credit_card',
                'credit_card' => array(
	                'token_id' => $token_id,
	                //'save_token_id' => isset($_POST['save_cc'])
	                'bank'    => 'bni'
	             ),
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details
            
        );
			
		//print_r($transaction_data);

        $res = Veritrans_VtDirect::charge($transaction_data);
		return $res; 
	}
	
	function mandiriClickPay() {
		$data = $this->input->post(NULL, TRUE);
        
        $transaction_details = array(
          'order_id'    => rand(),
          'gross_amount'  => getTotalPayment("dc")
        );
        
		$item_details = $this->getItemDetailCart();
        
        $customer_details = array(
            'first_name'    => getUsername(),
            'last_name'     => "", // Optional
            'email'         => getUserEmail(),
            'phone'         => getUserPhone()
        );
        
        // Data that will be sent for charge transaction request with Mandiri ClickPay.
        $transaction_data = array(
          'payment_type'      => 'mandiri_clickpay',
          'mandiri_clickpay' => array(
              'card_number' => $data['card-number'],
              'input1' => $data['input1'],
              'input2' => $data['input2'],
              'input3' => $data['input3'],
              'token' => $data['token']
            ),
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
        
        $res = Veritrans_VtDirect::charge($transaction_data);
		return $res;
	}

	function cimbClick() {
		$item_details = $this->getItemDetailCart();	
		$transaction_details = array(
          'order_id'    => rand(),
          'gross_amount'  => getTotalPayment("dc")
        );
        
        $customer_details = array(
            'first_name'    => getUsername(),
            'last_name'     => "", // Optional
            'email'         => getUserEmail(),
            'phone'         => getUserPhone()
            );
        
        // Data yang akan dikirim untuk request charge transaction dengan credit card.
        $transaction_data = array(
          'payment_type' => 'cimb_clicks',
          'cimb_clicks' => array(
              'description' => "Contoh Deskripsi",
            ),
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
        
        $res = Veritrans_VtDirect::charge($transaction_data);
		return $res;
	}
	
	function briEpay() {
		$item_details = $this->getItemDetailCart();
		$transaction_details = array(
          'order_id'    => rand(),
          'gross_amount'  => getTotalPayment("dc")
        );
        
        $customer_details = array(
            'first_name'    => getUsername(),
            'last_name'     => "", // Optional
            'email'         => getUserEmail(),
            'phone'         => getUserPhone()
            );
        
        // Data yang akan dikirim untuk request charge transaction dengan e-Pay BRI.
        $transaction_data = array(
          'payment_type' => 'bri_epay',
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details
          );
        
        $result = Veritrans_VtDirect::charge($transaction_data);
        return $result;
	}
	
	/*function saveEcommerceTrx2($veritrans, $jne) {
		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$bns = substr($personal_info['bnsperiod'], 3, 7);
		$totPay = getTotalPayNet();
		$admin_cost = $veritrans->gross_amount - ($totPay + $shipping_jne_info['price']);
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		
		try {
			$orderno   = $this->payment_model->generateOrderNO();
			$insChkOut = $this->payment_model->insertCheckOutData($orderno);
			
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}*/
	
	function insertECommerceTrxWithSGO($sgo, $jne) {
		
	}
		
	function saveECommerceTrx($veritrans, $jne) {
	    //$this->xmlrpc->set_debug(TRUE);
        $personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$bns = substr($personal_info['bnsperiod'], 3, 7);
		$totPay = getTotalPayNet();
		//$admin_cost = $veritrans->gross_amount - ($totPay + $shipping_jne_info['price']);
        //$admin_cost = 2000;
        $admin_cost = 2500 + (2500 * 0.1);
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$request = array();
		$valReturn = array();
  
        $this->xmlrpc->method('set.transCommerce');
		$app_code = "";
		if(isset($veritrans->approval_code)) {
			$app_code = $veritrans->approval_code;
		} 
		$tglx = date("Y-m-d H:i:s", strtotime($veritrans->transaction_time));
		if($personal_info['delivery'] === "2") {
			
	        $request = array(array(array(
	        			"idmember" => $personal_info['idmemberx'],
	        			"nmmember" => $personal_info['membername'],
	        			"userlogin" => getUserID(),
	        			"totpay" => $totPay,
	        			"shipment_cost" => $shipping_jne_info['price'],
	        			"admin_cost" => $admin_cost,
	        			"totbv" => $this->cart->total_bv(),
	        			"idstk" => $personal_info['stockist'],
				        "nmstk" => $nmstkSS,
				        "pricecode" => $pricecode,
				        "sentTo" => $personal_info['delivery'],
				        "hp_login" => getUserPhone(), 
				        //shipment Address
				        "prov_code" => $personal_info['provinsi'],
				        "kab_code" => $personal_info['kota'],
				        "kec_code" => $personal_info['kecamatan'],
				        "kel_code" => "",
				        "addr1" => $personal_info['alamat'],
				        "addr2" => "",
				        "addr3" => "",
				        "tel_hp1" => $personal_info['notlp'],
				        "tel_hp2" => $personal_info['notlp'],
				        "email" => $personal_info['email'],
				        
						"bonusmonth" => $bns,
						//from JNE
	        			"konot" => $jne,
	        			"jne_service_code" => $shipping_jne_info['service_code'],
	        			"jne_service_display" => $shipping_jne_info['service_display'],
	        			
						//from Veritrans
				        //"token_id" => $veritrans->order_id,
				        "status_code" => $veritrans->status_code,
				        "payment_type" => $veritrans->payment_type,
				        //"approval_code" => $veritrans->approval_code,
				        "approval_code" => $app_code,
				        "transaction_id" => $veritrans->transaction_id,
				        "transaction_status" => $veritrans->transaction_status,
				        "gross_amount" => $veritrans->gross_amount,
				        "status_message" => $veritrans->status_message,
				        "transaction_time" =>  $tglx,
				        "trans_id" => $veritrans->order_id,
				        
				        //produk
				        "produk" => json_encode($this->cart->contents()),
	                    "signature" => array(md5($personal_info['idmemberx'] . '12345'), 'string')
	                ), 'struct'
	        ));
		}else {
			$request = array(array(array(
	        			"idmember" => $personal_info['idmemberx'],
	        			"nmmember" => $personal_info['membername'],
	        			"userlogin" => getUserID(),
	        			"totpay" => $totPay,
	        			"shipment_cost" => $shipping_jne_info['price'],
	        			"admin_cost" => $admin_cost,
	        			"totbv" => $this->cart->total_bv(),
	        			"idstk" => $personal_info['stockist'],
				        "nmstk" => $nmstkSS,
				        "pricecode" => $pricecode,
				        "sentTo" => $personal_info['delivery'],
				        "hp_login" => getUserPhone(), 
				        //shipment Address
				        "prov_code" => $personal_info['provinsi'],
				        "kab_code" => "",
				        "kec_code" => "",
				        "kel_code" => "",
				        "addr1" => "",
				        "addr2" => "",
				        "addr3" => "",
				        "tel_hp1" => "",
				        "tel_hp2" => "",
				        "email" => "",
				        
						"bonusmonth" => $bns,
						//from JNE
	        			"konot" => $jne,
	        			"jne_service_code" => "",
	        			"jne_service_display" => "",
						//from Veritrans
				        //"token_id" => $veritrans->order_id,
				        /*"status_code" => "okeh",
				        "payment_type" => "credit_card",
				        //"approval_code" => $veritrans->approval_code,
				        "approval_code" => "11244",
				        "transaction_id" => "TEST333",
				        "transaction_status" => "capture",
				        "gross_amount" => 10000,
				        "status_message" => "SUKSES TESST",
				        "transaction_time" => "2015-08-31 12:07:31",
				        "trans_id" => "TEST233",*/
				        
				        "status_code" => $veritrans->status_code,
				        "payment_type" => $veritrans->payment_type,
				        //"approval_code" => $veritrans->approval_code,
				        "approval_code" => $app_code,
				        "transaction_id" => $veritrans->transaction_id,
				        "transaction_status" => $veritrans->transaction_status,
				        "gross_amount" => $veritrans->gross_amount,
				        "status_message" => $veritrans->status_message,
				        "transaction_time" =>  $tglx,
				        "trans_id" => $veritrans->order_id,
				        
				        //produk
				        "produk" => json_encode($this->cart->contents()),
	                    "signature" => array(md5($personal_info['idmemberx'] . '12345'), 'string')
	                ), 'struct'
	        ));
		}	
		//print_r($request);
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array(
			  "idmember" => $arr['idmember'],
              "orderno" => $arr['orderno']
			  //"listStk" => json_decode($arr['listStk'])
			);
			return $valReturn;
        }
		
	} 
	
	function generateJNENo($order_id = null) {
		
		$shipping = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		$sender_address = $this->session->userdata('sender_address');
		$destination_address = $this->session->userdata('destination_address');
		$jne_branch = $this->session->userdata('jne_branch');
		$ssx = explode(" - ", $personal_info['nama_stockist']);
		$nama_stk2 = str_replace("-", "", $ssx[1]);
		$nama_stk = substr($nama_stk2, 0, 20);	
		//$url = "http://api.jne.co.id:8889/tracing/apitest/generateCnoteTraining";
        $curl_post_data = array(
                            "username" => $this->jne_username,
                            "api_key" => $this->jne_api_key,
                            "OLSHOP_BRANCH" => $jne_branch,
                            "OLSHOP_CUST" => $this->jne_OLSHOP_CUST,
                            "OLSHOP_ORIG" => $sender_address,
                            "OLSHOP_ORDERID" => $order_id,
                            "OLSHOP_SHIPPER_NAME" => $nama_stk,
                            "OLSHOP_SHIPPER_ADDR1" => $nama_stk,
                            "OLSHOP_SHIPPER_ADDR2" => $nama_stk,
                            "OLSHOP_SHIPPER_ADDR3" => $nama_stk,
                            "OLSHOP_SHIPPER_CITY" => $nama_stk,
                            "OLSHOP_SHIPPER_REGION" => $nama_stk,
                            "OLSHOP_SHIPPER_ZIP" => "12620",
                            "OLSHOP_SHIPPER_PHONE" => getUserPhone(),
                            "OLSHOP_RECEIVER_NAME" => $personal_info['nama_penerima'],
                            "OLSHOP_RECEIVER_ADDR1" => substr($personal_info['alamat'], 0, 20),
                            "OLSHOP_RECEIVER_ADDR2" => substr($personal_info['alamat'], 21, 20),
                            "OLSHOP_RECEIVER_ADDR3" => substr($personal_info['alamat'], 41, 20),
                            "OLSHOP_RECEIVER_CITY" => $personal_info['nama_kota'],
                            "OLSHOP_RECEIVER_REGION" => $personal_info['nama_provinsi'],
                            "OLSHOP_RECEIVER_ZIP" => "12620",
                            "OLSHOP_RECEIVER_PHONE" => $personal_info['notlp'],
                            "OLSHOP_DEST" => $destination_address,
                            "OLSHOP_SERVICE" => $shipping['service_code'],
                            "OLSHOP_QTY" => $this->cart->total_items(),
                            "OLSHOP_WEIGHT" => $this->cart->total_weight(), 
                            
                            "OLSHOP_GOODSTYPE" => "2",
                            "OLSHOP_GOODSDESC" => "SUPLEMENT",
                            "OLSHOP_INST" => "FRAGILE",
                            "OLSHOP_GOODSVALUE" => getTotalPayNet(),
                            "OLSHOP_INSURANCE" => "Y"
                            );
        //print_r($curl_post_data);
		//echo "<br />";
		//echo "<br />";
        //$curl = curl_init($url);
        //echo "URL : $this->jne_url_conot<br />";
        $curl = curl_init($this->jne_url_conot);
		
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
		return $curl_response; 
		
		//return "CGKTES0002";
		
		
	}

	/*
	function generateCNoteJNE_baru($order_id = null, $orderno) {
		    $this->db = $this->load->database('db_ecommerce', true);	
		    $qryShpHdr = "SELECT * FROM ecomm_trans_shipaddr_sgo WHERE orderno = '$order_id'";
			$queryShpHdr = $this->db->query($qryShpHdr);
		    if($queryShpHdr->num_rows() > 0) {
				foreach($queryShpHdr->result() as $row)
	            {
		            
					$curl_post_data = array(
	                            "username" => $this->jne_username,
	                            "api_key" => $this->jne_api_key,
	                            "OLSHOP_BRANCH" => $row->jne_branch,
	                            "OLSHOP_CUST" => $this->jne_OLSHOP_CUST,
	                            "OLSHOP_ORIG" => $row->sender_address,
	                            "OLSHOP_ORDERID" => $orderno,
	                            "OLSHOP_SHIPPER_NAME" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ADDR1" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ADDR2" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ADDR3" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_CITY" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_REGION" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ZIP" => "12620",
	                            "OLSHOP_SHIPPER_PHONE" => $row->shipper_telhp,
	                            "OLSHOP_RECEIVER_NAME" => $row->receiver_name,
	                            "OLSHOP_RECEIVER_ADDR1" => substr($row->addr1, 0, 20),
	                            "OLSHOP_RECEIVER_ADDR2" => substr($row->addr1, 21, 20),
	                            "OLSHOP_RECEIVER_ADDR3" => substr($row->addr1, 41, 20),
	                            "OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
	                            "OLSHOP_RECEIVER_REGION" => $row->province_name,
	                            "OLSHOP_RECEIVER_ZIP" => "12620",
	                            "OLSHOP_RECEIVER_PHONE" => $row->tel_hp1,
	                            "OLSHOP_DEST" => $row->dest_address,
	                            "OLSHOP_SERVICE" => $row->service_type_id,
	                            "OLSHOP_QTY" => $row->total_item,
	                            "OLSHOP_WEIGHT" => $row->total_weight, 
	                            
	                            "OLSHOP_GOODSTYPE" => "2",
	                            "OLSHOP_GOODSDESC" => "SUPLEMENT",
	                            "OLSHOP_INST" => "FRAGILE",
	                            "OLSHOP_GOODSVALUE" => $row->total_pay_net,
	                            "OLSHOP_INSURANCE" => "Y"
	                            );
			        
			        $curl = curl_init($this->jne_url_conot);
					
			        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($curl, CURLOPT_POST, true);
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			        $curl_response = curl_exec($curl);	
					$arr = json_decode($curl_response);
					//print_r($arr);
					$jne_cnote = $arr->detail[0]->cnote_no;
					
					if($arr->detail == null || $arr->detail[0]->status == "Error") {
						$jne_cnote = null;
					} else if($arr->detail[0]->status == "Sukses") {
						$jne_cnote = $arr->detail[0]->cnote_no;
					} 
		            
		            $insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr 
		            					(orderno,idstk,prov_code,kab_code,kec_code,
		            					 addr1,email,tel_hp1, 
		            					 conoteJNE, service_type_id, service_type_name,
		            					 receiver_name, total_item, total_weight)
		                             values('".$orderno."','".$row->idstk."','".$row->prov_code."','".$row->kab_code."',
		                                    '".$row->kec_code."','".strtoupper($row->addr1)."',
		                                    '".$row->email."','".$row->tel_hp1."', 
		                                    '$jne_cnote','$row->service_type_id','$row->service_type_name',
		                                    '".$row->receiver_name."', ".$row->total_item.", ".$row->total_weight.")";
		            //echo "ShipAddr <br>".$insShipAddr."<br><br>"; 
		            $queryShipAddr = $this->db->query($insShipAddr);
					 
				}
			} 
	} */

	function getConoteJNE($orderno) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getConoteJNE($orderno);
		return $arr;
	}

	
}			
	
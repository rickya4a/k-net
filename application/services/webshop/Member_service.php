<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_service extends MY_Service {	
	
	public function __construct() {
	    parent::__construct();
		$this->load->model("webshop/Member_model", "member_model");
	}
	
	function getListRecruit($data) {
		$res = $this->member_model->getListRecruit($data);
		return $res;
	}

	function getListPayment($data) {
		$res = $this->member_model->getListPayment($data);
		return $res;
	}
	
	/*-------------------------
	 * SGO MODULE
	 -------------------------*/
	 function insertTempMemberSGO($orderid) { 	
		$res = $this->member_model->insertTempMemberSGO($orderid);
		return $res;
	 } 
	 
	 function insertTempMemberLPSGO($orderid) { 	
		$res = $this->member_model->insertTempMemberLPSGO($orderid);
		return $res;
	 } 
	 
	 function deleteTempMemberSGO($orderid) {
	 	$res = $this->member_model->deleteTempMemberSGO($orderid);
		return $res;
	 } 
	
	 /*---------------
	  * END SGO
	  ---------------*/
	function getShippingData($personal_info) {
		$request = array();
		$valReturn = array();	
		
			
	        //$this->xmlrpc->set_debug(TRUE);
	        $this->xmlrpc->method('get.getShippingData');
	        $request = array(array(array(
	                    "provinsi" => $personal_info['provinsi'],
	                    "kota" => $personal_info['kota'],
	                    "kecamatan" => $personal_info['kecamatan'],
	                    "idmember" => getUserID(),
	                    "signature" => array(md5(getUserID() . '12345'), 'string')
	                ), 'struct'
	        ));
	        //print_r($request);
			$this->xmlrpc->request($request);
	
	        if (!$this->xmlrpc->send_request()) {
	            echo $this->xmlrpc->display_error();
	        } else {
	        	$arr = $this->xmlrpc->display_response();
				$setArr = array(
					//"provinsi" => json_decode($arr['provinsi']),
					"kota" => json_decode($arr['kota']),
					"kecamatan" => json_decode($arr['kecamatan']),
					//"kelurahan" => json_decode($arr['kelurahan'])
					"listStk" => json_decode($arr['listStk'])
				);
				//print_r($setArr);
	            return $setArr;
	        }
	   
	}

	function getMemberInfo($userid) {
	    $arr = $this->member_model->getMemberInfo($userid);
	    return $arr;
	}
	
	
	function getMemberInfoxx($userid) {
		$request = array();
		$valReturn = array();
      
        $this->xmlrpc->method('check.sponsor');
		$request = array(array(array(
        			"idsponsor" => $userid,
        			"userlogin" => getUserID(),
                    "signature" => array(md5($userid . '12345'), 'string')
                ), 'struct'
        ));	
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //$ss = json_decode($arr['arrayData']);
            if($arr['sukses'] ==  TRUE) {
				$valReturn = array("response" => "true", "message" => $arr['responseMessage'], "sponsorname" => $arr['sponsorname']);
			} else {
				if($arr['responseMessage'] == "Id Sponsor Tidak Ditemukan") {
					$valReturn = array("response" => "err", "message" => $arr['responseMessage']);
				} else {	
					$valReturn = array("response" => "false", "message" => $arr['responseMessage'], "sponsorname" => $arr['sponsorname']);
				}
			}
           return $valReturn;
        }
	} 

	function getListBank() {
		/*$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listbank');	
        $request = array(array(array(
        			//"idmemberx" => getUserID(),
        			"idmemberx" => "",
                    //"signature" => array(md5(getUserID() . '12345'), 'string')
                    "signature" => array(md5("xxx" . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $ss = json_decode($arr['arrayData']);
			$valReturn = array("response" => "true", "arrayData" => $ss);
            return $valReturn;
        }
		*/
		$returnArr = array("response" => "false");
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$valReturn = $this->member_model->getListAffiliateBank();
		if($valReturn != null) {
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
		} 
		return $returnArr;
	}
	
	function checkValidationVoucher($voucherno, $voucherkey) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.voucher');	
        $request = array(array(array(
        			"voucherno" => $voucherno,
        			"voucherkey" => $voucherkey,
        			"userlogin" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
		//print_r($request);
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
			$valReturn = array("sukses" => $arr['sukses'], "responseMessage" => $arr['responseMessage'], "prdcd" => $arr['prdcd']);
            return $valReturn;
		}	
	}
	
	function checkValidationVoucher2($voucherno, $voucherkey) {
		
	    $res = $this->member_model->checkValidationVoucher2($voucherno, $voucherkey);
		if($res != null) {
			return array("sukses" => "TRUE", "arrayData" => $res);
		} else {
			return array("sukses" => "false");
		}
	}
	
	function checkValidationVoucherNew($voucherno, $voucherkey) {
		$res = $this->member_model->checkValidationVoucherNew($voucherno, $voucherkey);
		if($res != null) {
			if($res[0]->status == "0" || $res[0]->status == "" || $res[0]->status == null) {
				return array("sukses" => "false", "message" => "Voucher belum di release, silahkan hubungi stokis tempat anda membeli..");
			} else if($res[0]->status == "2") {
			    $pesan = "Voucher sudah aktif dengan member : ".$res[0]->activate_dfno." - ".$res[0]->fullnm;
				return array("sukses" => "false", "message" => $pesan);
			} else if($res[0]->status == "1") {
				return array("sukses" => "TRUE", "arrayData" => $res);
			} else if($res[0]->status == "X") {
				return array("sukses" => "false", "message" => "Voucher 50rb sudah tidak berlaku");
			} else {
				return array("sukses" => "false", "message" => "Status Voucher Invalid");
			}	
			
		} else {
			return array("sukses" => "false", "message" => "Voucher No atau Voucher Key tidak valid..");
		}
	}

	
	
	function showStockistByArea($userid) {
		$request = array();
        $valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.area');
        $request = array(array(array(
                    "idmember" => $userid,
                    "signature" => array(md5($userid . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array("arrayData" => json_decode($arr['arrayData']));
			return $valReturn;
        }
	}
	
	function getListStockistByArea($area) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.stkByArea');
        $request = array(array(array(
        			"area" => $area,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        
		$dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
        
	}
	
	function showPricecodeByStockistID($nilai) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.priceStk');
        $request = array(array(array(
        			"stk" => $nilai,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	/*function showListProvinsi() {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listProvinsi');
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}*/
	
	function showListProvinsi() {
		
		$res = $this->member_model->getListProvinsi();
		if($res != null) {
			return array(
                            'arrayData' => $res,
							'sukses' => TRUE);
			
		} else {
			return array(
                            'arrayData' => null,
							'sukses' => False);
		}
	}
	
	function showListKota($prov) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKota');
        $request = array(array(array(
        			"prov" => $prov,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showListKecamatan($kota) {
		$request = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKec');
        $request = array(array(array(
        			"kota" => $kota,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $dta = setRPCResponse($this->xmlrpc, $request);
        return $dta;
	}
	
	function showListKelurahan($kec) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('get.listKel');
        $request = array(array(array(
        			"kecamatan" => $kec,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
		
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
            if(isset($arr['arrayData']) && isset($arr['listStk'])) {
				$valReturn = array(
				  "arrayData" => json_decode($arr['arrayData']),
				  "listStk" => json_decode($arr['listStk'])
				);
			} else {
				$valReturn = array(
				  "arrayData" => NULL,
				  "listStk" => NULL
				);
			}
			return $valReturn;
        }
        //$dta = setRPCResponse($this->xmlrpc, $request);
        //return $dta;
	}

    /*function checkNoKTP($noktp) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.ktpMember');	
        $request = array(array(array(
        			"noktp" => $noktp,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
		//print_r($request);
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //$ss = $arr;
			//$valReturn = array("response" => $res, "responseMessage" => $msg);
            return $arr;
		}	
	}*/
	
	function checkNoKTP($noktp) {
		$res = $this->member_model->checkDoubleData("idno", $noktp);
		if($res != null) {
			$pemilik = $res[0]->dfno." / ".$res[0]->fullnm;
			return array('responseMessage' => "No KTP sudah terdaftar dengan ID : $pemilik",
                                'idmember' =>$res[0]->dfno,
                                'nmmember' =>$res[0]->fullnm,
                                'sukses' => "TRUE");
		} else {
			return array("sukses" => "FALSE");
		}
	}

	/*function checkNoHP($nohp) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.noHp');	
        $request = array(array(array(
        			"telhp" => $nohp,
        			"idmember" => getUserID(),
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
		//print_r($request);
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            return $arr;
		}	
	}*/
	
	function checkNoHP($nohp) {
		$res = $this->member_model->checkDoubleData("tel_hp", $nohp);
		if($res != null) {
			return array('responseMessage' => "No HP sudah terdaftar",
                                'idmember' =>$res[0]->dfno,
                                'nmmember' =>$res[0]->fullnm,
                                'sukses' => "TRUE");
		} else {
			return array("sukses" => "FALSE");
		}
	}	

	/*function listStarterkit() {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.listStarterkit');	
        $request = array(array(array(
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            $ss = json_decode($arr['arrayData']);
			$valReturn = array("arrayData" => $ss);
			return $valReturn;
        }
	}*/
	
	function listStarterkit() {
		$res = $this->member_model->getListStarterkit();
		if($res != null) {
			return array(
                            'arrayData' => $res,
							'sukses' => TRUE);
			
		} else {
			return array(
                            'arrayData' => null,
							'sukses' => False);
		}
		
	}
		
	function sessionRegPersonalInfo($data) {
		$this->session->unset_userdata('personal_info');
		if($data['delivery'] == "1") {
			$arr = array(
			  "delivery" => $data['delivery'],
			  /*"stkarea" => $data['stkarea'],
			  "nama_stkarea" => $data['nama_stkarea'],
			  "stockist" => $data['stockist'],
			  "nama_stockist" => $data['nama_stockist'], */
			  "provinsi" => $data['provinsi1'],
			  "nama_provinsi" => $data['nama_provinsi1'],
			  "kota" => $data['kota1'],
			  "nama_kota" => $data['nama_kota1'],
			  "kecamatan" => $data['kecamatan1'],
			  "nama_kecamatan" => $data['nama_kecamatan1'],
			  "stockist" => $data['stockist'],
			  "nama_stockist" => $data['nama_stockist'],
			  "state" => $data['state1']
			);
		} else {
			if($data['is_charge'] == "0") {
				$arr = array(
					"delivery" => "1",
					"provinsi" => $data['provinsi'],
				    "nama_provinsi" => $data['nama_provinsi'],
				    "kota" => $data['kota'],
				    "nama_kota" => $data['nama_kota'],
				    "kecamatan" => $data['kecamatan'],
				    "nama_kecamatan" => $data['nama_kecamatan'],
				    "stockist" => $data['stockistref'],
				    "nama_stockist" => $data['nama_stockistr1ref'],
				    "state" => $data['state1']
			    );
			} else {
				$arr = array(
				  "delivery" => $data['delivery'],
				  "nama_penerima" => $data['nama_penerima'],
				  //"lastname" => $data['lastname'],
				  "email" => $data['email'],
				  "notlp" => $data['notlp'],
				  "stkarea" => NULL,
				  "nama_stkarea" => NULL,
				  "stockist" => $data['stockistref'],
				  "nama_stockist" => $data['nama_stockistr1ref'],
				  "provinsi" => $data['provinsi'],
				  "nama_provinsi" => $data['nama_provinsi'],
				  "kota" => $data['kota'],
				  "nama_kota" => $data['nama_kota'],
				  "kecamatan" => $data['kecamatan'],
				  "nama_kecamatan" => $data['nama_kecamatan'],
				  //"kelurahan" => $data['kelurahan'],
				  //"nama_kelurahan" => $data['nama_kelurahan'],
				  //"kodepos" => $data['kodepos'],
				  "alamat" => $data['alamat'],
				  "state" => $data['state1']
				  
				);
			}	
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "reg_member");
		$sess_sk = $this->session->userdata('personal_info');
		return $sess_sk;
	}

	function sessionRegPersonalInfo_baru($data) {
		$this->session->unset_userdata('personal_info');
		if($data['delivery'] == "1") {
			$arr = array(
			  "delivery" => $data['delivery'],
			  /*"stkarea" => $data['stkarea'],
			  "nama_stkarea" => $data['nama_stkarea'],
			  "stockist" => $data['stockist'],
			  "nama_stockist" => $data['nama_stockist'], */
			  "provinsi" => $data['provinsi1'],
			  "nama_provinsi" => $data['nama_provinsi1'],
			  "kota" => $data['kota1'],
			  "nama_kota" => $data['nama_kota1'],
			  "kecamatan" => $data['kecamatan1'],
			  "nama_kecamatan" => $data['nama_kecamatan1'],
			  "stockist" => $data['stockist'],
			  "nama_stockist" => $data['nama_stockist'],
			  "state" => $data['state1']
			);
		} else {
			if($data['is_charge'] == "0") {
				if($data['shipper'] == "1") {
					$arr = array(
						"delivery" => "1",
						"provinsi" => $data['provinsi'],
					    "nama_provinsi" => $data['nama_provinsi'],
					    "kota" => $data['kota'],
					    "nama_kota" => $data['nama_kota'],
					    "kecamatan" => $data['kecamatan'],
					    "nama_kecamatan" => $data['nama_kecamatan'],
					    "stockist" => $data['stockistref'],
					    "nama_stockist" => $data['nama_stockistr1ref'],
					    "state" => $data['state1']
				    );
				} else {
					$arr = array(
						"delivery" => "1",
						"provinsi" => $data['provinsi'],
					    "nama_provinsi" => $data['nama_provinsi'],
					    "kota" => $data['kota'],
					    "nama_kota" => $data['nama_kota'],
					    "kecamatan" => "",
					    "nama_kecamatan" => "",
					    "stockist" => $data['stockistref'],
					    "nama_stockist" => $data['nama_stockistr1ref'],
					    "state" => $data['state1']
				    );
				}
			} else {
				
				if($data['shipper'] == "1") {
				$arr = array(
				  "delivery" => $data['delivery'],
				  "nama_penerima" => $data['nama_penerima'],
				  //"lastname" => $data['lastname'],
				  "email" => $data['email'],
				  "notlp" => $data['notlp'],
				  "stkarea" => NULL,
				  "nama_stkarea" => NULL,
				  "stockist" => $data['stockistref'],
				  "nama_stockist" => $data['nama_stockistr1ref'],
				  "shipper" => $data['shipper'],
				  "provinsi" => $data['provinsi'],
				  "nama_provinsi" => $data['nama_provinsi'],
				  "kota" => $data['kota'],
				  "nama_kota" => $data['nama_kota'],
				  //-----upd dion @05/12/2017
				  "kecamatan" => $data['kecamatan'],
				  "nama_kecamatan" => $data['nama_kecamatan'],
				  //---end
				  //"kelurahan" => $data['kelurahan'],
				  //"nama_kelurahan" => $data['nama_kelurahan'],
				  //"kodepos" => $data['kodepos'],
				  "alamat" => $data['alamat'],
				  //"idmemberx" => $data['idmemberx'],
				  //"membername" => $data['membername'],
				  
				  //"bnsperiod" => $data['bnsperiod'],
				  //"id_lp" => $data['id_lp']
				  //"alamat" => $data['alamat'],
				  "state" => $data['state1']
				);
			} else {
				$arr = array(
				  "delivery" => $data['delivery'],
				  "nama_penerima" => $data['nama_penerima'],
				  //"lastname" => $data['lastname'],
				  "email" => $data['email'],
				  "notlp" => $data['notlp'],
				  "stkarea" => NULL,
				  "nama_stkarea" => NULL,
				  "stockist" => $data['stockistref'],
				  "nama_stockist" => $data['nama_stockistr1ref'],
				  "shipper" => $data['shipper'],
				  "provinsi" => $data['provinsi'],
				  "nama_provinsi" => $data['nama_provinsi'],
				  "kota" => $data['kota'],
				  "nama_kota" => $data['nama_kota'],
				  //-----upd dion @05/12/2017
				  "kecamatan" => "",
				  "nama_kecamatan" => "",
				   //---end
				  //"kelurahan" => $data['kelurahan'],
				  //"nama_kelurahan" => $data['nama_kelurahan'],
				  //"kodepos" => $data['kodepos'],
				  "alamat" => $data['alamat'],
				  //"idmemberx" => $data['idmemberx'],
				 // "membername" => $data['membername'],
				  
				  //"bnsperiod" => $data['bnsperiod'],
				  //"id_lp" => $data['id_lp']
				  "state" => $data['state1']
				);
			} 
				/*$arr = array(
				  "delivery" => $data['delivery'],
				  "nama_penerima" => $data['nama_penerima'],
				  //"lastname" => $data['lastname'],
				  "email" => $data['email'],
				  "notlp" => $data['notlp'],
				  "stkarea" => NULL,
				  "nama_stkarea" => NULL,
				  "stockist" => $data['stockistref'],
				  "nama_stockist" => $data['nama_stockistr1ref'],
				  "provinsi" => $data['provinsi'],
				  "nama_provinsi" => $data['nama_provinsi'],
				  "kota" => $data['kota'],
				  "nama_kota" => $data['nama_kota'],
				  "kecamatan" => $data['kecamatan'],
				  "nama_kecamatan" => $data['nama_kecamatan'],
				  //"kelurahan" => $data['kelurahan'],
				  //"nama_kelurahan" => $data['nama_kelurahan'],
				  //"kodepos" => $data['kodepos'],
				  "alamat" => $data['alamat'],
				  "state" => $data['state1']
				  
				);*/
			}	
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "reg_member");
		$sess_sk = $this->session->userdata('personal_info');
		return $sess_sk;
	}

	function sessionRegStarterkit($data) {
		$this->session->unset_userdata('starterkit_prd');
		$prod = explode("|", $data['pilStarterkit']);
		if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4") {
			$harga = $prod[3];
		} else {
			$harga = $prod[4];
		}
		$pricecode = $this->session->set_userdata("pricecode", $data['pricecode']);
		$arr = array(
			  "prdcd" => $prod[0],
			  "prdnm" =>  $prod[1],
			  "price" => $harga,
			  "qty" => 1,
			  "weight" => $prod[2]
	    );
		$this->session->set_userdata('starterkit_prd', $arr);
		$sess_sk = $this->session->userdata('starterkit_prd');
		return $sess_sk;
	}
	
	function setShippingInfo($dataForm) {
		$personal_info = $this->session->userdata('personal_info');	
		if($personal_info['delivery'] == "1" || $dataForm['is_charge'] == "0") {
			   $arrx = array(
			           "service_code" => "",
			           "origin_name" => "",
			           "destination_name" => "",
			           "service_display" => "",
			           "price" => 0,
			           "etd_from" => "",
			           "etd_thru" => "",
			           "times" => "",
					);
				$this->session->unset_userdata('shipping_jne_info');		
				$this->session->set_userdata('shipping_jne_info', $arrx);	
			return 1;		
		} else {
			 $starterkit_prd = $this->session->userdata('starterkit_prd');
			 $dtx = json_decode($this->showListPriceJne($starterkit_prd['weight']));	
			 //$dtx = json_decode($this->showPriceResult2());
	    	 //print_r($dtx); 
			  
			    $arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
						$arrx = $this->setLowestJnePrice($dtx);
						$this->session->unset_userdata('shipping_jne_info');		
						$this->session->set_userdata('shipping_jne_info', $arrx);
						return $arrx;
					
			   }		
		}	  
				  
		
	}

	function setShippingInfoBaru() {
		$personal_info = $this->session->userdata('personal_info');	
		if($personal_info['delivery'] == "1") {
			   $arrx = array(
			           "service_code" => "",
			           "origin_name" => "",
			           "destination_name" => "",
			           "service_display" => "",
			           "price" => 0,
			           "etd_from" => "",
			           "etd_thru" => "",
			           "times" => "",
					);
			   $this->session->unset_userdata('shipping_jne_info');		
			   $this->session->set_userdata('shipping_jne_info', $arrx);		
			   return 1;
		} else {
			if($personal_info['shipper'] == "1") {
				$starterkit_prd = $this->session->userdata('starterkit_prd');
			    $dtx = json_decode($this->showListPriceJne($starterkit_prd['weight']));
			    $arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
						$arrx = $this->setLowestJnePrice($dtx);
						$this->session->unset_userdata('shipping_jne_info');		
						$this->session->set_userdata('shipping_jne_info', $arrx);
						return $arrx;
					
				}	
			} else {
				$starterkit_prd = $this->session->userdata('starterkit_prd');
			    $dtx = json_decode($this->showListPriceKgb($starterkit_prd['weight']));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
			           "origin_name" => $dtx->NamaOrigin,
			           "destination_name" => $dtx->NamaDestinasi,
			           "service_code" => "",
			           "service_display" => "",
			           "price" => $harga,
			           "etd_from" => "",
			           "etd_thru" => "",
			           "times" => "",
					);
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;	
				} else {
					$arrx = array(
			           "origin_name" => "",
			           "destination_name" => "",
			           "service_code" => "",
			           "service_display" => "",
			           "price" => "",
			           "etd_from" => "",
			           "etd_thru" => "",
			           "times" => "",
					);
					$this->session->unset_userdata('shipping_jne_info');		
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}
		}	
	}
	
	public function showPriceResult2() {
		$data = $this->input->post(NULL, TRUE);
        //$xx = $this->session->userdata('shipping_jne_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');
		
        $curl_post_data = array(
                            //"username" => "KLINK",
                            //"api_key" => "76270305bef5d402220c96d59ac61977",
                            "username" => $this->jne_username,
                            "api_key" => $this->jne_api_key,
                            //"from" => "CGK10000",
                            //"thru" => "BDO10000",
                            "from" => $this->session->userdata("sender_address"),
                            "thru" => $this->session->userdata("destination_address"),
                            /*"from" => $xx['origin_name'],
                            "thru" => $xx['destination_name'],*/
                            "weight" => $starterkit_prd['weight']
                            );
        //print_r($curl_post_data);
        //$curl = curl_init($url);
		$curl = curl_init($this->generate_url_ship_price);
		
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return $curl_response;	
	}

	function checkInputMemberWithVoucher($data) {
		$srvReturn = jsonTrueResponse();
		$err = 0;	
		$msg1 = ""; $msg2 = ""; $msg3 = "";
		$cekSponsor = $this->getMemberInfo($data['sponsorid']);
		if($cekSponsor['response'] == "err") {
			$err++;
			$msg1 = "<font color=red>* ID Sponsor $data[sponsorid] salah atau TERMINATION</font><br />";
		}
		$cekKTP = $this->checkNoKTP($data['idno']);
		if($cekKTP['sukses'] == "TRUE") {
			$err++;	
			$msg2 = "<font color=red>* KTP $data[idno] sudah terdaftar</font><br />";
		}
		
		$cekhp = $this->checkNoHP($data['tel_hp']);
		if($cekhp['sukses'] == "TRUE") {
			$err++;
			$msg3 = "<font color=red>* No HP $data[tel_hp] sudah terdaftar</font>";
		}
		
		if($err > 0) {
			$srvReturn = array("response" => "false", "err" => $msg1."".$msg2."".$msg3);
		}
		return $srvReturn;
	}
	
	function getListStockist() {
		$arr = $this->member_model->getListStockist();
		return $arr;
	}

	function saveMemberWithVoucher2() {
		$member_info = $this->session->userdata("member_info");	
		$stk = explode("|", $member_info['stk']);
		//echo $stk[0];
		try {
			$lastkit = $this->member_model->showLastkitno($stk[0]);
            if(!empty($lastkit)) {
            	
                if($lastkit[0]->lastkitno < 99999) {
                	//$new_id = $this->member_model->createNewID($lastkit);
                	$new_id = $this->member_model->createNewIDRec($lastkit, $stk[0]);
					$updlastkitno = $this->member_model->setLastKitNo($stk[0]);
                    if($updlastkitno > 0) {
                    	 //echo "masuk di lastkitno kurang dari 99999 <br>";
                         return $this->insertMember($new_id, $stk[0]);
					} else {
						 //"masuk di lastkitno mulai dari awal atau 0";	
						 $setLastKid = $this->member_model->setLastKitToZero($stk[0]);
                         $lastkit = $this->member_model->showLastkitno($stk[0]);
						 //$new_id = $this->member_model->createNewID($lastkit);
						 $new_id = $this->member_model->createNewIDRec($lastkit, $stk[0]);
						 $updlastkitno = $this->member_model->setLastKitNo($stk[0]);
                         if($updlastkitno > 0) {
                         	return $this->insertMember($new_id, $stk[0]);
                         }	
					}	
				}	
			}	
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);	
		}
	}

	private function insertMember($new_id, $idstk) {
		$cek_seQ = $this->member_model->cek_seQMemb();
        $ordernoo = $this->member_model->get_idnoMemb();
		$ins = $this->member_model->insertNewMemberWithVoucher($new_id, $ordernoo);
		if($ins == null) {
		 	$dec_lastkitno = $this->member_model->DecrementingLastKitNo($idstk);
			return jsonFalseResponse("Penginputan data member gagal.."); 
		} else {
			$telp = getUserPhone();
			if($telp == "" || $telp == null) {
		 	  $sendMsg = $this->member_model->sendSMSNotification($ins, $telp);
			}	
		 	//$sendMsg = $this->sendSMSNotification($ins, getUserPhone());
			return jsonTrueResponse($ins, "");
		}
	}
	
	function sendSMSNotification($arr, $phone) {
		$request = array();
		$valReturn = array();
		$this->xmlrpc->method('set.sendSMSNotification');
		$request = array(array(array(
	        			"userlogin" => getUserID(),
	        			"nohp" => $phone,
	        			"text" => $arr,
	        			"signature" => array(md5(getUserID() . '12345'), 'string')
	                ), 'struct'
	     ));
		 
		 $this->xmlrpc->request($request);

         if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
         } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array ( 
              "response" => $arr['response']
			);
			return $valReturn;
        }
	}

	function saveMemberWithVoucher() {
		//$this->xmlrpc->set_debug(TRUE);
		$member_info = $this->session->userdata('member_info');
		$memb_choose_pay = $this->session->userdata('memb_choose_pay');
		$request = array();
		$valReturn = array();
        $birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
        $this->xmlrpc->method('set.registerMemberWithVoucher');
		
	        $request = array(array(array(
	        			//DATA REGISTER MEMBER
	        			"userlogin" => getUserID(),
	        			"idsponsor" => $member_info['sponsorid'],
                        "rekruiterid" => getUserID(),
	        			"membername" => $member_info['membername'],
						"noktp" => $member_info['idno'],
						"addr1" => $member_info['addr1'],
						"addr2" => $member_info['addr2'],
						"addr3" => $member_info['addr3'],
						"telhm" => "",
						"tel_hp" => $member_info['tel_hp'],
						"email" => $member_info['memb_email'],
						"idstk" => "BID06",
						"nmstk" => "PT KLINK",
						"sex" => $member_info['sex'],
						"birthdt" => $birthdt,
						"bankaccno" => $member_info['no_rek'],
						"bankaccnm" => $member_info['membername'],
						"bankcode" => $member_info['pilBank'],
						"state" => "JH",
						"sentTo" => "3",
						"ipaddress" => $_SERVER['REMOTE_ADDR'],
						"hp_login" => getUserPhone(),
						
						//TIPE VOUCHER / NON VOUCHER
						"pay_tipe" => $memb_choose_pay['pay_tipe'],
						"voucherno" => $memb_choose_pay['voucherno'],
						"voucherkey" => $memb_choose_pay['voucherkey'],
						
						"signature" => array(md5(getUserID() . '12345'), 'string')
	                ), 'struct'
	        ));
		
		//print_r($request);
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array(
			  
              "idmember" => $arr['idmember'],
              "nmmember" => $arr['nmmember'],
              "password" => $arr['password'],
              "idsponsor" => $arr['idsponsor'],
              "nmsponsor" => $arr['nmsponsor'],
              //"stk" => $arr['stk'],
              
			  //"listStk" => json_decode($arr['listStk'])
			);
			return $valReturn;
        }
	}
}
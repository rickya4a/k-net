<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redis_service extends MY_Service {	

public function __construct() {
	    parent::__construct();
		$this->load->driver('cache', array('adapter' => 'redis', 'backup' => 'file'));
	}

	
	function getRedisProduct() {
			$valReturn = $this->cache->get('cache:prod');
			return $valReturn;
		}
		
	function getRedisMainBanner() {
		$valReturn = $this->cache->get('cache:mainbanner');
		return $valReturn;
	}

	function getRedisProductCat() {
		$valReturn = $this->cache->get('cache:prodcategory');
		return $valReturn;
	}

	function getRedisProductVera() {
		$valReturn = $this->cache->get('cache:prodvera');
		return $valReturn;
	}

	function getRedisProductJohn() {
		$valReturn = $this->cache->get('cache:prodvera1');
		return $valReturn;
	}

	function getRedisProductByName($name) {
		$valReturn = $this->cache->redis->hget('cache:prodbyname',(strtoupper($name)));
		return $valReturn;
	}
	
	function getRedisProdByCatHeader($id) {
		$arr = $this->cache->redis->hget('cache:prodbycatheader', $id);
		if($arr != null) {
            $res = array("response" => "true", "arrayData" => $arr);
        } else {
            $res = array("response" => "false");
        }   
		
		return $res;
	}

	function getRedisBonusReport($idmember,$year) {
		$valReturn = $this->cache->redis->hget('cache:bonus:'.$year,$idmember);
		return $valReturn;
	}

//Delete cache untuk keys yang berhubungan dengan halaman index, update produk
	function delProdRelatedCache() {
		$a = $this->cache->delete('cache:prodvera1');
		$b = $this->cache->delete('cache:prod');
		$b = $this->cache->delete('cache:prodbycatheader');
		$b = $this->cache->delete('cache:prodbyname');

		return compact('a', 'b');
	}


}
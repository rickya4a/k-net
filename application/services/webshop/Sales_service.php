<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_service extends MY_Service {	
	
	public function __construct() {
	    parent::__construct();
	}
    
    /*function getSalesMember($bulan,$tahun){
        $this->xmlrpc->set_debug(false);
   	    $request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('check.transMember');	
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "bulan" => $bulan,
                    "tahun" => $tahun,
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        $arr = $this->xmlrpc->display_response();
			$setArr = array(
				//"provinsi" => json_decode($arr['provinsi']),
				"header" => json_decode($arr['header']),
				//"detail" => json_decode($arr['detail'])
			);
            return $setArr;
        }
    }*/
	
	function getSalesMember($bulan,$tahun) {
		  $this->load->model('webshop/sales_model', 'sales');
		   $header = $this->sales->showHeaderTrans(getUserID());
            $detail = $this->sales->showDetailTrans(getUserID(),$bulan,$tahun);
            
            if($detail > 0 && $header > 0){
                $response = array(
                                    //'provinsi' => json_encode($provinsi),
                                    'header' => $header,
                                    'detail' => $detail,
                                    'sukses' =>TRUE); 
    		    //print_r($parameters);
    			return $response;
            }else{
                $response = array(
                            'header' => "no data",
                            'detail' => "no data",
                            'sukses' => FALSE); 
                return $response;
            }
	}	
    
   /* function detailSalesMember($trcd,$orderno,$tipe){
        //$this->xmlrpc->set_debug(TRUE);
        $request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('get.transDetail');	
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "trcd" => $trcd,
                    "orderno" => $orderno,
                    "tipe" => $tipe,
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        $arr = $this->xmlrpc->display_response();
			$setArr = array(
				//"provinsi" => json_decode($arr['provinsi']),
				//"header" => json_decode($arr['header']),
				"detail" => json_decode($arr['detail'])
			);
            return $setArr;
        }
    }*/
	
	function detailSalesMember($trcd,$orderno,$tipe){
		 $this->load->model('webshop/sales_model', 'sales');
		$header = $this->sales->showHeaderTrans(getUserID());
            $detTrans = $this->sales->showDetTrans($tipe, $trcd, $orderno);
            if($detTrans > 0 && $header > 0){
                $response = array(
                            'detail' => $detTrans,
                            'header' => $header,
                            'sukses' =>TRUE); 
    		    //print_r($parameters);
    			return $response;
            }else{
                $response = array(
                            'detail' => "no data",
                            'header' => "no data",
                            'sukses' =>TRUE); 
    		    //print_r($parameters);
    			return $response;
            }
	}	
    
    
    function getSalesMemberOnline($bnsperiod){
        $this->xmlrpc->set_debug(false);
   	    $request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('check.transMemberOnline');	
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "bnsperiod" => $bnsperiod,
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        $arr = $this->xmlrpc->display_response();
			$setArr = array(
				//"provinsi" => json_decode($arr['provinsi']),
				"header" => json_decode($arr['header']),
				//"detail" => json_decode($arr['detail'])
			);
            return $setArr;
        }
    }
	
	function getSalesMemberOnline2($idmember, $bnsperiod){
		$this->load->model('webshop/sales_model', 'sales');
		$header = $this->sales->showHeadTransOnline($idmember, $bnsperiod);
		$res = jsonFalseResponse("Tidak ada transaksi di bonus period".$bnsperiod);
		if($header != null) {
			$res = jsonTrueResponse($header);
		}
		return $res;
	}
	
	function detailSalesMemberOnline2($idmember,$orderno,$bnsperiod) {
		$this->load->model('webshop/sales_model', 'sales');
		/*$header = $this->sales->showDetProdTransOnline($orderno);
		$res = jsonFalseResponse("Tidak ada detail produk di TTP : ".$orderno);
		if($header != null) {
			$res = jsonTrueResponse($header);
		}
		return $res;*/
		
		$header = $this->sales->showDetTransOnline($idmember,$orderno,$bnsperiod);
        $detTrans = $this->sales->showDetProdTransOnline($orderno);
		if($header != null && $detTrans != null) {
			$arr = array(
				"header" => $header,
				"detail" => $detTrans,
				"response" => "true",
			);
		} else {
			$arr = jsonFalseResponse("Data tidak ada");
		}
		
		return $arr;
	}
    
    function detailSalesMemberOnline($orderno,$bnsperiod){
        $this->xmlrpc->set_debug(false);
        $request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('get.transDetailOnline');	
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "orderno" => $orderno,
                    "bnsperiod" => $bnsperiod,
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        $arr = $this->xmlrpc->display_response();
			$setArr = array(
				//"header" => json_decode($arr['header']),
				"header" => json_decode($arr['header']),
				"detail" => json_decode($arr['detail'])
			);
            return $setArr;
        }
    }
    
   
	function getBonusmonths(){
        $this->load->model('webshop/sales_model', 'sales');
		$arr = $this->sales->showBonusmonth(getUserID());
		if($arr != null) {
			$returnArr = array("sukses" => TRUE, "bonusmonth" => $arr);
		} else {
			$returnArr = array("sukses" => FALSE, "bonusmonth" => null);
		}

		return $returnArr;
    }
    
   
	
	function getDirectDownline1($idmember,$month,$year) {
		   $this->load->model('webshop/sales_model', 'sales');
		    $pbv = $this->sales->personalbv($idmember,$month,$year);
            $gbv = $this->sales->groupbv($idmember,$month,$year);
            
            if($pbv > 0 && $gbv > 0){
                $response = array(
                            'pbv' => $pbv,
                            'gbv' => $gbv,
                            'idmember'=>$idmember,
                            'month'=>$month,
                            'year'=>$year,
                            'sukses' =>TRUE); 
    		    return $response;
            }else{
                $response = array(
                            'pbv' => "no data",
                            'gbv' => "no data",
                            'idmember'=>$idmember,
                            'month'=>$month,
                            'year'=>$year,
                            'sukses' => FALSE); 
                return $response;
            }
	}	
    
    function getBonusMember($bulan,$tahun){
        $this->xmlrpc->set_debug(TRUE);
        $request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('get.bonusReport');	
        $request = array(array(array(
        			"idmember" => getUserID(),
                    "month" => $bulan,
                    "year" => $tahun,
                    "signature" => array(md5(getUserID() . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
            
            $arr = $this->xmlrpc->display_response();
            if($arr['dtBonus'] == "no data" && $arr['dtMember'] == "no data"){
                $setArr = array("dtBonus" => "No Data","dtMember" => "No Data");
            }else{
                $setArr = array(
                        "dtMember" => json_decode($arr['dtMember']),
                        "dtBonus" => json_decode($arr['dtBonus']),
                        "dtCurrency" => json_decode($arr['dtCurrency']),
                        "dtMonthly" => json_decode($arr['dtMonthly']),
                        "dtDownline" => json_decode($arr['dtDownline']),
                        "dtSelfBonus" => json_decode($arr['dtSelfBonus']),
                        "dtDevelopBonus" => json_decode($arr['dtDevelopBonus']),
                        "dtLeaderShip" => json_decode($arr['dtLeaderShip']),
                        "dtInfinityBonus" => json_decode($arr['dtInfinityBonus']),
                        "dtUnilevelBonus" => json_decode($arr['dtUnilevelBonus']),
                        "dtLevel" => json_decode($arr['dtLevel']),
                        "dtBonusVoucher" => json_decode($arr['dtBonusVoucher'])
			     );
            }
			
            //print_r($arr['dtBonusVoucher']);
            return $setArr;
        }
    }

    function getCashback($idmember,$month,$year){
        $this->load->model('webshop/sales_model', 'sales');
//        $header = $this->sales->showHeaderTrans(getUserID());
        $detail = $this->sales->getCashback($idmember,$month,$year);
//        print_r($detail);

        if($detail > 0){
            $response = array(
                //'provinsi' => json_encode($provinsi),
                'detail' => $detail,
                'idmember'=>$idmember,
                'month'=>$month,
                'year'=>$year,
                'sukses' =>TRUE);
            //print_r($parameters);
            return $response;
        }else{
            $response = array(
//                'header' => "no data",
                'detail' => "no data",
                'idmember'=>$idmember,
                'month'=>$month,
                'year'=>$year,
                'sukses' => FALSE);
            return $response;
        }

//        print_r($response);
    }

    function getListTrans($data) {
        $this->load->model('webshop/sales_model', 'sales');
        $res = $this->sales->getListTrans($data);
        return $res;
    }

    function createChaptcha() {
        $this->load->model('webshop/m_transfer_bv', 'transferbv');
		$srvReturn = $this->transferbv->createChaptcha();
		return $srvReturn;
    }
    
    function getSponsorCode($idmember) {
        $this->load->model('webshop/sales_model', 'sales');
        $res = $this->sales->getSponsorCode($idmember);
        return $res;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Knet_payment_service extends MY_Service {
		
	public function __construct() {
        parent::__construct();
	
    }
	
	/*----------------------------------
	 * E-COMMERCE MODULE
	 * 
	 *----------------------------------*/
	
	function updateTrxSGO($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			if($upd > 0) {
				$this->generateCNoteJNE_baru($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	function updateTrxSGO_baru($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			//$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($order_id, $trcd, "D");
			$updcashback = $this->ecomm_trans_model->updatePaymentVchCashback($order_id, $trcd);
			if($upd > 0) {
				$this->generate_konot($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	function updateTrxSGO_vera($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcommDev($order_id, $trcd, $res['jne']);
			//$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($order_id, $trcd, "D");
			$updcashback = $this->ecomm_trans_model->updatePaymentVchCashback($order_id, $trcd);
			if($upd > 0) {
				$this->generate_konot($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}
	
	function update_trx_knetbaru($order_id, $noresi) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			//$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($order_id, $trcd, "D");
			$updcashback = $this->ecomm_trans_model->updatePaymentVchCashback($order_id, $trcd);
			if($upd > 0) {
				//$this->generate_konot($order_id, $trcd);
				
				$this->insertTblEcommTransShipAddrNew($trcd, $order_id, $noresi);
				
				$return = true;
			}			
		} 
		return $return;
	}
	
	function updateTrxSGO_baruV2($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
		if($arr) {		    
			$trcd = $this->ecomm_trans_model->getsOrderno(); 
		    $upd = $this->ecomm_trans_model->insertTrxTblEcommV2($order_id, $trcd, $res['jne']);
			//$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($order_id, $trcd, "D");
			$updcashback = $this->ecomm_trans_model->updatePaymentVchCashback($order_id, $trcd);
			if($upd > 0) {
				$this->generate_konot($order_id, $trcd);
				$return = true;
			}			
		} 
		return $return;
	}


	function updateTrxSGO_baruDIGITAL($order_id) {
		$res['jne'] = "";
		$return = false;
		$personal_info = $this->session->userdata('personal_info');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());
		if($arr) {
			$trcd = $this->ecomm_trans_model->getsOrdernoDigital();
			$upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
			//$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($order_id, $trcd, "D");
			$updcashback = $this->ecomm_trans_model->updatePaymentVchCashback($order_id, $trcd);
		}
		return $return;
	}

	function getInsertTrxOrderID($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderID($order_id);
		return $arr;
	}
	
	function getInsertTrxMampirKak($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxMampirKak($order_id);
		return $arr;
	}
	
	function sendTrxSMS2($data) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$upd = $this->ecomm_trans_model->sendTrxSMS2($data);
	}
	
	function sendSMSPromoSalesAndReg($data) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$upd = $this->ecomm_trans_model->sendSMSPromoSalesAndReg($data);
	}
	
	function sendTrxSmsMampirKak($data) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$upd = $this->ecomm_trans_model->sendTrxSmsMampirKak($data);
	}
	
	function sendSmsRegMembMampirKak($result) {
		$this->load->model('webshop/member_model', 'member_model');
		$upd = $this->member_model->sendSmsRegMembMampirKak($result);
	}

	function sendTrxSMSNonMember($data) {
		$upd = $this->ecomm_trans_model->sendTrxSMSNonMember($data);
		return $upd;
	}
	
	function getRegMemberKnetPlus($orderno) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$upd = $this->ecomm_trans_model->getRegMemberKnetPlus($orderno);
	}
	
	/*----------------------------------
	 * REGISTER MEMBER MODULE
	 * 
	 *----------------------------------*/
	function createNewShoppingVoucher($resultInsMemb) {
		$this->load->model('webshop/member_model', 'member_model');
    	$res = $this->member_model->createNewShoppingVoucher($resultInsMemb);
		return $res;
    } 
	
	function saveMemberSGO_knetbaru($orderno, $dta, $noresi) {
		$this->load->model('webshop/member_model', 'member_model');	
		
		$qry = "SELECT * FROM db_ecommerce.dbo.ecomm_memb_ok_sgo a WHERE a.trx_no = '$orderno'";
		$hasil = $this->member_model->getRecordset($qry, null, "db_ecommerce");
		//$personal_info = $this->session->userdata("personal_info");	
		//$stk = explode("|", $member_info['stk']);
		//echo $stk[0];
		
		if($hasil != null) {
		    if($hasil[0]->sponsorid != null && $hasil[0]->sponsorid != "" && $hasil[0]->recruiterid != null && $hasil[0]->recruiterid != "" && $hasil[0]->idno != null && $hasil[0]->idno != "" && $hasil[0]->membername != null && $hasil[0]->membername != "" && $hasil[0]->stk_code != null && $hasil[0]->stk_code != "") {
				$kodeStockist = $hasil[0]->stk_code;
				$noktp = $hasil[0]->idno;
				
				$checkKTP = "SELECT a.dfno, a.fullnm, a.idno 
						FROM klink_mlm2010.dbo.msmemb a where a.idno = '$noktp'";
				$hasilCek = $this->member_model->getRecordset($checkKTP, null, "klink_mlm2010");
				
				//add by DION to makes sure ID sponsor and ID Recruiter is correct
				//@08/05/2019
				$sponsor = $hasil[0]->sponsorid;
				$recruiterid = $hasil[0]->recruiterid;
				//$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
				//$cekValidRecruiter = $this->member_model->cekValidSponsor($recruiterid);
				$resCheckSponsor = 0;
				$resCheckRecruiter = 0;
				if($sponsor == $recruiterid) {
					$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
					if($cekValidSponsor != null) {
						$resCheckSponsor = 1;
						$resCheckRecruiter = 1;
					}
				} else {
					$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
					if($cekValidSponsor != null) {
						$resCheckSponsor = 1;
					}	

					$cekValidRecruiter = $this->member_model->cekValidSponsor($recruiterid);
					if($cekValidRecruiter != null) {
						$resCheckRecruiter = 1;
					}
				}

				if($hasilCek == null && $resCheckSponsor == 1 && $resCheckRecruiter == 1) {
				//if($hasilCek == null) {
					try {
						$lastkit = $this->member_model->showLastkitno($dta[0]->idstk);
						if(!empty($lastkit)) {
							
							if($lastkit[0]->lastkitno < 99999) {
								
								$updlastkitno = $this->member_model->setLastKitNo($dta[0]->idstk);
								if($updlastkitno > 0) {
									 //echo "masuk di lastkitno kurang dari 99999 <br>";
									 $new_id = $this->member_model->createNewID($lastkit);
									 return $this->insertMember_knetbaru($orderno, $new_id, $dta[0]->idstk, $noresi);
								} else {
									 //"masuk di lastkitno mulai dari awal atau 0";	
									 $setLastKid = $this->member_model->setLastKitToZero($dta[0]->idstk);
									 $lastkit = $this->member_model->showLastkitno($dta[0]->idstk);
									 $new_id = $this->member_model->createNewID($lastkit);
									 $updlastkitno = $this->member_model->setLastKitNo($dta[0]->idstk);
									 if($updlastkitno > 0) {
										return $this->insertMember_knetbaru($orderno, $new_id, $dta[0]->idstk, $noresi);
									 } else {
										return array("response" => "false", "message" => "Failed when update laskitno");	
									 }	
								}	
							}	
						}	
					} catch(Exception $e) {
						throw new Exception($e->getMessage(), 1);	
					}
				} else 	{
					//return false;
					$memberid = $hasilCek[0]->dfno;
					$fullnm = $hasilCek[0]->fullnm;
					return jsonFalseResponse("KTP sudah terdaftar : $memberid / $fullnm"); 
			    }
			} else {
				//return false;
				return jsonFalseResponse("Data sponsor/recruiter/KTP/stokis kurang lengkap.."); 
			}
		} else {
			//return false;
			return jsonFalseResponse("No Token tidak ditemukan..."); 
		}	
	}
	
	private function insertMember_knetbaru($orderno, $new_id, $idstk, $noresi) {
		$res['jne'] = "";	
		$this->load->model('webshop/member_model', 'member_model');	
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');	
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($orderno); 
		
		if($arr) {
		    $cek_seQ = $this->member_model->cek_seQMemb();
		    $trcd = $this->member_model->get_idnoMemb();			
		    $upd = $this->ecomm_trans_model->insertTrxAndDataMember($new_id, $orderno, $trcd, "0");
			$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($orderno, $trcd, "D");
			if($upd > 0) {
				//$this->generateCNoteJNE_baru($orderno, $trcd);
				
				$this->insertTblEcommTransShipAddrNew($trcd, $orderno, $noresi);
				//$this->generate_konot($orderno, $trcd);
	            return array("response" => "true", "memberid" => $new_id);	
				
			} else {
				return jsonFalseResponse("Penginputan Trx input member.., upd : $upd"); 
			} 
			
		} 
		
	}
	 
	
	function saveMemberSGO($orderno, $dta) {
		$this->load->model('webshop/member_model', 'member_model');	
		
		$qry = "SELECT * FROM db_ecommerce.dbo.ecomm_memb_ok_sgo a WHERE a.trx_no = '$orderno'";
		$hasil = $this->member_model->getRecordset($qry, null, "db_ecommerce");
		//$personal_info = $this->session->userdata("personal_info");	
		//$stk = explode("|", $member_info['stk']);
		//echo $stk[0];
		
		if($hasil != null) {
		    if($hasil[0]->sponsorid != null && $hasil[0]->sponsorid != "" && $hasil[0]->recruiterid != null && $hasil[0]->recruiterid != "" && $hasil[0]->idno != null && $hasil[0]->idno != "" && $hasil[0]->membername != null && $hasil[0]->membername != "" && $hasil[0]->stk_code != null && $hasil[0]->stk_code != "") {
				$kodeStockist = $hasil[0]->stk_code;
				$noktp = $hasil[0]->idno;
				
				$checkKTP = "SELECT a.dfno, a.fullnm, a.idno 
						FROM klink_mlm2010.dbo.msmemb a where a.idno = '$noktp'";
				$hasilCek = $this->member_model->getRecordset($checkKTP, null, "klink_mlm2010");
				
				//add by DION to makes sure ID sponsor and ID Recruiter is correct
				//@08/05/2019
				$sponsor = $hasil[0]->sponsorid;
				$recruiterid = $hasil[0]->recruiterid;
				//$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
				//$cekValidRecruiter = $this->member_model->cekValidSponsor($recruiterid);
				$resCheckSponsor = 0;
				$resCheckRecruiter = 0;
				if($sponsor == $recruiterid) {
					$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
					if($cekValidSponsor != null) {
						$resCheckSponsor = 1;
						$resCheckRecruiter = 1;
					}
				} else {
					$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
					if($cekValidSponsor != null) {
						$resCheckSponsor = 1;
					}	

					$cekValidRecruiter = $this->member_model->cekValidSponsor($recruiterid);
					if($cekValidRecruiter != null) {
						$resCheckRecruiter = 1;
					}
				}

				if($hasilCek == null && $resCheckSponsor == 1 && $resCheckRecruiter == 1) {
					try {
						$lastkit = $this->member_model->showLastkitno($dta[0]->idstk);
						if(!empty($lastkit)) {
							
							if($lastkit[0]->lastkitno < 99999) {
								
								$updlastkitno = $this->member_model->setLastKitNo($dta[0]->idstk);
								if($updlastkitno > 0) {
									 //echo "masuk di lastkitno kurang dari 99999 <br>";
									 $new_id = $this->member_model->createNewID($lastkit);
									 return $this->insertMember($orderno, $new_id, $dta[0]->idstk);
								} else {
									 //"masuk di lastkitno mulai dari awal atau 0";	
									 $setLastKid = $this->member_model->setLastKitToZero($dta[0]->idstk);
									 $lastkit = $this->member_model->showLastkitno($dta[0]->idstk);
									 $new_id = $this->member_model->createNewID($lastkit);
									 $updlastkitno = $this->member_model->setLastKitNo($dta[0]->idstk);
									 if($updlastkitno > 0) {
										return $this->insertMember($orderno, $new_id, $dta[0]->idstk);
									 } else {
										return array("response" => "false", "message" => "Failed when update laskitno");	
									 }	
								}	
							}	
						}	
					} catch(Exception $e) {
						throw new Exception($e->getMessage(), 1);	
					}
				} else 	{
					//return false;
					$memberid = $hasilCek[0]->dfno;
					$fullnm = $hasilCek[0]->fullnm;
					return jsonFalseResponse("KTP sudah terdaftar : $memberid / $fullnm"); 
			    }
			} else {
				//return false;
				return jsonFalseResponse("Data sponsor/recruiter/KTP/stokis kurang lengkap.."); 
			}
		} else {
			//return false;
			return jsonFalseResponse("No Token tidak ditemukan..."); 
		}	
	}
	
	/*private function insertMember($new_id, $idstk) {
		$this->load->model('webshop/member_model', 'member_model');	
		$cek_seQ = $this->member_model->cek_seQMemb();
        $ordernoo = $this->member_model->get_idnoMemb();
		$ins = $this->member_model->insertNewMemberWithVoucher($new_id, $ordernoo);
		if($ins == null) {
		 	$dec_lastkitno = $this->member_model->DecrementingLastKitNo($idstk);
			return jsonFalseResponse("Penginputan data member gagal.."); 
		} else {
			$telp = getUserPhone();
			if($telp == "" || $telp == null) {
		 	  $sendMsg = $this->member_model->sendSMSNotification($ins, $telp);
			}	
		 	//$sendMsg = $this->sendSMSNotification($ins, getUserPhone());
			return jsonTrueResponse($ins, "");
		}
	}*/
	
	private function insertMember($orderno, $new_id, $idstk) {
		$res['jne'] = "";	
		$this->load->model('webshop/member_model', 'member_model');	
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');	
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($orderno); 
		
		if($arr) {
		    $cek_seQ = $this->member_model->cek_seQMemb();
		    $trcd = $this->member_model->get_idnoMemb();			
		    $upd = $this->ecomm_trans_model->insertTrxAndDataMember($new_id, $orderno, $trcd, "0");
			$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($orderno, $trcd, "D");
			if($upd > 0) {
				$this->generateCNoteJNE_baru($orderno, $trcd);
				//$this->generate_konot($orderno, $trcd);
	            return array("response" => "true", "memberid" => $new_id);	
				
			} else {
				return jsonFalseResponse("Penginputan Trx input member.., upd : $upd"); 
			} 
			
		} 
		
	} 
	
	function transaksiKnetPlusMemb($orderid, $resultInsert) {
		$this->load->model('webshop/member_model', 'member_model');	
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res['jne'] = "";
		$qry = "SELECT * FROM db_ecommerce.dbo.ecomm_memb_ok_sgo a WHERE a.trx_no = '$orderid'";
		$hasil = $this->member_model->getRecordset($qry, null, "db_ecommerce");
		//echo $qry;
		if($hasil != null) {
		    if($hasil[0]->sponsorid != null && $hasil[0]->sponsorid != "" && $hasil[0]->recruiterid != null && $hasil[0]->recruiterid != "" && $hasil[0]->idno != null && $hasil[0]->idno != "" && $hasil[0]->membername != null && $hasil[0]->membername != "" && $hasil[0]->stk_code != null && $hasil[0]->stk_code != "") {
				$kodeStockist = $hasil[0]->stk_code;
				$noktp = $hasil[0]->idno;
				
				$checkKTP = "SELECT a.dfno, a.fullnm, a.idno 
						FROM klink_mlm2010.dbo.msmemb a where a.idno = '$noktp'";
				$hasilCek = $this->member_model->getRecordset($checkKTP, null, "klink_mlm2010");

				//add by DION to makes sure ID sponsor and ID Recruiter is correct
				//@08/05/2019
				$sponsor = $hasil[0]->sponsorid;
				$recruiterid = $hasil[0]->recruiterid;
				//$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
				//$cekValidRecruiter = $this->member_model->cekValidSponsor($recruiterid);
				$resCheckSponsor = 0;
				$resCheckRecruiter = 0;
				if($sponsor == $recruiterid) {
					$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
					if($cekValidSponsor != null) {
						$resCheckSponsor = 1;
						$resCheckRecruiter = 1;
					}
				} else {
					$cekValidSponsor = $this->member_model->cekValidSponsor($sponsor);
					if($cekValidSponsor != null) {
						$resCheckSponsor = 1;
					}	

					$cekValidRecruiter = $this->member_model->cekValidSponsor($recruiterid);
					if($cekValidRecruiter != null) {
						$resCheckRecruiter = 1;
					}
				}

				if($hasilCek == null && $resCheckSponsor == 1 && $resCheckRecruiter == 1) {
				
					try {
						$lastkit = $this->member_model->showLastkitno($kodeStockist);
						if(!empty($lastkit)) {
							
							if($lastkit[0]->lastkitno < 99999) {
								
								$updlastkitno = $this->member_model->setLastKitNo($kodeStockist);
								if($updlastkitno > 0) {
									 //echo "masuk di lastkitno kurang dari 99999 <br>";
									$new_id = $this->member_model->createNewIDRec($lastkit, $kodeStockist);
									$arrMembIns = array(
										   "orderno" => $resultInsert[0]->orderno,
										   "new_id" => $new_id,
										   "datamember" => $hasil
									);
									$insmemb = $this->member_model->insertNewMemberPromoMampirKak($arrMembIns);
									if($insmemb > 0) {
										return true;
									} else {
										return false;
									}
								} else {
									 //"masuk di lastkitno mulai dari awal atau 0";	
									 $setLastKid = $this->member_model->setLastKitToZero($kodeStockist);
									 $lastkit = $this->member_model->showLastkitno($kodeStockist);
									 $new_id = $this->member_model->createNewIDRec($lastkit, $kodeStockist);
									 $updlastkitno = $this->member_model->setLastKitNo($kodeStockist);
									 if($updlastkitno > 0) {
										$arrMembIns = array(
										   "orderno" => $resultInsert[0]->orderno,
										   "new_id" => $new_id,
										   "datamember" => $hasil
										 );
										 $insmemb = $this->member_model->insertNewMemberPromoMampirKak($arrMembIns);
										 if($insmemb > 0) {
											return true;
										 } else {
											return false;
										 }
									 } else {
										return false;	
									 }	
								}	
							}	
						}
				
					} catch(Exception $e) {
						return false;
					}
				} else 	{
					return false;
			    }
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function processTrxMampirKak($orderid) {
		$this->load->model('webshop/member_model', 'member_model');	
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res['jne'] = "";
		$qry = "SELECT * FROM db_ecommerce.dbo.ecomm_memb_ok_sgo a WHERE a.trx_no = '$orderid'";
		$hasil = $this->member_model->getRecordset($qry, null, "db_ecommerce");
		//echo $qry;
		if($hasil != null) {
			$kodeStockist = $hasil[0]->stk_code;
			/*$arrMembIns = array(
			   "orderno" => "TES",
			   "new_id" => "TESDSDSD",
			   "datamember" => $hasil
			);
			$insmemb = $this->member_model->insertNewMemberPromoMampirKak($arrMembIns); */
			
			try {
				$lastkit = $this->member_model->showLastkitno($kodeStockist);
	            if(!empty($lastkit)) {
	            	
	                if($lastkit[0]->lastkitno < 99999) {
	                	
						$updlastkitno = $this->member_model->setLastKitNo($kodeStockist);
	                    if($updlastkitno > 0) {
	                    	 //echo "masuk di lastkitno kurang dari 99999 <br>";
	                    	$new_id = $this->member_model->createNewIDRec($lastkit, $kodeStockist);
	                        $arr = $this->ecomm_trans_model->updateSgoTrxMampirKak($orderid, $new_id, $hasil[0]->membername); 
							if($arr) {		    
								$trcd = $this->ecomm_trans_model->getsOrderno(); 
							    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($orderid, $trcd, $res['jne']);
								$arrMembIns = array(
								   "orderno" => $trcd,
								   "new_id" => $new_id,
								   "datamember" => $hasil
								);
								$insmemb = $this->member_model->insertNewMemberPromoMampirKak($arrMembIns);
								if($upd > 0) {
									$this->generate_konot($orderid, $trcd);
									return true;
								}			
							} 
						} else {
							 //"masuk di lastkitno mulai dari awal atau 0";	
							 $setLastKid = $this->member_model->setLastKitToZero($kodeStockist);
	                         $lastkit = $this->member_model->showLastkitno($kodeStockist);
							 $new_id = $this->member_model->createNewIDRec($lastkit, $kodeStockist);
							 $updlastkitno = $this->member_model->setLastKitNo($kodeStockist);
	                         if($updlastkitno > 0) {
	                         	$arr = $this->ecomm_trans_model->updateSgoTrxMampirKak($orderid, $new_id, $hasil[0]->membername); 
								if($arr) {		    
									$trcd = $this->ecomm_trans_model->getsOrderno(); 
								    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($orderid, $trcd, $res['jne']);
									$arrMembIns = array(
									   "orderno" => $trcd,
									   "new_id" => $new_id,
									   "datamember" => $hasil
									);
									$insmemb = $this->member_model->insertNewMemberPromoMampirKak($arrMembIns);
									if($upd > 0) {
										$this->generate_konot($orderid, $trcd);
										return true;
									}			
								} 
	                         } else {
	                         	return false;	
	                         }	
						}	
					}	
				}
	 	
			} catch(Exception $e) {
				return false;
			} 
		} else {
			//return false;
			
			//$res['jne'] = "";
			$return = false;
			//$personal_info = $this->session->userdata('personal_info');
			//$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
			//$trcd = $this->ecomm_trans_model->getsOrderno();
			$arr = $this->ecomm_trans_model->updatePayStatusSGO($orderid);
			//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());  
			//print_r($arr);
			//echo "</br>";
			if($arr) {		    
				$trcd = $this->ecomm_trans_model->getsOrderno(); 
			    $upd = $this->ecomm_trans_model->insertTrxTblEcomm($orderid, $trcd, $res['jne']);
				//$updcashback = $this->ecomm_trans_model->setCashBackPointBalance($order_id, $trcd, "D");
				//$updcashback = $this->ecomm_trans_model->updatePaymentVchCashback($orderid, $trcd);
				if($upd > 0) {
					$this->generate_konot($orderid, $trcd);
					$return = true;
				}			
			} 
			return $return;
		}
	}
	
	/*----------------------------------
	 * STOCKIST ONLINE PAYMENT
	 *----------------------------------*/
	 
	 function getStockistPayment($orderno) {
	 	$this->load->model('backend/stockist/m_knet_pay_stk', 'paystk');
		$res = $this->paystk->getStkPayHeader($orderno);
		return $res;
	 }
	 
	 function insertTblEcommTransShipAddr($resultInsert, $orderid, $noresi) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
	    $this->load->model('webshop/member_model', 'member_model');
		$qry = "SELECT * FROM [db_ecommerce].[dbo].ecomm_trans_shipaddr_sgo where orderno = '$orderid'";
		$resultQry = $this->member_model->getRecordset($qry, null, "db_ecommerce");
		if($resultQry != null) {
			$array = array(
				"orderno" => $resultInsert[0]->orderno,
				"idstk" => $resultQry[0]->idstk,
				"kec_code" => $resultQry[0]->idstk,
				"receiver_name" => $resultQry[0]->idstk,
				"addr1" => $resultQry[0]->idstk,
				"email" => $resultQry[0]->email,
				"conoteJNE" => $noresi,
				"service_type_id" => $resultQry[0]->service_type_id,
				"service_type_name" => $resultQry[0]->service_type_name,
				//"total_item" => $resultQry[0]->total_item,
				//"total_weight" => $resultQry[0]->total_weight,
				"lat_dest" => $resultQry[0]->lat_dest,
				"long_dest" => $resultQry[0]->long_dest,
				"cargo_id" => $resultQry[0]->cargo_id
			);
			
			$dbqryx->insert("db_ecommerce.dbo.ecomm_trans_shipaddr", $array);	
		}
	}
	
	function insertTblEcommTransShipAddrNew($orderno_idec, $orderid, $noresi) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
	    $this->load->model('webshop/member_model', 'member_model');
		$qry = "SELECT * FROM [db_ecommerce].[dbo].ecomm_trans_shipaddr_sgo where orderno = '$orderid'";
		$resultQry = $this->member_model->getRecordset($qry, null, "db_ecommerce");
		if($resultQry != null) {
			$array = array(
				"orderno" => $orderno_idec,
				"idstk" => $resultQry[0]->idstk,
				"kec_code" => $resultQry[0]->kec_code,
				"receiver_name" => $resultQry[0]->receiver_name,
				"addr1" => $resultQry[0]->addr1,
				"email" => $resultQry[0]->email,
				"conoteJNE" => $noresi,
				"service_type_id" => $resultQry[0]->service_type_id,
				"service_type_name" => $resultQry[0]->service_type_name,
				"total_item" => $resultQry[0]->total_item,
				"total_weight" => $resultQry[0]->total_weight,
				"lat_dest" => $resultQry[0]->lat_dest,
				"long_dest" => $resultQry[0]->long_dest,
				"cargo_id" => $resultQry[0]->cargo_id,
				"tel_hp1" => $resultQry[0]->tel_hp1
			);
			
			$dbqryx->insert("db_ecommerce.dbo.ecomm_trans_shipaddr", $array);	
		}
	}

	function processInsertParamSgo($data) {
		//$arr[0] = $this->sgo_signature_key;
		
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);

		$data['rq_datetime'] = date("Y-m-d H:i:s",strtotime($data['rq_datetime']));
		$data['payment_datetime'] = date("Y-m-d H:i:s",strtotime($data['payment_datetime']));
		$data['amount'] = (double) $data['amount'];
        //tambahan dion @07/05/2018
        $data['total_amount'] = (double) $data['total_amount'];
		$potongan = $data['total_amount'] - $data['amount'];
		//end
		//$data['member_id'] = $data['member_code'];
		//QUERY FOR CHECK DOUBLE TRANSACTION ID
		$dbqryx->select('payment_ref');
		$dbqryx->from('ecomm_paynotif_req');
		//$dbqryx->where('rq_uuid', $data['rq_uuid']);
		$dbqryx->where('payment_ref', $data['payment_ref']);
		$query = $dbqryx->get();
		//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
		if($query->num_rows() == 0) {
			
			$insArr = array(
				"rq_uuid" => $data['rq_uuid'],
				"rq_datetime" => $data['rq_datetime'],
				"sender_id" => $data['sender_id'],
				"receiver_id" => $data['receiver_id'],
				"password" => $data['password'],
				"comm_code" => $data['comm_code'],
				"member_code" => $data['member_code'],
				"member_cust_id" => $data['member_cust_id'],
				"member_cust_name" => $data['member_cust_name'],
				"amount" => $data['amount'],
				"debit_from" => $data['debit_from'],
				"debit_from_name" => $data['debit_from_name'],
				"debit_from_bank" => $data['debit_from_bank'],
				"receiver_id" => $data['receiver_id'],
				"credit_to" => $data['credit_to'],
				"credit_to_name" => $data['credit_to_name'],
				"credit_to_bank" => $data['credit_to_bank'],
				"payment_datetime" => $data['payment_datetime'],
				"payment_ref" => $data['payment_ref'],
				"payment_remark" => $data['payment_remark'],
				"order_id" => $data['order_id'],
				"product_code" => $data['product_code'],
				"product_value" => $data['product_value'],
				"message" => $data['message'],
				"status" => $data['status'],
				"token" => $data['token'],
				"total_amount" => $data['total_amount'],
				"member_id" => $data['member_id'],
				//"approval_code_full_bca" => $data['approval_code_full_bca'],
				"signature" => $data['signature'],
				"payment_gateway" => "sgo"
			);
			$ins = $dbqryx->insert('ecomm_paynotif_req', $insArr);
			
		}
			
		
	}
	
}
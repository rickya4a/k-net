<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_lp_member_service extends MY_Service {
		
	public function __construct() {
        parent::__construct();
		//Veritrans_Config::$serverKey = 'VT-server-qoQ6mGJFLS-zkG16wTg5ufR0';
        //Veritrans_Config::$isProduction = true;
		//Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
        //Veritrans_Config::$isProduction = false;
    }
	/*-----------------
	 * SGO MODULE
	 * ---------------*/
	
	function getTempMemberSGO($orderid) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getTrxSGOByOrderID($order_id);
		return $arr;
	}
	/*
	function getDataPaymentSGOByOrderID($orderid) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getDataPaymentSGOByOrderID($orderid);
		return $arr;
	}*/
	
	function getInsertTrxOrderID($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderID($order_id);
		return $arr;
	}
	
	/*function getNewMemberData($token) {
		$this->load->model('webshop/member_model', 'member_model');	
		$arr = $this->member_model->show_member_new2($token);
		return $arr;
	}*/
	
	function getInsertTrxOrderNo($order_id) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getInsertTrxOrderNo($order_id);
		return $arr;
	}
	
	function prepareConote($orderno) {
		$resi =  $this->generateJNENo($orderno);
        $arr = json_decode($resi);
		//print_r($arr);
		$dta['jne'] = $arr->detail[0]->cnote_no;
		$updResi = $this->ecomm_trans_model->updateConotByOrderNo($orderno, $dta['jne']);
		return $dta['jne'];
	}
	
	

	function sendTrxSMS($order_no) {
		$this->load->model('webshop/member_model', 'member_model');	
		//$upd = $this->member_model->sendSMSNotification($order_no, $order_no[0]->tel_hp);
		$upd = $this->member_model->sendSMSNotification($order_no, $order_no[0]->tel_hp);
	}
	
	
	/*function saveMemberSGO($orderno) {
		
	}*/	
	
	function saveMemberSGO($orderno, $dta) {
		$this->load->model('webshop/member_model', 'member_model');	
		
		$personal_info = $this->session->userdata("personal_info");	
		//$stk = explode("|", $member_info['stk']);
		//echo $stk[0];
		try {
			$lastkit = $this->member_model->showLastkitno($dta[0]->idstk);
            if(!empty($lastkit)) {
            	
                if($lastkit[0]->lastkitno < 99999) {
                	
					$updlastkitno = $this->member_model->setLastKitNo($dta[0]->idstk);
                    if($updlastkitno > 0) {
                    	 //echo "masuk di lastkitno kurang dari 99999 <br>";
                    	 $new_id = $this->member_model->createNewID($lastkit);
                         return $this->insertMember($orderno, $new_id, $dta[0]->idstk);
					} else {
						 //"masuk di lastkitno mulai dari awal atau 0";	
						 $setLastKid = $this->member_model->setLastKitToZero($dta[0]->idstk);
                         $lastkit = $this->member_model->showLastkitno($dta[0]->idstk);
						 $new_id = $this->member_model->createNewID($lastkit);
						 $updlastkitno = $this->member_model->setLastKitNo($dta[0]->idstk);
                         if($updlastkitno > 0) {
                         	return $this->insertMember($orderno, $new_id, $dta[0]->idstk);
                         }	
					}	
				}	
			}	
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);	
		}
	}

	private function insertMember($orderno, $new_id, $idstk) {
		$res['jne'] = "";	
		$this->load->model('webshop/member_model', 'member_model');	
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');	
		$arr = $this->ecomm_trans_model->updatePayStatusSGO($orderno); 
		
		if($arr) {
		    
			//$trcd = $this->ecomm_trans_model->getsOrderno();
			
			/*if($personal_info['delivery'] == "2") {
	            $resi =  $this->generateJNENo($trcd);
	            $arr = json_decode($resi);
				$res['jne'] = $arr->detail[0]->cnote_no;
			}*/
			$cek_seQ = $this->member_model->cek_seQMemb();
		    $trcd = $this->member_model->get_idnoMemb();
		    //$upd = $this->ecomm_trans_model->insertTrxTblEcommLP($new_id, $orderno, $trcd, $res['jne']);
			$upd = $this->ecomm_trans_model->insertTrxAndDataMember($new_id, $orderno, $trcd, "1");
			//$ins = $this->member_model->insertNewMemberLPUsingSGO($orderno, $new_id, $trcd, "1");
			if($upd > 0) {
				//$this->generateCNoteJNE_baru($orderno, $trcd);
				$this->generate_konot($orderno, $trcd);
	            return array("response" => "true", "memberid" => $new_id);	
				/*$ins = $this->member_model->insertNewMemberLPUsingSGO($orderno, $new_id, $trcd, "1");
				if($ins > 0) {
				 	return array("response" => "true", "memberid" => $new_id);	
				 	
				} else {
				 	$dec_lastkitno = $this->member_model->DecrementingLastKitNo($idstk);
					return jsonFalseResponse("Penginputan data member gagal.."); 
				}*/
				 
			} else {
				return jsonFalseResponse("Penginputan Trx input member.., upd : $upd"); 
			} 
			
		} 
		
	} 
	/*-----------------
	 * END
	 * ---------------*/
	
	
	function getItemPrdRegMember() {
		$item_details = array();
		$starterkit_prd = $this->session->userdata('starterkit_prd');
			
		$dtaArr = array(
		 "id" => $starterkit_prd['prdcd'],
		 "price" => $starterkit_prd['price'],
	     "quantity" => 1,
	     "name" => $starterkit_prd['prdnm'],
		 );
		
		array_push($item_details, $dtaArr);
		
		//$totPayment = $starterkit_prd['price'];
		//$adminCost = ($totPayment * 0.032) + 2500;
		$adminCost = 2500 + (2500 * 0.1);
		$dtaArr2 = array(
			 "id" => "ADM_COST",
			 "price" => $adminCost,
		     "quantity" => 1,
		     "name" => "ADM_COST",
	    );
		array_push($item_details, $dtaArr2);
		
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$shipCost = 0;
		if($shipping_jne_info != null) {
			$shipCost = $shipping_jne_info['price'];
		}
		
		$dtaArr3 = array(
			"id" => "SHIP_COST",
			 "price" => $shipCost,
		     "quantity" => 1,
		     "name" => "SHIP_COST",
		);
		array_push($item_details, $dtaArr3);
		return $item_details;
	}
	
	function creditCard($token_id) {
		
        
        if (empty($token_id)) {
            die('Empty token_id!');
        }

		// Uncomment for production environment
		// Veritrans_Config::$isProduction = true;
		// Uncomment to enable sanitization
		// Veritrans_Config::$isSanitized = true;
		$item_details = $this->getItemPrdRegMember();

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => getTotalPaymentSK("cc")
        );
		
		$member_info = $this->session->userdata('member_info');
		$customer_details = array(
            'first_name'    =>$member_info['membername'],
            'last_name'     => "", // Optional
            'email'         => $member_info['email_pendaftar'],
            'phone'         => $member_info['tel_hp'],
            //'billing_address' => $billing_address,
            //'shipping_address' => $shipping_address
        );
        
		// Transaction data to be sent
        $transaction_data = array(
                'payment_type' => 'credit_card',
                'credit_card' => array(
	                'token_id' => $token_id,
	                'save_token_id' => isset($_POST['save_cc'])
	             ),
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details
            
        );

        $res = Veritrans_VtDirect::charge($transaction_data);
		return $res; 
	}
	
	function mandiriClickPay() {
		$data = $this->input->post(NULL, TRUE);
        $member_info = $this->session->userdata('member_info');
        $transaction_details = array(
          'order_id'    => rand(),
          'gross_amount'  => getTotalPaymentSK("dc")
        );
        
        $customer_details = getCustomerDetail("lp");
        
        // Data that will be sent for charge transaction request with Mandiri ClickPay.
        $transaction_data = array(
          'payment_type'      => 'mandiri_clickpay',
          'mandiri_clickpay' => array(
              'card_number' => $data['card-number'],
              'input1' => $data['input1'],
              'input2' => $data['input2'],
              'input3' => $data['input3'],
              'token' => $data['token']
            ),
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details
          );
        
        $res = Veritrans_VtDirect::charge($transaction_data);
		return $res;
	}

	function cimbClick() {
			
		$transaction_details = array(
          'order_id'    => rand(),
          'gross_amount'  => getTotalPaymentSK("dc")
        );
        
        $customer_details = getCustomerDetail("lp");
        
        // Data yang akan dikirim untuk request charge transaction dengan credit card.
        $transaction_data = array(
          'payment_type' => 'cimb_clicks',
          'cimb_clicks' => array(
              'description' => "Contoh Deskripsi",
            ),
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details
          );
        
        $res = Veritrans_VtDirect::charge($transaction_data);
		return $res;
	}
	
	function briEpay() {
			
		$transaction_details = array(
          'order_id'    => rand(),
          'gross_amount'  => getTotalPaymentSK("dc")
        );
        
        $customer_details = getCustomerDetail("lp");
        
        // Data yang akan dikirim untuk request charge transaction dengan e-Pay BRI.
        $transaction_data = array(
          'payment_type' => 'bri_epay',
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details
          );
        
        $result = Veritrans_VtDirect::charge($transaction_data);
        return $result;
	}
		
	function saveECommerceTrx($veritrans, $jne) {
	    //$this->xmlrpc->set_debug(TRUE);
        $personal_info = $this->session->userdata('personal_info');
		$member_info = $this->session->userdata('member_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		//$bns = substr($personal_info['bnsperiod'], 3, 7);
		//edited by dion 19/06/2015
		$birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
		//end edit
		$totPay = $starterkit_prd['price'];
		$admin_cost = $veritrans->gross_amount - ($totPay + $shipping_jne_info['price']);
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('set.registerMembCommerce');
		if($personal_info['delivery'] === "2") {
	        $request = array(array(array(
	        			//DATA REGISTER MEMBER
	        			"userlogin" => $member_info['recruiterid'],
	        			"idsponsor" => "0000999",
                        "rekruiterid" => $member_info['recruiterid'],
	        			"membername" => $member_info['membername'],
						"noktp" => $member_info['idno'],
						"addr1" => $member_info['addr1'],
						"addr2" => $member_info['addr2'],
						"addr3" => $member_info['addr3'],
						"telhm" => "",
						"tel_hp" => $member_info['tel_hp'],
						"email" => "",
						"idstk" => $personal_info['stockist'],
						"nmstk" => $personal_info['nama_stockist'],
						"sex" => $member_info['sex'],
						//"birthdt" => $member_info['birthdt'],
						"birthdt" => $birthdt,
						"bankaccno" => $member_info['no_rek'],
						"bankaccnm" => $member_info['membername'],
						"bankcode" => $member_info['pilBank'],
						"state" => $personal_info['state'],
						"sentTo" => $personal_info['delivery'],
						"ipaddress" => $_SERVER['REMOTE_ADDR'],
						"hp_login" => $member_info['tel_hp'],
						//add by dion 09/09/2015
						"is_landingpage" => "1",
						//end add
						
						//TIPE VOUCHER / NON VOUCHER
						"pay_tipe" => "0",
						"voucherno" => "",
						"voucherkey" => "",
						
						//DATA STARTERKIT
						"pricecode" => $pricecode,
						"prdcd" => $starterkit_prd['prdcd'],
						"prdnm" => $starterkit_prd['prdnm'],
						"qty" => $starterkit_prd['qty'],
						"bv" => 0,
						"price" => $starterkit_prd['price'],
						
						//DATA SHIPMENT ADDRESS
						"prov_code" => $personal_info['provinsi'],
				        "kab_code" => $personal_info['kota'],
				        "kec_code" => $personal_info['kecamatan'],
				        "kel_code" => "",
				        "addrship1" => $personal_info['alamat'],
				        
				        "tel_hp1" => $personal_info['notlp'],
				        "tel_hp2" => $personal_info['notlp'],
				        "emailShip" => $personal_info['email'],
				        
						//from JNE
	        			"konot" => $jne,
	        			
						//DATA PEMBAYARAN
						"status_code" => $veritrans->status_code,
				        "payment_type" => $veritrans->payment_type,
				        "approval_code" => $veritrans->approval_code,
				        "transaction_id" => $veritrans->transaction_id,
				        "transaction_status" => $veritrans->transaction_status,
				        "gross_amount" => $veritrans->gross_amount,
				        "status_message" => $veritrans->status_message,
				        "transaction_time" => $veritrans->transaction_time,
				        "trans_id" => $veritrans->order_id,
				        
						
						//DATA PEMBAYARAN ADMINISTRASI
	        			"totpay" => $totPay,
	        			"shipment_cost" => $shipping_jne_info['price'],
	        			"admin_cost" => $admin_cost,
	        			"totbv" => 0,
	                    "signature" => array(md5($member_info['recruiterid'] . '12345'), 'string')
	                ), 'struct'
	        ));
		} else {
			$request = array(array(array(
	        			//DATA REGISTER MEMBER
	        			"userlogin" => $member_info['recruiterid'],
	        			"idsponsor" => "0000999",
                        "rekruiterid" => $member_info['recruiterid'],
	        			"membername" => $member_info['membername'],
						"noktp" => $member_info['idno'],
						"addr1" => $member_info['addr1'],
						"addr2" => $member_info['addr2'],
						"addr3" => $member_info['addr3'],
						"telhm" => "",
						"tel_hp" => $member_info['tel_hp'],
						"email" => "",
						"idstk" => $personal_info['stockist'],
						"nmstk" => $personal_info['nama_stockist'],
						"sex" => $member_info['sex'],
						//"birthdt" => $member_info['birthdt'],
						"birthdt" => $birthdt,
						"bankaccno" => $member_info['no_rek'],
						"bankaccnm" => $member_info['membername'],
						"bankcode" => $member_info['pilBank'],
						"state" => $personal_info['state'],
						"sentTo" => $personal_info['delivery'],
						"ipaddress" => $_SERVER['REMOTE_ADDR'],
						"hp_login" => $member_info['tel_hp'],
						
						//add by dion 09/09/2015
						"is_landingpage" => "1",
						//end add
						
						//TIPE VOUCHER / NON VOUCHER
						"pay_tipe" => "0",
						"voucherno" => "",
						"voucherkey" => "",
						
						//DATA STARTERKIT
						"pricecode" => $pricecode,
						"prdcd" => $starterkit_prd['prdcd'],
						"prdnm" => $starterkit_prd['prdnm'],
						"qty" => $starterkit_prd['qty'],
						"bv" => 0,
						"price" => $starterkit_prd['price'],
						
						//DATA SHIPMENT ADDRESS
						"prov_code" => $personal_info['provinsi'],
				        "kab_code" => $personal_info['kota'],
				        "kec_code" => $personal_info['kecamatan'],
				        "kel_code" => "",
				        "addrship1" => "",
				       
				        "tel_hp1" => "",
				        "tel_hp2" => "",
				        "emailShip" => "",
				        
						//from JNE
	        			"konot" => $jne,
	        			
						//DATA PEMBAYARAN
						"status_code" => $veritrans->status_code,
				        "payment_type" => $veritrans->payment_type,
				        "approval_code" => $veritrans->approval_code,
				        "transaction_id" => $veritrans->transaction_id,
				        "transaction_status" => $veritrans->transaction_status,
				        "gross_amount" => $veritrans->gross_amount,
				        "status_message" => $veritrans->status_message,
				        "transaction_time" => $veritrans->transaction_time,
				        "trans_id" => $veritrans->order_id,
				        
						
						//DATA PEMBAYARAN ADMINISTRASI
	        			"totpay" => $totPay,
	        			"shipment_cost" => $shipping_jne_info['price'],
	        			"admin_cost" => $admin_cost,
	        			"totbv" => 0,
	                    "signature" => array(md5($member_info['recruiterid'] . '12345'), 'string')
	                ), 'struct'
	        ));
		}	
		//print_r($request);
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array(
			  
              "idmember" => $arr['idmember'],
              "nmmember" => $arr['nmmember'],
              "password" => $arr['password'],
              "idsponsor" => $arr['idsponsor'],
              "nmsponsor" => $arr['nmsponsor'],
              "stk" => $arr['stk'],
              "orderno" => $arr['orderno'],
			  //"listStk" => json_decode($arr['listStk'])
			);
			return $valReturn;
        }
		
	}

	function saveECommerceTrxTest($jne) {
	    $this->xmlrpc->set_debug(TRUE);
        $personal_info = $this->session->userdata('personal_info');
		$member_info = $this->session->userdata('member_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		//$bns = substr($personal_info['bnsperiod'], 3, 7);
		//edited by dion 19/06/2015
		$birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
		//end edit
		$totPay = $starterkit_prd['price'];
		//$admin_cost = $veritrans->gross_amount - ($totPay + $shipping_jne_info['price']);
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$request = array();
		$valReturn = array();
        
        $this->xmlrpc->method('set.registerMembCommerce');
		if($personal_info['delivery'] === "2") {
	        $request = array(array(array(
	        			//DATA REGISTER MEMBER
	        			"userlogin" => $member_info['recruiterid'],
	        			"idsponsor" => "0000999",
                        "rekruiterid" => $member_info['recruiterid'],
	        			"membername" => $member_info['membername'],
						"noktp" => $member_info['idno'],
						"addr1" => $member_info['addr1'],
						/*"addr2" => $member_info['addr2'],
						"addr3" => $member_info['addr3'],*/
						"addr2" => "",
						"addr3" => "",
						"telhm" => "",
						"tel_hp" => $member_info['tel_hp'],
						"email" => "",
						"idstk" => $personal_info['stockist'],
						"nmstk" => $personal_info['nama_stockist'],
						"sex" => $member_info['sex'],
						//"birthdt" => $member_info['birthdt'],
						"birthdt" => $birthdt,
						/*"bankaccno" => $member_info['no_rek'],
						"bankaccnm" => $member_info['membername'],
						"bankcode" => $member_info['pilBank'],*/
						"bankaccno" => "",
						"bankaccnm" => "",
						"bankcode" => "",
						"state" => $personal_info['state'],
						"sentTo" => $personal_info['delivery'],
						"ipaddress" => $_SERVER['REMOTE_ADDR'],
						"hp_login" => $member_info['tel_hp'],
						//add by dion 09/09/2015
						"is_landingpage" => "1",
						//end add
						
						//TIPE VOUCHER / NON VOUCHER
						"pay_tipe" => "0",
						"voucherno" => "",
						"voucherkey" => "",
						
						//DATA STARTERKIT
						"pricecode" => $pricecode,
						"prdcd" => $starterkit_prd['prdcd'],
						"prdnm" => $starterkit_prd['prdnm'],
						"qty" => $starterkit_prd['qty'],
						"bv" => 0,
						"price" => $starterkit_prd['price'],
						
						//DATA SHIPMENT ADDRESS
						"prov_code" => $personal_info['provinsi'],
				        "kab_code" => $personal_info['kota'],
				        "kec_code" => $personal_info['kecamatan'],
				        "kel_code" => "",
				        "addrship1" => $personal_info['alamat'],
				        
				        "tel_hp1" => $personal_info['notlp'],
				        "tel_hp2" => $personal_info['notlp'],
				        "emailShip" => $personal_info['email'],
				        
						//from JNE
	        			"konot" => $jne,
	        			
						//DATA PEMBAYARAN
						"status_code" => "test",
				        "payment_type" => "test",
				        "approval_code" => "test",
				        "transaction_id" => "test",
				        "transaction_status" => "test",
				        "gross_amount" => $totPay,
				        "status_message" => "test",
				        "transaction_time" => date("Y-m-d H:i:s"),
				        "trans_id" => "test",
				        
						
						//DATA PEMBAYARAN ADMINISTRASI
	        			"totpay" => $totPay,
	        			"shipment_cost" => $shipping_jne_info['price'],
	        			"admin_cost" => $admin_cost,
	        			"totbv" => 0,
	                    "signature" => array(md5($member_info['recruiterid'] . '12345'), 'string')
	                ), 'struct'
	        ));
		} else {
			$request = array(array(array(
	        			//DATA REGISTER MEMBER
	        			"userlogin" => $member_info['recruiterid'],
	        			"idsponsor" => "0000999",
                        "rekruiterid" => $member_info['recruiterid'],
	        			"membername" => $member_info['membername'],
						"noktp" => $member_info['idno'],
						"addr1" => $member_info['addr1'],
						/*"addr2" => $member_info['addr2'],
						"addr3" => $member_info['addr3'],*/
						"addr2" => "",
						"addr3" => "",
						"telhm" => "",
						"tel_hp" => $member_info['tel_hp'],
						"email" => "",
						"idstk" => $personal_info['stockist'],
						"nmstk" => $personal_info['nama_stockist'],
						"sex" => $member_info['sex'],
						//"birthdt" => $member_info['birthdt'],
						"birthdt" => $birthdt,
						/*"bankaccno" => $member_info['no_rek'],
						"bankaccnm" => $member_info['membername'],
						"bankcode" => $member_info['pilBank'],*/
						"bankaccno" => "",
						"bankaccnm" => "",
						"bankcode" => "",
						"state" => $personal_info['state'],
						"sentTo" => $personal_info['delivery'],
						"ipaddress" => $_SERVER['REMOTE_ADDR'],
						"hp_login" => $member_info['tel_hp'],
						
						//add by dion 09/09/2015
						"is_landingpage" => "1",
						//end add
						
						//TIPE VOUCHER / NON VOUCHER
						"pay_tipe" => "0",
						"voucherno" => "",
						"voucherkey" => "",
						
						//DATA STARTERKIT
						"pricecode" => $pricecode,
						"prdcd" => $starterkit_prd['prdcd'],
						"prdnm" => $starterkit_prd['prdnm'],
						"qty" => $starterkit_prd['qty'],
						"bv" => 0,
						"price" => $starterkit_prd['price'],
						
						//DATA SHIPMENT ADDRESS
						"prov_code" => $personal_info['provinsi'],
				        "kab_code" => $personal_info['kota'],
				        "kec_code" => $personal_info['kecamatan'],
				        "kel_code" => "",
				        "addrship1" => "",
				       
				        "tel_hp1" => "",
				        "tel_hp2" => "",
				        "emailShip" => "",
				        
						//from JNE
	        			"konot" => $jne,
	        			
						//DATA PEMBAYARAN
						"status_code" => "test",
				        "payment_type" => "test",
				        "approval_code" => "test",
				        "transaction_id" => "test",
				        "transaction_status" => "test",
				        "gross_amount" => $totPay,
				        "status_message" => "test",
				        "transaction_time" => date("Y-m-d H:i:s"),
				        "trans_id" => "test",
				        
						
						//DATA PEMBAYARAN ADMINISTRASI
	        			"totpay" => $totPay,
	        			"shipment_cost" => $shipping_jne_info['price'],
	        			"admin_cost" => $admin_cost,
	        			"totbv" => 0,
	                    "signature" => array(md5($member_info['recruiterid'] . '12345'), 'string')
	                ), 'struct'
	        ));
		}	
		//print_r($request);
		$this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	$arr = $this->xmlrpc->display_response();
            //return $valReturn = json_decode($arr['arrayData']);
			$valReturn = array(
			  
              "idmember" => $arr['idmember'],
              "nmmember" => $arr['nmmember'],
              "password" => $arr['password'],
              "idsponsor" => $arr['idsponsor'],
              "nmsponsor" => $arr['nmsponsor'],
              "stk" => $arr['stk'],
              "orderno" => $arr['orderno'],
			  //"listStk" => json_decode($arr['listStk'])
			);
			return $valReturn;
        }
		
	}
	
	function generateJNENo($order_id = null) {
			
		$shipping = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		$sender_address = $this->session->userdata('sender_address');
		$destination_address = $this->session->userdata('destination_address');
		$jne_branch = $this->session->userdata('jne_branch');
		$member_info = $this->session->userdata('member_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');
		$ssx = explode(" - ", $personal_info['nama_stockist']);
		$nama_stk2 = str_replace("-", "", $ssx[1]);
		$nama_stk = substr($nama_stk2, 0, 20);	
		 	
		//$url = "http://api.jne.co.id:8889/tracing/apitest/generateCnoteTraining";
        $curl_post_data = array(
                            "username" => $this->jne_username,
                            "api_key" => $this->jne_api_key,
                            "OLSHOP_BRANCH" => $jne_branch,
                            "OLSHOP_CUST" => $this->jne_OLSHOP_CUST,
                            "OLSHOP_ORIG" => $sender_address,
                            "OLSHOP_ORDERID" => $order_id,
                            
                            /*"OLSHOP_SHIPPER_NAME" => $personal_info['nama_stockist'],
                            "OLSHOP_SHIPPER_ADDR1" => $personal_info['nama_stockist'],
                            "OLSHOP_SHIPPER_ADDR2" => $personal_info['nama_stockist'],
                            "OLSHOP_SHIPPER_ADDR3" => $personal_info['nama_stockist'],
                            "OLSHOP_SHIPPER_CITY" => $personal_info['nama_stockist'],
                            "OLSHOP_SHIPPER_REGION" => $personal_info['nama_stockist'],
                            */
                            "OLSHOP_SHIPPER_NAME" => $nama_stk,
                            "OLSHOP_SHIPPER_ADDR1" => $nama_stk,
                            "OLSHOP_SHIPPER_ADDR2" => $nama_stk,
                            "OLSHOP_SHIPPER_ADDR3" => $nama_stk,
                            "OLSHOP_SHIPPER_CITY" => $nama_stk,
                            "OLSHOP_SHIPPER_REGION" => $nama_stk,
                            
                            
                            "OLSHOP_SHIPPER_ZIP" => "12620",
                            "OLSHOP_SHIPPER_PHONE" => $personal_info['notlp'],
                            "OLSHOP_RECEIVER_NAME" => $member_info['membername'],
                            /*"OLSHOP_RECEIVER_ADDR1" => $member_info['addr1'],
                            "OLSHOP_RECEIVER_ADDR2" => $member_info['addr2'],
                            "OLSHOP_RECEIVER_ADDR3" =>$member_info['addr3'],
                            */
                            "OLSHOP_RECEIVER_ADDR1" => substr($member_info['addr1'], 0, 20),
                            "OLSHOP_RECEIVER_ADDR2" => substr($member_info['addr1'], 10, 20),
                            "OLSHOP_RECEIVER_ADDR3" => substr($member_info['addr1'], 20, 20),
							
                            "OLSHOP_RECEIVER_CITY" => $personal_info['nama_kota'],
                            "OLSHOP_RECEIVER_REGION" => $personal_info['nama_provinsi'],
                            "OLSHOP_RECEIVER_ZIP" => "12620",
                            "OLSHOP_RECEIVER_PHONE" => $personal_info['notlp'],
                            "OLSHOP_DEST" => $destination_address,
                            "OLSHOP_SERVICE" => $shipping['service_code'],
                            "OLSHOP_QTY" => $starterkit_prd['qty'],
                            "OLSHOP_WEIGHT" => $starterkit_prd['weight'], 
                           
                            "OLSHOP_GOODSTYPE" => "2",
                            "OLSHOP_GOODSDESC" => "PAKET STARTERKIT PLUS SUPLEMENT",
                            "OLSHOP_INST" => "FRAGILE",
                            "OLSHOP_GOODSVALUE" => $starterkit_prd['price'],
                            "OLSHOP_INSURANCE" => "Y"
                            );
        
		$curl = curl_init($this->jne_url_conot);
		
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
		return $curl_response;
		
		//return "CGKTES0002";
	}
}			
	
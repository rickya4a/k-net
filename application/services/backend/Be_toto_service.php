<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_toto_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_toto_model", "m_toto");
    }
	
	function getListStokist() {
		$arr = $this->m_umroh->getListStokistM();
		return $arr;
	}
	
	function getJadwalKeberangkatan() {
		$arr = $this->m_umroh->getJadwalKeberangkatanM();
		return $arr;
	}
	
	function getJadwalZiarah() {
		$arr = $this->m_umroh->getJadwalZiarahM();
		return $arr;
	}
	
	function getDistributorInfo($idmember) {
		$srvReturn = jsonFalseResponse();
		$res = $this->m_umroh->getDistributorInfo($idmember);
		if($res != null) {
			$srvReturn = jsonTrueResponse($res);
		}
		return $srvReturn;
	}
	
	function getSponsorInfo($idsponsor) {
		$srvReturn = jsonFalseResponse();
		$res = $this->m_umroh->getSponsorInfo($idsponsor);
		if($res != null) {
			$srvReturn = jsonTrueResponse($res);
		}
		return $srvReturn;
	}
	
	function getCheckNoKTP($noktp) {
		$srvReturn = jsonFalseResponse();
		$res = $this->m_umroh->getCheckNoKTP($noktp);
		if($res != null) {
			$srvReturn = jsonTrueResponse($res);
		}
		return $srvReturn;
	}
	
	function getCheckNoHP($nohp) {
		$srvReturn = jsonFalseResponse();
		$res = $this->m_umroh->getCheckNoHP($nohp);
		if($res != null) {
			$srvReturn = jsonTrueResponse($res);
		}
		return $srvReturn;
	}
	
	function checkValidationDataUmroh($dt) {
		$cekKtp = $this->m_umroh->getCheckNoKTP($dt['idno']);
        $cekHp = $this->m_umroh->getCheckNoKTP($dt['tel_hp']);
		$err = "data valid";
		$validUmur = date('Y') - $dt['thnlhr'];   
        $checkResponse =  jsonFalseResponse($err);   
        //if($cekKtp > 0 && $dt['tipeJamaah'] != '2'){
        if($cekKtp != null && $dt['tipeJamaah'] == '3'){	
            $err = "Ktp Sudah Terdaftar";
			$checkResponse = jsonFalseResponse($err);
        //} elseif($cekHp > 0){
        } elseif($cekHp != null && $dt['tipeJamaah'] == '3'){	
            $err = "No Hp Sudah Terdaftar";
			$checkResponse = jsonFalseResponse($err);
        } elseif($dt['tipeJamaah'] != '3' && $dt['idstk'] == '0'){
            $err = "Silahkan Pilih Stockist";
			$checkResponse = jsonFalseResponse($err);
        } elseif($validUmur < 18 && ($dt['statusJamaah'] == '2' ||$dt['statusJamaah'] == '3'||$dt['statusJamaah'] == '4' )){
            $err = "Umur Kurang dari 18 Tahun,jika Hubungan Keluarga Istri/Suami/Saudara";
			$checkResponse = jsonFalseResponse($err);
        } else {
        	$checkDoubleInput = $this->m_umroh->checkDoubleInputUmroh($dt);
			if($checkDoubleInput > 0 || $checkDoubleInput != null) {
				$err = "Data Umroh sudah ada, silahkan cek terlebih dahulu";
				$checkResponse = array("response" => "double", "arrayData" => $checkDoubleInput, "message" => $err);
			} else {
				
				$checkResponse = jsonTrueResponse();
			}
		}
		return 	$checkResponse;
	}
	
	function saveDataUmroh($arr) {
		/*$srvReturn = jsonFalseResponse("Simpan data umroh gagal..");	
		$sequence = $this->m_umroh->cekSeq();
        $regNo = $this->m_umroh->getRegisterNo();	
		$save = $this->m_umroh->saveDataUmroh($regNo, $sequence, $arr);
		if($save['response'] == 1) {
			$res = $this->m_umroh->getDtJamaah($regNo);
			//$sms = $this->m_umroh->sendSMSUmroh($res);
			$srvReturn = jsonTrueResponse($res, "Registrasi berhasil dengan No : $regNo");
		}
		return $srvReturn; 
		*/
		
		
		$check = $this->checkValidationDataUmroh($arr);
		if($check['response'] == "true") {
			$srvReturn = jsonFalseResponse("Simpan data umroh gagal..");		
			$sequence = $this->m_umroh->cekSeq($arr['type_ks']);
	        $regNo = $this->m_umroh->getRegisterNo($arr['type_ks']);	
			$save = $this->m_umroh->saveDataUmroh($regNo, $sequence, $arr);
			if($save['response'] == 1) {
				$res = $this->m_umroh->getDtJamaah($regNo);
				$srvReturn = jsonTrueResponse($res, "Registrasi berhasil dengan No : $regNo");
			}
		    return $srvReturn; 
		} else {
			return $check;
		}	
	}
	
	function getListUmrohReg() {
		$srvReturn = jsonFalseResponse();
		$res = $this->m_umroh->getListUmrohReg();
		if($res != null) {
			$srvReturn = jsonTrueResponse($res);
		}
		return $srvReturn;
	}
	
	function getListingRegUmroh($from, $to) {
		$srvReturn = jsonFalseResponse();
		$res = $this->m_umroh->getListingRegUmroh($from, $to);
		if($res != null) {
			$srvReturn = jsonTrueResponse($res);
		}
		return $srvReturn;
	}
	
	function getDataUmrohByRegno($regno) {
		$res = null;		
		$res = $this->m_umroh->getDataUmrohByRegno($regno);
		if($res != null) {
			$srvReturn = $res;
		}
		return $srvReturn;
	}
	

}
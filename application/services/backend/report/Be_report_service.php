<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Be_report_Service extends MY_Service{

        function __construct() {

            parent::__construct();
            $this->load->model("backend/report/Model_report_timorleste", 'm_reportTL');
        }

        function getListTL() {

            $srvReturn = jsonFalseResponse();
            $data = $this->input->post(NULL, TRUE);

            $arr = $this->m_reportTL->getReportTimorLeste($data);

            if($arr != null) {
                $srvReturn = jsonTrueResponse($arr);
            }

            return $srvReturn;
        }

        function insertStat(){
//
//            $data = $this->input->post(NULL, TRUE);
//            $param = set_list_array_to_string($data['idmemb']);
//            $mdReturn = $this->m_reportTL->insertStatTL($param);
//            return $mdReturn;

            $data = $this->input->post('idmemb');
            //print_r($data);
            foreach($data as $dt){

                //echo $dt."<br>";
                $this->m_reportTL($dt);

            }
        }
    }
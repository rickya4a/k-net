<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_warehouse_service extends MY_Service {
  
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_warehouse_model", 'm_warehouse');
    }
	
	function getWHList() {
		$res = null;
		//$res = $this->m_warehouse->getWHList();
		$res = $this->m_warehouse->getWHListPlus();
		if($res == null) {
			throw new Exception("List warehouse is empty", 1);
		} 
		return $res;
	}
	
	function getDetailProductShipping($orderno) {
		$res = null;
		$data = $this->input->post(NULL, TRUE);
		try {
			$res = $this->m_warehouse->getDetailProductShipping($orderno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $res;
	}
	
	function getShippingList() {
		$res = null;
		$data = $this->input->post(NULL, TRUE);
		try {
			$res = $this->m_warehouse->getShippingList($data);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $res;
	}
	
	function getPrintShippingSummary($form) {
		$res = null;
		//$data = $this->input->post(NULL, TRUE);
		try {
			$param = set_list_array_to_string($form['orderno']);
			$res = $this->m_warehouse->getShippingSummary($param);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $res;
	}
	
	function getListShippingLabel() {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		try {
			$srvReturn = $this->m_warehouse->getListShippingLabel($data);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $srvReturn;
	}
	
	function getListVcrByParam($isdate=0, $param1, $param2=null, $param3=null, $searchby) {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = null;
		if(($isdate == 0 && $param1 != null) || ($isdate == 0 && $param1 != "")){
			$mdReturn = $this->m_warehouse->getListVcrByParam($isdate, $param1, $param2, $param3, $searchby);	
		}elseif(($isdate == 1 && $param2 != null && $param3 != null) || ($isdate == 1 && $param2 != "" && $param3 != "")){
			$mdReturn = $this->m_warehouse->getListVcrByParam($isdate, $param1, $param2, $param3, $searchby);
		}
		//var_dump($mdReturn);
		return $mdReturn;
	}
	
	function getDetailVoucher($param) {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = null;
		
			$mdReturn = $this->m_warehouse->getDetailVoucher($param);
		
		//var_dump($mdReturn);
		return $mdReturn;
	}
	
	function getListProductForSK($orderno) {
		$mdReturn = $this->m_warehouse->getListProductForSK($orderno);
		//var_dump($mdReturn);
		return $mdReturn;
	}
	
	function getDetailProductByOrderNo($orderno) {
		$mdReturn = $this->m_warehouse->getDetailProductByOrderNo($orderno);
		//var_dump($mdReturn);
		return $mdReturn;
	}
	
	function getStarterkitBrOrderno($trcd, $orderno) {
		$mdReturn = $this->m_warehouse->getStarterkitBrOrderno($trcd, $orderno);
		//var_dump($mdReturn);
		return $mdReturn;
	}
	
	function getCheckvoucher($vchno) {
		$mdReturn = $this->m_warehouse->getCheckvoucher($vchno);
		//var_dump($mdReturn);
		return $mdReturn;
	}
	
	function getValidVoucher($vch_start, $qty) {
		$mdReturn = $this->m_warehouse->getValidVoucher($vch_start, $qty);
		//var_dump($mdReturn);
		return $mdReturn;
	}	
	
	function updateReleaseVoucher($form) {
		$mdReturn = $this->m_warehouse->updateReleaseVoucher($form);
		if($mdReturn > 0) {
			$res = jsonTrueResponse(null, "Release Voucher berhasil..");
		} else {
			$res = jsonFalseResponse("Release Voucher gagal..");
		}
		//var_dump($mdReturn);
		return $res;
	}
	
	function getReleaseVcrSKupd($vchno, $usrname, $trcd, $prdcd) {
		$mdReturn = $this->m_warehouse->getReleaseVcrSKupd($vchno, $usrname, $trcd, $prdcd);
		//var_dump($mdReturn);
		return $mdReturn;
	}	
	
	
}
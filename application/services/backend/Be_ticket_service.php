<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_ticket_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_ticket_model");
    }
	
	function getListTicketLocation() {
		$srvReturn = null;
		$srvReturn = $this->be_ticket_model->getListTicketLocation();
		return $srvReturn;
	}
	
	function getListPendingPaymentTicket() {
		$srvReturn = null;
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = $this->be_ticket_model->getListPendingPaymentTicket($data);
		return $srvReturn;
	}
	
	function getListApprovePaymentTicket($data) {
		$srvReturn = null;
		$srvReturn = $this->be_ticket_model->getListApprovePaymentTicket($data);
		return $srvReturn;
	}
	
	function approvePaymentTicket($bookingno) {
		$srvReturn = null;
		$updStatus = $this->be_ticket_model->updatePaymentByBookingNo($bookingno);
		if($updStatus > 0) {
			//$dataArray = $this->be_ticket_model->getListPaymentByBookingNo($bookingno);
			$header = $this->be_ticket_model->insertEventTransHDR($bookingno);
			$detail = $this->be_ticket_model->insertEventTransDetail($bookingno);
			if($header > 0 && $detail > 0) { 
				 
				 $dt['dtReg'] = $this->be_ticket_model->getDtRegister($bookingno);
	             $dt['det'] = $this->be_ticket_model->getDtDetail($bookingno);
	             if($dt['dtReg'] != null && $dt['dtReg'] != null) {
	                $sendSms = $this->be_ticket_model->sendSmsTiket($dt['det'],$dt['dtReg']); 
					$srvReturn = 1;
	             }
				
			}
		}
		return $srvReturn;
	}
	
	function sendSmsTiket($det, $head){
        $alternate2 = $this->load->database('alternate2', true);
        foreach($head as $row) {
            $bookNo = $row->bookingNo;
            $telhp = $row->trans_hdr_hp;
            $nmmember = $row->trans_hdr_name;
            $email = $row->trans_hdr_email;
            $eventPlace = $row->event_address;
            $eventDesc = $row->event_desc;
            $eventDate = date("d/m/Y H:i", strtotime($row->event_date));
			$eventPlace2 = $eventDesc.", date ".$eventDate; //->format('d/m/Y H:i');
        }
    
        foreach($det as $list) {
            $tiket = $list->trans_tiket_no;
            $telhape = $list->trans_det_hp;
            $nmmember = $list->trans_det_name;
            
            $textDet = 'K-LINK EVENT YOGA: Ticket Number: '.$tiket.',Nama:'.$nmmember.',Tempat : '.$eventPlace2.'';
            
            $insDet = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
	                    VALUES ('".$telhape."','".$textDet."','K-LINK Event Yoga')";
            //echo $insDet;
            //return $insDet;
            $queryDet = $alternate2->query($insDet);
        }
    }
	
	function getDetailEvent($trans_hdr_id) {
		$srvReturn = null;
		$srvReturn = $this->be_ticket_model->getDetailEvent($trans_hdr_id);
		return $srvReturn;
	}
	
	function getListEvent() {
		$srvReturn = null;
		$srvReturn = $this->be_ticket_model->getListEvent();
		return $srvReturn;
	}
	
	function getListParticipantByLocation() {
		$srvReturn = null;
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = $this->be_ticket_model->getListParticipantByLocation($data['event_id'], $data['pay_status']);
		return $srvReturn;
	}
	
	function getTicketSMSList() {
		$srvReturn = null;
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = $this->be_ticket_model->getTicketSMSList($data);
		return $srvReturn;
	}
	
	function sendTicketSMS($ticketno) {
		$srvReturn = 0;
		$dt['dtReg'] = $this->be_ticket_model->getDtRegisterByTicketNo($ticketno);
         $dt['det'] = $this->be_ticket_model->getDtDetailByTicketNo($ticketno);
         if($dt['dtReg'] != null && $dt['dtReg'] != null) {
            $sendSms = $this->be_ticket_model->sendSmsTiket($dt['det'],$dt['dtReg']); 
			$srvReturn = 1;
         }
		 return $srvReturn;
	}
}
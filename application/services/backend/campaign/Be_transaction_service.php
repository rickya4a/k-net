<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_transaction_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_transaction_model", 'm_trans');
    }
	
	function getListTransactionToPost() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTransactionToPost($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListTrxToDelete($form) {
		$srvReturn = jsonFalseResponse();	
		//$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTrxToDelete($form);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListCN() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListCN($data, $data['cnPrintStt']);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListKW() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListKW($data, $data['kwPrintStt']);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getCNReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$mdReturn = $this->m_trans->getCNReportToPrint($data);
		if($data['cnPrintStt'] == "0") {
			$param = set_list_array_to_string($data['cnno']);
			$upd = $this->m_trans->updateCNStatus($param, "1");
		}
		return $mdReturn;
	}
	
	function getKWReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$param = set_list_array_to_string($data['kwno']);
		$mdReturn = $this->m_trans->getKWReportToPrint($param);
		if($data['kwPrintStt'] == "0") {
			$upd = $this->m_trans->updateKWStatus($param, "1");
		}
		return $mdReturn;
	}
	
	function savePostingTrxSrv() {
		$srvReturn = jsonFalseResponse("Posting Transaction failed..!!");	
		$orderno = $this->input->post("orderno");
		$param = set_list_array_to_string($orderno);
		$mdReturn = $this->m_trans->savePostingTransaction($param);
		if($mdReturn > 0) {
			$srvReturn = jsonTrueResponse($mdReturn, "Posting Transaction $mdReturn records success..!!");
		} 
        return $srvReturn;
		
	}
	
	function getListTransactionReportToSMSResend() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTransactionReportToSMSResend($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListTransactionReport() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTransactionReport($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getTrxByID($id) {
		$srvReturn = jsonFalseResponse();	
		$header = $this->m_trans->getTrxHeader($id);
		$detail = $this->m_trans->getTrxDetail($header[0]->orderno);
		$payment = $this->m_trans->getTrxPayment($header[0]->orderno);
		$shipaddr = null;
		if($header[0]->sentTo == "2") {
			$shipaddr = $this->m_trans->getShipAddr($header[0]->orderno);
		} else {
			$shipaddr = $this->m_trans->getGoodReceiptStatus($header[0]->orderno);
		}
		if($header != null) {
			$srvReturn = array(
				"response" => "true", 
				"header" => $header, 
				"detail" => $detail, 
				"payment" => $payment,
				"shipaddr" => $shipaddr
		    );
		}
		return $srvReturn;
	}
	
	function getListIP() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListIP($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getIPReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$param = set_list_array_to_string($data['ipno']);
		$mdReturn = $this->m_trans->getIPReportToPrint($param);
		if($data['ipPrintStt'] == "0") {
			$upd = $this->m_trans->updateIPStatus($param, "1");
		}
		return $mdReturn;
	}
	
	function resendSMSTrx($orderno) {
		$srvReturn = jsonFalseResponse();	
		$text = "";
		$arx = substr($orderno, 0, 1);
		if($arx == "I") {
			$dta = $this->m_trans->getTrxShopOL($orderno);
			if($dta[0]->sentTo == "1") {
				$ship = 'Kode Ambil Brg: '.$dta[0]->secno;
			} else {
				$ship = 'Conote: '.$dta[0]->conoteJNE;
			}			
			$text = 'K-LINK : Trx sukses,No Order: '.$orderno.',Jumlah Trx: '.$dta[0]->total_pay.','.$ship.'';	
			
			//print_r($dta);
		} else {
			$dta = $this->m_trans->getTrxInputMember($orderno);
			if($dta[0]->sentTo == "1") {
				$ship = 'Kode Ambil Brg: '.$dta[0]->secno;
			} else {
				$ship = 'Conote: '.$dta[0]->conoteJNE;
			}		
			$text = 'K-LINK : Trx sukses,No Order: '.$orderno.', Idmember : '.$dta[0]->memberid.',Password : '.$dta[0]->password.','.$ship.'';
		}
		$send = $this->m_trans->resendSMSNotification($dta[0]->tel_hp, $text);
		if($send > 0) {
			$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");	
		}
		return $srvReturn; 
		
	}
	
	function getListJNEConote($param, $value) {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListJNEConote($data, $param, $value);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListConoteToEmail() {
		$data = $this->input->post(NULL, TRUE);
		
		$tanggal = date("Y-m-d", strtotime($data['datepick']));
		$pickup_datetime = $tanggal." ".$data['jam'].":".$data['menit'].":00";
		$pickup_datetime = date("Y-m-d h:i:s", strtotime($pickup_datetime));
		
		$res = $this->m_trans->getListEmailToSend();
		$char = "";
		if($res == null) {
			$char = setErrorMessage();
		} else {
			foreach($res as $data) {
				$char .= "<table border=1 width=100%><tr><td width='35%'>Nama Perusahaan</td><td >$data->CP_NAME</td></tr>";
						$char .= "<tr><td>Nomor Account</td><td>$data->NATIONAL_ACCOUNT</td></tr>";
						$char .= "<tr><td>Alamat Pick Up</td><td>$data->ADDR</td></tr>";
						$char .= "<tr><td>Area</td><td>$data->area</td></tr>";
						$char .= "<tr><td>PIC</td><td>$data->PIC</td></tr>";
						$char .= "<tr><td>Telepon</td><td>$data->TEL_NO</td></tr>";
						$char .= "<tr><td>Paket / Dokumen</td><td>$data->PACKAGE</td></tr>";
						$char .= "<tr><td>Qty / Pcs</td><td>$data->QTY</td></tr>";
						$char .= "<tr><td>Ukuran Paket</td><td>$data->DIMENS</td></tr>";
						$char .= "<tr><td>Service ( SS, YES, REG )</td><td>$data->SERVICE_TYPE</td></tr>";
						$char .= "<tr><td>Armada (mobil / motor)</td><td>$data->PICKUP_VHC</td></tr>";
						$char .= "<tr><td>Waktu Pick Up</td><td>$pickup_datetime</td></tr>";
						$char .= "<tr><td>Kota Tujuan</td><td>$data->DEST_CITY</td></tr>";
						$char .= "<tr><td>Keterangan lain-lain</td><td>$data->REMARK</td></tr>";
						$char .= "</table>";
			}	
        }
		return $char;
	}
	function getListPickUP() {
		$res = $this->m_trans->getListPickUP();
		return $res;
	}
	
	function sendEmailConote($data) {
			//var_dump($data);
			$tanggal = date("Y-m-d", strtotime($data['datepick']));
			$pickup_datetime = $tanggal." ".$data['jam'].":".$data['menit'].":00";
			$pickup_datetime = date("Y-m-d h:i:s", strtotime($pickup_datetime));
			//echo $pickup_datetime;
			$res = $this->m_trans->getListEmailToSend();
			if($res > 0) {
				//$char = "";
				
				$char = "<html>
						<head>
						  <title>Pengiriman List Conot</title>
						</head>
						
						<body>";
				foreach($res as $data) {
					//$char .= "";
					$char .= "<table border=1><tr><td width='35%'>Nama Perusahaan</td><td >$data->CP_NAME</td></tr>";
					$char .= "<tr><td>Nomor Account</td><td>$data->NATIONAL_ACCOUNT</td></tr>";
					$char .= "<tr><td>Alamat Pick Up</td><td>$data->ADDR</td></tr>";
					$char .= "<tr><td>Area</td><td>$data->area</td></tr>";
					$char .= "<tr><td>PIC</td><td>$data->PIC</td></tr>";
					$char .= "<tr><td>Telepon</td><td>$data->TEL_NO</td></tr>";
					$char .= "<tr><td>Paket / Dokumen</td><td>$data->PACKAGE</td></tr>";
					$char .= "<tr><td>Qty / Pcs</td><td>$data->QTY</td></tr>";
					$char .= "<tr><td>Ukuran Paket</td><td>$data->DIMENS</td></tr>";
					$char .= "<tr><td>Service ( SS, YES, REG )</td><td>$data->SERVICE_TYPE</td></tr>";
					$char .= "<tr><td>Armada (mobil / motor)</td><td>$data->PICKUP_VHC</td></tr>";
					$char .= "<tr><td>Waktu Pick Up</td><td>$pickup_datetime</td></tr>";
					$char .= "<tr><td>Kota Tujuan</td><td>$data->DEST_CITY</td></tr>";
					$char .= "<tr><td>Keterangan lain-lain</td><td>$data->REMARK</td></tr>";
					$char .= "</table><br />";
					//$char .= "<tr><td colspan=2>&nbsp;</td></tr>";
				//} //update hilal 2016-01-02
					$char .= "</body></html>";
					
					//echo $char;
					
					$this->load->library('email');
					
					/*===================================EDIT HILAL 2015-12-19===================================*/
					
					/*
					$this->load->library('email');
					 
					//$this->email->from('dionrespati@gmail.com', 'Dion R');
					$this->email->from('knet.klink@gmail.com', 'PT. K-Link Nusantara');
					//$this->email->to('pickup-admin@k-link.co.id');
					$this->email->to('adm.csfrontline@jne.co.id');
					$this->email->cc('salessupport4@jne.co.id,handrian.purba@jne.co.id,dionrespati@gmail.com,oshiro_gery@hotmail.com,hilal.lucky@gmail.com,nandang.klink@gmail.com,nandang.klink@yahoo.com,hilal@k-link.co.id,supportk-link@k-link.co.id');
					$this->email->bcc('hilal@klink.co.id');
					$this->email->set_mailtype("html");
					$this->email->subject("List Pengiriman K-LINK");
					$this->email->message($char);
					 */
					 
					$resEmail = $this->m_trans->getConfigEmail('Req. Pickup');
					if($resEmail > 0) {
						$sendTo = "";
						$sendCc = "";
						$sendBcc = "";
						
						$senderEmail = "";
						$senderPassword = "";
						$senderAlias = "";
						$senderSmtp = "";
						$senderSmtpPort = "";
						
						foreach($resEmail as $data) {
							$flag_sender 	= $data->flag_sender;
							$flag_to 		= $data->flag_to;
							$flag_cc 		= $data->flag_cc;
							$flag_bcc 		= $data->flag_bcc;
							
							if($flag_sender == "1"){
								$senderEmail 	= $data->email;
								$senderPassword = $data->password;
								$senderAlias 	= $data->alias_name;
								$senderSmtp 	= $data->smtp;
								$senderSmtpPort = $data->smtp_port;
							}
							
							if($flag_to == "1"){
								$sendTo = $sendTo.$data->email.',';
							}
							
							if($flag_cc == "1"){
								$sendCc = $sendCc.$data->email.',';
							}
							
							if($flag_bcc == "1"){
								$sendBcc = $sendBcc.$data->email.',';
							}
						}
						$sendTo = substr($sendTo, 0, -1);
						$sendCc = substr($sendCc, 0, -1);
						$sendBcc = substr($sendBcc, 0, -1);
						
						/*
						echo "To nya adalah = $sendTo<br />";
						echo "CC nya adalah = $sendCc<br />";
						echo "BCC adalah = $sendBcc<br />";
						
						echo "Sender Email adalah $senderEmail<br />";
						echo "Sender Password adalah $senderPassword<br />";
						echo "Sender Alias adalah $senderAlias<br />";
						echo "Sender SMTP adalah $senderSmtp<br />";
						echo "Sender PORT adalah $senderSmtpPort<br />";
						*/
						
					    $config = Array(
									    'protocol' 	=> 'smtp',
									    'smtp_host' => $senderSmtp,
									    'smtp_port' => $senderSmtpPort,
									    'smtp_user' => $senderEmail,
									    'smtp_pass' => $senderPassword,
									    'mailtype'  => 'text', 
									    'charset'   => 'utf-8',
									    'wordwrap' 	=> TRUE,
									    'newline' 	=> '\r\n',
									);
						$this->load->library('email', $config);
						//var_dump($config);
						
						//kirim email
						$this->email->from($senderEmail, $senderAlias);
						$this->email->to($sendTo);
						$this->email->cc($sendCc);
						$this->email->bcc($sendBcc);
						$this->email->set_mailtype("html");
						$this->email->subject("List Pengiriman K-LINK");
						$this->email->message($char);
						/*===================================END EDIT HILAL 2015-12-19=================================*/
						
						//ori**
						if ($this->email->send()){
				            echo "<div class='alert-success'>Email sent successfully..!!</div>"; 
				            foreach($res as $data) {
				            	$arr = $this->m_trans->updateFlagSendConote($data->ORDERNO, $pickup_datetime);  
							}	
				            //$arr = $this->m_trans->updateFlagSendConote($data->ORDERNO, $pickup_datetime);  
				        }else{
				            echo "<div class='alert-error'>There is error in sending mail!</div>";
							echo $this->email->print_debugger();
				        }
				        
						//end ori**
						
					} else {
						 echo "<div class='alert-error'>Please Set Email First.</div>";
					}
			}//update hilal 2016-01-02
			//here for ori**
			} else {
				 echo "<div class='alert-error'>Empty Record</div>";
			}
	
	}

	function getDetailPickUpProduct($id) {
		$srvReturn = null;	
		$mdReturn = $this->m_trans->getDetailPickUpProduct($id);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getListShippingLabel() {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = $this->m_trans->getListShippingLabel($data['prt_form'], $data['prt_to'], $data['print_count']);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getDetailProductByOrderNo($orderno) {
		$srvReturn = null;
		$mdReturn = $this->m_trans->getDetailProductByOrderNo($orderno);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getPrintShippingPDF($orderno) {
		$srvReturn = null;
		//$data = $this->input->post(NULL, TRUE);
		//$param = set_list_array_to_string($data['orderno']);
		$param = set_list_array_to_string($orderno);
		$mdReturn = $this->m_trans->printLabelShipping($param);
		if($mdReturn !=  null) {
			$upd = $this->m_trans->updateStatusPrintShipping($param);
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function deleteTransaction($orderid) {
		$srvReturn = jsonFalseResponse("$orderid failed to be deleted...");
		$mdReturn = $this->m_trans->deleteTransaction($orderid);
		if($srvReturn >  0 || $srvReturn == null) {
			$delMemb = $this->m_trans->deleteTrxIfMember($orderid);
			$srvReturn = jsonTrueResponse(NULL, "$orderid deleted successfully..");	
		}
		return $srvReturn;
		
	}
	
	function saveImportToDB($data) {
		$srvReturn = $this->m_trans->saveImportToDB($data);
		return $srvReturn;
	}
	
	function getListSGOTrx($data) {
		$srvReturn = $this->m_trans->getListSGOTrx($data);
		return $srvReturn;
	}
	
	function saveTrxToDB($orderid) {
		//$srvReturn = $this->m_trans->saveTrxToDB($orderid);
		//return $srvReturn;
		
		$srvReturn = jsonFalseResponse("$orderid failed to reconcile...");
		$mdReturn = $this->m_trans->saveTrxToDB($orderid);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = jsonTrueResponse(NULL, "$orderid reconcile successfully..");	
		}
		return $srvReturn;
	}
	
	function saveMEMToDB($memb_status, $memberid, $orderid) {
		$srvReturn = jsonFalseResponse("$orderid failed to reconcile...");
		$mdReturn = $this->m_trans->saveMEMToDB($memb_status, $memberid, $orderid);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = jsonTrueResponse(NULL, "$orderid -- $memberid reconcile successfully..");	
		}
		return $srvReturn;
	}
	
	function getListBankCharge() {
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->getListBankCharge();
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListBankChargeByParam($param, $value) {
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->getListBankChargeByParam($param, $value);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function postSaveBankCharge($data) {
		$srvReturn = setErrorMessage("Insert Bank Charge failed..!");	
		$arr = $this->m_trans->saveBankCharge($data);
		if($arr > 0) {
			$srvReturn = setSuccessMessage("Insert Bank Charge success..!");
		}
		return $srvReturn;
	}
	
	function postUpdateBankCharge($data) {
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->updateBankCharge($data);
		if($arr > 0) {
			$srvReturn = jsonTrueResponse(NULL, "Bank Charge $data[bankDisplayNm] successfully updated..!");
		}
		return $srvReturn;
	}
	
	function getListVoucherByParam($searchby, $param1, $param2=null, $param3=null) {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = $this->m_trans->getListVoucherByParam($searchby, $param1, $param2, $param3);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getCheckConoteIsNULL($orderno) {
		$srvReturn = null;
		$mdReturn = $this->m_trans->getCheckConoteIsNULL($orderno);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function postUpdateCnoteByOrderno() {
		$data = $this->input->post(NULL, TRUE);
		//var_dump($data);
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->updateCnoteByOrderno($data);
		if($arr > 0) {
			$srvReturn = array('message' => "Conote for $data[orderno] successfully updated..!", 
				               'response' => "true",
				               'conoteno' => "$data[conoteno]"); //jsonTrueResponse(NULL, "Conote for $data[orderno] successfully updated..!");
		} 
		return $srvReturn;
	}
}
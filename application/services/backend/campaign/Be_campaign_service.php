<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_campaign_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/campaign/be_campaign_model");
    }
	
	 /*------------------------
	 * PRODUCT
	 -------------------------*/
	 
	 function listAllCampaignService($noexception = true) {
	 	$modelReturn = null;
		if($noexception) {
			try {
				$modelReturn = $this->be_campaign_model->getlistAllCampaign($noexception);		
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);		
			}	
		} else {
			$modelReturn = $this->be_campaign_model->getlistAllCampaign($noexception);
		}
		return $modelReturn;
	 }
	 
	 function listCampaignService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_campaign_model->getListCampaign($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function listCampaignServiceByID($param, $noexception = true) {
		$srvReturn = null;
		if($noexception) {
			try {
				$srvReturn = $this->be_campaign_model->getDetCampaign('id', $param, $noexception);
				//$srvReturn = jsonTrueResponse($modelReturn);
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);		
			}	
		} else {
			$srvReturn = $this->be_campaign_model->getDetCampaign('id', $param, $noexception);
		}
		return $srvReturn;
	 }
	 
	 function inputCampaignService() {
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_campaign_model->uploadCampaign(); //"test"; //
			$srvReturn = jsonTrueResponse($modelReturn, "Upload Campaign Success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function updateProductService() {
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->updateProduct();
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function deleteProductService($id) {
	 	$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("cat_inv_id", "master_prd_pricetab", $id);	
		   $modelReturn = $this->be_product_model->deleteProduct($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Product success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	 }
	 
	 
}
	
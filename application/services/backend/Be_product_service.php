<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_product_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_product_model");
    }
	
	/*--------------------
	 * PRODUCT CATEGORY
	 *-------------------*/
	
	function listUserProductCatService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListProductCat($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputProductCatService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->saveInputProductCat();
			$srvReturn = jsonTrueResponse($modelReturn, "Input Product Category Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function listAllProductCat() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListAllProductCat();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function updateProductCatService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->saveUpdateProductCat();
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Category Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteProductCatService($id) {
		$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("cat_id", "master_prd_cat_inv", $id);	
		   $modelReturn = $this->be_product_model->deleteProductCat($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Product Cat success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
	
	
	/*------------------------
	 * PRICE CODE
	 -------------------------*/
	 function listPriceCodeService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListPriceCode($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	 
	 
	 function listAllPriceCodeService() {
	 	$modelReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListAllPriceCode();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	 }
	 
	 function inputPriceCodeService() {
	 	$data = $this->input->post(NULL, TRUE);
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->savePriceCode($data);
			$modelReturn = $this->be_product_model->savePriceCodeKlinkMlm($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Input Price Code Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function updatePriceCodeService() {
	 	$data = $this->input->post(NULL, TRUE);
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->updatePriceCode($data);
			$modelReturn = $this->be_product_model->updatePriceCodeKlinkMlm($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Price Code Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function deletePriceCodeService($id) {
	 	$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("pricecode", "master_prd_pricetab", $id);	
		   $modelReturn = $this->be_product_model->deletePriceCode($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Price Code success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	 }
	 
	 /*------------------------
	 * PRODUCT
	 -------------------------*/
	 
	 function listAllProductService($noexception = true) {
	 	$modelReturn = null;
		if($noexception) {
			try {
				$modelReturn = $this->be_product_model->getlistAllProduct($noexception);		
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);		
			}	
		} else {
			$modelReturn = $this->be_product_model->getlistAllProduct($noexception);
		}
		return $modelReturn;
	 }
	function listAllBannerService($noexception = true) {
		$modelReturn = null;
		if($noexception) {
			try {
				$modelReturn = $this->be_product_model->getlistAllBanner($noexception);
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);
			}
		} else {
			$modelReturn = $this->be_product_model->getlistAllBanner($noexception);
		}
		return $modelReturn;
	}

	 function listProductService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListProduct($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	function listBannerService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListBanner($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $srvReturn;
	}
	 function inputProductService() {
	 	$data = $this->input->post(NULL, TRUE);
	 	$srvReturn = null;
		try {

			$modelReturn = $this->be_product_model->inputProduct($data);
			$modelReturn = $this->be_product_model->inputProductKlinkMlm($data);
			$modelReturn = $this->be_product_model->inputProductWMS($data);

			
			$srvReturn = jsonTrueResponse($modelReturn, "Input Product Success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	function inputBannerService() {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		try {

			$modelReturn = $this->be_product_model->inputBanner($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Input Banner Success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $srvReturn;
	}
	 function updateProductService() {
	 	$data = $this->input->post(NULL, TRUE);
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->updateProduct($data);
			$modelReturn = $this->be_product_model->updateProductKlinkMlm($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	function updateBannerService() {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->updateBanner($data);
//			$modelReturn = $this->be_product_model->updateProductKlinkMlm($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Banner Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);
		}
		return $srvReturn;
	}
	 function deleteProductService($id) {
	 	$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("cat_inv_id", "master_prd_pricetab", $id);	
		   $modelReturn = $this->be_product_model->deleteProduct($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Product success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	 }
	 
	 /*-------------------------
	  * PRODUCT PRICE
	  *--------------------------/*
	  */ 
	  function checkProductPriceCodeService() {
	  	 $data = $this->input->post(NULL, TRUE);	
	  	 $srvReturn = null;
		 try {
		   $checkMasterPrd = $this->be_product_model->getListProduct("cat_inv_id",  $data['cat_inv_id']);	
		   $checkPriceCodeByProdID = $this->be_product_model->showProductPriceCode($data);
		   if($checkPriceCodeByProdID == null) {
		   	   $res = $this->be_product_model->getListPriceCode("status", "1"); 
			   $srvReturn = array("response" => "true", "message" => "2", "arrayData" => $res, "prdnm" => $checkMasterPrd[0]->cat_inv_desc);
		   } else {
			   $srvReturn = array("response" => "true", "message" => "1", "arrayData" => $checkPriceCodeByProdID, "prdnm" => $checkMasterPrd[0]->cat_inv_desc);
		   } 
		   
		 } catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		 }	
		 return $srvReturn;
	  }
	  
	  function inputProductPriceService() {
	  	 $data = $this->input->post(NULL, TRUE);	
	  	 $srvReturn = null;
		 try {
			$modelReturn = $this->be_product_model->saveProductPrice($data);
			$this->be_product_model->saveproductPriceKlinkMLM($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Input Product Price Success..!!");
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		 return $srvReturn;
	  }
	  
	  function updateProductPriceService() {
	  	 $data = $this->input->post(NULL, TRUE);	
	  	 $srvReturn = null;
		 try {
			$del = $this->be_product_model->deleteProductPrice($data);
			$modelReturn = $this->be_product_model->saveProductPrice($data);
			$del = $this->be_product_model->deleteProductPriceKlinkMlm($data);
			$modelReturn = $this->be_product_model->saveProductPriceKlinkMlm($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Price Success..!!");
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		 return $srvReturn;
	  }
	  
	  function deleteProductPriceService($id) {
	  	  
	  }
	  
	  function getProductClaim() {
	  	 $data = $this->input->post(NULL, TRUE);
		 $srvReturn = null;
		 try {
		 	$res = $this->be_product_model->getProductClaim($data);
			$srvReturn = jsonTrueResponse($res);
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		return $srvReturn;
	  }
	  
	  function getProductClaimDetail($id) {
	  	//$data = $this->input->post(NULL, TRUE);
		 $srvReturn = null;
		 try {
		 	$res = $this->be_product_model->getProductClaimDetail($id);
			$srvReturn = jsonTrueResponse($res);
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		return $srvReturn;
	  }
	 
	  /*-----------------------
	   * PRODUCT BUNDLING
	   * ---------------------*/
	   function getListAllProductBundling() {
	   	  $res = $this->be_product_model->getListAllProductBundling();
		  return $res;
	   }
	   
	   function getListProductBundlingByID($id) {
	   	  $srvReturn = jsonFalseResponse("Data product bundling $id is empty..");
	   	  $header = $this->be_product_model->getListPrdBundlingHeader($id);
		  //$srvReturn = array("response" => "fa", "header" => $header, "detail" => $detail, "detailKlink" => $detKlinkMlm);  
		  if($header != null) {
		  	$detail = $this->be_product_model->getListPrdBundlingDetail($id);
			$detKlinkMlm = $this->be_product_model->getListPrdBundlingDetailKlinkMlm($id);
			/*if($detail != null) {
				$arrDet = array("status" => "1", "detail" => $detail);
			} else {
				$detKlinkMlm = $this->be_product_model->getListPrdBundlingDetailKlinkMlm($id);
				if($detKlinkMlm != null) {
					$arrDet = array("status" => "2", "detail" => $detKlinkMlm);
				} else {
					$arrDet = array("status" => "0", "detail" => null);
				}
			}*/
			$srvReturn = array("response" => "true", "header" => $header, "detail" => $detail, "detailKlink" => $detKlinkMlm);  
		  }
		  return $srvReturn;
	   }
	   
	   function getListProductBundlingById_v2($id) {
	   	  $srvReturn = jsonFalseResponse("Data product bundling $id is empty..");
	   	  $header = $this->be_product_model->getListPrdBundlingHeader($id);
		  //print_r($header);
		  //$srvReturn = array("response" => "fa", "header" => $header, "detail" => $detail, "detailKlink" => $detKlinkMlm);  
		  if($header != null) {
		  	$detail = $this->be_product_model->getListPrdBundlingDetail($id);
			$detKlinkMlm = $this->be_product_model->getListPrdBundlingDetailKlinkMlm($id);
			//print_r($detKlinkMlm);
			//$detKlinkMlm2 = array_map('utf8_encode', $detKlinkMlm);
			//$detail = null;
			//$detKlinkMlm = null;
			/*if($detail != null) {
				$arrDet = array("status" => "1", "detail" => $detail);
			} else {
				$detKlinkMlm = $this->be_product_model->getListPrdBundlingDetailKlinkMlm($id);
				if($detKlinkMlm != null) {
					$arrDet = array("status" => "2", "detail" => $detKlinkMlm);
				} else {
					$arrDet = array("status" => "0", "detail" => null);
				}
			}*/
			$srvReturn = array("response" => "true", "header" => $header, "detail" => $detail, "detailKlink" => $detKlinkMlm);  
		  } else {
			  $srvReturn = jsonFalseResponse("Data bundling $id is empty..");
		  }
		  return $srvReturn;
	   }
	   
	   function getListProductBundling($param, $id) {
	   	  $srvReturn = jsonFalseResponse("Double input parameter");
	   	  $res = $this->be_product_model->getListProductBundling($param, $id);
		  if($res > 0) {
		  	$srvReturn = jsonTrueResponse($res);
		  } 
		  return $srvReturn;
	   }
	   
	   function saveInputProductBundling() {
	   	  $data = $this->input->post(NULL, TRUE);
		  $srvReturn = null;
		  $mdReturn = null;
		 try {
		 	if($data['db_comm'] == "0") {
		 		$mdReturn = $this->be_product_model->saveInputProductBundlingDetail($data);
				$mdReturn = $this->be_product_model->saveInputProductBundlingPricetab($data);
			    
			    if($data['db_klink'] == "0") {
			    	$mdReturn = $this->be_product_model->deleteProductBundlingKlinkMLM($data);
			    	$mdReturn = $this->be_product_model->inputProductBundlingKlinkMLM($data);
				}		
		 	} else {
		 		//if($data['db_comm'])	
		 		if($data['db_klink'] == "0") {
			    	$mdReturn = $this->be_product_model->inputProductBundlingKlinkMLM($data);
				}
		 	}

			$mdReturn = $this->be_product_model->saveInputProductBundlingDetail($data);
			$mdReturn = $this->be_product_model->saveInputProductBundlingPricetab($data);
		  	$mdReturn = $this->be_product_model->deleteProductBundlingKlinkMLM($data);
		  	$mdReturn = $this->be_product_model->inputProductBundlingKlinkMLM($data);
			 $mdReturn = $this->be_product_model->updateWeight($data);
			 $srvReturn = jsonTrueResponse($mdReturn, "Save Product Bundling success..");
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		return $srvReturn;
	   }
	   
	   function saveUpdateProductBundling() {
	   	   $data = $this->input->post(NULL, TRUE);
		   $srvReturn = null;
		   try {
		   	    $mdReturn = $this->be_product_model->saveInputProductBundlingDetail($data);
				$mdReturn = $this->be_product_model->saveInputProductBundlingPricetab($data);
			  	$mdReturn = $this->be_product_model->deleteProductBundlingKlinkMLM($data);
			    $mdReturn = $this->be_product_model->inputProductBundlingKlinkMLM($data);
			    $mdReturn = $this->be_product_model->updateWeight($data);
				$srvReturn = jsonTrueResponse(null, "Update Product Bundling success..");
		   } catch(Exception $e) {
				$srvReturn = $this->jsonFalseResponse($e->getMessage());
				throw new Exception($e->getMessage(), 1);		
		   }	
			return $srvReturn;
	   }
	   
	 /*----------------------------------------------
	 * PRODUCT IMPORT FROM PINEAPPLE TO DB COMMERCE
	 ----------------------------------------------*/
	 
	 function getListProductPineByID($id) {
	 	$srvReturn = null;
		 try {
		 	//cek apakah produk sudah ada di tabel master_prd_cat_inv pada DB_ECOMMERCE
		 	$arr = array(
				"fieldName" => "cat_inv_id",
				"tableName" => "master_prd_cat_inv",
				"paramValue" => $id,
				"db" => "db_ecommerce"
			);
		 	if($this->be_product_model->ifRecordExist($arr)) {
		 		throw new Exception("Product with code $id is already exist in data e-commerce..", 1);
		 	}
			$header = $this->be_product_model->getHeaderPrdPine($id);
		 	$detail = $this->be_product_model->getListPrdBundlingDetailKlinkMlm($id);
			$category = $this->be_product_model->getListAllProductCat();
			$srvReturn = array("response" => "true", "header" => $header, "detail" => $detail, "category" => $category);
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		return $srvReturn;
	 }
	 
	 function saveImportProductFromPine($data) {
	 	$srvReturn = null;
		 try {
		 	$mdReturn = $this->be_product_model->importProductFromPine($data);
			$mdReturn = $this->be_product_model->importPricetabFromPine($data);
		 	if($data['inv_type'] == "B") {
		 		$mdReturn = $this->be_product_model->importProductBundlingDetail($data);		
		 	} 
			$srvReturn = jsonTrueResponse($mdReturn, "Import data product $data[head_prdcd] to DB E-Commerce success..");
		
			 
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		return $srvReturn;
	 }

	function cekStokist($loccd) {
		$mdReturn = $this->be_product_model->prosesCekStokist($loccd);
		return $mdReturn;
	}

	function cekNonAvailable($loccd,$prdcd) {
		$mdReturn = $this->be_product_model->prosesCekNonAvailable($loccd,$prdcd);
		return $mdReturn;
	}

	function cekProduct($prdcd) {
		$mdReturn = $this->be_product_model->prosesCekProduct($prdcd);
		return $mdReturn;
	}

	function saveNonAvailable($loccd,$prdcd,$prdnm) {
		$mdReturn = $this->be_product_model->prosesSaveNonAvailable($loccd,$prdcd,$prdnm);
		return $mdReturn;
	}

	function updtNonAvailable($prdcd) {
		$mdReturn = $this->be_product_model->prosesUpdtNonAvailable($prdcd);
		return $mdReturn;
	}

	function delNonAvailable($loccd,$prdcd,$prdnm) {
		$mdReturn = $this->be_product_model->prosesDelNonAvailable($loccd,$prdcd,$prdnm);
		return $mdReturn;
	}

	function updtNonAvailable2($prdcd) {
		$mdReturn = $this->be_product_model->prosesUpdtNonAvailable2($prdcd);
		return $mdReturn;
	}

	function listAllProductService2($noexception = true) {
		$modelReturn = null;
		if($noexception) {
			try {
				$modelReturn = $this->be_product_model->getlistAllProduct2($noexception);
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);
			}
		} else {
			$modelReturn = $this->be_product_model->getlistAllProduct2($noexception);
		}
		return $modelReturn;
	}
}
	
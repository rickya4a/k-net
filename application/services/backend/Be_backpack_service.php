<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_backpack_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/stockist/be_backpack_model", "m_backpack");
    }
	
	function getStockistInfoSrv($idstk) {
		$srvReturn = jsonFalseResponse("Invalid Stockist ID");
		$arr = $this->m_backpack->getStockistInfo($idstk);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getMemberInfoSrv($idmemb) {
		$srvReturn = jsonFalseResponse("Invalid Member ID");
		$arr = $this->m_backpack->getMemberInfo($idmemb);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function saveTrfBackpack($form) {
		$srvReturn = jsonFalseResponse("Save Transfer Backpack failed..");
		$arr = $this->m_backpack->saveTrfBackpack($form);
		if($arr != null) {
			//$srvReturn = jsonTrueResponse($arr, "Save Transfer Backpack success..");
			$srvReturn = array("response" => "true", "id" => $arr['id'], "message" => "Save Transfer Backpack success..");
		}
		return $srvReturn;
	}
	
	function getReportTrfBackpackSrv($form) {
		$srvReturn = jsonFalseResponse("No result found,try another parameter");
		$arr = $this->m_backpack->getReportTrfBackpack($form);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function updateStatusPakai($form) {
		$srvReturn = jsonFalseResponse("Update Status failed..");
		$arr = $this->m_backpack->updateStatusPakai($form);
		if($arr > 0) {
			$srvReturn = jsonTrueResponse(null, "Update Status success..");
		}
		return $srvReturn;
	}
	
	function getDetailTrf($id) {
		$srvReturn = jsonFalseResponse("No Record found");
		$arr = $this->m_backpack->getDetailTrf($id);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_pulsa_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_pulsa_model", "m_pulsa");
    }
	
	function searchTrxPulsaByDate($data) {
		$srvReturn = null; 
		try {			
			$srvReturn = $this->m_pulsa->searchTrxPulsaByDate($data['tgl_trans'], $data['tgl_trans']);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return $srvReturn;
	}
	
	function listSearchTrxPulsa($form) {
		$srvReturn = null;
		if($form['search_type'] == "hutang") {
			$srvReturn = $this->m_pulsa->listTrxOnDebt("L");
		} elseif($form['search_type'] == "hutang_tanto") {
			$srvReturn = $this->m_pulsa->listTrxOnDebt("V");
		} elseif($form['search_type'] == "cust") {
			$srvReturn = $this->m_pulsa->listCellPhoneByCustName($form['param']);
		} elseif($form['search_type'] == "trx_client") {
			$srvReturn = $this->m_pulsa->listTrxByClient($form['param'], $form['from'], $form['to']);
		} else {
			$srvReturn = $this->m_pulsa->listTrxByPhoneNumber($form['param'], $form['from'], $form['to']);
		}	 
		return $srvReturn;
	}
	
	function updatePayDebtClient($order_type, $op, $value) {
		$srvReturn = jsonFalseResponse();	
		//if(isset($data['']))
		if($op == "=") {
		   $mdReturn = $this->m_pulsa->updatePayDebtClient($order_type, $op, $value);	
		} else {
		   $value = $this->input->post(NULL, TRUE);
		   $param = set_list_array_to_string($value['client']);	
		   $mdReturn = $this->m_pulsa->updatePayDebtClient($order_type, "IN", $param);
		}
		
		if($mdReturn > 0) {
			//$res = $this->m_pulsa->listTrxOnDebt($order_type);	
			$srvReturn = jsonTrueResponse(null, "Update Client Debt success..!!");
		}
		
		return $srvReturn;
	}
	
	function updatePayDebtSelected($op, $value) {
		$srvReturn = jsonFalseResponse("Salah..");	
		//if(isset($data['']))
		if($op == "=") {
		   $mdReturn = $this->m_pulsa->updatePayDebtSelected($op, $value);	
		} else {
		   $value = $this->input->post(NULL, TRUE);
		   $param = set_list_array_to_string($value['idtrx'], "");	
		   $mdReturn = $this->m_pulsa->updatePayDebtSelected("IN", $param);
		}
		
		if($mdReturn > 0) {
			//$res = $this->m_pulsa->listTrxOnDebt($order_type);	
			$srvReturn = jsonTrueResponse(null, "Update Client Debt success..!!");
		}
		
		return $srvReturn;
	}
	
	function detailClientDebt($client_name, $order_lgs) {
		$srvReturn = null;
		$srvReturn = $this->m_pulsa->detailClientDebt($client_name, $order_lgs);
		return $srvReturn;
	}
	
	function processTrxByFullcode($fullcode) {
		$srvReturn = jsonFalseResponse("Trx Code is already exist..!!");	
		$trx = explode(" ", $fullcode);
		$harga_beli = str_replace(",", "", $trx[1]) * -1;
		$harga_beli = str_replace(".", "", $trx[1]) * -1;
		$jam = $trx[0].":00";
		$check = $this->m_pulsa->checkCodePulsa($fullcode);
		if($check == null) {
			$harga = 0;
			$secondDelimiter = explode(".", $trx[2]);
			$kode = $secondDelimiter[0];
			$phoneNumber = $secondDelimiter[1];
			
			//Check Price, add new if doesn't exist
			$price = $this->m_pulsa->checkPriceByCode($kode);
			if($price == null) {
				$harga = $this->setHarga($kode, $harga_beli);
			} else {
				$harga = $price[0]->harga_jual;
			}
			
			//Check client By Phone Number
			 $resClient = $this->m_pulsa->getClientNameByPhoneNumber($phoneNumber);
			 if($resClient == null) {
				$client = "";
			 } else {
				$client = $resClient[0]->client;
			 }
			 
			 $srvReturn = array("response" => "true", "jam" => $jam, "client" => $client, "fullcode" => $fullcode, "kode" => $kode, "harga_beli" => $harga_beli, "price" => $harga, "phone" => $phoneNumber);
		}	 
		
		return $srvReturn;
		 
		 /*$srvReturn = jsonTrueResponse($check);
		return $srvReturn;
		  * 
		  */
	}
	
	function setHarga($code, $harga_beli) {
		if($cost >= 1000 && $cost < 2000) {
			$setPrice = 2000;
		} else if($cost >= 2000 && $cost < 3000) {
			$setPrice = 3000;
		} else if($cost >= 3000 && $cost < 4000) {
			$setPrice = 4000;
		}  else if($cost >= 4000 && $cost < 5000) {
			$setPrice = 5000;
		}  else if($cost >= 5000 && $cost < 6000) {
			$setPrice = 7000;
		}  else if($cost >= 9000 && $cost < 12000) {
			$setPrice = 12000;
		}  else if($cost >= 9000 && $cost < 12000) {
			$setPrice = 12000;
		}  else if($cost >= 15000 && $cost < 17000) {
			$setPrice = 17000;
		}  else if($cost >= 19000 && $cost < 22000) {
			$setPrice = 22000;
		}  else if($cost >= 22000 && $cost <= 26000) {
			$setPrice = 27000;
		}  else if($cost >= 27000 && $cost <= 31000) {
			$setPrice = 32000;
		}  else if($cost >= 48000 && $cost <= 51000) {
			$setPrice = 52000;
		}  else if($cost >= 72000 && $cost <= 74000) {
			$setPrice = 77000;
		}  else if($cost >= 96000 && $cost <= 100000) {
			$setPrice = 101000;
		}  else if($cost >= 96000 && $cost <= 101000) {
			$setPrice = 102000;
		}  else if($cost >= 197000 && $cost <= 201000) {
			$setPrice = 203000;
		}  else if($cost >= 247000 && $cost <= 251000) {
			$setPrice = 253000;
		}
		
		$this->m_pulsa->insertNewPrice($code, $harga_beli, $setPrice);
		return $setPrice;
	}

	function saveTrxPulsa($formData) {
		$srvReturn = jsonFalseResponse("Trx Code is already exist..!!");		
		$check = $this->m_pulsa->checkCodePulsa($formData['trx_id']);
		if($check == null) {
			$ins = $this->m_pulsa->saveTrxPulsa($formData);
			if($ins > 0) {
				$srvReturn = jsonTrueResponse(null, "Transaction insert success..!!");
			}
		}
		return $srvReturn;	
	}
	
	function updateTrxPulsa($formData) {
		$srvReturn = jsonFalseResponse("Update Transaction ID : $formData[idx] failed..!!");		
		$ins = $this->m_pulsa->updateTrxPulsa($formData);
		if($ins > 0) {
			$srvReturn = jsonTrueResponse(null, "Update Transaction ID : $formData[idx] success..!!");
		}
		return $srvReturn;	
	}
	
	function trxReport($thn) {
		$srvReturn = $this->m_pulsa->trxReport($thn);
		return $srvReturn;
	}
	
	function summaryReport($from, $to) {
		$srvReturn = null;
		$srvReturn = $this->m_pulsa->summaryReport($from, $to);
		return $srvReturn;
	}
	
	function searchTrxPulsaByID($id) {
		$srvReturn = jsonFalseResponse();
		$arr = $this->m_pulsa->searchTrxPulsaByID($id);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function deletePulsaTrx($id) {
		$srvReturn = jsonFalseResponse("Delete transaction failed..!!");
		$arr = $this->m_pulsa->deletePulsaTrx($id);
		if($arr > 0) {
			$srvReturn = jsonTrueResponse(null, "Delete transaction ID : $id success..!");
		}
		return $srvReturn;
	}
	
	function getListClient() {
		$srvReturn = null;
		$srvReturn = $this->m_pulsa->getListClient();
		return $srvReturn;
	}
	
	function getDataClient($no_tujuan) {
		$srvReturn = jsonFalseResponse();
		$arr = $this->m_pulsa->getDataClient($no_tujuan);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function saveNewClient($data) {
		$srvReturn = jsonFalseResponse("Insert new client failed, number $data[no_tujuan] already exist..!!");		
		$check = $this->m_pulsa->getDataClient($data['no_tujuan']);
		if($check == null) {
			$ins = $this->m_pulsa->saveNewClient($data);
			if($ins > 0) {
				$srvReturn = jsonTrueResponse(null, "Insert new client success..!!");
			}
		}
		return $srvReturn;	
	}
	
	function deleteClientByPhoneNo($no_tujuan) {
		$srvReturn = jsonFalseResponse("Delete client failed..!!");
		$arr = $this->m_pulsa->deleteClientByPhoneNo($no_tujuan);
		if($arr > 0) {
			$srvReturn = jsonTrueResponse(null, "Delete client success..!");
		}
		return $srvReturn;
	}
	
	function apiUserPulsaLogin($usrname, $password) {
		$srvReturn = jsonFalseResponse("Login failed..!!");
		$arr = $this->m_pulsa->apiUserPulsaLogin($usrname, $password);
		if($arr > 0) {
			$srvReturn = jsonTrueResponse(null, "Login success..!");
		}
		return $srvReturn;
	}
	
}

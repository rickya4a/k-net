<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_login_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_login_model");
    }
	
	function handleLoginService() {
		$data = $this->input->post(NULL, TRUE);	
		$mdReturn = null;
		try {
			$checkAuth = $this->be_login_model->checkAuthLogin($data);
			//$mdReturn = $this->be_login_model->fetchingMenu($checkAuth[0]->groupid);
			$arr = array('ecom_user' => $checkAuth[0]->username,
						 'ecom_groupid' => $checkAuth[0]->groupid,
						 'ecom_groupname' => $checkAuth[0]->groupname);
			
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);			
		}
		return $arr;
	}
	
	function fetchingMenu($groupid) {
		$mdReturn = $this->be_login_model->fetchingMenu($groupid);
		return $mdReturn;
	}
	
	
}
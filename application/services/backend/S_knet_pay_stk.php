<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S_knet_pay_stk extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/stockist/m_knet_pay_stk", "m_knet");
    }
	
	function getCurrentPeriodSCO() {
		$srvReturn = jsonFalseResponse("Bns Period empty..");
		try {
			$result = $this->m_knet->getCurrentPeriodSCO();
			$srvReturn = jsonTrueResponse($result, "");
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
		return $srvReturn;
	}
	
	function getAuthStk($data) {
		$srvReturn = jsonFalseResponse("Invalid Username and password");
		try {
			$result = $this->m_knet->getAuthStk($data);
			$srvReturn = jsonTrueResponse($result, "");
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
		return $srvReturn;
	}
	
	function getListTrxToPay() {
		$srvReturn = null;
		$data = $this->input->post(NULL, TRUE);
		try {
			$result = $this->m_knet->getListTrxToPay($data);
			$srvReturn = jsonTrueResponse($result, "");
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
		return $srvReturn;
		
		//return $data;
	}
	
	function getDetailTTPbySSRno($ssrno) {
		$srvReturn = null;
		try {
			$result = $this->m_knet->getDetailTTPbySSRno($ssrno);
			$srvReturn = jsonTrueResponse($result, "");
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
		return $srvReturn;
	}
	
	function previewSelectedSSR($res) {
		//$data = $this->input->post(NULL, TRUE);
		
		$ssr = set_list_array_to_string($res['ssr']);		
		try {
			$resx = $this->m_knet->getListSelectedSSR($ssr);
			//print_r($resx);
			$result = $this->m_knet->saveTempPayStl($resx, $res);
			$srvReturn = array("response" => "true", "arrayData" => $resx);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
		return $srvReturn;
		
	}
	
	function getDataPaymentStk($orderid) {
		$res = $this->m_knet->getStkPayHeader($orderid);
		return $res;
	}
	
	function getDataPaymentDetailStk($orderid) {
		$res = $this->m_knet->getStkPayDetail($orderid);
		return $res;
	}
	
	function setStatusPay($sgo_token, $stt) {
		$result = $this->m_knet->setStatusPay($sgo_token, $stt);
		return $res;
	}
}
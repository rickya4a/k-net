<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_userconfig_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
		$this->load->model("backend/be_login_model");
        $this->load->model("backend/be_userconfig_model");
    }
	
	/*------------------------------
	 * ACCESS MENU
	 *---------------------------- */
	
	function fetchingMenuService() {
		$srvReturn = null;
		try {
			$srvReturn = $this->be_login_model->fetchingAllMenu();
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function getListUserByRoleID($roleid, $menuid) {
		$srvReturn = null;
		try {
			$srvReturn = $this->be_login_model->getListUserByRoleID($roleid, $menuid);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputAccessMenuService() {
		$data = $this->input->post(NULL, TRUE);	
		$srvReturn = null;
		try {
		    //echo $data['grpid'];
			$del = $this->be_userconfig_model->deleteUserAuthByID($data['grpid']);
			$modelReturn = $this->be_userconfig_model->saveInputAccessMenu($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Saving Group User Config Menu Success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	/*----------------
	 * USER GROUP
	 * --------------*/
	function listUserGroupService($param, $value) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListUserGroup($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function listAllUserGroupService() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListAllUserGroup();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function inputUserGroupService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->saveInputUserGroup();
			$srvReturn = jsonTrueResponse($modelReturn, "Input User Group Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function updateUserGroupService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->updateUserGroup();
			$srvReturn = jsonTrueResponse($modelReturn, "Update User Group Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteUserGroupService($id) {
		$srvReturn = null;
		try {
		   $ifRoot = $this->be_userconfig_model->checkDataFromTable("groupid", "ecomm_usergroup", 1);	
		   $checkUser = $this->be_userconfig_model->checkDataFromTable("groupid", "ecomm_user", $id);
		   $checkUsrAuth = $this->be_userconfig_model->checkDataFromTable("groupid", "ecomm_userauthority", $id);
		   $modelReturn = $this->be_userconfig_model->deleteUserGroup($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete User Group success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
	
	/*------------------
	 * USER
	 ------------------*/
	
	function listAllUserService() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListAllUser();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function listUserService($param, $value) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListUser($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputUserService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->saveInputUser();
			$srvReturn = jsonTrueResponse($modelReturn, "Input User success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());	
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function updateUserService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->saveUpdateUser();
			$srvReturn = jsonTrueResponse($modelReturn, "Update User Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteUserService($id) {
		$srvReturn = null;
		try {
		   $modelReturn = $this->be_userconfig_model->deleteUser($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete User success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
	
	/*------------------
	 * APPLICATION
	 ------------------*/
	
	function listAllApplicationService() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListAllApplication();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function listApplicationService($param, $value) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListApplication($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputApplicationService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->saveInputApplication();
			$srvReturn = jsonTrueResponse($modelReturn, "Input Application success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());	
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function updateApplicationService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->saveUpdateApplication();
			$srvReturn = jsonTrueResponse($modelReturn, "Update Application Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteApplicationService($id) {
		$srvReturn = null;
		try {
		   $checkUser = $this->be_userconfig_model->checkDataFromTable("app_id", "app_tabprg", $id);	
		   $modelReturn = $this->be_userconfig_model->deleteApplication($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Application success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
	
	/*------------------
	 * GROUP MENU
	 ------------------*/
	
	function listAllGroupMenuService() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListAllGroupMenu();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function listGroupMenuService($param, $value) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListGroupMenu($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputGroupMenuService() {
		$srvReturn = null;
		try {
			$grpmenu_id = $this->be_userconfig_model->getMenuID();
			$modelReturn = $this->be_userconfig_model->saveInputGroupMenu($grpmenu_id);
			$srvReturn = jsonTrueResponse($modelReturn, "Input Group Menu success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());	
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function updateGroupMenuService() {
		$srvReturn = null;
		$data = $this->input->post(NULL, TRUE);
		try {
			$upd = $this->be_userconfig_model->updateAppIdOnSubMenu($data);
			$modelReturn = $this->be_userconfig_model->saveUpdateGroupMenu($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Group Menu Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteGroupMenuService($id) {
		$srvReturn = null;
		try {
		   	
		   $checkUsrAuth = $this->be_userconfig_model->checkDataFromTable("menuid", "ecomm_userauthority", $id);	
		   $checkUser = $this->be_userconfig_model->checkDataFromTable("app_menu_parent_id", "app_tabprg", $id);	
		   $modelReturn = $this->be_userconfig_model->deleteGroupMenu($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Group Menu success..!!");
		} catch(Exception $e) {
		   throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
	
	/*------------------
	 * SUB MENU
	 ------------------*/
	
	function listAllSubMenuService() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListAllSubMenu();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function listSubMenuService($param, $value) {	
		$srvReturn = null;
		try {
			$modelReturn = $this->be_userconfig_model->getListSubMenu($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputSubMenuService() {
		$data = $this->input->post(NULL, TRUE);	
		$srvReturn = null;
		try {
			$menu_id = $this->be_userconfig_model->getMenuID($data['app_submenu_prefix']);
			$modelReturn = $this->be_userconfig_model->saveInputSubMenu($menu_id, $data);
			$srvReturn = jsonTrueResponse($modelReturn, "Input Sub Menu success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());	
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function updateSubMenuService() {
		$srvReturn = null;
		$data = $this->input->post(NULL, TRUE);
		try {
			$modelReturn = $this->be_userconfig_model->saveUpdateSubMenu($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Sub Menu Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteSubMenuService($id) {
		$srvReturn = null;
		try {
		   $checkUsrAuth = $this->be_userconfig_model->checkDataFromTable("menuid", "ecomm_userauthority", $id);	
		   $modelReturn = $this->be_userconfig_model->deleteGroupMenu($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Sub Menu success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
}
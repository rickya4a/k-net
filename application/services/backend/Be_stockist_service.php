<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_product_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_product_model");
    }
	
	/*--------------------
	 * PRODUCT CATEGORY
	 *-------------------*/
	
	function listUserProductCatService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListProductCat($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function inputProductCatService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->saveInputProductCat();
			$srvReturn = jsonTrueResponse($modelReturn, "Input Product Category Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function listAllProductCat() {
		$modelReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListAllProductCat();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	}
	
	function updateProductCatService() {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->saveUpdateProductCat();
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Category Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	
	function deleteProductCatService($id) {
		$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("cat_id", "master_prd_cat_inv", $id);	
		   $modelReturn = $this->be_product_model->deleteProductCat($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Product Cat success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	}
	
	
	/*------------------------
	 * PRICE CODE
	 -------------------------*/
	 function listPriceCodeService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListPriceCode($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	}
	 
	 
	 function listAllPriceCodeService() {
	 	$modelReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListAllPriceCode();		
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $modelReturn;
	 }
	 
	 function inputPriceCodeService() {
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->savePriceCode();
			$srvReturn = jsonTrueResponse($modelReturn, "Input Price Code Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function updatePriceCodeService() {
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->updatePriceCode();
			$srvReturn = jsonTrueResponse($modelReturn, "Update Price Code Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function deletePriceCodeService($id) {
	 	$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("pricecode", "master_prd_pricetab", $id);	
		   $modelReturn = $this->be_product_model->deletePriceCode($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Price Code success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	 }
	 
	 /*------------------------
	 * PRODUCT
	 -------------------------*/
	 
	 function listAllProductService($noexception = true) {
	 	$modelReturn = null;
		if($noexception) {
			try {
				$modelReturn = $this->be_product_model->getlistAllProduct($noexception);		
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);		
			}	
		} else {
			$modelReturn = $this->be_product_model->getlistAllProduct($noexception);
		}
		return $modelReturn;
	 }
	 
	 function listProductService($param, $value) {
		$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->getListProduct($param, $value);
			$srvReturn = jsonTrueResponse($modelReturn);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function inputProductService() {
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->inputProduct();
			$srvReturn = jsonTrueResponse($modelReturn, "Input Product Success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function updateProductService() {
	 	$srvReturn = null;
		try {
			$modelReturn = $this->be_product_model->updateProduct();
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Success..!!");
		} catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		}	
		return $srvReturn;
	 }
	 
	 function deleteProductService($id) {
	 	$srvReturn = null;
		try {
		   $checkMasterPrd = $this->be_product_model->checkDataFromTable("cat_inv_id", "master_prd_pricetab", $id);	
		   $modelReturn = $this->be_product_model->deleteProduct($id);
		   $srvReturn = jsonTrueResponse($modelReturn,  "Delete Product success..!!");
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		}	
		return $srvReturn;
	 }
	 
	 /*-------------------------
	  * PRODUCT PRICE
	  *--------------------------/*
	  */ 
	  function checkProductPriceCodeService() {
	  	 $data = $this->input->post(NULL, TRUE);	
	  	 $srvReturn = null;
		 try {
		   $checkMasterPrd = $this->be_product_model->getListProduct("cat_inv_id",  $data['cat_inv_id']);	
		   $checkPriceCodeByProdID = $this->be_product_model->showProductPriceCode($data);
		   if($checkPriceCodeByProdID == null) {
		   	   $res = $this->be_product_model->getListPriceCode("status", "1"); 
			   $srvReturn = array("response" => "true", "message" => "2", "arrayData" => $res, "prdnm" => $checkMasterPrd[0]->cat_inv_desc);
		   } else {
			   $srvReturn = array("response" => "true", "message" => "1", "arrayData" => $checkPriceCodeByProdID, "prdnm" => $checkMasterPrd[0]->cat_inv_desc);
		   } 
		   
		 } catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);
		 }	
		 return $srvReturn;
	  }
	  
	  function inputProductPriceService() {
	  	 $data = $this->input->post(NULL, TRUE);	
	  	 $srvReturn = null;
		 try {
			$modelReturn = $this->be_product_model->saveProductPrice($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Input Product Price Success..!!");
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		 return $srvReturn;
	  }
	  
	  function updateProductPriceService() {
	  	 $data = $this->input->post(NULL, TRUE);	
	  	 $srvReturn = null;
		 try {
			$del = $this->be_product_model->deleteProductPrice($data);
			$modelReturn = $this->be_product_model->saveProductPrice($data);
			$srvReturn = jsonTrueResponse($modelReturn, "Update Product Price Success..!!");
		 } catch(Exception $e) {
			$srvReturn = $this->jsonFalseResponse($e->getMessage());
			throw new Exception($e->getMessage(), 1);		
		 }	
		 return $srvReturn;
	  }
	  
	  function deleteProductPriceService($id) {
	  	
	  }
	 
}
	
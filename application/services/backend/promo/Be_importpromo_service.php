<?php if(! defined ('BASEPATH')) exit ('NO DIRECT ACCESS ALLOWED');

class Be_importpromo_service extends MY_Service{

    function __construct(){

        parent::__construct();
        $this->load->model("backend/promo/be_importpromo_model","m_promo");
    }

    function listImportPromo(){

        $impro = $this->m_promo->listImportPromo();
        return $impro;
    }

    function importToDB($dt){

        $impro = $this->m_promo->importToDB($dt);
        return $impro;
    }


}
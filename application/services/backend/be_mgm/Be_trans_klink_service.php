<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_trans_klink_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_mgm/be_trans_klink_model", 'm_ktrans');
    }
	
	function getListTrxByParam($searchby, $param, $bnsmonth=null, $bnsyear=null, $from=null, $to=null) {
		/*
		echo  "$searchby<br/>";
		echo  "$param<br/>";
		echo  "$bnsmonth<br/>";
		echo  "$bnsyear<br/>";
		echo  "$from<br/>";
		echo  "$to<br/>";
		 */
		
		$srvReturn = null;
		$mdReturn = $this->m_ktrans->getListTrxByParam($searchby, $param, $bnsmonth, $bnsyear, $from, $to);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getTrxByTrcd($param, $trcd) {
		$header = $this->m_ktrans->getTrxByTrcdHead($param, $trcd);
		if($header != null) {
			//var_dump($header);
			//$detail = null;		
			$detail = $this->m_ktrans->geTrxByTrcdDet("trcd", $header[0]->trcd);
			$pay = $this->m_ktrans->getTrxByTrcdPay("trcd", $header[0]->trcd);
			//var_dump($detail);
			//$payment = $this->m_ktrans->getTrxPayment($trcd);
			//var_dump($payment);
			if($header != null && $detail != null) {
				$srvReturn = array("response" => "true", "header" => $header, "detail" => $detail, "pay" =>$pay);
			} else {
				if($header == null) {
					$headerx = "Data header kosong";
				}
				if($detail == null) {
					$detailx = "Data detail kosong";
				}
				$msg = $headerx." ".$detailx;
				$srvReturn = array("response" => "false", "message" => $msg);
			}
			//$srvReturn = array("response" => "true", "header" => $header, "detail" => $detail);
			 //,							"payment" => $payment);
			return $srvReturn;
		} else {
			return null;
		}
	}
	
	function getStockist($loccd) {
		$srvReturn = $this->m_ktrans->getStockist($loccd);
		//var_dump($srvReturn);
		return $srvReturn;
	}
	
	function getAllStockist() {
		$srvReturn = $this->m_ktrans->getAllStockist();
		//var_dump($srvReturn);
		return $srvReturn;
	}
	
	function getListRecapSales($param, $bnsmonth=null, $bnsyear=null) {
		$srvReturn = null;
		$mdReturn = $this->m_ktrans->getListRecapSales($param, $bnsmonth, $bnsyear);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getListRecapBonus($param, $rptType, $bnsmonth=null, $bnsyear=null) {
		$srvReturn = null;
		$mdReturn = $this->m_ktrans->getListBonus_Vcr($param, $rptType, $bnsmonth, $bnsyear);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function listWHFromGNV() {
		$srvReturn = $this->m_ktrans->listWHFromGNV();
		//var_dump($srvReturn);
		return $srvReturn;
	}
	
	function saveImportToDB($data, $whcd) {
		$srvReturn = $this->m_ktrans->saveImportToDB($data, $whcd);
		return $srvReturn;
	}
	
	function delDataStockAwal($whcd) {
		$srvReturn = $this->m_ktrans->delDataStockAwal($whcd);
		return $srvReturn;
	}
	
	function listBnsPeriod() {
		$srvReturn = $this->m_ktrans->listBnsPeriod();
		//var_dump($srvReturn);
		return $srvReturn;
	}
	
	function uptBnsPeriod($period, $param) {
		$srvReturn = $this->m_ktrans->uptBnsPeriod($period, $param);
		//var_dump($srvReturn);
		return $srvReturn;
	}
	
	function showSysPref(){
		$srvReturn = $this->m_ktrans->showSysPref();
		//var_dump($srvReturn);
		return $srvReturn;
	}
	
	
	function getOperatorSMS() {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = $this->m_ktrans->getOperatorSMS();
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
}
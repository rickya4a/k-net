<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_transaction_service extends MY_Service {

    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model("backend/be_transaction_model", 'm_trans');
    }
	
	function getListTransactionToPost() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTransactionToPost($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListTrxToDelete($form) {
		$srvReturn = jsonFalseResponse();	
		//$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTrxToDelete($form);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListCN() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListCN($data, $data['cnPrintStt']);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListKW() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListKW($data, $data['kwPrintStt']);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getCNReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$mdReturn = $this->m_trans->getCNReportToPrint($data);
		if($data['cnPrintStt'] == "0") {
			$param = set_list_array_to_string($data['cnno']);
			$upd = $this->m_trans->updateCNStatus($param, "1");
		}
		return $mdReturn;
	}
	
	function getKWReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$param = set_list_array_to_string($data['kwno']);
		$mdReturn = $this->m_trans->getKWReportToPrint($param);
		if($data['kwPrintStt'] == "0") {
			$upd = $this->m_trans->updateKWStatus($param, "1");
		}
		return $mdReturn;
	}
	
	function savePostingTrxSrv() {
		$srvReturn = jsonFalseResponse("Posting Transaction failed..!!");	
		$orderno = $this->input->post("orderno");
		$param = set_list_array_to_string($orderno);
		$mdReturn = $this->m_trans->savePostingTransaction($param);
		if($mdReturn > 0) {
			$srvReturn = jsonTrueResponse($mdReturn, "Posting Transaction $mdReturn records success..!!");
		} 
        return $srvReturn;
		
	}
	
	function getListTransactionReportToSMSResend() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListTransactionReportToSMSResend($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListTransactionReport() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		//print_r($data);
		if($data['trx_type'] == "TOP") {
			$arr = $this->m_trans->getListTopUpVaReport($data);
		} else if ($data['trx_type'] == "double") {
			$arr = $this->m_trans->checkDoubleTransaction($data);
		} else if($data['trx_type'] == "vchbel") {
			$arr = $this->m_trans->getListTrxUsingVoucher($data);
			echo "<pre>";
			print_r($arr);
			echo "</pre>";
		} else if($data['trx_type'] == "failRegMampirkak") {
			$arr = $this->m_trans->listTransaksiMampirkakDenganRegMemberGagal();
		}
		else if($data['searchBy'] == "orderno" || $data['searchBy'] == "CNno" || $data['searchBy'] == "KWno" || $data['searchBy'] == "conoteJNE" || $data['searchBy'] == "token") {
			$arr = $this->m_trans->getListTrxById($data['searchBy'], $data['paramValue']);
		}
		else if($data['trx_type'] == "PRD") {
			$arr = $this->m_trans->getProductSummary($data);
		} else if($data['trx_type'] == "TDKLENGKAP") {
			$arr = $this->m_trans->listTransaksiDenganDataTidakLengkap($data);
		} else {
			$arr = $this->m_trans->getListTransactionReport($data);	
			//print_r($arr);
		}	
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
			//print_r($srvReturn);
		}
		return $srvReturn;
	}
	
	function getTrxByID($id) {
		$srvReturn = jsonFalseResponse();	
		//echo "no id nya = $id";
		$header = $this->m_trans->getTrxHeader($id);
		//var_dump($header);
		$detail = $this->m_trans->getTrxDetail($header[0]->orderno);
		//var_dump($detail);
		$payment = $this->m_trans->getTrxPayment($header[0]->orderno);
		
		$member = $this->m_trans->getEcommMemb($header[0]->orderno);
		//var_dump($payment);
		$shipaddr = null;
		if($header[0]->sentTo == "2") {
			$shipaddr = $this->m_trans->getShipAddr($header[0]->orderno);
			//echo "<pre>";
			//var_dump($shipaddr);
			//echo "</pre>";
		} else {
			$shipaddr = $this->m_trans->getGoodReceiptStatus($header[0]->orderno);
		}
		//var_dump($shipaddr);
		if($header != null) {
			$srvReturn = array(
				"response" => "true", 
				"header" => $header, 
				"detail" => $detail, 
				"payment" => $payment,
				"shipaddr" => $shipaddr,
				"member" => $member
		    );
		}
		//echo "<pre>";
		//print_r($srvReturn);
		//echo "</pre>";
		return $srvReturn;
	}

	function getTrxByID2($id){
		$d = $this->m_trans->getTrxByID2($id);

		if($d != null) {
			$srvReturn = array(
				"response" => "true",
				"detail" => $d
			);
		}

		return $srvReturn;
	}
	
	function getListIP() {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListIP($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getIPReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$param = set_list_array_to_string($data['ipno']);
		$mdReturn = $this->m_trans->getIPReportToPrint($param);
		if($data['ipPrintStt'] == "0") {
			$upd = $this->m_trans->updateIPStatus($param, "1");
		}
		return $mdReturn;
	}
	
	function resendSMSTrxBaru($orderno, $token) {
		$no = substr($token, 0, 2);
		$srvReturn = jsonFalseResponse("SMS gagal..");
		if($no == "EM") {
			$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
			$resultInsert = $this->ecomm_trans_model->getTrxSalesPlusReg($token);
			
			if($resultInsert != null) {
				//$this->paymentService->sendTrxSMS2($resultInsert);
				$this->ecomm_trans_model->sendSMSPromoSalesAndReg($resultInsert);
				//sms ke member yg di daftarkan dan yg mendaftarkan
				$this->ecomm_trans_model->getRegMemberKnetPlus($resultInsert[0]->orderno);
				$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");	
			} 
		} else if($no == "EP") {
			//TRANSAKSI MAMPIRKAK
			$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
			$resultInsert = $this->ecomm_trans_model->getInsertTrxMampirKak($token);
			
			if($resultInsert != null) {
				if($resultInsert[0]->new_member_reg != null && $resultInsert[0]->new_member_reg != "") {
				   $this->load->model('webshop/member_model', 'member_model');
				   $upd = $this->member_model->sendSmsRegMembMampirKak($resultInsert);
				}	
			  $this->ecomm_trans_model->sendTrxSmsMampirKak($resultInsert);
			  $srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");
			}
			
			
		} else if($no == "EC" || $no == "EA") {
			$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
			$resultInsert = $this->ecomm_trans_model->getInsertTrxOrderID($token);
			$this->ecomm_trans_model->sendTrxSMS2($resultInsert);

			$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($token);
			$this->load->model("webshop/Payment_model",'pmodel');
				$data_catalog=$this->pmodel->getKATALOG($token);
				if($data_catalog != null){

					emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($token, $data_catalog));
					foreach($data_catalog as $row) {
						$link="Download katalog di : ".base_url('catalogue/download/'.$token.'/'.$row->cat_inv_id);
						smsTemplate( $identitas[0]->telp_customer, $link);
						//smsTemplate("087780441874", $link);
					}

				}

			$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");
		} else if($no == "RM" || $no == "RC" || $no == "LP" || $no == "RA") {
			$this->load->model('webshop/member_model', 'member_model');
			$result = $this->member_model->show_member_new2($token);
			
			//$this->load->model('webshop/member_model', 'member_model');
		    $upd = $this->member_model->sendTrxSmsRegMember($result);
			$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");
		} else if($no == "TA") {
			$this->load->model("dtc/mdtc_mbr", "mdtc");
			//$sms  = $this->mdtc->sms2Dion($token);
			$sms  = $this->mdtc->sms2($token);
			$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");
		}
		return $srvReturn;
	}
	
	function resendSMSTrx($orderno) {
	    $this->load->model("webshop/Payment_model",'pmodel');
		$srvReturn = jsonFalseResponse("Data kosong..");	
		$text = "";
		$arx = substr($orderno, 0, 1);
		if($arx == "I" || $arx == "O") {
			$dta = $this->m_trans->getTrxShopOL($orderno);
			//print_r($dta);
			if($dta[0]->sentTo == "1") {
				$ship = 'Kode Ambil Brg: '.$dta[0]->secno;
				$shipRyan = 'SC : '.$dta[0]->secno;
			} else {
				if($dta[0]->cargo_id == "2") {
					$ship = 'Conote: '.$dta[0]->conoteJNE. 'Tracking : http://bit.ly/kgptracking';
				} else {
					$ship = 'Conote: '.$dta[0]->conoteJNE. 'Tracking : http://bit.ly/jnetracking';	
				}
				$shipRyan = 'Resi : '.$dta[0]->conoteJNE;
				//$ship = 'Conote: '.$dta[0]->conoteJNE;
			}			
			//$text = 'Trx sukses,No Order: '.$orderno.',Jumlah Trx: '.$dta[0]->total_pay.','.$ship.'';	
			$text = 'Trx sukses,No Order: '.$orderno.','.$ship.'';	
			$send = smsTemplate($dta[0]->tel_hp, $text);
			if($dta[0]->tel_hp_login != null && $dta[0]->tel_hp_login != "" && $dta[0]->tel_hp_login != $dta[0]->tel_hp) {
				$send = smsTemplate($dta[0]->tel_hp_login, $text);
			}
			
			if($dta[0]->no_hp_konfirmasi != null && $dta[0]->no_hp_konfirmasi != "") {
				$send = smsTemplate($dta[0]->no_hp_konfirmasi, $text);
			}
			
			
			//$sendDion = smsTemplate("087780441874", $text);
			$textRyan = 'OrderNo: '.$dta[0]->orderno.', Pay/BV : '.$dta[0]->total_pay.' / '.$dta[0]->total_bv.', MEMBER : '.$dta[0]->namamember.', '.$shipRyan.'';
			$send = smsTemplate("087784698951", $textRyan);
			//$send = smsTemplate("08121028554", $text);
			//$send = smsTemplate("087780441874", $text);
			//$send = smsTemplate("087780441874", $textRyan);
			//echo $send;
			//$send = smsTemplate("081315732191", $text);
			$token = substr($dta[0]->token, 0, 2);
			if($token == "EP" && $dta[0]->memberbaru != null) {
			   $text2 = 'ID Member : '.$dta[0]->memberbaru.' / '.$dta[0]->namamember.', Password : '.$dta[0]->password.'';	
			   $send = smsTemplate($dta[0]->tel_hp, $text2);
			   $send = smsTemplate($dta[0]->no_hp_konfirmasi, $text2);
			   //$send = smsTemplate("087780441874", $text2);
			   
			   //$send = smsTemplate("087780441874", $text2);
			   $send = smsTemplate("087784698951", $text2);
			   //$send = smsTemplate("081315732191", $text);
			}
			
			$recName = substr($dta[0]->receiver_name,10);
			$textCust = 'K-NET : '.$recName.', Trx sukses,No Order: '.$dta[0]->orderno.','.$ship.'';
			//$sendDion = smsTemplate("087780441874", $textDion);
			//$send = smsTemplate("087784698951", $text);
			if($dta[0]->tel_hp1 != null && $dta[0]->tel_hp1 != "") {
				$send = smsTemplate($dta[0]->tel_hp1, $textCust);
			}
			
			$data_catalog = $this->pmodel->getKATALOG($dta[0]->token);
			//print_r($data_catalog);
			if($data_catalog != null) {

				emailSend('Katalog Digital K-Link', $dta->email, KatalogEmail($dta[0]->token, $data_catalog));
				foreach($data_catalog as $row){
					$link="Download katalog di : ".base_url('catalogue/download/'.$dta[0]->token.'/'.$row->cat_inv_id);
					smsTemplate( $resultInsert[0]->tel_hp_login, $link);
					smsTemplate( $resultInsert[0]->tel_hp_as, $link);
				}

			} 
			

			
			$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");	
			
		} else {
			$dta = $this->m_trans->getTrxInputMember($orderno);
			//print_r($dta);
			if($dta[0]->prdcd == "SK004AO") {
				$ship = 'Kartu dan AyuArtis oil akan di kirim ke alamat member';
			} else {
				if($dta[0]->sentTo == "1") {
					$ship = 'Kode Ambil Brg: '.$dta[0]->secno;
					$shipRyan = 'SC : '.$dta[0]->secno;
				} else {
					//$ship = 'Conote: '.$dta[0]->conoteJNE;
					if($dta[0]->cargo_id == "2") {
						$ship = 'Conote: '.$dta[0]->conoteJNE. 'Tracking : http://bit.ly/kgptracking';
					} else {
						$ship = 'Conote: '.$dta[0]->conoteJNE. 'Tracking : http://bit.ly/jnetracking';	
					}
					$shipRyan = 'Resi : '.$dta[0]->conoteJNE;
				}	
			}	
			$text = 'Trx sukses,No Order: '.$orderno.', Idmember : '.$dta[0]->memberid.',Password : '.$dta[0]->password.','.$ship.'';
			//echo $text;
			//$send = $this->m_trans->resendSMSNotification($dta[0]->tel_hp, $text);
			//$sendToMember = $this->m_trans->resendSMSNotification($dta[0]->telp_member_hp, $text);
			//$sendEmail = $this->resendWelcomeEmail($dta);
			//print_r($sendEmail);
			//if($send > 0 && $sendToMember > 0) {
			$send = smsTemplate($dta[0]->tel_hp, $text);
			$sendToMember = smsTemplate($dta[0]->telp_member_hp, $text);
			//$sendDion = smsTemplate("087780441874", $text);
			$textRyan = 'OrderNo: '.$orderno.', Idmember : '.$dta[0]->memberid.',Password : '.$dta[0]->password.','.$shipRyan.'';
			$send = smsTemplate("087784698951", $textRyan);
			//$arrData = array("resp" => $sendDion);
			$srvReturn = jsonTrueResponse(NULL,  "SMS sudah dikirim");	
			
			
		}
		
		/*
		$send = $this->m_trans->resendSMSNotification($dta[0]->tel_hp, $text);
		//tambahan DION 02/03/2016
		$sendToMember = $this->m_trans->resendSMSNotification($dta[0]->telp_member_hp, $text);
		//end
		if($send > 0 && $sendToMember > 0) {
			$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");	
		}
		*/
		return $srvReturn; 
		
	}

	function resendEmailTrx($orderno) {
		$dta = $this->m_trans->getTrxInputMember($orderno);
		$sendEmail = $this->resendWelcomeEmail($dta);
		return $sendEmail;
	}
	
	function getListJNEConote($param, $value) {
		$srvReturn = jsonFalseResponse();	
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListJNEConote($data, $param, $value);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListConoteToEmail() {
		$data = $this->input->post(NULL, TRUE);
		
		$tanggal = date("Y-m-d", strtotime($data['datepick']));
		$pickup_datetime = $tanggal." ".$data['jam'].":".$data['menit'].":00";
		$pickup_datetime = date("Y-m-d h:i:s", strtotime($pickup_datetime));
		
		
		$cargo_id = $this->input->post("cargo_id");
		$cargo_name = $_POST['cargo_id'];
		$datepick = $this->input->post("datepick");
		$datepick = date("d/m/Y", strtotime($datepick));
		$res = $this->m_trans->getListEmailToSend($cargo_id);
		
		//$res = $this->m_trans->getListEmailToSend();
		$char = "";
		if($res == null) {
			$char = setErrorMessage();
		} else {
			foreach($res as $data) {
				$char .= "<table border=1 width=100%><tr><td width='35%'>Nama Perusahaan</td><td >$data->CP_NAME</td></tr>";
						$char .= "<tr><td>Dikirim Oleh</td><td>$data->shipper_name</td></tr>";
						$char .= "<tr><td>Nomor Account</td><td>$data->NATIONAL_ACCOUNT</td></tr>";
						$char .= "<tr><td>Alamat Pick Up</td><td>$data->ADDR</td></tr>";
						$char .= "<tr><td>Area</td><td>$data->area</td></tr>";
						$char .= "<tr><td>PIC</td><td>$data->PIC</td></tr>";
						$char .= "<tr><td>Telepon</td><td>$data->TEL_NO</td></tr>";
						$char .= "<tr><td>Paket / Dokumen</td><td>$data->PACKAGE</td></tr>";
						$char .= "<tr><td>Qty / Pcs</td><td>$data->QTY</td></tr>";
						$char .= "<tr><td>Ukuran Paket</td><td>$data->DIMENS</td></tr>";
						$char .= "<tr><td>Service ( SS, YES, REG )</td><td>$data->SERVICE_TYPE</td></tr>";
						$char .= "<tr><td>Armada (mobil / motor)</td><td>$data->PICKUP_VHC</td></tr>";
						$char .= "<tr><td>Waktu Pick Up</td><td>$pickup_datetime</td></tr>";
						$char .= "<tr><td>Kota Tujuan</td><td>$data->DEST_CITY</td></tr>";
						$ordernotrx = str_replace("`", "", $data->ORDERNO);
					    $char .= "<tr><td>No Trx</td><td>$ordernotrx</td></tr>";
						$char .= "<tr><td>Keterangan lain-lain</td><td>$data->REMARK</td></tr>";
						$char .= "</table>";
			}	
        }
		return $char;
	}
	function getListPickUP() {
		$cargo_id = $this->input->post("cargo_id");
		$datepick = $this->input->post("datepick");
		$datepick = date("d/m/Y", strtotime($datepick));
		$res = $this->m_trans->getListPickUP($cargo_id, $datepick);
		return $res;
	}
	
	function sendEmailConote($data) {
		$tanggal = date("Y-m-d", strtotime($data['datepick']));
			$pickup_datetime = $tanggal." ".$data['jam'].":".$data['menit'].":00";
			$pickup_datetime = date("Y-m-d h:i:s", strtotime($pickup_datetime));
			//echo $pickup_datetime;
			
			$cargo_id = $this->input->post("cargo_id");
			$cargo_name = $_POST['cargo_id'];
			$datepick = $this->input->post("datepick");
			$datepick = date("d/m/Y", strtotime($datepick));
			$res = $this->m_trans->getListEmailToSend($cargo_id);
			
			//$res = $this->m_trans->getListEmailToSend();

			/*echo "<pre>";
			print_r($res);
			echo "</pre>";*/
			if($res > 0) {
				$char = "<html>
						<head>
						  <title>Pengiriman List Conot</title>
						</head>
						
						<body>";	
				foreach($res as $data) {
					$char .= "<table border=1><tr><td width='35%'>Nama Perusahaan</td><td >$data->CP_NAME</td></tr>";
					$char .= "<tr><td>Dikirim Oleh</td><td>$data->shipper_name</td></tr>";
					$char .= "<tr><td>Nomor Account</td><td>$data->NATIONAL_ACCOUNT</td></tr>";
					$char .= "<tr><td>Alamat Pick Up</td><td>$data->ADDR</td></tr>";
					$char .= "<tr><td>Area</td><td>$data->area</td></tr>";
					$char .= "<tr><td>PIC</td><td>$data->PIC</td></tr>";
					$char .= "<tr><td>Telepon</td><td>$data->TEL_NO</td></tr>";
					$char .= "<tr><td>Paket / Dokumen</td><td>$data->PACKAGE</td></tr>";
					$char .= "<tr><td>Qty / Pcs</td><td>$data->QTY</td></tr>";
					$char .= "<tr><td>Ukuran Paket</td><td>$data->DIMENS</td></tr>";
					$char .= "<tr><td>Service ( SS, YES, REG )</td><td>$data->SERVICE_TYPE</td></tr>";
					$char .= "<tr><td>Armada (mobil / motor)</td><td>$data->PICKUP_VHC</td></tr>";
					$char .= "<tr><td>Waktu Pick Up</td><td>$pickup_datetime</td></tr>";
					$char .= "<tr><td>Kota Tujuan</td><td>$data->DEST_CITY</td></tr>";
					$ordernotrx = str_replace("`", "", $data->ORDERNO);
					$char .= "<tr><td>No Trx</td><td>$ordernotrx</td></tr>";
					$char .= "<tr><td>Keterangan lain-lain</td><td>$data->REMARK</td></tr>";
					$char .= "</table><br />";
					
				    $arr = $this->m_trans->updateFlagSendConote($data->ORDERNO, $pickup_datetime);
						
				}	
				$char .= "</body></html>";
				echo $char;
				
				
				$resEmail = $this->m_trans->getConfigEmail('Req. Pickup');
				//$recipientArr = array();
				$ccArr = null;
				$bccArr = null;
				$fromSender = null;
				$recipient = null;
				foreach($resEmail as $data) {
					if($data->flag_to == '1' && $data->status == '1'){
						//array_push($recipientArr, $data->email);
						$recipient = "$recipient,$data->email";	
					}
					
					if($data->flag_cc == '1' && $data->status == '1'){
						//array_push($ccArr, $data->email);
						$ccArr = "$ccArr|$data->email";	
					}
					
					if($data->flag_bcc == '1' && $data->status == '1'){
						//array_push($bccArr, $data->email);	
						$bccArr = "$bccArr|$data->email";
					}
					
					if($data->flag_sender == '1' && $data->status == '1'){
						$fromSender = $data->email;	
					}
						
				}	
				
				$recipient = substr($recipient, 1, strlen($recipient));
				$ccArr = substr($ccArr, 1, strlen($ccArr));	
				$bccArr = substr($bccArr, 1, strlen($bccArr));
				
				echo "recipient = $recipient<br />";
								
				$from = $fromSender; //"noreply@k-link.co.id";
		        $fromname = "PT K-Link Indonesia";
		        $to = $recipient; //"dionrespati@gmail.com"; //Recipients list (semicolon separated)
		        $api_key = "87d9af6eab922de72f919312ce9d3e4a";
		        $subject = "Req. Pickup";
		        $content = $char;
		        
		        
		        $data=array();
				//rawurlencode
		        $data['subject']= $subject;                                                                       
		        $data['fromname']= $fromname;                                                             
		        $data['api_key'] = $api_key;
		        $data['from'] = $from;
		        $data['content']= $content;
		        $data['recipients']= $to;
				
				if($ccArr != null || !empty($ccArr) || $ccArr != ''){
					echo "ccArr = $ccArr<br />";
					$data['recipients_cc']= $ccArr;	
				}
				
				if($bccArr != null || !empty($bccArr) || $bccArr != ''){
					echo "bccArr = $bccArr<br />";
					$data['bcc']= $bccArr;	
				}
				
		        $apiresult = $this->callApi(@$api_type,@$action,$data);
		        echo trim($apiresult);
				 
			}
			//update hilal 2016-01-02
			//here for ori**
			else {
				 echo "<div class='alert-error'>Empty Record</div>";
			}	
	}
	/*
	function sendEmailConote($data) {
			//var_dump($data);
			$tanggal = date("Y-m-d", strtotime($data['datepick']));
			$pickup_datetime = $tanggal." ".$data['jam'].":".$data['menit'].":00";
			$pickup_datetime = date("Y-m-d h:i:s", strtotime($pickup_datetime));
			//echo $pickup_datetime;
			$res = $this->m_trans->getListEmailToSend();
			if($res > 0) {
				//$char = "";
				
				$char = "<html>
						<head>
						  <title>Pengiriman List Conot</title>
						</head>
						
						<body>";
				foreach($res as $data) {
					//$char .= "";
					$char .= "<table border=1><tr><td width='35%'>Nama Perusahaan</td><td >$data->CP_NAME</td></tr>";
					$char .= "<tr><td>Nomor Account</td><td>$data->NATIONAL_ACCOUNT</td></tr>";
					$char .= "<tr><td>Alamat Pick Up</td><td>$data->ADDR</td></tr>";
					$char .= "<tr><td>Area</td><td>$data->area</td></tr>";
					$char .= "<tr><td>PIC</td><td>$data->PIC</td></tr>";
					$char .= "<tr><td>Telepon</td><td>$data->TEL_NO</td></tr>";
					$char .= "<tr><td>Paket / Dokumen</td><td>$data->PACKAGE</td></tr>";
					$char .= "<tr><td>Qty / Pcs</td><td>$data->QTY</td></tr>";
					$char .= "<tr><td>Ukuran Paket</td><td>$data->DIMENS</td></tr>";
					$char .= "<tr><td>Service ( SS, YES, REG )</td><td>$data->SERVICE_TYPE</td></tr>";
					$char .= "<tr><td>Armada (mobil / motor)</td><td>$data->PICKUP_VHC</td></tr>";
					$char .= "<tr><td>Waktu Pick Up</td><td>$pickup_datetime</td></tr>";
					$char .= "<tr><td>Kota Tujuan</td><td>$data->DEST_CITY</td></tr>";
					$char .= "<tr><td>Keterangan lain-lain</td><td>$data->REMARK</td></tr>";
					$char .= "</table><br />";
					//$char .= "<tr><td colspan=2>&nbsp;</td></tr>";
				//} //update hilal 2016-01-02
					$char .= "</body></html>";
					
					//echo $char;
					
					$this->load->library('email');
					
					/*===================================EDIT HILAL 2015-12-19===================================
					
					 
					$resEmail = $this->m_trans->getConfigEmail('Req. Pickup');
					//rint_r($resEmail);
					if($resEmail > 0) {
						$sendTo = "";
						$sendCc = "";
						$sendBcc = "";
						
						$senderEmail = "";
						$senderPassword = "";
						$senderAlias = "";
						$senderSmtp = "";
						$senderSmtpPort = "";
						
						foreach($resEmail as $data) {
							$flag_sender 	= $data->flag_sender;
							$flag_to 		= $data->flag_to;
							$flag_cc 		= $data->flag_cc;
							$flag_bcc 		= $data->flag_bcc;
							
							if($flag_sender == "1"){
								$senderEmail 	= $data->email;
								$senderPassword = $data->password;
								$senderAlias 	= $data->alias_name;
								$senderSmtp 	= $data->smtp;
								$senderSmtpPort = $data->smtp_port;
							}
							
							if($flag_to == "1"){
								$sendTo = $sendTo.$data->email.',';
							}
							
							if($flag_cc == "1"){
								$sendCc = $sendCc.$data->email.',';
							}
							
							if($flag_bcc == "1"){
								$sendBcc = $sendBcc.$data->email.',';
							}
						}
						$sendTo = substr($sendTo, 0, -1);
						$sendCc = substr($sendCc, 0, -1);
						$sendBcc = substr($sendBcc, 0, -1);
						
						
						
					    $config = Array(
									    'protocol' 	=> 'smtp',
									    'smtp_host' => $senderSmtp,
									    'smtp_port' => $senderSmtpPort,
									    'smtp_user' => $senderEmail,
									    'smtp_pass' => $senderPassword,
									    'mailtype'  => 'text', 
									    'charset'   => 'utf-8',
									    'wordwrap' 	=> TRUE,
									    'newline' 	=> '\r\n',
									);
						$this->load->library('email', $config);
						//var_dump($config);
						
						//kirim email
						$this->email->from($senderEmail, $senderAlias);
						$this->email->to($sendTo);
						$this->email->cc($sendCc);
						$this->email->bcc($sendBcc);
						$this->email->set_mailtype("html");
						$this->email->subject("List Pengiriman K-LINK");
						$this->email->message($char);
						/*===================================END EDIT HILAL 2015-12-19=================================
						
						//ori
						if ($this->email->send()){
				            echo "<div class='alert-success'>Email sent successfully..!!</div>"; 
				            foreach($res as $data) {
				            	$arr = $this->m_trans->updateFlagSendConote($data->ORDERNO, $pickup_datetime);  
							}	
				            //$arr = $this->m_trans->updateFlagSendConote($data->ORDERNO, $pickup_datetime);  
				        }else{
				            echo "<div class='alert-error'>There is error in sending mail!</div>";
							echo $this->email->print_debugger();
				        }
				        
						//end ori**
						
					} else {
						 echo "<div class='alert-error'>Please Set Email First.</div>";
					}
			}//update hilal 2016-01-02
			//here for ori**
			} else {
				 echo "<div class='alert-error'>Empty Record</div>";
			}
			
				
	} */

function callApi($api_type='', $api_activity='', $api_input=''){
        $data = array();
        $result = $this->http_post_form("https://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
    }

	function getDetailPickUpProduct($id) {
		$srvReturn = null;	
		$mdReturn = $this->m_trans->getDetailPickUpProduct($id);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getListShippingLabel() {
		//update conote GOSEND
		/*$today=date("Y-m-d");
		$next=date('Y-m-d', strtotime('+1 days', strtotime($today)));

		$this->db = $this->load->database('db_ecommerce', true);
		$qryShpHdr = "SELECT a.*,b.*
		              FROM ecomm_trans_shipaddr a
					  LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
		              WHERE b.datetrans between '$today 00:00:00' AND '$next 00:00:00' AND a.conoteJNE ='' AND
		              (a.service_type_name='SameDay' OR a.service_type_name='Instant')  ";
		$queryShpHc = $this->db->query($qryShpHdr); //get nomor transaksi yang kosong

		if($queryShpHc->num_rows() > 0) {
			$row = $queryShpHc->result();
			$orderno=$row[0]->orderno;
			$lat=$row[0]->lat_dest;
			$long=$row[0]->long_dest;

			$metode = $row[0]->service_type_name;
			$des_name = $row[0]->receiver_name;
			$des_phone = $row[0]->tel_hp1;
			$des_addr = cleanAddress($row[0]->addr1);
			$des_coord = $lat.",".$long;
			$price = $row[0]->total_pay;

			$qryItem = "SELECT a.prdnm,a.qty
						  FROM ecomm_trans_det_prd a
						  WHERE a.orderno = '$orderno'";
			$queryExec = $this->db->query($qryItem);

			if($queryExec->num_rows() > 0) {
				$no=0;
				foreach($queryExec->result() as $row1)
				{
					$no++;
					$prdnm= $row1->prdnm;
					$qty= $row1->qty;
					$concat_item= "(".$prdnm." : ".$qty." pc)";
					$item_arr[]= $concat_item;
					$item=implode(" ",$item_arr);
				}
			}

			$senderName="Bagian Warehouse";
			$senderNote="K-Mart Lobby";
			$senderContact="K-Link Indonesia";
			$senderPhone="081388319355";
			$senderCoord="-6.2400292,106.8343388";
			$senderAlamat="Jalan Jend. Gatot Subroto Kav. 59A Jakarta Selatan";

			//get user key gosend
			$this->load->model('lokasi/M_location', 'M_location');
			$getKey= $this->M_location->getUserKey();
			$user_key= $getKey[0]->username;
			$pass_key= $getKey[0]->key_user;

			//mendapatkan jam buka gosend
			$day = date('D');
			$now = new Datetime("now");
			$type_service=$row[0]->service_type_name;

			$hp= $row[0]->tel_hp1;
			$hp_val=str_replace("62","0",$hp);
			$hp_val=str_replace("+","",$hp_val);
			$hp_val=str_replace("-","",$hp_val);
			$hp_val=str_replace(" ","",$hp_val);

			$hp1="081388319355";
			$hp2="08568369705";

			//Instant
			$getInstant= $this->M_location->getSchedule($day,'Instant');
			$open_inst= $getInstant[0]->opened;
			$closed_inst= $getInstant[0]->closed_pay; //batas akhir konfirmasi pembayaran
			$begintime1 = new DateTime($open_inst);
			$endtime1 = new DateTime($closed_inst);

			$inst_open=date('H:i:s', strtotime($open_inst));
			$inst_closed=date('H:i:s', strtotime($closed_inst));

			//SameDay
			$getSame= $this->M_location->getSchedule($day,'SameDay');
			$open_same= $getSame[0]->opened;
			$closed_same= $getSame[0]->closed_pay; // batas akhir konfirmasi pembayaran
			$begintime2 = new DateTime($open_same);
			$endtime2 = new DateTime($closed_same);

			$same_open=date('H:i:s', strtotime($open_same));
			$same_closed=date('H:i:s', strtotime($closed_same));

			//CALL API GOSEND
			if($type_service == 'Instant'){
				if($now >= $begintime1 && $now <= $endtime1) {
					$dt = booking_gsend($user_key,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
					$result= json_decode($dt, TRUE);
					$order_g=$result['orderNo'];

					//sms ke gudang
					$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
					smsTemplate($hp1, $txt);
					smsTemplate($hp2, $txt);
				}else{
					$order_g="";
					$text = "Terimakasih telah melakukan pembelanjaan di K-Link, jadwal pickup GOSEND Instant ".$inst_open." WIB - ".$inst_closed." WIB. Pesanan Anda akan segera dikirimkan di hari kerja besok.";
					smsTemplate($hp_val, $text);
				}
			} else if($type_service == 'SameDay'){
				if($now >= $begintime2 && $now <= $endtime2){
					$dt = booking_gsend($user_key, $pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
					$result= json_decode($dt, TRUE);
					$order_g=$result['orderNo'];

					//sms ke gudang
					$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
					smsTemplate($hp1, $txt);
					smsTemplate($hp2, $txt);

				} else {
					$order_g="";
					$text = "Terimakasih telah melakukan pembelanjaan di K-Link, jadwal pickup GOSEND SameDay ".$same_open." WIB - ".$same_closed." WIB. Pesanan Anda akan segera dikirimkan pada hari kerja besok. ";
					smsTemplate($hp_val, $text);
				}

			}else{
				$order_g="";
				$text = "Terimakasih telah melakukan pembelanjaan di K-Link, pesanan Anda akan segera dikirimkan di hari kerja besok";
				smsTemplate($hp_val, $text);
			}

			$y = $this->mm_trans->updateConotByOrderNo($orderno,$order_g);

		}*/

		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = $this->m_trans->getListShippingLabel($data['prt_form'], $data['prt_to'], $data['print_count'], $data['list_shipping']);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getDetailProductByOrderNo($orderno) {
		$srvReturn = null;
		$mdReturn = $this->m_trans->getDetailProductByOrderNo($orderno);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getPrintShippingPDF($orderno) {
		$srvReturn = null;
		//$data = $this->input->post(NULL, TRUE);
		//$param = set_list_array_to_string($data['orderno']);
		$param = set_list_array_to_string($orderno);
        
		$mdReturn = $this->m_trans->printLabelShipping($param);
		if($mdReturn !=  null) {
			$upd = $this->m_trans->updateStatusPrintShipping($param);
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getPrintShippingPDF_2($orderno) {
		$srvReturn = null;
		//$data = $this->input->post(NULL, TRUE);
		//$param = set_list_array_to_string($data['orderno']);
		$param = set_list_array_to_string($orderno);
        
		$mdReturn = $this->m_trans->printLabelShipping($param);
		if($mdReturn !=  null) {
			$upd = $this->m_trans->updateStatusPrintShipping($param);
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getPrintShippingPDF_2TEs($orderno) {
		$srvReturn = null;
		//$data = $this->input->post(NULL, TRUE);
		//$param = set_list_array_to_string($data['orderno']);
		$param = set_list_array_to_string($orderno);
        
		$mdReturn = $this->m_trans->printLabelShippingTes($param);
		//print_r($mdReturn);
		if($mdReturn !=  null) {
			$upd = $this->m_trans->updateStatusPrintShipping($param);
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function deleteTransaction($orderid) {
		$srvReturn = jsonFalseResponse("$orderid failed to be deleted...");
		$mdReturn = $this->m_trans->deleteTransaction($orderid);
		if($srvReturn >  0 || $srvReturn == null) {
			$delMemb = $this->m_trans->deleteTrxIfMember($orderid);
			$srvReturn = jsonTrueResponse(NULL, "$orderid deleted successfully..");	
		}
		return $srvReturn;
		
	}
	
	function saveImportToDB($data) {
		$srvReturn = $this->m_trans->saveImportToDB($data);
		return $srvReturn;
	}
	
	function saveImportToDBGW($data) {
		$srvReturn = $this->m_trans->saveImportToDBGW($data);
		return $srvReturn;
	}
	
	function updateSgoTrxId($data) {
		$srvReturn = $this->m_trans->updateSgoTrxId($data);
		return $srvReturn;
	}
	
	function getListSGOTrx($data) {
		//$srvReturn = $this->m_trans->getListSGOTrx($data);
		$srvReturn = $this->m_trans->getListSGOTrxForReconcile($data);
		return $srvReturn;
	}

	function getListManualTrx($data) {
		//$srvReturn = $this->m_trans->getListSGOTrx($data);
		$srvReturn = $this->m_trans->getListManualTrxForReconcile($data);
		return $srvReturn;
	}
	
	function getListSGOTrxPpob($data) {
		$srvReturn = $this->m_trans->getListSGOTrxPpob($data);
		return $srvReturn;
	}
	
	function getListSGOTrxPpobById($data) {
		$srvReturn = $this->m_trans->getListSGOTrxPpobById($data);
		return $srvReturn;
	}
	
	function saveTrxToDB($orderid) {
		//$srvReturn = $this->m_trans->saveTrxToDB($orderid);
		//return $srvReturn;
		
		$srvReturn = jsonFalseResponse("$orderid failed to reconcile...");
		$mdReturn = $this->m_trans->saveTrxToDB($orderid);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = jsonTrueResponse(NULL, "$orderid reconcile successfully..");	
		}
		return $srvReturn;
	}
	
	function saveMEMToDB($memb_status, $memberid, $orderid) {
		$srvReturn = jsonFalseResponse("$orderid failed to reconcile...");
		
		$cekValidMember = $this->m_trans->checkValidDataNewMember($orderid);
		if($cekValidMember['response'] == "true") {
			$mdReturn = $this->m_trans->saveMEMToDB($memb_status, $memberid, $orderid);
			if($mdReturn['insSuccess'] != 0) {
				$srvReturn = jsonTrueResponse(NULL, "$orderid -- $memberid reconcile successfully..");	
			}
			else if($mdReturn['insSuccess'] == 0) {
			    $et = array("response" => "false", "message" => $mdReturn['message']);
				$srvReturn = $et;
			}
		} else {
			$srvReturn = $cekValidMember;
		}
		return $srvReturn;
	}
	
	function getListBankCharge() {
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->getListBankCharge();
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListBankChargeByParam($param, $value) {
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->getListBankChargeByParam($param, $value);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function postSaveBankCharge($data) {
		$srvReturn = setErrorMessage("Insert Bank Charge failed..!");	
		$arr = $this->m_trans->saveBankCharge($data);
		if($arr > 0) {
			$srvReturn = setSuccessMessage("Insert Bank Charge success..!");
		}
		return $srvReturn;
	}
	
	function postUpdateBankCharge($data) {
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->updateBankCharge($data);
		if($arr > 0) {
			$srvReturn = jsonTrueResponse(NULL, "Bank Charge $data[bankDisplayNm] successfully updated..!");
		}
		return $srvReturn;
	}
	
	function getListVoucherByParam($searchby, $param1, $param2=null, $param3=null) {
		$data = $this->input->post(NULL, TRUE);
		$srvReturn = null;
		$mdReturn = $this->m_trans->getListVoucherByParam($searchby, $param1, $param2, $param3);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function getCheckConoteIsNULL($orderno) {
		$srvReturn = null;
		$mdReturn = $this->m_trans->getCheckConoteIsNULL($orderno);
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}
	
	function postUpdateCnoteByOrderno() {
		$data = $this->input->post(NULL, TRUE);
		//var_dump($data);
		$srvReturn = jsonFalseResponse();	
		$arr = $this->m_trans->updateCnoteByOrderno($data);
		if($arr > 0) {
			$srvReturn = array('message' => "Conote for $data[orderno] successfully updated..!", 
				               'response' => "true",
				               'conoteno' => "$data[conoteno]"); //jsonTrueResponse(NULL, "Conote for $data[orderno] successfully updated..!");
		} 
		return $srvReturn;
	}
	
	function getListBank() {
		$srvReturn = null;
		$mdReturn = $this->m_trans->getListBank();
		if($srvReturn >  0 || $srvReturn == null) {
			$srvReturn = $mdReturn;
		}
		return $srvReturn;
	}


	function getListSP() {
		$srvReturn = jsonFalseResponse();
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListSP($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
	}
	
	function getListSPBlmKirim() {
		$srvReturn = jsonFalseResponse();
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->m_trans->getListSPBlmKirim($data);
		if($arr != null) {
			$srvReturn = jsonTrueResponse($arr);
		}
		return $srvReturn;
		
	}

	function getSPReportToPrint() {
		$data = $this->input->post(NULL, TRUE);
		$mdReturn = $this->m_trans->getSPReportToPrint($data);
		/*if($data['cnPrintStt'] == "0") {
			$param = set_list_array_to_string($data['cnno']);
			$upd = $this->m_trans->updateSPStatus($param, "1");
		}*/
		return $mdReturn;
	}
	
}
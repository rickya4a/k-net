<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|getListNoConot
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|<ins></ins>
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['cargo/price'] = 'webshop/shipping/checkHargaPengiriman';
$route['utkagus'] = 'webshop/tes_paktoto/tesDion';
$route['sgo/tes'] = 'webshop/api_payment_reconcile/sgoTes';

$route['testdev'] = 'welcome/tesDion';


$route['cargo/price'] = 'webshop/shipping/checkHargaPengiriman';
$route['promo/agust2018'] = 'webshop/cartshop/showListPromoAgustus2018';
$route['tesAPI/tesAPI'] = 'tesAPI/TesAPI/APIPOST';

$route['default_controller'] = 'product/getProduct'; // old route
$route[''] = 'product/getProduct';					// old route
//$route['default_controller'] = 'product/redirectGetProduct'; // new route (2019-04-03)
//$route[''] = 'product/redirectGetProduct';
$route['shop/product2'] = 'product/getProduct2';
///$route['404_override'] = 'welcome/error404';
//$route['404_override'] = 'Error';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['testdb'] = "welcome/testdb";
$route['phpinfo'] = "welcome/info";

$route['memb/log'] = 'backend/backend_api/membLogin';
$route['native/set'] = 'webshop/cartshop_customer/tesNative';
$route['native/get'] = 'webshop/cartshop_customer/tesNativeGet';
$route['native/out'] = 'webshop/cartshop_customer/nativeLogout';

$route['jatis/sms'] = 'webshop/account/tesJatisSms';

$route['tesEmail'] = 'webshop/payment/tesSendEmail';
$route['tes/sms/memb/(:any)'] = 'webshop/member_payment/tesSMS/$1';
$route['tes/sms/ecom/(:any)'] = 'webshop/payment/getTrxByTokenIDx/$1';
$route['tes/vch'] = 'webshop/member_payment/tesVch';
$route['promo/listing'] = 'product/listPromo';
$route['api/gojek/origin/list'] = 'audio_store/sicepatListOrigin';

$route['emailtes'] = 'sendEmail/tesFalconide';
$route['sendmail/(:any)'] = 'sendEmail/tesSendEmail/$1';

/* ----------------------
 *  TES API SI CEPAT
 * ---------------------*/

$route['api/sicepat/origin/list'] = 'API_Sicepat/ListOrigin';
$route['api/sicepat/destination/list'] = 'API_Sicepat/ListDestination';
$route['api/sicepat/tarif/(:any)/(:any)/(:any)'] = 'API_Sicepat/Tarif/$1/$2/$3';
$route['api/sicepat/tracking/(:any)'] = 'API_Sicepat/Waybill/$1';
$route['api/sicepat/getresi/(:any)'] = 'API_Sicepat/getResi/$1';

$route['sicepat/tracking'] = 'tesAPI/TesAPIsicepat/formInput';
$route['api/sicepat/tracking'] = 'tesAPI/TesAPIsicepat/tracking';

$route['sicepat/tarif'] = 'tesAPI/TesAPIsicepat/formInputTarif';
$route['api/sicepat/tarif'] = 'tesAPI/TesAPIsicepat/tarif';

/*---------------------------
 * PAYMENT ONLINE STOCKIST
 * -------------------------*/
$route['stk/login'] = "knet_pay_stk/auth";
$route['stk/login/auth'] = "knet_pay_stk/getAuthStk";
$route['stk/logout'] = "knet_pay_stk/logout";
$route['stk/trx'] = "knet_pay_stk/formListTrxToPay";
$route['stk/trx/list'] = "knet_pay_stk/getListTrxToPay";
$route['stk/trx/ssr/(:any)'] = "knet_pay_stk/getDetailTTPbySSRno/$1";
$route['stk/trx/preview'] = "knet_pay_stk/previewSelectedSSR";
$route['stk/pay/inquiry/dev'] = 'knet_pay_stk/getResponseFromSGODev';
$route['stk/pay/notif/dev'] = 'knet_pay_stk/notifAfterPaymentDev';
$route['stk/pay/finish/dev/(:any)'] = 'knet_pay_stk/afterPaymentWithSGODev/$1';

$route['stk/pay/inquiry'] = 'knet_pay_stk/getResponseFromSGO';
$route['stk/pay/notif'] = 'knet_pay_stk/notifAfterPayment';
$route['stk/pay/finish/(:any)'] = 'knet_pay_stk/afterPaymentWithSGO/$1';
$route['tes/inquiry/(:any)'] = 'webshop/payment/tesOrderInq/$1';

/*--------------------------
 * ONLINE TICKET
 * ------------------------ */
$route['yoga'] = 'ticket/onlineticket';
$route['event'] = 'ticket/event';
$route['sgo_test'] = 'ticket/event/sgo_test';
$route['ticketing'] = 'ticket/ticket';
$route['ticketing/payment'] = 'ticket/payment';
$route['ticketing/preview'] = 'ticket/payment/getPreview';
$route['ticketing/input/finish'] = 'ticket/payment/inputByTrf';
$route['kirim'] = 'ticket/emailexample';

$route['ticketing/sgo/inquiryDev'] = 'ticket/payment/getInquirySgoDev';
$route['ticketing/sgo/notifDev'] = 'ticket/payment/postPaymentTicketSgoDev';
$route['ticketing/sgo/inquiry'] = 'ticket/payment/getInquirySgo';
$route['ticketing/sgo/notif'] = 'ticket/payment/postPaymentTicketSgo';
$route['ticketing/finish/payment/(:any)'] = 'ticket/payment/finishPaymentTicket/$1';
$route['ticketing/print'] = 'ticket/payment/printPDFTicket';

/*-------------------
 * API HELPER
 * -----------------*/
$route['helper/klink_mlm2010/(:any)/(:any)/(:any)'] = "backend/backend_api/helperCheckTable/$1/$2/$3";

$route['db1/get/(:any)/from/(:any)/(:any)/(:any)'] = "backend/backend_api/getRetrieveFieldDB1/$1/$2/$3/$4";
$route['db1/list/from/(:any)/(:any)/(:any)'] = "backend/backend_api/getRetrieveAllFieldB1/$1/$2/$3";

$route['db2/get/(:any)/from/(:any)/(:any)/(:any)'] = "backend/backend_api/getRetrieveFieldDB2/$1/$2/$3/$4";
$route['db2/list/from/(:any)/(:any)/(:any)'] = "backend/backend_api/getRetrieveAllFieldDB2/$1/$2/$3";
//$route['db2/get/(:any)/list'] = "backend/backend_api/getAllDataFromTable/$1";
$route['db2/get/(:any)/where/(:any)/(:any)'] = "backend/backend_api/getDataWithSpecificField/$1/$2/$3";

$route['db4/get/(:any)/from/(:any)/(:any)/(:any)'] = "backend/backend_api/getRetrieveFieldDB4/$1/$2/$3/$4";
$route['db4/list/from/(:any)/(:any)/(:any)'] = "backend/backend_api/getRetrieveAllFieldDB4/$1/$2/$3";
$route['db2/delete/from/(:any)/(:any)/(:any)'] = "backend/backend_api/deleteFromTable/$1/$2/$3";
$route['api/email/id/(:any)'] = "backend/backend_api/getDetailEmail/$1";
$route['api/email'] = "backend/backend_api/getEmail/";

//$route['helper/klink/db_ecommerce/(:any)/(:any)/(:any)'] = "backend/backend_api/getFullNameByID/$1/$2/$3";

/*-----------------------------
 * NEWERA REWRITE
 * ----------------------------*/
$route['be/klink/incoming/update'] = 'backend/newera/be_newera_trx/formUpdateIncomingPayment';
$route['be/klink/incoming/detail'] = 'backend/newera/be_newera_trx/getDetailIncomingPayment';
$route['be/klink/incoming/fullname/(:any)/(:any)'] = 'backend/newera/be_newera_trx/getDetailFullname/$1/$2';
$route['be/klink/incoming/update/save'] = 'backend/newera/be_newera_trx/saveUpdateIncomingPayment';
$route['be/klink/stockist/search'] = 'backend/newera/be_newera_stockist/formSearchStockist';
$route['be/klink/stockist/search/list'] = 'backend/newera/be_newera_stockist/getSearchStockist';
$route['be/klink/stockist/id/(:any)'] = 'backend/newera/be_newera_stockist/getSearchStockistByID/$1';
$route['be/klink/stockist/skCoffee'] = 'backend/newera/be_newera_stockist/skCoffeeForm';
$route['be/klink/stockist/skCoffee/list'] = 'backend/newera/be_newera_stockist/getSearchSKCoffee';
$route['be/klink/stockist/skCoffee/delete/(:any)'] = 'backend/newera/be_newera_stockist/skCoffeeDel/$1';
$route['be/klink/trx/update'] = 'backend/newera/be_newera_trx/formUpdateDistTrx';
$route['be/klink/trx/(:any)/(:any)'] = 'backend/newera/be_newera_trx/getListTrxByID/$1/$2';
$route['be/klink/stockist/barcode_check'] = 'backend/newera/be_newera_stockist/barcode_checkForm';
$route['be/klink/stockist/barcode_check/list'] = 'backend/newera/be_newera_stockist/getBarcodeList';
$route['be/klink/stockist/barcode_check/det/(:any)'] = 'backend/newera/be_newera_stockist/getBarcodeDet/$1';
$route['be/klink/shipping'] = 'backend/newera/be_newera_trx/formUpdateShipping';
$route['be/klink/shipping/kw/(:any)'] = 'backend/newera/be_newera_trx/getDataShippingByKW/$1';
$route['be/klink/shipping/update'] = 'backend/newera/be_newera_trx/postUpdateShipping';
//END
/*--------------------------
 * LOGIN CONTROLLER ADMIN
 * ------------------------ */
$route['backend'] = 'backend/backend_login';
$route['backend/login'] = 'backend/backend_login/handleLogin';
$route['backend/login/err'] = 'backend/backend_login/login_err';
$route['backend/logout'] = 'backend/backend_login/handleLogout';
$route['backend/main'] = 'backend/backend_login/mainMenu';
$route['backend/main2'] = 'backend/backend_login/mainMenu2';

/*---------------------------
 * BACKEND FOR MOBILE | masih tahap development
 --------------------------*/
// $route['backend/mobile'] = 'backend/mobile/be_login';
// $route['backend/mobile/login'] = 'backend/mobile/be_login/actLogin';
// $route['backend/mobile/dashboard'] = 'backend/mobile/be_dashboard';
// $route['backend/mobile/sesslogin'] = 'backend/mobile/be_login/showSessLogin';
// $route['backend/mobile/logout'] = 'backend/mobile/be_login/logout';

// $route['be_mobile/getname'] = 'backend/mobile/event/getName';

// $route['be_mobile/user'] = 'backend/mobile/user';
// $route['be_mobile/event/pin'] = 'backend/mobile/event';
// $route['be_mobile/event/getBV'] = 'backend/mobile/event/getTotalBv';


/*-------------------------------
 * APPLICATION MAINTENANCE ADMIN
 * ------------------------------ */
$route['app'] = 'backend/userconfig/getInputApplication';
$route['app/id/(:any)'] = 'backend/userconfig/getListApplicationByID/$1';
$route['app/list'] = 'backend/userconfig/getListAllApplication';
$route['app/list/json'] = 'backend/userconfig/getListAllApplication/$1';
$route['app/list/(:any)/(:any)'] = 'backend/userconfig/getListApplication/$1/$2';
$route['app/save'] = 'backend/userconfig/saveInputApplication';
$route['app/update'] = 'backend/userconfig/saveUpdateApplication';
$route['app/delete/(:any)'] = 'backend/userconfig/deleteApplication/$1';

/*--------------------------
 * USER MAINTENANCE ADMIN
 * ------------------------ */
$route['user'] = 'backend/userconfig/getInputUser';
$route['user/id/(:any)'] = 'backend/userconfig/getListUserByID/$1';
$route['user/save'] = 'backend/userconfig/saveInputUser';
$route['user/list'] = 'backend/userconfig/getListAllUser';
$route['user/list/(:any)/(:any)'] = 'backend/userconfig/getListUser/$1/$2';
$route['user/update'] = 'backend/userconfig/saveUpdateUser';
$route['user/delete/(:any)'] = 'backend/userconfig/deleteUser/$1';

/*--------------------------
 * USER GROUP MAINTENANCE ADMIN
 * ------------------------ */
$route['user/group'] = 'backend/userconfig/getInputUserGroup';
$route['user/group/id/(:any)'] = 'backend/userconfig/getListUserGroupByID/$1';
$route['user/group/list'] = 'backend/userconfig/getListAllUserGroup';
$route['user/group/list/json'] = 'backend/userconfig/getListAllUserGroup/$1';
$route['user/group/list/(:any)/(:any)'] = 'backend/userconfig/getListUserGroup/$1/$2';
$route['user/group/save'] = 'backend/userconfig/saveInputUserGroup';
$route['user/group/update'] = 'backend/userconfig/saveUpdateUserGroup';
$route['user/group/delete/(:any)'] = 'backend/userconfig/deleteUserGroup/$1';

/*--------------------------
 * GROUP MENU MAINTENANCE ADMIN
 * ------------------------ */
$route['menu/group'] = 'backend/userconfig/getInputGroupMenu';
$route['menu/group/list'] = 'backend/userconfig/getListAllGroupMenu';
$route['menu/group/list/json'] = 'backend/userconfig/getListAllGroupMenu/$1';
$route['menu/group/id/(:any)'] = 'backend/userconfig/getListGroupMenuByID/$1';
$route['menu/group/list/(:any)/(:any)'] = 'backend/userconfig/getListGroupMenu/$1/$2';
$route['menu/group/save'] = 'backend/userconfig/saveInputGroupMenu';
$route['menu/group/update'] = 'backend/userconfig/saveUpdateGroupMenu';
$route['menu/group/delete/(:any)'] = 'backend/userconfig/deleteGroupMenu/$1';

/*--------------------------
 * SUB MENU MAINTENANCE ADMIN
 * ------------------------ */
$route['menu'] = 'backend/userconfig/getInputSubMenu';
$route['menu/list'] = 'backend/userconfig/getListAllSubMenu';
$route['menu/list/json'] = 'backend/userconfig/getListAllSubMenu/$1';
$route['menu/id/(:any)'] = 'backend/userconfig/getListSubMenuByID/$1';
$route['menu/list/(:any)/(:any)'] = 'backend/userconfig/getListSubMenu/$1/$2';
$route['menu/save'] = 'backend/userconfig/saveInputSubMenu';
$route['menu/update'] = 'backend/userconfig/saveUpdateSubMenu';
$route['menu/delete/(:any)'] = 'backend/userconfig/deleteSubMenu/$1';

/*--------------------------
 * USER GROUP PRIVELEDGE MENU
 * ------------------------ */
$route['menu/access'] = 'backend/userconfig/getInputAccessMenu';
$route['menu/check'] = 'backend/userconfig/getShowListMenuByGroupID';
$route['menu/access/save'] = 'backend/userconfig/saveInputAccessMenu';

/*-----------------------------------
 * HO-FI TRANSACTION
 * ---------------------------------*/
$route['hifi/trx/importFile'] = 'backend/hifi/importFormFile';
$route['hifi/trx/importFile/preview'] = 'backend/hifi/previewListHifiTrxFromFile';
$route['hifi/trx/importFile/save'] = 'backend/hifi/importHifiTrxFromFile';
$route['hifi/trx/import'] = 'backend/hifi/importForm';
$route['hifi/trx/import/preview'] = 'backend/hifi/previewListHifiTrx';
$route['hifi/trx/import/save'] = 'backend/hifi/importHifiTrx';
$route['hifi/tesapi'] = 'backend/hifi/tesAPI';
$route['hifi/trx/report'] = 'backend/hifi/reportHifiTrx';
$route['hifi/trx/report/list'] = 'backend/hifi/getListReportHifiTrx';
$route['hifi/trx/id/(:any)'] = 'backend/hifi/getDetHifiTrx/$1';


$route['sgo_ppob/trxtype/list/(:any)'] = 'backend/hifi/listTrxTypeByPPOB/$1';
$route['sgo_ppob/trx'] = 'backend/hifi/sgoPpobTrx';
$route['sgo_ppob/importProduct'] = 'backend/hifi/sgoPpobImportPrd';
$route['sgo_ppob/trx/listByCat/(:any)'] = 'backend/hifi/listPpobTrxByCat/$1';
$route['sgo_ppob/inquiry'] = 'backend/hifi/sgoPpobInquiry';
$route['sgo_ppob/inquiry/show'] = 'backend/hifi/sgoPpobInquiryShow';
$route['sgo_ppob/inquiryPay'] = 'backend/hifi/sgoPpobInquiryPayHifi';
$route['sgo_ppob/inquiryPay/show'] = 'backend/hifi/sgoPpobInquiryPayShow';
$route['sgo_ppob/inquiry/bill/status'] = 'backend/hifi/sgoPpobInquiryPayStatus';
$route['sgo_ppob/inquiry/pay'] = 'backend/hifi/sgoPpobInqPay';
//$route['sgo_ppob/trx'] = 'backend/hifi/getListProductSgoPpob';
//$route['sgo_ppob/url/(:any)'] = 'backend/hifi/getListProductSgoPpob/$1';
$route['sgo_ppob/trx/fe'] = 'backend/hifi/sgoPpobTrxFrontEnd';
$route['sgo_ppob/saveTemp'] = 'backend/hifi/saveTempTrx';
$route['sgo_ppob/trx/id/(:any)'] = 'backend/hifi/getResultTrxByID/$1';
$route['sgo_ppob/signTes/(:any)/(:any)'] = 'backend/hifi/hasilSign/$1/$2';

$route['sgo_ppob/detail/(:any)'] = 'backend/hifi/getResultTrxByID2/$1';

$route['sgo_ppob/save/va'] = 'backend/hifi/sgoPpobSaveVa';
$route['ppob/va/result/(:any)'] = 'backend/hifi/getDataInsertPpobTrxVa/$1';

$route['xl/paketdata/fe'] = 'backend/hifi/XlPaketDataFE';
$route['xl/saveTemp'] = 'backend/hifi/XlPaketDataSaveTemp';
$route['xl/trx/id/(:any)'] = 'backend/hifi/getTrxXLByID/$1';
$route['xl/trx/msidn/(:any)'] = 'backend/hifi/getDataMemberXLbyMsidn/$1';
$route['xl/get/encryptkey'] = 'backend/hifi/getEncryptKey';
$route['xl/get/stock'] = 'backend/hifi/getXlPaketDataStock';
$route['xl/stock/report'] = 'backend/hifi/XlPaketDataStockReport';
$route['xl/stock/report/acc'] = 'backend/hifi/XlPaketDataStockReportForAcc';

$route['xl/redeem/stock'] = 'backend/hifi/redeemPaketDataStock';
$route['xl/paketData/saveTemp'] = 'backend/hifi/XlPaketDataSaveTemp';
$route['xl/paketData/save/va'] = 'backend/hifi/XlPaketDataSaveVa';

$route['xl/redeem/manual'] = 'backend/hifi/redeemStockXLManual';

$route['xl/checkencrypt'] = 'api_xl_encrypt/checkEncrypt';
$route['xl/generatekey'] = 'api_xl_encrypt/generateEncryptionXL';
$route['xl/getencryptiondate'] = 'api_xl_encrypt/getEncryptionDate';
$route['xl/generateEncrypt/(:any)'] = 'api_xl_encrypt/createEncryptKey/$1';
$route['xl/encrypt/generate'] = 'api_xl_encrypt/createEncryptKeyCRON';
$route['xl/redeem/status/(:any)'] = 'api_xl_encrypt/checkXlRedeemStatus/$1';

$route['xl/va/result/(:any)'] = 'backend/hifi/getDataInsertXLTrxVa/$1';

$route['va/invoice/send'] = 'backend/hifi/VaSendInvoice/$1';
$route['va/invoice/send/post'] = 'backend/hifi/sendInvoiceVatoSgo';
$route['va/history/trx'] = 'backend/transaction/vaHistoryTrx';
$route['va/history/trx/getVar/(:any)/(:any)'] = 'backend/transaction/getMemberSaldo/$1/$2';
$route['va/history/trx/get'] = 'backend/transaction/vaHistoryTrxReportList';
$route['va/history/trx/fe'] = 'backend/hifi/vaHistoryTrx';
$route['va/history/trx/list'] = 'backend/hifi/vaHistoryTrxList';
$route['va/balance/rekap'] = 'backend/transaction/VaBalanceRekap';
$route['va/saldo/member'] = 'backend/transaction/rekapSaldoMember';
$route['va/mutasiTrx'] = 'backend/transaction/mutasiTrx';
$route['va/mutasiTrx/excel'] = 'backend/transaction/mutasiTrxExcel';
$route['va/rekapTrx/member/(:any)'] = 'backend/transaction/rekapTrxmember/$1';
$route['va/member/save'] = 'backend/hifi/saveVaMemb';

$route['xl/paketData/cash/(:any)/(:any)/(:any)'] = 'backend/hifi/XlPaketDataSaveCash/$1/$2/$3';

$route['ppob/rekap/trx'] = 'backend/hifi/ppobRekapTrx';
$route['ppob/rekap/trx/result'] = 'backend/hifi/getListPpobRekapTrx';
/*--------------------------
 * PRODUCT
 * ------------------------ */
$route['product'] = 'backend/product/getInputProduct';
$route['productANP'] = 'backend/product/getInputProductANP';
$route['bannerANP'] = 'backend/product/getInputBannerANP';
$route['bannerANP/save'] = 'backend/product/saveInputBanner';
$route['bannerANP/update'] = 'backend/product/saveUpdateBanner';

$route['bannerANP/list'] = 'backend/product/getListAllBanner';
$route['bannerANP/list/(:any)/(:any)'] = 'backend/product/getListBanner/$1/$2';


$route['product/list'] = 'backend/product/getListAllProduct';
$route['product/list/json'] = 'backend/product/getListAllProduct/$1';
$route['product/id/(:any)'] = 'backend/product/getListProductByID/$1';
$route['product/list/(:any)/(:any)'] = 'backend/product/getListProduct/$1/$2';
$route['product/save'] = 'backend/product/saveInputProduct';
$route['product/update'] = 'backend/product/saveUpdateProduct';
$route['product/delete/(:any)'] = 'backend/product/deleteProduct/$1';
$route['product/claim'] = 'backend/product/formProductClaim';
$route['product/claim/list'] = 'backend/product/getProductClaim';
$route['product/claim/detail/(:any)'] = 'backend/product/getProductClaimDetail/$1';
//import product from nanas to db commerce
$route['product/pine/import'] = 'backend/product/formImportProductPine';
$route['product/pine/list/id'] = 'backend/product/getListProductPineByID';
$route['product/pine/import/save'] = 'backend/product/saveImportProductFromPine';

$route['product/search'] = 'backend/product_search/productSearch';
$route['product/search/list'] = 'backend/product_search/productSearchByParam';
$route['product/id/(:any)/(:any)'] = 'backend/product_search/productSearchByID/$1/$2';

$route['product/status/update'] = 'backend/product_search/updateProductStatus';


/*--------------------------
 * PRODUCT  BUNDLING
 * ------------------------ */
$route['product/bundling'] = 'backend/product/getInputProductBundling';
$route['product/bundling/list'] = 'backend/product/getListAllProductBundling';
$route['product/bundling/list/json'] = 'backend/product/getListAllProductBundling/$1';
$route['product/bundling/id/(:any)'] = 'backend/product/getListProductBundlingByID/$1';
$route['product/bundling/id2'] = 'backend/product/getListProductBundlingByIDPost';
$route['product/bundling/list/(:any)/(:any)'] = 'backend/product/getListProductBundling/$1/$2';
$route['product/bundling/save'] = 'backend/product/saveInputProductBundling';
$route['product/bundling/update'] = 'backend/product/saveUpdateProductBundling';
$route['product/bundling/delete/(:any)'] = 'backend/product/deleteProductBundling/$1';
//$route['product/bundling/helper/id/(:any)'] = 'backend/product/helperProductID/$1';

/*--------------------------
 * PRODUCT  CATEGORY
 * ------------------------ */
$route['product/cat'] = 'backend/product/getInputProductCat';
$route['product/cat/list'] = 'backend/product/getListAllProductCat';
$route['product/cat/list/json'] = 'backend/product/getListAllProductCat/$1';
$route['product/cat/id/(:any)'] = 'backend/product/getListProductCatByID/$1';
$route['product/cat/list/(:any)/(:any)'] = 'backend/product/getListProductCat/$1/$2';
$route['product/cat/save'] = 'backend/product/saveInputProductCat';
$route['product/cat/update'] = 'backend/product/saveUpdateProductCat';
$route['product/cat/delete/(:any)'] = 'backend/product/deleteProductCat/$1';

/*--------------------------
 * PRODUCT PRICE
 * ------------------------ */
$route['product/price'] = 'backend/product/getInputProductPrice';
$route['product/price/check'] = 'backend/product/checkProductPriceCode';
$route['product/price/list'] = 'backend/product/getListAllProductPrice';
$route['product/price/list/json'] = 'backend/product/getListAllProductPrice/$1';
$route['product/price/id/(:any)'] = 'backend/product/getListProductPriceByID/$1';
$route['product/price/list/(:any)/(:any)'] = 'backend/product/getListProductPrice/$1/$2';
$route['product/price/save'] = 'backend/product/saveInputProductPrice';
$route['product/price/update'] = 'backend/product/saveUpdateProductPrice';
$route['product/price/delete/(:any)'] = 'backend/product/deleteProductPrice/$1';


/*--------------------------
 * PRICE CODE
 * ------------------------ */
$route['pricecode'] = 'backend/product/getInputPriceCode';
$route['pricecode/list'] = 'backend/product/getListAllPriceCode';
$route['pricecode/list/json'] = 'backend/product/getListAllPriceCode/$1';
$route['pricecode/id/(:any)'] = 'backend/product/getListPriceCodeByID/$1';
$route['pricecode/list/(:any)/(:any)'] = 'backend/product/getListPriceCode/$1/$2';
$route['pricecode/postlist/(:any)'] = 'backend/product/postListPriceCode/$1';
$route['pricecode/save'] = 'backend/product/saveInputPriceCode';
$route['pricecode/update'] = 'backend/product/saveUpdatePriceCode';
$route['pricecode/delete/(:any)'] = 'backend/product/deletePriceCode/$1';

/*--------------------------
 * BACKEND TICKET
 * ------------------------ */
$route['be/ticket/payment'] = 'backend/backend_ticket/getListPaymentTicket';
$route['be/ticket/list'] = 'backend/backend_ticket/getListTicket';
$route['be/ticket/listResult'] = 'backend/backend_ticket/getListTicketResult';
$route['be/ticket/payment/list'] = 'backend/backend_ticket/getListPaymentTicketResult';
$route['be/ticket/payment/approve/(:any)'] = 'backend/backend_ticket/approvePaymentTicket/$1';
$route['be/ticket/payment/report'] = 'backend/backend_ticket/getReportPayment';
$route['be/ticket/payment/report/list'] = 'backend/backend_ticket/getReportPaymentResult';
$route['be/ticket/event/detail/(:any)'] = 'backend/backend_ticket/getDetailEvent/$1';
$route['be/ticket/location'] = 'backend/backend_ticket/getTicketLocationReport';
$route['be/ticket/location/list'] = 'backend/backend_ticket/getTicketLocationReportResult';
$route['be/ticket/location/listXls'] = 'backend/backend_ticket/getTicketLocationReportResultXls';
$route['be/ticket/sms'] = 'backend/backend_ticket/getTicketSMS';
$route['be/ticket/sms/list'] = 'backend/backend_ticket/getTicketSMSList';
$route['be/ticket/sms/send/(:any)'] = 'backend/backend_ticket/resendTicketSMS/$1';

/*-----------------------------
 *      BACKEND TRANSACTION
 * ---------------------------- */

$route['trans/ship/invoice'] = 'backend/transaction/formShippingList';
$route['trans/ship/invoice/list'] = 'backend/transaction/getShipInvList';
$route['trans/ship/invoice/list/excell'] = 'backend/transaction/getShipInvListExcell';
$route['trans/ship/invoice/id/(:any)'] = 'backend/transaction/getShipInvListByID/$1';

$route['trans/posting'] = 'backend/transaction/getPostingTrx';
$route['trans/posting/list'] = 'backend/transaction/getListTrxToPost';
$route['trans/posting/save'] = 'backend/transaction/savePostingTrx';
$route['trans/id/(:any)'] = 'backend/transaction/getTrxByID/$1';
$route['trans/id2/(:any)'] = 'backend/transaction/getTrxByID2/$1';
$route['trans/sms'] = 'backend/transaction/getSmsTrxList';
$route['trans/sms/list'] = 'backend/transaction/postTrxForSMSResend';
$route['trans/sms/resend/(:any)'] = 'backend/transaction/resendSMSTrx/$1';

$route['trans/sms/resend2/(:any)/(:any)'] = 'backend/transaction/resendSMSTrx2/$1/$2';

$route['trans/email/resend/(:any)'] = 'backend/transaction/resendEmailTrx/$1';
$route['trans/print/cn'] = 'backend/transaction/getPrintCN';
$route['trans/print/cn/list'] = 'backend/transaction/postPrintCN';
$route['trans/print/cn/txt'] = 'backend/transaction/printCNToTxt';
$route['trans/print/kw'] = 'backend/transaction/getPrintKW';
$route['trans/print/kw/list'] = 'backend/transaction/postPrintKW';
$route['trans/print/kw/txt'] = 'backend/transaction/printKWToTxt';
$route['trans/print/ip'] = 'backend/transaction/getPrintIP';
$route['trans/print/ip/list'] = 'backend/transaction/postPrintIP';
$route['trans/print/ip/txt'] = 'backend/transaction/printIPToTxt';
$route['trans/report'] = 'backend/transaction/getTransactionReport';
$route['trans/report/list'] = 'backend/transaction/postTransactionReport';
$route['trans/payment/report'] = 'backend/transaction/getTransactionPaymentReport';
$route['trans/report/toExcel'] = 'backend/transaction/transReportExcel';
$route['trans/upd/loccd'] = 'backend/transaction/updateLoccdTrx';
$route['trans/report/kvms'] = 'backend/transaction/getTransactionReportKvms';
$route['trans/report/toExcelKvms'] = 'backend/transaction/transReportExcelKvms';

$route['trans/cargo/upd/(:any)/(:any)'] = 'backend/transaction/updateCargoShipper/$1/$2';

$route['trans/reconcile/id/(:any)'] = 'backend/transaction/reconcileDataTrx/$1';
$route['trans/reconcile/save/(:any)/(:any)/(:any)/(:any)'] = 'backend/transaction/saveReconcileTrxData/$1/$2/$3/$4';
$route['trans/reconcile/part/save'] = 'backend/transaction/partReconcile';

//REPORT SALES BY VERA
$route['trans/report/product_sales'] = 'backend/transaction/getPrintSP';
$route['trans/report/product_sales/list'] = 'backend/transaction/postPrintSP';
$route['trans/report/product_sales/txt'] = 'backend/transaction/printSPToTxt';

//REPORT BONUS BY FENDI
$route['trans/shipping/report'] = 'backend/promo/Cariinfoship/formShipping';
$route['trans/shipping/report/list'] = 'backend/promo/Cariinfoship/formShippingList';

$route['statementreport'] = 'backend/promo/ReportStatement/getReportStatement';
$route['promo/printreportstatement'] = 'backend/promo/ReportStatement/postBonus';
$route['statreport/list'] = 'backend/promo/ReportStatement/getReport';

$route['bonusayuartis'] = 'backend/promo/BonusAyuArtis/getBonusAyuArtis';
$route['bonusayuartis/tampil'] = 'backend/promo/BonusAyuArtis/formBonusAyuArtis';
$route['bonusayuartis/tampil/detail'] = 'backend/promo/BonusAyuArtis/formDetailBonusAyuArtis';

//HARUS NYA DI CONTROLLER WAREHOUSE
$route['trans/email/list/get'] = 'backend/transaction/getListTrxToJNE';
$route['trans/email/list/send'] = 'backend/transaction/sendListTrxViaEmail';
$route['trans/email/conote/send'] = 'backend/transaction/sendListConoteToEmail';
$route['trans/email/conote/list'] = 'backend/transaction/getListConoteToEmail';
$route['trans/email/pickup/list'] = 'backend/transaction/getListPickUpTrx';
$route['trans/email/pickup/id/(:any)'] = 'backend/transaction/getDetailPickUpProduct/$1';

$route['print/shipping/label'] = 'backend/transaction/getPrintShippingLabel';
$route['print/shipping/label/list'] = 'backend/transaction/postPrintShippingLabel';
$route['print/shipping/orderno/(:any)'] = 'backend/transaction/getDetailProductByOrderNo/$1';
$route['print/shipping/updcnote'] = 'backend/transaction/postUpdCnoteByOrderno';
//$route['print/shipping/orderno2/(:any)/(:any)'] = 'backend/transaction/getDetailProductByOrderNo2/$1/$2';
$route['print/shipping/pdf/(:any)'] = 'backend/transaction/getPrintShippingPDF2/$1';
$route['print/shipping/label/pdf'] = 'backend/transaction/printLabelShipping';
$route['print/shipping/manifest/pdf'] = 'backend/transaction/printManifestShipping';
$route['print/shipping/manifest/pdfTes'] = 'backend/transaction/printManifestShippingTes';
$route['print/shipping/manifest/pdfTesnew'] = 'backend/transaction/printManifestShippingTesnew';
//END

$route['trans/delete'] = 'backend/transaction/getListTrxToDelete';
$route['trans/delete/list'] = 'backend/transaction/postListTrxToDelete';
$route['trans/delete/(:any)'] = 'backend/transaction/deleteTransaction/$1';
$route['trans/sgo/import'] = 'backend/transaction/formImportSGOtoDB';
$route['trans/sgo/import/preview'] = 'backend/transaction/previewFileCSV';
$route['trans/sgo/import/save'] = 'backend/transaction/saveImportToDB';
$route['trans/sgo/contoh'] = 'backend/transaction/contoh';
$route['trans/sgo/reconcile'] = 'backend/transaction/formSGOReconcileTrx';
$route['trans/sgo/reconcile/list/(:any)'] = 'backend/transaction/getListSGOTrx/$1';
$route['trans/sgo/reconcile/save/(:any)'] = 'backend/transaction/saveSGOByTrx/$1';
$route['trans/sgo/reconcile/saveMemb/(:any)/(:any)/(:any)'] = 'backend/transaction/saveSGOMembByTrx/$1/$2/$3';
$route['trans/sgo/reconcile/detTrxXL/(:any)'] = 'backend/transaction/getDetTrxXL/$1';
$route['kgb/print'] = 'backend/transaction/kgbTestPrint';

$route['trans/sgo/import/goworld'] = 'backend/transaction/formImportSGOtoDBGW';
$route['trans/sgo/import/goworld/preview'] = 'backend/transaction/previewFileCSVGW';
$route['trans/sgo/import/goworld/save'] = 'backend/transaction/saveImportToDBGW';
$route['trans/sgo/import/updateTrxId'] = 'backend/transaction/updateSgoTrxId';

/*---------------
REKENING
----------------*/

/* ============================================= *
 *  tambahan dari ananech (backend transaction)  *
 * ============================================= */
$route['trans/reinputTRx'] = 'backend/transaction/frmReinputTrx';
$route['trans/helper_show_prod_by_id'] = 'backend/transaction/helperShowProdByID';
$route['trans/sgo/report'] = 'backend/transaction/getFrmSgoRpt';
//$route['trans/sgo/report'] = 'backend/transaction/getFrmSgoRpt';
$route['trans/sgo/report/act'] = 'backend/transaction/sgoReportAct';
$route['trans/sgo/report/csv'] = 'backend/transaction/sgoReportCsv';
$route['trans/jne/frmNoConot'] = 'backend/transaction/getFrmNoConot';
$route['trans/jne/getListNoConot'] = 'backend/transaction/getListNoConot';
$route['trans/jne/noConotDetails'] = 'backend/transaction/noConotDets';
$route['trans/jne/updtEConotJne/(:any)/(:any)/(:any)'] = 'backend/transaction/updtEConotJne/$1/$2/$3';
$route['trans/shipaddr/check/(:any)/(:any)'] = 'backend/transaction/checkShipAddr/$1/$2';
$route['trans/shipaddr/ins/(:any)/(:any)'] = 'backend/transaction/insShipAddrFromSGO/$1/$2';

//$route['shipping/konos/create/(:any)/(:any)'] = 'webshop/shipping/getShippingKonos/$1/$2';
$route['shipping/konos/create'] = 'webshop/shipping/getShippingKonos';

$route['trans/conote/track/(:any)'] = 'backend/transaction/getTrackingJNE/$1';

//NANTI DI UPDATE
$route['trans/integrity/check'] = 'backend/warehouse/formCheckIntegrityTransaction';

//Payment Bank Charge Setting
$route['trans/bank/charge'] = 'backend/transaction/formBankCharge';
$route['trans/bank/charge/list'] = 'backend/transaction/getListBankCharge';
$route['trans/bank/get/(:any)/(:any)'] = 'backend/transaction/getListBankChargeByParam/$1/$2';
$route['trans/bank/charge/save'] = 'backend/transaction/postSaveBankCharge';
$route['trans/bank/charge/update'] = 'backend/transaction/postUpdateBankCharge';
$route['trans/bank/charge/delete/(:any)'] = 'backend/transaction/postDeleteBankCharge/$1';
/*========================================================
 * START HILAL BACKEND VERITRANS REPORT
 *=======================================================
*/

$route['trans/report/VT'] 				= 'backend/report/report_vt/getReportVT';
$route['trans/report/VT/act/(:any)'] 	= 'backend/report/report_vt/getReportVTAct/$1';
$route['trans/report/printExcell'] 		= 'backend/report/report_vt/getReportVTExcell';

/*
$route['be_umroh/report/installment'] = 'backend/finance_umroh/getReportInstall';
$route['be_umroh/report/installment/act'] = 'backend/finance_umroh/reportInstallAct';
$route['be_umroh/report/printExcell'] = 'backend/finance_umroh/reportPrintExcell';
$route['be_umroh/report/printExcellMDR'] = 'backend/finance_umroh/reportPrintExcellMDR';
 */

/*========================================================
 * END HILAL BACKEND VERITRANS REPORT
 *=======================================================
*/


/*---------------------------
 * BACKEND UMROH
 * -------------------------*/
$route['be_umroh'] = 'backend/backend_umroh/index';
$route['be_umroh/helper/distInfo/(:any)']='backend/backend_umroh/getDistributorInfo/$1';
//$route['be_umroh/helper/sponsorInfo/{:any}']='backend/backend_umroh/getSponsorInfo/$1';
$route['be_umroh/helper/ktp/(:any)']='backend/backend_umroh/getCheckNoKTP/$1';
$route['be_umroh/helper/hp/(:any)']='backend/backend_umroh/getCheckNoHP/$1';
$route['be_umroh/preview']='backend/backend_umroh/getPreviewInputUmroh';
$route['be_umroh/reg/list']='backend/finance_umroh/getListRegUmroh';
$route['be_umroh/reg/list/get']='backend/finance_umroh/postListRegUmroh';
$route['be_umroh/reg/list/print']='backend/finance_umroh/printListRegUmroh';
$route['be_umroh/print/notaUmroh/(:any)']='backend/backend_umroh/printNotaUmroh/$1';
$route['be_umroh/listingReg'] = 'backend/backend_umroh/formListingRegUmroh';
$route['be_umroh/listingReg/list'] = 'backend/backend_umroh/getListingRegUmroh';
$route['be_umroh/regno/(:any)'] = 'backend/backend_umroh/getDataUmrohByRegno/$1';
$route['be_umroh/helper/journey/(:any)']='backend/backend_umroh/getTipePerjalanan/$1';
$route['be_umroh/listUpload/journey']='backend/finance_umroh/getlistUploadToWeb';
$route['be_umroh/listUpload/journey/act'] = 'backend/finance_umroh/reportUmrToWebAct';
$route['be_umroh/listUpload/journey/xls'] = 'backend/finance_umroh/reportUmrohToXlsWeb';
//$route['be_umroh/helperd/journey/(:any)']='backend/backend_umroh/getTipePerjalanan/$1';

//be_mt940
$route['be_umroh/be_mt940'] = 'backend/backend_umroh/getUploadMT940';
$route['be_umroh/save_upload'] = 'backend/backend_umroh/actUploadMT940';


/*  =======================================================
            By Ananech backend Finance K-link Rohani
    ======================================================= */

$route['be_umroh/listPayment']='backend/finance_umroh/form_approve';
$route['be_umroh/listPayment/app']='backend/finance_umroh/get_appPayment';
$route['be_umroh/listPayment/appRes/(:any)/(:any)/(:any)/(:any)']='backend/finance_umroh/approvePayment/$1/$2/$3/$4';
$route['be_umroh/printKwSk']='backend/finance_umroh/printKwSk';
$route['be_umroh/reprint/VoucherUmroh']='backend/finance_umroh/getVoucherInfo';
$route['be_umroh/reprint/voucher/act']='backend/finance_umroh/voucherInfo';
$route['be_umroh/reprint/voucher/txt']='backend/finance_umroh/reprintVoucher';
$route['be_umroh/print/kw/txt']='backend/finance_umroh/reprintKWx';

$route['be_umroh/resendSms']='backend/backend_umroh/getDtSmsUmroh';
$route['be_umroh/resendSms/act/(:any)']='backend/backend_umroh/resendSmsUmroh/$1';



//========================START BROADCAST SMS=============================
$route['be_umroh/listsmspost'] = 'backend/finance_umroh/getListSMSPost';
$route['be_umroh/smsbcpost/act'] = 'backend/finance_umroh/reportSMSBCAct';
$route['be_umroh/smsbcpost/prev'] = 'backend/finance_umroh/reportSMSBCPrev';
$route['be_umroh/smsbcpost/send'] = 'backend/backend_umroh/sendBC';
/*$route['be_umroh/post/upd'] = 'backend/finance_umroh/reportPostUpdate';


$route['be_umroh/post'] = 'backend/finance_umroh/getReportPost';
$route['be_umroh/post/act'] = 'backend/finance_umroh/reportPostAct';
$route['be_umroh/post/prev'] = 'backend/finance_umroh/reportPostPrev';
$route['be_umroh/post/upd'] = 'backend/finance_umroh/reportPostUpdate';
 */
//========================END BROADCAST SMS===============================


$route['be_umroh/installTrf']='backend/finance_umroh/form_installments';
$route['be_umroh/helper/regnoInfo']='backend/finance_umroh/helperRegnoInfo';
$route['be_umroh/save/installTrf']='backend/finance_umroh/save_installments';
$route['be_umroh/list/va']='backend/finance_umroh/getListVA';
$route['be_umroh/report/installment'] = 'backend/finance_umroh/getReportInstall';
$route['be_umroh/report/installment/act'] = 'backend/finance_umroh/reportInstallAct';
$route['be_umroh/report/printExcell'] = 'backend/finance_umroh/reportPrintExcell';
$route['be_umroh/report/printExcellMDR'] = 'backend/finance_umroh/reportPrintExcellMDR';

$route['be_umroh/mutasi/umroh'] = 'backend/mutasi_umroh/form_mutasi';
$route['be_umroh/mutasi/action'] = 'backend/mutasi_umroh/list_mutasi';
$route['be_umroh/mutasi/details/(:any)'] = 'backend/mutasi_umroh/mutasi_details/$1';

//cancel umroh by fendi
$route['be_umroh/cancel'] = 'backend/finance_umroh/getCancel';
$route['be_umroh/cancel/act'] = 'backend/finance_umroh/getListCancel';
$route['be_umroh/cancel/act2'] = 'backend/finance_umroh/prosesCancelUmroh';
/**
 * @Author: Ricky
 * @Date: 2019-01-28 16:25:31
 * @Desc: checking member data cancelation
 */
$route['be_umroh/cancel/getmemberinfo/(:any)'] = 'backend/finance_umroh/getMemberDataById/$1';
$route['be_umroh/cancel/getmutation/(:any)'] = 'backend/finance_umroh/getMutationDetail/$1';
$route['be_umroh/cancel/addmutation'] = 'backend/finance_umroh/addMutation';
$route['be_umroh/cancel/savemutation'] = 'backend/finance_umroh/saveMutation';
$route['be_umroh/cancel/print'] = 'backend/finance_umroh/printCancelUmroh';

/*  ================================================================
            end tambahan By Ananech backend Finance K-link Rohani
    ================================================================ */

$route['be_umroh/post'] = 'backend/finance_umroh/getReportPost';
$route['be_umroh/post/act'] = 'backend/finance_umroh/reportPostAct';
$route['be_umroh/post/prev'] = 'backend/finance_umroh/reportPostPrev';
$route['be_umroh/post/upd'] = 'backend/finance_umroh/reportPostUpdate';

//$route['be_umroh/listingReg'] = 'backend/mutasi_umroh/form_listing';
//$route['be_umroh/listingReg/list'] = 'backend/mutasi_umroh/list';

/*---------------------------
 * BACKEND MEMBER
 * -------------------------*/
$route['be/member/recruit'] = 'backend/backend_member/formListRecruit';
$route['be/member/recruit/list'] = 'backend/backend_member/getListRecruit';
$route['be/member/recruit/list/excell'] = 'backend/backend_member/exportRecruitExcell';
$route['be/member/search'] = 'backend/backend_member/formSearchMember';
$route['be/member/list'] = 'backend/backend_member/list_member';
$route['be/member/details/(:any)'] = 'backend/backend_member/details_member/$1';
$route['be/member/knet'] = 'backend/backend_member/formListRegMembKNET';
$route['be/member/knet/list'] = 'backend/backend_member/getListRegMembKNET';
$route['member/knet/print'] = 'backend/backend_member/printListRegMembKNET';
$route['be/member/rank'] = 'backend/backend_member/rank_member';
$route['be/member/helper/idInfo/(:any)'] = 'backend/backend_member/get_id/$1';
$route['be/member/helper/levelInfo/(:any)'] = 'backend/backend_member/get_level/$1';
$route['be/member/save'] = 'backend/backend_member/update_rank';
$route['be/member/rank/list'] = 'backend/backend_member/list_rank';
$route['be/member/rank/list/details'] = 'backend/backend_member/search_rank';

$route['be/member/place'] = 'backend/backend_member/get_place';
$route['be/member/place/input'] = 'backend/backend_member/input_place';
$route['be/member/place/save'] = 'backend/backend_member/save_place';

$route['be/member/knet/reg'] = 'backend/backend_member/formMemberReg';
$route['be_member/helper/dtByOrderID/(:any)'] = 'backend/backend_member/getDtByOrderID/$1';
$route['be/member/insert'] = 'backend/backend_member/memberInsKnet';
$route['be/member/helper/checkSponsor/(:any)/(:any)'] = 'backend/backend_member/checkSponsor/$1/$2';
$route['be_member/helper/ktp/(:any)']='backend/backend_member/checkKtp/$1';
$route['be_member/helper/distInfo/(:any)'] = 'backend/backend_member/getUserlogin/$1';
$route['be_member/helper/hp/(:any)']='backend/backend_member/checkTlp/$1';
$route['be_member/helper/checkExistIDMember/(:any)']='backend/backend_member/checkExistIDMember/$1';

$route['be/member/update'] = 'backend/backend_member/formMemberUpdate';
$route['be/member/id/(:any)'] = 'backend/backend_member/getDetailMemberByID/$1';
$route['be/member/stk/id/(:any)'] = 'backend/backend_member/getDetailStockistByID/$1';
$route['be/member/update/save'] = 'backend/backend_member/saveMemberUpdate';
$route['be/member/info'] = 'backend/backend_member/formMemberUpdateInfo';
$route['be/member/info/update'] = 'backend/backend_member/saveMemberInfoUpdate';
$route['be/member/info/id/(:any)'] = 'backend/backend_member/getMemberInfoByID/$1';

$route['be/member/info/adm'] = 'backend/backend_member/formMemberUpdateInfoAdmin';
$route['be/member/info/updateAdm'] = 'backend/backend_member/saveMemberInfoUpdateAdm';
$route['be/memberinfo/(:any)/(:any)/(:any)'] = 'backend/backend_member/getInfoMemberStokis/$1/$2/$3';

$route['be/member/voucher/reconcile'] = 'backend/backend_member/formReconcileMemberRegVoucher';
$route['be/member/voucher/reconcile/list'] = 'backend/backend_member/getListReconcileMemberRegVoucher';
$route['be/member/voucher/reconcile/save'] = 'backend/backend_member/saveReconcileMemberRegVoucher';

$route['be/member/voucherkey/update'] = 'backend/backend_member/formUpdateVchKeyByVchNo';
$route['be/member/voucherkey/list'] = 'backend/backend_member/readFileVch';
$route['be/member/voucherkey/update/save'] = 'backend/backend_member/saveUpdateVchKey';
$route['be/member/voucherkey/print'] = 'backend/backend_member/printResultToExcel';
$route['be/member/release_voucher'] = 'backend/backend_member/releaseVoucher';

$route['be/member/vchno/(:any)'] = 'backend/backend_member/getDetailVch/$1';
//$route['be/member/voucher/reconcile/save/(:any)'] = 'backend/backend_member/saveReconcileMemberRegVoucher/$1';
/*
$route['be/member/register'] = 'backend/backend_member/reg_member';
$route['be/member/insert'] = 'backend/backend_member/ins_member';
$route['be/member/report'] = 'backend/backend_member/rep_member';
$route['be/member/excel'] = 'backend/backend_member/excel_rep';
$route['be/member/edit/(:any)'] = 'backend/backend_member/edit/$1';
$route['be/member/edit2'] = 'backend/backend_member/edit2';
$route['be/member/delete/(:any)'] = 'backend/backend_member/delete/$1';
$route['be_member/helper/hp/(:any)']='backend/backend_member/checkTlp/$1';
*/

/*---------------------------
 * BACKEND TOTO
 * -------------------------*/
$route['be_toto'] = 'backend/backend_toto/index';
$route['be_toto/helper/distInfo/(:any)']='backend/backend_toto/getDistributorInfo/$1';
//$route['be_toto/helper/sponsorInfo/{:any}']='backend/backend_umroh/getSponsorInfo/$1';
$route['be_toto/helper/ktp/(:any)']='backend/backend_toto/getCheckNoKTP/$1';
$route['be_toto/helper/hp/(:any)']='backend/backend_toto/getCheckNoHP/$1';
$route['be_toto/preview']='backend/backend_toto/getPreviewInputUmroh';
$route['be_toto/print/notaUmroh/(:any)']='backend/backend_toto/printNotaUmroh/$1';
$route['be_toto/listingReg'] = 'backend/backend_toto/formListingRegUmroh';
$route['be_toto/listingReg/list'] = 'backend/backend_toto/getListingRegUmroh';
$route['be_toto/regno/(:any)'] = 'backend/backend_toto/getDataUmrohByRegno/$1';
$route['be_toto/helper/journey/(:any)']='backend/backend_toto/getTipePerjalanan/$1';




/*-------------------------
* SHIPPING MAINTENANCE
* -----------------------*/


$route['wh/shipping/summary'] = 'backend/warehouse/formShippingSummary';
$route['wh/shipping/summary/list'] = 'backend/warehouse/getShippingSummary';
$route['wh/shipping/summary/print'] = 'backend/warehouse/printShippingSummary';
$route['wh/shipping/detail/(:any)'] = 'backend/warehouse/getDetailProductShipping/$1';
$route['wh/kgp/stk/update'] = 'backend/warehouse/updateNullStockistKgp';
$route['wh/kgp/province'] = 'backend/warehouse/listProvinceKgp';
$route['wh/kgp/stk/list'] = 'backend/warehouse/listStkByProvinceKgp';
$route['wh/kgp/stk/saveupd'] = 'backend/warehouse/updateStkByProvinceKgp';
//barcode product
$route['barcode/upload'] = 'backend/barcode/uploadPrdBarcodeForm';
$route['barcode/preview'] = 'backend/barcode/previewFileCSV';
$route['barcode/upload/save'] = 'backend/barcode/saveUploadPrdBarcode';
$route['barcode/check/do/(:any)/(:any)'] = 'backend/barcode/getValidDO/$1/$2';
$route['barcode/check/do/(:any)'] = 'backend/barcode/getValidDO/$1';

//end
$route['wh/do'] = 'backend/transaction/getDeliveryOrder';
$route['wh/doList'] = 'backend/transaction/getDOList';
$route['wh/do/group'] = 'backend/transaction/getDOGroup';
$route['wh/do/groupAct'] = 'backend/transaction/actDOGroup';
$route['wh/do/txt'] = 'backend/transaction/printTxtDO';
$route['wh/do/txt1'] = 'backend/transaction/printTxtDO1';
$route['wh/do/listing'] = 'backend/transaction/getListGroupDO';
$route['wh/do/listing/act'] = 'backend/transaction/listingGroupDOact';
$route['wh/do/list/det/(:any)/(:any)'] = 'backend/transaction/listingGroupDOdet/$1/$2';

$route['wh/shipping/label'] = 'backend/warehouse/formShippingLabel';
$route['wh/shipping/label/list'] = 'backend/warehouse/getListShippingLabel';

$route['wh/releasevcr'] = 'backend/warehouse/getReleaseVcrSK';
$route['wh/releasevcr/act'] = 'backend/warehouse/getReleaseVcrSKact';
$route['wh/releasevcr/det/(:any)'] = 'backend/warehouse/getReleaseVcrSKdet/$1';
$route['wh/releasevcr/upd/(:any)/(:any)/(:any)'] = 'backend/warehouse/getReleaseVcrSKupd/$1/$2/$3';
$route['wh/releasevcr/product/(:any)/(:any)'] = 'backend/warehouse/detailReleasedSK/$1/$2';
$route['wh/releasevcr/check/formno/(:any)/(:any)'] = 'backend/warehouse/checkFormNo/$1/$2';
$route['wh/releasevcr/save'] = 'backend/warehouse/saveReleaseVch';
/*--------------------------
 * PULSA TRX DION (JGN DI UBAH2..!!)
 * -------------------------*/
$route['be/pulsa'] = 'backend/backend_pulsa/formInputTrxPulsa';
$route['be/pulsa/fullcode'] = 'backend/backend_pulsa/processTrxByFullcode';
$route['be/pulsa/date'] = 'backend/backend_pulsa/searchTrxPulsaByDate';
$route['be/pulsa/debt/(:any)/(:any)'] = 'backend/backend_pulsa/detailClientDebt/$1/$2';
$route['be/pulsa/pay/selected'] = 'backend/backend_pulsa/paySelectedID';
$route['be/pulsa/pay/(:any)'] = 'backend/backend_pulsa/payClientDebt/$1';
$route['be/pulsa/pay'] = 'backend/backend_pulsa/paySelectedClientDebt';

$route['be/pulsa/tanto/pay/(:any)'] = 'backend/backend_pulsa/payTantoClientDebt/$1';
$route['be/pulsa/tanto/pay'] = 'backend/backend_pulsa/paySelectedTantoClientDebt';
$route['be/pulsa/id/(:any)'] = 'backend/backend_pulsa/searchTrxPulsaByID/$1';
$route['be/pulsa/delete/(:any)'] = 'backend/backend_pulsa/deletePulsaTrx/$1';
$route['be/pulsa/save'] = 'backend/backend_pulsa/saveTrxPulsa';
$route['be/pulsa/update'] = 'backend/backend_pulsa/updateTrxPulsa';
$route['be/pulsa/search'] = 'backend/backend_pulsa/formSearchTrxPulsa';
$route['be/pulsa/search/list'] = 'backend/backend_pulsa/listSearchTrxPulsa';
$route['be/pulsa/report'] = 'backend/backend_pulsa/formReportTrxPulsa';
$route['be/pulsa/report/list'] = 'backend/backend_pulsa/listReportTrxPulsa';
$route['be/pulsa/summary'] = 'backend/backend_pulsa/formSummaryReport';
$route['be/pulsa/summary/list'] = 'backend/backend_pulsa/listSummaryReport';
$route['be/pulsa/client'] = 'backend/backend_pulsa/formInputClient';
$route['be/pulsa/client/no/(:any)'] = 'backend/backend_pulsa/getClientByNo/$1';
$route['be/pulsa/client/list'] = 'backend/backend_pulsa/getListClient';
$route['be/pulsa/client/save'] = 'backend/backend_pulsa/saveNewClient';
$route['be/pulsa/client/delete/(:any)'] = 'backend/backend_pulsa/deleteClientByPhoneNo/$1';
$route['be/pulsa/client/(:any)/(:any)'] = 'backend/backend_pulsa/getDataClient/$1/$2';
$route['be/pulsa/trx/export/excel']  = 'backend/backend_pulsa/exportToExcel';
$route['api/pulsa/login']  = 'backend/backend_pulsa/postLoginPulsaUser';
$route['money']  = 'backend/backend_pulsa/formInputMonify';
$route['money/list']  = 'backend/backend_pulsa/getListMonify';


/*---------------------------
       * BACKEND PROMO *
---------------------------*/
$route['be/promo/getSummary']  = 'backend/Be_promo/getSummary';
$route['be/promo/getSummary/act'] = 'backend/be_promo/getSummaryAct';
$route['be/promo/details/(:any)/(:any)/(:any)'] = 'backend/be_promo/getSummaryDetails/$1/$2/$3';
$route['be/promo/printSummary/txt'] = 'backend/be_promo/printSummaryTxt';
$route['be/promo/report']  = 'backend/Be_promo/getReport';

$route['be/promo/idc'] = 'backend/klinkpromo/idc_promo/idcPromoForm';
$route['be/promo/idc/result/(:any)'] = 'backend/klinkpromo/idc_promo/idcPromoResult/$1';
$route['be/promo/detail'] = 'backend/klinkpromo/idc_promo/idcPromoDetail';
$route['be/promo/idc/report'] = 'backend/klinkpromo/idc_promo/formIdcReport';
$route['be/promo/idc/report/result'] = 'backend/klinkpromo/idc_promo/formIdcReportResult';
$route['be/promo/idc/report/print'] = 'backend/klinkpromo/idc_promo/formIdcReportPrint';

$route['be/promo/cashback'] = 'backend/promo_cashback/cashbackForm';
$route['be/promo/cashback/list'] = 'backend/promo_cashback/cashbackGetList';
$route['be/promo/cashback/save'] = 'backend/promo_cashback/cashbackSave';
$route['be/promo/cashback/batch/(:any)'] = 'backend/promo_cashback/listDetailBatch/$1';
$route['be/promo/cashback/csv/(:any)'] = 'backend/promo_cashback/listDetailBatchCSV/$1';
$route['be/promo/cashback/import'] = 'backend/promo_cashback/importFileBatch';

$route['be/promo/cashback/sendsmsCashback'] = 'backend/cashback_promo/sendsmsCashback';

/*----------------------------
	* K-LINK PROMO RP 20.000
*---------------------------*/
$route['be/promo/ayuartis'] = 'backend/promo/ayuartispromo/formCariMember';
$route['be/promo/ayuartis/list'] = 'backend/promo/ayuartispromo/tampil_member';

$route['be/promo/importayu'] = 'backend/promo/importpromo/formInputPromo';
$route['be/promo/importayu/action1'] = 'backend/promo/importpromo/promoImportForm';
$route['be/promo/importayu/action2'] = 'backend/promo/importpromo/promoImportAction';

/*---------------------------
    * PROMO OCT2017 - APR2018 BY FENDI
*--------------------------*/
$route['be/promo/promoct'] = 'backend/promo/promoctapr/formPromo';
$route['be/promo/promoct/list'] = 'backend/promo/promoctapr/tampil_member';
$route['be/promo/promoct/detail'] = 'backend/promo/promoctapr/tampil_detail';

$route['be/promo/promoctv2'] = 'backend/promo/promoctapr2/formPromo2';
$route['be/promo/promoctv2/list'] = 'backend/promo/promoctapr2/tampil_member2';
$route['be/promo/promoctv2/detail'] = 'backend/promo/promoctapr2/tampil_detailprom';



/*---------------------------
    * WEB-STORE INTERFACE
*--------------------------*/
$route['loginmember'] = 'webshop/login/member_login';
$route['loginmember/action'] = 'webshop/login/handleLogin';
$route['logout'] = 'webshop/login/logout';

/*----------------------------------
 * PROMO NOVEMBER 2017 - APRIL 2018
 * --------------------------------*/
$route['promo_novapr'] = 'webshop/promo_novapr/index';
$route['promo_novapr/bns'] = 'webshop/promo_novapr/getListPerBnsPeriod';
$route['promo_novapr/detailPrd'] = 'webshop/promo_novapr/detailPrd';
/*---------------------------
    * WEBSHOP PROMO IDC INFO
-----------------------------*/
$route['promo/idc']  = 'webshop/promo_rekrut/idcPromo';
$route['promo/idc/detail']  = 'webshop/promo_rekrut/idcPromoDetails';

//$route['download/(:any)'] = 'webshop/utility/formDownloadBackpack/$1';
$route['download/(:any)'] = 'webshop/utility/formDownloadBackpack/$1';
$route['download_hargabaru'] = 'webshop/utility/download2File';

/*
$route['shop/product'] = 'webshop/product/getProduct';
$route['shop/product/cat'] = 'webshop/product/getProductCategory';
$route['shop/product/cat/id/(:any)'] = 'webshop/product/getProdByCat/$1';
$route['shop/product/cat/id/(:any)/(:any)'] = 'webshop/product/getProdByCat2/$1/$2';
$route['shop/prodDet'] = 'webshop/product/getProdDetails';
$route['shop/productHeader/cat/id/(:any)'] = 'webshop/product/getProdByCatHeader/$1';
*/

/*-----------------------------
 * SALES NON MEMBER / CUSTOMER
 * ---------------------------*/
$route['shop/customer'] = 'webshop/cartshop_customer/getListProduct';

//LP pembelanjaan non member dengan parameter
$route['shop/cart/prdcd/(:any)'] = 'webshop/cartshop_customer/autoAddPrdcdToCart/$1';


$route['shop/shipping'] = 'webshop/shipping_customer/getShipping';
$route['shop/shipping/getStockist/(:any)'] = 'webshop/shipping_customer/getStockist/$1';
$route['shop/shipping/getPricecode/(:any)'] = 'webshop/shipping_customer/getPriceStockist/$1';
$route['shop/shipping/getPricecodeX/(:any)/(:any)'] = 'webshop/shipping_customer/getPriceStockistX/$1/$2';
$route['shop/shipping/kota/(:any)'] = 'webshop/shipping_customer/getListKota/$1';
$route['shop/shipping/kecamatan/(:any)'] = 'webshop/shipping_customer/getListKecamatan/$1';
$route['shop/shipping/kelurahan/(:any)'] = 'webshop/shipping_customer/getListKelurahan/$1';

/*---------------------
 * cargo and ekspedisi
 * -------------------*/
$route['cargo/province/list/(:any)'] = 'webshop/shipping/listCargoProvince/$1';
$route['cargo/kota/list'] = 'webshop/shipping/listCargoKota';
$route['cargo/stockist/list'] = 'webshop/shipping/listCargoStockist';
$route['cargo/pricecode/(:any)'] = 'webshop/shipping/setPricecodeKGB/$1';

$route['shop/cart/remove'] = 'webshop/cartshop_customer/removeCart';
$route['shop/cart/update'] = 'webshop/cartshop_customer/updateCart';
$route['shop/cart/upd'] = 'webshop/cartshop_customer/updateCartBeforeProceed';

$route['shop/pay/sgo/inquiry/dev'] = 'webshop/cartshop_customer_payment/getResponseFromSGODev';
$route['shop/pay/sgo/notif/dev'] = 'webshop/cartshop_customer_payment/notifAfterPaymentDev';
$route['shop/pay/sgo/finish/dev/(:any)'] = 'webshop/cartshop_customer_payment/afterPaymentWithSGODev/$1';

$route['shop/pay/sgo/inquiry'] = 'webshop/cartshop_customer_payment/getResponseFromSGO';
$route['shop/pay/sgo/notif'] = 'webshop/cartshop_customer_payment/notifAfterPayment';
$route['shop/pay/sgo/finish/(:any)'] = 'webshop/cartshop_customer_payment/afterPaymentWithSGO/$1';
$route['downloaddev'] = 'webshop/cartshop_customer_payment/downloader';

$route['shop/customer/prd/name'] = 'cartshop_customer/getProdByName';

$route['shop/customer/(:any)/(:any)/(:any)/(:any)'] = 'webshop/cartshop_customer/getListProductForLpMember/$1/$2/$3/$4';
$route['shop/cart/add'] = 'webshop/cartshop_customer/addToCart';
$route['shop/cart/list'] = 'webshop/cartshop_customer/listCart';
$route['shop/cart/checkout'] = 'webshop/cartshop_customer/CheckoutCart';
$route['shop/pay/preview'] = 'webshop/cartshop_customer/paySGOPreview';

$route['quiz/winner/(:any)'] = 'webshop/cartshop_winner/shopQuizWinner/$1';
$route['quiz/winner/cart/list'] = 'webshop/cartshop_winner/listCart';
$route['quiz/winner/cart/checkout'] = 'webshop/cartshop_winner/CheckoutCart';
$route['quiz/winner/sgo/pay/preview'] = 'webshop/cartshop_winner/payPreview';

$route['shop/customer/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'webshop/cartshop_customer/getListProductForLpMemberPrd/$1/$2/$3/$4/$5';


$route['shop/customer_dev/(:any)/(:any)/(:any)/(:any)'] = 'webshop/cartshop_customer_dev/getListProductForLpMember/$1/$2/$3/$4';
$route['shop/cart/add_dev'] = 'webshop/cartshop_customer_dev/addToCart';
$route['shop/cart/list_dev'] = 'webshop/cartshop_customer_dev/listCart';
$route['shop/cart/checkout_dev'] = 'webshop/cartshop_customer_dev/CheckoutCart';
//$route['shop/pay/preview'] = 'webshop/cartshop_customer/paySGOPreview';
//END

//$route['shop/product'] = 'product/redirectGetProduct';
$route['shop/product'] = 'product/getProduct';
$route['shop/product/test'] = 'product/redirectGetProductTest'; // with views/webshop/shop_home_empty_test.php

$route['shop/product_org'] = 'product/getProduct';
$route['shop/product/cat'] = 'product/getProductCategory';
$route['shop/product/cat/id/(:any)'] = 'product/getProdByCat/$1';
$route['shop/product/name'] = 'product/getProdByName';
$route['shop/product/cat/id/(:any)/(:any)'] = 'product/getProdByCat2/$1/$2';
$route['shop/prodDet'] = 'product/getProdDetails';
$route['shop/productHeader/cat/id/(:any)'] = 'product/getProdByCatHeader/$1';

$route['shop/productdev'] = 'product/getProductDev';
$route['shop/emptyProduct'] = 'product/redirectGetProduct';

$route['shipping'] = 'webshop/shipping/getShipping';
$route['shipping/getStockist/(:any)'] = 'webshop/shipping/getStockist/$1';
$route['shipping/getPricecode/(:any)'] = 'webshop/shipping/getPriceStockist/$1';
$route['shipping/getPricecodeX/(:any)/(:any)'] = 'webshop/shipping/getPriceStockistX/$1/$2';
$route['shipping/kota/(:any)'] = 'webshop/shipping/getListKota/$1';
$route['shipping/kecamatan/(:any)'] = 'webshop/shipping/getListKecamatan/$1';
$route['shipping/kelurahan/(:any)'] = 'webshop/shipping/getListKelurahan/$1';

//tambahan COD
$route['shipping/kecamatan_cod/(:any)'] = 'webshop/shipping/getListKecamatanCOD/$1';
$route['shipping/kotaKP/(:any)'] = 'webshop/shipping/getListKotaKP/$1';
$route['shipping/kecamatanKP/(:any)'] = 'webshop/shipping/getListKecamatanKP/$1';
$route['shipping/kelurahanKP/(:any)'] = 'webshop/shipping/getListKelurahanKP/$1';
$route['shipping/cek_kodepos/(:any)/(:any)/(:any)/(:any)'] = 'webshop/shipping/cek_kodepos/$1/$2/$3/$4';
$route['shipping/getPricecodeCOD/(:any)/(:any)'] = 'webshop/shipping/getPriceStockistCOD/$1/$2';
//$route['shipping/rebook_gsend'] = 'webshop/shipping/getRebook';

$route['shipping/rebook_gsend'] = 'backend/Rebook_gosend/rebookSearch';
$route['shipping/rebook_gsend/list'] = 'backend/Rebook_gosend/rebookSearchByParam';
$route['rebook_gsend/id'] = 'backend/Rebook_gosend/rebookSearchByID';

$route['shipping/complain_gsend'] = 'backend/Rebook_gosend/orderSearch';
$route['complain_gsend/id'] = 'backend/Rebook_gosend/orderSearchByID';

$route['cart/add'] = 'webshop/cartshop/addToCart';
$route['cart/list'] = 'webshop/cartshop/listCart';
$route['cart/list2'] = 'webshop/cartshop/liatCart';
$route['cart/remove'] = 'webshop/cartshop/removeCart';
$route['cart/update'] = 'webshop/cartshop/updateCart';
$route['cart/upd'] = 'webshop/cartshop/updateCartBeforeProceed';
//AKAN DI UPDATE 16/11/2018
//$route['cart/checkout'] = 'webshop/cartshop/CheckoutCart';
$route['cart/checkout'] = 'webshop/Promo_nov/CheckoutCart';
//end
$route['cart/member/id/(:any)'] = 'webshop/cartshop/getMemberInfo/$1';
$route['cart/bnsperiod/get'] = 'webshop/cartshop/getBnsPeriod';
$route['static/why'] = 'webshop/static_page/why';
$route['static/delivery'] = 'webshop/static_page/delivery';
$route['static/how-to-order'] = 'webshop/static_page/how_to_order';
$route['static/shipping_rate'] = 'webshop/static_page/shipping_rate';
$route['static/replacement'] = 'webshop/static_page/replacement';
$route['static/privacy-policy'] = 'webshop/static_page/privacy_policy';
$route['static/how_to_pay'] = 'webshop/static_page/howTo_pay_SGO';
$route['static/about'] = 'webshop/static_page/about';

//AUGMENTED REALITY
$route['cart/prdcd/(:any)'] = 'webshop/cartshop/salesPrdcd/$1';
$route['cart/prd/add'] = 'webshop/cartshop/addPrdToCart';
//

$route['jne'] = 'webshop/cartshop/getShippingCost';
$route['jne/tes_konot'] = 'webshop/cartshop/generateJNENo2';
$route['jne/conot'] = 'webshop/payment/generateJNENo';
$route['jne/price'] = 'webshop/cartshop/showPriceResult';
$route['jne/price2'] = 'webshop/cartshop/showPriceResultTest';

/*---------------------------
 PAYMENT EXAMPLE
*--------------------------*/
$route['payment'] = 'webshop/PaymentExample/payCartRedirect';
$route['checkout'] = 'webshop/PaymentExample/payCartDirect';
$route['checkout_process'] = 'webshop/PaymentExample/checkoutProcess';

$route['payment'] = 'webshop/payment/proceedPaymentCart';
//$route['payment/choose'] = 'webshop/cartshop/choosePaymentType';
$route['payment/card'] = 'webshop/cartshop/chooseCardType';
$route['payment/dc/(:any)'] = 'webshop/cartshop/chooseDebitCardPaymentType/$1';

//$route['checkout'] = 'webshop/PaymentExample/payCartDirect';

$route['debit'] = 'webshop/PaymentExample/formDebit';
$route['debit_process'] = 'webshop/PaymentExample/checkoutProcessDebit';

$route['cimb'] = 'webshop/PaymentExample/formCimbDebit';
$route['cimb_process'] = 'webshop/PaymentExample/processCimb';

$route['briEpay'] = 'webshop/PaymentExample/formBriDebit';
$route['briProcess'] = 'webshop/PaymentExample/processBri';
$route['briAfterPay'] = 'webshop/PaymentExample/afterPaymentBri';

//PAYMENT VERITRANS

$route['pay/cc'] = 'webshop/payment/creditCard';
$route['pay/mandiri/clickpay'] = 'webshop/payment/mandiriClickPay';
$route['pay/cimb/click'] = 'webshop/payment/cimbClick';
//$route['pay/cimb/click/proceed'] = 'webshop/payment/cimbClickProceed';
$route['pay/va'] = 'webshop/payment/virtualAccount';
$route['pay/bri/epay'] = 'webshop/payment/briEpay';
//$route['pay/bri/epay/proceed'] = 'webshop/payment/briEpayProceed';

$route['pay/finish'] = 'webshop/payment/afterPaymentPage';
$route['pay/unfinish'] = 'webshop/payment/unfinish';
$route['pay/error'] = 'webshop/payment/error';

/*---------------------
 * PAYMENT SGO
 ----------------------*/

//E-COMMERCE SALES
$route['cart/checkout/sgo'] = 'webshop/cartshop/CheckoutCartSGO';
$route['cart/checkout/sgo/pay/preview'] = 'webshop/cartshop/paySGOPreview';

//AKAN DI UPDATE 16/11/2018
//$route['cart/checkout/sgo/pay/preview2'] = 'webshop/cartshop/paySGOPreview2';
$route['cart/checkout/sgo/pay/preview2'] = 'webshop/Promo_nov/paySGOPreview2';

//$route['cart/checkout/sgo/pay/previewTes'] = 'webshop/promo_nov/paySGOPreview2';

$route['pay/sgo/inquiry'] = 'webshop/payment/getResponseFromSGO';
$route['pay/sgo/notif'] = 'webshop/payment/notifAfterPayment';
$route['pay/sgo/finish/(:any)'] = 'webshop/payment/afterPaymentWithSGO/$1';
//dev sgo
//$route['pay/sgo/inquiry/dev'] = 'webshop/payment/getResponseFromSGODev';
//$route['pay/sgo/notif/dev'] = 'webshop/payment/notifAfterPaymentDev';
//$route['pay/sgo/finish/dev/(:any)'] = 'webshop/payment/afterPaymentWithSGODev/$1';

/*-----------------------
* PAYMENT SALES USING VA
------------------------*/
$route['cart/pay/va'] = 'webshop/cartshop/payUsingVa';
$route['cart/pay/va2'] = 'webshop/cartshop/payUsingVa2';
//END

/*---------------------------
* PAYMENT REG MEMBER USING VA
-----------------------------*/
$route['member/reg/pay/va'] = 'webshop/member/memberPayVa';
//END

//single payment gateway
//development
$route['pay/sgo/inquiry/dev'] = 'webshop/api_payment/getResponseFromSGO_dev';
$route['pay/sgo/notif/dev'] = 'webshop/api_payment/notifAfterPayment_dev';
//production
//$route['pay/sgo/inquiry'] = 'webshop/api_payment/getResponseFromSGO';
//$route['pay/sgo/notif'] = 'webshop/api_payment/notifAfterPayment';

$route['payment/finish/(:any)'] = 'webshop/payment/apiFinishPayment/$1';


$route['pay/sgo/finish/dev/(:any)'] = 'webshop/payment/afterPaymentWithSGODev/$1';
$route['catalogue/download/(:any)/(:any)'] = 'webshop/payment/downloader/$1/$2';


$route['pay/vch'] = 'webshop/payment/savePaymentVch';
$route['pay/vch/finish/(:any)'] = 'webshop/payment/afterSavePaymentVch/$1';
//end
$route['trxtest/(:any)'] = 'webshop/payment/testSSS/$1';
$route['rand/(:any)'] = 'webshop/payment/randomNumber/$1';
$route['pay/inp/test'] = 'webshop/payment/testInputSales';

//MEMBER REGISTRATION
$route['member/checkout/sgo'] = 'webshop/member/memberCheckoutSGO';
$route['member/sgo/inquiry'] = 'webshop/member_payment/getResponseFromSGO';
$route['member/sgo/notif'] = 'webshop/member_payment/notifAfterPayment';
$route['member/sgo/finish/(:any)'] = 'webshop/member_payment/afterPaymentWithSGO/$1';
$route['member/inp/test'] = 'webshop/member_payment/inputMemberTest';
$route['member/token/(:any)'] = 'webshop/member_payment/showNewMember/$1';
//SGO DEVELOPMENT
//$route['member/sgo/inquiry/dev'] = 'webshop/member_payment/getResponseFromSGODev';
//$route['member/sgo/notif/dev'] = 'webshop/member_payment/notifAfterPaymentDev';
//$route['member/sgo/finish/dev/(:any)'] = 'webshop/member_payment/afterPaymentWithSGODev/$1';

$route['member/sgo/inquiry/dev'] = 'webshop/api_payment/getResponseFromSGO';
$route['member/sgo/notif/dev'] = 'webshop/api_payment/notifAfterPayment';
$route['member/sgo/finish/dev/(:any)'] = 'webshop/member_payment/afterPaymentWithSGODev/$1';

$route['member/checkout/sgo/pay/preview'] = 'webshop/member/memberPreviewPay';

//END
//MEMBER LANDING PAGE REGISTRATION
$route['memberlp/checkout/sgo'] = 'webshop/member_lp/memberCheckoutSGO';
$route['memberlp/sgo/inquiry'] = 'webshop/member_lp_payment/getResponseFromSGO';
$route['memberlp/sgo/notif'] = 'webshop/member_lp_payment/notifAfterPayment';
$route['memberlp/sgo/finish/(:any)'] = 'webshop/member_lp_payment/afterPaymentWithSGO/$1';
$route['memberlp/inp/test'] = 'webshop/member_lp_payment/inputMemberTest';
$route['memberlp/inp/nopayment'] = 'webshop/member_lp_payment/testInputNoPayment';
//END

//DEVELOPMENT LANDING PAGE
$route['memberlp/sgo/inquiry/dev'] = 'webshop/member_lp_payment/getResponseFromSGODev';
$route['memberlp/sgo/notif/dev'] = 'webshop/member_lp_payment/notifAfterPaymentDev';
$route['memberlp/sgo/finish/dev/(:any)'] = 'webshop/member_lp_payment/afterPaymentWithSGODev/$1';

//END

$route['authorize'] = 'webshop/PaymentExample/authorizePayment';
$route['authorize_process'] = 'webshop/PaymentExample/checkoutAuthorize';

$route['virtualAccount'] = 'webshop/PaymentExample/virtualAccount';
$route['va_process'] = 'webshop/PaymentExample/processVa';

/*---------------------------
 ACCOUNT
*--------------------------*/
$route['myaccount'] = 'webshop/account/getAccountInfo';
$route['history/trx'] = 'webshop/account/getTransactionHistory';
$route['tracking'] = 'webshop/account/getPackageTrackingInfo';
$route['updEmailFirst'] = 'webshop/account/getUpdateEmail';
$route['tracking2'] = 'webshop/account/getPackageTrackingInfo2';
$route['tracking/process'] = 'webshop/account/trackingProcess';
$route['tracking/cnote/(:any)'] = 'webshop/account/getStatusCnoteByID/$1';
$route['myEmail'] = 'webshop/account/getEmailInfo';
$route['update/email'] = 'webshop/account/updtEmail';
$route['updtEmailMain'] = 'webshop/account/updtEmailMain';
$route['myPassword'] = 'webshop/account/getPasswordbyId';
$route['update/password'] = 'webshop/account/updPassByID';
$route['bnsstt_view'] = 'webshop/account/updateViewBnsStt';
$route['saldoVA'] = 'webshop/account/stmViewVA'; // nandang 201804
$route['bnsstt_view/update'] = 'webshop/account/updateStatusBnsSttView';
//$route['tracking/process/(:any)'] = 'webshop/account/trackingProcess/$1';

$route['jne/tarif'] = 'webshop/account/tarifJneNew';
$route['jne/tracking/(:any)/(:any)'] = 'webshop/shipping/jneTracking/$1/$2';
$route['jne/api/tes'] = 'webshop/shipping/tes';

/*-------------------------
 * MEMBER
 * ------------------------
 */
$route['downline/check/(:any)/(:any)'] = 'webshop/member/checkSatujaringan/$1/$2';
$route['member/choosePay'] = 'webshop/member/choosePayType';
$route['member/choosePay/pesan/(:any)'] = 'webshop/member/choosePayType/$1';
$route['member/choosePay/err'] = 'webshop/member/choosePayType/$1';
$route['member/voucher/check/(:any)/(:any)'] = 'webshop/member/getVoucherCheck/$1/$2';
$route['member/voucher/save'] = 'webshop/member/saveMemberWithVoucher';
$route['member'] = 'webshop/member/getRegisterMember2';
$route['member/back'] = 'webshop/member/backToUpdateDataMember';
$route['member/id/(:any)'] = 'webshop/member/getMemberInfo/$1';
$route['member/id3/(:any)'] = 'webshop/member/getMemberInfo3/$1';
$route['member/ktp/(:any)'] = 'webshop/member/checkNoKTP/$1';
$route['member/no_hp/(:any)'] = 'webshop/member/checkNoHP/$1';
$route['member/starterkit'] = 'webshop/member/chooseStarterkit';
$route['member/checkout'] = 'webshop/member/memberCheckout';
$route['member/starterkit/back'] = 'webshop/member/backToChooseStarterkit';
$route['member/listbank'] = 'webshop/member/getListAffiliateBank';
$route['member/listStockist'] = 'webshop/member/listStockist';

/**
 * @Author: Ricky
 * @Date: 2019-03-05 16:16:07
 * @Desc: Member registration dev routes
 */
$route['dev/member/check'] = 'webshop/member/checkFormData';

/* ------------------------------------------------------
 * TES MODIFIKASI K-NET.CO.ID/MEMBER PADA INPUT ALAMAT
 * ------------------------------------------------------
 */
$route['cobamember'] = 'webshop/cobamember/getRegisterMember';


/* ------------------------------------------------------ */

$route['member/recruit'] = 'webshop/member/formGetListRecruit';
$route['member/recruit/list'] = 'webshop/member/getListRecruit';
$route['member/payment/list'] = 'webshop/member/getListPayment';

$route['member/payment/card'] = 'webshop/member/chooseCardType';
$route['member/payment/dc/(:any)'] = 'webshop/member/chooseDebitCardPaymentType/$1';

$route['member/pay/cc'] = 'webshop/member_payment/creditCard';
$route['member/pay/mandiri/clickpay'] = 'webshop/member_payment/mandiriClickPay';
$route['member/pay/cimb/click'] = 'webshop/member_payment/cimbClick';
//$route['pay/cimb/click/proceed'] = 'webshop/payment/cimbClickProceed';
$route['member/pay/va'] = 'webshop/member_payment/virtualAccount';
$route['member/pay/bri/epay'] = 'webshop/member_payment/briEpay';

$route['member/check/inp/(:any)'] = 'webshop/member/checkInput/$1';
//$route['pay/bri/epay/proceed'] = 'webshop/payment/briEpayProceed';

//$route['pay/finish'] = 'webshop/member_payment/afterPaymentPage';

/*-------------------------
* MEMBER FILE STARTERKIT
* ------------------------
*/
$route['member/file/sk'] = 'webshop/member/getStarterkitFile';


/*-------------------------
* SALES MEMBER
* -------------------------
*/
$route['sales'] = 'webshop/sales/getSalesMember';
$route['sales/cek/transaksi'] = 'webshop/sales/postSalesMember';
$route['sales/detail/transaksi/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'webshop/sales/detailSalesMember/$1/$2/$3/$4/$5';
$route['sales/online'] = 'webshop/sales/getSalesMemberOnline';
$route['sales/cek/transOnline'] = 'webshop/sales/postSalesMemberOnline';
$route['sales/detail/transaksiOnline/(:any)/(:any)/(:any)'] = 'webshop/sales/detailSalesMemberOnline/$1/$2/$3';

$route['sales/direct/downline'] = 'webshop/sales/getDirectDownline';
$route['sales/direct/downlineInfo'] = 'webshop/sales/getDirectDownlineInfo';
$route['sales/direct/downlineInfo2/(:any)/(:any)/(:any)'] = 'webshop/sales/getDirectDownlineInfo2/$1/$2/$3';
$route['sales/print'] = 'webshop/sales/getPrintJaringan';
$route['sales/recruiter/bonus'] = 'webshop/sales/getRecruiterBonus';
$route['sales/recruiter/bonusinfo'] = 'webshop/sales/getDataRecruit';
$route['sales/recruiter/detailinfo'] = 'webshop/sales/getDetailRekrut';
$route['sales/cashback_hydro'] = 'webshop/sales/cashback_hydro';
$route['sales/cashback_hydro/process'] = 'webshop/sales/cashback_process';

$route['sales/cek_payment'] = 'webshop/sales/getSalesMemberPayment';
$route['sales/cek_trans_payment'] = 'webshop/sales/getTransPayment';

/*-------------------------
* BONUS MEMBER
* -------------------------
*/
$route['bonus'] = 'webshop/sales/getformBonusMember';
$route['bonus/action'] = 'webshop/sales/postBonusMember';
$route['bonus/action/pajak'] = 'webshop/sales/postPajakMember';
$route['bonus/beraksi'] = 'webshop/sales/postBonusMember3';
$route['bonus/cekvalidmember'] = 'webshop/sales/cekvalidmember';
$route['bonus/cekvalidmember2'] = 'webshop/sales/cekvalidmember2';

/*-------------------------
* MEMBER FROM LANDING PAGE
* ------------------------
*/
$route['member/reg/list/recruit'] = 'webshop/member_lp/getlistRecruitLP';
$route['member/reg/downline'] = 'webshop/member_lp/getUpdateDownline';
$route['member/reg/sponsor/update'] = 'webshop/member_lp/updateSponsor';

$route['member/reg/payment/card'] = 'webshop/member_lp/chooseCardType';
$route['member/reg/payment/dc/(:any)'] = 'webshop/member_lp/chooseDebitCardPaymentType/$1';



$route['blog/reg/(:any)'] = 'webshop/member_lp/regFromBlog/$1';
$route['member/reg/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage/$1/$2';
$route['member/reg/(:any)/(:any)/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage2/$1/$2/$3/$4';
$route['member/reg/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage3/$1/$2/$3/$4/$5'; //tambahan Hilal
$route['member/reg/starterkit'] = 'webshop/member_lp/chooseStarterkit2';
$route['shipping/reg/getStockist/(:any)'] = 'webshop/member_lp/getStockist/$1';
$route['shipping/reg/getPricecode/(:any)'] = 'webshop/member_lp/getPriceStockist/$1';
$route['shipping/reg/getPricecodeX/(:any)/(:any)'] = 'webshop/member_lp/getPriceStockistX/$1/$2'; //TAMBAHAN HILAL
$route['shipping/reg/kota/(:any)'] = 'webshop/member_lp/getListKota/$1';
$route['shipping/reg/kecamatan/(:any)'] = 'webshop/member_lp/getListKecamatan/$1';
$route['shipping/reg/kelurahan/(:any)'] = 'webshop/member_lp/getListKelurahan/$1';
$route['member/reg/checkout'] = 'webshop/member_lp/memberCheckout';


$route['reg/pay/cc'] = 'webshop/member_lp_payment/creditCard';
$route['reg/pay/mandiri/clickpay'] = 'webshop/member_lp_payment/mandiriClickPay';
$route['reg/pay/cimb/click'] = 'webshop/member_lp_payment/cimbClick';
//$route['pay/cimb/click/proceed'] = 'webshop/payment/cimbClickProceed';
$route['reg/pay/va'] = 'webshop/member_lp_payment/virtualAccount';
$route['reg/pay/bri/epay'] = 'webshop/member_lp_payment/briEpay';
//$route['pay/bri/epay/proceed'] = 'webshop/payment/briEpayProceed';
$route['memberlp/checkout/sgo/pay/preview'] = 'webshop/member_lp/memberLpPreviewPay';

/* --------------------------------
    * UMROH INTERFACE BY ANANECH
 * --------------------------------- */
$route['umroh'] = 'webshop/rohani_online/getFormUmroh';
//$route['rohani'] = 'webshop/rohani_online/getFormUmroh';
$route['umroh/helper/distInfo'] = 'webshop/rohani_online/helperDistributorInfo';
$route['umroh/helper/sponsorInfo'] = 'webshop/rohani_online/helperSponsorInfo';
$route['umroh/helper/ktpInfo'] = 'webshop/rohani_online/helperKtpInfo';
$route['umroh/helper/hpInfo'] = 'webshop/rohani_online/helperNoHpInfo';
$route['umroh/add/jamaah'] = 'webshop/rohani_online/tambahJamaahUmroh';
$route['umroh/preview'] = 'webshop/rohani_online/previewJamaahUmroh';
$route['umroh/payment'] = 'webshop/rohani_online/getPaymentUmroh';
$route['umroh/input/finish'] = 'webshop/rohani_online/pendingUmroh';

$route['umroh/responseSgo'] = 'webshop/rohani_online/getInquirySgo';
$route['umroh/post/payment'] = 'webshop/rohani_online/postPaymentUmrohSgo/';

$route['umroh/responseSgoDev'] = 'webshop/rohani_online/getInquirySgoDev';
$route['umroh/post/paymentDev'] = 'webshop/rohani_online/postPaymentUmrohSgoDev/';

$route['umroh/helper/regnoInfo'] = 'webshop/rohani_online/helperRegnoInfo';
$route['umroh/installments'] = 'webshop/rohani_online/getFormInstallment';
$route['umroh/installments/pay'] = 'webshop/rohani_online/getFormInstallmentPay';

$route['umroh/finish/paymentCicilan/(:any)'] = 'webshop/rohani_online/finishPayCicilanUmroh/$1';
$route['umroh/finish/payment/(:any)'] = 'webshop/rohani_online/finishPaymentUmroh/$1';
$route['umroh/delete/sessJamaah/(:any)'] = 'webshop/rohani_online/deleteJamaahSess/$1';

$route['umroh/cek/mutasi'] = 'webshop/rohani_online/cekMutasiUmroh/';
$route['umroh/get/mutasi'] = 'webshop/rohani_online/getMutasiUmroh/';
$route['umroh/print/notaUmroh'] = 'webshop/rohani_online/printNotaUmroh/';
$route['umroh/print/notaUmroh1'] = 'webshop/rohani_online/printNotaUmroh1/';
$route['umroh/print/notaCicilanUmroh'] = 'webshop/rohani_online/printNotaCicilan/';

$route['umroh/reprint'] = 'webshop/rohani_online/getFormReprint';
$route['umroh/reprint/act'] = 'webshop/rohani_online/reprintRegno';

$route['umroh/printVchNo'] = 'webshop/rohani_online/getFormPrintVchno';
$route['umroh/printVchNo/act'] = 'webshop/rohani_online/printVchnoUmroh';
$route['umroh/printVchNo/txt'] = 'webshop/rohani_online/printVchnoTxt';

$route['umroh/sess'] = 'webshop/rohani_online/getSessionUmroh';
$route['umroh/sess/clear'] = 'webshop/rohani_online/clear_sess';

/* ------------------------------ *
 *   ZIARAH INTERFACE BY ANANECH   *
 * ------------------------------ */
$route['ziarah'] = 'webshop/rohani_online/getFormZiarah';
$route['ziarah/inquirySgo'] = 'webshop/rohani_online/inquiryZiarahSgo';
$route['ziarah/inquirySgoDev'] = 'webshop/rohani_online/inquiryZiarahSgoDev';

$route['ziarah/notifPaySgo'] = 'webshop/rohani_online/paymentZiarahSgo/';
$route['ziarah/notifPaySgoDevProduction'] = 'webshop/rohani_online/paymentZiarahSgoDev/';
$route['ziarah/finish/payment/(:any)'] = 'webshop/rohani_online/finishPaymentZiarah/$1';

$route['ziarah/helper/regnoInfo'] = 'webshop/rohani_online/helperRegnoInfo1';
$route['ziarah/installments'] = 'webshop/rohani_online/getFormCicilanZiarah';
$route['ziarah/installments/pay'] = 'webshop/rohani_online/getFormPayZiarah';

$route['ziarah/finish/paymentCicilan/(:any)'] = 'webshop/rohani_online/finishPayCicilanZiarah/$1';



//author : nandang
$route['umroh/saving'] = 'webshop/umroh_cicil/cicilUmroh';

$route['barcodetest'] =  'webshop/account/tes_barcode';
$route['promoayu/(:any)'] = 'webshop/C_ayupromo50/ambildata/$1';
$route['promo/(:any)/(:any)'] = 'webshop/promo_campaign/getListPromo/$1/$2';

$route['detprdSGO/(:any)'] = 'webshop/payment/testDetPrdSGO/$1';

//hilal promo stockist 100 bv free app
$route['be_stockist/addpromo100']='backend/stockist/be_stockist/form_addpromo100';
$route['be_stockist/listPromo']='backend/stockist/be_stockist/getListStockistPromo';
$route['be_stockist/savePromo']='backend/stockist/be_stockist/saveStockistPromo';
$route['be_stockist/deleteSCPromo/(:any)/(:any)/(:any)'] = 'backend/stockist/be_stockist/deleteStockistPromo/$1/$2/$3';
$route['be_stockist/ship/reff'] = 'backend/stockist/be_stockist/updateStkReff';
$route['be_stockist/ship/reff/update'] = 'backend/stockist/be_stockist/saveUpdateStkReff';

/*--------------------------
 * CAMPAIGN
 * ------------------------ */
/*
$route['product/list'] = 'backend/product/getListAllProduct';
$route['product/list/json'] = 'backend/product/getListAllProduct/$1';
$route['product/id/(:any)'] = 'backend/product/getListProductByID/$1';
$route['product/list/(:any)/(:any)'] = 'backend/product/getListProduct/$1/$2';
*/
$route['campaign'] = 'backend/campaign/campaign/getInputCampaignPhoto';
$route['campaign/save'] = 'backend/campaign/campaign/saveInputCampaign';
$route['campaign/list'] = 'backend/campaign/campaign/getListAllCampaign';
$route['campaign/det/(:any)'] = 'backend/campaign/campaign/getDetCampaignByID/$1';
//$route['campaign/det/(:any)'] = 'backend/campaign/campaign/getDetCampaign/$1';
//$route['product/update'] = 'backend/campaign/campaign/saveUpdateProduct';
//$route['product/delete/(:any)'] = 'backend/product/deleteProduct/$1';

$route['campaign/listUploadAyu']='backend/campaign/ayuIndia/getlistUploadToWeb';
$route['campaign/listUploadAyu/act/(:any)'] = 'backend/campaign/ayuIndia/reportAyuToWebAct/$1';
$route['campaign/listUploadAyu/xls'] = 'backend/campaign/ayuIndia/reportAyuToXlsWeb';

/*--------------------------
 * BACKPACK TRANSFER by DION
 * ------------------------ */
$route['be/backpack']='backend/stockist/be_backpack/formInputTrfBackpack';
$route['be/backpack/save']='backend/stockist/be_backpack/postSaveTrfBackpack';
$route['be/backpack/member/(:any)']='backend/stockist/be_backpack/getMemberInfo/$1';
$route['be/backpack/stockist/(:any)']='backend/stockist/be_backpack/getStockistInfo/$1';
$route['be/backpack/report']='backend/stockist/be_backpack/formReportTrfBackpack';
$route['be/backpack/report/list']='backend/stockist/be_backpack/getReportTrfBackpack';
$route['be/backpack/update/status']='backend/stockist/be_backpack/updateStatusPakai';
$route['be/backpack/detail/(:any)']='backend/stockist/be_backpack/getDetailTrf/$1';


/*--------------------------
 * BACKEND UTILITY bY HILAL
 * ------------------------ */

$route['trans/searchVcr'] = 'backend/transaction/getSearchVcr';
$route['trans/searchVcr/list'] = 'backend/transaction/postSearchVcr';
$route['hilal_email'] = 'sendEmail/sendEmailTest';
$route['transklink/searchTrx'] = 'backend/be_trans_klink/getSearchTrx';
$route['transklink/searchTrx/list'] = 'backend/be_trans_klink/postSearchTrx';
$route['transklink/detTrxByID/(:any)'] = 'backend/be_trans_klink/getDetailProductByID/$1';
$route['transklink/recapSales'] = 'backend/be_trans_klink/getRecapSales';
$route['transklink/recapSales/list/(:any)'] = 'backend/be_trans_klink/postRecapSales/$1';
$route['transklink/recapBonus'] = 'backend/be_trans_klink/getRecapBonus';
$route['transklink/recapBonus/list/(:any)'] = 'backend/be_trans_klink/postRecapBonus/$1';
$route['transklink/getStockist/(:any)'] = 'backend/be_trans_klink/getStockist/$1';
$route['transklink/gnvUpload'] = 'backend/be_trans_klink/gnvUpload';
$route['transklink/gnvUploadPrev'] = 'backend/be_trans_klink/gnvUploadPrev';
$route['transklink/gnvUploadAction'] = 'backend/be_trans_klink/gnvUploadAction';
$route['transklink/setBnsPeriod'] = 'backend/be_trans_klink/setBnsPeriod';
$route['transklink/setBnsPeriodAction/(:any)'] = 'backend/be_trans_klink/setBnsPeriodAction/$1';

$route['transklink/operatorSms'] = 'backend/be_trans_klink/getOperatorSms';
$route['transklink/operatorSms/list/(:any)'] = 'backend/be_trans_klink/getListAllOperator/$1';
$route['transklink/operatorSms/id/(:any)'] = 'backend/be_trans_klink/getListOperatorByID/$1';
$route['transklink/operatorSms/list/(:any)/(:any)'] = 'backend/be_trans_klink/getListOperator/$1/$2';
$route['transklink/operatorSms/postlist/(:any)'] = 'backend/be_trans_klink/postListOperator/$1';
$route['transklink/operatorSms/save'] = 'backend/be_trans_klink/saveInputOperator';
$route['transklink/operatorSms/update'] = 'backend/be_trans_klink/saveUpdateOperator';
$route['transklink/operatorSms/delete/(:any)'] = 'backend/be_trans_klink/deleteOperator/$1';
$route['transklink/broadcastSms'] = 'backend/be_trans_klink/getSearchSMS';

$route['transklink/recover/vc/ip'] = 'backend/be_trans_klink/recoverIncPayVc';
$route['transklink/recover/vc/ip/detail'] = 'backend/be_trans_klink/getDetailIncPayVc';
$route['transklink/recover/vc/ip/save'] = 'backend/be_trans_klink/saveRecoverIncPayVc';
$route['transklink/list/(:any)/(:any)'] = 'backend/be_trans_klink/listTTP/$1/$2';
$route['transklink/recover/ssr'] = 'backend/be_trans_klink/recoverSSR';
$route['transklink/recover/checkssr'] = 'backend/be_trans_klink/checkSSR';
$route['transklink/recover/update'] = 'backend/be_trans_klink/updateRecoverTrx';
$route['transklink/recover/remove'] = 'backend/be_trans_klink/removeTrx';
/*--------------------------
 * CAMPAIGN PROMO WEB BY DION
 * ------------------------ */
$route['promo/calendar'] = 'webshop/promo_campaign/formPromoCalendar';
//$route['promo/calendar/check/(:any)'] = 'webshop/promo_campaign/getListPromoCalender/$1';
$route['promo/calendar/check/(:any)/(:any)'] = 'webshop/promo_campaign/getListPromoCalender/$1/$2';
$route['promo/calendar/save'] = 'webshop/promo_campaign/savePromoCalendar';
$route['promo/calendar/print/(:any)'] = 'webshop/promo_campaign/printNota/$1';

/* ----------------------------------------
 *  BACKEND STOCKIST MAINTENANCE by ananech
 * ---------------------------------------- */
$route['be/stockist/maintenance'] = 'backend/stockist/be_stockist/getDtStockist';
$route['be/stockist/getListKecamatan'] = 'backend/stockist/be_stockist/getListKecamatan';
$route['be/stockist/updateStkEcomm'] = 'backend/stockist/be_stockist/updateStkEcommm';
$route['be/stockist/unsetStk'] = 'backend/stockist/be_stockist/unsetStockist';
$route['be/stockist/listKab/(:any)'] = 'backend/stockist/be_stockist/getListKab/$1';

/* -------------------------------------------
 * BACKEND DATA MEMBER LANDING PAGE BY ANANECH
 * ------------------------------------------- */
$route['be_member/memberLP'] = 'backend/backend_member/getListMemberLP';
$route['be_member/memberLP/act'] = 'backend/backend_member/listMemberLPRes';
$route['be_member/memberLP/download'] = 'backend/backend_member/listMemberLpCSV';

/*-------------------
 * follow up
 * -----------------*/
$route['followup'] = 'backend/follow_up/followupInputForm';
$route['followup/resign-reactive/(:any)'] = 'backend/follow_up/followupResignReactive/$1';
$route['followup/form/resign/(:any)/(:any)/(:any)'] = 'backend/follow_up/formResignation/$1/$2/$3';
$route['followup/form/resign'] = 'backend/follow_up/formResignation2';
$route['followup/resign/save']= 'backend/follow_up/saveResignTerminate';
$route['followup/form/reactive/(:any)/(:any)/(:any)'] = 'backend/follow_up/formReactive/$1/$2/$3';
$route['followup/detail/id/(:any)'] = 'backend/follow_up/getFollowUpDetailByID/$1';
$route['followup/memb/(:any)/(:any)'] = 'backend/follow_up/getMemberInfo/$1/$2';
$route['followup/update/id/(:any)'] = 'backend/follow_up/getUpdFollowUpDetailByID/$1';
$route['followup/update/bv/(:any)/(:any)/(:any)'] = 'backend/follow_up/getUpdateBV/$1/$2/$3';
$route['followup/status/update'] = 'backend/follow_up/updateStatusFollowUp';
$route['followup/update/bv/save'] = 'backend/follow_up/saveUpdateBv';
$route['followup/bv/form'] = 'backend/follow_up/transferBVForm';
$route['followup/trx_klink/(:any)'] = 'backend/follow_up/getDetailTrxByTrcd/$1';
$route['followup/bv/ttp/add/(:any)'] = 'backend/follow_up/transferBvCheckTTP/$1';
$route['followup/save'] = 'backend/follow_up/saveFollowupInput';
//update n delete followup
$route['followup/edit'] = 'backend/follow_up/followupEditForm';
$route['followup/edit/list'] = 'backend/follow_up/getListUpdateFollowup';
$route['followup/edit/save'] = 'backend/follow_up/saveEditFollowup';
$route['followup/edit/id/(:any)'] = 'backend/follow_up/formEditFollowUp/$1';
$route['followup/delete/id/(:any)'] = 'backend/follow_up/deleteFollowUp/$1';
//end
$route['followup/type'] = 'backend/follow_up/followupTypeInputForm';
$route['followup/type/list'] = 'backend/follow_up/followupTypeList';
$route['followup/type/save'] = 'backend/follow_up/saveFollowUpType';
$route['followup/type/update'] = 'backend/follow_up/updateFollowUpType';
$route['followup/type/delete(:any)'] = 'backend/follow_up/deleteFollowUpType/$1';
$route['followup/list'] = 'backend/follow_up/followupListForm';
$route['followup/list/get'] = 'backend/follow_up/getListFollowup';
$route['followup/process'] = 'backend/follow_up/followupProcess';
$route['followup/process/list'] = 'backend/follow_up/getListFollowUpToProcess';
$route['followup/process/excel'] = 'backend/follow_up/ExportFUtoExcel';

$route['followup/los'] = 'backend/follow_up/createLOS';
$route['followup/los/type/list'] = 'backend/follow_up/getListFollowupLOS';
$route['followup/los/save'] = 'backend/follow_up/saveLOS';
$route['followup/los/list'] = 'backend/follow_up/listLOS';
$route['followup/los/list/get'] = 'backend/follow_up/getListLOS';
$route['followup/los/print/trfname/(:any)'] = 'backend/follow_up/printTransferName/$1';
$route['followup/los/print/trfline/(:any)'] = 'backend/follow_up/printTransLine/$1';
$route['followup/los/print/trfrecruiter/(:any)'] = 'backend/follow_up/printTransRecruiter/$1';
$route['followup/los/print/excacc/(:any)'] = 'backend/follow_up/printExchangeAccount/$1';
$route['followup/los/print/resign/(:any)'] = 'backend/follow_up/printResignation/$1';
$route['followup/los/print/resign'] = 'backend/follow_up/printResignation2';
$route['followup/los/print/reactive/(:any)'] = 'backend/follow_up/printReactivation/$1';
$route['followup/los/print/reactive'] = 'backend/follow_up/printReactivation2';
$route['followup/los/print/namecorrection/(:any)'] = 'backend/follow_up/printCorrectionNameLos/$1';
$route['followup/los/batch'] = 'backend/follow_up/formBatchLos';
$route['followup/los/batch/list'] = 'backend/follow_up/getListLosByDate';
$route['followup/los/batch/save'] = 'backend/follow_up/saveBatchLos';
$route['followup/batch/list'] = 'backend/follow_up/formListBatchLos';
$route['followup/batch/list/get'] = 'backend/follow_up/getListBatchLos';
$route['followup/batch/id/(:any)'] = 'backend/follow_up/getDetailBatchById/$1';
$route['followup/batch/acc/update'] = 'backend/follow_up/updateReqAccBatch';
$route['followup/los/upload'] = 'backend/follow_up/losUploadForm';
$route['followup/los/upload/save'] = 'backend/follow_up/saveLosUpload';
$route['followup/los/upload/get'] = 'backend/follow_up/getListFU';
$route['followup/los/upload/form/(:any)'] = 'backend/follow_up/getFormUpload/$1';

$route['followup/img/(:any)/(:any)'] = 'backend/follow_up/imgDet/$1/$2';

$route['followup/mkdirtest'] = 'backend/follow_up/tesCreateDir';
$route['img-resize'] = 'backend/follow_up/resizeImg';

$route['followup/exchange/acc/(:any)/(:any)/(:any)'] = 'backend/follow_up/exchangeAccForm/$1/$2/$3';
$route['followup/exchange/acc'] = 'backend/follow_up/exchangeAccForm2';
$route['followup/exchange/acc/save']= 'backend/follow_up/saveExchangeAcc';

$route['followup/transfer/line/(:any)/(:any)/(:any)'] = 'backend/follow_up/tranferLineForm/$1/$2/$3';
$route['followup/transfer/line'] = 'backend/follow_up/tranferLineForm2';
$route['followup/transfer/line/save'] = 'backend/follow_up/tranferLineSave';

$route['followup/transfer/recruiter/(:any)/(:any)/(:any)'] = 'backend/follow_up/tranferRecruiterForm/$1/$2/$3';
$route['followup/transfer/recruiter'] = 'backend/follow_up/tranferRecruiterForm2';
$route['followup/transfer/recruiter/save'] = 'backend/follow_up/tranferRecruiterSave';

$route['followup/transfer/name/(:any)/(:any)/(:any)'] = 'backend/follow_up/tranferNameForm/$1/$2/$3';
$route['followup/transfer/name'] = 'backend/follow_up/tranferNameForm2';
$route['followup/transfer/name/save'] = 'backend/follow_up/tranferNameSave';

$route['followup/correction/name/(:any)/(:any)/(:any)'] = 'backend/follow_up/correctionNameForm/$1/$2/$3';
$route['followup/correction/name'] = 'backend/follow_up/correctionNameForm2';
$route['followup/correction/name/save'] = 'backend/follow_up/correctionNameSave';
$route['followup/temp_surat'] = 'backend/follow_up/temp_surat';


/*----------------------------
 * PROMO REKRUT & Kolagen Anniv
 * -------------------------*/
$route['promo_rekrut'] = 'webshop/promo_rekrut';
$route['promo_rekrut/id/(:any)'] = 'webshop/promo_rekrut/checkPoinRewards/$1';
$route['be/promo/rekrut'] = 'backend/be_promo/checkPoinRewards';
$route['be/promo/rekrut/id/(:any)'] = 'backend/be_promo/getPointRekrutByID/$1';
$route['promo_kolagen'] = 'webshop/promo_rekrut/promo_kolagen';
//$route['be/promo/rekrut/id/(:any)'] = 'backend/be_promo/getPointRekrutByID/$1';
$route['be/promo/anniv_kolagen'] = 'backend/be_promo/checkUndianAnnivKolagen';
$route['be/promo/anniv_kolagen/id/(:any)'] = 'backend/be_promo/getPointAnnivKolagenByID/$1';
$route['be/promo/anniv_kolagen/export'] = 'backend/be_promo/exportAnnivKolagen';
$route['be/promo/anniv_kolagen/export/header'] = 'backend/be_promo/exportAnnivKolagenHeader';
$route['be/promo/anniv_kolagen/export/detail'] = 'backend/be_promo/exportAnnivKolagenDetail';
$route['be/promo/config'] = 'backend/be_promo/promoConfig';
$route['be/promo/config/list'] = 'backend/be_promo/promoConfigList';
$route['be/promo/config/save'] = 'backend/be_promo/promoConfigSave';
$route['be/promo/config/id/(:any)'] = 'backend/be_promo/getPromoById/$1';
$route['be/promo/config/update'] = 'backend/be_promo/updatePromoConfig';

$route['be/promo/banner/config'] = 'backend/be_promo/inputBannerPromo';
$route['be/promo/banner/config/save'] = 'backend/be_promo/saveBannerPromo';
$route['be/promo/banner/config/update'] = 'backend/be_promo/updateBannerPromo';
$route['be/promo/banner/id/(:any)'] = 'backend/be_promo/getBannerPromoById/$1';
$route['be/promo/banner/config/list'] = 'backend/be_promo/bannerList';

/*--------------------------------
 * PROMO KOLAGEN 400 BV
 *------------------------------- */
$route['be/promo/kolagen400bv'] = 'backend/be_promo/formKolagen400Bv';
$route['be/promo/kolagen400bv/redemp/check'] = 'backend/be_promo/checkRedempRemain';
$route['be/promo/kolagen400bv/stockist/check/(:any)'] = 'backend/be_promo/checkDataStockist/$1';
$route['be/promo/kolagen400bv/price/(:any)/(:any)'] = 'backend/be_promo/checkDataProduct/$1/$2';
$route['be/promo/kolagen400bv/redemp/save'] = 'backend/be_promo/saveKolagenRedemp';
$route['be/promo/kolagen400bv/generate'] = 'backend/be_promo/formGenerateKolagenRedemp';
$route['be/promo/kolagen400bv/generate/list'] = 'backend/be_promo/getListKolagenToGenerate';
$route['be/promo/kolagen400bv/generate/save'] = 'backend/be_promo/saveGenerateKolagen';
$route['be/promo/kolagen400bv/generate/save2'] = 'backend/be_promo/saveGenerateKolagen2';
//$route['be/promo/kolagen400bv'] = 'backend/be_promo/formKolagen400Bv';

/* ============================================= *
 *  PENDING TRANSACTION REPORT K-NET by Ananech  *
 * ============================================= */
$route['trans/report/pending'] = 'backend/transaction/getTransReportPending';
$route['trans/report/pending/list'] = 'backend/transaction/postTransReportPending';
$route['trans/payment/pending/report'] = 'backend/transaction/getTransPaymentRptPending';
$route['trans/pending/det/(:any)'] = 'backend/transaction/getDetPendingTrx/$1';
$route['trans/reconcile/cs'] = 'backend/transaction/reconcileTrxCS';
$route['trans/pending/check/(:any)'] = 'backend/transaction/checkNmStockist/$1';
/* =============================================== *
 * END PENDING TRANSACTION REPORT K-NET by Ananech *
 * =============================================== */

/* ============================================= *
 *  PENDING TRANSACTION REPORT K-NET by Vera Yunita  *
 * ============================================= */

$route['trans/reconcile/trans_manual'] = 'backend/transaction/reconcileTrxManual';
$route['trans/sgo/reconcile/manual/(:any)'] = 'backend/transaction/getListTrxManual/$1';
$route['trans/sgo/reconcile/postmanual'] = 'webshop/PaymentVera/status_update2'; //production

/* ============================================= *
 *  END PENDING TRANSACTION REPORT K-NET by Vera Yunita  *
 * ============================================= */

/* ===================================================== *
 * REPORT POIN REWARD RECRUITER BACKEND K-NET by Ananech *
 * ===================================================== */
$route['recruit/rptRedemp'] = 'backend/be_promo/getRptRecruitRedemp';
$route['recruit/redemp/list'] = 'backend/be_promo/listRecruitRedemp';
$route['recruit/redemp/details/(:any)'] = 'backend/be_promo/detRecruitRedemp/$1';
/* ========================================================= *
 * END REPORT POIN REWARD RECRUITER BACKEND K-NET by Ananech *
 * ========================================================= */


/* ===================================================== *
 * PROMO OLIVE OIL & K-HONEY, INITIATIVE ELITE CHALLANGE *
 * ===================================================== */
$route['promo/oliveKhoney'] = 'webshop/promo_rekrut/listOliveHoney';
$route['be/iec'] = 'backend/klinkpromo/iec_promo/iecPromoForm';
$route['be/iec/result/(:any)'] = 'backend/klinkpromo/iec_promo/iecPromoResult/$1';
$route['be/iec/detail/(:any)/(:any)/(:any)/(:any)'] = 'backend/klinkpromo/iec_promo/iecPromoDetail/$1/$2/$3/$4';

$route['iec'] = 'webshop/promo_iec/listIec';
$route['iec/list'] = 'webshop/promo_iec/getListIecRes';
/* =========================================================================== *
 * END REPORT PROMO OLIVE OIL & K-HONEY, INITIATIVE ELITE CHALLANGE by Ananech *
 * =========================================================================== */


/* ============================= *
 *  Managament Asset By Ananech  *
 * ============================= */
$route['ma/catInv'] = 'backend/managementA/management_asset/categoryFormInput';
$route['ma/catInv/check/(:any)/(:any)'] = 'backend/managementA/management_asset/catInvByID/$1/$2';
$route['ma/catInv/save'] = 'backend/managementA/management_asset/categoryInvSave';
$route['ma/catInv/update'] = 'backend/managementA/management_asset/categoryInvUpdate';
$route['ma/catInv/delete/(:any)'] = 'backend/managementA/management_asset/categoryInvDelete/$1';
$route['ma/catInv/list'] = 'backend/managementA/management_asset/getListCatInv';
$route['ma/catInv/list/(:any)'] = 'backend/managementA/management_asset/getListCatInvById/$1';

$route['ma/supplier'] = 'backend/managementA/management_asset/supplierFormInput';
$route['ma/supplier/check/(:any)/(:any)'] = 'backend/managementA/management_asset/cekSupplierByID/$1/$2';
$route['ma/supplier/save'] = 'backend/managementA/management_asset/supplierSave';
$route['ma/supplier/list'] = 'backend/managementA/management_asset/getListSupplier';
$route['ma/supplier/update'] = 'backend/managementA/management_asset/supplierUpdate';
$route['ma/supplier/delete/(:any)'] = 'backend/managementA/management_asset/supplierDelete/$1';

$route['ma/inv'] = 'backend/managementA/management_asset/invFormInput';
$route['ma/inv/check/(:any)/(:any)'] = 'backend/managementA/management_asset/invByID/$1/$2';
$route['ma/inv/save'] = 'backend/managementA/management_asset/invSave';
$route['ma/inv/list'] = 'backend/managementA/management_asset/getListInv';
$route['ma/inv/update'] = 'backend/managementA/management_asset/invUpdate';
$route['ma/inv/delete/(:any)'] = 'backend/managementA/management_asset/invDelete/$1';

$route['ma/employee'] = 'backend/managementA/management_asset/empFormInput';
$route['ma/employee/check/(:any)/(:any)'] = 'backend/managementA/management_asset/employeeByID/$1/$2';
$route['ma/employee/save'] = 'backend/managementA/management_asset/employeeSave';
$route['ma/employee/list'] = 'backend/managementA/management_asset/getListEmploy';
$route['ma/employee/update'] = 'backend/managementA/management_asset/employUpdate';
$route['ma/employee/delete/(:any)'] = 'backend/managementA/management_asset/employDelete/$1';

$route['ma/mutasi/inv'] = 'backend/managementA/management_asset/mutasiInvForm';
$route['ma/mutasi/save'] = 'backend/managementA/management_asset/mutasiInvSave';
$route['ma/mutasi/list'] = 'backend/managementA/management_asset/getListMutasi';
$route['ma/mutasi/check/(:any)/(:any)'] = 'backend/managementA/management_asset/mutasiByID/$1/$2';
$route['ma/mutasi/update'] = 'backend/managementA/management_asset/mutasiUpdate';
$route['ma/mutasi/delete/(:any)'] = 'backend/managementA/management_asset/mutasiDelete/$1';
$route['ma/mutasi/report'] = 'backend/managementA/management_asset/mutasiFrmRpt';
$route['ma/mutasi/report/act/(:any)'] = 'backend/managementA/management_asset/mutasiRptAction/$1';
/* ================================= *
 *  end Managament Asset By Ananech  *
 * ================================= */

/* ================================= *
 *    Send BV to Email By Ananech    *
 * ================================= */
$route['send/bv'] = 'backend/broadcast_email/getFrmSendBV';
$route['send/bv/act'] = 'backend/broadcast_email/getListBVById';
$route['send/bnsreport'] = 'backend/broadcast_email/getSendBnsReport';
$route['send/bnsreport/act'] = 'backend/broadcast_email/sendBnsReport';

/* =================================== *
 *    end Send BV to Email By Ananech  *
 * =================================== */


/* ========================================= *
 *  SISTEM INFORMASI KEPEGAWAIAN BY ANANECH  *
 * ========================================= */
$route['sik/employee'] = 'backend/managementA/si_kepegawaian/empSIkFrmInput';
$route['sik/employee/save'] = 'backend/managementA/si_kepegawaian/emplSikSave';
$route['sik/employee/update'] = 'backend/managementA/si_kepegawaian/emplSikUpdate';

/* ============================================= *
 *  END SISTEM INFORMASI KEPEGAWAIAN BY ANANECH  *
 * ============================================= */


/*-------------------
 * KGB EXPEDISI ROUTE
 * -----------------*/
$route['kgb/kota/list'] = 'backend/kgb/listKota';
$route['kgp/track/(:any)'] = 'webshop/account/kgpTrackCnote/$1';


/*-----------------
 * MOBILE APPS API
 * ----------------*/
/*$route['api/login'] = 'mobile_api/handleLoginAPI';
$route['api/err'] = 'mobile_api/failApiResponse';
$route['api/otp/get'] = 'mobile_api/sendOTP';
$route['api/otp/auth'] = 'mobile_api/OtpAuth';
$route['api/password/send'] = 'mobile_api/sendPassword';
$route['api/password/change'] = 'mobile_api/changePassword';
//$route['api/member/id/(:any)'] = 'mobile_api/getMemberInfo/$1';
$route['api/member/info'] = 'mobile_api/getMemberInfo/$1';
//$route['api/member/check/(:any)/(:any)'] = 'mobile_api/checkDoubleDataMsmemb/$1/$2';
$route['api/member/id'] = 'mobile_api/getMemberInfoByID';
$route['api/member/name'] = 'mobile_api/getMemberInfoByName';
$route['api/member/recruit/list'] = 'mobile_api/getListRecruit';
$route['api/member/check/(:any)'] = 'mobile_api/checkDoubleDataMsmemb/$1';
$route['api/sponsor/check'] = 'mobile_api/getSponsorInfo';
$route['api/sponsor/area/list'] = 'mobile_api/getListAreaForUpline';
$route['api/sponsor/list'] = 'mobile_api/getListUplineByArea';

$route['api/member/checkout'] = 'mobile_api/memberCheckout';
$route['api/product/cat/list'] = 'mobile_api/getListProductCat';
//$route['api/product/list/cat/(:any)'] = 'mobile_api/getListAllProductByCat/$1';
$route['api/product/list/cat'] = 'mobile_api/getListAllProductByCat';
$route['api/product/id'] = 'mobile_api/getDetailPrdcdByID';
$route['api/product/name'] = 'mobile_api/getDetailPrdcdByName';
$route['api/voucher/check'] = 'mobile_api/checkValidasiVoucher';
$route['api/account/list'] = 'mobile_api/getListAffiliateBank';
$route['api/stockist/vch/list'] = 'mobile_api/getListStockist';
$route['api/starterkit/list'] = 'mobile_api/getListStarterkit';

$route['api/product/display'] = 'mobile_api/listProductDisplay';
$route['api/product/display/detail'] = 'mobile_api/listProductDisplayDetail';
$route['api/product/display/id'] = 'mobile_api/getDetailProduk';
//shipping
//$route['api/cargo/list'] = 'mobile_api/listCargoProvince';
$route['api/cargo/list'] = 'mobile_api/listCargo';
$route['api/province/list'] = 'mobile_api/listCargoProvince';
$route['api/kota/list'] = 'mobile_api/listCargoKota';
$route['api/kec/list'] = 'mobile_api/listCargoKecamatan';
//$route['api/stockist/list'] = 'mobile_api/listCargoStockist';
$route['api/stockist/list'] = 'mobile_api/listCargoKecamatanJNE';
$route['api/pricecode'] = 'mobile_api/setPricecode';
$route['api/shipping/price'] = 'mobile_api/getShippingCost';
$route['api/stockist/info'] = 'mobile_api/getInfoStockist';
//sales
$route['api/bnsperiod'] = 'mobile_api/getCurrentPeriod';
$route['api/sales/checkout'] = 'mobile_api/salesCheckout';
$route['api/sales/checkout/tes/(:any)'] = 'mobile_api/salesCheckout2/$1';
//payment
$route['api/payment/list'] = 'mobile_api/getListPaymentType';
$route['api/payment/detail'] = 'mobile_api/getPaymentInfoByID';


//my sales trx
$route['api/sales/member'] = 'mobile_api/getSalesMember';
$route['api/member/info/update'] = 'mobile_api/updateInfoMember';
$route['api/personal/check/bv'] = 'mobile_api/checkPersonalBV';
$route['api/direct/downline/bv'] = 'mobile_api/directDownlineBv';
$route['api/my/info'] = 'mobile_api/myInfo';
$route['api/sales/member/detail'] = 'mobile_api/getSalesMemberDetail';
$route['api/delivery/status/update'] = 'mobile_api/updateDeliveryStt';
$route['api/listbonus'] = 'mobile_api/ListBonus';
//acara
$route['api/event/speaker/list'] = 'mobile_api/getListSpeaker';
$route['api/event/regional/list'] = 'mobile_api/getListRegional';
$route['api/event/list'] = 'mobile_api/getSummaryAcaraByPeriod';

//pay checkout
$route['api/pay/preview/(:any)'] = 'mobile_api/payPreview/$1';




$route['api/sales/finish/(:any)'] = 'mobile_api/paySalesFinish/$1';
$route['api/sales/succees/(:any)'] = 'mobile_api/paySalesSuccess/$1';
$route['api/sales/failed/(:any)'] = 'mobile_api/paySalesFailed/$1';
$route['api/member/finish/(:any)'] = 'mobile_api/payMemberFinish/$1';
$route['api/member/success/(:any)'] = 'mobile_api/payMemberSuccess/$1';
$route['api/member/failed/(:any)'] = 'mobile_api/payMemberFailed/$1';
$route['knet/sales/finish/(:any)'] = 'mobile_api/knetSalesFinish/$1';
$route['knet/member/finish/(:any)'] = 'mobile_api/knetMemberFinish/$1';

//menu home
$route['api/home'] = 'mobile_api/home';
$route['api/product/catalog'] = 'mobile_api/getCatalogPrd';
$route['api/material'] = 'mobile_api/getListMaterialDownload';
$route['api/member/update'] = 'mobile_api/updateInfoMember2';
$route['api/product/promoted'] = 'mobile_api/getListPromotedProducts';
$route['api/banner/activity'] = 'mobile_api/getListPromotionActivityBanner';
$route['api/bonus'] = 'mobile_api/getBonusReportMember';
$route['api/bonusm'] = 'mobile_api/getBonusReportMember2/$1/$2/$3/$4/$5/$5';
 $route['api/listbonus'] = 'mobile_api/ListBonus';
$route['encrypt/(:any)/(:any)'] = 'mobile_api/encryptToken/$1/$2';
*/

$route['api/member/cs-info'] =  'mobile_api/ShowMemberInfoJatis';
$route['api/enkrip/(:any)/(:any)'] =  'mobile_api/aes128Encrypt/$1/$2';

$route['api/err'] = 'mobile_api/failApiResponse';
$route['api/login'] = 'mobile_api/handleLoginAPI';
$route['api/otp/get'] = 'mobile_api/sendOTP';
$route['api/otp/auth'] = 'mobile_api/OtpAuth';
$route['api/password/send'] = 'mobile_api/sendPassword';
$route['api/password/change'] = 'mobile_api/changePassword';

$route['api/login2'] = 'mobile_api/handleLoginAPI2';

$route['api/sponsor/area/list'] = 'mobile_api/getListAreaForUpline';
$route['api/sponsor/list'] = 'mobile_api/getListUplineByArea';
$route['api/member/recruit/list'] = 'mobile_api/getListRecruit';

$route['api/member/info'] = 'mobile_api/getMemberInfo';
$route['api/member/id'] = 'mobile_api/getMemberInfoByID';
$route['api/member/name'] = 'mobile_api/getMemberInfoByName';
$route['api/member/check/(:any)'] = 'mobile_api/checkDoubleDataMsmemb/$1';

$route['api/member/email_hp'] = 'mobile_api/checkEmailHpMember';
$route['getToken'] = 'mobile_api/getToken';
$route['api/sponsor/check'] = 'mobile_api/getSponsorInfo';
$route['api/voucher/check'] = 'mobile_api/checkValidasiVoucher';
$route['api/account/list'] = 'mobile_api/getListAffiliateBank';
$route['api/stockist/vch/list'] = 'mobile_api/getListStockist';
$route['api/starterkit/list'] = 'mobile_api/getListStarterkit';
$route['api/starterkit/list2'] = 'mobile_api/getListStarterkit2';

$route['api/member/checkout'] = 'mobile_api/memberCheckout';
$route['api/member/checkoutTes'] = 'mobile_api/memberCheckoutTes';
$route['api/member/checkout/vch'] = 'mobile_api/saveMemberWithVoucher';
$route['api/member/membRegister'] = 'mobile_api/membRegister';
$route['api/member/membRegisterTes'] = 'mobile_api/membRegisterTesting';

$route['api/cargo/list'] = 'mobile_api/listCargo';
$route['api/province/list'] = 'mobile_api/listCargoProvince';
$route['api/kota/list'] = 'mobile_api/listCargoKota';
$route['api/kec/list'] = 'mobile_api/listCargoKecamatan';
$route['api/stockist/list'] = 'mobile_api/listCargoKecamatanJNE';

//$route['api/stockist/list'] = 'mobile_api/listCargoStockist';

//k-net baru
$route['api/sales/insert'] = 'mobile_api/simpanSalesKnetBaru';

$route['api/pricecode'] = 'mobile_api/setPricecode';
$route['api/stockist/info'] = 'mobile_api/getInfoStockist';
$route['api/shipping/price'] = 'mobile_api/getShippingCost';
$route['api/product/catalog'] = 'mobile_api/getCatalogPrd';
$route['api/product/cat/list'] = 'mobile_api/getListProductCat';
$route['api/product/list/cat'] = 'mobile_api/getListAllProductByCat';
$route['api/product/id'] = 'mobile_api/getDetailPrdcdByID';
$route['api/product/name'] = 'mobile_api/getDetailPrdcdByName';
$route['api/bnsperiod'] = 'mobile_api/getCurrentPeriod';
$route['api/sales/checkout/tes/(:any)'] = 'mobile_api/salesCheckout2/$1';
$route['api/sales/checkout'] = 'mobile_api/salesCheckout';
//untuk pebayaran menggunakan voucher produk & cash
$route['api/sales/checkout2'] = 'mobile_api/salesCheckoutVch';
//end
$route['api/product/display'] = 'mobile_api/listProductDisplay';
$route['api/product/display/detail'] = 'mobile_api/listProductDisplayDetail';
$route['api/product/display/id'] = 'mobile_api/getDetailProduk';
$route['api/payment/list'] = 'mobile_api/getListPaymentType';
$route['api/payment/detail'] = 'mobile_api/getPaymentInfoByID';
$route['api/sales/member'] = 'mobile_api/getSalesMember';
$route['api/sales/member/detail'] = 'mobile_api/getSalesMemberDetail';
$route['api/material'] = 'mobile_api/getListMaterialDownload';
$route['api/delivery/status/update'] = 'mobile_api/updateDeliveryStt';
$route['api/member/info/update'] = 'mobile_api/updateInfoMember';
$route['api/member/update'] = 'mobile_api/updateInfoMember2';
$route['api/personal/check/bv'] = 'mobile_api/checkPersonalBV';
$route['api/direct/downline/bv'] = 'mobile_api/directDownlineBv';
$route['api/my/info'] = 'mobile_api/myInfo';
$route['api/event/speaker/list'] = 'mobile_api/getListSpeaker';
$route['api/event/regional/list'] = 'mobile_api/getListRegional';
$route['api/event/list'] = 'mobile_api/getSummaryAcaraByPeriod';
$route['api/home'] = 'mobile_api/home';
$route['api/product/promoted'] = 'mobile_api/getListPromotedProducts';
$route['api/banner/activity'] = 'mobile_api/getListPromotionActivityBanner';
$route['api/bonus'] = 'mobile_api/getBonusReportMember';

$route['api/bonus/dev'] = 'mobile_api/getBonusReportMemberDev';

$route['api/bonusm/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'mobile_api/getBonusReportMember2/$1/$2/$3/$4/$5/$6';
$route['api/pay/preview/(:any)'] = 'mobile_api/payPreview/$1';
$route['api/sales/finish/(:any)'] = 'mobile_api/paySalesFinish/$1';

$route['api/sales/finishVch/(:any)'] = 'mobile_api/paySalesFinishVch/$1';

$route['api/stk/login'] = 'mobile_api/handleStkLoginAPI';
$route['api/stkdelivery/status/update'] = 'mobile_api/stkUpdateDeliveryStt';
$route['api/stkorder/verify/upload'] = 'mobile_api/uploadVeriryImg';
$route['api/stk/profile'] = 'mobile_api/stockistProfile';

//$route['api/sales/succees/(:any)'] = 'mobile_api/paySalesSuccess/$1';
$route['api/sales/success/(:any)'] = 'mobile_api/paySalesSuccess/$1';
$route['api/sales/failed/(:any)'] = 'mobile_api/paySalesFailed/$1';
$route['api/member/finish/(:any)'] = 'mobile_api/payMemberFinish/$1';
$route['api/member/success/(:any)'] = 'mobile_api/payMemberSuccess/$1';
$route['api/member/failed/(:any)'] = 'mobile_api/payMemberFailed/$1';
$route['knet/sales/finish/(:any)'] = 'mobile_api/knetSalesFinish/$1';
$route['knet/member/finish/(:any)'] = 'mobile_api/knetMemberFinish/$1';
$route['api/listbonus'] = 'mobile_api/ListBonus';
$route['api/listbonus2'] = 'mobile_api/ListBonus2';
$route['encrypt/(:any)/(:any)'] = 'mobile_api/encryptToken/$1/$2';
$route['apiencrypt'] = 'sendEmail/encryptTokenXX';
//$route['api/listbonus'] = 'mobile_api/ListBonus';
$route['api/sgo/inquiry'] = 'webshop/api_payment/getResponseFromSGO';
$route['api/sgo/notif'] = 'webshop/api_payment/notifAfterPayment';
$route['api/sgo/notif/dev'] = 'webshop/api_payment/notifAfterPayment_dev';
//$route['api/sgo/renotif'] = 'webshop/api_payment/renotifAfterPayment';
$route['api/sgo/renotif'] = 'webshop/api_payment_reconcile/renotifTransaction';

$route['api/voucher/(:any)/(:any)/(:any)'] = 'mobile_api/listVoucherMember/$1/$2/$3';
$route['api/voucher2/(:any)/(:any)/(:any)'] = 'mobile_api/listVoucherMember2/$1/$2/$3';
$route['api/voucher/sales/check'] = 'mobile_api/checkSalesVchGeneral';
$route['api/voucher/list'] = 'mobile_api/getListVoucherDiskon';
$route['api/tax/(:any)/(:any)/(:any)'] = 'mobile_api/listTaxMember/$1/$2/$3';

$route['api/getxl'] = 'mobile_api/getxl/';
$route['api/getStokXL'] = 'mobile_api/getStokXL/';
$route['api/getxl2'] = 'mobile_api/getxl2/';
$route['api/enkripxl'] = 'mobile_api/postenkripxl/';

$route['api/saldo/va'] = 'mobile_api/getSaldoVa';
$route['va/balance/(:any)'] = 'backend/hifi/getSaldoVa/$1';


$route['simcard'] = 'backend/simcard';
$route['simcard/cariKelipatan'] = 'backend/simcard/cariKelipatan';
$route['simcard/simCardSales'] = 'backend/simcard/simCardSales';
$route['simcard/simCardSalesMKT'] = 'backend/simcard/simCardSalesMKT';
$route['simcard/simCardInput'] = 'backend/simcard/simCardInput';
$route['simcard/report'] = 'backend/simcard/reportsimCard';
$route['simcard/getList'] = 'backend/simcard/getList';
$route['simcard/generate/(:any)'] = 'backend/simcard/generateReport/$1';
$route['simcard/printtxt/(:any)'] = 'backend/simcard/printTXT/$1';

$route['api/sms/send'] =  'mobile_api/sendSmsGateway';
$route['api/sms/sendtext/(:any)'] = 'webshop/api_sms/sendSMS/$1';
$route['api/sms/kosong'] = 'webshop/api_sms/kosong';

//test email ningsih
$route['api/email/send'] =  'webshop/Api_email/sendEmail';

$route['api/notif/reg'] = 'mobile_api/regNotifMember';
$route['api/notif/delete'] = 'mobile_api/deleteNotifMember';
$route['api/notif/blast'] = 'mobile_api/blastNotifMember';
//$route['api/notif/blast'] = 'mobile_api/blastNotifMember';
$route['api/order/stk/approve'] = 'mobile_api/approveStockistOrder';
$route['api/order/stk/cancel'] = 'mobile_api/cancelStockistOrder';
$route['api/trx/stk/takeorder'] = 'mobile_api/TrxOrderTakebyMS';
$route['api/order/stk/list'] = 'mobile_api/listStockistOrder';
$route['api/order/stk/detail'] = 'mobile_api/detailStockistOrder';
$route['api/stockist/update/loc'] = 'mobile_api/updateLocStockist';
$route['api/stk/password/send'] = 'mobile_api/stkSendPassword';

$route['api/notif/stockist'] = 'mobile_api/blastNotifStk';

/*---------------------
 * PROMO
 * -------------------*/

//AWAL ROUTING MAMPIRKAK
$route['mampirlagikak'] = 'nonmember_promo/nonmember_promo/homeLagi';
 /*
$route['mampirkakTes'] = 'nonmember_promo/nonmember_promo/home2';
$route['mampirkak'] = 'nonmember_promo/nonmember_promo/home'; //'product/getProduct' ; //
$route['mampirlagikak'] = 'nonmember_promo/nonmember_promo/homeLagi';
*/
$route['mampirkak/cart/list'] = 'nonmember_promo/nonmember_promo/listCart';
$route['mampirlagikak/cart/list'] = 'nonmember_promo/nonmember_promo/listCart';

$route['mampirkak/cart/listDev'] = 'nonmember_promo/nonmember_promo/listCartDev';
$route['mampirlagikak/cart/listDev'] = 'nonmember_promo/nonmember_promo/listCartDev';

$route['mampirkak/checkout'] = 'nonmember_promo/nonmember_promo/CheckoutCart';
$route['mampirlagikak/checkout'] = 'nonmember_promo/nonmember_promo/CheckoutCart';

$route['mampirkak/checkout2'] = 'nonmember_promo/nonmember_promo/CheckoutCart2';
$route['mampirlagikak/checkout2'] = 'nonmember_promo/nonmember_promo/CheckoutCart2';

$route['mampirkak/register_membership'] = 'nonmember_promo/register_membership'; //ningsih
$route['mampirlagikak/register_membership'] = 'nonmember_promo/register_membership'; //ningsih

$route['mampirkak/register_membership/back'] = 'nonmember_promo/register_membership/backToRegisterMember';
$route['mampirlagikak/register_membership/back'] = 'nonmember_promo/register_membership/backToRegisterMember';

$route['mampirkak/product'] = 'nonmember_promo/nonmember_product/getProduct'; //vera
$route['mampirlagikak/product'] = 'nonmember_promo/nonmember_product/getProduct'; //vera

$route['nonmember/getSponsorName'] = 'nonmember_promo/register_membership/getSponsorName'; //vera
$route['nonmember/getSponsorNamae'] = 'nonmember_promo/register_membership/getSponsorName2';
$route['mampirkak/productHeader2/cat/id/(:any)'] = 'nonmember_promo/nonmember_product/getProdByCatHeader/$1';//vera
$route['mampirlagikak/productHeader2/cat/id/(:any)'] = 'nonmember_promo/nonmember_product/getProdByCatHeader/$1';//vera

$route['mampirkak/product/cat/id/(:any)'] = 'nonmember_promo/nonmember_product/getProdByCat/$1';//vera
$route['mampirlagikak/product/cat/id/(:any)'] = 'nonmember_promo/nonmember_product/getProdByCat/$1';//vera

$route['mampirkak/productHeader/cat/id/(:any)'] = 'nonmember_promo/nonmember_product/getProdByCatHeader/$1';//vera
$route['mampirlagikak/productHeader/cat/id/(:any)'] = 'nonmember_promo/nonmember_product/getProdByCatHeader/$1';//vera

$route['mampirkak/product/name'] = 'nonmember_promo/nonmember_product/getProdByName';
$route['mampirlagikak/product/name'] = 'nonmember_promo/nonmember_product/getProdByName';

$route['nonmember/saveTemp'] = 'nonmember_promo/nonmember_promo/saveTempTrx';
$route['mampirkak/finish/id/(:any)'] = 'nonmember_promo/nonmember_promo/finishMampirKak/$1';
$route['mampirlagikak/finish/id/(:any)'] = 'nonmember_promo/nonmember_promo/finishMampirKak/$1';

//END MAMPIRKAK

$route['digitalproduct'] = 'nonmember_promo/nonmember_digital/home';
$route['digitalproduct/cart/list'] = 'nonmember_promo/nonmember_digital/listCart';
$route['digitalproduct/cart/CheckoutCartSGODigital'] = 'nonmember_promo/nonmember_digital/CheckoutCartSGODigital';
$route['digitalproduct/cart/submitDIGITAL'] = 'nonmember_promo/nonmember_digital/submitDIGITAL';
$route['digitalproduct/cart/finish/(:any)'] = 'nonmember_promo/nonmember_digital/afterPaymentWithSGODev/$1';

/*------------------
 DTC
 * ----------------*/
$route['dtc/login'] = 'backend/dtc_login/handleLogin';
$route['dtc'] = 'dtc/Cdtc_mbr';
$route['dtcdev'] = 'dtc/Cdtc_mbrDEV';
$route['dtc3'] = 'dtc/Cdtc_mbr/NonMember';
$route['dtc/submit'] = 'dtc/Cdtc_mbr/simpan';
$route['dtc/submitVera'] = 'dtc/Cdtc_mbr/simpanVera';
$route['dtcdev/submit'] = 'dtc/Cdtc_mbrDEV/simpan';
$route['dtc/cariID2'] = 'dtc/Cdtc_mbr/cariID2';
$route['dtc/cariID'] = 'dtc/Cdtc_mbr/cariID';
$route['dtc/listEventNew/(:any)'] = 'dtc/Cdtc_mbr/listEventByCategoryNew/$1';

$route['dtc/done/(:any)'] = 'dtc/Cdtc_mbr/done/$1';
$route['dtc/submit/va'] = 'dtc/Cdtc_mbr/saveTrxDTC_Va';
$route['dtc/check/beforeProcess'] = 'dtc/Cdtc_mbr/checkBeforePayment';


$route['dtc/listEvent/(:any)'] = 'dtc/Cdtc_mbr/listEventByCategory/$1';

$route['dtc/createIP/(:any)'] = 'dtc/Cdtc_mbr/generateIPTicket/$1';
/**
 * @Author: Ricky
 * @Date: 2019-04-04 17:21:31
 * @Desc: Power Net
 */

$route['dtc2'] = 'dtc/Cdtc_mbr_2';
//$route['dtc3'] = 'dtc/Cdtc_mbr/NonMember';
$route['dtc2/submit'] = 'dtc/Cdtc_mbr_2/simpan';
$route['dtc2/submitVera'] = 'dtc/Cdtc_mbr_2/simpanVera';
//$route['dtcdev/submit'] = 'dtc/Cdtc_mbrDEV/simpan';
$route['dtc2/cariID2'] = 'dtc/Cdtc_mbr_2/cariID2';
$route['dtc2/cariID'] = 'dtc/Cdtc_mbr_2/cariID';
$route['dtc2/listEvent/(:any)'] = 'dtc/Cdtc_mbr_2/listEventByCategory/$1';

$route['dtc2/done/(:any)'] = 'dtc/Cdtc_mbr_2/done/$1';
$route['dtc2/submit/va'] = 'dtc/Cdtc_mbr_2/saveTrxDTC_Va';
$route['dtc2/check/beforeProcess'] = 'dtc/Cdtc_mbr_2/checkBeforePayment';


$route['be_dtc/getDetailTiket'] = 'backend/Backend_dtc/getDetailTiket';
$route['be_dtc/getDetailTiketV2'] = 'backend/Backend_dtc/getDetailTiketV2';

$route['be_dtc'] = 'backend/Backend_dtc/formListing';
$route['be_dtc/submit'] = 'backend/Backend_dtc/submit';
$route['be_dtc/emailtest'] = 'backend/Backend_dtc/kirimEmail2/';


$route['etiket'] = 'backend/Etiket/getInputEtiket';
$route['etiket/list'] = 'backend/Etiket/getListAllTiket';
$route['etiket/save'] = 'backend/Etiket/saveInputEtiket';
$route['etiket/update'] = 'backend/Etiket/updateEtiket';
$route['etiket/updateNew'] = 'backend/Etiket/updateEtiketNew';

$route['etiket/report'] = 'backend/Etiket/formInputReport';
$route['etiket/reconcile'] = 'backend/Etiket/formInputReport';
$route['etiket/getReport'] = 'backend/Etiket/ReportEtiketData/0';
$route['etiket/cetak'] = 'backend/Etiket/ReportEtiketData/1';
$route['etiket/email'] = 'backend/Etiket/kirimEmail';
$route['etiket/status/update'] = 'backend/Etiket/updateStatusEtiket';
$route['etiket/listEventByCat/(:any)'] = 'backend/Etiket/listEventByCat/$1';

$route['etiket/cekin'] = 'backend/Etiket/formCheckin';
$route['etiket/validasitiket'] = 'backend/Etiket/validasitiket';
$route['etiket/submittiket'] = 'backend/Etiket/submittiket';

$route['etiket/listPesertaByAcara/(:any)'] = 'backend/Etiket/listPesertaByAcara/$1';
$route['etiket/resendsms/(:any)/(:any)'] = 'backend/Etiket/resendsms/$1/$2';

$route['etiket/pindahJadwal'] = 'backend/Etiket/pindahJadwal';
$route['etiket/pindahJadwal/save'] = 'backend/Etiket/pindahJadwalSave';

$route['etiket/merchandise/set/(:any)'] = 'backend/Etiket/formSetMerchandise/$1';
//$route['etiket/merchandise/tipe/(:any)'] = 'backend/Etiket/formSetMerchandise/$1';
$route['etiket/merchandise/getPrice'] = 'backend/Etiket/getHargaProduct';
$route['etiket/merchandise/save'] = 'backend/Etiket/saveMerchandise';
$route['etiket/location/input'] = 'backend/Etiket/formLocationInput';
$route['etiket/location/check/(:any)/(:any)'] = 'backend/Etiket/checkDoubleLocation/$1/$2';
$route['etiket/location/input/save'] = 'backend/Etiket/saveLocation';
$route['etiket/location/list'] = 'backend/Etiket/listLocation';
/**
 * @Author: Ricky
 * @Date: 2019-05-17 14:17:53
 * @Desc: Input TOM Manual
 */
$route['etiket/tom/manual'] = 'backend/Tom_manual/formInput';
$route['etiket/tom/manual/save'] = 'backend/Tom_manual/saveTomManual';
$route['etiket/tom/manual/list'] = 'backend/Tom_manual/listTomManual';

$route['etiket/getEditTiket/(:any)'] = 'backend/Etiket/getEditTiket/$1';
$route['etiket/list2'] = 'backend/Etiket/getListAllTRXTiket';
//$route['etiket/list2'] = 'backend/Backend_dtc/getListing';
$route['etiket/list3'] = 'backend/Etiket/getListKlEtiket';
$route['etiket/klaimEtiket/(:any)'] = 'backend/Etiket/KlaimEtiket/$1';
$route['etiket/klaim'] = 'backend/Etiket/klaim';
$route['etiket/delete/(:any)'] = 'backend/Etiket/deletetkt/$1';


$route['vtiket'] = 'dtc/Cdtc_mbr/vtiket';
$route['vtiket2/(:any)'] = 'dtc/Cdtc_mbr/vtiket2/$1';

$route['etiket/cat/input'] = 'backend/Etiket/inputEtiketCategory';
$route['etiket/cat/input/save'] = 'backend/Etiket/saveEtiketCategory';
$route['etiket/cat/id/(:any)'] = 'backend/Etiket/getEtiketCategoryById/$1';
$route['etiket/cat/update/save'] = 'backend/Etiket/updateEtiketCategory';
$route['etiket/cat/list'] = 'backend/Etiket/getListEtiketCategory';

$route['etiket/integrity/check'] = 'backend/Etiket/checkIntegrity';
$route['etiket/integrity/check/list'] = 'backend/Etiket/checkIntegrityList';


$route['iac'] = 'webshop/Promo_iac/listIac';
$route['iac/cetakpolos'] = 'webshop/Promo_iac/cetak2';
$route['iac/cetak'] = 'webshop/Promo_iac/cetak';

/*---------------------------
 * LOUNGE SMART OFFICE
 * ------------------------*/

$route['lounge/hilal/(:any)']='backend/Smoffice/getHilal/$1';
$route['loungeHilal']='backend/Smoffice/getInputEntryHilal';
$route['lounge/getListStatusHilal']='backend/Smoffice/getListStatusHilal';
$route['lounge/listHilal']='backend/Smoffice/getListAllAttendee';

$route['lounge']='backend/Smoffice/getInputEntry';
$route['lounge/list']='backend/Smoffice/getListAllAttendee';
$route['lounge/getBonus']='backend/Smoffice/getReportCahyono';
$route['lounge/saveInputOffice']='backend/Smoffice/saveInputOffice';
$route['lounge/nota/(:any)']='backend/Smoffice/cetakNota/$1';

$route['loungereport']='backend/Smoffice/getReportLounge';
$route['loungereport/getReport']='backend/Smoffice/ReportLoungeData';
$route['loungereport/cetak']='backend/Smoffice/cetakReport';


$route['klaim']='webshop/KlaimBonusPaket/getformKlaim';
$route['KlaimBonusPaket/getProdukKlaim/(:any)']='webshop/KlaimBonusPaket/getProdukKlaim/$1';
$route['KlaimBonusPaket/getDetKw/(:any)']='webshop/KlaimBonusPaket/getDetKw/$1';
$route['KlaimBonusPaket/getformstockist/(:any)/(:any)']='webshop/KlaimBonusPaket/getformstockist/$1/$2';
$route['KlaimBonusPaket/Submit']='webshop/KlaimBonusPaket/Submit';


$route['daftaremail']='webshop/Email_controllerss/netcoreAPI';


$route['promo_cashback']='webshop/Promo_cashback/listIac';
$route['undian']='dtc/Cdtc_mbr/undian';
$route['undian2']='dtc/Cdtc_mbr/undian2';

$route['sales/getReportCahyono']='webshop/sales/getReportCahyono';
$route['shop/productHeader1']='Product/getProdByCahyono1';

$route['promo_novapr/info'] = 'webshop/promo_novapr/getListPromo';

$route['api/sgo/notif/topupva/dev'] = 'webshop/api_payment/notifTopUpVa_dev';
$route['api/sgo/notif/topupva'] = 'webshop/api_payment/notifTopUpVa';

/*----------------------
 * API JATIS
 * --------------------*/
$route['api/hifi/trx'] = 'mobile_api/saveHifiTrx';

/* -------------------------------------------
 * VERIFIKASI SMS UTK CETAK VOUCHER&REPORT
 * -----------------------------------------*/
$route['tes_verifikasi'] = 'webshop/Verifikasi/verifikasi_sms';
$route['tes_tampilreport'] = 'webshop/Verifikasi/tampil_report';

/*----------------------
 * Voucher Klaim
 * --------------------*/
$route['be_voucher'] = 'backend/Backend_voucher/getFormScan';
$route['be_voucher/cariID'] = 'backend/Backend_voucher/cariID';

/*----------------------------------
 * Report statement bonus backend
  --------------------------------*/

$route['report/statement_bonus'] = 'backend/report/reportStatementBonus';

/*--------------------------------
 * Report Summ BV
 -------------------------------*/

$route['report/summ_bv'] = 'backend/report/Report_BV/formReportBV';
$route['report/summ_bv/tampil'] = 'backend/report/Report_BV/getListingReportBV';
$route['report/summ_bv/cetakpdf'] = 'backend/report/Report_BV/printReportBVPdf';
$route['report/summ_bv/cetakexcel'] = 'backend/report/Report_BV/printReportBVExcel';



/*----------------------------------
 * Recover Incoming Payment
  --------------------------------*/

$route['recover_ip'] = 'backend/stockist/Be_SCO_recover/index';
$route['recover_ip/getSSR'] = 'backend/stockist/Be_SCO_recover/getSSR';
$route['recover_ip/getDetail'] = 'backend/stockist/Be_SCO_recover/getDetail';
$route['recover_ip/delete/(:any)'] = 'backend/stockist/Be_SCO_recover/delete/$1';

/*---------------------------------
*   TES UNTUK GENERATE VOUCHER ACARA PAK RADZI
    ---------------------------------*/

$route['get_voucher'] = 'webshop/Tes_voucher/getVoucher';
$route['tes_voucher/produk'] = 'webshop/Tes_voucher/pilihProduk';
$route['tes_voucher/claim'] = 'webshop/Tes_voucher/claimVoucher';
$route['tes_voucher/proses'] = 'webshop/Tes_voucher/prosesVoucher';
$route['vch/winner/sgo/pay/preview'] = 'webshop/tes_voucher/payPreview';
$route['livestream/vch/finish/(:any)'] = 'webshop/payment/liveStreamVch/$1';

//test for listing voucher have released or claimed in event on April 11, 2018

$route['voucher_list/listVch'] = 'backend/voucher_list/voucher_list/listVch';
$route['voucher_list/getList'] = 'backend/voucher_list/voucher_list/getList';
$route['voucher_list/id/(:any)'] = 'backend/voucher_list/voucher_list/getDetByID/$1';

$route['generate_voucher'] = 'simulasi';

$route['snk'] = 'simulasi/snk';
$route['tes_voucher/sendSMS'] = 'webshop/Tes_voucher/sendSMS';

/*------------------
 * teleakses
 * --------------*/
$route['tele/api/bonus'] = 'teleakses/getBonusReportMember';



/*------------------
 * alat pengecek voucher
 * --------------*/
$route['cekvoucher']='backend/sales_stockist_report/voucherReport';
$route['cekvoucher/list'] = 'backend/sales_stockist_report/voucherReportList';
$route['cekvoucher/extend'] = 'backend/sales_stockist_report/extendExpiration';
//$route['cekvoucher/voucher/no/(:any)'] = 'backend/sales_stockist_report/getDetailVoucherNo/$1';
$route['cekvoucher/voucher/no/(:any)/(:any)'] = 'backend/sales_stockist_report/getDetailVoucherNo/$1/$2';

/* -----------------------------
 *  Promo Heboh Banget !!!
 * ---------------------------*/
$route['kupon-hebohbanget'] = 'webshop/Promo_heboh/getListKupon';
$route['det_kupon'] = 'webshop/Promo_heboh/getDetailKupon';

$route['be/report/hebohbanget'] = 'backend/report/Report_HB/formHB';
$route['be/report/list_hb'] = 'backend/report/Report_HB/getListHB';
$route['be/report/det_rekrutan'] = 'backend/report/Report_HB/detRekrutanHB';

/* -------------------------
    Promo LBC Trip to Bangkok
-----------------------------*/
$route['promo-lbc'] = 'webshop/Promo_lbc/getInfo';
$route['det_lbc'] = 'webshop/Promo_lbc/getDetailLBC';
$route['det_rekrutan_lbc'] = 'webshop/Promo_lbc/getRekrutanLBC';

/* -----------------------------
 *  Backend Report Bonus Timor Leste
 * ---------------------------*/
$route['be/report/timor_leste'] = 'backend/report/Report_timorleste';
$route['be/report/getDetailTimor'] = 'backend/report/Report_timorleste/getList';
$route['be/report/saveDetail'] = 'backend/report/Report_timorleste/saveDetail';
$route['be/report/timor_leste/detail/(:any)/(:any)'] = 'backend/report/Report_timorleste/getDetail/$1/$2';

$route['be/report/prevSave'] = 'backend/report/Report_timorleste/prevSave';
$route['be/report/insertDetail'] = 'backend/report/Report_timorleste/insertDetail';

/* -------------------------------
 *  Daily Omset Report
   -----------------------------*/
$route['be/daily_omset_report'] = 'backend/report/Report_omset/cariOmset';
$route['be/list_report'] = 'backend/report/Report_omset/listReport';

/*-------------------------------*/
$route['trans/null_shipping'] = 'backend/transaction/null_shipping';
$route['trans/get_ongkir'] = 'backend/transaction/get_ongkir';

$route['trans/null_shipping/cekOrderNo/(:any)'] = 'backend/transaction/cekOrderNo/$1';
$route['trans/null_shipping/cekTransShipAddrSgo/(:any)'] = 'backend/transaction/cekTransShipAddrSgo/$1';
$route['trans/null_shipping/cekTransShipAddr/(:any)'] = 'backend/transaction/cekTransShipAddr/$1';
$route['trans/null_shipping/(:any)/(:any)'] = 'backend/transaction/getInfoShipping/$1/$2';

/* -----------------------------
*   TES COBA2 PAK TOTO
   ---------------------------*/
$route['dodol_garut'] = 'webshop/Tes_paktoto/home';
$route['dodol'] = 'webshop/Tes_paktoto/home2';

//laporan LBC pak toto
$route['cek_LBC'] = 'backend/report/Cek_memberLBC/cek_member';
$route['tampil_memberLBC'] = 'backend/report/Cek_memberLBC/tampil_memberLBC';
$route['cetak_excelLBC'] = 'backend/report/Cek_memberLBC/cetak_memberLBC';

$route['be/print_salesLBC'] = 'backend/report/Cek_memberLBC/cek_salesLBC';
$route['be/print_LBC'] = 'backend/report/Cek_memberLBC/print_salesLBC';

/*------------ Vera ---------------*/
$route['member/cek_jarak/(:any)/(:any)'] = 'webshop/member/cek_jarak/$1/$2';

$route['lokasi'] = 'c_location';
$route['find_location/(:any)/(:any)'] = 'c_location/index/$1/$2';
$route['otp_location'] = 'c_location/verifikasi_sms3';
$route['otp_location2/(:any)/(:any)/(:any)'] = 'c_location/verifikasi_sms2/$1/$2/$3';
$route['otp_send/(:any)/(:any)/(:any)'] = 'c_location/otp_verifikasi/$1/$2/$3';
$route['rute'] = 'c_location/get_route';
$route['test_select'] = 'c_location/test_select';
$route['getRek'] = 'c_location/getRek';
$route['getUnique'] = 'c_location/getUnique';
$route['pay/sgo/selesai/dev'] = 'webshop/PaymentVera/afterBayar';

$route['cart/listvera'] = 'webshop/cartshop_vera/listCart';
$route['cart/checkout/sgovera'] = 'webshop/cartshop_vera/CheckoutCartSGO';
$route['cart/checkout/sgo/pay/verapreview2'] = 'webshop/cartshop_vera/paySGOPreview2';
$route['sales/tracking/(:any)'] = 'webshop/sales/getSalesTracking/$1';
$route['sales/rebooking/(:any)'] = 'webshop/sales/getRebooking/$1';
$route['sales/save_rebooking'] = 'webshop/sales/saveRebooking';
$route['sk_gosend'] = 'c_location/sk_gosend';

$route['tracking_gosend'] = 'c_location/getTracking';
$route['rebooking_gosend'] = 'c_location/getRebooking';

$route['gosend/cek_jarak/(:any)/(:any)'] = 'c_location/cek_jarak2/$1/$2';
$route['gosend/cek_jarak2/(:any)/(:any)'] = 'c_location/cek_jarak3/$1/$2';
$route['cek_warehouse'] = 'c_location/cek_warehouse';
$route['trans/gsend/RebookEConotG/(:any)/(:any)/(:any)'] = 'backend/rebook_gosend/updtEConotG/$1/$2/$3';
$route['trans/gsend/CancelEConotG/(:any)/(:any)/(:any)'] = 'backend/rebook_gosend/cancelEConotG/$1/$2/$3';
$route['cek_gs_product'] = 'c_location/cek_gs_product';

$route['getNo/(:any)'] = 'c_location/getSalesTracking/$1';
$route['getWebhook/(:any)'] = 'c_location/getWebhook/$1';

$route['getKodePay'] = 'c_location/getKodePay';
$route['getIpaymu'] = 'c_location/getIpaymu';
$route['getDistanceKP'] = 'c_location/getDistanceKP'; // COD
$route['getValidate'] = 'c_location/getValidate';

$route['dummyvera'] = 'dummy_promo/dummy_promo1/home'; //'product/getProduct'
$route['dummyvera1/getProd'] = 'dummy_promo/dummy_promo1/getProd';
$route['dummyvera1/getProd2'] = 'dummy_promo/dummy_promo1/getProd2';
$route['dummyvera1/getProd3'] = 'dummy_promo/dummy_promo1/getProd3';
$route['dummyvera1/getProd4'] = 'dummy_promo/dummy_promo1/getProd4';
$route['dummyvera1/getProdSP'] = 'dummy_promo/dummy_promo1/getProdSP';

$route['dummyvera/cart/list'] = 'dummy_promo/dummy_promo/listCart';
$route['dummyvera/getProd'] = 'dummy_promo/dummy_promo/getProd';

$route['cart/checkoutTes'] = 'webshop/Promo_bundle/CheckoutCart';
$route['cart/checkout/sgo/pay/previewTes'] = 'webshop/promo_bundle/paySGOPreview2';
$route['cart/register/newmemberTes'] = 'webshop/Promo_bundle/regisnewmember';
$route['cart/register/newmemberMarch'] = 'webshop/Promo_bundle/newmemberBeforeApr';
$route['cart/register/newmemberMarchProd'] = 'webshop/Promo_nov/newmemberBeforeApr';
$route['cart/register/promoApril'] = 'webshop/Promo_bundle/regisnewmember';
$route['webhook_payment'] = 'webshop/PaymentVera/status_update'; //production
$route['pay/manual'] = 'webshop/paymentvera/savePaymentVch';
$route['pay/manual/finish/(:any)'] = 'webshop/paymentvera/afterSavePaymentVch/$1';

$route['product/nonavailable'] = 'backend/product/getInputProductNon';
$route['product/list_non_available'] = 'backend/product/getListAll';
$route['product/cek_stokist'] = 'backend/product/cek_stokist';
$route['product/cek_nonavailable'] = 'backend/product/cek_nonavailable';
$route['product/save_nonavailable'] = 'backend/product/save_nonavailable';
$route['product/del_nonavailable'] = 'backend/product/del_nonavailable';

//API GOSEND
$route['cek_stockist'] = 'c_location/cek_jarak';
$route['cek_price'] = 'c_location/price_send';//production
$route['cek_price_dev'] = 'c_location/price_send_dev';//develop
$route['make_booking'] = 'c_location/booking_send';
$route['status_booking'] = 'c_location/status_send';
$route['webhook_gosend'] = 'c_location/status_update'; //production
$route['webhook'] = 'c_location/status_tracking'; //develop
$route['get_jarak'] = 'c_location/cek_jarak1';

$route['otp_proses/(:any)/(:any)/(:any)'] = 'c_location/otp_proses/$1/$2/$3';
$route['otp_proses2/(:any)/(:any)/(:any)'] = 'c_location/otp_proses2/$1/$2/$3';


/*----------------------------------
 *  cetak surat pemberitahuan PDF
  --------------------------------*/
$route['be/form_cetak'] = 'backend/report/Cetak_surat/tampil_cetak';
$route['be/print_surat'] = 'backend/report/Cetak_surat/cetak';


/*-----------------------
AUDIO STORE API
-----------------------*/
$route['api/audio/available/list'] = 'audio_store/availableAudioList';
$route['api/audio/mylist/add'] = 'audio_store/addMyAudioStore';
$route['api/audio/mylist'] = 'audio_store/myAudioStore';
$route['api/audio/voucher/verify'] = 'audio_store/verifyAudioVoucher';

$route['api/xlstringkey'] = 'audio_store/xlStringKey';

$route['jneCheckPrice'] = 'webshop/shipping/checkHargaJne';

$route['api/region/list'] = 'audio_store/listRegion';
$route['api/stockist-by-region/list'] = 'audio_store/listStockistByRegion';
$route['api/webinar/list'] = 'audio_store/listWebinar';
$route['api/lbc/list'] = 'audio_store/listLbcNews';
/*-------------------------
CASHBACK 500RB HYDRO WATER
--------------------------*/
$route['hydro/tessms'] = 'backend/cashback_promo/tesSendSms';
$route['cashback/hydrowater'] = 'backend/cashback_promo/hydrowater';
$route['cashback/hydrowater/list'] = 'backend/cashback_promo/getListCashbackHydroWater';
$route['cashback/hydrowater/batch/process'] = 'backend/cashback_promo/previewBatchCashbackHydroWater';
$route['cashback/hydrowater/batch/save'] = 'backend/cashback_promo/saveBatchCashbackHydroWater';
$route['cashback/hydrowater/batch/cancel'] = 'backend/cashback_promo/cancelBatchHydroWater';
$route['cashback/hydrowater/batch/list'] = 'backend/cashback_promo/listAllBatch';
$route['cashback/hydrowater/batch/print'] = 'backend/cashback_promo/printBatch';
$route['cashback/hydrowater/batch/reject_prev'] = 'backend/cashback_promo/prevRejectTransfer';
$route['cashback/hydrowater/batch/reject'] = 'backend/cashback_promo/rejectTransfer';
$route['cashback/hydrowater/batch/id'] = 'backend/cashback_promo/listTransferByBatchNo';
$route['cashback/hydrowater/batch/ok'] = 'backend/cashback_promo/TransferOk';
$route['cashback/hydrowater/sendsms'] = 'backend/cashback_promo/sendSms';
$route['cashback/hydrowater/report'] = 'backend/cashback_promo/hydrowaterReport';
$route['cashback/hydrowater/report/list'] = 'backend/cashback_promo/hydrowaterReportList';
$route['cashback/hydrowater/pending/(:any)'] = 'backend/cashback_promo/pendingTrf/$1';
$route['cashback/hydrowater/trfprocess/(:any)'] = 'backend/cashback_promo/listTrfByBatch/$1';
$route['cashback/update/adminbank'] = 'backend/cashback_promo/updateAdminBank';
$route['cashback/hydrowater/norekhp/excel'] = 'backend/cashback_promo/norekhpExcel';


/**
 * @Author: Ricky
 * @Date: 2019-04-05 10:01:20
 * @Desc: Landing Page Routes
 */
$route['hydro/tessms'] = 'backend/landingpage_promo/tesSendSms';
$route['cashback/landing_page'] = 'backend/landingpage_promo/landingpage';
$route['cashback/landing_page/list'] = 'backend/landingpage_promo/getListCashback';
$route['cashback/landing_page/batch/process'] = 'backend/landingpage_promo/previewBatchCashback';
$route['cashback/landing_page/batch/save'] = 'backend/landingpage_promo/saveBatchCashback';
$route['cashback/landing_page/batch/cancel'] = 'backend/landingpage_promo/cancelBatch';
$route['cashback/landing_page/batch/list'] = 'backend/landingpage_promo/listAllBatch';
$route['cashback/landing_page/batch/print'] = 'backend/landingpage_promo/printBatch';
$route['cashback/landing_page/batch/reject_prev'] = 'backend/landingpage_promo/prevRejectTransfer';
$route['cashback/landing_page/batch/reject'] = 'backend/landingpage_promo/rejectTransfer';
$route['cashback/landing_page/batch/id'] = 'backend/landingpage_promo/listTransferByBatchNo';
$route['cashback/landing_page/batch/ok'] = 'backend/landingpage_promo/TransferOk';
$route['cashback/landing_page/sendsms'] = 'backend/landingpage_promo/sendSms';
$route['cashback/landing_page/report'] = 'backend/landingpage_promo/cashbackReport';
$route['cashback/landing_page/report/list'] = 'backend/landingpage_promo/cashbackReportList';
$route['cashback/landing_page/pending/(:any)'] = 'backend/landingpage_promo/pendingTrf/$1';
$route['cashback/landing_page/trfprocess/(:any)'] = 'backend/landingpage_promo/listTrfByBatch/$1';
$route['cashback/update/adminbank'] = 'backend/landingpage_promo/updateAdminBank';
$route['cashback/landing_page/norekhp/excel'] = 'backend/landingpage_promo/norekhpExcel';
/*---------------------
UPDATE BV K-NET
---------------------*/
$route['trans/knet/bv'] = 'backend/transaction/formKnetBvUpdate';
$route['trans/knet/bv/info'] = 'backend/transaction/detailBVInfo';
$route['trans/knet/bv/update'] = 'backend/transaction/updateBvKnet';

/*----------------------
    PROMO NOVEMBER 2018
 -----------------------*/
$route['promo_november'] = 'webshop/Promo_nov/home';
$route['addtocart'] = 'webshop/Promo_nov/addToCart';
$route['select/product'] = 'webshop/Promo_nov/selectProduct';
$route['cart/checkout/sgo/pay/previewfendi'] = 'webshop/Promo_nov/checkout';

/*-------------------------
    tambahan Mas Dion
 ------------------------*/
$route['trx/reconcile/null_member'] = 'backend/member_reconcile/reconcileMemberForm';
$route['trx/reconcile/null_member/save'] = 'backend/member_reconcile/reconcileMemberSave';
$route['trx/reconcile/token/check'] = 'backend/member_reconcile/checkNoToken';
$route['trx/reconcile/helper/(:any)/(:any)/(:any)'] = 'backend/member_reconcile/getFullNameMemberStockist/$1/$2/$3';
$route['trx/reconcile/nonbangkok/token/check'] = 'backend/member_reconcile/checkNoTokenRegBiasa';
$route['trx/reconcile/token/check2'] = 'backend/member_reconcile/checkTokenMembReg';
$route['member/transaction_only/recon'] = 'backend/member_reconcile/reconTrxMemberOnly';
/**
 * @Author: Ricky
 * @Date: 2019-01-17 09:33:33
 * @Desc: membership belum reconcile
 */
$route['member/reconcile/bangkok'] = 'backend/member_reconcile/formReconBangkok';
$route['member/reconcile/bangkok/list'] = 'backend/member_reconcile/listReconBangkok';
$route['member/reconcile/bangkok/list/detail/(:any)'] = 'backend/member_reconcile/detailListReconBangkok/$1';
$route['member/reconcile/bangkok/update/(:any)'] = 'backend/member_reconcile/memberReconcileBangkok/$1';
$route['member/reconcile/bangkok/list/xls'] = 'backend/member_reconcile/printResultToExcel';

/**
 * @Author: Ricky
 * @Date: 2019-02-12 10:43:16
 * @Desc: K-Wallet Reconciling Transaction
 */
$route['k-wallet/recon/trx'] = 'backend/k_wallet_recon/kWalletForm';
$route['k-wallet/recon/trx/list'] = 'backend/k_wallet_recon/reportList';
$route['k-wallet/recon/trx/only'] = 'backend/k_wallet_recon/reconTrxOnly';
$route['k-wallet/recon/token/update'] = 'backend/k_wallet_recon/updateToken';
$route['k-wallet/recon/insert/failtrx'] = 'backend/k_wallet_recon/insertFailTrx';
$route['k-wallet/recon/knet/balance/recover'] = 'backend/k_wallet_recon/recoverBalance';
$route['k-wallet/recon/xl/balance/recover'] = 'backend/k_wallet_recon/recoverBalanceXL';

/**
 * @Author: Ricky
 * @Date: 2019-02-20 17:30:01
 * @Desc: Import Top Up K-Wallet
 */
$route['k-wallet/topup/import'] = 'backend/k_wallet_recon/formImportTopUpKWallet';
$route['k-wallet/topup/import/preview'] = 'backend/k_wallet_recon/previewFileCSVTopUp';
$route['k-wallet/topup/import/save'] = 'backend/k_wallet_recon/importTopUpKwallet';

/*--------------------
 REPORT FINNANCE TRX
--------------------*/
$route['trx/finance'] = 'backend/report/FinanceTrx/home';
$route['trx/finance/report/act'] = 'backend/report/FinanceTrx/financeReportAct';
$route['trans/updPrd/(:any)'] = 'backend/transaction/updateProdukSales/$1';
$route['trans/updPrd/upd/save'] = 'backend/transaction/updateProdukSalesSave';
$route['trx/finance/report/mandiri'] = 'backend/report/FinanceTrx/mandiriAPI';


/*-------------------------
LBC SALES CHECK
--------------------------*/
$route['lbc/sales/check'] = 'backend/promo/c_lbc/salesCheckForm';
$route['lbc/sales/check/list'] = 'backend/promo/c_lbc/getListLbcSales';

/*---------------------------------------------
    SEARCH REKENING BANK MEMBER N STOCKIST
---------------------------------------------*/
$route['search/rekening_member/form'] = 'backend/search/SearchRekMemNStockist/searchRekMember';
$route['search/rekening_member/act'] = 'backend/search/SearchRekMemNStockist/listRekMember';

$route['search/rekening_stockist/form'] = 'backend/search/SearchRekMemNStockist/searchRekStockist';
$route['search/rekening_stockist/act'] = 'backend/search/SearchRekMemNStockist/listRekStockist';

/*---------------------------------
    BE REPORT SALES BY STOCKIST
---------------------------------*/
$route['be/report/sales/bystockist/form'] = 'backend/report/Report_salesByStockist/searchReport';

/*----------------
    promo nov
---------------*/
$route['coba/belanja/checkout2'] = 'webshop/Promo_nov/checkout';
$route['cart/register/newmember'] = 'webshop/Promo_nov/regisnewmember';
//disable krn pindah prod
//$route['cart/checkout/tesfendi'] = 'webshop/Promo_nov/CheckoutCart';
//$route['cart/checkout/sgo/pay/previewfendi'] = 'webshop/Promo_nov/paySGOPreview2';

/*-----------------------------
    BE CEK SALES MEMBER
----------------------------*/
$route['be/ceksales/member'] = 'backend/report/Report_SalesMember/form';
$route['be/ceksales/member/act'] = 'backend/report/Report_SalesMember/act';

/*---------------------------------
    BE CEK SALES PROMO BANG-KOK
---------------------------------*/
$route['be/ceksales/bangkok'] = 'backend/report/Report_SalesMember/formBangkok';
$route['be/ceksales/bangkok/act'] = 'backend/report/Report_SalesMember/actBangkok';

$route['conote/update'] = 'backend/conot_resi';
$route['conote/update/list'] = 'backend/conot_resi/listResiFisikBelumDiupdate';
$route['conote/update/save'] = 'backend/conot_resi/updateResiFisik';

/**
 * @Author: Ningsih
 * @Date: 2019-01-29
 * @Desc: input resep product dept.
 */
$route['resep/view'] = 'backend/resep/view';
$route['resep/save'] = 'backend/resep/save';

/*
DION @ 12/02/2019
*/
$route['sales/pindahbv'] = 'webshop/transfer_bv/transferBvForm';
$route['sales/pindahbv/search'] = 'webshop/transfer_bv/listTransaction';
$route['sales/pindahbv/form'] = 'webshop/transfer_bv/formUpdateBv';
$route['sales/pindahbv/showfullnm'] = 'webshop/transfer_bv/showFullNameById';
$route['sales/pindahbv/update'] = 'webshop/transfer_bv/updateBv';
$route['sgo/payment/check/status'] = 'API_sgo/checkUpdateStatus';
$route['sgo/payment/update/form'] = 'API_sgo/formUpdateSukses';
$route['sgo/payment/update/form/preview'] = 'API_sgo/updateSukses';
$route['sgo/payment/update/form/save'] = 'API_sgo/saveToDB';
$route['sgo/payment/update/get/check/(:any)'] = 'API_sgo/getCheckUpdateStatus/$1';
$route['sgo/payment/update/get/statusfailed'] = 'API_sgo/failedUpdate';

/*
 * Fendi | print tax stokist | 05/02/2019
 */
$route['be/printtax/form'] = 'backend/report/Print_tax_stk/formPrintTax';
$route['be/printtax/act'] = 'backend/report/Print_tax_stk/printTax';

$route['resend/catalog/link'] = 'webshop/api_payment_reconcile/sendEmailCatalog';

//add dion 14/03/2019
/**
 * @Author: Dion, Ricky
 * @Date Created: 2019-03-14
 * @Date Updated: 2019-04-12
 * @Desc: Reconciling incomplete trx
 */
$route['be/trans/integrity'] = 'backend/trxintegrity/form';
$route['be/trans/integrity/check'] = 'backend/trxintegrity/getList';
$route['be/trans/integrity/reconcile/(:any)'] = 'backend/trxintegrity/getDetail/$1';
$route['be/trans/integrity/reconcileTrx/newtrh'] = 'backend/trxintegrity/reconcileNewtrh';
$route['be/trans/integrity/reconcileTrx/billivhdr'] = 'backend/trxintegrity/reconcileBillivhdr';
$route['be/trans/integrity/reconcileTrx/bbhdr'] = 'backend/trxintegrity/reconcileBbhdr';
$route['be/trans/integrity/reconcileTrx/newivtrh'] = 'backend/trxintegrity/reconcileNewivtrh';
$route['be/trans/integrity/reconcileTrx/bonusmonth'] = 'backend/trxintegrity/reconcileBonusMonth';
$route['be/trans/integrity/reconcileTrx/idmember'] = 'backend/trxintegrity/reconcileIdMember';
$route['be/trans/integrity/reconcileTrx/detailmember/(:any)'] = 'backend/trxintegrity/memberRecordDetail/$1';

/*
 * FENDI | PRDCD dari bundling yg kebanyakan @22-03-2019
 */
$route['be/getPrdcd/form'] = 'backend/search/SearchPrdcd/formSearch';
$route['be/getPrdcd/act'] = 'backend/search/SearchPrdcd/actSearch';

/*
 * VERA | PRDCD dari bundling yg IDBC018 @16-04-2019
 */
$route['be/getPrdcd/form2'] = 'backend/search/SearchPrdcd/formSearch2';
$route['be/getPrdcd/act2'] = 'backend/search/SearchPrdcd/actSearch2';

//add DION 26/03/2019
$route['check/promoApr'] = 'webshop/cartshop/checkPromoApril2019';
$route['cart/specOfferProd'] = 'webshop/cartshop/specOfferProd';


/**
 * @Author: Ricky
 * @Date: 2019-04-12 10:44:03
 * @Desc: Rekap produk diambil di stockist
 */
$route['rekap/produk/stockist'] = 'backend/report/Report_produk_stockist/reportForm';
$route['rekap/produk/stockist/check'] = 'backend/report/Report_produk_stockist/getList';
$route['hilal'] = 'backend/hilal/serviceMutasi';

/*
 * FENDI | Update currency voucher
 */
$route['be/update/voucher/form'] = 'backend/UpdateVoucher';
$route['be/update/voucher/getDetail'] = 'backend/UpdateVoucher/getDetailVoucher';
$route['be/update/voucher'] = 'backend/UpdateVoucher/actUpdate';

/*
 * TRACKING API
 */
//utk API
$route['api/tracking/(:any)'] = "backend/Api_tracking/getDataTracking/$1";
$route['api/tracking'] = "backend/Api_tracking/getAllDataTracking";

//utk frontend
$route['tracking/info'] = "backend/Api_tracking/getTrackingInfo";

/**
 * @Author: Ricky
 * @Date: 2019-05-03 11:31:04
 * @Desc: Reconciling PPOB Transactions
 */
$route['xl/recon/trx_only'] = 'backend/ppobxl/Recon_ppob/reconPpob';
$route['api/ship/kmart/update'] = 'mobile_api/updateShippingKmartOnline';

/*
 * VOUCHER FREE MEMBERSHIP | FENDI | 9-5-2019
 */
$route['free/membership'] = 'webshop/vch_memb';

$route['vch/free/membership/form'] = 'backend/report/AllVoucher';
$route['vch/free/membership/act'] = 'backend/report/AllVoucher/getDetail';

/*---------------------------------------------
    SEARCH BONUS MEMBER BULANAN
---------------------------------------------*/
$route['search/bonus_member/form'] = 'backend/search/SearchBonusMemb/searchBonusMember';
$route['search/bonus_member/act'] = 'backend/search/SearchBonusMemb/listBonusMember';
$route['search/getNameMemb'] = 'backend/search/SearchBonusMemb/getNameMemb';

/*---------------------------------------------
    TOLAKKAN BANK
---------------------------------------------*/
$route['bank/tolak/form'] = 'backend/bank/BankControl/FormBank';
$route['bank/tolak/act'] = 'backend/bank/BankControl/actTolakanBank';
$route['bank/tolak/list'] = 'backend/bank/BankControl/listTolakanBank';
//$route['search/getNameMemb'] = 'backend/search/SearchBonusMemb/getNameMemb';

/*-------------------------
TOM MANUAL
--------------------------*/
$route['tom/import/manual'] = 'backend/tom_import/formImportTom';
$route['tom/import/manual/read'] = 'backend/tom_import/readFile';
$route['tom/import/manual/save'] = 'backend/tom_import/saveImportTom';
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class MY_Hdr_buku_besar extends CI_Autowraptable 
{
	public $data_pdf = array();
	
	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'Letter' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(), $options = array()){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
		}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("", "B", 15);
		$this->MultiCell(0, 12, 'PT. MSA KARGO' );
		$this->Cell(0, 1, " ", "B");
		$this->Ln(10);
		$this->SetFont("", "B", 15);
		$this->SetX($left); $this->Cell(0, 10, 'LAPORAN BUKU BESAR' , 0, 1,'C' );
		$this->Ln(2);
		$this->SetFont("",'', 12);
		$this->SetX($left); $this->Cell(0, 18, $mst['sbu_name'] , 0, 1,'C' );
		$this->SetX($left); $this->Cell(0, 12, $mst['addres'] , 0, 1,'C' );
		$this->Ln(10);
		
		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{
		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 9);
		$left = $this->GetX();
		$this->Cell(20,$h,'Nomor Perkiraan :' ,0,0,'L' ,false);
		$this->SetX(110); $this->Cell(110, $h, $mst['no_perkiraan'] , 0, 0, 'L' ,false);
		
		
		$this->Ln(12);
		$this->Cell(20,$h,'Nama Perkiraan :' ,0,0,'L' ,false);
		$this->SetX(110); $this->Cell(110, $h, $mst['nm_perkiraan'] , 0, 0, 'L' ,false);
		$this->SetX(480); $this->Cell(480, $h, 'Bulan :' , 0, 0, 'L' ,false);
		$this->SetX(520); $this->Cell(520, $h, $mst['bulan'] , 0, 0, 'L' ,false);
			
		
		$this->Ln(15);
		
		$this->HeaderTable();
	}
	
	public function HeaderTable()
	{
		$h = 13;
		$left = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "B", 9);
		$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(20,$h,'NO' ,1,0,'L' ,true);
		$this->SetX($left += 20); $this->Cell(50, $h, 'Tanggal' , 1, 0, 'C' ,true);
		$this->SetX($left += 50); $this->Cell(60, $h, 'NOTA' , 1, 0, 'C' ,true);
		$this->SetX($left += 60); $this->Cell(200, $h, 'Keterangan' , 1, 0, 'C' ,true);
		$this->SetX($left += 200); $this->Cell(70, $h, 'Debit' , 1, 0, 'C' ,true);
		$this->SetX($left += 70); $this->Cell(70, $h, 'Kredit' , 1, 0, 'C' ,true);
		$this->SetX($left += 70); $this->Cell(70, $h, 'Saldo' , 1, 0, 'C' ,true);
	}
	
	public function rptDetailData () {
		//
		$mst = $this->mst;
		$balance = $mst['balance'];
		
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;
 
		//header
		
		$this->Ln();
		$this->SetFont('Arial' ,'' ,9);
		$left = $this->GetX();
		$this->Cell(470,13,'Saldo Awal' ,1,0,'C' ,FALSE);
		$this->SetX($left += 470);; $this->Cell(70, 13,number_format($balance,2,',','.'), 1, 0, 'L' ,FALSE);
		$this->Ln();
 		
		$this->SetFont('Arial' ,'' ,8.5);
		$this->SetWidths(array(20,50,60,200,70,70,70));
		$this->SetAligns(array('C','L','L','L','L','L','L'));
		$no = 1; $this->SetFillColor(255);
		foreach ($this->data as $rows) {
			$balance = ($balance+$rows['DEBET']) - $rows['CREDIT'];
			$this->Row(
				array($no++,
				$rows['CRT_DATE'],
				$rows['DOCUMENT_NO'],
				$rows['DOCUMENT_DESCRIPTION'],
				number_format($rows['DEBET'],2,',','.'),
				number_format($rows['CREDIT'],2,',','.'),
				number_format($balance,2,',','.'),
			  )
			);
		}

		$this->SetFont("", "B", 9);
		$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(20,$h,'NO' ,1,0,'L' ,true);
		$this->SetX($left += 20); $this->Cell(50, $h, 'Tanggal' , 1, 0, 'C' ,true);
		$this->SetX($left += 50); $this->Cell(60, $h, 'NOTA' , 1, 0, 'C' ,true);
		$this->SetX($left += 60); $this->Cell(200, $h, 'Keterangan' , 1, 0, 'C' ,true);
		$this->SetX($left += 200); $this->Cell(70, $h, 'Debit' , 1, 0, 'C' ,true);
		$this->SetX($left += 70); $this->Cell(70, $h, 'Kredit' , 1, 0, 'C' ,true);
		$this->SetX($left += 70); $this->Cell(70, $h, 'Saldo' , 1, 0, 'C' ,true);
 
	}

}

function print_pdf($coa,$date_awal,$date_akhir,$cabang)
{
	define('FPDF_FONTPATH',$this->config->item('fonts_path'));

	$date_awal = str_replace('-', '/',$date_awal);
	$date_akhir = str_replace('-', '/',$date_akhir);

	$tgl_balance = $this->m_all->getTglBalance($date_awal);
	$rate_idr = $this->m_all->getCurrencyRate('IDR');
	$rate_usd = $this->m_all->getCurrencyRate('USD');
	$result = $this->m_buku_besar->getDetail($date_awal,$date_akhir,$coa,$rate_idr,$rate_usd,$cabang);
	//$balance = $this->m_buku_besar->getSumSaldo($coa,$bulan,$tahun,$rate_idr,$rate_usd);
	if($tgl_balance != '')
		$balance = $this->m_buku_besar->getSumSaldo($tgl_balance,$coa,$rate_idr,$rate_usd,$cabang);
	else
		$balance = 0;
	$sbu = $this->m_all->getDtlSBU($cabang)->row();


	$data_mst = array(
		'bulan' => '',
		'sbu_name' => $sbu->SBU_NAME,
		'addres' => $sbu->ADDRESS,
		'no_perkiraan' =>$coa,
		'nm_perkiraan' => $this->m_all->getNmAccount($coa,$cabang),
		'balance' => $balance
	);

	foreach($result as $rows)
	{
		$data_pdf[] = array(
			'CRT_DATE' => date('d/m/Y',strtotime($rows->CRT_DATE)),
			'DOCUMENT_NO' => $rows->DOCUMENT_NO,
			'DOCUMENT_DESCRIPTION' => $rows->DOCUMENT_DESCRIPTION,
			'DEBET' => $rows->DEBET,
			'CREDIT'=> $rows->CREDIT
		);
	}

	$options = array(
		'filename'  => 'General_Ledger-'.date('dmy').'.pdf', //nama file penyimpanan, kosongkan jika output ke browser
		'destinationfile'  => 'D' , //I=inline browser (default), F=local file, D=download
		'paper_size' =>'Letter' ,	//paper size: F4, A3, A4, A5, Letter, Legal
		'orientation' =>'P'  //orientation: P=portrait, L=landscape
	);

	$tabel = new MY_Hdr_buku_besar($data_mst,$data_pdf,$options);
	$tabel->printPDF();

}
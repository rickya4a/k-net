<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Controller extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *        http://example.com/index.php/welcome
	 *    - or -
	 *        http://example.com/index.php/welcome/index
	 *    - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
	{
		parent::__construct();

		$this->username = $this->session->userdata('ecom_user');
		$this->usergroup = $this->session->userdata('ecom_groupname');
		$this->groupid = $this->session->userdata('ecom_groupid');
		$this->usermenu = $this->session->userdata('ecom_menu');
		$this->store_info = $this->session->userdata('store_info');
		$this->sk_path_file = $_SERVER['DOCUMENT_ROOT'] . '/asset/file/ocd_3.0.pdf';
		$this->stkFolder = 'backend/stockist/stkpayment/';

		$this->sgo_usrname = "KLINK";
		$this->sgo_signature_key = "EFPnjZzcGSKgZPCZ";
		$this->sgo_sendinvoice_usrname = "TOPKLINK";
		$this->sgo_sendinvoice_signature_key = "KLInk70puP";
		$this->sgo_password = "vPqVRtBNRR6rde5F";

		//$this->sgo_url_sendinvoice = "https://sandbox-api.espay.id/rest/merchant/sendinvoice";
		$this->sgo_url_sendinvoice = "https://api.espay.id/rest/merchant/sendinvoice";

		//$this->sgo_url_listprd_dev = "https://sandbox-api.espay.id/rest/billertools/getregisteredproducts";
		$this->sgo_url_listprd_dev = "https://api.espay.id/rest/billertools/getregisteredproducts";

		//$this->sgo_url_inquiry_dev = "https://sandbox-api.espay.id/rest/biller/inquirytransaction";
		$this->sgo_url_inquiry_dev = "https://api.espay.id/rest/biller/inquirytransaction";


		//$this->sgo_url_payment_dev = "https://sandbox-api.espay.id/rest/biller/paymentreport";
		$this->sgo_url_payment_dev = "https://api.espay.id/rest/biller/paymentreport";

		//$this->sgo_url_inqpay_status = "https://sandbox-api.espay.id/rest/billertools/gettransactioninfo";
		$this->sgo_url_inqpay_status = "https://api.espay.id/rest/billertools/gettransactioninfo";

		//XL
		$this->adv_id = "2370";
		$this->pass_xl = "klinkTest123";
		$this->key_xl = "ThisTestKey";

		//port dev : 443
		//port prod : 8243
		//ori DION
		$this->xl_get_encrypt = "https://api.xl.co.id:8243/digitalreward/v1/encrypt";
		$this->xl_get_stock_api = "https://api.xl.co.id:8243/digitalreward/v1/stock/?key=";
		$this->xl_redeem_stock_api = "https://api.xl.co.id:8243/digitalreward/v1/redeem/?key=";

//		 $this->xl_get_encrypt = "https://112.215.105.53:8243/digitalreward/v1/encrypt";
//		 $this->xl_get_stock_api = "https://112.215.105.53:8243/digitalreward/v1/stock/?key=";
//		 $this->xl_redeem_stock_api = "https://112.215.105.53:8243/digitalreward/v1/redeem/?key=";
		$this->headerXL = array(
			"Cache-Control: no-cache",
			"Content-Type: application/json",
			"channel: MyXL",
			"request-id: abc123",
		);

		//SICEPAT
		$this->sicepatkey = "525ce3d154e4f068a24fe213c671ab17";
	}

	public function getStoreInfo()
	{
		if (isset($this->store_info)) {
			return $this->store_info;
		} else {
			return null;
		}
	}

	protected function _checkSessionStoreUser($redirectTo = 'loginmember')
	{
		$webReturn = TRUE;
		if ($this->store_info == null) {
			$webReturn = FALSE;
			redirect($redirectTo);
		}
		return $webReturn;
	}

	public function checkSessionBeUser($redirectTo = 'loginmember')
	{
		$webReturn = TRUE;
		if ($this->store_info == null) {
			$webReturn = FALSE;
			throw new Exception("Session expired, please login..", 1);
		}
		return $webReturn;
	}

	public function checkSessionBE($redirectTo = 'loginmember')
	{
		$webReturn = TRUE;
		if ($this->username == null) {
			$webReturn = FALSE;
			throw new Exception("Session expired, please login..", 1);
		}
		return $webReturn;
	}

	public function ifDion($redirectTo = 'loginmember')
	{
		$webReturn = TRUE;
		if ($this->username == "DION") {
			$webReturn = FALSE;
			throw new Exception("Only Dion Allowed..!!", 1);
		}
		return $webReturn;
	}

	public function createThumbnails($vfile_upload, $thumb_dir, $img_name)
	{
		//identitas file asli
		$im_src = imagecreatefromjpeg($vfile_upload);
		$src_width = imageSX($im_src);
		$src_height = imageSY($im_src);

		//Simpan dalam versi small 320 pixel
		//set ukuran gambar perbandingan perubahan
		$dst_width = 200;
		$dst_height = 200;


		//resize height
		$ratio = $dst_height / $src_height;
		$width = $src_width * $ratio;
		//$this->resize($width,$height);

		//resize width
		$ratio = $dst_width / $src_width;
		$height = $src_height * $ratio;
		//$this->resize($width,$height);

		//scale
		$scale = 50;
		$width = $src_width * $scale / 100;
		$height = $src_height * $scale / 100;
		//$this->resize($width,$height);

		//proses perubahan ukuran
		$im = imagecreatetruecolor($width, $height);
		imagecopyresampled($im, $im_src, 0, 0, 0, 0, $width, $height, $src_width, $src_height);


		//Simpan gambar
		imagejpeg($im, $thumb_dir . $img_name);

		imagedestroy($im_src);
		imagedestroy($im);
	}

	public function setTemplateStkPayment($mainTemplate, $data = null)
	{
		$this->load->view($this->stkFolder . 'header_stk', $data);
		$this->load->view($this->stkFolder . $mainTemplate, $data);
		$this->load->view($this->stkFolder . 'footer_stk', $data);
	}

	public function setTemplate($mainTemplate, $data = null)
	{
		$this->load->view('includes/div_opening', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('includes/div_closing', $data);
	}
	
	function checkTglBatasMampirkak() {
		$tgl = date("Y-m-d h:i:s ");
		$tgl_pembanding = "2018-11-01 01:00:00";
		if($tgl_pembanding < $tgl) {
			return array("response" => "false", "min_dp" => 470000, "route" => "mampirlagikak");
		} else {
			return array("response" => "true", "min_dp" => 350000, "route" => "mampirkak");
		}
	}

	public function setTempWebShop($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/header', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopEspay($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerEsp', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopNonMember($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerSGONonMember', $data);
		//$this->load->view('shopinc/Nonmember_product', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}
	
	public function setTempWebShopNonMember2($mainTemplate, $data = null) {
		$this->load->view('shopinc/headerSGONonMember2', $data);
		//$this->load->view('shopinc/Nonmember_product', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopNonMemberDigital($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerSGONonMemberDigital', $data);
		//$this->load->view('shopinc/Nonmember_product', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShop22($mainTemplate, $data = null)
	{	
		$this->load->view('shopinc/header2', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebUmroh($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerUmroh', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footerUmroh', $data);
	}

	public function setTempWebShopLP($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerLP', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopLPSales($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerLPSalesV2', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopLPSalesNoBV($mainTemplate, $data = null)
	{
		$this->load->service("webshop/Cartshop_service", 'cartshopService');
		$data['listVoucher'] = $this->cartshopService->getListValidVoucherPemenang($data['no_voucher']);
		$this->load->view('shopinc/headerLPSalesNoBv', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopLPSalesNoBV2($mainTemplate, $data = null)
	{
		$this->load->service("webshop/Cartshop_service", 'cartshopService');
		//$data['listVoucher'] = $this->cartshopService->getListValidVoucherPemenang($data['no_voucher']);
		$this->load->view('shopinc/headerLPSalesNoBv2', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}


	public function setTempWebShopDevLP_SGO($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerLPSGO_Dev', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTempWebShopLP_SGO($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerLPSGO', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTemplateDevSGO($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerSGOdev', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTemplateSGO($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerSGO', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function setTemplateSGOLPSales($mainTemplate, $data = null)
	{
		$this->load->view('shopinc/headerSGOLPSales', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function runValidation($param)
	{
		if ($this->form_validation->run($param) == FALSE) {
			$err = "Please fill required input";
			if ($param == 'admLogin') {
				redirect('admin/login/err');
			}
			throw new Exception($err, 1);
		}
	}

	public function checkSessionUser()
	{

	}

	function jsonTrueResponse($data)
	{
		$arr = array("response" => "true", "arrayData" => $data);
		return $arr;
	}

	function jsonFalseResponse($message)
	{
		$arr = array("response" => "false", "message" => $message);
		return $arr;
	}


	/*SGO PPOB*/
	public function sgo_curl($array)
	{

		$curl = curl_init();

		$char = $this->sgo_usrname . ":" . $this->sgo_password;
		$resAuth = base64_encode($char);

		$arx = array(
			CURLOPT_URL => $array['url'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $array['postdata'],
			CURLOPT_HTTPHEADER => array(
				// "Cache-Control: no-cache",
				"Content-Type: application/x-www-form-urlencoded",
				"Authorization: Basic $resAuth"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
		} else {
			return $response;
			//print_r($response);
			//print_r($response);
		}
	}

	public function setFieldPost($fields)
	{
		//$data_string = json_encode($fields);
		//url-ify the data for the POST
		$fields_string = '';
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		//rtrim($fields_string, '&');
		$fields_string = rtrim($fields_string, '&');
		return $fields_string;
	}

	public function setAuthSgo()
	{
		$char = $this->sgo_usrname . ":" . $this->sgo_password;
		$res = base64_encode($char);
	}
	
	public function getStringKeyXL() {
		$inpString = $this->adv_id . "|" . $this->pass_xl . "|" . date("dmYH");
		return $inpString;
	}
	
	public function getXLEncryptKeyV2($trxid) {
		$inpString = $this->adv_id . "|" . $this->pass_xl . "|" . date("YmdH");
		$arr = array(
			'inputString' => $inpString,
			'encryptKey' => $this->key_xl
		);
		$urlx = $this->xl_get_encrypt;
		//echo $urlx;
		//$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
		$curl = curl_init();
		//echo "<pre>";
		//print_r($arr);
		//echo "</pre>";
		curl_setopt_array($curl, array(
			CURLOPT_PORT => "8243",
			CURLOPT_URL => $urlx,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($arr),
			CURLOPT_HTTPHEADER => $this->headerXL,
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		/*echo "<pre>";
		print_r($arr);
		print_r($info);
		echo "</pre>";
		*/
		if ($err) {
            //echo "Err when request encryptkey ";
			//echo "<br />";
			//print_r($err);
			
			echo "Err when request encryptkey ";
			echo "<br />";
			echo "<pre>";
			echo "param : ";
			echo "<br />";
			print_r($arr);
			echo "<br />";
			echo "error : ";
			echo "<br />";
			print_r($err);
			echo "<br />";
			echo "curl info : ";
			echo "<br />";
			print_r($info);
			echo "<br />";
			echo "response : ";
			echo "<br />";
			print_r($response);
			echo "</pre>";
			
			return 0;
		} else {
			//echo "dekripnya = ".$inpString."\n";
			//echo "enkrip key = ".$this->key_xl."\n";
			//print_r($err);
			//print_r($response);
			//curl -v -k https://112.215.105.53:8243/digitalreward/v1/encrypt -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Postman-Token: dc9a1b01-a655-49ed-b546-06e9fd1bb075' -H 'Channel: MyXL' -H 'Request-Id: 90812307518' -d '{"inputString": "2370|klinkTest123|2018111217","encryptKey": "ThisTestKey"}'
			return $response;
		}
	}

	public function getXLEncryptKey()
	{
		$inpString = $this->adv_id . "|" . $this->pass_xl . "|" . date("YmdH");
		$arr = array(
			'inputString' => $inpString,
			'encryptKey' => $this->key_xl
		);
		$urlx = $this->xl_get_encrypt;
		//echo $urlx;
		//$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
		$curl = curl_init();
		//echo "<pre>";
		//print_r($arr);
		//echo "</pre>";
		curl_setopt_array($curl, array(
			CURLOPT_PORT => "8243",
			CURLOPT_URL => $urlx,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($arr),
			CURLOPT_HTTPHEADER => $this->headerXL,
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		/*echo "<pre>";
		print_r($arr);
		print_r($info);
		echo "</pre>";
		*/
		if ($err) {
            //echo "Err when request encryptkey ";
			//echo "<br />";
			//print_r($err);
			
			echo "Err when request encryptkey ";
			echo "<br />";
			echo "<pre>";
			echo "param : ";
			echo "<br />";
			print_r($arr);
			echo "<br />";
			echo "error : ";
			echo "<br />";
			print_r($err);
			echo "<br />";
			echo "curl info : ";
			echo "<br />";
			print_r($info);
			echo "<br />";
			echo "response : ";
			echo "<br />";
			print_r($response);
			echo "</pre>";
			
			return 0;
		} else {
			//echo "dekripnya = ".$inpString."\n";
			//echo "enkrip key = ".$this->key_xl."\n";
			//print_r($err);
			//print_r($response);
			//curl -v -k https://112.215.105.53:8243/digitalreward/v1/encrypt -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Postman-Token: dc9a1b01-a655-49ed-b546-06e9fd1bb075' -H 'Channel: MyXL' -H 'Request-Id: 90812307518' -d '{"inputString": "2370|klinkTest123|2018111217","encryptKey": "ThisTestKey"}'
			return $response;
		}
	}
	
	public function getXLStock2() {
	    $final_key = "Vv7ISPxjugXTJ61uwnAJ8TWUdkpliWOe4qyp95JNMhY=";
		$urlx = $this->xl_get_stock_api . $final_key;
				//$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
				//echo $urlx;
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_PORT => "8243",
					CURLOPT_URL => $urlx,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => $this->headerXL,
				));

				//print_r($this->headerXL);
				//echo $final_key;

				$response = curl_exec($curl);
				$info = curl_getinfo($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
					echo "<pre>";
				//print_r($response);
				print_r($info);
				print_r($err);
				echo "<br />";
				print_r($this->headerXL);
				echo "</pre>";
					return 0;
				}
				/*
				echo "<pre>";
				//print_r($response);
				print_r($info);
				print_r($err);
				print_r($this->headerXL);
				echo "</pre>";*/
				return $response;
	}

	public function getXlStock()
	{
	    //key 
	    //Vv7ISPxjugXTJ61uwnAJ8TWUdkpliWOe4qyp95JNMhY=
		/*$keyEncrypt = $this->getXLEncryptKey();
		$hasil = json_decode($keyEncrypt);
		
		if($hasil != null) {
		if (array_key_exists("key", $hasil)) {
				$key = $hasil->key;
				$final_key = str_replace("+", "%2B", $key);
		*/
		$time = date("YmdH");
		$keyRes = $this->getEncryptFromDB($time);
		if($keyRes != null) {
				$final_key = str_replace("+", "%2B", $keyRes[0]->encrypt_key);
			

				$urlx = $this->xl_get_stock_api . $final_key;
				//$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
				//echo $urlx;
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_PORT => "8243",
					CURLOPT_URL => $urlx,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => $this->headerXL,
				));

				//print_r($this->headerXL);
				//echo $final_key;

				$response = curl_exec($curl);
				$info = curl_getinfo($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
					echo "<pre>";
				//print_r($response);
				print_r($info);
				print_r($err);
				echo "<br />";
				print_r($this->headerXL);
				echo "</pre>";
					return 0;
				}
				/*
				echo "<pre>";
				//print_r($response);
				print_r($info);
				print_r($err);
				print_r($this->headerXL);
				echo "</pre>";*/
				return $response;
			//}
		} else {
			echo "response from XL api encryptkey : ";
		}
	}
	
	function getEncryptFromDB($tgl) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('*');
		$dbqryx->from('ppob_xl_encrypt');
		$dbqryx->where('encrypt_time', $tgl);
		$query = $dbqryx->get();	
				
		if($query->num_rows() > 0) {
			return $query->result();
		} else {
			return 0;
		}
	}
	
	public function redeemStockXLnoEncrypt($arr) {
	        $time = date("YmdH");
			$keyRes = $this->getEncryptFromDB($time);
			if($keyRes != null) {
				$final_key = str_replace("+", "%2B", $keyRes[0]->encrypt_key);

				$urlx = $this->xl_redeem_stock_api . $final_key;
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_PORT => "8243",
					CURLOPT_URL => $urlx,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 60,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => json_encode($arr),
					CURLOPT_HTTPHEADER => $this->headerXL,
				));
				/*echo "<pre>";
				print_r($arr);
				echo "<br />";
				print_r($this->headerXL);
				echo "</pre>";*/
				$response = curl_exec($curl);
				$info = curl_getinfo($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
					echo "<pre>";
					print_r($arr);
					echo "<br />";
					print_r($info);
					print_r($this->headerXL);
					echo "</pre>";
					return 0;
				} else {
					return $response;

				}
			} else {
				return json_encode(array("response" => "false", "message" => "Encrypt key doesn't exist.."));
			}	
	}
	
	public function redeemStockXL2($arr) {
		$keyEncrypt = $this->getXLEncryptKeyV2($arr['trx_id']);
		$hasil = json_decode($keyEncrypt);
		$resKeyArray = json_decode($keyEncrypt);
		$response = 0;
		//HASIL ENKRIPSI G DAPAT KL JAM 24.00-07.00
		//ERROR DI LINE ARRAY_KEY_EXISTS
		if (is_object($resKeyArray) && array_key_exists("key", $hasil)) {
			$key = $hasil->key;
			$final_key = str_replace("+", "%2B", $key);

			$urlx = $this->xl_redeem_stock_api . $final_key;
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_PORT => "8243",
				CURLOPT_URL => $urlx,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($arr),
				CURLOPT_HTTPHEADER => $this->headerXL,
			));
			/*echo "<pre>";
			print_r($arr);
			echo "<br />";
			print_r($this->headerXL);
			echo "</pre>";*/
			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			    echo "<pre>";
				print_r($arr);
				echo "<br />";
				print_r($info);
				print_r($this->headerXL);
				echo "</pre>";
				return 0;
			}
		}
		return $response;
	}

	public function redeemStockXL($arr)
	{
		$keyEncrypt = $this->getXLEncryptKey();
		$hasil = json_decode($keyEncrypt);
		$resKeyArray = json_decode($keyEncrypt);
		$response = 0;
		//HASIL ENKRIPSI G DAPAT KL JAM 24.00-07.00
		//ERROR DI LINE ARRAY_KEY_EXISTS
		if (is_object($resKeyArray) && array_key_exists("key", $hasil)) {
			$key = $hasil->key;
			$final_key = str_replace("+", "%2B", $key);

			$urlx = $this->xl_redeem_stock_api . $final_key;
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_PORT => "8243",
				CURLOPT_URL => $urlx,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($arr),
				CURLOPT_HTTPHEADER => $this->headerXL,
			));
			/*echo "<pre>";
			print_r($arr);
			echo "<br />";
			print_r($this->headerXL);
			echo "</pre>";*/
			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			    echo "<pre>";
				print_r($arr);
				echo "<br />";
				print_r($info);
				print_r($this->headerXL);
				echo "</pre>";
				return 0;
			}
		}
		return $response;
	}

	public function sgoPpobInquiryPay($data)
	{

		//GENERATE SIGNATURE
		$rr_uuid = $this->sgo_usrname . "-PAY-" . date("ymd-his");
		$arr[0] = $this->sgo_usrname;

		$fields = array(
			'rq_uuid' => urlencode($rr_uuid),
			'rq_datetime' => urlencode(date("Y-m-d h:i:s")),
			'sender_id' => urlencode($this->sgo_usrname),
			'password' => urlencode($this->sgo_password)
		);

		if (array_key_exists("amountx", $data)) {
			//INQUIRY PAY
			$arr[1] = $data['order_id'];
			$arr[2] = $data['product_code'];
			$arr[3] = $data['amountx'];
			$arr[4] = $rr_uuid;
			$arr[5] = $this->sgo_signature_key;

			$sign = ppob_generateSignature($arr);

			$fields['order_id'] = urlencode($data['order_id']);
			$fields['product_code'] = urlencode($data['product_code']);
			$fields['amount'] = urlencode($data['amountx']);
			$fields['signature'] = urlencode($sign);
			$fields['data'] = urlencode($data['data']);

		} else if (array_key_exists("ref_id", $data)) {
			//INQUIRY PAY STATUS CHECK
			$arr[1] = $data['ref_id'];
			$arr[2] = $rr_uuid;
			$arr[3] = $this->sgo_signature_key;

			$sign = ppob_generateSignature($arr);
			//$sign2 = ppob_generateSignatureBeforeHash($arr);

			$fields['ref_id'] = urlencode($data['ref_id']);
			$fields['signature'] = urlencode($sign);
			//print_r($fields);
			//echo $sign2;
		} else {
			$arr[1] = $data['order_id'];
			$arr[2] = $data['product_code'];
			$arr[3] = $rr_uuid;
			$arr[4] = $this->sgo_signature_key;

			$sign = ppob_generateSignature($arr);

			$fields['order_id'] = urlencode($data['order_id']);
			$fields['product_code'] = urlencode($data['product_code']);
			$fields['signature'] = urlencode($sign);
		}
		//print_r($fields);
		/*if(array_key_exists("data", $data)) {
			$fields['data'] = urlencode($data['data']);
		}*/
		/*if(array_key_exists("amountx", $data)) {
			$fields['amount'] = urlencode($data['amountx']);
		}*/

		$postData = setFieldPost($fields);

		//SET URL CURL AND DATA
		$arr = array(
			"url" => $data['url'],
			"postdata" => $postData
		);

		$hasil = $this->sgo_curl($arr);

		return $hasil;

	}

	public function setTempWebClaimVoucher($mainTemplate, $data = null)
	{

		//$this->load->model('voucher/Claim_voucher_model', 'claim_voucher_model');
		//$data['nominal'] = $this->claim_voucher_model->getInfo();
		//$data['detail'] = $this->claim_voucher_model->getDetailVch($data['id_vch']);

		//print_r($data['detail']);
//		foreach($data['detail'] as $a){
//
//			$data['nominal'] = $a->nominal;
//		}
		$data['nominal'] = 25000;

		$this->load->view('shopinc/headerWebClaimVoucher', $data);
		$this->load->view($mainTemplate, $data);
		$this->load->view('shopinc/footer', $data);
	}

	public function tarifSicepat($origin, $destination, $weight)
	{

		$urlx = "http://api.sicepat.com/customer/tariff?api-key=" . $this->sicepatkey . "&origin=" . $origin . "&destination=" . $destination . "&weight=" . $weight;

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $urlx,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		//echo $response;
		//print_r($info);

//            $json = file_get_contents($urlx);
//            $data = json_decode($json, TRUE);
//            echo '<pre>';
//            print_r($data['sicepat']['results']);
//            echo '</pre>';

		if ($err) {

			return 0;

		} else {

			return $response;
		}
	}

	public function trackingSicepat($noresi)
	{

		$urlx = "http://api.sicepat.com/customer/waybill?api-key=" . $this->sicepatkey . "&waybill=" . $noresi;

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $urlx,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		if ($err) {

			return 0;

		} else {

			return $response;
		}

	}

	//START GOSEND API

	function price_gsend($x,$y){
		$curl = curl_init();

		$fields = array(
			'origin' => $x,
			'destination' => $y,
			'paymentType' => '3'
		);
		$postData = setFieldPost($fields); //create parameter

		$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v3/calculate/price?",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/calculate/price",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function booking_gsend($metode,$des_name,$des_phone,$des_add,$des_coord,$item,$price){
		$curl = curl_init();

		/*$metode="SameDay";
		$des_name="Tes Vera";
		$des_phone="0857";
		$des_add="Petamburan II";
		$des_coord="-6.2400292,106.8343388";
		$item="Product K-Link";
		$price="50000";*/

		$client_id = "k-link-engine";
		$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";
		$body='{
		   "paymentType": 3,
		   "deviceToken": "",
		   "collection_location":"pickup",
		  "shipment_method":"'.$metode.'",
		   "routes":
		   [{
			   "originName": "",
			   "originNote": "",
			   "originContactName": "PT. K-Link Indonesia",
			   "originContactPhone": "081388319355",
			   "originLatLong": "-6.2400292,106.8343388",
			   "originAddress": "Jalan Jend. Gatot Subroto Kav. 59A Jakarta Selatan",
			   "destinationName": "",
			   "destinationNote": "",
			   "destinationContactName": "'.$des_name.'",
			   "destinationContactPhone": "'.$des_phone.'",
			   "destinationLatLong": "'.$des_coord.'",
			   "destinationAddress": "'.$des_add.'",
			   "item": "'.$item.'",
			   "storeOrderId":"",
			   "insuranceDetails":
				{
				  "applied": "false",
				  "fee": "2500",
				  "product_description": "'.$item.'",
				  "product_price": "'.$price.'"
				}
		  }]}';
		/*		  $body='
        {
           "paymentType": 3,
           "deviceToken": "",
           "collection_location":"pickup",
          "shipment_method":"SameDay",
           "routes":
           [{
               "originName": "",
               "originNote": "",
               "originContactName": "The Kingdom Shop",
               "originContactPhone": "6285201311802",
               "originLatLong": "-6.1263348,106.7890888",
               "originAddress": "Jalan Pancoran Buntu I, Pancoran, 12780, Note origin example blablablaxhsdhs",
               "destinationName": "",
               "destinationNote": "",
               "destinationContactName": "Toko Jaya Agung",
               "destinationContactPhone": "6281254564161",
               "destinationLatLong": "-6.284508001748839,106.8295789",
               "destinationAddress": "Jalan Jatianom, Pasar Minggu, 12540, Note destination example",
               "item": "Shoes with size 27",
               "storeOrderId":"",
               "insuranceDetails":
                {
                  "applied": "false",
                "fee": "2500",
                  "product_description": "LED TV",
                  "product_price": "3500000"
                }
          }]}
        ';*/
		//echo $body;
		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/booking/v3/makeBooking",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $body,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Content-Type: application/json",
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//echo($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function status_gsend($orderno){
		$curl = curl_init();
		$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v2/booking/orderno/$orderno",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking/orderno/$orderno",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "",
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function cek_distance($latitude1,$longitude1){

		if($latitude1 !="" && $longitude1 !=""){
			$r= "true";
		}
		else{
			$r= "false";
		}

		$this->load->model("webshop/Member_model", "member_model");
		//$sql = $this->member_model->getConcat('vera_location', 'loccd');
		$sql = $this->member_model->getConcat2('V_ECOMM_GOSEND_SENDER_DET', 'loccd');
		foreach($sql as $row) {
			//foreach ($sql->result() as $row) {
			//$R = trim($row->R);
			$whcd= trim($row->whcd);
			$loccd= trim($row->loccd);
			$scnm = trim($row->fullnm);
			$addr1= trim($row->addr1);
			$addr2=trim($row->addr2);
			$addr3=trim($row->addr3);
			$city=trim($row->city);
			$tel_of=trim($row->tel_of);
			$alamat = $addr1." ".$addr2." ".$addr3." ".$city." ".$tel_of;
			$latitude2 = trim($row->latitude);
			$longitude2 = trim($row->longitude);
			//$latitude2 = trim($row->X);
			//$longitude2 = trim($row->Y);
			$state = trim($row->WH_HEAD);
			$wh_area = trim($row->WH_AREA);

			$theta = $longitude1 - $longitude2;
			$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
			$distance = acos($distance);
			$distance = rad2deg($distance);
			$distance = $distance * 60 * 1.1515;
			$distance = ($distance * 1.609344);
			$distance = $distance + 8;
			$round= round($distance,2);
			$penghubung="+";

			// $jarak[]= round($distance,2);
			$n2[]=$round.$penghubung.$scnm.$penghubung.$loccd.$penghubung.$state.$penghubung.$alamat;
			$list[]=array($round=>$round.'+'.$scnm.'+'.$loccd.'+'.$wh_area.'+'.$alamat.'+'.$whcd);

		}
		//echo "Tes Vera: ".$count;


		$r1= "false";
		$arr = array("response" => $r, "response1" => $r1, "error"=> "Error");
		$namelist1="";

		foreach ($list as $key => $val) {
				foreach ($val as $k => $v) {
					if($k <= 50){

						$namelist1[$k]=$v;
						$r1="true";

						foreach ($namelist1 as $m => $n) {
							$namelist2[]=$n;

							$arr = array("response" => $r, "response1" => $r1, "error"=> "Error","message" => "Berhasil!","jarak" => array_unique($namelist2));
						}
					}else{
						unset($val[$k]); //menghapus data array > 40
					}
					/*$namelist1[$k]=$v;
					$r1="true";*/
				}
		}

		/*foreach ($namelist1 as $m => $n) {
			$namelist2[]=$n;
		}
		$arr = array("response" => $r, "response1" => $r1, "error"=> "Error","message" => "Berhasil!","jarak" => $namelist2);*/
		echo json_encode($arr);
		//print_r($arr);
	}

	function cek_distance1($lat1,$long1,$lat2,$long2){

		if($lat1 !="" && $long1 !="" && $lat2 !="" && $long2 !=""){
			$latitude1= $lat1;
			$longitude1=$long1;

			$latitude2= $lat2;
			$longitude2=$long2;

			$r= "true";
			$theta = $longitude1 - $longitude2;
			$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
			$distance = acos($distance);
			$distance = rad2deg($distance);
			$distance = $distance * 60 * 1.1515;
			$distance = ($distance * 1.609344);
			$distance = $distance + 8;
			$round= round($distance,1);
			$round= $round * 1;
			$arr = array("response" => $r,"message" => "Berhasil!","jarak" => $round);

		}
		else{
			$r= "false";
			$arr = array("response" => $r, "error"=> "Error");
		}

		echo json_encode($arr);
		//print_r($arr);
	}

	function cek_distance2($clientid,$passkey,$latitude1,$longitude1){

		$y= $latitude1.",".$longitude1;

		if($latitude1 !="" && $longitude1 !=""){
			$r= "true";
		}
		else{
			$r= "false";
		}

		$this->load->model("webshop/Member_model", "member_model");
		//$sql = $this->member_model->getConcat('vera_location', 'loccd');
		$sql = $this->member_model->getConcat3('V_ECOMM_GOSEND_SENDER_DET', 'loccd');
		foreach($sql as $row) {
			//foreach ($sql->result() as $row) {
			//$R = trim($row->R);

			$whcd= trim($row->whcd);
			$loccd= trim($row->loccd);
			$scnm = trim($row->fullnm);
			$addr1= trim($row->addr1);
			$addr2=trim($row->addr2);
			$addr3=trim($row->addr3);
			$city=trim($row->city);
			$tel_of=trim($row->tel_of);
			$alamat = $addr1." ".$addr2." ".$addr3." ".$city." ".$tel_of;
			$latitude2 = trim($row->latitude);
			$longitude2 = trim($row->longitude);
			//$latitude2 = trim($row->X);
			//$longitude2 = trim($row->Y);
			$state = trim($row->WH_HEAD);
			$wh_area = trim($row->WH_AREA);

			$x= $latitude2.",".$longitude2;


			$round=100;
			$penghubung="";

			$curl = curl_init();

			$fields = array(
				'origin' =>$x,
				'destination' => $y,
				'paymentType' => '3'
			);
			$postData = setFieldPost($fields); //create parameter

			$client_id = $clientid;
			$pass_key = $passkey; //Development

			$arx = array(
				//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gokilat/v10/calculate/price",
				CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/calculate/price",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_POSTFIELDS => $postData,
				CURLOPT_HTTPHEADER => array(
					// masukkan data header disini
					"Client-ID: $client_id",
					"Pass-Key: $pass_key"));
			curl_setopt_array($curl, $arx);
			//ganti start dan end date sesuai data post yg diinginkan

			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				return $err;
			} else {
				$json = json_decode($response, true);
				//print_r($json);
				foreach($json as $i){
					if($i['shipment_method']=='Instant' && $i['serviceable'] == 1){
						//print_r($i['distance']);
						$round=$i['distance'];
						//print_r($list);
					}else{
						$round = 100;
					}

					$penghubung="+";
					$n2[]=$round.$penghubung.$scnm.$penghubung.$loccd.$penghubung.$state.$penghubung.$alamat;//ini ga ke pake
					$list[]=array($round=>$round.'+'.$scnm.'+'.$loccd.'+'.$wh_area.'+'.$alamat.'+'.$whcd);
				}
			}

			/*$theta = $longitude1 - $longitude2;
			$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
			$distance = acos($distance);
			$distance = rad2deg($distance);
			$distance = $distance * 60 * 1.1515;
			$distance = ($distance * 1.609344);
			$distance = $distance + 8;
			$round= round($distance,2);
			$penghubung="+";

			// $jarak[]= round($distance,2);
			$n2[]=$round.$penghubung.$scnm.$penghubung.$loccd.$penghubung.$state.$penghubung.$alamat;
			$list[]=array($round=>$round.'+'.$scnm.'+'.$loccd.'+'.$wh_area.'+'.$alamat.'+'.$whcd);*/

		}
		//echo "Tes Vera: ".$count;


		$r1= "false";
		$arr = array("response" => $r, "response1" => $r1, "error"=> "Error");
		$namelist1="";

		foreach ($list as $key => $val) {
			foreach ($val as $k => $v) {
				if($k <= 40){

					$namelist1[$k]=$v;
					$r1="true";

					foreach ($namelist1 as $m => $n) {
						$namelist2[]=$n;

						$arr = array("response" => $r, "response1" => $r1, "error"=> "Error","message" => "Berhasil!","jarak" => array_unique($namelist2));
					}
				}else{
					unset($val[$k]); //menghapus data array > 40
				}
				/*$namelist1[$k]=$v;
                $r1="true";*/
			}
		}

		/*foreach ($namelist1 as $m => $n) {
			$namelist2[]=$n;
		}
		$arr = array("response" => $r, "response1" => $r1, "error"=> "Error","message" => "Berhasil!","jarak" => $namelist2);*/
		echo json_encode($arr);
		//print_r($arr);
	}

	/* ----------------------
	 * development fendi @2019
	 -----------------------*/
	function setTempWebBackendM($mainTemplate, $data = null){

		$this->load->view('backend/mobile/tempWeb/v_header', $data);
		$this->load->view('backend/mobile/'.$mainTemplate, $data);
		$this->load->view('backend/mobile/tempWeb/v_navbar', $data);
		$this->load->view('backend/mobile/tempWeb/v_footer', $data);
	}
	/*-----------------------
	 * end development fendi
	 ----------------------*/

	function cek_distance3($clientid,$passkey,$latitude1,$longitude1){

		$y= $latitude1.",".$longitude1;

		if($latitude1 !="" && $longitude1 !=""){
			$r= "true";
		}
		else{
			$r= "false";
		}

		$this->load->model("webshop/Member_model", "member_model");
		//$sql = $this->member_model->getConcat('vera_location', 'loccd');
		$sql = $this->member_model->getConcat3('V_ECOMM_GOSEND_SENDER_DET', 'loccd');
		foreach($sql as $row) {
			//foreach ($sql->result() as $row) {
			//$R = trim($row->R);

			$whcd= trim($row->whcd);
			$loccd= trim($row->loccd);
			$scnm = trim($row->fullnm);
			$addr1= trim($row->addr1);
			$addr2=trim($row->addr2);
			$addr3=trim($row->addr3);
			$city=trim($row->city);
			$tel_of=trim($row->tel_of);
			$alamat = $addr1." ".$addr2." ".$addr3." ".$city." ".$tel_of;
			$latitude2 = trim($row->latitude);
			$longitude2 = trim($row->longitude);
			$state = trim($row->WH_HEAD);
			$wh_area = trim($row->WH_AREA);

			$x= $latitude2.",".$longitude2;


			$round=100;
			$penghubung="";

			$curl = curl_init();

			$fields = array(
				'origin' =>$x,
				'destination' => $y,
				'paymentType' => '3'
			);
			$postData = setFieldPost($fields); //create parameter

			$client_id = $clientid;
			$pass_key = $passkey; //Development

			$arx = array(
				//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gokilat/v10/calculate/price",
				CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/calculate/price",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_POSTFIELDS => $postData,
				CURLOPT_HTTPHEADER => array(
					// masukkan data header disini
					"Client-ID: $client_id",
					"Pass-Key: $pass_key"));
			curl_setopt_array($curl, $arx);
			//ganti start dan end date sesuai data post yg diinginkan

			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				return $err;
			} else {
				$json = json_decode($response, true);
				//print_r($json);
				foreach($json as $i){

					if($i['shipment_method']=='Instant' && $i['serviceable'] == 1){
						print_r($i['distance']);
						$round=$i['distance'];
						//print_r($list);
					}else{
						$round = 100;
					}

					$penghubung="+";
					$n2[]=$round.$penghubung.$scnm.$penghubung.$loccd.$penghubung.$state.$penghubung.$alamat;
					$list[]=array($round=>$round.'+'.$scnm.'+'.$loccd.'+'.$wh_area.'+'.$alamat.'+'.$whcd);
				}
			}


			/*$theta = $longitude1 - $longitude2;
			$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
			$distance = acos($distance);
			$distance = rad2deg($distance);
			$distance = $distance * 60 * 1.1515;
			$distance = ($distance * 1.609344);
			$distance = $distance + 8;
			$round= round($distance,2);
			$penghubung="+";

			// $jarak[]= round($distance,2);
			$n2[]=$round.$penghubung.$scnm.$penghubung.$loccd.$penghubung.$state.$penghubung.$alamat;
			$list[]=array($round=>$round.'+'.$scnm.'+'.$loccd.'+'.$wh_area.'+'.$alamat.'+'.$whcd);*/


		}
		//echo "Tes Vera: ".$count;


		$r1= "false";
		$arr = array("response" => $r, "response1" => $r1, "error"=> "Error");
		$namelist1="";

		foreach ($list as $key => $val) {
			foreach ($val as $k => $v) {
				if($k <= 40){

					$namelist1[$k]=$v;
					$r1="true";

					foreach ($namelist1 as $m => $n) {
						$namelist2[]=$n;

						$arr = array("response" => $r, "response1" => $r1, "error"=> "Error","message" => "Berhasil!","jarak" => array_unique($namelist2));
					}
				}else{
					unset($val[$k]); //menghapus data array > 40
				}
				/*$namelist1[$k]=$v;
                $r1="true";*/
			}
		}

		/*foreach ($namelist1 as $m => $n) {
			$namelist2[]=$n;
		}
		$arr = array("response" => $r, "response1" => $r1, "error"=> "Error","message" => "Berhasil!","jarak" => $namelist2);*/
		echo json_encode($arr);
		//print_r($arr);
	}

	function cek_wh($val1, $val2){

		if($val1!="" || $val2 !=""){
			$r= "true";
		}
		else{
			$r= "false";
		}

		$this->load->model("webshop/Member_model", "member_model");
		//$sql = $this->member_model->getConcat('vera_location', 'loccd');
		$sql = $this->member_model->getwh('V_ECOMM_GOSEND_SENDER_DET', 'loccd',$val1,$val2);
		foreach($sql as $row) {
			$whnm= trim($row->whnm);
			$fullnm= trim($row->fullnm);
			$latitude2 = trim($row->latitude);
			$longitude2 = trim($row->longitude);
			$state= trim($row->state);
		}

		$day = date('D');
		$kolom1= $day."_open";
		$kolom2= $day."_closed";

		$sql1 = $this->member_model->getschedule('master_schedule_gosend2', 'loccd',$val2,$day);
		foreach($sql1 as $row1) {
			$open= trim($row1->buka);
			$closed= trim($row1->tutup);
		}

		$open=date('H:i:s',strtotime($open));
		$closed=date('H:i:s',strtotime($closed));

		$time= date('H:i:s');

		$arr = array("response" => $r, "error"=> "Error","message" => "Berhasil!","nmwh" => $whnm,"nmstok" => $fullnm,"state" => $state,"lat" => $latitude2,"long" => $longitude2,"open" => $open,"closed" => $closed,"time" => $time);
		echo json_encode($arr);
		//print_r($arr);
	}

	function cek_prd_stok($val1, $val2, $val3){

		if($val1!="" && $val2 !="" && $val3 ){
			$r= "true";
		}
		else{
			$r= "false";
		}

		/*$nilai=0;
		$nm_prd="";
		$this->load->model("lokasi/M_location", "mlocation");
		$sql1 = $this->mlocation->getstok_avail('master_warehouse_stock_not_avail', $val1,$val2,$val3);
		if($sql1->num_rows()==1){
			foreach ($sql1 as $row1) {
				$nilai = trim($row1->jumlah);
				$nm_prd = trim($row1->prddesc);
			}
		}*/


		$ar_where=array('whcd' => $val1, 'loccd' => $val2, 'prdcd' => $val3);
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('count(prdcd) as jumlah, prddesc');
		$dbqryx->from('master_warehouse_stock_not_avail');
		$dbqryx->where($ar_where);
		$dbqryx->group_by("prddesc");
		$query = $dbqryx->get();

		if($query->num_rows() > 0) {
			//return $query->result();
			foreach ($query->result() as $row1) {
				$nilai = trim($row1->jumlah);
				$nm_prd = trim($row1->prddesc);
			}
		} else {
			//return 0;
			$nilai=0;
			$nm_prd="";
		}

		/*$this->db = $this->load->database('db_ecommerce', true);
		$qryShpHdr = "select count(prdcd) as jumlah, prddesc
						from master_warehouse_stock_not_avail where whcd='$val1' AND loccd='$val2'AND prdcd='$val3'
						group by prddesc'";
		$queryShpHc = $this->db->query($qryShpHdr);
		if($queryShpHc->num_rows() > 0) {
			$row1 = $queryShpHc->result();
			$nilai=$row1[0]->jumlah;
			$nm_prd=$row1[0]->prddesc;
		}*/

		$arr = array("response" => $r, "error"=> "Error","message" => "Berhasil!","nilai" => $nilai,"nmprd" => $nm_prd);
		echo json_encode($arr);
		//print_r($arr);
	}

	function cek_prd_stok2($val1, $val2, $val3){

		if($val1!="" && $val2!="" && $val3!=""){
			$r= "true";

			$nprd=explode(",",$val3);

			foreach ($nprd as $key => $value) {

				/*$this->load->model("lokasi/M_location", "mlocation");
				$sql1 = $this->mlocation->getstok_avail('master_warehouse_stock_not_avail', $val1,$val2,$value);
				foreach ($sql1 as $row1) {
					$nilai = trim($row1->jumlah);

					if($nilai == 1){
						$prdnm[] = trim($row1->prddesc);
					}
				}
			}*/

				$ar_where = array('whcd' => $val1, 'loccd' => $val2, 'prdcd' => $value);
				$dbqryx = $this->load->database("db_ecommerce", TRUE);
				$dbqryx->select('count(prdcd) as jumlah, prddesc');
				$dbqryx->from('master_warehouse_stock_not_avail');
				$dbqryx->where($ar_where);
				$dbqryx->group_by("prddesc");
				$query = $dbqryx->get();

				if ($query->num_rows() > 0) {
					//return $query->result();
					foreach ($query->result() as $row1) {
						$nilai = trim($row1->jumlah);
						//$nm_prd = trim($row1->prddesc);
						if ($nilai == 1) {
							$prdnm[] = trim($row1->prddesc);
						}
					}
					$n=1;
				} else {
					//return 0;
					$n = 0;
					$prdnm = "";
					//$r="false";
				}

				//$nm_prd = join(" & ", $prdnm);
				$arr = array("response" => $r, "error" => "Error", "message" => "Berhasil!", "nmprd" => array($prdnm),"nket" => $n);
			}

		}
		else{
			$r= "false";
			$nm_prd="";
			$arr = array("response" => $r, "error"=> "Error","message" => "Berhasil!","nmprd" => $nm_prd);
		}

		//$arr = array("response" => $r, "error"=> "Error","message" => "Berhasil!","nmwh" => $whnm,"nmstok" => $fullnm,"state" => $state,"lat" => $latitude2,"long" => $longitude2,"open" => $open,"closed" => $closed);
		echo json_encode($arr);

	}

	function cek_rek($val){
		$this->load->model("lokasi/M_location","M_location");
		$dts= $this->M_location->getRek($val);
		if($dts != null){
			foreach($dts as $row){
				$norekbank=$row->norek;
				$namapemilik=$row->namapemilik;
			}
		}

		$random=rand(0001,2000);
		$ref=rand(0000001,1000000);

		$curl = curl_init();

		/*$fields = array(
			  'nominal' => '100500',
			  'no_rek_tujuan' => '94409546454',
			  'nama_rek_tujuan' => 'Vera',
			  'type' => 'Cr'
		);
		$postData = setFieldPost($fields); //create parameter*/
		$body='{
				  "nominal": "100500",
				  "no_rek_tujuan": "'.$norekbank.'",
				  "nama_rek_tujuan": "'.$namapemilik.'",
				  "type": "Cr"
				}';

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gokilat/v10/calculate/price",
			CURLOPT_URL => "http://service-mutasi.k-net.co.id/api/api_payment",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $body,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Content-Type: application/json"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		//$json = json_decode($response, true);
		//print_r($json);

		/*if ($err) {
			return $err;
			print_r($err);
		} else {
			$json = json_decode($response, true);
			//print_r($json);
			foreach($json as $i){

				$random=$i['kode_unik'];
				$ref=$i['kd_ref'];

				$arr = array("koneksi" =>"true","kode_unik" => $random,"total_bayar" => "100000","kd_pay" => "000001","total_belanja" => "200000","kd_ref" =>$ref);
				//echo json_encode($arr);

				print_r($arr);
			}
		}*/

		$arr = array("koneksi" =>"true","kode_unik" => $random,"total_bayar" => "100000","kd_pay" => "000001","total_belanja" => "200000","kd_ref" =>$ref);
		echo json_encode($arr);

	}

	function cek_unik($nominal,$norek,$nmpemilik,$type,$payship){
		$this->load->model("lokasi/M_location","M_location");
		$kode_unik = rand(0001,2000);
		$total_produk = $nominal + $kode_unik;

		/*$kd_pay = rand(000001,100000);
		$kd_ref = rand(0000001,1000000);*/

		$dts= $this->M_location->kd_pay();
		if($dts != null){
			foreach($dts as $row){
				$kdpay =$row->kd_pay;
				$kd_pay = $kdpay + 1;
			}
		}

		$dts1= $this->M_location->kd_ref();
		if($dts1 != null){
			foreach($dts1 as $row1){
				$kdref =$row1->kd_pay;
				$kd_ref = $kdref + 1;
				$kd_ref=sprintf("%06s",$kd_ref);
			}


		}

		//$kd_ref= $this->M_location->kd_ref();
		$save_payment=$this->M_location->save_payment($kd_pay, $kd_ref, $nominal, $norek, $nmpemilik, $type, $total_produk, $kode_unik);

		if($save_payment){
			$response="true";
			$arr = array("koneksi" =>"true","kode_unik" => $kode_unik,"total_bayar" => $total_produk,"kd_pay" => $kd_pay,"total_belanja" => $nominal,"kd_ref" =>$kd_ref);
		}else{
			$response="false";
			$arr = array("koneksi" =>"false","kode_unik" => "","total_bayar" => "","kd_pay" => "","total_belanja" => "","kd_ref" =>"");
		}
		//save log
		$this->M_location->save_log1($nominal,$norek,$nmpemilik,$type,$response,$payship);
		$this->M_location->save_log2($kode_unik,$total_produk,$kd_pay,$nominal,$kd_ref,$response,$payship);
		echo json_encode($arr);

	}

	function get_api_distance_matrix($kel,$kodepos1,$city,$postcd,$loccd){
		//$CI = & get_instance();
		$curl = curl_init();
			//echo "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$postcd.",".$city."&destinations=".$kodepos1.",".$kel."&key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU";
			//echo "<br>";
			$arx = array(
				//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v3/calculate/price?",
				CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$postcd.",".$city."&destinations=".$kodepos1.",".$kel."&key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				//CURLOPT_POSTFIELDS => $postData,
				CURLOPT_HTTPHEADER => array(
					// masukkan data header disini
				));
			curl_setopt_array($curl, $arx);
			//ganti start dan end date sesuai data post yg diinginkan

			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);

			curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}

			/*if ($err) {
				return $err;
			} else {

				//$array_wh=array();


			}*/


		/*$arr = array("distance" => $dist_km,"loccd" => $loccd);
		echo json_encode($arr);*/
	}
}
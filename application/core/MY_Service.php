<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Service extends CI_Service {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 private $url = 'www.k-net.co.id/commerce_rpc/index.php/shopping/';
	 public function __construct() {
	    parent::__construct();
	    $this->xmlrpc->server(prep_url($this->url), 80);
		$this->datetime = date("Y-m-d H:i:s ");

        //$this->api_email = "http://192.168.0.168/ecm/go.php/api/email";
		//$this->jne_url_conot = "http://api.jne.co.id:8889/tracing/klink/generateCnoteTraining";
		//"http://api.jne.co.id:8889/tracing/klink/generateCnote";
		$this->jne_url_conot = conot_url("1"); 
		//url konot yg baru
		$this->jne_url_conot_baru = conot_url("1");
		//
		$this->jne_username = "KLINK";
		$this->jne_api_key = "76270305bef5d402220c96d59ac61977";
		$this->jne_OLSHOP_CUST = "80075400";

		//$this->generate_url_ship_priceBaru = "http://api.jne.co.id:8889/tracing/klink/price/";
		//$this->generate_url_ship_priceBaru = "http://apiv2.jne.co.id:10101/tracing/api/pricedev";
		$this->generate_url_ship_priceBaru = pricing_url("1");
		Veritrans_Config::$serverKey = 'VT-server-qoQ6mGJFLS-zkG16wTg5ufR0';
        Veritrans_Config::$isProduction = true;
	 }

     function jsonTrueResponse($data = null, $message = null) {
     	$arr = array("response" => "true", "arrayData" => $data, "message" => $message);
		return $arr;
     }

	 function jsonFalseResponse($message) {
	 	$arr = array("response" => "false", "message" => $message);
		return $arr;
	 }

	 //UPDATE SOME FIELDS IN TABLE ECOMM_TRANS_HDR_SGO (ONLY FOR SGO PAYMENT)
	 function updateBankCodePayment($temp_id, $arr) {
	 	$this->load->model('webshop/shared_module', 'shared');
    	$res = $this->shared->updateBankCodePayment($temp_id, $arr);
		$res2 = $this->shared->getDataPaymentSGOByOrderID($temp_id);
		return $res2;
	 }
	 
	 //UPDATE SOME FIELDS IN TABLE ECOMM_TRANS_HDR_SGO (ONLY FOR SGO PAYMENT)
	 function updateBankCodePayment2($temp_id, $arr) {
	 	$this->load->model('webshop/shared_module', 'shared');
    	$res = $this->shared->updateBankCodePayment2($temp_id, $arr);
		$res2 = $this->shared->getDataPaymentSGOByOrderID($temp_id);
		return $res2;
	 }
	 
	 //INSERT VOUCHER NO INTO ECOMM_TRANS_HDR_SGO
	 function insertVoucherList($arr) {
	 	$this->load->model('webshop/shared_module', 'shared');
    	$res = $this->shared->insertVoucherList($arr);
		//$res2 = $this->shared->getDataPaymentSGOByOrderID($temp_id);
		return $res;
	 }

	 function updatePaydetSGO($arr) {
	 	$this->load->model('webshop/shared_module', 'shared');
    	$res = $this->shared->updatePaydetSGO($arr);
		return $res;
	 }

	//tambahan transfer ATM manual Vera
	function updateHdrdetSGO($arr) {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->updateHdrdetSGO($arr);
		return $res;
	}

	function updNewTrh($arr) {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->updNewTrh($arr);
		return $res;
	}
	 
	 //DELETE FROM TABLE
	 function deleteFromTable($table, $param, $value) {
	 	$this->load->model('webshop/shared_module', 'shared');
    	$res = $this->shared->deleteFromTable($table, $param, $value);
		return $res;
		
	 }

	 //SHOW LIST CATEGORY PRODUCT
	 function getProductCategory() {
        $this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->getListCatProd();
		return $arr;
	 }
	 
	 function getProductByID($id) {
	 	$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->getProductByID($id);
		return $arr;
	 }
	 
	 function checkDoubleTokenTransHdr($token) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->checkDoubleTokenTransHdr($token);
		return $res;
	  }
 
	 //SHOW LIST CATEGORY PRODUCT
	 function getBank() {
        $this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getBank();
		return $res;

     }

	function getCCBank($total) {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getCCBank($total);
		return $res;
	}

	function getBank2() {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getBank2();
		return $res;

	}
	 
	 function getBankNonVABaru() {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getBankNonVABaru();
		return $res;
	 }
	 
	 function getListCargo() {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getListCargo();
		return $res;
	 }
	 
	 function getListCargoJhonDoe() {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getListCargoJhonDoe();
		return $res;
	 }
	 
	 function getListCargoJNE() {
	 	$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$res = $this->ecomm_trans_model->getListCargoJNE();
		return $res;
	 }

	function getCOD() {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getCOD();
		return $res;
	}

	 function getBankJohnDoe() {
	 	$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getBankJohnDoe();
		return $res;
	 }

	function getBankJohnDoe2() {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getBankJohnDoe2();
		return $res;
	}

	function getCCJohnDoe($total) {
		$this->load->model('webshop/shared_module', 'shared');
		$res = $this->shared->getCCJohnDoe($total);
		return $res;
	}

	//SHOW LIST FOR KODEPOS
	function showListProvinsiKP() {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->getListProvinsiKP();
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	}

	function showListKotaKP($prov) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKotaKP($prov);
		//print_r($valReturn);
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	}

	function showListKecamatanKP($kota) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKecamatanKP($kota);
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	}

	function showListKelurahanKP($kec) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKelurahanKP($kec);
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	}

	function cek_kodepos($prov,$kota,$kec,$kel) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showKodePos($prov,$kota,$kec,$kel);
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	}

	 //SHOW LIST ALL PROVINCE FOR JNE NEED
	 function showListProvinsi() {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->getListProvinsi2();
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	 }
	 
	 //SHOW LIST ALL PROVINCE FOR KGB NEED
	 function showListProvinsiKgb() {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->listProvinceKGB();
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	 }

	  //SHOW LIST ALL CITY/KABUPATEN FOR JNE NEED
	 function showListKota($prov) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKota2($prov);
		//print_r($valReturn);	
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	 }
	 
	 //SHOW LIST ALL CITY/KABUPATEN FOR Kgb NEED
	 function showListKotaKgb($prov) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->listCargoKota($prov);
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	 }

	 //SHOW LIST KECAMATAN
	 function showListKecamatan($kota) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKecamatan2($kota);
		$returnArr = array("response" => "true", "arrayData" => $valReturn);
		return $returnArr;
	 }

	 function showListKelurahan($kec) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKelurahan2($kec);
		return $valReturn;
	 }

	function showListKecamatanCOD($kec) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showListKecamatanCOD($kec);
		return $valReturn;
	}

	 function getCurrentPeriod() {
	 	$returnArr = array("response" => "false");
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->showCurrentPeriod();
		if($valReturn != null) {
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
		}
		return $returnArr;
	 }

	 function getAddressReferenceByID($memberid) {
		$this->load->model('webshop/shared_module', 'shared');
		$valReturn = $this->shared->getAddressReferenceByID($memberid);
		return $valReturn;
	 }

	 function showPricecodeByStockistIDX($nilai, $kec) {

		$returnArr = array("response" => "false");
		$this->load->model('webshop/shipping_model', 'shipping');
		$valReturn = $this->shipping->showPricecodeStockistX($nilai, $kec);
		if($valReturn != null) {
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
		}
		return $returnArr;
	}

	function showPricecodeByStockistCOD($nilai, $kec) {

		$returnArr = array("response" => "false");
		$this->load->model('webshop/shipping_model', 'shipping');
		$valReturn = $this->shipping->showPricecodeStockistCOD($nilai, $kec);
		if($valReturn != null) {
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
		}
		return $returnArr;
	}

	function getNewMemberData($token) {
		$this->load->model('webshop/member_model', 'member_model');
		$arr = $this->member_model->show_member_new2($token);
		return $arr;
	}
	
	function getNewMemberDataVA($token) {
		$this->load->model('webshop/member_model', 'member_model');
		$arr = $this->member_model->show_member_newVA($token);
		return $arr;
	}

	function sendTrxSmsRegMember($result) {
		$this->load->model('webshop/member_model', 'member_model');
		$upd = $this->member_model->sendTrxSmsRegMember($result);
	}
	
	

	function getMemberInfo3($userid) {
		$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->show_idmember($userid);
		if($arr != null) {
			$returnArr = array("sukses" => "true", "arrayData" => $arr);
		} else {
			$returnArr = array("sukses" => "false", "arrayData" => null);
		}

		return $returnArr;
	}

	function getMemberInfo($userid) {
		$request = array();
		$valReturn = array();
        //$this->xmlrpc->set_debug(TRUE);
        $this->xmlrpc->method('check.idmember');
        $request = array(array(array(
        			"idmemberx" => $userid,
                    "signature" => array(md5($userid . '12345'), 'string')
                ), 'struct'
        ));
        $this->xmlrpc->request($request);

        if (!$this->xmlrpc->send_request()) {
            echo $this->xmlrpc->display_error();
        } else {
        	if($userid == '0000999'){
        		echo $this->xmlrpc->display_error();
        	}else{
        		$arr = $this->xmlrpc->display_response();
	            $ss = json_decode($arr['arrayData']);
				$valReturn = array("sukses" => $arr['sukses'], "arrayData" => $ss);
	            //print_r($arr);
				//$valReturn = array("sukses" => $arr['sukses'], "idmemb" => $arr['idmemb'], "fullnm" => $arr['fullnm']);
				return $valReturn;
        	}

        }
	}

	function insertEcommShipAddr($order_id, $personal_info, $jne) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->insertEcommShipAddr($order_id, $personal_info, $jne);
		return $arr;
	}

	function getDataPaymentSGOByOrderID($orderid) {
		$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->getDataPaymentSGOByOrderID($orderid);
		return $arr;
	}
	
	
	function getDataPaymentSGOByOrderIDDev($orderid) {
		$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->getDataPaymentSGOByOrderIDDev($orderid);
		return $arr;
	}
	
	function getDataPaymentSGOByOrderID_withVch($orderid) {
		$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->getDataPaymentSGOByOrderID_withVch($orderid);
		return $arr;
	}
	
	function getDataPaymentSGOByOrderIDDevxxx($orderid) {
		$this->load->model('webshop/shared_module', 'shared');
		$arr = $this->shared->getDataPaymentSGOByOrderIDDevxxx($orderid);
		return $arr;
	}
	
	function JneConoteGenerateShow($row, $orderno) {
		//$addr1 = str_replace(array("\n","\r","(",")"), '', substr($row[0]->addr1, 0, 19));
		$addr1 = cleanAddress(substr($row[0]->addr1, 0, 19));
		//$addr2 = str_replace(array("\n","\r","(",")"), '', substr($row[0]->addr1, 19, 19));
		$addr2 = cleanAddress(substr($row[0]->addr1, 19, 19));
		//$addr3 = str_replace(array("\n","\r","(",")"), '', substr($row[0]->addr1, 38, 19));
		$addr3 = cleanAddress(substr($row[0]->addr1, 38, 19));
												//$telhp = str_replace("+", "", $telhp);
												//$telhp = str_replace(" ", "", $telhp);
		$stk_name = str_replace(array("\n","\r"), '', substr($row[0]->stockist_name, 0, 19));
		
		$prov_name = "PROPINSI";
		if($row[0]->province_name != null || $row[0]->province_name != "") {
			$prov_name = str_replace(array("\n","\r"), '', substr($row[0]->province_name, 0, 19));
		}
		
		$namaKota = "KABUPATEN";
		if($row[0]->kabupaten_name != null || $row[0]->kabupaten_name != "") {
			$kabu = strtoupper("Kab.");
			$namaKota = str_replace($kabu, "", $row[0]->kabupaten_name);
		}	
		
		$xhp = str_replace(array("\n","\r"), '', substr($row[0]->tel_hp1, 0, 14));
		$telhp = str_replace(array("-","+", " "), "", $xhp);
		
		$shipper_xhp = str_replace(array("\n","\r"), '', substr($row[0]->shipper_telhp, 0, 14));
		$shipper_telhp = str_replace(array("-","+", " "), "", $shipper_xhp);
					
		$curl_post_data = array(
            "username" => urlencode($this->jne_username),
            "api_key" => urlencode($this->jne_api_key),
            "OLSHOP_BRANCH" => urlencode($row[0]->jne_branch),
			"OLSHOP_CUST" => urlencode($this->jne_OLSHOP_CUST),
			"OLSHOP_ORIG" => urlencode($row[0]->sender_address),
			"OLSHOP_ORDERID" => urlencode($orderno),
			"OLSHOP_SHIPPER_NAME" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ADDR1" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ADDR2" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ADDR3" => urlencode($stk_name),
			"OLSHOP_SHIPPER_CITY" => urlencode($stk_name),
			"OLSHOP_SHIPPER_REGION" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ZIP" => urlencode("12620"),
			"OLSHOP_SHIPPER_PHONE" => urlencode($shipper_telhp),
			"OLSHOP_RECEIVER_NAME" => urlencode($row[0]->receiver_name),
			"OLSHOP_RECEIVER_ADDR1" => urlencode($addr1),
			"OLSHOP_RECEIVER_ADDR2" => urlencode($addr2),
			"OLSHOP_RECEIVER_ADDR3" => urlencode($addr3),
			//"OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
			"OLSHOP_RECEIVER_CITY" => urlencode($namaKota),
			"OLSHOP_RECEIVER_REGION" => urlencode($prov_name),
			"OLSHOP_RECEIVER_ZIP" => urlencode("12620"),
			"OLSHOP_RECEIVER_PHONE" => urlencode($telhp),
			"OLSHOP_DEST" => urlencode($row[0]->dest_address),
			//"OLSHOP_SERVICE" => urlencode($row[0]->service_type_id),
			"OLSHOP_SERVICE" => urlencode($row[0]->service_type_name),
			"OLSHOP_QTY" => urlencode($row[0]->total_item),
			"OLSHOP_WEIGHT" => urlencode(ceil($row[0]->total_weight)),

			"OLSHOP_GOODSTYPE" =>  urlencode("2"),
			"OLSHOP_GOODSDESC" =>  urlencode("SUPLEMENT"),
			"OLSHOP_INST" =>  urlencode("FRAGILE"),
			//"OLSHOP_GOODSVALUE" =>  urlencode($row[0]->total_pay_net),
			"OLSHOP_GOODSVALUE" =>  urlencode(""),
			//"OLSHOP_INSURANCE" => "Y",
			"OLSHOP_INS_FLAG" =>  urlencode("N"),
			"OLSHOP_COD_FLAG" =>  urlencode("N"),
			//"OLSHOP_COD_AMOUNT" =>  urlencode($row[0]->total_pay_net),
			"OLSHOP_COD_AMOUNT" =>  urlencode(""),
        );		
		
		//$postData = setFieldPost($curl_post_data);
		
		echo "<pre>";
		print_r($curl_post_data);
		echo "</pre>";
		
		$curl = curl_init();
        $arx = array(
		  CURLOPT_URL => $this->jne_url_conot_baru, 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_SSL_VERIFYPEER => true,
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => http_build_query($curl_post_data), 
		  CURLOPT_HTTPHEADER => array( 
		    "Content-Type: application/x-www-form-urlencoded")
		  );
		curl_setopt_array($curl, $arx);
		$curl_response = curl_exec($curl);
		curl_close($curl);
        return $curl_response;
	}
	
	
	function JneConoteGenerate($row, $orderno) {
		//$addr1 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 0, 19));
		//$addr2 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 19, 19));
		//$addr3 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 38, 19));
		
		$addr1 = cleanAddress(substr($row[0]->addr1, 0, 19));
		//$addr2 = str_replace(array("\n","\r","(",")"), '', substr($row[0]->addr1, 19, 19));
		$addr2 = cleanAddress(substr($row[0]->addr1, 19, 19));
		//$addr3 = str_replace(array("\n","\r","(",")"), '', substr($row[0]->addr1, 38, 19));
		$addr3 = cleanAddress(substr($row[0]->addr1, 38, 19));
		
												//$telhp = str_replace("+", "", $telhp);
												//$telhp = str_replace(" ", "", $telhp);
		$stk_name = str_replace(array("\n","\r"), '', substr($row[0]->stockist_name, 0, 19));
		
		$prov_name = "PROPINSI";
		if($row[0]->province_name != null || $row[0]->province_name != "") {
			$prov_name = str_replace(array("\n","\r"), '', substr($row[0]->province_name, 0, 19));
		}
		
		$namaKota = "KABUPATEN";
		if($row[0]->kabupaten_name != null || $row[0]->kabupaten_name != "") {
			$kabu = strtoupper("Kab.");
			$namaKota = str_replace($kabu, "", $row[0]->kabupaten_name);
		}	
		
		$xhp = str_replace(array("\n","\r"), '', substr($row[0]->tel_hp1, 0, 14));
		$telhp = str_replace(array("-","+", " "), "", $xhp);
		
		$shipper_xhp = str_replace(array("\n","\r"), '', substr($row[0]->shipper_telhp, 0, 14));
		$shipper_telhp = str_replace(array("-","+", " "), "", $shipper_xhp);
					
		$curl_post_data = array(
            "username" => urlencode($this->jne_username),
            "api_key" => urlencode($this->jne_api_key),
            "OLSHOP_BRANCH" => urlencode($row[0]->jne_branch),
			"OLSHOP_CUST" => urlencode($this->jne_OLSHOP_CUST),
			"OLSHOP_ORIG" => urlencode($row[0]->sender_address),
			"OLSHOP_ORDERID" => urlencode($orderno),
			"OLSHOP_SHIPPER_NAME" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ADDR1" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ADDR2" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ADDR3" => urlencode($stk_name),
			"OLSHOP_SHIPPER_CITY" => urlencode($stk_name),
			"OLSHOP_SHIPPER_REGION" => urlencode($stk_name),
			"OLSHOP_SHIPPER_ZIP" => urlencode("12620"),
			"OLSHOP_SHIPPER_PHONE" => urlencode($shipper_telhp),
			"OLSHOP_RECEIVER_NAME" => urlencode($row[0]->receiver_name),
			"OLSHOP_RECEIVER_ADDR1" => urlencode($addr1),
			"OLSHOP_RECEIVER_ADDR2" => urlencode($addr2),
			"OLSHOP_RECEIVER_ADDR3" => urlencode($addr3),
			//"OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
			"OLSHOP_RECEIVER_CITY" => urlencode($namaKota),
			"OLSHOP_RECEIVER_REGION" => urlencode($prov_name),
			"OLSHOP_RECEIVER_ZIP" => urlencode("12620"),
			"OLSHOP_RECEIVER_PHONE" => urlencode($telhp),
			"OLSHOP_DEST" => urlencode($row[0]->dest_address),
			//"OLSHOP_SERVICE" => urlencode($row[0]->service_type_id),
			"OLSHOP_SERVICE" => urlencode($row[0]->service_type_name),
			"OLSHOP_QTY" => urlencode($row[0]->total_item),
			"OLSHOP_WEIGHT" => urlencode(ceil($row[0]->total_weight)),

			"OLSHOP_GOODSTYPE" =>  urlencode("2"),
			"OLSHOP_GOODSDESC" =>  urlencode("SUPLEMENT"),
			"OLSHOP_INST" =>  urlencode("FRAGILE"),
			//"OLSHOP_GOODSVALUE" =>  urlencode($row[0]->total_pay_net),
			"OLSHOP_GOODSVALUE" =>  urlencode(""),
			//"OLSHOP_INSURANCE" => "Y",
			"OLSHOP_INS_FLAG" =>  urlencode("N"),
			"OLSHOP_COD_FLAG" =>  urlencode("N"),
			//"OLSHOP_COD_AMOUNT" =>  urlencode($row[0]->total_pay_net),
			"OLSHOP_COD_AMOUNT" =>  urlencode(""),
        );		
		
		//$postData = setFieldPost($curl_post_data);
		
		$curl = curl_init();
        $arx = array(
		  CURLOPT_URL => $this->jne_url_conot_baru, 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_SSL_VERIFYPEER => true,
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => http_build_query($curl_post_data), 
		  CURLOPT_HTTPHEADER => array( 
		    "Content-Type: application/x-www-form-urlencoded")
		  );
		curl_setopt_array($curl, $arx);
		$curl_response = curl_exec($curl);
		curl_close($curl);
        return $curl_response;
		
					
	}
	
	function generate_konot_jne_only($order_id = null, $orderno) {
		$queryShipAddr = null;
	    $this->db = $this->load->database('db_ecommerce', true);
	    $qryShpHdr = "SELECT a.*, b.userlogin
		              FROM ecomm_trans_shipaddr_sgo a 
					  LEFT JOIN ecomm_trans_hdr_sgo b ON (a.orderno = b.orderno)
		              WHERE a.orderno = '$order_id'";
		$queryShpHc = $this->db->query($qryShpHdr);
		
		$arrReturn = null;
		if($queryShpHc->num_rows() > 0) {
			$row = $queryShpHc->result();
			if($row[0]->cargo_id == "1") {
				 $resJne = $this->JneConoteGenerateShow($row, $orderno);
				 //$resJne = $this->JneConoteGenerateShow($row, $orderno);
				 $arrReturn = json_decode($resJne);
					
			}
			
		}	
		return $arrReturn;
	}
	
	function generate_konot($order_id = null, $orderno) {
		$queryShipAddr = null;
	    $this->db = $this->load->database('db_ecommerce', true);
	    $qryShpHdr = "SELECT a.*, b.userlogin
		              FROM ecomm_trans_shipaddr_sgo a 
					  LEFT JOIN ecomm_trans_hdr_sgo b ON (a.orderno = b.orderno)
		              WHERE a.orderno = '$order_id'";
		$queryShpHc = $this->db->query($qryShpHdr);

		if($queryShpHc->num_rows() > 0) {
			$row = $queryShpHc->result();

			if($row[0]->cargo_id == "1") {
			        /*
				    $addr1 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 0, 19));
					$addr2 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 19, 19));
					$addr3 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 38, 19));
					$telhp = str_replace(array("-","+", " "), "", $row[0]->tel_hp1);
					$stk_name = str_replace(array("\n","\r"), '', substr($row[0]->stockist_name, 0, 19));
					$prov_name = str_replace(array("\n","\r"), '', substr($row[0]->province_name, 0, 19));
	            	$kabu = strtoupper("Kab.");
		            $namaKota = str_replace($kabu, "", $row[0]->kabupaten_name);
					$curl_post_data = array(
	                            "username" => $this->jne_username,
	                            "api_key" => $this->jne_api_key,
	                            "OLSHOP_BRANCH" => $row[0]->jne_branch,
	                            "OLSHOP_CUST" => $this->jne_OLSHOP_CUST,
	                            "OLSHOP_ORIG" => $row[0]->sender_address,
	                            "OLSHOP_ORDERID" => $orderno,
	                            "OLSHOP_SHIPPER_NAME" => $stk_name,
	                            "OLSHOP_SHIPPER_ADDR1" => $stk_name,
	                            "OLSHOP_SHIPPER_ADDR2" => $stk_name,
	                            "OLSHOP_SHIPPER_ADDR3" => $stk_name,
	                            "OLSHOP_SHIPPER_CITY" => $stk_name,
	                            "OLSHOP_SHIPPER_REGION" => $stk_name,
	                            "OLSHOP_SHIPPER_ZIP" => "12620",
	                            "OLSHOP_SHIPPER_PHONE" => $row[0]->shipper_telhp,
	                            "OLSHOP_RECEIVER_NAME" => $row[0]->receiver_name,
	                            "OLSHOP_RECEIVER_ADDR1" => $addr1,
	                            "OLSHOP_RECEIVER_ADDR2" => $addr2,
	                            "OLSHOP_RECEIVER_ADDR3" => $addr3,
	                            //"OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
	                            "OLSHOP_RECEIVER_CITY" => $namaKota,
	                            "OLSHOP_RECEIVER_REGION" => $prov_name,
	                            "OLSHOP_RECEIVER_ZIP" => "12620",
	                            "OLSHOP_RECEIVER_PHONE" => $telhp,
	                            "OLSHOP_DEST" => $row[0]->dest_address,
	                            "OLSHOP_SERVICE" => $row[0]->service_type_id,
	                            "OLSHOP_QTY" => $row[0]->total_item,
	                            "OLSHOP_WEIGHT" => $row[0]->total_weight,

	                            "OLSHOP_GOODSTYPE" => "2",
	                            "OLSHOP_GOODSDESC" => "SUPLEMENT",
	                            "OLSHOP_INST" => "FRAGILE",
	                            "OLSHOP_GOODSVALUE" => $row[0]->total_pay_net,
	                            "OLSHOP_INSURANCE" => "Y"
	                            );

			        $curl = curl_init($this->jne_url_conot);

			        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($curl, CURLOPT_POST, true);
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			        $curl_response = curl_exec($curl);
					$arr = json_decode($curl_response);
					*/

                    $resJne = $this->JneConoteGenerate($row, $orderno);
										
					$arr = json_decode($resJne);
					
					/*if($row[0]->userlogin == "IDSPAAA66834") {
					    echo "<pre>";
						echo $resJne;
						echo "</pre>";
					}*/
					$jne_cnote = null;
					if($arr != null) {
						if($arr->detail == null) {
							$jne_cnote = null;
						} else if($arr->detail[0]->status == "sukses") {
							$jne_cnote = $arr->detail[0]->cnote_no;
						} else if($arr->detail[0]->status == "Error") {
							$jne_cnote = null;
						}
					}
					
					$shipper_type = 0;
					if($row[0]->cargo_id !=  null) {
						$shipper_type = $row[0]->cargo_id;
					} 
					$telhp = str_replace(array("-","+", " "), "", $row[0]->tel_hp1);
					
		            $insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr
		            					(orderno,idstk,prov_code,kab_code,kec_code,
		            					 addr1,email,tel_hp1,
		            					 conoteJNE, service_type_id, service_type_name, receiver_name, total_item, total_weight, cargo_id, lat_dest, long_dest)
		                             values('".$orderno."','".$row[0]->idstk."','".$row[0]->prov_code."','".$row[0]->kab_code."',
		                                    '".$row[0]->kec_code."','".strtoupper(cleanAddress($row[0]->addr1))."',
		                                    '".$row[0]->email."','".$telhp."',
		                                    '$jne_cnote','".$row[0]->service_type_id."','".$row[0]->service_type_name."', 
		                                    '".$row[0]->receiver_name."', ".$row[0]->total_item.", ".$row[0]->total_weight.", ".$shipper_type.", 
		                                    '".$row[0]->lat_dest."', '".$row[0]->long_dest."')";
		            //echo "ShipAddr <br>".$insShipAddr."<br><br>";
		            $dsShipAddr = $this->db->query($insShipAddr);
					return $dsShipAddr;
		        	
			} else if($row[0]->cargo_id == "5") {
				$telhp = str_replace(array("-","+", " "), "", $row[0]->tel_hp1);
				$shipper_type = $row[0]->cargo_id;	
		            $insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr
		            					(orderno,idstk,prov_code,kab_code,kec_code,
		            					 addr1,email,tel_hp1,
		            					 conoteJNE, service_type_id, service_type_name, receiver_name, total_item, total_weight, cargo_id, lat_dest, long_dest)
		                             values('".$orderno."','".$row[0]->idstk."','".$row[0]->prov_code."','".$row[0]->kab_code."',
		                                    '".$row[0]->kec_code."','".strtoupper(cleanAddress($row[0]->addr1))."',
		                                    '".$row[0]->email."','".$telhp."',
		                                    '','".$row[0]->service_type_id."','".$row[0]->service_type_name."', 
		                                    '".$row[0]->receiver_name."', ".$row[0]->total_item.", ".$row[0]->total_weight.", ".$shipper_type.", 
		                                    '".$row[0]->lat_dest."', '".$row[0]->long_dest."')";
		            //echo "ShipAddr <br>".$insShipAddr."<br><br>";
		            $dsShipAddr = $this->db->query($insShipAddr);
					return $dsShipAddr;
			} 	
			
			else if($row[0]->cargo_id == "2" || $row[0]->cargo_id == "" || $row[0]->cargo_id == null) {
				//$urlKota = "http://www.kgpdev.co.id:8080/Service.svc/ResponseKonos";
				$urlKota = "202.152.20.135:8080/Service.svc/ResponseKonos";
		        //$curl = curl_init($urlKota);
				$curl_data = array(
						 //"usernama"	    => "relas",
						 //"passw"		=> "123456",
						 "usernama"	    => "k-link2",
						 "passw"		=> "pusat@59A",
						 "transaksi_id" => $orderno,
						 "kota"		    => $row[0]->dest_address,
						 "penerima"	    => $row[0]->receiver_name,
						 "alamat"		=> $row[0]->addr1,
						 "telepon"		=> $row[0]->tel_hp1,
						 "berat"	    => $row[0]->total_weight);
						 
			   $data_string = json_encode($curl_data);                                                                                   
		                                                                                                                     
				$ch = curl_init($urlKota);                                                                      
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				    'Content-Type: application/json',                                                                                
				    'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
			                                                                                                                     
			   $curl_response = curl_exec($ch);
			   $arr = json_decode($curl_response);
			   
			   if($arr->status == "999") {
					$jne_cnote = null;
			   } else {
					$jne_cnote = $arr->KonosemenNo;
			   }
			   
			   $shipper_type = 0;
				if($row[0]->cargo_id !=  null) {
					$shipper_type = $row[0]->cargo_id;
				} 
			   
			   $insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr
	            					(orderno,idstk,prov_code,kab_code,kec_code,
	            					 addr1,email,tel_hp1,
	            					 conoteJNE, service_type_id, service_type_name, receiver_name, total_item, total_weight, cargo_id)
	                             values('".$orderno."','".$row[0]->idstk."','".$row[0]->prov_code."','".$row[0]->kab_code."',
	                                    '".$row[0]->kec_code."','".strtoupper($row[0]->addr1)."',
	                                    '".$row[0]->email."','".$row[0]->tel_hp1."',
	                                    '$jne_cnote','".$row[0]->service_type_id."','".$row[0]->service_type_name."', 
	                                    '".$row[0]->receiver_name."', ".$row[0]->total_item.", ".$row[0]->total_weight.", ".$shipper_type.")";
	            //echo "ShipAddr <br>".$insShipAddr."<br><br>";
	            $dsShipAddr = $this->db->query($insShipAddr);
				return $dsShipAddr;

				//tambahan gosend
			} else if($row[0]->cargo_id == "4") {

				$qryItem = "SELECT a.prdnm,a.qty
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
				$queryExec = $this->db->query($qryItem);

				if($queryExec->num_rows() > 0) {
					$no=0;
					foreach($queryExec->result() as $row1)
					{
						$no++;
						$prdnm= $row1->prdnm;
						$qty= $row1->qty;

						$concat_item= "(".$prdnm." : ".$qty." pc)";
						$item_arr[]= $concat_item;
						$item=implode(" ",$item_arr);
					}

				}

				$jml=0;
				$qryItem2 = "select count(token) as jml from ecomm_trans_hdr where token='$order_id'";
				$queryExec2 = $this->db->query($qryItem2);

				if($queryExec2->num_rows() > 0) {
					foreach($queryExec2->result() as $row2)
					{
						$jml= $row2->jml;
					}

				}

				$lat=$row[0]->lat_dest;
				$long=$row[0]->long_dest;

				if($lat == NULL && $long == NULL){
					$X= $row[0]->kabupaten_name;
					$Y= $row[0]->province_name;
				}else{
					$X= $lat;
					$Y=$long;
				}

				//menggunakan API k-net
				$metode = $row[0]->service_type_name;
				$des_name = $row[0]->receiver_name;
				$des_phone = $row[0]->tel_hp1;
				$des_phone= preg_replace('/[^0-9]/', '', $des_phone); // just number.

				$des_addr = cleanAddress($row[0]->addr1);
				$des_addr= preg_replace('/[^A-Za-z0-9-]/', '', $des_addr); // Removes special chars.

				$des_coord = $X.",".$Y;
				$price = $row[0]->total_pay_net;

				$warehouse=$row[0]->whcd;
				$stk=$row[0]->idstk;



				//$qryItem1 = "SELECT * FROM V_ECOMM_GOSEND_SENDER_DET where whcd='$warehouse' AND loccd='$stk'";
				$qryItem1 = "SELECT * FROM V_ECOMM_GOSEND_SENDER_DET where loccd='$stk'";
				$queryExec1 = $this->db->query($qryItem1);

				if($queryExec1->num_rows() > 0) {
					foreach($queryExec1->result() as $rows)
					{
						$fullnm= $rows-> fullnm;
						$addr1= $rows->addr1;
						$addr2= $rows->addr2;
						$addr3= $rows->addr3;
						$city= $rows->city;
						$wh_tel_hp= $rows->tel_hp;
						$wh_lat= $rows->latitude;
						$wh_long= $rows->longitude;
						$wh_head= $rows->WH_HEAD;
						$hp_confirm= $rows->hp_to_confirm;

					}
				}


				if($hp_confirm == ""){
					$origin_kontak= $wh_tel_hp;
				}else{
					$origin_kontak= $hp_confirm;
				}
				$sendhp = explode(",", $origin_kontak);


				$senderName= $wh_head;
				$senderNote= $hp_confirm;
				$senderContact= $fullnm;
				$senderPhone= $sendhp[0];
				$senderCoord= $wh_lat.",".$wh_long;
				$senderAlamat= $addr1." ".$addr2." ".$addr3." ".$city;

				/*$senderName="Bagian Warehouse";
				$senderNote="K-Mart Lobby Timur";
				$senderContact="K-Link Indonesia";
				$senderPhone="08568369705";
				$senderCoord="-6.2400292,106.8343388";
				$senderAlamat="Jalan Jend. Gatot Subroto Kav. 59A Jakarta Selatan (Lobby Timur)";*/

				/*print_r($metode);
				print_r($des_name);
				print_r($des_phone);
				print_r($des_addr);
				print_r($des_coord);
				print_r($price);
				print_r($item);*/

				//get user key gosend
				$this->load->model('lokasi/M_location', 'M_location');
				$getKey= $this->M_location->getUserKey();
				$user_key= $getKey[0]->username;
				$pass_key= $getKey[0]->key_user;

				$getKey1= $this->M_location->getUserKeyDev(); //get API development
				$user_key1= $getKey1[0]->username;
				$pass_key1= $getKey1[0]->key_user;

				//mendapatkan jam buka gosend
				$day = date('D');
				$time= date('H:i:s');
				$now = new Datetime($time);
				//$now = new Datetime("now");
				$type_service=trim($row[0]->service_type_name);

				$hp01= $row[0]->tel_hp1;
				$hp02= $row[0]->tel_hp2;
				if($hp01 == ""){
					$hp=$hp02;
				}else{
					$hp=$hp01;
				}

				$hp_val=str_replace("62","0",$hp);
				$hp_val=str_replace("+","",$hp_val);
				$hp_val=str_replace("-","",$hp_val);
				$hp_val=str_replace(" ","",$hp_val);

				$hp1= $sendhp[1];
				$hp2= $sendhp[0];

				/*$hp1= "081388319355";
				$hp2= "08568369705";*/
				$hp3="085717743438"; //nohp vera

				//Instant
				//$getInstant= $this->M_location->getSchedule($day,'Instant');
				$getInstant= $this->M_location->getSchedule2($day,'Instant',$stk);
				$open_inst= $getInstant[0]->opened;
				$closed_inst= $getInstant[0]->closed_pay; //batas akhir konfirmasi pembayaran
				$begintime1 = new DateTime($open_inst);
				$endtime1 = new DateTime($closed_inst);

				$inst_open=date('H:i:s', strtotime($open_inst));
				$inst_closed=date('H:i:s', strtotime($closed_inst));

				//SameDay
				//$getSame= $this->M_location->getSchedule($day,'SameDay');
				$getSame= $this->M_location->getSchedule2($day,'SameDay',$stk);
				$open_same= $getSame[0]->opened;
				$closed_same= $getSame[0]->closed_pay; // batas akhir konfirmasi pembayaran
				$begintime2 = new DateTime($open_same);
				$endtime2 = new DateTime($closed_same);

				$same_open=date('H:i:s', strtotime($open_same));
				$same_closed=date('H:i:s', strtotime($closed_same));

				$order_g="";

				// CALL API GOSEND
				if($type_service == 'Instant'){
					if(($time >= $inst_open && $time <= $inst_closed)) {
						if ($row[0]->userlogin == "IDSPAAA66834") {
							$dt = booking_gsend_dev($user_key1,$pass_key1,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
						} else {
							$dt = booking_gsend($user_key,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
						}

						//$dt = booking_gsend($user_key,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
						$result= json_decode($dt, TRUE);
						if($result == null || $result =='' || $result['errors'] !=''){
							$order_g="";
							/*//sms ke gudang
							$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
							smsTemplate($hp1, $txt);
							smsTemplate($hp2, $txt);*/
						}else{
							$order_g=$result['orderNo'];
							/*//sms ke vera
							$txt = "error gosend Sameday:".$orderno.",error:".$result['orderNo'];
							smsTemplate($hp3, $txt);*/
						}

						if($order_g != ""){
							//sms ke gudang
							$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
							smsTemplate($hp1, $txt);
							smsTemplate($hp2, $txt);
						}else{
							if($jml > 1){
								$order_g="";
								$txt = "error gosend Instant, Token Double:".$orderno;
								smsTemplate($hp3, $txt);
							}else{
								$txt = "error gosend Instant:".$orderno;
								smsTemplate($hp3, $txt);
							}
						}

					}else{
						$order_g="";
						//sms ke warehouse
						$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari kerja besok: nomor order".$orderno.",penerima:".$des_name;
						smsTemplate($hp1, $txt);
						smsTemplate($hp2, $txt);
					}

				} else if($type_service == 'SameDay'){
					if(($time >= $same_open && $time <= $same_closed)){

						if ($row[0]->userlogin == "IDSPAAA66834") {
							$dt = booking_gsend_dev($user_key1,$pass_key1,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);

						} else {
							$dt = booking_gsend($user_key,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
						}

						//$dt = booking_gsend($user_key, $pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
						//echo $dt;
						$result= json_decode($dt, TRUE);
						if($result == null || $result ==''||  $result['errors'] !=''){
							$order_g="";
							/*//sms ke gudang
							$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
							smsTemplate($hp1, $txt);
							smsTemplate($hp2, $txt);*/
						}else{
							$order_g=$result['orderNo'];
							/*//sms ke vera
							$txt = "error gosend Sameday:".$orderno.",error:".$result['orderNo'];
							smsTemplate($hp3, $txt);*/
						}

						if($order_g != ""){
							//sms ke gudang
							$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
							smsTemplate($hp1, $txt);
							smsTemplate($hp2, $txt);
						}else{
							//sms ke vera
							if($jml > 1){
								$order_g="";
								$txt = "error gosend Sameday, Token Double:".$orderno;
								smsTemplate($hp3, $txt);
							}else{
								$txt = "error gosend Sameday:".$orderno;
								smsTemplate($hp3, $txt);
							}

						}

					} else {
						$order_g="";
						$text = "Terimakasih telah melakukan pembelanjaan di K-Link, jadwal order GOSEND SameDay ".$same_open." WIB - ".$same_closed." WIB. Pesanan Anda akan segera dikirimkan pada hari kerja besok. ";
						smsTemplate($hp_val, $text);

						//sms ke warehouse
						$txt = "Info pesanan gosend ".$metode." yang akan dikirim hari kerja besok: nomor order".$orderno.",penerima:".$des_name;
						smsTemplate($hp1, $txt);
						smsTemplate($hp2, $txt);
					}

				}else{
					$order_g="";
					$text = "Terimakasih telah melakukan pembelanjaan di K-Link, metode pengiriman Anda akan kami cek kembali.";
					smsTemplate($hp_val, $text);

					//sms ke vera
					$txt = "error gosend:".$orderno."conote kosong!";
					smsTemplate($hp3, $txt);
				}


				$shipper_type = 0;
				if($row[0]->cargo_id !=  null) {
					$shipper_type = $row[0]->cargo_id;
				}

				//$row[0]->kec_code diganti jadi 411 utk sementara
				//$kec_kode='411';
				/*$insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr
	            					(orderno,idstk,prov_code,kab_code,kec_code,
	            					 addr1,email,tel_hp1,
	            					 conoteJNE, service_type_id, service_type_name, receiver_name, total_item, total_weight, cargo_id,lat_dest,long_dest)
	                             values('".$orderno."','".$row[0]->idstk."','".$row[0]->prov_code."','".$row[0]->kab_code."',
	                                    '411','".strtoupper($row[0]->addr1)."',
	                                    '".$row[0]->email."','".$row[0]->tel_hp1."',
	                                    '".$order_g."','','".$row[0]->service_type_name."',
	                                    '".$row[0]->receiver_name."', ".$row[0]->total_item.", ".$row[0]->total_weight.", $shipper_type,".$X.",".$Y.")";*/

				$alamat=$row[0]->addr1;
				$alamat= preg_replace('/[^A-Za-z0-9-]/', ' ', $alamat); // Removes special chars.

				$phone= $row[0]->tel_hp1;
				$phone= preg_replace('/[^0-9]/', '', $phone); // just number.

				$insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr
	            					(orderno,idstk,prov_code,kab_code,kec_code,
	            					 addr1,email,tel_hp1,
	            					 conoteJNE, service_type_id, service_type_name, receiver_name, total_item, total_weight, cargo_id,lat_dest,long_dest,whcd,whnm)
	                             values('".$orderno."','".$row[0]->idstk."','".$row[0]->prov_code."','".$row[0]->kab_code."',
	                                    '','".strtoupper($alamat)."',
	                                    '".$row[0]->email."','".$phone."',
	                                    '','','".$row[0]->service_type_name."',
	                                    '".$row[0]->receiver_name."', '".$row[0]->total_item."', '".$row[0]->total_weight."', '".$shipper_type."','".$X."','".$Y."','".$row[0]->whcd."','".$row[0]->whnm."')";
				$dsShipAddr = $this->db->query($insShipAddr);
				
				//update Vera 17012019
				$updtShipAddr="update db_ecommerce.dbo.ecomm_trans_shipaddr set conoteJNE='".$order_g."' where orderno='".$orderno."'";
				$duShipAddr = $this->db->query($updtShipAddr);
				return $duShipAddr;
			}
		}
	}

	function generateCNoteJNE_baru($order_id = null, $orderno) {
		    $queryShipAddr = null;
		    $this->db = $this->load->database('db_ecommerce', true);
		    $qryShpHdr = "SELECT * FROM ecomm_trans_shipaddr_sgo WHERE orderno = '$order_id'";
			$queryShpHdr = $this->db->query($qryShpHdr);
		    if($queryShpHdr->num_rows() > 0) {
				foreach($queryShpHdr->result() as $row)
	            {
	            	$kabu = strtoupper("Kab.");
		            $namaKota = str_replace($kabu, "", $row->kabupaten_name);
					$curl_post_data = array(
	                            "username" => $this->jne_username,
	                            "api_key" => $this->jne_api_key,
	                            "OLSHOP_BRANCH" => $row->jne_branch,
	                            "OLSHOP_CUST" => $this->jne_OLSHOP_CUST,
	                            "OLSHOP_ORIG" => $row->sender_address,
	                            "OLSHOP_ORDERID" => $orderno,
	                            "OLSHOP_SHIPPER_NAME" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ADDR1" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ADDR2" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ADDR3" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_CITY" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_REGION" => $row->stockist_name,
	                            "OLSHOP_SHIPPER_ZIP" => "12620",
	                            "OLSHOP_SHIPPER_PHONE" => $row->shipper_telhp,
	                            "OLSHOP_RECEIVER_NAME" => $row->receiver_name,
	                            "OLSHOP_RECEIVER_ADDR1" => substr($row->addr1, 0, 20),
	                            "OLSHOP_RECEIVER_ADDR2" => substr($row->addr1, 21, 20),
	                            "OLSHOP_RECEIVER_ADDR3" => substr($row->addr1, 41, 20),
	                            //"OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
	                            "OLSHOP_RECEIVER_CITY" => $namaKota,
	                            "OLSHOP_RECEIVER_REGION" => $row->province_name,
	                            "OLSHOP_RECEIVER_ZIP" => "12620",
	                            "OLSHOP_RECEIVER_PHONE" => $row->tel_hp1,
	                            "OLSHOP_DEST" => $row->dest_address,
	                            //"OLSHOP_SERVICE" => $row->service_type_id,
								"OLSHOP_SERVICE" => urlencode($row[0]->service_type_name),
	                            "OLSHOP_QTY" => $row->total_item,
	                            "OLSHOP_WEIGHT" => $row->total_weight,

	                            "OLSHOP_GOODSTYPE" => "2",
	                            "OLSHOP_GOODSDESC" => "SUPLEMENT",
	                            "OLSHOP_INST" => "FRAGILE",
	                            //"OLSHOP_GOODSVALUE" => $row->total_pay_net,
								"OLSHOP_GOODSVALUE" => "",
	                            "OLSHOP_INSURANCE" => "N"
	                            );

			        $curl = curl_init($this->jne_url_conot);

			        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($curl, CURLOPT_POST, true);
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			        $curl_response = curl_exec($curl);
					$arr = json_decode($curl_response);
					//print_r($arr);

					//edit 29/03/2016
					//$jne_cnote = $arr->detail[0]->cnote_no;

					$jne_cnote = null;
					if($arr != null) {
						if($arr->detail == null) {
							$jne_cnote = null;
						} else if($arr->detail[0]->status == "sukses") {
							$jne_cnote = $arr->detail[0]->cnote_no;
						} else if($arr->detail[0]->status == "Error") {
							$jne_cnote = null;
						}
					}

		            $insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr
		            					(orderno,idstk,prov_code,kab_code,kec_code,
		            					 addr1,email,tel_hp1,
		            					 conoteJNE, service_type_id, service_type_name, receiver_name, total_item, total_weight, cargo_id)
		                             values('".$orderno."','".$row->idstk."','".$row->prov_code."','".$row->kab_code."',
		                                    '".$row->kec_code."','".strtoupper($row->addr1)."',
		                                    '".$row->email."','".$row->tel_hp1."',
		                                    '$jne_cnote','".$row->service_type_id."','".$row->service_type_name."', '".$row->receiver_name."', $row->total_item, $row->total_weight, '".$row->cargo_id."')";
		            //echo "ShipAddr <br>".$insShipAddr."<br><br>";
		            $queryShipAddr = $this->db->query($insShipAddr);


				}
			}
			return $queryShipAddr;
	}

	/*function apiEmailSend() {
		$curl_post_data = array(
			"login_username"  => "catur.bharata@gmail.com",
			"login_password"  =>"asdqwe123",
			"template_id"    => "1",
			"mail_to"         => "muammarqathafi@gmail.com",
			"mail_subject"    => "test",
			"mail_from"       => "development@fiwa.sch.id",
			"mail_name"       => "fiwa",
			"email"           => "muammarqathafi@gmail.com",
			"username"        => "Muammar%20Qathafi",
			"password"        => "123456",
			"id_number"       => "12347890",
			"sponsor_name"    => "Catur%20Bharata",
			"sponsor_email"   => "catur.bharata@gmail.com"
		);
		
		$qry_str = "?login_username=".$usr;
		$qry_str .= "&login_password=".$usr;
		$qry_str .= "&template_id=".$usr;
		$qry_str .= "&mail_to=".$usr;
		$qry_str .= "&mail_subject=".$usr;
		$qry_str .= "&mail_from=".$usr;
		$qry_str .= "&mail_name=".$usr;
		$qry_str .= "&email=".$usr;
		$qry_str .= "&username=".$usr;
		$qry_str .= "&password=".$usr;
		$qry_str .= "&id_number=".$usr;
		$qry_str .= "&sponsor_name=".$usr;
		$qry_str .= "&sponsor_email=".$usr;
		
		$ch = curl_init();
		
		// Set query data here with the URL
		curl_setopt($ch, CURLOPT_URL, '"http://192.168.0.168/ecm/go.php/api/email"' . $qry_str); 
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '3');
		$content = trim(curl_exec($ch));
		curl_close($ch);
	}*/

	function getConoteJNE($orderno) {
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		$arr = $this->ecomm_trans_model->getConoteJNE($orderno);
		return $arr;
	}

	function trackingJneConot($conot) {
		$url = "http://apiv2.jne.co.id:10101/tracing/api/list/cnote/$conot";
		//$url = "http://api.jne.co.id:8889/tracing/klink/list/cnote/$awb";
        $curl_post_data = array(
                            "username" => $this->jne_username,
                            "api_key" => $this->jne_api_key
                            );
		$postData = setFieldPost($curl_post_data);					
        /*$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;*/
		
		$curl = curl_init();
        $arx = array(
		  CURLOPT_URL => $url, 
		  CURLOPT_RETURNTRANSFER => true, 
		 CURLOPT_SSL_VERIFYPEER => true,
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => $postData, 
		  CURLOPT_HTTPHEADER => array( 
		    "Content-Type: application/x-www-form-urlencoded")
		 
		  );
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		if ($err) {
			echo $err;
		} else {
           return $response;
		}
		
	}

	/*function showListPriceJne($weight) {
		$data = $this->input->post(NULL, TRUE);
        $xx = $this->session->userdata('shipping_jne_info');
		$url = "http://api.jne.co.id:8889/tracing/klink/price/";

        $curl_post_data = array(
                            "username" => $this->jne_username,
                            "api_key" => $this->jne_api_key,
                            "from" => $this->session->userdata("sender_address"),
                            "thru" => $this->session->userdata("destination_address"),
                            "weight" => $weight
                            );
        //$curl = curl_init($url);
		$curl = curl_init($this->generate_url_ship_price);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return $curl_response;
	}*/

    function showListPriceJne($weight) {
    	//$data = $this->input->post(NULL, TRUE);
        $xx = $this->session->userdata('shipping_jne_info');
        
		$curl_post_data = array(
            "username" => urlencode($this->jne_username),
            "api_key" => urlencode($this->jne_api_key),
            "from" => urlencode($this->session->userdata("sender_address")),
            "thru" => urlencode($this->session->userdata("destination_address")),
            "weight" => urlencode($weight)
        );		
		
		$postData = setFieldPost($curl_post_data);
		
		$curl = curl_init();
        $arx = array(
		  CURLOPT_URL => $this->generate_url_ship_priceBaru, 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_SSL_VERIFYPEER => true,
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => $postData, 
		  CURLOPT_HTTPHEADER => array( 
		    "Content-Type: application/x-www-form-urlencoded")
		  );
		curl_setopt_array($curl, $arx);
		$curl_response = curl_exec($curl);
		curl_close($curl);
        return $curl_response;
	}	

	function  showListPriceKgb($weight) {
		$xx = $this->session->userdata('shipping_jne_info');
		//$urlKota = "http://36.37.81.131:8080/Service.svc/KlinkTarifResponse";
		$urlKota = pricing_url("2");
		$curl = curl_init($urlKota);
        $data = array("usernama" => "k-link2",
                      "passw" => "pusat@59A",
					  //"kota" => $personal_info['kota'],
					  "kota" => $this->session->userdata('destination_address'),
					  "berat" => $this->cart->total_weight());        
		//print_r($data);			                                                         
		$data_string = json_encode($data);                                                                                   
		                                                                                                                     
		$ch = curl_init($urlKota);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($ch);
		return $result;
	}	

       function  showListPriceKgb2($weight) {
		$xx = $this->session->userdata('shipping_jne_info');
		//$urlKota = "http://36.37.81.131:8080/Service.svc/KlinkTarifResponse";
		$urlKota = pricing_url("2");
		//$urlKota = "202.152.20.135/Service.svc/KlinkTarifResponse";
		//$urlKota = "http://36.37.81.131:8080/Service.svc/KlinkTarifResponse";
		$curl = curl_init($urlKota);
        /*$data = array("usernama" => "relas",
                      "passw" => "123456",
					  //"kota" => $personal_info['kota'],
					  "kota" => "040101",
					  "berat" => $this->cart->total_weight());     */
				   
	    $data = array("usernama" => "k-link2",
                      "passw" => "pusat@59A",
					  //"kota" => $personal_info['kota'],
					  "kota" => $this->session->userdata('destination_address'),
					  //"kota" => "040101",
					  "berat" => $this->cart->total_weight());
					                                                        
		$data_string = json_encode($data);                                                                                   
		//print_r($data_string);                                                                                                                     
		$ch = curl_init($urlKota);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                    
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($ch);
		$err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
		$info = null;
		$dfd = array("return" => $result, "err" => $err, "errmsg" => $errmsg, "info" => $info);
		//$sd = json_decode($result, true);
		//print_r($dfd);	
		return $result;
	}	

	function setLowestJnePrice($arr) {
		$test = array();
		$arrx = null;
			foreach($arr->price as $dta) {
				if($dta->service_code != "OKE15" && $dta->service_code != "CTCOKE15") {
						$arrx = array(
						  "service_code" => $dta->service_code,
						  "service_display" => $dta->service_display,
						  "origin_name" => $dta->origin_name,
						  "destination_name" => $dta->destination_name,
						  "price" => $dta->price,
						  "etd_from" => $dta->etd_from,
						  "etd_thru" => $dta->etd_thru,
						  "times" => $dta->times,
						);
					array_push($test, $arrx);
				}
			}

			$lowest = $test[0]['price'];
			//echo "Harga pertama : $lowest<br />";
			foreach($test as $dta) {
				if($dta['price'] <= $lowest) {
					$lowest = $dta['price'];
					$arrx = array(
					  "service_code" => $dta['service_code'],
					  "service_display" => $dta['service_display'],
					  "origin_name" => $dta['origin_name'],
					  "destination_name" => $dta['destination_name'],
					  "price" => $dta['price'],
					  "etd_from" => $dta['etd_from'],
					  "etd_thru" => $dta['etd_thru'],
					  "times" => $dta['times']
					);
				}
			}

			return $arrx;
	}

	function sendWelcomeEmail($resultInsert) {
		
		
		//$resultInsert = $this->paymentService->getNewMemberData($tokenno);
		if($resultInsert[0]->email == "" || $resultInsert[0]->email == null) {
			$arr = array("status" => "failed", "message" => "Email member kosong..");
		} else {
			$membername = str_replace(" ", "%20", $resultInsert[0]->membername);
			$sponsorname = str_replace(" ", "%20", $resultInsert[0]->sponsorname);
			$qry_str = "?login_username="."catur.bharata@gmail.com";
			$qry_str .= "&login_password="."asdqwe123";
			$qry_str .= "&template_id="."1";
			$qry_str .= "&mail_to=".$resultInsert[0]->email;
			$qry_str .= "&mail_subject="."Welcome%20to%20K-Link%20";
			$qry_str .= "&mail_from="."noreply@k-link.co.id";
			$qry_str .= "&mail_name="."fiwa";
			$qry_str .= "&email=".$resultInsert[0]->email;
			$qry_str .= "&username=".$membername;
			$qry_str .= "&password=".$resultInsert[0]->password;
			$qry_str .= "&id_number=".$resultInsert[0]->memberid;
			$qry_str .= "&sponsor_name=".$sponsorname;
			$qry_str .= "&sponsor_email=".$resultInsert[0]->sponsoremail;
			
			
			$comxxcx = "http://192.168.0.168/ecm/go.php/api/email".$qry_str;
			
			$curl = curl_init($comxxcx);
			// Set query data here with the URL
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        $content = curl_exec($curl);
			curl_close($curl);
			$arr = json_decode($content);
			if($arr[0]->status == "sent") {
				$arr = array("status" => $arr[0]->status, "message" => "Email sudah terkirim..");
			} else if($arr[0]->status == "invalid") {
				$arr = array("status" => $arr[0]->status, "message" => "Invalid data");
			} else {
				$arr = array("failed" => $arr[0]->status, "message" => "Email gagal dikirim");
			}
		}	
		return $arr;
		
		
	}

	function resendWelcomeEmail($resultInsert) {
		if($resultInsert[0]->email == "" || $resultInsert[0]->email == null) {
			$arr = array("status" => "failed", "message" => "Email member kosong..");
		} else {
			$membername = str_replace(" ", "%20", $resultInsert[0]->membername);
			$sponsorname = str_replace(" ", "%20", $resultInsert[0]->sponsorname);
			$qry_str = "?login_username="."catur.bharata@gmail.com";
			$qry_str .= "&login_password="."asdqwe123";
			$qry_str .= "&template_id="."1";
			$qry_str .= "&mail_to=".$resultInsert[0]->email;
			$qry_str .= "&mail_subject="."Welcome%20to%20K-Link%20";
			$qry_str .= "&mail_from="."noreply@k-link.co.id";
			$qry_str .= "&mail_name="."fiwa";
			$qry_str .= "&email=".$resultInsert[0]->email;
			$qry_str .= "&username=".$membername;
			$qry_str .= "&password=".$resultInsert[0]->password;
			$qry_str .= "&id_number=".$resultInsert[0]->memberid;
			$qry_str .= "&sponsor_name=".$sponsorname;
			$qry_str .= "&sponsor_email=".$resultInsert[0]->sponsoremail;
			
			
			$comxxcx = "http://192.168.0.168/ecm/go.php/api/email".$qry_str;
			
			$curl = curl_init($comxxcx);
			// Set query data here with the URL
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        $content = curl_exec($curl);
			curl_close($curl);
			$arr = json_decode($content);
			if($arr[0]->status == "sent") {
				$arr = array("status" => $arr[0]->status, "message" => "Email sudah terkirim..");
			} else if($arr[0]->status == "invalid") {
				$arr = array("status" => $arr[0]->status, "message" => "Invalid data");
			} else if($arr[0]->status == "rejected") {
				$arr = array("failed" => $arr[0]->status, "message" => "Email gagal dikirim");
			} else {
				$arr = array("failed" => $arr[0]->status, "message" => "Pengiriman email masih dalam proses antrian");
			}
		}	
		return $arr;
	}	

    function callApi($api_type='', $api_activity='', $api_input=''){
        $data = array();
        $result = $this->http_post_form("https://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
    }
    
    function http_post_form($url,$data,$timeout=20){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);
        $result = curl_exec($ch); 
        $result = curl_error($ch) ? curl_error($ch) : $result;
        curl_close($ch);
        return $result;
    }

	/*function getMemberInfo($userid) {

	} */
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $db1;
	var $db2;
	var $db10;
	
	function __construct() {
		log_message('debug', "Model Class Initialized");
	    $this->db1 = $this->setDB(1); 
		$this->db2 = $this->setDB(2);
		$this->db3 = $this->setDB(3);
		$this->db4 = $this->setDB(4);
        $this->db5 = $this->setDB(5);
		$this->db6 = $this->setDB(6);
		$this->db7 = $this->setDB(7);
		$this->db8 = $this->setDB(8);
		$this->db9 = $this->setDB(9);
		$this->db10 = $this->setDB(10);
		$this->dateTime = date("Y-m-d h:m:s");
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->username = $this->session->userdata('ecom_user');
		
		//dev
		//$this->tbl_hdr = "ppob_trx_hdr1";
		//$this->tbl_det = "ppob_trx_det1";
		//prod
		$this->tbl_hdr = "ppob_trx_hdr";
		$this->tbl_det = "ppob_trx_det";
	} 
	
	function setDB($i) {
		if($i == 1) {
			return "db_ecommerce";
		} elseif ($i == 2) {
			return "klink_mlm2010";
		} elseif ($i == 3) {
			return "alternate2";
		} elseif ($i == 4) {
			return "tes_newera4";
		} elseif ($i == 5){
		  return "db_test";
		} elseif ($i == 6){
		  return "pdo_tes_newera4";
		} elseif ($i == 7){
		  return "data_mining";
		} elseif ($i == 8){
		  return "dblos";
		} elseif ($i == 9){
			return "MAINMEMBER";
		}elseif ($i == 10){
			return "db_mutasi";
		}
	}
	 
	/*function getRecordset($qry, $qryParam = null, $choose_db = 'default') {
		if($choose_db != 'default')	{
        	$this->db = $this->load->database($choose_db, true);

		}	
		$q = $this->db->conn_id->prepare($qry);
        $q->execute($qryParam);
        return $q->fetchAll(PDO::FETCH_CLASS);
	}
	
	function getRecordset($choose_db = 'digital_sign', $qry, $qryParam = null) {
		$nilai = null;	
		if($choose_db != 'digital_sign')	{
        	$this->db = $this->load->database($choose_db, true);
		}	
        $query = $this->db->query($qry, $qryParam); 
		if($query->num_rows() > 0)  {
            $nilai = $query->result();   
        } 
        return $nilai;  
	}*/
	
	function getRecordset($qry, $qryParam = null, $choose_db = 'db_ecommerce') {
		$nilai = null;	
		//if($choose_db != 'db_ecommerce')	{
        	$this->db = $this->load->database($choose_db, true);
		//}	
        $query = $this->db->query($qry, $qryParam); 
		if($query->num_rows() > 0)  {
            $nilai = $query->result();   
        } 
        return $nilai;  
	}

	function getRecordsetCache($qry, $qryParam = null, $choose_db = 'db_ecommerce') {
		$nilai = null;	
		//if($choose_db != 'db_ecommerce')	{
        	$this->db = $this->load->database($choose_db, true);
		//}	
        $this->db->cache_on();	
        $query = $this->db->query($qry, $qryParam);

         $this->db->cache_off();	
		if($query->num_rows() > 0)  {
            $nilai = $query->result();   
        } 
        return $nilai;  
	}
	
	function getDataResult($qry, $qryParam = null, $choose_db = 'db_ecommerce') {
		$nilai = null;	
		//if($choose_db != 'db_ecommerce')	{
        	$this->db = $this->load->database($choose_db, true);
		//}	
        $query = $this->db->query($qry, $qryParam); 
		if($query->num_rows() > 0)  {
            $nilai = $query->result();   
        } 
        return array("rowCount" => $query->num_rows(), "result" => $nilai);  
	}
	
	/*$arr = getDataResult();
	
	$arr['rowCount'];
	
	$arr['result'];*/
	
	function getRecordsetArray($qry, $qryParam = null, $choose_db = 'db_ecommerce') {
		$nilai = null;	
		//if($choose_db != 'db_ecommerce')	{
        	$this->db = $this->load->database($choose_db, true);
		//}	
        $query = $this->db->query($qry, $qryParam); 
		if($query->num_rows() > 0)  {
            $nilai = $query->result_array();   
        } 
        return $nilai;  
	}
    
    
	
	function getRecordset2($qry, $qryParam = null, $choose_db = 'db_ecommerce') {
		/*$nilai = null;	
		if($choose_db != 'db_ecommerce')	{
        	$this->db = $this->load->database($choose_db, true);
		}	
        $query = $this->db->query($qry, $qryParam); 
		if(!$query) {
			throw new Exception("Query Failed", 1);			
		} else {
			if($query->num_rows() > 0)  {
	            $nilai = $query->result();   
	        } 
		}
        return $nilai;  
		*/
	    $this->db = $this->load->database($choose_db, true);
	    $q = $this->db->conn_id->prepare($qry);
		$q->execute($qryParam);
		
		/*if($q) {
			throw new Exception("Query Failed", 1);			
		} else { */
		    $arr = array(
		        "rowCount" => $q->rowCount(),
		        "arrayData" => $q->fetchAll(PDO::FETCH_CLASS));
		//}		
	    return $arr;
	}
	
	function fetchRow($qry, $qryParam = null, $choose_db = 'db_ecommerce') {
		$this->db = $this->load->database($choose_db, true);
	    $q = $this->db->conn_id->prepare($qry);
		$q->execute($qryParam);
		
		/*if($q) {
			throw new Exception("Query Failed", 1);			
		} else { */
		    $arr = array(
		        "rowCount" => $q->rowCount(),
		        "arrayData" => $q->fetchAll(PDO::FETCH_ROW));
		//}		
	    return $arr;
	}
	
	function executeQuery($qry, $choose_db = 'db_ecommerce') {
		//if ($choose_db != 'default') {
	    $this->db = $this->load->database($choose_db, true);
	    //}
	    $query = $this->db->query($qry);
	    return $this->db->affected_rows();
	}
	
	function getRecordsetWithException($choose_db = 'digital_sign', $qry, $qryParam = null) {
		$nilai = null;	
		if($choose_db != 'digital_sign')	{
        	$this->db = $this->load->database($choose_db, true);
		}	
        $query = $this->db->query($qry, $qryParam); 
		if($query->num_rows() > 0)  {
            $nilai = $query->result();   
        } else {
        	throw new Exception("Empty Result from Database..!", 1);
			
        }
        return $nilai;  
	}
	
	function checkListPromo() {
		//$this->db = $this->load->database($this->db1);
		$tgl = date("Y-m-d");
		$qry = "SELECT id, max_discount, full_discount
				FROM promo_period a WHERE a.[status] = 1
				AND a.period_start <= '$tgl' AND a.period_end >= '$tgl'";	
		//echo $qry;			
        $res = $this->getRecordset($qry, NULL, $this->db1);
        /*$query = $this->db->query($qry);
        $res = array();
		foreach ($query->result() as $row) {
			array_push($res, $row);
		}*/
		return $res;
	}
	
	
    
    public function empty_record()
    {
       //echo "<div class=\"alert alert-error\"><p align=center>No record found</p></div>";
       echo "No record found";
    }
    
    public function set_error_message($message)
    {
       echo "<div class=\"alert alert-error\"><p align=center>".$message."</p></div>";
    }
    
    public function set_success_message($message)
    {
        echo "<div class=\"alert alert-success\"><p align=center>".$message."</p></div>";
        
    }
    
    public function json_set_error_message($message = "No record found")
    {
        $arr = array("response" => "false", "message" => $message);
        echo json_encode($arr);
    }
    
    public function json_set_success_message($message = "Success")
    {
        $arr = array("response" => "true", "message" => $message);
        echo json_encode($arr);
    }
    
    public function setTrueResponse($message = "OK")
    {
        $arr = array("response" => "true", "message" => $message);
        return $arr;
    } 
    
    public function setFalseResponse($message)
    {
        $arr = array("response" => "false", "message" => $message);
        return $arr;
    }
    
    public function setInvalidResponse($message = "Data already exist, please check again..!!") {
        $arr = array("response" => "invalid", "message" => $message);
        return $arr;
    }
	
	public function checkDataFromTable($param, $fromTable, $value, $exception = true) {
		$qry = "SELECT $param FROM $fromTable WHERE $param = '$value'";
		$res = $this->getRecordset($qry, null, $this->db1);
		if($res != null && $exception == true) {
			throw new Exception("Data related to table $fromTable, please check..!!", 1);
		}
		return $res;
	}
	
	
	public function deleteFromTable($table, $param, $value) {
		$qry = "DELETE FROM $table WHERE $param = '$value'";
		$res = $this->executeQuery($qry, $this->db1);
		return $res;
	}
	
	public function checkRecordFromTable($arr) {
		
		$qry = "SELECT $arr[fieldName] FROM $arr[tableName] WHERE $arr[fieldName] = '$arr[paramValue]'";
		$res = $this->getRecordset($qry, null, $arr['db']);
		if($res == null) {
			$errMsg = "Record with value $arr[paramValue] doesn't exist in table $arr[tableName]";
			if (array_key_exists("errMsg",$arr)) { 
				$errMsg = $arr['errMsg'];
			}
			throw new Exception($errMsg, 1);
		}
		return $res;
	}
	
	public function ifRecordExist($arr) {
		$res = false;
		$qry = "SELECT $arr[fieldName] FROM $arr[tableName] WHERE $arr[fieldName] = '$arr[paramValue]'";
		$res = $this->getRecordset($qry, null, $arr['db']);
		if($res != null) {
			$res = true;
		}
		return $res;
	}
    
    function getWHList(){
        $qry = "SELECT whcd,description FROM mswh";				
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
    }
    
	function getWHListPlus(){
        $qry = "SELECT whcd, whnm as description FROM master_warehouse_list group by whcd, whnm";				
		//$res = $this->getRecordset($qry, null, $this->db2);
		$res = $this->getRecordset($qry);
		return $res;
    }
    
    function getStkList(){
         $qry = "select distinct a.idstk,b.fullnm,b.addr1,b.addr2,b.tel_hm,b.tel_hp
                 from ecomm_trans_hdr a 
                 left join klink_mlm2010.dbo.mssc b on (a.idstk = b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS)
                 order by a.idstk asc";
         $res = $this->getRecordset($qry);
		return $res;
    }


	function SaveVAPaydet($data)
	{


		$this->db->trans_start();
		$this->db->insert('va_cust_pay_det',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			//if something went wrong, rollback everything
			$this->db->trans_rollback();
			return FALSE;
		} else {
			//if everything went right, delete the data from the database
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
	function getSaldoVa($idmember) {
		$qry = "SELECT novac, dfno, amount FROM va_cust_pay_bal a WHERE a.dfno = '$idmember'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
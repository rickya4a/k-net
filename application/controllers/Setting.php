<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_setting');
		$yuu=$this->session->userdata('menu');
		if(!isset($yuu))
		{
			header("Location: index.php");
		}
	}
	
	 function index()
	{
		$this->Add();
	}
//
//	 function View()
//	{
//		$data['main_view'] = 'master/setting/list_produk';
//		$data['add'] = anchor('produk/add','<i class="fa fa-plus"> </i> Add', array('class'=>'btn btn-primary'));
//
//		$this->load->view('index',$data);
//	}

	function Add()
	{
		$id=1;
		$data['main_view'] = 'master/setting/form_setting';
		$data['action'] = site_url('setting/add/'.$id);
		//$data['LIST_UOM'] = $this->m_setting->getDataUOM(); //for view data option select



		if($id != '')
		{
			$data['submit'] = 'Edit';

			$result = $this->m_setting->getDataEdit($id)->row();

			$data['RACK_CODE'] = $result->RACK_CODE;
			$data['RACK_NAME'] = $result->RACK_NAME;
			$data['MIN'] = $result->MIN;
			$data['MAX'] = $result->MAX;
			$data['IS_ACTIVE'] = $result->IS_ACTIVE;
			$data['UPDATEDT'] = $result->UPDATEDT;
			$data['UPDATENM'] = $result->UPDATENM;


		}else{
			$data['submit'] = 'Add';
		}

		$this->form_validation->set_rules('RACK_CODE','RACK_CODE','trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div>', '</div>');

		if($this->form_validation->run() == TRUE)
		{
			if($id == ''){
				$arr_data = array(
					'ID_PRODUCT'=>$this->uuid->v4(),
					'PRODUCT_CODE' => $this->input->post('PRODUCT_CODE'),
					'PRODUCT_NAME' => $this->input->post('PRODUCT_NAME'),
					'DESCRIPTION' => $this->input->post('DESCRIPTION'),
					'BERAT' => $this->input->post('BERAT'),
					'ID_UOM' => $this->input->post('ID_UOM'),

					'BOX' => $this->input->post('BOX'),
					'PALLETE' => $this->input->post('PALLETE'),
					'PIECE'=>'1',
					'IS_ACTIVE'=>'0',
					'CREATED_DATE' => date("Y-m-d") ,
					'CREATED_BY' => $this->session->userdata("NAME")
				);
				$this->m_setting->add($arr_data);
			}else{
				$arr_data = array(
					'RACK_CODE' => $this->input->post('RACK_CODE'),
					'RACK_NAME' => $this->input->post('RACK_NAME'),
					'MIN' => $this->input->post('MIN'),
					'MAX' => $this->input->post('MAX'),
					'IS_ACTIVE' => $this->input->post('IS_ACTIVE'),
					'UPDATEDT' => date("Y-m-d h:m:s"),
					'UPDATENM' => $this->session->userdata("NAME")

				);

				$this->m_setting->update($id,$arr_data);
			}

			redirect('setting');
		}
		$this->load->view('index',$data);
	}


	public function del($id)
	{
		$this->m_setting->del($id);
		redirect('produk');
	}
	function getTable()
	{
		if(count($this->input->post("order")) > 0){
			$order =  $this->input->post("order");
			$column_no = $order[0]['column'];
			$dir = $order[0]['dir'];
			$column = $this->input->post("columns");
			$field = $column[$column_no]['name'];
		}else{
			$dir = '';
			$field = '';
		}

		$search = $this->input->post('search');
		$s_value = $search['value'];

		$start = $this->input->post('start');

		$lenght = $this->input->post('length');

		$list = $this->m_setting->get_table_kw();
		$data = array();
		$no = $start+1;

		foreach ($list as $supplier) {
			$row = array();
			$row[] = $no++;
			$row[] = $supplier->PRODUCT_CODE;
			$row[] = $supplier->PRODUCT_NAME;
			$row[] = date("d-m-Y",strtotime($supplier->CREATED_DATE));


			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'produk/add/'.$supplier->ID_PRODUCT.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="'.base_url().'produk/del/'.$supplier->ID_PRODUCT.'" title="Hapus" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			$data[] = $row;
		}

		$output = array(

			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function check_user_availability()
	{
		$username   = $this->input->post('PRODUCT_CODE');
		$result     = $this->m_setting->get_all_usernames( $username );  #send the post variable to the model
		//value got from the get metho
		$user_name= $username;

		if( $result !=1){
			echo "1";
		}else{
			echo "0";
		}
	}

}
?>

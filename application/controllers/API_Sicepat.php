<?php defined('BASEPATH') OR EXIT('NO DIRECT ACCESS ALLOWED');

    class API_Sicepat extends MY_Controller
    {

        public function __construct()
        {

            parent::__construct();

            $this->sicepatkey = "525ce3d154e4f068a24fe213c671ab17";
        }

//$route['api/sicepat/origin/list'] = 'API_Sicepat/ListOrigin';
        public function ListOrigin()
        {

            //$api_key = "525ce3d154e4f068a24fe213c671ab17";
            $urlx = "http://api.sicepat.com/customer/origin?api-key=" . $this->sicepatkey;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlx,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $err = curl_error($curl);

            echo $response;
            //return $response;

//            $json = file_get_contents($urlx);
//            $data = json_decode($response, TRUE);
//            echo $data;
//            echo '<pre>';
//            print_r($data['sicepat']['results']);
//            echo '</pre>';
        }

//$route['api/sicepat/destination/list'] = 'API_Sicepat/ListDestination';
        public function ListDestination()
        {

            $urlx = "http://api.sicepat.com/customer/destination?api-key=" . $this->sicepatkey;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlx,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $err = curl_error($curl);

            echo $response;
        }

//$route['api/sicepat/tarif/(:any)/(:any)/(:any)'] = 'API_Sicepat/Tarif/$1/$2/$3';
        public function Tarif($origin, $destination, $weight)
        {

            $urlx = "http://api.sicepat.com/customer/tariff?api-key=" . $this->sicepatkey . "&origin=" . $origin . "&destination=" . $destination . "&weight=" . $weight;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlx,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $err = curl_error($curl);

            echo $response;
            //print_r($info);

//            $json = file_get_contents($urlx);
//            $data = json_decode($json, TRUE);
//            echo '<pre>';
//            print_r($data['sicepat']['results']);
//            echo '</pre>';

        }

//$route['api/sicepat/tracking/(:any)'] = 'API_Sicepat/Waybill/$1';
        public function Waybill($kode)
        {

            $urlx = "http://api.sicepat.com/customer/waybill?api-key=" . $this->sicepatkey . "&waybill=" . $kode;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlx,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            echo $response;


//            $json = file_get_contents($urlx);
//            $data = json_decode($json, TRUE);
//            echo '<pre>';
//            print_r($data['sicepat']['results']);
//            echo '</pre>';

        }

//$route['api/sicepat/getresi/(:any)'] = 'API_Sicepat/getResi/$1';
        public function getResi($kode){

            $urlx = "http://api.sicepat.com/customer/waybillNumber?api-key=".$this->sicepatkey."&orderId".$kode;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlx,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            echo $response;


//            $json = file_get_contents($urlx);
//            $data = json_decode($json, TRUE);
//            echo '<pre>';
//            print_r($data['sicepat']['results']);
//            echo '</pre>';
        }

    }
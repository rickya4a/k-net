<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class SendEmail extends MY_Controller {
	public function __construct() {
	    parent::__construct();
        $this->load->library('email');
        $this->load->model('backend/be_transaction_model', 'm_trans');
	}
	
	function sendEmailTest() {
			
		$char = "<html>
				<head>
				  <title>Pengiriman List Conot</title>
				</head>
				
				<body>";
		//$char .= "";
		$char .= "<table border=1><tr><td width='35%'>Nama Perusahaan</td><td >data->CP_NAME</td></tr>";
		$char .= "<tr><td>Nomor Account</td><td>data->NATIONAL_ACCOUNT</td></tr>";
		$char .= "<tr><td>Alamat Pick Up</td><td>data->ADDR</td></tr>";
		$char .= "<tr><td>PIC</td><td>data->PIC</td></tr>";
		$char .= "<tr><td>Telepon</td><td>data->TEL_NO</td></tr>";
		$char .= "<tr><td>Paket / Dokumen</td><td>data->PACKAGE</td></tr>";
		$char .= "<tr><td>Qty / Pcs</td><td>data->QTY</td></tr>";
		$char .= "<tr><td>Ukuran Paket</td><td>data->DIMENS</td></tr>";
		$char .= "<tr><td>Service ( SS, YES, REG )</td><td>data->SERVICE_TYPE</td></tr>";
		$char .= "<tr><td>Armada (mobil / motor)</td><td>data->PICKUP_VHC</td></tr>";
		$char .= "<tr><td>Jam Pick Up</td><td>data->PICKUP_TIME</td></tr>";
		$char .= "<tr><td>Kota Tujuan</td><td>data->DEST_CITY</td></tr>";
		$char .= "<tr><td>Keterangan lain-lain</td><td>data->REMARK</td></tr>";
		$char .= "</table><br />";
		//$char .= "<tr><td colspan=2>&nbsp;</td></tr>";
		$char .= "</body></html>";
			
			
		/*
		$this->load->library('email');
		
	    $config['protocol'] = 'smtp';
	    //$config['smtp_host'] = 'ssl://mail.k-link.co.id'; //change this
	    $config['smtp_host'] = 'ssl://smtp.gmail.com'; //change this
	    $config['smtp_port'] = '465';
	    //$config['smtp_user'] = 'pickup-admin@k-link.co.id'; //change this
	    //$config['smtp_pass'] = 'pickup123'; //change this
	    $config['smtp_user'] = 'knet.klink@gmail.com'; //change this
	    $config['smtp_pass'] = 'knet2015'; //change this
	    $config['mailtype'] = 'text'; 
	    $config['charset'] = 'utf-8';
	    //$config['charset'] = 'iso-8859-1';
	    $config['wordwrap'] = TRUE;
	    $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
	    */
	    $res = $this->m_trans->getConfigEmail('Req. Pickup');
		if($res > 0) {
			$sendTo = "";
			$sendCc = "";
			$sendBcc = "";
			
			$senderEmail = "";
			$senderPassword = "";
			$senderAlias = "";
			$senderSmtp = "";
			$senderSmtpPort = "";
			
			foreach($res as $data) {
				$flag_sender 	= $data->flag_sender;
				$flag_to 		= $data->flag_to;
				$flag_cc 		= $data->flag_cc;
				$flag_bcc 		= $data->flag_bcc;
				
				if($flag_sender == "1"){
					$senderEmail 	= $data->email;
					$senderPassword = $data->password;
					$senderAlias 	= $data->alias_name;
					$senderSmtp 	= $data->smtp;
					$senderSmtpPort = $data->smtp_port;
				}
				
				if($flag_to == "1"){
					$sendTo = $sendTo.$data->email.',';
				}
				
				if($flag_cc == "1"){
					$sendCc = $sendCc.$data->email.',';
				}
				
				if($flag_bcc == "1"){
					$sendBcc = $sendBcc.$data->email.',';
				}
			}
			$sendTo = substr($sendTo, 0, -1);
			$sendCc = substr($sendCc, 0, -1);
			$sendBcc = substr($sendBcc, 0, -1);
			
			echo "To nya adalah = $sendTo<br />";
			echo "CC nya adalah = $sendCc<br />";
			echo "BCC adalah = $sendBcc<br />";
			
			echo "Sender Email adalah $senderEmail<br />";
			echo "Sender Password adalah $senderPassword<br />";
			echo "Sender Alias adalah $senderAlias<br />";
			echo "Sender SMTP adalah $senderSmtp<br />";
			echo "Sender PORT adalah $senderSmtpPort<br />";
			
		    $config = Array(
						    'protocol' 	=> 'smtp',
						    'smtp_host' => $senderSmtp,
						    'smtp_port' => $senderSmtpPort,
						    'smtp_user' => $senderEmail,
						    'smtp_pass' => $senderPassword,
						    'mailtype'  => 'text', 
						    'charset'   => 'utf-8',
						    'wordwrap' 	=> TRUE,
						    'newline' 	=> '\r\n',
						);
			$this->load->library('email', $config);
			//var_dump($config);
		}

		$this->email->from($senderEmail, $senderAlias);
		$this->email->to($sendTo);
		$this->email->cc($sendCc);
		$this->email->bcc($sendBcc);
		$this->email->set_mailtype("html");
		$this->email->subject("List Pengiriman K-LINK");
		$this->email->message($char);
		
		if ($this->email->send()){
            echo "<div class='alert-success'>Email sent successfully..!!</div>"; 
            //$arr = $this->m_trans->updateFlagSendConote(data->ORDERNO);  
        }else{
            echo "<div class='alert-error'>There is error in sending mail!</div>";
			echo $this->email->print_debugger();
        } 
	}
    
    function tesFalconide(){
        $this->load->view('form_emailFalcon');
    }
    
    function submitSendEmail(){
        $form = $this->input->post(null,true);
        $from = $form['sender'];
        $fromname = $form['namamember'];
        $to = $form['penerimaEMail']; //Recipients list (semicolon separated)
        $api_key = "87d9af6eab922de72f919312ce9d3e4a";
        $subject = $form['subjectEmail'];
        //$content = welcomeEmail($form);
         $content = $form['isiEmail'];
        
        $data=array();
		//rawurlencode
        $data['subject']= $subject;                                                                       
        $data['fromname']= $fromname;                                                             
        $data['api_key'] = $api_key;
        $data['from'] = $from;
        $data['content']= $content;
        $data['recipients']= $to;
        $apiresult = $this->callApi(@$api_type,@$action,$data);
        echo trim($apiresult);
    }
	
	//$route['sendmail/(:any)'] = 'webshop/sendEmail/tesSendEmail/$1';
	//$route['sendmail/(:any)'] = 'sendEmail/tesSendEmail/$1';
	

	function tesSendEmail($orderno) {
		$no = substr($orderno, 0, 2);
		
		if($no == "RM" || $no == "RA") {
			$this->load->service("webshop/Payment_member_service",'payMemberService');
		    $resultInsMemb = $this->payMemberService->getNewMemberData($orderno);
			//print_r($resultInsMemb);
			sendWelcomeEmail($resultInsMemb);	
		} else if($no == "EC" || $no == "EA") {
			$this->load->service("webshop/Payment_service",'paymentService');
			$resultInsMemb = $this->paymentService->getInsertTrxOrderID($orderno);   
			//print_r($resultInsMemb);
			sendNotifSalesTransaction($resultInsMemb);
		}
		
	}
    
    function callApi($api_type='', $api_activity='', $api_input=''){
        $data = array();
        $result = $this->http_post_form("https://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
    }
    
    function http_post_form($url,$data,$timeout=20){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);
        $result = curl_exec($ch); 
        $result = curl_error($ch) ? curl_error($ch) : $result;
        curl_close($ch);
        return $result;
    }
	
	//$route['apiencrypt'] = 'sendEmail/encryptTokenXX';
	function encryptTokenXX() {
		/*$data = $this->input->post(NULL,TRUE);
		$this->load->library("aes");
		$plaintext = $data['username']."|".$data['password']."|1903201810";
        $password = $data['key'];
        
        $imputText = $plaintext;
		$imputKey = $password;
		$blockSize = 128;
		$aes = new AES($imputText, $imputKey, $blockSize);
		
		$enc = $aes->encrypt();
		$aes->setData($enc);
		$dec=$aes->decrypt();
		echo "plaintext : ".$plaintext."<br/>";
		echo "username : ".$data['username']."<br/>";
		echo "password : ".$data['password']."<br/>";
		echo "key : ".$data['key']."<br/>";
		echo "After encryption: ".$enc."<br/>";
		echo "After decryption: ".$dec."<br/>";*/
		
			
		
		
		
		$arr = array(
		  'inputString' => '2370|klinkTest123|2018032118',
		  'encryptKey' =>  'ThisTestKey'
		);
		$data = setFieldPost($arr);
        
		 $urlx = "https://api.xl.co.id:443/digitalreward/v1/encrypt"; 
		 //$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
		 $curl = curl_init();
  
		curl_setopt_array($curl, array(
		  CURLOPT_PORT => "443",
		  CURLOPT_URL => $urlx,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($arr),
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    "channel: MyXL",
		    "request-id: abc123",
		  ),
		));
		
		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		  echo "<br />";
		  echo "<pre>";
		  print_r($info);
		  echo "</pre>";
		} else {
		  echo $response;
		}
		
		
		//ganti start dan end date sesuai data post yg diinginkan

		 //print_r($curl);
		

       //echo "$base64Encrypted";
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_user');

		$yuu=$this->session->userdata('menu');
		if(!isset($yuu))
		{
			header("Location: index.php");
		}
	}
	
	 function index()
	{
		$this->View();
	}
	public function del($id)
	{
		$this->m_user->del($id);
		redirect('user');
	}

	 function View()
	{
		$data['main_view'] = 'master/user/list_user';
		$data['add'] = anchor('user/add','<i class="fa fa-plus"> </i> Add', array('class'=>'btn btn-primary'));

		$this->load->view('index',$data);
	}

	function Add($id = '')
	{
		$data['main_view'] = 'master/user/form_user';
		$data['action'] = site_url('user/add/'.$id);
		$data['LIST_ROLE'] = $this->m_user->getDataRole(); //for view data option select
		$data['LIST_WAREHOUSE'] = $this->m_user->getDataWarehouse(); //for view data option select

		if($id != '')
		{
			$data['submit'] = 'Edit';

			$result = $this->m_user->getDataEdit($id)->row();

			$data['USERNAME'] = $result->USERNAME;
			$data['EMAIL'] = $result->EMAIL;
			$data['ID_ROLE'] = $result->ID_USERROLE;
			$data['NAME'] = $result->NAME;
			$data['ID_WAREHOUSE'] = $result->ID_WAREHOUSE;



		}else{
			$data['submit'] = 'Add';
		}

		$this->form_validation->set_rules('USERNAME','USERNAME','trim|required|xss_clean');

		$this->form_validation->set_error_delimiters('<div>', '</div>');

		if($this->form_validation->run() == TRUE)
		{
			if($id == ''){
				$arr_data = array(

					'ID_USER' => $this->uuid->v4(),
					'USERNAME' => $this->input->post('USERNAME'),
					'PASSWORD' => hash ( "sha256", $this->input->post('password') ),
					'EMAIL' => $this->input->post('email'),
					'ID_USERROLE' => $this->input->post('ID_ROLE'),
					'ID_WAREHOUSE' => $this->input->post('ID_WAREHOUSE'),
					'NAME' => $this->input->post('NAME'),

					'IS_ACTIVE'=>'0',
					'CREATED_DATE' => date("Y-m-d") ,
					'CREATED_BY' => "SYSTEM"

				);

				$this->m_user->add($arr_data);
			}else{
				$arr_data = array(
					'USERNAME' => $this->input->post('USERNAME'),
					'PASSWORD' => hash ( "sha256", $this->input->post('password') ),
					'EMAIL' => $this->input->post('email'),
					'ID_USERROLE' => $this->input->post('ID_ROLE'),
					'ID_WAREHOUSE' => $this->input->post('ID_WAREHOUSE'),
					'NAME' => $this->input->post('NAME'),

					'IS_ACTIVE'=>'0',
					'UPDATE_DATE' => date("Y-m-d") ,
					'UPDATE_BY' => $this->session->userdata("NAME")

				);

				$this->m_user->update($id,$arr_data);
			}

			redirect('user');
		}
		$this->load->view('index',$data);
	}


	function getTable()
	{
		if(count($this->input->post("order")) > 0){
			$order =  $this->input->post("order");
			$column_no = $order[0]['column'];
			$dir = $order[0]['dir'];
			$column = $this->input->post("columns");
			$field = $column[$column_no]['name'];
		}else{
			$dir = '';
			$field = '';
		}

		$search = $this->input->post('search');
		$s_value = $search['value'];

		$start = $this->input->post('start');

		$lenght = $this->input->post('length');

		$list = $this->m_user->get_table_kw();
		$data = array();
		$no = $start+1;

		foreach ($list as $supplier) {
			$row = array();
			$row[] = $no++;
			$row[] = $supplier->NAME;
			$row[] = $supplier->EMAIL;


			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'user/add/'.$supplier->ID_USER.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="'.base_url().'user/del/'.$supplier->ID_USER.'" title="Hapus" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			$data[] = $row;
		}

		$output = array(

			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}


	function check_user_availability()
	{
		$username   = $this->input->post('USERNAME');
		$result     = $this->m_user->get_all_usernames( $username );  #send the post variable to the model
		//value got from the get metho
		$user_name= $username;

		if( $result !=1){
			echo "1";
		}else{
			echo "0";
		}
	}

}
?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Summary extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_summary');
		$this->load->library('pdf_sum');
	//	$this->load->library('pdf_do_closing_in2');
		$yuu=$this->session->userdata('menu');
		if(!isset($yuu))
		{
			header("Location: index.php");
		}


	}

	 function index()
	{
		$this->View();
	}
	
	 function View()
	{
		$data['main_view'] = 'sales/summary/form_sum';
		$data['action'] = site_url('summary/cetak/');
		$data['LIST_WAREHOUSE'] = $this->m_summary->getDataWarehouse(); //for view data option select
		$this->load->view('index',$data);


	}


	function cetak()
	{
		//$warehouse =  $this->input->post('ID_WAREHOUSE');
		//$category =  $this->input->post('CATEGORY');


		define('FPDF_FONTPATH',$this->config->item('fonts_path'));
		$data_mst = array();
		$data_pdf=$this->m_summary->get_table_kw();
		//$data_pdf2=$this->m_summary->getDataDetail2($tgl_awal, $tgl_akhir);
		//$result = $this->m_summary->getNamaWarehouse($warehouse)->row();
		//$WAREHOUSE_NAME = $result->WAREHOUSE_NAME;

		$data_indent=array();

		//print_r($data_pdf);
		//$this->m_manualDO->getIndent($id);
		$options = array(
			'filename'  => 'GR-'.date('dmy').'.pdf', //nama file penyimpanan, kosongkan jika output ke browser
			'destinationfile'  => 'D' , //I=inline browser (default), F=local file, D=download
			'paper_size' =>'Letter' ,	//paper size: F4, A3, A4, A5, Letter, Legal
			'orientation' =>'P'  //orientation: P=portrait, L=landscape
		);



		foreach($data_pdf as $rows)
		{
			$data_pdf1[] = array(
				'ID_PRODUCT' => $rows->ID_PRODUCT,
				'PRODUCT_NAME' => $rows->PRODUCT_NAME,


				'QTY' => $rows->QTY,


			);
		}


		//$status =  $this->input->post('status');
		//if( $status==1)
			$tabel = new pdf_sum($data_mst,$data_pdf1,$data_indent,$options);
		//else
		//	$tabel = new pdf_do_closing_in2($data_mst,$data_pdf2,$data_indent,$options,$tgl_awal,$tgl_akhir);
		$tabel->printPDF('js');
	}




	function closing()
	{

		$tgl_awal =  date('Y-m-d',strtotime($this->input->post('tgl_awal')));
		$tgl_akhir =  date('Y-m-d',strtotime($this->input->post('tgl_akhir')));
		$status =  $this->input->post('status');


		echo 'keong';
		$this->cetak($tgl_awal,$tgl_akhir,$status);

	}


	function getPCS()
	{
		$ID_PRODUCT = $this->input->post('ID_PRODUCT');
		$qty2 = $this->input->post('qty2');

		$return = $this->m_stockout->getPCS($ID_PRODUCT)->row();

		$data['qtyPCS']= $qty2 * $return->BOX;




		if('IS_AJAX') {
			echo json_encode($data);
		}
	}

	function getNomorPO()
	{
		$JENIS_ORDER = $this->input->post('JENIS_ORDER');


		$data['NO_PO']= $this->my_counter->Counter2($JENIS_ORDER);

		if('IS_AJAX') {
			echo json_encode($data);
		}
	}


	function getTable()
	{
		if(count($this->input->post("order")) > 0){
			$order =  $this->input->post("order");
			$column_no = $order[0]['column'];
			$dir = $order[0]['dir'];
			$column = $this->input->post("columns");
			$field = $column[$column_no]['name'];
		}else{
			$dir = '';
			$field = '';
		}

		$search = $this->input->post('search');
		$ID_WAREHOUSE = $this->input->post('ID_WAREHOUSE');
		$CATEGORY = $this->input->post('CATEGORY');
		$s_value = $search['value'];

		$start = $this->input->post('start');

		$lenght = $this->input->post('length');

		$list = $this->m_summary->get_table_kw();
		$data = array();

		foreach ($list as $supplier) {
			$row = array();
			//$row[] = $no++;
			$row[] = $supplier->ID_PRODUCT;
			$row[] = $supplier->PRODUCT_NAME;

			$row[] = $supplier->QTY;

			//add html for action
			$data[] = $row;
		}
		$output = array(
			"data" => $data
		);
		//output to json format
		echo json_encode($output);
	}
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class menu extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$yuu=$this->session->userdata('menu');
		if(!isset($yuu))
		{
			header("Location: index.php");
		}

	}
	
	 function index()
	{
		$this->View();
	}
	
	 function View()
	{
		$data['main_view'] = 'master/menu/list_menu';
		$data['add'] = anchor('menu/add','<i class="fa fa-plus"> </i> Add', array('class'=>'btn btn-primary'));

		$this->load->view('index',$data);
	}

	function Add()
	{
		$data['main_view'] = 'master/menu/form_menu';

		$this->load->view('index',$data);
	}



}
?>
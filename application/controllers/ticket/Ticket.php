<?php
class Ticket extends CI_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->urlReturn = get_class($this);
        $this->load->model('ticket/Event_model', 'em');
    }

    //put your code here
    public function index() {
    	$data['event'] = $this->em->showEventPriceCat();
		 $this->session->unset_userdata('ticket');
        $this->load->view('ticket/include/header');
        $this->load->view('ticket/Ticket', $data);
    }
	
}
?>
<?php
class Payment extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->urlReturn = get_class($this);
        $this->load->model('ticket/Event_model','eventModel');
    }


    public function index() {
		$arr = $this->input->post(NULL, TRUE);
		$ticketSess = $this->session->userdata('ticket');
        $tipePay = array("tipePay" => $arr['tipepaymentt']);
        $xx = array_merge($ticketSess[0],$tipePay);
        //print_r($xx);
        $this->session->set_userdata('ticket',$xx);
        $dt['bookingNo'] = $this->eventModel->getBookingNo();
        
        $htm = $this->eventModel->getHTM($xx['ticket']);
		$xxx = $this->session->userdata('ticket');
        
        $dt['bank'] = $this->eventModel->getBank();
        
        
        if($xxx['tipePay'] == '0'){
            $jum = count($xxx['name']);
            $randno = $this->eventModel->randomInt(4);
            $dp = $jum * $htm[0]->price_tab_dp;
            $totDP = $dp + $randno;
            
            $dt['prePaymnet'] = $this->eventModel->setPrePayment($htm[0]->price_tab_dp,$dt['bookingNo'],$htm[0]->price_tab_id,$xxx,$totDP);
            $dt['dtReg'] = $this->eventModel->getDtRegisterPre($dt['bookingNo']);
            $dt['det'] = $this->eventModel->getDtDetailPre($dt['bookingNo']);
            $this->load->view('ticket/include/header');
            $this->load->view('ticket/payment_finish',$dt);
            $this->load->view('ticket/include/footer');
        }else{
            //key develop
            $dt['key'] = 'f9fc68f085a074cabf29382527c4cc07';
            
            //key production
            //$dt['key'] = '348ed50443f314e41acc971efc94cc3e';
            
            $jum = count($xxx['name']);
        
            $dt['tot_dp'] = $jum * $htm[0]->price_tab_dp;
    		if ($jum == 0){
    			redirect('ticketing');
                //echo "Please select Place or Quantity for ticket.";
    		}else{
    			$dt['prePaymnet'] = $this->eventModel->setPrePayment($htm[0]->price_tab_dp,$dt['bookingNo'],$htm[0]->price_tab_id,$xxx,$dt['tot_dp']);
                $this->load->view('ticket/include/header');
    			$this->load->view('ticket/'.$this->urlReturn,$dt);
    			$this->load->view('ticket/include/footer');
    		}
            /*$dt['msg'] = 'internet banking';
            $this->load->view('ticket/inputByTrf',$dt);*/
        }
    }
    
    function getPreview(){
        $dt = $this->input->post(NULL, TRUE);
        if($dt == null){
            redirect('ticketing');
        }else{
            $arr = array();
            array_push($arr, $dt);
            $this->session->set_userdata('ticket',$arr);
            $dt['ticketingg'] = $this->session->userdata('ticket');
            $id = $dt['ticketingg'][0]['ticket'];
            $dt['eventPlace'] = $this->eventModel->getEventPlace($id);
            
            if($dt['ticketingg'][0]['quantity'] == '0'){
                redirect('ticketing');
            }else{
                $this->load->view('ticket/include/header');
                $this->load->view('ticket/prevTicketing',$dt);
                $this->load->view('ticket/include/footer');
            }
            
        }
    }
    
    function getInquirySgo(){
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
        $abc = $this->eventModel->getAmtTiket($xx['order_id']);
        $amount = $abc[0]->dp;
        //$amount = 3500000;
        if(!$xx['order_id']){
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array('errorCode' => '0',
                             'errorDesc' => 'Success',
                             'orderId' => $xx['order_id'],
                             'amount' => $amount,
                             'ccy' => 'IDR',
                             'desc' => 'event K-Link',
                             'trx_date' => date('d/m/Y H:i:s')
                             );
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
    
    function getInquirySgoDev(){
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
        $abc = $this->eventModel->getAmtTiket($xx['order_id']);
        //$amount = $abc[0]->dp;
        $amount = 210000;
        if(!$xx['order_id']){
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array('errorCode' => '0',
                             'errorDesc' => 'Success',
                             'orderId' => $xx['order_id'],
                             'amount' => $amount,
                             'ccy' => 'IDR',
                             'desc' => 'event K-Link',
                             'trx_date' => date('d/m/Y H:i:s')
                             );
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
    
    
    function postPaymentTicketSgo(){
            $xx = $this->input->post(null,true);
            $password = 'k-net181183';
            //$totalDP = 3500000;
            
            $abc = $this->eventModel->getAmtTiket($xx['order_id']);
            $amount = $abc[0]->dp;
            
            if($xx['amount'] !=  $amount){   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
    
                $updtTabel = $this->eventModel->updTabel($xx['order_id'],$xx['amount']);
                
                $resSukses = array('success_flag' => 0,'error_message' => 'Success','reconcile_id' => rand(15,32),'order_id' => $xx['order_id'],'reconcile_datetime' =>date('d/m/Y H:i:s'));
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];   
            }
       }
    
    function postPaymentTicketSgoDev()
    {
            $xx = $this->input->post(null,true);
            $password = 'k-net181183';
            //$totalDP = 3500000;
            
            $abc = $this->eventModel->getAmtTiket($xx['order_id']);
            $amount = $abc[0]->dp;
            
            if($xx['amount'] !=  $amount){   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
    
                $updtTabel = $this->eventModel->updTabel($xx['order_id'],$xx['amount']);
                
                $resSukses = array('success_flag' => 0,'error_message' => 'Success','reconcile_id' => rand(15,32),'order_id' => $xx['order_id'],'reconcile_datetime' =>date('d/m/Y H:i:s'));
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];   
            }
    }
    
    function finishPaymentTicket($bookingNo)
    //function finishPaymentTicket()
    {
        $alternate2 = $this->load->database('alternate2', true);
        //if($this->_checkSessionStoreUser()) {
            $dt['dtReg'] = $this->eventModel->getDtRegister($bookingNo);
            $dt['det'] = $this->eventModel->getDtDetail($bookingNo);
            if($dt['dtReg'] != null && $dt['dtReg'] != null)
            {
                $sendSms = $this->sendSmsTiket($dt['det'],$dt['dtReg']);
                
                $this->load->view('ticket/include/header');
                $this->load->view('ticket/payment_finish',$dt);
                $this->load->view('ticket/include/footer');
            }else{
                $this->load->view('ticket/include/header');
                $this->load->view('ticket/payment_unfinish',$dt);
                $this->load->view('ticket/include/footer');
            }
       // }
    }
    
    function sendSmsTiket($det,$head){
        $alternate2 = $this->load->database('alternate2', true);
        foreach($head as $row){
            $bookNo = $row->bookingNo;
            $telhp = $row->trans_hdr_hp;
            $nmmember = $row->trans_hdr_name;
            $email = $row->trans_hdr_email;
            $eventPlace = $row->event_address;
            $eventDesc = $row->event_desc;
            $eventDate = date("d/m/Y H:i", strtotime($row->event_date));
			$eventPlace2 = $eventDesc.", date ".$eventDate; //->format('d/m/Y H:i');
        }
     /*
	  
	    $text = 'K-LINK EVENT YOGA: Booking Number: '.$bookNo.',Nama:'.$nmmember.',Email: '.$email.',Tempat : '.$eventPlace2.'';
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID) 
    		                    VALUES ('".$telhp."','".$text."','K-LINK Event Yoga', 'MyPhone0')";
        $query = $alternate2->query($insSms);
	 
	  */
        
        foreach($det as $list){
            $tiket = $list->trans_tiket_no;
            $telhape = $list->trans_det_hp;
            $nmmember = $list->trans_det_name;
            
            $textDet = 'K-LINK EVENT YOGA: Ticket Number: '.$tiket.',Nama:'.$nmmember.',Tempat : '.$eventPlace2.'';
            
            $insDet = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
	                    VALUES ('".$telhape."','".$textDet."','K-LINK Event Yoga')";
            //echo $insDet;
            $queryDet = $alternate2->query($insDet);
        }
    }
    
    function printPDFTicket(){
        $bookno = $this->input->post('bookno');
        $tipe = $this->input->post('paytipe');
        
        if($tipe == '1'){
            $dt['head'] = $this->eventModel->getDtRegister($bookno);
            $dt['det'] = $this->eventModel->getDtDetail($bookno);
        }else{
            $dt['head'] = $this->eventModel->getDtRegisterPre($bookno);
            $dt['det'] = $this->eventModel->getDtDetailPre($bookno);
        }
        
        
        if($dt['head'] != null && $dt['det'] != null){
            $this->load->view('ticket/ticket_pdf',$dt);
        }
    }
    
    function testSMS(){
        $alternate2 = $this->load->database('alternate2', true);
        //$text = 'K-LINK EVENT YOGA: Book No: '.$bookNo.',Nama:'.$nmmember.',Email: '.$email.',Tempat : '.$eventPlace.'';
                
                
                $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('087780441874','fhdghd','K-LINK Event Yoga')";
                
		        $query = $alternate2->query($insSms);
    }
    
    function testLoopp(){
        $xx = $this->eventModel->getloopp();
    }
    
    function testRandomInt(){
        $ee = $this->eventModel->randomInt(4);
        echo "test random ".$ee;
    }
    function tesSession(){
        $abc = $this->session->userdata('ticket');
        
        if($abc == null){
            echo "No Data";
        }else{
            print_r($abc);
            echo "<br>";
            echo "header ".$abc[0]['quantity'];
            echo "<br>";
            
            /*for($i=0;$i < count($abc['name']);$i++){
                echo "nama pembeli $i ". $abc['name'][$i];
                echo "<br>";
            }*/
        }
    }
    
	
}
?>
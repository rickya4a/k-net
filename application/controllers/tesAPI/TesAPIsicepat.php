<?php

    class TesAPIsicepat extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->apikey = "525ce3d154e4f068a24fe213c671ab17";

            $this->folderView = 'webshop/tes_form/';

        }

//$route['sicepat/tracking'] = 'tesAPI/TesAPIsicepat/formInput';
        public function formInput(){

            //$data = getUserID();
            $username = getUserID();

            if($this->_checkSessionStoreUser()) {

                $this->load->model('webshop/shared_module', 'shared');
                $data['prodCat'] = $this->shared->getListCatProd();

                $this->setTempWebShop($this->folderView.'formInputTracking',$data);
            }

        }

//$route['api/sicepat/tracking'] = 'tesAPI/TesAPIsicepat/tracking';
        public function tracking(){

            $data = $this->input->post(null, true);
            //print_r($data);

            //$dt = "http://api.sicepat.com/customer/waybill?api-key=" . $this->apikey . "&waybill=" . $data['noresi'];
            $dt = $this->trackingSicepat($data['noresi']);
            //echo $dt;
            //$json = file_get_contents($dt);
            $dt2['result'] = json_decode($dt, TRUE);

//            echo '<pre>';
//            print_r($dt2);
//            echo '</pre>';

            $this->load->view($this->folderView.'tampilTracking', $dt2);

        }

        public function formInputTarif(){

            //$data = getUserID();
            $username = getUserID();

            if($this->_checkSessionStoreUser()) {

                $this->load->model('webshop/shared_module', 'shared');
                $data['prodCat'] = $this->shared->getListCatProd();

                $this->setTempWebShop($this->folderView.'formInputTarif',$data);
            }
        }

        public function tarif(){

            $data = $this->input->post(null, true);

            $dt = $this->tarifSicepat($data['origin'], $data['destination'], $data['weight']);

            $dt2['result'] = json_decode($dt, TRUE);

            $this->load->view($this->folderView.'tampilTarif', $dt2);

        }

    }
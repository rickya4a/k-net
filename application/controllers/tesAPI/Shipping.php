<?php

    class Shipping extends MY_Controller{

        private $api_key = 'bf108afa7df5a1e144ebb2ab22905270';

        public function index(){

            $curl = curl_init();

            curl_setopt_array($curl, array(

                CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURLOPT_HTTP_VERSION,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key : $this->api_key"
                ),

            ));

            $response = curl_exec($curl);
            $err = curl_errno($curl);

            curl_close($curl);

            if($err){

                echo "cURL Error #:" . $err;
            }else{

                echo $response;
            }
        }
    }
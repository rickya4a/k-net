<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    public function __construct() {
	    parent::__construct();
		//$this->load->model("webshop/Product_model",'productM');
		$this->load->service("webshop/Product_service",'productService');
        $this->load->service("webshop/Login_service",'loginService');
		$this->folderPrd = base_url()."assets/images/products/thumb/";
		$this->folderView = "webshop/sales_non_member/";
	}

    public function index() {
        $this->template->load('template', 'dashboard1');
    }

    function dashboard3() {
        $this->template->load('template', 'dashboard3');
    }

    function form_general() {
        $this->template->load('template', 'general_form');
    }

    function form_advanced() {
        $this->template->load('template', 'form_advanced');
    }

    function form_validation() {
        $this->template->load('template', 'form_validation');
    }

    function form_wizard() {
        $this->template->load('template', 'form_wizard');
    }

    function form_upload() {
        $this->template->load('template', 'form_upload');
    }

    function form_button() {
        $this->template->load('template', 'form_button');
    }

    function table() {
        $this->template->load('template', 'table');
    }

    function table_dynamic() {
        $this->template->load('template', 'table_dynamic');
    }
    
    function contact() {
        $this->template->load('template', 'contact');
    }
	
	function error404(){
		$arr = jsonFalseResponse("Page/Routing not found");
		echo json_encode($arr);
    }
    
    function tesDion() {
        $dt['prdnm'] = $this->input->post("name");
            $dt['formAction'] = "".site_url('cart/addtocart')."";
            //$dt['banner'] = $this->productService->getHeaderBanner();
           // $dt['prod'] = $this->productService->getProductShop();
            $dt['prodCat'] = $this->productService->getProductCategoryCache();
             $dt['bannerCache'] = $this->productService->getHeaderBannerCache();
             $dt['prodCache'] = $this->productService->getProductShopCache();


            $dt['prodv'] = $this -> productService -> getProductShopVera();
            $dt['prodjhon'] = $this -> productService -> getProductShopVera1();

            $dt['prod1'] = $this -> productService -> getProductShopVeraD1();
            $dt['prod2'] = $this -> productService -> getProductShopVeraD2();
            $dt['prod3'] = $this -> productService -> getProductShopVeraD3();


            $dt['p1'] = $this -> productService -> getProductShopVeraP1();
            $dt['p2'] = $this -> productService -> getProductShopVeraP2();
            $dt['p3'] = $this -> productService -> getProductShopVeraP3();
            $dt['p4'] = $this -> productService -> getProductShopVeraP4();
            $dt['p5'] = $this -> productService -> getProductShopVeraP5();
            $dt['p6'] = $this -> productService -> getProductShopVeraP6();
            $dt['p7'] = $this -> productService -> getProductShopVeraP7();


            $dt['T1'] = $this -> productService -> getProductShopVeraT1();
            $dt['T2'] = $this -> productService -> getProductShopVeraT2();

            $dt['folderPrd'] = $this->folderPrd;
			//echo $this->folderPrd;
            $this->session->unset_userdata('sales_lp');
            $username = getUserID();
            $this->setTemplateDevSGO('webshop/shopDev', $dt);
            //echo "ok";
            
            /*if(!isset($username) || $username == "") {
                //$this->setTempWebShop('webshop/shop_home_guest', $dt);
                //$this->setTempWebShop($this->folderView.'shop_home_guest1', $dt);
                $this->session->sess_destroy();
                $this->setTempWebShop22($this->folderView.'shop_home_guest1_iman', $dt);
                //$this->setTempWebShop22($this->folderView.'shop_home_guest1', $dt);
            } else {
                //$this->setTempWebShop('webshop/shop_home', $dt);
                if($username == "IDSPAAA66834") {
                    //$this->setTempWebShop22('webshop/shop_home_iman', $dt);
                    $this->setTemplateDevSGO('webshop/shop_home_iman_jhondoe1', $dt);
                } else {
                    //$this->setTempWebShop22('webshop/shop_home_iman', $dt);
                    $this->setTempWebShop('webshop/shop_home_iman', $dt);
                }

            }*/
    }

}

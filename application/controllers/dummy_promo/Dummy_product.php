<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dummy_product extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model("webshop/Product_model",'productM');
        $this->load->service("webshop/Product_service",'productService');
        $this->folderPrd = base_url()."assets/images/products/thumb/";
        $this->folderView = "webshop/sales_non_member/";
    }

    //$route['shop/product'] = 'webshop/product/getProduct';
    function getProduct() {
        $dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['banner'] = $this->productService->getHeaderBanner();
        $dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['folderPrd'] = $this->folderPrd;
        $this->session->unset_userdata('sales_lp');
        $username = getUserID();
        //echo "ok";
        if(!isset($username) || $username == "") {
            //$this->setTempWebShopNonmember('webshop/shop_home_guest', $dt);
            //$this->setTempWebShopNonmember($this->folderView.'shop_home_guest1', $dt);
            $this->setTempWebShopNonmember($this->folderView.'shop_home_guest1_iman', $dt);
        } else {
            //$this->setTempWebShopNonmember('webshop/shop_home', $dt);
            if($username == "IDSPAAA66834") {
                //$this->setTempWebShopNonmember22('webshop/shop_home_iman', $dt);
                $this->setTemplateDevSGO('nonmember_promo/shop_home_iman', $dt);
            } else {
                $this->setTempWebShopNonmember('nonmember_promo/shop_home_iman', $dt);
            }

        }

    }

    //$route['shop/productdev'] = 'product/getProductDev';
    function getProductDev() {
        $dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['banner'] = $this->productService->getHeaderBanner();
        $dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->session->unset_userdata('sales_lp');
        $dt['folderPrd'] = $this->folderPrd;
        $username = getUserID();
        /*if(!isset($username) || $username == "") {
            //$this->setTempWebShopNonmember('webshop/shop_home_guest', $dt);
            $this->setTempWebShopNonmember($this->folderView.'shop_home_guest1', $dt);
        } else {*/
        //$this->setTempWebShopNonmember22('webshop/shop_home_iman', $dt);
        $this->setTempWebShopNonmember($this->folderView.'shop_home_guest1_iman', $dt);
        //}

    }

    //$route['shop/product2'] = 'product/getProduct2';
    function getProduct2() {
        $dt['banner'] = $this->productService->getHeaderBanner();
        $dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['folderPrd'] = $this->folderPrd;
        $this->setTempWebShopNonmember('webshop/shop_home', $dt);
    }

    //$route['shop/product/cat'] = 'webshop/product/getProductCategory';
    function getProductCategory() {
        //if($this->_checkSessionStoreUser())
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['folderPrd'] = $this->folderPrd;
        $this->setTempWebShopNonmember('webshop/product_category',$dt);

    }

    //$route['shop/product/cat/id/(:any)'] = 'webshop/product/getProdByCat/$1';
    /*function getProdByCat($id) {
         //if($this->_checkSessionStoreUser())


        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $data['totData']= $this->productService->getCountProduct();

        $config['base_url'] = site_url("shop/product/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';

        $this->ajax_pagination->initialize($config);

        $dt['halaman'] = $this->ajax_pagination->create_links();

        $data['per_page'] = $config['per_page'];


        //echo "jumlah ".$data['totData']['jml']."<br>";
        $dt['prodCat'] = $this->productService->getProdByCat($id,$limit,$this->uri->segment(3));

         $this->load->view('webshop/prodByCat',$dt);
    } */

    //$route['nonmember/product/name'] = 'nonmember_promo/nonmember_product/getProdByName';
    function getProdByName() {
        /*$dt['prdnm'] = $this->input->post("name");
        //print_r($dt['prdnm']);
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        //print_r($dt['prod']);
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShopNonmember('webshop/shop_home', $dt);
        */
        $dt['banner'] = $this->productService->getHeaderBanner();
        $dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['folderPrd'] = $this->folderPrd;
        $username = getUserID();
        if(!isset($username) || $username == "") {
            //$this->setTempWebShopNonmember('webshop/shop_home_guest', $dt);
            $this->setTempWebShopNonmember($this->folderView.'shop_home_iman', $dt);
        } else {
            $this->setTempWebShopNonmember($this->folderView.'shop_home', $dt);
        }
    }
    function getProdByName2($ID) {
        /*$dt['prdnm'] = $this->input->post("name");
        //print_r($dt['prdnm']);
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        //print_r($dt['prod']);
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShopNonmember('webshop/shop_home', $dt);
        */
        $dt['banner'] = $this->productService->getHeaderBanner();
        $dt['prdnm'] = $ID;
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['folderPrd'] = $this->folderPrd;
        $username = getUserID();
        if(!isset($username) || $username == "") {
            //$this->setTempWebShopNonmember('webshop/shop_home_guest', $dt);
            $this->setTempWebShopNonmember($this->folderView.'shop_home_guest1', $dt);
        } else {
            $this->setTempWebShopNonmember($this->folderView.'shop_home', $dt);
        }
    }


    function getProdByCat($id) {
        //if($this->_checkSessionStoreUser())

        $limit = 9;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $data['totData']= $this->productService->getCountProductByID($id);

        $config['base_url'] = site_url("shop/product/cat/id/$id/");
        //$config['base_url'] = "tes";
        //$config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['total_rows'] =  $data['totData'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';*/
        $config['div'] = 'listprd';
        $config['param_prd'] = $id;

        $this->jquery_pagination->initialize($config);

        $dt['halaman'] = $this->jquery_pagination->create_links();

        $data['per_page'] = $config['per_page'];


        //echo "jumlah ".$data['totData']['jml']."<br>";
        //echo $limit;
        $dt['prodCat'] = $this->productService->getProdByCat($id,$limit,$this->uri->segment(3));
        $dt['folderPrd'] = $this->folderPrd;
        $username = getUserID();
        if(!isset($username) || $username == "") {
            $this->load->view('webshop/prodByCat_guest', $dt);
        } else {
            $this->load->view('webshop/prodByCat', $dt);
        }
        //$this->load->view('webshop/prodByCat',$dt);
    }

    function getProdByCat2($id, $offset) {
        //if($this->_checkSessionStoreUser())

        $limit = 9;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $data['totData']= $this->productService->getCountProductByID($id);

        $config['base_url'] = site_url("shop/product/cat/id/$id/");
        //$config['base_url'] = "tes";
        //$config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['total_rows'] =  $data['totData'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/
        $config['div'] = 'listprd';
        $config['param_prd'] = $id;

        $this->jquery_pagination->initialize($config);

        $dt['halaman'] = $this->jquery_pagination->create_links();

        $data['per_page'] = $config['per_page'];
        $dt['folderPrd'] = $this->folderPrd;

        //echo "jumlah ".$data['totData']['jml']."<br>";
        $dt['prodCat'] = $this->productService->getProdByCat($id,$limit, $offset);

        $username = getUserID();
        if(!isset($username) || $username == "") {
            $this->load->view('webshop/prodByCat_guest', $dt);
        } else {
            $this->load->view('webshop/prodByCat', $dt);
        }
    }

    function getProdByCatHeader($id){
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        //$data['totData']= $this->productService->getCountProduct();
        $data['totData']= $this->productService->getCountProductByID($id);
        /*$config['base_url'] = site_url("shop/productHeader/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/

        /*$this->pagination->initialize($config);

        $dt['halaman'] = $this->pagination->create_links();

        $data['per_page'] = $config['per_page'];*/

        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['listProd'] = $this->productService->getProdByCatHeader($id);
        $dt['bannerCat'] = $this->productService->getHeaderBannerCat();
        $dt['folderPrd'] = $this->folderPrd;
        //$this->setTempWebShopNonmember('webshop/product_category1',$dt);

        $username = getUserID();
        if(!isset($username) || $username == "") {
            $this->setTempWebShopNonmember('nonmember_promo/product_category1_guest', $dt);
        } else {
            $this->setTempWebShopNonmember('nonmember_promo/product_category1', $dt);
        }
    }

    //$route['shop/prodDet'] = 'webshop/product/getProdDetails';
    function getProdDetails($prdcd){
        $dt['prodDetail'] = $this->productService->getProductDetails($prdcd);
        //if($this->_checkSessionStoreUser())
        //$this->setTempWebShopNonmember('webshop/product_detail');
        $this->load->view('webshop/product_detail');
    }

    //$route['promo/listing'] = 'product/listPromo';
    public function listPromo() {
        $dt['month'] = date("m");
        $dt['year'] = date("Y");
        $dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['listPromo'] = $this->productService->getListCurrentPromo($dt['year'], $dt['month']);
        $this->setTempWebShopNonmember('webshop/promoListing', $dt);
    }

    //cahyono
    function getProdByCahyono($id){
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        //$data['totData']= $this->productService->getCountProduct();
        $data['totData']= $this->productService->getCountProductByID($id);
        /*$config['base_url'] = site_url("shop/productHeader/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/

        /*$this->pagination->initialize($config);

        $dt['halaman'] = $this->pagination->create_links();

        $data['per_page'] = $config['per_page'];*/

        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['listProd'] = $this->productService->getProdByCahyono($id);
        $dt['bannerCat'] = $this->productService->getHeaderBannerCat();
        $dt['folderPrd'] = $this->folderPrd;
        //$this->setTempWebShopNonmember('webshop/product_category1',$dt);

        $username = getUserID();
        if(!isset($username) || $username == "") {
            $this->setTempWebShopNonmember('webshop/product_category1_guest', $dt);
        } else {
            $this->setTempWebShopNonmember('webshop/product_category1', $dt);
        }
    }
    function getProdByCahyono1(){
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $id= $this->input->post("nm");
        //$data['totData']= $this->productService->getCountProduct();
        $data['totData']= $this->productService->getCountProductByID($id);
        /*$config['base_url'] = site_url("shop/productHeader/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/

        /*$this->pagination->initialize($config);

        $dt['halaman'] = $this->pagination->create_links();

        $data['per_page'] = $config['per_page'];*/

        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['listProd'] = $this->productService->getProdByCahyono($id);
        $dt['bannerCat'] = $this->productService->getHeaderBannerCat();
        $dt['folderPrd'] = $this->folderPrd;
        //$this->setTempWebShopNonmember('webshop/product_category1',$dt);

        $username = getUserID();
        if(empty($dt['listProd']['arrayData'])){
            redirect('http://www.k-net.co.id/');
        }
        else{
            if(!isset($username) || $username == "") {
                $this->setTempWebShopNonmember('webshop/product_category1_guest', $dt);
            } else {
                $this->setTempWebShopNonmember('webshop/product_category1', $dt);
            }
        }
    }

}
<?php

class Dummy_promo extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this -> load -> model("dtc/Mdtc_mbr", 'mdtc');
        //$this -> load -> service("webshop/Member_service", 'memberService');
        $this -> load -> service("webshop/Product_service", 'productService');
        $this -> load -> service("webshop/Login_service", 'loginService');
        $this->load->service("webshop/Cartshop_service",'cartshopService');
        $this -> folderPrd = base_url() . "assets/images/products/thumb/";

        //$this -> folderView = "dummy_promo/";
        $this -> folderView="webshop/sales_non_member/";

        //key development
        //$this->sgokey = "0df5835ee198d49944c372ead860c241";
        //key production
        $this->sgokey = "51edf5e8117da341a8be702d9bc18de5";

        // di jadikan constructor agar langsung di jalankan saat di akses dan bisa di akses ke semua fungction
    }

    //$route['mampirkak'] = 'nonmember_promo/nonmember_promo/home';
    public function home() {

        //		$data = $this->input->post(NULL, TRUE);
        //IDSPAAA91433
        $login = $this -> mdtc -> checkAuthLoginKnet();
        //		print_r($login);
        if ($login != null) {
            $telhp = getUserPhone();
            $email = getUserEmail();
            $this -> session -> set_userdata('store_info', $login);
            $this -> session -> set_userdata('pricecode', "12W3");

            //if(getUserID() == "IDSPAAA96407") {
            //print_r($promo);
            $promo = $this -> loginService -> checkListPromo();

            $this -> session -> set_userdata('promo', $promo);
            //}
        }

        $dt['prodCat'] = $this -> cartshopService -> getProductCategory();

        $dt['prdnm'] = $this -> input -> post("name");
        $dt['formAction'] = "" . site_url('cart/addtocart') . "";
        $dt['banner'] = $this -> productService -> getHeaderBanner();
        $dt['prod'] = $this -> productService -> getProductShopVera();
        $dt['prod1'] = $this -> productService -> getProductShopVeraD1();
        $dt['prod2'] = $this -> productService -> getProductShopVeraD2();
        $dt['prod3'] = $this -> productService -> getProductShopVeraD3();
        //$dt['prodCat'] = $this -> productService -> getProductCategory();
        $dt['folderPrd'] = $this -> folderPrd;

        $this -> setTempWebShopNonMember($this -> folderView.'shop_home_iman', $dt);

    }


    //$route['mampirkak/cart/list'] = 'nonmember_promo/nonmember_promo/listCart';
    public function listCart()
    {
        if($this->_checkSessionStoreUser()) {
            $dt['formAction'] = "".site_url('shipping/postShipping')."";
            $dt['prodCat'] = $this->cartshopService->getProductCategory();
            //$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
            $dt['bns'] = $this->cartshopService->getCurrentPeriod();
            //print_r($dt['bns']);
            $dt['cart'] = $this->cart->contents();
            $personal_info = $this->session->userdata('personal_info');
            if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAA96407") {
                //$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
                $dt['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
            } else {
                $dt['listCargo'] = $this->cartshopService->getListCargo();
            }


            //mendapatkan jam buka gosend
            $day = date('D');
            $now = new Datetime("now");
            $this->load->model('lokasi/M_location', 'M_location');

            //Instant
            $getInstant= $this->M_location->getSchedule($day,'Instant');
            $open_inst= $getInstant[0]->opened;
            $closed_inst= $getInstant[0]->closed;
            $closed_pay= $getInstant[0]->closed_pay;
            $ket_inst= $getInstant[0]->keterangan;

            $begintime1 = new DateTime($open_inst);
            $endtime1 = new DateTime($closed_inst);

            if($now >= $begintime1 && $now <= $endtime1){
                $dt['value1']='"1-"+rupiah1';
                $dt['option1']='"Instant "+instant_hours+" Rp"+ rupiah1';
            } else {
                $dt['value1']='""';
                //$dt['option1']='"Instant - TUTUP (Jam Pickup Barang: "'.$open_inst.'" WIB - "'.$closed_inst.'" WIB)"';
                $dt['option1']='"'.$ket_inst.'"';
            }

            //SameDay
            $getSame= $this->M_location->getSchedule($day,'SameDay');
            $open_same= $getSame[0]->opened;
            $closed_same= $getSame[0]->closed;
            $ket_same= $getSame[0]->keterangan;

            $begintime2 = new DateTime($open_same);
            $endtime2 = new DateTime($closed_same);

            if($now >= $begintime2 && $now <= $endtime2){
                $dt['value2']='"2-"+rupiah1';
                $dt['option2']='"SameDay "+sameday_hours+" Rp"+ rupiah1';
            } else {
                $dt['value2']='""';
                //$dt['option1']='"SameDay - TUTUP (Jam Pickup Barang: "'.$open_same.'" WIB - "'.$closed_same.'" WIB)"';
                $dt['option2']='"'.$ket_same.'"';
            }

            //cargo GOSEND
            $getCargo= $this->M_location->getCargo('4');
            $dt['info_html']=$getCargo[0]->info_html;
            $dt['open_gsend']= $getCargo[0]->open;
            $dt['closed_gsend']= $getCargo[0]->close;

            $dt['begintime'] = new DateTime($dt['open_gsend']);
            $dt['endtime'] = new DateTime($dt['closed_gsend']);

            $this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
            $dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();

            if($personal_info != null) {
                //ubah dion @ 21/05/2017
                if(array_key_exists("shipper", $personal_info)) {
                    if($personal_info['shipper'] == "1") {
                        $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
                        $dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
                    } else {
                        $dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb();
                        $dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);
                    }
                } else {
                    $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
                    $dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
                }


                //print_r($dt['shipping']);
                //echo "c";
                $this->setTempWebShopNonMember($this -> folderView.'shippingWithCartData2',$dt);
            } else {
                $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
                $res = $this->cartshopService->getAddressReferenceByID(getUserID());
                if($res != null) {
                    $personal_infox = array(
                        "provinsi" => $res[0]->provinsi,
                        "kota" => $res[0]->kota,
                        "kecamatan" => $res[0]->kecamatan,
                    );
                    $dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);
                    $this->cartshopService->setAutomaticPersonalInfo($res);
                    //print_r($dt['shipping'])
                    //echo "e";
                    $this->setTempWebShopNonMember($this -> folderView.'shippingAutoAddrReff',$dt);
                    //$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
                } else {
                    //echo "v";
                    $this->setTempWebShopNonMember($this -> folderView.'shipping2',$dt);
                }
            }
        }
    }

    //$route['mampirkak/cart/listDev'] = 'nonmember_promo/nonmember_promo/listCartDev';
    public function listCartDev()
    {
        if($this->_checkSessionStoreUser()) {
            $dt['formAction'] = "".site_url('shipping/postShipping')."";
            $dt['prodCat'] = $this->cartshopService->getProductCategory();
            //$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
            $dt['bns'] = $this->cartshopService->getCurrentPeriod();
            //print_r($dt['bns']);
            $dt['cart'] = $this->cart->contents();
            $personal_info = $this->session->userdata('personal_info');
            if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAA96407") {
                //$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
                $dt['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
            } else {
                $dt['listCargo'] = $this->cartshopService->getListCargo();
            }

            $this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
            $dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();

            if($personal_info != null) {
                //ubah dion @ 21/05/2017
                if(array_key_exists("shipper", $personal_info)) {
                    if($personal_info['shipper'] == "1") {
                        $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
                        $dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
                    } else {
                        $dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb();
                        $dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);
                    }
                } else {
                    $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
                    $dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
                }


                //print_r($dt['shipping']);
                //echo "c";
                $this->setTempWebShopNonMember($this -> folderView.'shippingWithCartData2Dev',$dt);
            } else {
                $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
                $res = $this->cartshopService->getAddressReferenceByID(getUserID());
                if($res != null) {
                    $personal_infox = array(
                        "provinsi" => $res[0]->provinsi,
                        "kota" => $res[0]->kota,
                        "kecamatan" => $res[0]->kecamatan,
                    );
                    $dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);
                    $this->cartshopService->setAutomaticPersonalInfo($res);
                    //print_r($dt['shipping'])
                    //echo "e";
                    $this->setTempWebShopNonMember($this -> folderView.'shippingAutoAddrReff',$dt);
                    //$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
                } else {
                    //echo "v";
                    $this->setTempWebShopNonMember($this -> folderView.'shipping2dev',$dt);
                }
            }
        }
    }

    //$route['mampirkak/checkout'] = 'nonmember_promo/nonmember_promo/CheckoutCart';
    function CheckoutCart() {
        if($this->_checkSessionStoreUser()) {
            $data = $this->input->post(NULL, TRUE);
            $personal_info = $this->session->userdata('personal_info');
            $dt['personal_info'] = $personal_info;
            $dt['cart'] = $this->cart->contents();
            //print_r($data);
            if($data != null) {
                if($data['sponsor'] == "input") {
                    $sponsorid = $data['sponsoridinput'];
                } else {
                    $sponsorid = $data['sponsoridpilih'];
                }

                $this->load->model('nonmember_promo/M_nonmember_promo', 'm_nonmember');
                $check = $this->m_nonmember->checkDoubleKtpAndCellPhone($data['tel_hp'], $data['idno']);
                if($check && $sponsorid != "") {
                    //echo $check;
                    //$eerr = $this->updateCartBeforeProceed();
                    $prdx = set_list_array_to_stringCart($dt['cart'], "id");
                    $berat = $this->cart->total_weight();
                    $promo_free_prd = $this->session->userdata('promo_free_prd');
                    $dt['promo_free_prd'] = $promo_free_prd;
                    $dt['berat_total'] = $berat + ($promo_free_prd['qty'] * $promo_free_prd['weight']);
                    $ship = $this->cartshopService->setShippingInfoNonMember($dt['berat_total']);
                    $dt['prodCat'] = $this->cartshopService->getProductCategory();

                    $dt['listBank'] = $this->cartshopService->getBank();
                    //production
                    //$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
                    //development 0df5835ee198d49944c372ead860c241
                    $dt['key'] = $this->sgokey;



                    $arrMember = array(
                        "noktp" => $data['idno'],
                        "membername" => $data['membername'],
                        "tgllhr" => $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'],
                        "sex" => $data['sex'],
                        "addr" => $data['addr1'],
                        "no_hp" => $data['tel_hp'],
                        "email" => $data['memb_email'],
                        "sponsorid" => $sponsorid,
                        "sponsorname" => $data['sponsorname'],
                        "recruiterid" => $sponsorid,
                        "rekruitername" => $data['sponsorname'],
                    );

                    $this->session->set_userdata('new_member_reg', $arrMember);

                    $dt['new_member_reg'] = $this->session->userdata('new_member_reg');
                    if($personal_info['delivery'] == "2" && $ship != 1 && $ship == null) {
                        echo "ship : ".$ship;
                    } else {
                        //$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
                        //$this->setTemplateDevSGO('webshop/payment_sgo',$dt);
                        $this->setTempWebShopNonMember($this->folderView . 'payment_sgo',$dt);
                    }
                } else {

                    $arrMember = array(
                        "noktp" => $data['idno'],
                        "membername" => $data['membername'],
                        "tgllhr" => $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'],
                        "sex" => $data['sex'],
                        "addr" => $data['addr1'],
                        "no_hp" => $data['tel_hp'],
                        "email" => $data['memb_email'],
                        "sponsorid" => $sponsorid,
                        "sponsorname" => $data['sponsorname'],
                        "recruiterid" => $sponsorid,
                        "rekruitername" => $data['sponsorname'],
                    );

                    $this->session->set_userdata('new_member_reg', $arrMember);


                    //$msg = $e->getMessage();
                    //echo "<script>alert('$msg')</script>";
                    redirect('mampirkak/register_membership');
                }

            } else {

                redirect('mampirkak/register_membership/back');
                //echo "ok";
            }


            /*} else {
                //$reg = $this->cartshopService->sessionRegPersonalInfo($data);
                //echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
                //redirect('cart/list');
                echo "<script>
                        window.location.href='list';
                        alert('Mohon data dilengkapi dahulu..');
                        </script>";
            }	*/
        }   else {
            redirect('mampirkak');
        }
    }



    //$route['mampirkak/checkout2'] = 'nonmember_promo/nonmember_promo/CheckoutCart2';
    function CheckoutCart2() {
        if($this->_checkSessionStoreUser()) {
            $data = $this->input->post(NULL, TRUE);
            $personal_info = $this->session->userdata('personal_info');
            $dt['personal_info'] = $personal_info;
            $dt['cart'] = $this->cart->contents();
            //print_r($data);
            if($data != null) {
                if ($data['isMember'] == 1) {
                    //echo $check;
                    //$eerr = $this->updateCartBeforeProceed();
                    $prdx = set_list_array_to_stringCart($dt['cart'], "id");
                    $berat = $this->cart->total_weight();
                    $promo_free_prd = $this->session->userdata('promo_free_prd');
                    $dt['promo_free_prd'] = $promo_free_prd;
                    $dt['berat_total'] = $berat + ($promo_free_prd['qty'] * $promo_free_prd['weight']);
                    $ship = $this->cartshopService->setShippingInfoNonMember($dt['berat_total']);
                    $dt['prodCat'] = $this->cartshopService->getProductCategory();
                    $dt['isMember']=$data['isMember'];
                    //$dt['listBank'] = $this->cartshopService->getBank();
                    $dt['listBank'] = $this->cartshopService->getBankNonVABaru();
                    //production
                    //$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
                    //development 0df5835ee198d49944c372ead860c241
                    $dt['key'] = $this->sgokey;


                    $arrMember = array(
                        "noktp" => $data['dfnoo'],
                        "membername" => $data['fullnmo'],
                        "tgllhr" => "",
                        "sex" => "",
                        "addr" => "",
                        "no_hp" => "",
                        "email" => "",
                        "sponsorid" => "IS_MEMBER",
                        "sponsorname" => "",
                        "recruiterid" => "",
                        "rekruitername" => "",
                    );

                    $this->session->set_userdata('new_member_reg', $arrMember);

                    $dt['new_member_reg'] = $this->session->userdata('new_member_reg');
                    if ($personal_info['delivery'] == "2" && $ship != 1 && $ship == null) {
                        //echo "ship : " . $ship;
                        echo "<script>
//						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
                    } else {
                        //$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
                        //$this->setTemplateDevSGO('webshop/payment_sgo',$dt);
                        $this->setTempWebShopNonMember($this->folderView . 'payment_sgo', $dt);
                    }

                }
                else
                {

                    if ($data['sponsor'] == "input") {
                        $sponsorid = $data['sponsoridinput'];
                    } else {
                        $sponsorid = $data['sponsoridpilih'];
                    }
                    if ($data['recruiter'] == "input") {
                        $recruiterid = $data['recruiteridinput'];
                    } else {
                        $recruiterid = $data['recruiteridpilih'];
                    }

                    $this->load->model('nonmember_promo/M_nonmember_promo', 'm_nonmember');
                    $check = $this->m_nonmember->checkDoubleKtpAndCellPhone($data['tel_hp'], $data['idno']);
                    if ($check && $sponsorid != "") {
                        //echo $check;
                        //$eerr = $this->updateCartBeforeProceed();
                        $prdx = set_list_array_to_stringCart($dt['cart'], "id");
                        $berat = $this->cart->total_weight();
                        $promo_free_prd = $this->session->userdata('promo_free_prd');
                        $dt['promo_free_prd'] = $promo_free_prd;
                        $dt['berat_total'] = $berat + ($promo_free_prd['qty'] * $promo_free_prd['weight']);
                        $ship = $this->cartshopService->setShippingInfoNonMember($dt['berat_total']);
                        $dt['prodCat'] = $this->cartshopService->getProductCategory();
                        $dt['isMember']=$data['isMember'];

                        //$dt['listBank'] = $this->cartshopService->getBank();
                        $dt['listBank'] = $this->cartshopService->getBankNonVABaru();
                        //production
                        //$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
                        //development 0df5835ee198d49944c372ead860c241
                        $dt['key'] = $this->sgokey;


                        $arrMember = array(
                            "noktp" => $data['idno'],
                            "membername" => $data['membername'],
                            "tgllhr" => $data['thnlhr'] . "-" . $data['blnlhr'] . "-" . $data['tgllhr'],
                            "sex" => $data['sex'],
                            "addr" => $data['addr1'],
                            "no_hp" => $data['tel_hp'],
                            "email" => $data['memb_email'],
                            "sponsorid" => $sponsorid,
                            "sponsorname" => $data['sponsorname'],
                            "recruiterid" => $recruiterid,
                            "rekruitername" => $data['recruitername'],
                        );

                        $this->session->set_userdata('new_member_reg', $arrMember);

                        $dt['new_member_reg'] = $this->session->userdata('new_member_reg');
                        if ($personal_info['delivery'] == "2" && $ship != 1 && $ship == null) {
                            //echo "ship : " . $ship;
                            echo "<script>
//						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
                        } else {
                            //$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
                            //$this->setTemplateDevSGO('webshop/payment_sgo',$dt);
                            $this->setTempWebShopNonMember($this->folderView . 'payment_sgo', $dt);
                        }


                    } else {

                        $arrMember = array(
                            "noktp" => $data['idno'],
                            "membername" => $data['membername'],
                            "tgllhr" => $data['thnlhr'] . "-" . $data['blnlhr'] . "-" . $data['tgllhr'],
                            "sex" => $data['sex'],
                            "addr" => $data['addr1'],
                            "no_hp" => $data['tel_hp'],
                            "email" => $data['memb_email'],
                            "sponsorid" => $sponsorid,
                            "sponsorname" => $data['sponsorname'],
                            "recruiterid" => $recruiterid,
                            "rekruitername" => $data['recruitername'],
                        );

                        $this->session->set_userdata('new_member_reg', $arrMember);


                        //$msg = $e->getMessage();
                        //echo "<script>alert('$msg')</script>";
                        redirect('mampirkak/register_membership');
                    }
                }
            } else {

                redirect('mampirkak/register_membership/back');
                //echo "ok";
            }


            /*} else {
                //$reg = $this->cartshopService->sessionRegPersonalInfo($data);
                //echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
                //redirect('cart/list');
                echo "<script>
                        window.location.href='list';
                        alert('Mohon data dilengkapi dahulu..');
                        </script>";
            }	*/
        }   else {
            redirect('mampirkak');
        }
    }



    function updateCartBeforeProceed() {

        if($this->_checkSessionStoreUser()) {
            $arr = jsonFalseResponse(NULL, "Update Cart gagal..!");
            $data = $this->input->post(NULL, TRUE);
            $upd = $this->cartshopService->updateCart($data);
            $this->session->set_userdata('pricecode', $data['pricecode']);
            if($upd) {
                $arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
            }
            //echo json_encode($arr);
            return $arr;
        }
    }

    //$route['mampirkak/saveTemp'] = 'nonmember_promo/nonmember_promo/saveTempTrx';
    function saveTempTrx() {
        $data = $this->input->post(NULL, TRUE);

        $this -> load -> model("nonmember_promo/M_nonmember_trx", 'm_nonmember_trx');
        $dt['payID'] = "EP".randomNumber(8);
        //$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID'], $data);

        $insTrxTemp = $this->m_nonmember_trx->insert_temp_trx_sgo($dt['payID'], $data);
        if($insTrxTemp > 0) {
            $this->load->model('webshop/shared_module', 'shared');
            $arr = $this->shared->getDataPaymentSGOByOrderIDDev($dt['payID']);
            if($arr != null) {
                $new_member_reg = $this->session->userdata("new_member_reg");
                $personal_info = $this->session->userdata("personal_info");
                if($new_member_reg['sponsorid'] != "IS_MEMBER") {
                    $arrMemb['sponsorid'] = $new_member_reg['sponsorid'];
                    $arrMemb['trx_id'] = $dt['payID'];
                    $arrMemb['membername'] = $new_member_reg['membername'];
                    $arrMemb['noktp'] = $new_member_reg['noktp'];
                    $arrMemb['no_hp'] = $new_member_reg['no_hp'];
                    $arrMemb['address'] = $new_member_reg['addr'];
                    $arrMemb['tgllhr'] = $new_member_reg['tgllhr'];
                    $arrMemb['email'] = $new_member_reg['email'];
                    $arrMemb['sex'] = $new_member_reg['sex'];
                    $arrMemb['idstk'] = $personal_info['stockist'];
                    $arrMemb['bank'] = "";
                    $arrMemb['no_rek'] = "";
                    $arrMemb['bank_acc_name'] = "";
                    $arrMemb['id_lp'] = $personal_info['id_lp'];
                    $arrMemb['flag_voucher'] = "0";
                    $arrMemb['voucherno'] = "";
                    $arrMemb['voucherkey'] = "";
                    $arrMemb['state'] = $personal_info['state'];
                    $arrMemb['rekruiterid'] = $new_member_reg['recruiterid'];
                    $arrMemb['prdcd'] = "X";

                    $insmemb = $this->m_nonmember_trx->insertEcommMembSgo($arrMemb);
                    if($insmemb > 0) {
                        $res = jsonTrueResponse($arr, "OK");
                    } else {
                        $res = jsonFalseResponse("Insert Member failed..");
                    }
                } else {
                    $res = jsonTrueResponse($arr, "OK");
                }
            } else {
                $res = jsonFalseResponse("Data Empty Result..");
            }
        } else {
            $res = jsonFalseResponse("Error requesting..");
        }

        echo json_encode($res);
    }

    //$route['mampirkak/finish/id/(:any)'] = 'nonmember_promo/nonmember_promo/finishMampirKak/$1';
    public function finishMampirKak($order_id) {
        $this->load->service("webshop/Knet_payment_service",'paymentService');
        $dta['trxInsertStatus'] = "fail";
        $dta['prodCat'] = $this->paymentService->getProductCategory();
        $dta['trans'] = $this->paymentService->getInsertTrxMampirKak($order_id);
        if ($dta['trans'] > 0 || $dta['trans'] != null) {
            $dta['trxInsertStatus'] = "ok";
            $dta['jne'] = $dta['trans'][0]->conoteJNE;
        } else {
            //$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
            $sgo = $this->paymentService->getDataPaymentSGOByOrderID_withVch($order_id);
            if($sgo[0]->bank_code_payment == "16") {
                $dta['trxInsertStatus'] = "pending";
            }
        }
        session_destroy();
        $this->setTempWebShopNonMember($this->folderView.'pay_ecommerce_result',$dta);
    }

    public function getProd(){
        $value=$this -> input -> post("prdcd");

        $getProd= $this -> productService -> getProdDet($value);
        /*$prdcd= $getProd[0]->prdcd;
        $prdnm= $getProd[0]->prdnm;
        $bv= $getProd[0]->bv;
        $price_e= $getProd[0]->price_e;
        $price_w= $getProd[0]->price_w;
        $price_cw= $getProd[0]->price_cw;
        $price_ce= $getProd[0]->price_ce;
        $weight=$getProd[0]->weight;*/

        if( $getProd != null){
            $prdcd= $getProd[0]->prdcd;
            $prdnm= $getProd[0]->prdnm;
            $bv= $getProd[0]->bv;
            $price_e= $getProd[0]->price_e;
            $price_w= $getProd[0]->price_w;
            $price_cw= $getProd[0]->price_cw;
            $price_ce= $getProd[0]->price_ce;
            $weight=$getProd[0]->weight;
        }else{
            $prdcd= "";
            $prdnm= "";
            $bv= "";
            $price_e= "";
            $price_w= "";
            $price_cw= "";
            $price_ce= "";
            $weight= "";
        }

        //print_r($getProd);

        if($prdcd != ''){
            $stat= "true";
            $arr = array("response" => $stat, "prdcd" => $prdcd,"prdnm" =>$prdnm,"bv"=>$bv,"price_e"=>$price_e,"price_w"=>$price_w,"price_cw"=>$price_cw,"price_ce"=>$price_ce,"weight"=>$weight);

        }else{
            $stat="false";
            $arr = array("response" => $stat, "message" =>"Silahkan cari ukuran/warna lain untuk produk K-Wrist Band..");

        }
        //$arr = array("response" => $stat, "prdcd" => $prdcd,"prdnm" =>$prdnm,"bv"=>$bv,"price_e"=>$price_e,"price_w"=>$price_w,"price_cw"=>$price_cw,"price_ce"=>$price_ce,"weight"=>$weight);
        echo json_encode($arr);
    }

    public function getProd1(){
        $value1=$this -> input -> post("prdcd1");
        $value2=$this -> input -> post("prdcd2");

        $getProd1= $this -> productService -> getProdDet($value1);
        if( $getProd1 != null){
            $prdcd1= $getProd1[0]->prdcd;
            $prdnm1= $getProd1[0]->prdnm;
            $bv1= $getProd1[0]->bv;
            $price_e1= $getProd1[0]->price_e;
            $price_w1= $getProd1[0]->price_w;
            $price_cw1= $getProd1[0]->price_cw;
            $price_ce1= $getProd1[0]->price_ce;
            $weight1=$getProd1[0]->weight;
        }else{
            $prdcd1= "";
            $prdnm1= "";
            $bv1= "";
            $price_e1= "";
            $price_w1= "";
            $price_cw1= "";
            $price_ce1= "";
            $weight1= "";
        }

        $getProd2= $this -> productService -> getProdDet($value2);
        if( $getProd2 != null){
            $prdcd2= $getProd2[0]->prdcd;
            $prdnm2= $getProd2[0]->prdnm;
            $bv2= $getProd2[0]->bv;
            $price_e2= $getProd2[0]->price_e;
            $price_w2= $getProd2[0]->price_w;
            $price_cw2= $getProd2[0]->price_cw;
            $price_ce2= $getProd2[0]->price_ce;
            $weight2=$getProd2[0]->weight;
        }else{
            $prdcd2= "";
            $prdnm2= "";
            $bv2= "";
            $price_e2= "";
            $price_w2= "";
            $price_cw2= "";
            $price_ce2= "";
            $weight2= "";
        }

        //print_r($getProd);

        if($prdcd1 != '' AND $prdcd2 !=''){
            $stat= "true";
            $arr = array("response" => $stat,
                "prdcd1" => $prdcd1,"prdnm1" =>$prdnm1,"bv1"=>$bv1,"price_e1"=>$price_e1,"price_w1"=>$price_w1,"price_cw1"=>$price_cw1,"price_ce1"=>$price_ce1,"weight1"=>$weight1,
                "prdcd2" => $prdcd2,"prdnm2" =>$prdnm2,"bv2"=>$bv2,"price_e2"=>$price_e2,"price_w2"=>$price_w2,"price_cw2"=>$price_cw2,"price_ce2"=>$price_ce2,"weight2"=>$weight2);
        }else{
            $stat="false";
            $arr = array("response" => $stat, "message" =>"Ukuran/warna K- Wrist Band kosong, silahkan pilih detail yang lain ..");

        }
        //$arr = array("response" => $stat, "prdcd" => $prdcd,"prdnm" =>$prdnm,"bv"=>$bv,"price_e"=>$price_e,"price_w"=>$price_w,"price_cw"=>$price_cw,"price_ce"=>$price_ce,"weight"=>$weight);
        echo json_encode($arr);
    }

}

<?php

class test_landing_page extends MY_Controller{

    public function index(){

//        $ambil = $this->my_counter->counterByFendi();
//        $this->load->view('landing-page/v_landing',$ambil);
        $this->load->model('M_landing_page');
        $this->load->view('landing-page/v_landing');
    }

    public function getNamaMemb()
    {
        $this->load->model('M_landing_page');

        $idmemb = $this->input->post('idmemb');

        //$return = $this->M_landing_page->getNamaMemb($idmemb);
        $dt['result'] = $this->M_landing_page->getNamaMemb($idmemb);

        //$dt['nama']=$return->nama;

        if('IS_AJAX') {
            echo json_encode($dt['result']);
        }

    }
}
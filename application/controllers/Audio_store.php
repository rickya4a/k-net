<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audio_store extends MY_Controller {

	public function __construct() {
		// Call the Model constructor
        parent::__construct();
		$this->usr_api = "k-net.apps";
		$this->pwd_api = "apps@2017";
		$this->salt = "knet";
		$this->dataForm = $this->input->post(NULL, TRUE);
		//SGO
		//key development
		//$this->sgokey = "0df5835ee198d49944c372ead860c241";
		//key production
		
		$this->sgokey = "51edf5e8117da341a8be702d9bc18de5";

	}
	
	public function xlStringKey() {
		echo date("Y-m-d H:i:s");
		/*$data = $this->getStringKeyXL();
		$key = $this->key_xl;
		$inpString = $this->adv_id . "|" . $this->pass_xl . "|" . date("YmdH");
		
		if(16 !== strlen($key)) $key = hash('MD5', $key, true);
		$padding = 16 - (strlen($data) % 16);
		$data .= str_repeat(chr($padding), $padding);
		$res = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16)));
		echo $res;
		echo "<br />";
		
		echo $inpString. " -- " .$key;
		echo "<br />";
		$res = $this->getXLStock2();
		echo $res; */
		
		$inpString = $this->adv_id . "|" . $this->pass_xl . "|" . date("YmdH");
		$arr = array(
			'inputString' => $inpString,
			'encryptKey' => $this->key_xl
		);
		$urlx = $this->xl_get_encrypt;
		//echo $urlx;
		//$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
		$curl = curl_init();
		//echo "<pre>";
		//print_r($arr);
		//echo "</pre>";
		curl_setopt_array($curl, array(
			CURLOPT_PORT => "8243",
			CURLOPT_URL => $urlx,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($arr),
			CURLOPT_HTTPHEADER => $this->headerXL,
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		/*echo "<pre>";
		print_r($arr);
		print_r($info);
		echo "</pre>";
		*/
		if ($err) {
            echo "Err when request encryptkey ";
			echo "<br />";
			echo "<pre>";
			echo "param : ";
			echo "<br />";
			print_r(json_encode($arr));
			echo "<br />";
			echo "error : ";
			echo "<br />";
			print_r($err);
			echo "<br />";
			echo "curl info : ";
			echo "<br />";
			print_r($info);
			echo "<br />";
			echo "response : ";
			echo "<br />";
			print_r($response);
			echo "</pre>";
			//print_r($err);
			//return 0;
		} else {
			//echo "dekripnya = ".$inpString."\n";
			//echo "enkrip key = ".$this->key_xl."\n";
			echo "<pre>";
			print_r($err);
			echo "<br />";
			print_r($info);
			echo "<br />";
			print_r($response);
			echo "</pre>";
			//curl -v -k https://112.215.105.53:8243/digitalreward/v1/encrypt -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Postman-Token: dc9a1b01-a655-49ed-b546-06e9fd1bb075' -H 'Channel: MyXL' -H 'Request-Id: 90812307518' -d '{"inputString": "2370|klinkTest123|2018111217","encryptKey": "ThisTestKey"}'
			//return $response;
		}

	}
	
	function apiAuthResponse($return) {
		$arr = array("response" => false, "message" => "Otentifikasi API gagal, silahkan sertakan user dan password API");
		if($return) {
			$arr = array("response" => true, "message" => "API Auth success");
		}
		return $arr;
	}
	
	function checkAuthAPI($data) {
		$return = false;
		if(array_key_exists("api_usr", $data) && array_key_exists("api_pwd", $data)) {
			if($data['api_usr'] == $this->usr_api && $data['api_pwd'] == $this->pwd_api) {
				$return = true;
			} 
		} 
		return $return;
	}
	
	function checkAuthAPI4($data) {
		$return = false;
		if(!array_key_exists("api_usr", $data) || !array_key_exists("api_pwd", $data)) {
			$arr = jsonFalseResponse("parameter api_usr dan api_pwd harus dikirim..");
			return $arr;
		}
		
		if($data['api_usr'] != $this->usr_api || $data['api_pwd'] != $this->pwd_api) {
			$arr = $this->apiAuthResponse(false);
			return $arr;
		}
		
		if(!array_key_exists("token", $data)) {
			$arr = jsonFalseResponse("parame token harus dikirim..");
			return $arr;
		}
		
		if(!array_key_exists("idmember", $data)) {
			$arr = jsonFalseResponse("param idmember harus dikirim..");
			return $arr;
		}
		
		$this->load->model('M_mobile_api', 'm_mobile_api');
		$mdRes = $this->m_mobile_api->getPasswordMember($data['idmember']);
		if($mdRes == null) {
			$arr = jsonFalseResponse("Invalid ID Member..");
			return $arr;
		} else if($mdRes != null && $mdRes[0]->password == null) {
			$arr = jsonFalseResponse("Password member tidak ada..");
			return $arr;
		} else {
			$password = $mdRes[0]->password;
			$stringP = $data['idmember']."::".$password;
			$this->load->library("decryptor");
			$key = "Kl1nkKn3t";
			//$token = "AgFE7sSNnknETGCKrAOBN6CX2yuujsmcxvh9EVDRALgIl8O+obCv2LdIVdXz6Zg8EV7ndMJnxPdL4QubEnJ8744kZfVvH7urVkoV2wToTX/MMA=="; // diisi token dari mobile
			try {
				$decryptor = new Decryptor;
				$plaintextDecrypted = $decryptor->decrypt($data['token'], $key);
				//error_reporting(0);
				//echo $plaintextDecrypted
				if($plaintextDecrypted == $stringP) {
					$dta = array("idmember" => $data['idmember']);
					$arr = jsonTrueResponse($dta, "token ok..");
				} else {
					$arr = jsonFalseResponse("token salah..");
				}
			} catch (Exception $e) {
				$arr = jsonFalseResponse("token salah..");
			}
			return $arr;
		}
	}
	
	//$route['api/audio/available/list'] = 'audio_store/availableAudioList';
	public function availableAudioList(){
		$data = $this->dataForm;
		$auth = $this->checkAuthAPI4($this->dataForm);
		if($auth['response'] == "false") {
			echo json_encode($auth);
			return;
		} 
		
		$page = null;
		$max_limit_show = null;
		if(array_key_exists('max_limit_show', $data)) {
			$max_limit_show = $data['max_limit_show'];
		}	
		
		if(array_key_exists('page', $data)) {
			$page = $data['page'];
		}
		
		$res = $this->listAvailableAudio($data['idmember'], $page, $max_limit_show);
		echo json_encode($res);
		
		
	}
	
	//$route['api/audio/mylist/add'] = 'audio_store/addMyAudioStore';
	public function addMyAudioStore() {
	    $data = $this->dataForm;
		$auth = $this->checkAuthAPI4($this->dataForm);
		if($auth['response'] == "false") {
			$res = $auth;
			echo json_encode($auth);
			return;
		} 
		
		if(!array_key_exists("audioId", $data) || !array_key_exists("idmember", $data)) {
			$res = jsonFalseResponse("parameter idmember dan audioId harus disertakan");
			echo json_encode($res);
			return;
		}
		
		if($data['idmember'] == "" || $data['idmember'] == null || $data['audioId'] == "" || $data['audioId'] == null) {
			$res = jsonFalseResponse("parameter idmember dan audioId tidak boleh kosong..");
			echo json_encode($res);
			return;
		}
		
		$this->load->model('m_audio_store', 'audio');	
		//param ke tiga bisa diisi voucherCode
		//$res = $this->audio->addMyAudioStore($data['idmember'], $data['audioId'], $data['voucherCode']);
		$hasil = $this->audio->addMyAudioStore($data['idmember'], $data['audioId']);
		$res = jsonFalseResponse("gagal di add ke list");
		if($hasil == 1) {
			$myList = $this->audio->getDetailAudio($data['audioId']); 
			$res = jsonTrueResponse($myList, "Data audio ".$data['audioId']." berhasil di masukkan ke list ");	
		} else if($hasil == 2 || $hasil == 0) {
			$myList = $this->audio->getDetailAudio($data['audioId']); 
			$res = jsonTrueResponse($myList, "Data audio ".$data['audioId']." sudah ada di dalam list");	
		}
		echo json_encode($res);
		
	}
	
	//$route['api/audio/mylist'] = 'audio_store/myAudioStore'
	public function myAudioStore(){
		$data = $this->dataForm;
		$auth = $this->checkAuthAPI4($this->dataForm);
		if($auth['response'] == "false") {
			echo json_encode($auth);
			return;
		} 
		
		$this->load->model('m_audio_store', 'audio');	
		$page = null;
		$max_limit_show = null;
		if(array_key_exists('max_limit_show', $data)) {
			$max_limit_show = $data['max_limit_show'];
		}	
		
		if(array_key_exists('page', $data)) {
			$page = $data['page'];
		}
		
		$mdReturn= $this->audio->myListAudio($data['idmember'], $page, $max_limit_show);
		if($mdReturn == null) {
			$res = jsonFalseResponse("Belum ada audio yg di download di MyList..");
			echo json_encode($res);
			return;
		} else {
			$res = jsonTrueResponse($mdReturn);
			echo json_encode($res);
			return;	
		}
		
	}
	
	//$route['api/audio/voucher/verify'] = 'audio_store/verifyAudioVoucher'
	public function verifyAudioVoucher() {
		$data = $this->dataForm;
		$auth = $this->checkAuthAPI4($this->dataForm);
		if($auth['response'] == "false") {
			$res = $auth;
			echo json_encode($auth);
			return;
		} 
		
		if(!array_key_exists("voucherCode", $data) || !array_key_exists("voucherUnique", $data)) {
			$res = jsonFalseResponse("Missing parameter voucherCode / voucherUnique");
			return;
		}
		
		$res = jsonFalseResponse("Voucher salah / tidak sesuai dengan ID Member");
		//check apakah voucher valid dan belum dipakai/diregister oleh member tertentu
		$this->load->model('m_audio_store', 'audio');		
		//check apakah voucher valid,
		$hasil = $this->audio->checkValidVoucher($data['voucherCode'], $data['voucherUnique'], $data['idmember']);
		if($hasil['response'] == "false") {
			$res = $hasil;
		} else if($hasil['response'] == "true" && $hasil['status'] == "updateReady") {
			//Status voucher = belum dipakai /di register dan siap untuk di register
			$res = $this->audio->activateVoucher($data['voucherCode'], $data['voucherUnique'], $data['idmember']);
			if($res) {
				$res = jsonTrueResponse(null, "Voucher berhasil diaktifkan");
			} else {
				$res = jsonFalseResponse("Voucher gagal diaktifkan");
			}
		} else if($hasil['response'] == "true" && $hasil['status'] == "available") {
			//status voucher = sudah diregister, data voucher dan id member yg di kirim cocok
			$res = $this->listAvailableAudio($data['idmember']);
			//echo "</pre>";
		}
		echo json_encode($res); 
		
	}
	
	private function listAvailableAudio($idmember,  $page = null, $max_limit_show = null) {
	    $this->load->model('m_audio_store', 'audio');
		$getLatestPeriod=$this->audio->getLatestPeriodPrd();
		//print_r($getLatestPeriod);
		$latestperiod = $getLatestPeriod[0]->latestperiod;
		//echo 'PERIODE TERAKHIR KASET '.$latestperiod;
		//echo "<pre>";
		$dataVcr = $this->audio->cekMasaAktifVcr2($idmember) ;//, $latestperiod);
		//print_r($dataVcr);
		if ($dataVcr > 0) {
			$kaset = $this->audio->ListBundling2($dataVcr[0]->startdt, $dataVcr[0]->enddt, $dataVcr[0]->memberid,  $page, $max_limit_show);
			if($kaset == null) {
				$res = jsonFalseResponse("Belum ada kaset audio yang tersedia");
			} else {
				$res = array(
					"response" => "true",
					"isVoucherValid" => "true",
					"memberid" => $dataVcr[0]->memberid,
					"voucherValidPeriode" => $dataVcr[0]->enddt,
					"arrayData" => $kaset
				);
			}
		} else {
			$dataVcr2 = $this->audio->cekMasaAktifVcr($idmember);
			if($dataVcr2 == null) {
				//$res = jsonFalseResponse("Cek masa aktif voucher");
				$kaset = $this->audio->listAllBundling($idmember, $page, $max_limit_show);
				$res = array(
					"response" => "true",
					"isVoucherValid" => "false",
					"memberid" => $idmember,
					"voucherValidPeriode" => null,
					"arrayData" => $kaset
				);
			}
		}
		
		return $res;
	}
	
	//$route['api/webinar/list'] = 'audio_store/listWebinar'
	function listWebinar() {
	     $data = $this->dataForm;
		$auth = $this->checkAuthAPI($data);
		$maxlimitshow = null;
		$page = null;
		
		if($auth == false) {
			//$res = $auth;
			echo json_encode($this->apiAuthResponse($auth));
			return;
		} 
	
	    $res = jsonFalseResponse("Empty List Region..");
		$this->load->model('m_audio_store', 'audio');
		
		if(array_key_exists("max_limit_show", $data) && array_key_exists("page", $data)) {
			$maxlimitshow = $data['max_limit_show'];
			$page = $data['page'];
		} 
		
		$listRegion = $this->audio->listWebinar($maxlimitshow, $page);
		if($listRegion != null) {
			$res = jsonTrueResponse($listRegion);
		}
		echo json_encode($res);
	}
	
	//$route['api/region/list'] = 'audio_store/listRegion'
	function listRegion() {
		$data = $this->dataForm;
		$auth = $this->checkAuthAPI($data);
		$maxlimitshow = null;
		$page = null;
		
		if($auth == false) {
			//$res = $auth;
			echo json_encode($this->apiAuthResponse($auth));
			return;
		} 
	
	    $res = jsonFalseResponse("Empty List Region..");
		$this->load->model('m_audio_store', 'audio');
		
		if(array_key_exists("max_limit_show", $data) && array_key_exists("page", $data)) {
			$maxlimitshow = $data['max_limit_show'];
			$page = $data['page'];
		} 
		
		$listRegion = $this->audio->listRegion($maxlimitshow, $page);
		if($listRegion != null) {
			$res = jsonTrueResponse($listRegion);
		}
		echo json_encode($res);
	}
	
	//$route['api/stockist-by-region/list'] = 'audio_store/listStockistByRegion';
	function listStockistByRegion() {
		$data = $this->dataForm;
		$auth = $this->checkAuthAPI($data);
		$maxlimitshow = null;
		$page = null;
		
		if($auth == false) {
			//$res = $auth;
			echo json_encode($this->apiAuthResponse($auth));
			return;
		} 
	    
	    $res = jsonFalseResponse("Empty List Stockist..");
		$this->load->model('m_audio_store', 'audio');
		if(array_key_exists("max_limit_show", $data) && array_key_exists("page", $data)) {
			$maxlimitshow = $data['max_limit_show'];
			$page = $data['page'];
		} 
		
		$listRegion = $this->audio->listStockistByRegion($data['regionId'], $maxlimitshow, $page);
		if($listRegion != null) {
			$res = jsonTrueResponse($listRegion);
		}
		echo json_encode($res);
	}
	
	//$route['api/lbc/list'] = 'audio_store/listLbcNews';
	function listLbcNews() {
		$data = $this->dataForm;
		$auth = $this->checkAuthAPI($data);
		$maxlimitshow = null;
		$page = null;
		$cat = null;
		
		if($auth == false) {
			//$res = $auth;
			echo json_encode($this->apiAuthResponse($auth));
			return;
		} 
	    
	    $res = jsonFalseResponse("Empty List Stockist..");
		$this->load->model('m_audio_store', 'audio');
		if(array_key_exists("max_limit_show", $data) && array_key_exists("page", $data)) {
			$maxlimitshow = $data['max_limit_show'];
			$page = $data['page'];
		} 
		
		if(array_key_exists("categoryId", $data)) {
			$cat = $data['categoryId'];
		}
		
		$listRegion = $this->audio->listLbcNews($cat, $maxlimitshow, $page);
		if($listRegion != null) {
			$res = jsonTrueResponse($listRegion);
		}
		echo json_encode($res);
	}
      
}

<?php

class Knet_pay_stk extends MY_Controller {
	function Knet_pay_stk() {
		parent::__construct();
	    $this->load->service('backend/s_knet_pay_stk', 'knet');
        //$this->load->model('m_knet_pay_stk', 'knet');
	}


	//$route['stk/login'] = "knet_pay_stk/auth";
	function auth() {
		$this->load->view($this->stkFolder.'login');
	}
	//$route['stk/login'] = "knet_pay_stk/auth";
	function downloader() {
		force_download('download/Cat2018.pdf',NULL);
	}

	//$route['stk/login/auth'] = "knet_pay_stk/getAuthStk";
	function getAuthStk() {
		$data = $this->input->post(null,true);
		try {
			$auth = $this->knet->getAuthStk($data);
			if($auth['response'] == "true") {
				$arr = array(
				 "username" => $auth['arrayData'][0]->username,
				 "fullname" => $auth['arrayData'][0]->fullnm,
				 "login" => true
				);
				//print_r($auth);
				$this->session->set_userdata('login', $arr);
				redirect('stk/trx');
			} 	
		} catch(Exception $e) {
			redirect('stk/login');
		}
	}
	//$route['stk/logout'] = "knet_pay_stk/logout";
	function logout() {
		$this->session->sess_destroy();
		redirect('stk/login');
	}
	
	//$route['stk/trx'] = "knet_pay_stk/formListTrxToPay";	
	function formListTrxToPay() {	
	    $login = $this->session->userdata('login');
		if($login['login'] == TRUE) {
            $data['user'] = $login['username'];
            $data['from'] = date("Y-m-d");
            $data['to'] = date("Y-m-d");
			//$data['idstk'] = $username;
			$data['bank'] = $this->knet->getBank();
			$data['period'] = $this->knet->getCurrentPeriodSCO();
			$this->setTemplateStkPayment('listTrxToPayForm',$data);
        } else  {
	       	redirect('stk/login');
	    }
	}
	
	//$route['stk/trx/list'] = "knet_pay_stk/getListTrxToPay";
	function getListTrxToPay() {
		
		$login = $this->session->userdata('login');
		//print_r($login);
		if($login['login'] == TRUE) {
			try {
            	$data['result'] = $this->knet->getListTrxToPay();
				//print_r($data['result']);
				$this->load->view($this->stkFolder.'listTrxToPayResult',$data);
            } catch(Exception $e) {
            	echo setErrorMessage($e->getMessage());
            }
        } else  {
	       	redirect('stk/login');
	    }
	}
	
	//$route['stk/trx/ssr/(:any)'] = "knet_pay_stk/getDetailTTPbySSRno/$1";
	function getDetailTTPbySSRno($ssrno) {
		$login = $this->session->userdata('login');
		if($login['login'] == TRUE) {
			try {
            	$data['result'] = $this->knet->getDetailTTPbySSRno($ssrno);			
            } catch(Exception $e) {
            	$data['error'] = setErrorMessage($e->getMessage());
            }
			$this->load->view($this->stkFolder.'listTrxDetailTTPResult',$data);
        } else  {
	       	redirect('stk/login');
	    }
	}
	
	//$route['stk/trx/preview'] = "knet_pay_stk/previewSelectedSSR";
	function previewSelectedSSR() {
		$login = $this->session->userdata('login');
		if($login['login'] == TRUE) {	
			$data = $this->input->post(NULL, TRUE);	
			//key development
			$data['key'] = "e74ba03ac0aaaccd267bffcb4d869450";
			$data['payID'] = "ST".randomNumber(8);
			$data['backURL'] = "http://www.k-net.co.id/stk/pay/finish/dev/".$data['payID'];
			//print_r($data);
			$data['result'] = $this->knet->previewSelectedSSR($data);
			
			$this->setTemplateStkPayment('listTrxToPayPreview',$data);
		} else  {
	       	redirect('stk/login');
	    }
	}
	
	
	/*------------------------
	  * SGO PAYMENT (DEV)
	  * -------------------------*/
	  
	  //$route['stk/pay/inquiry/dev'] = 'knet_pay_stk/getResponseFromSGODev';
	  function getResponseFromSGODev() {
        
		$xx = $this->input->post(null,true);
        $password = 'k-net181183';
		$dta = $this->knet->getDataPaymentStk($xx['order_id']);
		$cost = $dta[0]->total_pay_ssr + $dta[0]->charge_connectivity;
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'Stockist Sales Report Payment',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
        
        
      }

      
      //$route['stk/pay/notif/dev'] = 'knet_pay_stk/notifAfterPaymentDev';
      public function notifAfterPaymentDev() {
		    $xx = $this->input->post(null,true);
            $password = 'k-net181183';
			//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
			$dta = $this->knet->getDataPaymentStk($xx['order_id']);
            //$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
			$cost = $dta[0]->total_pay_ssr + $dta[0]->charge_connectivity;
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            } else{
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                $updtTabel = $this->knet->setStatusPay($xx['order_id'], "1");
				//$this->paymentService->sendTrxSMS2($resultInsert);	
                $resSukses = array(
                    'success_flag' => 0,
                    'error_message' => 'SSR Payment Success',
                    'reconcile_id' => rand(15,32),
                    'order_id' => $xx['order_id'],
                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                );
                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];             
				
				
				//die('04,Test reject,,,,,');
            }
	  }

	  //$route['stk/pay/finish/dev/(:any)'] = 'knet_pay_stk/afterPaymentWithSGODev/$1';
		public function afterPaymentWithSGODev($order_id) {
			$dta['trxInsertStatus'] = "fail";	
			
			$dta['header'] = $this->knet->getDataPaymentStk($order_id);
			$dta['detail'] = $this->knet->getDataPaymentDetailStk($order_id);	
			//harus diubah 	&& $dta['header'][0]->receiptno == null
			if ($dta['header'][0]->status_pay == "1" ) {
				$dta['trxInsertStatus'] = "ok";			
			} else {
				$sgo = $this->knet->getDataPaymentStk($order_id);
				if($sgo[0]->pay_tipe == "16") {
					$dta['trxInsertStatus'] = "pending";
				}
			}
			//$this->_destroy_cart_session();
			$this->load->view('backend/stockist/stkpayment/listTrxPaymentSuccess',$dta);
			//print_r($dt['prodCat']);
		}
	  
	  /*-------------
	   * END
	   * ------------*/
	
}
<?php
/**
 * Power Net controller
 */
class Cdtc_mbr_2 extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model("dtc/Mdtc_mbr_2", "mdtc");
    $this->load->service("webshop/Member_service",'memberService');
    $this->load->service("webshop/Product_service",'productService');
    $this->load->service("webshop/Login_service",'loginService');
    $this->load->model("backend/be_hifi_model", "hifi");
  }

  //$route['dtc/createIP/(:any)'] = 'dtc/Cdtc_mbr/generateIPTicket/$1';
  function generateIPTicket($orderid) {
    $check = $this->mdtc->checkIPExist($orderid);
    if($check != null) {
      if($check[0]->refno == null || $check[0]->refno == "") {
        $createIP = $this->mdtc->generateIP($orderid);
        echo json_encode($createIP);	
      } else {
          $inc = $check[0]->refno;
        echo json_encode(array("response" => "false", "message"=> "No Incoming Payment sudah ada $inc.."));
      }
    } else {
      echo json_encode(array("response" => "false", "message"=> "Transaksi $orderid tidak ada.."));
    }
  }

  //$route['dtc2'] = 'dtc/Cdtc_mbr_2';
  public function index()
  {
    if($this->_checkSessionStoreUser()) {
      $return=$this->mdtc->getTrxReturn(getUserID());
      if($return != NULL){
        $data['prodCat'] = $this->memberService->getProductCategory();
        $sgo=$this->mdtc->getTrxSGO($return[0]->orderno);
        $data['total_pay']=$sgo[0]->total_pay;
        $data['bankCode']=$sgo[0]->bankCode;
        $data['bankDesc']=$sgo[0]->bankDesc;
        $data['orderno'] = $return[0]->orderno;
        $data['tabel'] = $this->mdtc->TrxDetail($return[0]->orderno);
        $data['key'] = "key";
        $data['payID'] = "payID";

        $this->load->model('backend/be_etiket_model');
        $data['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket(); 
        if(getUserID() == "ID9999A00251") {
          $data['submit_url'] = 'dtc/submitVera';
          $data['action'] = site_url('dtc/submitVera');
          $this->setTempWebShopEspay('backend/dtc/vdtc_return', $data);
        } else {
          $data['submit_url'] = 'dtc/submitVera';
          $data['action'] = site_url('dtc/submitVera');
          $this->setTempWebShopEspay('backend/dtc/vdtc_returnVera', $data);
        }
      } else {
        $data['listBank'] = $this->mdtc->getBankForTicket();
        $data['key'] = "key";
        $data['payID'] = "payID";
        $data['isiTabel'] = $this->mdtc->tabelData();
        $data['prodCat'] = $this->memberService->getProductCategory();

        $this->load->model('backend/be_etiket_model');
        
        if(getUserID() == "ID9999A00251") {
          //echo "di MEMBER : ".getUserID();
          $data['listTipeAcara'] = $this->mdtc->getListTipeAcaraVera();
          
          $data['submit_url'] = 'dtc2/submitVera';
          $data['action'] = site_url('dtc2/submitVera');
          $this->setTempWebShopEspay('backend/dtc/vdtc_acaraVera', $data);
        } else {
          //echo "sdsaa";
          //echo "di MEMBER : ".getUserID();
          $data['listTipeAcara'] = $this->mdtc->getListTipeAcara();
          $data['submit_url'] = 'dtc2/submitVera';
          $data['action'] = site_url('dtc2/submitVera');
          $this->setTempWebShopEspay('backend/dtc/vdtc_acara', $data);
        }
      }
    }
  }
  //$route['dtc/listEvent/(:any)'] = 'dtc/Cdtc_mbr/listEventByCategory/$1';
  function listEventByCategory($cat_id) {
    $mdReturn = jsonFalseResponse("Tidak ada acara yang aktif...");
    if(getUserID() == "ID9999A00251") {
      $res = $this->mdtc->getAcaraOnlineByCategoryVera($cat_id);
    } else {  
      $res = $this->mdtc->getAcaraOnlineByCategoryNew($cat_id);
    }  
    if($res != null) {
      $mdReturn = jsonTrueResponse($res);
    }
    echo json_encode($mdReturn);
  }
  
  //$route['dtc/listEventNew/(:any)'] = 'dtc/Cdtc_mbr/listEventByCategoryNew/$1';
  function listEventByCategoryNew($cat_id) {
    $mdReturn = jsonFalseResponse("Tidak ada acara yang aktif...");
    $res = $this->mdtc->getAcaraOnlineByCategoryNew($cat_id);
    if($res != null) {
      $mdReturn = jsonTrueResponse($res);
    }
    echo json_encode($mdReturn);
  }
  
  
  //$route['dtc/submit/va'] = 'dtc/Cdtc_mbr/saveTrxDTC_Va';
  public function saveTrxDTC_Va() {
    $data = $this->input->post(NULL, TRUE);
//		$orderno = $this->my_counter->getCounter2('TA', $this->uuid->v4());

    $checkMember = $this->hifi->getFullNameActiveMember("dfno", $data['memberid']);
    if($checkMember != null) {
      $temp_id = $data['paymentId'];
      $res = $this->realTrxDTC_Va($temp_id, $data);
      echo json_encode($res);
    }
    else {
      $arr = jsonFalseResponse("ID Member salah / tidak terdaftar..");
      echo json_encode($arr);
    }
  }
  private function realTrxDTC_Va($temp_id, $data) {

    $amount = (double) $data['paymentAmount'];
//		$amount_cashback = (double) $data['amount_cashback'];

    $saldo = $this->hifi->getSaldoVa(getUserid());
    if($saldo != null) {

      $sisaSaldo = (double) $saldo[0]->amount;
      if(getUserid() == null) {
        return jsonFalseResponse("Silahkan login kembali..");
      }

      if($amount <= $sisaSaldo) {
        {
          $dta = $this->mdtc->updateTrxSGO($temp_id);

          $hasil2 = json_decode($dta);

          if($hasil2 == "0") {
            $falseRes = array("response" => "false", "message" => "Transaksi tidak dapat dilakukan pada jam yang sudah ditentukan..");
            return $falseRes;
          } else if($hasil2 == null) {
            $falseRes = array("response" => "false", "message" => "No response from XL");
            return $falseRes;
          }
          else {
            //set data payment
            $trtype = "DTC";
            $arr = array(
              "trcd"    => $temp_id,
              "trtype"  => $trtype,
              "effect"  => "-",
              "effect_acc"  => "+",
              "custtype"=> "M",
              "novac"   => getUserNovac(),
              "dfno"    => getUserID(),
              "docno"   => "",
              "refno"   => "",
              "amount"  => $amount,
              "createnm"=> getUserID(),
              "tipe_dk" => "D",
              "tipe_dk_acc" => "K"
            );
            $dta = $this->mdtc->insertVA($arr);

            return jsonTrueResponse(null, "Selamat");
          }
        }




        //echo json_encode($insReturn);
      } else {
        return jsonFalseResponse("Saldo VA anda tidak cukup, sisa saldo anda : ".$sisaSaldo);
      }
    } else {
      return jsonFalseResponse("Saldo anda tidak cukup atau belum di top up");
    }
  }

  public function NonMember()
  {

//		$data = $this->input->post(NULL, TRUE);
    $login = $this->mdtc->checkAuthLoginKnet();
//		print_r($login);
    if($login != null) {
      $telhp = getUserPhone();
      $email = getUserEmail();
      $this->session->set_userdata('store_info', $login);
      $this->session->set_userdata('pricecode', "12w4");

      //if(getUserID() == "IDSPAAA96407") {
      //print_r($promo);
      $promo = $this->loginService->checkListPromo();

      $this->session->set_userdata('promo', $promo);
      //}
    }




    $dt['prodCat'] = $this->memberService->getProductCategory();

    $dt['prdnm'] = $this->input->post("name");
    $dt['formAction'] = "".site_url('cart/addtocart')."";
    $dt['banner'] = $this->productService->getHeaderBanner();
    $dt['prod'] = $this->productService->getProductShop();
    $dt['prodCat'] = $this->productService->getProductCategory();
    $dt['folderPrd'] = $this->folderPrd;

    $this->setTempWebShopNonMember('webshop/shop_home_iman', $dt);


  }

  public function cariID(){
    $id = $this->input->post("idm");

    //	$ID = $this->Mdtc_mbr/checkID($data);
    //echo "Nandang";
    //	echo $id;
    //	$this->load->model("dtc/Mdtc_mbr",$id);
    $nama=$this->mdtc->checkID1($id);

    //echo $nama[0]->fullnm;
    //print_r ($nama) ;
    //array("tes"=> "sdsds");
    echo json_encode($nama);
  }
  public function cariIDGO(){
    $id = $this->input->post("idm");

    //	$ID = $this->Mdtc_mbr/checkID($data);
    //echo "Nandang";
    //	echo $id;
    //	$this->load->model("dtc/Mdtc_mbr",$id);
    $nama=$this->mdtc->checkIDGO($id);

    //echo $nama[0]->fullnm;
    //print_r ($nama) ;
    //array("tes"=> "sdsds");
    echo json_encode($nama);
  }


  //$route['dtc2/cariID2'] = 'dtc/Cdtc_mbr_2/cariID2';
  public function cariID2(){
    $id = $this->input->post("idm");
    $acara=$this->input->post("acara");
    $tipe_acara=$this->input->post("tipe_acara");
    $nama=$this->mdtc->checkID2($id,$acara,$tipe_acara);
    echo json_encode($nama);
  }
  
  //$route['dtc/check/beforeProcess'] = 'dtc/Cdtc_mbr/checkBeforePayment'
  public function checkBeforePayment() {
    $data = $this->input->post(NULL, TRUE);
    
    $check = $this->mdtc->checkJumlahpendaftar($data['orderno']);
    if($check != null) {
      $id_etiket = $check[0]->id_etiket;
      $jumlah_pendaftar = $check[0]->jumlah_pendaftar;
      $check2 = $this->mdtc->checkValidAcara($id_etiket, $jumlah_pendaftar);
      $jsonRes = $check2;
    } else {
      $jsonRes = jsonFalseResponse("Data tiket sudah hangus atau sudah lunas..");
    }
    echo json_encode($jsonRes);
  }
  
  //$route['dtc/submitVera'] = 'dtc/Cdtc_mbr/simpanVera';
  function simpanVera()
  {
    if($this->_checkSessionStoreUser()) {
      $idmbr = getUserID();
      $fullnm = getUsername();
      $tkt = $this->input->post('opsi');
      $emailrow = $this->input->post('emailrow');
      $idtiket = $this->input->post('event_db');
      $notelp = $this->input->post('notelprow');
      $nmtiket = $this->input->post('event_nm');
      $jumlah = $this->input->post('jumlah');
      $valid_name = $this->input->post('nmpendaftar');
      $jumlah_pendaftar = count($valid_name);

      $event_cat_id = $this->input->post('event_cat_id');

      
      
      if(getUserID() == "ID9999A00251") {
        $arrsisa = array("sisa_kuota" => 50);
        $checkValid = array("response" => "true", "arrayData" => $arrsisa);
        //$checkValid['response'] == "true";
      } else {
        $checkValid = $this->mdtc->checkValidAcara($idtiket[0], $jumlah_pendaftar);
      }  
      
      if($checkValid['response'] == "false") {
        $x = $checkValid;
        //return;
      } else {
      
        $isEarly = $this->input->post('early_status');
        
        $prd_bundling = $this->input->post('prd_bundling');
        
        $realtotal = 0;
        //print_r($this->input->post('nmpendaftar') );
        foreach ($valid_name as $xdx => $v) {
          $realtotal = $realtotal + $jumlah[$xdx];
        }


        $valid_dfno = $this->input->post('idpendaftar');
        
        $orderno = $this->my_counter->getCounter2('TA', $this->uuid->v4());
        //print_r($orderno);
        $flag_online = 'W';
        $price = $this->input->post('harga');
        $createnm = $this->username;

        $totprd = 0;
        $kE = 0;
        $totalharga = 0;
        $urutan = 0;
  
        $return = $this->mdtc->getDetailTiket($idtiket[0])->row();
        $price_offline = $return->price_online;
        $max_offline = $return->max_online;

        $banker = $this->mdtc->getDetailBank($this->input->post('bankid'))->row();
        $hrgtot = $price_offline * $realtotal;

        $sisa_kuota = $checkValid['arrayData']['sisa_kuota'];
        if ($realtotal > $sisa_kuota) {
          $x = jsonFalseResponse("Pembelian melebihi stok, pendaftar : $realtotal, stock : $max_offline");

        } else {
          $minus = array(
            'max_online' => $sisa_kuota - $realtotal
          );
          $this->mdtc->updateStok($idtiket[0], $minus);

          if (!empty($idtiket)) {
  //                echo $this->mdtc->Counter2($idtiket);
            foreach ($valid_name as $xdx => $v) {


              for ($k = 0; $k < $jumlah[$xdx]; $k++) {
                $no = $this->mdtc->getNoTiket($idtiket[$xdx]);
                $urutan++;
                $arr_dtl = array(
                  //'urut' => $xd,
                  'urut' => $urutan,
                  'order_no' => $orderno,
                  'dfno' => $idmbr,
                  'fullnm' => $fullnm,
                  'notiket' => $no,
                  'id_etiket' => $idtiket[$xdx],
                  'price' => $price[$xdx],
                  'flag_online' => $flag_online,
                  'valid_dfno' => $valid_dfno[$xdx],
                  'valid_fullnm' => $valid_name[$xdx],
                  'status' => '1',
                  'createdt' => date("Y-m-d h:i:s"),
                  'email' => $emailrow[$xdx],
                  'notelp' => $notelp[$xdx],
                  'createnm' => getUserID(),
                  'is_early' => $isEarly
                );
                $kE++;
                $totalharga = $totalharga + $price[$xdx];
                $this->mdtc->saveDetail($arr_dtl);
                
                //jika tipe acara adalah power net event
                if($event_cat_id == "6") {
                  $this->mdtc->insertPrdBundlingTicketPowerNet($no, $idtiket[$xdx]);
                } else {
                  $this->mdtc->insertPrdBundlingTicket($no, $idtiket[$xdx], $prd_bundling[$xdx]);
                }
              }


            }

            $arr_det_prd_sgo = array(
              'orderno' => $orderno,
              'prdcd' => $idtiket[$xdx],
              'prdnm' => $nmtiket[$xdx],
              'qty' => $urutan,
              'bvr' => '0',
              'dpr' => $price[$xdx],
              'pricecode' => '12W4'
            );
            $this->mdtc->simpanDetTrx($arr_det_prd_sgo);


            $arr_hdr_sgo = array(
              'orderno' => $orderno,
              'token' => $orderno,
              'id_memb' => $idmbr,
              'nmmember' => $fullnm,
              'total_pay' => $totalharga,
              'total_bv' => '0',
              'pricecode' => '12W4',
              'bonusmonth' => date('m') . '/' . date('Y'),
              'datetrans' => date("Y-m-d h:i:s"),
              'idstk' => 'BID06',
              'nmstkk' => 'BID06 - PT. K-LINK NUSANTARA',
              'flag_trx' => 'W',
              'sentTo' => '1',
              'status_vt_pay' => '0',
              'payShip' => '0',
              'secno' => '',

              'payAdm' => $banker->charge_admin,
              'is_umroh' => '0',
              'bank_code_payment' => $banker->id,
              'userlogin' => getUserID(),
              'payConnectivity' => $banker->charge_connectivity,
              'is_login' => '1',
              'totPayCP' => '0',
              'free_shipping' => '0',
              'discount_shipping' => '0'
            );


            $arr_paydet_sgo = array(
              'orderno' => $orderno,
              'seqno' => '1',
              'paytype' => 'sgo',
              'docno' => '',
              'payamt' => $totalharga,
              'deposit' => '0',
              'notes' => 'pending',
              'paystatus' => 'pending',
              'bank_code_payment' => $banker->id,
              'charge_admin' => $banker->charge_admin
            );
            $status_trx = array(
              'orderno' => $orderno,
              'status' => '0',
              'dfno' => $idmbr,
              'createnm' => $idmbr,
              'createdt' => date("Y-m-d h:i:s")

            );

            $res = $this->mdtc->simpanTrx($arr_hdr_sgo, $arr_paydet_sgo, $status_trx);

            if ($res != null) {

              $x = jsonTrueResponse($res);


            } else {
              $x = jsonFalseResponse("Transaksi gagal");


            }

          } else {
            $x = jsonFalseResponse("Pembelian melebihi stok, $idtiket");


          }


        }
      } 

      echo json_encode($x);
    }
  }

  //$route['dtc/submit'] = 'dtc/Cdtc_mbr/simpan';
  function simpan()
  {
    if($this->_checkSessionStoreUser()) {
      $idmbr = getUserID();
      $fullnm = getUsername();
      $tkt = $this->input->post('opsi');
      $emailrow = $this->input->post('emailrow');
      $idtiket = $this->input->post('event_db');
      $notelp = $this->input->post('notelprow');
      $nmtiket = $this->input->post('event_nm');
      $jumlah = $this->input->post('jumlah');
      $valid_name = $this->input->post('nmpendaftar');
      $jumlah_pendaftar = count($valid_name);
      
      $isEarly = $this->input->post('early_status');
      
      $prd_bundling = $this->input->post('prd_bundling');
      
      $realtotal = 0;
      //print_r($this->input->post('nmpendaftar') );
      foreach ($valid_name as $xdx => $v) {
        $realtotal = $realtotal + $jumlah[$xdx];
      }


      $valid_dfno = $this->input->post('idpendaftar');
      
      $orderno = $this->my_counter->getCounter2('TA', $this->uuid->v4());
      //print_r($orderno);
      $flag_online = 'W';
      $price = $this->input->post('harga');
      $createnm = $this->username;

      $totprd = 0;
      $kE = 0;
      $totalharga = 0;
      $urutan = 0;
//            echo $tkt;
//            echo "<script type='text/javascript'>alert('pembelian melebihi stok = $idtiket[0]'  );</script>";

      $return = $this->mdtc->getDetailTiket($idtiket[0])->row();
      $price_offline = $return->price_online;
      $max_offline = $return->max_online;

      $banker = $this->mdtc->getDetailBank($this->input->post('bankid'))->row();
//            echo $price_offline;
//            echo $max_offline;
      $hrgtot = $price_offline * $realtotal;


      if ($realtotal > $max_offline) {
//			echo setErrorMessage('Pembelian melebihi stok');
        $x = jsonFalseResponse("Pembelian melebihi stok");

      } else {
        /*
        $minus = array(
          'max_online' => $max_offline - $realtotal
        );
        $this->mdtc->updateStok($idtiket[0], $minus);
        */
        
        if (!empty($idtiket)) {
//                echo $this->mdtc->Counter2($idtiket);
          foreach ($valid_name as $xdx => $v) {


            for ($k = 0; $k < $jumlah[$xdx]; $k++) {
//                        echo $this->mdtc->Counter2($idtiket);
                            //UPDATE DION 2912/2018
                            /*
              $xd = $this->mdtc->Counter2($idtiket[$xdx]);
              $urutan++;
              $no = "TA" . str_pad($idtiket[$xdx], 3, "000", STR_PAD_LEFT) . str_pad($xd, 4, "00", STR_PAD_LEFT);
              */
              
              $no = $this->mdtc->getNoTiket($idtiket[$xdx]);
                            $urutan++;
              $arr_dtl = array(
                //'urut' => $xd,
                'urut' => $urutan,
                'order_no' => $orderno,
                'dfno' => $idmbr,
                'fullnm' => $fullnm,
                'notiket' => $no,
                'id_etiket' => $idtiket[$xdx],
                'price' => $price[$xdx],
                'flag_online' => $flag_online,
                'valid_dfno' => $valid_dfno[$xdx],
                'valid_fullnm' => $valid_name[$xdx],
                'status' => '1',
                'createdt' => date("Y-m-d h:i:s"),
                'email' => $emailrow[$xdx],
                'notelp' => $notelp[$xdx],
                'createnm' => getUserID(),
                'is_early' => $isEarly
              );
              $kE++;
              $totalharga = $totalharga + $price[$xdx];
              $this->mdtc->saveDetail($arr_dtl);
              
              $this->mdtc->insertPrdBundlingTicket($no, $idtiket[$xdx], $prd_bundling[$xdx]);

            }


          }

          $arr_det_prd_sgo = array(
            'orderno' => $orderno,
            'prdcd' => $idtiket[$xdx],
            'prdnm' => $nmtiket[$xdx],
            'qty' => $urutan,
            'bvr' => '0',
            'dpr' => $price[$xdx],
            'pricecode' => '12W3'
          );
          $this->mdtc->simpanDetTrx($arr_det_prd_sgo);


          $arr_hdr_sgo = array(
            'orderno' => $orderno,
            'token' => $orderno,
            'id_memb' => $idmbr,
            'nmmember' => $fullnm,
            'total_pay' => $totalharga,
            'total_bv' => '0',
            'pricecode' => '12W3',
            'bonusmonth' => date('m') . '/' . date('Y'),
            'datetrans' => date("Y-m-d h:i:s"),
            'idstk' => 'BID06',
            'nmstkk' => 'BID06 - PT. K-LINK NUSANTARA',
            'flag_trx' => 'W',
            'sentTo' => '1',
            'status_vt_pay' => '0',
            'payShip' => '0',
            'secno' => '',

            'payAdm' => $banker->charge_admin,
            'is_umroh' => '0',
            'bank_code_payment' => $banker->id,
            'userlogin' => getUserID(),
            'payConnectivity' => $banker->charge_connectivity,
            'is_login' => '1',
            'totPayCP' => '0',
            'free_shipping' => '0',
            'discount_shipping' => '0'
          );


          $arr_paydet_sgo = array(
            'orderno' => $orderno,
            'seqno' => '1',
            'paytype' => 'sgo',
            'docno' => '',
            'payamt' => $totalharga,
            'deposit' => '0',
            'notes' => 'pending',
            'paystatus' => 'pending',
            'bank_code_payment' => $banker->id,
            'charge_admin' => $banker->charge_admin
          );
          $status_trx = array(
            'orderno' => $orderno,
            'status' => '0',
            'dfno' => $idmbr,
            'createnm' => $idmbr,
            'createdt' => date("Y-m-d h:i:s")

          );

          $res = $this->mdtc->simpanTrx($arr_hdr_sgo, $arr_paydet_sgo, $status_trx);

//				echo setSuccessMessage("Data tersimpan");

          if ($res != null) {

            $x = jsonTrueResponse($res);


          } else {
            $x = jsonFalseResponse("Transaksi gagal");


          }

        } else {
          $x = jsonFalseResponse("Pembelian melebihi stok");


        }


      }


      echo json_encode($x);
    }
  }

  //$route['dtc/done/(:any)'] = 'dtc/Cdtc_mbr/done/$1';
  function done($orderno){
    $transfer=$this->mdtc->isMetodeTranfer($orderno);
    if($transfer==0)
    {
      $result= $this->mdtc->isSukses($orderno);
      if($result==0){
        $this->mdtc->hangus($orderno);
        $data['orderno']=$orderno;
        $data['prodCat'] = $this->memberService->getProductCategory();
        $data['msg']='Transaksi Selesai';
        $this->setTempWebShopEspay('backend/dtc/hangus2', $data);
      }
      else
      {
        $this->mdtc->proses($orderno);

        $data['orderno']=$orderno;
        $data['prodCat'] = $this->memberService->getProductCategory();
        $data['msg']='Transaksi Berhasil';
        $this->setTempWebShopEspay('backend/dtc/hangus2', $data);
      }
    }
    else
    {
      $data['orderno']=$orderno;
      $data['prodCat'] = $this->memberService->getProductCategory();
      $data['msg']='Transaksi Selesai';
      $this->setTempWebShopEspay('backend/dtc/hangus2', $data);
    }
  }

//OTW///
  function vtiket(){
    if($this->_checkSessionStoreUser()) {

      $data['prodCat'] = $this->memberService->getProductCategory();
      $data['msg']='Transaksi Berhasil';
      $data['tabelBuyer'] = $this->mdtc->TabelTiketBuyer(getUserID());
      $data['tabelValid'] = $this->mdtc->TabelTiketValid(getUserID());

      $this->setTempWebShopEspay('backend/dtc/vdtc_view', $data);

    }
  }


  function vtiket2($notiket){
    $this->load->library('ciqrcode');

    $params['data'] = $notiket;
    $params['level'] = 'H';
    $params['size'] = 10;
    $params['savename'] = FCPATH.'captcha/'.$notiket.'.png';
    $this->ciqrcode->generate($params);

//		$dt['qr2']= '<img src="'.base_url().'captcha/'.$notiket.'.jpg" />';
    $dt['qr']= 'captcha/'.$notiket.'.png';
    $dt['data']=$this->mdtc->showDataPrint($notiket);
//		assets/images/logo.jpg
//		echo FCPATH;
    $dt['res1'] = array(1);

    $this->load->view('backend/dtc/printQRTiket',$dt);

  }




  function formListingDTC(){
    if($this->username != null) {
      $x['form_header'] = 'DTC';
      $x['icon'] = 'icon-list';
      $x['form_reload'] = 'be_klaimbonus';
      $this->setTemplate($this->folderView.'formListing', $x);
    }
    else
    {
      redirect('backend', 'refresh');
    }
  }

  function undian(){
    //if($this->_checkSessionStoreUser()) {

    $data['main_view'] = 'backend/dtc/text3';

    $data['pemenang'] = $this->mdtc->getPemenangSemua();


    $this->load->view('backend/dtc/text3', $data);

  }


  function undian2(){
    $data['motor']= $this->mdtc->getPemenang(11);
    $data['leptop']= $this->mdtc->getPemenang(10);
    $data['tv']= $this->mdtc->getPemenang(9);
    $data['star']= $this->mdtc->getPemenang(8);
    $star1 = $this->mdtc->getPemenang20(8,5,5);
    $star2 = $this->mdtc->getPemenang20(8,5,10);
    $star3 = $this->mdtc->getPemenang20(8,5,15);
    $star4 = $this->mdtc->getPemenang20(8,5,20);
    $star5 = $this->mdtc->getPemenang20(8,5,25);
    $data['star1']=array();
    $data['star2']=array();
    $data['star3']=array();
    $data['star4']=array();
    $data['star5']=array();
    foreach ($star1 as $dta) {
      array_push($data['star1'], $dta->NOUNDIAN);
    }
    foreach ($star2 as $dta2) {
      array_push($data['star2'], $dta2->NOUNDIAN);
    }
    foreach ($star3 as $dta3) {
      array_push($data['star3'], $dta3->NOUNDIAN);
    }
    foreach ($star4 as $dta4) {
      array_push($data['star4'], $dta4->NOUNDIAN);
    }
    foreach ($star5 as $dta5) {
      array_push($data['star5'], $dta5->NOUNDIAN);
    }
    $data['kolagen']= $this->mdtc->getPemenang(7);
    $data['yoropen']= $this->mdtc->getPemenang(6);
    $yoropen1 = $this->mdtc->getPemenang20(6,10,10);
    $yoropen2 = $this->mdtc->getPemenang20(6,10,20);
    $yoropen3 = $this->mdtc->getPemenang20(6,10,30);
    $yoropen4 = $this->mdtc->getPemenang20(6,10,40);
    $yoropen5 = $this->mdtc->getPemenang20(6,10,50);
    $data['yoropen1']=array();
    $data['yoropen2']=array();
    $data['yoropen3']=array();
    $data['yoropen4']=array();
    $data['yoropen5']=array();
    foreach ($yoropen1 as $dta) {
      array_push($data['yoropen1'], $dta->NOUNDIAN);
    }
    foreach ($yoropen2 as $dta) {
      array_push($data['yoropen2'], $dta->NOUNDIAN);
    }
    foreach ($yoropen3 as $dta) {
      array_push($data['yoropen3'], $dta->NOUNDIAN);
    }
    foreach ($yoropen4 as $dta) {
      array_push($data['yoropen4'], $dta->NOUNDIAN);
    }
    foreach ($yoropen5 as $dta) {
      array_push($data['yoropen5'], $dta->NOUNDIAN);
    }
    $data['STARTERKIT']= $this->mdtc->getPemenang(5);
    $STARTERKIT1 = $this->mdtc->getPemenang20(5,10,10);
    $STARTERKIT2 = $this->mdtc->getPemenang20(5,10,20);
    $data['STARTERKIT1']=array();
    $data['STARTERKIT2']=array();
    foreach ($STARTERKIT1 as $dta) {
      array_push($data['STARTERKIT1'], $dta->NOUNDIAN);
    }
    foreach ($STARTERKIT2 as $dta) {
      array_push($data['STARTERKIT2'], $dta->NOUNDIAN);
    }
    $data['STARTERKITz']= $this->mdtc->getPemenang(4);
    $STARTERKITz1 = $this->mdtc->getPemenang20(4,10,10);
    $STARTERKITz2 = $this->mdtc->getPemenang20(4,10,20);
    $data['STARTERKITz1']=array();
    $data['STARTERKITz2']=array();
    foreach ($STARTERKITz1 as $dta) {
      array_push($data['STARTERKITz1'], $dta->NOUNDIAN);
    }
    foreach ($STARTERKITz2 as $dta) {
      array_push($data['STARTERKITz2'], $dta->NOUNDIAN);
    }
    $data['engine']= $this->mdtc->getPemenang(3);
    $engine1 = $this->mdtc->getPemenang20(3,10,10);
    $engine2 = $this->mdtc->getPemenang20(3,10,20);
    $engine3 = $this->mdtc->getPemenang20(3,10,30);
    $engine4 = $this->mdtc->getPemenang20(3,10,40);
    $data['engine1']=array();
    $data['engine2']=array();
    $data['engine3']=array();
    $data['engine4']=array();
    foreach ($engine1 as $dta) {
      array_push($data['engine1'], $dta->NOUNDIAN);
    }
    foreach ($engine2 as $dta) {
      array_push($data['engine2'], $dta->NOUNDIAN);
    }
    foreach ($engine3 as $dta) {
      array_push($data['engine3'], $dta->NOUNDIAN);
    }
    foreach ($engine4 as $dta) {
      array_push($data['engine4'], $dta->NOUNDIAN);
    }
    $data['teavigo']= $this->mdtc->getPemenang(2);
    $teh1 = $this->mdtc->getPemenang20(2,10,10);
    $teh2 = $this->mdtc->getPemenang20(2,10,20);
    $teh3 = $this->mdtc->getPemenang20(2,10,30);
    $teh4 = $this->mdtc->getPemenang20(2,10,40);
    $teh5 = $this->mdtc->getPemenang20(2,10,50);
    $data['teh1']=array();
    $data['teh2']=array();
    $data['teh3']=array();
    $data['teh4']=array();
    $data['teh5']=array();
    foreach ($teh1 as $dta) {
      array_push($data['teh1'], $dta->NOUNDIAN);
    }
    foreach ($teh2 as $dta) {
      array_push($data['teh2'], $dta->NOUNDIAN);
    }
    foreach ($teh3 as $dta) {
      array_push($data['teh3'], $dta->NOUNDIAN);
    }
    foreach ($teh4 as $dta) {
      array_push($data['teh4'], $dta->NOUNDIAN);
    }
    foreach ($teh5 as $dta) {
      array_push($data['teh5'], $dta->NOUNDIAN);
    }


    $loop1 = $this->mdtc->getPemenang20(1,10,10);
    $loop2 = $this->mdtc->getPemenang20(1,10,20);
    $loop3 = $this->mdtc->getPemenang20(1,10,30);
    $loop4 = $this->mdtc->getPemenang20(1,10,40);
    $loop5 = $this->mdtc->getPemenang20(1,10,50);
    $loop6 = $this->mdtc->getPemenang20(1,10,60);
    $loop7 = $this->mdtc->getPemenang20(1,10,70);
    $loop8 = $this->mdtc->getPemenang20(1,10,80);
    $loop9 = $this->mdtc->getPemenang20(1,10,90);
    $loop10 = $this->mdtc->getPemenang20(1,10,100);
    $data['shaker1']=array();
    $data['shaker2']=array();
    $data['shaker3']=array();
    $data['shaker4']=array();
    $data['shaker5']=array();
    $data['shaker6']=array();
    $data['shaker7']=array();
    $data['shaker8']=array();
    $data['shaker9']=array();
    $data['shaker10']=array();


    foreach ($loop1 as $dta) {
      array_push($data['shaker1'], $dta->NOUNDIAN);
    }
    foreach ($loop2 as $dta) {
      array_push($data['shaker2'], $dta->NOUNDIAN);
    }
    foreach ($loop3 as $dta) {
      array_push($data['shaker3'], $dta->NOUNDIAN);
    }
    foreach ($loop4 as $dta) {
      array_push($data['shaker4'], $dta->NOUNDIAN);
    }
    foreach ($loop5 as $dta) {
      array_push($data['shaker5'], $dta->NOUNDIAN);
    }

    foreach ($loop6 as $dta) {
      array_push($data['shaker6'], $dta->NOUNDIAN);
    }
    foreach ($loop7 as $dta) {
      array_push($data['shaker7'], $dta->NOUNDIAN);
    }
    foreach ($loop8 as $dta) {
      array_push($data['shaker8'], $dta->NOUNDIAN);
    }
    foreach ($loop9 as $dta) {
      array_push($data['shaker9'], $dta->NOUNDIAN);
    }
    foreach ($loop10 as $dta) {
      array_push($data['shaker10'], $dta->NOUNDIAN);
    }



    $data['main_view'] = 'text2';

    $this->load->view('backend/dtc/text2', $data);
  }




  function undianslide(){
    $data['motor']= $this->mdtc->getPemenang(11);
    $data['leptop']= $this->mdtc->getPemenang(10);
    $data['tv']= $this->mdtc->getPemenang(9);



    $data['star']= $this->mdtc->getPemenang(8);
    $star1 = $this->mdtc->getPemenang20(8,5,5);
    $star2 = $this->mdtc->getPemenang20(8,5,10);
    $star3 = $this->mdtc->getPemenang20(8,5,15);
    $star4 = $this->mdtc->getPemenang20(8,5,20);
    $star5 = $this->mdtc->getPemenang20(8,5,25);

    $data['star1']=array();
    $data['star2']=array();
    $data['star3']=array();
    $data['star4']=array();
    $data['star5']=array();
    foreach ($star1 as $dta) {
      array_push($data['star1'], $dta->NOUNDIAN);
    }
    foreach ($star2 as $dta2) {
      array_push($data['star2'], $dta2->NOUNDIAN);
    }
    foreach ($star3 as $dta3) {
      array_push($data['star3'], $dta3->NOUNDIAN);
    }
    foreach ($star4 as $dta4) {
      array_push($data['star4'], $dta4->NOUNDIAN);
    }
    foreach ($star5 as $dta5) {
      array_push($data['star5'], $dta5->NOUNDIAN);
    }




    $data['kolagen']= $this->mdtc->getPemenang(7);



    $data['yoropen']= $this->mdtc->getPemenang(6);
    $yoropen1 = $this->mdtc->getPemenang20(6,10,10);
    $yoropen2 = $this->mdtc->getPemenang20(6,10,20);
    $yoropen3 = $this->mdtc->getPemenang20(6,10,30);
    $yoropen4 = $this->mdtc->getPemenang20(6,10,40);
    $yoropen5 = $this->mdtc->getPemenang20(6,10,50);

    $data['yoropen1']=array();
    $data['yoropen2']=array();
    $data['yoropen3']=array();
    $data['yoropen4']=array();
    $data['yoropen5']=array();
    foreach ($yoropen1 as $dta) {
      array_push($data['yoropen1'], $dta->NOUNDIAN);
    }
    foreach ($yoropen2 as $dta) {
      array_push($data['yoropen2'], $dta->NOUNDIAN);
    }
    foreach ($yoropen3 as $dta) {
      array_push($data['yoropen3'], $dta->NOUNDIAN);
    }
    foreach ($yoropen4 as $dta) {
      array_push($data['yoropen4'], $dta->NOUNDIAN);
    }
    foreach ($yoropen5 as $dta) {
      array_push($data['yoropen5'], $dta->NOUNDIAN);
    }






    $data['STARTERKIT']= $this->mdtc->getPemenang(5);
    $STARTERKIT1 = $this->mdtc->getPemenang20(5,10,10);
    $STARTERKIT2 = $this->mdtc->getPemenang20(5,10,20);
    $data['STARTERKIT1']=array();
    $data['STARTERKIT2']=array();
    foreach ($STARTERKIT1 as $dta) {
      array_push($data['STARTERKIT1'], $dta->NOUNDIAN);
    }
    foreach ($STARTERKIT2 as $dta) {
      array_push($data['STARTERKIT2'], $dta->NOUNDIAN);
    }




    $data['STARTERKITz']= $this->mdtc->getPemenang(4);
    $STARTERKITz1 = $this->mdtc->getPemenang20(4,10,10);
    $STARTERKITz2 = $this->mdtc->getPemenang20(4,10,20);
    $data['STARTERKITz1']=array();
    $data['STARTERKITz2']=array();
    foreach ($STARTERKITz1 as $dta) {
      array_push($data['STARTERKITz1'], $dta->NOUNDIAN);
    }
    foreach ($STARTERKITz2 as $dta) {
      array_push($data['STARTERKITz2'], $dta->NOUNDIAN);
    }



    $data['engine']= $this->mdtc->getPemenang(3);
    $engine1 = $this->mdtc->getPemenang20(3,10,10);
    $engine2 = $this->mdtc->getPemenang20(3,10,20);
    $engine3 = $this->mdtc->getPemenang20(3,10,30);
    $engine4 = $this->mdtc->getPemenang20(3,10,40);
    $data['engine1']=array();
    $data['engine2']=array();
    $data['engine3']=array();
    $data['engine4']=array();
    foreach ($engine1 as $dta) {
      array_push($data['engine1'], $dta->NOUNDIAN);
    }
    foreach ($engine2 as $dta) {
      array_push($data['engine2'], $dta->NOUNDIAN);
    }
    foreach ($engine3 as $dta) {
      array_push($data['engine3'], $dta->NOUNDIAN);
    }
    foreach ($engine4 as $dta) {
      array_push($data['engine4'], $dta->NOUNDIAN);
    }



    $data['teavigo']= $this->mdtc->getPemenang(2);
    $teh1 = $this->mdtc->getPemenang20(2,10,10);
    $teh2 = $this->mdtc->getPemenang20(2,10,20);
    $teh3 = $this->mdtc->getPemenang20(2,10,30);
    $teh4 = $this->mdtc->getPemenang20(2,10,40);
    $teh5 = $this->mdtc->getPemenang20(2,10,50);

    $data['teh1']=array();
    $data['teh2']=array();
    $data['teh3']=array();
    $data['teh4']=array();
    $data['teh5']=array();
    foreach ($teh1 as $dta) {
      array_push($data['teh1'], $dta->NOUNDIAN);
    }
    foreach ($teh2 as $dta) {
      array_push($data['teh2'], $dta->NOUNDIAN);
    }
    foreach ($teh3 as $dta) {
      array_push($data['teh3'], $dta->NOUNDIAN);
    }
    foreach ($teh4 as $dta) {
      array_push($data['teh4'], $dta->NOUNDIAN);
    }
    foreach ($teh5 as $dta) {
      array_push($data['teh5'], $dta->NOUNDIAN);
    }


    $loop1 = $this->mdtc->getPemenang20(1,10,10);
    $loop2 = $this->mdtc->getPemenang20(1,10,20);
    $loop3 = $this->mdtc->getPemenang20(1,10,30);
    $loop4 = $this->mdtc->getPemenang20(1,10,40);
    $loop5 = $this->mdtc->getPemenang20(1,10,50);
    $loop6 = $this->mdtc->getPemenang20(1,10,60);
    $loop7 = $this->mdtc->getPemenang20(1,10,70);
    $loop8 = $this->mdtc->getPemenang20(1,10,80);
    $loop9 = $this->mdtc->getPemenang20(1,10,90);
    $loop10 = $this->mdtc->getPemenang20(1,10,100);
    $data['shaker1']=array();
    $data['shaker2']=array();
    $data['shaker3']=array();
    $data['shaker4']=array();
    $data['shaker5']=array();
    $data['shaker6']=array();
    $data['shaker7']=array();
    $data['shaker8']=array();
    $data['shaker9']=array();
    $data['shaker10']=array();


    foreach ($loop1 as $dta) {
      array_push($data['shaker1'], $dta->NOUNDIAN);
    }
    foreach ($loop2 as $dta) {
      array_push($data['shaker2'], $dta->NOUNDIAN);
    }
    foreach ($loop3 as $dta) {
      array_push($data['shaker3'], $dta->NOUNDIAN);
    }
    foreach ($loop4 as $dta) {
      array_push($data['shaker4'], $dta->NOUNDIAN);
    }
    foreach ($loop5 as $dta) {
      array_push($data['shaker5'], $dta->NOUNDIAN);
    }

    foreach ($loop6 as $dta) {
      array_push($data['shaker6'], $dta->NOUNDIAN);
    }
    foreach ($loop7 as $dta) {
      array_push($data['shaker7'], $dta->NOUNDIAN);
    }
    foreach ($loop8 as $dta) {
      array_push($data['shaker8'], $dta->NOUNDIAN);
    }
    foreach ($loop9 as $dta) {
      array_push($data['shaker9'], $dta->NOUNDIAN);
    }
    foreach ($loop10 as $dta) {
      array_push($data['shaker10'], $dta->NOUNDIAN);
    }

    $data['main_view'] = 'text2';

    $this->load->view('backend/dtc/text', $data);
  }
}

?>
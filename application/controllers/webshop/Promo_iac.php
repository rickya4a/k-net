<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 *
 */
class Promo_iac extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("webshop/m_promo_rekrut", "promo");
		$this->load->service("webshop/Member_service",'memberService');
		$this->load->model("backend/klinkpromo/m_iec_promo", "iec_promo");
	}

	//$route['iec'] = 'webshop/promo_rekrut/promo_iec/listIec';
	public function listIac() {
		$login = $this->session->userdata('store_info');
		if ($login==null){
			$x['idmember'] = 0;
		}
		else
			$x['idmember'] = getUserID();
		$x['prodCat'] = $this->memberService->getProductCategory();
		$x['pemenang'] = $this->iec_promo->getPemenang($x['idmember']);
		$x['result'] = $this->iec_promo->getIAC();

		$this->setTempWebShop('webshop/promoIac2',$x);

		//$this->load->model("webshop/m_promo_rekrut", "promo");
//		if($this->_checkSessionStoreUser()) {
//			$x['idmember'] = getUserID();
//			$x['flag'] = '2';
//			$x['prodCat'] = $this->memberService->getProductCategory();
//			$x['header'] = $this->iec_promo->headerIEC($x['idmember']);
//			$x['result'] = $this->iec_promo->totalSummaryIEC($x['idmember']);
//			$this->setTempWebShop('webshop/promoIec',$x);
//		}
	}



	public function getListIec() {
		$this->load->service("webshop/Member_service",'memberService');
		$this->load->model("webshop/m_promo_rekrut", "promo");
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			//$dt['listRecruit'] = $this->memberService->getListRecruitLandingPage(getUserID(), "kiv");
			$dt['res'] = $this->promo->getListIec(getUserID());
			$this->setTempWebShop('webshop/promoIec',$dt);
		}
	}



	function cetak(){
		if($this->_checkSessionStoreUser())
		{
			$idmember = getUserID();


			$dt['res1'] = $this->iec_promo->showDataPrint($idmember);


			$this->load->view('webshop/printFormIAC',$dt);
		}
	}



	function cetak2(){
		if($this->_checkSessionStoreUser())
		{
			$idmember = getUserID();


			$dt['res1'] = array(1);


			$this->load->view('webshop/printFormIAC',$dt);
		}
	}

	public function getListIecRes() {
		//$this->load->service("webshop/Member_service",'memberService');

		$data = $this->input->post(NULL, TRUE);
		//print_r($data);
		$arr = $this->promo->getListIec($data['period'], $data['recruitid']);
		/*$i = 1;
		$more400 = 0;
		$less400 = 0;
		$res = null;*/

		$this->load->view('webshop/iecListByID',$data);
		/*if($arr != null) {
			
			$res = "
					
					<div class=\"col-lg-12 col-xs-12 cart_header\">  
		            	<div class=\"col-lg-1 col-xs-1\">No</div>
		                <div class=\"col-lg-2 col-xs-2\">ID Member</div>
		                <div class=\"col-lg-3 col-xs-2\">Nama Member</div> 
		                <div class=\"col-lg-1 col-xs-2\">Tgl Join</div>
		                <div class=\"col-lg-1 col-xs-1\">BV</div>  
		             </div>
		             <div class=\"col-lg-12 col-xs-12 cart_content_noborder\" style=\"height:300px; overflow-y: auto;\">";
					 
			foreach($arr as $dtas) {
							
				$totbv = $dtas->totbv;
				if($totbv >= 400){
					$more400 = $more400 + 1;	
				}else{
					$less400 = $less400 + 1;
				}
				
				$res = $res."<div class=\"col-lg-12 col-xs-12 cart_content_thin text-center\" >
				        <div class=\"col-lg-1 col-xs-1\">$i</div>
						<div class=\"col-lg-2 col-xs-2\">$dtas->dfno</div>
		                <div class=\"col-lg-3 col-xs-2\">$dtas->fullnm</div>
		                <div class=\"col-lg-1 col-xs-2\">$dtas->jointdt</div>  
		                <div class=\"col-lg-1 col-xs-1\">$dtas->totbv</div>
		              </div>";
			   $i++;		  
			}
			
			$i = $i - 1;
			
			$resSumm = "<div class=\"col-lg-3 col-xs-3\">Total Rec = $i </div>
                  <div class=\"col-lg-4 col-xs-4\">400 BV (Above) = $more400</div>
                  <div class=\"col-lg-4 col-xs-4\">0 s/d 399 BV = $less400</div>";
			echo $resSumm."</div>".$res;
		} else {
			
		}*/
	}
}
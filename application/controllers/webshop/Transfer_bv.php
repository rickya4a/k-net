<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Sales
 *  *
 * @author ananech
 */
class Transfer_bv extends MY_Controller {

    public function __construct() {
        parent::__construct();
				$this->load->model("webshop/Sales_model",'salesM');
        $this->load->service("webshop/Product_service",'productService');
        $this->load->service("webshop/Sales_service",'salesService');
        $this->load->service("webshop/Member_service",'memberService');
        $this->folderView = "webshop/transferbv/"; 

    }
	
	//$route['sales/pindahbv'] = 'webshop/transfer_bv/transferBvForm';
	public function transferBvForm() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->productService->getProductCategory();
            //$dt['bonusmont'] = $this->salesService->getBonusMonth();
            //print_r($dt['listBank']);
			$this->load->model("webshop/M_transfer_bv", "bv");
			$dt['period'] = $this->bv->getPeriodForUpdateBv();
			$this->setTempWebShop($this->folderView.'transferBv', $dt);
		}
	}
	
	//$route['sales/pindahbv/search'] = 'webshop/transfer_bv/listTransaction';
	public function listTransaction() {
		if($this->_checkSessionStoreUser()) {
            //$data = $this->input->post(NULL, TRUE);
            $dt['bnsmonth'] = $this->input->post('bnsmonth');
            $dt['bln'] = substr($dt['bnsmonth'], 5, 2);
			$dt['thn'] = substr($dt['bnsmonth'], 0, 4);
			//print_r($dt);
			/*$tahun = $this->input->post('tahun');
            $dt['bln'] = $this->input->post('bulan');
            $dt['thn'] = $this->input->post('tahun');*/
            $dt['cekTrans'] = $this->salesService->getSalesMember($dt['bln'], $dt['thn']);
            $this->load->view($this->folderView.'transferBvSalesList',$dt);
        }
	}
	
	//$route['sales/pindahbv/showfullnm'] = 'webshop/transfer_bv/showFullNameById';
	public function showFullNameById() {
		$data = $this->input->post(NULl, TRUE);
		$this->load->model("webshop/M_transfer_bv", "bv");
		$hasil = $this->bv->showFullNameById("dfno", $data['new_idmember']);
		$checkDownline = $this->bv->checkDownline($data['new_idmember'], getUserID());

		// var_dump($checkDownline[0]->hasil);
		$res = jsonFalseResponse("ID Member salah / Invalid / Termination");
		if($hasil != NULL && $checkDownline != NULL){
		    if($hasil[0]->status != "1") {
				$res = jsonFalseResponse("Status Member Inactive");	
			}else if($hasil[0]->fullnm == "TERMINATION" || $hasil[0]->fullnm == "RESIGNATION") {
				$res = jsonFalseResponse("Member sudah TERMINATION / RESIGNATION");
			} else if($checkDownline[0]->hasil == '0') {
				$res = jsonFalseResponse("Tidak satu jaringan dengan Anda");
			}
			 else {
				$res = jsonTrueResponse($hasil, "ada");
			}
		}
		echo json_encode($res);	
		
	}
	
	//$route['sales/pindahbv/form'] = 'webshop/transfer_bv/formUpdateBv';
	public function formUpdateBv() {
		$data = $this->input->post(NULL, TRUE);
		if($this->_checkSessionStoreUser()) {

            //echo "isinya ".$trcd." - ".$orderno." - ".$tipe." ";
            $tes = substr($data['trx'], 0, 3);
			//IDI =  invoice
			
			$this->load->model("webshop/M_transfer_bv", "bv");
			if($tes == "IDI") {
				$tipe = "1";
			} else {
				$tipe = "2";
			}
			
			$dt['tipe'] = $tipe;
			$dt['hasil'] = $this->bv->checkSalesMemberByTrx($data['trx'], $tipe);
			//print_r($hasil);
			$dt['captcha_img'] = $this->salesService->createChaptcha();
			$this->load->view($this->folderView.'formUpdateBv',$dt);
			//<input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="Sales.backToSearch();" name="submit" id="submit"/>
			} else {
			echo "<script>alert('Silahkan login kembali..')</alert>";
		}
	}
	
	//$route['sales/pindahbv/update'] = 'webshop/transfer_bv/updateBv';
	public function updateBv() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			$this->load->model("webshop/M_transfer_bv", "bv");
			// $captcha = $this->bv->captchaCheck($data['captcha']);
			$check = $this->bv->checkLastProcessBonus($data['notrx']);
			
			/* if ($captcha['response']) {
				unlink($_SERVER['DOCUMENT_ROOT'] . '/captcha/'.$captcha['word'].'.jpg'); */
				if($check['response'] == "true") {
					$secret_key = "6LdNPJoUAAAAALvjN5iIBEJoIBSkNFhUfFS9ChhJ";
					$verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$data['g-recaptcha-response']);
					$response = json_decode($verify);
					$checkValidmember = $this->bv->checkValidmember($data['new_idmember']);
					$checkDownline = $this->bv->checkDownline($data['new_idmember'], getUserID());
					if($checkValidmember != null && $checkDownline[0]->hasil == "1" && $response->success == '1') {
						$arr = array(
							"updateby" => getUserID(),
							"idmember" => $data['idmember'],
							"nmmember" => $data['nmmember'],
							"new_idmember" => $data['new_idmember'],
							"new_nmmember" => $data['new_nmmember'],
							"notrx" => $data['notrx'],
							"tipetrx" => $data['tipetrx'],
						);
						$data['hasilUpdate'] = $this->bv->updateBV($arr);
						if($data['hasilUpdate'] == true) {
							$resJson = jsonTrueResponse(null, "BV berhasil di update dari $data[idmember] ke $data[new_nmmember], silahkan ke form sebelum nya lalu klik tombol cek transaksi kembali..");
						} else {
							$resJson = jsonFalseResponse("BV gagal di update..");
						}
					} elseif ($response->success == '0') {
						$message = "Lengkapi CAPTCHA";
						$resJson = jsonFalseResponse($message);
					} else {
						$message = "ID member yang akan di update tidak valid";
						$resJson = jsonFalseResponse($message);
					}
				} else {
					$message = $check['message'];
					$resJson = jsonFalseResponse($message);
				}
			/* } else {
				return NULL;
			} */
			echo json_encode($resJson);
		} else {
			echo "<script>alert('Silahkan login kembali..')</script>";
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cartshop_winner extends MY_Controller {
    		
    public function __construct() {
	    parent::__construct();
		//header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		
		$this->load->service("webshop/Product_service",'productService');
		$this->folderView = "webshop/sales_winner/";
		//header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		//$this->load->library('nativesession');
	}
	
	//$route['quiz/winner/(:any)'] = 'webshop/cartshop_customer/shopQuizWinner/$1';
    public function shopQuizWinner($no_voucher) {
    	//$dt['prdnm'] = $this->input->post("name");
    	/*$this->session->unset_userdata('non_member_info');
		$this->session->unset_userdata('order_payment');
		$this->session->unset_userdata('personal_info');
		$this->session->unset_userdata('trxtype');
		//$this->session->unset_userdata('pricecode');
		//$this->session->unset_userdata('cart_contents');
		$this->session->unset_userdata('shipping_jne_info');
		$this->session->unset_userdata('sender_address');
		$this->session->unset_userdata('destination_address');
		$this->session->unset_userdata('jne_branch');
		$this->session->unset_userdata('pay_sgo_id');*/
		$this->session->unset_userdata('pay_sgo_id');
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProductShop();
		
		//$memb = $this->productService->getMemberInfo($idmember);
		//print_r($memb);
        //$dt['prodCat'] = $this->cartshopService->getProductCategory();
        $this->load->service("webshop/Cartshop_service",'cartshopService');
        $dt['listVoucher'] = $this->cartshopService->getListValidVoucherPemenang($no_voucher);
		//print_r( $dt['listVoucher']);
		if($dt['listVoucher']['response'] == "true") {
			$dt['idmember'] = "0000999";
			$dt['nmmemb'] = "KIV CODE";
			$dt['no_voucher'] = $no_voucher;
	        $dt['banner'] = $this->productService->getHeaderBanner();
			//session_start();
			$this->load->service("webshop/Login_service",'loginService');
		    $promo = $this->loginService->checkListPromo();
		    //print_r($promo);
		    //$dt['listVoucher'] = $this->cartshopService->getListValidVoucherPemenang($personal_info['id_lp']);
		  
		    $this->session->set_userdata('promo', $promo);	
			$dt['link_home'] = 'quiz/winner/'.$no_voucher;	
		//print_r($dt);
		   $this->setTempWebShopLPSalesNoBV($this->folderView.'shop_winner_no_bv', $dt);
		} else {
			echo "<h3>Maaf, voucher ini sudah tidak berlaku lagi..</h3>";
		}
		
    }
	
	//$route['quiz/winner/cart/list'] = 'webshop/cartshop_winner/listCart';
	public function listCart() {
		    $this->load->service("webshop/Cartshop_service",'cartshopService');
		    $no_voucher = $this->input->post('no_voucher');
			//echo "No Voucher : ".$no_voucher;
		    $dt['formAction'] = "".site_url('shipping/postShipping')."";
            //$dt['prodCat'] = $this->cartshopService->getProductCategory();
            $dt['listCargo'] = $this->cartshopService->getListCargo();
            //$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
			//$dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb();
			//$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();  
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			//print_r($dt['listCargo']);
			$personal_info = $this->session->userdata('personal_info');
		    $dt['id_memb'] = $this->input->post('idmember');
			$dt['idmember'] = $this->input->post('idmember');
			if($no_voucher == "" || $no_voucher == null) {
				$dt['no_voucher'] = $personal_info['id_lp'];
				//echo "sdsd";
			} else {
				$dt['no_voucher'] = $no_voucher;
				//echo "sdsdsdsds";
			}
			
			$dt['id_lp'] = $no_voucher;
			$dt['nmmemb'] = $this->input->post('nmmemb');
		    $dt['cart'] = $this->cart->contents(); 
            
			
			
			$dt['link_home'] = 'quiz/winner/'.$no_voucher;	
			if($personal_info != null) {
				$dt['id_memb'] = $personal_info['idmemberx'];
				$dt['idmember'] = $personal_info['idmemberx'];
				$dt['no_voucher'] = $no_voucher;
				$dt['id_lp'] = $no_voucher;
				$dt['nmmemb'] = $personal_info['membername'];
				$dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);	
				//print_r($dt['shipping']['kota']);
				$this->setTempWebShopLPSalesNoBV($this->folderView.'shippingWithCartData2Dev',$dt);
			} else {
				
					$this->setTempWebShopLPSalesNoBV($this->folderView.'shipping2Dev',$dt);
				
			} 
			//$personal_info = $this->session->userdata('personal_info');
			//print_r($personal_info);
	}

	//$route['quiz/winner/checkout'] = 'webshop/shipping_customer/CheckoutCart';
	function CheckoutCart() {
		$this->load->service("webshop/Cartshop_service",'cartshopService');
        $data = $this->input->post(NULL, TRUE);
		
		//print_r($data);
		if($this->cartshopService->checkShippingData($data)) {
		    $dt['cart'] = $this->cart->contents();
			if($data != null) {
				
				$eerr = $this->updateCartBeforeProceed();
				$prdx = set_list_array_to_stringCart($dt['cart'], "id");
				
				
				$reg = $this->cartshopService->registerCustomerNonMember($data);
				$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
				$ship = $this->cartshopService->setShippingInfoBaru();	
				
				$personal_info = $this->session->userdata('personal_info');
				//print_r($personal_info);
				$dt['listVoucher'] = $this->cartshopService->getListValidVoucherPemenang($personal_info['id_lp']);
				//print_r($dt['listVoucher']);
                //$dt['prodCat'] = $this->cartshopService->getProductCategory();
                //print_r($reg);
				//$dt['listBank'] = $this->cartshopService->getBank();
				$dt['listBank'] = $this->cartshopService->getBankNonVABaru();
				//$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
				//production
				$dt['key'] = '23536c365506cdaa587fb9c1833f23f0';	
				//$dt['key'] = '23536c365506cdaa587fb9c1833f23f0';	
                //development
                //$dt['key'] = '0df5835ee198d49944c372ead860c241';
				
				
				$dt['id_memb'] = $personal_info['idmemberx'];
				$dt['idmember'] = $personal_info['idmemberx'];
				$dt['no_voucher'] = $personal_info['id_lp'];
				$dt['id_lp'] = $personal_info['id_lp'];
				$dt['nmmemb'] = $personal_info['membername'];
				$dt['link_home'] = 'quiz/winner/'.$personal_info['id_lp'];		
				$pay_sgo_id = $this->session->userdata("pay_sgo_id");
				if($pay_sgo_id == null || !isset($pay_sgo_id)) {
					//$dt['payID'] = "EC".randomNumber(8);
					$dt['payID'] = "EN".randomNumber(8);
					$this->session->set_userdata("pay_sgo_id", $dt['payID']);
					$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				} else {
					$dt['payID'] = $this->session->userdata("pay_sgo_id");
					$del = $this->cartshopService->deleteTempTrxSGO($dt['payID']);
					$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				}
				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['payID'];
				//print_r($dt);
			}
			
			if($data['delivery'] == "2" && $ship != 1 && $ship == null) {
				
				/*echo "<script>
					window.location.href='list';
					alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
					</script>"; */
					echo "ship : ".$ship;
					echo "<br />";
					echo "delivery : ".$data['delivery'];
			} else {
				//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				//$this->setTemplateDevSGO($this->folderView.'payment_sgo',$dt);
				$this->setTempWebShopLPSalesNoBV($this->folderView.'payment_sgo_vch',$dt);
			}
			
		} else {
			//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
			//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
			//redirect('cart/list');
			echo "<script>
					window.location.href='list';
					alert('Mohon data dilengkapi dahulu..');
					</script>"; 
		}	
   }   

	//$route['quiz/winner/sgo/pay/preview'] = 'webshop/cartshop_winner/payPreview';
	function payPreview() {
		$this->load->service("webshop/Cartshop_service",'cartshopService');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		$promo = $this->session->userdata('promo');
		$dt = $this->input->post(NULL, TRUE);
		$tot_pay = getTotalPayNet();
		
			$payShip = $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
			$shipDiscount = $shipping_jne_info['ship_discount'];
			$biaya = getTotalPayNetBaru();
		    $tot_hrs_dibayar = $biaya['total_pay'] + $payShip;
		    $dt['freeship'] = $biaya['freeship'];
		$dt['id_memb'] = $personal_info['idmemberx'];
				$dt['idmember'] = $personal_info['idmemberx'];
				$dt['no_voucher'] = $personal_info['id_lp'];
				$dt['id_lp'] = $personal_info['id_lp'];
				$dt['nmmemb'] = $personal_info['membername'];
				$dt['link_home'] = 'quiz/winner/'.$personal_info['id_lp'];		
		
		if(count($dt) > 0) {
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			$jml_voucher = isset($dt['shop_vch']) ? count($dt['shop_vch']) : 0;
			$dt['vch_stt'] = false;
			$dt['res'] = null;
			$dt['pay_gateway'] = true;
			$tot_nominal_vch = 0;
			//Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
			//atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar
			
			
			
			if($jml_voucher > 0) {
				$dt['vch_stt'] = true;
				$tempArr = array();
				$tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
				$this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $dt['temp_paymentIdx']);
				$xc = 0;
				for($i = 0; $i < $jml_voucher; $i++) {
					$xc++;
					$tot_nominal_vch += $dt['shop_vch_nominal'][$i];
					//$xc = $i + 1;
					$tbl .= "<tr>";
					$tbl .= "<td align=right>".$xc."&nbsp;</td>";
					$tbl .= "<td align=center>".$dt['shop_vch'][$i]."</td>";
					$tbl .= "<td align=right>Rp.".$dt['shop_vch_nominal'][$i]."&nbsp;</td>";
					$tbl .= "</tr>";
					$insAr = array(
					  "seqno" => $xc,
					  "orderno" => $dt['temp_paymentIdx'],
					  "paytype" => "vch",
					  "docno" => $dt['shop_vch'][$i],
					  "payamt" => $dt['shop_vch_nominal'][$i],
					  "bank_code_payment" => 19,
					  "charge_admin" => 0
					);
					$this->cartshopService->insertVoucherList($insAr);
					
				}
				$tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp.".$tot_nominal_vch."</td></tr>";
				$tbl .= "</table>";
				//Jika total nominal voucher lebih besar atau sama dengan
				//nominal belanja yang harus dibayar
				if($tot_nominal_vch >= $tot_hrs_dibayar) {
					//status payment gateway = false, data Langsung insert ke database tanpa pembayaran
					$dt['pay_gateway'] = false;
					$arr = array(
						"bank_code_payment" => 19,
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => 0,
						"payConnectivity" => 0,
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID()
					);
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					$dt['tot_hrs_dibayar'] = $tot_hrs_dibayar;
					
				} else {
				    //Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO
					$arr = array(
						"bank_code_payment" => $dt['bankid'],
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => $dt['charge_admin'],
						"payConnectivity" => $dt['charge_connectivity'],
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID()
					);
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					
					//Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
					$sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
					$seqno = $xc + 1;
					$insAr = array(
					  "seqno" => $seqno,
					  "orderno" => $dt['temp_paymentIdx'],
					  "paytype" => "sgo",
					  "docno" => "",
					  "payamt" => $sisaByr,
					  "bank_code_payment" => $dt['bankid'],
					  "charge_admin" => $dt['charge_admin']
					);
					$this->cartshopService->insertVoucherList($insAr);
					$dt['tot_hrs_dibayar'] = $sisaByr;
				}
			
				$dt['tot_nilai_voucher'] = $tot_nominal_vch;
				$dt['tot_pay'] = $tot_pay;
				$dt['listVch'] = $tbl;
			} else {
				$arr = array(
					"bank_code_payment" => $dt['bankid'],
					"payShip" => $shipping_jne_info['price'],
					"disc_shipping" => $shipDiscount,
					"payAdm" => $dt['charge_admin'],
					"payConnectivity" => $dt['charge_connectivity'],
					"sentTo" => $personal_info['delivery'],
					"userlogin" => getUserID()
			    );
				$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
				
				$updPaySGO = array(
					  "orderno" => $dt['temp_paymentIdx'],
					  "bank_code_payment" => $dt['bankid'],
					  "charge_admin" => $dt['charge_admin']
				);
				$this->cartshopService->updatePaydetSGO($updPaySGO);
				
				$dt['tot_pay'] = $tot_pay;
				$dt['tot_nilai_voucher'] = $tot_nominal_vch;
					
			}
									
			//if(getUserID() == "IDSPAAA66834") {
				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
				$this->setTempWebShopLPSalesNoBV($this->folderView.'payment_sgo_vch_preview_dev',$dt);
				
				//$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
				//$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
			/*} else {
				
					$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
				    $this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
				
				
			}	*/
		} else {
			redirect('cart/list', 'refresh');
		} 
	}

	function updateCartBeforeProceed() {
    	
    	$this->load->service("webshop/Cartshop_service",'cartshopService');
    		$arr = jsonFalseResponse(NULL, "Update Cart gagal..!");	
        	$data = $this->input->post(NULL, TRUE);	
        	$upd = $this->cartshopService->updateCart($data);
			$this->session->set_userdata('pricecode', $data['pricecode']);
			if($upd) {
				$arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
			} 
			//echo json_encode($arr);
			return $arr;	
		
    }

}
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * 
 */
class Ppob_payment extends MY_Controller {
		
		
		
	//put your code here
    public function __construct() {
    	parent::__construct();
    	//$this->load->service("webshop/Knet_payment_service",'paymentService');	
		$this->sgoPasswordAccess = 'k-net181183';
	}
	
	private function trxType($xx) {
		$no = substr($xx['order_id'], 0, 2);
		$dta = null;
		$cost = 0;
		$desc = null;
		$error_message = null;
		
		
		//Transaksi XL Co branded, SGO
		//PX = PPOB XL Co branded
		//PS = PPOB SGO Payment Gateway
	
		if($no == "PX") { 
			$this->load->model("webshop/m_ppob_trx", "hifi");
			$dta = $this->hifi->getDetailTempPPOBTrxByID($xx['order_id']);
			if($dta == null) {
				$error_message = "Record tidak ditemukan..";
			} else {
				$cost = $dta[0]->nominal;
				//$desc = "";	
				//$cost = $xx['amount'];
				$desc = "XL Co Branded Payment";
				$error_message = "XL Co Branded Payment success";
			}	
		} 
		else if($no == "PS") {
			$this->load->model("webshop/m_ppob_trx", "hifi");
			$dta = $this->hifi->getDetailTempPPOBTrxByID($xx['order_id']);
			if($dta == null) {
				$error_message = "Record tidak ditemukan..";
			} else {
				$cost = $dta[0]->nominal;
				$desc = "PPOB SGO Transaction";
				$error_message = "PPOB SGO Trx success";
			}	
		} 
		
		
		$arr = array("no" => $no, "dta" => $dta, "cost" => $cost, "desc" => $desc, "error_message" => $error_message);
		return $arr;
	}	


   /*-------------------------------
	 * PAYMENT GATEWAY
	 * DEVELOPMENT STAGE
	 * -----------------------------*/

    	//Send Inquiry to SGO
	public function getResponseFromSGO_dev() {
		//Retrieve POST Data
		$xx = $this->input->post(null, true);
		
		
		if(!$xx['order_id']) {
            die('04;Order ID Tidak Ada;;;;;');
        } else if($xx['password'] != $this->sgoPasswordAccess) {
            die('04;Autentifikasi Salah;;;;;');
        } else {
        	$arr = $this->trxType($xx);
        	if($arr['dta'] == null) {
        		die('04;Record tidak ditemukan;;;;;');
        	} else {
	            $trxData = array (
	                        'errorCode' => 0,
	                        'errorDesc' => 'Success',
	                        'orderId' => $xx['order_id'],
	                        //utk test 'amount' => 350000,
	                        'amount' => $arr['cost'],
	                        'ccy' => 'IDR',
	                        'desc' => $arr['desc'],
	                        'trx_date' => date('d/m/Y H:i:s')
	                        );
	                        
	            //echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . '. $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
				echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
			}
        }
	}
	
	//Receive Notif from SGO
	public function notifAfterPayment_dev() {	
		$xx = $this->input->post(null,true);
         //$password = 'k-net181183';
		$arr = $this->trxType($xx);	
		if($xx['amount'] != $arr['cost']) {   
            die('04,Total DP tidak sama,,,,,');
        }
        else if($xx['password'] != $this->sgoPasswordAccess) {
            die('04,Autentifikasi Salah,,,,,');
        } else {
        	if($no == "PX") {
				
			} else if($no == "PS") {
				
			}	 
		}	

		//print_r($xx);
		//Bila tipe pembayaran adalah Online Stockist Payment, maka tidak ada pengecekan jumlah yg harus dibayar
		$resSukses = array(
                'success_flag' => 0,
                'error_message' => $arr['error_message'],
                'reconcile_id' => rand(15,32),
                'order_id' => $xx['order_id'],
                'reconcile_datetime' =>date('Y-m-d H:i:s')
         );
		 
		 echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
		 //return $resSukses;
	}
	
	
	/*-------------------------------
	 * PAYMENT GATEWAY
	 * PRODUCTION STAGE
	 * -----------------------------*/
	 
	//Send Inquiry to SGO
	//$route['api/sgo/inquiry'] = 'webshop/api_payment/getResponseFromSGO';
	public function getResponseFromSGO() {
		//Retrieve POST Data
		$xx = $this->input->post(null, true);
		//Password to access SGO payment
		//$password = 'k-net181183';
		
		$arr = $this->trxType($xx);
		//print_r($xx);
		
		
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        } else if($xx['password'] != $this->sgoPasswordAccess){
            die('04;Autentifikasi Salah;;;;;');
        } else {
        	if($arr['dta'] == null) {
        		die('04;Record tidak ditemukan;;;;;');
        	} else {
	            $trxData = array (
	                        'errorCode' => 0,
	                        'errorDesc' => 'Success',
	                        'orderId' => $xx['order_id'],
	                        //utk test 'amount' => 350000,
	                        'amount' => $arr['cost'],
	                        'ccy' => 'IDR',
	                        'desc' => $arr['desc'],
	                        'trx_date' => date('d/m/Y H:i:s')
	                        );
	                        
	            //echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . '. $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
				echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
			}
        }
	}
	
	//Receive Notif from SGO
	//$route['api/sgo/notif'] = 'webshop/api_payment/notifAfterPayment';
	public function notifAfterPayment() {	
		$xx = $this->input->post(null,true);
         //$password = 'k-net181183';
		$arr = $this->trxType($xx);	
		//if($arr['no'] != "SC") {
			if($arr['dta'] == null) {
        		die('04;Record tidak ditemukan,,,,,');
        	} 
        	else if($xx['amount'] != $arr['cost']) {   
	            die('04,Total DP tidak sama,,,,,');
	        }
	        else if($xx['password'] != $this->sgoPasswordAccess) {
	            die('04,Autentifikasi Salah,,,,,');
	        } else {
	        	//if($xx['error_code'] == "0000" || $xx['error_code'] == "0") {
		        	//Bila transaksi adalah pembelanjaan online
			        if($arr['no'] == "EC" || $arr['no'] == "SC" || $arr['no'] == "EA") {
		            	$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
						$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);    
						$this->paymentService->sendTrxSMS2($resultInsert);
						sendNotifSalesTransaction($resultInsert);
		            } 	            
		            //Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
		            else if($arr['no'] == "EN") {
		            	$updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
		                $resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);    
						$this->paymentService->sendTrxSMSNonMember($resultInsert);	
		            }
		            //bila transaksi adalah Registration Member / Landing Page Registration
		            else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
		            	$updtTabel =$this->paymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
						$resultInsMemb = $this->paymentService->getNewMemberData($xx['order_id']);
						//$this->paymentService->createNewShoppingVoucher($resultInsMemb);
						$this->paymentService->sendTrxSmsRegMember($resultInsMemb);
						//$this->paymentService->sendWelcomeEmail($resultInsMemb);
						
		            } 
					//bila transaksi adalah Online Stockist Payment
					else if($arr['no'] == "ST") {
				
			        }
					
					//bila transaksi adalah Ticket Online  Payment
					else if($arr['no'] == "TA") {
						$this->load->model("dtc/mdtc_mbr", "mdtc");
						$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
			        }
		        //}		
			}	
		//}

		//print_r($xx);
		//Bila tipe pembayaran adalah Online Stockist Payment, maka tidak ada pengecekan jumlah yg harus dibayar
		$resSukses = array(
                'success_flag' => 0,
                'error_message' => $arr['error_message'],
                'reconcile_id' => rand(15,32),
                'order_id' => $xx['order_id'],
                'reconcile_datetime' =>date('Y-m-d H:i:s')
         );
		 
		 echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
		 //return $resSukses;
	}

	
}		
			
    	
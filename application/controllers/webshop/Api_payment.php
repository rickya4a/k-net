<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 *
 */
class Api_payment extends MY_Controller {



	//put your code here
	public function __construct() {
		parent::__construct();
		$this->load->service("webshop/Knet_payment_service",'paymentService');
		$this->sgoPasswordAccess = 'k-net181183';
	}

	public function trxType($xx) {
		$no = substr($xx['order_id'], 0, 2);
		$dta = null;
		$cost = 0;
		$desc = null;
		$error_message = null;


		//Transaksi Sales Member
		//EC = Ecommerce K-net web
		//SC = Ecommerce Web K-net yg baru
		//EA = Ecommerce K-net mobile apps
		if($no == "EC" || $no == "SC" || $no == "EA" || $no == "EK") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
				$desc = "K-NET E-Commerce";
				$error_message = "Transaction E-Commerce success";
			}
		} 
		else if($no == "EM") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
				$desc = "K-NET E-Commerce Plus Reg Member";
				$error_message = "Transaction E-Commerce plus Reg Member success";
			}
		}
		else if($no == "ED") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
				$desc = "K-NET Digital Product";
				$error_message = "Transaction Digital Product success";
			}
		}
		else if($no == "EP") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
				$desc = "K-NET Promo Mampir Kak E-Commerce";
				$error_message = "Transaction Promo Mampir Kak success";
			}
		}
		//Transaksi Sales Non Member / Landing Page
		else if($no == "EN") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
			$desc = "K-NET E-Commerce Non Member";
			$error_message = "Transaction Sales Non Member success";
		}
		//Transaksi Register Member
		//RM = Reg Member via k-net web
		//RC = Ecommerce Web K-net yg baru
		//RA = Reg Member via mobile apps
		else if($no == "RM" || $no == "RC" || $no == "RA" || $no == "RK") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
				$desc = "K-NET Registration Member";
				$error_message = "Register Member success";
			}
		}
		//Transaksi Register Member Landing Page
		else if($no == "LP") {
			$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
			$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
			$desc = "K-NET Registration Member Landing Page";
			$error_message = "Register Member LP success";
		}
		//Pembayaran Online Stockist
		else if ($no == "ST") {
			$dta = $this->paymentService->getStockistPayment($xx['order_id']);
			$cost = $dta[0]->total_pay_ssr + $dta[0]->charge_connectivity;
			//$desc = "";	
			//$cost = $xx['amount'];
			$desc = "Stockist Sales Payment";
			$error_message = "Stockist Sales Payment success";
		}
		//Pembayaran TIKET ONLINE By ALDI
		else if($no == "TA") {
			$this->load->model("dtc/mdtc_mbr", "mdtc");
			$dta = $this->mdtc->ReqSGO($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				$cost = $dta[0]->total_pay+ $dta[0]->payConnectivity;
				//$desc = "";	
				//$cost = $xx['amount'];
				$desc = "Tiket Online Payment";
				$error_message = "Tiket Online Payment success";
			}
		}
		//PS = PPOB SGO Payment Gateway
		else if($no == "PS") {
			//$this->load->model("webshop/m_ppob_trx", "hifi");
			$this->load->model("backend/be_hifi_model", "hifi");
			$dta = $this->hifi->getDetailTempPPOBTrxByID($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				//bila nilai nominal 102500.00 akan menjadi 10250000 (desimal .00 digabungkan)
				//$cost = $dta[0]->amount_sgo;
				//bila nilai nominal 102500.00 akan menjadi 102500
				$cost = $dta[0]->nominal;
				$desc = "PPOB SGO Transaction";
				$error_message = "PPOB SGO Trx success";
			}
		}

		//PX = Paket Data XL
		else if($no == "PX") {
			//$this->load->model("webshop/m_ppob_trx", "hifi");
			$this->load->model("backend/be_hifi_model", "hifi");
			$dta = $this->hifi->getDetailTempPPOBTrxByID($xx['order_id']);
			if($dta == null) {
				$error_message = "Record $xx[order_id] tidak ditemukan..";
			} else {
				//bila nilai nominal 102500.00 akan menjadi 10250000 (desimal .00 digabungkan)
				//$cost = $dta[0]->amount_sgo;
				//bila nilai nominal 102500.00 akan menjadi 102500
				$cost = $dta[0]->nominal;
				$desc = "Paket Data XL Co Branded";
				$error_message = "Paket Data XL Trx success";
			}
		}

		$arr = array("no" => $no, "dta" => $dta, "cost" => $cost, "desc" => $desc, "error_message" => $error_message);
		return $arr;
	}




	/*-------------------------------
      * PAYMENT GATEWAY
      * DEVELOPMENT STAGE
      * -----------------------------*/

	//Send Inquiry to SGO
	//$route['pay/sgo/inquiry/dev'] = 'webshop/api_payment/getResponseFromSGO_dev';
	public function getResponseFromSGO_dev() {
		//Retrieve POST Data
		$xx = $this->input->post(null, true);
		//Password to access SGO payment
		//$password = 'k-net181183';

		$arr = $this->trxType($xx);
		//print_r($xx);


		if(!$xx['order_id'])
		{
			die('04;Order ID Tidak Ada;;;;;');
		} else if($xx['password'] != $this->sgoPasswordAccess){
			die('04;Autentifikasi Salah;;;;;');
		} else {
			if($arr['dta'] == null) {
				$errMsg = $arr['error_message'];
				die('04;'.$errMsg.';;;;;');
			} else {

				/*add Vera @20.03.2019*/
				$arrBnk = $arr['dta'];
				$bankCodePayment = $arrBnk[0]->bank_code_payment;

				if($bankCodePayment == "32" || $bankCodePayment == "33" || $bankCodePayment == "34") {
					$trxData = array (
						'errorCode' => 0,
						'errorDesc' => 'Success',
						'orderId' => $xx['order_id'],
						//utk test 'amount' => 350000,
						'amount' => $arr['cost'],
						'ccy' => 'IDR',
						'desc' => $arr['desc'],
						'trx_date' => date('d/m/Y H:i:s'),
						'tes' => "",
						'cicilan' => "Y"
					);
					echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date']. ';' . $trxData['tes']. ';' . $trxData['cicilan'];
				} else {
					$trxData = array (
						'errorCode' => 0,
						'errorDesc' => 'Success',
						'orderId' => $xx['order_id'],
						//utk test 'amount' => 350000,
						'amount' => $arr['cost'],
						'ccy' => 'IDR',
						'desc' => $arr['desc'],
						'trx_date' => date('d/m/Y H:i:s')
					);
					echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
				}
				/*$trxData = array (
					'errorCode' => 0,
					'errorDesc' => 'Success',
					'orderId' => $xx['order_id'],
					//utk test 'amount' => 350000,
					'amount' => $arr['cost'],
					'ccy' => 'IDR',
					'desc' => $arr['desc'],
					'trx_date' => date('d/m/Y H:i:s')
				);

				//echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . '. $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
				echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];*/
			}
		}
	}





	//Receive Notif from SGO
	//$route['pay/sgo/notif/dev'] = 'webshop/api_payment/notifAfterPayment_dev';
	public function notifAfterPayment_dev() {
		$xx = $this->input->post(null,true);
		//$password = 'k-net181183';
		$arr = $this->trxType($xx);
		//if($arr['no'] != "SC") {
		if($xx['amount'] != $arr['cost']) {
			die('04,Total DP tidak sama,,,,,');
		}
		else if($xx['password'] != $this->sgoPasswordAccess) {
			die('04,Autentifikasi Salah,,,,,');
		} else {
			//if($xx['error_code'] == "0000" || $xx['error_code'] == "0") {
			//Bila transaksi adalah pembelanjaan online
			if($arr['no'] == "EC" || $arr['no'] == "SC" || $arr['no'] == "EA") {
				$this->load->model("webshop/Payment_model",'pmodel');
				//$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
				$updtTabel = $this->paymentService->updateTrxSGO_baruV2($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$this->paymentService->sendTrxSMS2($resultInsert);

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){
//					$dta['catalog']=$data_catalog;
//					$dta['token']=$xx['order_id'];
					emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
					//echo "<pre>";
					//print_r($data_catalog);
					//echo "</pre><br />";
					foreach($data_catalog as $row){
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $resultInsert[0]->tel_hp_login, $link);
						smsTemplate( $resultInsert[0]->tel_hp_as, $link);
					}

				}

				if($arr['no'] == "EA") {
					$this->blastNotif($resultInsert[0]->orderno);
				}
				if($arr['no']=="EC"){

					$count=$this->pmodel->getPertama($xx['order_id'],$resultInsert[0]->total_pay,$resultInsert[0]->total_bv,$resultInsert[0]->id_memb,$resultInsert[0]->tel_hp_login,$resultInsert[0]->orderno);
				}
				echo $this->setPaymentNotif($arr, $xx);

			} 
			else if($arr['no'] == "EM") {
				//transaksiKnetPlusMemb
				//$this->transaksiKnetPlusMemb($xx, $arr);
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					$this->transaksiKnetPlusMemb($xx, $arr);
				} else {
				    echo $this->setErrorDoubleId($xx['order_id']);
				}
			
			}
			elseif($arr['no'] == "ED") {
				$this->load->model("webshop/Payment_model",'pmodel');
				$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
				$updtTabel = $this->paymentService->updateTrxSGO_baruDIGITAL($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($xx['order_id']);

				$this->paymentService->sendTrxSMS2($resultInsert);

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){
//					$dta['catalog']=$data_catalog;
//					$dta['token']=$xx['order_id'];
					emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
//					echo $identitas[0]->email;

					//echo "<pre>";
					//print_r($data_catalog);
					//echo "</pre><br />";
					foreach($data_catalog as $row){
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $identitas[0]->telp_customer, $link);
//						echo $identitas[0]->telp_customer;
//						smsTemplate( $resultInsert[0]->tel_hp_as, $link);
					}

				}
				echo $this->setPaymentNotif($arr, $xx);
			}
			//Transaksi K-net Non Member "Mampir Kak"
			elseif($arr['no'] == "EP") {
				$this->transaksiMampirKak($xx, $arr);
			}
			//Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
			else if($arr['no'] == "EN") {
				$updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$this->paymentService->sendTrxSMSNonMember($resultInsert);
				echo $this->setPaymentNotif($arr, $xx);
			}
			//bila transaksi adalah Registration Member / Landing Page Registration
			else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
				$updtTabel =$this->paymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
				$resultInsMemb = $this->paymentService->getNewMemberData($xx['order_id']);
				//$this->paymentService->createNewShoppingVoucher($resultInsMemb);
				$this->paymentService->sendTrxSmsRegMember($resultInsMemb);
				//$this->paymentService->sendWelcomeEmail($resultInsMemb);
				sendWelcomeEmail($resultInsMemb);
				echo $this->setPaymentNotif($arr, $xx);

			}
			//bila transaksi adalah Online Stockist Payment
			else if($arr['no'] == "ST") {

			}

			//bila transaksi adalah Ticket Online  Payment
			else if($arr['no'] == "TA") {
				$this->load->model("dtc/mdtc_mbr", "mdtc");
				$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
				echo $this->setPaymentNotif($arr, $xx);
			}

			else if($arr['no'] == "PS") {
				$this->processPpobTrxSGO($xx, $arr);
			}

			else if($arr['no'] == "PX") {
				//step 1, insert all param sent by sgo
				$this->paymentService->processInsertParamSgo($xx);

				$this->processXlDataSGO($xx, $arr);	
			}
			//}
		}
		//}

		//print_r($xx);
		//Bila tipe pembayaran adalah Online Stockist Payment, maka tidak ada pengecekan jumlah yg harus dibayar

		//return $resSukses;
	}

	public function notifAfterPayment_dev2() {
		$this->load->model("webshop/Payment_model",'pmodel');

		$xx = $this->input->post(null,true);
		//$password = 'k-net181183';
		$arr = $this->trxType($xx);
		//if($arr['no'] != "SC") {
		if($xx['amount'] != $arr['cost']) {
			die('04,Total DP tidak sama,,,,,');
		}
		else if($xx['password'] != $this->sgoPasswordAccess) {
			die('04,Autentifikasi Salah,,,,,');
		} else {
			//if($xx['error_code'] == "0000" || $xx['error_code'] == "0") {
			//Bila transaksi adalah pembelanjaan online
			if($arr['no'] == "EC" || $arr['no'] == "SC" || $arr['no'] == "EA") {
				$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$this->paymentService->sendTrxSMS2($resultInsert);

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){
//					$dta['catalog']=$data_catalog;
//					$dta['token']=$xx['order_id'];
					emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
					//echo "<pre>";
					//print_r($data_catalog);
					//echo "</pre><br />";
					foreach($data_catalog as $row){
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $resultInsert[0]->tel_hp_login, $link);
						smsTemplate( $resultInsert[0]->tel_hp_as, $link);
					}

				}

				if($arr['no'] == "EA") {
					$this->blastNotif($resultInsert[0]->orderno);
				}
				if($arr['no']=="EC"){
					$count=$this->pmodel->getPertama($xx['order_id']);
				}
				echo $this->setPaymentNotif($arr, $xx);

			}
			elseif($arr['no'] == "ED") {
				$this->load->model("webshop/Payment_model",'pmodel');
				$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
				$updtTabel = $this->paymentService->updateTrxSGO_baruDIGITAL($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($xx['order_id']);

				$this->paymentService->sendTrxSMS2($resultInsert);

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){
//					$dta['catalog']=$data_catalog;
//					$dta['token']=$xx['order_id'];
					emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
//					echo $identitas[0]->email;

					//echo "<pre>";
					//print_r($data_catalog);
					//echo "</pre><br />";
					foreach($data_catalog as $row){
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $identitas[0]->telp_customer, $link);
//						echo $identitas[0]->telp_customer;
//						smsTemplate( $resultInsert[0]->tel_hp_as, $link);
					}

				}
				echo $this->setPaymentNotif($arr, $xx);
			}
			//Transaksi K-net Non Member "Mampir Kak"
			elseif($arr['no'] == "EP") {
				$this->transaksiMampirKak($xx, $arr);
			}
			//Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
			else if($arr['no'] == "EN") {
				$updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$this->paymentService->sendTrxSMSNonMember($resultInsert);
				echo $this->setPaymentNotif($arr, $xx);
			}
			//bila transaksi adalah Registration Member / Landing Page Registration
			else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
				$updtTabel =$this->paymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
				$resultInsMemb = $this->paymentService->getNewMemberData($xx['order_id']);
				//$this->paymentService->createNewShoppingVoucher($resultInsMemb);
				$this->paymentService->sendTrxSmsRegMember($resultInsMemb);
				//$this->paymentService->sendWelcomeEmail($resultInsMemb);
				sendWelcomeEmail($resultInsMemb);
				echo $this->setPaymentNotif($arr, $xx);

			}
			//bila transaksi adalah Online Stockist Payment
			else if($arr['no'] == "ST") {

			}

			//bila transaksi adalah Ticket Online  Payment
			else if($arr['no'] == "TA") {
				$this->load->model("dtc/mdtc_mbr", "mdtc");
				$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
				echo $this->setPaymentNotif($arr, $xx);
			}

			else if($arr['no'] == "PS") {
				$this->processPpobTrxSGO($xx, $arr);
			}

			else if($arr['no'] == "PX") {
				$this->processXlDataSGO($xx, $arr);
			}
			//}




		}
		//}

		//print_r($xx);
		//Bila tipe pembayaran adalah Online Stockist Payment, maka tidak ada pengecekan jumlah yg harus dibayar

		//return $resSukses;
	}

	/*-------------------------------
	 * PAYMENT GATEWAY
	 * PRODUCTION STAGE
	 * -----------------------------*/

	//Send Inquiry to SGO
	//$route['api/sgo/inquiry'] = 'webshop/api_payment/getResponseFromSGO';
	public function getResponseFromSGO() {
		//Retrieve POST Data
		$xx = $this->input->post(null, true);
		//Password to access SGO payment
		//$password = 'k-net181183';

		$arr = $this->trxType($xx);
		//print_r($xx);


		if(!$xx['order_id'])
		{
			die('04;Order ID Tidak Ada;;;;;');
		} else if($xx['password'] != $this->sgoPasswordAccess){
			die('04;Autentifikasi Salah;;;;;');
		} else {
			if($arr['dta'] == null) {
				die('04;Record tidak ditemukan;;;;;');
			} else {
				/*add Vera @20.03.2019*/
				$arrBnk = $arr['dta'];
				$bankCodePayment = $arrBnk[0]->bank_code_payment;

				if($bankCodePayment == "32" || $bankCodePayment == "33" || $bankCodePayment == "34") {
					$trxData = array (
						'errorCode' => 0,
						'errorDesc' => 'Success',
						'orderId' => $xx['order_id'],
						//utk test 'amount' => 350000,
						'amount' => $arr['cost'],
						'ccy' => 'IDR',
						'desc' => $arr['desc'],
						'trx_date' => date('d/m/Y H:i:s'),
						'tes' => "",
						'cicilan' => "Y"
					);
					echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date']. ';' . $trxData['tes']. ';' . $trxData['cicilan'];
				} else {
					$trxData = array (
						'errorCode' => 0,
						'errorDesc' => 'Success',
						'orderId' => $xx['order_id'],
						//utk test 'amount' => 350000,
						'amount' => $arr['cost'],
						'ccy' => 'IDR',
						'desc' => $arr['desc'],
						'trx_date' => date('d/m/Y H:i:s')
					);
					echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
				}
				/*$trxData = array (
					'errorCode' => 0,
					'errorDesc' => 'Success',
					'orderId' => $xx['order_id'],
					//utk test 'amount' => 350000,
					'amount' => $arr['cost'],
					'ccy' => 'IDR',
					'desc' => $arr['desc'],
					'trx_date' => date('d/m/Y H:i:s')
				);
				//echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . '. $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
				echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
			*/
			}
		}
	}

	//Receive Notif from SGO
	//$route['api/sgo/notif'] = 'webshop/api_payment/notifAfterPayment';
	public function notifAfterPayment() {
		$xx = $this->input->post(null,true);
		//$password = 'k-net181183';
		$arr = $this->trxType($xx);
		//if($arr['no'] != "SC") {
		if($arr['dta'] == null) {
			die('04;Record tidak ditemukan,,,,,');
		}
		else if($xx['amount'] != $arr['cost']) {
			die('04,Total DP tidak sama '.$xx['amount'].' dan '.$arr['cost'].',,,,,');
		}
		else if($xx['password'] != $this->sgoPasswordAccess) {
			die('04,Autentifikasi Salah,,,,,');
		} else {
			//if($xx['error_code'] == "0000" || $xx['error_code'] == "0") {
			//Bila transaksi adalah pembelanjaan online
			if($arr['no'] == "EC" || $arr['no'] == "SC" || $arr['no'] == "EA") {
			    /*$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
					$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
					$this->paymentService->sendTrxSMS2($resultInsert);
					sendNotifSalesTransaction($resultInsert);
					echo $this->setPaymentNotif($arr, $xx);
				} else {
				    echo $this->setErrorDoubleId($xx['order_id']);
				}*/
				
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					$this->load->model("webshop/Payment_model",'pmodel');
					//step 1, insert all param sent by sgo
					$this->paymentService->processInsertParamSgo($xx);

					$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
					$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
					$this->paymentService->sendTrxSMS2($resultInsert);
					sendNotifSalesTransaction($resultInsert);

					$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
					if($data_catalog != null) {

						emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
						foreach($data_catalog as $row){
							$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
							smsTemplate( $resultInsert[0]->tel_hp_login, $link);
							smsTemplate( $resultInsert[0]->tel_hp_as, $link);
						}

					} 
					/*
					if($arr['no'] == "EA") {
						$this->blastNotif($resultInsert[0]->orderno);
					}
					*/
					
					//berlaku hanya untuk 1 september 2018 - 30 november 2018
					if($arr['no']=="EC"){

						$count=$this->pmodel->getPertama($xx['order_id'],$resultInsert[0]->total_pay,$resultInsert[0]->total_bv,$resultInsert[0]->id_memb,$resultInsert[0]->tel_hp_login,$resultInsert[0]->orderno);
					}
					echo $this->setPaymentNotif($arr, $xx); 
				} else {
				    echo $this->setErrorDoubleId($xx['order_id']);
				}	
				
			}
			//Bila transaksi adalah pembelanjaan knet plus registrasi member
			else if($arr['no'] == "EM") {
				//transaksiKnetPlusMemb
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					//step 1, insert all param sent by sgo
					$this->paymentService->processInsertParamSgo($xx);

					$this->transaksiKnetPlusMemb($xx, $arr);
				} else {
				    echo $this->setErrorDoubleId($xx['order_id']);
				}
			
			}
			//Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
			else if($arr['no'] == "EN") {
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					//step 1, insert all param sent by sgo
					$this->paymentService->processInsertParamSgo($xx);

					$updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
					$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
					$this->paymentService->sendTrxSMSNonMember($resultInsert);
					echo $this->setPaymentNotif($arr, $xx);
				} else {
				  echo $this->setErrorDoubleId($xx['order_id']);	
				}	
			}
			//bila transaksi adalah Registration Member / Landing Page Registration
			else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					//step 1, insert all param sent by sgo
					$this->paymentService->processInsertParamSgo($xx);

					$updtTabel =$this->paymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
					if($updtTabel['response'] == "true") {
						$resultInsMemb = $this->paymentService->getNewMemberData($xx['order_id']);
						//$this->paymentService->createNewShoppingVoucher($resultInsMemb);
						$this->paymentService->sendTrxSmsRegMember($resultInsMemb);
						//$this->paymentService->sendWelcomeEmail($resultInsMemb);
						sendWelcomeEmail($resultInsMemb);
						echo $this->setPaymentNotif($arr, $xx);
					} else {
						//echo json_encode($updtTabel);
						$resSukses = array(
						'success_flag' => 1,
						'error_message' => $updtTabel['message'],
						'error_code' => "003",
						'order_id' => $xx['order_id'],
						'reconcile_datetime' =>date('Y-m-d H:i:s')
						);
						echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
					}	
				} else {
				  echo $this->setErrorDoubleId($xx['order_id']);	
				}	
			}
			elseif($arr['no'] == "ED") {
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					//step 1, insert all param sent by sgo
					$this->paymentService->processInsertParamSgo($xx);

					$this->load->model("webshop/Payment_model",'pmodel');
					$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
					$updtTabel = $this->paymentService->updateTrxSGO_baruDIGITAL($xx['order_id']);
					$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
					$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($xx['order_id']);

					$this->paymentService->sendTrxSMS2($resultInsert);

					$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
					if($data_catalog != null){

						emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
						foreach($data_catalog as $row) {
							$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
							smsTemplate( $identitas[0]->telp_customer, $link);
						}

					}
					echo $this->setPaymentNotif($arr, $xx);
				} else {
				  echo $this->setErrorDoubleId($xx['order_id']);	
				}		
			}

			//Transaksi K-net Non Member "Mampir Kak"
			else if($arr['no'] == "EP") {
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					$this->transaksiMampirKak($xx, $arr);
				} else {
				  echo $this->setErrorDoubleId($xx['order_id']);	
				}			
			}
			
			//bila transaksi adalah Online Stockist Payment
			else if($arr['no'] == "ST") {

			}

			//bila transaksi adalah Ticket Online  Payment
			else if($arr['no'] == "TA") {
				//$createIP = $this->mdtc->generateIP($xx['order_id']);
				//print_r($createIP);
				
				$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
				if($double == null) {
					//step 1, insert all param sent by sgo
					$this->paymentService->processInsertParamSgo($xx);

					$this->load->model("dtc/mdtc_mbr", "mdtc");
					$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
					$createIP = $this->mdtc->generateIP($xx['order_id']);
					echo $this->setPaymentNotif($arr, $xx);
				} else {
				  echo $this->setErrorDoubleId($xx['order_id']);	
				} 
			}
		
			else if($arr['no'] == "PS") {
				$this->processPpobTrxSGO($xx, $arr);
			}

			else if($arr['no'] == "PX") {
				$this->processXlDataSGO($xx, $arr);	
			}


		}
		//}


	}
	
	

	
	function blastNotif($orderno) {
		$this->load->model('M_mobile_api', 'm_mobile_api');
	  	//"IDEC1803000037"   
		$mdRes = $this->m_mobile_api->blastNotifByOrderId($orderno);
		if ($mdRes != null) {
			$postData = array(
			  "app_id"=> "16385089-6e35-4778-b602-c8e5e03f6634",
			  "include_player_ids" => $mdRes,
			  "data" => array("orderId" => $orderno),
			  //"headings" => array("en" =>"Title English"),
			  //"subtitle"=> array("en"=> "Subtitle English"),
			  //"contents"=> array("en"=> "Message English"),
			  //"ttl" => "1800"
			  
			  "headings" => array("en" =>"New order"),
			  //"subtitle"=> array("en"=> "Subtitle English"),
			  "contents"=> array("en"=> "Open to see detail"),
			  "ttl" => "86400"
			);
			$arr = array(
			  "url" => "https://onesignal.com/api/v1/notifications",
			  "method" => "POST",
			  "postdata" => json_encode($postData)
			);
			$notif = blastNotifCurl($arr);
			return $notif;
		}
	}
	
	function transaksiKnetPlusMemb($xx, $arr) {
		$this->load->model("webshop/Payment_model",'pmodel');
		$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
		$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
		if($resultInsert != null) {
			$insMemb = $this->paymentService->transaksiKnetPlusMemb($xx['order_id'], $resultInsert);

			$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
			if($data_catalog != null){
	//					$dta['catalog']=$data_catalog;
	//					$dta['token']=$xx['order_id'];
				emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
				//echo "<pre>";
				//print_r($data_catalog);
				//echo "</pre><br />";
				foreach($data_catalog as $row){
					$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
					smsTemplate( $resultInsert[0]->tel_hp_login, $link);
					smsTemplate( $resultInsert[0]->tel_hp_as, $link);
				}

			}
			
			if($resultInsert != null) {
				//$this->paymentService->sendTrxSMS2($resultInsert);
				$this->paymentService->sendSMSPromoSalesAndReg($resultInsert);
			} 
			
			if($insMemb == "true") {
				$this->paymentService->getRegMemberKnetPlus($resultInsert[0]->orderno);
			}
			echo $this->setPaymentNotif($arr, $xx);
		} else {
			$resSukses = array(
				'success_flag' => 1,
				'error_message' => "Trx K-net plus Reg member Gagal",
				'error_code' => "110",
				'order_id' => $xx['order_id'],
				'reconcile_datetime' =>date('Y-m-d H:i:s')
			);
			$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
	        die($fail);
		}
	}

	function transaksiMampirKak($xx, $arr) {
		$hasil = $this->paymentService->processTrxMampirKak($xx['order_id']);
		$resultInsert = $this->paymentService->getInsertTrxMampirKak($xx['order_id']);
		//print_r($resultInsert);
		if($resultInsert != null) {
			if($resultInsert[0]->new_member_reg != null && $resultInsert[0]->new_member_reg != "") {
		  	  $this->paymentService->sendSmsRegMembMampirKak($resultInsert);
			}	
		  $this->paymentService->sendTrxSmsMampirKak($resultInsert);
		  echo $this->setPaymentNotif($arr, $xx);
		} else {
			//$fail = $this->setPaymentNotifFail("Trx failed to process", $xx);
			$resSukses = array(
				'success_flag' => 1,
				'error_message' => "Trx Mampirkak Gagal",
				'error_code' => "110",
				'order_id' => $xx['order_id'],
				'reconcile_datetime' =>date('Y-m-d H:i:s')
			);
			$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
	        die($fail);
		}
	}

	private function setPaymentNotif($arr, $xx) {
		$resSukses = array(
			'success_flag' => 0,
			'error_message' => $arr['error_message'],
			'reconcile_id' => rand(15,32),
			'order_id' => $xx['order_id'],
			'reconcile_datetime' =>date('Y-m-d H:i:s')
		);
		$hasil = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
		return $hasil;
	}

	private function setPaymentNotifFail($hasil2, $xx) {
		$resSukses = array(
			'success_flag' => 1,
			'error_message' => $hasil2->error_desc,
			'error_code' => $hasil2->error_code,
			'order_id' => $xx['order_id'],
			'reconcile_datetime' =>date('Y-m-d H:i:s')
		);
		$hasil = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
		return $hasil;
	}
	
	private function setErrorDoubleId($token) {
		$resSukses = array(
			'success_flag' => 1,
			'error_message' => "Double Transaction Exist $token",
			'error_code' => "002",
			'order_id' => $token,
			'reconcile_datetime' =>date('Y-m-d H:i:s')
		);
		$hasil = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
		return $hasil;
	}
	
	
	
	function processPpobTrxSGO($xx, $arr) {
		$this->load->model("backend/be_hifi_model", "hifi");
		$resultInsert = $this->hifi->getPpobTrxById($xx['order_id'], "temp");
		//BILLER PAYMENT PROCESS
		$arrResult = array(
			"order_id" => $resultInsert[0]->cust_no,
			"product_code" => $resultInsert[0]->trx_type,
			"amountx" => $resultInsert[0]->amount_sgo,
			"data" => $resultInsert[0]->param_data,
			"url" => $this->sgo_url_payment_dev
		);

		$hasil = $this->sgoPpobInquiryPay($arrResult);
		$hasil2 = json_decode($hasil);
		//END
		/*echo "<pre>";
        print_r($arrResult);
        echo "</pre>";

        echo "<br /><pre>";
        print_r($hasil2);
        echo "</pre>"; */

		if($hasil2->error_code == "0000") {
			//CHECK PAYMENT STATUS AND GET COMMISION FEE FOR CASHBACK
			$arrStt = array(
				"ref_id" => $hasil2->ref_id,
				"url" => $this->sgo_url_inqpay_status
			);

			$payStatus = $this->sgoPpobInquiryPay($arrStt);
			$payStatus2 = json_decode($payStatus);
			//END
			if($payStatus2->error_code == "0000") {
				$insReal = $this->hifi->insertPpobFromTemp2($xx['order_id'], $hasil, $payStatus2);
				if($insReal['response'] == "true") {
					echo $this->setPaymentNotif($arr, $xx);
				} else {
					die('1,Fail insert data header and detail,,,,,');
				}
			} else {
				$err = '2,'.$payStatus2->error_desc.',,,,,';
				die($err);
			}
		} else {
			$fail = $this->setPaymentNotifFail($hasil2, $xx);
			die($fail);
		}
	}

	function processXlDataSGO($xx, $arr) {
		$this->load->model("backend/be_hifi_model", "hifi");
		$resultInsert = $this->hifi->getPpobTrxById($xx['order_id'], "temp");
		if($resultInsert != null) {
			$arrXL = array(
				"brand_id" => $this->adv_id,
				"prod_id" => $resultInsert[0]->trx_type,
				"trx_id" => $resultInsert[0]->trx_id,
				"msisdn" => $resultInsert[0]->cust_no,
			);


			//$redeem = $this->redeemStockXL2($arrXL);
			$redeem = $this->redeemStockXLnoEncrypt($arrXL);
			$hasil2 = json_decode($redeem);
			if($hasil2 == "0" || $hasil2 == null) {
			   $trx_id = $resultInsert[0]->trx_id;	
			   $this->hifi->updateStatusResponseXL_tempsgo($trx_id, $redeem);	
			   die('1,No response from XL or No Valid Hours,,,,,');
			} /*else if($hasil2 == null) {
			   die('1,No response from XL,,,,,');
			}*/ 
			else if($hasil2->code == "00") {
				$insReal = $this->hifi->insertPpobFromTemp2($xx['order_id'], json_encode($hasil2));
				if($insReal['response'] == "true") {
					
					$reff_pay_id = "REC";
					if($hasil2->reff_id != null && $hasil2->reff_id != "") {
						$reff_pay_id = $hasil2->reff_id;
					}
					
					$arr_data = array(
						'id_xl'=> $resultInsert[0]->trx_type,
						'msisdn'=> $resultInsert[0]->cust_no,
						'createnm'=>$resultInsert[0]->memberid,
						'createdt'=>$resultInsert[0]->createdt,
						'price'=>$resultInsert[0]->nominal,
						'trcd'=>$resultInsert[0]->trx_id,
						//'reff_id'=>$hasil2->reff_id,
						'reff_id' => $reff_pay_id,
						//'reff_id'=>"redsds",
						'kategori'=> 0,
						'status'=> 0
					);
					//print_r($arr_data);
					$this->hifi->SaveTRXPaket($arr_data);

					echo $this->setPaymentNotif($arr, $xx);
				} else {
					die('1,Fail insert data header and detail,,,,,');
				}
			} else {
				$arr_data = array(
					'id_xl'=> $resultInsert[0]->trx_type,
					'msisdn'=> $resultInsert[0]->cust_no,
					'createnm'=>$resultInsert[0]->memberid,
					'createdt'=>$resultInsert[0]->createdt,
					'price'=>$resultInsert[0]->nominal,
					'trcd'=>$resultInsert[0]->trx_id,
					//'reff_id'=>$hasil2->reff_id,
					'reff_id'=>NULL,
					'reff_id'=>"FAIL_INS",
					'kategori'=> 0,
					'status'=> 1
				);
				//print_r($arr_data);
				$this->hifi->SaveTRXPaket($arr_data);
				$err = '1,'.$hasil2->error.',,,,,';
				die($err);
			}
		} else {
			die('1,Temp Trx is empty,,,,,');
		}
	}


	

	private function setArray($data, $signature) {

	}

	//$route['api/sgo/notif/topupva/dev'] = 'webshop/api_payment/notifTopUpVa_dev';
	public function notifTopUpVa_dev() {
		$data = $this->input->post(NULL, TRUE);
		print_r($data);
		//$this->processTopUpVa($data);
	}

	//$route['api/sgo/notif/topupva'] = 'webshop/api_payment/notifTopUpVa';
	public function notifTopUpVa() {
		$data = $this->input->post(NULL, TRUE);

		//step 1, insert all param sent by sgo
		$this->paymentService->processInsertParamSgo($data);

		$this->processTopUpVa($data);			
	}

	

	function processTopUpVa($data) {
		//$arr[0] = $this->sgo_signature_key;
		$arr[0] = $this->sgo_sendinvoice_signature_key;
		$arr[1] = $data['rq_datetime'];
		$arr[2] = $data['order_id'];
		$arr[3] = "PAYMENTREPORT";


		$signCheck = ppob_generateSignature($arr);

		if($data['signature'] != $signCheck) {
			$resSukses = array(
				'success_flag' => 1,
				'error_message' => "Signature Error",
				'error_code' => "0004",
				'order_id' => $data['order_id'],
				'reconcile_datetime' =>date('Y-m-d H:i:s')
			);
			$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
			die($fail);
		} else {

			$this->load->model('webshop/login_model', 'login');
			$order_id = preg_replace( "/\r|\n/", "", $data['order_id']);
			$res = $this->login->showDataMemberByNovac($order_id);
			if($res != null)  {
				$dbqryx  = $this->load->database("db_ecommerce", TRUE);

				$data['rq_datetime'] = date("Y-m-d H:i:s",strtotime($data['rq_datetime']));
				$data['payment_datetime'] = date("Y-m-d H:i:s",strtotime($data['payment_datetime']));
				$data['amount'] = (double) $data['amount'];
                //tambahan dion @07/05/2018
                $data['total_amount'] = (double) $data['total_amount'];
				$potongan = $data['total_amount'] - $data['amount'];
				//end
				$data['member_id'] = $res[0]->dfno;
				// //QUERY FOR CHECK DOUBLE TRANSACTION ID
				$dbqryx->select('rq_uuid');
				$dbqryx->from('va_sgo_paynotif_req');
				//$dbqryx->where('rq_uuid', $data['rq_uuid']);
				$dbqryx->where('payment_ref', $data['payment_ref']);
				$query = $dbqryx->get();
				//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
				if($query->num_rows() > 0) {
					$resSukses = array(
						'success_flag' => 1,
						//'error_message' => "RQ_UUID $data[rq_uuid] DOUBLE",
						'error_message' => "payment_ref $data[payment_ref] DOUBLE",
						'error_code' => "0005",
						'order_id' => $data['order_id'],
						'reconcile_datetime' =>date('Y-m-d H:i:s')
					);
					$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
					die($fail);
				} else {
				    $insArr = array(
						"rq_uuid" => $data['rq_uuid'],
						"rq_datetime" => $data['rq_datetime'],
						"sender_id" => $data['sender_id'],
						"receiver_id" => $data['receiver_id'],
						"password" => $data['password'],
						"comm_code" => $data['comm_code'],
						"member_code" => $data['member_code'],
						"member_cust_id" => $data['member_cust_id'],
						"member_cust_name" => $data['member_cust_name'],
						"amount" => $data['amount'],
						"debit_from" => $data['debit_from'],
						"debit_from_name" => $data['debit_from_name'],
						"debit_from_bank" => $data['debit_from_bank'],
						"receiver_id" => $data['receiver_id'],
						"credit_to" => $data['credit_to'],
						"credit_to_name" => $data['credit_to_name'],
						"credit_to_bank" => $data['credit_to_bank'],
						"payment_datetime" => $data['payment_datetime'],
						"payment_ref" => $data['payment_ref'],
						"payment_remark" => $data['payment_remark'],
						"order_id" => $data['order_id'],
						"product_code" => $data['product_code'],
						"product_value" => $data['product_value'],
						"message" => $data['message'],
						"status" => $data['status'],
						"token" => $data['token'],
						"total_amount" => $data['total_amount'],
						"member_id" => $data['member_id'],
						//"approval_code_full_bca" => $data['approval_code_full_bca'],
						"signature" => $data['signature']
					);
					$ins = $dbqryx->insert('va_sgo_paynotif_req', $insArr);
					//echo "sdsd : ".$dbqryx->last_query();
					//Bila data berhasil diinput, success_flag = 0;
					//echo "row : ".$ins;
					if($ins > 0) {
						$resSukses = array(
							'success_flag' => 0,
							'error_message' => "PAYMENT RECEIVED",
							'reconcile_id' => rand(15,32),
							'order_id' => $data['order_id'],
							'reconcile_datetime' =>date('Y-m-d H:i:s')
						);
						//send SMS notif
						$text = "Saldo VA anda sudah di Top Up sebesar Rp ".$data['amount'];
						$text .= ", potongan sebesar Rp ".$potongan. " dari Rp ". $data['total_amount'];
						$send = smsTemplate($res[0]->tel_hp, $text);
						//$send = smsTemplate("087780441874", $text);
						//end
						echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
					} else {
						$resSukses = array(
							'success_flag' => 1,
							'error_message' => "FAILED INSERT DATA $data[rq_uuid]",
							'error_code' => "0001",
							'order_id' => $data['order_id'],
							'reconcile_datetime' =>date('Y-m-d H:i:s')
						);
						$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
						die($fail);
					}
				}
			} else {
				$erMsg = "No VA $data[order_id] is not found on Database";
				$str = preg_replace( "/\r|\n/", "", $erMsg );
				$resSukses = array(
					'success_flag' => 1,
					'error_message' => $str,
					'error_code' => "0003",
					'order_id' => $data['order_id'],
					'reconcile_datetime' =>date('Y-m-d H:i:s')
				);
				$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
				die($fail);
			}

		}
		
	}


}		
			
    	
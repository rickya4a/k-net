<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * 
 */
class Payment extends MY_Controller {
	//put your code here
    public function __construct() {
        parent::__construct();
		//Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
        //Veritrans_Config::$isProduction = false;
        $this->load->service("webshop/Product_service",'productService');
		$this->load->service("webshop/Payment_service",'paymentService');
		$this->load->service("webshop/Payment_member_service",'payMemberService');
		$this->load->service("webshop/Payment_lp_member_service",'pay_member_lp');
    }
	
	/*-----------------------------
	 * PAYMENT SGO MODULE
	 * ----------------------------*/
	 
	 //$route['detprdSGO/(:any)'] = 'webshop/payment/testDetPrdSGO/$1';
	 function testDetPrdSGO($tes) {
	 	$this->db = $this->load->database('db_ecommerce', true);
	 	$qryDet = "SELECT * FROM ecomm_trans_det_prd_sgo WHERE orderno = '$tes'";
		    
		$queryDet = $this->db->query($qryDet);
			//$num = $queryDet->num_rows();
		$jum = 0;
		foreach($queryDet->result() as $row) {
			echo $row->prdcd." - ".$row->qty." - ".$row->bvr;
                
		}
            
	 }
	 
	 function testInputSales() {
	 	$xx = $this->input->post(null,true);
		$personal_info = $this->session->userdata('personal_info');
		
		//$dta['addr_ref'] = $this->paymentService->insertAddressReference($personal_info);
		
		$updtTabel =$this->paymentService->updateTrxSGO($xx['temp_paymentId']);
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($xx['temp_paymentId']);
		
		
		
		if($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = "";
			if($personal_info['delivery'] == "2") {
				//jika kartu kredit, maka masukkan referensi alamat pengiriman	
	            $dta['addr_ref'] = $this->paymentService->insertAddressReference($personal_info);
				$dta['jne'] = $this->paymentService->prepareConote($dta['trans'][0]->orderno);
			}
			
			//$this->paymentService->sendTrxSMS($dta['trans'][0]->orderno, $dta['trans'][0]->total_pay, $dta['trans'][0]->secno);
		    //$this->_destroy_cart_session();
		}
		
		$this->setTemplateDevSGO('webshop/payment_sgo/pay_ecommerce_result',$dta);
		
	 }

	 /*------------------------
	  * SGO PAYMENT (DEV)
	  * -------------------------*/
	  //$route['pay/vch'] = 'webshop/payment/savePaymentVch';
	  function savePaymentVch() {
	  	$xx = $this->input->post(null, true);
	  	//$updtTabel =$this->paymentService->updateTrxSGO($xx['temp_orderid']);
		$updtTabel =$this->paymentService->updateTrxSGO_baru($xx['temp_orderid']);
		$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['temp_orderid']);    
		$this->paymentService->sendTrxSMS2($resultInsert);
		
		
		redirect('pay/vch/finish/'.$xx['temp_orderid']);
                
	  }
	  
	  //$route['pay/vch/finish/(:any)'] = 'webshop/payment/afterSavePaymentVch/$1';
	  public function afterSavePaymentVch($order_id) {
	  	$dta['trxInsertStatus'] = "fail";	
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);		
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;			
		} 
		$this->_destroy_cart_session();
		//print_r($dta['trans']);
		$this->setTemplateSGO('webshop/payment_sgo_vch/pay_ecommerce_vch_result',$dta);
	  }
	  
	  //$route['pay/sgo/inquiry/dev'] = 'webshop/payment/getResponseFromSGODev';
	  function getResponseFromSGODev() {
        
		$xx = $this->input->post(null,true);
        $password = 'k-net181183';
		//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
		$dta = $this->paymentService->getDataPaymentSGOByOrderIDDev($xx['order_id']);
		$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'K-NET E-Commerce',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
        
        
    }
	
	//$route['pay/sgo/notif/dev'] = 'webshop/payment/notifAfterPaymentDev';
	public function notifAfterPaymentDev() {
		$this->load->model("webshop/Payment_model",'pmodel');

		$xx = $this->input->post(null,true);
            $password = 'k-net181183';
			//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
			$dta = $this->paymentService->getDataPaymentSGOByOrderIDDev($xx['order_id']);
            $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            } else{
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                $updtTabel =$this->paymentService->updateTrxSGO($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);    
				$this->paymentService->sendTrxSMS2($resultInsert);	
                $resSukses = array(
                    'success_flag' => 0,
                    'error_message' => 'Transaction K-Net Success',
                    'reconcile_id' => rand(15,32),
                    'order_id' => $xx['order_id'],
                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                );

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){
//					$dta['catalog']=$data_catalog;
//					$dta['token']=$xx['order_id'];
					emailSend('Katalog Digital K-Link', getUserEmail(), KatalogEmail($xx['order_id'], $data_catalog));

					foreach($data_catalog as $row){
						$link='Download katalog di : '.base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate(getUserPhone(), $link);
					}

				}

                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];             
				
				
				//die('04,Test reject,,,,,');
            }
	}
	
	
	//$route['pay/sgo/finish/dev/(:any)'] = 'webshop/payment/afterPaymentWithSGODev/$1';
	public function afterPaymentWithSGODev($order_id) {
		$this->load->model("webshop/Payment_model",'pmodel');

		$dta['trxInsertStatus'] = "fail";	
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);
		$dta['catalog']=null;
		//print_r($dta['trans']);	
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;
			$data_catalog=$this->pmodel->getKATALOG($order_id);
			if($data_catalog != null){
				$dta['catalog']=$data_catalog;
				$dta['token']=$order_id;
				emailSend('Katalog Digital K-Link', getUserEmail(), KatalogEmail($order_id, $data_catalog));

				foreach($data_catalog as $row){
					$link='Download katalog di : '.base_url('catalogue/download/'.$order_id.'/'.$row->cat_inv_id);
					smsTemplate(getUserPhone(), $link);
				}

			}

		} else {
			
			//$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
			$sgo = $this->paymentService->getDataPaymentSGOByOrderIDDev($order_id);
			if($sgo[0]->bank_code_payment == "16") {
				$dta['trxInsertStatus'] = "pending";
			}
		}
		
		$this->_destroy_cart_session();
		$this->setTemplateDevSGO('webshop/payment_sgo/pay_ecommerce_result',$dta);	
		//print_r($dt['prodCat']);
	}


	function downloader($token, $produk) {
		$this->load->model("webshop/Payment_model",'pmodel');
		$data_catalog=$this->pmodel->getDownload($token, $produk);
		if($data_catalog == null){
			redirect('shop/product');
		}else

		force_download($data_catalog[0]->download_filename,NULL);
	}

//	public function getCatalog($order_id){
//		emailSend('Catalogue Digital K-link',getUserEmail(),'');
//		kirimEmail
//		kirimSMS
//
//		return link;
//	}
	  
	  /*-------------
	   * END
	   * ------------*/
	   //$route['tes/inquiry/(:any)'] = 'webshop/payment/tesOrderInq/$1';
	   function tesOrderInq($id) {
	      $dta = $this->paymentService->getDataPaymentSGOByOrderIDDevxxx($id);
		  $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
	      print_r($dta);
	   }
	 /*--------------------------
	  * SGO PAYMENT PRODUCTION
	  ---------------------------*/ 
	 //$route['pay/sgo/inquiry'] = 'webshop/payment/getResponseFromSGO';
	 function getResponseFromSGO(){
	 	//UPDATE @ 01/12/2015, SET ONLINE
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
		//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
		$dta = $this->paymentService->getDataPaymentSGOByOrderIDDev($xx['order_id']);
		$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'K-NET E-Commerce',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
	
	//$route['pay/sgo/notif'] = 'webshop/payment/notifAfterPayment';
	public function notifAfterPayment() {
		    $xx = $this->input->post(null,true);
            $password = 'k-net181183';
			//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
			$dta = $this->paymentService->getDataPaymentSGOByOrderIDDev($xx['order_id']);
            $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
            //$totalDP = $sgo_trx[0]->total_pay;
            
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            } else{
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                /*$updtTabel =$this->paymentService->updateTrxSGO($xx['order_id']);
                $resSukses = array(
                    'success_flag' => 0,
                    'error_message' => 'Transaction K-Net Success',
                    'reconcile_id' => rand(15,32),
                    'order_id' => $xx['order_id'],
                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                );

                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
				*/

				//$updtTabel =$this->paymentService->updateTrxSGO($xx['order_id']);
				$updtTabel =$this->paymentService->updateTrxSGO_baru($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);    
				//if($resultInsert[0]->tel_hp_login == null) {
				$this->paymentService->sendTrxSMS2($resultInsert);
				sendNotifSalesTransaction($resultInsert);

				$this->load->model("webshop/Payment_model",'pmodel');

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){
//					$dta['catalog']=$data_catalog;
//					$dta['token']=$xx['order_id'];
					emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
					//echo "<pre>";
					//print_r($data_catalog);
					//echo "</pre><br />";
					foreach($data_catalog as $row){
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $resultInsert[0]->tel_hp_login, $link);
						smsTemplate( $resultInsert[0]->tel_hp_as, $link);
					}

				}

				//}	
                $resSukses = array(
                    'success_flag' => 0,
                    'error_message' => 'Transaction K-Net Success',
                    'reconcile_id' => rand(15,32),
                    'order_id' => $xx['order_id'],
                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                );
                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
                
				
				    
            }
	}
	
	//$route['pay/sgo/finish/(:any)'] = 'webshop/payment/afterPaymentWithSGO/$1';
	public function afterPaymentWithSGO($order_id) {
		 
		$dta['trxInsertStatus'] = "fail";	
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);		
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;			
		} else {
			//$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
			$sgo = $this->paymentService->getDataPaymentSGOByOrderIDDev($order_id);
			if($sgo[0]->bank_code_payment == "16" || $sgo[0]->bank_code_payment == "26" || $sgo[0]->bank_code_payment == "27") {
				$dta['trxInsertStatus'] = "pending";
			}
		}
		$this->_destroy_cart_session();
		$this->setTemplateSGO('webshop/payment_sgo/pay_ecommerce_result',$dta);
		
	}
	
	//$route['livestream/vch/finish/(:any)'] = 'webshop/payment/liveStreamVch/$1';
	public function liveStreamVch($order_id) {
		$dta['trxInsertStatus'] = "fail";	
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);		
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;			
		} else {
			//$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
			$sgo = $this->paymentService->getDataPaymentSGOByOrderIDDev($order_id);
			if($sgo[0]->bank_code_payment == "16") {
				$dta['trxInsertStatus'] = "pending";
			}
		}
		$this->_destroy_cart_session();
		session_destroy();
		$this->setTemplateSGO('webshop/payment_sgo/pay_ecommerce_result',$dta);
	}
	
	//$route['payment/finish/(:any)'] = 'webshop/payment/apiFinishPayment/$1';
	public function apiFinishPayment($order_id) {
		 
		$dta['trx_status'] = "fail";	
		//$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);		
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trx_status'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;			
		} else {
			//$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
			$sgo = $this->paymentService->getDataPaymentSGOByOrderIDDev($order_id);
			if($sgo[0]->bank_code_payment == "16") {
				$dta['trxInsertStatus'] = "pending";
			}
			$dta['trans'] = $sgo;
		}
		echo json_encode($dta);
		
	}

	public function testSSS($order_id) {
		$dta['trans'] = $this->paymentService->getInsertTrxOrderNo($order_id);
		if($dta['trans'] > 0 || $dta['trans'] != null) {
				echo "Order No : ".$dta['trans'][0]->orderno;
		}
		print_r($dta['trans']);
	}
	
	//$route['tes/sms/ecom/(:any)'] = 'webshop/payment/token/$1';
	public function getTrxByTokenIDx($order_id) {
		$resultInsert = $this->paymentService->getInsertTrxOrderID($order_id);
		print_r($resultInsert);
	}
	
	/*-----------------------------
	 * END SGO MODULE
	 * ----------------------------*/

	/*---------------------------
	 * CREDIT CARD
	 ---------------------------*/
	//$route['pay/cc'] = 'webshop/payment/creditCard';
	public function creditCard() {
		if($this->_checkSessionStoreUser()) {
            $res['prodCat'] = $this->productService->getProductCategory();
			$token_id = $this->input->post('token_id');
			$personal_info = $this->session->userdata('personal_info');
	        if (empty($token_id)) {
	            die('Empty token_id!');
	        }
			$res['result'] = $this->paymentService->creditCard($token_id);
			if($res['result']->transaction_status == "capture") {
				$res['jne'] = "";	
				if($personal_info['delivery'] == "2") {
					//$res['jne'] =  $this->paymentService->generateJNENo($res['result']->order_id);
	                $resi =  $this->paymentService->generateJNENo($res['result']->order_id);
	                $arr = json_decode($resi);
					$res['jne'] = $arr->detail[0]->cnote_no;
	                //$res['jne'] =  "CGKTES0002";
				}
				$res['trans'] = $this->paymentService->saveECommerceTrx($res['result'], $res['jne']);
				$this->_destroy_cart_session();
			} 
            /*$res['result'] = "";
            $res['jne'] = "";
            $res['trans'] = $this->paymentService->saveECommerceTrx($res['result'], $res['jne']);
		    */
			$this->setTempWebShop('webshop/payment_method/credit_card_result', $res);
		}	
    }    
    
	//$route['jne/conot'] = 'webshop/payment/generateJNENo';
    function generateJNENo() {
		$res = $this->paymentService->generateJNENo();
		echo $res;
	}
    
	
     /*---------------------
	 * MANDIRI Click Pay
	 * -------------------*/
	 //$route['pay/mandiri/clickpay'] = 'webshop/payment/mandiriClickPay';
	 public function mandiriClickPay() {
		if($this->_checkSessionStoreUser()) {
		    $res['prodCat'] = $this->productService->getProductCategory();
			$personal_info = $this->session->userdata('personal_info');		
			/*$res['result'] = $this->paymentService->mandiriClickPay();
			if($res['result']->transaction_status == "settlement") {
				$res['jne'] = "";	
				if($personal_info['delivery'] == "2") {
					$resi =  $this->paymentService->generateJNENo($res['result']->order_id);
	                $arr = json_decode($resi);
					$res['jne'] = $arr->detail[0]->cnote_no;
			        //$res['jne'] =  "CGKTES0002";
				}
				$res['trans'] = $this->paymentService->saveECommerceTrx($res['result'], $res['jne']);	
				$this->_destroy_cart_session();
			} */
			$res['result'] = array(
			    "transaction_id" => "cddd6c1f-3b03-4c89-b0a8-0ea7dccf5217",
			    "order_id" => "TESTDION00010",
			    "gross_amount" => "10000.00",
			    "payment_type" => "mandiri_clickpay",
			    "transaction_time" => "2014-04-03 17:21:46",
			    "transaction_status" => "settlement",
			    "status_code" => "200",
			    "approval_code" => "1405402217483",
			    "masked_card" => "411111-1111",
			    "status_message" => "Success, Mandiri Clickpay transaction is successful"
			);
			//print_r($res['result']);
			
			if($personal_info['delivery'] == "2") {
					$resi =  $this->paymentService->generateJNENo($res['result']['order_id']);
	                $arr = json_decode($resi);
					$res['conot'] = $arr->detail[0]->cnote_no;
	                //$res['jne'] =  "CGKTES0002";
		    }
			print_r($res['conot']);
		    //$this->setTempWebShop('webshop/payment_method/mandiri_clickpay_result', $res);
		}			
    }
 
    
    /*---------------------
	 * CIMB Click Payment
	 * -------------------*/
	 //$route['pay/cimb/click'] = 'webshop/payment/cimbClick';
	 public function cimbClick(){
		if($this->_checkSessionStoreUser()) {		
			$res['result'] = $this->paymentService->cimbClick();
	        if($res['result']->status_code == "201") {
	          $this->session->set_userdata("order_payment", $res['result']);
	          header('Location: ' . $res['result']->redirect_url);
	        } else {
	          //error
	          echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
	          echo "<h3>Result:</h3>";
	          var_dump($result);
	        }
		}	
    }

	//$route['pay/cimb/click/proceed'] = 'webshop/payment/cimbClickProceed';
	public function cimbClickProceed() {
		
	}
    
    
    //$route['pay/va'] = 'webshop/payment/virtualAccount';
    public function virtualAccount() {
    	
    }
	
	/*---------------------
	 * e-pay BRI
	 * -------------------*/
	//$route['pay/bri/epay'] = 'webshop/payment/briEpay';
	 public function briEpay() {
    	if($this->_checkSessionStoreUser()) {
    		//$personal_info = $this->session->userdata('personal_info');			
	    	$res['result'] = $this->paymentService->briEpay();
	        if($res['result']->status_code == "201") {				
              	$this->session->set_userdata("order_payment", $res['result']);
	            header('Location: ' . $res['result']->redirect_url);
	        } else {
	          //error
	          echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
	          echo "<h3>Result:</h3>";
	          var_dump($res['result']);
	        }
		}	
    }

	//$route['pay/finish'] = 'webshop/payment/afterPaymentPage';
	public function afterPaymentPage() {
	        $dt['prodCat'] = $this->productService->getProductCategory();
	    	$personal_info = $this->session->userdata('personal_info');	
			$trx_type = $this->session->userdata('trxtype');		
		    $order_payment = $this->session->userdata("order_payment");
	        
	        $res['result'] = Veritrans_Transaction::status($order_payment->order_id);
			if($res['result']->status_code == "200") {
				$res['jne'] = "";	
					if($personal_info['delivery'] == "2") {
						$res['jne'] =  $this->paymentService->generateJNENo();
		                //$res['jne'] =  "CGKTES0002";
					}
				
				if($trx_type == "reg_member") {
					$res['trans'] = $this->payMemberService->saveECommerceTrx($res['result'], $res['jne']);
					$this->setTempWebShop('webshop/member_payment_method/cimb_bri_result', $res);
					$this->session->unset_userdata('starterkit_prd');
				} else if($trx_type == "reg_member_lp") {
					$res['trans'] = $this->pay_member_lp->saveECommerceTrx($res['result'], $res['jne']);	
					$this->setTempWebShopLP('webshop/member_lp/cimb_bri_result', $res);
					$this->session->unset_userdata('member_info');
					$this->session->unset_userdata('starterkit_prd');
				} else if($trx_type == "product_shopping") {
					$res['trans'] = $this->paymentService->saveECommerceTrx($res['result'], $res['jne']);
					$this->setTempWebShop('webshop/payment_method/cimb_bri_result', $res);	
				}		
				$this->_destroy_cart_session();
			}	
			 
	        //print_r($order_payment);
	        //print_r($res['result']);
		
	}

	//$route['pay/unfinish'] = 'webshop/payment/unfinish';
	public function unfinish() {
		$trx_type = $this->session->userdata('trxtype');
		$order_payment = $this->session->userdata("order_payment");
		$res['result'] = Veritrans_Transaction::status($order_payment->order_id);
		print_r($res['result']);
		/*if($trx_type == "reg_member") {
			
		} else if($trx_type == "reg_member_lp") {
			
		} else if($trx_type == "product_shopping") {
			
		}*/			
	}
	
	//$route['pay/error'] = 'webshop/payment/error';
	public function error() {
		$trx_type = $this->session->userdata('trxtype');
		$order_payment = $this->session->userdata("order_payment");
		$res['result'] = Veritrans_Transaction::status($order_payment->order_id);
		print_r($res['result']);
		/*if($trx_type == "reg_member") {
			
		} else if($trx_type == "reg_member_lp") {
			
		} else if($trx_type == "product_shopping") {
			
		}*/	
	}
	
	
	private function _destroy_cart_session() {
	   
       //TAMBAHAN ANA 31/03/2016
        $this->cart->destroy();
       // END TAMBAHAN
        
		$this->session->unset_userdata('non_member_info');
		$this->session->unset_userdata('order_payment');
		$this->session->unset_userdata('personal_info');
		$this->session->unset_userdata('trxtype');
		//$this->session->unset_userdata('pricecode');
		//$this->session->unset_userdata('cart_contents');
		$this->session->unset_userdata('shipping_jne_info');
		$this->session->unset_userdata('sender_address');
		$this->session->unset_userdata('destination_address');
		$this->session->unset_userdata('jne_branch');
		$this->session->unset_userdata('pay_sgo_id');
		
		
	}
	
	//$route['tesEmail] = webshop/payment/tesSendEmail
	public function tesSendEmail() {
		
		$resultInsert = $this->paymentService->getNewMemberData("RM151262385760");
		//print_r($resultInsert);
		/*$qry_str = "?login_username="."catur.bharata@gmail.com";
		$qry_str .= "&login_password="."asdqwe123";
		$qry_str .= "&template_id="."1";
		$qry_str .= "&mail_to="."dionrespati@gmail.com";
		$qry_str .= "&mail_subject="."Welcome to K-Link";
		$qry_str .= "&mail_from="."noreply@k-link.co.id";
		$qry_str .= "&mail_name="."fiwa";
		$qry_str .= "&email=".$resultInsert[0]->email;
		$qry_str .= "&username=".$resultInsert[0]->memberid;
		$qry_str .= "&password="."tessds";
		$qry_str .= "&id_number=".$resultInsert[0]->memberid;
		$qry_str .= "&sponsor_name=".$resultInsert[0]->sponsorname;
		$qry_str .= "&sponsor_email="."reess";
		*/
		$membername = str_replace(" ", "%20", $resultInsert[0]->membername);
		$sponsorname = str_replace(" ", "%20", $resultInsert[0]->sponsorname);
		$qry_str = "?login_username="."catur.bharata@gmail.com";
		$qry_str .= "&login_password="."asdqwe123";
		$qry_str .= "&template_id="."1";
		$qry_str .= "&mail_to="."dio";
		$qry_str .= "&mail_subject="."Welcome%20to%20K-Link%20";
		$qry_str .= "&mail_from="."noreply@k-link.co.id";
		$qry_str .= "&mail_name="."fiwa";
		$qry_str .= "&email="."dionre";
		$qry_str .= "&username=".$membername;
		$qry_str .= "&password=".$resultInsert[0]->password;
		$qry_str .= "&id_number=".$resultInsert[0]->memberid;
		$qry_str .= "&sponsor_name=".$sponsorname;
		$qry_str .= "&sponsor_email=".$resultInsert[0]->sponsoremail;
		
		
		$comxxcx = "http://192.168.0.168/ecm/go.php/api/email".$qry_str;
		$comxxc = "www.k-net.co.id/db1/list/from/ecomm_memb_ok/memberid/IDAHACA03003";
		$curl = curl_init($comxxcx);
		// Set query data here with the URL
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
		curl_close($curl);
		$arr = json_decode($content);
		print_r($arr);
		
		
	}
	
}
	
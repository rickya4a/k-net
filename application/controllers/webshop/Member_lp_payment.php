<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * @author sahid
 */
class Member_lp_payment extends MY_Controller {
	//put your code here
    public function __construct() {
        parent::__construct();
		//Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
        //Veritrans_Config::$isProduction = false;
        //$this->load->service("webshop/Product_service",'productService');
		$this->load->service("webshop/Payment_lp_member_service",'pay_member_lp');
    }
	
	/*-----------------------------
	 * PAYMENT SGO MODULE DEVELOPMENT
	 * ----------------------------*/
	 //$route['memberlp/sgo/inquiry/dev'] = 'webshop/member_lp_payment/getResponseFromSGODev';
	 function getResponseFromSGODev() {
		$xx = $this->input->post(null,true);
        $password = 'k-net181183';
		//$dta = $this->pay_member_lp->getDataPaymentSGOByOrderID($xx['order_id']);
		$dta = $this->pay_member_lp->getDataPaymentSGOByOrderIDDev($xx['order_id']);
		$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
		
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'K-NET Registration Member Landing Page',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
	}

    //$route['memberlp/sgo/notif/dev'] = 'webshop/member_lp_payment/notifAfterPaymentDev';
	function notifAfterPaymentDev() {
		    $xx = $this->input->post(null,true);
            $password = 'k-net181183';
			//$dta = $this->pay_member_lp->getDataPaymentSGOByOrderID($xx['order_id']);
			$dta = $this->pay_member_lp->getDataPaymentSGOByOrderIDDev($xx['order_id']);
            $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                $updtTabel = $this->pay_member_lp->saveMemberSGO($xx['order_id'], $dta);
				$insMembLP = $this->pay_member_lp->getNewMemberData($xx['order_id']);
				$this->pay_member_lp->sendTrxSmsRegMember($insMembLP);
                $resSukses = array('success_flag' => 0,
                                    'error_message' => 'Success',
                                    'reconcile_id' => rand(15,32),
                                    'order_id' => $xx['order_id'],
                                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                                     );
                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
                    
            }
	}
	
	//$route['memberlp/sgo/finish/dev/(:any)'] = 'webshop/member_lp_payment/afterPaymentWithSGODev/$1';
	function afterPaymentWithSGODev($orderid) {
		$personal_info = $this->session->userdata('personal_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');
		$dta['trxInsertStatus'] = "fail";	
		$dta['trans'] = $this->pay_member_lp->getNewMemberData($orderid);
		
		if($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;
		}
		$this->_destroy_cart_session();
		$this->setTempWebShopLP_SGO('webshop/payment_sgo/pay_member_result',$dta);
	}	
	 
	 /*-------------
	  * END SGO DEV
	  ---------------*/ 
	
	/*-----------------------------
	 * PAYMENT SGO MODULE
	 * ----------------------------*/
	 //$route['memberlp/inp/test'] = 'webshop/member_payment/inputMemberTest';
	function inputMemberTest() {
		$xx = $this->input->post(null,true);
		$personal_info = $this->session->userdata('personal_info');
		//print_r($xx);
		//$updtTabel =$this->pay_member_lp->updateTrxSGO($xx['temp_paymentId']);
		$dta['xxx'] = $this->pay_member_lp->saveMemberSGO($xx['temp_paymentId']);
		//print_r($dta['trans']);
		
		//print_r($dta['xxx']);
		
		
		if($dta['xxx']['response'] == "true") {
			//$dta['trans'] = $this->payMemberService->getNewMemberData($dta['xxx']['memberid']);
			$dta['trans'] = $this->pay_member_lp->getNewMemberData($xx['temp_paymentId']);
			
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = "";
			if($personal_info['delivery'] == "2") {
	           
				$dta['jne'] = $this->pay_member_lp->prepareConote($dta['trans'][0]->orderno);
			}
			
			//$this->payMemberService->sendTrxSMS($dta['trans'][0]->orderno, $dta['trans'][0]->total_pay, $dta['trans'][0]->secno);
		    //$this->_destroy_cart_session();
		} 
		
		$this->setTemplateDevSGO('webshop/payment_sgo/pay_member_result',$dta);
	}

	//$route['memberlp/sgo/inquiry'] = 'webshop/member_payment/getResponseFromSGO';
	function getResponseFromSGO() {
		$xx = $this->input->post(null,true);
        $password = 'k-net181183';
		//$dta = $this->pay_member_lp->getDataPaymentSGOByOrderID($xx['order_id']);
		$dta = $this->pay_member_lp->getDataPaymentSGOByOrderIDDev($xx['order_id']);
		$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
		
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'K-NET Registration Member Landing Page',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
	}
	
	//$route['memberlp/sgo/notif'] = 'webshop/member_payment/notifAfterPayment';
	function notifAfterPayment() {
		    $xx = $this->input->post(null,true);
            $password = 'k-net181183';
			//$dta = $this->pay_member_lp->getDataPaymentSGOByOrderID($xx['order_id']);
			$dta = $this->pay_member_lp->getDataPaymentSGOByOrderIDDev($xx['order_id']);
            $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                $updtTabel = $this->pay_member_lp->saveMemberSGO($xx['order_id'], $dta);
				$insMembLP = $this->pay_member_lp->getNewMemberData($xx['order_id']);
				$this->pay_member_lp->sendTrxSmsRegMember($insMembLP);
				//add @ 30/11/2016
				//$this->pay_member_lp->sendWelcomeEmail($insMembLP);
				//end
                $resSukses = array('success_flag' => 0,
                                    'error_message' => 'Success',
                                    'reconcile_id' => rand(15,32),
                                    'order_id' => $xx['order_id'],
                                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                                     );
                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
                    
            }
	}
	
	//$route['memberlp/sgo/finish/(:any)'] = 'webshop/member_payment/afterPaymentWithSGO/$1'; 
	function afterPaymentWithSGO($orderid) {
 
		$personal_info = $this->session->userdata('personal_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');
		/*
		$dta['trxInsertStatus'] = "fail";	
		$dta['trans'] = $this->pay_member_lp->getNewMemberData($orderid);
		
		if($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = "";
			$res = $this->pay_member_lp->getConoteJNE($dta['trans'][0]->trx_no);
			if($res > 0 || $res != null) {
				$dta['jne'] = $res[0]->conoteJNE;
			} 
			
			$this->pay_member_lp->sendTrxSMS($dta['trans']);
		    
		}
		$this->_destroy_cart_session();
		*/
		$dta['trxInsertStatus'] = "fail";	
		$dta['trans'] = $this->pay_member_lp->getNewMemberData($orderid);
		
		if($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;
		} else {
			//$sgo = $this->pay_member_lp->getDataPaymentSGOByOrderID($orderid);
			$sgo = $this->pay_member_lp->getDataPaymentSGOByOrderIDDev($orderid);
			if($sgo[0]->bank_code_payment == "16") {
				$dta['trxInsertStatus'] = "pending";
			}
		}
		$this->_destroy_cart_session();
		$this->setTempWebShopLP_SGO('webshop/payment_sgo/pay_member_result',$dta);
	}	
	 
	 /*----------
	 * END 
	 ----------*/
    
    /*---------------------------
	 * CREDIT CARD
	 ---------------------------*/
	 
	//$route['memberlp/inp/nopayment'] = 'webshop/member_lp_payment/testInputNoPayment'; 
	public function testInputNoPayment() {
		$personal_info = $this->session->userdata('personal_info');
		$res['jne'] = "";	
		
		if($personal_info['delivery'] == "2") {
			//$res['jne'] =  $this->pay_member_lp->generateJNENo();
            //$res['jne'] =  "CGKTES0002";
            $resi =  $this->pay_member_lp->generateJNENo($res['result']->order_id);
            $arr = json_decode($resi);
			$res['jne'] = $arr->detail[0]->cnote_no;
		}
		$res['trans'] = $this->pay_member_lp->saveECommerceTrxTest($res['jne']);
		//$this->setTempWebShopLP('webshop/member_lp/test_lp_dion', $res);
		print_r($res['trans']);
	}
	
	//$route['reg/pay/cc'] = 'webshop/payment/creditCard';
	public function creditCard() {
			
	        $token_id = $this->input->post('token_id');
			$personal_info = $this->session->userdata('personal_info');
	        if (empty($token_id)) {
	            die('Empty token_id!');
	        }
			$res['result'] = $this->pay_member_lp->creditCard($token_id);
			if($res['result']->transaction_status == "capture") {
				$res['jne'] = "";	
				if($personal_info['delivery'] == "2") {
					//$res['jne'] =  $this->pay_member_lp->generateJNENo();
	                //$res['jne'] =  "CGKTES0002";
	                $resi =  $this->pay_member_lp->generateJNENo($res['result']->order_id);
	                $arr = json_decode($resi);
					$res['jne'] = $arr->detail[0]->cnote_no;
				}
				$res['trans'] = $this->pay_member_lp->saveECommerceTrx($res['result'], $res['jne']);
				$this->_destroy_cart_session();
			} 
		    $this->setTempWebShopLP('webshop/member_lp/credit_card_result', $res);
		
    }
    
    //$route['jne/conot'] = 'webshop/payment/generateJNENo';
    function generateJNENo() {
		$res = $this->paymentService->generateJNENo();
		echo $res;
	}
    
    
    
     /*---------------------
	 * MANDIRI Click Pay
	 * -------------------*/
	 //$route['member/pay/mandiri/clickpay'] = 'webshop/payment/mandiriClickPay';
	 public function mandiriClickPay() {
		
				
			$personal_info = $this->session->userdata('personal_info');		
			$res['result'] = $this->pay_member_lp->mandiriClickPay();
			if($res['result']->transaction_status == "settlement") {
				$res['jne'] = "";	
				if($personal_info['delivery'] == "2") {
					$res['jne'] =  $this->paymentService->generateJNENo();
	                //$res['jne'] =  "CGKTES0002";
				}
				$res['trans'] = $this->pay_member_lp->saveECommerceTrx($res['result'], $res['jne']);	
				$this->_destroy_cart_session();
			}
		    $this->setTempWebShopLP('webshop/member_lp/mandiri_clickpay_result', $res);
				
    }
    
     /*---------------------
	 * CIMB Click Payment
	 * -------------------*/
	 //$route['member/pay/cimb/click'] = 'webshop/payment/cimbClick';
	 public function cimbClick(){
		
			$res['result'] = $this->pay_member_lp->cimbClick();
	        if($res['result']->status_code == "201") {
	          $this->session->set_userdata("order_payment", $res['result']);
	          header('Location: ' . $res['result']->redirect_url);
	        } else {
	          //error
	          echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
	          echo "<h3>Result:</h3>";
	          var_dump($result);
	        }
			
    }
    
    
	//$route['pay/cimb/click/proceed'] = 'webshop/payment/cimbClickProceed';
	public function cimbClickProceed() {
		
	}
    
    
    //$route['pay/va'] = 'webshop/payment/virtualAccount';
    public function virtualAccount() {
    	
    }
	
	//$route['member/pay/bri/epay'] = 'webshop/payment/briEpay';
	 public function briEpay() {
    		
			
			$res['result'] = $this->pay_member_lp->briEpay();
	        if($res['result']->status_code == "201") {				
              	$this->session->set_userdata("order_payment", $res['result']);
	            header('Location: ' . $res['result']->redirect_url);
	        } else {
	          //error
	          echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
	          echo "<h3>Result:</h3>";
	          var_dump($res['result']);
	        }
		
    }

	//$route['pay/finish'] = 'webshop/payment/afterPaymentPage';
	/*public function afterPaymentPage() {
	    //$order_payment = json_encode($this->session->userdata("order_payment"));
        $order_payment = $this->session->userdata("order_payment");
	    //$order_id = "1027383414";
        $result = Veritrans_Transaction::status($order_payment->order_id);
        print_r($result);
	}*/
	
	private function _destroy_cart_session() {
		$this->session->unset_userdata('personal_info');
		$this->session->unset_userdata('member_info');
		$this->session->unset_userdata('pricecode');
		//$this->session->set_userdata('pricecode', '12W3');
		$this->session->unset_userdata('starterkit_prd');
		$this->session->unset_userdata('shipping_jne_info');
		$this->session->unset_userdata('sender_address');
		$this->session->unset_userdata('destination_address');
		$this->session->unset_userdata('jne_branch');
		$this->session->unset_userdata('pay_memb_sgo_id');
		
	}
 }
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Promo_rekrut extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->load->service("webshop/Product_service",'productService');
		$this->load->model("webshop/m_promo_rekrut", "promo");
        $this->load->model("backend/klinkpromo/m_idc_promo", "promoidc");
		$this->folderView = "webshop/promo_rekrut/";
	}
	
	//$route['promo_rekrut'] = 'webshop/promo_rekrut';
	public function index() {
		$data['prodCat'] = $this->productService->getProductCategory();
		$id = getUserID();
		$name = getUserName();
		if($id != NULL) {
			$data['idmember'] = $id;
			$data['nmmember'] = $name;
			$data['changeid'] = "readonly=readonly";
			$x['promo'] = $this->promo->getListDataPromo("dfno", $id);	
			$x['redemp'] = $this->promo->getListDataRedemptionRekrut("dfno", $id);
			//print_r($x);
			$data['datax'] = $this->load->view("backend/promo/promoPoinResult", $x, TRUE);
			$this->setTempWebShop($this->folderView."promoCheckPoinAuthLogin", $data);
		} else {
			$data['idmember'] = "";
			$data['nmmember'] = "";
			$data['changeid'] = "";
			$this->setTempWebShop($this->folderView."promoCheckPoin", $data);
		}
		
			   
	}
	
	
    //$route['promo_rekrut/id/(:any)'] = 'webshop/promo_rekrut/checkPoinRewards/$1';
	public function checkPoinRewards($id) {
		try {
			$res = $this->promo->checkValidMember($id);
			$data['promo'] = $this->promo->getListDataPromo("dfno", $id);	
			$data['redemp'] =$this->promo->getListDataRedemptionRekrut("dfno", $id);
			$data['datax'] = $this->load->view("backend/promo/promoPoinResult", $data, TRUE);
			$this->load->view($this->folderView."promoPoinResult", $data);
		} catch(Exception $e) {
			echo "<h4>".$e->getMessage()."<h4>";
		}
	}
	
	//$route['promo_kolagen'] = 'webshop/promo_rekrut/promo_kolagen';
	public function promo_kolagen() {
		$data['prodCat'] = $this->productService->getProductCategory();
		$id = getUserID();
		if($id != null) {
			$data['promo'] = $this->promo->checkUndianKolagen($id);
			$this->setTempWebShop($this->folderView."kolagenCheckUndian", $data);
		} else {
		   echo "<script>alert('Silahkan login terlebih dahulu..')</script>";
		   redirect('loginmember', 'refresh');
		}
		//echo "tes";
	}
    
    function idcPromo(){
        $x['prodCat'] = $this->productService->getProductCategory();
		$id = getUserID();
		if($id != null) {
            //$x['idcSummary'] = $this->promoidc->getListDataPromo("dfno", $id);
            $x['bnsperiod'] = $this->promoidc->listBnsPeriod($id);
            //$x['summaryIDC'] = $this->promoidc->checkValidIDC($id);
            $this->setTempWebShop($this->folderView."idcCheckPoin", $x);
        }
        else 
        {
		   echo "<script>alert('Silahkan login terlebih dahulu..')</script>";
		   redirect('loginmember', 'refresh');
		}
    }
    
    /*function idcPromoDetails(){
        $id = getUserID();
        $bnsperiod = $this->input->post('bnsperiod');
        $x['summaryIDC'] = $this->promoidc->checkValidIDC($id,$bnsperiod);
        $x['detailIdc'] = $this->promoidc->getListIDCDetail($id,$bnsperiod);
        $this->load->view('webshop/idcDetailsByPeriod',$x);
    }*/
    
    function listOliveHoney(){
        $x['prodCat'] = $this->productService->getProductCategory();
		$id = getUserID();
		
        $x['listing'] = $this->promo->getListOliveHoney();
        $this->setTempWebShop('webshop/listOliveHoney', $x);
    }
}
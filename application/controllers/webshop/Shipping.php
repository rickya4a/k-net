<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Shipping extends MY_Controller {
    	
    public function __construct() {
	    parent::__construct();
		$this->load->model("webshop/Shipping_model",'shippingM');
		$this->load->service("webshop/Shipping_service",'shippingService');
		$this->load->service("webshop/Product_service",'productService');
	}
	
    //$route['shipping'] = 'webshop/shipping/getShipping'
    function getShipping() {
        if($this->_checkSessionStoreUser()) {
	        $dt = $this->shippingService->showStockistByArea(getUserID());
	        $dt['prodCat'] = $this->productService->getProductCategory();
	        $this->setTempWebShop('webshop/shipping', $dt);
		}  
    }
	
    //$route['shipping/getStockist/(:any)'] = 'webshop/shipping/getStockist/$1';
	function getStockist($area) {       
        if($this->_checkSessionStoreUser()) {
			$cek = $this->shippingService->getListStockistByArea($area);
	        echo json_encode($cek);      
		}
    }
 
    //$route['shipping/getPricecode/(:any)'] = 'webshop/shipping/getPriceStockist/$1';
	function getPriceStockist($nilai) {
        if($this->_checkSessionStoreUser()) {
			$cek = $this->shippingService->showPricecodeByStockistID($nilai);
	        echo json_encode($cek);
		}
    }
	
    //$route['shipping/reg/getPricecodeX/(:any)/(:any)'] = 'webshop/member_lp/getPriceStockistX/$1/$2'; //TAMBAHAN HILAL
    //$route['shipping/getPricecodeX/(:any)/(:any)'] = 'webshop/shipping/getPriceStockistX/$1/$2';
	function getPriceStockistX($nilai, $kec) {
        //if($this->_checkSessionStoreUser()) {
			$cek = $this->shippingService->showPricecodeByStockistIDX($nilai, $kec);
	        echo json_encode($cek);
		//}
    }

	//$route['shipping/getPricecodeCOD/(:any)/(:any)'] = 'webshop/shipping/getPriceStockistCOD/$1/$2';
	function getPriceStockistCOD($nilai, $kec) {
		//if($this->_checkSessionStoreUser()) {
		$cek = $this->shippingService->showPricecodeByStockistCOD($nilai, $kec);
		echo json_encode($cek);
		//}
	}
    
    
    //$route['shipping/kota/(:any)'] = 'webshop/shipping/getListKota/$1';
	function getListKota($prov) {
        //if($this->_checkSessionStoreUser()) {
	        $res = $this->shippingService->showListKota($prov);
	        echo json_encode($res);      
		//}
    }
    
    //$route['shipping/kecamatan/(:any)'] = 'webshop/shipping/getListKecamatan/$1';
	function getListKecamatan($kota) {
        //if($this->_checkSessionStoreUser()) {
			$res = $this->shippingService->showListKecamatan($kota);
	        echo json_encode($res);
		//}     
    }
    
    //$route['shipping/kelurahan/(:any)'] = 'webshop/shipping/getListKelurahan/$1';
    function getListKelurahan($kec) {
        //if($this->_checkSessionStoreUser()) {
			$res = $this->shippingService->showListKelurahan($kec);
	        echo json_encode($res);      
		//}
        
    }

	//$route['shipping/kecamatan_cod/(:any)'] = 'webshop/shipping/getListKecamatanCOD/$1';
	function getListKecamatanCOD($kec) {
		//if($this->_checkSessionStoreUser()) {
		$res = $this->shippingService->showListKecamatanCOD($kec);
		echo json_encode($res);
		//}

	}

	//$route['shipping/kotaKP/(:any)'] = 'webshop/shipping/getListKotaKP/$1';
	function getListKotaKP($prov) {
		//if($this->_checkSessionStoreUser()) {
		$res = $this->shippingService->showListKotaKP($prov);
		echo json_encode($res);
		//}
	}

	//$route['shipping/kecamatanKP/(:any)'] = 'webshop/shipping/getListKecamatanKP/$1';
	function getListKecamatanKP($kota) {
		//if($this->_checkSessionStoreUser()) {
		$res = $this->shippingService->showListKecamatanKP($kota);
		echo json_encode($res);
		//}
	}

	//$route['shipping/kelurahanKP/(:any)'] = 'webshop/shipping/getListKelurahanKP/$1';
	function getListKelurahanKP($kec) {
		//if($this->_checkSessionStoreUser()) {
		$res = $this->shippingService->showListKelurahanKP($kec);
		echo json_encode($res);
		//}

	}

	//$route['shipping/cek_kodepos/(:any)/(:any)/(:any)/(:any)'] = 'webshop/shipping/cek_kodepos/$1/$2/$3/$4';
	function cek_kodepos($prov,$kota,$kec,$kel) {
		//if($this->_checkSessionStoreUser()) {
		$res = $this->shippingService->cek_kodepos($prov,$kota,$kec,$kel);
		echo json_encode($res);
		//}

	}

	//$route['cargo/province/list/(:any)'] = 'webshop/shipping/listCargoProvince/$1';
	function listCargoProvince($id_shipper) {
		//JNE Cargo	
		if($id_shipper == "1") {
			$this->load->model('webshop/shared_module', 'shared');
			$valReturn = $this->shared->getListProvinsi2();
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			echo json_encode($returnArr);
		} 
		//KGB Cargo
		else if($id_shipper == "2") {
			$this->load->model('webshop/shared_module', 'shared');
			$valReturn = $this->shared->listProvinceKGB();
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			echo json_encode($returnArr);
		}

		else if($id_shipper == "4") {
			$returnArr = array("response" => "true");
			echo json_encode($returnArr);
		}

		else if($id_shipper == "6") {
			$returnArr = array("response" => "true");
			echo json_encode($returnArr);
		}
	}
	
	//$route['cargo/kota/list'] = 'webshop/shipping/listCargoKota';
	function listCargoKota() {
		$this->load->model('webshop/shared_module', 'shared');
		$prov = $this->input->post("provinsiNama");
			$valReturn = $this->shared->listCargoKota($prov);
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			echo json_encode($returnArr);
	}

    //$route['cargo/stockist/list'] = 'webshop/shipping/listCargoStockist';
	function listCargoStockist() {
		$this->load->model('webshop/shared_module', 'shared');
		$kota = $this->input->post("kotaKode");
			$valReturn = $this->shared->listCargoStockist($kota);
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			echo json_encode($returnArr);
	}
		
	//$route['cargo/pricecode/(:any)'] = 'webshop/shipping/setPricecodeKGB/$1';
    //$route['cargo/pricecode/(:any)'] = 'webshop/shipping/setPricecodeKGB/$1';
	
	function setPricecodeKGB($kota) {
		$this->load->model('webshop/shared_module', 'shared');
		//$kota = $this->input->post("kotaKode");
			$valReturn = $this->shared->setPricecodeKGB($kota);
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
			echo json_encode($returnArr);
	}
	
    //$route['cargo/price'] = 'webshop/shipping/checkHargaPengiriman';
	function checkHargaPengiriman() {
		$data = $this->input->post(NULL, TRUE);
		
		if(!array_key_exists("cargo_id", $data) || !array_key_exists("stockist_code", $data) || !array_key_exists("dest_code", $data) || !array_key_exists("weight", $data)) {
			$hasil = jsonFalseResponse("Missing parameter cargo_id / stockist_code / dest_code / weight");
		} else {
			$this->load->library('delivery_cargo');
			$cargo = new Delivery_cargo();
			$cargo->setTipeCargo($data['cargo_id'])
				  ->setStockist($data['stockist_code'])
			      ->setKodeArea($data['dest_code'])
			      ->setWeight($data['weight']);
			
			$hasil = $cargo->getPriceListShipping();
		}
		
	    echo json_encode($hasil);		
	}
	
	//$route['jneCheckPrice'] = 'webshop/shipping/checkHargaJne';
	function checkHargaJne() {
	    $data = $this->input->post(NULL, TRUE);
		$username = "KLINK";
		$password = "76270305bef5d402220c96d59ac61977";
		$curl_post_data = array(
					"username" => urlencode($username),
					"api_key" => urlencode($password),
					"from" => urlencode($data['from']),
					"thru" => urlencode($data['to']),
					"weight" => urlencode($data['weight'])
				);		
				
				$postData = setFieldPost($curl_post_data);
			
				$curl = curl_init();
				$arx = array(
				  CURLOPT_URL => pricing_url("1"), //"http://apiv2.jne.co.id:10101/tracing/api/pricedev", 
				  CURLOPT_RETURNTRANSFER => true, 
				  CURLOPT_SSL_VERIFYPEER => true,
				  CURLOPT_CUSTOMREQUEST => "POST", 
				  CURLOPT_POSTFIELDS => $postData, 
				  CURLOPT_HTTPHEADER => array( 
					"Content-Type: application/x-www-form-urlencoded")
				  );
				curl_setopt_array($curl, $arx);
				$curl_response = curl_exec($curl);
				curl_close($curl);
				
				echo $curl_response;
	}
	
	//$route['jne/konos/create/(:any)'] = 'webshop/shipping/getShippingKonos/$1';
	//$route['shipping/konos/create] = 'webshop/shipping/getShippingKonos';
	function getShippingKonos() {
	    $data = $this->input->post(NULL, TRUE);
		
		if(!array_key_exists("cargo_id", $data) || !array_key_exists("orderno", $data)) {
			$hasil = jsonFalseResponse("Missing parameter cargo_id / orderno");
		} else {
			$this->load->library('delivery_cargo');
			$cargo = new Delivery_cargo();
			$cargo->setTipeCargo($data['cargo_id'])
				  ->setTransactionId($data['orderno']);
			$hasil = $cargo->getShippingKonos();	
			//echo $cargo->getErrorMessage();
			
		}
		echo json_encode($hasil);	
		//
		//echo "sdsd";
		//echo "sdsd";
	}
	
	//$route['jne/tracking/(:any)/(:any)'] = 'webshop/shipping/jneTracking/$1/$2';
	function jneTracking($cargotype, $konos) {
		$this->load->library('delivery_cargo');
		$cargo = new Delivery_cargo();
		$res = $cargo->setTipeCargo($cargotype)->trackingKonos($konos);
		//echo "<pre>";
		echo $res;
		//echo "</pre>";
		//echo "sdsd";
	}
	
	
	function checkHargaKGP($param) {
	
	}
 }
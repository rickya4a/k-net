<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 *
 */
class KlaimBonusPaket extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("webshop/klaim_model", "klaim");
		$this->load->service("webshop/Member_service",'memberService');
		$this->load->service("webshop/Cartshop_service",'cartshopService');

		$this->load->model("backend/klinkpromo/m_iec_promo", "iec_promo");
	}

	function getformKlaim(){
		if($this->_checkSessionStoreUser()) {
			$idmembers = getUserID();

			$dt['prodCat'] = $this->memberService->getProductCategory();
			$dt['listing'] = $this->klaim->getListingKlaim($idmembers);
			//print_r($dt['listBank']);
			$this->setTempWebShop('webshop/klaimBonus', $dt);
		}
	}


	function getDetKw($trd)
	{
		if($this->_checkSessionStoreUser()) {
			$dt['listdetail'] = $this->klaim->getListingDetail($trd);

			$this->load->view('webshop/detailKlaim',$dt);
		}
	}

	function getDetKlaim($trd)
	{
		if($this->_checkSessionStoreUser()) {
			$dt['formdetail'] = $this->klaim->getFormDetail($trd);
//
			$dt['listdetail'] = $this->klaim->getListingDetail($trd);

			$this->load->view('webshop/detailFormKlaim',$dt);
		}
	}


	function getProdukKlaim($bnsperiod)
	{
		if($this->_checkSessionStoreUser()) {
			$idmembers = getUserID();

			$dt['list'] = $this->klaim->getProdukKlaim($bnsperiod,$idmembers);

			$this->load->view('webshop/detailProdukKlaim',$dt);
		}
	}

	function getformstockist($trd,$dfno)
	{
		if($this->_checkSessionStoreUser()) {
			$dt['liststockis'] = $this->klaim->getListingstokis();
			$dt['trd']=$trd;
			$dt['dfno']=$dfno;

			$this->load->view('webshop/submitKlaim',$dt);
		}
	}

	function submit()
	{
		if($this->_checkSessionStoreUser()) {

			$vc=$this->input->post('vc');
			$st=$this->input->post('stockist');
			$dfno=$this->input->post('dfno');

			$x= $this->klaim->submit($vc,$st,$dfno);
			//$dt['listdetail'] = $this->klaim->submit($vc,$st);

			//$this->load->view('webshop/submitKlaim',$dt);
			return $x;

		}
	}

}
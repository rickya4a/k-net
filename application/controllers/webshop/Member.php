<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in t<br />he editor.
 */

/**
 * Description of PaymentExample
 *
 * @author sahid
 */
class Member extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->service("webshop/Member_service",'memberService');
		//$this->load->service("webshop/Product_service",'memberService');
		$this->load->service("webshop/Shipping_service",'shippingService');
		$this->folderView = "webshop/member_payment_method/";
	}

	//$route['member/choosePay'] = 'webshop/member/choosePayType';
	public function choosePayType($err = null) {
		if($this->_checkSessionStoreUser()) {
		    $new_err = str_replace(array("%20"), " ", $err);
			$dt['err'] = $new_err;
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$this->setTempWebShop('webshop/memberChoosePayType', $dt);
		}
	}

	//$route['member'] = 'webshop/member/getRegisterMember2';
	public function getRegisterMember2() {
		if($this->_checkSessionStoreUser()) {
			$dt = $this->input->post(NULL, TRUE);

			if($dt['pay_tipe'] == "1") {
				//$arr = $this->memberService->checkValidationVoucher2($dt['voucherno'], $dt['voucherkey']);
				$arr = $this->memberService->checkValidationVoucherNew($dt['voucherno'], $dt['voucherkey']);
				//print_r($arr);

				if($arr['sukses'] != "TRUE") {
					//print_r($arr);
					if(getUserID() != "ID9999A00251") {
						$pesan = $arr['message'];
						//echo "<script>alert($pesan)</script>";
						header("Location: member/choosePay/pesan/$pesan");
					} else {
						$pesan = $arr['message'];
						//echo "<script>alert($pesan)</script>";
						header("Location: member/choosePay/pesan/$pesan");
					}
				} else {
					$rd = array(
						"pay_tipe" => $dt['pay_tipe'],
						"voucherno" => $dt['voucherno'],
						"voucherkey" => $dt['voucherkey'],
						"prdcd" => $arr['arrayData'][0]->prdcd
					);
					$this->session->set_userdata('memb_choose_pay', $rd);
					$dt['prodCat'] = $this->memberService->getProductCategory();
					$dt['listBank'] = $this->memberService->getListBank();
					$dt['listStk'] = $this->memberService->getListStockist();
					$dt['show_provinsi'] = $this->memberService->showListProvinsi();
					//$this->setTempWebShop('webshop/memberRegisterVoucher', $dt);
					//$this->setTempWebShop('webshop/memberRegisterVoucherDev', $dt); // ASLI
					if(getUserID() == "IDSPAAA66834") {
						$this->setTempWebShop('webshop/memberRegisterVoucherVera', $dt);
					} else {
						$this->setTempWebShop('webshop/memberRegisterVoucherDev', $dt);
					}
				}
			} else {
					if (getUserID() != "IDSPAAA66834") {
						$this->session->set_userdata('memb_choose_pay', $dt);
						$dt['prodCat'] = $this->memberService->getProductCategory();
						$dt['listBank'] = $this->memberService->getListBank();
						$this->setTempWebShop('webshop/memberRegisterDev', $dt);
					} else {
						$this->setTempWebShop('webshop/memberRegister', $dt);
					}
					/*if(getUserID() == "IDSPAAA66834") {
						$this->setTempWebShop('webshop/memberRegisterDev', $dt);
					} else {
						$this->setTempWebShop('webshop/memberRegister', $dt);
					}*/
			}
		}
	}

	//$route['member/voucher/save'] = 'webshop/member/saveMemberWithVoucher';
	public function saveMemberWithVoucher() {
		$dataForm = $this->input->post(NULL, TRUE);
		$this->session->set_userdata('member_info', $dataForm);
		$check = $this->memberService->checkInputMemberWithVoucher($dataForm);
		if($check['response'] == "true") {
			$dt['res'] = $this->memberService->saveMemberWithVoucher2();
			$this->setTempWebShop('webshop/memberRegVoucherSuccess',$dt);
			$this->session->unset_userdata('memb_choose_pay');
			$this->session->unset_userdata('member_info');

		} else {
			$dt['err_msg'] = $check['err'];
			$dt['prodCat'] = $this->memberService->getProductCategory();
		    $dt['listBank'] = $this->memberService->getListBank();
			$dt['listStk'] = $this->memberService->getListStockist();
			$this->setTempWebShop('webshop/memberRegVoucherBackError',$dt);
		}
		//
	}

	//$route['member/back'] = 'webshop/member/backToUpdateDataMember';
	public function backToUpdateDataMember() {
		$dt['err_msg'] = "";
			$dt['prodCat'] = $this->memberService->getProductCategory();
		    $dt['listBank'] = $this->memberService->getListBank();
			$dt['listStk'] = $this->memberService->getListStockist();
			//$this->setTempWebShop('webshop/memberRegisterBack',$dt);
			$this->setTempWebShop('webshop/memberRegisterBackDev', $dt);
			/*if(getUserID() == "IDSPAAA66834") {
				$this->setTempWebShop('webshop/memberRegisterBackDev', $dt);
				//echo "ok";
			} else {
				$this->setTempWebShop('webshop/memberRegisterBack', $dt);
			}*/
	}
	
	//$route['member/recruit'] = 'webshop/member/formGetListRecruit';
	public function formGetListRecruit() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			//$dt['listRecruit'] = $this->memberService->getListRecruitLandingPage(getUserID(), "kiv");
			$this->setTempWebShop('webshop/memberRecruit',$dt);
		}	
	}
    //$route['member/recruit/list'] = 'webshop/member/getListRecruit';
    public function getListRecruit() {
    	$data = $this->input->post(NULL, TRUE);
		$arr = $this->memberService->getListRecruit($data);
		$i=1;
		if($arr != null) {
			foreach($arr as $dtas) {
				
				echo "<div class=\"col-lg-12 col-xs-12 cart_content_thin text-center\">
				        <div class=\"col-lg-1 col-xs-1\">$i</div>
						<div class=\"col-lg-2 col-xs-2\">$dtas->dfno</div>
		                <div class=\"col-lg-3 col-xs-2\">$dtas->fullnm</div> 
		                <div class=\"col-lg-1 col-xs-2\">$dtas->jointdt</div> 
		                <div class=\"col-lg-2 col-xs-2\">$dtas->sponsorid</div>
		                <div class=\"col-lg-3 col-xs-3\">$dtas->sponsorname</div>
		              </div>";
			   $i++;		  
			}
		} else {
			
		}
    }

	//$route['member/voucher/check/(:any)/(:any)'] = 'webshop/member/getVoucherCheck/$1/$2';
	public function getVoucherCheck($voucherno, $voucherkey) {
		$arr = $this->memberService->checkValidationVoucher($voucherno, $voucherkey);
		echo json_encode($arr);
	}


	//$route['member/id/(:any)'] = 'webshop/member/getMemberInfo/$1';
	public function getMemberInfo($id) {

			$arr = $this->memberService->getMemberInfo($id);
			echo json_encode($arr);

	}
	
	//$route['downline/check/(:any)/(:any)'] = 'webshop/member/checkSatujaringan/$1/$2';
	public function checkSatujaringan($id, $idlogin) {
			$this->load->model("webshop/Member_model", "member_model");
			$arr = $this->member_model->checkSatujaringan($id, $idlogin);
			echo json_encode($arr);

	}
	
	//$route['member/listbank'] = 'webshop/member/getListAffiliateBank';
	public function getListAffiliateBank() {
		$returnArr = array("response" => "false");
		$this->load->model("webshop/Member_model", "member_model");
		$valReturn = $this->member_model->getListAffiliateBank();
		if($valReturn != null) {
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
		} 
		echo json_encode($returnArr);
	}
	
	//$route['member/listStockist'] = 'webshop/member/listStockist';
	public function listStockist() {
		$returnArr = array("response" => "false");
		$this->load->model("webshop/Member_model", "member_model");
		$valReturn = $this->member_model->getListStockist();
		if($valReturn != null) {
			$returnArr = array("response" => "true", "arrayData" => $valReturn);
		} 
		echo json_encode($returnArr);
	}

	//$route['member/id3/(:any)'] = 'webshop/member/getMemberInfo/$1';
	public function getMemberInfo3($id) {
	
			$arr = $this->memberService->getMemberInfo3($id);
			echo json_encode($arr);

	}

	public function getMemberInfo2() {
		echo json_encode(jsonFalseResponse("Data sponsor harus diisi"));
	}

	//$route['member/ktp/(:any)'] = 'webshop/member/checkNoKTP/$1';
	public function checkNoKTP($noktp) {
		$arr = $this->memberService->checkNoKTP($noktp);
		echo json_encode($arr);
	}

	//$route['member/no_hp/(:any)'] = 'webshop/member/checkNoHP/$1';
	public function checkNoHP($noktp) {
		$arr = $this->memberService->checkNoHP($noktp);
		echo json_encode($arr);
	}

	//$route['member/starterkit'] = 'webshop/member/chooseStarterkit';
	public function chooseStarterkit() {
		if($this->_checkSessionStoreUser()) {
			$cart = $cart_check = $this -> cart -> contents();
			if(!empty($cart)) {
				$this->session->unset_userdata('personal_info');
				$this->session->unset_userdata('cart_contents');
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->unset_userdata('sender_address');
				$this->session->unset_userdata('destination_address');
				$this->session->unset_userdata('jne_branch');

			}
            $dt['listCargo'] = $this->memberService->getListCargo();
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$member_info = $this->session->userdata('member_info');
			$personal_info = $this->session->userdata('personal_info');
			$dtaForm = $this->input->post(NULL, TRUE);
			$checkInp = null;
				if($dtaForm != null) {
					$this->session->set_userdata('member_info', $dtaForm);
					$checkInp = $this->memberService->checkInputMemberWithVoucher($dtaForm);
				}

			if($checkInp['response'] == "false") {
				$dt['err_msg'] = $checkInp['err'];
				$dt['prodCat'] = $this->memberService->getProductCategory();
		    	$dt['listBank'] = $this->memberService->getListBank();
				$this->setTempWebShop('webshop/memberRegisterBackError',$dt);
			}  else {
				$dt['show_provinsi'] = $this->memberService->showListProvinsi();
			    $dt['listStarterkit'] = $this->memberService->listStarterkit();
				//print_r($dt['listStartekit']);
				if($personal_info != null) {
					//$dt['shipping'] = $this->cartshopService->getShippingData($personal_info);
					$this->setTempWebShop('webshop/memberChooseSK',$dt);
				} else {
					$this->setTempWebShop('webshop/memberChooseSK',$dt);
				}
			}
		}
	}

	//$route['member/starterkit/back'] = 'webshop/member/backToChooseStarterkit';
	public function backToChooseStarterkit() {
		$personal_info = $this->session->userdata('personal_info');
		$dt['prodCat'] = $this->memberService->getProductCategory();
		$dt['listCargo'] = $this->memberService->getListCargo();
		$dt['show_provinsi'] = $this->shippingService->showListProvinsi();
	    $dt['listStarterkit'] = $this->memberService->listStarterkit();
		//$dt['listBank'] = $this->memberService->getListBank();
		if($personal_info != null) {
			$dt['shipping'] = $this->memberService->getShippingData($personal_info);
			$this->setTempWebShop('webshop/memberChooseSKWithData',$dt);
		} else {
			$this->setTempWebShop('webshop/memberChooseSK',$dt);
		}
	}

	//$route['member/payment/card'] = 'webshop/member/chooseCardType';
	function chooseCardType() {
		$data = $this->input->post(NULL, TRUE);
		if($data['tipekartu'] == "cc") {
			$this->load->view('webshop/member_payment_method/credit_card');
		} else {
			echo "<option value=\"\" selected>--Pilih jenis pembayaran--</option>";
			echo "<option value=\"mandiri_clickpay\">Mandiri ClickPay</option>";
			echo "<option value=\"cimb_click\">CIMB Clicks</option>";
			echo "<option value=\"virtual_account\">Virtual Account</option>";
			echo "<option value=\"epay_bri\">e-Pay BRI</option>";
		}
	}

	//$route[''member/payment/dc/(:any)'] = 'webshop/member/chooseDebitCardPaymentType/$1';
	function chooseDebitCardPaymentType($payType) {
		if($this->_checkSessionStoreUser()) {
			$this->load->view("webshop/member_payment_method/".$payType."");
		}
	}

	//$route['member/checkout'] = 'webshop/member/memberCheckout';
	/*public function memberCheckout() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$dataForm = $this->input->post(NULL, TRUE);
			$dt['sess_memb'] = $this->session->userdata('member_info');
			$dt['sess_pinfo'] = $this->memberService->sessionRegPersonalInfo($dataForm);
			$dt['sess_sk'] = $this->memberService->sessionRegStarterkit($dataForm);
			$dt['sess_ship'] = $this->memberService->setShippingInfo($dataForm);

			if($dt['sess_ship'] != 1 && $dt['sess_ship'] == null) {
					echo "<script>
						window.location.href='starterkit/back';
						alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
						</script>";
					//echo "isi sess :".$dt['sess_ship'];
				} else {
					$this->setTempWebShop('webshop/memberCheckout',$dt);
				}

			//$this->setTempWebShop('webshop/memberCheckout',$dt);
			//print_r($this->session->)
		}
	}*/

	//CHANGE PAYMENT GATEWAY TO SGO ON 12/07/2015
	//$route['member/checkout'] = 'webshop/member/memberCheckout';
	public function memberCheckout() {

		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$dataForm = $this->input->post(NULL, TRUE);
			//print_r($dataForm);
			$dt['sess_memb'] = $this->session->userdata('member_info');
			$dt['sess_pinfo'] = $this->memberService->sessionRegPersonalInfo_baru($dataForm);
			$dt['sess_sk'] = $this->memberService->sessionRegStarterkit($dataForm);
			$dt['sess_ship'] = $this->memberService->setShippingInfoBaru($dataForm);
			/*
			$dt['sess_pinfo'] = $this->memberService->sessionRegPersonalInfo_baru($dataForm);
			$dt['sess_sk'] = $this->memberService->sessionRegStarterkit($dataForm);
			$dt['sess_ship'] = $this->memberService->setShippingInfoBaru($dataForm);
			*/

			if($dt['sess_ship'] != 1 && $dt['sess_ship'] == null) {
					echo "<script>
						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
						</script>";
				} else {
				    //KEY DEVELOPMENT
				    //$dt['key'] = '331e590bc8ba4fd5694cc18c2ed8af6a';

					//KEY PRODUCTION
					//$dt['key'] = '31328e16fc07ae6447e34492eede79fc';
					$dt['key'] = '51edf5e8117da341a8be702d9bc18de5';

					//Remark by DION @ 07/12/2015
					/*
					if(getUserID() == "IDSPAAA66834") {
					   $dt['listBank'] = $this->memberService->getBankJohnDoe();
					} else {
					   $dt['listBank'] = $this->memberService->getBank();
					}*/

					//remark by DION 15/03/2016
					/*$dt['listBank'] = $this->memberService->getBank();
					$pay_memb_sgo_id = $this->session->userdata("pay_memb_sgo_id");
					if($pay_memb_sgo_id == null || !isset($pay_memb_sgo_id)) {
						$dt['payID'] = "RM".randomNumber(8);
						$this->session->set_userdata("pay_memb_sgo_id", $dt['payID']);
						$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
					} else {
						$dt['payID'] = $this->session->userdata("pay_memb_sgo_id");
						$del = $this->memberService->deleteTempMemberSGO($dt['payID']);
						$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
					}*/
					$dt['listBank'] = $this->memberService->getBank();
					$dt['payID'] = "RM".randomNumber(8);
                    $this->session->unset_userdata('pay_memb_sgo_id');
					$this->session->set_userdata("pay_memb_sgo_id", $dt['payID']);
					$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
					
					//GET SALDO VA
					$this -> load -> model("backend/be_hifi_model", "hifi");
					$resSaldo = $this->hifi->getSaldoVa(getUserID());
					if($resSaldo == null) {
						$dt['saldo_va'] = "0";
					} else {
					    $dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
					}

					$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/".$dt['payID'];
					//$this->setTemplateDevSGO('webshop/memberCheckoutSGO',$dt);
					$this->setTemplateSGO('webshop/memberCheckoutSGO',$dt);
				}
		}
	}

	//$route['member/checkout/sgo'] = 'webshop/member/memberCheckoutSGO';;
	public function memberCheckoutSGO() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$dataForm = $this->input->post(NULL, TRUE);
			//print_r($dataForm);
			$dt['sess_memb'] = $this->session->userdata('member_info');
			$dt['sess_pinfo'] = $this->memberService->sessionRegPersonalInfo_baru($dataForm);
			$dt['sess_sk'] = $this->memberService->sessionRegStarterkit($dataForm);
			$dt['sess_ship'] = $this->memberService->setShippingInfoBaru($dataForm);

			if($dt['sess_ship'] != 1 && $dt['sess_ship'] == null) {
					echo "<script>
						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
						</script>";
				} else {
				    //KEY DEVELOPMENT
				    $dt['key'] = '331e590bc8ba4fd5694cc18c2ed8af6a';

					//KEY PRODUCTION
					//$dt['key'] = '31328e16fc07ae6447e34492eede79fc';

					/*if(getUserID() == "IDSPAAA66834") {
					   $dt['listBank'] = $this->memberService->getBankJohnDoe();
					} else {
					   $dt['listBank'] = $this->memberService->getBank();
					}
					$pay_memb_sgo_id = $this->session->userdata("pay_memb_sgo_id");
					if($pay_memb_sgo_id == null || !isset($pay_memb_sgo_id)) {
						$dt['payID'] = "RM".randomNumber(8);
						$this->session->set_userdata("pay_memb_sgo_id", $dt['payID']);
						$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
					} else {
						$dt['payID'] = $this->session->userdata("pay_memb_sgo_id");
						$del = $this->memberService->deleteTempMemberSGO($dt['payID']);
						$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
					}*/

					$dt['listBank'] = $this->memberService->getBankJohnDoe();
					$dt['payID'] = "RM".randomNumber(8);
                    $this->session->unset_userdata('pay_memb_sgo_id');
					$this->session->set_userdata("pay_memb_sgo_id", $dt['payID']);
					$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
					//$this->setTempWebShop('webshop/memberCheckoutSGO',$dt);
					
					//GET SALDO VA
					$this -> load -> model("backend/be_hifi_model", "hifi");
					$resSaldo = $this->hifi->getSaldoVa(getUserID());
					if($resSaldo == null) {
						$dt['saldo_va'] = "0";
					} else {
					    $dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
					}

					//$this->setTemplateDevSGO('webshop/memberCheckoutSGO',$dt);
					$this->setTemplateDevSGO('webshop/memberCheckoutSGO',$dt);
				}
		}
	}

	//$route['member/checkout/sgo/pay/preview'] = 'webshop/member/memberPreviewPay';
	function memberPreviewPay() {
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
        
		/*$biaya = getTotalSKandShipCost2();
	    $dt['tot_hrs_dibayar'] = $biaya['total_pay'];
	    $dt['freeship'] = $biaya['freeship'];
		 */
		//print_r($dt);
		$dt = $this->input->post(NULL, TRUE);
		$dt['prodCat'] = $this->memberService->getProductCategory();
		$arr = array(
			"bank_code_payment" => $dt['bankid'],
			"payShip" => $shipping_jne_info['price'],
			"payAdm" => $dt['charge_admin'],
			"payConnectivity" => $dt['charge_connectivity'],
			"sentTo" => $personal_info['delivery'],
			"userlogin" => getUserID()
		);
		$dt['res'] = $this->memberService->updateBankCodePayment($dt['temp_paymentIdx'], $arr);
		
		//Add by DION @ 09/08/2016
		$updPaySGO = array(
					  "orderno" => $dt['temp_paymentIdx'],
					  "bank_code_payment" => $dt['bankid'],
					  "charge_admin" => $dt['charge_admin']
		);
		$this->memberService->updatePaydetSGO($updPaySGO);
		//END

		//Jika Transfer Manual Vera
		$bankid = $dt['bankid'];
		if($bankid == "29" || $bankid == "30" || $bankid == "31"){
			$orderno=$dt['temp_paymentIdx'];
			$kode_unik = $dt['kode_unik'];
			$kode_ref = $dt['kode_ref'];
			$kode_pay = $dt['kode_pay'];

			$charge_admin=$dt['charge_admin'];
			$payAdm= $charge_admin + $kode_unik;
			$nomPay = $dt['total_byr'];

			$updHdrSGO = array(
				"orderno" => $orderno,
				"payAdm" => $payAdm,
				"kodeunik" => $kode_unik,
				"koderef" => $kode_ref,
				"kodepay" => $kode_pay,
				"nompay" => $nomPay
			);
			$this->memberService->updateHdrdetSGO($updHdrSGO);

		}

		if(getUserID() == "IDSPAAA66834") {
			//$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/dev/".$dt['temp_paymentIdx'];
			//$this->setTemplateDevSGO($this->folderView.'payment_sgo_preview',$dt);
			if($bankid == "29" || $bankid == "30" || $bankid == "31"){
				$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/".$dt['temp_paymentIdx'];
				$this->setTemplateDevSGO($this->folderView.'payment_sgo_preview_dev2',$dt);
			}else{
				$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/".$dt['temp_paymentIdx'];
				$this->setTemplateDevSGO($this->folderView.'payment_sgo_preview_dev',$dt);
			}

		} else {
			if($bankid == "29" || $bankid == "30" || $bankid == "31"){
				$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/".$dt['temp_paymentIdx'];
				$this->setTemplateSGO($this->folderView.'payment_sgo_preview_dev2',$dt);
			}else{
				$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/".$dt['temp_paymentIdx'];
				$this->setTemplateSGO($this->folderView.'payment_sgo_preview_dev',$dt);
			}

			/*$dt['backURL'] = "https://www.k-net.co.id/member/sgo/finish/".$dt['temp_paymentIdx'];
			//$this->setTemplateSGO($this->folderView.'payment_sgo_preview',$dt);
			$this->setTemplateSGO($this->folderView.'payment_sgo_preview_dev',$dt);*/
		}
	}
	
	//$route['member/reg/pay/va'] = 'webshop/member/memberPayVa';
	public function memberPayVa() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			//print_r($data);
			$amount = (double) $data['pay_amount'];
			$this -> load -> model("backend/be_hifi_model", "hifi");
			$saldo = $this->hifi->getSaldoVa(getUserId());
			//print_r($saldo);
			if($saldo != null) {
			    $sisaSaldo = (int) $saldo[0]->amount; 
				//print_r($sisaSaldo);
				if($amount <= $sisaSaldo) {
					$this->load->service("webshop/Knet_payment_service",'paymentService');
					$double = $this->paymentService->checkDoubleTokenTransHdr($data['temp_orderno']);
				    if($double == null) {
						$dbqryx  = $this->load->database("db_ecommerce", TRUE);
						$arr = array(
							"trcd"    => $data['temp_orderno'],
							//TRS = Transaksi K-NET Sales
							"trtype"  => "TRM",
							"effect"  => "-",
							"effect_acc"  => "+",
							"custtype"=> "M",
							"novac"   => getUserNovac(),
							//"dfno"    => $header['memberid'],
							"dfno"    => getUserID(),
							"docno"   => "",
							"refno"   => "",
							"amount"  => $amount,
							"createnm"=> getUserID(),
							"tipe_dk" => "D",
							"tipe_dk_acc" => "K"
						);
						$potSaldo = $dbqryx->insert('va_cust_pay_det',$arr);
						
						if($potSaldo > 0) {
							$dta = $this->paymentService->getDataPaymentSGOByOrderID_withVch($data['temp_orderno']);
					
							$updtTabel =$this->paymentService->saveMemberSGO($data['temp_orderno'], $dta);
							//$resultInsMemb = $this->paymentService->getNewMemberData($data['temp_orderno']);
							$resultInsMemb = $this->paymentService->getNewMemberDataVA($data['temp_orderno']);
							//$this->paymentService->createNewShoppingVoucher($resultInsMemb);
							
							
							//$updtTabel = $this->paymentService->updateTrxSGO_baru($data['temp_orderno']);
							//$resultInsert = $this->paymentService->getInsertTrxOrderID($data['temp_orderno']);
							if($resultInsMemb != null) {
								
								
								$dbqryx->set('trcd', $resultInsMemb[0]->orderno);
								$dbqryx->where('trcd', $data['temp_orderno']);
								$dbqryx->update('va_cust_pay_det');
								
								$dbqryx->set('trcd', $resultInsMemb[0]->orderno);
								$dbqryx->where('trcd', $data['temp_orderno']);
								$dbqryx->update('va_cust_pay_det_acc');
								
								sendWelcomeEmail($resultInsMemb);
								$this->paymentService->sendTrxSmsRegMember($resultInsMemb);
							
							}
							
							//GET SALDO VA
							
							$resSaldo = $this->hifi->getSaldoVa(getUserID());
							if($resSaldo == null) {
								$dt['saldo_va'] = "0";
							} else {
								$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
							}

							
							$sisa = $sisaSaldo - $amount;
							$resJson = jsonTrueResponse(null, "Sisa saldo : ".$sisa); 
						} else {
							$resJson = jsonFalseResponse("Potong saldo gagal..");
						}
					
					} else {
						$resJson = jsonFalseResponse("Transaksi double..");
					}
					
					
					
				} else {
					$resJson = jsonFalseResponse("Saldo VA anda tidak cukup : ".number_format($sisaSaldo, 0, ",", "."). ", total pembayaran : ".number_format($amount, 0, ",", "."));
				}
				$resJson = jsonTrueResponse(null, "Sisa saldo : "); 
			//$sisaSaldo = (double) $saldo[0]->amount;
			} else {
				$resJson = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
			}
			
			echo json_encode($resJson);
		}	
	}

	//$route['member/file/sk'] = 'webshop/member/getStarterkitFile';
	public function getStarterkitFile() {
	    if($this->_checkSessionStoreUser()) {
			//echo $this->sk_path_file;
			$file = $this->sk_path_file;
			$filename = 'test.pdf'; /* Note: Always use .pdf at the end. */

			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($file));
			header('Accept-Ranges: bytes');

			@readfile($file);
		}
	}

	//$route['member/check/inp/(:any)'] = 'webshop/member/checkInput/$1';
	function checkInput($order_id) {
		$qry = "SELECT a.idstk, a.nmstkk, a.total_pay, a.total_bv, a.payShip, a.payAdm, a.payConnectivity,
				       a.bank_code_payment,a.sentTo, b.membername, a.pricecode,
				       a.bonusmonth, c.charge_connectivity,
				       b.sponsorid, b.idno, b.addr1, b.addr2,
					   b.addr3,b.tel_hp,b.email,b.stk_code,b.sex,b.birthdt,
					   b.acc_no,b.acc_name,b.bankid,b.joindt,b.trx_no,b.kdpos,
					   b.[state],b.ip_address,b.[password],b.recruiterid,
					   b.flag_voucher, b.voucher_no, b.voucher_key,
				       a.userlogin, d.prdcd, d.prdnm, d.qty, d.dpr, d.bvr
				FROM ecomm_trans_hdr_sgo a
				INNER JOIN ecomm_memb_ok_sgo b ON (a.orderno = b.trx_no)
				INNER JOIN ecomm_bank c ON (a.bank_code_payment = c.id)
				INNER JOIN ecomm_trans_det_prd_sgo d ON (a.orderno = d.orderno)
				WHERE a.orderno = '$order_id'";
		$query = $this->db->query($qry);
		$pay_tipe = "0";
			$voucherno = "";
			$voucherkey = "";
			$kdpos1 =  "000004";
		foreach ($query->result() as $row) {
			$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
		                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
		                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
		                    flag_voucher, voucher_no, voucher_key, is_landingpage)
		                    values ('".$row->sponsorid."','TES','".strtoupper($row->membername)."','".$row->idno."','".strtoupper($row->addr1)."',
		                            '".strtoupper($row->addr2)."','".strtoupper($row->addr3)."','".$row->tel_hp."','".$row->email."','".$row->stk_code."','".$row->sex."',
		                            '".$row->birthdt."','".$row->acc_no."','".strtoupper($row->acc_name)."','".$row->bankid."',
		                            '0000-00-00','TES ORDERNO','".$kdpos1."','".$row->state."','".$row->ip_address."','".$row->prdcd."',
		                            '".$row->password."','".$row->recruiterid."','".$row->recruiterid."',
		                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '1')";
		    echo $insMemb;
		}
	}

	function cek_jarak($latitude1,$longitude1){
			$dt = $this->cek_distance($latitude1,$longitude1);
			$result= json_decode($dt, TRUE);
			echo($result);
	}

	function cek_jarak2($latitude1,$longitude1){
		$dt = $this->cek_distance2($latitude1,$longitude1);
		$result= json_decode($dt, TRUE);
		echo($result);
	}

	


/** 
 * @Author: Ricky 
 * @Date: 2019-03-05 16:20:52 
 * @Desc: dev methods 
 */

	//$route['dev/member/check'] = 'webshop/member/checkFormData';
	function checkFormData() {
		if($this->_checkSessionStoreUser()) {
			$cart = $cart_check = $this -> cart -> contents();
			if(!empty($cart)) {
				$this->session->unset_userdata('personal_info');
				$this->session->unset_userdata('cart_contents');
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->unset_userdata('sender_address');
				$this->session->unset_userdata('destination_address');
				$this->session->unset_userdata('jne_branch');

			}
            $dt['listCargo'] = $this->memberService->getListCargo();
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$member_info = $this->session->userdata('member_info');
			$personal_info = $this->session->userdata('personal_info');
			$dtaForm = $this->input->post(NULL, TRUE);
			$checkInp = null;
				if($dtaForm != null) {
					$this->session->set_userdata('member_info', $dtaForm);
					$checkInp = $this->memberService->checkInputMemberWithVoucher($dtaForm);
				}

			if($checkInp['response'] == "false") {
				$dt['err_msg'] = $checkInp['err'];
				$dt['prodCat'] = $this->memberService->getProductCategory();
		    	$dt['listBank'] = $this->memberService->getListBank();
				$this->setTempWebShop('webshop/memberRegisterBackError',$dt);
			}  else {
				$dt['show_provinsi'] = $this->memberService->showListProvinsi();
			    $dt['listStarterkit'] = $this->memberService->listStarterkit();
				//print_r($dt['listStartekit']);
				if($personal_info != null) {
					//$dt['shipping'] = $this->cartshopService->getShippingData($personal_info);
					$this->setTempWebShop('webshop/memberChooseSK',$dt);
				} else {
					$this->setTempWebShop('webshop/memberChooseSK',$dt);
				}
			}
		}
		
		/*$data = $this->input->post(NULL, TRUE);

        $check = $this->memberService->checkInputMemberWithVoucher($data);
		print_r($check);
		$check = $this->memberService->checkNoKTP($data['idno']);
		if($check['sukses'] == "TRUE") {
			
		}
		return $check;

		$checkRec = $this->memberService->getMemberInfo($data['sponsorid']);
		return $checkRec;

		$checkSpr = $this->memberService->getMemberInfo($data['recruiterid']);
		return $checkSpr;

		if ($data != NULL) {
			if (!empty($check)) {
				echo "KTP sudah terdaftar";
			} else {
				echo "KTP belum terdaftar";
			}
		}*/
	}
}

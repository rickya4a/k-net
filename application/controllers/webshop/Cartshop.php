<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cartshop extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->service("webshop/Cartshop_service", 'cartshopService');
		$this->folderView = "webshop/payment_sgo_vch/";
	}

	//$route['cart/member/id/(:any)'] = 'webshop/cartshop/getMemberInfo/$1';
	function getMemberInfo($idmember)
	{
		//$showstk = $this->cartshopService->getMemberInfo($idmember);
		$showstk = $this->cartshopService->getMemberInfo3($idmember);
		//print_r($expression);
		echo json_encode($showstk);
	}

	//$route['cart/bnsperiod/get'] = 'webshop/cartshop/getBnsPeriod';
	function getBnsPeriod()
	{
		$ss = $this->cartshopService->getCurrentPeriod();
		//print_r($expression);
		echo json_encode($ss);
	}

	//$route['cart/prdcd/(:any)'] = 'webshop/cartshop/salesPrdcd/$1';
	function salesPrdcd($prdcd)
	{
		$dt['prdcd'] = $prdcd;
		$this->load->service("webshop/Login_service", 'loginService');
		//$dt['prodCat'] = $this->cartshopService->getProductCategory();
		$this->load->model("webshop/product_model", "product");
		$dt['captcha_img'] = $this->loginService->createChaptcha();
		$dt['prod'] = $this->product->getProductShopByID($prdcd);
		//print_r($dt['detPrd']);
		$this->load->view('shopinc/headerClean', $dt);
		$this->load->view('webshop/salesPrdcd', $dt);
		//$this->load->view('shopinc/footer',$dt);
	}

	//$route['cart/prd/add'] = 'webshop/cartshop/addPrdToCart';
	public function addPrdToCart()
	{
		//$this->session->sess_destroy();
		//print_r($this->input->post(NULL, TRUE));
		//$prdcd = $this->input->post('prdcd');
		$data = $this->input->post(NULL, TRUE);
		$this->load->service("webshop/Login_service", 'loginService');
		if ($this->form_validation->run('login') == FALSE) {
			echo "<script>alert('Semua field harus diisi..')</script>";
			echo "sdsd";
		} else {
			//$data = $this->input->post(NULL, TRUE);
			//print_r($data);
			$login = $this->loginService->checkAuthLoginSSS($data);

			if ($login != null) {
				$telhp = getUserPhone();
				$email = getUserEmail();
				$this->session->set_userdata('store_info', $login);
				$this->session->set_userdata('pricecode', "12W3");
				//print_r($login);
				$promo = $this->loginService->checkListPromo();
				$this->session->set_userdata('promo', $promo);


				if (strlen($login[0]->tel_hp) < 7 || $login[0]->email == "") {


					echo "<script>
					alert('Silahkan update email dan nomor hp anda..');
					window.location.href='" . base_url() . "updEmailFirst';
					</script>";
				} else {
					if (!$this->cartshopService->IfDoubleInputCart($data['prdcd'])) {
						$arr = $this->cartshopService->addToCartXXX($data);
					} else {
						$arr = array("response" => "false", "message" => "Data product " . $this->input->post('prdnm') . " sudah ada di cart");
					}
					//print_r($arr);
					header('Location: http://www.k-net.co.id/cart/list');
					exit;
				}
			} else {
				//$this->member_login("Proses Otentifikasi Login gagal");
				//header('Location: http://www.k-net.co.id/cart/P');
				header("Location: http://www.k-net.co.id/cart/prdcd/" . "$data[prdcd]");
				exit;
			}
			//print_r($login);
		}
	}

	//$route['cart/add'] = 'webshop/cartshop/addToCart';
	function addToCart()
	{
		$form = $this->input->post(NULL, TRUE);
		if ($this->_checkSessionStoreUser()) {
			$member_info = $this->session->userdata('member_info');
			if (!empty($member_info)) {
				$this->session->unset_userdata('personal_info');
				$this->session->unset_userdata('starterkit_prd');
				$this->session->unset_userdata('memb_choose_pay');
				$this->session->unset_userdata('member_info');
				$this->session->unset_userdata('sender_address');
				$this->session->unset_userdata('destination_address');
				$this->session->unset_userdata('jne_branch');
				$this->session->unset_userdata('shipping_jne_info');
			}
			
			if (!$this->cartshopService->IfDoubleInputCart($this->input->post('prdcd'))) {
				if(array_key_exists("desc", $form)){
					$arr = $this->cartshopService->addToCartVera();
				} else {	
					$arr = $this->cartshopService->addToCart();
				}	
			} else {
				if($this->input->post('desc') == 1){
					$arr = $this->cartshopService->addToCartVera();
					//$arr = array("response" => "false", "message" => "Data product Paket K-ION => " . $this->input->post('prdnm') . " sudah ada di cart");
				}else{
					$arr = array("response" => "false", "message" => "Data product " . $this->input->post('prdnm') . " sudah ada di cart");
				}
				//$arr = array("response" => "false", "message" => "Data product " . $this->input->post('prdnm') . " sudah ada di cart");
			}
			echo json_encode($arr);
		}
	}

	//$route['cart/list'] = 'webshop/cartshop/listCart';
	public function listCart()
	{
		//if(getUserID() == "IDSPAAA66834") { 
			$this->session->unset_userdata('spec_free_prd');
		//}
		$this->load->model("webshop/Shared_module", "mShared");
		
		if ($this->_checkSessionStoreUser()) {
			$dt['formAction'] = "" . site_url('shipping/postShipping') . "";
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			//$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();    
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			//print_r($dt['bns']);
			$dt['cart'] = $this->cart->contents();
			$personal_info = $this->session->userdata('personal_info');
			$dt['min_free_bonus'] = 700000;
			if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
				//$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
				$dt['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
			} else {
				$dt['listCargo'] = $this->cartshopService->getListCargo();
			}


			//================START EDIT HILAL FOR BONUS PERIOD @2019-03-23=====================
			$dt['bns_hilal'] = $this->mShared->showCurrentPeriod_hilal(); //add 2019-03-22 hilal
			//print_r($dt['bns_hilal']);   
			foreach($dt['bns_hilal'] as $row){
				if($row->date_only_now > $row->endofdatebnsperiod){
					//echo "masuk sini if";
					$dt['bns_hilal'] = array($row->bnsperiod_now);
				}else{
					$dt['bns_hilal'] = array($row->bnsperiod_prev, $row->bnsperiod_now);
				}
		    }
			//================END EDIT HILAL FOR BONUS PERIOD @2019-03-23=======================

//			if (getUserID() == "IDSPAAA66834") {
//
//				$this->load->model("webshop/Sales_model", "mSales");
//				$dt['hadiah_awal'] = $this->mSales->getListHadiah3();
			/*echo "<pre>";
			print_r($dt['hadiah_awal']);
			echo "</pre>";*/
//			}

			//mendapatkan jam buka gosend
			$day = date('D');
			//$now = new Datetime("now");
			$now= date("H:i:s");
			$tgl_now= date('Y-m-d');
			$this->load->model('lokasi/M_location', 'M_location');

			$holiday=0;
			//cek tanggal libur nasional
			$getHoli= $this->M_location->getLibur($tgl_now);
			$holiday= $getHoli[0]->holiday;

			//Instant
			//$getInstant= $this->M_location->getSchedule($day,'Instant');
			$getInstant= $this->M_location->getSchedule($day,'Instant');
			$open_inst= $getInstant[0]->opened;
			$closed_inst= $getInstant[0]->closed;
			$closed_pay= $getInstant[0]->closed_pay;
			$ket_inst= $getInstant[0]->keterangan;
			$begintime1 = new DateTime($open_inst);
			$endtime1 = new DateTime($closed_inst);

				if($now >= $open_inst && $now <= $closed_inst){
					$dt['value1']='"1-"+rupiah1';
					$dt['option1']='"Instant "+instant_hours+" Rp"+ rupiah1';
				} else {
					$dt['value1']='""';
					$dt['option1']='"'.$ket_inst.'"';
				}

			//SameDay
			$getSame= $this->M_location->getSchedule($day,'SameDay');
			$open_same= $getSame[0]->opened;
			$closed_same= $getSame[0]->closed;
			$ket_same= $getSame[0]->keterangan;

			$begintime2 = new DateTime($open_same);
			$endtime2 = new DateTime($closed_same);

				if($now >= $open_same && $now <= $closed_same){
					$dt['value2']='"2-"+rupiah1';
					$dt['option2']='"SameDay "+sameday_hours+" Rp"+ rupiah1';
				} else {
					$dt['value2']='""';
					$dt['option2']='"'.$ket_same.'"';
				}
			
			//cargo GOSEND
			$getCargo= $this->M_location->getCargo('4');
			$dt['info_html']=$getCargo[0]->info_html;
			$dt['begintime']= $getCargo[0]->open;
			$dt['endtime']= $getCargo[0]->close;

			//$dt['begintime'] = new DateTime($dt['open_gsend']);
			//$dt['endtime'] = new DateTime($dt['closed_gsend']);
			$dt['val_holiday']= $holiday;

			//END Gosend

			$dt['listFreeProduct'] = null;
			if ($personal_info != null) {
				//ubah dion @ 21/05/2017
				if (array_key_exists("shipper", $personal_info)) {
					if ($personal_info['shipper'] == "1") {
						$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
						$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
						$dt['show_provinsiKP'] = $this->cartshopService->showListProvinsiKP();//COD
					} else {
						$dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb();
						$dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);
						$dt['show_provinsiKP'] = $this->cartshopService->showListProvinsiKP();//COD
					}
				} else {
					$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
					$dt['show_provinsiKP'] = $this->cartshopService->showListProvinsiKP();//COD
				}


				//print_r($dt['shipping']);
				//echo "c";
				if (getUserID() == 'IDSPAAA66834' || getUserID() == "IDSPAAB01065") {
					//$this->setTempWebShop('webshop/shipping2TestWithAddress',$dt);
					//$this->setTempWebShop('webshop/promo_nov/ShippingTes', $dt); //punya fendi
					//$this->setTempWebShop('webshop/vera/shipping2Test2JhonDoe', $dt);

					$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
					$dt['show_provinsiKP'] = $this->cartshopService->showListProvinsiKP();//COD

					$dt['whoisnow'] = "Halaman Jhon Doe Cart list setelah back";
					$this->setTempWebShop('webshop/vera/shipping2Test2JhonDoe3', $dt);
					//$this->setTempWebShop('webshop/vera/shipping2Test2ProdBack', $dt);

				} 
				else {
					//upd by dion @ 07/11/2018
					//$this->setTempWebShop('webshop/shipping2TestWithAddress', $dt);
					//print_r($dt['bns_hilal']);
					$dt['whoisnow'] = "";
					$this->setTempWebShop('webshop/shipping2Test', $dt);
				}

			}
			else {
				$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
				$dt['show_provinsiKP'] = $this->cartshopService->showListProvinsiKP();//COD
				$res = $this->cartshopService->getAddressReferenceByID(getUserID());
				if ($res != null) {
					$personal_infox = array(
						"provinsi" => $res[0]->provinsi,
						"kota" => $res[0]->kota,
						"kecamatan" => $res[0]->kecamatan,
					);
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);
					$this->cartshopService->setAutomaticPersonalInfo($res);
					//print_r($dt['shipping'])
					//echo "e";
					if (getUserID() == 'IDSPAAA66834') {
						//$this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
						//$dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();
						$dt['whoisnow'] = "Halaman Jhon Doe Cart list ke Xsds";
						$this->setTempWebShop('webshop/shippingAutoTest', $dt);


					} else {
						//$this->setTempWebShop('webshop/shippingAutoAddrReff',$dt);
						//upd by dion 09/09/2018
						//$this->setTempWebShop('webshop/shippingAutoTest', $dt);
						$dt['whoisnow'] = "";
						$this->setTempWebShop('webshop/shipping2Test', $dt);
					}

					//$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
				}
				else {
					//echo "v";
					if (getUserID() == 'IDSPAAA66834' || getUserID() == "IDSPAAB01065") {
//					    $this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
//			            $dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();
						//echo "s";
						//$this->setTempWebShop('webshop/vera/shipping2Test2', $dt);
						
						//echo "test John Doe";
						//$this->setTempWebShop('webshop/vera/shipping2Test2JhonDoe', $dt);
						
						
						$dt['whoisnow'] = "Halaman Jhon Doe Cart first";
						$this->setTempWebShop('webshop/vera/shipping2Test2JhonDoe3', $dt);
						//$this->setTempWebShop('webshop/vera/shipping2Test2Prod', $dt);
					} 
					else {
						//$this->setTempWebShop('webshop/shipping2',$dt);
						//upd by dion 16/11/2018
						//$this->setTempWebShop('webshop/shipping2Test', $dt);
						$dt['whoisnow'] = "";
						$this->setTempWebShop('webshop/vera/shipping2Test2', $dt);
					}
				}
			}
		}
	}

	//$route['check/promoApr'] = 'webshop/cartshop/checkPromoApril2019';
	function checkPromoApril2019() {
		$data = $this->input->post(NULL, TRUE);
		$this->load->model('webshop/promoApr2019', 'm_promoApr');
		
		$upd = $this->updateCartBeforeProceed();
		$prdArr = $this->cart->contents();
		$checkPrd = $this->m_promoApr->checkPromoAprilAnd($data);
		//print_r($data['amountBV'][0]);
		//print_r($prdArr);
		//print_r($checkPrd);
		//$i = 0;
		$checkPromoValid = 0;
		
		foreach($checkPrd as $items1) {
			$prd1 = $items1->prdcd;
			$qty1 = $items1->min_qty;
			$prd2 = $items1->prdcd2;
			$qty2 = $items1->qty2;
		}
		
		//hilal start
		$cekPromoApr19 = 0;
		foreach($prdArr as $items2) {
			$prdCart = $items2['id'];
			$qtyCart = $items2['qty'];
			
			if($prdCart == $prd1 && $qtyCart == $qty1){
				$cekPromoApr19 = $cekPromoApr19 + 1;
			}
			
			if($prdCart == $prd2 && $qtyCart == $qty2){
				$cekPromoApr19 = $cekPromoApr19 + 1;
			}
			//echo $prdCart.'*'.$qtyCart.'*';
		}
		
		if($cekPromoApr19 == 2) {
			$checkPromoValid = 1;
		}
		
		
		//tambahan May-06-2019, jika BV min 150, maka mendapatka free membership
		$jumPrd = count($data['amountBV']);
		$total_bv_mf = 0;
		$total_bv_mf_min = 150;
		for($i=0;$i<$jumPrd;$i++) {
			$total_bv_mf += $data['amountBV'][$i]; 
		}
		if($total_bv_mf >= 150){
			$checkPromoValid = 3;
		}

		/*
		$total_bv_mf = $data['amountBV'][0];
		$total_bv_mf_min = 150;
		if($total_bv_mf >= 150){
			$checkPromoValid = 3;
		} */
		//echo "total_bv_mf = $total_bv_mf </br> total_bv_mf_min = $total_bv_mf_min </br> checkPromoValid = $checkPromoValid";
		//hilal end
		
		//$checkPromoValid = 0;
		$check = $this->m_promoApr->checkPromoApril($data);
		if($check == 1) {
			$checkPromoValid = 1;
		}	

		$checkGamatPromo = 0;
		$checkBv = $this->m_promoApr->getTotalBvandPrice($data);
		if($checkBv >= 50) {
			$checkGamatPromo = 1;
		}	
		//$arrResponse = jsonFalseResponse("Promo tidak memenuhi");
		$arrResponse = array(
			"promoGamat" => $checkGamatPromo,
			"promoApr" => $checkPromoValid,
		);
		echo json_encode($arrResponse);
	}

	//$route['cart/specOfferProd'] = 'webshop/cartshop/specOfferProd';
	public function specOfferProd() {
		$data = $this->input->post(NULL, TRUE);
		$eerr = $this->updateCartBeforeProceed();
		$dt['promoApr'] = $data['promoApr'];
		$dt['promoGamat'] = $data['promoGamat'];

		//echo "<pre>";
		//print_r($data);
		//echo "</pre>";

		if ($this->cartshopService->checkShippingDataBaru($data)) {
			$dt['cart'] = $this->cart->contents();
			//$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
			if ($data != null) {
				$this->load->model("webshop/Member_model", "member_model");
				//$dt['listStockist'] = $this->member_model->getListStockist();
				//$dt['listStockist'] = $this->member_model->getListStockistDion();
				//$prdx = set_list_array_to_stringCart($dt['cart'], "id");
				//edited, disabled by Dion @13/10/2015
				//$dt = $this->cartshopService->getProductShop($prdx, $this->session->userdata('pricecode'));
				//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
				//$ship = $this->cartshopService->setShippingInfo();
				$ship = $this->cartshopService->setShippingInfoBaru();
				$dt['prodCat'] = $this->cartshopService->getProductCategory();
				//$dt['bank'] = $this->Member_model->getListAffiliateBank();
				$dt['free_memb'] = $data['free_memb'];
				//ubah dion 15/11/2018
				//$this->setTempWebShopNonMember($this->folderView . 'RegisterMembership', $dt);
				$this->setTempWebShop($this->folderView . 'specOfferProd', $dt);
				
			}
		}
	}

	//$route['promo/agust2018'] = 'webshop/cartshop/showListPromoAgustus2018';
	function showListPromoAgustus2018()
	{
		$this->load->model('login_model', 'login');
		$res = $this->login->showListPromoAgustus2018();
		if ($res == null) {
			$arr = jsonFalseResponse("tidak ada promo");
		} else {
			$arr = jsonTrueResponse($res);
		}
		echo json_encode($arr);
	}

	//$route['cart/list2'] = 'webshop/cartshop/liatCart';
	function liatCart()
	{
		//print_r($this->session->all_userdata());
		//echo "<br /><br />";
		echo "member info : <br />";

		echo "<pre>";
		print_r(getUserid());
		echo "</pre>";

		echo "<pre>";
		print_r(MinTrxPromoNonMember());
		echo "</pre>";
		echo "store_info : <br />";
		echo "<pre>";
		print_r($this->session->userdata('store_info'));

		echo "</pre>";
		print_r($this->session->userdata('no_hp_konfirmasi'));
		echo "no_hp_konfirmasi : ";
		print_r($this->session->userdata('no_hp_konfirmasi'));
		echo "<br /><br />";
		echo "cart_contents : <br />";
		//print_r($this->session->userdata('cart_contents'));
		echo "<pre>";
		print_r($this->cart->contents());
		echo "</pre>";
		//print_r($this->session->userdata('cart_contents'));
		echo "<table border=1>";
		//echo "<tr><th>ID</th><th>Name</th><th>Qty</th><th>BV</th><th>12W3</th><th>12E3</th><th>Berat</th></tr>";
		/*foreach($cart as $items) {
            echo "<tr>";
            echo "<td>".$items['id']."</td>";
            echo "<td>".$items['name']."</td>";
            echo "<td>".$items['qty']."</td>";
            echo "<td>".$items['bv']."</td>";
            echo "<td>".$items['west_price']."</td>";
            echo "<td>".$items['east_price']."</td>";
            echo "<td>".$items['weight']."</td>";
            echo "</tr>";
        }*/
		//echo "</table>";
		echo "<br />";
		//echo "<br /><br />";
		echo "promo : <br />";
		echo "<pre>";
		print_r($this->session->userdata('promo'));
		echo "</pre>";
		echo "<br /><br />";
		echo "personal_info : <br />";
		echo "<pre>";
		print_r($this->session->userdata('personal_info'));
		echo "</pre>";
		echo "<br /><br />";
		echo "shipping_jne_info : <br />";
		echo "<pre>";
		print_r($this->session->userdata('shipping_jne_info'));
		echo "</pre>";
		echo "<br /><br />";
		echo "sender_address : ";
		echo "<pre>";
		print_r($this->session->userdata('sender_address'));
		echo "</pre>";
		echo "<br /><br />";
		echo "destination_address : ";
		echo "<pre>";
		print_r($this->session->userdata('destination_address'));
		echo "</pre>";
		echo "<br /><br />";
		echo "jne_branch : ";
		echo "<pre>";
		print_r($this->session->userdata('jne_branch'));
		echo "</pre>";
		echo "<br /><br />";
		echo "trxtype : ";
		print_r($this->session->userdata('trxtype'));
		echo "<br /><br />";
		echo "pay_sgo_id : ";
		print_r($this->session->userdata('pay_sgo_id'));
		echo "pay_memb_sgo_id : ";
		print_r($this->session->userdata('pay_memb_sgo_id'));
		echo "<br /><br />";
		echo "pricecode :";
		print_r($this->session->userdata('pricecode'));
        if($this->session->userdata('pricecode') == null) {
			echo "pricecode ngga ada";
		}

		$cust_memb = $this->session->userdata("non_member_info");
		$ss = getUserID();
		if (isset($cust_memb)) {
			$is_login = "2";
			$tot_paynet = getTotalPayNet();
		} else {
			$memb = $this->session->userdata("store_info");
			if (isset($memb)) {
				$is_login = "1";
				$tot_paynet = 0;
			}
		}
		//print_r($this->session->userdata('promo'));
		//print_r(getTotalPayNetAndShipCost2());
		echo "<pre>";
		print_r($this->session->all_userdata());
		echo "</pre>";
	}

	//$route['cart/remove'] = 'webshop/cartshop/removeCart';
	function removeCart()
	{
		$arr = $this->cartshopService->removeCart();
		echo json_encode($arr);
	}

	//$route['cart/update'] = 'webshop/cartshop/updateCart';
	function updateCart()
	{
		if ($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			$upd = $this->cartshopService->updateCart($data);
			$dt['cart'] = $this->cart->contents();
			$this->session->set_userdata('pricecode', $data['pricecode']);
			$this->load->view('webshop/order_details', $dt);
		}
	}

	//$route['cart/upd'] = 'webshop/cartshop/updateCartBeforeProceed';
	function updateCartBeforeProceed()
	{

		if ($this->_checkSessionStoreUser()) {
			$arr = jsonFalseResponse(NULL, "Update Cart gagal..!");
			$data = $this->input->post(NULL, TRUE);
			$upd = $this->cartshopService->updateCart($data);
			$this->session->set_userdata('pricecode', $data['pricecode']);
			if ($upd) {
				$arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
			}
			//echo json_encode($arr);
			return $arr;
		}
	}

	//$route['cart/vch/check/(:any)'] = 'webshop/cartshop/getVoucherValidCheck/$1';
	function getVoucherValidCheck($vch)
	{
		$arr = $this->cartshopService->getVoucherValidCheck($vch);
		echo json_encode($arr);
	}


	//$route['cart/checkout'] = 'webshop/cartshop/CheckoutCart';
	/*function CheckoutCart() {
       if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			if($this->cartshopService->checkShippingData($data)) {
				$dt['cart'] = $this->cart->contents();
				if($data != null) {
					$eerr = $this->updateCartBeforeProceed();
					$prdx = set_list_array_to_stringCart($dt['cart'], "id");
					//edited, disabled by Dion @13/10/2015
					//$dt = $this->cartshopService->getProductShop($prdx, $this->session->userdata('pricecode'));
					// if(getUserID() == "IDJHID000065") {
					//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
					$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
					//$ship = $this->cartshopService->setShippingInfo();
					$ship = $this->cartshopService->setShippingInfoBaru();
					

					$dt['prodCat'] = $this->cartshopService->getProductCategory();

					
					$dt['listBank'] = $this->cartshopService->getBank();
					//production
					$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
					//development 0df5835ee198d49944c372ead860c241
					//$dt['key'] = '0df5835ee198d49944c372ead860c241';

					

					$dt['payID'] = "EC".randomNumber(8);
					$this->session->unset_userdata('pay_sgo_id');
					$this->session->set_userdata("pay_sgo_id", $dt['payID']);
					$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);

					$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['payID'];
					//print_r($dt);
				}

				if($data['delivery'] == "2" && $ship != 1 && $ship == null) {
					echo "<script>
						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
						</script>";
				} else {
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					//$this->setTemplateDevSGO('webshop/payment_sgo',$dt);
					$this->setTemplateSGO('webshop/payment_sgo',$dt);
				}

			} else {
				//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
				//redirect('cart/list');
				echo "<script>
						window.location.href='list';
						alert('Mohon data dilengkapi dahulu..');
						</script>";
			}
		}
    }*/

	//$route['cart/checkout'] = 'webshop/cartshop/CheckoutCart';
	function CheckoutCart()
	{
		if ($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			if ($this->cartshopService->checkShippingDataBaru($data)) {
				$dt['cart'] = $this->cart->contents();
				$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
				if ($data != null) {
					$eerr = $this->updateCartBeforeProceed();
					$prdx = set_list_array_to_stringCart($dt['cart'], "id");
					//edited, disabled by Dion @13/10/2015
					//$dt = $this->cartshopService->getProductShop($prdx, $this->session->userdata('pricecode'));
					//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
					$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
					//$ship = $this->cartshopService->setShippingInfo();	
					$ship = $this->cartshopService->setShippingInfoBaru();
					$dt['prodCat'] = $this->cartshopService->getProductCategory();


					if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
						$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
					} else {
						$dt['listBank'] = $this->cartshopService->getBank();
					}
					//production
					//$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';	
					//development 0df5835ee198d49944c372ead860c241
					$dt['key'] = '0df5835ee198d49944c372ead860c241';

					/*	
					$pay_sgo_id = $this->session->userdata("pay_sgo_id");
					if($pay_sgo_id == null || !isset($pay_sgo_id)) {
						$dt['payID'] = "EC".randomNumber(8);
						$this->session->set_userdata("pay_sgo_id", $dt['payID']);
						$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					} else {
						$dt['payID'] = $this->session->userdata("pay_sgo_id");
						$del = $this->cartshopService->deleteTempTrxSGO($dt['payID']);
						$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					}
					 * */
					//add testing by DION 05/03/2016

					/*------------------------------------------------------------
					 * PROMO 17 AGUSTUS FREE SHIPPING WITH BUYING SPECIAL PRODUCT
					 * -----------------------------------------------------------*/

					/*$dt['prd_promo'] = $this->cartshopService->getPromo17Agustus();
					if($dt['prd_promo'] != null) {
						$arr = $dt['prd_promo']['arrayData'];
						
						//print_r($dt['cart']);
						$ketemu = 0;
						foreach($dt['cart'] as $items) {
							if(in_array($items['id'], $arr)) {
								$ketemu = 1;
								break;
							}
						}	
						if($ketemu == 1) { */
					//$totDiscount = $ship['price'];
					if (getUserID() == "IDSPAAA96407") {
						$dt['biaya'] = getTotalPayNetAndShipCostTerbaru();
					}
					/*}
                }*/
					/*------------------------------------------------------------
					 * END PROMO 17 AGUSTUS CODE
					 * -----------------------------------------------------------*/

					//GET SALDO VA
					$this->load->model("backend/be_hifi_model", "hifi");
					$resSaldo = $this->hifi->getSaldoVa(getUserID());
					if ($resSaldo == null) {
						$dt['saldo_va'] = "0";
					} else {
						$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
					}

					$dt['payID'] = "EC" . randomNumber(8);
					//$this->session->unset_userdata('pay_sgo_id');
					//$this->session->set_userdata("pay_sgo_id", $dt['payID']);
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);

					$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/" . $dt['payID'];
					//print_r($dt);
				}

				//ASLI DION
				/*if($data['delivery'] == "2" && $ship != 1 && $ship == null) {
					echo "<script>
//						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
				} else {
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
					$this->setTemplateSGO($this->folderView . 'payment_sgo_vch', $dt);
					//$this->setTemplateSGO('webshop/payment_sgo',$dt);
				}*/
				//tambahan Vera
				/*if ($data['delivery'] == "2") {
					if ($this->input->post("shipper") == 1) {
						if ($ship != 1 && $ship == null) {
							//echo "TES".$ship;
							echo "<script>
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
						}else {
							$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
							$this->setTemplateSGO($this->folderView . 'payment_sgo_vch', $dt);
							//$this->setTemplateSGO('webshop/payment_sgo',$dt);
						}
					}else {
						$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
						$this->setTemplateSGO($this->folderView . 'payment_sgo_vch', $dt);
						//$this->setTemplateSGO('webshop/payment_sgo',$dt);
					}

				}*/

				//ASLI DION
				if($data['delivery'] == "2" && $ship != 1 && $ship == null) {
					echo "<script>
//						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist tes2 nya..');
						</script>";
				} else {
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
					$this->setTemplateSGO($this->folderView . 'payment_sgo_vch', $dt);
					//$this->setTemplateSGO('webshop/payment_sgo',$dt);
				}

			} else {
				//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
				//redirect('cart/list');
				echo "<script>
						window.location.href='list';
						alert('Mohon data dilengkapi dahulu..');
						</script>";
			}
		}
	}

	//$route['cart/checkout/sgo'] = 'webshop/cartshop/CheckoutCartSGO';
	function CheckoutCartSGO()
	{
		if ($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			if ($this->cartshopService->checkShippingDataBaru($data)) {
				$dt['cart'] = $this->cart->contents();
				$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
				if ($data != null) {
					$eerr = $this->updateCartBeforeProceed();
					$prdx = set_list_array_to_stringCart($dt['cart'], "id");
					//edited, disabled by Dion @13/10/2015
					//$dt = $this->cartshopService->getProductShop($prdx, $this->session->userdata('pricecode'));
					//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
					$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
					//$ship = $this->cartshopService->setShippingInfo();
					$ship = $this->cartshopService->setShippingInfoBaru();
					//print_r($ship);
					$dt['prodCat'] = $this->cartshopService->getProductCategory();


					if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
						$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
					} else {
						$dt['listBank'] = $this->cartshopService->getBank();
					}
					//production
					//$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
					//development 0df5835ee198d49944c372ead860c241
					$dt['key'] = '0df5835ee198d49944c372ead860c241';

					/*
					$pay_sgo_id = $this->session->userdata("pay_sgo_id");
					if($pay_sgo_id == null || !isset($pay_sgo_id)) {
						$dt['payID'] = "EC".randomNumber(8);
						$this->session->set_userdata("pay_sgo_id", $dt['payID']);
						$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					} else {
						$dt['payID'] = $this->session->userdata("pay_sgo_id");
						$del = $this->cartshopService->deleteTempTrxSGO($dt['payID']);
						$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					}
					 * */
					//add testing by DION 05/03/2016

					/*------------------------------------------------------------
					 * PROMO 17 AGUSTUS FREE SHIPPING WITH BUYING SPECIAL PRODUCT
					 * -----------------------------------------------------------*/

					/*$dt['prd_promo'] = $this->cartshopService->getPromo17Agustus();
					if($dt['prd_promo'] != null) {
						$arr = $dt['prd_promo']['arrayData'];

						//print_r($dt['cart']);
						$ketemu = 0;
						foreach($dt['cart'] as $items) {
							if(in_array($items['id'], $arr)) {
								$ketemu = 1;
								break;
							}
						}
						if($ketemu == 1) { */
					//$totDiscount = $ship['price'];
					if (getUserID() == "IDSPAAA96407") {
						$dt['biaya'] = getTotalPayNetAndShipCostTerbaru();
					}
					/*}
                }*/
					/*------------------------------------------------------------
					 * END PROMO 17 AGUSTUS CODE
					 * -----------------------------------------------------------*/

					//GET SALDO VA
					$this->load->model("backend/be_hifi_model", "hifi");
					$resSaldo = $this->hifi->getSaldoVa(getUserID());
					if ($resSaldo == null) {
						$dt['saldo_va'] = "0";
					} else {
						$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
					}

					$dt['payID'] = "EC" . randomNumber(8);
					//$this->session->unset_userdata('pay_sgo_id');
					//$this->session->set_userdata("pay_sgo_id", $dt['payID']);
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);

					$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/" . $dt['payID'];
					//print_r($dt);
				}

				//ASLI DION
				if($data['delivery'] == "2" &&  $ship != 1 &&  $ship == null) {
                  echo "<script>
                  alert('Maaf area yang anda pilih tidak ada pricelist tes1 nya..');
                  </script>";
                 }else{
					if (getUserID() == "IDSPAAA66834") {
						//$this->load->model("webshop/Sales_model", "mSales");
						//$dt['hadiah_awal'] = $this->mSales->getListHadiah3();
						$dt['previewUrl'] = "cart/checkout/sgo/pay/previewfendi";
						$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch', $dt);
						//$this->setTemplateDevSGO('webshop/promo_nov/payment_sgo_vch2', $dt);
						//$this->setTemplateDevSGO('webshop/promo_nov/RegisterMembership', $dt);

					} else {
						$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
						$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch', $dt);
					}
				}

			//tambahan Vera
			/*if($data['delivery'] == "2") {
				if ($this->input->post("shipper") == 1) {
					if ($ship != 1 && $ship == null) {
						//echo "TES".$ship;
						echo "<script>
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
					} else {
						//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
						if (getUserID() == "IDSPAAA66834") {

//						$dt['harga'] = getTotalPayNetAndShipCostTerbaru();
//						$this->load->model("webshop/Sales_model","mSales");
//						$dt['list_hadiah'] = $this->mSales->getListHadiah($dt['harga']['total_pay']);
//						//print_r($dt['harga']['total_pay']);
//						if($dt['harga']['total_pay'] > 599999 && $dt['harga']['total_pay'] <= 1200000){
//
//							$dt['previewUrl'] = "cart/checkout/sgo/pay/previewfendi";
//							$this->setTemplateDevSGO('webshop/promo_nov/payment_sgo_vch2',$dt);
//						}
//						elseif($dt['harga']['total_pay'] > 1200000 && $dt['harga']['total_pay'] <= 2000000){
//
//							$dt['previewUrl'] = "cart/checkout/sgo/pay/previewfendi";
//							$this->setTemplateDevSGO('webshop/promo_nov/payment_sgo_vch2',$dt);
//						}
//						elseif($dt['harga']['total_pay'] > 2000000){
//
//							$dt['previewUrl'] = "cart/checkout/sgo/pay/previewfendi";
//							$dt['list_hadiah2'] = $this->mSales->getListHadiah2();
//							$this->setTemplateDevSGO('webshop/promo_nov/payment_sgo_vch3',$dt);
//						}
//						else{
//							$this->setTemplateDevSGO($this->folderView.'payment_sgo_vch',$dt);
//						}
							$this->load->model("webshop/Sales_model", "mSales");
							$dt['hadiah_awal'] = $this->mSales->getListHadiah3();
							$dt['previewUrl'] = "cart/checkout/sgo/pay/previewfendi";
							$this->setTemplateDevSGO('webshop/promo_nov/payment_sgo_vch2', $dt);
						} else {
							$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
							$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch', $dt);
						}

						//$this->setTemplateSGO('webshop/payment_sgo',$dt);
					}
				} else {
					if (getUserID() == "IDSPAAA66834") {
						$this->load->model("webshop/Sales_model", "mSales");
						$dt['hadiah_awal'] = $this->mSales->getListHadiah3();
						$dt['previewUrl'] = "cart/checkout/sgo/pay/previewfendi";
						$this->setTemplateDevSGO('webshop/promo_nov/payment_sgo_vch2', $dt);
					} else {
						$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
						$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch', $dt);
					}

				}
			}*/

			} else {
				//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
				//redirect('cart/list');
				echo "<script>
						window.location.href='list';
						alert('Mohon data dilengkapi dahulu..');
						</script>";
			}
		}
	}

	//$route['cart/checkout/sgo/pay/preview2'] = 'webshop/cartshop/paySGOPreview2';
	public function paySGOPreview2()
	{
		$data = $this->input->post(NULL, TRUE);
		//echo "<pre>";
		//print_r($data);
		//echo "</pre>";

		$data['trans_id'] = "EC" . randomNumber(8);

		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$sender_address = $this->session->userdata('sender_address');
		$dest_address = $this->session->userdata('destination_address');

		$jne_branch = $this->session->userdata('jne_branch');
		$promoDiscShipping = $this->session->userdata('promo');

		//$bns = substr($personal_info['bnsperiod'], 3, 7);
		
		//edit hilal @2019-02-06 01:45 START
		$bnsX = date('d/m/Y',strtotime($personal_info['bnsperiod']));
		$bns = substr($bnsX, 3, 7);
		//echo "personal_info['bnsperiod'] = ".$personal_info['bnsperiod'];
		//echo "bonus = ".$bns;
		//edit hilal @2019-02-06 01:45 END
		
		//SET HARGA
		$harga = getTotalPayNetBaru();
		$totPay = $harga['total_pay'];
		$selisihHargaCust = 0;
		//END

		$totalbv = $this->cart->total_bv();
		$freeship = "0";

		$biaya = getTotalPayNetAndShipCost2();
		$gross_amount = $biaya['total_pay'];
		$freeship = $biaya['freeship'];
		$discount = $biaya['tot_discount'];

		$shipper_telhp = getUserPhone();
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0] . " - " . $nmstk[1];
		$produk = $this->cart->contents();

		$cust_memb = $this->session->userdata("non_member_info");
		if (isset($cust_memb)) {
			$is_login = "2";
			$tot_paynet = getTotalPayNet();
		} else {
			$memb = $this->session->userdata("store_info");
			if (isset($memb)) {
				$is_login = "1";
				$tot_paynet = 0;
			}
		}

		if ($personal_info['delivery'] == "2") {

			if($personal_info ['shipper'] == '4'){
				$arrx = array(
					"origin_name" => $shipping_jne_info['origin_name'],
					"destination_name" => $shipping_jne_info['destination_name'],
					"service_code" => $shipping_jne_info['service_code'],
					"service_display" => $shipping_jne_info['service_display'],
					"price" => $data['total_ship_price'],
					"etd_from" => $shipping_jne_info['etd_from'],
					"etd_thru" => $shipping_jne_info['etd_thru'],
					"times" => $shipping_jne_info['times'],
					"ship_discount" => $data['total_diskon_ship'],
					"lat_dest" => $shipping_jne_info['lat_dest'], //tambahan Vera
					"long_dest" => $shipping_jne_info['long_dest']
				);
			}else{
				$arrx = array(
					"origin_name" => $shipping_jne_info['origin_name'],
					"destination_name" => $shipping_jne_info['destination_name'],
					"service_code" => $shipping_jne_info['service_code'],
					"service_display" => $shipping_jne_info['service_display'],
					"price" => $data['total_ship_price'],
					"etd_from" => $shipping_jne_info['etd_from'],
					"etd_thru" => $shipping_jne_info['etd_thru'],
					"times" => $shipping_jne_info['times'],
					"ship_discount" => $data['total_diskon_ship']
				);
			}
			//print_r($arrx);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
			$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		}

		//CHECK APAKAH ADA PROMO AGUSTUS YG AKTIF
		$promo_agust = "0";
		if (array_key_exists("promo_agust", $data)) {
			$promo_agust = $data['promo_agust'];
		}

		//CHECK APAKAH MEMBER MEMILIH PEMBELANJAAN DROPSHIPPER
		$dropship = $personal_info['id_lp'];
		if ($personal_info['id_lp'] == "DROPSHIP") {
			$selisihHargaCust = getTotalPayNet() - $totPay;
			$totPay = getTotalPayNet();
		}

		//SET PARAM UTK HADIAH
		//$this->session->unset_userdata('promo_free_prd');
		//$count = count($data['free_prdcd']);
		//echo $count;

//		if($data['free_prdcd'] != "kosong"){
//
//			$arrNov['free_prdcd'] = $data['free_prdcd'];
//			$arrNov['free_prdnm'] = $data['free_prdnm'];
//			$arrNov['free_qty'] = $data['free_qty'];
//			$arrNov['free_bv'] = $data['free_bv'];
//			$arrNov['free_westPrice'] = $data['free_westPrice'];
//			$arrNov['free_eastPrice'] = $data['free_eastPrice'];
//			$arrNov['free_weight'] = $data['free_weight'];
//
//			$this->session->set_userdata('promo_free_prd', $arrNov);
//		}else{
//
//			$arrNov = NULL;
//		}

		//SET PARAM UNTUK PRODUK
		$prdParam = array(
			"trans_id" => $data['trans_id'],
			"total_sales_weight" => $this->cart->total_weight(),
			"dropship" => $dropship,
			"listProduct" => $produk,
			"pricecode" => $pricecode,
			"dataForm" => $data,
			"sentTo" => $personal_info['delivery'],
			"promoAgust2018" => $promo_agust

		);
//		if($arrNov != null){
//
//			$prdParam = array(
//				"trans_id" => $data['trans_id'],
//				"total_sales_weight" => $this->cart->total_weight(),
//				"dropship" => $dropship,
//				"listProduct" => $produk,
//				"pricecode" => $pricecode,
//				"dataForm" => $data,
//				"sentTo" => $personal_info['delivery'],
//				"promoAgust2018" => $promo_agust,
//				"promoNovember" => $arrNov
//			);
//		}
//		else{
//
//			$prdParam = array(
//				"trans_id" => $data['trans_id'],
//				"total_sales_weight" => $this->cart->total_weight(),
//				"dropship" => $dropship,
//				"listProduct" => $produk,
//				"pricecode" => $pricecode,
//				"dataForm" => $data,
//				"sentTo" => $personal_info['delivery'],
//				"promoAgust2018" => $promo_agust
//
//			);
//		}


//		echo "<pre>";
//		print_r($prdParam);
//		echo "</pre>";

		$insPrdArr = $this->insertProduct($prdParam);
		$arrx['product'] = $insPrdArr['product'];

		//CHECK APAKAH BARANG DIKIRIM / DI AMBIL DI STOKIS
		$arrShip = null;
		if ($personal_info['delivery'] == "2") {
			// $stk = explode(" - ", $personal_info['stockist']);
			//$nama_stk = trim($stk[1]);

			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);

			//print_r($ssx);

			if($personal_info['shipper'] == '4'){
				$arrShip = array(
					"orderno" => $data['trans_id'],
					"idstk" => $personal_info['stockist'],
					"prov_code" => $personal_info['provinsi'],
					"kab_code" => $personal_info['kota'],
					"kec_code" => $personal_info['kecamatan'],
					"province_name" => $personal_info['nama_provinsi'],
					"kabupaten_name" => $personal_info['nama_kota'],
					"stockist_name" => $nama_stk,

					"receiver_name" => $personal_info['nama_penerima'],
					"addr1" => str_replace("â??", "", $personal_info['alamat']),
					"email" => $personal_info['email'],
					"tel_hp1" => $personal_info['notlp'],
					"tel_hp2" => getUserPhone(),
					"shipper_telhp" => getUserPhone(),

					"service_type_id" => $shipping_jne_info['service_code'],
					"service_type_name" => $shipping_jne_info['service_display'],
					"total_item" => $insPrdArr['total_item'],
					"total_weight" => $insPrdArr['total_weight'],
					"sender_address" => $sender_address,
					"dest_address" => $dest_address,
					"jne_branch" => $jne_branch,
					"cargo_id" => $personal_info['shipper'],
					"total_pay_net" => $totPay,
					"lat_dest" => $shipping_jne_info['lat_dest'], //tambahan Vera
					"long_dest" => $shipping_jne_info['long_dest'],
					"whcd" => $personal_info['whcd'],
					"whnm" => $personal_info['whnm']
				);
			}else{
				$arrShip = array(
					"orderno" => $data['trans_id'],
					"idstk" => $personal_info['stockist'],
					"prov_code" => $personal_info['provinsi'],
					"kab_code" => $personal_info['kota'],
					"kec_code" => $personal_info['kecamatan'],
					"province_name" => $personal_info['nama_provinsi'],
					"kabupaten_name" => $personal_info['nama_kota'],
					"stockist_name" => $nama_stk,

					"receiver_name" => $personal_info['nama_penerima'],
					"addr1" => str_replace("â??", "", $personal_info['alamat']),
					"email" => $personal_info['email'],
					"tel_hp1" => $personal_info['notlp'],
					"tel_hp2" => getUserPhone(),
					"shipper_telhp" => getUserPhone(),

					"service_type_id" => $shipping_jne_info['service_code'],
					"service_type_name" => $shipping_jne_info['service_display'],
					"total_item" => $insPrdArr['total_item'],
					"total_weight" => $insPrdArr['total_weight'],
					"sender_address" => $sender_address,
					"dest_address" => $dest_address,
					"jne_branch" => $jne_branch,
					"cargo_id" => $personal_info['shipper'],
					"total_pay_net" => $totPay
				);
			}

		}

		$arrx['shipping'] = $arrShip;

		//print_r($arrShip);

		//echo "Total Weight semua : ".$total_weight;
		//echo "<br />nTotal Belanja : ".$totPay;
		//echo "<br />Selisih harga CUST/DIST : ".$selisihHargaCust;
		if (array_key_exists("bankid", $data)) {
			$bankid = $data['bankid'];
			$charge_connectivity = $data['charge_connectivity'];
			$charge_admin = (int)$data['charge_admin'];
		} else {
			$bankid = "19";
			$charge_connectivity = "";
			$charge_admin = 0;
		}

		$arrx['summary'] = array(
			"orderno" => $data['trans_id'],
			"userlogin" => getUserID(),
			"bankaccno" => '',
			"token" => $data['trans_id'],
			"id_memb" => trim($personal_info['idmemberx']),
			"nmmember" => trim($personal_info['membername']),
			"total_pay" => $totPay,
			"total_bv" => $totalbv,
			"pricecode" => $pricecode,
			"bonusmonth" => $bns,
			"datetrans" => date("Y-m-d H:i:s"),
			"idstk" => trim($personal_info['stockist']),
			"nmstkk" => trim($nmstkSS),
			"secno" => "0",
			"flag_trx" => "W",
			"sentTo" => $personal_info['delivery'],
			"status_vt_pay" => "0",
			"status_vt_reject_dt" => "",
			"payShip" => $shipping_jne_info['price'],
			"payAdm" => $data['charge_admin'],
			"is_login" => $is_login,
			"totPayCP" => $selisihHargaCust,
			"id_lp" => $personal_info['id_lp'],
			"free_shipping" => $freeship,
			"discount_shipping" => $shipping_jne_info['ship_discount'],
			"bank_code_payment" => $bankid,
			"payConnectivity" => $charge_connectivity,
			"payAdm" => $charge_admin,
			"whcd" => $personal_info['whcd'],
			"whnm" => $personal_info['whnm']
		);

		//$arrx['summary'] = null;

		$res = $this->checkPaymentSales($data, $arrx);
		$arrx['pay'] = $res;

		$hasil = $this->insertTempTableSgo($arrx);
		//echo "<pre>";
		//print_r($hasil);
		//echo "</pre>";


		$arrx['bankDescDetail'] = $data['bankDescDetail'];
		$arrx['bankDesc'] = $data['bankDesc'];
		$arrx['bankCode'] = $data['bankCode'];
		$arrx['tot_pay'] = $arrx['summary']['total_pay'];
		$arrx['tot_nilai_voucher'] = $arrx['pay']['tot_nilai_voucher'];
		$arrx['prd_value_minus_voucher'] = $arrx['summary']['total_pay'] - $arrx['pay']['tot_nilai_voucher'];
		$arrx['pay_gateway'] = $arrx['pay']['pay_gateway'];
		$arrx['vch_stt'] = $arrx['pay']['vch_stt'];
		$arrx['temp_paymentIdx'] = $data['trans_id'];
		$arrx['prodCat'] = $this->cartshopService->getProductCategory();
		$arrx['keySgo'] = $this->session->userdata('pay_sgo_id');
		$this->load->model('webshop/shared_module', 'shared');
		//$res = $this->shared->updateBankCodePayment2($temp_id, $arr);
		$arrx['temp_result'] = $this->shared->getDataPaymentSGOByOrderID($data['trans_id']);

		if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {

			$arrx['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/dev/" . $arrx['temp_paymentIdx'];
			if ($arrx['temp_result'] != null) {
				//print_r($arrx['temp_result']);
				//production
				//$arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
				//development 0df5835ee198d49944c372ead860c241
				$arrx['keySgo'] = '0df5835ee198d49944c372ead860c241';
				$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
			} else {
				echo "Data tidak dapat diinput..";
			}

			//$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
			//$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
		} else {

			$arrx['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/" . $arrx['temp_paymentIdx'];
			if ($arrx['temp_result'] != null) {
				$arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
				$this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
				//$this->setTemplateSGO('webshop/payment_sgo_preview',$arrx);
			} else {
				echo "Data tidak dapat diinput..";
			}

		}
	}

	function insertTempTableSgo($arrx)
	{
		//echo "<pre>";
		//print_r($arrx);
		//echo "</pre>";

		$res = jsonFalseResponse("insert gagal..");
		if (!array_key_exists("product", $arrx) || $arrx['product'] == null) {
			return jsonFalseResponse("Array Product tidak ada..");
		}

		if (!array_key_exists("summary", $arrx) || $arrx['summary'] == null) {
			return jsonFalseResponse("Array Summary tidak ada..");
		}

		if (!array_key_exists("pay", $arrx) || $arrx['pay'] == null) {
			return jsonFalseResponse("Array Summary tidak ada..");
		}

		$dbqryx = $this->load->database('db_ecommerce', true);
		$dbqryx->trans_start();
		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_DET_PRD_SGO
		----------------------------------------*/
		foreach ($arrx['product'] as $dataPrd) {
			/*
			$prdSql = $this->db->set($dataPrd)->get_compiled_insert('ecomm_trans_det_prd_sgo');
			echo $prdSql;
			echo "<br />";
			*/
			$dbqryx->insert('ecomm_trans_det_prd_sgo', $dataPrd);
		}

		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_HDR_SGO
		----------------------------------------*/
		/*
		echo "<br />";
		$hdrSql = $this->db->set($arrx['summary'])->get_compiled_insert('ecomm_trans_hdr_sgo');
		echo $hdrSql;
		echo "<br />";
		*/
		$dbqryx->insert('ecomm_trans_hdr_sgo', $arrx['summary']);
		//$dbqryx->last_query();
		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_PAYDET_SGO
		----------------------------------------*/
		//echo "<br />";
		foreach ($arrx['pay']['list_payment'] as $pay) {
			/*
			$paySql = $this->db->set($pay)->get_compiled_insert('ecomm_trans_paydet_sgo');
			echo $paySql;
			echo "<br />";
			*/
			$dbqryx->insert('ecomm_trans_paydet_sgo', $pay);
		}

		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_SHIPPADDR_SGO
		----------------------------------------*/
		if ($arrx['shipping'] != null) {
			/*
			echo "<br />";
			$shipSql = $this->db->set($arrx['shipping'])->get_compiled_insert('ecomm_trans_shipaddr_sgo');
			echo $shipSql;
			echo "<br />";
			*/
			$dbqryx->insert('ecomm_trans_shipaddr_sgo', $arrx['shipping']);
		}

		//END TRANSACTION
		$dbqryx->trans_complete();

		//CHECK IF TRANSACTION PROCESS IS COMMITED
		if ($dbqryx->trans_status() === FALSE) {
			//if something went wrong, rollback everything
			$dbqryx->trans_rollback();
			return false;
		} else {
			//if everything went right, insert the data to the database
			$dbqryx->trans_commit();
			return true;
		}

		//return $res;
	}

	function checkPaymentSales($data, $arrx)
	{
		$summary = $arrx['summary'];
		$tot_pay = getTotalPayNet();
		$tot_hrs_dibayar = $summary['total_pay'] + $summary['payShip'] - $summary['discount_shipping'];
		if (count($data) > 0) {
			$jml_voucher = isset($data['shop_vch']) ? count($data['shop_vch']) : 0;
			$pay['vch_stt'] = false;
			$pay['res'] = null;
			$pay['pay_gateway'] = true;
			$tot_nominal_vch = 0;
			//Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
			//atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar

			if ($jml_voucher > 0) {
				$pay['vch_stt'] = true;
				$tempArr = array();
				$tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
				$this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $data['trans_id']);
				$xc = 0;
				for ($i = 0; $i < $jml_voucher; $i++) {
					$xc++;
					$tot_nominal_vch += $data['shop_vch_nominal'][$i];
					//$xc = $i + 1;
					$tbl .= "<tr>";
					$tbl .= "<td align=right>" . $xc . "&nbsp;</td>";
					$tbl .= "<td align=center>" . $data['shop_vch'][$i] . "</td>";
					$tbl .= "<td align=right>Rp." . $data['shop_vch_nominal'][$i] . "&nbsp;</td>";
					$tbl .= "</tr>";
					$insAr = array(
						"seqno" => $xc,
						"orderno" => $summary['orderno'],
						"paytype" => "vch",
						"docno" => $data['shop_vch'][$i],
						"payamt" => $data['shop_vch_nominal'][$i],
						"bank_code_payment" => 19,
						"charge_admin" => 0
					);
					/*echo "<pre>";
                    print_r($insAr);
                    echo "</pre>";*/
					$pay['list_payment'][$i] = $insAr;
					//$this->cartshopService->insertVoucherList($insAr);

				}
				$tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp." . $tot_nominal_vch . "&nbsp;</td></tr>";
				$tbl .= "</table>";

				//Jika total nominal voucher lebih besar atau sama dengan
				//nominal belanja yang harus dibayar
				if ($tot_nominal_vch >= $tot_hrs_dibayar) {
					//status payment gateway = false, data Langsung insert ke database tanpa pembayaran
					$pay['pay_gateway'] = false;

					//$pay['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					$pay['tot_hrs_dibayar'] = $tot_hrs_dibayar;

				} else {
					//Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO

					//$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

					//Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
					$sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
					$seqno = $xc + 1;
					$insAr = array(
						"seqno" => $seqno,
						"orderno" => $summary['orderno'],
						"paytype" => "sgo",
						"docno" => "",
						"payamt" => $sisaByr,
						"bank_code_payment" => $data['bankid'],
						"charge_admin" => $summary['payAdm']
					);
					//$this->cartshopService->insertVoucherList($insAr);
					array_push($pay['list_payment'], $insAr);
					//$pay['list_payment'][$i] = $insAr;
					$pay['tot_hrs_dibayar'] = $sisaByr;
				}

				$pay['tot_nilai_voucher'] = $tot_nominal_vch;
				$pay['tot_pay'] = $tot_pay;
				$pay['listVch'] = $tbl;
			} else {
				$pay['list_payment'] = array();
				$insAr = array(
					"seqno" => 1,
					"orderno" => $summary['orderno'],
					"paytype" => "sgo",
					"docno" => "",
					"payamt" => $tot_hrs_dibayar,
					"bank_code_payment" => $data['bankid'],
					"charge_admin" => $summary['payAdm']
				);
				//$this->cartshopService->insertVoucherList($insAr);
				array_push($pay['list_payment'], $insAr);
				$pay['tot_pay'] = $tot_pay;
				$pay['tot_nilai_voucher'] = $tot_nominal_vch;
			}
		}

		return $pay;
	}

	function insertProduct($array)
	{
		//SET DATA PRODUK FREE (JIKA ADA)
		$data = $array['dataForm'];
		$total_weight = $array['total_sales_weight'];
		$total_item = 0;
		$arrx['product'] = array();
		if (array_key_exists("promoAgust2018", $array)) {
			if ($array['promoAgust2018'] == "1" && array_key_exists("free_prdcd", $data)) {
				//TAMBAHKAN BERAT TOTAL PRODUK PEMBELANJAAN DENGAN TOTAL FREE PRODUK
				$total_weight += $data['total_free_weight'];
				$jumFreePrd = count($data['free_prdcd']);
				for ($i = 0; $i < $jumFreePrd; $i++) {
					if ($array['pricecode'] == "12W3") {
						$harga = $data['free_westPrice'][$i];
					} else {
						$harga = $data['free_eastPrice'][$i];
					}

					$arrx['product'][$i] = array(
						"orderno" => $array['trans_id'],
						"prdcd" => $data['free_prdcd'][$i],
						"prdnm" => $data['free_prdnm'][$i],
						"qty" => $data['free_qty'][$i],
						"bvr" => $data['free_bv'][$i],
						"dpr" => $harga,
						"pricecode" => $array['pricecode'],
						"sentTo" => $array['sentTo'],

					);
					$total_item += $data['free_qty'][$i];
					//array_push($arrx['product'], $res);
				}
			}
		}

//		if(array_key_exists("promoNovember", $array) ) {
//
//			if($array['promoNovember'] != null) {
//
//				//TAMBAHKAN BERAT TOTAL PRODUK PEMBELANJAAN DENGAN TOTAL FREE PRODUK
//				$total_weight += $data['total_sales_weight'];
//
//				$jumFreePrd = count($array['promoNovember']);
////                    echo $jumFreePrd;
////                    echo "<pre>";
////                    print_r($array['promoNovember']);
////                    echo "</pre>";
////				if($jumFreePrd != 7){
////
////					for($i = 0; $i < $jumFreePrd; $i++) {
////
////						$arrx['product'][$i] = array(
////
////							"orderno" => $array['trans_id'],
////							"prdcd" => $array['promoNovember'][$i]['free_prdcd'],
////							"prdnm" => $array['promoNovember'][$i]['free_prdnm'],
////							"qty" => $array['promoNovember'][$i]['free_qty'],
////							"bvr" => 0,
////							"dpr" => 0,
////							"pricecode" => $array['pricecode'],
////							"sentTo" => $array['sentTo'],
////
////						);
////						$total_item += $data['free_qty'][$i];
////						//array_push($arrx['product'], $res);
////					}
////
////				} else{
////
//					$arrx['product'][0] = array(
//
//						"orderno" => $array['trans_id'],
//						"prdcd" => $array['promoNovember']['free_prdcd'],
//						"prdnm" => $array['promoNovember']['free_prdnm'],
//						"qty" => $array['promoNovember']['free_qty'],
//						"bvr" => 0,
//						"dpr" => 0,
//						"pricecode" => $array['pricecode'],
//						"sentTo" => $array['sentTo'],
//
//					);
//					$total_item += $data['free_qty'];
//					//array_push($arrx['product'], $res);
////				}
//
//			}
//		}

		//SET DATA PRODUK SALES
		foreach ($array['listProduct'] as $row) {

			$prdcd = $row['id'];
			$qty = (int)$row['qty'];
			$bv = (int)$row['bv'];
			$prdnm = $row['name'];
			if ($array['pricecode'] == '12W3' && $array['dropship'] == "DROPSHIP") {
				$harga = (int)$row['west_Cprice'];
			} elseif ($array['pricecode'] == '12W3' && $array['dropship'] != "DROPSHIP") {
				$harga = (int)$row['west_price'];
			} elseif ($array['pricecode'] == '12E3' && $array['dropship'] == "DROPSHIP") {
				$harga = (int)$row['east_Cprice'];
			} else {
				$harga = (int)$row['east_price'];
			}

			$res = array(
				"orderno" => $array['trans_id'],
				"prdcd" => $prdcd,
				"prdnm" => $prdnm,
				"qty" => $qty,
				"bvr" => $bv,
				"dpr" => $harga,
				"pricecode" => $array['pricecode'],
				"sentTo" => $array['sentTo'],

			);
			$total_item += $qty;
			array_push($arrx['product'], $res);

		}
		//echo "<pre>";
		//print_r($array);
		//echo "</pre>";
		$arrx['total_weight'] = number_format($total_weight, 2);
		$arrx['total_item'] = $total_item;
		return $arrx;
		//END
	}

	//$route['cart/checkout/sgo/pay/preview'] = 'webshop/cartshop/paySGOPreview';
	public function paySGOPreviewOld()
	{

		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		$promo = $this->session->userdata('promo');
		$dt = $this->input->post(NULL, TRUE);
		$tot_pay = getTotalPayNet();
		/*if(in_array("FREESHIP", $promo)) {
			$tot_hrs_dibayar = $tot_pay;
		} else {
			$tot_hrs_dibayar = getTotalPayNetAndShipCost();
		}*/

		//if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAA96407") {
		$payShip = $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
		$shipDiscount = $shipping_jne_info['ship_discount'];
		$biaya = getTotalPayNetBaru();
		$tot_hrs_dibayar = $biaya['total_pay'] + $payShip;
		$dt['freeship'] = $biaya['freeship'];

		/*} else {
			$biaya = getTotalPayNetAndShipCost2();
		    $tot_hrs_dibayar = $biaya['total_pay'];
		    $dt['freeship'] = $biaya['freeship'];
			$payShip = $shipping_jne_info['price'];
			$shipDiscount = 0;
		}	*/

		if (count($dt) > 0) {
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			$jml_voucher = isset($dt['shop_vch']) ? count($dt['shop_vch']) : 0;
			$dt['vch_stt'] = false;
			$dt['res'] = null;
			$dt['pay_gateway'] = true;
			$tot_nominal_vch = 0;
			//Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
			//atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar


			if ($jml_voucher > 0) {
				$dt['vch_stt'] = true;
				$tempArr = array();
				$tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
				$this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $dt['temp_paymentIdx']);
				$xc = 0;
				for ($i = 0; $i < $jml_voucher; $i++) {
					$xc++;
					$tot_nominal_vch += $dt['shop_vch_nominal'][$i];
					//$xc = $i + 1;
					$tbl .= "<tr>";
					$tbl .= "<td align=right>" . $xc . "&nbsp;</td>";
					$tbl .= "<td align=center>" . $dt['shop_vch'][$i] . "</td>";
					$tbl .= "<td align=right>Rp." . $dt['shop_vch_nominal'][$i] . "&nbsp;</td>";
					$tbl .= "</tr>";
					$insAr = array(
						"seqno" => $xc,
						"orderno" => $dt['temp_paymentIdx'],
						"paytype" => "vch",
						"docno" => $dt['shop_vch'][$i],
						"payamt" => $dt['shop_vch_nominal'][$i],
						"bank_code_payment" => 19,
						"charge_admin" => 0
					);
					$this->cartshopService->insertVoucherList($insAr);

				}
				$tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp." . $tot_nominal_vch . "</td></tr>";
				$tbl .= "</table>";
				//Jika total nominal voucher lebih besar atau sama dengan
				//nominal belanja yang harus dibayar
				if ($tot_nominal_vch >= $tot_hrs_dibayar) {
					//status payment gateway = false, data Langsung insert ke database tanpa pembayaran
					$dt['pay_gateway'] = false;
					$arr = array(
						"bank_code_payment" => 19,
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => 0,
						"payConnectivity" => 0,
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID()
					);
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					$dt['tot_hrs_dibayar'] = $tot_hrs_dibayar;

				} else {
					//Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO
					$arr = array(
						"bank_code_payment" => $dt['bankid'],
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => $dt['charge_admin'],
						"payConnectivity" => $dt['charge_connectivity'],
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID()
					);
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

					//Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
					$sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
					$seqno = $xc + 1;
					$insAr = array(
						"seqno" => $seqno,
						"orderno" => $dt['temp_paymentIdx'],
						"paytype" => "sgo",
						"docno" => "",
						"payamt" => $sisaByr,
						"bank_code_payment" => $dt['bankid'],
						"charge_admin" => $dt['charge_admin']
					);
					$this->cartshopService->insertVoucherList($insAr);
					$dt['tot_hrs_dibayar'] = $sisaByr;
				}

				$dt['tot_nilai_voucher'] = $tot_nominal_vch;
				$dt['tot_pay'] = $tot_pay;
				$dt['listVch'] = $tbl;
			} else {
				$arr = array(
					"bank_code_payment" => $dt['bankid'],
					"payShip" => $shipping_jne_info['price'],
					"disc_shipping" => $shipDiscount,
					"payAdm" => $dt['charge_admin'],
					"payConnectivity" => $dt['charge_connectivity'],
					"sentTo" => $personal_info['delivery'],
					"userlogin" => getUserID()
				);
				$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

				$updPaySGO = array(
					"orderno" => $dt['temp_paymentIdx'],
					"bank_code_payment" => $dt['bankid'],
					"charge_admin" => $dt['charge_admin']
				);
				$this->cartshopService->updatePaydetSGO($updPaySGO);

				$dt['tot_pay'] = $tot_pay;
				$dt['tot_nilai_voucher'] = $tot_nominal_vch;

			}

			if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/dev/" . $dt['temp_paymentIdx'];
				$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch_preview_dev', $dt);

				//$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
				//$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
			} else {

				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/" . $dt['temp_paymentIdx'];
				$this->setTemplateSGO('webshop/payment_sgo_preview', $dt);


			}
		} else {
			redirect('cart/list', 'refresh');
		}

	}

	public function paySGOPreview()
	{

		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		$promo = $this->session->userdata('promo');
		$dt = $this->input->post(NULL, TRUE);
		$hrg= getTotalPayNetCust();
		$tot_pay = $hrg['total_pay'];

		/*if(in_array("FREESHIP", $promo)) {
			$tot_hrs_dibayar = $tot_pay;
		} else {
			$tot_hrs_dibayar = getTotalPayNetAndShipCost();
		}*/

		//if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAA96407") {
		$payShip = $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
		$shipDiscount = $shipping_jne_info['ship_discount'];

		$biaya = getTotalPayNetBaru();
		$tot_hrs_dibayar = $biaya['total_pay'] + $payShip;
		$dt['freeship'] = $biaya['freeship'];

		/*} else {
			$biaya = getTotalPayNetAndShipCost2();
		    $tot_hrs_dibayar = $biaya['total_pay'];
		    $dt['freeship'] = $biaya['freeship'];
			$payShip = $shipping_jne_info['price'];
			$shipDiscount = 0;
		}	*/

		if (count($dt) > 0) {
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			$jml_voucher = isset($dt['shop_vch']) ? count($dt['shop_vch']) : 0;
			$dt['vch_stt'] = false;
			$dt['res'] = null;
			$dt['pay_gateway'] = true;
			$tot_nominal_vch = 0;

			/*$selisihHargaCust = 0;*/

			//Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
			//atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar
			$bankid=$dt['bankid'];
			if($bankid == "29" || $bankid == "30" || $bankid == "31"){
				//call API Kode Unik

				$random= $dt['kode_unik'];
				$nom= ($tot_pay * 1);
				$totPay = $nom;
				$dt['kode_unik']= $random;
				$payAdm= $dt['charge_admin'] + $random;
				$nompay= $nom + $payAdm + $shipping_jne_info['price'];

				$dt['norekbank']=$dt['bank_acc'];
				$dt['namapemilik']=$dt['bank_pemilik'];
				$dt['nomor_ref']=$dt['kode_ref'];
				$kodepay=$dt['kode_pay'];
				$koderef=$dt['kode_ref'];
				//get tabel rekening bank

			}else{
				$random=0;
				$totPay = $tot_pay;
				$payAdm= $dt['charge_admin'];
				$dt['kode_unik']= 0;
				$kodepay="";
				$koderef="";
				$nompay= 0;
			}

			//tambahan Vera
			$today=date("Y-m-d H:i:s");
			$tgl_exp = date('Y-m-d H:i:s', strtotime('+4 hour', strtotime($today))); //penambahan 4jam

			if ($jml_voucher > 0) {
				$dt['vch_stt'] = true;
				$tempArr = array();
				$tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
				$this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $dt['temp_paymentIdx']);
				$xc = 0;
				for ($i = 0; $i < $jml_voucher; $i++) {
					$xc++;
					$tot_nominal_vch += $dt['shop_vch_nominal'][$i];
					//$xc = $i + 1;
					$tbl .= "<tr>";
					$tbl .= "<td align=right>" . $xc . "&nbsp;</td>";
					$tbl .= "<td align=center>" . $dt['shop_vch'][$i] . "</td>";
					$tbl .= "<td align=right>Rp." . $dt['shop_vch_nominal'][$i] . "&nbsp;</td>";
					$tbl .= "</tr>";
					$insAr = array(
						"seqno" => $xc,
						"orderno" => $dt['temp_paymentIdx'],
						"paytype" => "vch",
						"docno" => $dt['shop_vch'][$i],
						"payamt" => $dt['shop_vch_nominal'][$i],
						"bank_code_payment" => 19,
						"charge_admin" => 0
					);
					$this->cartshopService->insertVoucherList($insAr);

				}
				$tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp." . $tot_nominal_vch . "</td></tr>";
				$tbl .= "</table>";
				//Jika total nominal voucher lebih besar atau sama dengan
				//nominal belanja yang harus dibayar
				if ($tot_nominal_vch >= $tot_hrs_dibayar) {
					//status payment gateway = false, data Langsung insert ke database tanpa pembayaran
					$dt['pay_gateway'] = false;
					$arr = array(
						"bank_code_payment" => 19,
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => 0,
						"payConnectivity" => 0,
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID()
					);
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					$dt['tot_hrs_dibayar'] = $tot_hrs_dibayar;

				} else {
					//Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO
					$arr = array(
						"bank_code_payment" => $dt['bankid'],
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => $dt['charge_admin'],
						"payConnectivity" => $dt['charge_connectivity'],
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID(),
						"kode_pay" => $kodepay,
						"kode_ref_bank" => $koderef,
						"date_expired" => $tgl_exp,
						"kode_unik" => $dt['kode_unik'],
						"nom_pay" => $nompay
					);
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

					//Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
					$sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
					$seqno = $xc + 1;
					$insAr = array(
						"seqno" => $seqno,
						"orderno" => $dt['temp_paymentIdx'],
						"paytype" => "sgo",
						"docno" => "",
						"payamt" => $sisaByr,
						"bank_code_payment" => $dt['bankid'],
						"charge_admin" => $dt['charge_admin']
					);
					$this->cartshopService->insertVoucherList($insAr);
					$dt['tot_hrs_dibayar'] = $sisaByr;
				}

				$dt['tot_nilai_voucher'] = $tot_nominal_vch;
				$dt['tot_pay'] = $tot_pay;
				$dt['listVch'] = $tbl;
			} else {
				$arr = array(
					"bank_code_payment" => $dt['bankid'],
					"payShip" => $shipping_jne_info['price'],
					"disc_shipping" => $shipDiscount,
					"payAdm" => $dt['charge_admin'],
					"payConnectivity" => $dt['charge_connectivity'],
					"sentTo" => $personal_info['delivery'],
					"userlogin" => getUserID(),
					"kode_pay" => $kodepay,
					"kode_ref_bank" => $koderef,
					"date_expired" => $tgl_exp,
					"kode_unik" => $dt['kode_unik'],
					"nom_pay" => $nompay
				);
				$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

				$updPaySGO = array(
					"orderno" => $dt['temp_paymentIdx'],
					"bank_code_payment" => $dt['bankid'],
					"charge_admin" => $dt['charge_admin']
				);
				$this->cartshopService->updatePaydetSGO($updPaySGO);

				$dt['tot_pay'] = $tot_pay;
				$dt['tot_nilai_voucher'] = $tot_nominal_vch;

			}

			if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/dev/" . $dt['temp_paymentIdx'];
				$this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch_preview_dev', $dt);

				//$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
				//$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
			} else {

				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/" . $dt['temp_paymentIdx'];

				if($bankid == "29" || $bankid == "30" || $bankid == "31"){
					//$this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new2', $dt);
					$this->setTemplateSGO('webshop/payment_sgo_preview2', $dt);
				}else{
					//$this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
					$this->setTemplateSGO('webshop/payment_sgo_preview', $dt);
				}



			}
		} else {
			redirect('cart/list', 'refresh');
		}

	}
	
	//$route['cart/pay/va2'] = 'webshop/cartshop/payUsingVa2';
	public function payUsingVa2()
	{
		$this->load->model("webshop/Payment_model", 'pmodel');

		if ($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			//print_r($data);
			$amount = (double)$data['pay_amount'];
			$this->load->model("backend/be_hifi_model", "hifi");
			$saldo = $this->hifi->getSaldoVa(getUserId());
			//print_r($saldo);
			if ($saldo != null) {
				$sisaSaldo = (int)$saldo[0]->amount;
				//print_r($sisaSaldo);
				if ($amount <= $sisaSaldo) {
					
					$this->load->service("webshop/Knet_payment_service", 'paymentService');
					$double = $this->paymentService->checkDoubleTokenTransHdr($data['temp_orderno']);
				    if($double == null) {

						$dbqryx = $this->load->database("db_ecommerce", TRUE);
						$arr = array(
							"trcd" => $data['temp_orderno'],
							//TRS = Transaksi K-NET Sales
							"trtype" => "TRS",
							"effect" => "-",
							"effect_acc" => "+",
							"custtype" => "M",
							"novac" => getUserNovac(),
							//"dfno"    => $header['memberid'],
							"dfno" => getUserID(),
							"docno" => "",
							"refno" => "",
							"amount" => $amount,
							"createnm" => getUserID(),
							"tipe_dk" => "D",
							"tipe_dk_acc" => "K"
						);
						$potSaldo = $dbqryx->insert('va_cust_pay_det', $arr);	
						
						if($potSaldo > 0) {
						
							$noAwal = substr($data['temp_orderno'], 0, 2);
							
							$this->load->model("backend/be_hifi_model", "hifi");

							$updtTabel = $this->paymentService->updateTrxSGO_baru($data['temp_orderno']);
							$resultInsert = $this->paymentService->getInsertTrxOrderID($data['temp_orderno']);
							if ($resultInsert != null) {
								
								$dbqryx->set('trcd', $resultInsert[0]->orderno);
								$dbqryx->where('trcd', $data['temp_orderno']);
								$dbqryx->update('va_cust_pay_det');
								
								$dbqryx->set('trcd', $resultInsert[0]->orderno);
								$dbqryx->where('trcd', $data['temp_orderno']);
								$dbqryx->update('va_cust_pay_det_acc');
								
								//TAMBAHAN UNTUK PLUS REG MEMBER
								if($noAwal == "EM") {
									$insMemb = $this->paymentService->transaksiKnetPlusMemb($data['temp_orderno'], $resultInsert);
									if($insMemb == "true") {
										$this->paymentService->getRegMemberKnetPlus($resultInsert[0]->orderno);
									}
								}
															
								$this->paymentService->sendTrxSMS2($resultInsert);
								sendNotifSalesTransaction($resultInsert);
							}

							//GET SALDO VA

							$resSaldo = $this->hifi->getSaldoVa(getUserID());
							if ($resSaldo == null) {
								$dt['saldo_va'] = "0";
							} else {
								$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
							}

							//$count=$this->pmodel->getPertama($data['temp_orderno'],$amount,getUserPhone());
							$count = $this->pmodel->getPertama($data['temp_orderno'], $resultInsert[0]->total_pay, $resultInsert[0]->total_bv, $resultInsert[0]->id_memb, $resultInsert[0]->tel_hp_login, $resultInsert[0]->orderno);
							//END
							//$resultInsert[0]->sisa_saldo_va = $dt['saldo_va'];
							//$resJson = jsonTrueResponse($resultInsert);
							$sisa = $sisaSaldo - $amount;
							$resJson = jsonTrueResponse(null, "Sisa saldo : " . $sisa);
							
						} else {
							$resJson = jsonFalseResponse("Potong saldo gagal..");
						}
					} else {
						$resJson = jsonFalseResponse("Transaksi double..");
					}		
				} else {
					$resJson = jsonFalseResponse("Saldo VA anda tidak cukup : " . number_format($sisaSaldo, 0, ",", ".") . ", total pembayaran : " . number_format($amount, 0, ",", "."));
				}

				//$sisaSaldo = (double) $saldo[0]->amount;
			} else {
				$resJson = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
			}

			echo json_encode($resJson);
		}
	}
	
	//$route['cart/pay/va'] = 'webshop/cartshop/payUsingVa';
	public function payUsingVa()
	{
		$this->load->model("webshop/Payment_model", 'pmodel');

		if ($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			//print_r($data);
			$amount = (double)$data['pay_amount'];
			$this->load->model("backend/be_hifi_model", "hifi");
			$saldo = $this->hifi->getSaldoVa(getUserId());
			//print_r($saldo);
			if ($saldo != null) {
				$sisaSaldo = (int)$saldo[0]->amount;
				//print_r($sisaSaldo);
				if ($amount <= $sisaSaldo) {
					
					$noAwal = substr($data['temp_orderno'], 0, 2);
					
					$this->load->service("webshop/Knet_payment_service", 'paymentService');
					$this->load->model("backend/be_hifi_model", "hifi");

					$updtTabel = $this->paymentService->updateTrxSGO_baru($data['temp_orderno']);
					$resultInsert = $this->paymentService->getInsertTrxOrderID($data['temp_orderno']);
					if ($resultInsert != null) {
						$this->paymentService->sendTrxSMS2($resultInsert);
						sendNotifSalesTransaction($resultInsert);
						
						//TAMBAHAN UNTUK PLUS REG MEMBER
						if($noAwal == "EM") {
							$insMemb = $this->paymentService->transaksiKnetPlusMemb($data['temp_orderno'], $resultInsert);
							if($insMemb == "true") {
								$this->paymentService->getRegMemberKnetPlus($resultInsert[0]->orderno);
							}
						}

						$dbqryx = $this->load->database("db_ecommerce", TRUE);
						$arr = array(
							"trcd" => $resultInsert[0]->orderno,
							//TRS = Transaksi K-NET Sales
							"trtype" => "TRS",
							"effect" => "-",
							"effect_acc" => "+",
							"custtype" => "M",
							"novac" => getUserNovac(),
							//"dfno"    => $header['memberid'],
							"dfno" => getUserID(),
							"docno" => "",
							"refno" => "",
							"amount" => $amount,
							"createnm" => getUserID(),
							"tipe_dk" => "D",
							"tipe_dk_acc" => "K"
						);
						$dbqryx->insert('va_cust_pay_det', $arr);

					}

					//GET SALDO VA

					$resSaldo = $this->hifi->getSaldoVa(getUserID());
					if ($resSaldo == null) {
						$dt['saldo_va'] = "0";
					} else {
						$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
					}

					//$count=$this->pmodel->getPertama($data['temp_orderno'],$amount,getUserPhone());
					//$count = $this->pmodel->getPertama($data['temp_orderno'], $resultInsert[0]->total_pay, $resultInsert[0]->total_bv, $resultInsert[0]->id_memb, $resultInsert[0]->tel_hp_login, $resultInsert[0]->orderno);
					//END
					//$resultInsert[0]->sisa_saldo_va = $dt['saldo_va'];
					//$resJson = jsonTrueResponse($resultInsert);
					$sisa = $sisaSaldo - $amount;
					$resJson = jsonTrueResponse(null, "Sisa saldo : " . $sisa);
				} else {
					$resJson = jsonFalseResponse("Saldo VA anda tidak cukup : " . number_format($sisaSaldo, 0, ",", ".") . ", total pembayaran : " . number_format($amount, 0, ",", "."));
				}

				//$sisaSaldo = (double) $saldo[0]->amount;
			} else {
				$resJson = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
			}

			echo json_encode($resJson);
		}
	}

	//$route['payment/card'] = 'webshop/cartshop/chooseCardType';
	function chooseCardType()
	{
		$data = $this->input->post(NULL, TRUE);
		if ($data['tipekartu'] == "cc") {
			$this->load->view('webshop/payment_method/credit_card');
		} else {
			echo "<option value=\"\" selected>--Pilih jenis pembayaran--</option>";
			echo "<option value=\"mandiri_clickpay\">Mandiri ClickPay</option>";
			echo "<option value=\"cimb_click\">CIMB Clicks</option>";
			echo "<option value=\"virtual_account\">Virtual Account</option>";
			echo "<option value=\"epay_bri\">e-Pay BRI</option>";
		}
	}

	//$route['payment/dc/(:any)'] = 'webshop/cartshop/chooseDebitCardPaymentType/$1';
	function chooseDebitCardPaymentType($payType)
	{
		if ($this->_checkSessionStoreUser()) {
			$this->load->view("webshop/payment_method/" . $payType . "");
		}
	}

	public function getShippingCost()
	{
		if ($this->_checkSessionStoreUser()) {
			$this->setTempWebShop('webshop/jne');
		}
	}

	public function showPriceResult()
	{
		$data = $this->input->post(NULL, TRUE);
		$url = "http://api.jne.co.id:8889/tracing/klink/price/";

		$curl_post_data = array(
			"username" => "KLINK",
			"api_key" => "76270305bef5d402220c96d59ac61977",
			"from" => $data['from'],
			"thru" => $data['thru'],
			"weight" => $data['weight']
		);
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$arr = json_decode($curl_response);
		if (isset($arr->price)) {
			print_r($arr->price);
			echo "<br /><br />";
			$ss = array();
			$lowest = $arr->price[0]->price;
			echo "Harga pertama : $lowest";
			foreach ($arr->price as $dta) {
				if ($dta->price <= $lowest) {
					$lowest = $dta->price;
					$ss = array(
						"service_code" => $dta->service_code,
						"service_display" => $dta->service_display,
						"origin_name" => $dta->origin_name,
						"destination_name" => $dta->destination_name,
						"price" => $dta->price,
						"etd_from" => $dta->etd_from,
						"etd_thru" => $dta->etd_thru,
						"times" => $dta->times,
					);
				}
			}

			print_r($ss);
		} else {
			echo "Harga ga ada..";
			print_r($arr);
		}

	}

	public function showPriceResultTest()
	{
		$data = $this->input->post(NULL, TRUE);
		$url = "http://api.jne.co.id:8889/tracing/klink/price/";

		$curl_post_data = array(
			"username" => "KLINK",
			"api_key" => "76270305bef5d402220c96d59ac61977",
			"from" => "BOO10000",
			"thru" => "BOO10000",
			"weight" => 1.5
		);
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$arr = json_decode($curl_response);
		if (isset($arr->price)) {
			print_r($arr->price);
			echo "<br /><br />";
			$ss = array();
			//$lowest = $arr->price[0]->price;

			$test = array();
			$arrx = null;
			foreach ($arr->price as $dta) {
				if ($dta->service_code != "OKE15" && $dta->service_code != "CTCOKE15") {
					$arrx = array(
						"service_code" => $dta->service_code,
						"service_display" => $dta->service_display,
						"origin_name" => $dta->origin_name,
						"destination_name" => $dta->destination_name,
						"price" => $dta->price,
						"etd_from" => $dta->etd_from,
						"etd_thru" => $dta->etd_thru,
						"times" => $dta->times,
					);
					array_push($test, $arrx);
				}
			}
			$lowest = $test[0]['price'];
			echo "Harga pertama : $lowest<br />";
			foreach ($test as $dta) {
				if ($dta['price'] <= $lowest) {
					$lowest = $dta['price'];
					$arrx = array(
						"service_code" => $dta['service_code'],
						"service_display" => $dta['service_display'],
						"origin_name" => $dta['origin_name'],
						"destination_name" => $dta['destination_name'],
						"price" => $dta['price'],
						"etd_from" => $dta['etd_from'],
						"etd_thru" => $dta['etd_thru'],
						"times" => $dta['times']
					);
				}
			}

			print_r($arrx);
		} else {
			echo "Harga ga ada..";
			print_r($arr);
		}

	}

	//$route['jne/tes_konot'] = 'webshop/cartshop/generateJNENo2';
	function generateJNENo2()
	{
		/* //production
		$this->jne_url_conot = "http://api.jne.co.id:8889/tracing/klink/generateCnote";
		$this->jne_username = "KLINK";
		$this->jne_api_key = "76270305bef5d402220c96d59ac61977";
		$this->jne_OLSHOP_CUST = "80075400";
		 */
		$data = $this->input->post(NULL, TRUE);
		$url = "http://api.jne.co.id:8889/tracing/klink/generateCnote";

		$curl_post_data = array(
			"username" => "KLINK",
			"api_key" => "76270305bef5d402220c96d59ac61977",
			"OLSHOP_BRANCH" => $data['branch'],
			"OLSHOP_CUST" => "80075400",
			"OLSHOP_ORIG" => $data['from'],
			"OLSHOP_ORDERID" => $data['order_id'],
			"OLSHOP_SHIPPER_NAME" => $data['shipper_name'],
			"OLSHOP_SHIPPER_ADDR1" => $data['shipper_name'],
			"OLSHOP_SHIPPER_ADDR2" => $data['shipper_name'],
			"OLSHOP_SHIPPER_ADDR3" => $data['shipper_name'],
			"OLSHOP_SHIPPER_CITY" => $data['shipper_name'],
			"OLSHOP_SHIPPER_REGION" => $data['shipper_name'],
			"OLSHOP_SHIPPER_ZIP" => "12260",
			"OLSHOP_SHIPPER_PHONE" => "032323",
			"OLSHOP_RECEIVER_NAME" => "TEST RECEIVER",
			"OLSHOP_RECEIVER_ADDR1" => "TEST RECEIVER",
			"OLSHOP_RECEIVER_ADDR2" => "TEST RECEIVER",
			"OLSHOP_RECEIVER_ADDR3" => "TEST RECEIVER",
			"OLSHOP_RECEIVER_CITY" => "TEST RECEIVER",
			"OLSHOP_RECEIVER_REGION" => "TEST RECEIVER",
			"OLSHOP_RECEIVER_ZIP" => "12320",
			"OLSHOP_RECEIVER_PHONE" => "032323",
			"OLSHOP_DEST" => $data['thru'],
			"OLSHOP_SERVICE" => $data['service_code'],
			"OLSHOP_QTY" => 3,
			"OLSHOP_WEIGHT" => $data['weight'],

			"OLSHOP_GOODSTYPE" => 2,
			"OLSHOP_GOODSDESC" => "SUPLEMENT",
			"OLSHOP_INST" => "FRAGILE",
			"OLSHOP_GOODSVALUE" => 10000,
			"OLSHOP_INSURANCE" => "Y"
		);
		//$curl = curl_init($url);
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		print_r($curl_post_data);
		echo "<br />";

		echo "<br />";
		echo "<br />";
		$arr = json_decode($curl_response);
		print_r($arr);
		if ($arr->detail == null) {
			echo "data null";
		} else if ($arr->detail[0]->status == "Sukses") {
			echo "Sukses, cnote : ";
			echo $arr->detail[0]->cnote_no;
		} else if ($arr->detail[0]->status == "Error") {
			echo $arr->detail[0]->reason;
		}
	}
}

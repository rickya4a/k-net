<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Static_page extends MY_Controller {
    	
    public function __construct() {
	    parent::__construct();
		//$this->load->model("webshop/Product_model",'productM');
		$this->load->service("webshop/Product_service",'productService');
	}
    
	public function about(){
		$dt['prodCat'] = $this->productService->getProductCategory();
		$this->setTempWebShop('webshop/about',$dt);
	}

	public function why(){
		$dt['prodCat'] = $this->productService->getProductCategory();
		$this->setTempWebShop('webshop/why',$dt);
	}     
 

    function delivery(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/delivery',$dt);
    }


    function how_to_order(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/how_to_order',$dt);
    }


    function shipping_rate(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/shipping_rate',$dt);
    }


    function replacement(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/replacement',$dt);
    }


    function privacy_policy(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/privacy_policy',$dt);
    }
    
    function howTo_pay_SGO(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/sgo_panduan',$dt);
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 *
 */
require_once("Api_payment.php");
class Api_payment_reconcile extends Api_payment {



	//put your code here
	public function __construct() {
		parent::__construct();
		$this->load->service("webshop/Knet_payment_service",'paymentService');
		$this->sgoPasswordAccess = 'k-net181183';
	}
	
	//$route['sgo/tes'] = 'webshop/api_payment_reconcile/sgoTes';
	public function sgoTes() {
		echo "tes";
	}

	
	//$route['api/sgo/renotif'] = 'webshop/api_payment_reconcile/renotifTransaction';
	public function renotifTransaction() {
		$xx = $this->input->post(null,true);
		//$arr['no'] = substr($xx['order_id'], 0, 2);
		$arr = $this->trxType($xx);
		//Bila transaksi adalah pembelanjaan online
		if($arr['no'] == "EC" || $arr['no'] == "EB" || $arr['no'] == "SC" || $arr['no'] == "EA") {
			
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {
				$this->load->model("webshop/Payment_model",'pmodel');
				$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$this->paymentService->sendTrxSMS2($resultInsert);
				sendNotifSalesTransaction($resultInsert);

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null) {

					emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
					foreach($data_catalog as $row){
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $resultInsert[0]->tel_hp_login, $link);
						smsTemplate( $resultInsert[0]->tel_hp_as, $link);
					}

				} 
				/*
				if($arr['no'] == "EA") {
					$this->blastNotif($resultInsert[0]->orderno);
				}
				*/
				
				//berlaku hanya untuk 1 september 2018 - 30 november 2018
				/*
				if($arr['no']=="EC"){

					$count=$this->pmodel->getPertama($xx['order_id'],$resultInsert[0]->total_pay,$resultInsert[0]->total_bv,$resultInsert[0]->id_memb,$resultInsert[0]->tel_hp_login,$resultInsert[0]->orderno);
				}
				*/
				echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
			} else {
				echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));
			}	
			
		}
		
		else if($arr['no'] == "EM") {
				//transaksiKnetPlusMemb
				//$this->transaksiKnetPlusMemb($xx, $arr);
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {	
				$this->load->model("webshop/Payment_model",'pmodel');
				$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				if($resultInsert != null) {
					$insMemb = $this->paymentService->transaksiKnetPlusMemb($xx['order_id'], $resultInsert);

					$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
					if($data_catalog != null){
			//					$dta['catalog']=$data_catalog;
			//					$dta['token']=$xx['order_id'];
						emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
						//echo "<pre>";
						//print_r($data_catalog);
						//echo "</pre><br />";
						foreach($data_catalog as $row){
							$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
							smsTemplate( $resultInsert[0]->tel_hp_login, $link);
							smsTemplate( $resultInsert[0]->tel_hp_as, $link);
						}

					}
					
					if($resultInsert != null) {
						//$this->paymentService->sendTrxSMS2($resultInsert);
						$this->paymentService->sendSMSPromoSalesAndReg($resultInsert);
					} 
					
					if($insMemb == "true") {
						$this->paymentService->getRegMemberKnetPlus($resultInsert[0]->orderno);
					}
					echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
				} 
			} else {
				echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));
			}
		}
		//Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
		else if($arr['no'] == "EN") {
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {
				$updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$this->paymentService->sendTrxSMSNonMember($resultInsert);
				echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
			} else {
			    echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));	
			}	
		}
		//bila transaksi adalah Registration Member / Landing Page Registration
		else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {
			    $arr['dta'] = $this->paymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
				$updtTabel =$this->paymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
				if($updtTabel['response'] == "true") {
					$resultInsMemb = $this->paymentService->getNewMemberData($xx['order_id']);
					//$this->paymentService->createNewShoppingVoucher($resultInsMemb);
					$this->paymentService->sendTrxSmsRegMember($resultInsMemb);
					//$this->paymentService->sendWelcomeEmail($resultInsMemb);
					sendWelcomeEmail($resultInsMemb);
					echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
				} else {
					echo json_encode($updtTabel);
				}
			} else {
			    echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));	
			}	
		}
		elseif($arr['no'] == "ED") {
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {
				$this->load->model("webshop/Payment_model",'pmodel');
				$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
				$updtTabel = $this->paymentService->updateTrxSGO_baruDIGITAL($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
				$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($xx['order_id']);

				$this->paymentService->sendTrxSMS2($resultInsert);

				$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
				if($data_catalog != null){

					emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
					foreach($data_catalog as $row) {
						$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
						smsTemplate( $identitas[0]->telp_customer, $link);
					}

				}
				echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
			} else {
			  echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));	
			}		
		}

		//Transaksi K-net Non Member "Mampir Kak"
		else if($arr['no'] == "EP") {
			//print_r($arr['no']);
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {
				//$this->transaksiMampirKak($xx, $arr);
				$hasil = $this->paymentService->processTrxMampirKak($xx['order_id']);
				$resultInsert = $this->paymentService->getInsertTrxMampirKak($xx['order_id']);
				//print_r($resultInsert);
				if($resultInsert != null) {
					if($resultInsert[0]->new_member_reg != null && $resultInsert[0]->new_member_reg != "") {
					  $this->paymentService->sendSmsRegMembMampirKak($resultInsert);
					}	
				    $this->paymentService->sendTrxSmsMampirKak($resultInsert);
				    echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
				} else {
					echo json_encode(jsonFalseResponse("Trx $xx[order_id] gagal di reconcile.."));
				}
			} else {
				echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));	
			}			
		}
		
		//bila transaksi adalah Online Stockist Payment
		else if($arr['no'] == "ST") {

		}

		//bila transaksi adalah Ticket Online  Payment
		else if($arr['no'] == "TA") {
			$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
			if($double == null) {
				$this->load->model("dtc/mdtc_mbr", "mdtc");
				$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
				$createIP = $this->mdtc->generateIP($xx['order_id']);
				echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
			} else {
				echo json_encode(jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database.."));	
			}		
		}
	
		else if($arr['no'] == "PS") {
			//$this->processPpobTrxSGO($xx, $arr);
		}

		else if($arr['no'] == "PX") {
			//$this->processXlDataSGO($xx, $arr);	
			$this->load->model("backend/be_hifi_model", "hifi");
			$resultInsert = $this->hifi->getPpobTrxById($xx['order_id'], "temp");
			if($resultInsert != null) {
				$arrXL = array(
					"brand_id" => $this->adv_id,
					"prod_id" => $resultInsert[0]->trx_type,
					"trx_id" => $resultInsert[0]->trx_id,
					"msisdn" => $resultInsert[0]->cust_no,
				);


				//$redeem = $this->redeemStockXL($arrXL);
				$redeem = $this->redeemStockXLnoEncrypt($arrXL);
				$hasil2 = json_decode($redeem);
				if($hasil2 == "0" || $hasil2 == null) {
					$trx_id = $resultInsert[0]->trx_id;
					$this->hifi->updateStatusResponseXL_tempsgo($trx_id, $redeem);
				   //die('1,No response from XL or No Valid Hours,,,,,');
				   echo json_encode(jsonFalseResponse("No response from XL or No Valid Hours"));
				} /*else if($hasil2 == null) {
				   die('1,No response from XL,,,,,');
				}*/ 
				else if($hasil2->code == "00") {
					$insReal = $this->hifi->insertPpobFromTemp2($xx['order_id'], json_encode($hasil2));
					if($insReal['response'] == "true") {
						
						$reff_pay_id = "REC";
						if($hasil2->reff_id != null && $hasil2->reff_id != "") {
							$reff_pay_id = $hasil2->reff_id;
						}
						
						$arr_data = array(
							'id_xl'=> $resultInsert[0]->trx_type,
							'msisdn'=> $resultInsert[0]->cust_no,
							'createnm'=>$resultInsert[0]->memberid,
							'createdt'=>$resultInsert[0]->createdt,
							'price'=>$resultInsert[0]->nominal,
							'trcd'=>$resultInsert[0]->trx_id,
							//'reff_id'=>$hasil2->reff_id,
							'reff_id'=>$reff_pay_id,
							//'reff_id'=>"redsds",
							'kategori'=> 0,
							'status'=> 0
						);
						//print_r($arr_data);
						$this->hifi->SaveTRXPaket($arr_data);

						echo json_encode(jsonTrueResponse(null, "Transaksi $xx[order_id] berhasil di reconcile.."));
					} else {
						//die('1,Fail insert data header and detail,,,,,');
						echo json_encode(jsonFalseResponse("Trx $xx[order_id] gagal di reconcile.."));
						
					}
				} else {
					$arr_data = array(
						'id_xl'=> $resultInsert[0]->trx_type,
						'msisdn'=> $resultInsert[0]->cust_no,
						'createnm'=>$resultInsert[0]->memberid,
						'createdt'=>$resultInsert[0]->createdt,
						'price'=>$resultInsert[0]->nominal,
						'trcd'=>$resultInsert[0]->trx_id,
						//'reff_id'=>$hasil2->reff_id,
						'reff_id'=>NULL,
						'reff_id'=>"FAIL_INS",
						'kategori'=> 0,
						'status'=> 1
					);
					//print_r($arr_data);
					$this->hifi->SaveTRXPaket($arr_data);
					echo json_encode(jsonFalseResponse("Trx $xx[order_id] gagal dalam proses reconcile.."));
				}
			} else {
				echo json_encode(jsonFalseResponse("Trx $xx[order_id] tidak ditemukan di table TEMP"));
			}
			
		}



	}
	
	//$route['resend/catalog/link'] = 'webshop/api_payment_reconcile/sendEmailCatalog';
	function sendEmailCatalog() {
		
		$xx['order_id'] = $this->input->post('order_id');
		$this->load->model("webshop/Payment_model",'pmodel');
		//$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
		$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
		//$this->paymentService->sendTrxSMS2($resultInsert);
		//sendNotifSalesTransaction($resultInsert);

		$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
		if($data_catalog != null) {

			emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
			emailSend('Katalog Digital K-Link', "dionrespati@gmail.com", KatalogEmail($xx['order_id'], $data_catalog));
			foreach($data_catalog as $row){
				$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
				smsTemplate( $resultInsert[0]->tel_hp_login, $link);
				smsTemplate( $resultInsert[0]->tel_hp_as, $link);
				smsTemplate( "087780441874", $link);
			}

		} 
	}
	
}
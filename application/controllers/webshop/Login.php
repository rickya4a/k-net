<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Login extends MY_Controller {
    //private $url = 'www.k-linkmember.co.id/commerce_rpc/index.php/shopping/';	
    
    public function __construct() {
	    parent::__construct();
		//$this->load->model("webshop/Login_model",'loginModel');
		//$this->xmlrpc->server(prep_url($this->url), 80);
		$this->load->service("webshop/Login_service",'loginService');
        $this->load->service("webshop/Product_service",'productService');
	}
    
    function member_login($err = ""){
		
		$tgl = date("Y-m-d-H");
        
		/*if($tgl <= '2018-12-16-13' ) {
			echo "<h2>Maaf sedang ada maintenance pada sistem kami, silahkan akses kembali besok siang</h2>";
		} else {*/
		
        $dt['error'] = $err;
        $this->session->sess_destroy();
        //echo "testt ".$err;
        $dt['formAction'] = "".site_url('loginmember/action')."";
        $dt['prodCat'] = $this->productService->getProductCategory();
		$dt['captcha_img'] = $this->loginService->createChaptcha();
		$this->load->view('login/login_member',$dt);
		$this->load->view('shopinc/footer');
		
		//}
		/*
		$testGD = get_extension_funcs("gd"); // Grab function list 
		if (!$testGD){ echo "GD not even installed."; exit; }
		echo"<pre>".print_r($testGD,true)."</pre>";
		 
		 */
    }
    
    function handleLogin(){
        if($this->form_validation->run('login') == FALSE) {
			   $this->member_login("Semua field harus diisi..!!");
        } else {
            $data = $this->input->post(NULL, TRUE);		
            $login = $this->loginService->checkAuthLogin($data); 
//			print_r($login);
			if($login != null) {
				$telhp = getUserPhone();
				$email = getUserEmail();
                $this->session->set_userdata('store_info', $login);
				
				if(getUserId() == "ID9999A00251") {
					$this->session->set_userdata('pricecode', "12W4");
				} else {
					$this->session->set_userdata('pricecode', "12W4");
				}	
					
				
				//if(getUserID() == "IDSPAAA96407") {
					//print_r($promo);
					$promo = $this->loginService->checkListPromo();
					
					$this->session->set_userdata('promo', $promo);	
				//}
				
				
				//print_r(strlen($telhp));
				/*if(strlen($telhp) < 7 || $email == "") {
				redirect('updEmailFirst');
				} else {
					redirect('shop/product');	
				}
				if(getUserID() == "IDSPAAA66834") {
				    if(strlen($login[0]->tel_hp) < 7 || $login[0]->email == "") {
					  echo "<script language='javascript'>alert('Silahkan update email dan nomor hp anda..')</script>";	
					  redirect('updEmailFirst');
					} else {
						redirect('shop/product');	
					}
					echo "<script language='javascript'>alert('Silahkan update email dan nomor hp anda..')</script>";
				} else {
                  redirect('shop/product');
				}*/
				if(strlen($login[0]->tel_hp) < 7 || $login[0]->email == "") {
					//echo "<script>alert('Silahkan update email dan nomor hp anda..')</script>";
					/*echo '<script type="text/javascript">';
					echo 'alert("Silahkan update email dan nomor hp anda..")';
					echo '</script>';	
					  redirect('updEmailFirst');*/
					  
					echo "<script>
					alert('Silahkan update email dan nomor hp anda..');
					window.location.href='" . base_url() . "updEmailFirst';
					</script>";  
					} else {
						if(getUserId() == "ID9999A00251") {
							redirect('shop/product');	
						} else {
							redirect('shop/product');
						}
						
					}
            }else{
                $this->member_login("Proses Otentifikasi Login gagal");
            }
        }
    }

        
    function logout(){
        $this->loginService->insertLogoutData(); 	
        $this->session->sess_destroy();
		redirect('loginmember');
    }
    
    
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cartshop_customer extends MY_Controller {
    		
    public function __construct() {
	    parent::__construct();
		//header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		$this->load->service("webshop/Cartshop_service",'cartshopService');
		$this->load->service("webshop/Product_service",'productService');
		$this->folderView = "webshop/sales_non_member/";
		//header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		//$this->load->library('nativesession');
	}
	
	//$route['native/set'] = 'cartshop_customer/tesNative';
	public function tesNative() {
		$this->nativesession->set('isi', "okeeee");
	}
	
	//$route['native/set'] = 'cartshop_customer/tesNativeGet';
	public function tesNativeGet() {
		$isi = $this->nativesession->get('isi');
		print_r($isi);
		
	}
	
	//$route['native/out'] = 'webshop/cartshop_customer/nativeLogout';
	public function nativeLogout() {
		session_destroy();
		//print_r($isix);
		
	}
	
	//$route['shop/customer'] = 'cartshop_customer/getListProduct';
	public function getListProduct() {
		$dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        //$dt['prod'] = $this->productService->getProductShop();
		$dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        $dt['prodCat'] = $this->cartshopService->getProductCategory();	
        $dt['banner'] = $this->productService->getHeaderBanner();
        //session_start();
		$this->load->service("webshop/Login_service",'loginService');
	    $promo = $this->loginService->checkListPromo();
	    //print_r($promo);
	    $this->session->set_userdata('promo', $promo);		
		$this->setTempWebShop($this->folderView.'shop_home_guest1', $dt);	
		//echo "tes";
	}
	
	//$route['shop/customer/$1/$2/$3/$4'] = 'cartshop_customer/getListProductForLpMember';
	public function getListProductForLpMember($idmember, $email, $nama, $id_lp) {
		//$this->session->sess_destroy();
		//$this->cart->destroy();
		/*$dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProductShop();
		$dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
		$memb = $this->productService->getMemberInfo($idmember);
		$arr = array("idmemb" => $idmember,
				     "email" => $email,
				     "nama" => $nama,
				     "id_lp" => $id_lp,
				     "nmmemb" => $memb['arrayData'][0]->fullnm
		);
		//print_r($arr);
		$this->session->set_userdata("sales_lp", $arr);
		//$this->nativesession->set("sales_lp", $arr);
        $dt['prodCat'] = $this->cartshopService->getProductCategory();
		
		$this->setTempWebShop($this->folderView.'shop_home_guest2', $dt);
		*/
		$dt['prdnm'] = $this->input->post("name");
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProductShop();
//		$dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
		$memb = $this->productService->getMemberInfo3($idmember);
		//print_r($memb);
        $dt['prodCat'] = $this->cartshopService->getProductCategory();
		$dt['idmember'] = $idmember;
		$dt['email'] = $email;
		$dt['nama'] = $nama;
		$dt['id_lp'] = $id_lp;
		//print_r($memb);
		$dt['nmmemb'] = $memb['arrayData'][0]->fullnm;
        $dt['banner'] = $this->productService->getHeaderBanner();
		//session_start();
		$this->load->service("webshop/Login_service",'loginService');
	    $promo = $this->loginService->checkListPromo();
	    //print_r($promo);
	    $this->session->set_userdata('promo', $promo);		
		$this->setTempWebShopLPSales($this->folderView.'shop_home_guest3', $dt);
		
		//echo "tes";
	}

    
    //$route['shop/customer/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'webshop/cartshop_customer/getListProductForLpMemberPrd/$1/$2/$3/$4/$5';
    public function getListProductForLpMemberPrd($idmember, $email, $nama, $id_lp, $prdcd) {
    	//session_start();
		$this->load->service("webshop/Login_service",'loginService');
	    $promo = $this->loginService->checkListPromo();
	    //print_r($promo);
	    $this->session->set_userdata('promo', $promo);
    	$this->session->set_userdata('pricecode', "12W3");
		$membDTA = $this->productService->getMemberInfo($idmember);
		$arr = array("idmemb" => $idmember,
				     "email" => $email,
				     "nama" => $nama,
				     "id_lp" => $id_lp,
				     "nmmemb" => $membDTA['arrayData'][0]->fullnm
	    );
		$this->session->set_userdata('sales_lp', $arr);
		
    	if(!$this->cartshopService->IfDoubleInputCart($prdcd)) {
            //$arr = $this->cartshopService->addProductToCart($prdcd);
			$arr = $this->cartshopService->addProductToCartVera($prdcd); //vera
			 
			 //echo "masuk sini";
         } else {
             $arr = array("response" => "false", "message" => "Data product ".$this->input->post('prdnm')." sudah ada di cart");
         }
         redirect("shop/cart/list");
    }
	
	//Digunakan untuk landing page untuk secara spesifik berbelanja produk tertentu
	//$route['shop/cart/prdcd/(:any)'] = 'webshop/cartshop_customer/autoAddPrdcdToCart/$1';
	public function autoAddPrdcdToCart($prdcd) {
		$member_info = $this->session->userdata('member_info');
		 if(!empty($member_info)) {
			$this->session->unset_userdata('personal_info');
			$this->session->unset_userdata('starterkit_prd');
			$this->session->unset_userdata('memb_choose_pay');
			$this->session->unset_userdata('member_info');
			$this->session->unset_userdata('sender_address');
			$this->session->unset_userdata('destination_address');
			$this->session->unset_userdata('jne_branch');
			$this->session->unset_userdata('shipping_jne_info');
			
		}
		 
		$arr = array("id_lp" => "TES01");
		//print_r($arr);
		$this->session->set_userdata("sales_lp", $arr);
        
		 
		 
		 if(!$this->cartshopService->IfDoubleInputCart($prdcd)) {
            $arr = $this->cartshopService->addProductToCart($prdcd); 
         } else {
             $arr = array("response" => "false", "message" => "Data product ".$this->input->post('prdnm')." sudah ada di cart");
         }
		 
		 $dt['formAction'] = "".site_url('shipping/postShipping')."";
            $dt['prodCat'] = $this->cartshopService->getProductCategory();
			$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();    
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			$this->session->set_userdata('pricecode', "12W3");
			//print_r($dt['bns']);
			$dt['cart'] = $this->cart->contents(); 
            		$this->setTempWebShop($this->folderView.'shipping2',$dt);
				 
			
			
	}
	
	//$route['shop/cart/add'] = 'webshop/cartshop_customer/addToCart';
    function addToCart() {
         
    	 $member_info = $this->session->userdata('member_info');
		 if(!empty($member_info)) {
			$this->session->unset_userdata('personal_info');
			$this->session->unset_userdata('starterkit_prd');
			$this->session->unset_userdata('memb_choose_pay');
			$this->session->unset_userdata('member_info');
			$this->session->unset_userdata('sender_address');
			$this->session->unset_userdata('destination_address');
			$this->session->unset_userdata('jne_branch');
			$this->session->unset_userdata('shipping_jne_info');
			
		}
		 if(!$this->cartshopService->IfDoubleInputCart($this->input->post('prdcd'))) {
            $arr = $this->cartshopService->addToCart();
         } else {
             $arr = array("response" => "false", "message" => "Data product ".$this->input->post('prdnm')." sudah ada di cart");
         }   
         echo json_encode($arr);
        
    }
	
	//$route['shop/cart/list'] = 'webshop/cartshop_customer/listCart';
	public function listCart() {
		    $dt['formAction'] = "".site_url('shipping/postShipping')."";
            $dt['prodCat'] = $this->cartshopService->getProductCategory();
			$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();    
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			$dt['listCargo'] = $this->cartshopService->getListCargo();
			//print_r($dt['bns']);
			$dt['cart'] = $this->cart->contents();
			//print_r($dt['cart']);
            $personal_info = $this->session->userdata('personal_info');
			if($personal_info != null) {
				
				$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);	
				//print_r($personal_info);
				$this->setTempWebShop($this->folderView.'shippingWithCartData2',$dt);
			} else {
				$res = $this->cartshopService->getAddressReferenceByID(getUserID());
				//print_r($res);
				if($res != null) {
					$personal_infox = array(
					    "provinsi" => $res[0]->provinsi,
	                    "kota" => $res[0]->kota,
	                    "kecamatan" => $res[0]->kecamatan,
					);
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);	
					$this->cartshopService->setAutomaticPersonalInfo($res);
					//print_r($dt['shipping'])

					$this->setTempWebShop($this->folderView.'shippingAutoAddrReff',$dt);
					//$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
				} else {
					//echo "tess masuk..";
					$this->setTempWebShop($this->folderView.'shipping2',$dt);
				} 
			}
	}

	//$route['shop/cart/remove'] = 'webshop/shipping_customer/removeCart';
    function removeCart(){
         $arr = $this->cartshopService->removeCart();
		 echo json_encode($arr);
    }
    
	//$route['shop/cart/update'] = 'webshop/shipping_customer/updateCart';
    function updateCart() {
        
        	$data = $this->input->post(NULL, TRUE);	
        	$upd = $this->cartshopService->updateCart($data);
            $dt['cart'] = $this->cart->contents();
            $this->session->set_userdata('pricecode', $data['pricecode']);   
            $this->load->view($this->folderView.'order_details',$dt);
		 
    }
	
	//$route['shop/cart/upd'] = 'webshop/shipping_customer/updateCartBeforeProceed';
    function updateCartBeforeProceed() {
    	$arr = jsonFalseResponse(NULL, "Update Cart gagal..!");	
        	$data = $this->input->post(NULL, TRUE);	
        	$upd = $this->cartshopService->updateCart($data);
			$this->session->set_userdata('pricecode', $data['pricecode']);
			if($upd) {
				$arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
			} 
			//echo json_encode($arr);
			return $arr;	
		
    }
	
	//$route['shop/cart/checkout'] = 'webshop/shipping_customer/CheckoutCart';
	function CheckoutCart() {
		
        $data = $this->input->post(NULL, TRUE);

		if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4"){
			$totPayNet = $data['totalAmountWest'];
		}else{
			$totPayNet = $data['totalAmountEast'];
		}
		echo "Masuk Sini: ".$subtotal;

		//$totPay = "100000"; //sementara default vera
		//echo $totPayNet;


		if($this->cartshopService->checkShippingDataJNE($data)) {
		    $dt['cart'] = $this->cart->contents();

			if($data != null) {
				$eerr = $this->updateCartBeforeProceed();
				$prdx = set_list_array_to_stringCart($dt['cart'], "id");
				$reg = $this->cartshopService->registerCustomerNonMember($data);
				$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				$ship = $this->cartshopService->setShippingInfo();	
                $dt['prodCat'] = $this->cartshopService->getProductCategory();
                
				//$dt['listBank'] = $this->cartshopService->getBank();
				$dt['listBank'] = $this->cartshopService->getBankNonVABaru();
				$dt['listCC'] = $this->cartshopService->getCCBank($totPayNet);//tambahan Vera
				//production
				//$dt['key'] = '23536c365506cdaa587fb9c1833f23f0';
				$dt['key']='51edf5e8117da341a8be702d9bc18de5';
                //development
                //$dt['key'] = '7abba47aa19935ee8a84acb2737e1c27';
					
				$pay_sgo_id = $this->session->userdata("pay_sgo_id");
				if($pay_sgo_id == null || !isset($pay_sgo_id)) {
					//$dt['payID'] = "EC".randomNumber(8);
					$dt['payID'] = "EN".randomNumber(8);
					$this->session->set_userdata("pay_sgo_id", $dt['payID']);
					$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				} else {

					$dt['payID'] = $this->session->userdata("pay_sgo_id");
					$dt['payID'] = "EN".randomNumber(8);
					$del = $this->cartshopService->deleteTempTrxSGO($dt['payID']);
					$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				}
				$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['payID'];
				//print_r($dt);
			}
			
			if($data['delivery'] == "2" && $ship != 1 && $ship == null) {
				echo "<script>
					window.location.href='list';
					alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
					</script>";
			} else {
				//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				//$this->setTemplateDevSGO($this->folderView.'payment_sgo',$dt);
				$this->setTemplateSGO('webshop/payment_sgo',$dt);
			}
			
		} else {
			//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
			//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
			//redirect('cart/list');
			echo "<script>
					window.location.href='list';
					alert('Mohon data dilengkapi dahulu..');
					</script>";
		}	
   }   

    //$route['shop/pay/preview'] = 'webshop/cartshop_customer/paySGOPreview';
	public function paySGOPreview() {
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		$dt = $this->input->post(NULL, TRUE);
		/*
		 echo "disini ya......".count($dt);
		var_dump($dt);
		*/
		
		if(count($dt) > 0){
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			
			$arr = array(
				"bank_code_payment" => $dt['bankid'],
				"payShip" => $shipping_jne_info['price'],
				"payAdm" => $dt['charge_admin'],
				"payConnectivity" => $dt['charge_connectivity'],
				"sentTo" => $personal_info['delivery'],
				"userlogin" => getUserID()
			);
			$dt['res'] = $this->cartshopService->updateBankCodePayment($dt['temp_paymentIdx'], $arr);
			
				//$dt['backURL'] = "http://www.k-net.co.id/shop/pay/sgo/finish/dev/".$dt['temp_paymentIdx'];
				//$this->setTemplateDevSGO($this->folderView.'payment_sgo_preview',$dt);
				$dt['backURL'] = "http://www.k-net.co.id/shop/pay/sgo/finish/".$dt['temp_paymentIdx'];
				$this->setTemplateSGO($this->folderView.'payment_sgo_preview',$dt);
			
		}else{
			//balik ke hal cart/checkout
			//$this->setTemplateSGO('http://www.k-net.co.id/cart/checkout');
		}
        
	}

	
	
		
}

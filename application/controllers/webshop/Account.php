<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Account extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->load->service("webshop/Cartshop_service",'cartshopService');
        $this->load->service("webshop/Product_service",'productService');
        $this->load->model('webshop/Member_model','memberM');
	}
	
	//$route['bnsstt_view'] = 'webshop/account/updateViewBnsStt';
	public function updateViewBnsStt() {
		if($this->_checkSessionStoreUser()){
		  $dt['member'] = $this->memberM->getStatusBnsSttView(getUserID());
	      // $dt['prodCat'] = $this->productService->getProductCategory();
		   $this->load->view('webshop/updateViewBnsStt', $dt);
	   }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
	}
	
	//$route['bnsstt_view/update'] = 'webshop/account/updateStatusBnsSttView';/
	public function updateStatusBnsSttView() {
		$arr = jsonFalseResponse("Ubah Status View Bonus Statement Gagal..");
		$data = $this->input->post(NULL, TRUE);
		$upd = $this->memberM->updateStatusBnsSttView($data['idmember'], $data['bnsstatus_view']);
		if($upd > 0) {
			$arr = jsonTrueResponse(null, "Ubah Status View Bonus Statement Berhasil..");
		}
		echo json_encode($arr);
	}
	
	//$route['saldoVA'] = 'webshop/account/stmViewVA'; nandang 201804
	public function stmViewVA() {
		if($this->_checkSessionStoreUser()){
		  $dt['member'] = $this->memberM->getStatusVAView(getUserID());
			//print_r($dt['member']);
           // print_r($dt['member'][0]->dfno );
          $dataVA['dfno']=$dt['member'][0]->dfno;
		  $dataVA['fullnm']=$dt['member'][0]->fullnm;
		   $dataVA['novac']=$dt['member'][0]->novac;
          
		  $dataVA['saldo']= $this->memberM->getDataVA($dataVA['dfno']);  
	      // $dt['prodCat'] = $this->productService->getProductCategory();
		   $this->load->view('webshop/listViewVA', $dataVA);
		 //echo $dataVA['fullnm'];
	   }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
	}
	
	//$route['jatis/sms'] = 'webshop/account/tesJatisSms';
	public function tesJatisSms() {
		$no_hp = "087780441874";
		$text = "TES top VA berhasil sebesar Rp.15000";
		//$text2 = str_replace(" ", "%20", $text);
		
		$res = smsTemplate($no_hp, $text);
		echo $res;
	}
	
	//$route['getUpdateEmail'] = 'webshop/account/getUpdateEmail';'
	public function getUpdateEmail() {
	   if($this->_checkSessionStoreUser()){
		   $dt['member'] = $this->memberM->checkAuthLoginKnet(getUserID());
	       $dt['prodCat'] = $this->productService->getProductCategory();
		   $this->setTempWebShop('webshop/updateEmailMain',$dt);
	   }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
	}
	
	//$route['myaccount'] = 'webshop/account/getAccountInfo';
	public function getAccountInfo() {
	   if($this->_checkSessionStoreUser()){
	   	   //$this->load->model('webshop/Ecomm_trans_model','trx');
	       $dt['prodCat'] = $this->productService->getProductCategory();
		   //$dt['listTrx'] = $this->trx->getEcommerceTrxByUserLogin(getUserID());
	       $this->load->view('webshop/personalParticular');
	   }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
        
	}
    
    //$route['myEmail'] = 'webshop/account/getEmailInfo';
    function getEmailInfo(){
        if($this->_checkSessionStoreUser()){
	       $dt['prodCat'] = $this->productService->getProductCategory();
           $x['dt'] = $this->memberM->getEmail();
           	
	       $this->load->view('webshop/updtEmail',$x);
	   }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
    }
    
    //$route['update/email'] = 'webshop/account/updtEmail';
    function updtEmail(){
        
            $data = $this->input->post(NULL, TRUE);            
            $cekFirstEmail = $this->memberM->cekNewRegisterEmail($data);
            $cekHistoryUpdate = $this->memberM->cekHistoryUpdtEmail($data);
            $cekFirstHP = $this->memberM->cekNewHP($data);
            //print_r($cekFirstEmail);
            if($cekFirstEmail[0]->email_stat == "0" && $cekHistoryUpdate[0]->jml == 0 && $cekFirstHP[0]->hp_stat == "0"){
                //echo "new register email";
                
                if(($data['new_email'] == '' || $data['new_email'] == "") && $data['new_hp'] == '' || $data['new_hp'] == "")
                {
                    $arr = array("response" => "false", "message" => 'Silahkan Isi Email & No Hp Terbaru');
                    echo json_encode($arr);
                }elseif(!valid_email($data['new_email'])){
                    $arr = array("response" => "false", "message" => 'Format Penulisan Email Salah');
                    echo json_encode($arr);
                }else{
                    $updt = $this->memberM->updateEmaildanHP($data);
                    $toHistory = $this->memberM->toHistoryUpdate($data);
                    if($updt == 1){
                        $ressx = $this->memberM->checkAuthLoginKnet(getUserID());
        				$this->session->set_userdata('store_info', $ressx);
                        //$genVoucher = $this->memberM->generateVoucher(getUserID());
                        
                        //Dibuka kalo sudah ada pemberitahuan lebih lanjut
                        /*$text = "Kode Voucher Anda ".$genVoucher['voucher']." utk potongan belanja di www.k-net.co.id";
                        if($genVoucher['response'] == "1"){
                            $sendSms = $this->memberM->sendSmsVoucher($text);
                        }*/
                        $arr = array("response" => "true", "message" => 'Sukses Update Email Baru',"tel_hp" => ''.$ressx[0]->tel_hp.'',"email" => ''.$ressx[0]->email.'' );
                        echo json_encode($arr);
                    }else{
                        $arr = array("response" => "false", "message" => 'Gagal Update Email');
                        echo json_encode($arr);
                    }
                }
            }else{
                /*if($data['new_email'] == '' || $data['new_email'] == ""){
                    $arr = array("response" => "false", "message" => 'Silahkan Isi Email Terbaru');
                    echo json_encode($arr);
                }else*/if(!valid_email($data['new_email']) && $data['new_email'] != ''){
                    $arr = array("response" => "false", "message" => 'Format Penulisan Email Salah');
                    echo json_encode($arr);
                }else{
                    //echo "update email yg sudah ada";
                    $updt = $this->memberM->updateEmaildanHP($data);
                    $toHistory = $this->memberM->toHistoryUpdate($data);
                    if($updt == 1){
                        $ressx = $this->memberM->checkAuthLoginKnet(getUserID());
        				$this->session->set_userdata('store_info', $ressx);
                        $arr = array("response" => "true", "message" => 'Sukses Update Email',"tel_hp" => ''.$ressx[0]->tel_hp.'',"email" => ''.$ressx[0]->email.'' );
                        echo json_encode($arr);
                    }else{
                        $arr = array("response" => "false", "message" => 'Gagal Update Email');
                        echo json_encode($arr);
                    }
                }
            } 
       
    }
    
    //$route['myPassword'] = 'webshop/account/getPasswordbyId';
    function getPasswordbyId(){
        if($this->_checkSessionStoreUser()){
	       $dt['prodCat'] = $this->productService->getProductCategory();
           $x['info'] = $this->memberM->getEmail();
           	
	       $this->load->view('webshop/updatePassword',$x);
	   }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
    }
    
    function getResponseCaptcah($str=''){
        $google_url="https://www.google.com/recaptcha/api/siteverify";
        $secret="6LfdygYUAAAAAOEaZsO_WCyqokllg7HAt1ivTbYt";
        $ip=$_SERVER['REMOTE_ADDR'];
        $url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $res = curl_exec($curl);
        curl_close($curl);
        $res= json_decode($res, true);
        return $res;
    }
    
    function updPassByID(){
        $data = $this->input->post(NULL, TRUE); 
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_recaptcha');
        $capctah = trim($data['g-recaptcha-response']);
        $y = $this->getResponseCaptcah($capctah);
        $lengthPAss = strlen($data['new_pass']);
        
        if(empty($y['success'])){
            $arr = array("response" => "false", "message" => 'Please Checklist Captcha');
            echo json_encode($arr);
        }else{
            if($data['new_pass'] != $data['conf_pass']){
                $arr = array("response" => "false", "message" => 'New Pass and Confirm Pass must be same');
                echo json_encode($arr);
            }elseif($lengthPAss < 6){
                $arr = array("response" => "false", "message" => 'Password is too short');
                echo json_encode($arr);
            }else{
                $updt = $this->memberM->updtPassMember($data);
                $ressx = $this->memberM->checkAuthLoginKnet(getUserID());
                $this->session->set_userdata('store_info', $ressx);
                if($updt == 1){
                    $arr = array("response" => "true", "message" => 'New Password Updated',"password" =>''.$ressx[0]->password.'');
                    echo json_encode($arr);
                }else{
                    $arr = array("response" => "false", "message" => 'New Password Failed');
                    echo json_encode($arr);
                }
            }
        }
    }

	//$route['history/trx'] = 'webshop/account/getTransactionHistory';	
	public function getTransactionHistory() {
	   if(!empty($store_info)){
	       $dt['prodCat'] = $this->productService->getProductCategory();
		$this->load->view('webshop/History_trans');
        }else{
	       echo "Silahkan Login Terlebih dahulu";
	   }
	}	
	
	//$route['tracking'] = 'webshop/account/getPackageTrackingInfo';
	public function getPackageTrackingInfo() {
		
		if($this->_checkSessionStoreUser()){

			$this->load->model('webshop/Ecomm_trans_model','trx');
			$dt['prodCat'] = $this->productService->getProductCategory();
			$dt['listTrx'] = $this->trx->getEcommerceTrxByUserLogin(getUserID());
			$this->setTempWebShop('webshop/account',$dt);
		}

	}
	
	//$route['kgp/track(:any)'] = 'webshop/account/kgpTrackCnote/$1';
	public function kgpTrackCnote($id) {
		$url = "http://www.kgponline.co.id:8080/Service.svc/KlinkUpdStatusResponse";
		$curl_post_data = array("usernama" => "k-link2",
		          "passw" => "pusat@59A",
		          "konos" => "$id");
		$data_string = json_encode($curl_post_data);                                                                                   
		                                                                                                                     
		$ch = curl_init($url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
	                                                                                                                     
	   $curl_response = curl_exec($ch);
	   echo $curl_response;
	}
	
	//$route['tracking2'] = 'webshop/account/getPackageTrackingInfo2';
	public function getPackageTrackingInfo2() {
		//$this->load->view('webshop/tracking');
		$this->load->model('webshop/Ecomm_trans_model','trx');
	    //$dt['prodCat'] = $this->productService->getProductCategory();
		$dt['listTrx'] = $this->trx->getEcommerceTrxByUserLogin(getUserID());
		//if($dt['listTrx'] != null) {
		 $this->load->view('webshop/track_page',$dt);
		//}	
	}
    
    function trackingProcess(){
        $data = $this->input->post(NULL, TRUE);	
		/*$url = "http://api.jne.co.id:8889/tracing/klink/list/cnote/$data[awb]";
		//$url = "http://api.jne.co.id:8889/tracing/klink/list/cnote/$awb";
        $curl_post_data = array(
                            "username" => "KLINK",
                            "api_key" => "76270305bef5d402220c96d59ac61977"
                            );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
		curl_close($curl);
		echo $curl_response;
		*/
		$arr = $this->productService->trackingJneConot($data['awb']);
        echo $arr;
       
    }
	
	//$route['tracking/cnote/(:any)'] = 'webshop/account/getStatusCnoteByID/$1';
	public function getStatusCnoteByID($id) {
		$arr = $this->cartshopService->getStatusCnoteByID($id);
		if($arr['response'] == "true") {
			//echo "<table>";
			//echo "<tr><th>Conote</th><th>Service Type</th><th>Order No</th><th>Status</th></tr>";
			foreach($arr['arrayData'] as $dta) {
				echo "<div class=\"col-lg-12 col-xs-12 cart_content text-center\">  
                <div class=\"col-lg-3 col-xs-3\">".$dta->conoteJNE."</div>
                <div class=\"col-lg-3 col-xs-3\">".$dta->service_type_name."</div> 
                <div class=\"col-lg-3 col-xs-3\">".$dta->orderno."</div>";
				if($dta->flag_send_conote == "0") {
					echo "<div class=\"col-lg-3 col-xs-3\">On Progress</div>";
				} else {
					echo "<div class=\"col-lg-3 col-xs-3\">Sudah di kirim</div>";
				}
                
			}
			
		} else {
			echo $arr['message'];
		}
	}
	
    function testQry(){
        $id = 'IDJHID032175';
        $cekFirstEmail = $this->memberM->cekNewRegisterEmail($id);
        //print_r($cekFirstEmail);
        echo "isinya berapa ".$cekFirstEmail[0]->jml ;
        /*$panjang = 5;
        $karakter = '66834';
        $string = '';
        for($i = 0;$i < $panjang;$i++){
            $pos = rand(0,strlen($karakter)-1);
            $string .= $karakter{$pos};
        }
        $VCH = 'VE'.$string;
        
        //$rand = rand(10000,100);
        echo "random char ".$VCH;*/
    }
	public function tes_barcode()
    {
        //I'm just using rand() function for data example
        $temp = rand(10000, 99999);
        $this->set_barcode($temp);
        //echo "tes";
    }

    private function set_barcode($code)
    {
        //load library
        $this->load->library('zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
    }
	
	//$route['jne/tarif'] = 'webshop/account/tarifJneNew';
	function tarifJneNew() {
		$urlPricing = "http://apiv2.jne.co.id:10101/tracing/api/pricedev";
		//$urlPricing = "http://api.jne.co.id:8889/tracing/klink/price/";
		
		$data = $this->input->post(NULL, TRUE);
        
        $jne_username = "KLINK";
		$jne_api_key = "76270305bef5d402220c96d59ac61977";
        $curl_post_data = array(
            "username" => urlencode($jne_username),
            "api_key" => urlencode($jne_api_key),
            "from" => urlencode($data['from']),
            "thru" => urlencode($data['thru']),
            "weight" => urlencode($data['weight'])
        );		
		$postData = setFieldPost($curl_post_data);
		
		$curl = curl_init();
        $arx = array(
		  CURLOPT_URL => $urlPricing, 
		  CURLOPT_RETURNTRANSFER => true, 
		 CURLOPT_SSL_VERIFYPEER => true,
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => $postData, 
		  CURLOPT_HTTPHEADER => array( 
		    "Content-Type: application/x-www-form-urlencoded")
		 
		  );
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		if ($err) {
			echo $err;
			echo "error";
		} else {
           echo $response;
		}
	}
	
	function kgpTarif() {
		$urlKota = "kgponline.co.id:8080/Service.svc/KlinkTarifResponse";
		//$urlKota = "202.152.20.135/Service.svc/KlinkTarifResponse";
		//$urlKota = "http://36.37.81.131:8080/Service.svc/KlinkTarifResponse";
		$curl = curl_init($urlKota);
        /*$data = array("usernama" => "relas",
                      "passw" => "123456",
					  //"kota" => $personal_info['kota'],
					  "kota" => "040101",
					  "berat" => $this->cart->total_weight());     */
				   
	    $data = array("usernama" => "k-link2",
                      "passw" => "pusat@59A",
					  //"kota" => $personal_info['kota'],
					  //"kota" => $this->session->userdata('destination_address'),
					  "kota" => "040101",
					  "berat" => 1.3);
					                                                        
		$data_string = json_encode($data);                                                                                   
		//print_r($data_string);                                                                                                                     
		$ch = curl_init($urlKota);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                    
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($ch);
		$err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
		$info = null;
		$dfd = array("return" => $result, "err" => $err, "errmsg" => $errmsg, "info" => $info);
		//$sd = json_decode($result, true);
		print_r($dfd);	
		return $result;
	}
}	
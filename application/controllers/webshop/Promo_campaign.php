<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * @author sahid
 */
class Promo_campaign extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
		$this->load->service("webshop/member_service", "memberService");
		$this->load->model("webshop/promo_campaign_model", "promo_campaign");
    }
	
	//$route['promo/calendar'] = 'webshop/promo_campaign/formPromoCalendar';
	function formPromoCalendar() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$dt['listStockist'] = $this->promo_campaign->getListStokistM();
			$dt['listPromo'] = $this->promo_campaign->promo_limit();
			$this->setTempWebShop('webshop/promo_campaign/calendarPromoForm', $dt);
		}	
	}
	
	//$route['promo/calendar/check/(:any)/(:any)'] = 'webshop/promo_campaign/getListPromoCalender/$1/$2';
	function getListPromoCalender($id, $promotype) {
		if($this->_checkSessionStoreUser()) {
			$arr = $this->promo_campaign->cekPromo500BV($id, $promotype);
		} else {
			$arr = jsonFalseResponse("Silahkan login terlebih dahulu..");
		}
		echo json_encode($arr);
	}
	
	//$route['promo/calendar/stk'] = 'webshop/promo_campaign/chooseStockist';
	function chooseStockist() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->memberService->getProductCategory();
			$this->setTempWebShop('webshop/promo_campaign/calendarPromoChooseStk', $dt);
		}	
	}
	
	//$route['promo/calendar/save'] = 'webshop/promo_campaign/savePromoCalendar';
	function savePromoCalendar() {
		if($this->_checkSessionStoreUser()) {
			$dataForm = $this->input->post(NULL, TRUE);
			$arr = $this->promo_campaign->cekPromo500BV($dataForm['idmember'], $dataForm['promotype']);
			if($arr['response'] == "false") {
				$ins = jsonFalseResponse($arr['message']);
			} else {
				$ins = jsonFalseResponse("Save Promo failed..");
				$res = $this->promo_campaign->savePromoCalendar($dataForm);
				if($res != null) {
					$ins = jsonTrueResponse($res, "Input Promo berhasil");
					$this->promo_campaign->sendSMSConfirm($res);
				}
			}
			
		} else {
			$ins = jsonFalseResponse("Silahkan login terlebih dahulu..");
		}
		echo json_encode($ins);
	}

	//$route['promo/calendar/print/(:any)'] = 'webshop/promo_campaign/printNota/$1';
	function printNota($orderno) {
		
		$data['result'] = $this->promo_campaign->getDataPromoByOrderNo($orderno);
		if($data['result'] != null) {
			$data['header'] = "BUKTI CLAIM PROMO CALENDAR";
			$this->load->view("webshop/promo_campaign/calendarPromoPrintNota", $data);
		} else {
			echo "<h3>Silahkan login terlebih dahulu..</h3>";
		}	
	}
	
	function getListPromo($promotype, $period){
		//if($this->_checkSessionStoreUser()) {
			$yr  = substr($period, 0, 2);
			$mth = substr($period, 2, 2);
			$bnsperiod = "20$yr-$mth-01";
			$dt['period'] = $bnsperiod;
			$result = $this->promo_campaign->getListCollagen($promotype, $bnsperiod);
			if($result != null){
				$dt['listCollagen'] = $result;
			}else{
				$result = "No record(s) founded.";
			}
			
			$this->setTempWebShop('webshop/promo_campaign/collagen2016', $dt);
		//}
	}
}
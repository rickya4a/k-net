<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * 
 */
class Cartshop_customer_payment extends MY_Controller {
	//put your code here
    public function __construct() {
        parent::__construct();
		//Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
        //Veritrans_Config::$isProduction = false;
        $this->load->service("webshop/Product_service",'productService');
		$this->load->service("webshop/Payment_service",'paymentService');
		$this->load->service("webshop/Payment_member_service",'payMemberService');
		$this->load->service("webshop/Payment_lp_member_service",'pay_member_lp');
    }
	
	 /*------------------------
	  * SGO PAYMENT (DEV)
	  * -------------------------*/
	  
	  //$route['shop/pay/sgo/inquiry/dev'] = 'webshop/cartshop_customer_payment/getResponseFromSGODev';
	  function getResponseFromSGODev() {
        //$orderid = $this->getPaymentUmroh();
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
		//$dta = $this->paymentService->getTrxSGOByOrderID($xx['order_id']);
		$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
		
		//$this->session->set_userdata("sgo_trx", $dta);
		//print_r($dta);
		$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'K-NET Non Member E-Commerce',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
        
        
    }
    function downloader() {
        force_download('download/Cat2018.pdf',NULL);
    }
	//$route['shop/pay/sgo/notif/dev'] = 'webshop/cartshop_customer_payment/notifAfterPaymentDev';
	public function notifAfterPaymentDev() {
		    $xx = $this->input->post(null,true);
			//var_dump($xx);
            $password = 'k-net181183';
			$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
            $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
            //$totalDP = $sgo_trx[0]->total_pay;
            
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
				//$arr = array("message" => "Total DP tidak sama", "amount" => $xx['amount'], "cost" => $cost);
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
				//$arr = array("message" => "Autentifikasi Salah");
            } else{
                	
               
                $updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
                $resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);    
				$this->paymentService->sendTrxSMSNonMember($resultInsert);	
                $resSukses = array('success_flag' => 0,
                                    'error_message' => 'Transaction K-Net Non Member Success',
                                    'reconcile_id' => rand(15,32),
                                    'order_id' => $xx['order_id'],
                                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                                     );
                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
                    
                //$arr = array("message" => "Siap diinput", "updtTabel" => $updtTabel);
            }
			//echo json_encode($arr);
	}
	
	
	//$route['shop/pay/sgo/finish/dev/(:any)'] = 'webshop/cartshop_customer_payment/afterPaymentWithSGODev/$1';
	public function afterPaymentWithSGODev($order_id) {
		$personal_info = $this->session->userdata('personal_info');
		$dta['trxInsertStatus'] = "fail";	
		//$updtTabel = $this->paymentService->updateTrxSGO($order_id);
		
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);		
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
            $dta['jne'] = $dta['trans'][0]->conoteJNE;

		}
        
		$this->_destroy_cart_session();
		//$arr = $this->cartshopService->removeCart();

		$this->setTemplateDevSGO('webshop/payment_sgo/pay_ecommerce_result',$dta);
		
		//print_r($dt['prodCat']);
	}
	  
	  /*-------------
	   * END
	   * ------------*/
	 /*--------------------------
	  * SGO PAYMENT PRODUCTION
	  ---------------------------*/ 
	 //$route['shop/pay/sgo/inquiry'] = 'webshop/cartshop_customer_payment/getResponseFromSGO';
	 function getResponseFromSGO(){
	 	//UPDATE @ 01/12/2015, SET ONLINE
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
		//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
		$dta = $this->paymentService->getDataPaymentSGOByOrderIDDev($xx['order_id']);
		$cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
		if(!$xx['order_id'])
        {
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array (
                        'errorCode' => 0,
                        'errorDesc' => 'Success',
                        'orderId' => $xx['order_id'],
                        //utk test 'amount' => 350000,
                        'amount' => $cost,
                        'ccy' => 'IDR',
                        'desc' => 'K-NET Non Member E-Commerce',
                        'trx_date' => date('d/m/Y H:i:s')
                        );
                        
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
	
	//$route['shop/pay/sgo/notif'] = 'webshop/cartshop_customer_payment/notifAfterPayment';
	public function notifAfterPayment() {
		    $xx = $this->input->post(null,true);
			//var_dump($xx);
            $password = 'k-net181183';
			//$dta = $this->paymentService->getDataPaymentSGOByOrderID($xx['order_id']);
			$dta = $this->paymentService->getDataPaymentSGOByOrderIDDev($xx['order_id']);
            $cost = $dta[0]->total_pay + $dta[0]->payShip + $dta[0]->charge_connectivity;
            //$totalDP = $sgo_trx[0]->total_pay;
            
            if($xx['amount'] != $cost)
            {   
                die('04,Total DP tidak sama,,,,,');
				//$arr = array("message" => "Total DP tidak sama", "amount" => $xx['amount'], "cost" => $cost);
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
				//$arr = array("message" => "Autentifikasi Salah");
            } else{
                	
               
                $updtTabel = $this->paymentService->updateTrxSGO($xx['order_id']);
                $resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);    
				$this->paymentService->sendTrxSMSNonMember($resultInsert);	
                $resSukses = array('success_flag' => 0,
                                    'error_message' => 'Transaction K-Net Non Member Success',
                                    'reconcile_id' => rand(15,32),
                                    'order_id' => $xx['order_id'],
                                    'reconcile_datetime' =>date('Y-m-d H:i:s')
                                     );
                
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
                    
                //$arr = array("message" => "Siap diinput", "updtTabel" => $updtTabel);
            }
	}
	
	//$route['shop/pay/sgo/finish/(:any)'] = 'webshop/cartshop_customer_payment/afterPaymentWithSGO/$1';
	public function afterPaymentWithSGO($order_id) {

		$personal_info = $this->session->userdata('personal_info');
		$dta['trxInsertStatus'] = "fail";	
		//$updtTabel = $this->paymentService->updateTrxSGO($order_id);
		
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->paymentService->getInsertTrxOrderID($order_id);		
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
            /* if($type == "SameDay" || $type == "Instant"){
                 $dta['jne']= $dta['trans'][0]->service_type_id;
             }else{
                 $dta['jne'] = $dta['trans'][0]->conoteJNE;
             }*/

			$dta['jne'] = $dta['trans'][0]->conoteJNE;
		} else {
			//$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
			$sgo = $this->paymentService->getDataPaymentSGOByOrderIDDev($order_id);
			if($sgo[0]->bank_code_payment == "16") {
				$dta['trxInsertStatus'] = "pending";
			}
		}
        
		$this->_destroy_cart_session();
		//$arr = $this->cartshopService->removeCart();

		$this->setTemplateDevSGO('webshop/payment_sgo/pay_ecommerce_result',$dta);
		
	}

	public function testSSS($order_id) {
		$dta['trans'] = $this->paymentService->getInsertTrxOrderNo($order_id);
		if($dta['trans'] > 0 || $dta['trans'] != null) {
				echo "Order No : ".$dta['trans'][0]->orderno;
		}
		//print_r($dta['trans']);
	}
	
	private function _destroy_cart_session() {
        
		//TAMBAHAN ANA 31/03/2016
        $this->cart->destroy();
       // END TAMBAHAN
       
		/*
		$this->session->unset_userdata('non_member_info');
		$this->session->unset_userdata('order_payment');
		$this->session->unset_userdata('personal_info');
		$this->session->unset_userdata('trxtype');
		//$this->session->unset_userdata('pricecode');
		$this->session->unset_userdata('cart_contents');
		$this->session->unset_userdata('shipping_jne_info');
		$this->session->unset_userdata('sender_address');
		$this->session->unset_userdata('destination_address');
		$this->session->unset_userdata('jne_branch');
		$this->session->unset_userdata('pay_sgo_id');
        $this->session->unset_userdata('total_items');
        $this->session->unset_userdata('total_west_price');
        $this->session->unset_userdata('total_east_price');
     	*/
     	
     	$this->session->sess_destroy();
	}
    
    
	
	/*-----------------------------
	 * END SGO MODULE
	 * ----------------------------*/

}
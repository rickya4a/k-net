<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Promo_novapr extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		//$this->load->model("backend/klinkpromo/m_idc_promo", "promoidc");
		$this->load->service("webshop/Member_service",'memberService');
		$this->load->model("M_mobile_api", "mobile_api");
		$this->folderView = "webshop/promo_novapr/";
	}
	
	public function index() {
		$dt['prodCat'] = $this->memberService->getProductCategory();
		//$dt['res'] = $this->promo->getListIec(getUserID());
		$dt['listBns'] = $this->mobile_api->listPeriodeBnsPromo();	
		if($this->store_info == null) {
			$dt['bns'] = $dt['listBns'][0]->bnsperiod;
			//echo $dt['bns'];
			//echo "dsds".$bnsTerbaru;
			$dt['listPemenangTerbaru'] = $this->mobile_api->listWinnerBnsNovApr($dt['bns']);
			//print_r($dt['listPemenangTerbaru']);
			$this->setTempWebShop($this->folderView.'listPemenang',$dt);
		} else {
			$dt['bns'] = $dt['listBns'][0]->bnsperiod;
			$idmember = getUserID();
			$dt['result'] = $this->mobile_api->getHeaderVchNovApr($idmember);
			//print_r($header);
			if($dt['result'] != null) {
				$arrHeader = array();
				$i=0;
				foreach($dt['result'] as $dc) {
					$arr['VoucherNo'] = $dc->VoucherNo;
					$arr['createdt'] = $dc->createdt;
					$arr['expiredt'] = $dc->ExpireDate;
					$arr['dfno'] = $dc->dfno;
					$arr['Fullnm'] = $dc->Fullnm;
					$arr['claimstatus'] = $dc->claimstatus;
					$arr['claim_date'] = $dc->claim_date;
					$arr['loccd'] = $dc->loccd;
					$arr['sc_fullnm'] = $dc->sc_fullnm;
					$arr['bnsperiod'] = $dc->bnsperiod;
					
					$arr['product'] = array();
					$dtax = $this->mobile_api->getDetailPrdVchNovApr($dc->VoucherNo);
					foreach($dtax as $ffc) {
						$ccx['prdcd'] = $ffc->prdcd;
						$ccx['prdnm'] = $ffc->prdnm;
						$ccx['qtyord'] = $ffc->qtyord;
						
						array_push($arr['product'], $ccx);
					}
					
					array_push($arrHeader, $arr);
					$i++;
				}
				//print_r($arrHeader);
				$dt['arr'] = $arrHeader;
				$this->load->view($this->folderView.'pdf_promoVoucherNovApr',$dt);
			} else {
//				$dt['listPemenangTerbaru'] = $this->mobile_api->listWinnerBnsNovApr($dt['bns']);
				$dt['listPemenangTerbaru'] = $this->mobile_api->listWinnerBnsNovApr();
				//print_r($dt['listPemenangTerbaru']);
				$this->setTempWebShop($this->folderView.'listPemenang',$dt);
			}
		}
	}
	
	//$route['promo_novapr/bns'] = 'webshop/promo_novapr/getListPerBnsPeriod';
	public function getListPerBnsPeriod() {
		$data = $this->input->post(NULL, TRUE);
		
		$dt['listPemenangTerbaru'] = $this->mobile_api->listWnrBnsNovApr($data['searchBy'], $data['searchValue'], $data['bnsperiod']);
		$this->load->view($this->folderView.'listPemenang2',$dt);
	}

		//$route['promo_novapr/info'] = 'webshop/promo_novapr/getListPromo';
	public function getListPromo(){

		//$data = $this->input->post(NULL, TRUE);
		$dt['listPromo'] = $this->mobile_api->listWinnerBnsNovAprAll();

		//print_r($dt['listPromo']);

		$dt = jsonFalseResponse();
		try {
			$dt = $this->mobile_api->listWinnerBnsNovAprAll();
		} catch(Exception $e) {}
		echo json_encode($dt);


	}
	
	//$route['promo_novapr/detailPrd'] = 'webshop/promo_novapr/detailPrd';
	function detailPrd() {
		$data = $this->input->post(NULL, TRUE);
		$dt['detPrd'] = $this->mobile_api->getDetailPrdVchNovApr($data['VoucherNo']);
		$this->load->view($this->folderView.'detPrd',$dt);
		
	}
}
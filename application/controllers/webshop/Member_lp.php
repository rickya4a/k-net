<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * @author sahid
 */
class Member_lp extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->service("webshop/Member_lp_service",'memberlp');
		$this->load->service("webshop/Member_service",'member');
		$this->load->service("webshop/Product_service",'productService');
		$this->folderView = "webshop/member_lp/";
	}

    //$route['blog/reg/(:any)'] = 'webshop/member_lp/regFromBlog/$1';
    public function regFromBlog($idmember) {
    	if($idmember == "") {
			redirect("loginmember");
		}	else {	
			$this->session->sess_destroy();
			$dt['idmember'] = $idmember;
			
				$this->load->service("webshop/member_lp_service", "member_lp");
				$check = $this->member_lp->checkMemberValid2($dt['idmember']);
				//print_r($check);
				if($check != null) {
				   //session_start();
					$this->load->service("webshop/Login_service",'loginService');
				    $promo = $this->loginService->checkListPromo();
				    //print_r($promo);
				    $this->session->set_userdata('promo', $promo);	
					
					$dt['email_pendaftar'] = $check[0]->email;
				  $this->setTempWebShopLP('webshop/member_lp/memberRegisterFromBlog', $dt);
				} else {
				  echo "<h3>ID Recruiter $idmember is not a valid</h3>";	
				}
			} 
    }

	//$route['member/reg/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage/$1/$2';
	public function getRegisterMemberLandingPage($idmember = "", $email_pendaftar = "") {
		if($idmember == "" || $email_pendaftar == "") {
			redirect("loginmember");
		}	else {	
			$this->session->sess_destroy();
			$dt['idmember'] = $idmember;
			
			//$checkAt = strchr($email_pendaftar, '[at]');
			//$checkDot = strchr($email_pendaftar, '[dot]');
			
			$checkAt = strchr($email_pendaftar, '@');
			$checkDot = strchr($email_pendaftar, '.');
			
			if($checkAt != "" && $checkDot != "") {
			
				/*$ss = str_replace("[at]", "@", $email_pendaftar);
				$ss2 = str_replace("[dot]", ".", $ss);
				 * 
				 */
				//$dt['email_pendaftar'] = $ss2;	
				$dt['email_pendaftar'] = $email_pendaftar;
				
				$this->load->service("webshop/member_lp_service", "member_lp");
				$check = $this->member_lp->checkValidationMemberID($dt['idmember']);
				if($check) {
				  session_start();
					$this->load->service("webshop/Login_service",'loginService');
				    $promo = $this->loginService->checkListPromo();
				    //print_r($promo);
				    $this->session->set_userdata('promo', $promo);	
				  $this->setTempWebShopLP('webshop/member_lp/memberRegisterLP2Param', $dt);
				} else {
				  echo "<h3>ID Recruiter $idmember is not a valid</h3>";	
				}
			} else {
				//echo "<h3>Invalid email format, should contains '[at]' and '[dot]'</h3>";
				echo "<h3>Invalid email format, should contains '@' and '.'</h3>";
			}
			
		}			
	}
	
	//$route['member/reg/(:any)/(:any)/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage2/$1/$2/$3/$4';
	public function getRegisterMemberLandingPage2($idmember, $email_pendaftar, $nama, $email_memb) {
				
			$this->session->sess_destroy();
			$dt['idmember'] = $idmember;
			
			//$checkAt = strchr($email_pendaftar, '[at]');
			//$checkDot = strchr($email_pendaftar, '[dot]');
			
			$checkAt = strchr($email_pendaftar, '@');
			$checkDot = strchr($email_pendaftar, '.');
			
			//$checkAt2 = strchr($email_memb, '[at]');
			//$checkDot2 = strchr($email_memb, '[dot]');
			
			$checkAt2 = strchr($email_memb, '@');
			$checkDot2 = strchr($email_memb, '.');
			
			if($checkAt != "" && $checkDot != "" && $checkAt2 != "" && $checkDot2 != "") {
			
				/*$ss = str_replace("[at]", "@", $email_pendaftar);
				$ss2 = str_replace("[dot]", ".", $ss);
				$dt['email_pendaftar'] = $ss2;	
				*/
				$dt['nama'] = $nama;
				$dt['email_pendaftar'] = $email_pendaftar;
				/*$ems = str_replace("[at]", "@", $email_memb);
				$ems2 = str_replace("[dot]", ".", $ems);
				
				 * 
				 */
				$dt['email_memb'] = $email_memb;	
				
				$this->load->service("webshop/member_lp_service", "member_lp");
				$check = $this->member_lp->checkValidationMemberID($dt['idmember']);
				if($check) {
				  session_start();
					$this->load->service("webshop/Login_service",'loginService');
				    $promo = $this->loginService->checkListPromo();
				    //print_r($promo);
				    $this->session->set_userdata('promo', $promo);	
				  $this->setTempWebShopLP('webshop/member_lp/memberRegisterLP', $dt);
				} else {
				  echo "<h3>ID Recruiter $idmember is not a valid</h3>";	
				}			
			} else {
				echo "<h3>Invalid email format, should contains '@' and '.'</h3>";
			}
			//$dt['listBank'] = $this->memberlp->getListBank();		
					
	}
	
	
	//$route['member/reg/(:any)/(:any)/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage3/$1/$2/$3/$4/$5';
	public function getRegisterMemberLandingPage3($idmember, $email_pendaftar, $nama, $email_memb, $id_lp) {
				
			$this->session->sess_destroy();
			$dt['idmember'] = $idmember;
			
			//$checkAt = strchr($email_pendaftar, '[at]');
			//$checkDot = strchr($email_pendaftar, '[dot]');
			
			$checkAt = strchr($email_pendaftar, '@');
			$checkDot = strchr($email_pendaftar, '.');
			
			//$checkAt2 = strchr($email_memb, '[at]');
			//$checkDot2 = strchr($email_memb, '[dot]');
			
			$checkAt2 = strchr($email_memb, '@');
			$checkDot2 = strchr($email_memb, '.');
			
			if($checkAt != "" && $checkDot != "" && $checkAt2 != "" && $checkDot2 != "") {
			
				/*$ss = str_replace("[at]", "@", $email_pendaftar);
				$ss2 = str_replace("[dot]", ".", $ss);
				$dt['email_pendaftar'] = $ss2;	
				*/
				$dt['nama'] = $nama;
				$dt['email_pendaftar'] = $email_pendaftar;
				/*$ems = str_replace("[at]", "@", $email_memb);
				$ems2 = str_replace("[dot]", ".", $ems);
				
				 * 
				 */
				$dt['email_memb'] = $email_memb;	
				$dt['id_lp'] = $id_lp;
				
				$this->load->service("webshop/member_lp_service", "member_lp");
				$check = $this->member_lp->checkValidationMemberID($dt['idmember']);
				if($check) {
					session_start();
					$this->load->service("webshop/Login_service",'loginService');
				    $promo = $this->loginService->checkListPromo();
				    //print_r($promo);
				    $this->session->set_userdata('promo', $promo);	
				
				  $this->setTempWebShopLP('webshop/member_lp/memberRegisterLP5Param', $dt);
				} else {
				  echo "<h3>ID Recruiter $idmember is not a valid</h3>";	
				}			
			} else {
				echo "<h3>Invalid email format, should contains '@' and '.'</h3>";
			}
			//$dt['listBank'] = $this->memberlp->getListBank();		
					
	}
	
	
	//$route['member/reg/starterkit'] = 'webshop/member_lp/chooseStarterkit2';
	public function chooseStarterkit2() {
					
			$member_info = $this->session->userdata('member_info');
			$personal_info = $this->session->userdata('personal_info');
			$dtaForm = $this->input->post(NULL, TRUE);
			
			if($dtaForm != null) {
				$this->session->set_userdata('member_info', $dtaForm);
			} 
			
			//$check = $this->memberlp->checkInputMemberLP($dtaForm);
			$check = $this->member->checkInputMemberWithVoucher($dtaForm);
			if($check['response'] == "false") {
				$dt['err_msg'] = $check['err'];
				//$dt['prodCat'] = $this->productService->getProductCategory();
		    	//$dt['listBank'] = $this->memberlp->getListBank();
		    	//echo "masuk sini";
				$this->setTempWebShopLP('webshop/member_lp/memberRegisterLPBackError',$dt); 
			} else {
				
				//echo "masuk sana";
				$dt['listCargo'] = $this->member->getListCargo();
				$dt['show_provinsi'] = $this->member->showListProvinsi();    
			    $dt['listStarterkit'] = $this->member->listStarterkit(); 
				//print_r($dt['listStartekit']);
				if($personal_info != null) {	
					//$dt['shipping'] = $this->cartshopService->getShippingData($personal_info);	
					$this->setTempWebShopLP('webshop/member_lp/memberChooseSK_LP',$dt);
				} else {
					$this->setTempWebShopLP('webshop/member_lp/memberChooseSK_LP',$dt); 
				}
			}		
			
	}
	
	
    //$route['shipping/reg/getStockist/(:any)'] = 'webshop/member_lp/getStockist/$1';
	function getStockist($area) {       
        if($this->_checkSessionStoreUser()) {
			$cek = $this->memberlp->getListStockistByArea($area);
	        echo json_encode($cek);      
		}
    }
 
    //$route['shipping/reg/getPricecode/(:any)'] = 'webshop/member_lp/getPriceStockist/$1';
	function getPriceStockist($nilai) {
        
			$cek = $this->memberlp->showPricecodeByStockistID($nilai);
	        echo json_encode($cek);
	
    }
	
	//$route['shipping/reg/getPricecode/(:any)/(:any)'] = 'webshop/shipping/getPriceStockist/$1';
	function getPriceStockistX($nilai, $kec) {
		$cek = $this->memberlp->showPricecodeByStockistIDX($nilai, $kec);
        echo json_encode($cek);
	}
    
    //$route['shipping/reg/kota/(:any)'] = 'webshop/member_lp/getListKota/$1';
	function getListKota($prov) {
        
	        $res = $this->memberlp->showListKota($prov);
	        echo json_encode($res);      
		
    }
    
    //$route['shipping/reg/kecamatan/(:any)'] = 'webshop/member_lp/getListKecamatan/$1';
	function getListKecamatan($kota) {
        
			$res = $this->memberlp->showListKecamatan($kota);
	        echo json_encode($res);
		     
    }
    
    //$route['shipping/reg/kelurahan/(:any)'] = 'webshop/member_lp/getListKelurahan/$1';
    function getListKelurahan($kec) {
        
			$res = $this->memberlp->showListKelurahan($kec);
			//print_r($res);
	        echo json_encode($res);      
		
    }
	
	//$route['member/reg/checkout'] = 'webshop/member_lp/memberCheckout';
	public function memberCheckout() {
		
				
			$dataForm = $this->input->post(NULL, TRUE);
			$dt['prodCat'] = $this->productService->getProductCategory();
			if(isset($dataForm)) {
				$dt['sess_memb'] = $this->session->userdata('member_info');
				$dt['sess_pinfo'] = $this->memberlp->sessionRegPersonalInfo($dataForm);
				$dt['sess_sk'] = $this->memberlp->sessionRegStarterkit($dataForm);
				$dt['sess_ship'] = $this->memberlp->setShippingInfo();
			} else {
				$dt['sess_memb'] = $this->session->userdata('member_info');
				$dt['sess_pinfo'] = $this->session->userdata('personal_info');
				$dt['sess_sk'] = $this->session->userdata('starterkit_prd');
				$dt['sess_ship'] = $this->session->userdata('shipping_jne_info');
			}
			$this->setTempWebShopLP('webshop/member_lp/memberCheckout_LP',$dt);
		
	}

	//$route['memberlp/checkout/sgo'] = 'webshop/member_lp/memberCheckoutSGO';
	public function memberCheckoutSGO() {		
			$dataForm = $this->input->post(NULL, TRUE);
			$this->load->service("webshop/Member_service",'memberService');
			if(isset($dataForm)) {
				$dt['sess_memb'] = $this->session->userdata('member_info');
				//$dt['sess_pinfo'] = $this->memberlp->sessionRegPersonalInfo($dataForm);
				//$dt['sess_sk'] = $this->memberlp->sessionRegStarterkit($dataForm);
				//$dt['sess_ship'] = $this->memberlp->setShippingInfo();
				
				$dt['sess_pinfo'] = $this->memberService->sessionRegPersonalInfo_baru($dataForm);
			    $dt['sess_sk'] = $this->memberService->sessionRegStarterkit($dataForm);
			    $dt['sess_ship'] = $this->memberService->setShippingInfoBaru($dataForm);
			} else {
				$dt['sess_memb'] = $this->session->userdata('member_info');
				$dt['sess_pinfo'] = $this->session->userdata('personal_info');
				$dt['sess_sk'] = $this->session->userdata('starterkit_prd');
				$dt['sess_ship'] = $this->session->userdata('shipping_jne_info');
			}
			//$this->setTempWebShopLP('webshop/member_lp/memberCheckout_LP_SGO',$dt);
			
			if($dt['sess_ship'] != 1 && $dt['sess_ship'] == null) {
					echo "<script>
						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya pada JNE..');
						</script>";
				} else {
					//SGO KEY DEVELOPMENT LANDING PAGE REG	
					//$dt['key'] = '100370066c985e4c08a53e5a27c730ce';
					//SGO KEY PRODUCTION LANDING PAGE REG	
					
					//$this->load->service("webshop/Cartshop_service",'cartshopService');
					if($dt['sess_memb']['recruiterid'] == "IDSPAAA66834") {
					   //$dt['key'] = '100370066c985e4c08a53e5a27c730ce';
						$dt['key'] = '0df5835ee198d49944c372ead860c241';
					   $dt['listBank'] = $this->memberService->getBankJohnDoe();
					} else {
					   //$dt['key'] = 'ea30886289b7281f10eac07d474c7c82';
					   $dt['key'] = '51edf5e8117da341a8be702d9bc18de5';
					   //$dt['listBank'] = $this->memberService->getBank();
					   $dt['listBank'] = $this->memberService->getBankNonVABaru();
					}
					//$dt['listBank'] = $this->memberService->getBank();		
					//remark  by Dionn @18/03/2016
					/*
					$pay_memb_sgo_id = $this->session->userdata("pay_memb_sgo_id");
					if($pay_memb_sgo_id == null || !isset($pay_memb_sgo_id)) {
						$dt['payID'] = "LP".randomNumber(8);
						$this->session->set_userdata("pay_memb_sgo_id", $dt['payID']);
						//$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
						$insTempTrx = $this->memberService->insertTempMemberLPSGO($dt['payID']);
					} else {
						$dt['payID'] = $this->session->userdata("pay_memb_sgo_id");
						$del = $this->memberService->deleteTempMemberSGO($dt['payID']);
						//$insTempTrx = $this->memberService->insertTempMemberSGO($dt['payID']);
						$insTempTrx = $this->memberService->insertTempMemberLPSGO($dt['payID']);
					}
					*/
					
					$dt['payID'] = "LP".randomNumber(8);
                    $this->session->unset_userdata('pay_memb_sgo_id');
					$this->session->set_userdata("pay_memb_sgo_id", $dt['payID']);
					$insTempTrx = $this->memberService->insertTempMemberLPSGO($dt['payID']);
					
					$this->setTempWebShopLP_SGO('webshop/member_lp/memberCheckout_LP_SGO',$dt);	
				}
		    
	}

    //$route['memberlp/checkout/sgo/pay/preview'] = 'webshop/member_lp/memberLpPreviewPay';
	public function memberLpPreviewPay() {
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$personal_info = $this->session->userdata('personal_info');
		
		//print_r($dt['prodCat']);
		$dt = $this->input->post(NULL, TRUE);
		$arr = array(
			"bank_code_payment" => $dt['bankid'],
			"payShip" => $shipping_jne_info['price'],
			"payAdm" => $dt['charge_admin'],
			"payConnectivity" => $dt['charge_connectivity'],
			"sentTo" => $personal_info['delivery'],
			"userlogin" => getUserID()
		);
		$dt['res'] = $this->memberlp->updateBankCodePayment($dt['temp_paymentIdx'], $arr);
		
		//Add by DION @ 23/08/2016
		$updPaySGO = array(
					  "orderno" => $dt['temp_paymentIdx'],
					  "bank_code_payment" => $dt['bankid'],
					  "charge_admin" => $dt['charge_admin']
		);
		$this->memberlp->updatePaydetSGO($updPaySGO);
		
		
		$member_info = $this->session->userdata("member_info");
		if($member_info['recruiterid'] == "IDSPAAA66834") {
			$dt['backURL'] = "http://www.k-net.co.id/memberlp/sgo/finish/dev/".$dt['temp_paymentIdx'];
			$this->setTempWebShopDevLP_SGO($this->folderView.'payment_sgo_preview',$dt);
		} else {
			$dt['backURL'] = "http://www.k-net.co.id/memberlp/sgo/finish/".$dt['temp_paymentIdx'];
			$this->setTempWebShopLP_SGO($this->folderView.'payment_sgo_preview',$dt);
		}	
	}
	
	//$route['member/reg/payment/card'] = 'webshop/member_lp/chooseCardType';
	function chooseCardType() {
		$data = $this->input->post(NULL, TRUE);
		if($data['tipekartu'] == "cc") {
			$this->load->view('webshop/member_lp/credit_card');
		} else {
			echo "<option value=\"\" selected>--Pilih jenis pembayaran--</option>";
			echo "<option value=\"mandiri_clickpay\">Mandiri ClickPay</option>";
			echo "<option value=\"cimb_click\">CIMB Clicks</option>";
			echo "<option value=\"virtual_account\">Virtual Account</option>";
			echo "<option value=\"epay_bri\">e-Pay BRI</option>";
		}
	}
	
	//$route[''member/reg/payment/dc/(:any)'] = 'webshop/member_lp/chooseDebitCardPaymentType/$1';
	function chooseDebitCardPaymentType($payType) {
		
			$this->load->view("webshop/member_lp/".$payType."");
			
	}
	
	//$route['member/reg/downline'] = 'webshop/member_lp/getUpdateDownline';
	function getUpdateDownline() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->productService->getProductCategory();
			$dt['listRecruit'] = $this->memberlp->getListRecruitLandingPage(getUserID(), "kiv");
			$this->setTempWebShop('webshop/memberUpdDownline',$dt);
		}	
	}
	
	//$route['member/reg/list/recruit'] = 'webshop/member_lp/getlistRecruitLP';
	function getlistRecruitLP() {
		if($this->_checkSessionStoreUser()) {
			$dt['prodCat'] = $this->productService->getProductCategory();
			$dt['listRecruit'] = $this->memberlp->getListRecruitLandingPage(getUserID(), "non-kiv");
			$this->setTempWebShop('webshop/memberUpdDownline',$dt);
		}	
	}
	
	//$route['member/reg/sponsor/update'] = 'webshop/member_lp/updateSponsor';
	function updateSponsor() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);	
			$arr = $this->memberlp->updateSponsorLP($data);
			echo json_encode($arr);
		}	
	}
}
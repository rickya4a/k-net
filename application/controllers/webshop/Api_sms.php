<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Api_sms extends MY_Controller {
	public function __construct() {
		parent::__construct();
	}
	
	/*-------------------------------------------
	REQUEST DARI MAS TOTO UTK KIRIM SMS KE MEMBER
	--------------------------------------------*/
	
	//$route['api/sms/sendtext/(:any)'] = 'webshop/api_sms/sendSMS/$1';
	public function sendSMS($id) {
	    //$route['api/sms/sendtext/(:any)'] = 'webshop/api_sms/sendSMS/$1';
		$this->load->model('webshop/m_api_sms', 'api_sms');
		$result = $this->api_sms->sendSMSMember($id);
		if($result != null) {
			$res = smsTemplate($result[0]->notelp, $result[0]->berita);
			$potong = substr($res, 0, 8);
			//Status=1 berarti SMS berhasil dikirim
			//echo $potong;
			if($potong == "Status=1") {
			    $updStatus = $this->api_sms->updateFlagSMS($id, "K");
				echo "Pesan berhasil dikirim";
			} else {
				$updStatus = $this->api_sms->updateFlagSMS($id, "G");
				echo "Pesan GAGAL dikirim";
			}	
		}  else {
			echo "Data dengan no trx : $id tidak ditemukan..";
		}
	}
	
	//$route['api/sms/kosong'] = 'webshop/api_sms/kosong';
	public function kosong() {
		$dest = $this->input->post("dest");
		$message = $this->input->post("message");

		$fields = array(
			'u' => urlencode("KLink1ndo"),
			'p' => urlencode("v58ZcM2L"),
			'd' => urlencode($dest),
			'm' => urlencode($message)
		);

		$postData = setFieldPost($fields);

		$arr = array(
			"url" => "https://smsgw.sprintasia.net/api/msg.php",
			"postdata" => $postData
		);

		$curl = curl_init();

		$arx = array(
			CURLOPT_URL => $arr['url'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $arr['postdata'],
			CURLOPT_HTTPHEADER => array(
				// "Cache-Control: no-cache",
				"Content-Type: application/x-www-form-urlencoded")
		);
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
		} else {
			echo $response;
			//print_r($response);
			//print_r($response);
		}
	}
}
<?php

    class Vch_memb extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->service("webshop/Product_service",'productService');
            $this->load->model('webshop/M_vch_membership','m_vch');
            
            //$this->load->service("webshop/Member_service",'memberService');
            //$this->folderView = "webshop/vch_member/";
        }

        public function index(){

            if($this->_checkSessionStoreUser()){
                
                $dt['prodCat'] = $this->productService->getProductCategory();
                $dt['result'] = $this->m_vch->getVoucher(getUserID());
                
                $this->setTempWebShop('webshop/v_vch_membership',$dt);
            }
        }
    }
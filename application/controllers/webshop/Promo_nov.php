<?php

    class Promo_nov extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->model("webshop/Sales_model","mSales");

            $this->load->service("webshop/Product_service",'productService');
            $this->load->service("webshop/Cartshop_service",'cartshopService');

            $this->folderPrd = base_url()."assets/images/products/thumb/";
            $this->folderView = "webshop/promo_nov/";
            $this->folderView2 = "webshop/payment_sgo_vch/";


            $this->load->model('webshop/Member_model');
            $this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
            $this->load->service("webshop/Cartshop_service",'cartshopService');
        }

        public function home(){

            if($this->_checkSessionStoreUser()) {

                $dt['prdnm'] = $this->input->post("name");
                $dt['formAction'] = "".site_url('cart/addtocart')."";
                $dt['banner'] = $this->productService->getHeaderBanner();
                $dt['prod'] = $this->productService->getProductShop();
                $dt['prodCat'] = $this->productService->getProductCategory();
                $dt['folderPrd'] = $this->folderPrd;
                $this->session->unset_userdata('sales_lp');
//            $username = getUserID();

                $this->setTempWebShop('webshop/promo_nov/Shop_home', $dt);
            }
        }

        function addToCart() {

            if($this->_checkSessionStoreUser()) {

                $member_info = $this->session->userdata('member_info');

                if(!empty($member_info)) {

                    $this->session->unset_userdata('personal_info');
                    $this->session->unset_userdata('starterkit_prd');
                    $this->session->unset_userdata('memb_choose_pay');
                    $this->session->unset_userdata('member_info');
                    $this->session->unset_userdata('sender_address');
                    $this->session->unset_userdata('destination_address');
                    $this->session->unset_userdata('jne_branch');
                    $this->session->unset_userdata('shipping_jne_info');
                }

                if(!$this->cartshopService->IfDoubleInputCart($this->input->post('prdcd'))) {

                    $arr = $this->cartshopService->addToCart();
                }
                else {

                    $arr = array("response" => "false", "message" => "Data product ".$this->input->post('prdnm')." sudah ada di cart");
                }

                echo json_encode($arr);
            }
        }

        function selectProduct(){

            $pilih = $this->input->post('pilih');

            $data = $this->mSales->pilihProduk($pilih);

            if($data != null){

                $arr = jsonTrueResponse($data);
            }else{

                $arr = jsonFalseResponses("data kososng!");
            }

            if('IS_AJAX') {
                echo json_encode($arr);
            }

        }

        function checkout(){

            $data = $this->input->post(NULL, TRUE);
            print_r($data);

            $data['trans_id'] = "EC".randomNumber(8);

            //$hadiahnya = $this->session->userdata('hadiahnya');
            //$this->session->set_userdata('hadiah', $data['hadiah']);

            $personal_info = $this->session->userdata('personal_info');
            $pricecode = $this->session->userdata('pricecode');
            $shipping_jne_info = $this->session->userdata('shipping_jne_info');
            $sender_address = $this->session->userdata('sender_address');
            $dest_address = $this->session->userdata('destination_address');

            $jne_branch = $this->session->userdata('jne_branch');
            $promoDiscShipping = $this->session->userdata('promo');
            $bns = substr($personal_info['bnsperiod'], 3, 7);

            //SET HARGA
            $harga = getTotalPayNetBaru();
            $totPay = $harga['total_pay'];
            $selisihHargaCust = 0;
            //END

            $totalbv = $this->cart->total_bv();
            $freeship = "0";

            $biaya = getTotalPayNetAndShipCost2();
            $gross_amount = $biaya['total_pay'];
            $freeship = $biaya['freeship'];
            $discount = $biaya['tot_discount'];

            $shipper_telhp = getUserPhone();
            $admin_cost = 0;
            $nmstk = explode(" - ", $personal_info['nama_stockist']);
            $nmstkSS = $nmstk[0]." - ".$nmstk[1];
            $produk = $this->cart->contents();

            $cust_memb = $this->session->userdata("non_member_info");
            if(isset($cust_memb)) {
                $is_login = "2";
                $tot_paynet = getTotalPayNet();
            } else {
                $memb = $this->session->userdata("store_info");
                if(isset($memb)) {
                    $is_login = "1";
                    $tot_paynet = 0;
                }
            }

            if($personal_info['delivery'] == "2") {
                $arrx = array(
                    "origin_name" => $shipping_jne_info['origin_name'],
                    "destination_name" => $shipping_jne_info['destination_name'],
                    "service_code" => $shipping_jne_info['service_code'],
                    "service_display" => $shipping_jne_info['service_display'],
                    "price" => $data['total_ship_price'],
                    "etd_from" => $shipping_jne_info['etd_from'],
                    "etd_thru" => $shipping_jne_info['etd_thru'],
                    "times" => $shipping_jne_info['times'],
                    "ship_discount" => $data['total_diskon_ship'],
                );
                //print_r($arrx);
                $this->session->unset_userdata('shipping_jne_info');
                $this->session->set_userdata('shipping_jne_info', $arrx);
                $shipping_jne_info = $this->session->userdata('shipping_jne_info');
            }

            //CHECK APAKAH ADA PROMO AGUSTUS YG AKTIF
            $promo_agust = "0";
            if(array_key_exists("promo_agust", $data)) {
                $promo_agust = $data['promo_agust'];
            }

            //CHECK APAKAH MEMBER MEMILIH PEMBELANJAAN DROPSHIPPER
            $dropship = $personal_info['id_lp'];
            if($personal_info['id_lp'] == "DROPSHIP") {
                $selisihHargaCust = getTotalPayNet() - $totPay;
                $totPay = getTotalPayNet();
            }

            //set hadiah
//            $hadiah = array(
//
//                "hadiahnya" => $hadiahnya;
//            );

            //SET PARAM UTK HADIAH
            $this->session->unset_userdata('promo_free_prd');
            //$count = count($data['free_prdcd']);
            //echo $count;

            if($data['free_prdcd'] != "kosong"){

                $arrNov['free_prdcd'] = $data['free_prdcd'];
                $arrNov['free_prdnm'] = $data['free_prdnm'];
                $arrNov['free_qty'] = $data['free_qty'];
                $arrNov['free_bv'] = $data['free_bv'];
                $arrNov['free_westPrice'] = $data['free_westPrice'];
                $arrNov['free_eastPrice'] = $data['free_eastPrice'];
                $arrNov['free_weight'] = $data['free_weight'];

                $this->session->set_userdata('promo_free_prd', $arrNov);

            }else{

                $arrNov = null;
            }


            //SET PARAM UNTUK
            if($arrNov != null){

                $prdParam = array(
                    "trans_id" => $data['trans_id'],
                    "total_sales_weight" => $this->cart->total_weight(),
                    "dropship" =>	$dropship,
                    "listProduct" => $produk,
                    "pricecode" => $pricecode,
                    "dataForm" => $data,
                    "sentTo" => $personal_info['delivery'],
                    "promoAgust2018" => $promo_agust,
                    "promoNovember" => $arrNov
                );

            }else{

                $prdParam = array(
                    "trans_id" => $data['trans_id'],
                    "total_sales_weight" => $this->cart->total_weight(),
                    "dropship" =>	$dropship,
                    "listProduct" => $produk,
                    "pricecode" => $pricecode,
                    "dataForm" => $data,
                    "sentTo" => $personal_info['delivery'],
                    "promoAgust2018" => $promo_agust
                );
            }

//            echo "<pre>";
//            print_r($prdParam);
//            echo "</pre>";
            $insPrdArr = $this->insertProduct($prdParam);

            $arrx['product'] = $insPrdArr['product'];

//            echo "<pre>";
//            print_r($insPrdArr['product']);
//            echo "</pre>";

            //CHECK APAKAH BARANG DIKIRIM / DI AMBIL DI STOKIS
            $arrShip = null;
            if($personal_info['delivery'] == "2") {
                // $stk = explode(" - ", $personal_info['stockist']);
                //$nama_stk = trim($stk[1]);

                $ssx = explode(" - ", $personal_info['nama_stockist']);
                $nama_stk2 = str_replace("-", "", $ssx[1]);
                $nama_stk = substr($nama_stk2, 0, 20);

                //print_r($ssx);
                $arrShip = array(
                    "orderno" => $data['trans_id'],
                    "idstk" => $personal_info['stockist'],
                    "prov_code" => $personal_info['provinsi'],
                    "kab_code" => $personal_info['kota'],
                    "kec_code" => $personal_info['kecamatan'],
                    "province_name" => $personal_info['nama_provinsi'],
                    "kabupaten_name" => $personal_info['nama_kota'],
                    "stockist_name" => $nama_stk,

                    "receiver_name" => $personal_info['nama_penerima'],
                    "addr1" => str_replace("�??", "", $personal_info['alamat']),
                    "email" => $personal_info['email'],
                    "tel_hp1" => $personal_info['notlp'],
                    "tel_hp2" => getUserPhone(),
                    "shipper_telhp" => getUserPhone(),

                    "service_type_id" => $shipping_jne_info['service_code'],
                    "service_type_name" => $shipping_jne_info['service_display'],
                    "total_item" => $insPrdArr['total_item'],
                    "total_weight" => $insPrdArr['total_weight'],
                    "sender_address" => $sender_address,
                    "dest_address" => $dest_address,
                    "jne_branch" => $jne_branch,
                    "cargo_id" => $personal_info['shipper'],
                    "total_pay_net" => $totPay,

                );
            }

            $arrx['shipping'] = $arrShip;

            //echo "Total Weight semua : ".$total_weight;
            //echo "<br />nTotal Belanja : ".$totPay;
            //echo "<br />Selisih harga CUST/DIST : ".$selisihHargaCust;
            if(array_key_exists("bankid", $data)) {
                $bankid = $data['bankid'];
                $charge_connectivity = $data['charge_connectivity'];
                $charge_admin = (int) $data['charge_admin'];
            }	 else {
                $bankid = "19";
                $charge_connectivity = "";
                $charge_admin = 0;
            }
			
			//echo "bnsperiod_ori = $bns";
            $arrx['summary'] = array(
                "orderno" => $data['trans_id'],
                "userlogin" => getUserID(),
                "bankaccno" => '',
                "token" => $data['trans_id'],
                "id_memb" => trim($personal_info['idmemberx']),
                "nmmember" => trim($personal_info['membername']),
                "total_pay" => $totPay,
                "total_bv" => $totalbv,
                "pricecode" => $pricecode,
                "bonusmonth" => $bns,
                "datetrans" => date("Y-m-d H:i:s"),
                "idstk" => trim($personal_info['stockist']),
                "nmstkk" => trim($nmstkSS),
                "secno" => "0",
                "flag_trx" =>"W",
                "sentTo" => $personal_info['delivery'],
                "status_vt_pay" => "0",
                "status_vt_reject_dt" => "",
                "payShip" => $shipping_jne_info['price'],
                "payAdm" => $data['charge_admin'],
                "is_login" => $is_login,
                "totPayCP" => $selisihHargaCust,
                "id_lp" => $personal_info['id_lp'],
                "free_shipping" => $freeship,
                "discount_shipping" => $shipping_jne_info['ship_discount'],
                "bank_code_payment" => $bankid,
                "payConnectivity" => $charge_connectivity,
                "payAdm" => $charge_admin,
            );

            //$arrx['summary'] = null;

            $res = $this->checkPaymentSales($data, $arrx);
            $arrx['pay'] =  $res;

            $hasil = $this->insertTempTableSgo($arrx);

//            echo "<pre>";
//            print_r($arrx['keySgo']);
//            echo "</pre>";


            $arrx['bankDescDetail'] = $data['bankDescDetail'];
            $arrx['bankDesc'] = $data['bankDesc'];
            $arrx['bankCode'] = $data['bankCode'];
            $arrx['tot_pay'] = $arrx['summary']['total_pay'];
            $arrx['tot_nilai_voucher'] = $arrx['pay']['tot_nilai_voucher'];
            $arrx['prd_value_minus_voucher'] = $arrx['summary']['total_pay'] - $arrx['pay']['tot_nilai_voucher'];
            $arrx['pay_gateway'] = $arrx['pay']['pay_gateway'];
            $arrx['vch_stt'] = $arrx['pay']['vch_stt'];
            $arrx['temp_paymentIdx'] = $data['trans_id'];
            $arrx['prodCat'] = $this->cartshopService->getProductCategory();
            $arrx['keySgo'] = $this->session->userdata('pay_sgo_id');
            $this->load->model('webshop/shared_module', 'shared');
            //$res = $this->shared->updateBankCodePayment2($temp_id, $arr);
            $arrx['temp_result'] = $this->shared->getDataPaymentSGOByOrderID($data['trans_id']);

            if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {

                $arrx['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/dev/".$arrx['temp_paymentIdx'];
                if($arrx['temp_result'] != null) {
                    //print_r($arrx['temp_result']);
                    //production
                    //$arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
                    //development 0df5835ee198d49944c372ead860c241
                    $arrx['keySgo'] = '0df5835ee198d49944c372ead860c241';
                    $this->setTemplateDevSGO($this->folderView.'payment_sgo_vch_prev_new',$arrx);
                } else {
                    echo "Data tidak dapat diinput..";
                }

                //$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
                //$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
            } else {

                $arrx['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$arrx['temp_paymentIdx'];
                if($arrx['temp_result'] != null) {
                    $arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
                    $this->setTemplateSGO($this->folderView.'payment_sgo_vch_prev_new',$arrx);
                    //$this->setTemplateSGO('webshop/payment_sgo_preview',$arrx);
                } else {
                    echo "Data tidak dapat diinput..";
                }

            }

//            echo "<pre>";
//            print_r($arrx);
//            echo "</pre>";
        }
		
		function insertProductBaru($array) {
			 $data = $array['dataForm'];
            $total_weight = $array['total_sales_weight'];
            $total_item = 0;
            $arrx['product']= array();
			
			//if(array_key_exists("free_prdcd", $data)) {
			if($array['free_produk'] != null) {
				$freePrd = array(
					"orderno" => $array['trans_id'],
					"prdcd" => $array['free_produk']['free_prdcd'],
					"prdnm" => $array['free_produk']['free_prdnm'],
					"qty" => "1",
					"bvr" => "0",
					"dpr" => "0",
					"pricecode" => $array['pricecode'],
					"sentTo" => $array['sentTo'],

				);
				
				$total_weight += $array['free_produk']['free_weight'];
				$total_item += 1;
				
				array_push($arrx['product'], $freePrd);
            }
            
            if($array['spec_prd'] != null) {
                //echo "harusnya masuk";
                $prdcd = $array['spec_prd']['spec_prdcd'];
                $qty = (int)$array['spec_prd']['spec_qty'];
                $bv = (int)$array['spec_prd']['spec_bv'];
                $prdnm = $array['spec_prd']['spec_prdnm'];
    
                //update vera jika ada produk bundling ion nano
                if(substr($prdcd,0,2) == '00') {
                    $prdcd = str_replace("00", "", $prdcd);
                }
    
                if($array['pricecode'] == '12W4' && $array['dropship'] == "DROPSHIP"){
                    $harga = (int)$array['spec_prd']['spec_CwestPrice'];
                    $hargaC= (int)$array['spec_prd']['spec_CwestPrice'];
                    $selisih= $hargaC - $harga;
    
                } elseif($array['pricecode'] == '12W4' && $array['dropship'] != "DROPSHIP") {
                    $harga = (int)$array['spec_prd']['spec_westPrice'];
                    $hargaC= (int)$array['spec_prd']['spec_CwestPrice'];
                    $selisih= $hargaC - $harga;
    
                } elseif($array['pricecode'] == '12E4' && $array['dropship'] == "DROPSHIP") {
                    $harga = (int)$array['spec_prd']['spec_CeastPrice'];
                    $hargaC= (int)$array['spec_prd']['spec_CeastPrice'];
                    $selisih= $hargaC - $harga;
    
                } else{
                    $harga = (int)$array['spec_prd']['spec_eastPrice'];
                    $hargaC= (int)$array['spec_prd']['spec_CeastPrice'];
                    $selisih= $hargaC - $harga;
                }
    
                $resSpecPrd = array(
                    "orderno" => $array['trans_id'],
                    "prdcd" => $prdcd,
                    "prdnm" => $prdnm,
                    "qty" => $qty,
                    "bvr" => $bv,
                    "dpr" => $harga,
                    "cpr" => $hargaC,
                    "pricecode" => $array['pricecode'],
                    "sentTo" => $array['sentTo'],
                    "profit_d" => $selisih,
    
                );
                $total_item += $qty;
    
                array_push($arrx['product'], $resSpecPrd);
            }
			
			//SET DATA PRODUK SALES
            foreach($array['listProduct'] as $row) {

                $prdcd = $row['id'];
                $qty = (int)$row['qty'];
                $bv = (int)$row['bv'];
                $prdnm = $row['name'];

                if(($array['pricecode'] == '12W3' || $array['pricecode'] == '12W4') && $array['dropship'] == "DROPSHIP"){
                    $harga = (int)$row['west_Cprice'];
                    $hargaC= (int)$row['west_Cprice'];
                    $selisih= $hargaC - $harga;

                } elseif(($array['pricecode'] == '12W3' || $array['pricecode'] == '12W4') && $array['dropship'] != "DROPSHIP") {
                    $harga = (int)$row['west_price'];
                    $hargaC= (int)$row['west_Cprice'];
                    $selisih= $hargaC - $harga;

                } elseif(($array['pricecode'] == '12W3' || $array['pricecode'] == '12W4') && $array['dropship'] == "DROPSHIP") {
                    $harga = (int)$row['east_Cprice'];
                    $hargaC= (int)$row['west_Cprice'];
                    $selisih= $hargaC - $harga;

                } else{
                    $harga = (int)$row['east_price'];
                    $hargaC= (int)$row['east_Cprice'];
                    $selisih= $hargaC - $harga;
                }

                $res = array(
                    "orderno" => $array['trans_id'],
                    "prdcd" => $prdcd,
                    "prdnm" => $prdnm,
                    "qty" => $qty,
                    "bvr" => $bv,
                    "dpr" => $harga,
                    "cpr" => $hargaC,
                    "pricecode" => $array['pricecode'],
                    "sentTo" => $array['sentTo'],
                    "profit_d" => $selisih,

                );
                $total_item += $qty;
                array_push($arrx['product'], $res);

            }

            $arrx['total_weight'] = number_format($total_weight, 2);
            $arrx['total_item'] = $total_item;
			
			return $arrx;
			//}
		}	

        function insertProduct($array) {

//            echo "<pre>";
//            print_r($array['listProduct']);
//            echo "</pre>";
//
//            echo "<pre>";
//            print_r($array);
//            echo "</pre>";

            //SET DATA PRODUK FREE (JIKA ADA)
            $data = $array['dataForm'];
            $total_weight = $array['total_sales_weight'];
            $total_item = 0;
            $arrx['product']= array();

            if(array_key_exists("promoAgust2018", $array) ) {
                if($array['promoAgust2018'] == "1" && array_key_exists("free_prdcd", $data)) {
                    //TAMBAHKAN BERAT TOTAL PRODUK PEMBELANJAAN DENGAN TOTAL FREE PRODUK
                    $total_weight += $data['free_weight'];
                    //echo "beratnya : ".$total_weight;
                    $jumFreePrd = count($data['free_prdcd']);
                    for($i = 0; $i < $jumFreePrd; $i++) {
                        if($array['pricecode'] == "12W3" || $array['pricecode'] == "12W4") {
                            $harga = $data['free_westPrice'][$i];
                        } else {
                            $harga = $data['free_eastPrice'][$i];
                        }

                        $arrx['product'][$i] = array(
                            "orderno" => $array['trans_id'],
                            "prdcd" => $data['free_prdcd'][$i],
                            "prdnm" => $data['free_prdnm'][$i],
                            "qty" => $data['free_qty'][$i],
                            "bvr" => $data['free_bv'][$i],
                            "dpr" => $harga,
                            "pricecode" => $array['pricecode'],
                            "sentTo" => $array['sentTo'],

                        );
                        $total_item += $data['free_qty'][$i];
                        //array_push($arrx['product'], $res);
                    }
                }
            }

            if(array_key_exists("promoNovember", $array) ) {

                if($array['promoNovember'] != null) {

                    //TAMBAHKAN BERAT TOTAL PRODUK PEMBELANJAAN DENGAN TOTAL FREE PRODUK
                    $total_weight += $data['free_weight'];
                    //echo "beratnya : ".$total_weight;
                    $jumFreePrd = count($array['promoNovember']);
//                    echo $jumFreePrd;
//                    echo "<pre>";
//                    print_r($array['promoNovember']);
//                    echo "</pre>";
                    if($jumFreePrd != 7){

                        for($i = 0; $i < $jumFreePrd; $i++) {

                            $arrx['product'][$i] = array(

                                "orderno" => $array['trans_id'],
                                "prdcd" => $array['promoNovember'][$i]['free_prdcd'],
                                "prdnm" => $array['promoNovember'][$i]['free_prdnm'],
                                "qty" => $array['promoNovember'][$i]['free_qty'],
                                "bvr" => 0,
                                "dpr" => 0,
                                "pricecode" => $array['pricecode'],
                                "sentTo" => $array['sentTo'],

                            );
                            $total_item += $data['free_qty'][$i];
                            //array_push($arrx['product'], $res);
                        }

                    } else{

                            $arrx['product'][0] = array(

                                "orderno" => $array['trans_id'],
                                "prdcd" => $array['promoNovember']['free_prdcd'],
                                "prdnm" => $array['promoNovember']['free_prdnm'],
                                "qty" => $array['promoNovember']['free_qty'],
                                "bvr" => 0,
                                "dpr" => 0,
                                "pricecode" => $array['pricecode'],
                                "sentTo" => $array['sentTo'],

                            );
                            $total_item += $data['free_qty'];
                            //array_push($arrx['product'], $res);
                    }
                }
            }

//            echo"<pre>";
//            print_r($arrx['product']);
//            echo"</pre>";

            //SET DATA PRODUK SALES
            foreach($array['listProduct'] as $row) {

                $prdcd = $row['id'];
                $qty = (int)$row['qty'];
                $bv = (int)$row['bv'];
                $prdnm = $row['name'];

                if($array['pricecode'] == '12W3' && $array['dropship'] == "DROPSHIP"){
                    $harga = (int)$row['west_Cprice'];
                    $hargaC= (int)$row['west_Cprice'];
                    $selisih= $hargaC - $harga;

                } elseif($array['pricecode'] == '12W3' && $array['dropship'] != "DROPSHIP") {
                    $harga = (int)$row['west_price'];
                    $hargaC= (int)$row['west_Cprice'];
                    $selisih= $hargaC - $harga;

                } elseif($array['pricecode'] == '12E3' && $array['dropship'] == "DROPSHIP") {
                    $harga = (int)$row['east_Cprice'];
                    $hargaC= (int)$row['east_Cprice'];
                    $selisih= $hargaC - $harga;

                } else{
                    $harga = (int)$row['east_price'];
                    $hargaC= (int)$row['east_Cprice'];
                    $selisih= $hargaC - $harga;
                }

                $res = array(
                    "orderno" => $array['trans_id'],
                    "prdcd" => $prdcd,
                    "prdnm" => $prdnm,
                    "qty" => $qty,
                    "bvr" => $bv,
                    "dpr" => $harga,
                    "cpr" => $hargaC,
                    "pricecode" => $array['pricecode'],
                    "sentTo" => $array['sentTo'],
                    "profit_d" => $selisih,

                );
                $total_item += $qty;
                array_push($arrx['product'], $res);

            }

            $arrx['total_weight'] = number_format($total_weight, 2);
            $arrx['total_item'] = $total_item;

//            echo "<pre>";
//            print_r($arrx);
//            echo "</pre>";

            return $arrx;

            //END
        }

        function checkPaymentSales($data, $arrx) {

           //echo "<pre>";
            //print_r($data);
            //echo "</pre>";
//
//            echo "<pre>";
//            print_r($arrx);
//            echo "</pre>";

            $summary = $arrx['summary'];
            $tot_pay = getTotalPayNet();
            $tot_hrs_dibayar = $summary['total_pay'] +  $summary['payShip'] - $summary['discount_shipping'];
            if(count($data) > 0) {
                $jml_voucher = isset($data['shop_vch']) ? count($data['shop_vch']) : 0;
                $pay['vch_stt'] = false;
                $pay['res'] = null;
                $pay['pay_gateway'] = true;
                $tot_nominal_vch = 0;
                //Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
                //atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar

                if($jml_voucher > 0) {
                    $pay['vch_stt'] = true;
                    $tempArr = array();
                    $tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
                    $this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $data['trans_id']);
                    $xc = 0;
                    for($i = 0; $i < $jml_voucher; $i++) {
                        $xc++;
                        $tot_nominal_vch += $data['shop_vch_nominal'][$i];
                        //$xc = $i + 1;
                        $tbl .= "<tr>";
                        $tbl .= "<td align=right>".$xc."&nbsp;</td>";
                        $tbl .= "<td align=center>".$data['shop_vch'][$i]."</td>";
                        $tbl .= "<td align=right>Rp.".$data['shop_vch_nominal'][$i]."&nbsp;</td>";
                        $tbl .= "</tr>";
                        $insAr = array(
                            "seqno" => $xc,
                            "orderno" => $summary['orderno'],
                            "paytype" => "vch",
                            "docno" => $data['shop_vch'][$i],
                            "payamt" => $data['shop_vch_nominal'][$i],
                            "bank_code_payment" => 19,
                            "charge_admin" => 0
                        );
                        /*echo "<pre>";
                        print_r($insAr);
                        echo "</pre>";*/
                        $pay['list_payment'][$i] = $insAr;
                        //$this->cartshopService->insertVoucherList($insAr);

                    }
                    $tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp.".$tot_nominal_vch."&nbsp;</td></tr>";
                    $tbl .= "</table>";

                    //Jika total nominal voucher lebih besar atau sama dengan
                    //nominal belanja yang harus dibayar
                    if($tot_nominal_vch >= $tot_hrs_dibayar) {
                        //status payment gateway = false, data Langsung insert ke database tanpa pembayaran
                        $pay['pay_gateway'] = false;

                        //$pay['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
                        $pay['tot_hrs_dibayar'] = $tot_hrs_dibayar;

                    } else {
                        //Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO

                        //$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

                        //Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
                        $sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
                        $seqno = $xc + 1;
                        $insAr = array(
                            "seqno" => $seqno,
                            "orderno" => $summary['orderno'],
                            "paytype" => "sgo",
                            "docno" => "",
                            "payamt" => $sisaByr,
                            "bank_code_payment" => $data['bankid'],
                            "charge_admin" => $summary['payAdm']
                        );
                        //$this->cartshopService->insertVoucherList($insAr);
                        array_push($pay['list_payment'], $insAr);
                        //$pay['list_payment'][$i] = $insAr;
                        $pay['tot_hrs_dibayar'] = $sisaByr;
                    }

                    $pay['tot_nilai_voucher'] = $tot_nominal_vch;
                    $pay['tot_pay'] = $tot_pay;
                    $pay['listVch'] = $tbl;
                } else {
                    $pay['list_payment'] = array();
                    $insAr = array(
                        "seqno" => 1,
                        "orderno" => $summary['orderno'],
                        "paytype" => "sgo",
                        "docno" => "",
                        "payamt" => $tot_hrs_dibayar,
                        "bank_code_payment" => $data['bankid'],
                        "charge_admin" => $summary['payAdm']
                    );
                    //$this->cartshopService->insertVoucherList($insAr);
                    array_push($pay['list_payment'], $insAr);
                    $pay['tot_pay'] = $tot_pay;
                    $pay['tot_nilai_voucher'] = $tot_nominal_vch;
                }
            }

//            echo "<pre>";
//            print_r($pay);
//            echo "</pre>";

            return $pay;
        }

        function insertTempTableSgo($arrx) {
//            echo "<pre>";
//            print_r($arrx['product']);
//            echo "</pre>";

            $res = jsonFalseResponse("insert gagal..");
            if(!array_key_exists("product", $arrx) || $arrx['product'] == null) {
                return jsonFalseResponse("Array Product tidak ada..");
            }

            if(!array_key_exists("summary", $arrx) || $arrx['summary'] == null) {
                return jsonFalseResponse("Array Summary tidak ada..");
            }

            if(!array_key_exists("pay", $arrx) || $arrx['pay'] == null) {
                return jsonFalseResponse("Array Summary tidak ada..");
            }

            $dbqryx = $this->load->database('db_ecommerce', true);
            $dbqryx->trans_start();
            /*--------------------------------------
            INSERT TABLE ECOMM_TRANS_DET_PRD_SGO
            ----------------------------------------*/
//            echo"<pre>";
//            print_r($arrx['product']);
//            echo"</pre>";
            foreach($arrx['product'] as $dataPrd) {
                /*
                $prdSql = $this->db->set($dataPrd)->get_compiled_insert('ecomm_trans_det_prd_sgo');
                echo $prdSql;
                echo "<br />";
                */
                $dbqryx->insert('ecomm_trans_det_prd_sgo', $dataPrd);
            }

            /*--------------------------------------
            INSERT TABLE ECOMM_TRANS_HDR_SGO
            ----------------------------------------*/
            /*
            echo "<br />";
            $hdrSql = $this->db->set($arrx['summary'])->get_compiled_insert('ecomm_trans_hdr_sgo');
            echo $hdrSql;
            echo "<br />";
            */
            
            //print_r($arrx['summary']);
            $dbqryx->insert('ecomm_trans_hdr_sgo', $arrx['summary']);
			//echo $dbqryx->last_query();
            /*--------------------------------------
            INSERT TABLE ECOMM_TRANS_PAYDET_SGO
            ----------------------------------------*/
            //echo "<br />";
            foreach($arrx['pay']['list_payment'] as $pay) {
                /*
                $paySql = $this->db->set($pay)->get_compiled_insert('ecomm_trans_paydet_sgo');
                echo $paySql;
                echo "<br />";
                */
                $dbqryx->insert('ecomm_trans_paydet_sgo', $pay);
            }

            /*--------------------------------------
            INSERT TABLE ECOMM_TRANS_SHIPPADDR_SGO
            ----------------------------------------*/
            if($arrx['shipping'] != null) {
                /*
                echo "<br />";
                $shipSql = $this->db->set($arrx['shipping'])->get_compiled_insert('ecomm_trans_shipaddr_sgo');
                echo $shipSql;
                echo "<br />";
                */
                $dbqryx->insert('ecomm_trans_shipaddr_sgo', $arrx['shipping']);
            }
			
			/*--------------------------------------
            INSERT TABLE ECOMM_TRANS_SHIPPADDR_SGO
            ----------------------------------------*/
            if($arrx['new_member_reg'] != null) {
                /*
                echo "<br />";
                $shipSql = $this->db->set($arrx['shipping'])->get_compiled_insert('ecomm_trans_shipaddr_sgo');
                echo $shipSql;
                echo "<br />";
                */
                $dbqryx->insert('ecomm_memb_ok_sgo', $arrx['new_member_reg']);
            }

            //END TRANSACTION
            $dbqryx->trans_complete();

            //CHECK IF TRANSACTION PROCESS IS COMMITED
            if ($dbqryx->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $dbqryx->trans_rollback();
                return false;
            } else {
                //if everything went right, insert the data to the database
                $dbqryx->trans_commit();
                return true;
            }

            //return $res;
        }

		//$route['cart/register/newmember'] = 'webshop/Promo_nov/regisnewmember';
        function regisnewmember(){

            if ($this->_checkSessionStoreUser()) {
                $data = $this->input->post(NULL, TRUE);
				//print_r($data);
				$eerr = $this->updateCartBeforeProceed();
				$dt['promoApr'] = $data['promoApr'];
            	$dt['promoGamat'] = $data['promoGamat'];
            if($dt['promoApr'] == "1" || $dt['promoGamat'] == "1") {
					if ($this->cartshopService->checkShippingDataBaru($data)) {
						$dt['cart'] = $this->cart->contents();
						$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
						if ($data != null) {
							$this->load->model("webshop/Member_model", "member_model");
							//$dt['listStockist'] = $this->member_model->getListStockist();
							$dt['listStockist'] = $this->member_model->getListStockistDion();
							$prdx = set_list_array_to_stringCart($dt['cart'], "id");
							//edited, disabled by Dion @13/10/2015
							//$dt = $this->cartshopService->getProductShop($prdx, $this->session->userdata('pricecode'));
							//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
							$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
							//$ship = $this->cartshopService->setShippingInfo();
							$ship = $this->cartshopService->setShippingInfoBaru();
							$dt['prodCat'] = $this->cartshopService->getProductCategory();
							$dt['bank'] = $this->Member_model->getListAffiliateBank();
                            //$dt['free_memb'] = $data['free_memb'];
                            $dt['free_memb'] = "1";
							//ubah dion 15/11/2018
							//$this->setTempWebShopNonMember($this->folderView . 'RegisterMembership', $dt);
                            $this->setTempWebShop('webshop/promo_nov/RegisterMembership', $dt);
						}
					}
				} else {
					//alert("Minimum total belanja harus 700.000..");
					//echo "Total Belanja : ".$total_belanja;
					//echo "\nmin free memb : ".$data['min_dp'];
					redirect('cart/list');
				}	
            }
        }

        function updateCartBeforeProceed(){

            if ($this->_checkSessionStoreUser()) {
                $arr = jsonFalseResponse(NULL, "Update Cart gagal..!");
                $data = $this->input->post(NULL, TRUE);
                $upd = $this->cartshopService->updateCart($data);
                $this->session->set_userdata('pricecode', $data['pricecode']);
                if ($upd) {
                    $arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
                }
                //echo json_encode($arr);
                return $arr;
            }
        }

		//$route['cart/checkout'] = 'webshop/Promo_nov/CheckoutCart';	
        function CheckoutCart()
        {
			
            if ($this->_checkSessionStoreUser()) {
                $data = $this->input->post(NULL, TRUE);
				/*
                echo"<pre>";
                print_r($data);
                echo"</pre>";
				*/
				$this->session->set_userdata('free_memb', $data['free_memb']);
                $personal_info = $this->session->userdata('personal_info');
				/*if($personal_info['idmemberx'] == null || $personal_info['idmemberx'] == "") {
					echo "<script>alert('Data yang diisi kurang lengkap')</script>";
					redirect('cart/list');
				}*/
                //$data['delivery']= $personal_info ['delivery'];
                //print_r($personal_info);
					$bnsperiod = $personal_info ['bnsperiod'];
					
                    $dt['cart'] = $this->cart->contents();
                    $dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
                    if ($data != null) {
						if($data['free_memb'] == 0 && $data['promoGamat'] == 0) {
							$eerr = $this->updateCartBeforeProceed();
							$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
							//$ship = $this->cartshopService->setShippingInfo();
							if(getUserID() == "IDSPAAA66834") {
							    //echo "Masuk disini...";
								$ship = setShippingInfoBaru();
							} else {
								$ship = $this->cartshopService->setShippingInfoBaru();
							}

						}
						
						$personal_info = $this->session->userdata('personal_info');
                        $pricecode = $this->session->userdata('pricecode');
                        $shipping_jne_info = $this->session->userdata('shipping_jne_info');
						
						if($personal_info['delivery'] == "2" && ($shipping_jne_info['price'] == null || $shipping_jne_info['price'] == "")) {
							
							 echo "<script>alert('Harga untuk area yang dipilih tidak tersedia')</script>";
							 redirect('cart/list');
							 //print_r($shipping_jne_info);
							
						} else {
						
							//$prdx = set_list_array_to_stringCart($dt['cart'], "id");

							//$reg = $this->cartshopService->sessionRegPersonalInfoBaru($personal_info);
							//$ship = $this->cartshopService->setShippingInfo();
							//$ship = $this->cartshopService->setShippingInfoBaru();
							$dt['prodCat'] = $this->cartshopService->getProductCategory();

                            $harga = getTotalPayNetBaru();
                            $totPay = $harga['total_pay'];

							if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
								$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
							} else {
								$dt['listBank'] = $this->cartshopService->getBank();
                                $dt['listCC'] = $this->cartshopService->getCCBank($totPay);//tambahan Vera
							}
							//production
							//$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
							//development 0df5835ee198d49944c372ead860c241
							$dt['key'] = '0df5835ee198d49944c372ead860c241';


							//$totDiscount = $ship['price'];
							if (getUserID() == "IDSPAAA96407") {
								$dt['biaya'] = getTotalPayNetAndShipCostTerbaru();
							}


							//GET SALDO VA
							$this->load->model("backend/be_hifi_model", "hifi");
							$resSaldo = $this->hifi->getSaldoVa(getUserID());
							if ($resSaldo == null) {
								$dt['saldo_va'] = "0";
							} else {
								$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
							}

							$dt['payID'] = "EC" . randomNumber(8);

							
							$sender_address = $this->session->userdata('sender_address');
							$dest_address = $this->session->userdata('destination_address');

							$jne_branch = $this->session->userdata('jne_branch');
							$promoDiscShipping = $this->session->userdata('promo');
							$bns = substr($personal_info['bnsperiod'], 3, 7);

							//SET HARGA
							$harga = getTotalPayNetBaru();
							$totPay = $harga['total_pay'];
							$selisihHargaCust = 0;
							//END

							$totalbv = $this->cart->total_bv();
							$freeship = "0";

							$biaya = getTotalPayNetAndShipCost2();
							$gross_amount = $biaya['total_pay'];
							$freeship = $biaya['freeship'];
							$discount = $biaya['tot_discount'];

							$shipper_telhp = getUserPhone();
							$admin_cost = 0;
							$nmstk = explode(" - ", $personal_info['nama_stockist']);
							$nmstkSS = $nmstk[0]." - ".$nmstk[1];
							$produk = $this->cart->contents();

							$cust_memb = $this->session->userdata("non_member_info");
							if(isset($cust_memb)) {
								$is_login = "2";
								$tot_paynet = getTotalPayNet();
							} else {
								$memb = $this->session->userdata("store_info");
								if(isset($memb)) {
									$is_login = "1";
									$tot_paynet = 0;
								}
							}

							if($personal_info['delivery'] == "2") {
								$arrx = array(
									"origin_name" => $shipping_jne_info['origin_name'],
									"destination_name" => $shipping_jne_info['destination_name'],
									"service_code" => $shipping_jne_info['service_code'],
									"service_display" => $shipping_jne_info['service_display'],
									//"price" => $data['total_ship_price'],
									"price" => $shipping_jne_info['price'],
									"etd_from" => $shipping_jne_info['etd_from'],
									"etd_thru" => $shipping_jne_info['etd_thru'],
									"times" => $shipping_jne_info['times'],
									//"ship_discount" => $data['total_diskon_ship'],
									"ship_discount" => 0,
								);
								//print_r($arrx);
								$this->session->unset_userdata('shipping_jne_info');
								$this->session->set_userdata('shipping_jne_info', $arrx);
								$shipping_jne_info = $this->session->userdata('shipping_jne_info');
							}

							//CHECK APAKAH ADA PROMO AGUSTUS YG AKTIF
							$promo_agust = "0";
							if(array_key_exists("promo_agust", $data)) {
								$promo_agust = $data['promo_agust'];
							}

							//CHECK APAKAH MEMBER MEMILIH PEMBELANJAAN DROPSHIPPER
							$dropship = $personal_info['id_lp'];
							if($personal_info['id_lp'] == "DROPSHIP") {
								$selisihHargaCust = getTotalPayNet() - $totPay;
								$totPay = getTotalPayNet();
							}


							//regis new member
							if($data['free_memb'] == "1") {

								$isinewmember = array(

									'noktp' => $data['idno'],
									'membername' => $data['membername'],
									'tgllhr' => $data['thnlhr'].'-'.$data['blnlhr'].'-'.$data['tgllhr'],
									'sex' => $data['sex'],
									'addr1' => $data['addr1'],
									'tel_hp' => $data['tel_hp'],
									'memb_email' => $data['memb_email'],
									'idsponsor' => $data['idsponsor'],
									//'sponsorname' => $data['sponsorname'],
									'idrecruiter' => $data['idrecruiter'],
									//'recruitername' => $data['recruitername'],
									'bank' => $data['bank'],
									'norek' => $data['norek']

								);
								
								//$isinewmember['st_state'] = "JK";
								if (array_key_exists('st_state', $data)) {
									if($data['st_state'] != null && $data['st_state'] != "") {
										$isinewmember['st_state'] = $data['st_state'];
									} else {
										$isinewmember['st_state'] = "JK";
									}
								} else {
									$isinewmember['st_state'] = "JK";
								}
								
								//$isinewmember['st_loccd'] = "BID06";
								if (array_key_exists('st_loccd', $data)) {
									if($data['st_loccd'] != null && $data['st_loccd'] != "") {
										$isinewmember['st_loccd'] = $data['st_loccd'];
									} else {
										$isinewmember['st_loccd'] = "BID06";
									}
								} else {
									$isinewmember['st_loccd'] = "BID06";
								}
								
								//$isinewmember['st_fullnm'] = "PT. K-LINK NUSANTARA";
								if (array_key_exists('st_fullnm', $data)) {
									if($data['st_loccd'] != null && $data['st_loccd'] != "") {
										$isinewmember['st_fullnm'] = $data['st_fullnm'];
									} else {
										$isinewmember['st_fullnm'] = "PT. K-LINK NUSANTARA";
									}
								} else {
									$isinewmember['st_fullnm'] = "PT. K-LINK NUSANTARA";
								}

								//$insmemb = $this->m_nonmember_trx->insertEcommMembSgo($isinewmember);

								$this->session->unset_userdata('new_member_reg');
								$this->session->set_userdata('new_member_reg', $isinewmember);

								//ADD DION 15/11/2018
								$this->session->unset_userdata('new_member_reg_add');
								$sponsor = array(
									'sponsorname' => $data['sponsorname'],
									'recruitername' => $data['recruitername']
								);
								$this->session->set_userdata('new_member_reg_add', $sponsor);
								
							}else{
								$this->session->set_userdata('new_member_reg_add', NULL);
								$this->session->set_userdata('new_member_reg', NULL);

							}

							//SET PARAM UTK HADIAH
							$this->session->unset_userdata('promo_free_prd');
							//$count = count($data['free_prdcd']);
							//echo $count;
                            
                            $dt['promoApr'] = $data['promoApr'];
                            $dt['promoGamat'] = $data['promoGamat'];
                            if($dt['promoGamat'] == "1") {
                                if($data['spec_prdcd'] != null && $data['spec_prdcd'] != "") {
                                    $arrNovX['spec_prdcd'] = $data['spec_prdcd'];
                                    $arrNovX['spec_prdnm'] = $data['spec_prdnm'];
                                    $arrNovX['spec_qty'] = $data['spec_qty'];
                                    $arrNovX['spec_bv'] = $data['spec_bv'];
                                    $arrNovX['spec_westPrice'] = $data['spec_westPrice'];
                                    $arrNovX['spec_eastPrice'] = $data['spec_eastPrice'];
                                    $arrNovX['spec_CwestPrice'] = $data['spec_CwestPrice'];
                                    $arrNovX['spec_CeastPrice'] = $data['spec_CeastPrice'];
                                    $arrNovX['spec_weight'] = $data['spec_weight'];
                                    $this->session->set_userdata('spec_free_prd', $arrNovX);    
                                }        
                                
                            }
							if($data['free_memb'] == "1"){

                                if(array_key_exists('free_prdcd', $data)) {
                                    $arrNov['free_prdcd'] = $data['free_prdcd'];
                                    $arrNov['free_prdnm'] = $data['free_prdnm'];
                                    $arrNov['free_qty'] = $data['free_qty'];
                                    $arrNov['free_bv'] = $data['free_bv'];
                                    $arrNov['free_westPrice'] = $data['free_westPrice'];
                                    $arrNov['free_eastPrice'] = $data['free_eastPrice'];
                                    $arrNov['free_weight'] = $data['free_weight'];

                                    $this->session->set_userdata('promo_free_prd', $arrNov);
                                } else {
                                    $this->session->set_userdata('promo_free_prd', NULL);
                                }

							}else{

								$arrNov = null;
								$this->session->set_userdata('promo_free_prd', NULL);
							}

							/*
							if($arrNov != null){

								$prdParam = array(
									"trans_id" => $data['trans_id'],
									"total_sales_weight" => $this->cart->total_weight(),
									"dropship" =>	$dropship,
									"listProduct" => $produk,
									"pricecode" => $pricecode,
									"dataForm" => $data,
									"sentTo" => $personal_info['delivery'],
									"promoAgust2018" => $promo_agust,
									"promoNovember" => $arrNov
								);

							}else{

								$prdParam = array(
									"trans_id" => $data['trans_id'],
									"total_sales_weight" => $this->cart->total_weight(),
									"dropship" =>	$dropship,
									"listProduct" => $produk,
									"pricecode" => $pricecode,
									"dataForm" => $data,
									"sentTo" => $personal_info['delivery'],
									"promoAgust2018" => $promo_agust
								);
							}

							$insPrdArr = $this->insertProduct($prdParam);
							//print_t($insPrdArr);
							$arrx['product'] = $insPrdArr['product'];
							*/

	//            echo "<pre>";
	//            print_r($insPrdArr['product']);
	//            echo "</pre>";



	//            echo "<pre>";
	//            print_r($arrx['keySgo']);
	//            echo "</pre>";

							//ASLI DION ubah 15/11/2018
							//if($personal_info['delivery'] == "2" && $ship != 1 && $ship == null) {
							if($personal_info['delivery'] == "2" && $shipping_jne_info == null) {
								echo "<script>
	//						window.location.href='list';
							alert('Maaf area yang anda pilih tidak ada pricelist tes2 nya..');
							</script>";
							} else {
                                $dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
                                $this->setTemplateSGO($this->folderView . 'payment_sgo_vch2', $dt);
                                /*if(getUserID() == "IDSPAAA66834") {
                                    $dt['previewUrl'] = "cart/checkout/sgo/pay/previewTes";
                                    $this->setTemplateSGO($this->folderView . 'payment_sgo_vch2', $dt);
                                }else{
                                    $dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
                                    $this->setTemplateSGO($this->folderView . 'payment_sgo_vch2', $dt);
                                }*/
								//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
								//$dt['previewUrl'] = "cart/checkout/sgo/pay/preview2";
								//$this->setTemplateSGO($this->folderView . 'payment_sgo_vch2', $dt);
								//$this->setTemplateSGO('webshop/payment_sgo',$dt);
							}
						}
					}
            }
        }

		//$route['cart/checkout/sgo/pay/previewTes'] = 'webshop/promo_nov/paySGOPreview2';
		//di set utk prod
		//$route['cart/checkout/sgo/pay/preview2'] = 'webshop/promo_nov/paySGOPreview2';
        public function paySGOPreview2()
        {
            $data = $this->input->post(NULL, TRUE);
            //echo "<pre>";
            //print_r($data);
            //echo "</pre>";
			$free_memb = $this->session->userdata('free_memb');
			if($free_memb == "0" || $free_memb == "" || $free_memb == null) {
				$data['trans_id'] = "EC" . randomNumber(8);
				$tipeTrx = "EC";
			} else {
				$data['trans_id'] = "EM" . randomNumber(8);
				$tipeTrx = "EM";
			}

            $personal_info = $this->session->userdata('personal_info');
			
			$pricecode = $this->session->userdata('pricecode');
			if($personal_info['delivery'] == "2" && $personal_info['shipper'] == "4") {
				$pricecode = "12W4";
			}
            
            $shipping_jne_info = $this->session->userdata('shipping_jne_info');
            $sender_address = $this->session->userdata('sender_address');
            $dest_address = $this->session->userdata('destination_address');

            $jne_branch = $this->session->userdata('jne_branch');
			$new_member_reg = $this->session->userdata('new_member_reg');
            $promoDiscShipping = $this->session->userdata('promo');
            
            //$bns = substr($personal_info['bnsperiod'], 3, 7);
			
			//edit hilal @2019-02-06 01:45 START
			$bnsX = date('d/m/Y',strtotime($personal_info['bnsperiod']));
			$bns = substr($bnsX, 3, 7);
			//echo "personal_info['bnsperiod'] = ".$personal_info['bnsperiod'];
			//echo "bonus = ".$bns;
			//edit hilal @2019-02-06 01:45 END
			
            //SET HARGA
            //UPDATE BY DION 28 MARET 2019
            $spec_prd = $this->session->userdata('spec_free_prd');
            $harga = getTotalPayNetBaru();
            $specPrdBv = 0;
            $specPrdHargaDist = 0;
            $specPrdHargaCust = 0;
            if($spec_prd != null) {
                if(array_key_exists('spec_prdcd', $spec_prd)) {
                    $specPrdBv = $spec_prd['spec_qty'] * $spec_prd['spec_bv'];    
                    if($pricecode == "12W4") {
                        $specPrdHargaDist = $spec_prd['spec_qty'] * $spec_prd['spec_westPrice']; 
                        $specPrdHargaCust = $spec_prd['spec_qty'] * $spec_prd['spec_CwestPrice']; 
                    } else {
                        $specPrdHargaDist = $spec_prd['spec_qty'] * $spec_prd['spec_eastPrice'];  
                        $specPrdHargaCust = $spec_prd['spec_qty'] * $spec_prd['spec_CeastPrice']; 
                    }
                }
            }
            $totPay = $harga['total_pay'] + $specPrdHargaDist;

            $tot_pay_dp = $harga['total_pay'] + $specPrdHargaDist;
            $tot_pay_cp= $harga['totalCPay'] + $specPrdHargaCust;
            $selisihHargaCust = $tot_pay_cp - $tot_pay_dp;
            //END

            //Jika Transfer Manual Vera
            $bankid = $data['bankid'];
            if($bankid == "29" || $bankid == "30" || $bankid == "31"){
                //call API Kode Unik

                //$random=rand(001,2000);
                $random= $data['kode_unik'];
                $nom= ($totPay * 1);
                //$totPay = $nom;
                //$totPay = $totPay ;
                $arrx['kode_unik']= $random;
                $payAdm= $data['charge_admin'] + $random;
                $nompay= $nom + $payAdm + $shipping_jne_info['price'];

                $arrx['norekbank']=$data['bank_acc'];
                $arrx['namapemilik']=$data['bank_pemilik'];
                $arrx['nomor_ref']=$data['kode_ref'];
                $kodepay=$data['kode_pay'];
                $koderef=$data['kode_ref'];

                //get tabel rekening bank
                /*$dts= $this->M_location->getRek($bankid);
                if($dts != null){
                    foreach($dts as $row){
                        $arrx['norekbank']=$row->norek;
                        $arrx['namapemilik']=$row->namapemilik;
                    }
                }*/
            }else{
                $random=0;
                $totPay = $totPay;
                $arrx['kode_unik']= 0;
                $kodepay="";
                $koderef="";
                $payAdm= $data['charge_admin'];
                $nompay= 0;
            }

            $totalbv = $this->cart->total_bv() + $specPrdBv;;
            $freeship = "0";

            $biaya = getTotalPayNetAndShipCost2();
            $gross_amount = $biaya['total_pay'];
            $freeship = $biaya['freeship'];
            $discount = $biaya['tot_discount'];

            $shipper_telhp = getUserPhone();
            $admin_cost = 0;
            $nmstk = explode(" - ", $personal_info['nama_stockist']);
            $nmstkSS = $nmstk[0] . " - " . $nmstk[1];
            $produk = $this->cart->contents();

            $cust_memb = $this->session->userdata("non_member_info");
            if (isset($cust_memb)) {
                $is_login = "2";
                $tot_paynet = getTotalPayNet();
            } else {
                $memb = $this->session->userdata("store_info");
                if (isset($memb)) {
                    $is_login = "1";
                    $tot_paynet = 0;
                }
            }
			
			$latdest = $personal_info['nama_kota'];
			$longdest = $personal_info['nama_provinsi']; 

            if ($personal_info['delivery'] == "2") {

                if($personal_info ['shipper'] == '4'){
				    /*if($tipeTrx == "EM") {
						$latdest = $personal_info['nama_provinsi'];
						$longdest = $personal_info['nama_kota'];
					} else {
						$latdest = $personal_info['nama_provinsi'];
						$longdest = $personal_info['nama_kota'];
					}*/
				 
				
                    $arrx = array(
                        "origin_name" => $shipping_jne_info['origin_name'],
                        "destination_name" => $shipping_jne_info['destination_name'],
                        "service_code" => $shipping_jne_info['service_code'],
                        "service_display" => $shipping_jne_info['service_display'],
                        "price" => $data['total_ship_price'],
                        "etd_from" => $shipping_jne_info['etd_from'],
                        "etd_thru" => $shipping_jne_info['etd_thru'],
                        "times" => $shipping_jne_info['times'],
                        "ship_discount" => $data['total_diskon_ship'],
                        "lat_dest" => $latdest, //tambahan Vera
                        "long_dest" => $longdest
                    );
                }else{
                    $arrx = array(
                        "origin_name" => $shipping_jne_info['origin_name'],
                        "destination_name" => $shipping_jne_info['destination_name'],
                        "service_code" => $shipping_jne_info['service_code'],
                        "service_display" => $shipping_jne_info['service_display'],
                        "price" => $data['total_ship_price'],
                        "etd_from" => $shipping_jne_info['etd_from'],
                        "etd_thru" => $shipping_jne_info['etd_thru'],
                        "times" => $shipping_jne_info['times'],
                        "ship_discount" => $data['total_diskon_ship']
                    );
                }
                //print_r($arrx);
                $this->session->unset_userdata('shipping_jne_info');
                $this->session->set_userdata('shipping_jne_info', $arrx);
                $shipping_jne_info = $this->session->userdata('shipping_jne_info');
            }

            //CHECK APAKAH ADA PROMO AGUSTUS YG AKTIF
            $promo_agust = "0";
            if (array_key_exists("promo_agust", $data)) {
                $promo_agust = $data['promo_agust'];
            }

            //CHECK APAKAH MEMBER MEMILIH PEMBELANJAAN DROPSHIPPER
            $dropship = $personal_info['id_lp'];
            if ($personal_info['id_lp'] == "DROPSHIP") {
                $selisihHargaCust = getTotalPayNet() - $totPay;
                $totPay = getTotalPayNet();
            }

            //SET PARAM UTK HADIAH
            //$this->session->unset_userdata('promo_free_prd');
            //$count = count($data['free_prdcd']);
            //echo $count;

//		echo "<pre>";
//		print_r($prdParam);
//		echo "</pre>";

			//SET PARAM UNTUK

            $prdParam = array(
                    "trans_id" => $data['trans_id'],
                    "total_sales_weight" => $this->cart->total_weight(),
                    "dropship" =>	$dropship,
                    "listProduct" => $produk,
                    "pricecode" => $pricecode,
                    "dataForm" => $data,
                    "sentTo" => $personal_info['delivery'],
                    "free_produk" => $this->session->userdata("promo_free_prd"),
                    "spec_prd" => $spec_prd
                    //"promoNovember" => $arrNov
                );
			
            $insPrdArr = $this->insertProductBaru($prdParam);
			$arrx['product'] = $insPrdArr['product'];
			
			$arrx['new_member_reg'] = null;
			if($new_member_reg != null ) {
			    $exp = explode(" - ", $personal_info['nama_stockist']);
				$pwdExp = explode("-", $new_member_reg['tgllhr']);
				$thn = substr($pwdExp[0], 2, 2);
				$password = $pwdExp[2].$pwdExp[1].$thn;
				
				$arrMemb['sponsorid'] = $new_member_reg['idsponsor'];
				$arrMemb['memberid'] = $data['trans_id'];
				$arrMemb['trx_no'] = $data['trans_id'];
				$arrMemb['userlogin'] = getUserID();
				$arrMemb['password'] = $password;
				$arrMemb['membername'] = $new_member_reg['membername'];
				$arrMemb['idno'] = $new_member_reg['noktp'];
				$arrMemb['tel_hp'] = $new_member_reg['tel_hp'];
				$arrMemb['addr1'] = $new_member_reg['addr1'];
				$arrMemb['birthdt'] = $new_member_reg['tgllhr'];
				$arrMemb['email'] = $new_member_reg['memb_email'];
				$arrMemb['sex'] = $new_member_reg['sex'];
				//$arrMemb['stk_code'] = $personal_info['stockist'];
				$arrMemb['stk_code'] = $new_member_reg['st_loccd'];
				$arrMemb['bankid'] = $new_member_reg['bank'];
				$arrMemb['acc_no'] = $new_member_reg['norek'];
				//$arrMemb['bank_acc_name'] = "";
				$arrMemb['id_landingpage'] = "sales_plus_reg";
				$arrMemb['flag_voucher'] = "0";
				$arrMemb['voucher_no'] = "";
				$arrMemb['voucher_key'] = "";
				//$arrMemb['state'] = $exp[2];
				$arrMemb['state'] = $new_member_reg['st_state'];
				$arrMemb['recruiterid'] = $new_member_reg['idrecruiter'];
				$arrMemb['prdcd'] = "X";
				
				$arrx['new_member_reg'] = $arrMemb;
			}
			
			/*echo "<pre>";	
			print_r($insPrdArr);	
			echo "</pre>";*/
            //CHECK APAKAH BARANG DIKIRIM / DI AMBIL DI STOKIS
            $arrShip = null;
            if ($personal_info['delivery'] == "2") {
                // $stk = explode(" - ", $personal_info['stockist']);
                //$nama_stk = trim($stk[1]);

                $ssx = explode(" - ", $personal_info['nama_stockist']);
                $nama_stk2 = str_replace("-", "", $ssx[1]);
                $nama_stk = substr($nama_stk2, 0, 20);

                if($bankid == "29" || $bankid == "30" || $bankid == "31"){
                    //call API Kode Unik

                    $random= $data['kode_unik'];
                    $nom= ($totPay * 1);
                    $totPay = $nom;
                    $arrx['kode_unik']= $random;
                    $payAdm= $data['charge_admin'] + $random;
                    $nompay= $nom + $payAdm + $shipping_jne_info['price'];

                    $arrx['norekbank']=$data['bank_acc'];
                    $arrx['namapemilik']=$data['bank_pemilik'];
                    $arrx['nomor_ref']=$data['kode_ref'];
                    $kodepay=$data['kode_pay'];
                    $koderef=$data['kode_ref'];
                    //get tabel rekening bank
                    /* $dts= $this->M_location->getRek($bankid);
                     if($dts != null){
                         foreach($dts as $row){
                             $arrx['norekbank']=$row->norek;
                             $arrx['namapemilik']=$row->namapemilik;
                         }
                     }*/
                }else{
                    $random=0;
                    $totPay = $totPay;
                    $payAdm= $data['charge_admin'];
                    $arrx['kode_unik']= 0;
                    $kodepay="";
                    $koderef="";
                    $nompay= 0;
                }

                //print_r($ssx);

                if($personal_info['shipper'] == '4'){
                    $arrShip = array(
                        "orderno" => $data['trans_id'],
                        "idstk" => $personal_info['stockist'],
                        "prov_code" => $personal_info['provinsi'],
                        "kab_code" => $personal_info['kota'],
                        "kec_code" => $personal_info['kecamatan'],
                        "province_name" => $personal_info['nama_provinsi'],
                        "kabupaten_name" => $personal_info['nama_kota'],
                        "stockist_name" => $nama_stk,

                        "receiver_name" => $personal_info['nama_penerima'],
                        "addr1" => cleanAddress($personal_info['alamat']),
                        "email" => $personal_info['email'],
                        "tel_hp1" => $personal_info['notlp'],
                        "tel_hp2" => getUserPhone(),
                        "shipper_telhp" => getUserPhone(),

                        "service_type_id" => $shipping_jne_info['service_code'],
                        "service_type_name" => $shipping_jne_info['service_display'],
                        "total_item" => $insPrdArr['total_item'],
                        "total_weight" => $insPrdArr['total_weight'],
                        "sender_address" => $sender_address,
                        "dest_address" => $dest_address,
                        "jne_branch" => $jne_branch,
                        "cargo_id" => $personal_info['shipper'],
                        "total_pay_net" => $totPay,
                        "lat_dest" => $shipping_jne_info['lat_dest'], //tambahan Vera
                        "long_dest" => $shipping_jne_info['long_dest'],
                        "whcd" => $personal_info['whcd'],
                        "whnm" => $personal_info['whnm']
                    );
                }else{
                    $arrShip = array(
                        "orderno" => $data['trans_id'],
                        "idstk" => $personal_info['stockist'],
                        "prov_code" => $personal_info['provinsi'],
                        "kab_code" => $personal_info['kota'],
                        "kec_code" => $personal_info['kecamatan'],
                        "province_name" => $personal_info['nama_provinsi'],
                        "kabupaten_name" => $personal_info['nama_kota'],
                        "stockist_name" => $nama_stk,

                        "receiver_name" => $personal_info['nama_penerima'],
                        "addr1" => cleanAddress($personal_info['alamat']),
                        "email" => $personal_info['email'],
                        "tel_hp1" => $personal_info['notlp'],
                        "tel_hp2" => getUserPhone(),
                        "shipper_telhp" => getUserPhone(),

                        "service_type_id" => $shipping_jne_info['service_code'],
                        "service_type_name" => $shipping_jne_info['service_display'],
                        "total_item" => $insPrdArr['total_item'],
                        "total_weight" => $insPrdArr['total_weight'],
                        "sender_address" => $sender_address,
                        "dest_address" => $dest_address,
                        "jne_branch" => $jne_branch,
                        "cargo_id" => $personal_info['shipper'],
                        "total_pay_net" => $totPay
                    );
                }

            }

            $arrx['shipping'] = $arrShip;

            //echo "Total Weight semua : ".$total_weight;
            //echo "<br />nTotal Belanja : ".$totPay;
            //echo "<br />Selisih harga CUST/DIST : ".$selisihHargaCust;
            if (array_key_exists("bankid", $data)) {
                $bankid = $data['bankid'];
                $charge_connectivity = $data['charge_connectivity'];
                $charge_admin = (int)$data['charge_admin'];
            } else {
                $bankid = "19";
                $charge_connectivity = "";
                $charge_admin = 0;
            }
            //echo "personal_info = $bns";

            //tambahan Vera
            $today=date("Y-m-d H:i:s");
            $tgl_exp = date('Y-m-d H:i:s', strtotime('+4 hour', strtotime($today))); //penambahan 4jam
            /*if($bankid == "29" || $bankid == "30" || $bankid == "31"){
                $payAdm= $data['charge_admin'] + $random;
            }else{
                $payAdm= $data['charge_admin'];
            }*/

            $arrx['summary'] = array(
                "orderno" => $data['trans_id'],
                "userlogin" => getUserID(),
                "bankaccno" => '',
                "token" => $data['trans_id'],
                "id_memb" => trim($personal_info['idmemberx']),
                "nmmember" => trim($personal_info['membername']),
                "total_pay" => $totPay,
                "total_bv" => $totalbv,
                "pricecode" => $pricecode,
                "bonusmonth" => $bns,
                "datetrans" => date("Y-m-d H:i:s"),
                "idstk" => trim($personal_info['stockist']),
                "nmstkk" => trim($nmstkSS),
                "secno" => "0",
                "flag_trx" => "W",
                "sentTo" => $personal_info['delivery'],
                "status_vt_pay" => "0",
                "status_vt_reject_dt" => "",
                "payShip" => $shipping_jne_info['price'],
                "payAdm" => $payAdm,
                "is_login" => $is_login,
                "totPayDP" => $tot_pay_dp,//tambahan vera
                "totPayCP" => $tot_pay_cp,//tambahan vera
                "profit_member" => $selisihHargaCust,//tambahan vera
                "id_lp" => $personal_info['id_lp'],
                "free_shipping" => $freeship,
                "discount_shipping" => $shipping_jne_info['ship_discount'],
                "bank_code_payment" => $bankid,
                "payConnectivity" => $charge_connectivity,
                //"payAdm" => $charge_admin,
                "whcd" => $personal_info['whcd'],
                "whnm" => $personal_info['whnm'],
                "kode_pay" => $kodepay,             //tambahan vera
                "kode_ref_bank" => $koderef,    //tambahan vera
                "date_expired" => $tgl_exp ,       //tambahan vera
                "kode_unik" => $data['kode_unik'], //tambahan vera
                "nom_pay" => $nompay //tambahan vera
            );

            //$arrx['summary'] = null;

            $res = $this->checkPaymentSales($data, $arrx);
            $arrx['pay'] = $res;

            $hasil = $this->insertTempTableSgo($arrx);
            //echo "<pre>";
            //print_r($arrx);
            //echo "</pre>";


            $arrx['bankDescDetail'] = $data['bankDescDetail'];
            $arrx['bankDesc'] = $data['bankDesc'];
            $arrx['bankCode'] = $data['bankCode'];
            $arrx['tot_pay'] = $arrx['summary']['total_pay'];
            $arrx['tot_bv'] = $arrx['summary']['total_bv'];
            $arrx['tot_nilai_voucher'] = $arrx['pay']['tot_nilai_voucher'];
            $arrx['prd_value_minus_voucher'] = $arrx['summary']['total_pay'] - $arrx['pay']['tot_nilai_voucher'];
            $arrx['pay_gateway'] = $arrx['pay']['pay_gateway'];
            $arrx['vch_stt'] = $arrx['pay']['vch_stt'];
            $arrx['temp_paymentIdx'] = $data['trans_id'];
            $arrx['prodCat'] = $this->cartshopService->getProductCategory();
            $arrx['keySgo'] = $this->session->userdata('pay_sgo_id');
            $this->load->model('webshop/shared_module', 'shared');

            //update ke newtrh
            /*$updNewTrh = array(
                "trcd" => $data['trans_id'],
                "pay3amt" => $random,
                "paynote3" => "Kode Unik Pay"
            );
            $this->cartshopService->updNewTrh($updNewTrh);*/

            //print_r($data['trans_id']);
            //$res = $this->shared->updateBankCodePayment2($temp_id, $arr);
            $arrx['temp_result'] = $this->shared->getDataPaymentSGOByOrderID($data['trans_id']);
            if (getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {

                $arrx['backURL'] = "https://www.k-net.co.id/pay/sgo/finish/dev/" . $arrx['temp_paymentIdx'];
                if ($arrx['temp_result'] != null) {
                    //print_r($arrx['temp_result']);
                    //production
                    //$arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
                    //development 0df5835ee198d49944c372ead860c241
                    $arrx['keySgo'] = '0df5835ee198d49944c372ead860c241';
                    $this->setTemplateDevSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
                } else {
                    echo "Data tidak dapat diinput..";
                }

                //$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
                //$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
            } else {

                $arrx['backURL'] = "https://www.k-net.co.id/pay/sgo/finish/" . $arrx['temp_paymentIdx'];
                if ($arrx['temp_result'] != null) {
                    $arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';

                    if($bankid == "29" || $bankid == "30" || $bankid == "31"){
                        $this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new2', $arrx);
                    }else{
                        $this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
                    }

					/*if(getUserID() == "ID9999A00251") {
						$this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
					} else {
						$this->setTemplateSGO($this->folderView . 'payment_sgo_vch_prev_new', $arrx);
					}*/
                    //$this->setTemplateSGO('webshop/payment_sgo_preview',$arrx);
                } else {
                    echo "Data tidak dapat diinput..";
                }

            }
        }


    }
<?php

    class Promo_heboh extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->model('webshop/M_heboh_banget','m_heboh');
            $this->load->service("webshop/Member_service",'memberService');
            $this->folderView = "webshop/kupon_heboh_banget/";

        }

        public function getListKupon(){

            $login = $this->session->userdata('store_info');

                if($login == NULL){

//                    $dt['idmember'] = 0;
//                    $dt['prodCat'] = $this->memberService->getProductCategory();
//                    $dt['det'] = $this->m_heboh->getKupon();
//                    //print_r($dt);
//                    $this->setTempWebShop($this->folderView.'v_kupon',$dt);
                    redirect('loginmember');

                }else{

                    $dt['idmember'] = getUserID();
                    $dt['prodCat'] = $this->memberService->getProductCategory();
                    $dt['det'] = $this->m_heboh->getKupon($dt['idmember']);
                    //print_r($dt);
                    $this->setTempWebShop($this->folderView.'v_kupon',$dt);
                }
        }

        public function getDetailKupon(){

            $dt = $this->input->post(null, true);

            $dt['detkupon'] = $this->m_heboh->getDetail($dt['dfno']);
            $dthdr = $this->m_heboh->getDetailHdr($dt['dfno']);

            foreach($dthdr as $row){

                $dt['fullnm'] = $row -> fullnm;

            }

            $dt['bnsperiod'] = '';
            $dt['tgl_undi'] = 'April - 2019';

            $this->load->view($this->folderView.'v_detail_kupon',$dt);
            //print_r($dt);
        }

    }
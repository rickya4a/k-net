<?php

class Promo_lbc extends MY_Controller {

    public function __construct(){

        parent::__construct();

        $this->load->model('webshop/M_promo_lbc','m_lbc');
        $this->load->service("webshop/Member_service",'memberService');
        $this->folderView = "webshop/promo_lbc/";

    }

    public function getInfo(){

        $login = $this->session->userdata('store_info');

        if($login == NULL){

            redirect('loginmember');

        }else{

            $dt['dfno']= getUserID();
            $dt['fullnm']= getUserName();

            $dt['prodCat'] = $this->memberService->getProductCategory();
            $dt['cekLBC'] = $this->m_lbc->cekLBC($dt['dfno']);

            if($dt['cekLBC'] != null) {

                $this->setTempWebShop($this->folderView.'v_formLBC',$dt);

            }else {

                $dt['cekLBC2'] = $this->m_lbc->cekLBC2($dt['dfno']);
                $this->setTempWebShop($this->folderView.'v_formLBC2',$dt);
                //redirect('loginmember');
            }
        }
    }


    public function getDetailLBC(){

        $data = $this->input->post(NULL, TRUE);

        //print_r($data);

        $dt['result'] = $this->m_lbc->getDetailLBC($data['dfno'], $data['bnsperiod']);

        $this->load->view($this->folderView.'v_detail_lbc',$dt);

    }

    public function getRekrutanLBC(){

        $data = $this->input->post(NULL, TRUE);

        //print_r($data);

        $dt['result'] = $this->m_lbc->getDetailRekrutanLBC($data['dfno'], $data['bnsperiod']);

        $dt['tot_bv'] = $this->m_lbc->totBV($data['dfno']);

        //print_r($dt['result']);
        $this->load->view($this->folderView.'v_detail_rekrutan_lbc',$dt);

    }

}

?>
<?php

class Verifikasi extends MY_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->model("webshop/Sales_model",'salesM');
        $this->load->service("webshop/Product_service",'productService');
        $this->load->service("webshop/Sales_service",'salesService');

    }

    public function kirim_sms(){

        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            //$dt['bonusmont'] = $this->salesService->getBonusMonth();
            //print_r($dt['listBank']);
            $this->setTempWebShop('webshop/stBonusReport2', $dt);
        }
    }

    function tampil_report(){
        if($this->_checkSessionStoreUser()) {

            $idmembers = getUserID();

            $year = $this->input->post('year');
            $dt['report'] = 1;
            $dt['report'] = $this->salesM->getReportBonus($idmembers,$year);

            $this->load->view('webshop/getListingFendi',$dt);
        }
    }

//$route['tes_verifikasi'] = 'webshop/Verifikasi/verifikasi_sms';
    public function verifikasi_sms(){

        $kode = rand(1111,9999);
        $text = "TES KODE VERIFIKASI CETAK VOUCHER, KODE ANDA ADALAH ".$kode;
//        $text = "Trx sukses, No Order : IDEC1810001969, Jumlah Trx 669000, Tracking : http://bit.ly/jnetracking";
        $telp = "081218303585";
        smsTemplate($telp, $text);

        echo "terkirim";
    }

    public function sendSms(){

        $no_hp = "081218303585";
        $message = "uang tidak bisa membeli kebahagiaan, tp pernahkah anda melihat org sedih ketika turun dari lambo?";
        smsTemplate($no_hp, $message);
        echo $no_hp."<br>".$message;
        //echo "hai";
    }

}

?>
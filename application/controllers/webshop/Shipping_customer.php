<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Shipping_customer extends MY_Controller {
    	
    public function __construct() {
	    parent::__construct();
		$this->load->model("webshop/Shipping_model",'shippingM');
		$this->load->service("webshop/Shipping_service",'shippingService');
		$this->load->service("webshop/Product_service",'productService');
	}
	
    //$route['shop/shipping'] = 'webshop/shipping_customer/getShipping'
    function getShipping() {
	     $dt = $this->shippingService->showStockistByArea(getUserID());
	     $dt['prodCat'] = $this->productService->getProductCategory();
	     $this->setTempWebShop('webshop/shipping', $dt);
    }
	
    //$route['shop/shipping/getStockist/(:any)'] = 'webshop/shipping_customer/getStockist/$1';
	function getStockist($area) {       
			$cek = $this->shippingService->getListStockistByArea($area);
	        echo json_encode($cek);      
    }
 
    //$route['shop/shipping/getPricecode/(:any)'] = 'webshop/shipping_customer/getPriceStockist/$1';
	function getPriceStockist($nilai) {
        $cek = $this->shippingService->showPricecodeByStockistID($nilai);
	    echo json_encode($cek);
    }
	
    
    //$route['shop/shipping/getPricecodeX/(:any)/(:any)'] = 'webshop/shipping_customer/getPriceStockistX/$1/$2';
	function getPriceStockistX($nilai, $kec) {
			$cek = $this->shippingService->showPricecodeByStockistIDX($nilai, $kec);
	        echo json_encode($cek);
    }
    
    
    //$route['shop/shipping/kota/(:any)'] = 'webshop/shipping_customer/getListKota/$1';
	function getListKota($prov) {
	        $res = $this->shippingService->showListKota($prov);
	        echo json_encode($res);      
    }
    
    //$route['shop/shipping/kecamatan/(:any)'] = 'webshop/shipping_customer/getListKecamatan/$1';
	function getListKecamatan($kota) {
        $res = $this->shippingService->showListKecamatan($kota);
	    echo json_encode($res);     
    }
    
    //$route['shop/shipping/kelurahan/(:any)'] = 'webshop/shipping_customer/getListKelurahan/$1';
    function getListKelurahan($kec) {
         $res = $this->shippingService->showListKelurahan($kec);
	     echo json_encode($res);          
    }
 }
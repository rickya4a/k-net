<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * 
 */
class Utility extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    function formDownloadBackpack($file)
    {

            $name = $file;

            $data = file_get_contents("assets/download/$file");// filenya
            force_download($name, $data);

    }

    function download2File()
    {

        if ($this->_checkSessionStoreUser()) {

            $name1 = 'D042Maret2019';
            $name2 = 'K042Maret2019';

            $data1 = file_get_contents("assets/download/$name1");
            force_download($name1, $data1);

            $data2 = file_get_contents("assets/download/$name2");
            force_download($name2, $data2);
        }
    }

}
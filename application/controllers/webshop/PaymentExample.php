<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * @author sahid
 */
class PaymentExample extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function payCartRedirect() {
        Veritrans_Config::$serverKey = 'VT-server-qoQ6mGJFLS-zkG16wTg5ufR0';
        Veritrans_Config::$isProduction = true;

        try {
            // Parameter untuk transaksi termasuk setting untuk pembayaran

            $params = array(
                "vtweb" => array(
                    "credit_card_3d_secure" => true,
                ),
                'transaction_details' => array(
                    'order_id' => 'IDX' . rand(),
                    'gross_amount' => 1320500,
                )
            );

            header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($params));
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function payCartDirect() {
        $this->load->view('webshop/checkout_example');
    }

    public function authorizePayment() {
        $this->load->view('webshop/authorize_example');
    }

    public function checkoutProcess() {

        $token_id = $this->input->post('token_id');


        if (empty($token_id)) {
            die('Empty token_id!');
        }

        Veritrans_Config::$serverKey = 'VT-server-qoQ6mGJFLS-zkG16wTg5ufR0';
        Veritrans_Config::$isProduction = true;

// Uncomment for production environment
// Veritrans_Config::$isProduction = true;
// Uncomment to enable sanitization
// Veritrans_Config::$isSanitized = true;

        $transaction_details = array(
            'order_id' => time(),
            'gross_amount' => 802000
        );

// Populate items
        $items = array(
            array(
                'id' => 'item1',
                'price' => 400000,
                'quantity' => 1,
                'name' => 'KinoTakara'
            ),
            array(
                'id' => 'item2',
                'price' => 201000,
                'quantity' => 2,
                'name' => 'Cholorophill'
        ));

// Populate customer's billing address
        $billing_address = array(
            'first_name' => "Andri",
            'last_name' => "Setiawan",
            'address' => "Karet Belakang 15A, Setiabudi.",
            'city' => "Jakarta",
            'postal_code' => "51161",
            'phone' => "081322311801",
            'country_code' => 'IDN'
        );

// Populate customer's shipping address
        $shipping_address = array(
            'first_name' => "John",
            'last_name' => "Watson",
            'address' => "Bakerstreet 221B.",
            'city' => "Jakarta",
            'postal_code' => "51162",
            'phone' => "081322311801",
            'country_code' => 'IDN'
        );

// Populate customer's info
        $customer_details = array(
            'first_name' => "Andri",
            'last_name' => "Setiawan",
            'email' => "andri@setiawan.com",
            'phone' => "081322311801",
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address
        );

// Transaction data to be sent
        $transaction_data = array(
            'payment_type' => 'credit_card',
            'credit_card' => array(
                'token_id' => $token_id,
                //'bank' => 'mandiri',
                'save_token_id' => isset($_POST['save_cc'])
            ),
            'transaction_details' => $transaction_details,
            'item_details' => $items,
            'customer_details' => $customer_details
        );

        $response = Veritrans_VtDirect::charge($transaction_data);

// Success
        if ($response->transaction_status == 'capture') {
            echo "<p>Transaksi berhasil.</p>";
            echo "<p>Status transaksi untuk order id $response->order_id: " .
            "$response->transaction_status</p>";

            echo "<h3>Detail transaksi:</h3>";
            echo "<pre>";
            var_dump($response);
            echo "</pre>";
        }
// Deny
        else if ($response->transaction_status == 'deny') {
            echo "<p>Transaksi ditolak.</p>";
            echo "<p>Status transaksi untuk order id .$response->order_id: " .
            "$response->transaction_status</p>";

            echo "<h3>Detail transaksi:</h3>";
            echo "<pre>";
            var_dump($response);
            echo "</pre>";
        }
// Challenge
        else if ($response->transaction_status == 'challenge') {
            echo "<p>Transaksi challenge.</p>";
            echo "<p>Status transaksi untuk order id $response->order_id: " .
            "$response->transaction_status</p>";

            echo "<h3>Detail transaksi:</h3>";
            echo "<pre>";
            var_dump($response);
            echo "</pre>";
        }
// Error
        else {
            echo "<p>Terjadi kesalahan pada data transaksi yang dikirim.</p>";
            echo "<p>Status message: [$response->status_code] " .
            "$response->status_message</p>";

            echo "<pre>";
            var_dump($response);
            echo "</pre>";
        }

        echo "<hr>";
        echo "<h3>Request</h3>";
        echo "<pre>";
        var_dump($response);
        echo "</pre>";
    }

    /*
     * Metode untuk Pre Authorization
     * Pre Authorization untuk mengecek Froud, Saldo, dll setelah itu baru di- 
     * capture, 
     * View : authorize_example.php
     * $route['authorize'] = 'webshop/PaymentExample/authorizePayment';
     * $route['authorize_process'] = 'webshop/PaymentExample/checkoutAuthorize';
     *      
     */

    public function checkoutAuthorize() {
        try {
            Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
            Veritrans_Config::$isProduction = false;

            $token = $this->input->post('token_id');

            if (empty($token)) {
                die('Empty token_id!');
            }

            echo $token;

            $transaction_details = array(
                'order_id' => time(),
                'gross_amount' => 200000
            );

            $items = array(
                array(
                    'id' => 'item1',
                    'price' => 100000,
                    'quantity' => 1,
                    'name' => 'Adidas f50'
                ),
                array(
                    'id' => 'item2',
                    'price' => 50000,
                    'quantity' => 2,
                    'name' => 'Nike N90'
            ));

            $billing_address = array(
                'first_name' => "Andri",
                'last_name' => "Setiawan",
                'address' => "Karet Belakang 15A, Setiabudi.",
                'city' => "Jakarta",
                'postal_code' => "51161",
                'phone' => "081322311801",
                'country_code' => 'IDN'
            );

            $customer_details = array(
                'first_name' => "Andri",
                'last_name' => "Setiawan",
                'email' => "andri@setiawan.com",
                'phone' => "081322311801",
                'billing_address' => $billing_address
            );

            $transaction_data = array(
                'payment_type' => 'credit_card',
                'credit_card' => array(
                    'token_id' => $token,
                    'bank' => 'mandiri',
                    'type' => 'authorize',
                    'save_token_id' => isset($_POST['save_cc'])
                ),
                'transaction_details' => $transaction_details,
//                'item_details' => $items,
                'customer_details' => $customer_details
            );

            $result = Veritrans_VtDirect::charge($transaction_data);

            var_dump($result);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /*
     * Metode Capture transaksi seperti memvalidasti trans yang gantung 
     * 
     * 
     */

    public function captureTransaction() {
        $transaction_id = 'b505b192-f202-41ad-b82a-18678d0571a9';
        $amount = 150000;

        $result = Veritrans_VtDirect::capture($transaction_id, $amount);

        var_dump($result);
    }

    function formDebit() {
        $this->load->view('webshop/checkout_debit');
    }

    function checkoutProcessDebit() {
        Veritrans_Config::$serverKey = "VT-server-qoQ6mGJFLS-zkG16wTg5ufR0";
        Veritrans_Config::$isProduction = true;

        $card_number = $_POST['card-number'];
        $input1 = $_POST['input1'];
        $input2 = $_POST['input2'];
        $input3 = $_POST['input3'];
        $token = $_POST['token'];

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => 200500
        );

        $customer_details = array(
            'first_name' => "Andri",
            'last_name' => "Litani", // Optional
            'email' => "Andri@Litani.com",
            'phone' => "081122334455"
        );

        // Data that will be sent for charge transaction request with Mandiri ClickPay.
        $transaction_data = array(
            'payment_type' => 'mandiri_clickpay',
            'mandiri_clickpay' => array(
                'card_number' => "4111111111111111",
                'input1' => "1111111111",
                'input2' => "200500",
                'input3' => "21392",
                'token' => "000000"
            ),
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details
        );

        $result = Veritrans_VtDirect::charge($transaction_data);

        if ($result->transaction_status == "settlement") {
            //success
            echo "Mandiri ClickPay Transaction Successful. <br />";
            echo "Transaction status for order id " . $result->order_id . ": " . $result->transaction_status;
            echo "<h3>Transaction details:</h3>";
            var_dump($result);
        } else if ($result->transaction_status == "deny") {
            //deny
            echo "Mandiri ClickPay Transaction Failed. <br />";
            echo "Transaction status for order id " . $result->order_id . ": " . $result->transaction_status;
            echo "<h3>Transaction details:</h3>";
            var_dump($result);
        } else {
            //error
            echo "Errors occurs on the sent transaction data.<br />";
            echo "<h3>Result:</h3>";
            var_dump($result);
        }
    }

    function formCimbDebit() {
        $this->load->view('webshop/payment_method/cimb_click');
    }

    function processCimb() {
        // TODO: change with your actual server_key that can be found on Merchant Administration Portal (MAP)
        Veritrans_Config::$serverKey = "VT-server-QYsxcIOfTcJ2edHZUACJiwkv";
        Veritrans_Config::$isProduction = false;

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => 20000,
        );

        $customer_details = array(
            'first_name' => "Andri",
            'last_name' => "Litani",
            'email' => "andri@litani.com",
            'phone' => "081122334455"
        );

        // Data yang akan dikirim untuk request charge transaction dengan credit card.
        $transaction_data = array(
            'payment_type' => 'cimb_clicks',
            'cimb_clicks' => array(
                'description' => "Contoh Deskripsi",
            ),
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details
        );

        $result = Veritrans_VtDirect::charge($transaction_data);
        if ($result->status_code == "201") {
            //success
            echo "Redirect CIMB Clicks. <br />";
            var_dump($result);
            //print_r($result);
            header('Location: ' . $result->redirect_url);
        } else {
            //error
            echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
            echo "<h3>Result:</h3>";
            var_dump($result);
        }
    }

    function formBriDebit() {
        $this->load->view('webshop/epay_briTest');
    }

    function processBri() {
        // TODO: change with your actual server_key that can be found on Merchant Administration Portal (MAP)
        Veritrans_Config::$serverKey = "VT-server-QYsxcIOfTcJ2edHZUACJiwkv";
        Veritrans_Config::$isProduction = false;

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => 145000,
        );

        $customer_details = array(
            'first_name' => "Andri",
            'last_name' => "Litani",
            'email' => "andri@litani.com",
            'phone' => "081122334455"
        );

        // Data yang akan dikirim untuk request charge transaction dengan e-Pay BRI.
        $transaction_data = array(
            'payment_type' => 'bri_epay',
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details
        );

        $result = Veritrans_VtDirect::charge($transaction_data);
        if ($result->status_code == "201") {

            echo "Redirect e-Pay BRI. <br />";
            //var_dump($result);
            //header('Location: ' . $result->redirect_url);
        } else {
            //error
            echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
            echo "<h3>Result:</h3>";
            var_dump($result);
        }
    }

    function virtualAccount() {
        $this->load->view('webshop/payment_method/virtual_account');
    }

    function processVa() {
        // TODO: change with your actual server_key that can be found on Merchant Administration Portal (MAP)
        Veritrans_Config::$serverKey = "VT-server-QYsxcIOfTcJ2edHZUACJiwkv";
        Veritrans_Config::$isProduction = false;

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => 145000,
        );

        $customer_details = array(
            'first_name' => "Andri",
            'last_name' => "Litani",
            'email' => "andri@litani.com",
            'phone' => "081122334455"
        );

        // Data yang akan dikirim untuk request charge transaction dengan credit card.
        $transaction_data = array(
            'payment_type' => 'bank_transfer',
            'bank_transfer' => array(
                'bank' => "permata",
            ),
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details
        );

        $result = Veritrans_VtDirect::charge($transaction_data);
        if ($result->status_code == "201") {
            //success
            echo "<b>Bank Transfer</b> <br /><br />";
            echo "<b>Silahkan transfer ke Bank Permata dengan No rekening:</b> " . $result->permata_va_number;
            echo "<br /><br />";
            echo "1. Anda memiliki waktu 2 jam untuk melakukan pembayaran, sebelum transaksi tersebut dibatalkan <br />";
            echo "2. Jika anda menemui kendala, silahkan hubungi kami di 021-22334456";
        } else {
            //error
            echo "Terjadi kesalahan pada data transaksi yang dikirim.<br />";
            echo "<h3>Result:</h3>";
            var_dump($result);
        }
    }

    public function mobilePayment() {
        if (empty($_POST['token-id'])) {
            echo "Empty token-id";
            exit;
        }

        Veritrans_Config::$serverKey = 'VT-server-QYsxcIOfTcJ2edHZUACJiwkv';
        Veritrans_Config::$isProduction = false;

        $token_id = $_POST['token-id'];
        echo $token_id;

        $transaction_details = array(
            'order_id' => rand(),
            'gross_amount' => $_POST['price']
        );

        $transaction_data = array(
            'payment_type' => 'credit_card',
            'credit_card' => array(
                'token_id' => $token_id,
                'bank' => 'bni'
            ),
            'transaction_details' => $transaction_details,
        );
        try {
            $result = Veritrans_VtDirect::charge($transaction_data);
            $json = json_encode($result);
            echo "{\"status\":\"success\", \"body\" : $json}";
        } catch (Exception $e) {
            echo "{\"status\":\"error\",\"body\": \".$e.\"}";
        }
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cartshop_vera extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->service("webshop/Cartshop_service", 'cartshopService');
		$this->folderView = "webshop/payment_sgo_vch/";
	}

	public function listCart()
	{
		if($this->_checkSessionStoreUser()) {
			$dt['formAction'] = "".site_url('shipping/postShipping')."";
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			//$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			//print_r($dt['bns']);
			$dt['cart'] = $this->cart->contents();
			$personal_info = $this->session->userdata('personal_info');
			if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
				//$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
				$dt['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
			} else {
				$dt['listCargo'] = $this->cartshopService->getListCargo();
			}
			$dt['listFreeProduct'] = null;

			//mendapatkan jam buka gosend
			$day = date('D');
			$now = new Datetime("now");
			$this->load->model('lokasi/M_location', 'M_location');

			//Instant
			$getInstant= $this->M_location->getSchedule($day,'Instant');
			$open_inst= $getInstant[0]->opened;
			$closed_inst= $getInstant[0]->closed;
			$begintime1 = new DateTime($open_inst);
			$endtime1 = new DateTime($closed_inst);

			if($now >= $begintime1 && $now <= $endtime1){
				$dt['value1']='"1-"+rupiah1';
				$dt['option1']='"Instant "+instant_hours+" Rp"+ rupiah1';
			} else {
				$dt['value1']='"#"';
				$dt['option1']='"Instant - TUTUP (Jam Pesanan: 10 Pagi - 5 Sore)"';
			}

			//SameDay
			$getSame= $this->M_location->getSchedule($day,'SameDay');
			$open_same= $getSame[0]->opened;
			$closed_same= $getSame[0]->closed;
			$begintime2 = new DateTime($open_same);
			$endtime2 = new DateTime($closed_same);

			if($now >= $begintime2 && $now <= $endtime2){
				$dt['value2']='"2-"+rupiah1';
				$dt['option2']='"SameDay "+sameday_hours+" Rp"+ rupiah1';
			} else {
				$dt['value2']='"#"';
				$dt['option2']='"Same Day - TUTUP (Jam Pesanan: 10 Pagi - 2 Sore)"';
			}

			//cargo GOSEND
			$getCargo= $this->M_location->getCargo('4');
			$open_gsend= $getCargo[0]->open;
			$closed_gsend= $getCargo[0]->close;
			$dt['begintime'] = new DateTime($open_gsend);
			$dt['endtime'] = new DateTime($closed_gsend);
			$dt['info_html']=$getCargo[0]->info_html;

			if($personal_info != null) {
				//ubah dion @ 21/05/2017
				if(array_key_exists("shipper", $personal_info)) {
					if($personal_info['shipper'] == "1") {
						$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
						$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
					} else {
						$dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb();
						$dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);
					}
				} else {
					$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
				}

				//print_r($dt['shipping']);
				//echo "c";
				if (getUserID()=='IDSPAAA66834' || getUserID() == "IDSPAAB01065")
				{

					$this->setTempWebShop('webshop/vera/shipping2TestWithAddress',$dt);
					//$this->setTempWebShop('webshop/shippingTesVera',$dt);

				}
				else
				{
					//$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
					//upd by dion 09/09/2018
					$this->setTempWebShop('webshop/vera/shipping2TestWithAddress',$dt);
				}

			} else {
				$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
				$res = $this->cartshopService->getAddressReferenceByID(getUserID());
				if($res != null) {
					$personal_infox = array(
						"provinsi" => $res[0]->provinsi,
						"kota" => $res[0]->kota,
						"kecamatan" => $res[0]->kecamatan,
					);
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);
					$this->cartshopService->setAutomaticPersonalInfo($res);
					//print_r($dt['shipping'])
					//echo "e";
					if (getUserID()=='IDSPAAA66834')
					{
						//$this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
						//$dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();
						$this->setTempWebShop('webshop/vera/shippingAutoTest',$dt);

					}
					else
					{
						//$this->setTempWebShop('webshop/shippingAutoAddrReff',$dt);
						//upd by dion 09/09/2018
						$this->setTempWebShop('webshop/vera/shippingAutoTest',$dt);
					}

					//$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
				} else {
					//echo "v";
					if (getUserID()=='IDSPAAA66834' || getUserID() == "IDSPAAB01065")
					{
						$this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
						$dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();

						$this->setTempWebShop('webshop/vera/shipping2Test',$dt);

					}
					else
					{
						//$this->setTempWebShop('webshop/shipping2',$dt);
						//upd by dion 09/09/2018
						$this->setTempWebShop('webshop/vera/shipping2Test',$dt);
					}
				}
			}
		}
	}

	function CheckoutCartSGO() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			if($this->checkShippingDataBaru($data)) {
				$dt['cart'] = $this->cart->contents();
				$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
				if($data != null) {
					$eerr = $this->updateCartBeforeProceed();
					$prdx = set_list_array_to_stringCart($dt['cart'], "id");
					//edited, disabled by Dion @13/10/2015
					//$dt = $this->cartshopService->getProductShop($prdx, $this->session->userdata('pricecode'));
					//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
					//$reg = $this->cartshopService->sessionRegPersonalInfoBaru($data);
					$reg = $this->sessionRegPersonalInfoNew($data);
					//$ship = $this->cartshopService->setShippingInfo();
					$ship = $this->setShippingInfoBaru();
					$dt['prodCat'] = $this->cartshopService->getProductCategory();


					if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
						$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
					} else {
						$dt['listBank'] = $this->cartshopService->getBank();
					}
					//production
					//$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
					//development 0df5835ee198d49944c372ead860c241
					$dt['key'] = '0df5835ee198d49944c372ead860c241';

					/*
                    $pay_sgo_id = $this->session->userdata("pay_sgo_id");
                    if($pay_sgo_id == null || !isset($pay_sgo_id)) {
                        $dt['payID'] = "EC".randomNumber(8);
                        $this->session->set_userdata("pay_sgo_id", $dt['payID']);
                        $insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
                    } else {
                        $dt['payID'] = $this->session->userdata("pay_sgo_id");
                        $del = $this->cartshopService->deleteTempTrxSGO($dt['payID']);
                        $insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
                    }
                     * */
					//add testing by DION 05/03/2016

					/*------------------------------------------------------------
                     * PROMO 17 AGUSTUS FREE SHIPPING WITH BUYING SPECIAL PRODUCT
                     * -----------------------------------------------------------*/

					/*$dt['prd_promo'] = $this->cartshopService->getPromo17Agustus();
                    if($dt['prd_promo'] != null) {
                        $arr = $dt['prd_promo']['arrayData'];

                        //print_r($dt['cart']);
                        $ketemu = 0;
                        foreach($dt['cart'] as $items) {
                            if(in_array($items['id'], $arr)) {
                                $ketemu = 1;
                                break;
                            }
                        }
                        if($ketemu == 1) { */
					//$totDiscount = $ship['price'];
					if(getUserID() == "IDSPAAA96407") {
						$dt['biaya'] = getTotalPayNetAndShipCostTerbaru();
					}
					/*}
                }*/
					/*------------------------------------------------------------
                     * END PROMO 17 AGUSTUS CODE
                     * -----------------------------------------------------------*/

					//GET SALDO VA
					$this -> load -> model("backend/be_hifi_model", "hifi");
					$resSaldo = $this->hifi->getSaldoVa(getUserID());
					if($resSaldo == null) {
						$dt['saldo_va'] = "0";
					} else {
						$dt['saldo_va'] = number_format($resSaldo[0]->amount, 0, ",", ".");
					}

					$dt['payID'] = "EC".randomNumber(8);
					//$this->session->unset_userdata('pay_sgo_id');
					//$this->session->set_userdata("pay_sgo_id", $dt['payID']);
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);

					$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['payID'];
					//print_r($dt);
				}

				if($data['delivery'] != "2") {
					echo "<script>
//						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
				} else {
					$dt['previewUrl'] = "cart/checkout/sgo/pay/verapreview2";
					$this->setTemplateDevSGO($this->folderView.'payment_sgo_vch',$dt);
				}

				//$this->setTemplateDevSGO($this->folderView.'payment_sgo_vch',$dt);

				/*if($data['delivery'] == "2" && $ship != 1 && $ship == null) {
					echo "<script>
//						window.location.href='list';
						alert('Maaf area yang anda pilih tidak ada pricelist nya..');
						</script>";
				} else {
					//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
					$this->setTemplateDevSGO($this->folderView.'payment_sgo_vch',$dt);
					//$this->setTemplateSGO('webshop/payment_sgo',$dt);
				}*/

			} else {
				//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
				//redirect('cart/list');
				/*echo "<script>
						window.location.href='list';
						alert('Mohon data dilengkapi dahulu..');
						</script>";*/
			}
		}

	}

	function sessionRegPersonalInfoNew($data) {
		$this->session->unset_userdata('personal_info');
		if($data['delivery'] == "1") {
			$arr = array(
				"delivery" => $data['delivery'],
				/*"stkarea" => $data['stkarea'],
                "nama_stkarea" => $data['nama_stkarea'],
                "stockist" => $data['stockist'],
                "nama_stockist" => $data['nama_stockist'], */
				"provinsi" => $data['provinsi1'],
				"nama_provinsi" => $data['nama_provinsi1'],
				"kota" => $data['kota1'],
				"nama_kota" => $data['nama_kota1'],
				"kecamatan" => $data['kecamatan1'],
				"nama_kecamatan" => $data['nama_kecamatan1'],
				"stockist" => $data['stockist'],
				"nama_stockist" => $data['nama_stockist'],
				"idmemberx" => $data['idmemberx'],
				"membername" => $data['membername'],
				"bnsperiod" => $data['bnsperiod'],
				"id_lp" => $data['id_lp']
			);
		} else {
			if($data['shipper'] == "1") {
				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $data['stockistref'],
					"nama_stockist" => $data['nama_stockistr1ref'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['provinsi'],
					"nama_provinsi" => $data['nama_provinsi'],
					"kota" => $data['kota'],
					"nama_kota" => $data['nama_kota'],
					"kecamatan" => $data['kecamatan'],
					"nama_kecamatan" => $data['nama_kecamatan'],
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					"idmemberx" => $data['idmemberx'],
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp']
				);

			}else if($data['shipper'] == "4") {
				$metode= $data['metode'];
				$cmetode=substr($metode,0,1);
				if($cmetode == 1){
					$cmetode="Instant";
				}else{
					$cmetode="SameDay";
				}

				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => $data['no_order'],
					"nama_stkarea" => $cmetode,
					"stockist" => "BID06",
					"nama_stockist" => "BID06 - PT. K-LINK NUSANTARA - JK",
					"shipper" => $data['shipper'],
					"provinsi" => NULL,
					"nama_provinsi" => NULL,
					"kota" => NULL,
					"nama_kota" => NULL,
					"kecamatan" => NULL,
					"nama_kecamatan" => NULL,
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					"idmemberx" => $data['idmemberx'],
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp']
				);
			}else {
				$arr = array(
					"delivery" => $data['delivery'],
					"nama_penerima" => $data['nama_penerima'],
					//"lastname" => $data['lastname'],
					"email" => $data['email'],
					"notlp" => $data['notlp'],
					"stkarea" => NULL,
					"nama_stkarea" => NULL,
					"stockist" => $data['stockistref'],
					"nama_stockist" => $data['nama_stockistr1ref'],
					"shipper" => $data['shipper'],
					"provinsi" => $data['provinsi'],
					"nama_provinsi" => $data['nama_provinsi'],
					"kota" => $data['kota'],
					"nama_kota" => $data['nama_kota'],
					"kecamatan" => "",
					"nama_kecamatan" => "",
					//"kelurahan" => $data['kelurahan'],
					//"nama_kelurahan" => $data['nama_kelurahan'],
					//"kodepos" => $data['kodepos'],
					"alamat" => $data['alamat'],
					"idmemberx" => $data['idmemberx'],
					"membername" => $data['membername'],

					"bnsperiod" => $data['bnsperiod'],
					"id_lp" => $data['id_lp']
				);
			}
			$this->session->set_userdata('sender_address', $data['sender_address']);
			$this->session->set_userdata('destination_address', $data['destination_address']);
			$this->session->set_userdata('jne_branch', $data['jne_branch']);
		}
		$this->session->set_userdata('personal_info', $arr);
		$this->session->set_userdata('trxtype', "product_shopping");
	}

	function checkShippingDataBaru($data) {
		if($data['delivery'] == null || $data['delivery'] == "") {
			return false;
		} else {
			if($data['delivery'] == "1") {
				if($data['provinsi1'] == null || $data['provinsi1'] == "") {
					return false;
				} elseif($data['kota1'] == null || $data['kota1'] == "") {
					return false;
				} elseif($data['kecamatan1'] == null || $data['kecamatan1'] == "") {
					return false;
				} elseif($data['stockist'] == null || $data['stockist'] == "") {
					return false;
				} else {
					return true;
				}

			} else {
				if($data['shipper'] == "1") {
					if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
					} elseif($data['notlp'] == null || $data['notlp'] == "") {
						return false;
					} elseif($data['provinsi'] == null || $data['provinsi'] == "") {
						return false;
					} elseif($data['kota'] == null || $data['kota'] == "") {
						return false;
					} elseif($data['kecamatan'] == null || $data['kecamatan'] == "") {
						return false;
					}
					elseif($data['alamat'] == null || $data['alamat'] == "") {
						return false;
					} elseif($data['stockistref'] == null || $data['stockistref'] == "") {
						return false;
					} else {
						return true;
					}
				}else if($data['shipper'] == "4") {
					if($data['nama_penerima'] == null || $data['nama_penerima'] == "") {
						return false;
					} elseif($data['notlp'] == null || $data['notlp'] == "") {
						return false;
					}elseif($data['alamat'] == null || $data['alamat'] == "") {
						return false;
					} elseif($data['stockistref2'] == null || $data['stockistref2'] == "") {
						return false;
					}elseif($data['metode'] == null || $data['metode'] == "") {
						return false;
					} else {
						return true;
					}
				} //end shipper 4

			}
		}
	}

	function updateCartBeforeProceed() {

		if($this->_checkSessionStoreUser()) {
			$arr = jsonFalseResponse(NULL, "Update Cart gagal..!");
			$data = $this->input->post(NULL, TRUE);
			$upd = $this->cartshopService->updateCart($data);
			$this->session->set_userdata('pricecode', $data['pricecode']);
			if($upd) {
				$arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
			}
			//echo json_encode($arr);
			return $arr;
		}
	}

	function setShippingInfoBaru() {
		$personal_info = $this->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
				"ship_discount" => 0,
				"lat_dest" => "",
				"long_dest" => ""
			);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
		} else {
			if($personal_info['shipper'] == "1") {
				$dtx = json_decode($this->showListPriceJne($this->cart->total_weight()));
//				echo "isi dtc = ";
//				print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $this->setLowestJnePrice($dtx);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}

			}else if($personal_info['shipper'] == "2") {
				$dtx = json_decode($this->showListPriceKgb($this->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$this->session->unset_userdata('shipping_jne_info');
					$this->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}else if($personal_info['shipper'] == "4") {
				//$dtx = json_decode($this->showListPriceGosend($this->cart->total_weight()));
				$metode=$this->input->post("metode");
				$cmetode=substr($metode,0,1);
				if($cmetode == 1){
					$cmetode="Instant";
				}else{
					$cmetode="SameDay";
				}

				$harga=substr($metode,2);
				$charga=str_replace(".","",$harga);
				$charga= $charga * 1;

				$arrx = array(
					"origin_name" => "K-Link Tower",
					"destination_name" => $this->input->post("alamat"),
					"service_code" => $this->input->post("no_order"),
					"service_display" => $cmetode,
					"price" => $charga,
					"etd_from" => "",
					"etd_thru" => "",
					"times" => "",
					"ship_discount" => 0,
					"lat_dest" => $this->input->post("lat_dest"),
					"long_dest" => $this->input->post("long_dest")
				);
				$this->session->unset_userdata('shipping_jne_info');
				$this->session->set_userdata('shipping_jne_info', $arrx);
				return null;

			}
		}
	}

	public function paySGOPreview2() {
		$data = $this->input->post(NULL, TRUE);
		//echo "<pre>";
		//print_r($data);
		//echo "</pre>";

		$data['trans_id'] = "EC".randomNumber(8);

		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$sender_address = $this->session->userdata('sender_address');
		$dest_address = $this->session->userdata('destination_address');

		$jne_branch = $this->session->userdata('jne_branch');
		$promoDiscShipping = $this->session->userdata('promo');
		$bns = substr($personal_info['bnsperiod'], 3, 7);

		//SET HARGA
		$harga = getTotalPayNetBaru();
		$totPay = $harga['total_pay'];
		$selisihHargaCust = 0;
		//END

		$totalbv = $this->cart->total_bv();
		$freeship = "0";

		$biaya = getTotalPayNetAndShipCost2();
		$gross_amount = $biaya['total_pay'];
		$freeship = $biaya['freeship'];
		$discount = $biaya['tot_discount'];

		$shipper_telhp = getUserPhone();
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$produk = $this->cart->contents();

		$cust_memb = $this->session->userdata("non_member_info");
		if(isset($cust_memb)) {
			$is_login = "2";
			$tot_paynet = getTotalPayNet();
		} else {
			$memb = $this->session->userdata("store_info");
			if(isset($memb)) {
				$is_login = "1";
				$tot_paynet = 0;
			}
		}

		if($personal_info['delivery'] == "2") {
			$arrx = array(
				"origin_name" => $shipping_jne_info['origin_name'],
				"destination_name" => $shipping_jne_info['destination_name'],
				"service_code" => $shipping_jne_info['service_code'],
				"service_display" => $shipping_jne_info['service_display'],
				"price" => $data['total_ship_price'],
				"etd_from" => $shipping_jne_info['etd_from'],
				"etd_thru" => $shipping_jne_info['etd_thru'],
				"times" => $shipping_jne_info['times'],
				"ship_discount" => $data['total_diskon_ship'],
				"lat_dest" => $shipping_jne_info['lat_dest'],
				"long_dest" => $shipping_jne_info['long_dest']
			);
			//print_r($arrx);
			$this->session->unset_userdata('shipping_jne_info');
			$this->session->set_userdata('shipping_jne_info', $arrx);
			$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		}

		//CHECK APAKAH ADA PROMO AGUSTUS YG AKTIF
		$promo_agust = "0";
		if(array_key_exists("promo_agust", $data)) {
			$promo_agust = $data['promo_agust'];
		}

		//CHECK APAKAH MEMBER MEMILIH PEMBELANJAAN DROPSHIPPER
		$dropship = $personal_info['id_lp'];
		if($personal_info['id_lp'] == "DROPSHIP") {
			$selisihHargaCust = getTotalPayNet() - $totPay;
			$totPay = getTotalPayNet();
		}

		//SET PARAM UNTUK PRODUK
		$prdParam = array(
			"trans_id" => $data['trans_id'],
			"total_sales_weight" => $this->cart->total_weight(),
			"dropship" =>	$dropship,
			"listProduct" => $produk,
			"pricecode" => $pricecode,
			"dataForm" => $data,
			"sentTo" => $personal_info['delivery'],
			"promoAgust2018" => $promo_agust
		);
		//echo "<pre>";
		//print_r($prdParam);
		//echo "</pre>";
		$insPrdArr = $this->insertProduct($prdParam);
		$arrx['product'] = $insPrdArr['product'];

		//CHECK APAKAH BARANG DIKIRIM / DI AMBIL DI STOKIS
		$arrShip = null;
		if($personal_info['delivery'] == "2") {
			// $stk = explode(" - ", $personal_info['stockist']);
			//$nama_stk = trim($stk[1]);

			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);

			//print_r($ssx);
			$arrShip = array(
				"orderno" => $data['trans_id'],
				"idstk" => $personal_info['stockist'],
				"prov_code" => $personal_info['provinsi'],
				"kab_code" => $personal_info['kota'],
				"kec_code" => $personal_info['kecamatan'],
				"province_name" => $personal_info['nama_provinsi'],
				"kabupaten_name" => $personal_info['nama_kota'],
				"stockist_name" => $nama_stk,

				"receiver_name" => $personal_info['nama_penerima'],
				"addr1" => str_replace("�??", "", $personal_info['alamat']),
				"email" => $personal_info['email'],
				"tel_hp1" => $personal_info['notlp'],
				"tel_hp2" => getUserPhone(),
				"shipper_telhp" => getUserPhone(),

				"service_type_id" => $shipping_jne_info['service_code'],
				"service_type_name" => $shipping_jne_info['service_display'],
				"total_item" => $insPrdArr['total_item'],
				"total_weight" => $insPrdArr['total_weight'],
				"sender_address" => $sender_address,
				"dest_address" => $dest_address,
				"jne_branch" => $jne_branch,
				"cargo_id" => $personal_info['shipper'],
				"total_pay_net" => $totPay,
				"lat_dest" => $shipping_jne_info['lat_dest'],
				"long_dest" => $shipping_jne_info['long_dest'],
				"whcd" => $personal_info['whcd'],
				"whnm" => $personal_info['whnm']

			);
		}

		$arrx['shipping'] = $arrShip;

		//echo "Total Weight semua : ".$total_weight;
		//echo "<br />nTotal Belanja : ".$totPay;
		//echo "<br />Selisih harga CUST/DIST : ".$selisihHargaCust;
		if(array_key_exists("bankid", $data)) {
			$bankid = $data['bankid'];
			$charge_connectivity = $data['charge_connectivity'];
			$charge_admin = (int) $data['charge_admin'];
		}	 else {
			$bankid = "19";
			$charge_connectivity = "";
			$charge_admin = 0;
		}

		$arrx['summary'] = array(
			"orderno" => $data['trans_id'],
			"userlogin" => getUserID(),
			"bankaccno" => '',
			"token" => $data['trans_id'],
			"id_memb" => trim($personal_info['idmemberx']),
			"nmmember" => trim($personal_info['membername']),
			"total_pay" => $totPay,
			"total_bv" => $totalbv,
			"pricecode" => $pricecode,
			"bonusmonth" => $bns,
			"datetrans" => date("Y-m-d H:i:s"),
			"idstk" => trim($personal_info['stockist']),
			"nmstkk" => trim($nmstkSS),
			"secno" => "0",
			"flag_trx" =>"W",
			"sentTo" => $personal_info['delivery'],
			"status_vt_pay" => "0",
			"status_vt_reject_dt" => "",
			"payShip" => $shipping_jne_info['price'],
			"payAdm" => $data['charge_admin'],
			"is_login" => $is_login,
			"totPayCP" => $selisihHargaCust,
			"id_lp" => $personal_info['id_lp'],
			"free_shipping" => $freeship,
			"discount_shipping" => $shipping_jne_info['ship_discount'],
			"bank_code_payment" => $bankid,
			"payConnectivity" => $charge_connectivity,
			"payAdm" => $charge_admin,
		);

		//$arrx['summary'] = null;

		$res = $this->checkPaymentSales($data, $arrx);
		$arrx['pay'] =  $res;

		$hasil = $this->insertTempTableSgo($arrx);
		//echo "<pre>";
		//print_r($hasil);
		//echo "</pre>";


		$arrx['bankDescDetail'] = $data['bankDescDetail'];
		$arrx['bankDesc'] = $data['bankDesc'];
		$arrx['bankCode'] = $data['bankCode'];
		$arrx['tot_pay'] = $arrx['summary']['total_pay'];
		$arrx['tot_nilai_voucher'] = $arrx['pay']['tot_nilai_voucher'];
		$arrx['prd_value_minus_voucher'] = $arrx['summary']['total_pay'] - $arrx['pay']['tot_nilai_voucher'];
		$arrx['pay_gateway'] = $arrx['pay']['pay_gateway'];
		$arrx['vch_stt'] = $arrx['pay']['vch_stt'];
		$arrx['temp_paymentIdx'] = $data['trans_id'];
		$arrx['prodCat'] = $this->cartshopService->getProductCategory();
		$arrx['keySgo'] = $this->session->userdata('pay_sgo_id');
		$this->load->model('webshop/shared_module', 'shared');
		//$res = $this->shared->updateBankCodePayment2($temp_id, $arr);
		$arrx['temp_result'] = $this->shared->getDataPaymentSGOByOrderID($data['trans_id']);

		if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {

			$arrx['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/dev/".$arrx['temp_paymentIdx'];
			if($arrx['temp_result'] != null) {
				//print_r($arrx['temp_result']);
				//production
				//$arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
				//development 0df5835ee198d49944c372ead860c241
				$arrx['keySgo'] = '0df5835ee198d49944c372ead860c241';
				$this->setTemplateDevSGO($this->folderView.'payment_sgo_vch_prev_new',$arrx);
			} else {
				echo "Data tidak dapat diinput..";
			}

			//$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
			//$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
		} else {

			$arrx['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$arrx['temp_paymentIdx'];
			if($arrx['temp_result'] != null) {
				$arrx['keySgo'] = '51edf5e8117da341a8be702d9bc18de5';
				$this->setTemplateSGO($this->folderView.'payment_sgo_vch_prev_new',$arrx);
				//$this->setTemplateSGO('webshop/payment_sgo_preview',$arrx);
			} else {
				echo "Data tidak dapat diinput..";
			}

		}
	}

	function insertTempTableSgo($arrx) {
		//echo "<pre>";
		//print_r($arrx);
		//echo "</pre>";

		$res = jsonFalseResponse("insert gagal..");
		if(!array_key_exists("product", $arrx) || $arrx['product'] == null) {
			return jsonFalseResponse("Array Product tidak ada..");
		}

		if(!array_key_exists("summary", $arrx) || $arrx['summary'] == null) {
			return jsonFalseResponse("Array Summary tidak ada..");
		}

		if(!array_key_exists("pay", $arrx) || $arrx['pay'] == null) {
			return jsonFalseResponse("Array Summary tidak ada..");
		}

		$dbqryx = $this->load->database('db_ecommerce', true);
		$dbqryx->trans_start();
		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_DET_PRD_SGO
		----------------------------------------*/
		foreach($arrx['product'] as $dataPrd) {
			/*
			$prdSql = $this->db->set($dataPrd)->get_compiled_insert('ecomm_trans_det_prd_sgo');
			echo $prdSql;
			echo "<br />";
			*/
			$dbqryx->insert('ecomm_trans_det_prd_sgo', $dataPrd);
		}

		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_HDR_SGO
		----------------------------------------*/
		/*
		echo "<br />";
		$hdrSql = $this->db->set($arrx['summary'])->get_compiled_insert('ecomm_trans_hdr_sgo');
		echo $hdrSql;
		echo "<br />";
		*/
		$dbqryx->insert('ecomm_trans_hdr_sgo', $arrx['summary']);
		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_PAYDET_SGO
		----------------------------------------*/
		//echo "<br />";
		foreach($arrx['pay']['list_payment'] as $pay) {
			/*
			$paySql = $this->db->set($pay)->get_compiled_insert('ecomm_trans_paydet_sgo');
			echo $paySql;
			echo "<br />";
			*/
			$dbqryx->insert('ecomm_trans_paydet_sgo', $pay);
		}

		/*--------------------------------------
		INSERT TABLE ECOMM_TRANS_SHIPPADDR_SGO
		----------------------------------------*/
		if($arrx['shipping'] != null) {
			/*
			echo "<br />";
			$shipSql = $this->db->set($arrx['shipping'])->get_compiled_insert('ecomm_trans_shipaddr_sgo');
			echo $shipSql;
			echo "<br />";
			*/
			$dbqryx->insert('ecomm_trans_shipaddr_sgo', $arrx['shipping']);
		}

		//END TRANSACTION
		$dbqryx->trans_complete();

		//CHECK IF TRANSACTION PROCESS IS COMMITED
		if ($dbqryx->trans_status() === FALSE) {
			//if something went wrong, rollback everything
			$dbqryx->trans_rollback();
			return false;
		} else {
			//if everything went right, insert the data to the database
			$dbqryx->trans_commit();
			return true;
		}

		//return $res;
	}

	function checkPaymentSales($data, $arrx) {
		$summary = $arrx['summary'];
		$tot_pay = getTotalPayNet();
		$tot_hrs_dibayar = $summary['total_pay'] +  $summary['payShip'] - $summary['discount_shipping'];
		if(count($data) > 0) {
			$jml_voucher = isset($data['shop_vch']) ? count($data['shop_vch']) : 0;
			$pay['vch_stt'] = false;
			$pay['res'] = null;
			$pay['pay_gateway'] = true;
			$tot_nominal_vch = 0;
			//Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
			//atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar

			if($jml_voucher > 0) {
				$pay['vch_stt'] = true;
				$tempArr = array();
				$tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
				$this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $data['trans_id']);
				$xc = 0;
				for($i = 0; $i < $jml_voucher; $i++) {
					$xc++;
					$tot_nominal_vch += $data['shop_vch_nominal'][$i];
					//$xc = $i + 1;
					$tbl .= "<tr>";
					$tbl .= "<td align=right>".$xc."&nbsp;</td>";
					$tbl .= "<td align=center>".$data['shop_vch'][$i]."</td>";
					$tbl .= "<td align=right>Rp.".$data['shop_vch_nominal'][$i]."&nbsp;</td>";
					$tbl .= "</tr>";
					$insAr = array(
						"seqno" => $xc,
						"orderno" => $summary['orderno'],
						"paytype" => "vch",
						"docno" => $data['shop_vch'][$i],
						"payamt" => $data['shop_vch_nominal'][$i],
						"bank_code_payment" => 19,
						"charge_admin" => 0
					);
					/*echo "<pre>";
                    print_r($insAr);
                    echo "</pre>";*/
					$pay['list_payment'][$i] = $insAr;
					//$this->cartshopService->insertVoucherList($insAr);

				}
				$tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp.".$tot_nominal_vch."&nbsp;</td></tr>";
				$tbl .= "</table>";

				//Jika total nominal voucher lebih besar atau sama dengan
				//nominal belanja yang harus dibayar
				if($tot_nominal_vch >= $tot_hrs_dibayar) {
					//status payment gateway = false, data Langsung insert ke database tanpa pembayaran
					$pay['pay_gateway'] = false;

					//$pay['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					$pay['tot_hrs_dibayar'] = $tot_hrs_dibayar;

				} else {
					//Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO

					//$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);

					//Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
					$sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
					$seqno = $xc + 1;
					$insAr = array(
						"seqno" => $seqno,
						"orderno" => $summary['orderno'],
						"paytype" => "sgo",
						"docno" => "",
						"payamt" => $sisaByr,
						"bank_code_payment" => $data['bankid'],
						"charge_admin" => $summary['payAdm']
					);
					//$this->cartshopService->insertVoucherList($insAr);
					array_push($pay['list_payment'], $insAr);
					//$pay['list_payment'][$i] = $insAr;
					$pay['tot_hrs_dibayar'] = $sisaByr;
				}

				$pay['tot_nilai_voucher'] = $tot_nominal_vch;
				$pay['tot_pay'] = $tot_pay;
				$pay['listVch'] = $tbl;
			} else {
				$pay['list_payment'] = array();
				$insAr = array(
					"seqno" => 1,
					"orderno" => $summary['orderno'],
					"paytype" => "sgo",
					"docno" => "",
					"payamt" => $tot_hrs_dibayar,
					"bank_code_payment" => $data['bankid'],
					"charge_admin" => $summary['payAdm']
				);
				//$this->cartshopService->insertVoucherList($insAr);
				array_push($pay['list_payment'], $insAr);
				$pay['tot_pay'] = $tot_pay;
				$pay['tot_nilai_voucher'] = $tot_nominal_vch;
			}
		}

		return $pay;
	}

	function insertProduct($array) {
		//SET DATA PRODUK FREE (JIKA ADA)
		$data = $array['dataForm'];
		$total_weight = $array['total_sales_weight'];
		$total_item = 0;
		$arrx['product']= array();
		if(array_key_exists("promoAgust2018", $array) ) {
			if($array['promoAgust2018'] == "1" && array_key_exists("free_prdcd", $data)) {
				//TAMBAHKAN BERAT TOTAL PRODUK PEMBELANJAAN DENGAN TOTAL FREE PRODUK
				$total_weight += $data['total_free_weight'];
				$jumFreePrd = count($data['free_prdcd']);
				for($i = 0; $i < $jumFreePrd; $i++) {
					if($array['pricecode'] == "12W3") {
						$harga = $data['free_westPrice'][$i];
					} else {
						$harga = $data['free_eastPrice'][$i];
					}

					$arrx['product'][$i] = array(
						"orderno" => $array['trans_id'],
						"prdcd" => $data['free_prdcd'][$i],
						"prdnm" => $data['free_prdnm'][$i],
						"qty" => $data['free_qty'][$i],
						"bvr" => $data['free_bv'][$i],
						"dpr" => $harga,
						"pricecode" => $array['pricecode'],
						"sentTo" => $array['sentTo'],

					);
					$total_item += $data['free_qty'][$i];
					//array_push($arrx['product'], $res);
				}
			}
		}
		//SET DATA PRODUK SALES

		foreach($array['listProduct'] as $row) {

			$prdcd = $row['id'];
			$qty = (int)$row['qty'];
			$bv = (int)$row['bv'];
			$prdnm = $row['name'];
			if($array['pricecode'] == '12W3' && $array['dropship'] == "DROPSHIP"){
				$harga = (int)$row['west_Cprice'];
			} elseif($array['pricecode'] == '12W3' && $array['dropship'] != "DROPSHIP") {
				$harga = (int)$row['west_price'];
			} elseif($array['pricecode'] == '12E3' && $array['dropship'] == "DROPSHIP") {
				$harga = (int)$row['east_Cprice'];
			} else{
				$harga = (int)$row['east_price'];
			}

			$res = array(
				"orderno" => $array['trans_id'],
				"prdcd" => $prdcd,
				"prdnm" => $prdnm,
				"qty" => $qty,
				"bvr" => $bv,
				"dpr" => $harga,
				"pricecode" => $array['pricecode'],
				"sentTo" => $array['sentTo'],

			);
			$total_item += $qty;
			array_push($arrx['product'], $res);

		}
		//echo "<pre>";
		//print_r($array);
		//echo "</pre>";
		$arrx['total_weight'] = number_format($total_weight, 2);
		$arrx['total_item'] = $total_item;
		return $arrx;
		//END
	}

} //end controller
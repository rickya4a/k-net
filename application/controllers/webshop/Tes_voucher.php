<?php

    Class Tes_voucher extends MY_Controller
    {

        public function __construct()
        {

            parent::__construct();
            $this->load->model("webshop/Ecomm_trans_model", 'ecomm_trans_model');
            $this->load->model('webshop/shared_module', 'shared');
            $this->load->model('voucher/Claim_voucher_model', 'claim_voucher_model');
            $this->load->service("webshop/Cartshop_service", 'cartshopService');

        }

        public function getVoucher()
        {

            $dt['link'] = $this->input->post('link');
            $dt['voucher_no'] = $this->input->post('voucher_no');
            $voucher_no=$this->session->userdata('voucher_no');

            if ($dt['link'] != null && !isset($voucher_no)) {
                $newdata=array(
                    'voucher_no'=> $dt['voucher_no']
                );

                $this->session->set_userdata($newdata);
                //$dt['voucher_no'] = $this->my_counter->counterByFendi();
                //$dt['id_vch'] = $this->claim_voucher_model->getId($dt['voucher_no']);
                //$insert1 = $this->claim_voucher_model->insertVoucherNo($dt['voucher_no']);
                $this->load->view('landing-page/v_landing', $dt);

            }
            else if (isset($voucher_no)) {
                $dt['link'] = "http://www.k-net.co.id/simulasi";
                $dt['voucher_no'] = $voucher_no;
                $this->load->view('landing-page/v_landing', $dt);
            }
            else if($dt['link'] == null && !isset($voucher_no) )
            {
                //echo 'test';
//                $message = "Anda Tidak Dapat Mengakses Halaman Ini, Silahkan Ikuti Live Streaming Terlebih Dahulu";
//
//                echo "<script>
//                        alert('$message');
//                        //window.location.href='simulasi';
//                        window.location.href = '" . base_url() . "simulasi';
//                        </script>";

                echo "<script>
                        
                          //window.location.href='simulasi';
                          window.location.href = '" . base_url() . "snk';
                      </script>";
            }

        }

        public function claimVoucher()
        {

            $dt['email'] = $this->input->post('email');
			$idm = $this->input->post('idmemb');
			if($idm == "" || $idm == null) {
				$dt['idmemb'] = "0000999";
			} else {
				$dt['idmemb'] = $idm;
			}
            
            $dt['nama'] = $this->input->post('nama');
            $dt['status'] = $this->input->post('status');
            $dt['telp'] = $this->input->post('telp');
            $dt['voucher'] = $this->input->post('voucher');

            $dt['prdcd'] = $this->input->post('prdcd');
            $dt['prdcdnm'] = $this->input->post('prdcdnm');
            $dt['weight'] = $this->input->post('weight');
            $dt['price'] = $this->input->post('price');
            $dt['westPrice'] = $this->input->post('westPrice');
            $dt['eastPrice'] = $this->input->post('eastPrice');

            $dt['listCargo'] = $this->ecomm_trans_model->getListCargo();
            //print_r($dt);
/**
            if ($dt['idmember'] != null) {

                $login = array(
                    'idmember' => $dt['idmember'],
                    'nama' =>  $dt['nama'],
                    'email' => $dt['email'],
                    'telp' => $dt['telp'],
                    'userlogin'=> $dt['idmember'],
                    'dfno'=> $dt['idmember']

                );
                $this->session->set_userdata('store_info', $login);

//                $this->session->set_userdata('store_info', $login);
                $dt['idmemb'] = $dt['idmember'];
            } else {

                $login = array(
                    'nama' =>  $dt['nama'],
                    'email' => $dt['email'],
                    'telp' => $dt['telp']
                );

                $this->session->set_userdata('non_member_info', $login);
                $dt['idmemb'] = '0000999';
            }
**/


            //$insert1 = $this->claim_voucher_model->insertVoucherNo($dt['voucher']);

            //$kirim_sms = $this->claim_voucher_model->sendTrxSMS($dt['nama'],$dt['telp'],$dt['voucher']);

            //$dt['detail'] = $this->claim_voucher_model->getDetailVch($dt['id_vch']);
/**
            $newdata = array(

                'email' => $dt['email'],
                'idmemb' => $dt['idmemb'],
                'nama' => $dt['nama'],
                'status' => $dt['status'],
                'telp' => $dt['telp'],
                'voucher' => $dt['voucher'],

                'prdcd' => $dt['prdcd'],
                'prdcdnm' => $dt['prdcdnm'],
                'weight' => $dt['weight'],
                'price' => $dt['price'],
                'westPrice' => $dt['westPrice'],
                'eastPrice' => $dt['eastPrice'],
                'listCargo' => $dt['listCargo'],

            );
            $this->session->set_userdata($newdata);
**/
            $addCart = array(array(
                'id' => $this->input->post('prdcd'),
                'name' => $this->input->post('prdcdnm'),
                'qty' => 1,
                'bv' => 0,
                'west_price' => $this->input->post('westPrice'),
                'east_price' => $this->input->post('eastPrice'),
                'weight' => $this->input->post('weight'),

            ));
            //print_r($addCart);

            $this->cart->destroy();
            $insCart = $this->cart->insert($addCart);

            $dt['bns'] = $this->cartshopService->getCurrentPeriod();


            //print_r($dt);

            if ($dt['idmemb'] != null) {

                $insert2 = $this->claim_voucher_model->insertStatus2($dt['voucher'], $dt['nama'], $dt['email'], $dt['telp'], $dt['idmemb']);
            } else {

                $insert3 = $this->claim_voucher_model->insertStatus3($dt['voucher'], $dt['nama'], $dt['email'], $dt['telp']);
            }

            $this->setTempWebClaimVoucher('webshop/voucher/v_claimVoucher', $dt);

        }

        public function prosesVoucher()
        {

//            $dt['nm_daftar'] = $this->input->post('nm_daftar');
//            $dt['email'] = $this->input->post('email');
//            $dt['idmemb'] = $this->input->post('idmemb');
//            $dt['nama'] = $this->input->post('nama');
//            $dt['status'] = $this->input->post('status');
//            $dt['telp'] = $this->input->post('telp');
//            $dt['voucher'] = $this->input->post('voucher');
//
//            $dt['prdcd'] = $this->input->post('prdcd');
//            $dt['prdcdnm'] = $this->input->post('prdcdnm');
//            $dt['weight'] = $this->input->post('weight');
//            $dt['price'] = $this->input->post('price');
//            $dt['westPrice'] = $this->input->post('westPrice');
//            $dt['eastPrice'] = $this->input->post('eastPrice');
//
//            $dt['alamat'] = $this->input->post('alamat');
//            $dt['nama_stockist'] = $this->input->post('nama_stockistr1ref');


//            $dt['nama'] = $this->session->userdata('nama');
//            $dt['telpon'] = $this->session->userdata('telp');
//            $dt['email'] = $this->session->userdata('email');


            $dt = $this->input->post(null, true);

            $newdata = array(

                'email' => $dt['email'],
                'idmemb' => $dt['idmemb'],
                'nama' => $dt['nama'],
                'status' => $dt['status'],
                'telp' => $dt['telp'],
                'voucher' => $dt['voucher'],

                'prdcd' => $dt['prdcd'],
                'prdcdnm' => $dt['prdcdnm'],
                'weight' => $dt['weight'],
                'price' => $dt['price'],
                'westPrice' => $dt['westPrice'],
                'eastPrice' => $dt['eastPrice'],
                'alamat' => $dt['alamat'],

            );
            $this->session->set_userdata($newdata);

            $dt['listBank'] = $this->cartshopService->getBank();
            //$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
			//$dt['listBank'] = $this->cartshopService->getBank();
//            $data = array(
//
//                'delivery' => '2',
//                'nama_penerima' => $dt['nama'],
//                'email' => $dt['email'],
//                'notlp' => $dt['telp'],
//                'stkarea' => null,
//                'nama_stkarea' => null,
//                'stockist' => null,
//                'nama_stockist' => $dt['nama_stockist'],
//                'shipper' => null,
//                'provinsi' => null,
//                'nama_provinsi' => null,
//                'kota' => null,
//                'nama_kota' => null,
//                'kecamatan' => null,
//                'nama_kecamatan' => null,
//                'alamat' => $dt['alamat'],
//                'idmemberx' => $dt['idmemb'],
//                'membername' => $dt['nama'],
//                'bnsperiod' => null,
//                'id_lp' => null
//            );

            if($dt['idmemberx'] == "" || $dt['idmemberx'] == NULL || $dt['idmemberx'] == "0000999") {
				$login = array(
				  "userlogin" =>  $dt['nama_penerima'],
				  "usertelp" =>  $dt['notlp'],
				  "useremail" =>  $dt['email'],
				);
				$this->session->set_userdata('non_member_info', $login);
			} else {
				$book = new stdClass;
				$book->dfno = $dt['idmemberx'];
				$book->fullnm = $dt['membername'];
				$book->tel_hp = $dt['telp'];
				$login = array();
				array_push($login, $book);
				$this->session->set_userdata('store_info', $login);
				
				
			}

			    //production
				$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';	
	            //development 
	            //$dt['key'] = '0df5835ee198d49944c372ead860c241';
			
			
			   //production
				//$dt['key'] = '23536c365506cdaa587fb9c1833f23f0';	
				//$dt['key'] = '23536c365506cdaa587fb9c1833f23f0';	
                //development
                //$dt['key'] = '0df5835ee198d49944c372ead860c241';


            $reg = $this->cartshopService->sessionRegPersonalInfoBaru($dt);
            $ship = $this->cartshopService->setShippingInfoBaruNew();
          
            $dt['payID'] = "EN".randomNumber(8);
            $this->session->unset_userdata('pay_sgo_id');
            $this->session->set_userdata("pay_sgo_id", $dt['payID']);
            $insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);

            $this->session->set_userdata('pricecode', $dt['pricecode']);
			
			

            $dt['shipping_jne_info'] = $this->session->userdata('shipping_jne_info');
            //$personal_info = $this->session->userdata('personal_info');

            //print_r($dt);
            $this->setTempWebClaimVoucher('webshop/voucher/v_prosesVoucher', $dt);
        }

        public function pilihProduk()
        {
            $datax = $this->input->post(NULL, TRUE);	
            if($datax != null) {	
	
	            $dt['email'] = $this->input->post('email');
	            $dt['idmemb'] = $this->input->post('idmemb');
	            $dt['nama'] = $this->input->post('nama');
	            $dt['status'] = $this->input->post('status');
	            $dt['telp'] = $this->input->post('telp');
	            $dt['voucher'] = $this->input->post('vch');
	            //$dt['id_vch'] = $this->input->post('id_vch');
	
	            $newdata = array(
	
	                'email' => $dt['email'],
	                'idmemb' => $dt['idmemb'],
	                'nama' => $dt['nama'],
	                'status' => $dt['status'],
	                'telp' => $dt['telp'],
	                'voucher' => $dt['voucher'],
	                'dfno'=>$dt['idmemb'],
	                'fullnm'=>$dt['nama']
	
	            );
	            $this->session->set_userdata($newdata);
	
	            $text = "Selamat voucher ".$dt['voucher']." anda telah release, segera ikuti langkah selanjutnya dan lakukan pembayaran";
	            smsTemplate($dt['telp'], $text);
	
	            $dt['prd'] = $this->claim_voucher_model->getListPrd();
	            //print_r($dt['prd']);
	
	            $this->setTempWebClaimVoucher('webshop/voucher/v_pilihProduct', $dt);
	         } else {
	         	redirect('snk');
	         } 

        }

        //$route['vch/winner/sgo/pay/preview'] = 'webshop/tes_voucher/payPreview';
	    function payPreview() {
			$this->load->service("webshop/Cartshop_service",'cartshopService');
			$shipping_jne_info = $this->session->userdata('shipping_jne_info');
			$personal_info = $this->session->userdata('personal_info');
			$promo = $this->session->userdata('promo');
			$dt = $this->input->post(NULL, TRUE);
			$tot_pay = getTotalPayNet();
			
				$payShip = $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
				$shipDiscount = $shipping_jne_info['ship_discount'];
				$biaya = getTotalPayNetBaru();
			    $tot_hrs_dibayar = $biaya['total_pay'] + $payShip;
			    $dt['freeship'] = $biaya['freeship'];
			        $dt['id_memb'] = $personal_info['idmemberx'];
					$dt['idmember'] = $personal_info['idmemberx'];
					$dt['no_voucher'] = $personal_info['id_lp'];
					$dt['id_lp'] = $personal_info['id_lp'];
					$dt['nmmemb'] = $personal_info['membername'];
					$dt['link_home'] = 'quiz/winner/'.$personal_info['id_lp'];		
			   
			if(count($dt) > 0) {
				$dt['prodCat'] = $this->cartshopService->getProductCategory();
				$jml_voucher = isset($dt['shop_vch']) ? count($dt['shop_vch']) : 0;
				$dt['vch_stt'] = false;
				$dt['res'] = null;
				$dt['pay_gateway'] = true;
				$tot_nominal_vch = 0;
				//Jika pembayaran menggunakan voucher, sisa pembayaran menggunakan payment gateway
				//atau jika jumlah nominal voucher sama dengan nilai yang harus dibayar
				
				
				
				if($jml_voucher > 0) {
					$dt['vch_stt'] = true;
					$tempArr = array();
					$tbl = "<table width=70% border=1 cellspacing=1 cellpadding=1 align=center><tr><th>No</th><th>Voucher Code</th><th>Nominal</th></tr>";
					$this->cartshopService->deleteFromTable("ecomm_trans_paydet_sgo", "orderno", $dt['temp_paymentIdx']);
					$xc = 0;
					for($i = 0; $i < $jml_voucher; $i++) {
						$xc++;
						$tot_nominal_vch += $dt['shop_vch_nominal'][$i];
						//$xc = $i + 1;
						$tbl .= "<tr>";
						$tbl .= "<td align=right>".$xc."&nbsp;</td>";
						$tbl .= "<td align=center>".$dt['shop_vch'][$i]."</td>";
						$tbl .= "<td align=right>Rp.".$dt['shop_vch_nominal'][$i]."&nbsp;</td>";
						$tbl .= "</tr>";
						$insAr = array(
						  "seqno" => $xc,
						  "orderno" => $dt['temp_paymentIdx'],
						  "paytype" => "vch",
						  "docno" => $dt['shop_vch'][$i],
						  "payamt" => $dt['shop_vch_nominal'][$i],
						  "bank_code_payment" => 19,
						  "charge_admin" => 0
						);
						$this->cartshopService->insertVoucherList($insAr);
						
					}
					$tbl .= "<tr><td colspan=2>Total</td><td align=right>Rp.".$tot_nominal_vch."</td></tr>";
					$tbl .= "</table>";
					
					
					//array_push($arxx['arrayData'], $obj);
					$dt['listVoucher'] = null;
					//Jika total nominal voucher lebih besar atau sama dengan
					//nominal belanja yang harus dibayar
					if($tot_nominal_vch >= $tot_hrs_dibayar) {
						//status payment gateway = false, data Langsung insert ke database tanpa pembayaran
						$dt['pay_gateway'] = false;
						$arr = array(
							"bank_code_payment" => 19,
							"payShip" => $shipping_jne_info['price'],
							"disc_shipping" => $shipDiscount,
							"payAdm" => 0,
							"payConnectivity" => 0,
							"sentTo" => $personal_info['delivery'],
							"userlogin" => getUserID()
						);
						$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
						$dt['tot_hrs_dibayar'] = $tot_hrs_dibayar;
						
					} else {
					    //Jika total nominal voucher kurang dari total yang harus si bayar, sisa nya menggunakan SGO
						$arr = array(
							"bank_code_payment" => $dt['bankid'],
							"payShip" => $shipping_jne_info['price'],
							"disc_shipping" => $shipDiscount,
							"payAdm" => $dt['charge_admin'],
							"payConnectivity" => $dt['charge_connectivity'],
							"sentTo" => $personal_info['delivery'],
							"userlogin" => getUserID()
						);
						$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
						
						//Input sisa pembayaran via SGO karena jumlah yg hrs di bayar > jumlah total nilai voucher
						$sisaByr = $tot_hrs_dibayar - $tot_nominal_vch;
						$seqno = $xc + 1;
						$insAr = array(
						  "seqno" => $seqno,
						  "orderno" => $dt['temp_paymentIdx'],
						  "paytype" => "sgo",
						  "docno" => "",
						  "payamt" => $sisaByr,
						  "bank_code_payment" => $dt['bankid'],
						  "charge_admin" => $dt['charge_admin']
						);
						$this->cartshopService->insertVoucherList($insAr);
						$dt['tot_hrs_dibayar'] = $sisaByr;
					}
				
					$dt['tot_nilai_voucher'] = $tot_nominal_vch;
					$dt['tot_pay'] = $tot_pay;
					$dt['listVch'] = $tbl;
				} else {
					$arr = array(
						"bank_code_payment" => $dt['bankid'],
						"payShip" => $shipping_jne_info['price'],
						"disc_shipping" => $shipDiscount,
						"payAdm" => $dt['charge_admin'],
						"payConnectivity" => $dt['charge_connectivity'],
						"sentTo" => $personal_info['delivery'],
						"userlogin" => getUserID()
				    );
					$dt['res'] = $this->cartshopService->updateBankCodePayment2($dt['temp_paymentIdx'], $arr);
					
					$updPaySGO = array(
						  "orderno" => $dt['temp_paymentIdx'],
						  "bank_code_payment" => $dt['bankid'],
						  "charge_admin" => $dt['charge_admin']
					);
					$this->cartshopService->updatePaydetSGO($updPaySGO);
					
					$dt['tot_pay'] = $tot_pay;
					$dt['tot_nilai_voucher'] = $tot_nominal_vch;
						
				}
				
				$cust_memb = $this->session->userdata("non_member_info");
				if(isset($cust_memb)) {
					$dt['backURL'] = "http://www.k-net.co.id/livestream/vch/finish/".$dt['temp_paymentIdx'];	
				} else {
					$dt['backURL'] = "http://www.k-net.co.id/livestream/vch/finish/".$dt['temp_paymentIdx'];			
					 //$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
				}
										
				//if(getUserID() == "IDSPAAA66834") {
					//$dt['backURL'] = "http://www.k-net.co.id/livestream/vch/finish/".$dt['temp_paymentIdx'];
					$this->setTempWebShopLPSalesNoBV2('webshop/sales_winner/payment_sgo_vch_preview_dev',$dt);
					
					//$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
					//$this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
				/*} else {
					
						$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
					    $this->setTemplateSGO('webshop/payment_sgo_preview',$dt);
					
					
				}	*/
			} else {
				redirect('cart/list', 'refresh');
			} 
		}

		public function sendSMS(){
            $result = array();
            $this->db->select('no_telp, vch_no');
            $this->db->from('fendi_tes_listvoucher');
            $this->db->where('status', "Identified");

            $q = $this->db->get();
            //echo $this->db->last_query();
            foreach($q->result() as $row)
            {
                $vch= $row->vch_no;
                $telp= $row->no_telp;
                echo $telp." ".$vch."<br>";

                $text = "Selamat voucher ".$vch." anda telah release, segera ikuti langkah selanjutnya dan lakukan pembayaran";
                smsTemplate($telp, $text);

            }

            //return $result;

//            foreach($q->result() as $row)
//            {
//                $result[] = $row;
//            }
//
//

//
        }
    }
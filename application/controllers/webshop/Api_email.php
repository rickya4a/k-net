<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_email extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    //Request dari hris/hrd untuk kirim email ke karyawan
    function cekParam($data){

        if(!array_key_exists("subject", $data)){
            $result = array(
                        'response' => "false",
                        'message' => "subject kosong"
                         );

            return $result;
        }

        if(!array_key_exists("to", $data)){
            $result = array(
                'response' => "false",
                'message' => "email tujuan kosong"
            );

            return $result;
        }

        if(!array_key_exists("content", $data)){
            $result = array(
                'response' => "false",
                'message' => "content kosong"
            );

            return $result;
        }

        if(!array_key_exists("api_user", $data)){
            $result = array(
                'response' => "false",
                'message' => "api user kosong"
            );

            return $result;
        }

        if(!array_key_exists("api_pwd", $data)){
            $result = array(
                'response' => "false",
                'message' => "api pass kosong"
            );

            return $result;
        }

        $result = array(
            'response' => "true",
            'message' => "data valid"
        );

        return $result;

    }

    function cekApi($data){

        $api_user = "k-net.apps";
        $api_pwd = "apps@2017";

        if($data['api_user'] == $api_user && $data['api_pwd'] == $api_pwd){
            $result = array(
                'response' => "true",
                'message' => "api valid"
            );

            return $result;
        }else{
            $result = array(
                'response' => "false",
                'message' => "api tidak valid"
            );

            return $result;
        }
    }


    function sendEmail(){
//        $subject, $email_to, $content
        $data = $this->input->post(NULL, TRUE);

        $a = $this->cekParam($data);
        if($a['response'] == "false"){
            echo json_encode($a);
            return;
        }

        $b = $this->cekApi($data);
//        print_r($a);
        if($b['response'] == "false"){
            echo json_encode($b);
            return;
        }

        $result = emailSend($data['subject'], $data['to'], $data['content']);

        $json = json_decode($result);

        if($json->message == 'SUCCESS'){
            $result = array(
                'response' => "true",
                'message' => "email berhasil dikirim"
            );
        }
        else{
            $result = array(
                'response' => "false",
                'message' => "email gagal dikirim"
            );
        }

        echo json_encode($result);

    }

}
?>
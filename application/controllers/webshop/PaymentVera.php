<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class PaymentVera extends MY_Controller{

    public function __construct(){

        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        $this->usr_api = "k-net.apps";
        $this->pwd_api = "apps@2017";
        $this->salt = "knet";
        $this->dataForm = $this->input->post(NULL, TRUE);
        $this->output->set_header( "Access-Control-Allow-Origin: *" );
        $this->output->set_header( "Access-Control-Allow-Credentials: true" );
        $this->output->set_header( "Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS" );
        $this->output->set_header( "Access-Control-Max-Age: 604800" );
        $this->output->set_header( "Access-Control-Request-Headers: x-requested-with" );
        $this->output->set_header( "Access-Control-Allow-Headers: x-requested-with, x-requested-by" );

        $this->load->model('lokasi/M_location');
        $this->load->model("webshop/Sales_model",'salesM');
        $this->load->model('webshop/ecomm_trans_model', 'mm_trans');

        $this->load->service("webshop/Product_service",'productService');
        $this->load->service("webshop/Payment_service",'paymentService');
        $this->load->service("webshop/Knet_payment_service",'KnetpaymentService'); //add to tabel real
    }

    public function getUniqueCode(){
        $orderno= $this->input->post('orderno');
        $nominal = $this->input->post('nominal');
        $norek = $this->input->post('norek');
        $nmrek = $this->input->post('nmrek');
        $type = $this->input->post('type');

        //getTrans
        /*$res= $this->M_location->getTrans($orderno,$nominal);
        if($res != null){

        }*/

        $dt = $this->getUniqueCode($orderno,$nominal,$norek,$nmrek,$type);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function status_update() {
        //get authorization
        $qry= $this->M_location->getAuth();
        $auth= $qry[0]->authorization;
        $content="application/json";
        $accept="application/json";

        //cek authorization
        // $auth= "gojek1234";
        $myHeaders = $this->input->request_headers();

        $headers_content= $myHeaders["Content-Type"];
        $headers_accept= $myHeaders["Accept"];
        $headers_auth= $myHeaders["Authorization"];

        //get request data
        $json = file_get_contents('php://input');
        $result= json_decode($json, TRUE);

        if($headers_auth == $auth){

            $kd_pay=$result["kd_pay"];
			$nom= $result["tot_bayar"];
			$date_time= $result["date_time"];
			$jam_bayar=date("Y-m-d H:i:s", strtotime($date_time));
			$nominal = $nom * 1;

			/*echo $kd_pay;
			echo "<br>";
			echo $nom;
			echo "<br>";
			echo $date_time;
			echo "<br>";
			echo $jam_bayar;
			echo "<br>";*/

			$res= $this->M_location->cekTrans1($kd_pay,$nom);
			if($res != null){
				foreach($res as $row2) {
					$order_no=$row2->orderno;
					$payship = $row2->payShip;
					$kode_unik = $row2->kode_unik;
				}

				$this->M_location->updateFlag($kd_pay,$nominal,$order_no);  //update flag payment

				$norek= $result["norek"];
				$tgl_pay=date("Y-m-d", strtotime($date_time));
				$time_pay=date("H:i:s", strtotime($date_time));


				//get orderno
				$res= $this->M_location->cekTrans2($kd_pay,$nominal);
				if($res != null){
					foreach($res as $row1){
						$tot_pay= $row1->total_pay;
						$datetrans = $row1->datetrans;
						$flag = $row1->flag_payment;
						$date_expired = $row1->date_expired;


						$tgl_trans=date("Y-m-d", strtotime($datetrans));
						$time_trans=date("H:i:s", strtotime($datetrans));
						$time_expired=date("H:i:s", strtotime($date_expired));

						/*if($tgl_pay > $tgl_trans){
                            $expired ="1";
                        }elseif($time_pay > $time_expired){
                            $expired= "1";
                        }else{
                            $expired= "0";
                        }*/

						//edit hilal @2019-03-23
						if($tgl_pay > $tgl_trans){
							$expired ="1";
						}else{
							$expired= "0";
						}
						//echo $expired;

						$arr['no']=substr($order_no,0,2);
						$xx['order_id']= $order_no;


						//Bila transaksi adalah pembelanjaan online
						$arr['dta']= $this->KnetpaymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
						if($arr['no'] == "EC" || $arr['no'] == "SC" || $arr['no'] == "EA") {

							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->load->model("webshop/Payment_model",'pmodel');
								$updtTabel = $this->KnetpaymentService->updateTrxSGO_baru($xx['order_id']);
								$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);

								$this->KnetpaymentService->sendTrxSMS2($resultInsert);
								sendNotifSalesTransaction($resultInsert);

								$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
								if($data_catalog != null) {

									emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
									foreach($data_catalog as $row){
										$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
										smsTemplate( $resultInsert[0]->tel_hp_login, $link);
										smsTemplate( $resultInsert[0]->tel_hp_as, $link);
									}

								}
								/*
                                if($arr['no'] == "EA") {
                                    $this->blastNotif($resultInsert[0]->orderno);
                                }
                                */

								//berlaku hanya untuk 1 september 2018 - 30 november 2018
								if($arr['no']=="EC"){

									$count=$this->pmodel->getPertama($xx['order_id'],$resultInsert[0]->total_pay,$resultInsert[0]->total_bv,$resultInsert[0]->id_memb,$resultInsert[0]->tel_hp_login,$resultInsert[0]->orderno);
								}
								//echo $this->setPaymentNotif($arr, $xx);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted="Berhasil";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);


							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$expired = "0";
								$noted="Double";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							}

						}
						//Bila transaksi adalah pembelanjaan knet plus registrasi member
						else if($arr['no'] == "EM") {
							//transaksiKnetPlusMemb
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->transaksiKnetPlusMemb($xx, $arr);

								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted="Berhasil";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);


							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);

								$success = "0";
								$expired = "0";
								$noted="Double";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							}

						}
						//Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
						else if($arr['no'] == "EN") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$updtTabel = $this->KnetpaymentService->updateTrxSGO($xx['order_id']);
								$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);
								$this->KnetpaymentService->sendTrxSMSNonMember($resultInsert);
								//echo $this->setPaymentNotif($arr, $xx);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted="Berhasil";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);


							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$expired = "0";
								$noted="Double";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							}
						}
						//bila transaksi adalah Registration Member / Landing Page Registration
						else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$updtTabel =$this->KnetpaymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
								if($updtTabel['response'] == "true") {
									$resultInsMemb = $this->KnetpaymentService->getNewMemberData($xx['order_id']);
									//$this->KnetpaymentService->createNewShoppingVoucher($resultInsMemb);
									$this->KnetpaymentService->sendTrxSmsRegMember($resultInsMemb);
									//$this->KnetpaymentService->sendWelcomeEmail($resultInsMemb);
									sendWelcomeEmail($resultInsMemb);

									//echo $this->setPaymentNotif($arr, $xx);
									$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
									$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

									$success= "1";
									$noted="Berhasil";
									$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
									echo json_encode($arr);


								} else {
									//echo json_encode($updtTabel);
									$resSukses = array(
										'success_flag' => 1,
										'error_message' => $updtTabel['message'],
										'error_code' => "003",
										'order_id' => $xx['order_id'],
										'reconcile_datetime' =>date('Y-m-d H:i:s')
									);
									//echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];

									$success= "0";
									$expired= "0";
									$noted="Gagal Response";
									$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
									echo json_encode($arr);

								}
							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$expired = "0";
								$noted="Double";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							}
						}
						elseif($arr['no'] == "ED") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->load->model("webshop/Payment_model",'pmodel');
								$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
								$updtTabel = $this->KnetpaymentService->updateTrxSGO_baruDIGITAL($xx['order_id']);
								$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);
								$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($xx['order_id']);

								$this->KnetpaymentService->sendTrxSMS2($resultInsert);

								$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
								if($data_catalog != null){

									emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
									foreach($data_catalog as $row) {
										$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
										smsTemplate( $identitas[0]->telp_customer, $link);
									}

								}
								//echo $this->setPaymentNotif($arr, $xx);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted="Berhasil";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);


							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);

								$success = "0";
								$expired = "0";
								$noted="Double";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							}
						}

						//Transaksi K-net Non Member "Mampir Kak"
						else if($arr['no'] == "EP") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->transaksiMampirKak($xx, $arr);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted="Berhasil";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);


							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);

								$success = "0";
								$expired = "0";
								$noted="Double";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							}
						}

						//bila transaksi adalah Online Stockist Payment
						else if($arr['no'] == "ST") {

						}

						//bila transaksi adalah Ticket Online  Payment
						else if($arr['no'] == "TA") {
							//$createIP = $this->mdtc->generateIP($xx['order_id']);
							//print_r($createIP);

							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->load->model("dtc/mdtc_mbr", "mdtc");
								$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
								$createIP = $this->mdtc->generateIP($xx['order_id']);
								//echo $this->setPaymentNotif($arr, $xx);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted="Berhasil";
								$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
								echo json_encode($arr);

							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$expired = "0";
								$noted="double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired","note" => $noted);
								echo json_encode($arr);
							}
						}

						else if($arr['no'] == "PS") {
							$this->processPpobTrxSGO($xx, $arr);
						}

						else if($arr['no'] == "PX") {
							$this->processXlDataSGO($xx, $arr);
						}

					}

					/*$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
					$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

					$success= "1";
					$noted= "Berhasil";
					$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note" => $noted);
					echo json_encode($arr);*/

				}else {
					$success = "0";
					$expired = "0";
					$noted="Kode Order Tidak Ditemukan";
					$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired","note" => $noted);
					echo json_encode($arr);
				}

			}else{
				$success = "0";
				$expired = "0";
				$noted= "Transaksi Sudah Dibayar";
				$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired","note" => $noted);
				echo json_encode($arr);
			}

		}else{
			$success="0";
			$expired= "0";
			$noted= "Koneksi Gagal";
			$arr = array("koneksi" =>"0","success" => "$success","expired" => "$expired","note" => $noted);
			echo json_encode($arr);
		}
    }

	public function status_update2() {
		//get authorization
		$qry= $this->M_location->getAuth();
		$auth= $qry[0]->authorization;
		$content="application/json";
		$accept="application/json";

		//cek authorization
		// $auth= "gojek1234";
		$kd_pay= $this->input->post('kd_pay');
		$nom = $this->input->post('tot_bayar');
		$norek = $this->input->post('norek');
		$datepay = $this->input->post('date_time');
		//$jam_bayar=date("Y-m-d H:i:s", strtotime($datepay));
		$jam_bayar = date('Y-m-d H:i:s', strtotime('-1 hour', strtotime($datepay))); //pengurangan 1 jam

		//echo $kd_pay."/";
		//echo $nom."/";
		//echo $datepay."/";
		//echo $jam_bayar;

		if($kd_pay!='' AND $nom !='' AND $norek !='' AND $datepay !=''){

			$res= $this->M_location->cekRTrans1($kd_pay,$nom,$jam_bayar);
			if($res != null){
				foreach($res as $row2) {
					$order_no=$row2->orderno;
					$kode_unik = $row2->kode_unik;
					$payship = $row2->payShip;
				}

				//$nom= $nominal;
				//$nominal = ($nom * 1) - ($kode_unik * 1);
				$nominal = $nom * 1;
				$norek= $norek;
				$date_time= $datepay;

				$tgl_pay=date("Y-m-d", strtotime($date_time));
				$time_pay=date("H:i:s", strtotime($date_time));


				//$this->KnetpaymentService->updateTrxSGO_baru($order_no); //insert ke table real
				$this->M_location->updateFlag($kd_pay,$nominal,$order_no);  //update flag payment

				//get orderno
				// $res= $this->M_location->cekTrans($kd_pay,$nominal);
				$res= $this->M_location->cekRTrans2($kd_pay,$nominal);
				if($res != null){
					foreach($res as $row1){
						$tot_pay= $row1->total_pay;
						$datetrans = $row1->datetrans;
						$flag = $row1->flag_payment;
						$date_expired = $row1->date_expired;


						$tgl_trans=date("Y-m-d", strtotime($datetrans));
						$time_trans=date("H:i:s", strtotime($datetrans));
						$time_expired=date("H:i:s", strtotime($date_expired));

						/*
                        echo "tgl_trans = $tgl_trans";
                        echo "tgl_pay = $tgl_pay";
                        echo "time_trans = $time_trans";
                        echo "time_expired = $time_expired";
                        */
						/* //ori vera
                        if($tgl_pay > $tgl_trans){
                            $expired ="1";
                        }elseif($time_pay > $time_expired){
                            $expired= "1";
                        }else{
                            $expired= "0";
                        }
                         */

						//edit hilal @2019-03-23
						if($tgl_pay > $tgl_trans){
							$expired ="1";
						}else{
							$expired= "0";
						}
						//echo $expired;

						$arr['no']=substr($order_no,0,2);
						$xx['order_id']= $order_no;

						//Bila transaksi adalah pembelanjaan online
						$arr['dta']= $this->KnetpaymentService->getDataPaymentSGOByOrderID_withVch($xx['order_id']);
						if($arr['no'] == "EC" || $arr['no'] == "SC" || $arr['no'] == "EA") {

							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->load->model("webshop/Payment_model",'pmodel');
								$updtTabel = $this->KnetpaymentService->updateTrxSGO_baru($xx['order_id']);
								$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);

								$this->KnetpaymentService->sendTrxSMS2($resultInsert);
								sendNotifSalesTransaction($resultInsert);

								$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
								if($data_catalog != null) {

									emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
									foreach($data_catalog as $row){
										$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
										smsTemplate( $resultInsert[0]->tel_hp_login, $link);
										smsTemplate( $resultInsert[0]->tel_hp_as, $link);
									}

								}
								/*
                                if($arr['no'] == "EA") {
                                    $this->blastNotif($resultInsert[0]->orderno);
                                }
                                */

								//berlaku hanya untuk 1 september 2018 - 30 november 2018
								if($arr['no']=="EC"){

									$count=$this->pmodel->getPertama($xx['order_id'],$resultInsert[0]->total_pay,$resultInsert[0]->total_bv,$resultInsert[0]->id_memb,$resultInsert[0]->tel_hp_login,$resultInsert[0]->orderno);
								}
								//echo $this->setPaymentNotif($arr, $xx);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted= "Berhasil";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);

							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);

								$success = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);

							}

						}
						//Bila transaksi adalah pembelanjaan knet plus registrasi member
						else if($arr['no'] == "EM") {
							//transaksiKnetPlusMemb
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->transaksiKnetPlusMemb($xx, $arr);


								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted= "Berhasil";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);

							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
								echo json_encode($arr);
							}

						}
						//Bila transaksi adalah pembelanjaan online untuk NON-MEMBER
						else if($arr['no'] == "EN") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$updtTabel = $this->KnetpaymentService->updateTrxSGO($xx['order_id']);
								$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);

								$this->KnetpaymentService->sendTrxSMSNonMember($resultInsert);
								//echo $this->setPaymentNotif($arr, $xx);

								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted= "Berhasil";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);

							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
								echo json_encode($arr);
							}
						}
						//bila transaksi adalah Registration Member / Landing Page Registration
						else if($arr['no'] == "RM" || $arr['no'] == "RC" || $arr['no'] == "LP" || $arr['no'] == "RA") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$updtTabel =$this->KnetpaymentService->saveMemberSGO($xx['order_id'], $arr['dta']);
								if($updtTabel['response'] == "true") {
									$resultInsMemb = $this->KnetpaymentService->getNewMemberData($xx['order_id']);
									//$this->KnetpaymentService->createNewShoppingVoucher($resultInsMemb);
									$this->KnetpaymentService->sendTrxSmsRegMember($resultInsMemb);
									//$this->KnetpaymentService->sendWelcomeEmail($resultInsMemb);
									sendWelcomeEmail($resultInsMemb);

									//echo $this->setPaymentNotif($arr, $xx);

									$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
									$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

									$success= "1";
									$noted= "Berhasil";
									$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
									echo json_encode($arr);

								} else {
									//echo json_encode($updtTabel);
									$resSukses = array(
										'success_flag' => 1,
										'error_message' => $updtTabel['message'],
										'error_code' => "003",
										'order_id' => $xx['order_id'],
										'reconcile_datetime' =>date('Y-m-d H:i:s')
									);
									//echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];

									$success= "0";
									$noted= "No Response";
									$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
									echo json_encode($arr);

								}
							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);

								$success = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
								echo json_encode($arr);
							}
						}
						elseif($arr['no'] == "ED") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->load->model("webshop/Payment_model",'pmodel');
								$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
								$updtTabel = $this->KnetpaymentService->updateTrxSGO_baruDIGITAL($xx['order_id']);
								$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);
								$identitas = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($xx['order_id']);

								$this->KnetpaymentService->sendTrxSMS2($resultInsert);

								$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
								if($data_catalog != null){

									emailSend('Katalog Digital K-Link', $identitas[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
									foreach($data_catalog as $row) {
										$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
										smsTemplate( $identitas[0]->telp_customer, $link);
									}

								}
								//echo $this->setPaymentNotif($arr, $xx);

								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted= "Berhasil";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);


							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
								echo json_encode($arr);
							}
						}

						//Transaksi K-net Non Member "Mampir Kak"
						else if($arr['no'] == "EP") {
							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->transaksiMampirKak($xx, $arr);


								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$noted= "Berhasil";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);

							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);

								$success = "0";
								$expired = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
								echo json_encode($arr);
							}
						}

						//bila transaksi adalah Online Stockist Payment
						else if($arr['no'] == "ST") {

						}

						//bila transaksi adalah Ticket Online  Payment
						else if($arr['no'] == "TA") {
							//$createIP = $this->mdtc->generateIP($xx['order_id']);
							//print_r($createIP);

							$double = $this->KnetpaymentService->checkDoubleTokenTransHdr($xx['order_id']);
							if($double == null) {
								$this->load->model("dtc/mdtc_mbr", "mdtc");
								$dta = $this->mdtc->updateTrxSGO($xx['order_id']);
								$createIP = $this->mdtc->generateIP($xx['order_id']);
								//echo $this->setPaymentNotif($arr, $xx);
								$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
								$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

								$success= "1";
								$expired = "0";
								$noted= "Berhasil";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Berhasil Reconcile..");
								echo json_encode($arr);

							} else {
								//echo $this->setErrorDoubleId($xx['order_id']);
								$success = "0";
								$expired = "0";
								$noted= "Double";
								$arr = array("koneksi" => "1", "success" => "$success", "expired" => "$expired", "note" => $noted,"message"=>"Gagal Reconcile..");
								echo json_encode($arr);
							}
						}

						else if($arr['no'] == "PS") {
							$this->processPpobTrxSGO($xx, $arr);
						}

						else if($arr['no'] == "PX") {
							$this->processXlDataSGO($xx, $arr);
						}

					}

					$this->M_location->updateFlag2($kd_pay,$xx['order_id']);  //update flag production
					$this->M_location->updateNewTrh($kode_unik,$xx['order_id']);  //update Newtrh

					$success= "1";
					$expired= "0";
					$noted="Berhasil";
					$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note"=>$noted,"message"=>"Berhasil Reconcile..");
					echo json_encode($arr);

				}else {
					$success = "1";
					$expired = "0";
					$noted="Gagal Simpan";
					$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note"=>$noted,"message"=>"Gagal Reconcile..");
					echo json_encode($arr);
				}

			}else{
				$success="0";
				$expired= "1";
				$noted="Kode Order Tidak Ditemukan";
				$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note"=>$noted,"message"=>"Gagal Reconcile..");
				echo json_encode($arr);
			}


		}else{
			$success="0";
			$expired= "0";
			$noted="Parameter Tidak Komplit";
			$arr = array("koneksi" =>"1","success" => "$success","expired" => "$expired","note"=>$noted,"message"=>"Gagal Reconcile..");
			echo json_encode($arr);
		}
	}

	function transaksiKnetPlusMemb($xx, $arr) {
		$this->load->model("webshop/Payment_model",'pmodel');
		$updtTabel = $this->KnetpaymentService->updateTrxSGO_baru($xx['order_id']);
		$resultInsert = $this->KnetpaymentService->getInsertTrxOrderID($xx['order_id']);
		if($resultInsert != null) {
			$insMemb = $this->KnetpaymentService->transaksiKnetPlusMemb($xx['order_id'], $resultInsert);

			$data_catalog=$this->pmodel->getKATALOG($xx['order_id']);
			if($data_catalog != null){
				//					$dta['catalog']=$data_catalog;
				//					$dta['token']=$xx['order_id'];
				emailSend('Katalog Digital K-Link', $resultInsert[0]->email, KatalogEmail($xx['order_id'], $data_catalog));
				//echo "<pre>";
				//print_r($data_catalog);
				//echo "</pre><br />";
				foreach($data_catalog as $row){
					$link="Download katalog di : ".base_url('catalogue/download/'.$xx['order_id'].'/'.$row->cat_inv_id);
					smsTemplate( $resultInsert[0]->tel_hp_login, $link);
					smsTemplate( $resultInsert[0]->tel_hp_as, $link);
				}

			}

			if($resultInsert != null) {
				//$this->paymentService->sendTrxSMS2($resultInsert);
				$this->KnetpaymentService->sendSMSPromoSalesAndReg($resultInsert);
			}

			if($insMemb == "true") {
				$this->KnetpaymentService->getRegMemberKnetPlus($resultInsert[0]->orderno);
			}
			echo $this->setPaymentNotif($arr, $xx);
		} else {
			$resSukses = array(
				'success_flag' => 1,
				'error_message' => "Trx K-net plus Reg member Gagal",
				'error_code' => "110",
				'order_id' => $xx['order_id'],
				'reconcile_datetime' =>date('Y-m-d H:i:s')
			);
			$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
			die($fail);
		}
	}

	function transaksiMampirKak($xx, $arr) {
		$hasil = $this->KnetpaymentService->processTrxMampirKak($xx['order_id']);
		$resultInsert = $this->KnetpaymentService->getInsertTrxMampirKak($xx['order_id']);
		//print_r($resultInsert);
		if($resultInsert != null) {
			if($resultInsert[0]->new_member_reg != null && $resultInsert[0]->new_member_reg != "") {
				$this->KnetpaymentService->sendSmsRegMembMampirKak($resultInsert);
			}
			$this->KnetpaymentService->sendTrxSmsMampirKak($resultInsert);
			echo $this->setPaymentNotif($arr, $xx);
		} else {
			//$fail = $this->setPaymentNotifFail("Trx failed to process", $xx);
			$resSukses = array(
				'success_flag' => 1,
				'error_message' => "Trx Mampirkak Gagal",
				'error_code' => "110",
				'order_id' => $xx['order_id'],
				'reconcile_datetime' =>date('Y-m-d H:i:s')
			);
			$fail = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['error_code'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
			die($fail);
		}
	}

    public function afterBayar() {

        //TAMBAHAN ANA 31/03/2016
        $this->cart->destroy();
        // END TAMBAHAN

        redirect('shop/product');
        //$this->setTemplateDevSGO('webshop/promo_bundle/pay_ecommerce_result',$dta);
        //print_r($dt['prodCat']);
    }

    function downloader($token, $produk) {
        $this->load->model("webshop/Payment_model",'pmodel');
        $data_catalog=$this->pmodel->getDownload($token, $produk);
        if($data_catalog == null){
            redirect('shop/product');
        }else

            force_download($data_catalog[0]->download_filename,NULL);
    }

	private function setPaymentNotif($arr, $xx) {
		$resSukses = array(
			'success_flag' => 0,
			'error_message' => $arr['error_message'],
			'reconcile_id' => rand(15,32),
			'order_id' => $xx['order_id'],
			'reconcile_datetime' =>date('Y-m-d H:i:s')
		);
		$hasil = $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];
		return $hasil;
	}
}
?>
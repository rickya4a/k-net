<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Sales
 *  *
 * @author ananech
 */
class Sales extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("webshop/Sales_model",'salesM');
        $this->load->service("webshop/Product_service",'productService');
        $this->load->service("webshop/Sales_service",'salesService');
        $this->load->service("webshop/Member_service",'memberService');
        $this->load->service("webshop/Redis_service",'redisService');


    }


    function getSalesMember(){
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            //$dt['bonusmont'] = $this->salesService->getBonusMonth();
            //print_r($dt['listBank']);
            $this->setTempWebShop('webshop/salesMember', $dt);
        }
    }

    function postSalesMember(){
        if($this->_checkSessionStoreUser()) {
            //$data = $this->input->post(NULL, TRUE);
            $bulan = $this->input->post('bulan');
            $tahun = $this->input->post('tahun');
            $dt['bln'] = $this->input->post('bulan');
            $dt['thn'] = $this->input->post('tahun');
            $dt['cekTrans'] = $this->salesService->getSalesMember($bulan,$tahun);
            $this->load->view('webshop/sales_detail',$dt);
        }
    }

    function detailSalesMember($trcd,$orderno,$tipe,$bln,$thn){
        //function detailSalesMember(){
        if($this->_checkSessionStoreUser()) {

            //echo "isinya ".$trcd." - ".$orderno." - ".$tipe." ";

            $tes = substr($trcd, 0, 3);
            $tes0 = substr($trcd, 0, 2);
            $tes4 = substr($orderno, 0, 4);
            if($tes == "IDE" || $tes4 == "IDLW" )
            {
                $tipe2 = "1";
            }
            elseif($tes == "IDI")
            {
                $tipe2 = "2";
            }
            elseif($tes == "ID1" || $tes0 == "CV"){
                $tipe2 = "4";
            } elseif($tes0 == "ID") {
				$tipe2 = "2";
			}
            else
            {
                $tes2 = substr($orderno, 0, 4);
                if($tes2 == "IDMW" or $tes2 == "IDSW")
                {
                    $tipe2 = "4";
                }
                else
                {
                    $tipe2 = "2";
                }
            }
            $dt['period'] = $bln."/".$thn;
            $dt['detTrans'] = $this->salesService->detailSalesMember($trcd,$orderno,$tipe2);
            $this->load->view('webshop/detail_produk',$dt);
        }
    }

    function getSalesMemberOnline(){
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            //$dt['bonusmont'] = $this->salesService->getBonusMonth();
            //print_r($dt['listBank']);
            $this->setTempWebShop('webshop/salesOnlineMember', $dt);
        }
    }

    function postSalesMemberOnline(){
        if($this->_checkSessionStoreUser()) {
            //$data = $this->input->post(NULL, TRUE);

            $dt['bln'] = $this->input->post('bulan');
            $dt['thn'] = trim($this->input->post('tahun'));
            if(is_numeric($dt['thn']) == false){
                echo "Format Tahun Salah";
            }else{
                $dt['bnsperiod'] = $dt['bln']."/".$dt['thn'];
                //$dt['cekTrans'] = $this->salesService->getSalesMemberOnline($dt['bnsperiod']);
				$dt['cekTrans'] = $this->salesService->getSalesMemberOnline2(getUserId(), $dt['bnsperiod']);
                //print_r($dt['cekTrans']);
				$this->load->view('webshop/sales_detailOnline',$dt);
            }
        }
    }

    function detailSalesMemberOnline($orderno,$bln,$thn){
        if($this->_checkSessionStoreUser()) {
            $dt['period'] = $bln ."/".$thn;
            //$dt['detTrans'] = $this->salesService->detailSalesMemberOnline($orderno,$dt['period']);
			$dt['detTrans'] = $this->salesService->detailSalesMemberOnline2(getUserId(),$orderno,$dt['period']);
			
            $this->load->view('webshop/detail_produkOnline',$dt);
        }
    }


    function getDirectDownline(){
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $dt['bnsmonth'] = $this->salesService->getBonusmonths();
            $this->setTempWebShop('webshop/get_downlineInfo', $dt);
        }
    }
    function getRecruiterBonus(){
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $dt['dari']=date("Y-m-d") ;
            $dt['ke']=date("Y-m-d") ;

            $this->setTempWebShop('webshop/get_recInfo', $dt);
        }
    }



    function getDirectDownlineInfo(){
        if($this->_checkSessionStoreUser()) {

            $idmembers = getUserID();

            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $dt['cekDownline'] = $this->salesService->getDirectDownline1($idmembers,$month,$year);
            $dt['sponsor'] = $idmembers;
            $this->load->view('webshop/get_downlineInfo1',$dt);
        }
    }
    function getDataRecruit(){
        if($this->_checkSessionStoreUser()) {

            $idmembers = getUserID();
            //$dt['sfno']=null;

            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $dari = $this->input->post('dari');
            $ke = $this->input->post('ke');

            $dts = $this->salesM->dt_diri_rec($idmembers);

            $dt['dt_table'] = $this->salesM->get_bonus($idmembers,$dari,$ke);

            if($dts != null){
                foreach($dts as $row){
                    $dt['dfno']=$row->dfno;
                    $dt['acc_no']=$row->bankaccno;
                    $dt['acc_nm']=$row->bankaccnm;
                    $dt['acc_bank']=$row->banknm;
                }
            }
//
//            print_r($dts);
//            echo ($dts);
//            echo $idmembers;

            $this->load->view('webshop/get_recInfo1',$dt);
        }
    }

    function getDetailRekrut(){

        //$x = $this->input->post('dfno');
        //$idmember = getUserID();

        $idmember = $this->input->post('dfno');
        $dari = $this->input->post('date1');
        $ke = $this->input->post('date2');

        $dt['detail'] = $this->salesM->get_detail($idmember, $dari, $ke);

        $this->load->view('webshop/get_detailrekrut',$dt);
    }


    function getDirectDownlineInfo2($idmember,$month,$year){
        if($this->_checkSessionStoreUser()) {

            /*$bonusmonth = $this->input->post('bonusmonth');
            $month = substr($bonusmonth,5,2);
            $year = substr($bonusmonth,0,4);*/
            //echo $idmember." >> ".$month." >> ".$year;
            $dt['cekDownline'] = $this->salesService->getDirectDownline1($idmember,$month,$year);
            if($dt['cekDownline']['pbv'] == 'no data' or $dt['cekDownline']['pbv']==null) {
                $res = $this->salesService->getSponsorCode($idmember);
                $dt['sponsor'] = $res[0]->sfno;
            }
            
            $this->load->view('webshop/get_downlineInfo1',$dt);
        }
    }

    function getPrintJaringan(){
        $idmember = $this->input->post('idmember');
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $dt['cekDownline'] = $this->salesService->getDirectDownline1($idmember,$month,$year);
        $this->load->view('webshop/printDownlinePDF',$dt);
    }

    function getformBonusMember(){
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            //$dt['bonusmont'] = $this->salesService->getBonusMonth();
            //print_r($dt['listBank']);
            $this->setTempWebShop('webshop/stBonusReport', $dt);
        }
    }

    /*
        --with RPC--
    function postBonusMember(){
        if($this->_checkSessionStoreUser()) {
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $dt['month'] = $this->input->post('month');
            $dt['year'] = $this->input->post('year');
            $dt['cekBonus'] = $this->salesService->getBonusMember($month,$year);
           
           $curr = $dt['cekBonus']['dtCurrency'];
                $dt['currency'] = $curr[0]->exchangerate;
            if($dt['cekBonus']['dtBonus'] == "no data" && $dt['cekBonus']['dtMember'] == "no data")
			  {
                echo "no data";
		      }else{
		          
                
                $dt['res1'] = $dt['cekBonus']['dtMember'];
                $dt['hasil1'] = $dt['cekBonus']['dtMonthly'];  
    		    $dt['hasil2'] = $dt['cekBonus']['dtDownline'];
    			$dt['hasil3'] = $dt['cekBonus']['dtSelfBonus'];
				  foreach($dt['cekBonus']['dtBonus'] as $dd)
				  {
				  	  if($dd->netincome > 0)
				  	  {
				  	  	$dt['res2'] = $dt['cekBonus']['dtBonus'];
				  	  }	
                      
				  	  if($dd->gpincome > 0)
					  {
						$dt['devresult'] = $dt['cekBonus']['dtDevelopBonus'];
					  } 
					  if($dd->ldbincome > 0)
					  {
						$dt['ldbresult'] = $dt['cekBonus']['dtLeaderShip'];
					  }
					  
                      $unilevel = $dd->planbincome + $dd->addinfinityincome;
					
				  		if($dd->infinityincome > 0)
					  	{
							$dt['hasil4'] = $dt['cekBonus']['dtInfinityBonus'];
					  	} 
					  	if($unilevel  > 0)
					  	{
							$dt['hasil5'] = $dt['cekBonus']['dtUnilevelBonus'];
							$dt['hasil6'] = $dt['cekBonus']['dtLevel'];
					  	}
				  }	
                  if($dt['cekBonus']['dtBonusVoucher'] != 0)
    			  {
    			  	$dt['voucher'] = $dt['cekBonus']['dtBonusVoucher'];
    			  }
                $this->load->view('webshop/printBonusReportPdf',$dt);
		          
		      }
        }
    }*/
    
    //$route['bonus/cekvalidmember'] = 'webshop/sales/cekvalidmember';
    function cekvalidmember() {
    	$month = $this->input->post('month');
		$bonusyear = $this->input->post('year');
		$aksi=site_url('bonus/cekvalidmember2');
    	echo "<form action=\"$aksi\" method=\"POST\">";
		echo "<label>Silahkan masukkan password anda :</label>";
		//echo "<input type=\"username\" name=\"username\" placeholder=\"ID Member\"/>";
		echo "<input type=\"password\" name=\"pwd_member\" placeholder=\"Password\"/>";
		echo "<input type=\"submit\" value=\"Submit\" />";
		echo "<input type=\"hidden\" id=\"month\" name=\"month\" value=\"$month\" />";
        echo "<input type=\"hidden\" id=\"year\" name=\"year\" value=\"$bonusyear\">";
		
		echo "</form>";
    }

	//$route['bonus/cekvalidmember2'] = 'webshop/sales/cekvalidmember2';
	function cekvalidmember2() {
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$dt['month'] = $this->input->post('month');
            $dt['year'] = $this->input->post('year');
		$idmember = getUserID();
		$pwd_member = $this->input->post('pwd_member');
		
		$arr = $this->salesM->cekMemberLog($idmember, $pwd_member);
		//print_r($arr);
		if($arr != null) {
			$dt['res1'] = $this->salesM->showDataMember($idmember,$month,$year);
            $dt['tableBaru'] = $this->salesM->showTableHilal($idmember,$month,$year);

            if($dt['res1'] == 0)
            {
                echo "Data Belum Ada";
            }
            else
            {
                if($this->salesM->showBonusMember($idmember,$month,$year) == 0)
                {
                    echo "Data statement bonus ".$idmember." tidak ada";
                }
                else
                {
                    //$dt['res2'] = $this->salesM->m_showBonusSummary();
                    // ---tambahan dr ana---
                    $dt['currency1'] = $this->salesM->getCurrency();
                    $dt['currency'] = $dt['currency1'][0]->exchangerate;
                    //print_r($dt['currency']);
                    $dt['hasil1'] = $this->salesM->cekmonthly($idmember,$month,$year);
                    $dt['hasil2'] = $this->salesM->downline($idmember,$month,$year);
                    $dt['hasil3'] = $this->salesM->selfBonus($idmember,$month,$year);
                    $cc = $this->salesM->showBonusMember($idmember,$month,$year);
                    if(isset($cc))
                    {
                        foreach($cc as $dd)
                        {
                            if($dd->netincome > 0)
                            {
                                $dt['res2'] = $this->salesM->showBonusMember($idmember,$month,$year);
                                $dt['initiateSUm'] = $this->salesM->showSumInitiative($idmember,$month,$year);
                            }
                            if($dd->gpincome > 0)
                            {
                                $dt['devresult'] = $this->salesM->developmentBonus($idmember,$month,$year);
                            }
                            if($dd->ldbincome > 0)
                            {
                                $dt['ldbresult'] = $this->salesM->leadershipBonus($idmember,$month,$year);
                            }

                            $unilevel = $dd->planbincome + $dd->addinfinityincome;

                            if($dt['year'] < 2017){
                                if($dd->infinityincome > 0)
                                {
                                    $dt['hasil4'] = $this->salesM->infinityBonus($idmember,$month,$year);
                                }
                            }else{
                                $dt['hasil4'] = $this->salesM->initiativeBonus($idmember,$month,$year);
                            }

                            if($unilevel  > 0)
                            {
                                $dt['hasil5'] = $this->salesM->unilevelBonus($idmember,$month,$year);
                                $dt['hasil6'] = $this->salesM->levelMember($idmember,$month,$year);
                            }
                        }
                    }
                    if($this->salesM->showBonusVoucher($idmember,$month,$year) != 0)
                    {
                        $dt['voucher'] = $this->salesM->showBonusVoucher($idmember,$month,$year);
                    }
                    $this->load->view('webshop/printBonusReportPdf',$dt);
                }
            }
		} else {
			echo "<script>alert('Password anda salah..')</script>";
			echo "<script>history.back()</script>";
		} 
		/*if($username != $this->_checkSessionStoreUser()) {
			echo "<script>alert('Username anda tidak sesuai dengan ')</script>";
		}*/
		
		
	}

    function postBonusMember(){

        if($this->_checkSessionStoreUser())
        {
            $idmember = getUserID();
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $dt['month'] = $this->input->post('month');
            $dt['year'] = $this->input->post('year');
            $dt['idmember'] = $idmember;
            $dt['res1'] = $this->salesM->showDataMember($idmember,$month,$year);
            $dt['tableBaru'] = $this->salesM->showTableHilal($idmember,$month,$year);

            /*
            if($dt['tableBaru'] == 0) {
                $dt['tableBaru'] = array(array('bonustype' => 'kosong'));
            }
            */
            if($dt['res1'] == 0)
            {
                echo "Data Belum Ada";
            }
            else
            {
                if($this->salesM->showBonusMember($idmember,$month,$year) == 0)
                {
                    echo "Data statement bonus ".$idmember." tidak ada";
                }
                else
                {
                    //$dt['res2'] = $this->salesM->m_showBonusSummary();
                    // ---tambahan dr ana---
                    $dt['currency1'] = $this->salesM->getCurrency();
                    $dt['currency'] = $dt['currency1'][0]->exchangerate;
                    //print_r($dt['currency']);
                    $dt['hasil1'] = $this->salesM->cekmonthly($idmember,$month,$year);
                    $dt['hasil2'] = $this->salesM->downline($idmember,$month,$year);
                    $dt['hasil3'] = $this->salesM->selfBonus($idmember,$month,$year);
                    $cc = $this->salesM->showBonusMember($idmember,$month,$year);


                    if(isset($cc))
                    {
                        foreach($cc as $dd)
                        {
                            if($dd->netincome > 0)
                            {
                                $dt['res2'] = $this->salesM->showBonusMember($idmember,$month,$year);
                                $dt['initiateSUm'] = $this->salesM->showSumInitiative($idmember,$month,$year);
                            }
                            if($dd->gpincome > 0)
                            {
                                $dt['devresult'] = $this->salesM->developmentBonus($idmember,$month,$year);
                            }
                            if($dd->ldbincome > 0)
                            {
                                $dt['ldbresult'] = $this->salesM->leadershipBonus($idmember,$month,$year);
                            }

                            $unilevel = $dd->planbincome + $dd->addinfinityincome;

                            if($dt['year'] < 2017){
                                if($dd->infinityincome > 0)
                                {
                                    $dt['hasil4'] = $this->salesM->infinityBonus($idmember,$month,$year);
                                }
                            }else{
                                $dt['hasil4'] = $this->salesM->initiativeBonus($idmember,$month,$year);
                            }

                            if($unilevel  > 0)
                            {
                                $dt['hasil5'] = $this->salesM->unilevelBonus($idmember,$month,$year);
                                $dt['hasil6'] = $this->salesM->levelMember($idmember,$month,$year);
                            }
                        }
                    }
                    if($this->salesM->showBonusVoucher($idmember,$month,$year) != 0)
                    {
                        $dt['voucher'] = $this->salesM->showBonusVoucher($idmember,$month,$year);
                    }

                    //permintaan pak toto
//                    if($month != 3){
//                        $this->load->view('webshop/printBonusReportPdf',$dt);
//                    }else{
//                        echo'maintenance';
//                    }

                    //============================
                    //  Komisi PPOB
                    //============================

                    $tgl = "01";
                    $gabung = $year . '/' . $month . '/' . $tgl;
                    $date = date('Y/m/d', strtotime($gabung));

                    $dt['ppob'] = $this->salesM->getKomisiPPOB($date, $idmember);


                    if( $dt['ppob'] != null){
                        foreach ($dt['ppob'] as $key => $value) {
                            $r_fullnm[] = $value->fullnm;
                            $r_dfno[] = $value->dfno;
                            $r_alvl[] = $value->alvl;
                            $r_tbv[] = $value->tbv;
                            $r_cbori[] = $value->CashBackORI;
                            $r_cbppob[] = $value->CashBackPPOB;
                        }

                        // $dt['r_ppob']=$ppob;
                        $dt['fullnm']=$r_fullnm;
                        $dt['alvl']=$r_alvl;
                        $dt['tbv']=$r_tbv;
                        $dt['cbppob']=$r_cbppob;
                        $dt['cbori']=$r_cbori;
                    }

                    //====================TAMPIL KUPON HEBOH BANGET============================

                    $dt['kuponhdr'] = $this->salesM->getListKuponHdr($date, $idmember);
                    $dt['kupon'] = $this->salesM->getListKupon($idmember);


                    $this->load->view('webshop/printBonusReportPdf',$dt);
                    //echo print_r($dt['kuponhdr']);
                }
            }
        }
    }

    function postBonusMember2(){
        //if($this->_checkSessionStoreUser()) 
        //{
        $idmember = $this->input->post('idmember');
        $month = $this->input->post('month_period');
        $year = $this->input->post('year_period');
        $dt['month'] = $month;
        $dt['year'] = $year;
		$dt['idmember'] = $idmember;

        $dt['res1'] = $this->salesM->showDataMember($idmember,$month,$year);
        if($dt['res1'] == 0)
        {
            echo "Data Belum Ada";
        }
        else
        {
            if($this->salesM->showBonusMember($idmember,$month,$year) == 0)
            {
                echo "Data statement bonus ".$idmember." tidak ada";
            }
            else
            {
                //$dt['res2'] = $this->salesM->m_showBonusSummary();
                // ---tambahan dr ana---
                $dt['currency1'] = $this->salesM->getCurrency();
                $dt['currency'] = $dt['currency1'][0]->exchangerate;
                //print_r($dt['currency']);
                $dt['hasil1'] = $this->salesM->cekmonthly($idmember,$month,$year);
                $dt['hasil2'] = $this->salesM->downline($idmember,$month,$year);
                $dt['hasil3'] = $this->salesM->selfBonus($idmember,$month,$year);
                $cc = $this->salesM->showBonusMember($idmember,$month,$year);
                if(isset($cc))
                {
                    foreach($cc as $dd)
                    {
                        if($dd->netincome > 0)
                        {
                            $dt['res2'] = $this->salesM->showBonusMember($idmember,$month,$year);
                            $dt['initiateSUm'] = $this->salesM->showSumInitiative($idmember,$month,$year);
                        }
                        if($dd->gpincome > 0)
                        {
                            $dt['devresult'] = $this->salesM->developmentBonus($idmember,$month,$year);
                        }
                        if($dd->ldbincome > 0)
                        {
                            $dt['ldbresult'] = $this->salesM->leadershipBonus($idmember,$month,$year);
                        }

                        $unilevel = $dd->planbincome + $dd->addinfinityincome;

                        if($dt['year'] < 2017){
                            if($dd->infinityincome > 0)
                            {
                                $dt['hasil4'] = $this->salesM->infinityBonus($idmember,$month,$year);
                            }
                        }else{
                            $dt['hasil4'] = $this->salesM->initiativeBonus($idmember,$month,$year);
                        }

                        if($unilevel  > 0)
                        {
                            $dt['hasil5'] = $this->salesM->unilevelBonus($idmember,$month,$year);
                            $dt['hasil6'] = $this->salesM->levelMember($idmember,$month,$year);
                        }
                    }
                }
                if($this->salesM->showBonusVoucher($idmember,$month,$year) != 0)
                {
                    $dt['voucher'] = $this->salesM->showBonusVoucher($idmember,$month,$year);
                }
                $this->load->view('webshop/printBonusReportPdf',$dt);
            }
        }
    }
    //}

    function postBonusMember3(){


        if($this->_checkSessionStoreUser())
        {

            $idmember = getUserID();
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $dt['month'] = $this->input->post('month');
            $dt['year'] = $this->input->post('year');

            $dt['res1'] = $this->salesM->showDataMember($idmember,$month,$year);
            $dt['tableBaru'] = $this->salesM->showTableHilal($idmember,$month,$year);

            /*
            if($dt['tableBaru'] == 0) {
                $dt['tableBaru'] = array(array('bonustype' => 'kosong'));
            }
            */
            if($dt['res1'] == 0)
            {
                echo "Data Belum Ada";
            }
            else
            {
                if($this->salesM->showBonusMember($idmember,$month,$year) == 0)
                {
                    echo "Data statement bonus ".$idmember." tidak ada";
                }
                else
                {
                    //$dt['res2'] = $this->salesM->m_showBonusSummary();
                    // ---tambahan dr ana---
                    $dt['currency1'] = $this->salesM->getCurrency();
                    $dt['currency'] = $dt['currency1'][0]->exchangerate;
                    //print_r($dt['currency']);
                    $dt['hasil1'] = $this->salesM->cekmonthly($idmember,$month,$year);
                    $dt['hasil2'] = $this->salesM->downline($idmember,$month,$year);
                    $dt['hasil3'] = $this->salesM->selfBonus($idmember,$month,$year);
                    $cc = $this->salesM->showBonusMember($idmember,$month,$year);
                    if(isset($cc))
                    {
                        foreach($cc as $dd)
                        {
                            if($dd->netincome > 0)
                            {
                                $dt['res2'] = $this->salesM->showBonusMember($idmember,$month,$year);
                                $dt['initiateSUm'] = $this->salesM->showSumInitiative($idmember,$month,$year);
                            }
                            if($dd->gpincome > 0)
                            {
                                $dt['devresult'] = $this->salesM->developmentBonus($idmember,$month,$year);
                            }
                            if($dd->ldbincome > 0)
                            {
                                $dt['ldbresult'] = $this->salesM->leadershipBonus($idmember,$month,$year);
                            }

                            $unilevel = $dd->planbincome + $dd->addinfinityincome;

                            if($dt['year'] < 2017){
                                if($dd->infinityincome > 0)
                                {
                                    $dt['hasil4'] = $this->salesM->infinityBonus($idmember,$month,$year);
                                }
                            }else{
                                $dt['hasil4'] = $this->salesM->initiativeBonus($idmember,$month,$year);
                            }

                            if($unilevel  > 0)
                            {
                                $dt['hasil5'] = $this->salesM->unilevelBonus($idmember,$month,$year);
                                $dt['hasil6'] = $this->salesM->levelMember($idmember,$month,$year);
                            }
                        }
                    }
                    if($this->salesM->showBonusVoucher($idmember,$month,$year) != 0)
                    {
                        $dt['voucher'] = $this->salesM->showBonusVoucher($idmember,$month,$year);
                    }               
                   
                    $dt['report'] = $this->salesM->getReportBonus($idmember,$year);
                  
                    
                    $this->load->view('webshop/printBonusReportPdf2',$dt);

                    $paper_size = 'A4';
                    $orientation = 'landscape';
                    $this->dompdf->set_paper($paper_size, $orientation);
                    $html = $this->output->get_output();

                    /* custom sendiri ukuran pdfnya
                    $customPaper = array(0,0,360,400);
                    $this->dompdf->set_paper($customPaper);
                    $html = $this->output->get_output();
                    */


                    //convert to pdf
                    $this->dompdf->load_html($html);
                    $this->dompdf->render();
                    $this->dompdf->stream("laporan.pdf", array('Attachment' => 0));


                }
            }
            //}
        }
    }

    //tambahan fendi buat cetak pajak di pdf
    function postPajakMember(){

        if($this->_checkSessionStoreUser()){

            $idmember = getUserID();
            $month = $this->input->post('month');
            $year = $this->input->post('year');

            $dt['idmember'] = $idmember;
            $dt['month'] = $month;
            $dt['year'] = $year;
            
            $tgl = "01";
            $gabung = $tgl . '/' . $month . '/' . $year;
            $date = date('m/d/Y', strtotime($gabung));

            $dt['pjk'] = $this->salesM->getDetailPajak($idmember, $date);

            $this->load->view('webshop/printPajakPdf',$dt);
            //print_r($dt);
        }
    }


    function getReportCahyono(){
        if($this->_checkSessionStoreUser()) {

            $idmembers = getUserID();

            $year = $this->input->post('year');
            $dt['report'] = 1;
            if($reportCache= $this->redisService->getRedisBonusReport($idmembers,$year))
            {
                $dt['report'] =$reportCache;
            }else{
                $dt['report'] = $this->salesM->getReportBonus($idmembers,$year);
            }
            $this->load->view('webshop/getListingCahyono',$dt);
        }
    }


    function getSalesTracking($orderno){
        if($this->_checkSessionStoreUser()) {

            $dt['prodCat'] = $this->memberService->getProductCategory();
            $orderno= base64_decode($orderno);

            $dts = $this->salesM->getProductOrder($orderno);
            if($dts != null){
                foreach($dts as $row){
                    $service=$row->service_type_id;
                    $dt['orderdate']=$row->datetrans;
                    $dt['no_gosend']=$service;
                    $dt['type']=$row->service_type_name;
                }
            }

            //get API
            //get user key gosend
            $getKey= $this->M_location->getUserKey();
            $user_key= $getKey[0]->username;
            $pass_key= $getKey[0]->key_user;

            $json = status_gsend($user_key,$pass_key,$service);
            $result= json_decode($json, TRUE);

            $no_ship= $result['orderNo'];
            $status_ship= $result['status'];
            $create_time= $result['orderCreatedTime'];
            $dispatch_time= $result['orderDispatchTime'];
            $arrival_time= $result['orderArrivalTime'];
            $closed_time= $result['orderClosedTime'];
            $desc=$result['liveTrackingUrl'];;
            $shipper_id="";
            //echo "tes".$service;

            //insert table ecomm_tracking_api
            $this->salesM->insertTrackingApi($orderno,$no_ship,$status_ship,$desc,$shipper_id,$create_time,$dispatch_time,$arrival_time,$closed_time);

            //echo $result['status'];
            /* if($status =="confirmed"){
                 $dt['status_ship']="Kami sedang melakukan pencarian Kurir";
             }elseif($status == "allocated"){
                 $dt['status_ship']="Kurir telah didapatkan";
             }elseif($status == "out_for_pickup"){
                 $dt['status_ship']="Kurir dalam perjalanan untuk pick-up";
             }elseif($status == "picked"){
                 $dt['status_ship']="Pesanan telah diambil Kurir";
             }elseif($status == "out_for_pickup"){
                 $dt['status_ship']="Kurir dalam perjalanan untuk pick-up";
             }elseif($status == "out_for_delivery"){
                 $dt['status_ship']="Kurir mengantarkan pesanan";
             }elseif($status == "delivered"){
                 $dt['status_ship']="Pesanan telah terkirim";
             }elseif($status == "no_driver"){
                 $dt['status_ship']="Kurir tidak ditemukan";
             }elseif($status == "rejected"){
                 $dt['status_ship']="Penerima tidak ditemukan";
             }elseif($status == "on_hold"){
                 $dt['status_ship']="Pesanan ditunda";
             }
             else {
                 $dt['status_ship']="Kami telah menerima rincian pesanan Anda";
             }*/

            /*$qryItem = "SELECT a.prdnm,a.qty
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
            $queryExec = $this->db->query($qryItem);

            if($queryExec->num_rows() > 0) {
                $no=0;
                foreach($queryExec->result() as $row1)
                {
                    $no++;
                    $prdnm= $row1->prdnm;
                    $qty= $row1->qty;

                    $concat_item= $prdnm." (".$qty." pc)";
                    $item_arr[]= $concat_item;
                    $item=implode(" ",$item_arr);
                    $dt['n_item']= str_replace("c) ","c),",$item);
                   // print_r($item_arr);
                }

            }*/

            //get ecomm_tracking_webhook
            //$dt['tracking'] = $this->salesM->getTracking($orderno);
            $dt['tracking'] = $this->salesM->getTrackingWebhook($orderno);
            $dt['orderno']= $orderno;
            $this->setTempWebShop('webshop/salesOnlineTracking', $dt);
        }
    }

    public function getRebooking($val){
        $orderno=base64_decode($val);
        $dts = $this->salesM->getProductOrder($orderno);
        if($dts != null){
            foreach($dts as $row){
                $dt['orderdate']=$row->datetrans;
            }
        }

        $dt['orderno']=$orderno;
        $this->setTempWebShop('webshop/salesRebooking', $dt);
    }

    public function saveRebooking(){

        $orderno = $this->input->post('no_order');
        $dts = $this->salesM->getProductOrder($orderno);
        if($dts != null){
            foreach($dts as $row){
                $orderdate=$row->datetrans;
                $price = $row->total_pay;
            }
        }

        $qryItem = "SELECT a.prdnm,a.qty
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
        $queryExec = $this->db->query($qryItem);

        if($queryExec->num_rows() > 0) {
            $no=0;
            foreach($queryExec->result() as $row1)
            {
                $no++;
                $prdnm= $row1->prdnm;
                $qty= $row1->qty;

                $concat_item= "(".$prdnm." : ".$qty." pc)";
                $item_arr[]= $concat_item;
                $item=implode(" ",$item_arr);
            }

        }
        //menggunakan API k-net

        //get user key gosend
        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey()->row();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $n_metode=$this->input->post('metode');
        $n_metode=substr($n_metode,0,1);

        if($n_metode == 1){
            $metode = "Instant";
        }else{
            $metode ="SameDay";
        }

        $des_name = $this->input->post('nama_penerima');
        $des_phone =$this->input->post('notlp');
        $des_addr = $this->input->post('alamat');
        $des_coord = $this->input->post('latlong2');
        $lat=$this->input->post('lat_dest');
        $long=$this->input->post('long_dest');
        //$item = "Produk K-Link";

        //hapus tabel history gosend
        $this->salesM->delTracking($orderno);

        $dt = booking_gsend($user_key,$pass_key,$des_name,$des_phone,$des_addr,$des_coord,$item,$price);
        $result= json_decode($dt, TRUE);
        $order_g=$result['orderNo'];

        //update tabel
        $exec= $this->salesM->updateShippaddr($orderno,$des_name,$des_addr,$des_phone,$order_g,$lat,$long);


        if($exec){
            $data['prodCat'] = $this->productService->getProductCategory();
            $this->setTempWebShop('webshop/salesOnlineMember', $data);
            /*
            $dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['temp_paymentIdx'];
            $this->setTemplateSGO('webshop/sales',$dt);*/

           // redirect('/sales/online', 'refresh');
        }else{
            echo "<script>alert('Order Gagal, Ulangi!')</script>";
            echo "<script>history.back()</script>";
        }
    }

    public function cashback_hydro(){
//        if($this->_checkSessionStoreUser()) {
//            $dt['dari']=date("Y-m-d") ;
//            $dt['ke']=date("Y-m-d") ;
//
//            $this->setTempWebShop('webshop/cashback_hydro', $dt);
//        }
        $login = getUserID();

        if($login != NULL){

            $dt['idmember'] = getUserID();
            $dt['prodCat'] = $this->memberService->getProductCategory();

            $dt['dari']=date("Y-m-d") ;
            $dt['ke']=date("Y-m-d") ;

            $this->setTempWebShop('webshop/cashback_hydro', $dt);
            //print_r($dt);
        }
        else{
            redirect('loginmember');
        }
    }

    public function cashback_process(){
        if($this->_checkSessionStoreUser()) {

            $idmember = getUserID();

            $month = $this->input->post('month');
            $year = $this->input->post('year');

            $dt['cashback'] = $this->salesService->getCashback($idmember,$month,$year);

//            echo "<pre>";
//            print_r($dt['cashback']);
//            echo "</pre>";
            $this->load->view('webshop/cashback_hydro1',$dt);
        }
    }

    function getSalesMemberPayment(){ //$route['sales/cek_payment'] = 'webshop/sales/getSalesMemberPayment';
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $dt['arr'] = null;
            //$dt['bonusmont'] = $this->salesService->getBonusMonth();
            //print_r($dt['listBank']);
            $this->setTempWebShop('webshop/salesOnlinePayment', $dt);
        }
    }

    public function getTransPayment() { //$route['sales/cek_trans_payment'] = 'webshop/sales/getTransPayment';
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();

            $data = $this->input->post(NULL, TRUE);
            $dt['arr'] = $this->salesService->getListTrans($data);

            $this->setTempWebShop('webshop/salesOnlinePayment', $dt);
        }

        //$i=1;
        /*if($arr != null) {
            foreach ($arr as $dtas) {

                $today = date("d-m-Y H:i:s");
                $date_exp = date("d-m-Y H:i:s", strtotime($dtas->date_expired));
                $tran_date = date("d-m-Y H:i:s", strtotime($dtas->datetrans));
                $flag = $dtas->flag_payment;
                $token = $dtas->orderno;
                $bonus_month = $dtas->bonusmonth;


                if ($flag != '1' && ($today > $date_exp)) {
                    $status = "EXPIRED";
                    $color = "#ff0a18";
                } elseif ($flag != '1' && ($today < $date_exp)) {
                    $status = "MENUNGGU PEMBAYARAN";
                    $color = "#1f34b9";
                } else {
                    $status = "SUKSES";
                    $color = "#208612";
                }

                $angka = $dtas->total_pay;
                $tot_rp = "Rp " . number_format($angka, 0, ',', '.');


              echo "<div class=\"col-lg-12 col-xs-12 cart_content_thin text-center\">
				        <div class=\"col-lg-1 col-xs-1\">$i</div>
						<div class=\"col-lg-2 col-xs-2\">$dtas->orderno</div>
		                <div class=\"col-lg-3 col-xs-2\">$tran_date</div>
		                <div class=\"col-lg-1 col-xs-2\">$dtas->bonusmonth</div>
		                <div class=\"col-lg-2 col-xs-2\">$tot_rp</div>
		                <div class=\"col-lg-3 col-xs-3\" style='color: $color'><strong>$status</strong></div>
		              </div>";
                $i++;
            }
        } else {
           echo "<div class=\"col-lg-12 col-xs-12 cart_content_thin text-center\">
				        <div class=\"col-lg-1 col-xs-1\">&nbsp;</div>
						<div class=\"col-lg-2 col-xs-2\">&nbsp;</div>
		                <div class=\"col-lg-3 col-xs-2\">DATA TIDAK DITEMUKAN!</div>
		                <div class=\"col-lg-1 col-xs-2\">&nbsp;</div>
		                <div class=\"col-lg-2 col-xs-2\">&nbsp;</div>
		                <div class=\"col-lg-3 col-xs-3\">&nbsp;</div>
		              </div>";

        }*/
    }

    public function getTransPaymentOld() {
        $data = $this->input->post(NULL, TRUE);
        $arr = $this->salesService->getListTrans($data);
        $i=1;
        if($arr != null) {
            foreach ($arr as $dtas) {

                $today = date("d-m-Y H:i:s");
                $date_exp = date("d-m-Y H:i:s", strtotime($dtas->date_expired));
                $tran_date = date("d-m-Y H:i:s", strtotime($dtas->datetrans));
                $flag = $dtas->flag_payment;
                $token = $dtas->orderno;
                $bonus_month = $dtas->bonusmonth;


                if ($flag != '1' && ($today > $date_exp)) {
                    $status = "EXPIRED";
                    $color = "#ff0a18";
                } elseif ($flag != '1' && ($today < $date_exp)) {
                    $status = "MENUNGGU PEMBAYARAN";
                    $color = "#1f34b9";
                } else {
                    $status = "SUKSES";
                    $color = "#208612";
                }

                $angka = $dtas->total_pay;
                $tot_rp = "Rp " . number_format($angka, 0, ',', '.');

                /* $array = array("koneksi"=>"TRUE","token" => $token,"tgl_trans" => $tran_date,"bns_month"=> $bonus_month,"total_bayar"=>$tot_rp,"ket"=>$status);
             }
         }else{
                 $array = array("koneksi"=>"FALSE","token" => "","tgl_trans" => "","bns_month"=> "","total_bayar"=>"","ket"=>"");
         }
         echo json_encode($arr);*/
                echo "<div class=\"col-lg-12 col-xs-12 cart_content_thin text-center\">
				        <div class=\"col-lg-1 col-xs-1\">$i</div>
						<div class=\"col-lg-2 col-xs-2\">$dtas->orderno</div>
		                <div class=\"col-lg-3 col-xs-2\">$tran_date</div>
		                <div class=\"col-lg-1 col-xs-2\">$dtas->bonusmonth</div>
		                <div class=\"col-lg-2 col-xs-2\">$tot_rp</div>
		                <div class=\"col-lg-3 col-xs-3\" style='color: $color'><strong>$status</strong></div>
		              </div>";
                $i++;
            }
        } else {
            echo "<div class=\"col-lg-12 col-xs-12 cart_content_thin text-center\">
				        <div class=\"col-lg-1 col-xs-1\">&nbsp;</div>
						<div class=\"col-lg-2 col-xs-2\">&nbsp;</div>
		                <div class=\"col-lg-3 col-xs-2\">DATA TIDAK DITEMUKAN!</div>
		                <div class=\"col-lg-1 col-xs-2\">&nbsp;</div>
		                <div class=\"col-lg-2 col-xs-2\">&nbsp;</div>
		                <div class=\"col-lg-3 col-xs-3\">&nbsp;</div>
		              </div>";

        }
    }

}
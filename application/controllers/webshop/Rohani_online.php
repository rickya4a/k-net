<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 //ob_start();
/**
 * Description of Umroh Online
 *  *
 * @author ananech
 */

 class Rohani_online extends MY_Controller{
    public function __construct() {
        
	    parent::__construct();
		$this->load->service("webshop/Product_service",'productService');
		//$this->load->service("webshop/Umroh_service",'umrohService');
        $this->load->model('webshop/Umroh_model','umrohModel');
        $this->load->model('backend/Be_finance_umroh_model','fUmroh');
        //$this->load->model("backend/be_member_model", 'mMember');
        
        //change setTempWebUmrdoh to setTempWebShop
	}
    
    
    /*-----------------------------------
        KUMPULAN HELPER UMROH & ZIARAH
    ------------------------------------*/
    
    function helperDistributorInfo(){
        if($this->_checkSessionStoreUser()) {
            $dt = $this->input->post(null,true);
            $infoDist = $this->umrohModel->showDistInfo(strtoupper($dt['idmember']));
            
            if($infoDist != null){
                foreach($infoDist as $dta)
                {
                    $return_arr = array("adadata" => "benar", "fullnm" => $dta->fullnm,
                                    "addr" => $dta->addr1,"tel_hp" => $dta->tel_hp,
                                    "tel_hm"=> $dta->tel_hm,"noktp" => $dta->idno, 
                                    "birthdt"=> $dta->birthdt,"loccd"=>$dta->loccd,
                                    "sfno"=>$dta->sfno);
                }
                echo json_encode($return_arr); 
            }
            else
            {
                $return_arr = array("adadata" => "salah");
                echo json_encode($return_arr);
            }
        }
    }
    
    function helperSponsorInfo(){
        if($this->_checkSessionStoreUser()) {
            $dt = $this->input->post(null,true);
            $infoSponsor = $this->umrohModel->showDistInfo(strtoupper($dt['idsponsor']));
            
            if($infoSponsor != null){
                foreach($infoSponsor as $dta)
                {
                    $return_arr = array("adadata" => "benar", "fullnm" => $dta->fullnm,
                                    "addr" => $dta->addr1,"tel_hp" => $dta->tel_hp,
                                    "tel_hm"=> $dta->tel_hm,"noktp" => $dta->idno);
                }
                echo json_encode($return_arr); 
            }
            else
            {
                $return_arr = array("adadata" => "salah");
                echo json_encode($return_arr);
            }
        }
    }
    
    function helperKtpInfo(){
        if($this->_checkSessionStoreUser()) {
            $dt = $this->input->post(null,true);
            $infoKtp = $this->umrohModel->showKtpInfo($dt['noktp']);
            
            if($infoKtp != null){
                foreach($infoKtp as $dta)
                {
                    $return_arr = array("adadata" => "salah", "fullnm" => $dta->fullnm,
                                        "noktp" => $dta->idno);
                }
                echo json_encode($return_arr); 
            }
            else
            {
                $return_arr = array("adadata" => "benar");
                echo json_encode($return_arr);
            }
        }
    }
    
    function helperNoHpInfo(){
        if($this->_checkSessionStoreUser()) {
            $dt = $this->input->post(null,true);
            $infoHp = $this->umrohModel->showHpInfo($dt['tel_hp']);
            
            if($infoHp != null){
                foreach($infoHp as $dta)
                {
                    $return_arr = array("adadata" => "salah", "fullnm" => $dta->fullnm,
                                        "noktp" => $dta->tel_hp);
                }
                echo json_encode($return_arr); 
            }
            else
            {
                $return_arr = array("adadata" => "benar");
                echo json_encode($return_arr);
            }
        }
    }
    
    function helperRegnoInfo(){
        if($this->_checkSessionStoreUser()) {
            $dt = $this->input->post(null,true);
            $infoRegno = $this->umrohModel->showRegnoInfo(strtoupper($dt['regnos']));
            
            $res = jsonFalseResponse();
            if($infoRegno != null) {
                $res = jsonTrueResponse($infoRegno);  
            } 
            echo json_encode($res);
        }
    }
    
    function helperRegnoInfo1(){
        if($this->_checkSessionStoreUser()) {
            $dt = $this->input->post(null,true);
            $infoRegno = $this->umrohModel->showRegnoInfo1(strtoupper($dt['regnos']));
            
            $res = jsonFalseResponse();
            if($infoRegno != null) {
                $res = jsonTrueResponse($infoRegno);  
            } 
            echo json_encode($res);
        }
    }
    
    
    
    /*-----------------------------------------
        KUMPULAN FUNCTION UTK UMROH & ZIARAH
    ------------------------------------------*/
    function previewJamaahUmroh(){
        if($this->_checkSessionStoreUser()) {
            $arr = array();
            if($this->_checkSessionStoreUser())
            {
                if($this->form_validation->run('umroh') == FALSE) 
                {
                    echo "Semua field harus diisi..!!";
                }
                else
                {
                    $dt = $this->input->post(null,true);
                    if($dt != null)
                    {
                        if($dt['tipePrjlanan']=='umr'){
                            $dt['head'] = 'Jamaah';
                        }else{
                            $dt['head'] = 'Jemaat';
                        }
                        $cekKtp = $this->umrohModel->showKtpInfo($dt['idno']);
                        $cekHp = $this->umrohModel->showHpInfo($dt['tel_hp']);
                        $cekBrkt = $this->umrohModel->checkSameSchedule($dt['jdwlbrkt'],$dt['idmember'],$dt['idno']);
                        /*$cekKtp=0;
						$cekHp=0;*/
                        $validUmur = date('Y') - $dt['thnlhr'];   
                        
                        //if($cekKtp != null && $dt['tipeJamaah'] != '3'){
						if($cekKtp != null && $dt['tipeJamaah'] == '3'){
                            echo "Ktp Sudah Terdaftar";
                        }/*elseif($cekHp > 0){
                            echo "No Hp Sudah Terdaftar";
                        }*/
						//elseif($dt['tipeJamaah'] != '3' && $dt['idstk'] == '0'){
						elseif($dt['tipeJamaah'] == '3' && $dt['idstk'] == '0'){
                            echo "Silahkan Pilih Stockist";
                        }
                        elseif($validUmur < 18 && ($dt['statusJamaah'] == '2' ||$dt['statusJamaah'] == '3'||$dt['statusJamaah'] == '4' )){
                            echo "Umur Kurang dari 18 Tahun,jika Hubungan Keluarga Istri/Suami/Saudara";
                        }elseif($cekBrkt > 0){
                            echo "Jadwal Keberangkatan Sudah Terpakai";
                        }
                        elseif($dt['idsponsor'] == '' && $dt['tipeJamaah'] == '3'){
                            echo "Id Sponsor Tidak Boleh Kosong";
                        }elseif($dt['tel_hp'] == '' && ($dt['tipeJamaah'] == '3' || $dt['tipeJamaah'] == '1')){
                            echo "No Hp Tidak Boleh Kosong";
                        }elseif($dt['idstk'] == '0'){
                            echo "Silahkan Pilih Stockist Tujuan";
                        }
                        else{
                            array_push($arr, $dt);
                            $this->session->set_userdata('umroh',$arr);
                            $dt['jamaah'] = $this->session->userdata('umroh');
                            $jum = count($dt['jamaah']);
                            $tot_biaya = $jum * 3500000;
                            $tot_biaya = array(
                                            'order_id' => rand(),
                                            'tot_biaya' => $tot_biaya
                                            );
                            $this->session->set_userdata('dp_umroh',$tot_biaya);
                            $dt['biaya'] = $this->session->userdata('dp_umroh');
                            $dt['bank'] = $this->umrohModel->getBank();
                            $this->load->view('webshop/rohani/prevJamaahUmroh',$dt);   
                        }
                    }
                }
            }
        }
    }
    
    function pendingUmroh(){
        if($this->_checkSessionStoreUser()) {
            $abc = $this->session->userdata('umroh');
            //echo "birthdate ".$abc[0]['tgllhr']."/".$abc[0]['blnlhr']."/".$abc[0]['thnlhr'];
            $dt['prodCat'] = $this->productService->getProductCategory();
            $dt['biaya'] = $this->session->userdata('dp_umroh');
            
            $dt['ins'] = $this->umrohModel->dtUmrohPending($dt['biaya']['tot_biaya']);
            
            $this->session->unset_userdata('umroh');
            $this->session->unset_userdata('dp_umroh');
            if($dt['ins']['response'] > 0){
                
                $dt['dtJamaah'] = $this->umrohModel->getDtJamaahByTrf($dt['ins']['regno']);
                
                $this->setTempWebShop('webshop/rohani/umrohTransPay',$dt);
            }
        }
    }
    
    function getPaymentUmroh(){
        if($this->_checkSessionStoreUser()) {
            
            $abc = $this->session->userdata('umroh');
            $dt = $this->input->post(null,true);
            $tipePay = $dt['tipepaymentt'];
            $dt['prjalanan'] = $abc[0]['tipePrjlanan'];
            //print_r($dt);
            $charge = $this->umrohModel->getCharge($tipePay);
            $dt['biaya'] = $this->session->userdata('dp_umroh');
            
            $dt['bankCode'] = $charge[0]->bankCode;
            $dt['bankDesc'] = $charge[0]->bankDesc;
            $dt['charge_connectivity'] = $charge[0]->charge_connectivity;
            $dt['charge_admin'] = $charge[0]->charge_admin;
            
            $dt['total'] = $dt['biaya']['tot_biaya'];
            
            if(isset($abc)){
                $dt['ins'] = $this->umrohModel->setUmrohData($dt['biaya']['tot_biaya'],$charge[0]->bankCode,$charge[0]->charge_connectivity,$charge[0]->charge_admin);
                    $dt['prodCat'] = $this->productService->getProductCategory();
                    if($dt['ins']['response'] > 0 && $dt['prjalanan'] == 'umr'){
                        //===key Production Umroh===
                        $dt['key'] = 'ed5c3103e371d7fbe4086b19c9e4955d';
            
                        //==key Develop Umroh==
                        //$dt['key'] = '94d2045c75a0c15324e44cd7a3f29bc4';
                        //echo "masuk di umroh";
                        $this->setTempWebShop('webshop/rohani/paymentFormUmroh',$dt);
                    }else{
                        
                        //===key Production Ziarah===
                        //$dt['key'] = 'ed5c3103e371d7fbe4086b19c9e4955dtest';
                        //echo "masuk di ziarah";
                        //==key Develop Ziarah==
                        $dt['key'] = '7a19dd00e76e1ddc23affd76825123aa';
                        
                        $this->setTempWebShop('webshop/rohani/paymentFormZiarah',$dt);
                    }
            }else{
                redirect('umroh');
            }
        }
    }
    
    function printNotaUmroh(){
        if($this->_checkSessionStoreUser())
        {
            $regnos = $this->input->post('regnos');
            $dt['dtUmroh'] = $this->umrohModel->pdfUmroh($regnos);
            if($dt['dtUmroh'] != null){
                $this->load->view('webshop/rohani/pdfUmroh',$dt);
            }
        }
    }
    
    function printNotaUmroh1(){
        if($this->_checkSessionStoreUser())
        {
            $regnos = $this->input->post('regnos');
            $dt['dtUmroh'] = $this->umrohModel->pdfUmroh1($regnos);
            if($dt['dtUmroh'] != null){
                $this->load->view('webshop/rohani/pdfUmroh',$dt);
            }
        }
    }
    
    function cekMutasiUmroh(){
        if($this->_checkSessionStoreUser())
        {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->setTempWebShop('webshop/rohani/form_mutasiUmroh',$dt);
        }
    }
    
    function getMutasiUmroh(){
        if($this->_checkSessionStoreUser())
        {
            $dt['headMutasi'] = $this->umrohModel->headMutasi();
            $dt['detMutasi'] = $this->umrohModel->mutasiUmroh();
            if($dt['detMutasi'] > 0){
                $this->load->view('webshop/rohani/mutasiUmroh_res',$dt);
            }
        }
    }
    
    function printNotaCicilan(){
        if($this->_checkSessionStoreUser())
        {
            $regnos = $this->input->post('regnos');
            $dt['dtCicilan'] = $this->umrohModel->pdfCicilanUmroh($regnos);
            if($dt['dtCicilan'] != null){
                $this->load->view('webshop/rohani/pdfInstallments',$dt);
            }
        }
    }
    
    function getFormPrintVchno(){
        if($this->_checkSessionStoreUser())
        {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->setTempWebShop('webshop/rohani/search_vchNo',$dt);
        }
    }
    
    function printVchnoUmroh(){
        if($this->_checkSessionStoreUser())
        {
            $dt = $this->input->post(null,true);
            $vchnooo = $dt['vchno'];
            $idmembers = $dt['idmember'];
            $dt['vchnoo'] = $this->umrohModel->getDtVchno($vchnooo,$idmembers);
            $this->load->view('webshop/rohani/search_vchNo_res',$dt);
        }
    }
    
    function printVchnoTxt(){
        if($this->_checkSessionStoreUser())
        {
             $dt = $this->input->post(null,true);
            $x['dtVch'] = $this->umrohModel->getPrintVch($dt['regnos']);
            if($x['dtVch'] != null){
                $this->load->view('webshop/rohani/printVchPdf', $x);
            }else{
                echo "No data";
            }
        }
    }
    
    function getFormReprint(){
        if($this->_checkSessionStoreUser())
        {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->setTempWebShop('webshop/rohani/search_regNo',$dt);
        }
    }
    
    function reprintRegno(){
        if($this->_checkSessionStoreUser())
        {
            $dt = $this->input->post(null,true);
            $dt['dtUmroh'] = $this->umrohModel->pdfUmroh($dt['regnos']);
            if($dt['dtUmroh'] != null){
                $this->load->view('webshop/rohani/reprint_res',$dt);
            }
        }
    }
    
    /*------------------------------------------------
    =====END KUMPULAN FUNCTION UTK UMROH & ZIARAH=====
    ------------------------------------------------*/
    
    /*-----------------------------
                UMROH
    ------------------------------*/
    function getFormUmroh($err = NULL){
        if($this->_checkSessionStoreUser()) {
            $tipeprjlanan = 'umr';
			$dt['prodCat'] = $this->productService->getProductCategory();
            $dt['login'] = $this->session->userdata('store_info');
            $dt['jadwal'] = $this->umrohModel->getJadwalKeberangkatan($tipeprjlanan);
            $dt['idstk'] = $this->umrohModel->getIdstk();
            //$dt['err'] = $err;
			$this->setTempWebShop('webshop/rohani/umrohRegister',$dt);
            //$this->setTempWebShop('webshop/rohani/umrohRegisterEdit',$dt);
		}
    }
    
    function getInquirySgo(){
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
        $abc = $this->umrohModel->getAmtUmrohh($xx['order_id']);
        $tot = $abc[0]->tot_fund;
        //$connectivity = $abc[0]->charge_connectivity;
        //$amount = $tot + $connectivity ;
        $amount = $tot;
        //$amount = $abc[0]->tot_fund + 7500;
        //$amount = 3500000;
        if(!$xx['order_id']){
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array('errorCode' => $xx['success_flag'],
                             'errorDesc' => $xx['error message'],
                             'orderId' => $xx['order_id'],
                             'amount' => $amount,
                             'ccy' => 'IDR',
                             'desc' => 'k-smart umroh',
                             'trx_date' => date('d/m/Y H:i:s')
                             );
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
    
    function getInquirySgoDev(){
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
        $abc = $this->umrohModel->getAmtUmrohh($xx['order_id']);
        $tot = $abc[0]->tot_fund;
        $connectivity = $abc[0]->charge_connectivity;
        $amount = $tot + $connectivity ;
        //$amount = $amount + 7500;
        //$amount = 3500000;
        if(!$xx['order_id']){
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array('errorCode' => '0',
                             'errorDesc' => 'Success',
                             'orderId' => $xx['order_id'],
                             'amount' => $amount,
                             'ccy' => 'IDR',
                             'desc' => 'k-smart umroh',
                             'trx_date' => date('d/m/Y H:i:s')
                             );
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
    
    function postPaymentUmrohSgo(){
        /*if($this->_checkSessionStoreUser())
        {*/
            $xx = $this->input->post(null,true);
            $password = 'k-net181183';
            //$totalDP = 3500000;
            
            if(!$xx['amount'] ){   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
                $prefix = substr($xx['order_id'],0,1);
                if($prefix == 'U'){
                    $updtTabel = $this->umrohModel->updtMutasiCicilan($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }else{
                    $updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                if($xx['success_flag'] == '1'){
                    $message = 'Failed';
                }else{
                    $message = 'Success';
                }
                $resSukses = array('success_flag' => $xx['success_flag'],
                                    'error_message' => $message,
                                    'reconcile_id' => rand(15,32),
                                    'order_id' => $xx['order_id'],
                                    'reconcile_datetime' =>date('d/m/Y H:i:s'));
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];   
            }
    }
    
    function postPaymentUmrohSgoDev(){
        /*if($this->_checkSessionStoreUser())
        {*/
            $xx = $this->input->post(null,true);
            $password = 'k-net181183';
            //$totalDP = 3500000;
            
            if(!$xx['amount'] ){   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
                $prefix = substr($xx['order_id'],0,1);
                if($prefix == 'U'){
                    $updtTabel = $this->umrohModel->updtMutasiCicilan($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }else{
                    $updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }
                //$updtTabel = $this->umrohModel->updtMutasiCicilan($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                
                $resSukses = array('success_flag' => 0,'error_message' => 'Success','reconcile_id' => rand(15,32),'order_id' => $xx['order_id'],'reconcile_datetime' =>date('d/m/Y H:i:s'));
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];   
            }
    }
    
    function finishPaymentUmroh($orderid)
    {
        $alternate2 = $this->load->database('alternate2', true);
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->session->unset_userdata('umroh');
            $this->session->unset_userdata('dp_umroh');
            $dt['dtJamaah'] = $this->umrohModel->getDtJamaah($orderid);
            if($dt['dtJamaah'] != null)
            {
                foreach($dt['dtJamaah'] as $row){
                    $noreg = $row->registerno;
                    $pktUmroh = $row->departuredesc;
                    $idmember = $row->dfno;
                    $voucherNo = $row->voucherno;
                    $isVoucher = $row->is_voucher;
                    $flagtipe = $row->flag_tipe;
                    $nama = $row->fullnm;
                    $telHp = $row->tel_hp;
                    $secno = $row->secno;
                    $cnno = $row->CNno;
                    $kdbank = '88146';
                    $va = $kdbank.$row->novac;
                }
                
                /*$parent = $this->umrohModel->getDtparent($noreg);
                foreach($parent as $xxx){
                    $tel_hp = $xxx->parenthp;
                }*/
                //if($isVoucher == '1'){
                    if($flagtipe == '1'){
                        $text = 'K-SMART UMROH: Reg no: '.$noreg.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.',VchNo: '.$voucherNo.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$noreg.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.'';
                    }elseif($flagtipe == '3'){
                        $text = 'K-SMART UMROH: Reg no: '.$regno.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.',Stk:'.$stk.',VchNo: '.$voucherNo.',SK: '.$secno.',MM: '.$cnno.',VA: '.$va.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$noreg.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.'';
                        $textLanjut = 'LANJUTAN: VchNo:'.$voucherNo.',SK: '.$secno.',MM: '.$cnno.',VA: '.$va.'';
                        $smsLnjt = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                           VALUES ('".$telHp."','".$textLanjut."','K-Smart Umroh Status')";
                        $query2 = $alternate2->query($smsLnjt);
                    }else{
                        $text = 'K-SMART UMROH: Reg no: '.$noreg.',Nama:'.$nama.',Pkt Umroh: '.$pktUmroh.',VchNo: '.$voucherNo.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$noreg.',Nama:'.$nama.',Pkt Umroh: '.$pktUmroh.'';
                    }   
                //}
                
                $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$telHp."','".$text."','K-Smart Umroh Status')";
                
                /*$smsParent = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$tel_hp."','".$txtParent."','K-Smart Umroh Status')";*/
		        //echo "Ins SMS : $insSms";
		        $query = $alternate2->query($insSms);
                //$query2 = $alternate2->query($smsParent);
                
                $this->setTempWebShop('webshop/rohani/payUmroh_result',$dt);
            }else{
                $this->setTempWebShop('webshop/rohani/payUmroh_unfinish',$dt);
            }
        }
    }
    
    function getFormInstallment(){
        if($this->_checkSessionStoreUser())
        {
            $dt['bank'] = $this->umrohModel->getBank();
            //echo "abcde";
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->setTempWebShop('webshop/rohani/formInstallmentsU',$dt);
        }
    }
    
    function getFormInstallmentPay(){
        if($this->_checkSessionStoreUser())
        {
            //$regnNos = $this->input->post('regnos');
            $dt = $this->input->post(null,true);
            if($dt['regnos'] != "" && $dt['amtUmroh'] != ''){
                $xx['regno'] = strtoupper($dt['regnos']);
                $xx['cicilann'] = $dt['amtUmroh'];
                $aaa = array
                        (
                            'regno' => $dt['regnos'],
                            'amtUmroh' => $dt['amtUmroh']
                        );
                $this->session->set_userdata('regnos',$aaa);
                
                $charge = $this->umrohModel->getCharge($dt['tipepaymentt']);
                
                $xx['bankCode'] = $charge[0]->bankCode;
                $xx['bankDesc'] = $charge[0]->bankDesc;
                $xx['charge_connectivity'] = $charge[0]->charge_connectivity;
                $xx['charge_admin'] = $charge[0]->charge_admin;
                
                $xx['total'] = $xx['cicilann'] + $xx['charge_admin'];
                
                $xx['ins'] = $this->umrohModel->insPrePayment($dt,$charge[0]->bankCode,$charge[0]->charge_connectivity,$charge[0]->charge_admin);
                
                //key Production Umroh
                $xx['key'] = 'ed5c3103e371d7fbe4086b19c9e4955d';
                
                //key Develop Umroh
                //$xx['key'] = '94d2045c75a0c15324e44cd7a3f29bc4';  
                
                $xx['prodCat'] = $this->productService->getProductCategory();
                $this->setTempWebShop('webshop/rohani/formPayInstall',$xx);
            }else{
                redirect('umroh/installments');
            }
            
        }
    }
    
    function finishPayCicilanUmroh($orderid){
        $alternate2 = $this->load->database('alternate2', true);
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->session->unset_userdata('regnos');
            
            $dt['dtCicilan'] = $this->umrohModel->getDtCicilan($orderid);
            if($dt['dtCicilan'] != null)
            {
                foreach($dt['dtCicilan'] as $row){
                    $regno = $row->regno;
                    $idmember = $row->idmember;
                    $nmmember = $row->nmmember;
                    $hp = $row->tel_hp;
                    $tot_fund = $row->tot_fund;
                    $pktumrh = $row->departuredesc;
                }
                
                $text = 'CICILAN K-SMART UMROH: Reg no: '.$regno.',Pkt Umroh: '.$pktumrh.',Id Member: '.$idmember.',Cicilan: '.$tot_fund.'';
                
                $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$hp."','".$text."','K-Smart Umroh Status')";
                $query = $alternate2->query($insSms);
                $this->setTempWebShop('webshop/rohani/payCicilan_finish',$dt);
            }else{
                $this->setTempWebShop('webshop/rohani/payCicilanUmroh_unfinish',$dt);
            }
        }
    }
    
    /*-----------------------------
             END UMROH
    ------------------------------*/
    
    
    /*-----------------------------
                ZIARAH
    ------------------------------*/
    function getFormZiarah(){
        if($this->_checkSessionStoreUser()) {
            $tipeprjlanan = 'ysr';
			$dt['prodCat'] = $this->productService->getProductCategory();
            $dt['login'] = $this->session->userdata('store_info');
            $dt['jadwal'] = $this->umrohModel->getJadwalKeberangkatan($tipeprjlanan);
            $dt['idstk'] = $this->umrohModel->getIdstk();
            //$dt['err'] = $err;
			$this->setTempWebShop('webshop/rohani/ziarahRegister',$dt);
            //$this->setTempWebShop('webshop/rohani/umrohRegisterEdit',$dt);
		}
    }
    
    function inquiryZiarahSgo(){
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
        $abc = $this->umrohModel->getAmtUmrohh($xx['order_id']);
        $tot = $abc[0]->tot_fund;
        //$connectivity = $abc[0]->charge_connectivity;
        //$amount = $tot + $connectivity ;
        $amount = $tot;
        //$amount = $abc[0]->tot_fund + 7500;
        //$amount = 3500000;
        if(!$xx['order_id']){
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array('errorCode' => '0',
                             'errorDesc' => 'Success',
                             'orderId' => $xx['order_id'],
                             'amount' => $amount,
                             'ccy' => 'IDR',
                             'desc' => 'k-smart ziarah',
                             'trx_date' => date('d/m/Y H:i:s')
                             );
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
    
    function inquiryZiarahSgoDev(){
        $xx = $this->input->post(null,true);
        $password = 'k-net181183';
        $abc = $this->umrohModel->getAmtUmrohh($xx['order_id']);
        $tot = $abc[0]->tot_fund;
        $connectivity = $abc[0]->charge_connectivity;
        $amount = $tot + $connectivity ;
        //$amount = $amount + 7500;
        //$amount = 3500000;
        if(!$xx['order_id']){
            die('04;Order ID Tidak Ada;;;;;');
        }
        else if($xx['password'] != $password){
            die('04;Autentifikasi Salah;;;;;');
        }else{
            $trxData = array('errorCode' => '0',
                             'errorDesc' => 'Success',
                             'orderId' => $xx['order_id'],
                             'amount' => $amount,
                             'ccy' => 'IDR',
                             'desc' => 'k-smart ziarah',
                             'trx_date' => date('d/m/Y H:i:s')
                             );
            echo $trxData['errorCode'] . ';' . $trxData['errorDesc'] . ';' . $trxData['orderId'] . ';' . $trxData['amount'] . ';' . $trxData['ccy'] . ';' . $trxData['desc'] . ';' . $trxData['trx_date'];
        }
    }
    
    function paymentZiarahSgo(){
        /*if($this->_checkSessionStoreUser())
        {*/
            $xx = $this->input->post(null,true);
            $password = 'k-net181183';
            //$totalDP = 3500000;
            
            if(!$xx['amount'] ){   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
                $prefix = substr($xx['order_id'],0,1);
                if($prefix == 'Z'){
                    $updtTabel = $this->umrohModel->updtMutasiCicilan($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }else{
                    $updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }
                //$updtTabel = $this->umrohModel->updttblMutasi($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                
                $resSukses = array('success_flag' => 0,'error_message' => 'Success','reconcile_id' => rand(15,32),'order_id' => $xx['order_id'],'reconcile_datetime' =>date('d/m/Y H:i:s'));
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];   
            }
    }
    
    function paymentZiarahSgoDev(){
        /*if($this->_checkSessionStoreUser())
        {*/
            $xx = $this->input->post(null,true);
            $password = 'k-net181183';
            //$totalDP = 3500000;
            
            if(!$xx['amount'] ){   
                die('04,Total DP tidak sama,,,,,');
            }
            else if($xx['password'] != $password){
                die('04,Autentifikasi Salah,,,,,');
            }else{
                $prefix = substr($xx['order_id'],0,1);
                if($prefix == 'U' || $prefix == 'Z'){
                    $updtTabel = $this->umrohModel->updtMutasiCicilan($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }else{
                    $updtTabel = $this->umrohModel->updttblMutasi1($xx['order_id'],$xx['payment_ref'],$xx['amount'],$xx['debit_from_bank']);
                }
                //$updtTabel = $this->umrohModel->updtMutasiCicilan($xx['order_id'],$xx['payment_ref'],$xx['amount']);
                
                $resSukses = array('success_flag' => 0,'error_message' => 'Success','reconcile_id' => rand(15,32),'order_id' => $xx['order_id'],'reconcile_datetime' =>date('d/m/Y H:i:s'));
                echo $resSukses['success_flag'] . ',' . $resSukses['error_message'] . ',' . $resSukses['reconcile_id'] . ',' . $resSukses['order_id'] . ',' . $resSukses['reconcile_datetime'];   
            }
    }
    
    function finishPaymentZiarah($orderid)
    {
        $alternate2 = $this->load->database('alternate2', true);
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->session->unset_userdata('umroh');
            $this->session->unset_userdata('dp_umroh');
            $dt['dtJamaah'] = $this->umrohModel->getDtJamaah($orderid);
            if($dt['dtJamaah'] != null)
            {
                foreach($dt['dtJamaah'] as $row){
                    $noreg = $row->registerno;
                    $pktUmroh = $row->departuredesc;
                    $idmember = $row->dfno;
                    $voucherNo = $row->voucherno;
                    $isVoucher = $row->is_voucher;
                    $flagtipe = $row->flag_tipe;
                    $nama = $row->fullnm;
                    $telHp = $row->tel_hp;
                    $secno = $row->secno;
                    $cnno = $row->CNno;
                    $kdbank = '88146';
                    $va = $kdbank.$row->novac;
                    $tipeRohani = $row->tipe_perjalanan;
                }
                
                /*$parent = $this->umrohModel->getDtparent($noreg);
                foreach($parent as $xxx){
                    $tel_hp = $xxx->parenthp;
                }*/
                //if($isVoucher == '1'){
                    if($flagtipe == '1'){
                        $text = 'K-SMART ZIARAH: Reg no: '.$noreg.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.',VchNo: '.$voucherNo.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$noreg.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.'';
                    }elseif($flagtipe == '3'){
                        $text = 'K-SMART ZIARAH: Reg no: '.$regno.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.',Stk:'.$stk.',';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$noreg.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.'';
                        $textLanjut = 'LANJUTAN: VchNo:'.$voucherNo.',SK: '.$secno.',MM: '.$cnno.',VA: '.$va.'';
                        $smsLnjt = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                           VALUES ('".$telHp."','".$textLanjut."','K-Smart Umroh Status')";
                        $query2 = $alternate2->query($smsLnjt);
                    }else{
                        $text = 'K-SMART ZIARAH: Reg no: '.$noreg.',Nama:'.$nama.',Pkt Umroh: '.$pktUmroh.',VchNo: '.$voucherNo.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$noreg.',Nama:'.$nama.',Pkt Umroh: '.$pktUmroh.'';
                    }   
                //}
                
                $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$telHp."','".$text."','K-Smart Umroh Status')";
                
                /*$smsParent = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$tel_hp."','".$txtParent."','K-Smart Umroh Status')";*/
		        //echo "Ins SMS : $insSms";
		        $query = $alternate2->query($insSms);
                //$query2 = $alternate2->query($smsParent);
                
                $this->setTempWebShop('webshop/rohani/payUmroh_result',$dt);
            }else{
                $this->setTempWebShop('webshop/rohani/payUmroh_unfinish',$dt);
            }
        }
    }
    
    function getFormCicilanZiarah(){
        if($this->_checkSessionStoreUser())
        {
            $dt['bank'] = $this->umrohModel->getBank();
            //echo "abcde";
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->setTempWebShop('webshop/rohani/formInstallmentsZ',$dt);
        }
    }
    
    function getFormPayZiarah(){
        if($this->_checkSessionStoreUser())
        {
            //$regnNos = $this->input->post('regnos');
            $dt = $this->input->post(null,true);
            if($dt['regnos'] != "" && $dt['amtUmroh'] != ''){
                $xx['regno'] = strtoupper($dt['regnos']);
                $xx['cicilann'] = $dt['amtUmroh'];
                $aaa = array
                        (
                            'regno' => $dt['regnos'],
                            'amtUmroh' => $dt['amtUmroh']
                        );
                $this->session->set_userdata('regnos',$aaa);
                
                $charge = $this->umrohModel->getCharge($dt['tipepaymentt']);
                
                $xx['bankCode'] = $charge[0]->bankCode;
                $xx['bankDesc'] = $charge[0]->bankDesc;
                $xx['charge_connectivity'] = $charge[0]->charge_connectivity;
                $xx['charge_admin'] = $charge[0]->charge_admin;
                
                $xx['total'] = $xx['cicilann'] + $xx['charge_admin'];
                
                $xx['ins'] = $this->umrohModel->insPrePayment($dt,$charge[0]->bankCode,$charge[0]->charge_connectivity,$charge[0]->charge_admin);
                
                //key Production Ziarah
                //$xx['key'] = 'ed5c3103e371d7fbe4086b19c9e4955dtest';
                
                //key Develop Ziarah
                $xx['key'] = '7a19dd00e76e1ddc23affd76825123aa';  
                
                $xx['prodCat'] = $this->productService->getProductCategory();
                $this->setTempWebShop('webshop/rohani/formPayInstallZiarah',$xx);
            }else{
                redirect('umroh/installments');
            }
            
        }
    }
    
    function finishPayCicilanZiarah($orderid){
        $alternate2 = $this->load->database('alternate2', true);
        if($this->_checkSessionStoreUser()) {
            $dt['prodCat'] = $this->productService->getProductCategory();
            $this->session->unset_userdata('regnos');
            
            $dt['dtCicilan'] = $this->umrohModel->getDtCicilan($orderid);
            if($dt['dtCicilan'] != null)
            {
                foreach($dt['dtCicilan'] as $row){
                    $regno = $row->regno;
                    $idmember = $row->idmember;
                    $nmmember = $row->nmmember;
                    $hp = $row->tel_hp;
                    $tot_fund = $row->tot_fund;
                    $pktumrh = $row->departuredesc;
                }
                
                $text = 'CICILAN K-SMART ZIARAH: Reg no: '.$regno.',Pkt Ziarah: '.$pktumrh.',Id Member: '.$idmember.',Cicilan: '.$tot_fund.'';
                
                $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$hp."','".$text."','K-Smart Umroh Status')";
                $query = $alternate2->query($insSms);
                $this->setTempWebShop('webshop/rohani/payCicilan_finish',$dt);
            }else{
                $this->setTempWebShop('webshop/rohani/payCicilanUmroh_unfinish',$dt);
            }
        }
    }
    /*-----------------------------
             END ZIARAH
    ------------------------------*/
    
    //JIKA SEKALI DAFTAR BISA BEBERAPA JAMAAH
    /*
    function tambahJamaahUmroh()
    {
        $arr = array();
        if($this->_checkSessionStoreUser())
        {
            if($this->form_validation->run('umroh') == FALSE) 
            {
                $arr = jsonFalseResponse("Semua field harus diisi..!!");
                echo json_encode($arr);
            }
            else
            {
                $dt = $this->input->post(null,true);
                if($dt != null)
                {
                    $prev_sess = $this->session->userdata('umroh');
                    if($prev_sess != null) {
                        foreach($prev_sess as $dd){
                            $rr = $dd['idno'];
                            $pass = $dd['passportno'];
                        }
                        if($rr != $dt['idno'] || $pass != $dt['passportno'])
                        {
                            if(is_array($prev_sess))
                            {
                                array_push($prev_sess, $dt);
                                $this->session->set_userdata('umroh',$prev_sess);
                            }
                            else
                            { 
                                array_push($arr, $dt);
                                $this->session->set_userdata('umroh',$arr);
                            }
                            $arr = jsonTrueResponse(null,"Data Berhasil Ditambahkan");
                            echo json_encode($arr);
                        }else{
                            $arr = jsonFalseResponse("KTP atau Passport sudah Terpakai");
                            echo json_encode($arr);
                        }
                    } else {
                        array_push($arr, $dt);
                        //array_values($arr, $dt);
                        $this->session->set_userdata('umroh',$arr);
                        $arr = jsonTrueResponse(null,"Data Berhasil Ditambahkan");
                        echo json_encode($arr);
                    }   
                }
            }
        }
    }
    
    function previewJamaahUmroh(){
        $arr = array();
        if($this->_checkSessionStoreUser())
        {
            if($this->form_validation->run('umroh') == FALSE) 
            {
                echo "Semua field harus diisi..!!";
            }
            else
            {
                $dt = $this->input->post(null,true);
                if($dt != null)
                {
                    $prev_sess = $this->session->userdata('umroh');
                    if(is_array($prev_sess))
                    {
                        array_push($prev_sess, $dt);
                        $this->session->set_userdata('umroh',$prev_sess);
                    }
                    else
                    {
                        array_push($arr, $dt);
                        
                        $this->session->set_userdata('umroh',$arr);
                    }
                    
                    $dt['jamaah'] = $this->session->userdata('umroh');
                    $this->load->view('webshop/prevJamaahUmroh',$dt);
                }
            }
        }
    }
    
    function getPaymentUmroh(){
        $dt['prodCat'] = $this->productService->getProductCategory();
        $arr = $this->session->userdata('umroh');
        $xx2 = array_values($arr);
        $this->session->set_userdata('umroh',$xx2);        
        $dt['jamaah'] = $this->session->userdata('umroh');
        $jum = count($dt['jamaah']);
        $dt['total_biaya'] = $jum * 3500000;
        //$this->load->view('webshop/paymentFormUmroh',$dt);
        $this->setTempWebShop('webshop/paymentFormUmroh',$dt);
    }
    
    */
    
    
    /*
        ==== TESTING FUNCTION ====
    */
    function deleteJamaahSess($id){
        if($this->_checkSessionStoreUser()) {
           $xx = $this->session->userdata('umroh');
           unset($xx[$id]);
           $yx = $this->session->set_userdata('umroh',$xx);
           $xyz = $this->session->userdata('umroh',$yx);
           
           $arr = array("response" => "true", "message" => "Hapus sukses..!!");
           echo json_encode($arr);
        }
    }
    
    function getSessionUmroh(){
        $arr = $this->session->userdata('umroh');
        $abc = $this->session->userdata('dp_umroh');
        $jum = count($arr);
        echo "Jumlah : $jum <br>";
        echo "idmember : ".$arr[0]['idmember'];
       
        if($arr != null){
            foreach($arr as $test){
            echo "<br> Nama ".$test['fullnm'];
            echo "<br>";
            }
        }else{
            echo "<br>kosong";
        }
        
        
        
        print_r($arr);
        echo "<br>";
        print_r($abc);
        /*echo "<br>";
        print_r($this->session->all_userdata());*/
    }
    
    function clear_sess() {
       
       $this->session->unset_userdata('umroh');
       $this->session->unset_userdata('dp_umroh');
    }
    
    function cekArray(){
        $ktp = '3603042912840001';
        $xx = $this->session->userdata('umroh');
        foreach($xx as $dd){
            $rr = $dd['idno'];
        }
        if($rr == $ktp){
                //$y = count($rr);
                //echo "hasil ".$y;
                echo "ktp tidak boleh sama";
            }else{
                echo "ktp boleh sama";
            }
    }
    
    function testArrays(){
        $item1_details = array(
            'id' => 'a1',
            'price' => 500000,
            'quantity' => 2,
            'name' => 'Apel'
            );
        $item2_details = array(
            'id' => 'a2',
            'price' => 100500,
            'quantity' => 1,
            'name' => 'Jeruk'
            );
        
        $item_details = array ($item1_details, $item2_details);
        
        print_r($item_details);
    }
    
    function testsubstr(){
        /*$str = "1-coba test";
        $str1 = substr($str,2,50);
        
        echo "test ".$str1;*/
        $trcd = 'PV1506007922';
        $aa = substr($trcd,0,2);
        
        echo "substr ".$aa;
        
    }
    
    function sendSms(){
        $alternate2 = $this->load->database('alternate2', true);
        
        //$text = 'K-SMART UMROH: Reg no: U00000145,Pkt Umroh: Paket Umroh Sahabat,Id Member: IDUMROA00107,Stk: IDSAH378,VchNo: VUMR00094,SK: BUQOX,MM: MMU1508000006,VA: 150077592';
        $text = 'K-SMART UMROH: Reg no: U00000160,Pkt Umroh: Paket Umroh Sahabat,Id Member: IDJTBAA04855,VchNo: VUMR00124';
        $telhp = '087780441874';
        
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".$telhp."','".$text."','K-Smart Umroh Status')";
                      
		        //echo "Ins SMS : $insSms";
        $query = $alternate2->query($insSms);
    }
    
    function iframeSGo(){
        /*$tgllhr = 29;
        $blnlhr = 12;
        $thnlhr = 1997;*/
        $this->load->view('webshop/rohani/formPay_tes');
        /*$birthdate1 = "".$blnlhr."/".$tgllhr."/".$thnlhr."";
        $birthdate2 = strtotime($birthdate1);
        $birthdate = date("Y-m-d",strtotime($birthdate1));
        
        echo "birthdate ".$birthdate1."<br>";
        
        echo "birthdate strtotime ".$birthdate;*/
    }
    
    function testQuery(){
        $alternate = $this->load->database('db_ecommerce', true);
        $abc = $this->umrohModel->testtQuery();
        
        echo $abc;
    }
    
    function testMundur(){
        $d=strtotime("-2 Months");
        $x = date("M", $d);
        echo "bulan kemaren ".$x;
    }
    
    function testDbTest(){
        $xx = $this->umrohModel->insDbTest();
        /*if($xx['response'] == '1'){
            echo "vvv ".$xx['regno'];
        }*/
        if($xx == 1){
            echo "sukses";
        }else{
            echo "gagal";
        }
    }
    
    function getBanks(){
        $orderid = '67942393';
        $abc = $this->umrohModel->getAmtUmrohh($orderid);
        $connect = $abc[0]->charge_connectivity;
         $amount = $abc[0]->tot_fund + $abc[0]->charge_connectivity;
         $tot = $amount + $connect;
         echo "<br>total ".$amount;
         echo "<br>";
        $bank = '008';
        $charge = $this->umrohModel->getCharge($bank);
            
        $bankCode = $charge[0]->bankCode;
        $bankDesc = $charge[0]->bankDesc;
        $charge_connectivity = $charge[0]->charge_connectivity;
        $charge_admin = $charge[0]->charge_admin;
        
        echo "bank ".$bankCode." - ".$bankDesc." - ".$charge_connectivity." - ".$charge_admin."";
    }
    
 }

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 *
 * @author sahid
 */
class Member_lp extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->service("webshop/MemberLPService",'memberlp');
		
	}



	//$route['member/reg/(:any)/(:any)'] = 'webshop/member_lp/getRegisterMemberLandingPage/$1/$2';
	public function getRegisterMemberLandingPage($idmember, $email_pendaftar) {
				
				
			$dt['idmember'] = $idmember;
			$ss = str_replace("[at]", "@", $email_pendaftar);
			$ss2 = str_replace("[dot]", ".", $ss);
			$dt['email_pendaftar'] = $ss2;	
			
			$dt['prodCat'] = $this->memberlp->getProductCategory();	
			$dt['listBank'] = $this->memberlp->getListBank();		
			$this->setTempWebShopLP('webshop/member_lp/memberRegisterLP', $dt);		
	}
	
	//$route['member/reg/starterkit'] = 'webshop/member_lp/chooseStarterkit2';
	public function chooseStarterkit2() {
		
			$member_info = $this->session->userdata('member_info');
			$personal_info = $this->session->userdata('personal_info');
			$dtaForm = $this->input->post(NULL, TRUE);
			if($dtaForm != null) {
				$this->session->set_userdata('member_info', $dtaForm);
			} 
		    
			$dt['show_provinsi'] = $this->memberlp->showListProvinsi();    
		    $dt['listStarterkit'] = $this->memberlp->listStarterkit(); 
			//print_r($dt['listStartekit']);
			if($personal_info != null) {	
				//$dt['shipping'] = $this->cartshopService->getShippingData($personal_info);	
				$this->setTempWebShopLP('webshop/memberChooseSK_LP',$dt);
			} else {
				$this->setTempWebShopLP('webshop/memberChooseSK_LP',$dt); 
			}
			
	}
}
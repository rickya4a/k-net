<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class c_location extends MY_Controller{

    public function __construct(){

        parent::__construct();
        $this->load->model('lokasi/M_location');

    }

    public function get_lokasi($a1,$a2){

        $data['id']=$a1;
        $data['page']=$a2;
        $this->load->view('lokasi/stokist', $data);
    }

    public function otp_verifikasi($a1,$a2,$a3){

        $data['lat']=$a1;
        $data['long']=$a2;
        $data['page']=$a3;
        $this->load->view('lokasi/v_otp', $data);
    }

    
}
?>
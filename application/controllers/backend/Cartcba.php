<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cartcba extends MY_Controller {
    public function __construct() {
	    parent::__construct();
		//$this->load->model("webshop/Cart_model",'cartM');
        $this->load->model("webshop/Product_model",'productM');
	}

	function getProduct(){
            $dt['userid'] = "DION";
            $dt['usernm'] = "DION";
            $dt['formAction'] = "".site_url('backend/cartcba/addtocart')."";
            $dt['prod'] = $this->productM->getProductShop();

            $this->setTempWebShop('backend/shop_home',$dt);

    }


    function addtocart()
    {

            $dt['formAction'] = "".site_url('cart/addtocart')."";
            $dt['prod'] = $this->productM->getProductShop();
            // Set array for send data.
            $addCart = array(array(
                        'id' => $this->input->post('prdcd'),
                        'name' => $this->input->post('prdnm'),
                        'price' => $this->input->post('price'),
                        'qty' => 1
                        )
                    );

            $dt['qty'] = $addCart[0]['qty'];

            // This function add items into cart.
            $this->cart->insert($addCart);


            // This will show insert data in cart.
            $this->setTempWebShop('backend/shop_home',$dt);
            //redirect('shopping');

    }

    function listCart() {
        print_r($this->cart->contents());
    }


    function testInsPrd(){
        $this->db = $this->load->database('db_ecommerce', true);

        $insHdr = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity)
                    values('TEST16000044','0','189834096TES','IDSPAAA66834','JHON DOE',117000,20,'12W3','01/2016','01/02/2016 16:44:03','BID06','PT K-LINK NUSANTARA',
			             '17TES','W','1','1','01/02/2016 16:44:25',0,1000, '1', 0, 0)";

        $qryHdr = $this->db->query($insHdr);

        $insSgo = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo(orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
                    values('TEST16000044','HD006A','KLORO',1,20,117000,'12W3','2')";
        echo $insSgo;
        $qryLogin = $this->db->query($insSgo);

        $slc = "select * from db_ecommerce.dbo.ecomm_trans_det_prd_sgo where orderno = 'IDEC16000044TES'";
        $x = $this->db->query($slc);
        if($x->num_rows() >  0 ){
            $row = $x->row();

            $insLogin = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
                        values('".$row->orderno."','".$row->prdcd."','".$row->prdnm."',".$row->qty.",".$row->bvr.",".$row->dpr.",'12W3','".$row->sentTo."')";
            echo $insLogin;
            $qryLogin = $this->db->query($insLogin);
        }


        if(!$qryLogin && !$qryHdr){
            echo "gagal";
        }else{
            echo "sukses";
        }
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_klaimbonus extends MY_Controller {
	public function __construct() {
	   parent::__construct();
       $this->folderView = "backend/klaimbonus/";
       $this->load->model("backend/be_klaim_model", 'klaim');
	}



    function formListingKlaim(){
        if($this->username != null) {
            $x['form_header'] = 'Listing Klaim Bonus';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_klaimbonus';
            $this->setTemplate($this->folderView.'formListing', $x);
        }
        else
        {
            redirect('backend', 'refresh');
        }
    }


    function getListing(){
        if($this->username != null) {
            $data = $this->input->post(null,true);
            $data['result'] = $this->klaim->getListingKlaim($data['reg_dfno']);
            //print_r($data);
            $this->load->view($this->folderView.'getListing', $data);
        }
        else
        {
            redirect('backend', 'refresh');
        }
    }
    function getDetailKlaim($regno) {
        if($this->username != null) {
            $data['result'] = $this->klaim->getListingDetail($regno);
            $data['regno']=$regno;
            $data['form']=$this->klaim->getFormDetail($regno);;

            $this->load->view($this->folderView.'getDetailKlaim', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    function getProdukKlaim($bnsperiod,$dfno) {
        if($this->username != null) {
            $data['result'] = $this->klaim->getProdukKlaim($bnsperiod,$dfno);
            $data['bns']=$bnsperiod;
            $data['form']=$this->klaim->getFormDetail($dfno);;

            $this->load->view($this->folderView.'getProdukKlaim', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

}
<?php

    class SearchPrdcd extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = ('backend/search/');
            $this->load->model('backend/search/Be_search_model', 'mSearch');
            $this->load->model('webshop/Product_model_baru','mProduct');
        }

        public function formSearch(){

            if($this->username != null) {
                $data['form_header'] = "Search Product Code";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'be/getPrdcd/form';
                $this->setTemplate($this->folderView.'formSearch', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        public function actSearch(){

            $data = $this->input->post(NULL, TRUE);

            $data['result'] = $this->mProduct->getProdDetFlora($data['prod1_color'],$data['prod2_color'],$data['prod3_color'],$data['prod4_color'],$data['prod5_color'],$data['prod6_color'],$data['prod7_color']);
            //print_r($data['result']['0']);
            foreach($data['result'] as $dt){
                //echo $dt->prdcd;
                $prdcd = $dt->prdcd;
            }

            $data['detail'] = $this->mSearch->getDetPrd($prdcd);

            $this->load->view($this->folderView.'formActSearch', $data);
        }

        public function formSearch2(){

            if($this->username != null) {
                $data['form_header'] = "Search Product Code";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'be/getPrdcd/form2';
                $this->setTemplate($this->folderView.'formSearch2', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        public function actSearch2(){

            $data = $this->input->post(NULL, TRUE);

            $data['result'] = $this->mProduct->getProdDetFlora2($data['prod1_color'],$data['prod2_color'],$data['prod3_color'],$data['prod4_color']);
            //print_r($data['result']['0']);
            foreach($data['result'] as $dt){
                //echo $dt->prdcd;
                $prdcd = $dt->prdcd;
            }

            $data['detail'] = $this->mSearch->getDetPrd($prdcd);

            $this->load->view($this->folderView.'formActSearch2', $data);
        }
    }
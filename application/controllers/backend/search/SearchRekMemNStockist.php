<?php

	class SearchRekMemNStockist extends MY_Controller{

		public function __construct(){

			parent::__construct();

			$this->folderView = ('backend/search/');
			$this->load->model("backend/search/Be_search_model","mSearch");
		}

		public function searchRekMember(){

			if($this->username != null) {
				$data['form_header'] = "Search Rekening Member";
				$data['icon'] = "icon-list";
				$data['form_reload'] = 'search/rekening_member/form';
				$this->setTemplate($this->folderView.'formRekMember', $data);
			}

			else {

				redirect('backend', 'refresh');
			}
		}

		public function listRekMember(){

			$idname = $this->input->post('idname');

			//$dt['result'] = $this->mSearch->listRekMember($idname);

			$hasil = $this->mSearch->listRekMember($idname);
			//print_r($hasil['0']);
			//$this->load->view($this->folderView.'listRekMember',$dt);

			if($hasil != null){

				$arr = jsonTrueResponse($hasil);

			}else{

				$arr = jsonFalseResponses("data kososng!");

			}

			if('IS_AJAX') {

				echo json_encode($arr);
			}
		}

		public function searchRekStockist(){

			if($this->username != null) {
				$data['form_header'] = "Search Rekening Member";
				$data['icon'] = "icon-list";
				$data['form_reload'] = 'search/rekening_stockist/form';
				$this->setTemplate($this->folderView.'formRekStockist', $data);
			}

			else {

				redirect('backend', 'refresh');
			}
		}

		public function listRekStockist(){

			$idname = $this->input->post('idname');

			$dt['result'] = $this->mSearch->listRekStockist($idname);

			//print_r($hasil);
			$this->load->view($this->folderView.'listRekStockist',$dt);

//            if($hasil != null){
//
//                $data['response'] = true;
//
//                foreach($hasil as $dt){
//
//                    $data['STOCKIST'] = $dt->STOCKIST;
//                    $data['IDMEMBER'] = $dt->IDMEMBER;
//                    $data['NAME'] = $dt->NAME;
//                    $data['REKNAME'] = $dt->REKNAME;
//                    $data['REKNUM'] = $dt->REKNUM;
//                    $data['BANKCODE'] = $dt->BANKCODE;
//                    $data['ADDDATE'] = date("d-m-Y", strtotime($dt->ADDDATE));
//
//                }
//
//            }else{
//
//                $data['response'] = false;
//            }
//
//            if('IS_AJAX') {
//
//                echo json_encode($data);
//            }
		}

	}
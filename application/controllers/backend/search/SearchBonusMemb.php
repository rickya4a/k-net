<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class SearchBonusMemb extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->folderView = ('backend/search/');
        $this->load->model("backend/search/Be_search_model","mSearch");
    }


    //$route['search/bonus_member/form'] = 'backend/search/SearchBonusMemb/searchBonusMember';
    public function searchBonusMember() {
        if($this->username != null) {
            $data['form_header'] = "Cek Bonus Member Bulanan";
            $data['form_action'] = base_url('search/bonus_member/act');
            $data['icon'] = "icon-search";
            $data['form_reload'] = 'search/bonus_member/form';
            $this->setTemplate($this->folderView.'formBonusMember', $data);
        } else {
            echo sessionExpireMessage(false);
        }
    }

    public function listBonusMember() {
        $data = $this->input->post(NULL,TRUE);
        $idmember = trim($data['id_member']);
        $nmmember = $data['nm_member'];
        $year = $data['tahun'];

        $res['listbonusmemb'] = $this->mSearch->getBonusMember($idmember,$year);

        if($res['listbonusmemb'] != null){
            $rekmemb = $this->mSearch->listRekMember($idmember);
            foreach($rekmemb as $row1) {
                $res['rek_num'] = $row1->REKNUM;
                $res['bank'] = $row1->BANKCODE;
            }
        }


        $res['nama']=$nmmember;
        $res['idmemb']=$idmember;

        $this->load->view($this->folderView.'listBonusMember', $res);

    }

    public function getNameMemb(){
        $id_memb = trim($this->input->post('idmemb'));

        $get= $this->mSearch->getNameMember($id_memb);

        $rek_num="";
        $rek_name="";

        $rekmemb = $this->mSearch->listRekMember($id_memb);
        if($rekmemb != null){
            foreach($rekmemb as $row1) {
                $rek_num = $row1->REKNUM;
                $rek_name = $row1->REKNAME;
            }
        }


        if($get != null){
            foreach($get as $row) {
                $fullnm= $row->fullnm;
            }

            $arr = array("response" => "TRUE", "nama" => "$fullnm","norek" => "$rek_num","nmrek" => "$rek_name");
            echo json_encode($arr);

        } else {
            $arr = array("response" => "FALSE", "nama" => "", "norek" => "", "nmrek" => "");
            echo json_encode($arr);
        }

    }

}
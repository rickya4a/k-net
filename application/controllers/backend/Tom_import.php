<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Tom_import extends MY_Controller {

    public function __construct() {
		// Call the Model constructor
        parent::__construct();
        $this->folderView = "backend/etiket/";            
    }

    //$route['tom/import/manual'] = 'backend/tom_import/formImportTom';
    public function formImportTom() {
        if ($this->username != null) {
            $this->load->library('csvreader');
            $data['form_header'] = "Import TOM Manual syarat PNE";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'tom/import/manual';
            $this->setTemplate($this->folderView.'tomImportManual', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['tom/import/manual/read'] = 'backend/tom_import/readFile';
    public function readFile() {
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            //print_r($data['csvData']);
            //print_r($data['csvData'][1]);
            /*foreach($data['csvData'] as $field) {
             echo $field[0]. " - ";
             echo $field[1];
             echo "<br />";
            }	*/
            $this->load->view($this->folderView.'tomPreviewFile', $data);
        } else {
            echo "No Data Uploaded";
        }
    }

    //$route['tom/import/manual/save'] = 'backend/tom_import/saveImportTom';
    public function saveImportTom() {
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            if($data['csvData'] != null) {
                $dbqryx  = $this->load->database("db_ecommerce", TRUE);    
                $sudahada = 0;
                $insertSukses = 0;
                $insertGagal = 0;
                $existArr = array();
                $insArr = array();
                $failArr = array();
                $urut = 1;
                foreach($data['csvData'] as $field) {
                    $idmember = $field[0];
                    $namamember = $field[1];

                    //cek apakah member sudah ada
                    $dbqryx->select('order_no, valid_dfno');
				    $dbqryx->from('db_ecommerce.dbo.trx_etiket');
                    $dbqryx->where('status', '0');
                    $dbqryx->where('id_etiket', '203');
                    $dbqryx->where('valid_dfno', $idmember);
				    $query = $dbqryx->get();	
                    $hasil = $query->result();

                    
                    //jika record sudah ada, tidak perlu diinput
                    if($hasil != null) {
                        array_push($existArr, $idmember);
                        $sudahada++;
                    } else {
                        //Insert record
                        $user = $this->username;
                        $arr = array(
                            "urut" => $urut,
                            "order_no"  => "INPMANUAL-".$user,
                            "dfno"  => $user,
                            "fullnm"  => $user,
                            "notiket"=> "TIKET1",
                            "id_etiket"=> "203",
                            "flag_online" => "F",
                            "price" => 320000,
                            "valid_dfno" => $idmember,
                            "valid_fullnm" => $namamember,
                            "createdt" => "2017-11-23",
                            "createnm" => "ADMIN",
                            "status" => "0",
                            "statusHitung" => "0",
                            "is_early" => "1",
                        );
                        $dbqryx->insert('trx_etiket',$arr);
                        $urut++;
                        $insertSukses++;
                        array_push($insArr, $idmember);
                    }
                }

                $rec['insArr'] = $insArr;
                $rec['existArr'] = $existArr;
                $rec['exist'] = $sudahada;
                $rec['ins'] = $insertSukses;
                $this->load->view($this->folderView.'tomImportSaveResult', $rec);
            }
            
        } else {
            echo "No Data Uploaded";
        }
    }
        
}
        
/* End of file  Tom_import.php */
        
                            
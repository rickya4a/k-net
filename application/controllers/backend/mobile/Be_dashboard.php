<?php

    class Be_dashboard extends MY_Controller{

        public function __construct(){

            parent::__construct();
        }

        public function index(){

            $dt['username'] = getBeMobileUser();
            $this->setTempWebBackendM('dashboard', $dt);
        }
    }
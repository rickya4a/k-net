<?php

    class Be_login extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->load->model("backend/mobile/Be_m_login","mLogin");
        }

        public function index(){

            $this->session->sess_destroy();
            $this->load->view('backend/mobile/formLogin');
        }

        public function actLogin(){

//            $this->form_validation->set_rules('username', 'Username', 'required|max_length[32]|');
//            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]|');
//
//            if($this->form_validation->run() == FALSE){
//
//                $this->index();
//            }else{

                $username = $this->input->post('username');
                $password = $this->input->post('password');

//                $pass = password_hash($password, PASSWORD_DEFAULT);
//                echo $pass;


                $insert = $this->mLogin->updateLogin($username);
                $result = $this->mLogin->loginAct($username, $password);

                if(count($result) > 0){

                    foreach($result as $dt){

                        $sessArray = array(
                            'userId' => $dt->id,
                            'username' => $dt->username,
                            'isLoggedIn' => TRUE
                        );

                        $this->session->set_userdata('data_login',$sessArray);
                        redirect('backend/mobile/dashboard');

                    }

                }else{

                    echo '<script language="javascript">';
                    echo 'alert("Username dan password salah !")';
                    echo '</script>';

                    $this->index();
                }
            //}
        }

        public function showSessLogin(){

            echo "<pre>";
            print_r($this->session->userdata('data_login'));
            echo "</pre>";
        }

        public function logout(){

            $this->session->sess_destroy();
            $this->load->view('backend/mobile/formLogin');
        }
    }
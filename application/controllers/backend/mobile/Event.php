<?php

    class Event extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->load->model("backend/mobile/Be_m_all","mCuy");
            $this->folderView = "backend/mobile/";
        }

        public function index(){

            $dt['username'] = getBeMobileUser();

            if($dt['username'] != null){


                $this->setTempWebBackendM('v_pin', $dt);
            }else{

                redirect('backend/mobile');
            }
        }

        public function getName(){

            $idmember = $this->input->post('idmember');

            $result = $this->mCuy->getName($idmember);
            //print_r($dt['result']);
            if($result != null){

                foreach($result as $dt){

                    $fullnm = $dt->fullnm;

                    $arr = array("response" => "TRUE", "nama" => "$fullnm");
                    echo json_encode($arr);
                }
            }else{

                $arr = array("response" => "FALSE", "nama" => "");
                echo json_encode($arr);
            }
        }

        public function getTotalBv(){

            $idmember = $this->input->post('idmember');

            $dt['result'] = $this->mCuy->totBV($idmember);

            $this->load->view($this->folderView.'v_detBV', $dt);
        }
    }
<?php

    class User extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->load->model("backend/mobile/Be_m_all","mCuy");

        }

        public function index(){

            $dt['username'] = getBeMobileUser();

            if($dt['username'] != null){

                $dt['list_user'] = $this->mCuy->getDataUser();
                $this->setTempWebBackendM('v_user', $dt);
            }else{

                redirect('backend/mobile');
            }


        }
    }
<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barcode extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this -> folderView = "backend/stockist/";
		$this -> load -> model('backend/stockist/be_barcode_model', 'm_barcode');
	}

	//$route['barcode/upload'] = 'backend/barcode/uploadPrdBarcodeForm';
	public function uploadPrdBarcodeForm() {
		$data['form_header'] = "Upload Barcode Stockist";
		$data['icon'] = "icon-pencil";
		$data['form_reload'] = 'barcode/upload';
		try {
			$this -> checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e -> getMessage();
		}
        $this -> setTemplate($this -> folderView . 'uploadPrdBarcodeForm', $data);
	}

	//$route['barcode/check/do/(:any)/(:any)'] = 'backend/barcode/getValidDO/$1/$2';
	//$route['barcode/check/do/(:any)'] = 'backend/barcode/getValidDO/$1';
	public function getValidDO($do) {
		//$arr = $this->m_barcode->getValidDO($shipto, $do);
		$arr = $this->m_barcode->getValidDO($do);
		echo json_encode($arr);
	}

	//$route['barcode/preview'] = 'backend/barcode/previewFileCSV';
	public function previewFileCSV() {
		$fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName == "" || $fileName == null) {
			setErrorMessage("Silahkan pilih file untuk diupload..");
		} else {
			$file_content = file_get_contents($fileName);
	        $isiFile = explode("|", $file_content);
			$jum = count($isiFile);
			echo "Jumlah Data : $jum<br />";
			print_r($isiFile);
		}
	}

	//$route['barcode/upload/save'] = 'backend/barcode/saveUploadPrdBarcode';
	public function saveUploadPrdBarcode() {
		$data = $this->input->post(NULL, TRUE);
		$fileName = $_FILES["myfile"]["tmp_name"];
		$this->form_validation->set_rules('idstk', 'ID Stockist', 'trim|required');
		$this->form_validation->set_rules('whcd', 'WH Code', 'trim|required');
		$this->form_validation->set_rules('prdcd', 'Product Code', 'trim|required');
		if($this->form_validation->run() == FALSE) {
			setErrorMessage("Silahkan isi ID Stockist, WH Code dan Product Code");
		}
		else if($fileName == "" || $fileName == null) {
			setErrorMessage("Silahkan pilih file untuk diupload..");
		} else {
			$file_content = file_get_contents($fileName);
	        $listBarcode = explode("|", $file_content);
			$jum = count($listBarcode);
			$save = $this->m_barcode->saveUploadPrdBarcode($data, $listBarcode, $jum);
			echo "Jumlah barcode : ".$jum;
			echo "<br />";
			echo "Insert success : ".$save['success']; 
		}
	}

}

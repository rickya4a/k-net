
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_dtc extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->folderView = "backend/dtc/";
        $this->load->model("backend/be_klaim_model", 'klaim');
        $this->load->model("dtc/Mdtc_mbr",'mdtc');

    }


//$route['be_dtc'] = 'backend/Backend_dtc/formListing';
    function formListing(){
        if($this->username != null) {
            $x['form_header'] = 'DTC';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_dtc';
            $x['isiTabel']=$this->mdtc->tabelData();
            $x['alamat']=$this->mdtc->getAcara2();
            $x['bank']=$this->mdtc->getBank();
            $x['key']="key";
            $x['payID']="payID";
			
			//$this->load->model('backend/be_etiket_model');
			//$x['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket(); 
			$x['listTipeAcara'] = $this->mdtc->getListTipeAcara();
            $x['action'] = site_url('be_dtc/submit');

            $this->setTemplate($this->folderView.'formListing', $x);
        }
        else
        {
            redirect('backend', 'refresh');
        }
    }


    function getListing(){
        if($this->username != null) {
            $data = $this->input->post(null,true);
            $data['result'] = $this->mdtc->checkID1($data['reg_dfno']);
            //print_r($data);
            $this->load->view($this->folderView.'getListing', $data);
        }
        else
        {
            redirect('backend', 'refresh');
        }
    }

	//$route['be_dtc/getDetailTiketV2'] = 'backend/Backend_dtc/getDetailTiketV2';
	function getDetailTiketV2() {
		$opsi = $this->input->post('opsi');

        $return = $this->mdtc->getDetailTiketV2($opsi);
		
		$data['tglnow']=date('Y-m-d H:m');
		$data['earlybird_date']=  date("Y-m-d H:m",strtotime($return[0]->earlybird_date));
		if($data['tglnow']>=$data['earlybird_date']){
			$sisa_kuota = $return[0]->max_kuota - $return[0]->jumlah_terjual;
			if($sisa_kuota < 0) {
				$sisa_kuota = 0;
			}
			$data['sisa_kuota']=  $sisa_kuota;
		    $data['jumlah_terjual']=  $return[0]->jumlah_terjual;
			$data['max_kuota']=  $return[0]->max_kuota;
			$data['price_offline']=  $return[0]->price_offline;
        	$data['max_offline']=  $return[0]->max_offline;
        	$data['price_online']=  $return[0]->price_online;
        	$data['max_online']=  $return[0]->max_online;
			$data['early_note'] = 'Harga Normal'; 	
			$data['earlystatus'] = "1";
			//$data['arrayData'] = $return;
			
			$data['merchandise'] = $this->mdtc->getMerchandiseByIdEtiket($opsi);
		}else{
			$sisa_kuota = $return[0]->max_kuota - $return[0]->jumlah_terjual;
			if($sisa_kuota < 0) {
				$sisa_kuota = 0;
			}
			$data['sisa_kuota']=  $sisa_kuota;
		    $data['jumlah_terjual']=  $return[0]->jumlah_terjual;
			$data['max_kuota']=  $return[0]->max_kuota;
			$data['price_offline']=  $return[0]->price_earlybird_online;
        	$data['max_offline']=  $return[0]->max_offline;
        	$data['price_online']=  $return[0]->price_earlybird_online;
        	$data['max_online']=  $return[0]->max_online;
			$data['early_note'] = 'Early Bird...!!';
			$data['earlystatus'] = "0";
			$data['merchandise'] = $this->mdtc->getMerchandiseByIdEtiket($opsi);
		}		
		//$data['price_earlybird_online']=  $return->price_earlybird_online;

        if('IS_AJAX') {
            echo json_encode($data);
        }
	}



    function getDetailTiket()
    {
        $opsi = $this->input->post('opsi');

        $return = $this->mdtc->getDetailTiket($opsi)->row();
		
		$data['tglnow']=date('Y-m-d H:m');
		$data['earlybird_date']=  date("Y-m-d H:m",strtotime($return->earlybird_date));
		if($data['tglnow']>=$data['earlybird_date']){
			$data['price_offline']=  $return->price_offline;
        	$data['max_offline']=  $return->max_offline;
        	$data['price_online']=  $return->price_online;
        	$data['max_online']=  $return->max_online;
			$data['early_note'] = 'Harga Normal'; 	
			$data['earlystatus'] = "1";
			//$data['arrayData'] = $return;
			
			$data['merchandise'] = $this->mdtc->getMerchandiseByIdEtiket($opsi);
		}else{
			$data['price_offline']=  $return->price_earlybird_online;
        	$data['max_offline']=  $return->max_offline;
        	$data['price_online']=  $return->price_earlybird_online;
        	$data['max_online']=  $return->max_online;
			$data['early_note'] = 'Early Bird...!!';
			$data['earlystatus'] = "0";
			$data['merchandise'] = $this->mdtc->getMerchandiseByIdEtiket($opsi);
		}		
		//$data['price_earlybird_online']=  $return->price_earlybird_online;

        if('IS_AJAX') {
            echo json_encode($data);
        }
    }

    function submit(){
        if($this->input->post('nmpendaftar')==null){
            echo setErrorMessage('Data tidak lengkap');

        }
        else{


            $idmbr = $this->input->post('idmbr');
            $fullnm = $this->input->post('display_info');
            $tkt = $this->input->post('opsi');
            $tgltrf=$this->input->post('tgltrf');
            $pembayaran=$this->input->post('pembayaran');
            $rekening=$this->input->post('rekening');
            $notelp=$this->input->post('notelprow');
            $email=$this->input->post('emailrow');
            $remark=$this->input->post('REMARK');
			$isEarly = $this->input->post('early_status');

            $idtiket = $this->input->post('event_db');
            $nmtiket = $this->input->post('event_nm');
            $jumlah = $this->input->post('jumlah');
			
			$prd_bundling = $this->input->post('prd_bundling');
			
            $realtotal=0;
            foreach ($this->input->post('nmpendaftar') as $xdx => $v) {
                $realtotal=$realtotal+$jumlah[$xdx];
            }


            $valid_dfno = $this->input->post('idpendaftar');
            $valid_name = $this->input->post('nmpendaftar');
            $orderno = $this->my_counter->getCounter2('TA', $this->uuid->v4());
            $flag_online = 'F';
            $price = $this->input->post('harga');
            $createnm = $this->username;

            $kE = 0;
            $PO_DATE = strtotime($this->input->post('PO_DATE'));
            $year = date("Y", $PO_DATE);
            $month = date("m", $PO_DATE);
            $totalharga = 0;
            $duplicate=0;
//            echo  $this->mdtc->Counter2($idtiket);
            $urutan = 0;
            $qtytot = $this->input->post('qtytot');
//            echo $tkt;
//            echo "<script type='text/javascript'>alert('pembelian melebihi stok = $idtiket[0]'  );</script>";

            $return = $this->mdtc->getDetailTiket($idtiket[0])->row();
            $price_offline = $return->price_offline;
            $max_offline = $return->max_offline;
			
			
//            echo $price_offline;
//            echo $max_offline;
            $hrgtot = $price_offline * $realtotal;

            if ($realtotal > $max_offline) {
                echo setErrorMessage('Pembelian melebihi stok');
            } else {
                $minus=array(
                    'max_offline'=>$max_offline-$realtotal
                );
                $this->mdtc->updateStok($idtiket[0], $minus);

                if (!empty($idtiket)) {

                    foreach ($this->input->post('nmpendaftar') as $xdx => $v) {
                        for ($k = 0; $k < $jumlah[$xdx]; $k++) {
                            $apdup=$this->mdtc->duplicate($valid_dfno[$xdx],$idtiket[$xdx] );
                            if($apdup== true){
                                $duplicate=$duplicate+1;
                            }
                        }
                    }
                    if($duplicate==0)
                    {
						foreach ($this->input->post('nmpendaftar') as $xdx => $v) {
							for ($k = 0; $k < $jumlah[$xdx]; $k++) {
							    //UPDATE DION 09/01/2019
                                /*
								$xd = $this->mdtc->Counter2($idtiket[$xdx]);
								$urutan++;
								$no = "TA" . str_pad($idtiket[$xdx], 3, "000", STR_PAD_LEFT) . str_pad($xd, 4, "00", STR_PAD_LEFT);
                                */ 
								
								$no = $this->mdtc->getNoTiket($idtiket[$xdx]);
								$urutan++;
								
								$arr_dtl = array(
									//'urut' => $xd,
									'urut' => $urutan,
									'order_no' => $orderno,
									'dfno' => $idmbr,
									'fullnm' => $fullnm,
									'notiket' => $no,
									'id_etiket' => $idtiket[$xdx],
									'price' => $price[$xdx],
									'flag_online' => $flag_online,
									'valid_dfno' => $valid_dfno[$xdx],
									'valid_fullnm' => $valid_name[$xdx],
									'status' => '0',
									'createdt' => date("Y-m-d h:i:s"),
									'createnm' => $createnm,
									'tgltrf' => $tgltrf,
									'pembayaran' => $pembayaran,
									'rekening' => $rekening,
									'remark' => $remark,
									'notelp' => $notelp[$xdx],
									'email' => $email[$xdx],
									'is_early' => $isEarly

							);
							
							//print_r($arr_dtl);
								$kE++;
								$totalharga = $totalharga + $price[$xdx];
								$this->mdtc->saveDetail($arr_dtl);
								
								$this->mdtc->insertPrdBundlingTicket($no, $idtiket[$xdx], $prd_bundling[$xdx]);
								
								$link="www.k-net.co.id/vtiket2/".$no;
								$this->mdtc->sendTrxSMS($valid_name[$xdx],$nmtiket[$xdx],$no,$link,$notelp[$xdx] );
								$this->kirimEmail($valid_name[$xdx],$nmtiket[$xdx],$no,$link,$email[$xdx] );

							}

							$arr_det_prd_sgo = array(
								'orderno' => $orderno,
								'prdcd' => $idtiket[$xdx],
								'prdnm' => $nmtiket[$xdx],
								'qty' => $jumlah[$xdx],
								'bvr' => '0',
								'dpr' => $price[$xdx],
								'pricecode' => '12W3'
							);
							$this->mdtc->simpanDetTrx($arr_det_prd_sgo);

						}
                    
						$arr_hdr_sgo = array(
							'orderno' => $orderno,
							'token' => $orderno,
							'id_memb' => $idmbr,
							'nmmember' => $fullnm,
							'total_pay' => $totalharga,
							'total_bv' => '0',
							'pricecode' => '12W3',
							'bonusmonth' => date('m') . '/' . date('Y'),
							'datetrans' => date("Y-m-d h:i:s"),
							'idstk' => 'BID06',
							'nmstkk' => 'BID06 - PT. K-LINK NUSANTARA',
							'flag_trx' => 'F',
							'sentTo' => '1',
							'status_vt_pay' => '0',
							'payShip' => '0',
							'secno' => '',

							'payAdm' => '0',
							'is_umroh' => '0',
							'bank_code_payment' => '17',
							'userlogin' => $this->username,
							'payConnectivity' => '0',
							'is_login' => '1',
							'totPayCP' => '0',
							'free_shipping' => '0',
							'discount_shipping' => '0'
						);


						$arr_paydet_sgo = array(

							'orderno' => $orderno,
							'seqno' => '1',
							'paytype' => 'sgo',
							'docno' => '',
							'payamt' => $totalharga,
							'deposit' => '0',
							'notes' => 'pending',
							'paystatus' => 'ok',
							'bank_code_payment' => '17',
							'charge_admin' => '0'


						);
						$status_trx = array(
							'orderno' => $orderno,
							'status' => '1',
							'dfno' => $idmbr,
							'createnm' => $idmbr,
							'createdt' => date("Y-m-d h:i:s")

						);

						$this->mdtc->simpanTrx($arr_hdr_sgo, $arr_paydet_sgo,$status_trx);
						$this->mdtc->generateIP($orderno);
						echo setSuccessMessage("Data tersimpan, no transaksi $orderno");
                    }
                    else
                        echo setErrorMessage("Data duplikat");

                }else
                    echo setErrorMessage('Pembelian melebihi stok');



            }


//            if($kE > 0)
//            {
////                $this->m_PO->add($arr_data);
//            }else{
//
//            }

//            echo '<script type="text/javascript">',
//            'All.reload_page("be_dtc");',
//            '</script>'
//            ;
        }
    }

//        $this->load->view('index',$data);

    function kirimEmail($valid_name,$nmtiket,$no,$link,$email) {
        //$this->load->service("webshop/Payment_member_service",'payMemberService');
        //$resultInsMemb = $this->payMemberService->getNewMemberData($orderno);
        $from = "noreply@k-link.co.id";
        $fromname = "PT K-link Indonesia";

        $api_key = "87d9af6eab922de72f919312ce9d3e4a";
        $subject = "DTC ";
        $content = 'Selamat, a/n '.$valid_name.' telah terdaftar dalam : '.$nmtiket.'. Nomor e-tiket anda: '.$no.' atau klik '.$link;
        //$content = "asdasdad";
        $data=array();
        //rawurlencode
        $data['subject']= $subject;
        $data['fromname']= $fromname;
        $data['api_key'] = $api_key;
        $data['from'] = $from;
        $data['content']= $content;
        $data['recipients']= $email;
        $apiresult = callApi(@$api_type,@$action,$data);
//        echo trim($apiresult);
    }
    function kirimEmail2() {
        //$this->load->service("webshop/Payment_member_service",'payMemberService');
        //$resultInsMemb = $this->payMemberService->getNewMemberData($orderno);
        $from = "noreply@k-link.co.id";
        $fromname = "PT K-link Indonesia";

        $api_key = "87d9af6eab922de72f919312ce9d3e4a";
        $subject = "DTC ";
        $content = 'Selamat, a/n  telah terdaftar dalam :  Nomor e-tiket anda: atau klik ';
        //$content = "asdasdad";
        $data=array();
        //rawurlencode
        $data['subject']= $subject;
        $data['fromname']= $fromname;
        $data['api_key'] = $api_key;
        $data['from'] = $from;
        $data['content']= $content;
        $data['recipients']= 'aldi.web16@gmail.com';
        $apiresult = callApi(@$api_type,@$action,$data);
        echo trim($apiresult);
    }
}
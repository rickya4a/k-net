<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

	class K_wallet_recon extends MY_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->model('backend/k_wallet', "kwallet");
			$this->folderView = 'backend/transaction/k-wallet/';
		}

		// $route['k-wallet/recon/trx'] = 'backend/k_wallet_recon/kWalletForm';
		function kWalletForm() {
			if($this->username != NULL) {
				$data['form_header'] = "K-Wallet Reconcile Trx";
				$data['icon'] = "icon-pencil";
				$data['form_reload'] = "k-wallet/recon/trx";
				$data['username'] = $this->username;
				$this->setTemplate($this->folderView.'kWalletReconForm', $data);
			} else {
				redirect('backend', 'refresh');
			}
		}

		// $route['k-wallet/recon/trx/list/'] = 'backend/k_wallet_recon/reportList/';
		function reportList() {
			// $this->load->model('backend/k_wallet', "kwallet");
			$param = $this->input->post(NULL, TRUE);

			$data['from'] = $param['date_from'];
			$data['to'] = $param['date_to'];

			if ($param['type'] == "1" && $param['trx'] == "1") {
				$data['result'] = $this->kwallet->getFailedTrx($data['from'], $data['to']);
				$this->load->view($this->folderView.'listK-netSales', $data);
			} elseif ($param['type'] == "2" && $param['trx'] == "1") {
				$data['result'] = $this->kwallet->listTransaksiKnetGagalPotongsaldo($data['from'], $data['to']);
				$this->load->view($this->folderView.'listKnetTrxGagalPtgSaldo', $data);
			} elseif ($param['type'] == "1" && $param['trx'] == "2") {
				$data['result'] = $this->kwallet->listXlTrxGagal($data['from'], $data['to']);
				$this->load->view($this->folderView. 'listXLSales', $data);
			}
		}

		// $route['k-wallet/recon/token/update'] = 'backend/k_wallet_recon/updateToken'
		function updateToken() {
			$data['token'] = $this->input->post('trcd');
			$data['orderno'] = $this->input->post('order_number');
			$check = $this->kwallet->getTrcd($data['token']);

			if ($check == NULL) {
				$return = jsonFalseResponse("Data token tidak ada di table va_cust_pay_det..");
			} else {
				$hasil = $this->kwallet->updateTrcd($data['orderno'], $data['token']);
				$return = jsonFalseResponse("Data token gagal di update..");
				if($hasil['response'] == "true") {
					$return = jsonTrueResponse(null, "Data token $data[token] berhasil di update menjadi $data[orderno]");
				}
			}

			echo json_encode($return);
		}

		//$route['k-wallet/recon/insert/failtrx'] = 'backend/k_wallet_recon/insertFailTrx';
		public function insertFailTrx() {
			$data = $this->input->post(NULL, TRUE);
			$awalan = substr($data['token'], 0, 2);

			$this -> load -> model("backend/be_hifi_model", "hifi");
			$saldo = $this->hifi->getSaldoVa($data['userlogin']);
			//print_r($saldo);
			if($saldo != null) {
				$sisaSaldo = (int) $saldo[0]->amount;
				if($awalan == "RM") {
					$trtype = "TRM";
					$hasil = $this->kwallet->getDetailTrx($data['orderno']);
					$nominal_trx = $hasil[0]->total_trx;
					$userlogin = $hasil[0]->userlogin;
					$novac = $hasil[0]->novac;
				} else if($awalan == "EC" || $awalan == "EM") {
					$trtype = "TRS";
					$hasil = $this->kwallet->getDetailTrx($data['orderno']);
					$nominal_trx = $hasil[0]->total_trx;
					$userlogin = $hasil[0]->userlogin;
					$novac = $hasil[0]->novac;
				}
				if($nominal_trx <= $sisaSaldo) {
					$this->load->service("webshop/Knet_payment_service",'paymentService');
					$double = $this->kwallet->checkDoubleInputKwallet($data['token'], $data['orderno']);
					//print_r($double);
					if($double == null) {
						$dbqryx  = $this->load->database("db_ecommerce", TRUE);
						$arr = array(
							"trcd"    => $data['orderno'],
							//TRS = Transaksi K-NET Sales
							"trtype"  => $trtype,
							"effect"  => "-",
							"effect_acc"  => "+",
							"custtype"=> "M",
							"novac"   => $novac,
							//"dfno"    => $header['memberid'],
							"dfno"    => $userlogin,
							"docno"   => "",
							"refno"   => "RECON",
							"amount"  => $nominal_trx,
							"createnm"=> $userlogin,
							"tipe_dk" => "D",
							"tipe_dk_acc" => "K"
						);
						$dbqryx->insert('va_cust_pay_det', $arr);
						$dbqryx->select('trcd, amount');
						$dbqryx->from('va_cust_pay_det');
						$dbqryx->where('trcd', $data['orderno']);
						$query = $dbqryx->get();
						$hasil = $query->result();
						if($hasil != null) {
							$resJson = jsonTrueResponse($hasil, "Transaksi berhasil di insert ke va_cust_pay_det..");
						} else {
							$resJson = jsonFalseResponse("Transaksi gagal di insert ke va_cust_pay_det");
						}
					} else {
						$resJson = jsonFalseResponse("Transaksi double..");
					}
				} else {
					$resJson = jsonFalseResponse("Saldo VA anda tidak cukup : ".number_format($sisaSaldo, 0, ",", "."). ", total pembayaran : ".number_format($nominal_trx, 0, ",", "."));
				}
			} else {
				$resJson = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
			}
			echo json_encode($resJson);
		}

		// $route['k-wallet/topup/import'] = 'backend/k_wallet_recon/formImportTopUpKWallet';
		function formImportTopUpKWallet() {
			if($this->username != NULL) {
				$data['form_header'] = "Import Top Up K-Wallet";
				$data['icon'] = "icon-pencil";
				$data['form_reload'] = "k-wallet/topup/import";
				$this->setTemplate('backend/transaction/k-wallet/importTopUpKWallet', $data);
			} else {
				redirect('backend', 'refresh');
			}
		}

	//$route['k-wallet/topup/import/preview'] = 'backend/k_wallet_recon/previewFileCSVTopUp';
    public function previewFileCSVTopUp() {
        $form = $this->input->post(NULL, TRUE);
		$this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != ""){
			$this->csvreader->set_separator(",");
	        $data['csvData'] = $this->csvreader->parse_file($fileName);

		    if($form['download_type'] == "goworld") {
				$this->load->view('backend/transaction/sgoPreviewFileGoWorld', $data);
			} else {
				$this->load->view('backend/transaction/sgoPreviewFile', $data);
			}
		}else{
			echo "No Data Uploaded";
		}

    }

	//$route['k-wallet/topup/import/save'] = 'backend/k_wallet_recon/importTopUpKwallet';
	public function importTopUpKwallet() {
		$form = $this->input->post(NULL, TRUE);
		$this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != ""){
			$this->csvreader->set_separator(",");
	        $data['csvData'] = $this->csvreader->parse_file($fileName);
			$save = $this->kwallet->saveImportToDB($data);
			$this->load->view($this->folderView.'sgoImportResultGW', $save);
		}else{
			echo "No Data Uploaded!";
		}
	}

	//$route['k-wallet/recon/xl/balance/recover'] = 'backend/k_wallet_recon/recoverBalanceXL';
	public function recoverBalanceXL() {
		$data = $this->input->post(NULL, TRUE);
		$json = jsonFalseResponse("Transaksi XL $data[trcd] berhasil di dibatalkan..");
		try {
			$hasil = $this->kwallet->checkIfRecoverTrxExist($data['trcd']);
			$dbqryx  = $this->load->database("db_ecommerce", TRUE);
			$rec_id = "SR".date("ymdhis");
			$arr = array(
				"trcd"    => $rec_id,
				"trtype"  => "SR",
				"effect"  => "+",
				"effect_acc"  => "-",
				"custtype"=> "M",
				"novac"   => $hasil[0]->novac,
				"dfno"    => $hasil[0]->dfno,
				"docno"   => "REFUND XL TRX",
				"refno"   => $data['trcd'],
				"amount"  => $hasil[0]->amount,
				"createnm"=> $hasil[0]->createnm,
				"tipe_dk" => "K",
				"tipe_dk_acc" => "D"
			);
			$recover = $this->kwallet->recoverSaldoKwalletFromXLTrx($arr);
			if($recover['response'] == "true") {
				$json = jsonTrueResponse(null, "Transaksi XL $data[trcd] berhasil di dibatalkan..");
			}
		} catch (Exception $e) {
			$message = $e->getMessage();
			$json = jsonFalseResponse($message);
		}
		echo json_encode($json);
	}

	//$route['k-wallet/recon/knet/balance/recover'] = 'backend/k_wallet_recon/recoverBalance';
	public function recoverBalance() {
		$data = $this->input->post(NULL, TRUE);
		$json = jsonFalseResponse("Transaksi K-NET $data[trcd] berhasil di dibatalkan..");
		try {

			$checkIfTrxAlreadyDeleted = $this->kwallet->checkIfTrxDeleted($data['trcd']);
			$hasil = $this->kwallet->checkIfRecoverTrxExist($data['trcd']);
			$rec_id = "SR".date("ymdhis");
			$arr = array(
				"trcd"    => $rec_id,
				"trtype"  => "SR",
				"effect"  => "+",
				"effect_acc"  => "-",
				"custtype"=> "M",
				"novac"   => $hasil[0]->novac,
				"dfno"    => $hasil[0]->dfno,
				"docno"   => "REFUND K-NET TRX",
				"refno"   => $data['trcd'],
				"amount"  => $hasil[0]->amount,
				"createnm"=> $hasil[0]->createnm,
				"tipe_dk" => "K",
				"tipe_dk_acc" => "D"
			);
			//print_r($arr);

			$recover = $this->kwallet->recoverSaldoKwalletFromXLTrx($arr);
			if($recover['response'] == "true") {
				$json = jsonTrueResponse(null, "Transaksi K-NET $data[trcd] berhasil di dibatalkan..");
			}

		} catch (Exception $e) {
			$message = $e->getMessage();
			$json = jsonFalseResponse($message);
		}
		echo json_encode($json);
	}

	// $route['k-wallet/recon/trx/only'] = 'backend/k_wallet_recon/reconTrxOnly';
	public function reconTrxOnly() {
		$data = $this->input->post(NULL, TRUE);
		$data['result'] = $this->kwallet->kwalletReconTrxOnly($data['orderid']);
		$response = jsonFalseResponse("Reconcile berhasil...");
		if ($data['result'] != NULL) {
			$reponse = jsonTrueResponse(NULL, "Gagal reconcile...");
		}
		echo json_encode($response);
	}
}
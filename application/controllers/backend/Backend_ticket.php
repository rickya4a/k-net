<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_ticket extends MY_Controller {
	
	
	public function __construct() {
	    parent::__construct();
		 $this->folderView = "backend/ticket/";
		$this->load->service('backend/be_ticket_service', 'be_ticket');
	}
	
	//$route['be/ticket/list'] = 'backend/backend_ticket/getListTicket';
	public function getListTicket() {
		if($this->username != null) {
            $x['form_header'] = 'Report Listing Ticket Booking';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be/ticket/payment';
			$x['location'] = $this->be_ticket->getListTicketLocation();
            $this->setTemplate($this->folderView.'getListTicket', $x);
        } else {
           redirect('backend', 'refresh');
        }
	}
	
	
	//$route['be/ticket/listResult'] = 'backend/backend_ticket/getListTicketResult';
	public function getListTicketResult() {
		$data['result'] = $this->be_ticket->getListTicket();
		$this->load->view($this->folderView.'getListTicketResult', $data);
	}
	
	
	//$route['be/ticket/payment'] = 'backend/backend_ticket/getListPaymentTicket';
	public function getListPaymentTicket() {
		if($this->username != null) {
            $x['form_header'] = 'Approve Payment Ticket';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be/ticket/payment';
            $this->setTemplate($this->folderView.'getListPaymentTicket', $x);
        } else {
           redirect('backend', 'refresh');
        }
	}
	
	//$route['be/ticket/payment/list'] = 'backend/backend_ticket/getListPaymentTicketResult';
	public function getListPaymentTicketResult() {
		$data['result'] = $this->be_ticket->getListPendingPaymentTicket();
		$this->load->view($this->folderView.'getListPaymentTicketResult', $data);
	}

	//$route['be/ticket/payment/approve/(:any)'] = 'backend/backend_ticket/approvePaymentTicket/$1';
	public function approvePaymentTicket($bookingno) {
		$res = jsonFalseResponse("Update Payment failed..");	
		$data = $this->be_ticket->approvePaymentTicket($bookingno);
		if($data > 0) {
			$res = jsonTrueResponse(null, "Update payment $bookingno success..");
		}
		echo json_encode($res);
	}
	
	//$route['be/ticket/payment/report'] = 'backend/backend_ticket/getReportPayment';
    public function getReportPayment() {
    	if($this->username != null) {
            $x['form_header'] = 'Payment Ticket Report';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be/ticket/payment/report';
            $this->setTemplate($this->folderView.'getReportPayment', $x);
        } else {
           redirect('backend', 'refresh');
        }
    }
    
    //$route['be/ticket/payment/report/list'] = 'backend/backend_ticket/getReportPaymentResult';
    public function getReportPaymentResult() {
    	$data = $this->input->post(NULL, TRUE);
		if($data['flag_paid'] == '0') {
			$data['result'] = $this->be_ticket->getListPendingPaymentTicket();
			$this->load->view($this->folderView.'getListPaymentTicketResult2', $data);
		} else {
			$data['result'] = $this->be_ticket->getListApprovePaymentTicket($data);
			$this->load->view($this->folderView.'getListApprovePaymentTicket', $data);
		}
        //echo "test";
    }
	
	public function getDetailEvent($trans_hdr_id) {
		$data['result'] = $this->be_ticket->getDetailEvent($trans_hdr_id);
		//print_r($data['result']);
		$this->load->view($this->folderView.'getDetailEvent', $data);
	}
	
	//$route['be/ticket/location'] = 'backend/backend_ticket/getTicketLocationReport';
	public function getTicketLocationReport() {
		if($this->username != null) {
            $x['form_header'] = 'Ticket Location Report';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be/ticket/location';
			$x['location'] = $this->be_ticket->getListEvent();
            $this->setTemplate($this->folderView.'getTicketLocationReport', $x);
        } else {
           redirect('backend', 'refresh');
        }
	}
	
	//$route['be/ticket/location/list'] = 'backend/backend_ticket/getTicketLocationReportResult';
	public function getTicketLocationReportResult() {
		$data = $this->input->post(NULL, TRUE);
		$data['eventid'] 	= $data['event_id'];
		$data['paystatus']	= $data['pay_status'];
		$data['result'] = $this->be_ticket->getListParticipantByLocation();
		$this->load->view($this->folderView.'getTicketLocationReportResult', $data);
	}
	
	public function getTicketLocationReportResultXls() {
		//echo "masuk kesini";
		
        $data = $this->input->post(NULL, TRUE);
		$data['result'] = $this->be_ticket->getListParticipantByLocation();
		$this->load->view($this->folderView.'getTicketLocationReportResultXls', $data);
    }
	
	
	//$route['be/ticket/sms'] = 'backend/backend_ticket/getTicketSMS';
	public function getTicketSMS() {
		if($this->username != null) {
            $x['form_header'] = 'Resend SMS Ticket';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be/ticket/sms';
			$this->setTemplate($this->folderView.'getTicketSMS', $x);
        } else {
           redirect('backend', 'refresh');
        }
	}
	
	//$route['be/ticket/sms/list'] = 'backend/backend_ticket/getTicketSMSList';
	public function getTicketSMSList() {
		$data['result'] = $this->be_ticket->getTicketSMSList();
	    $this->load->view($this->folderView.'getTicketSMSList', $data);
	}
	
	//$route['be/ticket/sms/send/(:any)'] = 'backend/backend_ticket/sendTicketSMS/$1';
	public function resendTicketSMS($ticketno) {
		$res = jsonFalseResponse("Send SMS failed..");	
		$data = $this->be_ticket->sendTicketSMS($ticketno);
		if($data > 0) {
			$res = jsonTrueResponse(null, "Send SMS with ticket $ticketno success..");
		}
		echo json_encode($res);
	}
}
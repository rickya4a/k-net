<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Member_reconcile extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = "backend/member/";
		$this->load->model("backend/be_member_model", "member");
	}

	//$route['trx/reconcile/null_member'] = 'backend/member_reconcile/reconcileMemberForm';
	public function reconcileMemberForm() {
		$data['form_header'] = "Member Update";
		$data['icon'] = "icon-pencil";
		$data['form_reload'] = 'trx/reconcile/null_member';
		try {
			$this->checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}

        //$data['idstk'] = $this->mumroh->getListStokistM();
        //$data['listBank'] = $this->wMember->getListAffiliateBank();
		$this->setTemplate($this->folderView.'memberUpdateFormReconcile', $data);
	}

	//$route['trx/reconcile/token/check'] = 'backend/member_reconcile/checkNoToken';
	public function checkNoToken() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("No Transaksi token tidak ditemukan..");

		$res = $this->member->checkNoToken($data['notoken']);
		if($res != null) {
			$hasil = jsonTrueResponse($res, "Data Ok");
		} else {
			$res2 = $this->member->checkNoToken_sgo($data['notoken']);
			$hasil = jsonTrueResponse($res2, "Data Ok");
		}

		echo json_encode($hasil);
	}

	//$route['trx/reconcile/token/check2'] = 'backend/member_reconcile/checkTokenMembReg';

	public function checkTokenMembReg() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("No Transaksi token tidak ditemukan..");

		$res = $this->member->checkTokenNew($data['notoken']);
		echo json_encode($res);
	}
	/*
	public function checkTokenMembReg() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("No Transaksi token tidak ditemukan..");

		$res2 = $this->member->checkEcommMembOkSgo("trx_no", $data['notoken']);
		//$hasil = jsonTrueResponse($res2, "Data Ok");

		if($res2 != null) {
		    $tampilHasil = $this->member->checkNoToken($data['notoken']);
			if($tampilHasil == null) {
				$tampilHasil = $this->member->checkNoToken_sgo($data['notoken']);
			}


			if($res2[0]->idno != null && $res2[0]->idno != "" && $res2[0]->idno != " ") {
				$cekmsmemb = $this->member->checkFromMsmemb("a.idno", $res2[0]->idno);
				if($cekmsmemb != null) {
					$idmemb = $cekmsmemb[0]->dfno;
					$nmmemb = $cekmsmemb[0]->fullnm;
					$jointdt = $cekmsmemb[0]->jointdt;

					$stat_reg = "";
					if($cekmsmemb[0]->stat_reg == "0") {
						$stat_reg = "REG FROM K-NET, TRX NO : ".$cekmsmemb[0]->trx_no;
					} else if($cekmsmemb[0]->stat_reg == "1") {
						$stat_reg = "REG FROM K-NET USING VOUCHER : ".$cekmsmemb[0]->voucher_no;
					} else if($cekmsmemb[0]->stat_reg == "2") {
						$stat_reg = "REG NOT FROM K-NET : ";
					}


					$hasil = array("response" => "false",
					  "arrayData" => $tampilHasil,
					  "message" => "No KTP sudah terdaftar an $idmemb / $nmmemb $stat_reg");

				} else {
					$hasil = jsonTrueResponse($tampilHasil, "Data Ok");
				}
			} else {
				$hasil = jsonTrueResponse($tampilHasil, "Data Ok");
			}
		}
		echo json_encode($hasil);
	}*/

	//$route['trx/reconcile/nonbangkok/token/check'] = 'backend/member_reconcile/checkNoTokenRegBiasa';
	/*public function checkNoTokenRegBiasa() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("No Transaksi token tidak ditemukan..");
		$res = $this->member->checkNoTokenBiasa($data['notoken']);
		if($res != null) {
			$hasil = jsonTrueResponse($res, "Data Ok");
		}
		echo json_encode($hasil);
	}*/

	//$route['trx/reconcile/null_member/save'] = 'backend/member_reconcile/reconcileMemberSave';
	public function reconcileMemberSave() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("Reconcile Member gagal..");
		//$res = $this->member->reconcileDataEcommMembSGO_mampirkak($data);

		$check = $this->member->checkFromMsmemb("a.idno", $data['idno']	);
		if($check != null) {
		    $paramx = $check[0]->dfno." / ".$check[0]->fullnm;
		    echo "<script>alert('Data membership sudah ada di MSMEMB dgn $paramx')</script>";

			$check2 = $this->member->checkEcommMembOk("memberid", $check[0]->dfno);
		    echo "<pre>";
			print_r($check2);
			echo "</pre>";
		} else {
		    if($data['insert_type'] == "insert") {
				$res = $this->member->reconcileDataEcommMembSGO_EM($data);
			} else if($data['insert_type'] == "update"){
				$res = $this->member->updateDataEcommMembSGO_EM($data);
			} else {
			   $res = array(
			    "keterangan" => "Data tidak bisa di update/insert"
			   );
			}
			echo "<pre>";
			print_r($res);
			echo "</pre>";
		}

		/*if($data['insert_type'] == "insert") {
			$res = $this->member->reconcileDataEcommMembSGO_EM($data);
		} else {
			$res = $this->member->updateDataEcommMembSGO_EM($data);
		}*/
		//echo json_encode($hasil);
		echo "<pre>";
		print_r($res);
		echo "</pre>";
	}

	//$route['trx/reconcile/helper/(:any)/(:any)/(:any)'] = 'backend/member_reconcile/getFullNameMemberStockist/$1/$2/$3';
	public function getFullNameMemberStockist($tbl, $param, $value) {
		$hasil = jsonFalseResponse("Data tidak ditemukan..");

		$nilai = str_replace(' ', '', strtoupper($value));
		//$nilai = strtoupper($stripped);
		$res = $this->member->getInfoMemberStokis($tbl, $param, $nilai);
		if($res != null) {
			$hasil = jsonTrueResponse($res);
		}
		echo json_encode($hasil);
	}

	//$route['member/reconcile/bangkok'] = 'backend/member_reconcile/formReconBangkok';
    public function formReconBangkok() {
		if ($this->username != null) {
			$x['form_header'] = 'Member Reconcile';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'member/reconcile/bangkok';
			$this->setTemplate($this->folderView . 'formReconBangkok', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}


	/**
	 * @Author: Ricky
	 * @Date: 2019-01-17 18:04:55
	 * @Desc: Reconciling Member Bangkok
	 * start of addition
	 */
	// $route['member/reconcile/bangkok/update/(:any)'] = 'backend/member_reconcile/memberReconcileBangkok/$1';
	public function memberReconcileBangkok($id) {
		/* $data['form_header'] = "Member Update";
        $data['icon'] = "icon-pencil";
		$data['form_reload'] = 'member/reconcile/bangkok/update/'; */
		// $data['memdetails'] = $this->member->get_headmem($id);
		$data['token'] = $id;
		try {
			$this->checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}

        //$data['idstk'] = $this->mumroh->getListStokistM();
		//$data['listBank'] = $this->wMember->getListAffiliateBank();
		$this->load->view($this->folderView.'memberReconBangkokForm', $data);
	}

	//$route['member/reconcile/bangkok/list'] = 'backend/member_reconcile/listReconBangkok';
	public function listReconBangkok() {
		$y = $this->input->post(null, true);
        $x['from'] = $y['from'];
        $x['to'] = $y['to'];
		$x['listReconciledBangkok'] = $this->member->getListNonReconciledMember($x['from'], $y['to']);
		/* echo "<pre>";
		print_r($x['listReconciledBangkok']);
		echo "</pre>"; */
		$this->load->view($this->folderView . 'listReconBangkok', $x);
	}

	// $route['member/reconcile/bangkok/list/detail/(:any)'] = 'backend/member_reconcile/detailListReconBangkok/$1';
	function detailListReconBangkok($id) {
		if ($this->username != null) {
			$x['memdetails'] = $this->member->get_headmem($id);
			$this->load->view($this->folderView . 'detailsMember', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	// $route['member/reconcile/bangkok/list/xls'] = 'backend/member_reconcile/printResultToExcel';
	function printResultToExcel() {
		if ($this->username != null) {
			$y = $this->input->post(null, true);
			$x['from'] = $y['from'];
			$x['to'] = $y['to'];
			$x['member'] = $this->member->getListNonReconciledMember($x['from'], $y['to']);
			/* echo "<pre>";
			print_r ($x['member']);
			echo "</pre>"; */
			$this->load->view($this->folderView . 'printMemberListReconBangkok', $x);
		} else {
			redirect('backend', 'refresh');
		}
   }
	//    end of addition

	function randomChars($length){

		$karakter= 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789';
		$string = '';
		for ($i = 0; $i < $length; $i++)
		{
			 $pos = rand(0, strlen($karakter)-1);
			 $string .= $karakter{$pos};
		}
		return $string;
	 }

	// $route['member/transaction_only/recon'] = 'backend/member_reconcile/reconTrxMemberOnly';
	function reconTrxMemberOnly() {
		$order_id = $this->input->post('token');
		$newid = $this->input->post('memberid');
		$ktp = $this->input->post('noktp');
		$no_order = $this->input->post('orderno');

		$this->load->service("webshop/Knet_payment_service",'paymentService');
		$double = $this->paymentService->checkDoubleTokenTransHdr($order_id);
		if($double == null) {
			$checkBayar = $this->cekPembayaran($order_id);
			if($checkBayar['response'] == "true") {
				$ins = $this->insertTrxOnly($newid, $order_id);
				if($ins['response'] == "true") {
					$upd = $this->updateEcommMembOk($newid, $no_order, $ins['orderno']);
					if($upd['response'] == "true") {
						$res = jsonTrueResponse(null, $upd['message']);	
					} else {
						$res = jsonFalseResponse($upd['message']);	
					}
				} else {
					$res = jsonFalseResponse($upd['message']);	
				}
			} else {
                $res = $checkBayar;
			}

		} else {
			$res = jsonFalseResponse("Transaksi dengan token : $order_id sudah ada");
		}
		echo json_encode($res);
	}

	function updateEcommMembOk($memberid, $notrx, $notrxbaru) {
		$qryx_db = $this->load->database('db_ecommerce', true);
		$qry = "SELECT a.trx_no, a.memberid, a.membername, a.idno
				FROM ecomm_memb_ok a
				WHERE a.memberid = '$memberid' and a.trx_no = '$notrx'";
		$query = $qryx_db->query($qry);
		if($query->num_rows() > 0) {
			$upd = "UPDATE ecomm_memb_ok SET trx_no = '$notrxbaru'
					WHERE trx_no = '$notrx' AND memberid = '$memberid'";
			//echo $upd;
			$hasil = $qryx_db->query($upd);
			if($hasil > 0) {
				$res = jsonTrueResponse(null, "Data di ecomm_memb_ok sudah di update..");
			} else {
				$res = jsonFalseResponse("Data di ecomm_memb_ok gagal di update..");
			}
			return $res;
		} else {
			$res = jsonFalseResponse("Data di ecomm_memb_ok untuk $notrx / $memberid  tidak ada..");
			return $res;
		}
	}

	function cekPembayaran($token) {
		$qryx_db = $this->load->database('db_ecommerce', true);
		$qry = "SELECT a.trx_no, b.Order_ID, c.trcd
				FROM ecomm_memb_ok_sgo a
				LEFT OUTER JOIN REPORT_DOWNLOAD_SGO b ON (a.trx_no COLLATE SQL_Latin1_General_CP1_CS_AS = b.Order_ID)
				LEFT OUTER JOIN va_cust_pay_det c ON (a.trx_no COLLATE SQL_Latin1_General_CP1_CS_AS = c.trcd)
				WHERE a.trx_no = '$token'";
		$query = $qryx_db->query($qry);
		if($query->num_rows() > 0) {
			$hasil = $query->result();
			if($hasil[0]->Order_ID != null) {
			   $res = jsonTrueResponse(null, "Bayar pake SGO");
			   return $res;
			}

			if($hasil[0]->trcd != null) {
				$res = jsonTrueResponse(null, "Bayar pake K-Wallet");
				return $res;
			}
			$res = jsonFalseResponse("Pembayaran tidak ada di SGO dan K-Wallet");
			return $res;
		} else {
			$res = jsonFalseResponse("Data di ecomm_memb_ok_sgo untuk $token  tidak ada..");
			return $res;
		}
	}

	function insertTrxOnly($newid, $order_id) {
		$is_landingpage = 0;

			$qryx_db = $this->load->database('db_ecommerce', true);
			$datetrans = date("Y-m-d H:i:s");

			$this->load->model("webshop/member_model", "member_model");
			$cek_seQ = $this->member_model->cek_seQMemb();
			$orderno = $this->member_model->get_idnoMemb();

			//$cek_seQ = "TESSEQ";
			//$orderno = "E-TES";

			$secno = $this->randomChars(5);

			$qry = "SELECT a.idstk, a.nmstkk, a.total_pay, a.total_bv, a.payShip, a.payAdm, a.payConnectivity,
						a.bank_code_payment,a.sentTo, b.membername, a.pricecode,
						a.bonusmonth, c.charge_connectivity,
						b.sponsorid, b.idno, b.addr1, b.addr2,
						b.addr3,b.tel_hp,b.email,b.stk_code,b.sex,b.birthdt,
						b.acc_no,b.acc_name,b.bankid,b.joindt,b.trx_no,b.kdpos,
						b.[state],b.ip_address,b.[password],b.recruiterid,
						b.flag_voucher, b.voucher_no, b.voucher_key,
						a.userlogin, d.prdcd, d.prdnm, d.qty, d.dpr, d.bvr, b.is_landingpage, b.id_landingpage, a.free_shipping, a.discount_shipping
					FROM ecomm_trans_hdr_sgo a
					INNER JOIN ecomm_memb_ok_sgo b ON (a.orderno = b.trx_no)
					INNER JOIN ecomm_bank c ON (a.bank_code_payment = c.id)
					INNER JOIN ecomm_trans_det_prd_sgo d ON (a.orderno = d.orderno)
					WHERE a.orderno = '$order_id'";
			$query = $qryx_db->query($qry);
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					if($is_landingpage == "1") {
						$usrlogina = $newid;
						//$id_lp = $id_lp;
					} else {
						$usrlogina = $row->userlogin;
					}

					$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno)
								values('".$newid."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$usrlogina."','".$orderno."')";
					//echo "checkout <br>".$insCheckout."<br><br>";
					$queryCheckout = $qryx_db->query($insCheckout);

					$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;

					$addr1 = "";
					$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin)
								values('".$newid."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
				    //echo "LogTrans <br>".$insLogTrans."<br><br>";
					$queryLogTrans = $qryx_db->query($insLogTrans);

					//UPDATED by DION @ 09/08/2016
					//from this
					/*$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus)
								values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
					//echo "Paydet <br>".$insPaydet."<br><br>";
					$queryPaydet = $this->db->query($insPaydet);
					*/
					//to this
					$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
								SELECT '".$orderno."', seqno,paytype,docno, payamt, 'trx ok','capture', bank_code_payment, charge_admin
								FROM 	db_ecommerce.dbo.ecomm_trans_paydet_sgo a
								WHERE a.orderno='".$order_id."'";
								//"values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
					//echo "Paydet <br>".$insPaydet."<br><br>";
					$queryPaydet = $qryx_db->query($insPaydet);

					//end updated

					if($row->sentTo == "1") {
						$sentTo = "1";
						$is_ship = "0";
					}  else {
						$sentTo = "2";
						$is_ship = "1";
					}
					//di set 1 saat live
					$st_vt_pay = "1";



					$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr
									(orderno,bankaccno,token,id_memb,nmmember,total_pay,
									total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
									secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,
									payShip, payAdm, is_ship,
									bank_code_payment, payConnectivity, free_shipping, discount_shipping)
								values('".$orderno."','0','".$order_id."','".$newid."','".strtoupper($row->membername)."',$row->total_pay,
									$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
									'".$secno."','W','$sentTo','$st_vt_pay','".$datetrans."',
									".$row->payShip.",".$row->payAdm.", '".$is_ship."',
									".$row->bank_code_payment.", ".$row->payConnectivity.", '".$row->free_shipping."', ".$row->discount_shipping.")";
					//echo "header<br>".$insHeader."<br><br>";
					$queryHeader = $qryx_db->query($insHeader);

					$prdcd = $row->prdcd;
					$qty = (int) $row->qty;
					$bv = (int) $row->bvr;
					$prdnm = $row->prdnm;
					$price = (int) $row->dpr;

					//if($queryHeader > 0) {
						$inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
									values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$row->pricecode."','".$row->sentTo."')";
						//echo "produk <br>".$inputProd."<br><br>";
						//echo $inputProd;
						$queryProd = $qryx_db->query($inputProd);

						$pay_tipe = "0";
						$voucherno = "";
						$voucherkey = "";
						$kdpos1 =  "000004";

						//Jika daftar dari landing page, userlogin adalah ID Member itu sndr
						if($is_landingpage == "1") {
							$usrloginx = $newid;
						} else {
							//$usrloginx = $row->recruiterid;
							$usrloginx = $row->userlogin;
						}
				}

				$res = array("response" => "true", "orderno" => $orderno, "message" => "recon sukses");
			} else {
				$res = jsonFalseResponse("Hasil query kosong..");
			}

			return $res;
	}
}
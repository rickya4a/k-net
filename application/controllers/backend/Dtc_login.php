<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dtc_login extends MY_Controller {
		
	public function __construct() {
	    parent::__construct();
	//	$this->load->service('backend/be_login_service');
	}
	
	public function index($err = null)  {
	    $data['error_message'] = $err;		
		$data['usr'] = "Admin";
		$data['formAction'] = base_url('dtc/login');
		//$this->load->view('includes/header', $data);
//		$this->load->view('backend/dtc/vdtc_login', $data);
		//$this->load->view('includes/footer', $data);
//		$this->load->view('backend/dtc/vdtc_entry');
		$this->load->view('backend/dtc/vdtc_entry');
	}
	
	//$route['dtc/login'] = 'backend/dtc_login/handleLogin';
	public function handleLogin() {
		try {
		    if($this->form_validation->run('admLogin') == FALSE) {
			   $this->index("Username dan password harus diisi..!!");
		    }
			
			$srvReturn = $this->be_login_service->handleLoginService();
			//print_r($srvReturn);
			$this->session->set_userdata('ecom_user', $srvReturn['ecom_user']);
			//$this->session->set_userdata('ecom_menu', $srvReturn['ecom_menu']);
			$this->session->set_userdata('ecom_groupid', $srvReturn['ecom_groupid']);
			$this->session->set_userdata('ecom_groupname', $srvReturn['ecom_groupname']);
			redirect('backend/main');
		} catch(Exception $e) {
			$this->index($e->getMessage());
		}	
	}
}
?>
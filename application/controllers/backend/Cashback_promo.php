<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Cashback_promo extends MY_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model("backend/m_cashback_hydro", "hydro");
    $this->folderView = "backend/hydropromo/";
  }

  //$route['hydro/tessms'] = 'backend/cashback_promo/tesSendSms';
  public function tesSendSms() {
    $this->hydro->tesSendSms();
  }

  //$route['cashback/hydrowater/report'] = 'backend/cashback_promo/hydrowaterReport';
  public function hydrowaterReport() {
    if($this->username != null) {
            $x['form_header'] = "Cashback Hydro Water Report";
            $x['icon'] = "icon-pencil";
        $x['form_reload'] = 'cashback/hydrowater/report';
            //$x['stk'] = $this->mPromo->getIdstk();

            $this->setTemplate($this->folderView.'hydroFormReport', $x);
        }else{
            redirect('backend', 'refresh');
        }
  }

  //$route['cashback/hydrowater/report/list'] = 'backend/cashback_promo/hydrowaterReportList';
  public function hydrowaterReportList() {
    if($this->username != null) {
      $data = $this->input->post(NULL, TRUE);
      if($data['rep_type'] == "batch") {
        $data['res'] = $this->hydro->checkTransferByMemberID($data['idmember']);
        /*echo "<pre>";
        print_r($data['res']);
        echo "</pre>";*/
        $this->load->view($this->folderView.'hydroFormReportResult', $data);
      } else {
        $data['res'] = $this->hydro->rekapListCashbackByMember($data['idmember']);
        /*echo "<pre>";
        print_r($data['res']);
        echo "</pre>";*/
        $this->load->view($this->folderView.'hydroFormReportResultParji', $data);
      }
    } else {
      echo setErrorMessage("Silahkan login kembali...");
    }
  }

  //$route['cashback/hydrowater/pending/(:any)'] = 'backend/cashback_promo/pendingTrf/$1';
  public function pendingTrf($idmmember) {

    $data['res'] = $this->hydro->pendingTrf($idmmember);
    $this->load->view($this->folderView.'hydroFormPendingByDfno', $data);
  }

  //$route['cashback/hydrowater/trfprocess/(:any)'] = 'backend/cashback_promo/listTrfByBatch/$1';
  public function listTrfByBatch($batch) {

    $data['listTrf'] = $this->hydro->listTransferByBatchID($batch);
    $this->load->view($this->folderView.'listTrfByBatch', $data);
  }

  //$route['cashback/hydrowater'] = 'backend/cashback_promo/hydrowater';
  public function hydrowater() {
    if($this->username != null) {
            $x['form_header'] = "Cashback 500rb Hydro Water";
            $x['icon'] = "icon-pencil";
        $x['form_reload'] = 'cashback/hydrowater';
            //$x['stk'] = $this->mPromo->getIdstk();

            $this->setTemplate($this->folderView.'hydroForm', $x);
        }else{
            redirect('backend', 'refresh');
        }
  }

  //$route['cashback/update/adminbank'] = 'backend/cashback_promo/updateAdminBank';
  public function updateAdminBank() {
    //$res = jsonFalseResponse("Admin bank gagal di update..");
    $upd = $this->hydro->updateAdminBankKosong();
    if($upd > 0) {
      //$res = jsonTrueResponse(null, "Admin bank yang kosong berhasil di update..");
      echo setSuccessMessage("Admin bank berhasil di update..");
    } else {
      echo setErrorMessage("Admin bank gagal di update / tidak ada admin bank yang kosong..");
    }
    //echo json_encode($res);
  }

  //$route['cashback/hydrowater/list'] = 'backend/cashback_promo/getListCashbackHydroWater';
  public function getListCashbackHydroWater() {
    $data = $this->input->post(NULL, TRUE);
    if($data['tipe'] == "blm_trf") {
       //$data['result'] = $this->hydro->listCashbackBelumDitransfer();
       $data['result'] = $this->hydro->listCashbackBelumDitransferV2();
       //print_r($data['result']);
       $this->load->view($this->folderView.'hydroBlmTransfer', $data);
    } else if($data['tipe'] == "tdk_lengkap") {
      $data['result'] = $this->hydro->listCashbackTanpaNoRekAtauHP();
      $this->load->view($this->folderView.'hydroTanpaNorekHP', $data);
    } else if($data['tipe'] == "tolak") {
       //$data['listTrf'] = $this->hydro->listCashbackRejected($data['from'], $data['to']);
       $data['listTrf'] = $this->hydro->listCashbackRejectedV2($data['from'], $data['to']);
       $this->load->view($this->folderView.'hydroListTolakan', $data);
    } else if($data['tipe'] == "sdh_trf") {
       $data['result'] = $this->hydro->listBatchTransferOK($data['from'], $data['to'], "3");
       $this->load->view($this->folderView.'hydroListBatchTrfOK', $data);
    } else if($data['tipe'] == "batch") {
      $data['result'] = $this->hydro->listBatchTransfer($data['from'], $data['to'], "1");
      $this->load->view($this->folderView.'hydroListBatch', $data);
    }
  }

  //$route['cashback/hydrowater/batch/process'] = 'backend/cashback_promo/previewBatchCashbackHydroWater';
  public function previewBatchCashbackHydroWater() {
    $data = $this->input->post(NULL, TRUE);

    $listTrx = set_list_array_to_string($data['trcd']);
    $data['listTrx'] = $listTrx;
    $data['rekap'] = $this->hydro->rekapCashbackByIdmember($listTrx);
    $this->load->view($this->folderView.'hydroPreviewBatch', $data);
  }

  //$route['cashback/hydrowater/batch/save'] = 'backend/cashback_promo/saveBatchCashbackHydroWater';
  public function saveBatchCashbackHydroWater() {
    $data = $this->input->post(NULL, TRUE);
    $save = $this->hydro->saveBatchCashbackHydroWater($data);
    echo json_encode($save);
  }

  //$route['cashback/hydrowater/batch/cancel'] = 'backend/cashback_promo/cancelBatchHydroWater';
  public function cancelBatchHydroWater() {
    $data = $this->input->post(NULL, TRUE);
    $cancel = $this->hydro->cancelBatchHydroWater($data['no_batch']);
    echo json_encode($cancel);
  }

  //$route['cashback/hydrowater/batch/id'] = 'backend/cashback_promo/listTransferByBatchNo';
  public function listTransferByBatchNo() {
    $data = $this->input->post(NULL, TRUE);
    $data['listTrf'] = $this->hydro->listTransferByBatchNo($data['no_batch']);

    $this->load->view($this->folderView.'hydroListTrfByBatch', $data);
  }

  //$route['cashback/hydrowater/batch/list'] = 'backend/cashback_promo/listAllBatch';
  public function listAllBatch() {

  }

  //$route['cashback/hydrowater/batch/print'] = 'backend/cashback_promo/printBatch';
  public function printBatch() {
    $data = $this->input->post(NULL, TRUE);
    $listID = set_list_array_to_string($data['batchno']);
    $data['res'] = $this->hydro->listTransferByBatchNo2($listID);
    $this->load->view($this->folderView.'hydroListTrfByBatchExcel', $data);

  }

  //$route['cashback/hydrowater/batch/reject_prev'] = 'backend/cashback_promo/prevRejectTransfer';
  public function prevRejectTransfer() {
    $data = $this->input->post(NULL, TRUE);
    //print_r($data);
    $listID = set_list_array_to_string($data['trcd']);
    $data['listTrf'] = $this->hydro->rejectTransferPrev($listID, $data['batchno']);
    $this->load->view($this->folderView.'hydroTolakanPrev', $data);
  }

  //$route['cashback/hydrowater/batch/reject'] = 'backend/cashback_promo/rejectTransfer';
  public function rejectTransfer() {
      $data = $this->input->post(NULL, TRUE);
    //print_r($data);
    $listID = set_list_array_to_string($data['trcd']);
    //$update = $this->hydro->rejectTransfer($listID, $data['batchno']);
    $update = $this->hydro->rejectTransfer2($data);
    echo json_encode($update);
  }

  //$route['cashback/hydrowater/batch/ok'] = 'backend/cashback_promo/TransferOk';
  public function TransferOk() {
      $data = $this->input->post(NULL, TRUE);
    //print_r($data);
    $listID = set_list_array_to_string($data['batchno']);
    $update = $this->hydro->transferSelesai($listID, $data['transferdt']);
    echo json_encode($update);
  }

  //$route['cashback/hydrowater/sendsms'] = 'backend/cashback_promo/sendSms';
  public function sendSms() {
    $update = $this->hydro->sendSms();
    if($update['response'] == "true") {
      echo setSuccessMessage($update['message']);
      echo "<table width=\"100%\" class='table table-striped table-bordered'>";
      echo "<tr>";
      echo "<td width=\"5%\">No</td>";
      echo "<td width=\"15%\">ID Member</td>";
      echo "<td>Nama Member</td>";
      echo "<td width=\"10%\">No HP</td>";
      echo "<td width=\"10%\">SMS</td>";
      echo "<td width=\"20%\">Response API</td>";
      echo "</tr>";
      $x = 1;
        foreach($update['arrayData']['list'] as $dta) {
        echo "<tr>";
        echo "<td>$x</td>";
        echo "<td>".$dta['dfno']."</td>";
        echo "<td>".$dta['fullnm']."</td>";
        echo "<td>".$dta['no_telp']."</td>";
        echo "<td>".$dta['status_sms']."</td>";
        echo "<td>".$dta['resSMS']."</td>";
        echo "</tr>";
        $x++;
      }
      echo "</table>";
    } else {
      echo setErrorMessage($update['message']);
    }
  }

  //$route['cashback/hydrowater/norekhp/excel'] = 'backend/cashback_promo/norekhpExcel';
  public function norekhpExcel() {
    $data = $this->input->post(NULL, TRUE);
    //print_r($data);
    $listID = set_list_array_to_string($data['trcd']);
    $data['res'] = $this->hydro->getReportByTrcd($listID);
    //print_r($data['res']);
    $this->load->view($this->folderView.'norekhpExcel', $data);
  }

  //$route['be/promo/cashback/sendsmsCashback'] = 'backend/cashback_promo/sendsmsCashback';
  public function sendsmsCashback() {
    $data = $this->input->post(NULL, TRUE);

    $kirim = $this->hydro->kirimSms($data['idmember'], $data['trcd']);
  }
}
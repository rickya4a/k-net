<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tom_manual extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->folderView = 'backend/ticket/';

	}

	public function formInput() {
		if ($this->username != NULL) {
			$data['form_header'] = "Input TOM Manual";
			$data['icon'] = "icon-list";
			$data['form_reload'] = 'etiket/tom/manual';
			$this->setTemplate($this->folderView. 'formTomManual', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function checkingData($db, $memberid) {
		$dbqryx = $this->load->database($db, TRUE);
		$dbqryx->select('order_no, valid_dfno');
		$dbqryx->from('db_ecommerce.dbo.trx_etiket');
		$dbqryx->where('status', '0');
		$dbqryx->where('id_etiket', '203');
		$dbqryx->where('valid_dfno', $memberid);
		$query = $dbqryx->get();
		$res = $query->result();
		return $res;
	}

	public function saveTomManual() {
		$data = $this->input->post(NULL, TRUE);
		$dbqryx = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('order_no, valid_dfno');
		$dbqryx->from('db_ecommerce.dbo.trx_etiket');
		$dbqryx->where('status', '0');
		$dbqryx->where('id_etiket', '203');
		$dbqryx->where('valid_dfno', $data['memberid']);
		$query = $dbqryx->get();
		$res = $query->result();
		if ($res != NULL) {
			echo setSuccessMessage("Data sudah ada");
		} else {
			$user = $this->username;
			$arr = array(
					"urut" => '1',
					"order_no"  => "INP-MANUAL-".$user,
					"dfno"  => $user,
					"fullnm"  => $user,
					"notiket"=> "TIKET1",
					"id_etiket"=> "203",
					"flag_online" => "F",
					"price" => 320000,
					"valid_dfno" => $data['memberid'],
					"valid_fullnm" => $data['membername'],
					"createdt" => "2017-11-23",
					"createnm" => "ADMIN",
					"status" => "0",
					"email" => $data['email'],
					"notelp" => $data['tel_hp'],
					"statusHitung" => "0",
					"is_early" => "1",
			);
			$dbqryx->insert('trx_etiket',$arr);
			echo setSuccessMessage("Data berhasil ditambahkan...");
		}
	}

	public function listTomManual() {
		$data = $this->input->post(NULL, TRUE);

		$dbqryx = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('id, order_no, valid_dfno, valid_fullnm, fullnm');
		$dbqryx->from('db_ecommerce.dbo.trx_etiket');
		$dbqryx->like('order_no', 'INP', 'after');
		$query = $dbqryx->get();
		$res['data'] = $query->result();
		$this->load->view($this->folderView. 'listTomManual', $res);
	}
}

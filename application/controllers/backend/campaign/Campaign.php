<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Campaign extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/campaign/";
		$this->load->service('backend/campaign/be_campaign_service');
		//$this->load->model('backend/be_product_model');
	}
	

	/*--------------------
	 * CAMPAIGN
	 *-------------------*/
	//$route['product'] = 'backend/campaign/campaign/getInputProduct';
	public function getInputCampaignPhoto() {		
        if($this->username != null) {
           $data['form_header'] = "Input User Group";
           $data['form_action'] = base_url('user/group');
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'campaign';
		   $data['error'] = array('error' => ' ');
		   //$data['listCampaign'] = $this->be_campaign_service->listAllProductCat();
		   //$data['listPrd'] = $this->be_product_service->listAllProductService(false);
		   
		   //$this->load->view($this->folderView.'Campaign_upload', array('error' => ' ' ));
		   
           $this->setTemplate($this->folderView.'getInputCampaign', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['campaign/save'] = 'backend/campaign/campaign/saveInputCampaign';
	public function saveInputCampaign() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());

		//$srvReturn = array("response"=> "true");
    	try {
    		$dt = $this->input->post(null,true);
    		if($this->form_validation->run('campaign') == TRUE) {
    			$srvReturn = $this->be_campaign_service->inputCampaignService();
				if($srvReturn['response'] == "true" && $_FILES["userfile"]['error'] == 0) {
	                $img_name = $this->uploadCampaignImage(); 
					$data = array('upload_data' => $img_name );
					//var_dump($data);
					//var_dump($img_name);
					//$this->load->view('backend/campaign/Campaign_upload_success', $data);   
	            }	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	public function getDetCampaignByID($param, $type = "array"){
        $srvReturn = jsonFalseResponse();
    	try {
    		$data['detCampaignByID'] = $this->be_campaign_service->listCampaignServiceByID($param);		
    	} catch(Exception $e) {}
		//echo json_encode($srvReturn);
		if($type == "array") {
			$this->load->view($this->folderView.'CampaignVote', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['detCampaignByID']));
		}
	}
	
		//$route['product/id/(:any)'] = 'backend/product/getListProductByID/$1';
	public function getListCampaignByID($id) {
		
	}
	//$route['product/list/(:any)/(:any)'] = 'backend/product/getListProduct/$1/$2';
	public function getListCampaign($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_campaign_service->listCampaignService($param, $value);		
    	} catch(Exception $e) {}
		//echo json_encode($srvReturn);
		if($type == "array") {
			$this->load->view($this->folderView.'listingCampaign_res', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listCampaign']));
		}
	}
	
	
	
	//$route['product/list'] = 'backend/product/getListAllProduct';
	//$route['product/list/json'] = 'backend/product/getListAllProduct/$1';
	public function getListAllCampaign($type = "array") {
		$data['listCampaign'] = null;
    	try {
    		$data['listCampaign'] = $this->be_campaign_service->listAllCampaignService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'listingCampaign_res2', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listCampaign']));
		}	
	}
	

	
	
	//$route['campaign/save'] = 'backend/campaign/campaign/saveUpdateProduct';
	public function saveUpdateProductxx() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product') == TRUE) {
    			$srvReturn = $this->be_product_service->inputProductService();
				if($srvReturn['response'] == "true" && $_FILES["myfile"]['error'] == 0) {
	                $img_name = $this->uploadProductImage();                 
	            }	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	private function uploadCampaignImage() {
        $output_dir = "assets/images/campaign/campaign_img/original/";
        $thumb_dir = "assets/images/campaign/campaign_img/thumbnail/";
        if(isset($_FILES["userfile"]))
        {
            $ret = array();
        	
			//echo "cek nama file";
            $error =$_FILES["userfile"]["error"];
            
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData() 
            if(!is_array($_FILES["userfile"]["name"])) //single file
            {
                $fileName = $_FILES["userfile"]["name"];
                $imageTitle = "";  
                $dirfile = $output_dir.$fileName;
                move_uploaded_file($_FILES["userfile"]["tmp_name"],$dirfile);
                $this->createThumbnails($dirfile, $thumb_dir, $fileName);
                //$this->m_admproduct->saveImagesProduct($idpict, $fileName, $imageTitle);
                $ret[]= $fileName;
            }
            
            return $ret;
         }   
   }
	
	//$route['product/update'] = 'backend/product/saveUpdateProduct';
	public function saveUpdateProduct() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product') == TRUE) {
    			$srvReturn = $this->be_product_service->updateProductService();	
				if($srvReturn['response'] == "true" && isset($_FILES["myfile"]) && $_FILES["myfile"]['name'] != "") {
	                $img_name = $this->uploadProductImage();                 
	            }	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	//$route['product/delete/(:any)'] = 'backend/product/deleteProduct/$1';
	public function deleteProduct($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_product_service->deleteProductService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
}	
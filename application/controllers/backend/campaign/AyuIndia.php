<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class AyuIndia extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/campaign/";
        $this->load->model("backend/campaign/be_ayuindia_model", 'mAyu');
	}
	

	//========================hilal====================
	function getlistUploadToWeb(){
        if($this->username != null) {
            $x['form_header'] = 'Form Transfer Data Ayu-India To Website';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'campaign/listUploadAyu';
            $x['getAyuPeriod'] = $this->mAyu->getPeriodAyuIndia();
            $this->setTemplate($this->folderView.'AyuIndiaCampaign', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportAyuToWebAct($param){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
			$x['period'] = $dt['period'];
			$x['param'] = $param;
        	$x['detListAyuIndia'] = $this->mAyu->getListAyuIndia();
			//if($param == 1){
				$this->load->view($this->folderView.'AyuIndiaList_Res', $x);
			/*}elseif($param == 2){
				echo "masuk sini";
				$this->load->view($this->folderView.'reportUplUmrToWebXls_res', $x);
			}*/
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportAyuToXlsWeb(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['detListUmroh'] = $this->fUmroh->getListUmrToWebCSV($dt);
            $this->load->view($this->folderView.'reportUplUmrToWebXls_res', $x);
        }
        else{
            
        }
    }
	//=======================end hilal=================
	
	
}	
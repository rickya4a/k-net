<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promoctapr extends MY_Controller{

    public function __construct() {

        parent::__construct();
        $this->folderView = "backend/promo/";
        $this->load->model('backend/promo/List_promo_model', 'm_Promo');
    }

    //$route['be/promo/promoct'] = 'backend/promo/promoctapr/formPromo';
    public function formPromo() {

        if($this->username != null) {
            $data['form_header'] = "Search Data Promo";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'be/promo/promoct';
            $this->setTemplate($this->folderView.'cariPromo', $data);
        }

        else {

            redirect('backend', 'refresh');
        }
    }

    //$route['be/promo/promoct/list'] = 'backend/promo/promoctapr/tampil_member';
    public function tampil_member() {

        if ($this->username != null) {
            $x = $this->input->post(null, true);
            $x['promo'] = $this->m_Promo->get_prom($x['iddfno']);
            //$x['detpromo'] = $this->m_Promo->get_detailprom($x['iddfno']);
            $this->load->view($this->folderView . 'tampilPromo', $x);
        }

        else {

            redirect('backend', 'refresh');
        }
    }

    //$route['be/promo/promoct/detail'] = 'backend/promo/promoctapr/tampil_detail';
    public function tampil_detail() {

        $a = $this->input->post('iddfno');
        $b = $this->input->post('bnsperiod');
        $z['detail'] = $this->m_Promo->get_detailprom($a, $b);

        $this->load->view($this->folderView . 'tampildetailpromoayuartis', $z);
    }

}
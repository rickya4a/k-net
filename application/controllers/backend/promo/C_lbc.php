<?php

class C_lbc extends MY_Controller{

    public function __construct(){

        parent::__construct();
        $this->folderView = "backend/promo/lbc/";
        $this->load->model('backend/promo/m_lbc', 'm_lbc');
    }
	
	//$route['lbc/sales/check'] = 'backend/promo/c_lbc/salesCheckForm';
	public function salesCheckForm() {
		
	 	if($this->username != null) {
	 		$data['form_header'] = "LBC Product Sales Check";
            $data['icon'] = "icon-list";
		    $data['form_reload'] = 'lbc/sales/check';
            $this->setTemplate($this->folderView.'lbcSalesCheckForm', $data); 
		} else {
			echo sessionExpireMessage(false);
		}
		//echo $arr;
	}
	
	//$route['lbc/sales/check/list'] = 'backend/promo/c_lbc/getListLbcSales';
	public function getListLbcSales() {
		$data = $this->input->post(NULL, TRUE);
		$data['mdReturn'] = $this->m_lbc->checkSalesLbc($data['memberid'], $data['bnsp']);
		//print_r($data['mdReturn']);
		if($data['mdReturn'] == null) {
			echo setErrorMessage("Tidak ada pembelanjaan produk LBC");
		} else {
			$this->load->view($this->folderView.'lbcSalesList', $data);
		}
		
		/*echo "<pre>";
		print_r($res);
		echo "</pre>";*/
	}
}
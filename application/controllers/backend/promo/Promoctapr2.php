<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promoctapr2 extends MY_Controller{

    public function __construct() {

        parent::__construct();
        $this->folderView = "backend/promo/";
        $this->load->model('backend/promo/List_promo_model2', 'm_Promo');

    }


    //$route['be/promo/promoct'] = 'backend/promo/promoctapr/formPromo';
    public function formPromo2() {

        if($this->username != null) {
            $data['form_header'] = "Search Data Promo";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'be/promo/promoctv2';
            $this->setTemplate($this->folderView.'cariPromo2', $data);
        }

        else {

            redirect('backend', 'refresh');
        }
    }



    //$route['be/promo/promoct/list'] = 'backend/promo/promoctapr/tampil_member';
    public function tampil_member2() {

        if ($this->username != null) {
            $x = $this->input->post(null, true);
            $x['promo'] = $this->m_Promo->get_prom($x['iddfno']);
            $x['detpromo'] = $this->m_Promo->get_detailprom2($x['iddfno']);
            $this->load->view($this->folderView . 'tampilPromo2', $x);
        }

        else {

            redirect('backend', 'refresh');
        }
    }
/*
        public function tampil_detailprom(){



            if ($this->username != null) {
                $x = $this->input->post(null, true);
                $x['detpromo'] = $this->m_Promo->get_detailprom($x['iddfno']);
                $this->load->view($this->folderView . 'tampilDetailPromo', $x);
            }

            else {

                redirect('backend', 'refresh');
            }
        }
*/
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Ayuartispromo extends MY_Controller{

		public function __construct() {
	    
	    	parent::__construct();
			$this->folderView = "backend/promo/";
			$this->load->model('backend/promo/List_member_model', 'm_Member');

		}	

		
		//$route['be/promo/ayuartis'] = 'backend/promo/ayuartispromo/formCariMember';
		public function formCariMember() {		
        
        	if($this->username != null) {
        		$data['form_header'] = "Search Data Member";
           		$data['icon'] = "icon-list";
		   		$data['form_reload'] = 'be/promo/ayuartis';
           		$this->setTemplate($this->folderView.'cariMember', $data);
           	}

        	else {
           	
           		redirect('backend', 'refresh');
        	}
		}
	

		
		//$route['be/promo/ayuartis/list'] = 'backend/promo/ayuartispromo/tampil_member';
		public function tampil_member() {

			if ($this->username != null) {
				$x = $this->input->post(null, true);
				$x['member'] = $this->m_Member->get_memb($x['idrekrut'], $x['from'], $x['to']);
				$x['memberdt'] = $this->m_Member->get_membname($x['idrekrut']);
				$this->load->view($this->folderView . 'tampilMember', $x);
			}

			else {
			
				redirect('backend', 'refresh');
			}
		}
	}
<?php
    if(!DEFINED('BASEPATH')) exit('NO DIRECT ACCESS ALLOWED');

    class Cariinfoship extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = "backend/promo/";
            $this->load->model('backend/promo/List_shipping_model', 'm_Member');
        }

        //$route['trans/shipping/report'] = 'backend/promo/Cariinfoship/formShipping';
        public function formShipping(){

            if($this->username != null) {
                $data['form_header'] = "Search Data Shipping";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'trans/shipping/report';
                $this->setTemplate($this->folderView.'cariShipping', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        //$route['trans/shipping/report/list'] = 'backend/promo/Cariinfoship/formShippingList';
        public function formShippingList(){

            if ($this->username != null) {
                $x = $this->input->post(null, true);
                $x['data'] = $this->m_Member->get_ship($x['no_trans']);
                $this->load->view($this->folderView . 'tampilShipping', $x);
            }

            else {

                redirect('backend', 'refresh');
            }
        }
    }
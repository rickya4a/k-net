<?php

class BonusAyuArtis extends MY_Controller{

    public function __construct(){

        parent::__construct();
        $this->folderView = "backend/promo/";
        $this->load->model('backend/promo/Bonus_AyuArtis_model', 'm_Bonus');
    }

//$route['bonusayuartis'] = 'backend/promo/BonusAyuArtis/getBonusAyuArtis';
    public function getBonusAyuArtis(){

        if($this->username != null) {
            $data['form_header'] = "AyuArtis Bonus Report";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'bonusayuartis';
            $this->setTemplate($this->folderView.'formInputBonus', $data);
        }

        else {

            redirect('backend', 'refresh');
        }
    }

//$route['bonusayuartis/tampil'] = 'backend/promo/BonusAyuArtis/formBonusAyuArtis';
    public function formBonusAyuArtis(){

        if ($this->username != null) {
            $x = $this->input->post(null, true);
            $x['bonus'] = $this->m_Bonus->get_bonus($x['idmember'], $x['from'], $x['to']);
            $this->load->view($this->folderView . 'tampilBonus', $x);
        }

        else {

            redirect('backend', 'refresh');
        }
    }

//$route['bonusayuartis/tampil/detail'] = 'backend/promo/BonusAyuArtis/formDetailBonusAyuArtis';
    public function formDetailBonusAyuArtis(){

            $x = $this->input->post('dfno');
            //$y = $this->input->post('date1');
            //$z = $this->input->post('date2');
            $y = $this->input->post('date1');
            $z = $this->input->post('date2');
            $a['detail'] = $this->m_Bonus->get_detail($x, $y, $z);

        $this->load->view($this->folderView . 'tampilDetail', $a);
    }

}
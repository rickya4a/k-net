<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Importpromo extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = "backend/promo/";
            //$this->load->service('backend/promo/Be_importpromo_service', 's_promo');
            //$this->load->model('backend/promo/Be_importpromo_model', 'm_importpromo');
            $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        }


        //$route['be/promo/importayu'] = 'backend/promo/importpromo/formInputPromo';
        public function formInputPromo(){

            if ($this->username != null) {
                $this->load->library('upload');
                $data['form_header'] = "Import Promo Ayuartis RP 20000";
                $data['icon'] = "icon-pencil";
                $data['form_reload'] = 'be/promo/importayu';
                $this->setTemplate($this->folderView . 'importMember', $data);
            } else {
                redirect('backend', 'refresh');
            }
        }

        /*
        //$route['be/promo/importayu/action1'] = 'backend/promo/importpromo/promoImportForm';
        public function promoImportForm()
        {

            $data = array();

            if (isset($_POST['preview'])) {
                $upload = $this->m_importpromo->upload_file($this->filename);

                if ($upload['result'] == "success") {
                    include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

                    $excelreader = new PHPExcel_Reader_Excel2007();
                    $loadexcel = $excelreader->load('assets/excel/' . $this->filename . '.xlsx');
                    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

                    $data['sheet'] = $sheet;
                } else {
                    $data['upload_error'] = $upload['error'];
                }
            }

            $this->setTemplate($this->folderView . 'tes', $data);
        }
        */

        //$route['be/promo/importayu/action2'] = 'backend/promo/importpromo/promoImportAction';
        public function promoImportAction(){

                $fileName = $this->input->post('file', TRUE);

                $config['upload_path'] = './upload/';
                $config['file_name'] = $fileName;
                $config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
                $config['max_size'] = 10000;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                    //$this->session->set_flashdata('msg', 'Ada kesalah dalam upload');
                    //redirect('backend', 'refresh');
                    //echo "<script>alert($error);history.go(-1);</script>";
                    print_r($error);

                } else {
                    $media = $this->upload->data();
                    $inputFileName = 'upload/' . $media['file_name'];

                    try {
                        $inputFileType = IOFactory::identify($inputFileName);
                        $objReader = IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }

                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();

                    for ($row = 1; $row <= $highestRow; $row++) {
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL,
                            TRUE,
                            FALSE);
                        $data = array(
                            "dfno" => $rowData[0][0],
                            "fullnm" => $rowData[0][1],
                            "sfno_reg" => $rowData[0][2],
                            "sfno" => $rowData[0][3],
                            "joindt" => $rowData[0][4],
                            "bonusrec" => $rowData[0][5],
                            "status_transfer" => $rowData[0][6]
                        );
                        //$this->m_importpromo->upload_file("msmemb_recruiter_bonus", $data);
                        $this->db2->insert('msmemb_recruiter_bonus', $data);
                        //print_r($data);
                        unlink($inputFileName);
                    }

                    //$this->session->set_flashdata('msg', 'Berhasil upload ...!!');
                    redirect('backend', 'refresh');
                }

        }
    }
<?php

 class ReportStatement extends MY_Controller{

     public function __construct(){

         parent::__construct();
         $this->folderView = "backend/promo/";
         $this->load->model("webshop/Sales_model",'salesM');
         $this->load->model('M_mobile_api', 'm_mobile_api');

     }

//$route['statementreport'] = 'backend/promo/ReportStatement/getReportStatement';
     public function getReportStatement(){

         if($this->username != null) {
             $data['form_header'] = "Statement Bonus Report";
             $data['icon'] = "icon-list";
             $data['form_reload'] = 'statementreport';
             $this->setTemplate($this->folderView.'formInputReport2', $data);
         }

         else {

             redirect('backend', 'refresh');
         }
     }

//$route['promo/printreportstatement'] = 'backend/promo/ReportStatement/postBonus';
     public function postBonus(){

         $idmember = $this->input->post('idnya');
         $month = $this->input->post('month');
         $year = $this->input->post('year');

             $dt['month'] = $this->input->post('month');
             $dt['year'] = $this->input->post('year');
             $dt['idmember'] = $this->input->post('idmember');

         //echo 'membernya adalah '.$idmember;

         $dt['res1'] = $this->salesM->showDataMember($idmember, $month, $year);
         $dt['tableBaru'] = $this->salesM->showTableHilal($idmember, $month, $year);


         if ($dt['res1'] == 0) {
             echo "Data Belum Ada";
         }
         else {
             if ($this->salesM->showBonusMember($idmember, $month, $year) == 0) {
                 echo "Data statement bonus " . $idmember . " tidak ada";
             } else {
                 //$dt['res2'] = $this->salesM->m_showBonusSummary();
                 // ---tambahan dr ana---
                 $dt['currency1'] = $this->salesM->getCurrency();
                 $dt['currency'] = $dt['currency1'][0]->exchangerate;
                 //print_r($dt['currency']);
                 $dt['hasil1'] = $this->salesM->cekmonthly($idmember, $month, $year);
                 $dt['hasil2'] = $this->salesM->downline($idmember, $month, $year);
                 $dt['hasil3'] = $this->salesM->selfBonus($idmember, $month, $year);
                 $cc = $this->salesM->showBonusMember($idmember, $month, $year);


                 if (isset($cc)) {
                     foreach ($cc as $dd) {
                         if ($dd->netincome > 0) {
                             $dt['res2'] = $this->salesM->showBonusMember($idmember, $month, $year);
                             $dt['initiateSUm'] = $this->salesM->showSumInitiative($idmember, $month, $year);
                         }
                         if ($dd->gpincome > 0) {
                             $dt['devresult'] = $this->salesM->developmentBonus($idmember, $month, $year);
                         }
                         if ($dd->ldbincome > 0) {
                             $dt['ldbresult'] = $this->salesM->leadershipBonus($idmember, $month, $year);
                         }

                         $unilevel = $dd->planbincome + $dd->addinfinityincome;

                         if ($dt['year'] < 2017) {
                             if ($dd->infinityincome > 0) {
                                 $dt['hasil4'] = $this->salesM->infinityBonus($idmember, $month, $year);
                             }
                         } else {
                             $dt['hasil4'] = $this->salesM->initiativeBonus($idmember, $month, $year);
                         }

                         if ($unilevel > 0) {
                             $dt['hasil5'] = $this->salesM->unilevelBonus($idmember, $month, $year);
                             $dt['hasil6'] = $this->salesM->levelMember($idmember, $month, $year);
                         }
                     }
                 }
                 if ($this->salesM->showBonusVoucher($idmember, $month, $year) != 0) {
                     $dt['voucher'] = $this->salesM->showBonusVoucher($idmember, $month, $year);
                 }else{
                     $dt['voucher'] = null;
                 }
             }
         }

         $this->load->model('backend/report/List_Report_BV');
         $dt['cekTTBV'] = $this->List_Report_BV->cekTTBV($idmember);

         $dt['result'] = $this->m_mobile_api->listVoucherMember($idmember, $month, $year);

             if($dt['result'] != 0){
                 $i = 0;
                 foreach ($dt['result'] as $dta) {
                     $no = substr($dta->VoucherNo, 0, 3);
                     if ($no == "XPV" || $no == "ZVO" || $no == "XPP" || $no = "XHD") {
                         $dtax = $this->m_mobile_api->getDetailPrdVchNovApr($dta->VoucherNo);
                         $dt['result'][$i]->product = $dtax;
                     }
                     $i++;
                 }
             }
         
         $tgl = "01";
         $gabung = $tgl . '/' . $month . '/' . $year;
         $date = date('m/d/Y', strtotime($gabung));
         $dt['pjk'] = $this->salesM->getDetailPajak($idmember, $date);


         //===========================PERHITUNGAN KOMISI PPOB=========================================

         $gabung2 = $year . '/' . $month . '/' . $tgl;
         $date2 = date('Y/m/d', strtotime($gabung2));

             $dt['ppob'] = $this->salesM->getKomisiPPOB($date2, $idmember);

         if( $dt['ppob'] != null){
             foreach ($dt['ppob'] as $key => $value) {
                 $r_fullnm[] = $value->fullnm;
                 $r_dfno[] = $value->dfno;
                 $r_alvl[] = $value->alvl;
                 $r_tbv[] = $value->tbv;
                 $r_cbori[] = $value->CashBackORI;
                 $r_cbppob[] = $value->CashBackPPOB;
             }

             // $dt['r_ppob']=$ppob;
             $dt['fullnm']=$r_fullnm;
             $dt['alvl']=$r_alvl;
             $dt['tbv']=$r_tbv;
             $dt['cbppob']=$r_cbppob;
             $dt['cbori']=$r_cbori;
         }


         //====================TAMPIL KUPON HEBOH BANGET============================

         //$dt['kuponhdr'] = $this->salesM->getListKuponHdr($date2, $idmember);
         //$dt['kupon'] = $this->salesM->getListKupon($idmember);

        //print_r($r_fullnm);
        //print_r($r_alvl);
        //print_r($r_cbppob);

        /* foreach ($r_alvl as $key => $value) {
             print_r($r_cbppob[$r_alvl[1]]);
         }*/

         //print_r($r_cbppob[$r_alvl[0]]);
         $this->load->view('backend/promo/printBonusReportPdf2', $dt);
         //echo print_r($dt);
     }

//$route['statreport/list'] = 'backend/promo/ReportStatement/getReport';
    function getReport(){

            $idmember = $this->input->post('idmember');
            $year = $this->input->post('year');

            //$dt['report'] = 1;
            $dt['header'] = $this->salesM->headerReportBackend($idmember,$year);
            $dt['report'] = $this->salesM->getReportBonus2($idmember,$year);

        //print_r($dt['report']);
        $this->load->view('backend/promo/getListingStatement',$dt);
    }
 }

?>
<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Kgb extends MY_Controller {
	public function __construct() {
		parent::__construct();
		//$this->load->model("backend/be_api_model", "m_api");	
	}
	
	//$route['kgb/kota/list'] = 'backend/kgb/listKota';
	public function listKota() {
		//$urlKota = "http://36.37.81.131:8080/Service.svc/KlinkKotaResponse";
		$urlKota = "http://kgponline.co.id:8080/Service.svc/KlinkKotaResponse";
		$curl = curl_init($urlKota);
        $data = array("usernama" => "relas",
                      "passw" => "123456");                                                                  
		$data_string = json_encode($data);                                                                                   
		                                                                                                                     
		$ch = curl_init($urlKota);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = json_decode(curl_exec($ch));
	    //print_r($result->KotaDaftar);
		
		header("Content-type: application/vnd.ms-excel; name='excel'");
	    header("Content-Disposition: Attachment; filename=listKota.xls");
		header("Pragma: no-cache");
	    header("Expires: 0");
	    date_default_timezone_set("Asia/Jakarta");
		echo 'KotaKode' . "\t" . 'KotaNama' . "\t" . 'ProvinsiNama' . "\t" . 'WaktuTempuh'."\n";
		foreach($result->KotaDaftar as $dta) {
			$kotaKode = (string) $dta->KotaKode;
			echo "'" . $kotaKode . "\t" . $dta->KotaNama . "\t" . $dta->ProvinsiNama .  "\t" . $dta->WaktuTempuh."\n";
		}
        
	}

}
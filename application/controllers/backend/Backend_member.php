<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Backend_member extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = "backend/member/";
		$this->load->model("backend/be_member_model", 'mMember');
		$this->load->model("backend/be_umroh_model", 'mumroh');
		$this->load->model("webshop/member_model", 'wMember');
	}

	//$route['be/member/recruit'] = 'backend/backend_member/formListRecruit';
	public function formListRecruit() {
		if ($this->username != null) {
			$x['form_header'] = 'List Recruit Member';
			$x['icon'] = 'icon-list';
			$x['form_reload'] = 'be/member/recruit';
			$this->setTemplate($this->folderView . 'memberRecruit', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}
	//$route['be/member/recruit/list'] = 'backend/backend_member/getListRecruit';
	public function getListRecruit() {
		$data = $this->input->post(NULL, TRUE);
		$x['listRecruit'] = $this->mMember->getListRecruit($data);
		$this->load->view($this->folderView . 'memberRecruitList', $x);
	}

	//$route['be/member/recruit/list/excell'] = 'backend/backend_member/exportRecruitExcell';
	public function exportRecruitExcell() {
		$data = $this->input->post(NULL, TRUE);
		$x['listRecruit'] = $this->mMember->getListRecruit($data);

		header("Content-type: application/vnd.ms-excel; name='excel'");
	    header("Content-Disposition: Attachment; filename=listRecruit.xls");
		header("Pragma: no-cache");
	    header("Expires: 0");
	    date_default_timezone_set("Asia/Jakarta");
		$this->load->view($this->folderView . 'memberRecruitList', $x);
	}

	//$route['be/member/search'] = 'backend/backend_member/formSearchMember';
	function formSearchMember() {
		if ($this->username == null) {
			return setErrorMessage("Session expire");
		}

		$x['form_header'] = 'Search Data Member';
		$x['icon'] = 'icon-list';
		$x['form_reload'] = 'be/member/search';
		$this->setTemplate($this->folderView . 'searchMember', $x);

	}

	function list_member() {
		if ($this->username != null) {
			$x = $this->input->post(null, true);
			if ($x['paramValue'] == "") {
				setErrorMessage("Please fill the parameter value..");
			} else {
				if($x['searchBy'] == "memberid") {
					$x['memdetails'] = $this->mMember->get_headmem($x['paramValue']);
					$x['rekening'] = $this->mMember->getDatRekening($x['paramValue']);
					$this->load->view($this->folderView . 'detailsMember', $x);
				} elseif ($x['searchBy'] == 'cell_no') {
					$x['memdetails'] = $this->mMember->get_headmemCellNo($x['paramValue']);
					$this->load->view($this->folderView. 'detailsMember', $x);
				} else {
					$x['member'] = $this->mMember->get_member($x['searchBy'], $x['paramValue']);
					$this->load->view($this->folderView . 'listMember', $x);
				}
			}
		} else {
			redirect('backend', 'refresh');
		}
	}

	function details_member($id) {
		if ($this->username != null) {
			$x['memdetails'] = $this->mMember->get_headmem($id);
			$this->load->view($this->folderView . 'detailsMember', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function rank_member() {
		if ($this->username != null) {
			$x['form_header'] = 'Rank Member';
			$x['icon'] = 'icon-ok';
			$x['form_reload'] = 'be/member/rank';
			$x['rank'] = $this->mMember->get_rank();
			$this->setTemplate($this->folderView . 'rankMember', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	public function get_id($dfno) {
		if ($this->username != null) {
			$x = $this->mMember->checkId(strtoupper($dfno));
			if ($x != null) {
				$arr = array("response" => "true", "arrayData" => $x);
				echo json_encode($arr);
			} else {
				$arr = array("response" => "false", "message" => "No data");
				echo json_encode($arr);
			}

		} else {
			redirect('backend', 'refresh');
		}
	}

	public function get_level($level) {
		if ($this->username != null) {
			$x = $this->mMember->checkRank($level);
			if ($x != null) {
				$arr = array("response" => "true", "arrayData" => $x);
				echo json_encode($arr);
			} else {
				$arr = array("response" => "false", "message" => "No data");
				echo json_encode($arr);
			}

		} else {
			redirect('backend', 'refresh');
		}
	}

	public function update_rank() {
		if ($this->username != null) {
			if ($this->input->post('dfno') == "") {
				echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
			} elseif ($this->input->post('newrank') == "0") {
				echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
			} elseif ($this->input->post('newperiod') == "0") {
				echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
			} elseif ($this->input->post('year2') == "0") {
				echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
			} else {
				$data = $this->input->post(null, true);
				$x = $this->mMember->get_update($data);
				echo json_encode($x);
				//$this->setTemplate($this->folderView.'rankMember', $x);
			}
		} else {
			redirect('backend', 'refresh');
		}
	}

	function list_rank() {
		if ($this->username != null) {
			$x['form_header'] = 'Rank Member';
			$x['icon'] = 'icon-ok';
			$x['form_reload'] = 'be/member/rank/list';
			$x['rank'] = $this->mMember->get_rank();
			$this->setTemplate($this->folderView . 'rankHead', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function search_rank() {
		if ($this->username != null) {
			$x = $this->input->post(null, true);
			$x['ranklist'] = $this->mMember->getRankSearch($x['searchBy'], $x['paramValue1'], $x['paramValue2'], $x['paramValue3'], $x['paramValue4']);
			$this->load->view($this->folderView . 'rankList', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function get_place() {
		if ($this->username != null) {
			$x['form_header'] = 'Input';
			$x['icon'] = 'icon-ok';
			$x['form_reload'] = 'be/member/place';
			$this->setTemplate($this->folderView . 'placeInfo', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function input_place() {
		if ($this->username != null) {
			$data = $this->input->post(null, true);
			$this->load->view($this->folderView . 'placeInput', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function save_place() {
		if ($this->username != null) {
			//if($this->input->post('dfno') == "") {
			//    echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
			//} else {
			$data = $this->input->post(null, true);
			$x = $this->mMember->inputPlace($data);
			echo json_encode($x);
			//}
		} else {
			redirect('backend', 'refresh');
		}
	}

	/*function reg_member(){
	 if($this->username != null) {
	 $x['form_header'] = 'Register Member';
	 $x['icon'] = 'icon-pencil';
	 $x['form_reload'] = 'be/member/register';
	 $this->setTemplate($this->folderView.'regMember', $x);
	 }
	 else
	 {
	 redirect('backend', 'refresh');
	 }
	 }

	 function ins_member(){
	 if($this->username != null) {
	 $x = $this->mMember->get_insert();
	 if($x == 1){
	 //echo '<script>alert("Data Berhasil Disimpan!");</script>';
	 //return true;
	 redirect('be/member/register', 'refresh');
	 }else{
	 echo '<script>alert("Gagal!");</script>';
	 }

	 }
	 else
	 {
	 redirect('backend', 'refresh');
	 }
	 }

	 function rep_member(){
	 if($this->username != null) {
	 $x['form_header'] = 'Report Member';
	 $x['icon'] = 'icon-ok';
	 $x['form_reload'] = 'be/member/report';
	 $x['repmember'] = $this->mMember->get_data();
	 $this->setTemplate($this->folderView.'repMember', $x);
	 }
	 else
	 {
	 redirect('backend', 'refresh');
	 }
	 }

	 function edit($id){
	 if($this->username != null) {
	 $data['form_header'] = 'Report Member';
	 $data['icon'] = 'icon-ok';
	 $data['form_reload'] = 'be/member/report';
	 $data['repmember'] = $this->mMember->getById($id);
	 //var_dump($data['repmember']);die();
	 $this->setTemplate($this->folderView.'editMember', $data);

	 }
	 else
	 {
	 redirect('backend', 'refresh');
	 }
	 }

	 function edit2(){
	 if($this->username != null)
	 {
	 //var_dump($this->input->post()); die();
	 ////echo "$registerno"; die();
	 if($this->input->post('submit'))
	 {
	 $id = $this->input->post('registerno');
	 //echo $id;
	 $update = $this->mMember->update($id);
	 if($update > 0){
	 redirect('be/member/report');
	 }else{
	 echo "gagal";
	 }

	 }

	 else
	 {
	 redirect('backend', 'refresh');
	 }
	 }
	 }

	 function delete($id){
	 if($this->username != null) {
	 $this->mMember->delete($id);
	 redirect('be/member/report');
	 }
	 else
	 {
	 redirect('backend', 'refresh');
	 }
	 }

	 public function checkKtp($ktpno) {
	 if($this->username != null) {
	 $x = $this->mMember->checkKtp(strtoupper($ktpno));
	 echo json_encode($x);
	 } else {
	 redirect('backend', 'refresh');
	 }
	 }

	 public function checkTlp($tel_hp) {
	 if($this->username != null) {
	 $x = $this->mMember->checkTlp($tel_hp);
	 echo json_encode($x);
	 } else {
	 redirect('backend', 'refresh');
	 }
	 }

	 public function excel_rep(){
	 $this->load->library('Excel_generator');
	 $this->load->database('db_test', true);
	 $query = $this->db->get('register');
	 $this->excel_generator->set_query($query);
	 $this->excel_generator->set_header(array('No. Register', 'Nama', 'No. KTP', 'Jenis Kelamin', 'No. Hp'));
	 $this->excel_generator->set_column(array('registerno', 'fullnm', 'ktpno', 'sex', 'tel_hp'));
	 $this->excel_generator->set_width(array(25, 20, 20, 18, 20));
	 $this->excel_generator->exportTo2007('Laporan Data Member');
	 $this->excel_generator->start_at(8);
	 }*/

	//$route['be/member/knet'] = 'backend/backend_member/formListRegMembKNET';
	public function formListRegMembKNET() {
		if ($this->username != null) {
			$x['form_header'] = 'K-NET Member Registration Listing';
			$x['icon'] = 'icon-list';
			$x['form_reload'] = 'be/member/knet';
			$this->setTemplate($this->folderView . 'formListRegMembKNET', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	//$route['be/member/knet/list'] = 'backend/backend_member/getListRegMembKNET';
	public function getListRegMembKNET() {
		if ($this->username != null) {
			$x = $this->input->post(null, true);
			$x['member'] = $this->mMember->getListRegMembKNET($x['from'], $x['to']);
			$this->load->view($this->folderView . 'getListRegMembKNET', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

	//$route['member/knet/print'] = 'backend/backend_member/printListRegMembKNET';
	public function printListRegMembKNET() {
		if ($this->username != null) {
			$x = $this->input->post(null, true);
			$x['member'] = $this->mMember->getListRegMembKNET($x['from'], $x['to']);
			$this->load->view($this->folderView . 'printListRegMembKNET', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

    function getUserlogin($userlogin){
            $sponsor = $this->mumroh->getDistributorInfo($userlogin);
            if($sponsor > 0)
            {
                $x = jsonTrueResponse($sponsor);
            }else{
                $x = jsonFalseResponse("ID Tidak Ditemukan");
            }
            echo json_encode($x) ;
    }

    public function checkTlp($tel_hp)
    {
    	 if($this->username != null) {
        	 $x = $this->mMember->checkTlp($tel_hp);
             if($x > 0){
                $xx = jsonTrueResponse($x);
             }else{
                $xx = jsonFalseResponse("ID Tidak Ditemukan");
             }
             echo json_encode($xx);
    	 } else {
    	   redirect('backend', 'refresh');
    	 }
	 }

    //$route['be/member/helper/checkSponsor/(:any)/(:any)'] = 'backend/backend_member/checkSponsor/$1/$2';
    function checkSponsor($idsponsor,$userlogin){
        $this->db = $this->load->database('klink_mlm2010', true);

        $stored_procedure = "exec klink_mlm2010.[dbo].SP_HILAL_WEB_CHECKUPLINE ?,?";
        //echo $stored_procedure;
        $query = $this->db->query($stored_procedure,array('dfno'=> $idsponsor,'login'=> $userlogin));
        $dta = $query->result();
        if($dta[0]->hasil ==  1)
        {
            $sponsor = $this->mumroh->getDistributorInfo($idsponsor);
            if($sponsor > 0)
            {
                $x = jsonTrueResponse($sponsor,"sukses");
            }else{
                $x = jsonFalseResponse("ID Tidak Ditemukan");
            }
        }else{
            $sponsor = $this->mumroh->getDistributorInfo($idsponsor);
            $x = jsonTrueResponse($sponsor, "Sponsor tidak satu jaringan dengan ".$userlogin."");
        }
        echo json_encode($x) ;
     }

     //$route['be_member/helper/checkExistIDMember/(:any)']='backend/backend_member/checkExistIDMember/$1';
     function checkExistIDMember($token){
        $checkExistIDmember = $this->mMember->getDtIDmember($token);
            /*if($checkExistIDmember > 0)
            {
                $x = jsonTrueResponse($checkExistIDmember,"sukses");
            }else{
                $x = jsonFalseResponse("ID Not Exist");
            }
            echo json_encode($x) ;*/
        echo json_encode($checkExistIDmember);
     }

     //$route['be_member/helper/dtByOrderID/(:any)'] = 'backend/backend_member/getDtByOrderID/$1';
    function getDtByOrderID($orderid){
   	    if ($this->username != null) {
			//$y = $this->mMember->checkValidOrderid(strtoupper($orderid));
            $x = $this->mMember->checkDtByOrderId(strtoupper($orderid));
			if ($x != null) {
				$arr = array("response" => "true", "arrayData" => $x);
				echo json_encode($arr);
			} else {
				$arr = array("response" => "false", "message" => "No data");
				echo json_encode($arr);
			}

		} else {
			redirect('backend', 'refresh');
		}
    }

    //$route['be_member/helper/ktp/(:any)']='backend/backend_member/checkKtp/$1';
    public function checkKtp($ktpno)
    {
	   if($this->username != null) {
	       $x = $this->mMember->checkKtp(strtoupper($ktpno));
           if($x > 0){
                echo json_encode($x);
           }else{
                $arr = array("response" => "false");
                echo json_encode($arr);
           }

	   } else {
	       redirect('backend', 'refresh');
	   }
     }

     //$route['be/member/id/(:any)'] = 'backend/backend_member/getDetailMemberByID/$1';
	public function getDetailMemberByID($id) {
		$res = jsonFalseResponse("No result found, try another ID..");
		$x = $this->mMember->getDetailMemberByID($id);
		if($x != null) {
			$res = jsonTrueResponse($x, "Result fetched..");
		}
		echo json_encode($res);
	}

	//$route['be/member/stk/id/(:any)'] = 'backend/backend_member/getDetailStockistByID/$1';
	public function getDetailStockistByID($id) {
		$res = jsonFalseResponse("No result found, try another ID..");
		$x = $this->mMember->getDetailStockistByID($id);
		if($x != null) {
			$res = jsonTrueResponse($x, "Result fetched..");
		}
		echo json_encode($res);
	}

	//$route['be/member/knet/reg'] = 'backend/backend_member/formMemberReg';
    /*MENU K-NET MEMBER REGISTRATION*/

	public function formMemberReg() {
		$data['form_header'] = "K-NET Member Registration";
        $data['icon'] = "icon-pencil";
		$data['form_reload'] = 'be/member/knet/reg';
        $data['idstk'] = $this->mumroh->getListStokistM();
        $data['listBank'] = $this->wMember->getListAffiliateBank();
		try {
			$this->checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'memberRegForm', $data);
	}

    //$route['be/member/insert'] = 'backend/backend_member/memberInsKnet';
    /*function memberInsKnetOriginal(){
        if ($this->username != null) {
            $dt = $this->input->post(null, true);

            if($dt['sponsorid']=="" || $dt['membername']=="" || $dt['ktpno']=="" || $dt['tel_hp']=="" || $dt['addr1']=="" || $dt['idstk']=="0" ){

                echo "Lengkapi Form";

            }else{
                $lastkit = $this->wMember->showLastkitno($dt['idstk']);
                if(!empty($lastkit)) {
                    if($lastkit[0]->lastkitno < 99999) {
                        $x['new_id'] = $this->wMember->createNewID($lastkit);
                        $x['new_id'] = 'IDJTBAA00545TEST';

                        $updlastkitno = $this->wMember->setLastKitNo($dt['idstk']);
                        if($updlastkitno > 0) {
                            $insMember = $this->mMember->insMembSgoOk($dt,$x['new_id']);
                            if( $insMember == 0) {
    		 	                $dec_lastkitno = $this->wMember->DecrementingLastKitNo($dt['idstk']);
                            }else{
                                $x['getDt'] = $this->mMember->getDetailMemberByID($x['new_id']);

                                $this->load->view($this->folderView.'memberReconRes',$x);
                            }
                        }else {

    						 $setLastKid = $this->member_model->setLastKitToZero($dt['idstk']);
                             $lastkit = $this->member_model->showLastkitno($dt['idstk']);
    						 $x['new_id'] = $this->member_model->createNewID($lastkit);
    						 $updlastkitno = $this->member_model->setLastKitNo($dt['idstk']);
                             if($updlastkitno > 0) {
                             	$insMember = $this->mMember->insMembSgoOk($dt,$x['new_id']);
                                if( $insMember == 0) {
        		 	                $dec_lastkitno = $this->wMember->DecrementingLastKitNo($dt['idstk']);
                                }else{
                                    $x['getDt'] = $this->mMember->getDetailMemberByID($x['new_id']);
                                    $this->load->view($this->folderView.'memberReconRes');
                                }
                             }
    					}
                    }
                }
            }


        }
        else{
            redirect('backend', 'refresh');
        }
    }*/

    //$route['be/member/insert'] = 'backend/backend_member/memberInsKnet';
    function memberInsKnet(){
        if ($this->username != null) {
            $dt = $this->input->post(null, true);

            if($dt['sponsorid']=="" || $dt['membername']=="" || $dt['ktpno']=="" || $dt['tel_hp']=="" || $dt['addr1']=="" || $dt['idstk']=="0" ){
                echo setErrorMessage('Lengkapi Form');
                //echo "Lengkapi Form";

            }else{
            	if($dt['tipe_upd'] == "0") {
	                $insMember = $this->mMember->insMembSgoOk($dt);
	               	if($insMember['membSgoOke'] == true) {
	    			 $result = setSuccessMessage("Insert Data ".$dt['tokenid']." Sukses,Silahkan ke menu Reconsile untuk generate IDMember..");
	        		} else {
	        		    $result = setErrorMessage("Update Data Failed..");
	        		}
				} else {
					$insMember = $this->mMember->updMembSgoOk($dt);
	               	if($insMember['membSgoOke'] == true) {
	    			 $result = setSuccessMessage("Insert Data ".$dt['tokenid']." Sukses,Silahkan ke menu Reconsile untuk generate IDMember..");
	        		} else {
	        		    $result = setErrorMessage("Update Data Failed..");
	        		}
				}
        		echo $result;
                /*if( $x['insMember'] == 0) {
		 	        echo "Insert Gagal";
                }else{
                    $x['getDt'] = $this->mMember->getDetailMemberByID($x['new_id']);

                    $this->load->view($this->folderView.'memberReconRes',$x);
                }*/

            }
        }
        else{
            redirect('backend', 'refresh');
        }
    }

    /*MENU UPDATE DATA MEMBER K-NET*/
	//$route['be/member/update'] = 'backend/backend_member/formMemberUpdate';
	public function formMemberUpdate() {
		$data['form_header'] = "Member Update";
        $data['icon'] = "icon-pencil";
		$data['form_reload'] = 'be/member/update';
		try {
			$this->checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}

        $data['idstk'] = $this->mumroh->getListStokistM();
        $data['listBank'] = $this->wMember->getListAffiliateBank();
		$this->setTemplate($this->folderView.'memberUpdateForm', $data);
	}


	//$route['be/member/update/save'] = 'backend/backend_member/saveMemberUpdate';
	public function saveMemberUpdate() {

		$data = $this->input->post(NULL, TRUE);
        if($data != null){
            $x = $this->mMember->saveMemberUpdate($data);
    		if($x['hdrTrans'] == true && $x['membOkk'] == true && $x['msmembb'] == true) {
    			$result = setSuccessMessage("Update Data ".$data['memberid']."/".$data['membername']." Success..");
    		} else {
    		    $result = setErrorMessage("Update Data Failed..");
    		}
    		echo $result;
        }
	}

	//$route['be/member/info'] = 'backend/backend_member/formMemberUpdateInfo';
	public function formMemberUpdateInfo() {
		$data['form_header'] = "Member Info Update";
        $data['icon'] = "icon-pencil";
		$data['form_reload'] = 'be/member/info';
		try {
			$this->checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}

		$this->setTemplate($this->folderView.'memberInfoUpdateForm', $data);
	}

	//$route['be/member/info/adm'] = 'backend/backend_member/formMemberUpdateInfoAdmin';
	public function formMemberUpdateInfoAdmin() {
		$data['form_header'] = "Update Data Member";
        $data['icon'] = "icon-pencil";
		$data['form_reload'] = 'be/member/info/adm';
		try {
			$this->checkSessionBE();
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}

		$this->setTemplate($this->folderView.'memberInfoUpdateFormAdm', $data);
	}

	//$route['be/member/info/id/(:any)'] = 'backend/backend_member/getMemberInfoByID/$1';
	public function getMemberInfoByID($id) {

		$res = $this->mMember->getMemberInfoByID($id);
		echo json_encode($res);
	}

	//$route['be/member/info/updateAdm'] = 'backend/backend_member/saveMemberInfoUpdateAdm';
	public function saveMemberInfoUpdateAdm() {
		$data = $this->input->post(NULL, TRUE);
		//print_r($data);
		try {
			$this->checkSessionBE();
			$hasil = $this->mMember->saveMemberData($data);
			if($hasil > 1) {
				$res = setSuccessMessage("Update Data $data[memberid]/$data[membername] success..");
			} else {
				$res = setErrorMessage("Update Data Failed..");
			}
		} catch(Exception $e) {
			$res = setErrorMessage($e->getMessage());
		}
		echo $res;
	}

	//$route['be/member/info/update'] = 'backend/backend_member/saveMemberInfoUpdate';
	public function saveMemberInfoUpdate() {

		$data = $this->input->post(NULL, TRUE);
		try {
			$this->checkSessionBE();
			$hasil = $this->mMember->saveMemberInfoUpdate($data);
			if($hasil > 1) {
				$res = setSuccessMessage("Update Data $data[memberid]/$data[membername] success..");
			} else {
				$res = setErrorMessage("Update Data Failed..");
			}
		} catch(Exception $e) {
			$res = setErrorMessage($e->getMessage());
		}
		echo $res;
	}

    //$route['be/memberLP'] = 'backend/stockist/backend_member/getListMemberLP';
    function getListMemberLP(){
        if ($this->username != null) {
            $x['form_header'] = 'Search List Member By Landing Page';
			$x['icon'] = 'icon-list';
			$x['form_reload'] = 'be/memberLP';
			$this->setTemplate($this->folderView . 'getListMemberLP', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }

    //$route['be_member/memberLP/act'] = 'backend/backend_member/listMemberLPRes';
	public function listMemberLPRes() {
		if ($this->username != null) {
			$x = $this->input->post(null, true);
			$x['memberLP'] = $this->mMember->getListMembLP($x['from'], $x['to']);
			$this->load->view($this->folderView . 'listMemberLP', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}

    //$route['be_member/memberLP/download'] = 'backend/backend_member/listMemberLpCSV';
    function listMemberLpCSV(){
        if ($this->username != null) {
			$y = $this->input->post(null, true);
            $x['from'] = $y['from'];
            $x['to'] = $y['to'];
			$x['memberLP'] = $this->mMember->getListMembLP($x['from'], $y['to']);
			$this->load->view($this->folderView . 'listMemberLPCSV', $x);
		} else {
			redirect('backend', 'refresh');
		}
    }

	/*---------------------------------------------------
	 * RECONCILE REGISTRASI MEMBER VIA VOUCHER YANG GAGAL
	 * --------------------------------------------------*/

	 //$route['be/member/voucher/reconcile'] = 'backend/backend_member/formReconcileMemberRegVoucher';
	 function formReconcileMemberRegVoucher() {
	 	if ($this->username != null) {
            $x['form_header'] = 'Reconcile Member via Voucher';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'be/member/voucher/reconcile';
			$this->setTemplate($this->folderView . 'reconcile_member_voucher/formReconcileMemberRegVoucher', $x);
        }else{
            redirect('backend', 'refresh');
        }
	 }
	 //$route['be/member/voucher/reconcile/list'] = 'backend/backend_member/getListReconcileMemberRegVoucher';
	 function getListReconcileMemberRegVoucher() {
	 	$y = $this->input->post(null, true);
        $x['from'] = $y['from'];
        $x['to'] = $y['to'];
		$x['listMember'] = $this->mMember->getListReconcileMemberRegVoucher($x['from'], $y['to']);
		//print_r($x['listMember']);
		$this->load->view($this->folderView . 'reconcile_member_voucher/getListReconcileMemberRegVoucher', $x);
	 }

	 //$route['be/member/voucher/reconcile/save'] = 'backend/backend_member/saveReconcileMemberRegVoucher';
	 function saveReconcileMemberRegVoucher() {
	 	$y = $this->input->post(null, true);
	 	$save = $this->mMember->saveReconcileMemberRegVoucher($y['idmember']);
		echo json_encode($save);
	 }

     /*---------------------------------
	  * UPDATE VOUCHER KEY SALAH CETAK
	  * ------------------------------*/
	  //$route['be/member/voucherkey/update'] = 'backend/backend_member/formUpdateVchKeyByVchNo';
	  function formUpdateVchKeyByVchNo() {
	  	 if($this->username != null) {
		   //$this->load->library('csvreader');
           $data['form_header'] = "Import & Update Voucher Key";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'be/member/voucherkey/update';
           $this->setTemplate($this->folderView.'voucher/formUpdateVchKeyByVchNo', $data);
        } else {
           redirect('backend', 'refresh');
        }
	  }

	  //$route['be/member/voucherkey/list'] = 'backend/backend_member/readFileVch';
	  function readFileVch() {
	  	$this->load->library('csvreader');
		$form = $this->input->post(NULL, TRUE);
        $fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != ""){
			if($form['pilih'] == "2") {
				$this->csvreader->set_separator(",");
		        $data['csvData'] = $this->csvreader->parse_file_no_header($fileName);
				$data['list'] = $this->mMember->showInfoVch($data['csvData']);
		        //print_r($data['csvData']);
		        $this->load->view($this->folderView.'voucher/vchListPreview', $data);
			} else {
				//$this->csvreader->set_separator(",");
		        $data['csvData'] = $this->csvreader->parse_file_no_header($fileName);
				$data['list'] = $this->mMember->showInfoVch($data['csvData']);
		        $this->load->view($this->folderView.'voucher/vchListPreview2', $data);
			}
		}else{
			echo "No Data Uploaded";
		}
	  }

	  //$route['be/member/vchno/(:any)'] = 'backend/backend_member/getDetailVch/$1';
	  public function getDetailVch($vchno) {
	  	$this->load->model("backend/be_warehouse_model", 'm_warehouse');
	  	$data['result'] = $this->m_warehouse->getDetailVoucher($vchno);
		$this->load->view('backend/warehouse/detailVoucher', $data);
	  }

	  // $route['be/member/voucherkey/update/save'] = 'backend/backend_member/saveUpdateVchKey';
	  function saveUpdateVchKey() {
	  	$this->load->library('csvreader');
		$form = $this->input->post(NULL, TRUE);
        $fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != ""){
			if($form['pilih'] == "2") {
				$this->csvreader->set_separator(",");
		        $data['csvData'] = $this->csvreader->parse_file_no_header($fileName);
				$data['update'] = $this->mMember->uploadNewVoucherKey($data['csvData']);

		        $this->load->view($this->folderView.'voucher/vchUploadResult', $data);
	        } else {
	        	echo setErrorMessage("Can not Import/Update Voucher Key..");
	        }
		}else{
			if($form['pilih'] == "3") {
	        	$res = $this->mMember->updateVchKeyByVchNo($form['vchno'], $form['vchkey']);
				if($res) {
					echo setSuccessMessage("Voucher $form[vchno] sudah diupdate voucher key nya menjadi $form[vchkey]");
				} else {
					echo setErrorMessage("Voucher Key gagal di update..");
				}
		    } else {
		    	echo "No Data Uploaded";
			}

		}
	  }

	  //$route['be/member/voucherkey/print'] = 'backend/backend_member/printResultToExcel';
	  function printResultToExcel() {
	  	 $this->load->library('csvreader');
		$form = $this->input->post(NULL, TRUE);
        $fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != ""){
			if($form['pilih'] == "2") {
				   $this->csvreader->set_separator(",");
		    }

			$data['csvData'] = $this->csvreader->parse_file($fileName);
			$data['list'] = $this->mMember->showInfoVch($data['csvData']);
		    //print_r($data['list']);
		    $this->load->view($this->folderView.'voucher/printExportExcel', $data);
		}else{
			echo "No Data Uploaded";
		}
	  }

	  //$route['be/memberinfo/(:any)/(:any)/(:any)'] = 'backend/backend_member/getInfoMemberStokis/$1/$2/$3';
	  function getInfoMemberStokis($table, $param, $value) {
			$arr = jsonFalseResponse("No Data");
		    $res = $this->mMember->getInfoMemberStokis($table, $param, $value);
			if($res != null) {
				$arr = jsonTrueResponse($res);
			}
			echo json_encode($arr);

	  }

		function releaseVoucher() {
			$data = $this->input->post(NULL, TRUE);
			$res = $this->mMember->releaseVoucher($data['voucher_no']);
			echo json_encode($res);
		}
}

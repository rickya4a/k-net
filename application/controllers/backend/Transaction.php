<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class Transaction extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->folderView = "backend/transaction/";
        $this->load->service("webshop/Cartshop_service", 'cartshopService');
        $this->load->service('backend/be_transaction_service', 's_trans');
        $this->load->model('backend/be_transaction_model', 'm_trans');
        $this->load->model('webshop/ecomm_trans_model', 'mm_trans');
        $this->load->model('webshop/shared_module', 'shared');
        $this->load->model("backend/be_hifi_model", "hifi");
    }

    /*------
    VA
    -------*/
    //$route['va/history/trx'] = 'backend/transaction/vaHistoryTrx';
    public function vaHistoryTrx()
    {
        if ($this->username != null) {
            $username = getUserID();
            $data['form_header'] = "VA History Trx";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'va/history/trx';

            $arr = array(
                "from" => date("Y-m-d"),
                "to" => date("Y-m-d"),
                "memberid" => getUserid(),
                "tipe_dk" => "",
                "trx_type" => "all"
            );
            $data['saldo'] = $this->hifi->getSaldoVa(getUserid());
            $data['res'] = $this->m_trans->getVaHistoryTrx($arr);
            $data['listTrtype'] = $this->hifi->getListVaTrxType();
            $data['prodCat'] = $this->shared->getListCatProd();

            $this->setTemplate($this->folderView.'va/vaHistoryTrx', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['va/history/trx/get'] = 'backend/transaction/vaHistoryTrxReportList';
    public function vaHistoryTrxReportList()
    {
        $data = $this->input->post(null, true);
        $arr = array(
              "from" => $data['from'],
              "to" => $data['to'],
              "memberid" => $data['memberid'],
              "tipe_dk" => "",
              "trx_type" => $data['trx_type']
            );
        $hasil['res'] = $this->m_trans->getVaHistoryTrx($arr);
        $hasil['tix'] = "1";
        $this->load->view($this->folderView.'va/vaHistoryTrxReportList', $hasil);
    }

    //$route['va/history/trx/getVar/(:any)/(:any)'] = 'backend/transaction/getMemberInfo/$1/$2';
    public function getMemberSaldo($param, $value)
    {
        $arr = $this->m_trans->getMemberSaldo($param, $value);
        $res = $arr != null ? jsonTrueResponse($arr, "OK") : jsonFalseResponse("Data member tidak ada di database");
        echo json_encode($res);
    }

    //$route['va/balance/rekap'] = 'backend/transaction/VaBalanceRekap';
    public function VaBalanceRekap()
    {
        if ($this->username != null) {
            //$username = getUserID();
            $data['form_header'] = "VA Balance Rekap";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'va/balance/rekap';

            /*$arr = array(
                "from" => date("Y-m-d"),
                "to" => date("Y-m-d"),
                "memberid" => getUserid(),
                "tipe_dk" => "",
                "trx_type" => "all"
            );*/
            $data['vaRekap'] = $this->m_trans->vaBalanceRekap();
            $data['listTrx'] = $this->m_trans->getListVaTrxType();
            //$this->setTemplate($this->folderView.'va/vaBalanceRekap', $data);
            $this->setTemplate($this->folderView.'va/vaRekapForm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['va/saldo/member'] = 'backend/transaction/rekapSaldoMember';
    public function rekapSaldoMember()
    {
        $data['vaRekap'] = $this->m_trans->vaBalanceRekap();
        $data['listTrx'] = $this->m_trans->getListVaTrxType();
        $this->load->view($this->folderView.'va/vaBalanceRekap', $data);
    }

    //$route['va/rekapTrx/member/(:any)'] = 'backend/transaction/rekapTrxmember/$1';
    public function rekapTrxmember($idmember)
    {
        $data['res'] = $this->m_trans->rekapTrxmember($idmember);
        $this->load->view($this->folderView.'va/vaRekapTrxMember', $data);
    }

    //$route['va/mutasiTrx'] = 'backend/transaction/mutasiTrx';
    public function mutasiTrx()
    {
        $form = $this->input->post(null, true);
        $data['res'] = $this->m_trans->rekapHistoryTrx($form);
        $data['tix'] = "1";
        //$this->load->view($this->folderView.'va/vaMutasiTrx', $data);
        $this->load->view($this->folderView.'va/vaHistoryAll', $data);
    }

    //$route['va/mutasiTrx/excel'] = 'backend/transaction/mutasiTrxExcel';
    public function mutasiTrxExcel()
    {
        $form = $this->input->post(null, true);
        $data['res'] = $this->m_trans->rekapHistoryTrx($form);
        $data['tix'] = "1";
        //$this->load->view($this->folderView.'va/vaMutasiTrx', $data);
        $this->load->view($this->folderView.'va/vaHistoryAllExcel', $data);
    }

    /*------
    END VA
    -------*/

    //$route['trans/posting'] = 'backend/transaction/getPostingTrx';
    public function getPostingTrx()
    {
        if ($this->username != null) {
            $data['form_header'] = "Posting Transaction";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/posting';
            $this->setTemplate($this->folderView.'getPostingTrx', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/posting/list'] = 'backend/transaction/getListTrxToPost';
    public function getListTrxToPost()
    {
        $data['result'] = $this->s_trans->getListTransactionToPost();
        //$this->load->view($this->folderView.'getListTrxToPost', $data);
        echo json_encode($data['result']);
    }

    //$route['trans/posting/save'] = 'backend/transaction/savePostingTrx';
    public function savePostingTrx()
    {
        $arr = $this->s_trans->savePostingTrxSrv();
        echo json_encode($arr);
    }

    //$route['trans/sms'] = 'backend/transaction/getSmsTrxList';
    public function getSmsTrxList()
    {
        if ($this->username != null) {
            $data['form_header'] = "Resend Trx SMS Notification";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/sms';
            $data['printtxt'] = base_url('trans/print/cn/txt');
            $this->setTemplate($this->folderView.'getSmsTrxList', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/sms/list'] = 'backend/transaction/postTrxForSMSResend';
    public function postTrxForSMSResend()
    {
        $data['result'] = $this->s_trans->getListTransactionReportToSMSResend();
        echo json_encode($data['result']);
    }


    //$route['trans/sms/resend/(:any)'] = 'backend/transaction/resendSMSTrx/$1';
    public function resendSMSTrx($orderno)
    {
        $data['result'] = $this->s_trans->resendSMSTrx($orderno);
        echo json_encode($data['result']);
    }

    //$route['trans/sms/resend2/(:any)/(:any)'] = 'backend/transaction/resendSMSTrx2/$1/$2';
    public function resendSMSTrx2($orderno, $token)
    {
        $data['result'] = $this->s_trans->resendSMSTrxBaru($orderno, $token);
        echo json_encode($data['result']);
    }

    //$route['trans/email/resend/(:any)'] = 'backend/transaction/resendEmailTrx/$1';
    public function resendEmailTrx($orderno)
    {
        $data['result'] = $this->s_trans->resendEmailTrx($orderno);
        echo json_encode($data['result']);
    }

    //$route['trans/sms/resend'] = 'backend/transaction/resendSMSTrx';

    /*-------------------------------
     * CN MODULE
     * -----------------------------*/
    //$route['trans/print/cn'] = 'backend/transaction/getPrintCN';
    public function getPrintCN()
    {
        if ($this->username != null) {
            $data['form_header'] = "Print CN";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/print/cn';
            $data['printtxt'] = base_url('trans/print/cn/txt');
            $this->setTemplate($this->folderView.'getPrintCN', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/print/cn/list'] = 'backend/transaction/postPrintCN';
    public function postPrintCN()
    {
        $data['result'] = $this->s_trans->getListCN();
        echo json_encode($data['result']);
    }

    //$route['trans/reprint/cn'] = 'backend/transaction/getReprintCN';
    public function getReprintCN()
    {
        if ($this->username != null) {
            $data['form_header'] = "Reprint CN";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/rerint/cn';
            $data['printtxt'] = base_url('trans/print/cn/txt');
            $this->setTemplate($this->folderView.'getRePrintCN', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/print/cn/txt'] = 'backend/transaction/printCNToTxt';
    public function printCNToTxt()
    {
        $data['printCN'] = $this->s_trans->getCNReportToPrint();
        //print_r($data['printCN']);
        $this->load->view($this->folderView.'printCNToTxt', $data);
    }

    /*-------------------------------
     * KW MODULE
     * -----------------------------*/
    //$route['trans/print/kw'] = 'backend/transaction/getPrintKW';
    public function getPrintKW()
    {
        if ($this->username != null) {
            $data['form_header'] = "Print KW";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/print/kw';
            $data['printtxt'] = base_url('trans/print/kw/txt');
            $this->setTemplate($this->folderView.'getPrintKW', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/print/kw/list'] = 'backend/transaction/postPrintKW';
    public function postPrintKW()
    {
        $data['result'] = $this->s_trans->getListKW();
        echo json_encode($data['result']);
    }

    //$route['trans/print/kw/txt'] = 'backend/transaction/printKWToTxt';
    public function printKWToTxt()
    {
        $data['printKW'] = $this->s_trans->getKWReportToPrint();
        $this->load->view($this->folderView.'printKWToTxt', $data);
    }

    /*-------------------------------
     * INCOMING PAYMENT MODULE
     * -----------------------------*/

    //$route['trans/print/ip'] = 'backend/transaction/getPrintIP';
    public function getPrintIP()
    {
        if ($this->username != null) {
            $data['form_header'] = "Print Incoming Payment";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/print/ip';
            $data['printtxt'] = base_url('trans/print/ip/txt');
            $this->setTemplate($this->folderView.'getPrintIP', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/print/ip/list'] = 'backend/transaction/postPrintIP';
    public function postPrintIP()
    {
        $data['result'] = $this->s_trans->getListIP();
        echo json_encode($data['result']);
    }

    //$route['trans/print/ip/txt'] = 'backend/transaction/printIPToTxt';
    public function printIPToTxt()
    {
        $data['username'] = $this->username;
        $data['inp'] = $this->input->post(null, true);
        $data['printIP'] = $this->s_trans->getIPReportToPrint();
        $this->load->view($this->folderView.'printIPToTxt', $data);
    }

    /*-------------------------------
     * TRANSACTION REPORT
     * -----------------------------*/
    //$route['trans/upd/loccd'] = 'backend/transaction/updateLoccdTrx';
    public function updateLoccdTrx()
    {
        $data = $this->input->post(null, true);
        $res = $this->m_trans->updateLoccdTrx($data['newidstk'], $data['orderno']);
        echo json_encode($res);
    }

    //$route['trans/report'] = 'backend/transaction/getTransactionReport';
    public function getTransactionReport()
    {
        if ($this->username != null) {
            $data['form_header'] = "Transaction Report";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/report';
            $this->load->model('backend/be_transaction_model', 'trans');
            $data['payType'] = $this->trans->getListActivePaymentType();
            $this->setTemplate($this->folderView.'getTransactionReport', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/report/list'] = 'backend/transaction/postTransactionReport';
    public function postTransactionReport()
    {
        $hasil = $this->s_trans->getListTransactionReport();
        //print_r($hasil);
        echo json_encode($hasil);
    }

    //$route['trans/reconcile/id/(:any)'] = 'backend/transaction/reconcileDataTrx/$1';
    public function reconcileDataTrx($orderno)
    {
        $data['orderno'] = $orderno;
        $data['result'] = $this->m_trans->getReconcileData($orderno);
        //$data['result'] = null;
        $this->load->view($this->folderView.'reconcileDataForm', $data);
    }

    //$route['trans/reconcile/part/save'] = 'backend/transaction/partReconcile';
    public function partReconcile()
    {
        $data = $this->input->post(null, true);
        $res = $this->m_trans->partReconcile($data);
        echo json_encode($res);
    }

    //$route['trans/reconcile/save/(:any)/(:any)/(:any)/(:any)'] = 'backend/transaction/saveReconcileTrxData/$1/$2/$3/$4';
    public function saveReconcileTrxData($orderno, $prd_stt, $ship_stt, $pay_stt)
    {
        $data['result'] = $this->m_trans->saveReconcileTrxData($orderno, $prd_stt, $ship_stt, $pay_stt);
        print_r($data['result']);
    }

    //route['trans/report/toExcel'] = 'backend/transaction/transReportExcel';
    public function transReportExcel()
    {
        if ($this->username != null) {
            $x = $this->input->post(null, true);

            if ($x['trx_type'] == "TOP") {
                $data['arr'] = $this->m_trans->getListTopUpVaReport($x);
                $this->load->view($this->folderView.'transRptVA', $data);
            } elseif ($x['trx_type'] == "vchbel") {
                $data['arr'] = $this->m_trans->getListTrxUsingVoucher($x);
                $this->load->view($this->folderView.'trxUsingVoucher', $data);
            //echo "<pre>";
                //print_r($data['arr']);
                //echo "</pre>";
            } elseif ($x['trx_type'] != "TOP" && $x['trx_type'] != "PRD") {
                $data['arr'] = $this->m_trans->getListTransactionReport($x);
                $this->load->view($this->folderView.'transRptExcell', $data);
            } else {
                $data['arr'] = $this->m_trans->getProductSummary($x);
            }

            //$data['arr'] = $this->m_trans->getListTransactionReport($x);
            //print_r($data['arr']);
            //if($data['arr'] != null){
                //$this->load->view($this->folderView.'transRptExcell', $data);
            //}
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/id/(:any)'] = 'backend/transaction/getTrxByID/$1';
    public function getTrxByID($id)
    {
        $d = $this->s_trans->getTrxByID($id);
        echo json_encode($d);
        //print_r($d);
    }

    public function getTrxByID2($id)
    {
        $cek = $this->s_trans->getTrxByID2($id);
        //		echo json_encode($data['d']);

        //		print_r($cek['detail']);
        $data['token'] = $id;
        foreach ($cek['detail'] as $d) {
            $idmemb = $d->id_memb;
            $nmmemb = $d->nmmember;
            $idstk = $d->idstk;
            $nmstk = $d->nmstkk;
            $disc = $d->discount_shipping;
            $bonus = $d->bonusmonth;
        }

        if ($idmemb != null) {
            $data['idmemb'] = $idmemb;
        } else {
            $data['idmemb'] = '';
        }
        if ($nmmemb != null) {
            $data['nmmemb'] = $nmmemb;
        } else {
            $data['nmmemb'] = '';
        }
        if ($idstk != null) {
            $data['idstk'] = $idstk;
        } else {
            $data['idstk'] = '';
        }
        if ($nmstk != null && $nmstk != '-') {
            $result = explode(' - ', $nmstk);
            $data['nmstk'] = $result[1];
        } elseif ($nmstk == '-') {
            //			echo "2";
            $data['nmstk'] = '';
        } else {
            //			echo "3";
            $data['nmstk'] = '';
        }
        if ($disc != null) {
            $data['disc'] = $disc;
        } else {
            $data['disc'] = '0';
        }
        if ($bonus != null || $bonus == "0" || $bonus == "") {
            $bns = date("m")."/".date("Y");
            $data['bonus'] = $bns;
        } else {
            $data['bonus'] = $bonus;
        }
        //		$this->load->view(detailTrans);
        $this->load->view($this->folderView.'detailTrans', $data);
    }

    //$route['trans/email/list/get] = 'backend/transaction/getListTrxToJNE';
    public function getListTrxToJNE()
    {
        if ($this->username != null) {
            $data['form_header'] = "List Conote JNE";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/email/list/get';
            $data['cargoList'] = $this->m_trans->getListCargo();
            $this->setTemplate($this->folderView.'getListSendConote', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/email/list/send'] = 'backend/transaction/sendListTrxViaEmail';
    public function sendListTrxViaEmail()
    {
        if ($this->username != null) {
            $result = $this->s_trans->getListJNEConote("flag_send_conote", "0");
            echo json_encode($result);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/email/conote/list'] = 'backend/transaction/getListConoteToEmail';
    public function getListConoteToEmail()
    {
        if ($this->username != null) {
            $res = $this->s_trans->getListConoteToEmail();
            echo $res;
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/email/pickup/list'] = 'backend/transaction/getListPickUpTrx';
    public function getListPickUpTrx()
    {
        if ($this->username != null) {
            $data['result'] = $this->s_trans->getListPickUP();
            $this->load->view($this->folderView.'getListPickup', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/email/pickup/id/(:any)'] = 'backend/transaction/getDetailPickUpProduct/$1';
    public function getDetailPickUpProduct($id)
    {
        $data['result'] = $this->s_trans->getDetailPickUpProduct($id);
        $data['pickup_no'] = $id;
        $this->load->view($this->folderView.'getDetailPickUpProduct', $data);
    }

    //$route['trans/email/conote/send'] = 'backend/transaction/sendListConoteToEmail';
    public function sendListConoteToEmail()
    {
        if ($this->username != null) {
            $data['time'] = $this->input->post(null, true);
            $res = $this->s_trans->sendEmailConote($data['time']);
            echo $res;
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/label'] = 'backend/transaction/getPrintShippingLabel';
    public function getPrintShippingLabel()
    {
        if ($this->username != null) {
            $data['form_header'] = "Print Shipping Label";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'print/shipping/label';
            $data['list_shipping'] = $this->m_trans->getListCargo();
            $this->setTemplate($this->folderView.'getPrintShippingLabel', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/label/list'] = 'backend/transaction/postPrintShippingLabel';
    public function postPrintShippingLabel()
    {
        if ($this->username != null) {
            $data['result'] = $this->s_trans->getListShippingLabel();
            $this->load->view($this->folderView.'getPrintShippingLabelResult', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/orderno/(:any)'] = 'backend/transaction/getDetailProductByOrderNo/$1';
    public function getDetailProductByOrderNo($orderno)
    {
        if ($this->username != null) {
            //echo "masuk sini";
            $data['result'] = $this->s_trans->getDetailProductByOrderNo($orderno);
            $data['conote_new'] = $this->s_trans->getCheckConoteIsNULL($orderno);
            $data['xorderno'] = $orderno;
            //echo $orderno;
            if ($data['result'] == null) {
                echo "false";
            } else {
                $this->load->view($this->folderView.'getPrintShippingDetailPrd', $data);
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/orderno/(:any)'] = 'backend/transaction/getDetailProductByOrderNo/$1';
    public function getDetailProductByOrderNo2($orderno, $conotenew)
    {
        if ($this->username != null) {
            $data['result'] = $this->s_trans->getDetailProductByOrderNo($orderno);
            if ($data['result'] == null) {
                echo "false";
            } else {
                var_dump($data);
                $this->load->view($this->folderView.'getPrintShippingDetailPrd', $data);
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/manifest/pdfTes'] = 'backend/transaction/printManifestShippingTes';
    public function printManifestShippingTes()
    {
        $this->load->library('pdf_mc_table');

        $this->load->library('ciqrcode');

        if ($this->username != null) {
            $datax = $this->input->post(null, true);
            $data['result'] = $this->s_trans->getPrintShippingPDF_2TEs($datax['orderno']);
            //print_r($data['result'][0]['conoteJNE']);
            /*if($data['result'][0]['cargo_id'] == 2) {
                //\print_r($data['result']);
                $this->load->view($this->folderView.'getPrintShipping_KGB', $data);
            } else {*/

            //$isiqrcode = $data['result'][0]['conoteJNE'];
            //$params['qrcode'] = $isiqrcode;
            //$data['qrcode'] = $this->ciqrcode->generate($params);
            //$data['qrcode'] = "ss";
            //$this->load->view($this->folderView.'getPrintShippingPDF_hilal', $data);
            //$this->load->view($this->folderView.'getPrintShippingPDF_hilal_test', $data);
            $this->load->view($this->folderView.'getPrintShippingPDF_hilal_line', $data);
        //}
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/manifest/pdfTes'] = 'backend/transaction/printManifestShippingTesnew';
    public function printManifestShippingTesnew()
    {
        $this->load->library('pdf_mc_table');

        $this->load->library('ciqrcode');

        if ($this->username != null) {
            $datax = $this->input->post(null, true);
            $data['result'] = $this->s_trans->getPrintShippingPDF_2TEs($datax['orderno']);
            $this->load->view($this->folderView.'getPrintShippingPDF_hilal_line_no', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }


    //$route['print/shipping/manifest/pdf'] = 'backend/transaction/printManifestShipping';
    public function printManifestShipping()
    {
        //$this->load->library('fpdf');
        //$this->load->library('PDF_Code128');
        $this->load->library('pdf_mc_table');

        $this->load->library('ciqrcode');
        //$params['data'] = $nim; //data yang akan di jadikan QR CODE
        //$params['level'] = 'H'; //H=High
        //$params['size'] = 10;
        //$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        //$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        if ($this->username != null) {
            $datax = $this->input->post(null, true);
            $data['result'] = $this->s_trans->getPrintShippingPDF_2($datax['orderno']);
            //print_r($data['result'][0]['conoteJNE']);
            /*if($data['result'][0]['cargo_id'] == 2) {
                //\print_r($data['result']);
                $this->load->view($this->folderView.'getPrintShipping_KGB', $data);
            } else {*/

            /*
            $isiqrcode = $data['result'][0]['conoteJNE'];
            $params['qrcode'] = $isiqrcode;
            $data['qrcode'] = $this->ciqrcode->generate($params);


            $this->load->library('ciqrcode');

            $params['conoteJNE'] = $data['result'][0]['conoteJNE'];
            $params['level'] = 'H';
            $params['size'] = 10;
            $params['savename'] = FCPATH.'captcha/'.$params['conoteJNE'].'.png';
            $data['qr']= site_url('captcha/'.$params['conoteJNE'].'.png');
            */

            //$this->load->view($this->folderView.'getPrintShippingPDF', $data); //ori 20181028 edit hilal
            //$this->load->view($this->folderView.'getPrintShippingPDF_hilal', $data);
            $this->load->view($this->folderView.'getPrintShippingPDF_be', $data);

        //}
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['kgb/print'] = 'backend/transaction/kgbTestPrint';
    public function kgbTestPrint()
    {
        $data['result'] = $this->s_trans->getPrintShippingPDF_2("'TESTDION'");
        //echo "oksds";
        //print_r($data['result']);
        $this->load->library('pdf_mc_table');
        if ($data['result'][0]['cargo_id'] == 2) {
            //print_r($data['result']);
            $this->load->view($this->folderView.'getPrintShipping_KGB', $data);
        } else {
            print_r($data['result']);
            //$this->load->view($this->folderView.'getPrintShippingPDF', $data);
        }
    }

    //$route['print/shipping/label/pdf'] = 'backend/transaction/printLabelShipping';
    public function printLabelShipping()
    {
        if ($this->username != null) {
            $datax = $this->input->post(null, true);
            $data['result'] = $this->s_trans->getPrintShippingPDF($datax['orderno']);
            $this->load->view($this->folderView.'getPrintLabel', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['print/shipping/pdf/(:any)'] = 'backend/transaction/getPrintShippingPDF2';
    public function getPrintShippingPDF2($orderno)
    {
        if ($this->username != null) {
            //$this->load->library("fpdf");
            $arr = array();
            array_push($arr, $orderno);
            $data['result'] = $this->s_trans->getPrintShippingPDF($arr);
            //print_r($data['result']);
            $this->load->view($this->folderView.'getPrintLabel', $data);
            $this->PrintShippingPDF($data);
        //print_r($data['result']);
        } else {
            redirect('backend', 'refresh');
        }
    }





    //$route['trans/payment/report'] = 'backend/transaction/getTransactionPaymentReport';

    //$route['trans/delete'] = 'backend/transaction/getListTrxToDelete';
    public function getListTrxToDelete()
    {
        if ($this->username != null) {
            $data['form_header'] = "Delete Transaction";
            $data['icon'] = "icon-trash";
            $data['form_reload'] = 'trans/delete';
            $this->setTemplate($this->folderView.'getListTrxToDelete', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/delete/list'] = 'backend/transaction/postListTrxToDelete';
    public function postListTrxToDelete()
    {
        $form = $this->input->post(null, true);
        $data['result'] = $this->s_trans->getListTrxToDelete($form);
        //print_r($form);
        $this->load->view($this->folderView.'postListTrxToDelete', $data);
    }

    //$route['trans/delete/(:any)'] = 'backend/transaction/deleteTransaction/$1';
    public function deleteTransaction($orderno)
    {
        $form = $this->input->post(null, true);
        $arr = $this->s_trans->deleteTransaction($orderno);
        echo json_encode($arr);
    }

    //$route['trans/sgo/import'] = 'backend/transaction/formImportSGOtoDB';
    public function formImportSGOtoDB()
    {
        if ($this->username != null) {
            $this->load->library('csvreader');
            $data['form_header'] = "Import SGO File to Database";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/sgo/import';
            $this->setTemplate($this->folderView.'sgoImportForm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/sgo/import/preview'] = 'backend/transaction/previewFileCSV';
    public function previewFileCSV()
    {
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            //print_r($data['csvData']);
            //print_r($data['csvData'][1]);
            /*foreach($data['csvData'] as $field) {
             echo $field[0]. " - ";
             echo $field[1];
             echo $field[4];
             echo $field[3];
             echo $field[7];
             echo $field[6];
             echo "<br />";
            }	*/
            $this->load->view($this->folderView.'sgoPreviewFile', $data);
        } else {
            echo "No Data Uploaded";
        }
        //var_dump($_POST);
    }

    //$route['trans/sgo/import/save'] = 'backend/transaction/saveImportToDB';
    public function saveImportToDB()
    {
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            //var_dump($data['csvData']);
            $save = $this->s_trans->saveImportToDB($data);
            //var_dump($save);
            $this->load->view($this->folderView.'sgoImportResult', $save);
        } else {
            echo "No Data Uploaded!";
        }
    }

    //$route['trans/sgo/import/goworld'] = 'backend/transaction/formImportSGOtoDBGW';
    public function formImportSGOtoDBGW()
    {
        if ($this->username != null) {
            $this->load->library('csvreader');
            $data['form_header'] = "Import GoWorld SGO File to Database";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/sgo/import/goworld';
            $this->setTemplate($this->folderView.'sgoImportFormGoworld', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }
    //$route['trans/sgo/import/goworld/preview'] = 'backend/transaction/previewFileCSVGW';
    public function previewFileCSVGW()
    {
        $form = $this->input->post(null, true);
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);

            if ($form['download_type'] == "goworld") {
                $this->load->view($this->folderView.'sgoPreviewFileGoWorld', $data);
            } else {
                $this->load->view($this->folderView.'sgoPreviewFile', $data);
            }
        } else {
            echo "No Data Uploaded";
        }
    }
    //$route['trans/sgo/import/goworld/save'] = 'backend/transaction/saveImportToDBGW';
    public function saveImportToDBGW()
    {
        $form = $this->input->post(null, true);
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            //var_dump($data['csvData']);
            if ($form['download_type'] == "goworld") {
                $save = $this->s_trans->saveImportToDBGW($data);
            } else {
                $save = $this->s_trans->saveImportToDB($data);
            }
            //var_dump($save);
            $this->load->view($this->folderView.'sgoImportResultGW', $save);
        } else {
            echo "No Data Uploaded!";
        }
    }

    //$route['trans/sgo/import/updateTrxId'] = 'backend/transaction/updateSgoTrxId';
    public function updateSgoTrxId()
    {
        $form = $this->input->post(null, true);
        $this->load->library('csvreader');
        $fileName = $_FILES["myfile"]["tmp_name"];
        if ($fileName != "") {
            $this->csvreader->set_separator(",");
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            //var_dump($data['csvData']);
            if ($form['download_type'] == "goworld") {
                $save = $this->s_trans->updateSgoTrxId($data);
            }
            $this->load->view($this->folderView.'sgoImportResultGW', $save);
        } else {
            echo "No Data Uploaded!";
        }
    }

    //$route['trans/sgo/contoh'] = 'backend/transaction/contoh';
    public function contoh()
    {
        $this->load->view($this->folderView.'contohImport');
        //phpinfo();
    }

    //$route['trans/sgo/reconcile'] = 'backend/transaction/formSGOReconcileTrx';
    public function formSGOReconcileTrx()
    {
        if ($this->username != null) {
            $data['form_header'] = "SGO Reconcile Transaction";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/sgo/reconcile';

            $data['bankList'] = $this->s_trans->getListBank();
            $this->setTemplate($this->folderView.'sgoFormReconcile', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/sgo/reconcile/list'] = 'backend/transaction/getListSGOTrx';
    public function getListSGOTrx($rptType = 0)
    {
        $form = $this->input->post(null, true);

        //echo $rptType;
        $data['rptType']=$rptType;
        $data['from']=$form['from'];
        $data['to']=$form['to'];
        $data['status']=$form['status'];
        $data['bank']=$form['bank'];
        //print_r($rptType);
        if ($rptType == '4') {
            //var_dump($data);
            $data['result'] = $this->s_trans->getListSGOTrx($form);
            //print_r($data['result']);
            $this->load->view($this->folderView.'sgoReconcileResCS', $data);
        } elseif ($rptType != '4' && $form['trx_type'] == "1") {
            //print_r($data);
            $data['result'] = $this->s_trans->getListSGOTrx($form);
            //print_r($data['result'][0]);
            $this->load->view($this->folderView.'sgoFormReconcileResult', $data);
        } elseif ($rptType != '4' && $form['trx_type'] == "2") {
            $data['result'] = $this->s_trans->getListSGOTrxPpob($form);
            /* echo "<pre>";
            print_r($data['result']);
            echo "</pre>"; */
            $this->load->view($this->folderView.'sgoFormReconcileResultPPob', $data);
        }
    }

    public function getListTrxManual($rptType = 0)
    { //$route['trans/sgo/reconcile/manual/(:any)'] = 'backend/transaction/getListTrxManual/$1';
        $form = $this->input->post(null, true);

        //echo $rptType;
        $data['rptType']=$rptType;
        $data['from']=$form['from'];
        $data['to']=$form['to'];
        /*$data['status']=$form['status'];
        $data['bank']=$form['bank'];*/

        $dtfrom = date("Y-m-d", strtotime($data['from']));
        $dtto = date("Y-m-d", strtotime($data['to']));

        /*$qryItem = "select * from ecomm_trans_hdr_sgo
                    where kode_pay !='' AND (datetrans BETWEEN '$dtfrom 00:00:00' AND '$dtto 23:59:59') AND  order by id DESC";
        $queryExec = $this->db->query($qryItem);

        if($queryExec->num_rows() > 0) {
            $no=0;
            foreach($queryExec->result() as $row) {
                $no++;
                $orderno = $row->orderno;

                $qryItem1 = "select * from ecomm_trans_hdr where token='$orderno'";
                $queryExec1 = $this->db->query($qryItem1);

                if($queryExec1->num_rows() > 0) {
                    $data['status_pay']="1";
                }else{
                    $data['status_pay']="0";
                }
            }
        }*/

        $data['result'] = $this->s_trans->getListManualTrx($form);

        $this->load->view($this->folderView.'sgoReconcileManual', $data);

        //print_r($rptType);
        /*if($rptType == '4'){
            //var_dump($data);
            $data['result'] = $this->s_trans->getListManualTrx($form);
            //print_r($data['result']);
            $this->load->view($this->folderView.'sgoReconcileManual', $data);
        }else if($rptType != '4' && $form['trx_type'] == "1"){
            //print_r($data);
            $data['result'] = $this->s_trans->getListSGOTrx($form);
            //print_r($data['result'][0]);
            $this->load->view($this->folderView.'sgoFormReconcileResult', $data);

        } else if($rptType != '4' && $form['trx_type'] == "2"){
            $data['result'] = $this->s_trans->getListSGOTrxPpob($form);
            /* echo "<pre>";
            print_r($data['result']);
            echo "</pre>"; */
            /*$this->load->view($this->folderView.'sgoFormReconcileResultPPob', $data);
        }*/
    }

    //$route['trans/sgo/import/save'] = 'backend/transaction/saveSGOByTrx';
    public function saveSGOByTrx($orderid)
    {
        $form = $this->input->post(null, true);
        //$data['status'] = $form['status'];
        $save = $this->s_trans->saveTrxToDB($orderid);
        echo json_encode($save);
        //$this->getListSGOTrx();
        //print_r($save);
        //$this->load->view($this->folderView.'sgoImportResult', $save);
    }

    //$route['trans/sgo/import/save'] = 'backend/transaction/saveSGOMembByTrx';
    //$route['trans/sgo/reconcile/saveMemb/(:any)/(:any)/(:any)'] = 'backend/transaction/saveSGOMembByTrx/$1/$2/$3';
    public function saveSGOMembByTrx($memb_status, $memberid, $orderid)
    {
        $save = $this->s_trans->saveMEMToDB($memb_status, $memberid, $orderid);
        echo json_encode($save);
    }

    /*
    public function saveMEMSGOByTrx($orderid) {
        $form = $this->input->post(NULL, TRUE);
        //$data['status'] = $form['status'];
        $save = $this->s_trans->saveMEMToDB($orderid);
        echo json_encode($save);
        //$this->getListSGOTrx();
        //print_r($save);
        //$this->load->view($this->folderView.'sgoImportResult', $save);
    }
    */
    /*---------------------
     * BANK CHARGE MODULE
     * -------------------*/

    //$route['trans/bank/charge'] = 'backend/transaction/formBankCharge';
    public function formBankCharge()
    {
        if ($this->username != null) {
            $data['form_header'] = "Bank Charge Setting";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/bank/charge';
            $this->setTemplate($this->folderView.'bankChargeForm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/bank/charge/list'] = 'backend/transaction/getListBankCharge';
    public function getListBankCharge()
    {
        if ($this->username != null) {
            $data['result'] = $this->s_trans->getListBankCharge();
            $this->load->view($this->folderView.'bankChargeList', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/bank/charge/save'] = 'backend/transaction/postSaveBankCharge';
    public function postSaveBankCharge()
    {
        $arr = sessionExpireMessage(false);
        if ($this->username != null) {
            $data = $this->input->post(null, true);
            $arr = $this->s_trans->postSaveBankCharge($data);
        }
        echo $arr;
    }

    //$route['trans/bank/get/(:any)/(:any)'] = 'backend/transaction/getListBankChargeByParam/$1/$2';
    public function getListBankChargeByParam($param, $value)
    {
        $arr = jsonFalseResponse("Session Expired, please re-login..!!");
        if ($this->username != null) {
            $arr = $this->s_trans->getListBankChargeByParam($param, $value);
        }
        echo json_encode($arr);
    }
    //$route['trans/bank/charge/update'] = 'backend/transaction/postUpdateBankCharge';
    public function postUpdateBankCharge()
    {
        $arr = sessionExpireMessage();
        if ($this->username != null) {
            $data = $this->input->post(null, true);
            $arr = $this->s_trans->postUpdateBankCharge($data);
        }
        echo json_encode($arr);
    }

    //$route['trans/bank/charge/delete/(:any)'] = 'backend/transaction/postDeleteBankCharge/$1';
    public function postDeleteBankCharge($id)
    {
    }


    /*========tambahan ananech=======*/

    //$route['do'] = 'backend/transaction/getDeliveryOrder';
    public function getDeliveryOrder()
    {
        if ($this->username != null) {
            $data['form_header'] = "Delivery Order Form";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'wh/do';
            $data['wh'] = $this->m_trans->getWHList();
            $data['stk'] = $this->m_trans->getStkList1();
            $this->setTemplate($this->folderView.'getDOForm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/doList'] = 'backend/transaction/getDOList';
    public function getDOList()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);

            $x['listDO'] = $this->m_trans->getListDO($dt);

            if ($dt['destTo'] == "") {
                $dt['destTo'] = "null";
            }
            if ($dt['sentToWH'] == "") {
                $dt['sentToWH'] = "null";
            }
            if ($dt['sentToStk'] == "") {
                $dt['sentToStk'] = "null";
            }
            $dt_input = array(
                            'trx_type' => $dt['trx_type'],
                            'destTo' => $dt['destTo'],
                            'sentToWH' => $dt['sentToWH'],
                            'sentToStk' => $dt['sentToStk']
                        );
            $this->session->set_userdata('group_do', $dt_input);
            $this->load->view($this->folderView.'listing_DO', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/group'] = 'backend/transaction/getDOGroup';
    public function getDOGroup()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);
            $abc = $this->session->userdata('group_do');
            $x['arrayCek'] = array('cekArray' => $dt['ceks']);

            $this->session->set_userdata('cekKw', $x['arrayCek']);

            if ($abc['trx_type'] == 'stk' && $abc['destTo'] == 'stockist') {
                $x['headNM'] = 'Stockist Code';
                $x['headDest'] = 'Stockist';
            } else {
                $x['headNM'] = 'Warehouse Code';
                $x['headDest'] = 'Warehouse';
            }

            $x['headDO'] = $this->m_trans->getAddrDO($abc);
            $x['kurir'] = $this->m_trans->getKurir();
            $x['groupDO'] = $this->m_trans->getGroupPrd($dt['ceks']);
            //$x['detDO'] = $this->m_trans->getDetGroup($dt['ceks']);
            if ($x['groupDO'] > 0) {
                $this->load->view($this->folderView.'getGroupPrd', $x);
            } else {
                echo "Gagal";
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/groupAct'] = 'backend/transaction/actDOGroup';
    public function actDOGroup()
    {
        if ($this->username != null) {
            $ceks = $this->session->userdata('cekKw');
            $abc = $this->session->userdata('group_do');
            $dt = $this->input->post(null, true);
            $x['actDO'] = $this->m_trans->generateDO($ceks, $dt['courier']);
            $x['DOno'] = $x['actDO']['DOno'];
            if ($x['actDO']['response'] == 1) {
                $x['hdrDO'] = $this->m_trans->getHdrDO($x['DOno'], $abc);
                $x['detDO'] = $this->m_trans->getDetDO($x['DOno']);
                $this->load->view($this->folderView.'groupDORes', $x);
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/txt'] = 'backend/transaction/printTxtDO';
    public function printTxtDO()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);
            $abc = $this->session->userdata('group_do');
            $x['hdrDO'] = $this->m_trans->getHdrDO($dt['doNo'], $abc);
            $x['detDO'] = $this->m_trans->getDetDO($dt['doNo']);
            $x['gdono'] = $dt['doNo'];
            $x['jums'] = $this->m_trans->getCountPRDDO($dt['doNo']);
            $this->load->view($this->folderView.'groupDOtxt', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/txt1'] = 'backend/transaction/printTxtDO';
    public function printTxtDO1()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);
            echo "Do No ".$dt['doNo']." - ".$dt['shipto']."";
        /*$x['hdrDO'] = $this->m_trans->getHdrDO1($dt['doNo'],$dt['shipto']);
        $x['detDO'] = $this->m_trans->getDetDO($dt['doNo']);
        $x['gdono'] = $dt['doNo'];
        $x['jums'] = $this->m_trans->getCountPRDDO($dt['doNo']);
        $this->load->view($this->folderView.'groupDOtxt', $x);*/
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/listing'] = 'backend/transaction/getListGroupDO';
    public function getListGroupDO()
    {
        if ($this->username != null) {
            $data['form_header'] = "Delivery Order Form";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'wh/do/listing';

            $this->setTemplate($this->folderView.'getDOListFrm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/listing/act'] = 'backend/transaction/listingGroupDOact';
    public function listingGroupDOact()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);

            $x['listGrpDO'] = $this->m_trans->getListGroupDO($dt);
            $this->load->view($this->folderView.'listing_groupDO', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['wh/do/list/det/(:any)/(:any))'] = 'backend/transaction/listingGroupDOdet/$1/$2';
    public function listingGroupDOdet($DoNo, $shipto)
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);

            $x['hdrDO'] = $this->m_trans->getHdrDO1($DoNo, $shipto);
            $x['detDO'] = $this->m_trans->getDetDO($DoNo);
            $this->load->view($this->folderView.'groupDORes', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/sgo/report'] = 'backend/transaction/getFrmSgoRpt';
    public function getFrmSgoRpt()
    {
        if ($this->username != null) {
            $data['form_header'] = "SGO Report";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/sgo/report';

            $this->setTemplate($this->folderView.'sgoFormReport', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/sgo/report/act'] = 'backend/transaction/sgoReportAct';
    public function sgoReportAct()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);
            $x['rptSgo'] = $this->m_trans->getReportSgo($dt);
            $x['exportTo'] = $dt['exportTo'];
            $exportTo = $dt['exportTo'];
            $x['from'] = $dt['from'];
            $x['to'] = $dt['to'];

            //echo $_POST['exportTo'];

            if ($exportTo == 0 or $exportTo == 2) {
                $this->load->view($this->folderView.'reportSgoRes', $x);
            } elseif ($exportTo == 1) {
                $x['totalSgo'] = $this->m_trans->getTotalSgo($dt);
                $this->load->view($this->folderView.'sgoResCsv', $x);
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/sgo/report/csv'] = 'backend/transaction/sgoReportCsv';
    public function sgoReportCsv()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);
            $exportTo = $dt['exportTo'];
            $x['rptSgo'] = $this->m_trans->getReportSgo($dt);
            $x['totalSgo'] = $this->m_trans->getTotalSgo($dt);
            //$filename = "rptSgo.csv";
            //force_download($filename, $x['rptSgo']['data1'],$x['rptSgo']['data2']);
            $x['from'] = $dt['from'];
            $x['to'] = $dt['to'];

            if ($exportTo == 0) {
                $this->sgoReportAct();
            } elseif ($exportTo == 1) {
                $this->load->view($this->folderView.'sgoResCsv', $x);
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/jne/frmNoConot'] = 'backend/transaction/getFrmNoConot';
    public function getFrmNoConot()
    {
        if ($this->username != null) {
            $x['form_header'] = "Update E-Conot JNE";
            $x['icon'] = "icon-pencil";
            $x['form_reload'] = 'trans/jne/frmNoConot';

            $this->setTemplate($this->folderView.'getFrmNoConot', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/jne/getListNoConot'] = 'backend/transaction/getListNoConot';
    public function getListNoConot()
    {
        if ($this->username != null) {
            $dt = $this->input->post(null, true);
            if ($dt['trx_type'] == "null_econote") {
                $x['listCargo'] = $this->m_trans->getListAllCargo();
                //$x['listnoConot'] = $this->m_trans->getListNoConot($dt);
                //update dion @17/01/2018 karena bnyak yg null
                $x['listnoConot'] = $this->m_trans->getListNoConotBaru($dt);
                $this->load->view($this->folderView.'listNoConot', $x);
            } elseif ($dt['trx_type'] == "rekap_kirim") {
                $x['listnoConot'] = $this->m_trans->getListPengirimanWH($dt);
                //print_r($x['listnoConot']);
                $x['title'] = "List Shipping Branch Warehouse";
                $this->load->view($this->folderView.'listNoResiFisik', $x);
            } elseif ($dt['trx_type'] == "data_shipping_not_complete") {
                $x['listnoConot'] = $this->m_trans->getEmptyListConnote($dt['dtfroms'], $dt['dttos']);
								$x['title'] = "List E-Connote Kosong dan Data Shipping Tidak Lengkap";
								$this->load->view($this->folderView.'emptyListConnote', $x);
            } else {
                $x['listnoConot'] = $this->m_trans->getListNullResiFisik($dt);
                //print_r($x['listnoConot']);
                $x['title'] = "List Resi Fisik yang belum diupdate Branch Warehouse";
                $this->load->view($this->folderView.'listNoResiFisik', $x);
            }
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/cargo/upd/(:any)/(:any)'] = 'backend/transaction/updateCargoShipper/$1/$2';
    public function updateCargoShipper($orderno, $cargo_id)
    {
        $res = jsonFalseResponse("Update Cargo ID gagal..");
        $res = $this->m_trans->updateCargoShipper($orderno, $cargo_id);
        if ($res > 0) {
            $res = jsonTrueResponse(null, "Update Ekspedisi berhasil..");
        }
        echo json_encode($res);
    }

    public function getEconotUpdtKgp($orderno, $addr1, $token, $tipe)
    {
        if ($addr1 == "" || $addr1 == null || $tipe == '0') {
            $yy = $this->m_trans->getDataFailedConot1($orderno, $token);
            //print_r($yy);
            if ($yy['response'] == 1) {
                $dta = $this->m_trans->getDataFailedConot($orderno);
            }
        } else {
            $dta = $this->m_trans->getDataFailedConot($orderno);
        }
        //$dta = $this->m_trans->getDataFailedConot($orderno);
        if ($dta[0]->phoneNumberSender == "" || $dta[0]->phoneNumberSender == null) {
            $dta[0]->phoneNumberSender = '00';
        }

        $urlKota = "202.152.20.135:8080/Service.svc/ResponseKonos";
        //$curl = curl_init($urlKota);
        $curl_data = array(
                         //"usernama"	    => "relas",
                         //"passw"		=> "123456",
                         "usernama"	    => "k-link2",
                         "passw"		=> "pusat@59A",
                         "transaksi_id" => $dta[0]->orderno,
                         "kota"		    => $dta[0]->destinationAddr,
                         "penerima"	    => $dta[0]->nama_penerima,
                         "alamat"		=> $dta[0]->addr1,
                         "telepon"		=> $dta[0]->phoneNumberReceiver,
                         "berat"	    => $dta[0]->total_weight);

        $data_string = json_encode($curl_data);
        //print_r($data_string);

        $ch = curl_init($urlKota);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );

        $curl_response = curl_exec($ch);
        $arr = json_decode($curl_response);
        //print_r($arr);
        if ($arr->status == "999") {
            $jne_cnote = null;
        } else {
            $jne_cnote = $arr->KonosemenNo;
            //$y = $this->mm_trans->updateConotByOrderNo($dta[0]->orderno,$jne_cnote);
        }
        return $arr;
    }

    /*function getKonotJNE($orderno, $token) {

    }*/

    public function getEconotUpdt($orderno, $addr1, $token, $tipe)
    {
        //function getEconotUpdt($orderno){
        $jne_url_conot = "http://api.jne.co.id:8889/tracing/klink/generateCnote";
        $jne_username = "KLINK";
        $jne_api_key = "76270305bef5d402220c96d59ac61977";
        $jne_OLSHOP_CUST = "80075400";


        if ($addr1 == "" || $addr1 == null || $tipe == '0') {
            $yy = $this->m_trans->getDataFailedConot1($orderno, $token);
            //print_r($yy);
            if ($yy['response'] == 1) {
                $dta = $this->m_trans->getDataFailedConot($orderno);
            }
        } else {
            $dta = $this->m_trans->getDataFailedConot($orderno);
            //print_r($dta);
        }
        //$dta = $this->m_trans->getDataFailedConot($orderno);
        if ($dta[0]->phoneNumberSender == "" || $dta[0]->phoneNumberSender == null) {
            $dta[0]->phoneNumberSender = '00';
        }
        //$ordernooo = $dta[0]->orderno."X";
        $ordernooo = $dta[0]->orderno;
        $addr1 = str_replace(array("\n","\r"), '', substr($dta[0]->addr1, 0, 19));
        $addr2 = str_replace(array("\n","\r"), '', substr($dta[0]->addr1, 19, 19));
        $addr3 = str_replace(array("\n","\r"), '', substr($dta[0]->addr1, 38, 19));
        $prov_name = str_replace(array("\n","\r"), '', substr($dta[0]->nama_provinsi, 0, 19));
        $stk_name = str_replace(array("\n","\r"), '', substr($dta[0]->stockist_name, 0, 19));
        $curl_post_data = array(
                            "username" => $jne_username,
                            "api_key" => $jne_api_key,
                            "OLSHOP_BRANCH" => $dta[0]->jne_branch,
                            "OLSHOP_CUST" => $jne_OLSHOP_CUST,
                            "OLSHOP_ORIG" => $dta[0]->senderAddr,
                            "OLSHOP_ORDERID" => $ordernooo,
                            //"OLSHOP_ORDERID" => 'IDEC160799999XY',
                            "OLSHOP_SHIPPER_NAME" => $stk_name,
                            "OLSHOP_SHIPPER_ADDR1" => $stk_name,
                            "OLSHOP_SHIPPER_ADDR2" => $stk_name,
                            "OLSHOP_SHIPPER_ADDR3" => $stk_name,
                            "OLSHOP_SHIPPER_CITY" => $stk_name,
                            "OLSHOP_SHIPPER_REGION" => $stk_name,
                            "OLSHOP_SHIPPER_ZIP" => "12620",
                            "OLSHOP_SHIPPER_PHONE" => $dta[0]->phoneNumberSender,
                            "OLSHOP_RECEIVER_NAME" => $dta[0]->nama_penerima,
                            "OLSHOP_RECEIVER_ADDR1" => $addr1,
                            "OLSHOP_RECEIVER_ADDR2" => $addr2,
                            "OLSHOP_RECEIVER_ADDR3" => $addr3,
                            /*"OLSHOP_RECEIVER_ADDR1" => $alamat,
                            "OLSHOP_RECEIVER_ADDR2" => "",
                            "OLSHOP_RECEIVER_ADDR3" => "",*/
                            //"OLSHOP_RECEIVER_ADDR3" => "BANDUNG",
                            "OLSHOP_RECEIVER_CITY" => str_replace("Kab.", "", $dta[0]->nmKabupaten),
                            "OLSHOP_RECEIVER_REGION" => $prov_name,
                            "OLSHOP_RECEIVER_ZIP" => "12620",
                            "OLSHOP_RECEIVER_PHONE" => $dta[0]->phoneNumberReceiver,
                            //"OLSHOP_RECEIVER_PHONE" => "08112100585",
                            "OLSHOP_DEST" => $dta[0]->destinationAddr,
                            "OLSHOP_SERVICE" => $dta[0]->servicetypeID,
                            "OLSHOP_QTY" => $dta[0]->total_Items,
                            "OLSHOP_WEIGHT" => $dta[0]->total_weight,
                            "OLSHOP_GOODSTYPE" => "2",
                            "OLSHOP_GOODSDESC" => "SUPLEMENT",
                            "OLSHOP_INST" => "FRAGILE",
                            "OLSHOP_GOODSVALUE" => $dta[0]->total_pay,
                            //"OLSHOP_GOODSVALUE" => 1000,
                            "OLSHOP_INSURANCE" => "Y"
                            );

        $curl = curl_init($jne_url_conot);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $curl_response = curl_exec($curl);
        $arr = json_decode($curl_response);

        echo "<pre>";
        print_r($curl_post_data);
        echo "</pre>";

        echo "<pre>";
        print_r($arr);
        echo "</pre>";

        //print_r($curl_response);
        //echo "<br />";
        //print_r($curl_post_data);
        //$jne_cnote = $arr->detail[0]->cnote_no;

        return $arr;
    }

    //$route['trans/jne/updtEConotJne'] = 'backend/transaction/updtEConotJne';
    public function updtEConotJne($orderno, $tipe, $token)
    {
        //if($tipe == '1'){
        $x = $this->m_trans->getDataFailedConot($orderno);
        //echo "fddsf ".$x[0]->addr1;
        if ($x[0]->cargo_id == "2") {
            $conot = $this->getEconotUpdtKgp($orderno, $x[0]->addr1, $token, $tipe);
            //if($conot != null) {
            if ($conot->status == "000") {
                $dtConot = $conot->KonosemenNo;
                $y = $this->mm_trans->updateConotByOrderNo($orderno, $dtConot);
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>Update Conot Sukses ".$dtConot."<br><br>";
            } else {
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>Update Conot Gagal<br><br>";
            }
            /*} else {
                print_r($conot);
            }*/
        } elseif ($x[0]->cargo_id == "4") {
            $this->db = $this->load->database('db_ecommerce', true);
            $qryShpHdr = "SELECT a.*,b.total_pay
		              FROM ecomm_trans_shipaddr a
					  LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
		              WHERE a.orderno = '$orderno'";
            $queryShpHc = $this->db->query($qryShpHdr);

            if ($queryShpHc->num_rows() > 0) {
                $row = $queryShpHc->result();
                $lat=$row[0]->lat_dest;
                $long=$row[0]->long_dest;

                $metode = $row[0]->service_type_name;
                $des_name = $row[0]->receiver_name;
                $des_phone = $row[0]->tel_hp1;
                $des_addr = cleanAddress($row[0]->addr1);
                $des_coord = $lat.",".$long;
                $price = $row[0]->total_pay;

                $warehouse=$row[0]->whcd;
                $stk=$row[0]->idstk;
            }


            $qryItem = "SELECT a.prdnm,a.qty
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
            $queryExec = $this->db->query($qryItem);

            if ($queryExec->num_rows() > 0) {
                $no=0;
                foreach ($queryExec->result() as $row1) {
                    $no++;
                    $prdnm= $row1->prdnm;
                    $qty= $row1->qty;
                    $concat_item= "(".$prdnm." : ".$qty." pc)";
                    $item_arr[]= $concat_item;
                    $item=implode(" ", $item_arr);
                }
            }

            $qryItem1 = "SELECT * FROM V_ECOMM_GOSEND_SENDER_DET where whcd='$warehouse' AND loccd='$stk'";
            $queryExec1 = $this->db->query($qryItem1);

            if ($queryExec1->num_rows() > 0) {
                foreach ($queryExec1->result() as $rows) {
                    $fullnm= $rows-> fullnm;
                    $addr1= $rows->addr1;
                    $addr2= $rows->addr2;
                    $addr3= $rows->addr3;
                    $city= $rows->city;
                    $wh_tel_hp= $rows->tel_hp;
                    $wh_lat= $rows->latitude;
                    $wh_long= $rows->longitude;
                    $wh_head= $rows->WH_HEAD;
                    $hp_confirm= $rows->hp_to_confirm;
                }
            }


            if ($hp_confirm == "") {
                $origin_kontak= $wh_tel_hp;
            } else {
                $origin_kontak= $hp_confirm;
            }
            $sendhp = explode(",", $origin_kontak);

            $senderName= $wh_head;
            $senderNote= $hp_confirm;
            $senderContact= $fullnm;
            $senderPhone= $sendhp[0];
            $senderCoord= $wh_lat.",".$wh_long;
            $senderAlamat= $addr1." ".$addr2." ".$addr3." ".$city;

            /*$senderName="Bagian Warehouse";
            $senderNote="K-Mart Lobby Timur";
            $senderContact="K-Link Indonesia";
            $senderPhone="081388319355";
            $senderCoord="-6.2400292,106.8343388";
            $senderAlamat="Jalan Jend. Gatot Subroto Kav. 59A Jakarta Selatan (Lobby Timur)";*/

            //get user key gosend
            $this->load->model('lokasi/M_location', 'M_location');
            $getKey= $this->M_location->getUserKey();
            $user_key= $getKey[0]->username;
            $pass_key= $getKey[0]->key_user;

            //mendapatkan jam buka gosend
            $day = date('D');
            $time= date('H:i:s');
            $now = new Datetime($time);
            //$now = new Datetime("now");
            $type_service=$row[0]->service_type_name;

            $hp= $row[0]->tel_hp1;
            $hp_val=str_replace("62", "0", $hp);
            $hp_val=str_replace("+", "", $hp_val);
            $hp_val=str_replace("-", "", $hp_val);
            $hp_val=str_replace(" ", "", $hp_val);

            /*$hp1="081388319355";
            $hp2="08568369705";*/

            $hp1= $sendhp[1];
            $hp2= $sendhp[0];

            $hp3="085717743438";

            //Instant
            $getInstant= $this->M_location->getSchedule($day, 'Instant');
            $open_inst= $getInstant[0]->g_open;
            $closed_inst= $getInstant[0]->g_closed; //batas akhir konfirmasi pembayaran
            $begintime1 = new DateTime($open_inst);
            $endtime1 = new DateTime($closed_inst);

            $inst_open=date('H:i:s', strtotime($open_inst));
            $inst_closed=date('H:i:s', strtotime($closed_inst));

            //SameDay
            $getSame= $this->M_location->getSchedule($day, 'SameDay');
            $open_same= $getSame[0]->g_open;
            $closed_same= $getSame[0]->g_closed; // batas akhir konfirmasi pembayaran
            $begintime2 = new DateTime($open_same);
            $endtime2 = new DateTime($closed_same);

            $same_open=date('H:i:s', strtotime($open_same));
            $same_closed=date('H:i:s', strtotime($closed_same));

            // CALL API GOSEND
            if ($type_service == 'Instant') {
                if ($time >= $inst_open && $time <= $inst_closed) {
                    $dt = booking_gsend($user_key, $pass_key, $senderName, $senderNote, $senderContact, $senderPhone, $senderCoord, $senderAlamat, $metode, $des_name, $des_phone, $des_addr, $des_coord, $item, $price, $orderno);
                    $result= json_decode($dt, true);
                    //print_r($result);
                    if ($result == null || $result ==''||  $result['errors'] !='') {
                        if ($result['errors'] == "Sorry, you can only place a new order 86400 second after the last order you placed") {
                            $dt = status_gsend($user_key, $pass_key, $orderno);
                            $result1= json_decode($dt, true);
                            $order_g= $result1['orderNo'];
                            print_r("masuk sini 1");
                        } else {
                            $order_g="";
                            print_r("masuk sini 2");
                        }

                        /*//sms ke gudang
                        $txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
                        smsTemplate($hp1, $txt);
                        smsTemplate($hp2, $txt);*/
                    } else {
                        $order_g=$result['orderNo'];
                        /*//sms ke vera
                        $txt = "error gosend Sameday:".$orderno.",error:".$result['orderNo'];
                        smsTemplate($hp3, $txt);*/
                    }

                    if ($order_g != "") {
                        //sms ke gudang
                        $txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
                        smsTemplate($hp1, $txt);
                        smsTemplate($hp2, $txt);
                    } else {
                        //sms ke vera
                        $txt = "error update conote:".$orderno;
                        smsTemplate($hp3, $txt);
                    }
                } else {
                    print_r("masuk sini 3");
                    print_r("<br>");
                    print_r($time.">=".$inst_open."<=".$inst_closed);

                    $order_g="";
                    $text = "Info pesanan gosend nomor order ".$orderno." melewati jam batas pengiriman dan akan dikirimkan di hari kerja besok.";
                    smsTemplate($hp_val, $text);
                }
            } elseif ($type_service == 'SameDay') {
                if ($time >= $same_open && $time <= $same_closed) {
                    $dt = booking_gsend($user_key, $pass_key, $senderName, $senderNote, $senderContact, $senderPhone, $senderCoord, $senderAlamat, $metode, $des_name, $des_phone, $des_addr, $des_coord, $item, $price, $orderno);
                    $result= json_decode($dt, true);
                    /*echo "s : <br />";
                    print_r($result);*/

                    /*if($result['response'] !='') {
                        $dt = status_gsend($user_key, $pass_key,$orderno);
                        $result1= json_decode($dt, TRUE);
                        $order_g= $result1['orderNo'];

                    }else*/
                    if ($result == null || $result ==''||  $result['errors'] !='') {
                        if ($result['errors'] == "Sorry, you can only place a new order 86400 second after the last order you placed") {
                            $dt = status_gsend($user_key, $pass_key, $orderno);
                            $result1= json_decode($dt, true);
                            $order_g= $result1['orderNo'];
                            print_r("masuk sini 3");
                        } else {
                            $order_g="";
                            print_r("masuk sini 4");
                        }
                    } else {
                        $order_g=$result['orderNo'];
                        /*//sms ke vera
                        $txt = "error gosend Sameday:".$orderno.",error:".$result['orderNo'];
                        smsTemplate($hp3, $txt);*/
                    }

                    if ($order_g != "") {
                        //sms ke gudang
                        $txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
                        smsTemplate($hp1, $txt);
                        smsTemplate($hp2, $txt);
                    } else {
                        //sms ke vera
                        $txt = "error update conote:".$orderno;
                        smsTemplate($hp3, $txt);
                    }
                } else {
                    $order_g="";
                    $text = "Info pesanan gosend nomor order ".$orderno." melewati jam batas pengiriman dan akan dikirimkan di hari kerja besok.";
                    smsTemplate($hp_val, $text);
                }
            } else {
                $order_g="";
                $text = "Info pesanan gosend nomor order ".$orderno." mohon dicek metode pengiriman untuk nomor order tersebut.";
                smsTemplate($hp_val, $text);
            }

            $y = $this->mm_trans->updateConotByOrderNo($orderno, $order_g);

            /*$dt = booking_gsend($user_key, $pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price);
            $result= json_decode($dt, TRUE);
            $order_g=$result['orderNo'];*/
            //if($conot != null) {
            if ($order_g != "") {
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>Update Conot Gosend Sukses ".$order_g."<br><br>";
            } else {
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                $dt = tes_booking_gsend($user_key, $pass_key, $senderName, $senderNote, $senderContact, $senderPhone, $senderCoord, $senderAlamat, $metode, $des_name, $des_phone, $des_addr, $des_coord, $item, $price, $orderno);
                //$paramxx = json_decode($dt);

                echo "<pre>";
                print_r($dt['response']);
                echo "</pre>";

                echo "<pre>";
                print_r($dt);
                echo "</pre>";

                print_r($user_key);
                echo "<br>";
                print_r($pass_key);
                echo "<br>";
                print_r($metode);
                echo "<br>";
                print_r($des_name);
                echo "<br>";
                print_r($des_phone);
                echo "<br>";
                print_r($des_addr);
                echo "<br>";
                print_r($des_coord);
                echo "<br>";
                print_r($price);
                echo "<br>";
                print_r($item);
                echo "<br>";
                echo "<br><br>Update Conot Gosend Gagal<br><br>";
            }
        } elseif ($x[0]->cargo_id == "1") {
            //$conot = $this->getEconotUpdt($orderno,$x[0]->addr1,$token,$tipe);
            $conot = $this->s_trans->generate_konot_jne_only($token, $orderno);
            echo "<pre>";
            //				echo "conot";
            print_r($conot);
            echo "</pre>";

            /*//print_r($conot);
            //$conot = $this->getEconotUpdt($orderno);
            //print_r($conot);
            //echo $conot->detail[0]->status;*/
            $dtConot = $conot->detail[0]->cnote_no;
            if ($conot != null) {
                //					if($conot->detail[0]->status == 'sukses' && $conot->detail[0]->status != 'Error') { //code bapak
                    if (($conot->detail[0]->status == 'sukses' || $conot->detail[0]->status == 'Error') && $dtConot != null) { //code yg baru
//						echo "test success";
//						$dtConot = $conot->detail[0]->cnote_no;
                        $y = $this->mm_trans->updateConotByOrderNo($orderno, $dtConot);
                        echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                        echo "<br><br>Update Conot Sukses ".$dtConot."<br><br>";
                    } else {
                        //						echo "test gagal";
                        echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                        echo "<br><br>Update Conot Gagal<br><br>";
                    }
            } else {
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>No response from JNE Server<br><br>";
            }
        } elseif ($x[0]->cargo_id == "5") {
        }

        /*}else{
            echo "Update Conot Gagal,Service type tidak ada";
        }*/
    }

    //$route['trans/shipaddr/check/(:any)/(:any)'] = 'backend/transaction/checkShipAddr/$1/$2';
    public function checkShipAddr($orderno, $token)
    {
        $data['orderno'] = $orderno;
        $data['result'] = $this->m_trans->getShipAddrSgo($token);
        //print_r($data['result']);
        $this->load->view($this->folderView.'previewShipAddrSGO', $data);
    }

    //$route['trans/shipaddr/ins/(:any)/(:any)'] = 'backend/transaction/insShipAddrFromSGO/$1/$2';
    public function insShipAddrFromSGO($orderno, $token)
    {
        //$res = $this->s_trans->generateCNoteJNE_baru($token, $orderno);
        $res = $this->s_trans->generate_konot($token, $orderno);
        if ($res > 0) {
            $hasil = array("response" => "true", "message" => "Data shipping successfully updated..");
        } else {
            $hasil = array("response" => "false", "message" => "Data shipping fail to update..");
        }
        echo json_encode($hasil);
    }

    public function helperShowProdByID()
    {
        $productcode = $this->input->post('productcode');
        $prdcdcode = strtoupper($productcode);
        $pricecode = $this->input->post('pricecode');
        $data = $this->m_trans->show_product_price($prdcdcode, $pricecode);
        if ($data != null) {
            $arr = array("response" => "true", "arrayData" => $data);
        } else {
            $arr = array("response" => "false");
        }

        echo json_encode($arr);
    }

    //$route['trans/reinputTRx'] = 'backend/transaction/frmReinputTrx';
    public function frmReinputTrx()
    {
        if ($this->username != null) {
            $data['form_header'] = "Re-input Trx Sales";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/reinputTRx';
            $data['dateNow'] = date('d F Y');
            $data['currentperiod'] = $this->cartshopService->getCurrentPeriod();
            $data['form_action'] = 'trans/reinputTrx/act';
            $this->setTemplate($this->folderView.'getFrmReinputTrx', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    /* -------------------------------------- *
     * TRANSACTION REPORT PENDING  by ananech *
     * -------------------------------------- */

    public function getTransReportPending()
    {
        if ($this->username != null) {
            $data['form_header'] = "Transaction Report Pending";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/report/pending';
            $this->setTemplate($this->folderView.'getTransPending', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    public function postTransReportPending()
    {
        $dt = $this->input->post(null, true);
        $x['results'] = $this->m_trans->getListTrxPending($dt);
        $this->load->view($this->folderView.'transPendingRpt', $x);
    }

    public function getDetPendingTrx($orderno)
    {
        if ($this->username != null) {
            $x['header'] = $this->m_trans->get_TrxPendingHeader($orderno);
            $x['detail'] = $this->m_trans->get_TrxPendingDetail($orderno);
            $x['payment'] = $this->m_trans->get_TrxPendingPayment($orderno);
            $this->load->view($this->folderView . 'trxPendingDet', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }

    public function reconcileTrxCS()
    {
        if ($this->username != null) {
            $data['form_header'] = "SGO Reconcile Trx";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/reconcile/cs';

            $data['bankList'] = $this->s_trans->getListBank();
            $this->setTemplate($this->folderView.'frmReconcileCS', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    /*======tambahan ananech selesei======*/

    public function reconcileTrxManual()
    { //$route['trans/reconcile/trans_manual'] = 'backend/transaction/reconcileTrxManual';
        if ($this->username != null) {
            $data['form_header'] = "Mandiri Manual Reconcile Trx";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/reconcile/trans_manual';

            $data['bankList'] = $this->s_trans->getListBank();
            $this->setTemplate($this->folderView.'frmReconcileManual', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    public function testSess()
    {
        $abc = $this->session->userdata('group_do');
        $ceks = $this->session->userdata('cekKw');
        echo "group name ".getBeGroupname()." - username ".getBeUsername()."<br>";
        print_r($abc);
        echo "<br/>";
        print_r($ceks);
    }


    /*TAMBAHAN HILAL @29/12/2015
     * START
     */
    //$route['print/shipping/updcnote/(:any)'] = 'backend/transaction/postUpdCnoteByOrderno/$1';
    public function postUpdCnoteByOrderno()
    {
        $arr = sessionExpireMessage();
        if ($this->username != null) {
            $data = $this->input->post(null, true);
            $arr = $this->s_trans->postUpdateCnoteByOrderno();
        }
        echo json_encode($arr);
    }


    public function getSearchVcr()
    {
        if ($this->username != null) {
            $data['form_header'] = "Transaction Report";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/searchVcr';
            $this->setTemplate($this->folderView.'getCheckVoucherForm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/report/list'] = 'backend/transaction/postTransactionReport';
    public function postSearchVcr()
    {
        $dt = $this->input->post(null, true);
        $searchby = $dt['searchby'];
        $paramValue = $dt['paramValue'];

        if ($this->username != null) {
            if ($searchby == 'vocuherno' || $searchby == 'ip') {
                $data['result'] = $this->s_trans->getListVoucherByParam($searchby, $paramValue, null, null);
            //echo 'hasilnya adalah === '.$dt['paramValue'];
            } elseif ($searchby == 'stockist') {
                $dtfrom = $dt['trx_from'];
                $dtto   = $dt['trx_to'];
                $data['result'] = $this->s_trans->getListVoucherByParam($searchby, $paramValue, $dtfrom, $dtto);
            }
            $this->load->view($this->folderView.'getCheckVoucherResult', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/id/(:any)'] = 'backend/transaction/getTrxByID/$1';
    public function getSearchVcrByID($id)
    {
        $data['result'] = $this->s_trans->getTrxByID($id);
        echo json_encode($data['result']);
    }

    //$route['trans/conote/track/(:any)'] = 'backend/transaction/getTrackingJNE/$1';
    public function getTrackingJNE($id)
    {
        $arr = $this->s_trans->trackingJneConot($id);
        $ship = json_decode($arr);
        //echo $ship;
        $data['shipping'] = $ship->manifest;
        //print_r($ship);
        $this->load->view($this->folderView.'checkShippingStatus', $data);
    }


    /*-------------------------------
     * TRANSACTION REPORT
     * -----------------------------*/
    //$route['trans/report/kvms'] = 'backend/transaction/getTransactionReportKvms';
    public function getTransactionReportKvms()
    {
        if ($this->username != null) {
            $data['form_header'] = "Transaction Report KVM";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/report/kvms';
            $this->setTemplate($this->folderView.'getTransactionReportKVMS', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/ship/invoice'] = 'backend/transaction/formShippingList';
    public function formShippingList()
    {
        if ($this->username != null) {
            $data['form_header'] = "Shipping List";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/ship/invoice';
            $data['list_shipping'] = $this->m_trans->getListCargo();
            $this->setTemplate($this->folderView.'shippingListForm', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/ship/invoice/list] = 'backend/transaction/getShipInvList';
    public function getShipInvList()
    {
        $data = $this->input->post(null, true);
        $data['result'] = $this->m_trans->getShipInvList($data['ship_from'], $data['ship_to'], $data['list_shipping']);
        $this->load->view($this->folderView.'shippingListResult', $data);
        //print_r($data['result']);
    }

    //$route['trans/ship/invoice/list/excell'] = 'backend/backend_member/getShipInvListExcell';
    public function getShipInvListExcell()
    {
        //$data = $this->input->post(NULL, TRUE);
        //$x['listRecruit'] = $this->mMember->getListRecruit($data);
        $data = $this->input->post(null, true);
        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Content-Disposition: Attachment; filename=listShipInv.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        date_default_timezone_set("Asia/Jakarta");

        $data['result'] = $this->m_trans->getShipInvList($data['ship_from'], $data['ship_to'], $data['list_shipping']);
        $this->load->view($this->folderView.'shippingListResultComplete', $data);
    }

    //$route['trans/ship/invoice/id/(:any)] = 'backend/transaction/getShipInvListByID';
    public function getShipInvListByID($id)
    {
        $data = $this->input->post(null, true);
        $data['result'] = $this->m_trans->getShipInvListByID($id);

        $this->load->view($this->folderView.'shippingListDet', $data);
        //print_r($data['result']);
    }


    //$route['trans/sgo/reconcile/detTrxXL/(:any)'] = 'backend/transaction/getDetTrxXL/$1';

    public function getDetTrxXL($id)
    {
        //get list bank from ecomm_bank for update payment

        $data['result'] = $this->shared->getBankNonVA();

        //get ppob_type first

        $arr = $this->s_trans->getListSGOTrxPpobById($id);
        //print_r($arr);
        foreach ($arr as $r) {
            $statustrx = $arr[0]->status_trx;
            //$statussgo = $arr[0]->Status_sgo;
            $hdr_trx_id = $arr[0]->hdr_trx_id;
            $det_trx_id = $arr[0]->det_trx_id;
            $pay_trx_id = $arr[0]->pay_trx_id;
        }


        if ($statustrx==0 || $statustrx==2 || $statustrx==3) {
            if ($hdr_trx_id==0 || $det_trx_id==0 || $pay_trx_id==0) {

                //get ppob_type first
                $dataHdr = $this->m_trans->getTrxHdr($id);

                foreach ($dataHdr as $r) {
                    $ppob_type = $r->ppob_type;
                }

                if ($ppob_type=='4') {
                    $dataHdr = $this->m_trans->getTrxHdr1($id);
                    $data['hdr']=$dataHdr;
                } elseif ($ppob_type=='3') {
                    $dataHdr = $this->m_trans->getTrxHdr2($id);
                    $data['hdr']=$dataHdr;
                }

                //get reff_pay_id
                $dataDet=$this->m_trans->getTrxDet($id);
                $data['det']=$dataDet;

                $dataPay=$this->m_trans->getTrxPay($id);
                $data['pay']=$dataPay;

                echo json_encode($data);
            }
        } elseif ($statustrx==1) {
            $dataHdr = $this->m_trans->getTrxHdrReal($id);

            foreach ($dataHdr as $r) {
                $ppob_type = $r->ppob_type;
            }

            if ($ppob_type=='4') {
                $dataHdr = $this->m_trans->getTrxHdrReal1($id);
                $data['hdr']=$dataHdr;
            } elseif ($ppob_type=='3') {
                $dataHdr = $this->m_trans->getTrxHdrReal2($id);
                $data['hdr']=$dataHdr;
            }

            //get reff_pay_id
            $dataDet=$this->m_trans->getTrxDetReal($id);
            $data['det']=$dataDet;

            $dataPay=$this->m_trans->getTrxPayReal($id);
            $data['pay']=$dataPay;

            echo json_encode($data);
        }
    }

    public function upDetTrans()
    {
        $id = $this->input->post('trx_id');

        $in = array(
            'trx_id' => $id,
            'cust_no' => $this->input->post('cust_no'),
            'qty' => 1,
            'reff_pay_id' => $this->input->post('reff_pay')
        );

        $up = array(
            //'trx_id' => $id,
            'cust_no' => $this->input->post('cust_no'),
            'qty' => 1,
            'reff_pay_id' => $this->input->post('reff_pay')
        );

        $where = array(
            'trx_id' => $id
        );

        //check first if data have already exist on the table or not
        $cek1 = $this->m_trans->getWhere('ppob_trx_det_tempsgo', 'trx_id', $id);

        if ($cek1->num_rows()==1) {
            $up1 = $this->m_trans->Update('ppob_trx_det_tempsgo', $up, $where);
        } else {
            $insert1 = $this->m_trans->Insert('ppob_trx_det_tempsgo', $in);
        }

        $cek2 = $this->m_trans->getWhere('ppob_trx_det', 'trx_id', $id);

        if ($cek2->num_rows()==1) {
            $up2 = $this->m_trans->Update('ppob_trx_det', $up, $where);
        } else {
            $insert2 = $this->m_trans->Insert('ppob_trx_det', $in);
        }


        if (($up1 || $up2 || $insert1 || $insert2) == true) {
            ?>
			<script>
				alert('Detail Transaction berhasil diupdate');
				redirect('backend', 'refresh');
			</script>

			<?php
        } else {
            ?>
			<script>
				alert('Detail Transaction gagal diupdate');
				redirect('backend', 'refresh');
			</script>

			<?php
        }
    }

    public function upDetPay()
    {
        $id = $this->input->post('trx_id');

        $in = array(
            'trx_id' => $id,
            'bank_code_payment' => $this->input->post('bank_code'),
            'charge_admin' => $this->input->post('charge_adm'),
            'payamt' => $this->input->post('payamt')
        );

        $up = array(
            //'trx_id' => $id,
            'bank_code_payment' => $this->input->post('bank_code'),
            'charge_admin' => $this->input->post('charge_adm'),
            'payamt' => $this->input->post('payamt')
        );

        $where = array(
            'trx_id' => $id
        );


        //check first if data have already exist on the table or not
        $cek1 = $this->m_trans->getWhere('ppob_trx_pay_tempsgo', 'trx_id', $id);

        if ($cek1->num_rows()==1) {
            $up1 = $this->m_trans->Update('ppob_trx_pay_tempsgo', $up, $where);
        } else {
            $insert1 = $this->m_trans->Insert('ppob_trx_pay_tempsgo', $in);
        }

        $cek2 = $this->m_trans->getWhere('ppob_trx_pay', 'trx_id', $id);

        if ($cek2->num_rows()==1) {
            $up2 = $this->m_trans->Update('ppob_trx_pay', $up, $where);
        } else {
            $insert2 = $this->m_trans->Insert('ppob_trx_pay', $in);
        }


        if (($up1 || $up2 || $insert1 || $insert2) == true) {
            ?>
			<script>
				alert('Detail Transaction berhasil diupdate');
				//All.reload_page('backend/transaction/getListSGOTrx/0');
				redirect('backend', 'refresh');

			</script>

			<?php
        } else {
            ?>
			<script>
				alert('Detail Transaction gagal diupdate');
				//All.reload_page('backend/transaction/getListSGOTrx/0');
				redirect('backend', 'refresh');
			</script>

			<?php
        }
    }


    public function getSelectBank()
    {
        $type_bank = $this->input->post('type_bank');

        $return = $this->m_trans->getSelectBank($type_bank)->row();
        $data['charge_adm']=$return->charge_admin;
        $data['bank_code']=$return->id;
        $data['bank_name']=$return->bankDisplayNm;

        if ('IS_AJAX') {
            echo json_encode($data);
        }
    }


    public function null_shipping()
    {
        if ($this->username != null) {
            $data['form_header'] = "Update Data Null Shipping";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'trans/null_shipping';

            //$personal_info = $this->session->userdata('personal_info');
            //echo $personal_info;
            $data['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
            $dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
            $this->setTemplate($this->folderView.'frmNullShipping', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    public function cekOrderNo($orderno)
    {
        //$orderno = $this->input->post('orderno');

        $valReturn  = $this->m_trans->cekOrderNo($orderno);
        $returnArr = array("response" => "true", "arrayData" => $valReturn);
        echo json_encode($returnArr);
    }

    public function cekTransShipAddrSgo($orderno)
    {
        $valReturn  = $this->m_trans->cekTransShipAddrSgo($orderno);
        $returnArr = array("response" => "true", "arrayData" => $valReturn);
        echo json_encode($returnArr);
    }

    public function cekTransShipAddr($orderno)
    {
        $valReturn  = $this->m_trans->cekTransShipAddr($orderno);
        $returnArr = array("response" => "true", "arrayData" => $valReturn);
        echo json_encode($returnArr);
    }

    public function InsertNullShipping()
    {
        $data = $this->input->post(null, true);
        $orderno = $data['orderno'];

        //get total_item and total_weight
        $get1 = $this->m_trans->getTotalWeight($orderno);

        foreach ($get1 as $r) {
            $total_weight = $r->total_weight;
            $total_item = $r->total_item;
        }

        $this->load->library('delivery_cargo');
        $cargo = new Delivery_cargo();
        $cargo->setTipeCargo($data['shipper'])
              ->setFrom($data['sender_address'])
              ->setTo($data['destination_address'])
              ->setWeight($total_weight);

        $harga = $cargo->getPriceCargo();
        $arr = json_decode($harga);
        //print_r($arr);

        if (isset($arr->price)) {
            $hasilHarga = $this->cartshopService->setLowestJnePrice($arr);
            $service_code = $hasilHarga['service_code'];
            $service_display = $hasilHarga['service_display'];
            $price = $hasilHarga['price'];

            $ss = $hasilHarga;

            //get total_pay_net
            $get2 = $this->m_trans->getTotalPay($orderno);

            foreach ($get2 as $d) {
                $total_pay_net = $d->total_pay;
            }

            if ($data['shipper'] == '1') {
                //shipper 1 == JNE

                $in1 = array(
                    "orderno" => $orderno,
                    "idstk" => $data['stockistref'],
                    "prov_code" => $data['provinsi'],
                    "kab_code" => $data['kota'],
                    "kec_code" => $data['kecamatan'],
                    //"kel_code" => $data[''],
                    "addr1" => $data['alamat'],
                    "email" => $data['email'],
                    "tel_hp1" => $data['telp'],
                    "total_item" => $total_item,
                    "total_weight" => $total_weight,
                    "total_pay_net" => $total_pay_net,
                    "receiver_name" => $data['namapenerima'],
                    "stockist_name" => $data['nama_stockistref'],
                    "kabupaten_name" => $data['nama_kota'],
                    "province_name" => $data['nama_provinsi'],
                    "sender_address" => $data['sender_address'],
                    "dest_address" => $data['destination_address'],
                    "jne_branch" => $data['jne_branch'],
                    "shipper_telhp" => $data['telp'],
                    "cargo_id" => $data['shipper'],
                    "service_type_id" => $service_code,
                    "service_type_name" => $service_display
                );

                $up_shipaddr = array(
                    //"orderno" => $orderno,
                    "idstk" => $data['stockistref'],
                    "prov_code" => $data['provinsi'],
                    "kab_code" => $data['kota'],
                    "kec_code" => $data['kecamatan'],
                    //"kel_code" => $data[''],
                    "addr1" => $data['alamat'],
                    "email" => $data['email'],
                    "tel_hp1" => $data['telp'],
                    "total_item" => $total_item,
                    "total_weight" => $total_weight,
                    "total_pay_net" => $total_pay_net,
                    "receiver_name" => $data['namapenerima'],
                    "stockist_name" => $data['nama_stockistref'],
                    "kabupaten_name" => $data['nama_kota'],
                    "province_name" => $data['nama_provinsi'],
                    "sender_address" => $data['sender_address'],
                    "dest_address" => $data['destination_address'],
                    "jne_branch" => $data['jne_branch'],
                    "shipper_telhp" => $data['telp'],
                    "cargo_id" => $data['shipper'],
                    "service_type_id" => $service_code,
                    "service_type_name" => $service_display
                );


                //update payShip di table ecomm_trans_hdr_sgo
                $up1 = array(
                    "payShip" => $price
                );

                //check first orderno have already exist in ecomm_trans_hdr_sgo
                $check = $this->m_trans->getWhere('ecomm_trans_hdr_sgo', 'orderno', $orderno);

                if ($check->num_rows() > 0) {

                    //check first orderno have already exist in ecomm_trans_shipaddr_sgo
                    $shipaddr = $this->m_trans->getWhere('ecomm_trans_shipaddr_sgo', 'orderno', $orderno);

                    if ($shipaddr->num_rows() > 0) {

                        //update payShip to ecomm_trans_hdr_sgo
                        $this->db->update('ecomm_trans_hdr_sgo', $up1, array('orderno' => $orderno));

                        //and update data to ecomm_trans_shipaddr_sgo
                        $res = $this->db->update('ecomm_trans_shipaddr_sgo', $up_shipaddr, array('orderno' => $orderno));

                        if ($res > 0) {
                            echo json_encode($res);
                            echo "Data berhasil diupdate";
                            redirect('backend/transaction/null_shipping', 'refresh');
                        } else {
                            echo "Data gagal diupdate";
                            redirect('backend/transaction/null_shipping', 'refresh');
                        }
                    } else {

                        //update payShip to ecomm_trans_hdr_sgo
                        $this->db->update('ecomm_trans_hdr_sgo', $up1, array('orderno' => $orderno));

                        //insert data to ecomm_trans_shipaddr_sgo
                        $res = $this->db->insert('ecomm_trans_shipaddr_sgo', $in1);

                        if ($res > 0) {
                            echo json_encode($res);
                            echo "Data berhasil diupdate";
                            echo "<br>";
                            echo "Ongkir = ".$up1[0]['payShip'];
                            redirect('backend/transaction/null_shipping', 'refresh');
                        } else {
                            echo "Data gagal diupdate";
                            redirect('backend/transaction/null_shipping', 'refresh');
                        }
                    }
                } else {
                    echo setErrorMessage("Data $orderno tidak ditemukan.");
                }
            } elseif ($data['shipper'] == '2') {
            }
        } else {
            echo setErrorMessage("Harga tidak ditemukan...");
            //print_r($arr);
        }
        /*
         */
        /*else{
            //shipper == KGP
            $in2 = array(
                "orderno" => $data['orderno'],
                "idstk" => $data['stockistref'],
                "prov_code" => $data['provinsi'],
                "kab_code" => $data['kota'],
                //"kec_code" => $data['kecamatan'],
                //"kel_code" => $data[''],
                "addr1" => $data['alamat'],
                "email" => $data['email'],
                "tel_hp1" => $data['telp'],
                "total_item" => $total_item,
                "total_weight" => $total_weight,
                "total_pay_net" => $total_pay_net,
                "receiver_name" => $data['namapenerima'],
                "stockist_name" => $data['nama_stockistr1ref'],
                "kabupaten_name" => $data['nama_kota'],
                "province_name" => $data['nama_provinsi'],
                //"sender_address" => $data['sender_address'],
                "dest_address" => $data['destination_address'], //KotaKode
                //"jne_branch" => $data['jne_branch'],
                "shipper_telhp" => $data['telp'],
                "cargo_id" => $data['shipper']
            );

            //$table = 'ecomm_trans_shipaddr_sgo';
            $res = $this->db->insert($table, $in2);
            return $res;


        }*/
    }

    public function checkNmStockist($id)
    {
        $data['fullnm'] = $this->m_trans->checkNmStockist($id);

        echo json_encode($data);
    }

    public function InsertTrxPending()
    {
        $data = $this->input->post(null, true);

        $orderno = $data['orderno'];
        $idstk = $data['idstk'];
        $nmstk = $data['nmstk'];

        $arr = array(
            'idstk' => $idstk,
            'nmstkk' => $idstk.'-'.$nmstk
        );

        $update = $this->db->update('ecomm_trans_hdr_sgo', $arr, array('orderno' => $orderno));

        $arr2 = array(
            'stk_code' => $idstk
        );

        $update2 = $this->db->update('ecomm_memb_ok_sgo', $arr2, array('trx_no' => $orderno));


        if ($update && $update2) {
            ?>
			<script>
				alert('Data Berhasil Diupdate');

			</script>
			<?php
            redirect('backend', 'refresh');
        } else {
            ?>
			<script>
				alert('Data Gagal Diupdate');
			</script>

			<?php
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/knet/bv'] = 'backend/transaction/formKnetBvUpdate';
    public function formKnetBvUpdate()
    {
        if ($this->username != null) {
            $data['form_header'] = "Update BV K-NET";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/knet/bv';
            $this->setTemplate($this->folderView.'knet_bv/formKnetBvUpdate', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/knet/bv/info'] = 'backend/transaction/detailBVInfo';
    public function detailBVInfo()
    {
        $data = $this->input->post(null, true);
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    //$route['trans/knet/bv/update'] = 'backend/transaction/updateBvKnet';
    public function updateBvKnet()
    {
    }

    /*
     * END TAMBAHAN HILAL
     */

    //$route['trans/updPrd/(:any)'] = 'backend/transaction/updateProdukSales/$1';
    public function updateProdukSales($orderno)
    {
        $data['hasil'] = $this->m_trans->checkDataProdukKnet($orderno);
        $this->load->view($this->folderView.'updateDataProduk', $data);
    }
    //$route['trans/updPrd/upd/save'] = 'backend/transaction/updateProdukSalesSave';
    public function updateProdukSalesSave()
    {
        $data = $this->input->post(null, true);

        $updPrd = $this->m_trans->updateEcommTransDetPrd($data);
        echo json_encode($updPrd);

        //echo "<pre>";
        //print_r($data);
        //echo "</pre>";
    }

    /*-------------------------------
 * SP MODULE BY VERA
 * -----------------------------*/
    //$route['trans/print/cn'] = 'backend/transaction/getPrintCN';
    public function getPrintSP()
    {
        if ($this->username != null) {
            $data['form_header'] = "Print Sales Product Summary";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/report/product_sales';
            $data['printtxt'] = base_url('trans/report/product_sales/txt');
            $this->setTemplate($this->folderView.'getPrintSP', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/print/cn/list'] = 'backend/transaction/postPrintCN';
    //$route['trans/report/product_sales/list'] = 'backend/transaction/postPrintSP';
    public function postPrintSP()
    {
        $form = $this->input->post(null, true);
        if ($form['statusKirim'] == "1") {
            $data['result'] = $this->s_trans->getListSP();
        } else {
            $data['result'] = $this->s_trans->getListSPBlmKirim();
        }
        echo json_encode($data['result']);
    }

    //$route['trans/reprint/cn'] = 'backend/transaction/getReprintCN';
    public function getReprintSP()
    {
        if ($this->username != null) {
            $data['form_header'] = "Reprint Sales Product";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'trans/rerint/cn';
            $data['printtxt'] = base_url('trans/report/product_sales/txt');
            $this->setTemplate($this->folderView.'getRePrintSP', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    //$route['trans/print/cn/txt'] = 'backend/transaction/printCNToTxt';
    public function printSPToTxt()
    {
        $data = $this->input->post(null, true);
        $data['dtfrom'] = date("d/m/Y", strtotime($data['cn_from']));
        $data['dtto'] = date("d/m/Y", strtotime($data['cn_to']));

        $data['printSP'] = $this->s_trans->getSPReportToPrint();
        //print_r($data['printCN']);
        $this->load->view($this->folderView.'printSPToXLS', $data);
    }

    public function get_ongkir()
    {
        $stk = $this->input->post('stk');
        $orderno = $this->input->post('orderno');
        $sender = $this->input->post('sender');
        $dest = $this->input->post('destination');
        $shipper = $this->input->post('shipper');

        //get total_item and total_weight
        $get1 = $this->m_trans->getTotalWeight($orderno);

        foreach ($get1 as $r) {
            $total_weight = $r->total_weight;
            $total_item = $r->total_item;
        }

        $this->load->library('delivery_cargo');
        $cargo = new Delivery_cargo();
        $cargo->setTipeCargo($shipper)
             ->setFrom($sender)
             ->setTo($dest)
             ->setWeight($total_weight);

        $harga = $cargo->getPriceCargo();
        $arr = json_decode($harga);
        //		 print_r($arr);

        if (isset($arr->price)) {
            $hasilHarga = $this->cartshopService->setLowestJnePrice($arr);
            $data['ongkir'] = $hasilHarga['price'];
        } else {
            $data['ongkir'] = "Harga Tidak Ditemukan";
        }

        if ('IS_AJAX') {
            echo json_encode($data);
        }
    }

    public function updateHdrSgo()
    {
        $data = $this->input->post(null, true);

        //check data have already exist in ecomm_trans_hdr
        $cek = $this->m_trans->cekTransHdr($data['token']);

        if ($cek == null) {
            $arr = array(
                'id_memb' => $data['idmemb'],
                'nmmember' => $data['nmmemb'],
                'idstk' => $data['idstk'],
                'nmstkk' => $data['idstk']." - ".$data['nmstk'],
                'sentTo' => $data['sent_to'],
                'discount_shipping' => $data['disc'],
                'payShip' => 0,
                'bonusmonth' => $data['bonus']
            );

            $up = $this->db->update('ecomm_trans_hdr_sgo', $arr, array('token' => $data['token']));

            if ($up > 0) {
                $message = "Data Berhasil Disimpan";
                $hasil = jsonTrueResponse($arr, $message);
                echo json_encode($hasil);
            } else {
                $message = "Data Gagal Disimpan";
                $hasil = jsonFalseResponse($message);
                echo json_encode($hasil);
            }
        } else {
            $message = "Data Tidak Valid/Sudah Direconsile";
            $hasil = jsonTrueResponse($cek, $message);
            echo json_encode($hasil);
        }
    }

    public function RebookEConotG($orderno, $tipe, $token)
    {
        $x = $this->m_trans->getRebookConot($orderno);
        echo $orderno;
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Be_trans_klink extends MY_Controller {
  function __construct() {
    parent::__construct();
    $this->folderView = "backend/transaction_mgm/";
    $this->folderViewSMS = "backend/sms_broadcast/";
    $this->load->service('backend/be_mgm/be_trans_klink_service', 's_ktrans');
    $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
    //$this->load->model('webshop/ecomm_trans_model', 'mm_trans');
  }

  public function getSearchTrx(){
    if($this->username != null) {
      $data['form_header'] = "Distributor Transaction Check";
      $data['icon'] = "icon-list";
      $data['form_reload'] = 'transklink/searchTrx';
      $this->setTemplate($this->folderView.'getCheckDistTrx', $data);
    } else {
      redirect('backend', 'refresh');
    }
  }

  //$route['transklink/searchTrx/list'] = 'backend/be_trans_klink/postSearchTrx';
  function postSearchTrx() {
    $dt = $this->input->post(NULL, TRUE);
    $searchby = $dt['searchby']; //dfno, cnno, batchno, trcd, orderno
    $paramValue = $dt['paramValue'];
    $chk = $dt['chk']; //if checked then no date param, not checked with date param

     if($this->username != null && $paramValue != null && $paramValue !="") {
      $param = null;
      $bnsmonth = null;
      $bnsyear = null;
      $from = null;
      $to = null;

       if($searchby == 'cnno'|| $searchby == 'batchno' || $searchby == 'trcd' || $searchby == 'orderno'){
         $param = $paramValue;
      }elseif($searchby == 'dfno'){
        $bnsmonth   = $dt['bnsmonth'];
        $bnsyear   = $dt['bnsyear'];
        $param = $paramValue;

        if($chk == 0){
          $from = $dt['from'];
          $to = $dt['to'];
        }
      }
      $data['result'] = $this->s_ktrans->getListTrxByParam($searchby, $param, $bnsmonth, $bnsyear, $from, $to);
      $this->load->view($this->folderView.'getCheckDistTrxResult', $data);

    }else{
      redirect('backend', 'refresh');
    }
  }

  function getDetailProductByID($trcd){
    if($this->username != null) {
      $data['result'] = $this->s_ktrans->getTrxByTrcd("trcd", $trcd);
      if($data['result'] == null) {
        echo "false";
      }else{
        //var_dump($data);
        $this->load->view($this->folderView.'getCheckDetTrxByID', $data);
      }
    } else {
      redirect('backend', 'refresh');
    }
  }

  public function getStockist($loccd){
    if($this->username != null) {
      if($loccd != null || $loccd != ''){
        $data['result'] = $this->s_ktrans->getStockist($loccd);
        if($data['result'] != null){
          //var_dump($data['result']);
          $arr = jsonTrueResponse($data['result']);
          //echo json_encode($data['result']);
        }else{
          $arr = jsonFalseResponse("Data not found.");
        }
      }else{
        $arr = jsonFalseResponse("Stockist code cannot be empty.");
      }
      echo json_encode($arr);
    } else {
      redirect('backend', 'refresh');
    }
  }


  public function getRecapSales(){
    if($this->username != null) {
      $data['form_header'] = "Distributor Transaction Check";
      $data['icon'] = "icon-list";
      $data['form_reload'] = 'transklink/recapSales';
      $this->setTemplate($this->folderView.'getSalesRecap', $data);
    } else {
      redirect('backend', 'refresh');
    }
  }

  //$route['transklink/searchTrx/list'] = 'backend/be_trans_klink/postSearchTrx';
  function postRecapSales($is_xls=0) {
    $dt = $this->input->post(NULL, TRUE);
    $param = $dt['sc_code'];
    $bnsmonth   = $dt['bnsmonth'];
    $bnsyear    = $dt['bnsyear'];
    //echo "controller $bnsmonth";
    if($this->username != null) {
      $data['is_xls'] = $is_xls;
      $data['result'] = $this->s_ktrans->getListRecapSales($param, $bnsmonth, $bnsyear);
      $this->load->view($this->folderView.'getSalesRecapResult', $data);
    }else{
      redirect('backend', 'refresh');
    }
  }

  //$route['transklink/recapBonus'] = 'backend/be_trans_klink/getRecapBonus';
  public function getRecapBonus(){
    if($this->username != null) {
      $data['form_header'] = "Distributor Transaction Check";
      $data['icon'] = "icon-list";
      $data['form_reload'] = 'transklink/recapBonus';
      $this->setTemplate($this->folderView.'getBonusRecap', $data);
    } else {
      redirect('backend', 'refresh');
    }
  }

  //$route['transklink/recapBonus/list/(:any)'] = 'backend/be_trans_klink/postRecapBonus/$1';
  function postRecapBonus($is_xls=0) {
    $dt = $this->input->post(NULL, TRUE);
    //print_r($dt);
    $param = $dt['country_cd'];
    $bnsmonth   = $dt['bnsmonth'];
    $bnsyear    = $dt['bnsyear'];
    $rptType	= $dt['rpt_type'];

     if($this->username != null) {
       $data['is_xls'] = $is_xls;
      $data['rptType']= $dt['rpt_type'];
      $data['result'] = $this->s_ktrans->getListRecapBonus($param, $rptType, $bnsmonth, $bnsyear);
      if($rptType == "cvr" or $rptType == "pvr"){
        $this->load->view($this->folderView.'getBonusRecapCVRRes', $data);
      }elseif($rptType == "allvr" or $rptType == "chq" or $rptType == "chq_stk" or $rptType == "novac"){
        $this->load->view($this->folderView.'getBonusRecapAllVRRes', $data);
      }

    }else{
      redirect('backend', 'refresh');
    }
  }


  //$route['transklink/gnvUpload'] = 'backend/be_trans_klink/gnvUpload';
  public function gnvUpload() {
    if($this->username != null) {
       $this->load->library('csvreader');
           $data['form_header'] = "Upload Stock GNV From File";
           $data['icon'] = "icon-pencil";
       $data['form_reload'] = 'transklink/gnvUpload';
       $data['listWH'] = $this->s_ktrans->listWHFromGNV();
           $this->setTemplate($this->folderView.'gnvImportForm', $data);
        } else {
           redirect('backend', 'refresh');
        }
  }

  //$route['transklink/gnvUploadPrev'] = 'backend/be_trans_klink/gnvUploadPrev';
    public function gnvUploadPrev() {

        $this->load->library('PHPExcel/PHPExcel');
    if (!empty($_FILES['myfile']['name'])) {

          $fileName = $_FILES["myfile"]["tmp_name"];

      if($fileName != ""){
        $pathinfo = pathinfo($_FILES['myfile']['name']);
              //membuat objek PHPExcel
               $xl_obj = new PHPExcel();

        $extensionType = null;
          if (isset($pathinfo['extension'])) {
              switch (strtolower($pathinfo['extension'])) {
                  case 'xlsx':            //    Excel (OfficeOpenXML) Spreadsheet
                  case 'xlsm':            //    Excel (OfficeOpenXML) Macro Spreadsheet (macros will be discarded)
                  case 'xltx':            //    Excel (OfficeOpenXML) Template
                  case 'xltm':            //    Excel (OfficeOpenXML) Macro Template (macros will be discarded)
                      $extensionType = 'Excel2007';
                      break;
                  case 'xls':                //    Excel (BIFF) Spreadsheet
                  case 'xlt':                //    Excel (BIFF) Template
                      $extensionType = 'Excel5';
                      break;
                  case 'ods':                //    Open/Libre Offic Calc
                  case 'ots':                //    Open/Libre Offic Calc Template
                      $extensionType = 'OOCalc';
                      break;
                  case 'slk':
                      $extensionType = 'SYLK';
                      break;
                  case 'xml':                //    Excel 2003 SpreadSheetML
                      $extensionType = 'Excel2003XML';
                      break;
                  case 'gnumeric':
                      $extensionType = 'Gnumeric';
                      break;
                  case 'htm':
                  case 'html':
                      $extensionType = 'HTML';
                      break;
                  case 'csv':
                      // Do nothing
                      // We must not try to use CSV reader since it loads
                      // all files including Excel files etc.
                      break;
                  default:
                      break;
                }
              }

        $reader = PHPExcel_IOFactory::createReader($extensionType);
        $reader->setReadDataOnly(true);
        $file = $reader->load($fileName);
        $objWorksheet = $file->getActiveSheet();


        $isheet=0;
          $irow=1;
          $icol='A';

        $sheet = $file->getSheet(intval($isheet));
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

        //echo "highest row = $highestRow.<br/>highest column = $highestColumn.<br/>";
         $valueData = null;
         $data['result'] = array();
            for ($row = intval($irow); $row <= $highestRow; $row++){
                //  Read a row of data into an array
                $valueData = $sheet->rangeToArray($icol . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
          array_push($data['result'], $valueData);
          //var_dump($data);
            }
        $this->load->view($this->folderView.'gnvImportPreview', $data);
      }

      /*
      echo '<table border=1>' . "\n";
      $no = 0;
      foreach ($objWorksheet->getRowIterator() as $row) {
        //var_dump($row);
        echo '<tr>' . "\n";
        $cellIterator = $row->getCellIterator();
        // This loops all cells, even if it is not set.
        // By default, only cells that are set will be iterated.
        $cellIterator->setIterateOnlyExistingCells(false);
        foreach ($cellIterator as $cell) {
          echo '<td>' . $cell->getValue() . '</td>' . "\n";
        }
        echo '</tr>' . "\n";
      }
      echo '</table>' . "\n";
      //var_dump($file);
       *
       */
    }else{
      echo "No Data";
    }
        //var_dump($_POST);

    }

  //$route['transklink/gnvUploadAction'] = 'backend/be_trans_klink/gnvUploadAction';
  public function gnvUploadAction() {
    $dt = $this->input->post(NULL, TRUE);

    $whcd = $dt['whcd'];

        $this->load->library('PHPExcel/PHPExcel');
    if (!empty($_FILES['myfile']['name'])) {

          $this->s_ktrans->delDataStockAwal($whcd);

          $fileName = $_FILES["myfile"]["tmp_name"];

      if($fileName != ""){
        $pathinfo = pathinfo($_FILES['myfile']['name']);
              //membuat objek PHPExcel
               $xl_obj = new PHPExcel();

        $extensionType = null;
          if (isset($pathinfo['extension'])) {
              switch (strtolower($pathinfo['extension'])) {
                  case 'xlsx':            //    Excel (OfficeOpenXML) Spreadsheet
                  case 'xlsm':            //    Excel (OfficeOpenXML) Macro Spreadsheet (macros will be discarded)
                  case 'xltx':            //    Excel (OfficeOpenXML) Template
                  case 'xltm':            //    Excel (OfficeOpenXML) Macro Template (macros will be discarded)
                      $extensionType = 'Excel2007';
                      break;
                  case 'xls':                //    Excel (BIFF) Spreadsheet
                  case 'xlt':                //    Excel (BIFF) Template
                      $extensionType = 'Excel5';
                      break;
                  case 'ods':                //    Open/Libre Offic Calc
                  case 'ots':                //    Open/Libre Offic Calc Template
                      $extensionType = 'OOCalc';
                      break;
                  case 'slk':
                      $extensionType = 'SYLK';
                      break;
                  case 'xml':                //    Excel 2003 SpreadSheetML
                      $extensionType = 'Excel2003XML';
                      break;
                  case 'gnumeric':
                      $extensionType = 'Gnumeric';
                      break;
                  case 'htm':
                  case 'html':
                      $extensionType = 'HTML';
                      break;
                  case 'csv':
                      // Do nothing
                      // We must not try to use CSV reader since it loads
                      // all files including Excel files etc.
                      break;
                  default:
                      break;
                }
              }

        $reader = PHPExcel_IOFactory::createReader($extensionType);
        $reader->setReadDataOnly(true);
        $file = $reader->load($fileName);
        $objWorksheet = $file->getActiveSheet();

        $isheet=0;
          $irow=1;
          $icol='A';

        $sheet = $file->getSheet(intval($isheet));
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

        //echo "highest row = $highestRow.<br/>highest column = $highestColumn.<br/>";
        $valueData = null;
        $data['xlsData'] = array();
            for ($row = intval($irow); $row <= $highestRow; $row++){
                //  Read a row of data into an array
                $valueData = $sheet->rangeToArray($icol . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
          array_push($data['xlsData'], $valueData);
            }
            $save = $this->s_ktrans->saveImportToDB($data, $whcd);
        $this->load->view($this->folderView.'gnvImportResult', $save);
      }


      /*
      echo '<table border=1>' . "\n";
      $no = 0;
      foreach ($objWorksheet->getRowIterator() as $row) {
        //var_dump($row);
        echo '<tr>' . "\n";
        $cellIterator = $row->getCellIterator();
        // This loops all cells, even if it is not set.
        // By default, only cells that are set will be iterated.
        $cellIterator->setIterateOnlyExistingCells(false);
        foreach ($cellIterator as $cell) {
          echo '<td>' . $cell->getValue() . '</td>' . "\n";
        }
        echo '</tr>' . "\n";
      }
      echo '</table>' . "\n";
      //var_dump($file);
       *
       */
    }else{
      echo "No Data Uploaded!";
    }

  }


  //$route['transklink/setBnsPeriod'] = 'backend/be_trans_klink/setBnsPeriod';
  public function setBnsPeriod() {
    if($this->username != null) {
           $data['form_header'] = "Set Bonus Period";
           $data['icon'] = "icon-pencil";
       $data['form_reload'] = 'transklink/setBnsPeriod';
       $data['scoPeriod'] = $this->s_ktrans->listBnsPeriod();
       $data['bns_status'] = $this->s_ktrans->showSysPref();
           $this->setTemplate($this->folderView.'setBonusPeriodForm', $data);
        } else {
           redirect('backend', 'refresh');
        }
  }

  //$route['transklink/setBnsPeriodAction'] = 'backend/be_trans_klink/setBnsPeriodAction';
  public function setBnsPeriodAction($param) {
    if($this->username != null) {
       $dt = $this->input->post(NULL, TRUE);
       //var_dump($dt);
       if(strlen($dt['bnsyear']) >= 4 && $dt['bnsmonth']!=""){
            $date = '01';
         $bnsmonth = $dt['bnsmonth'];
         $bnsyear = $dt['bnsyear'];
         $period = "$bnsyear-$bnsmonth-$date";
         $this->s_ktrans->uptBnsPeriod($period, $param);
      }
      //$this->setBnsPeriod();
        } else {
           redirect('backend', 'refresh');
        }
  }

  //$route['transklink/operatorSms'] = 'backend/be_trans_klink/getOperatorSms';
  public function getOperatorSms(){
    if($this->username != null) {
      $data['form_header'] = "Operator Setup";
            $data['icon'] = "icon-list";
        $data['form_reload'] = 'transklink/operatorSms';
            $this->setTemplate($this->folderViewSMS.'getOperatorSMS', $data);
    } else {
      redirect('backend', 'refresh');
    }
  }

  //$route['transklink/operatorSms/list'] = 'backend/be_trans_klink/getListAllOperator';
  function getListAllOperator($is_xls = '0') {
    $dt = $this->input->post(NULL, TRUE);
     if($this->username != null) {
       $data['result'] = $this->s_ktrans->getOperatorSMS();
      if($is_xls == 0){
        $this->load->view($this->folderViewSMS.'getOperatorSMSResult', $data);
      }
    }else{
      redirect('backend', 'refresh');
    }
  }

  //$route['transklink/recover/vc/ip'] = 'backend/be_trans_klink/recoverIncPayVc';
  function recoverIncPayVc() {
    if($this->username != null) {
      $data['form_header'] = "Recover/Update Transaction";
            $data['icon'] = "icon-pencil";
        $data['form_reload'] = 'transklink/recover/vc/ip';
            $this->setTemplate($this->folderView.'recoverIncPayVc', $data);
    } else {
      redirect('backend', 'refresh');
    }
  }
  //$route['transklink/recover/vc/ip/detail'] = 'backend/be_trans_klink/getDetailIncPayVc';
  function getDetailIncPayVc() {
    $data = $this->input->post(NULL, TRUE);
    if($data['trxtype'] == "ssr" || $data['trxtype'] == "csno") {
        if($data['trxtype'] == "ssr") {
        $field = "a.batchno";
      } else {
        $field = "a.csno";
      }
      $ssrno = $data['trxno'];
      $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
      $data['ssr'] = $this->m_trans->getSummaryTrxBySSR($field, $ssrno);
      $this->load->view($this->folderView.'recoverSSRNo', $data);
    } else if($data['trxtype'] == "trcd" || $data['trxtype'] == "orderno") {
      $data['result'] = $this->s_ktrans->getTrxByTrcd($data['trxtype'], $data['trxno']);
      if($data['result'] == null) {
        echo setErrorMessage();
      }else{
        $this->load->view($this->folderView.'getCheckDetTrxByIDforUpd', $data);
      }
    } else if($data['trxtype'] == "vcip") {
      $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
      $data['result'] = $this->m_trans->getVcipDetail($data['trxno']);
      if ($data['result'] == NULL) {
        echo setErrorMessage();
      } else {
        $this->load->view($this->folderView. 'getDetailVcip', $data);
      }
    } else if($data['trxtype'] == "vcd") {
      $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
      $data['result'] = $this->m_trans->getDepositVchInfo($data['trxno']);
      if($data['result']['deposit_header'] == null) {
        echo setErrorMessage();
      }else{
        $this->load->view($this->folderView.'recoverByDepositVch', $data);
      }
    } else {

    }
  }

/**
 * @Author: Ricky
 * @Date: 2019-03-21 16:05:04
 * @Desc: Recover Trx updating data
 * $route['transklink/recover/update'] = 'backend/be_trans_klink/updateRecoverTrx';
 */
  function updateRecoverTrx() {
    $data = $this->input->post(NULL, TRUE);
    $res = $this->m_trans->updateRecoverTrx($data['loccd'], $data['sc_co'], $data['sc_dfno'], $data['bnsperiod'], $data['trcd']);
    if($res['response'] == "true") {
			$ret = jsonTrueResponse($res['message']);
    } else {
			$ret = jsonFalseResponse("Gagal memperbarui data...");
		}
		echo json_encode($ret);
	}

/**
 * @Author: Ricky
 * @Date: 2019-05-07 10:06:09
 * @Desc: Remove Trx
 * $route['transklink/recover/remove'] = 'backend/be_trans_klink/removeTrx';
 */
	function removeTrx() {
		$data = $this->input->post(NULL, TRUE);
		$res = $this->m_trans->deleteTrx($data['trcd']);
		if ($res['response'] == 'true') {
			$ret = jsonTrueResponse($res['message']);
		} else {
			$ret = jsonFalseResponse("Gagal menghapus data...");
		}
		echo json_encode($ret);
	}

  // $route['transklink/recover/checkssr'] = 'backend/be_trans_klink/checkSSR'
  function checkSSR() {
    $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
    $data = $this->input->post();
    $check = $this->m_trans->checkingSSR($data['ssr']);
    if(empty($check)) {
      $return = $this->m_trans->recoverVoucher($data['trxno'], $data['ipsvc']);
    } else {
      $return = jsonFalseResponse("SSR belum di recover");
    }
    echo json_encode($return);
  }

  //$route['transklink/list/(:any)/(:any)'] = 'backend/be_trans_klink/listTTP/$1/$2';
  function listTTP($field, $value) {
    $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
    $data['result'] = $this->m_trans->listTtpById($field, $value);
    $this->load->view($this->folderView.'listTTP', $data);
  }
  //$route['transklink/recover/vc/ip/save'] = 'backend/be_trans_klink/saveRecoverIncPayVc';
  function saveRecoverIncPayVc() {

  }

  //$route['transklink/recover/ssr'] = 'backend/be_trans_klink/recoverSSR';
  public function recoverSSR() {
    $data = $this->input->post(NULL, TRUE);
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    $this->load->model('backend/be_mgm/be_trans_klink_model', 'm_trans');
    $res = $this->m_trans->recoverSSR($data);
    if($res['response'] == "true") {
        echo setSuccessMessage($res['message']);
      //$data['ssr'] = $this->m_trans->getSummaryTrxBySSR($data['batchno']);
      //$this->load->view($this->folderView.'recoverSSRNo', $data);
    } else {
      echo setErrorMessage($res['message']);
    }
  }
}
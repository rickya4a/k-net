<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_voucher extends MY_Controller {
		public function __construct() {
			// Call the Model constructor
	        parent::__construct();
			$this->folderView = "backend/voucher/";
		    $this->load->model("backend/be_voucher_model", 'voucher');
		}
		
		//$route['shop/vch/create'] = 'backend/shopping_voucher/shoppingVoucherForm';
		public function shoppingVoucherForm() {
			if ($this->username != null) {
				$x['form_header'] = 'Create Shopping Voucher';
				$x['icon'] = 'icon-pencil';
				$x['form_reload'] = 'shop/vch/create';
				$this->setTemplate($this->folderView . 'shoppingVoucherForm', $x);
			} else {
				redirect('backend', 'refresh');
			}
			//echo "sds";
		}
		
		//$route['shop/vch/create/preview'] = 'backend/shopping_voucher/shoppingVoucherPreview';
		public function shoppingVoucherPreview() {
			$data = $this->input->post(NULL, TRUE);
			$max = $data['jml_voucher'];
			echo "<form id='saveShopVch'><table align=center width=80% class='table table-bordered table-striped datatable'>";
			echo "<tr><th colspan=5>List Voucher</th></tr>";
			echo "<tr><td colspan=2>Nominal</td><td colspan=3>".$data['nominal']."<input type=hidden name=vch_nominal value=\"$data[nominal]\" /></td></tr>";
			echo "<tr><td colspan=2>Tgl Expire</td><td colspan=3>".$data['expiredt']."<input type=hidden name=vch_expire value=\"$data[expiredt]\"  /><input type=hidden name=prefix value=\"$data[prefix]\"  /><input type=hidden name=max_char value=\"$data[max_char]\"  /></td></tr>";
			echo "<tr><th width='3%'>No</th><th width='20%'>Voucher No</th><th width='10%'>Voucher Key</th><th>ID Member</th><th width='40%'>Nama Member</th></tr>";
			for($i=0;$i<$max;$i++) {
				$jum = $i+1;
				echo "<tr>";
				//$x[$i]['voucher'] = createRandomNumber($data['prefix'], $data['max_char']);
				//$x[$i]['key'] = createRandomNumber("", 4);
				$voucher=$data['prefix']."".$this->uuid->v4v2( $data['max_char'] - strlen($data['prefix']));
				$x[$i]['voucher'] = strtoupper ($voucher) ;
				$x[$i]['key'] = createRandomNumber("", 4);
				$var='All.getFullNameByID(this.value, "be/member/helper/idInfo", #idmember'.$i.')';
				$idm = "'#namamember$i'";
				echo "<td align=right>$jum</td>";
				echo "<td><input readonly=readonly class='span20' type='text' name='vchno[]' value='".$x[$i]['voucher']."' /></td>";
				echo "<td><input readonly=readonly class='span20' type='text' name='vchkey[]' value='".$x[$i]['key']."' /></td>";
				echo "<td><input class='span20' type='text' name='idmember[]'  id='idmember$i' onchange=\"All.getFullNameByID(this.value,'be/member/helper/idInfo',$idm)\" value='' /></td>";
				echo "<td><input class='span20' type='text' name='namamember[]'  id='namamember$i'  value='' /></td>";
				echo "</tr>";
			}
			echo "<tr><td colspan=2><input type=button class='btn btn-mini btn-primary' value='Create Shopping Voucher' onclick=\"be_promo.saveShoppingVoucher()\" /></td><td colspan=3>&nbsp;</td></tr>";
			echo "</table></form>";
		}
		
		//$route['shop/vch/create/save'] = 'backend/shopping_voucher/shoppingVoucherSave';
		public function shoppingVoucherSave() {
			$data = $this->input->post(NULL, TRUE);
			$x=$this->voucher->saveVoucher($data);

			backToMainForm();
			print_r($x);
		}
}	
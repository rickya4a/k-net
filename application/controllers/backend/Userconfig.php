<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Userconfig extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/userconfig/";
		$this->load->service('backend/be_userconfig_service');
	}
	
	/*--------------------
	 * USER GROUP
	 *-------------------*/
	//$route['user/group'] = 'backend/userconfig/getInputUserGroup';
	public function getInputUserGroup() {		
        if($this->username != null) {
           $this->load->helper('be_form');	
           $data['form_header'] = "Input User Group";
           $data['form_action'] = base_url('user/group');
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'user/group';
           $this->setTemplate($this->folderView.'getInputUserGroup', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['user/group/list/(:any)/(:any)'] = 'backend/userconfig/getListUserGroup/$1/$2';
	public function getListUserGroup($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listUserGroupService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	
	//$route['user/group/list'] = 'backend/userconfig/getListAllUserGroup';
	public function getListAllUserGroup($type = "array") {
		$data['listUserGroup'] = null;
    	try {
    		$data['listUserGroup'] = $this->be_userconfig_service->listAllUserGroupService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllUserGroup', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listUserGroup']));
		}	 
	}
	
	//$route['user/group/id/(:any)'] = 'backend/userconfig/getListUserGroupByID/$1';
	public function getListUserGroupByID($id) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listUserGroupService("groupid", $id);		
    	} catch(Exception $e) { }
		echo json_encode($srvReturn);
	}
	
	//$route['user/group/save'] = 'backend/userconfig/saveInputUserGroup';
	public function saveInputUserGroup() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('user/group') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->inputUserGroupService();	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['user/group/update'] = 'backend/userconfig/saveUpdateUserGroup';
	public function saveUpdateUserGroup() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('user/group') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->updateUserGroupService();	
    		}	  				
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	
	//$route['user/group/delete/(:any)'] = 'backend/userconfig/deleteUserGroup/$1';
	public function deleteUserGroup($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_userconfig_service->deleteUserGroupService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	/*--------------------
	 * USER 
	 *-------------------*/
	//$route['user'] = 'backend/userconfig/getInputUser';
	public function getInputUser() {		
        if($this->username != null) {
           $data['form_header'] = "Input User";
           $data['form_action'] = base_url('user');
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'user';
		   $data['listUserGroup'] = $this->be_userconfig_service->listAllUserGroupService();
           $this->setTemplate($this->folderView.'getInputUser', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['user/list'] = 'backend/userconfig/getListAllUser';
	public function getListAllUser() {
		$data['listUser'] = null;
    	try {
    		$data['listUser'] = $this->be_userconfig_service->listAllUserService();		
    	} catch(Exception $e) { }
		$this->load->view($this->folderView.'getListAllUser', $data); 
	}
	
	//$route['user/id/(:any)'] = 'backend/userconfig/getListUserByID/$1';
	public function getListUserByID($id) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listUserService("username", $id);		
    	} catch(Exception $e) { }
		echo json_encode($srvReturn);
	}
	
	//$route['user/list/(:any)/(:any)'] = 'backend/userconfig/getListUser/$1/$2';
	public function getListUser($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listUserService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	
	//$route['user/save'] = 'backend/userconfig/saveInputUser';
	public function saveInputUser() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('user') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->inputUserService();	
    		}	
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['user/update'] = 'backend/userconfig/saveUpdateUser';
	public function saveUpdateUser() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('user') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->updateUserService();	
    		}	   				
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['user/delete/(:any)'] = 'backend/userconfig/deleteUser/$1';
	public function deleteUser($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_userconfig_service->deleteUserService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	/*--------------------
	 * APPLICATION
	 *-------------------*/
	//$route['app'] = 'backend/userconfig/getInputNewApplication';
	public function getInputApplication() {		
        if($this->username != null) {
           $data['form_header'] = "Input Application";
           $data['form_action'] = base_url('app/save');
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'app';
           $this->setTemplate($this->folderView.'getInputApplication', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}
		
	//$route['app/list'] = 'backend/userconfig/getListAllApplication';
    //$route['app/list/json'] = 'backend/userconfig/getListAllApplication/$1';
	public function getListAllApplication($type = "array") {
		$data['listApp'] = null;
    	try {
    		$data['listApp'] = $this->be_userconfig_service->listAllApplicationService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllApplication', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listApp']));
		}	 
	}
	
	//$route['app/id/(:any)'] = 'backend/userconfig/getListApplicationByID/$1';
	public function getListApplicationByID($id) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listApplicationService("app_id", $id);		
    	} catch(Exception $e) { }
		echo json_encode($srvReturn);
	}
	
	//$route['app/list/(:any)/(:any)'] = 'backend/userconfig/getListApplication/$1/$2';
	public function getListApplication($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listApplicationService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	
	//$route['app/save'] = 'backend/userconfig/saveInputApplication';
	public function saveInputApplication() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('app') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->inputApplicationService();	
    		}   				
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['app/update'] = 'backend/userconfig/saveUpdateApplication';
	public function saveUpdateApplication() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('app') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->updateApplicationService();	
    		}     				
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['app/delete/(:any)'] = 'backend/userconfig/deleteApplication/$1';
	public function deleteApplication($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_userconfig_service->deleteApplicationService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	/*--------------------
	 * GROUP MENU
	 *-------------------*/
	 
	 //$route['menu/group'] = 'backend/userconfig/getInputGroupMenu';
	public function getInputGroupMenu() {		
        if($this->username != null) {
           $data['form_header'] = "Input Group Menu";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'menu/group';
		   $data['listApp'] = $this->be_userconfig_service->listAllApplicationService();
           $this->setTemplate($this->folderView.'getInputGroupMenu', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['menu/group/list'] = 'backend/userconfig/getListAllGroupMenu';
	//$route['menu/group/list/json'] = 'backend/userconfig/getListAllGroupMenu/$1';
	public function getListAllGroupMenu($type= "array") {
		$data['listGroupMenu'] = null;
    	try {
    		$data['listGroupMenu'] = $this->be_userconfig_service->listAllGroupMenuService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllGroupMenu', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listGroupMenu']));
		}
	}
	
	//$route['menu/group/id/(:any)'] = 'backend/userconfig/getListGroupMenuByID/$1';
	public function getListGroupMenuByID($id) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listGroupMenuService("app_menu_id", $id);		
    	} catch(Exception $e) { }
		echo json_encode($srvReturn);
	}
	
	//$route['menu/group/list/(:any)/(:any)'] = 'backend/userconfig/getListGroupMenu/$1/$2';
	public function getListGroupMenu($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listGroupMenuService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	
	//$route['menu/group/save'] = 'backend/userconfig/saveInputGroupMenu';
	public function saveInputGroupMenu() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('menu/group') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->inputGroupMenuService();	
    		}	
 	    } catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['menu/group/update'] = 'backend/userconfig/saveUpdateGroupMenu';
	public function saveUpdateGroupMenu() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('menu/group') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->updateGroupMenuService();	
    		}		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['menu/group/delete/(:any)'] = 'backend/userconfig/deleteGroupMenu/$1';
	public function deleteGroupMenu($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_userconfig_service->deleteGroupMenuService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	/*--------------------
	 * SUB MENU
	 *-------------------*/
	 //$route['menu'] = 'backend/userconfig/getInputSubMenu';
	 public function getInputSubMenu() {		
        if($this->username != null) {
           $data['form_header'] = "Input Group Menu";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'menu';
		   $data['listGroupMenu'] = $this->be_userconfig_service->listAllGroupMenuService();
           $this->setTemplate($this->folderView.'getInputSubMenu', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	 }
	 
	 //$route['menu/list'] = 'backend/userconfig/getListAllSubMenu';
	 //$route['menu/list/json'] = 'backend/userconfig/getListAllSubMenu/$1';
	public function getListAllSubMenu($type = "array") {
		$data['listSubMenu'] = null;
    	try {
    		$data['listSubMenu'] = $this->be_userconfig_service->listAllSubMenuService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllMenu', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listSubMenu']));
		}
	}
	
	//$route['menu/id/(:any)'] = 'backend/userconfig/getListSubMenuByID/$1';
	public function getListSubMenuByID($id) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listSubMenuService("app_menu_id", $id);		
    	} catch(Exception $e) { }
		echo json_encode($srvReturn);
	}
	
	//$route['menu/list/(:any)/(:any)'] = 'backend/userconfig/getListSubMenu/$1/$2';
	public function getListSubMenu($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_userconfig_service->listSubMenuService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	
	//$route['menu/save'] = 'backend/userconfig/saveInputSubMenu';
	public function saveInputSubMenu() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('menu') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->inputSubMenuService();	
    		}		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['menu/update'] = 'backend/userconfig/saveUpdateSubMenu';
	public function saveUpdateSubMenu() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('menu') == TRUE) {
    			$srvReturn = $this->be_userconfig_service->updateSubMenuService();	
    		}		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['menu/delete/(:any)'] = 'backend/userconfig/deleteSubMenu/$1';
	public function deleteSubMenu($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_userconfig_service->deleteSubMenuService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	/*---------------------------
	 * USER ACCESS MENU 
	 * --------------------------*/
	 
	 private function buildMenu($parent, $menu, $roleid)
    {
       
       $html = "";
       if (isset($menu['parents'][$parent]))
       {
           //$html .= "</br>";
           $s = 0;
           foreach ($menu['parents'][$parent] as $itemId)
           {
              //echo $menu['items'][$itemId]['menu_id'];
              //echo "<br />";
              //echo $roleid; 
              $exist = $this->be_userconfig_service->getListUserByRoleID($roleid, $menu['items'][$itemId]['menu_id']);
			  //echo "<pre>";
			  //print_r($exist);
			  //echo "</pre>";
              //$exist = $this->m_admuser->getListUserByRoleID($roleid, "10");
              $bt_chk = "";
			  $bt_unchk = "";
               if($exist != null)
               {
	              if($exist[0]->toggle_add == "1") {	
	              	$bt_chk .= "<td ><div align=center><input type=\"checkbox\" class=\"acc_add\" name=\"add[]\" value=\"".$exist[0]->toggle_add."\" checked=\"checked\" onclick=\"All.setValCheck(this)\" />&nbsp;Add</div></td>";
				  } else {
				  	$bt_chk .= "<td ><div align=center><input type=\"checkbox\" class=\"acc_add\" name=\"add[]\" value=\"".$exist[0]->toggle_add."\" onclick=\"All.setValCheck(this)\" />&nbsp;Add</div></td>";
				  }
				  
				  if($exist[0]->toggle_edit == "1") {
				  	$bt_chk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_edit\" name=\"edit[]\" value=\"".$exist[0]->toggle_edit."\" checked=\"checked\"  onclick=\"All.setValCheck(this)\"/>&nbsp;Edit</div></td>";	
				  } else {
				  	$bt_chk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_edit\" name=\"edit[]\" value=\"".$exist[0]->toggle_edit."\"  onclick=\"All.setValCheck(this)\"/>&nbsp;Edit</div></td>";
				  }
				  
				  if($exist[0]->toggle_view == "1") {
				  	$bt_chk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_view\" name=\"view[]\" value=\"".$exist[0]->toggle_view."\" checked=\"checked\" onclick=\"All.setValCheck(this)\" />&nbsp;View</div></td>";
				  } else {
				  	$bt_chk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_view\" name=\"view[]\" value=\"".$exist[0]->toggle_view."\" onclick=\"All.setValCheck(this)\" />&nbsp;View</div></td>";
				  }
				  
				  if($exist[0]->toggle_delete == "1") {
				  	$bt_chk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_delete\" name=\"delete[]\" value=\"".$exist[0]->toggle_delete."\" checked=\"checked\" onclick=\"All.setValCheck(this)\" />&nbsp;Delete</div></td>";
				  } else {
				  	$bt_chk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_delete\" name=\"delete[]\" value=\"".$exist[0]->toggle_delete."\" onclick=\"All.setValCheck(this)\" />&nbsp;Delete</div></td>";
				  }
				  
			   } else {
			   	  $bt_unchk .= "<td ><div align=center><input type=\"checkbox\" class=\"acc_add\" name=\"add[]\" value=\"0\" onclick=\"All.setValCheck(this)\" />&nbsp;Add</div></td>";
			  	  $bt_unchk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_edit\" name=\"edit[]\" value=\"0\" onclick=\"All.setValCheck(this)\" />&nbsp;Edit</div></td>";
			  	  $bt_unchk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_view\" name=\"view[]\" value=\"0\" onclick=\"All.setValCheck(this)\" />&nbsp;View</div></td>";
			  	  $bt_unchk .="<td ><div align=center><input type=\"checkbox\" class=\"acc_delete\" name=\"delete[]\" value=\"0\" onclick=\"All.setValCheck(this)\" />&nbsp;Delete</div></td>";
			   }
			  
              
              $ss = site_url();
              //echo $exist;
              if(!isset($menu['parents'][$itemId]))
              {
                 if($exist != null)
                 {
                    //echo 1;	
                    $html .= "<tr>";
                    $html .="<td><div align=center><input type=\"checkbox\" class=\"acc_checkmenu\" name=\"menuid[]\" value=\"".$menu['items'][$itemId]['menu_id']."\" checked=\"checked\" /></div></td><td>".$menu['items'][$itemId]['menu_desc']."</td>";
                    $html .= $bt_chk;
                    $html .="</tr>";
					
                    //$html ."";
                 }
                 else
                 {
                    //echo 2;	  	
                    $html .= "<tr>";
                    $html .= "<td ><div align=center><input type=\"checkbox\" class=\"acc_checkmenu\" name=\"menuid[]\" value=\"".$menu['items'][$itemId]['menu_id']."\" /></div></td><td>".$menu['items'][$itemId]['menu_desc']."</td>";
                    $html .= $bt_unchk;
                    
                    $html .= "</tr>";
                 }
                 
                 //$arr = array($menu['items'][$itemId]['menu_id'] => $menu['items'][$itemId]['menu_name']);   
              }

              if(isset($menu['parents'][$itemId]))
              {
                 //<li><input type=\"checkbox\" id=\"item-$s\"/><label for=\"item-$s\">".$menu['items'][$itemId]['menu_name']."</label>";
                 //$html .= "<tr bgcolor=\"lightgrey\"><td><input type=\"checkbox\" name=\"menuid\" value=\"".$menu['items'][$itemId]['menu_id']."\" />&nbsp;&nbsp;&nbsp;&nbsp;</td><td>".$menu['items'][$itemId]['menu_name']."</td></tr>";
                 if($exist != null)
                 {
                    //echo 3;	  	
                    $html .= "<tr bgcolor=\"lightgrey\">";
                    $html .= "<td ><div align=center><input type=\"checkbox\" class=\"acc_checkmenu\" name=\"menuid[]\" value=\"".$menu['items'][$itemId]['menu_id']."\" checked=\"checked\" /></div></td><td><strong>".$menu['items'][$itemId]['menu_desc']."</strong></td>";
					$html .= "<td colspan=4>";
					$html .= "<input type=\"hidden\" class=\"acc_add\" name=\"add[]\" value=\"1\"  />";
					$html .= "<input type=\"hidden\" class=\"acc_edit\" name=\"edit[]\" value=\"1\"  />";
					$html .= "<input type=\"hidden\" class=\"acc_view\" name=\"view[]\" value=\"1\"  />";
					$html .= "<input type=\"hidden\" class=\"acc_delete\" name=\"delete[]\" value=\1\"  />";
					$html .= "</td>";
                    $html .= "</tr>";
                 }
                 else
                 {
                    $html .= "<tr bgcolor=\"lightgrey\">";
                    $html .= "<td ><div align=center><input type=\"checkbox\" class=\"acc_checkmenu\" name=\"menuid[]\" value=\"".$menu['items'][$itemId]['menu_id']."\" /></div></td><td><strong>".$menu['items'][$itemId]['menu_desc']."</strong></td>";
                    
                    $html .= "<td colspan=4>";
					$html .= "<input type=\"hidden\" class=\"acc_add\" name=\"add[]\" value=\"1\" />";
					$html .= "<input type=\"hidden\" class=\"acc_edit\" name=\"edit[]\" value=\"1\" />";
					$html .= "<input type=\"hidden\" class=\"acc_view\" name=\"view[]\" value=\"1\" />";
					$html .= "<input type=\"hidden\" class=\"acc_delete\" name=\"delete[]\" value=\"1\" />";
					$html .= "</td>";
                    $html .="</tr>";
                 }
                 //$arr = array($menu['items'][$itemId]['menu_id'] => $menu['items'][$itemId]['menu_name']);
                 $html .= $this->buildMenu($itemId, $menu, $roleid);
                 
              } 
              
              $s++;
           }
           //$html .= "</br>"; 
       }
       return $html;
       //return $arr;
    } 
	 
	 //$route['menu/access'] = 'backend/userconfig/getInputAccessMenu';
	 public function getInputAccessMenu() {
	 	if($this->username != null) {
           $data['form_header'] = "Setting User Access Menu";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'menu/access';
		   $data['listGroupUser'] = $this->be_userconfig_service->listAllUserGroupService();
           $this->setTemplate($this->folderView.'getInputAccessMenu', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	 }
	 
	 //$route['menu/check'] = 'backend/userconfig/getShowListMenuByGroupID';
	 public function getShowListMenuByGroupID()
     {
        $groupid = $this->input->post('groupid');	
        $menu = $this->be_userconfig_service->fetchingMenuService();
		$data['grpid'] = $groupid;
		$data['res'] = $this->buildMenu(0, $menu, $groupid);
		$this->load->view($this->folderView.'getListMenuConfig', $data);

     }
	 
     //$route['menu/access/save'] = 'backend/userconfig/saveInputAccessMenu';
     public function saveInputAccessMenu() {
	 	$srvReturn = jsonFalseResponse("Input Access Menu User failed..!!");
    	try {
    		$srvReturn = $this->be_userconfig_service->inputAccessMenuService();		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	 }
}
	
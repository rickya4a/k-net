<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Hifi extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this -> folderView = 'backend/hifi/';
		$this -> load -> model("backend/be_hifi_model", "hifi");
		$this -> hifi_api_usrname = "klink";
		$this -> hifi_api_pwd = "klink123";
		
		
		
		//SGO
		//key development
		//$this->sgokey = "0df5835ee198d49944c372ead860c241";
		//key production
		$this->sgokey = "51edf5e8117da341a8be702d9bc18de5";
	}
	
	public function errorXL() {
		echo "<h3>Maaf sementara pembelian paket data XL Co Brand tidak dapat digunakan karena ada pemeliharaan sistem</h3>";
	}
	
	//$route['hifi/trx/importFile'] = 'backend/hifi/importFormFile';
	public function importFormFile() {
		if ($this -> username != null) {
			$data['form_header'] = "Import Hi-Fi Transaction From File";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'hifi/trx/importFile';
			$this -> setTemplate($this -> folderView . 'importFormFile', $data);
		} else {
			echo $this -> sessExpMessage;
		}
	}
	
	//$route['hifi/trx/importFile/preview'] = 'backend/hifi/previewListHifiTrxFromFile';
	public function previewListHifiTrxFromFile() {
		$this->load->library('csvreader');
		//$form = $this->input->post(NULL, TRUE);
        $fileName = $_FILES["hifiFile"]["tmp_name"];  
		//print_r($_FILES);
		if($fileName != ""){
		    $this->csvreader->set_separator(",");  
			$data['csvData'] = $this->csvreader->parse_file_dion($fileName);
			//$data['list'] = $this->mMember->showInfoVch($data['csvData']);
		    //print_r($data['csvData']);
		    echo "<table class='table table-bordered table-striped'>";
			echo "<tr>";
			echo "<th>No</th>";
			echo "<th>Member ID</th>";
			echo "<th>Trx Type</th>";
			echo "<th>Nominal</th>";
			echo "<th>Cashback</th>";
			echo "</tr>";
			$i=1;
		    foreach($data['csvData'] as $dtx) {
		    	echo "<tr>";
				echo "<td>$i</td>";
				echo "<td>".$dtx['memberid']." - ".$dtx['membername']."</td>";
				echo "<td>".$dtx['trx_type']."</td>";
				echo "<td align=right>".$dtx['nominal']."</td>";
				echo "<td align=right>".$dtx['cashback_ori']."</td>";
				echo "</tr>";
				$i++;
		    }
			echo "<table>";
		    //$this->load->view($this->folderView.'voucher/printExportExcel', $data);
		}else{
			echo "No Data Uploaded";
		}
	}
	
	//$route['hifi/trx/importFile/save'] = 'backend/hifi/importHifiTrxFromFile';
	public function importHifiTrxFromFile() {
		$this->load->library('csvreader');
		//$form = $this->input->post(NULL, TRUE);
        $fileName = $_FILES["hifiFile"]["tmp_name"];  
		if($fileName != ""){
		    $this->csvreader->set_separator(",");  
			$data['csvData'] = $this->csvreader->parse_file_dion($fileName);
			//$res = $data['csvData'];
			$data['list'] = $this->hifi->importHifiTrxFromFile($data['csvData']);
		    print_r($data['list']);
		    /*foreach($res as $dta) {
		    	echo $dta->trx_id."<br />";
		    }*/
		    //$this->load->view($this->folderView.'voucher/printExportExcel', $data);
		}else{
			echo "No Data Uploaded";
		}
	}

	//$route['hifi/trx/import'] = 'backend/hifi/importForm';
	public function importForm() {
		if ($this -> username != null) {
			$data['form_header'] = "Import Hi-Fi Transaction From API";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'hifi/trx/import';
			$data['curdate'] = date("Y-m-d");
			$this -> setTemplate($this -> folderView . 'importForm', $data);
		} else {
			echo $this -> sessExpMessage;
		}
	}

	private function getHifiTrxFromAPI($data) {
		//$url = "52.220.157.192/api/bulk_trans/proses";
		$url = "https://jsonplaceholder.typicode.com/posts/";
		$fields = array('username' => urlencode($this -> hifi_api_usrname), 'password' => urlencode($this -> hifi_api_pwd), 'startdate' => urlencode($data['from']), 'enddate' => urlencode($data['to']));

		$data_string = json_encode($fields);

		//url-ify the data for the POST
		$fields_string = '';
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		//rtrim($fields_string, '&');
		$fields_string = rtrim($fields_string, '&');
        
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://ngelink.ngobrol.co.id/api/bulk_trans/proses", 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_ENCODING => "", 
		  CURLOPT_MAXREDIRS => 10, 
		  CURLOPT_TIMEOUT => 30, 
		  CURLOPT_HTTP_VERSION => 
		  CURL_HTTP_VERSION_1_1, 
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => $fields_string, 
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache", 
		    "Content-Type: application/x-www-form-urlencoded"), ));
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return false;
		} else {
			return $response;
		}
	}

	//$route['hifi/tesapi'] = 'backend/hifi/tesAPI';
	public function tesAPI() {
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://ngelink.ngobrol.co.id/api/bulk_trans/proses", 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_ENCODING => "", 
		  CURLOPT_MAXREDIRS => 10, 
		  CURLOPT_TIMEOUT => 30, 
		  CURLOPT_HTTP_VERSION => 
		  CURL_HTTP_VERSION_1_1, 
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => "username=klink&password=klink123&startdate=2018-01-29&enddate=2018-01-29", 
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache", 
		    "Content-Type: application/x-www-form-urlencoded"), ));
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			print_r($err);
		} else {
			//return $response;
			print_r($curl);
		}
	}

	//$route['hifi/trx/import/preview'] = 'backend/hifi/previewListHifiTrx';
	public function previewListHifiTrx() {
		$data = $this -> input -> post(NULL, TRUE);
		$res = $this->getHifiTrxFromAPI($data);
		$hasil = json_decode($res);
		//print_r($hasil);
		
		if($hasil != false) {
			if($hasil->responseCode == "0000") {
				//$resImport = $this->hifi->importHifiTrx($hasil->raw, "1");
				$data['result'] = $hasil->raw;
				
			} else {
				$data['result'] = null;
			}
			//$this->load->view($this->folderView.'getListHifiTrx', $data);
		}
	}

	//$route['hifi/trx/import/save'] = 'backend/hifi/importHifiTrx';
	public function importHifiTrx() {
		$data = $this -> input -> post(NULL, TRUE);
		$res = $this->getHifiTrxFromAPI($data);
		$hasil = json_decode($res);
		//print_r($hasil);
		if($hasil != false) {
			if($hasil->responseCode == "0000") {
				$resImport = $this->hifi->importHifiTrx($hasil->raw, "1");
				print_r($resImport);
				//echo "ok";
			}
		}
		
	}
	
	//$route['hifi/trx/report'] = 'backend/hifi/reportHifiTrx';
	public function reportHifiTrx() {
		if ($this -> username != null) {
			$data['form_header'] = "PPOB Transaction Report";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'hifi/trx/report';
			$data['curdate'] = date("Y-m-d");
			//$data['listTrxType'] = $this->hifi->getListHifiTrxType();
			$this -> setTemplate($this -> folderView . 'HifiTrxReportForm', $data);
		} else {
			echo $this -> sessExpMessage;
		}
	}
	
	//$route['hifi/trx/report/list'] = 'backend/hifi/getListReportHifiTrx';
	public function getListReportHifiTrx() {
		$data = $this->input->post(NULL, TRUE);
		$data['result'] = $this->hifi->getListReportHifiTrx($data);
		$this->load->view($this->folderView.'getListReportHifiTrx', $data);
	}
	
	//$route['sgo_ppob/trx/(:any)'] = 'backend/hifi/sgo_curl/$1';
	
	//$route['sgo_ppob/trxtype/list/(:any)'] = 'backend/hifi/listTrxTypeByPPOB/$1';
	public function listTrxTypeByPPOB($id) {
	  $result = jsonFalseResponse("No result Found");
	  $mdResult = null;
	  if($id == "1") {
		$mdResult = $this->hifi->getListHifiTrxType();
	  } else if($id == "4") {
	    $mdResult = $this->hifi->getListPaketDataXL();
	  } else if($id == "3") {
	     $mdResult = $this->hifi->getListSgoPPobTrxType(); 
	  }
	  
	  
	  if($mdResult != null) {
	    $result = jsonTrueResponse($mdResult);
	  }
	  //echo "<pre>";
	  //print_r($mdResult);
	  //echo "</pre>";
	  echo json_encode($result);
	}
	
	
	
	//$route['sgo_ppob/trx'] = 'backend/hifi/sgoPpobTrx';
	public function sgoPpobTrx() {
		if ($this -> username != null) {
			$data['form_header'] = "SGO PPOB Trx";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'sgo_ppob/trx';
			//$data['curdate'] = date("Y-m-d");
			$data['listCat'] =$this->hifi->getListCatSgoTrx();
			
			$this->load->model('webshop/shared_module', 'shared');
		    $data['listBank'] = $this->shared->getBankJohnDoe();
			$data['key'] = "key";
			$data['payID'] = "payID";
			$data['keySgoPayment'] = $this->sgokey;
			//$data['listBank'] = $this->shared->getBank();
			$this -> setTemplate($this -> folderView . 'sgoPpobTrx', $data);
		} else {
			echo $this -> sessExpMessage;
		}
	}
	
	//$route['sgo_ppob/trx/fe'] = 'backend/hifi/sgoPpobTrxFrontEnd';
	public function sgoPpobTrxFrontEnd() {
		$username = getUserID();
//		if ($username != null)
		if($this->_checkSessionStoreUser()) {
			$this->load->model('webshop/shared_module', 'shared');	
			$data['form_header'] = "SGO PPOB Trx";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'sgo_ppob/trx';
			//$data['curdate'] = date("Y-m-d");
			$data['listCat'] =$this->hifi->getListCatSgoTrx();
			
			//$this->load->model('webshop/shared_module', 'shared');
		    $data['prodCat'] = $this->shared->getListCatProd();
			
			
		    $data['listBank'] = $this->shared->getBankJohnDoe();
			$data['key'] = "key";
			$data['payID'] = "payID";
			$data['keySgoPayment'] = $this->sgokey;
			//$data['listBank'] = $this->shared->getBank();
			//$this -> setTemplateDevSGO($this -> folderView . 'frontend/sgoPpobTrxFe', $data);
			$this->setTempWebShopEspay($this -> folderView . 'frontend/sgoPpobTrxFe', $data);
		}
//		else {
//			echo "<script>alert('silahkan login dahulu..')</scipt>";
//		}
	}
	
	//$route['sgo_ppob/trx/listByCat/(:any)'] = 'backend/hifi/listPpobTrxByCat/$1';
	public function listPpobTrxByCat($id) {
		$res = $this->hifi->listPpobTrxByCat($id);
		if($res == null) {
			$arr = jsonFalseResponse("List Transaksi dengan kategori tersebut tidak ada..");
		} else {
			$arr = jsonTrueResponse($res);
		}
		echo json_encode($arr);
	}
	
	private function generateSignature($arr) {
		$jum = count($arr);
		$str = "";
		for($i=0;$i<$jum;$i++) {
			$str .= "##".$arr[$i];
		}
		
		$str .= "##";
		$str = strtoupper($str);
		//echo $str;
		return hash("sha256", $str);
	}
	
	//$route['sgo_ppob/signTes/(:any)/(:any)'] = 'backend/hifi/hasilSign/$1/$2';
	public function hasilSign($orderid, $prdcode) {
		$rr_uuid = $this->sgo_usrname."-TES-".date("ymd-his");
		$arr[0] = $this->sgo_usrname;
		$arr[1] = $orderid;
		$arr[2] = $prdcode;
		$arr[3] = $rr_uuid;
		$arr[4] = $this->sgo_signature_key;
		
		$sign = $this->generateSignature($arr);
		echo $rr_uuid."</br>";
		echo $sign;
	}
	
	//$route['sgo_ppob/importProduct'] = 'backend/hifi/sgoPpobImportPrd';
	public function sgoPpobImportPrd() {
		$rr_uuid = $this->sgo_usrname."-PRD-".date("ymd-his");
 		$fields = array(
			'rq_uuid' => urlencode($rr_uuid), 
			'rq_datetime' => urlencode(date("Y-m-d h:i:s")), 
			'sender_id' => urlencode($this->sgo_usrname),
			'password' => urlencode($this->sgo_password),  
		);
		
		$postData = $this->setFieldPost($fields);
		
		$arr = array(
			"url" => $this->sgo_url_listprd_dev,
			"postdata" => $postData
		);
		$rawResult = $this->sgo_curl($arr);
		$result = json_decode($rawResult);
		echo "<pre>";
		print_r($result);
		echo "</pre>";
        /*$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		if($result->error_code == "0000") {
			
			$dbqryx->trans_start();
			$double = 0;
			$success = 0;
			$fail = 0;
			$failArray=array();
			$doubleArray=array();
			$successArray=array();
			
			foreach($result->products as $dta) {
				//QUERY FOR CHECK DOUBLE TRANSACTION ID
		        $dbqryx->select('product_code');
				$dbqryx->from('ppob_sgo_trxtype');
				$dbqryx->where('product_code', $dta->product_code);
				$query = $dbqryx->get();	
				//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
				if($query->num_rows() > 0) {
					$double++;
					array_push($doubleArray, $dta->product_code);
				} else {
					$dataPrd = array(
						'product_code' => $dta->product_code,
						'admin_fee'=> (int) $dta->admin_fee,
						'product_name'=> strtoupper($dta->product_name),
			        ); 
					
					$dbqryx->insert('ppob_sgo_trxtype', $dataPrd);				
					
					$success++;
					array_push($successArray, $dta->product_code);
				}	
			}
			
			//END TRANSACTION	
			$dbqryx->trans_complete();
	
		    //CHECK IF TRANSACTION PROCESS IS COMMITED
	        if ($dbqryx->trans_status() === FALSE) {
	            //if something went wrong, rollback everything
	            $dbqryx->trans_rollback();
	            $ess = array(
	              "response" => "false"
				);
	        } else {
	            //if everything went right, insert the data to the database
	            $dbqryx->trans_commit();
	            $ess = array(
	              "response" => "true",
				  "success" => $success,
				  //"arraySuccess" => $successArray,
				  "double" => $double,
				  "arrayDouble" => $doubleArray,
				);
	        } 
			print_r($ess);
		} else {
			echo "CURL error";
		} */
	}

	//$route['sgo_ppob/inquiry'] = 'backend/hifi/sgoPpobInquiry';
	private function sgoPpobInquiry($data) {
		
		//GENERATE SIGNATURE
		$rr_uuid = $this->sgo_usrname."-INQ-".date("ymd-his");
		$arr[0] = $this->sgo_usrname;
		$arr[1] = $data['order_id'];
		$arr[2] = $data['product_code'];
		$arr[3] = $rr_uuid;
		$arr[4] = $this->sgo_signature_key;
		
		$sign = $this->generateSignature($arr);
		//END GENERATE SIGNATURE
		
		//PREPARE DATA FOR CURL
 		$fields = array(
			'rq_uuid' => urlencode($rr_uuid), 
			'rq_datetime' => urlencode(date("Y-m-d h:i:s")), 
			'sender_id' => urlencode($this->sgo_usrname), 
			'password' => urlencode($this->sgo_password),
			'order_id' => urlencode($data['order_id']),
			'product_code' => urlencode($data['product_code']),	
			'signature' => urlencode($sign),
		);
		$postData = $this->setFieldPost($fields);
		
		//SET URL CURL AND DATA
		$arr = array(
			"url" => $this->sgo_url_inquiry_dev,
			"postdata" => $postData
		);
		
		$hasil = $this->sgo_curl($arr);
		
		return $hasil;
		//print_r($result); 
		//$nominal = ppob_set_nominal($result->amount);
		//echo "<br />".number_format($nominal, "2", ',', '.');
		
		//$hasil = show_info_inquiry_ppob($result);
		//echo $hasil;
		/*if($result->error_code == "0000") {
			echo "<table width='70%' class='table table-bordered table-striped'>";
			echo "<tr><th colspan=2>Inquiry Info</th></tr>";
			foreach($result->data as $key => $value) {
				echo "<tr><td width=30%>$key</td><td>$value</td></tr>";	
			}
			
			echo "</table>";
		} else	{
			
		}*/
	}

	//$route['sgo_ppob/saveTemp'] = 'backend/hifi/saveTempTrx';
	public function saveTempTrx() {
		$data = $this->input->post(NULL, TRUE);
		$temp_id = "PS".randomNumber(8);
		
		//set data header detail
		$detailTrx = array(
			'trx_id'=> $temp_id,
			'qty'=> 1,
			'nominal' => $data['amount'],
			'cust_no'=> $data['order_id'],
	    );
		
		//set data header transaksi 
		$headerTrx = array(
			'trx_id' => $temp_id,
			'memberid'=> $data['idmember'],
			'membername'=> strtoupper($data['membername']),
			'member_no_hp'=> $data['telp_hp'],
			'trx_type'=> $data['product_code'],
			'createdt'=> date('Y-m-d H:i:s'),
			'amount_sgo' => $data['amountx'],
			'nominal'=> $data['amount'],
			'cashback_ori'=> 0,
			'cashback_reff'=> 0,
			'ppob_type'=> "3",
			'param_data' => $data['inq_data']
	    ); 
		
		//set data payment
		$payment = array(
			'trx_id' => $temp_id,
			'bank_code_payment' => $data['bankid'],
			'charge_admin' => $data['charge_admin'],
			'docno' => "",
			'payamt' => $data['amount'],
		);
		
		$insReturn = $this->hifi->saveTrxPpoB_Temp($temp_id, $headerTrx, $detailTrx, $payment, "3");
		echo json_encode($insReturn);
	}

	//$route['sgo_ppob/inquiryPay'] = 'backend/hifi/sgoPpobInquiryPayHifi';
	private function sgoPpobInquiryPayHifi($data) {
		
		//GENERATE SIGNATURE
		$rr_uuid = $this->sgo_usrname."-PAY-".date("ymd-his");
		$arr[0] = $this->sgo_usrname;
		$arr[1] = $data['order_id'];
		$arr[2] = $data['product_code'];
		$arr[3] = $data['amountx'];
		$arr[4] = $rr_uuid;
		$arr[5] = $this->sgo_signature_key;
		
		$sign = $this->generateSignature($arr);
		//END GENERATE SIGNATURE
		
		//PREPARE DATA FOR CURL
 		$fields = array(
			'rq_uuid' => urlencode($rr_uuid), 
			'rq_datetime' => urlencode(date("Y-m-d h:i:s")), 
			'sender_id' => urlencode($this->sgo_usrname), 
			'password' => urlencode($this->sgo_password),
			'order_id' => urlencode($data['order_id']),
			'product_id' => urlencode($data['product_code']),	
			'amount' => urlencode($data['amountx']),
			'signature' => urlencode($sign),
		);
		$postData = $this->setFieldPost($fields);
		
		//SET URL CURL AND DATA
		$arr = array(
			"url" => $this->sgo_url_inquiry_dev,
			"postdata" => $postData
		);
		
		$hasil = $this->sgo_curl($arr);
		
		return $hasil;
		
	}

	//$route['sgo_ppob/inquiry/show'] = 'backend/hifi/sgoPpobInquiryShow';
	public function sgoPpobInquiryShow() {
		$data = $this->input->post(NULL, TRUE);
		//$hasil = $this->sgoPpobInquiry($data);
		$arrData = array(
			"order_id" => $data['order_id'],
	        "product_code" => $data['product_code'],
	        "url" => $this->sgo_url_inquiry_dev
	        //"amountx" => $data['amountx'],
		);
		$hasil =  $this->sgoPpobInquiryPay($arrData);
		$result = json_decode($hasil);
		echo json_encode($result);
		//$hasil = show_info_inquiry_ppob($result);
		//echo $hasil;
	}
	
	//$route['sgo_ppob/inquiry/pay'] = 'backend/hifi/sgoPpobInqPay';
	public function sgoPpobInqPay() {
		$data = $this->input->post(NULL, TRUE);
		//$hasil = $this->sgoPpobInquiry($data);
		$arrData = array(
			"order_id" => $data['order_id'],
	        "product_code" => $data['product_code'],
	        "amountx" => $data['amount'],
	        "data" => $data['data'],
	        "url" => $this->sgo_url_payment_dev
	        //"amountx" => $data['amountx'],
		);
		$hasil =  $this->sgoPpobInquiryPay($arrData);
		$result = json_decode($hasil);
		echo json_encode($result);
	}
	
	//$route['sgo_ppob/inquiry/bill/status'] = 'backend/hifi/sgoPpobInquiryPayStatus';
	public function sgoPpobInquiryPayStatus() {
		$data = $this->input->post(NULL, TRUE);
		//$hasil = $this->sgoPpobInquiry($data);
		$arrData = array(
			//"order_id" => $data['order_id'],
	        "ref_id" => $data['ref_id'],
	        "url" => $this->sgo_url_inqpay_status
	        //"amountx" => $data['amountx'],
		);
		$hasil =  $this->sgoPpobInquiryPay($arrData);
		$result = json_decode($hasil);
		echo json_encode($result);
	}
	
	//$route['sgo_ppob/inquiryPay/show'] = 'backend/hifi/sgoPpobInquiryPayShow';
	public function sgoPpobInquiryPayShow() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = $this->sgoPpobInquiryPayHifi($data);
		$result = json_decode($hasil);
		echo json_encode($result);
	}
	
	//$route['sgo_ppob/trx/id/(:any)'] = 'backend/hifi/getResultTrxByID/$1';
	public function getResultTrxByID($id) {
		$res = $this->hifi->getDetailPPOBTrxByID($id);
		if($res == null) {
			echo "data $id tidak ada";
		} else {
			$param_data = json_decode($res[0]->param_data);
			//echo show_info_ppob_payresult($res, $param_data);
			//print_r($res);
			$this->load->model('webshop/shared_module', 'shared');
		    $data['prodCat'] = $this->shared->getListCatProd();
			$data['res'] = $res;
			$data['param_data'] = $param_data;
			//$this -> setTemplateDevSGO($this -> folderView . 'frontend/sgoPpobPayResult', $data);
			$this->setTempWebShopEspay($this -> folderView . 'frontend/sgoPpobPayResult', $data);
		}
	}
	
	//$route['sgo_ppob/save/va'] = 'backend/hifi/sgoPpobSaveVa';
	public function sgoPpobSaveVa() {
		$data = $this->input->post(NULL, TRUE);
		$temp_id = "PS".randomNumber(8);
		
		$res = $this->saveTrxPpob_Va($temp_id, $data);
		echo json_encode($res);
	}
	
	//$route['va/balance/(:any)'] = 'backend/hifi/getSaldoVa/$1';
	public function getSaldoVa($idmember) {
		$saldo = $this->hifi->getSaldoVa($idmember);
		if($saldo != null) {
			$arr = jsonTrueResponse($saldo);
		} else {
			$arr = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
		}
		echo json_encode($arr);
	}
	
	private function saveTrxPpob_Va($temp_id, $data) {
		$amount = (double) $data['amount'];
		$saldo = $this->hifi->getSaldoVa(getUserid());
		if($saldo != null) {
			$sisaSaldo = (double) $saldo[0]->amount;
			if(getUserid() == null) {
				return jsonFalseResponse("Silahkan login kembali..");
			}
			
			if($amount <= $sisaSaldo) {
					$res = $this->hifi->getPpobTrxById($temp_id, "real");
					//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
					if($res != null) {
						$new_temp_id = "PS".randomNumber(8);
						$this->saveTrxPpob_Va($new_temp_id, $data);
					} else {
						
						$tgl = date('Y-m-d H:i:s');
						
					    $arrResult = array(
							"order_id" => $data['order_id'],
					        "product_code" => $data['product_code'],
					        "amountx" => $data['amountx'],
					        "data" => $data['inq_data'],
					        "url" => $this->sgo_url_payment_dev
						);
							   	
					    $hasil = $this->sgoPpobInquiryPay($arrResult);
						
						$hasil2 = json_decode($hasil);
						//$hasil2->code ="00";
						if($hasil2->error_code == "0000") {
							
							//CHECK PAYMENT STATUS AND GET COMMISION FEE FOR CASHBACK
					   	  $arrStt = array(
						    "ref_id" => $hasil2->ref_id,
						    "url" => $this->sgo_url_inqpay_status
						  );
						  
						  $payStatus = $this->sgoPpobInquiryPay($arrStt);
					      $payStatus2 = json_decode($payStatus);
						  //END
						  if($payStatus2->error_code == "0000") {
							    $commision = (double) $payStatus2->commission_fee;
								//set data header detail
								$trx['detail'] = array(
									'trx_id'=> $temp_id,
									'memberid_target'=> getUserid(),
									'qty'=> 1,
									'nominal' => (double) $data['amount'],
									'cust_no'=> $data['order_id'],
									'reff_pay_id'=> $hasil2->ref_id
							    );
								
								//set data header transaksi 
								$trx['header'] = array(
									'trx_id' => $temp_id,
									'memberid'=> $data['idmember'],
									'membername'=> strtoupper($data['membername']),
									'member_no_hp'=> $data['telp_hp'],
									'trx_type'=> $data['product_code'],
									'createdt'=> $tgl,
									'amount_sgo' => $data['amountx'],
									'nominal'=> $amount,
									'cashback_ori'=> 0.6 * $commision,
									'cashback_reff'=> 0.4 * $commision,
									'ppob_type'=> "3",
									//'param_data' => $hasil,
									'param_data' => $payStatus
							    ); 
								
								//set data payment
								$trx['payment'] = array(
									'trx_id' => $temp_id,
									'bank_code_payment' => $data['bankid'],
									'charge_admin' => $data['charge_admin'],
									'docno' => "",
									'payamt' => $data['amount'],
								);
								
								
								$trx['additional'] = null;
								
								/*
								echo "<pre>";
							    print_r($trx);
								echo "<br />";
								print_r($hasil2);
							    echo "</pre>";	
								*/
								
								$insReturn = $this->hifi->saveTrxPPOB_XlSgoVa($temp_id, $trx, "3");
						        return $insReturn;
					       } else {
					       	return jsonFalseResponse($hasil2->error_desc);
					       }	
						} else {
							/*echo "<pre>";
						    print_r($hasil2);
						    echo "</pre>";	
							*/
							return jsonFalseResponse($hasil2->error_desc);
						}	
					}	
					//echo json_encode($insReturn);
			} else {
				return jsonFalseResponse("Sisa saldo anda : ".$sisaSaldo);
			}
		} else {
			return jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
		}
	 }

	 //$route['sgo_ppob/va/result/(:any)'] = 'backend/hifi/getDataInsertPpobTrxVa/$1';
	public function getDataInsertPpobTrxVa($id) {
		$res = $this->hifi->getDataInsertPpobTrxVa($id);
		echo "<pre>";
		echo print_r($res);
		echo "</pre>";
	}
	
	/*--------------------------
	 * XL Co Branded Paket Data
	 * ------------------------*/
	 //$route['xl/paketdata/fe'] = 'backend/hifi/XlPaketDataFE';
	 public function XlPaketDataFE() {
	 	$username = getUserID();
		$this->load->model('webshop/shared_module', 'shared');	
			$data['form_header'] = "SGO PPOB Trx";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'sgo_ppob/trx';
			//$data['curdate'] = date("Y-m-d");
			$data['listProdXL'] = $this->hifi->getListPrdXlPaketData();
			//$data['listProdXL'] = $this->getXlPaketDataStock();
			//$this->load->model('webshop/shared_module', 'shared');
		    $data['prodCat'] = $this->shared->getListCatProd();
			
			
		    
			$data['key'] = "key";
			$data['payID'] = "payID";
			$data['keySgoPayment'] = $this->sgokey;
		 if($username != "" || $username != null)  {
			
			//$data['listBank'] = $this->shared->getBank();
			//$this -> setTemplateDevSGO($this -> folderView . 'frontend/XlPaketDataFE', $data);
			$data['listBank'] = $this->shared->getBankJohnDoe();
			$this -> setTempWebShopEspay($this -> folderView . 'frontend/XlPaketDataFE', $data);
		}
		 else {
		    $data['listBank'] = $this->shared->getBankNonVA(); 
			$this -> setTempWebShopEspay($this -> folderView . 'frontend/XlPaketDataFENonMember', $data);
		}
	 }
     
     //$route['xl/trx/id/(:any)'] = 'backend/hifi/getTrxXLByID/$1';
	 public function getTrxXLByID() {
	 	
	 }
	 
	 //$route['xl/get/encryptkey'] = 'backend/hifi/getEncryptKey';
	 public function getEncryptKey() {
	 	$keyEncrypt = $this->getXLEncryptKey();
		$resultJson = json_encode(array("error" => "01"));
		
		$resKeyArray = json_decode($keyEncrypt);
		//print_r($resKeyArray);
			if(is_object($resKeyArray) && array_key_exists("key", $resKeyArray)) {
				$resultJson = $keyEncrypt;
			}
		 //aldi
		 $resultJson = $keyEncrypt;
		 //
			echo $resultJson;
		
	 }
	 
	 //$route['xl/get/stock'] = 'backend/hifi/getXlPaketDataStock';
	 public function getXlPaketDataStock() {
	 	 $stock = $this->getXlStock();
	 	 echo $stock;
	 }
	 
	 //$route['xl/stock/report/acc'] = 'backend/hifi/XlPaketDataStockReportForAcc';
	 public function XlPaketDataStockReportForAcc() {
		 /*
		 $stock = $this->getXlStock();
	 	 $data['xl_rekap_stock'] = json_decode($stock);
		 $xl_rekap_stock = json_decode($stock);
		 */
		 //print_r($data['xl_rekap_stock']);
		 
		 $xl_rekap_stock = $this->hifi->getListKuotaXLFromDB();
		 $xl_rekap_ppob = $this->hifi->listRekapPaketDataXl();
		 //print_r($data['xl_rekap_ppob']);
		 
		 $data['xl_ppob_not_success'] = null;
		 //print_r($data['xl_ppob_not_success']);
		 
		 $buyAt = $this->hifi->paketDataXLBoughtFrom();
		 
		 $arr = array();
		 $i = 0;
		 foreach($xl_rekap_stock as $dta) {
		    $arr[$i]['prod_id'] = $dta->kode_paket;
			foreach($xl_rekap_ppob as $dta2) {
			  if($dta->kode_paket == $dta2->trx_type) {
				  $arr[$i]['nama_paket'] = $dta2->nama_paket;
				  $arr[$i]['kuota_db'] = $dta2->kuota;
				  $arr[$i]['qty_record'] = $dta2->qty_record;
				  //$arr[$i]['kuota_xl'] = $dta->quota;
				  //$arr[$i]['exp_date'] = $dta->exp_date;
				  
				  foreach($buyAt as $dta3) {
				    if($dta3->trx_type == $dta2->trx_type && $dta3->bought_from == "K-MART") {
						 $arr[$i]['kmart'] = $dta3->qty_record;
						 //$arr[$i]['knet'] = 0;
						  
					} else if($dta3->trx_type == $dta2->trx_type && $dta3->bought_from == "K-NET") {
						 $arr[$i]['knet'] = $dta3->qty_record;
						 //$arr[$i]['kmart'] = 0;
						
					} 
				  }

			  }
			  
			}	
			$i++;	
		 }
		 $data['rekap_xl'] = $arr;
		 $data['xl_last_trx'] = null;
		 
		 $this->load->view($this -> folderView.'getReportXLStockAcc', $data);
	 }
	 
	 //$route['xl/stock/report'] = 'backend/hifi/XlPaketDataStockReport';
	 public function XlPaketDataStockReport() {
	 	 $stock = $this->getXlStock();
	 	 //$data['xl_rekap_stock'] = json_decode($stock);
		 $xl_rekap_stock = json_decode($stock);
		 //print_r($data['xl_rekap_stock']);
		 
		 
		 //$data['xl_rekap_ppob'] = $this->hifi->listRekapPaketDataXl();
		 $xl_rekap_ppob = $this->hifi->listRekapPaketDataXl();
		 //print_r($data['xl_rekap_ppob']);
		 
		 $data['xl_ppob_not_success'] = $this->hifi->listXlWithNoMsidnOrReffId();
		 //print_r($data['xl_ppob_not_success']);
		 
		 $buyAt = $this->hifi->paketDataXLBoughtFrom();
		 
		 $arr = array();
		 $i = 0;
		 foreach($xl_rekap_stock->stock as $dta) {
		    $arr[$i]['prod_id'] = $dta->prod_id;
			foreach($xl_rekap_ppob as $dta2) {
			  if($dta->prod_id == $dta2->trx_type) {
				  $arr[$i]['nama_paket'] = $dta2->nama_paket;
				  $arr[$i]['kuota_db'] = $dta2->kuota;
				  $arr[$i]['qty_record'] = $dta2->qty_record;
				  $arr[$i]['kuota_xl'] = $dta->quota;
				  $arr[$i]['exp_date'] = $dta->exp_date;
				  
				  foreach($buyAt as $dta3) {
				    if($dta3->trx_type == $dta2->trx_type && $dta3->bought_from == "K-MART") {
						 $arr[$i]['kmart'] = $dta3->qty_record;
						 //$arr[$i]['knet'] = 0;
						  
					} else if($dta3->trx_type == $dta2->trx_type && $dta3->bought_from == "K-NET") {
						 $arr[$i]['knet'] = $dta3->qty_record;
						 //$arr[$i]['kmart'] = 0;
						
					} /*else {
							$arr[$i]['kmart'] = 0;
							$arr[$i]['knet'] = 0;
							echo "nol<br />";
					}*/
				  }

			  }
			  
			  
			  
			  /*$cari = array_search($dta->prod_id, array_column($data['buyAt'], 'trx_type'));
			  if($cari) {
			    echo "ada";
				echo "<br />";
			  } else {
			     echo "takaada";
				echo "<br />";
			  }*/
			}	
			$i++;	
		 }
		 $data['rekap_xl'] = $arr;
		 //print_r($data['rekap_xl']);
		 /*echo "<pre>";
		 print_r($arr);
		 echo "</pre>";
		 */
		 $data['xl_last_trx'] = $this->hifi->lastTrxXL();
		 
		 $this->load->view($this -> folderView.'getReportXLStock', $data);
	 }
	 
	 //$route['xl/redeem/manual'] = 'backend/hifi/redeemStockXLManual';
	 public function redeemStockXLManual() {
		$data = $this->input->post(NULL, TRUE);	
		$arrXl = array(
		  "brand_id" => $this->adv_id,
		  "prod_id" => $data['prod_id'],
		  "trx_id" => $data['trx_id'],
		  "msisdn" => $data['msisdn'],
		);
		
		//$redeem = $this->redeemStockXL($arrXl);
		$redeem = $this->redeemStockXLnoEncrypt($arrXl);
		$hasil2 = json_decode($redeem);
		print_r($hasil2);
		/*if($hasil2 == "0" || $hasil2 == null) {
		   $res = array("response" => "false", "message" => "No response from XL or No Valid Hours");
		} 
		else if($hasil2->code == "00") { 
		   $res = array("response" => "true", "arrayData" => $hasil2, "message" => "Transaksi Paket Data XL Berhasil..");
		}
		 else
		 {			 $res=$hasil2;}
	    echo json_encode($res);*/
	 }

	 //$route['xl/redeem/stock'] = 'backend/hifi/redeemPaketDataStock';
	 public function redeemPaketDataStock() {
	 	$data = $this->input->post(NULL, TRUE);	
		$arr = array(
		  "brand_id" => $this->adv_id,
		  //"prod_id" => $data['prod_id'],
		  //"trx_id" => $data['trx_id'],
		  //"msisdn" => $data['msisdn'],
		);
		
		//print_r($data);
		
		$res = jsonFalseResponse("Data transaksi $data[trx_id] tidak ada..");
		$checkDataReal = $this->hifi->getDetailPPOBTrxByID($data['trx_id']);
		if($checkDataReal == null) {
		   
		   $checkDataTemp = $this->hifi->getDetailTempPPOBTrxByID($data['trx_id']);
		   if($checkDataTemp != null && $checkDataTemp[0]->bank_code_payment != "25") {
			    //pembayaran non virtual account / menggunakan SGO  
				//belum diaktifkan
				$arr['prod_id'] = $checkDataTemp[0]->trx_type;
				$arr['trx_id'] = $data['trx_id'];
				$arr['msisdn'] = $checkDataTemp[0]->cust_no;
				
				$res = $this->processXlTrxUsingSgo($arr, $data['trx_id']);
		   } else {
		        //BILA TRANSAKSI DI K-MART
				$checkDataKmart = $this->hifi->getDetailTrxXlKmart($data['trx_id']);
				if($checkDataKmart != null) {
					$arr['prod_id'] = $checkDataKmart[0]->kode_paket;
					$arr['trx_id'] = $data['trx_id'];
					$arr['msisdn'] = $checkDataKmart[0]->member_no_hp;
					//print_r($arr);
					//$redeem = $this->redeemStockXL($arr);
					$redeem = $this->redeemStockXLnoEncrypt($arr);
					$hasil2 = json_decode($redeem);
					//print_r($hasil2);
					if($hasil2 == "0" || $hasil2 == null) {
					   $res = array("response" => "false", "message" => $hasil2, "arr" => $arr);
					   $this->hifi->updateStatusResponseXL($data['trx_id'], $redeem);
					} 
					else if($hasil2->code == "00") { 
					   $ins = $this->hifi->insertXlFromKmart($checkDataKmart, $hasil2);
					   if($ins == true) {
						$res = array("response" => "true", "arrayData" => $hasil2, "message" => "Transaksi Paket Data XL Berhasil..", "arr" => $arr);
					   }
					}
					//$res = jsonTrueResponse($checkDataKmart, "Transaksi di Kmart..");
				} 
		   }
		} else {
		   //APABILA TRANSAKSI PPOB MASUK TAPI NO REFF TRANSAKSI XL MASIH KOSONG
		   $reffid = $checkDataReal[0]->reff_pay_id;
		   if($reffid == null || $reffid == "") {
		        //$trx_id = $checkDataReal[0]->trx_id;
				$arr['prod_id'] = $checkDataReal[0]->trx_type;
				$arr['trx_id'] = $data['trx_id'];
				$arr['msisdn'] = $checkDataReal[0]->cust_no;
				
			    //$redeem = $this->redeemStockXL($arr);
				$redeem = $this->redeemStockXLnoEncrypt($arr);
				$hasil2 = json_decode($redeem);
				//print_r($hasil2);
				if($hasil2 == "0" || $hasil2 == null) {
				   $res = array("response" => "false", "message" => "No response from XL or No Valid Hours");
				   $this->hifi->updateStatusResponseXL($data['trx_id'], $redeem);
				   //return $falseRes;
				} 
				else if($hasil2->code == "00") { 
					//$reff_id = $hasil2->reff_id;
					
					$reff_id = "REC";
					if($hasil2->reff_id != null && $hasil2->reff_id != "") {
						$reff_id = $hasil2->reff_id;
					}
					
					$res = array("response" => "false", "message" => "Transaksi paket gagal, reff_id : ".$reff_id." tapi update reff_id ke DB gagal..");
					$upd = $this->hifi->updateReffIdXL($data['trx_id'], $reff_id);
					//$hasilS = $this->hifi->updateReffIdXL_new($reff_id, $trx_id, $redeem);
					if($upd > 0) {
						$res = array("response" => "true", "arrayData" => $hasil2, "message" => "Transaksi Paket Data XL sukses, reff_id : $reff_id");
					} 
				}
			 
		    } else {
		      $res = jsonFalseResponse("Data transaksi sudah ada dengan No reff Pembayaran dari XL : $reffid");
		    }
		}
		
		echo json_encode($res); 
		
	 }
	 
	 private function processXlTrxUsingSgo($arrXl, $trx_id) {
	    //$redeem = $this->redeemStockXL($arrXl);
		$redeem = $this->redeemStockXLnoEncrypt($arrXl);
		//print_r($redeem);
		$hasil2 = json_decode($redeem);
		//print_r($hasil2);
		//if($hasil2->response == "") []
		
		if($hasil2 == "0" || $hasil2 == null) {
		   $falseRes = array("response" => "false", "message" => "No response from XL or No Valid Hours");
		   return $falseRes;
		} 
		else if($hasil2->code == "00") { 
		   $insertRes = array("response" => "false", "message" => "Pulsa berhasil tapi simpan ke database gagal..");
		   $insReal = $this->hifi->insertPpobFromTemp2($trx_id, json_encode($hasil2));
		   if($insReal['response'] == "true") {
		        
		        $resultInsert = $this->hifi->getPpobTrxById($trx_id, null);
				
				$reff_pay_id = "REC";
				if($hasil2->reff_id != null && $hasil2->reff_id != "") {
					$reff_pay_id = $hasil2->reff_id;
				}
				
				$arr_data = array(
					'id_xl'=> $resultInsert[0]->trx_type,
					'msisdn'=> $resultInsert[0]->cust_no,
					'createnm'=>$resultInsert[0]->memberid,
					'createdt'=>$resultInsert[0]->createdt,
					'price'=>$resultInsert[0]->nominal,
					'trcd'=>$resultInsert[0]->trx_id,
					//'reff_id'=>$hasil2->reff_id,
					'reff_id'=>$reff_pay_id,
					'kategori'=> 0,
					'status'=> 0
				);
				//print_r($arr_data);
				$this->hifi->SaveTRXPaket($arr_data);
				$insertRes = array("response" => "true", "arrayData" => $hasil2, "message" => "Transaksi Paket Data XL Berhasil..");
				return $insertRes;
			}
		} else {
			$falseRes = array("response" => "false", "arrXl" => $arrXl, "arrayData" => $hasil2, "message" => $hasil2->error);
			return $falseRes;
			//return jsonFalseResponse($hasil2->error);
		} 
	 }
	 
	 //$route['xl/trx/msidn/(:any)'] = 'backend/hifi/getDataMemberXLbyMsidn/$1';
	 public function getDataMemberXLbyMsidn($no_hp) {
	 	 $arr = jsonFalseResponse("No HP $no_hp tidak termasuk XL Co Branded, hilangkan angka 0 di depan, contoh 8770909xxx");
	 	 $res = $this->hifi->getDataMemberXLbyMsidn($no_hp);
		 if($res != null) {
			 /*if($res[0]->status != "2") {
			 	$arr = jsonFalseResponse("No HP $no_hp belum di release,mohon hubungi kantor K-LINK..");
			 } else {
			 	$arr = jsonTrueResponse($res, "OK");
			 }*/
			 $arr = jsonTrueResponse($res, "OK");
		 }
		 echo json_encode($arr);
	 }
	 
	 
	 //$route['xl/paketData/saveTemp'] = 'backend/hifi/XlPaketDataSaveTemp';
	 public function XlPaketDataSaveTemp() {
	 	$data = $this->input->post(NULL, TRUE);
		$checkMember = $this->hifi->getFullNameActiveMember("dfno", $data['memberid']);
		if($checkMember != null) {
			$temp_id = "PX".randomNumber(8);
			$amount = (double) $data['amount'];
			$amount_cashback = (double) $data['amount_cashback'];
			//set data header detail
			
			$msidn = ltrim($data['msisdn'], '0');
			$msidn2 = preg_replace("/[^0-9]/", "", $msidn);
			$detailTrx = array(
				'trx_id'=> $temp_id,
				'memberid_target'=> $data['memberid'],
				'qty'=> 1,
				'nominal' => $amount,
				//'cust_no'=> "62".$data['msisdn'],
				'cust_no' => "62".$msidn2
			);
			
			//set data header transaksi 
			$headerTrx = array(
				'trx_id' => $temp_id,
				'memberid'=> $data['memberid'],
				'membername'=> strtoupper($data['membernamex']),
				'member_no_hp'=> $data['telp_hp'],
				'trx_type'=> $data['prod_id'],
				'createdt'=> date('Y-m-d H:i:s'),
				'amount_sgo' => $data['amountx'],
				'nominal'=> $amount,
				//'cashback_ori'=> 0,
				//'cashback_reff'=> 0,
				'cashback_ori'=> 0.06 * $amount_cashback,
				'cashback_reff'=> 0.06 * $amount_cashback,
				'ppob_type'=> "4",
				'param_data' => $data['inq_data'],
				'createby' => getUserid()
			); 
			
			//set data payment
			$payment = array(
				'trx_id' => $temp_id,
				'bank_code_payment' => $data['bankid'],
				'charge_admin' => $data['charge_admin'],
				'docno' => "",
				'payamt' => $amount,
			);
			
			$insReturn = $this->hifi->saveTrxPpoB_Temp($temp_id, $headerTrx, $detailTrx, $payment, "4");
			echo json_encode($insReturn);
		} else {
			$arr = jsonFalseResponse("ID Member salah / tidak terdaftar..");
			echo json_encode($arr);
        }		
	 }

	 //$route['xl/paketData/save/va'] = 'backend/hifi/XlPaketDataSaveVa';
	 public function XlPaketDataSaveVa() {
	 	$data = $this->input->post(NULL, TRUE);
		$checkMember = $this->hifi->getFullNameActiveMember("dfno", $data['memberid']);
		if($checkMember != null) {
			$temp_id = "PX".randomNumber(8);
			//$res = $this->saveTrxPaketDataXL_Va($temp_id, $data);
			$res = $this->saveTrxPaketDataXL_Va_new($temp_id, $data);
			echo json_encode($res);
		}
		else {
			$arr = jsonFalseResponse("ID Member salah / tidak terdaftar..");
			echo json_encode($arr);
        }	
	 }
	 
	 private function saveTrxPaketDataXL_Va_new($temp_id, $data) {
		$amount = (double) $data['amount'];
		$amount_cashback = (double) $data['amount_cashback'];
		
		$saldo = $this->hifi->getSaldoVa(getUserid());
		if($saldo != null) {
			$sisaSaldo = (double) $saldo[0]->amount;
			if(getUserid() == null) {
				return jsonFalseResponse("Silahkan login kembali..");
			}
			
			if($amount <= $sisaSaldo) {
				$res = $this->hifi->getPpobTrxById($temp_id, "real");
					//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
					if($res != null) {
						$new_temp_id = "PX".randomNumber(8);
						$this->saveTrxPaketDataXL_Va_new($new_temp_id, $data);
					} else {
						//PREPARE REQUEST FOR REDEEM STOCK DATA XL
								//$msisdn = "62".$data['msisdn'];
								$msidn = ltrim($data['msisdn'], '0');
								$msidn2 = preg_replace("/[^0-9]/", "", $msidn);
								$msidn3 = "62".$msidn2;
								$tgl = date('Y-m-d H:i:s');
								$arrXL = array(
									  "brand_id" => $this->adv_id,
									  "prod_id" => $data['prod_id'],
									  "trx_id" => $temp_id,
									  "msisdn" => $msidn3,
								);
					
							//set data header detail
							$trx['detail'] = array(
								'trx_id'=> $temp_id,
								'memberid_target'=> $data['memberid'],
								'qty'=> 1,
								'nominal' => $amount,
								//'cust_no'=> "62".$data['msisdn'],
								'cust_no'=> $msidn3,
								//'reff_pay_id' => $hasil2->reff_id
						    );
							
							//set data header transaksi 
							$trx['header'] = array(
								'trx_id' => $temp_id,
								'memberid'=> $data['idmember'],
								'membername'=> strtoupper($data['membername']),
								'member_no_hp'=> $data['telp_hp'],
								'trx_type'=> $data['prod_id'],
								'createdt'=> $tgl,
								'amount_sgo' => $data['amountx'],
								'nominal'=> $amount,
								'cashback_ori'=> 0.06 * $amount_cashback,
								'cashback_reff'=> 0.06 * $amount_cashback,
								'ppob_type'=> "4",
								'createby' => getUserid()
								//'param_data' => $redeem
						    ); 
							
							//set data payment
							$trx['payment'] = array(
								'trx_id' => $temp_id,
								'bank_code_payment' => $data['bankid'],
								'charge_admin' => $data['charge_admin'],
								'docno' => "",
								'payamt' => $data['amount'],
							);
							
							
							$trx['additional'] = array(
								'id_xl'    => $data['prod_id'],
								'msisdn'   => $msidn3,
								'createnm' => $data['idmember'],
								'createdt' => $tgl,
								'price'    => $amount,
								'trcd'     => $temp_id,
								//'reff_id'  => $hasil2->reff_id,
								//'reff_id'=>"redsds",
								'kategori'=> 0,
								//'status'=> 0
							);
							//print_r($trx);
							
							$insReturn = $this->hifi->saveTrxPPOB_XlSgoVa($temp_id, $trx, "4");
							//$insReturn = $this->hifi->saveTrxPPOB_XlSgoVa_baru($temp_id, $trx, "4");
							//print_r($insReturn);
							if($insReturn['response'] == "true") {
								
								$recordReturn = $insReturn['arrayData'];
								//$trx_id = $recordReturn[0]['trx_id'];
								$trx_id = $temp_id;
								
								$redeem = $this->redeemStockXLnoEncrypt($arrXL);
								$hasil2 = json_decode($redeem);
								
								if($hasil2 == "0") {
								   $falseRes = array("response" => "false", "message" => "Transaksi tidak dapat dilakukan pada jam yang sudah ditentukan..");
								   $this->hifi->updateStatusResponseXL($trx_id, $redeem);
								   return $falseRes;
								} else if($hasil2 == null) {
								   $falseRes = array("response" => "false", "message" => "No response from XL");
								   $this->hifi->updateStatusResponseXL($trx_id, $redeem);
								   return $falseRes;
								} else if($hasil2->code == "00") {
									//$reff_id = $hasil2->reff_id;
									
									$reff_id = "REC";
									if($hasil2->reff_id != null && $hasil2->reff_id != "") {
										$reff_id = $hasil2->reff_id;
									}
									
									
									$hasilS = $this->hifi->updateReffIdXL_new($reff_id, $trx_id, $redeem);
									return $hasilS;
									
									//$dbqryx->set('trcd', $resultInsert[0]->orderno);
								    //$dbqryx->where('trcd', $data['temp_orderno']);
								    //$dbqryx->update('va_cust_pay_det');
								
								    //$dbqryx->set('trcd', $resultInsert[0]->orderno);
								    //$dbqryx->where('trcd', $data['temp_orderno']);
								    //$dbqryx->update('va_cust_pay_det_acc');
								} else {
									$falseRes = array("response" => "false", "message" => "Undefined Error Type");
								    $this->hifi->updateStatusResponseXL($trx_id, $redeem);
								    return $falseRes;
								}
								
							} else {
								return jsonFalseResponse("Simpan transaksi XL gagal..");
							}
					}
			} else {
				return jsonFalseResponse("Saldo VA anda tidak cukup, sisa saldo anda : ".$sisaSaldo);
			}
		} else {
			return jsonFalseResponse("Saldo anda tidak cukup atau belum di top up");
		}	
	}

	 private function saveTrxPaketDataXL_Va($temp_id, $data) {
		$amount = (double) $data['amount'];
		$amount_cashback = (double) $data['amount_cashback'];
		
		$saldo = $this->hifi->getSaldoVa(getUserid());
		if($saldo != null) {
			
			$sisaSaldo = (double) $saldo[0]->amount;
			if(getUserid() == null) {
				return jsonFalseResponse("Silahkan login kembali..");
			}
			
			if($amount <= $sisaSaldo) {
					$res = $this->hifi->getPpobTrxById($temp_id, "real");
					//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
					if($res != null) {
						$new_temp_id = "PX".randomNumber(8);
						$this->saveTrxPaketDataXL_Va($new_temp_id, $data);
					} else {
						/*echo "<pre>";
					    print_r($data);
					    echo "</pre>";	
						*/
						
						//PREPARE REQUEST FOR REDEEM STOCK DATA XL
						//$msisdn = "62".$data['msisdn'];
						$msidn = ltrim($data['msisdn'], '0');
						$msidn2 = preg_replace("/[^0-9]/", "", $msidn);
						$msidn3 = "62".$msidn2;
						$tgl = date('Y-m-d H:i:s');
							$arrXL = array(
							  "brand_id" => $this->adv_id,
							  "prod_id" => $data['prod_id'],
							  "trx_id" => $temp_id,
							  "msisdn" => $msidn3,
						);
						
						
							/*echo "<pre>";
						    print_r($arrXL);
							echo "<br />";
							//print_r($hasil2);
						    echo "</pre>";	*/
							
	                    
						//$redeem = $this->redeemStockXL($arrXL);
						$redeem = $this->redeemStockXLnoEncrypt($arrXL);
					    $hasil2 = json_decode($redeem);
						
						if($hasil2 == "0") {
						   $falseRes = array("response" => "false", "message" => "Transaksi tidak dapat dilakukan pada jam yang sudah ditentukan..");
			               return $falseRes;
						} else if($hasil2 == null) {
						   $falseRes = array("response" => "false", "message" => "No response from XL");
			               return $falseRes;
						}
						
						else if($hasil2->code == "00") {
							//set data header detail
							
							$reff_pay_id = "REC";
							if($hasil2->reff_id != null && $hasil2->reff_id != "") {
								$reff_pay_id = $hasil2->reff_id;
							}
							
							$trx['detail'] = array(
								'trx_id'=> $temp_id,
								'memberid_target'=> $data['memberid'],
								'qty'=> 1,
								'nominal' => $amount,
								//'cust_no'=> "62".$data['msisdn'],
								'cust_no'=> $msidn3,
								//'reff_pay_id' => $hasil2->reff_id
								'reff_pay_id' => $reff_pay_id
						    );
							
							//set data header transaksi 
							$trx['header'] = array(
								'trx_id' => $temp_id,
								'memberid'=> $data['idmember'],
								'membername'=> strtoupper($data['membername']),
								'member_no_hp'=> $data['telp_hp'],
								'trx_type'=> $data['prod_id'],
								'createdt'=> $tgl,
								'amount_sgo' => $data['amountx'],
								'nominal'=> $amount,
								'cashback_ori'=> 0.06 * $amount_cashback,
								'cashback_reff'=> 0.06 * $amount_cashback,
								'ppob_type'=> "4",
								'param_data' => $redeem
						    ); 
							
							//set data payment
							$trx['payment'] = array(
								'trx_id' => $temp_id,
								'bank_code_payment' => $data['bankid'],
								'charge_admin' => $data['charge_admin'],
								'docno' => "",
								'payamt' => $data['amount'],
							);
							
							
							$trx['additional'] = array(
								'id_xl'    => $data['prod_id'],
								'msisdn'   => $msidn3,
								'createnm' => $data['idmember'],
								'createdt' => $tgl,
								'price'    => $amount,
								'trcd'     => $temp_id,
								//'reff_id'  => $hasil2->reff_id,
								'reff_id' => $reff_pay_id,
								//'reff_id'=>"redsds",
								'kategori'=> 0,
								'status'=> 0
							);
							
							
							
							$insReturn = $this->hifi->saveTrxPPOB_XlSgoVa($temp_id, $trx, "4");
					        return $insReturn;
						} else {
						    $falseRes = array("response" => "false", "arrXl" => $arrXL, "arrayData" => $hasil2, "message" => $hasil2->error);
			                return $falseRes;
							//return jsonFalseResponse($hasil2->error);
						}	
						 
					}	
								
					
					
					
					//echo json_encode($insReturn);
			} else {
				return jsonFalseResponse("Saldo VA anda tidak cukup, sisa saldo anda : ".$sisaSaldo);
			}
		} else {
			return jsonFalseResponse("Saldo anda tidak cukup atau belum di top up");
		}	
	 }	

	 //$route['xl/paketData/cash/(:any)/(:any)/(:any)'] = 'backend/hifi/XlPaketDataSaveCash/$1/$2/$3';
	 public function XlPaketDataSaveCash($trxid, $no_hp, $kode_paket) {
	 	
		
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		
		$dbqryx->select('trx_id');
		$dbqryx->from('ppob_xl_API_TWA');
		$dbqryx->where('trx_id', $trxid);
		$dbqryx->where('status', '0');
		$query = $dbqryx->get();	
		
		$res = jsonFalseResponse("Trx ID sudah diproses / gagal, silahkan buat transaksi baru");
		
		if($query->num_rows() > 0) {
			$tgl = date('Y-m-d H:i:s');
	        $arrXL = array(
	          "brand_id" => $this->adv_id,
			  "prod_id" => $kode_paket,
			  "trx_id" => $trxid,
			  "msisdn" => $no_hp,
			);
	        //print_r($arrXL);
			//$redeem = $this->redeemStockXL($arrXL);
			$redeem = $this->redeemStockXLnoEncrypt($arrXL);
			$hasil2 = json_decode($redeem);
			//print_r($redeem);
			if($hasil2 == "0" || $hasil2 == null) {
			   //$falseRes = array("response" => "false", "message" => "No response from XL or No Valid Hours");
			   //$res = jsonFalseResponse("No response from XL or No Valid Hours");
			   $datax = array(
			        'remark' => "No response from XL or No Valid Hours",
			        'status' => "2"
				);
			} 
			else if($hasil2->code == "00") {
				
				$reff_pay_id = "REC";
				if($hasil2->reff_id != null && $hasil2->reff_id != "") {
					$reff_pay_id = $hasil2->reff_id;
				}
				
				$datax = array(
			        'remark' => "Trx Success",
			        //'reff_id' => $hasil2->reff_id,
					'reff_id' => $reff_pay_id,
			        'status' => "1"
				);
				
			} else {
				$datax = array(
			        'remark' => $hasil2->error,
			        'status' => "2"
				);
			}
			
			$dbqryx->where('trx_id', $trxid);
		    $hslUpd = $dbqryx->update('ppob_xl_API_TWA', $datax);
			if($hslUpd > 0) {
				$res = jsonTrueResponse(null, "");
			} else {
				$res = jsonFalseResponse("Update Status = 2, gagal");
			}
		}	
		
		echo json_encode($res);
		
		
	 }
	 
	 //$route['va/invoice/send'] = 'backend/hifi/VaSendInvoice/$1';
	 public function VaSendInvoice() {
	 	$username = getUserID();
		 if($this->_checkSessionStoreUser()) {
			$this->load->model('webshop/shared_module', 'shared');	
			$data['form_header'] = "SGO PPOB Trx";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'sgo_ppob/trx';
			//$data['curdate'] = date("Y-m-d");
			//$data['listProdXL'] =$this->hifi->getListPrdXlPaketData();
			
			//$this->load->model('webshop/shared_module', 'shared');
		    $data['prodCat'] = $this->shared->getListCatProd();
			
			
		    $data['listBank'] = $this->shared->geListBankVa();
			$data['key'] = "key";
			$data['payID'] = "payID";
			$data['keySgoPayment'] = $this->sgokey;
			//$data['listBank'] = $this->shared->getBank();
			$data['listVa'] = $this->hifi->listVaMemb(getUserId());
			//$this -> setTemplateDevSGO($this -> folderView . 'frontend/VaSendInvoice', $data);
			$this -> setTempWebShopEspay($this -> folderView . 'frontend/VaSendInvoice', $data);
		}
	 }
	 
	 //$route['va/member/save'] = 'backend/hifi/saveVaMemb';
	 public function saveVaMemb() {
		$username = getUserID();
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
		    $check = $this->hifi->checkDoubleVaMemb($username, $data['va_number']);
			if($check == null) {
				$save = $this->hifi->saveVaMemb($data, $username);
				if($save > 0) {
					$arrx = jsonTrueResponse(null, "berhasil");
				} else {
					$arrx = jsonFalseResponse("gagal");
				}

			} else {
				$arrx = jsonFalseResponse("rekening sudah ada");
			}
			echo json_encode($arrx);
		}
	 }
	 
	 private function _sendInvoiceVatoSgo($data) {
	 	
		
	 	//GENERATE SIGNATURE
	 	$dateTime = date("Y-m-d H:i:s");
		$ccy = "IDR";
		$amount = "";
		$rr_uuid = $this->sgo_sendinvoice_usrname."-VAINV-".date("ymd-his");
		$arr[0] = $this->sgo_sendinvoice_signature_key;
		$arr[1] = $rr_uuid;
		$arr[2] = $dateTime;
		$arr[3] = $data['novac'];
		$arr[4] = $amount;
		$arr[5] = $ccy;
		$arr[6] = $this->sgo_sendinvoice_usrname;
		$arr[7] = "SENDINVOICE";
		
		$sign = $this->generateSignature($arr);
		//END GENERATE SIGNATURE
		
		$membername = preg_replace('/[^a-zA-Z]/', '', $data['membername']);
		
		//PREPARE DATA FOR CURL
 		$fields = array(
			'rq_uuid' => urlencode($rr_uuid), 
			'rq_datetime' => urlencode($dateTime), 
			'order_id' => urlencode($data['novac']),
			'amount' => urlencode($amount),
			'ccy' => urlencode($ccy),
			'comm_code' => $this->sgo_sendinvoice_usrname,
			'remark1' => urlencode($data['novac']),
			'remark2' => urlencode($membername),
			'remark3' => urlencode($data['email']),
			'bank_code' => $data['bank'],
			'signature' => $sign,
		);
		$postData = $this->setFieldPost($fields);
		
		//SET URL CURL AND DATA
		$arr = array(
			"url" => $this->sgo_url_sendinvoice,
			"postdata" => $postData
		);
		
		$hasil = $this->sgo_curl($arr);
		
		return $hasil;
	}

    //$route['xl/va/result/(:any)'] = 'backend/hifi/getDataInsertXLTrxVa/$1';
	public function getDataInsertXLTrxVa($id) {
		$res = $this->hifi->getDataInsertXLTrxVa($id);
		echo "<pre>";
		echo json_encode($res);
		echo "</pre>";
		/*$this->load->model('webshop/shared_module', 'shared');
		$username = getUserID();
		if ($username != null) {
			
			$data['saldo'] = $this->hifi->getSaldoVa($username);
			$data['res'] = $this->hifi->getDataInsertXLTrxVa($id);
			
			$data['prodCat'] = $this->shared->getListCatProd();
			$this->setTemplateDevSGO($this -> folderView . 'frontend/xlVaResult', $data); 
		} else {
			echo "<script>alert('silahkan login dahulu..')</scipt>";
		}*/	
	}

     //$route['va/invoice/send/post'] = 'backend/hifi/sendInvoiceVatoSgo';
	 public function sendInvoiceVatoSgo() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = $this->_sendInvoiceVatoSgo($data);
		$result = json_decode($hasil);
		echo json_encode($result);
	}
	 
	//$route['va/history/trx/fe'] = 'backend/hifi/vaHistoryTrx'; 
	public function vaHistoryTrx() {
		$username = getUserID();
		if($this->_checkSessionStoreUser()) {
			$this->load->model('webshop/shared_module', 'shared');
			$this->load->model('backend/be_transaction_model', 'm_trans');
			
			$arr = array(
			  "from" => date("Y-m-d"),
			  "to" => date("Y-m-d"),
			  "memberid" => getUserid(),
			  "tipe_dk" => "",
			  "trx_type" => "all"
			);
			$data['saldo'] = $this->hifi->getSaldoVa(getUserid());
			$data['res'] = $this->m_trans->getVaHistoryTrx($arr);
			$data['listTrtype'] = $this->hifi->getListVaTrxType();
			$data['prodCat'] = $this->shared->getListCatProd();
			//$this->setTemplateDevSGO($this -> folderView . 'frontend/vaHistoryTrx', $data);
			$this->setTempWebShopEspay($this -> folderView . 'frontend/vaHistoryTrx', $data);  
		} 
	}

	//$route['va/history/trx/list'] = 'backend/hifi/vaHistoryTrxList';
	public function vaHistoryTrxList() {
		$this->load->model('backend/be_transaction_model', 'm_trans');
		$formData = $this->input->post(NULL, TRUE);
		$tglf = $formData['thnf']."-".$formData['blnf']."-".$formData['tglf'];
		$tgl = $formData['thn']."-".$formData['bln']."-".$formData['tgl'];
		$arr = array(
			  "from" => $tglf,
			  "to" => $tgl,
			  "memberid" => getUserid(),
			  "tipe_dk" => "",
			  "trx_type" => $formData['trx_type']
			);
	    //print_r($arr);
		$res = $this->m_trans->getVaHistoryTrx($arr);
		if($res != null) {
			$arr =  jsonTrueResponse($res, null);
		} else {
			$arr =  jsonFalseResponse("Tidak ada transaksi");
		}
		echo json_encode($arr);
		//$this->load->view($this -> folderView . 'frontend/vaHistoryTrxList', $data);
	}
	
	//$route['sgo_ppob/detail/(:any)'] = 'backend/hifi/getResultTrxByID2/$1';
	public function getResultTrxByID2($id) {
	    $subStr = substr($id, 0, 2);
		if($subStr == "PX" || $subStr == "PS") {
			$res = $this->hifi->getDetailPPOBTrxByID($id);
			if($res == null) {
				$data['res'] = null;
				$this -> load -> view($this -> folderView . 'frontend/sgoPpobPayResult2', $data);
			} else {
				$param_data = json_decode($res[0]->param_data);
				//echo show_info_ppob_payresult($res, $param_data);
				//print_r($res);
				//$this->load->model('webshop/shared_module', 'shared');
				//$data['prodCat'] = $this->shared->getListCatProd();
				$data['res'] = $res;
				$data['param_data'] = $param_data;
				$this -> load -> view($this -> folderView . 'frontend/sgoPpobPayResult2', $data);
			}
		} else {
			$data['res'] = $this->hifi->getDetailTrxKnet($id);
			$this -> load -> view($this -> folderView . 'frontend/knetPayResult', $data);
		}
	}
	
	//$route['ppob/rekap/trx'] = 'backend/hifi/ppobRekapTrx';
	public function ppobRekapTrx() {
		$username = getUserID();

		if($this->_checkSessionStoreUser()) {
			$this->load->model('webshop/shared_module', 'shared');
            $data['prodCat'] = $this->shared->getListCatProd();
			
			$arr = array(
			  "from" => date("Y-m-d"),
			  "to" => date("Y-m-d"),
			  "memberid" => getUserid(),
			  "trx_type" => "all"
			);
			$data['res'] = $this->hifi->getListPpobXLTrx($arr);
			//$this->setTemplateDevSGO($this -> folderView . 'frontend/ppobRekapTrx', $data);
			$this->setTempWebShopEspay($this -> folderView . 'frontend/ppobRekapTrx', $data);  
		}
	}
    //$route['ppob/rekap/trx/result'] = 'backend/hifi/getListPpobRekapTrx';
    public function getListPpobRekapTrx() {
    	$formData = $this->input->post(NULL, TRUE);
		$tglf = $formData['thnf']."-".$formData['blnf']."-".$formData['tglf'];
		$tgl = $formData['thn']."-".$formData['bln']."-".$formData['tgl'];
    	$arr = array(
			  "from" => $tglf,
			  "to" => $tgl,
			  "memberid" => getUserid(),
			  "trx_type" => $formData['trx_type']
			);
		$data['res'] = $this->hifi->getListPpobXLTrx($arr);
		if($data['res'] != null) {
			$arr =  jsonTrueResponse($data['res'], null);
		} else {
			$arr =  jsonFalseResponse("Tidak ada transaksi");
		}
		echo json_encode($arr);
    }

	function getDetHifiTrx($id) {

		//get ppob_type first
		$cek = $this->hifi->getHeader1($id);
		//print_r($cek);

		foreach($cek as $r){
			//$id = $r->trx_id;
			$ppob_type = $r->ppob_type;
		}

		$trx_id = substr($id,0,2);
		//print_r($trx_id);

		if($ppob_type == '1' || $ppob_type == '2'){
			$dataH=$this->hifi->getHeader1($id);
			$data['header']=$dataH;

			$dataPay=$this->hifi->getPayment($id);
			$data['payment']=$dataPay;
		}
		else if($ppob_type == '3'){
			$dataH=$this->hifi->getHeader3($id);
			$data['header']=$dataH;

			$dataPay=$this->hifi->getPayment($id);
			$data['payment']=$dataPay;
		}
		else if($ppob_type == '4' && $trx_id != "KX"){
			//echo "ppob_xl";
			$dataH=$this->hifi->getHeader4($id);
			$data['header']=$dataH;

			$dataPay=$this->hifi->getPayment($id);
			$data['payment']=$dataPay;
		}
		else if($ppob_type == '4' && $trx_id == "KX"){
			$dataH=$this->hifi->getHeader4($id);
			$data['header']=$dataH;
			//echo "ppob_kmart";

			$dataPay = $this->hifi->getPayKMart($id);
			$data['payment']=$dataPay;

		}

		//$dataH=$this->hifi->getHeader($id);
		$dataDet=$this->hifi->getDetail($id);
		$data['det']=$dataDet;


		//$data['shipping']=$dataS;

		echo json_encode($data);
		//print_r($data);
		//print_r($dataPay);
	}


}

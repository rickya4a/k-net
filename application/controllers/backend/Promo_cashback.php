<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Promo_cashback extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("backend/m_promo_cashback", "cashback");	
		$this->folderView = "backend/promo_cashback/";
	}
	
	

	//$route['be/promo/cashback'] = 'backend/promo_cashback/cashbackForm';
	public function cashbackForm() {
		if($this->username != null) {
			$data['form_header'] = "Cashback Process";
            $data['icon'] = "icon-pencil";
		    $data['form_reload'] = 'be/promo/cashback';
            $this->setTemplate($this->folderView.'cashbackForm', $data); 
		} else {
			redirect('backend', 'refresh');
		}	
	}
	
	//$route['be/promo/cashback/list'] = 'backend/promo_cashback/cashbackGetList';
	public function cashbackGetList() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("Data tidak ditemukan...");
		if($data['trx_type'] == "recruiter") {
			$res = $this->cashback->rekapPromoRecruiter($data['jdtfroms'], $data['jdttos']);
			if($res != null) {
				$hasil = jsonTrueResponse($res);
			}	
			$this->load->view($this->folderView.'cashbackGetList', $hasil);
		} else if($data['trx_type'] == "rekap") {
			$res = $this->cashback->getListBatch($data['jdtfroms'], $data['jdttos']);
			if($res != null) {
				$hasil = jsonTrueResponse($res);
			}	
			$this->load->view($this->folderView.'cashbackBatchList', $hasil);
		}
		
	}
    
    //$route['be/promo/cashback/save'] = 'backend/promo_cashback/cashbackSave';
    public function cashbackSave() {
    	$data = $this->input->post(NULL, TRUE);
		if($data['trx_type'] == "recruiter") {
			$dd = $this->cashback->insertCashbackHdr($data['jdtfroms'], $data['jdttos']);
			if($dd['batchno'] != null) {
				echo setSuccessMessage("Proses berhasil, No Batch : ".$dd['batchno']);
			} else {
				echo setErrorMessage("Batch gagal di proses...");
			}
		} else {
			echo setErrorMessage("Pilihan tipe salah...");
		}
		//echo json_encode($hasil);
		
		
    }
	
	//$route['be/promo/cashback/batch/(:any)'] = 'backend/promo_cashback/listDetailBatch/$1';
	public function listDetailBatch($batchno) {
		$res = $this->cashback->getDetailByBatchNo($batchno);
	    if($res != null) {
	    	$hasil = jsonTrueResponse($res);
	    	$this->load->view($this->folderView.'cashbackBatchListDet', $hasil);	
	    } else {
	    	echo"<script>alert('Maaf data tidak ada..')</script>";
	    }
		
	}

    //$route['be/promo/cashback/csv/(:any)'] = 'backend/promo_cashback/listDetailBatchCSV/$1';
    public function listDetailBatchCSV($batchno) {
    	$res = $this->cashback->getDetailByBatchNo($batchno);
	    if($res != null) {
	    	$hasil = jsonTrueResponse($res);
	    	$filenm = "report-".$batchno;
			
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filenm.'');
			$output = fopen('php://output', 'r');
			fputcsv($output,array('No','Batch','ID Member','Nominal','Bank', 'No Rek','Status Trf','Remark'),';',' ');
        
            $no = 1;
            $tot = 0;
            foreach($res as $list){
            $y = array('No' => $no,
                        'Batch' => $list->batchno,
                        'ID_Member' => $list->sfno_reg." - ".$list->rekruitername,
                        'Nominal' => number_format($list->total_cashback,0,"",""),
                        'Bank' => $list->bankid,
                        'No_Rek' => $list->accountno,
                        'Status_Trf' => $list->status_trf,
                        'Remark' => $list->remark);
            $no++;
            //$tot += $list->Amount;
            fputcsv($output,$y,';',' ');
        }
        //fputcsv($output,array('','','Total',$tot),';',' ');
        
        fclose($output);
        
        
	    } else {
	    	echo"<script>alert('Maaf data tidak ada..')</script>";
	    }
    }

	//$route['be/promo/cashback/import'] = 'backend/promo_cashback/importFileBatch';
	public function importFileBatch() {
		/*$this->load->library("decryptor");
		$text = "username::password"; // dari database dijadikan string
        $password = "rahasia";
        $token = "AgFE7sSNnknETGCKrAOBN6CX2yuujsmcxvh9EVDRALgIl8O+obCv2LdIVdXz6Zg8EV7ndMJnxPdL4QubEnJ8744kZfVvH7urVkoV2wToTX/MMA=="; // diisi token dari mobile
		
		$decryptor = new Decryptor;
        $plaintextDecrypted = $decryptor->decrypt($token, $password);
        echo $plaintextDecrypted;*/
		
		$data = $this->input->post(NULL, TRUE);
		$res = $this->cashback->updateDetailBatch($data);
		echo json_encode($res);
		
	}
}
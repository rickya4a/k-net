<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class Recon_ppob extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('backend/m_recon_ppob', 'recon_ppob_model');
	}

	// $route['xl/recon/trx_only'] = 'backend/Recon_ppob/reconPpob';
	public function reconPpob() {
		$data = $this->input->post(NULL, TRUE);
		$data['result'] = $this->recon_ppob_model->reconPpob($data['orderid']);
		$response = jsonFalseResponse("Gagal reconcile...");
		if ($data['result'] != NULL) {
			$response = jsonTrueResponse(NULL, "Data reconciled...");
		}
		echo json_encode($response);
	}
}

<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Si_kepegawaian extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = "backend/managementA/";
		$this->load->model("backend/sik_m", 'sikModel');
        $this->load->model("backend/Management_assetm", 'mAmodel');
	}
    
    function empSIkFrmInput(){
        if ($this->username != null) {
            $x['form_header'] = 'Input Data Karyawan';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'sik/employee';
            
            $x['listDivisi'] = $this->mAmodel->getListDiv();            
            $this->setTemplate($this->folderView . 'inputEmplSik', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function emplSikSave(){
        $hasilReturn = jsonFalseResponse('Simpan Data Karyawan Gagal');
        if($this->form_validation->run('empl/asset') == TRUE) {
            $y = $this->input->post(NULL, TRUE);
            $param = 'id_employee';
            $xx = $this->mAmodel->getEmplInfo($param,$y['id_employee']);
            if($xx > 0){
                $hasilReturn = jsonFalseResponse($x, "Simpan Data Karyawan Gagal..!!");
            }else{
                $x = $this->sikModel->saveInputEmplSIK();
                if($x > 0){
                    $hasilReturn = jsonTrueResponse($x, "Simpan Data Karyawan Sukses..!!");
                }    
            }
        }
        echo json_encode($hasilReturn);
    }
    
}
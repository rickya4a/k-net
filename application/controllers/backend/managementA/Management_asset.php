<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Management_asset extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = "backend/managementA/";
		$this->load->model("backend/Management_assetm", 'mAmodel');
        
	}
    
    function categoryFormInput(){
        if ($this->username != null) {
            $x['form_header'] = 'Input Data Kategori Asset';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'ma/catInv';
            
            $this->setTemplate($this->folderView . 'inputCategoryInv', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function catInvByID($param,$value){
        $hasilReturn = jsonFalseResponse();
        $x = $this->mAmodel->getCatInfo($param,$value);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x);
        }
        echo json_encode($hasilReturn);
    }
    
    function getListCatInv(){
        //$x['cekInv'] = $this->mAmodel->cekCatInAsset();
        $x['listCat'] = $this->mAmodel->getListCat();
        $this->load->view($this->folderView . 'listCatInv', $x);
    }
    
    function categoryInvUpdate(){
        $hasilReturn = jsonFalseResponse('Update Data Kategori Gagal');
        
        $x = $this->mAmodel->saveUpdateCatInv();
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Update Data Kategori Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function categoryInvSave(){
        $hasilReturn = jsonFalseResponse('Simpan Data Kategori Gagal');
        if($this->form_validation->run('inv/cat') == TRUE) {
            $x = $this->mAmodel->saveInputCatInv();
            if($x > 0){
                $hasilReturn = jsonTrueResponse($x, "Simpan Data Kategori Success..!!");
            }
        }
        
        echo json_encode($hasilReturn);
    }
    
    function categoryInvDelete($id){
        $hasilReturn = jsonFalseResponse('Hapus Data Kategori Gagal');
        $cekInv = $this->mAmodel->cekCatInAsset($id);
        if($cekInv == null){
            $x = $this->mAmodel->deleteInputCatInv($id);
            if($x > 0){
                $hasilReturn = jsonTrueResponse($x, "Hapus Data Kategori Sukses..!!");
            }
        }else{
            $hasilReturn = jsonFalseResponse('Hapus Data Kategori Gagal,karena asset tersedia');
        }
        
        echo json_encode($hasilReturn);
    }
    
    function supplierFormInput(){
        if ($this->username != null) {
            $x['form_header'] = 'Input Data Supplier';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'ma/supplier';
            
            $this->setTemplate($this->folderView . 'inputSupplier', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function getListSupplier(){
        $x['listSupp'] = $this->mAmodel->getListSupplier();
        $this->load->view($this->folderView . 'listSupplier', $x);
    }
    
    function cekSupplierByID($param,$value){
        $hasilReturn = jsonFalseResponse();
        $x = $this->mAmodel->getSupplierInfo($param,$value);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x);
        }
        echo json_encode($hasilReturn);
    }
    
    function supplierDelete($id){
        $hasilReturn = jsonFalseResponse('Hapus Data Supplier Gagal');
        
        $x = $this->mAmodel->deleteInputSupplier($id);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Hapus Data Supplier Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function supplierSave(){
        $hasilReturn = jsonFalseResponse('Simpan Data Supplier Gagal');
        if($this->form_validation->run('inv/supplier') == TRUE) {
            $x = $this->mAmodel->saveInputSupplier();
            if($x > 0){
                $hasilReturn = jsonTrueResponse($x, "Simpan Data Supplier Sukses..!!");
            }
        }
        
        echo json_encode($hasilReturn);
    }
    
    function supplierUpdate(){
        $hasilReturn = jsonFalseResponse('Update Data Supplier Gagal');
        
        $x = $this->mAmodel->saveUpdateSupplier();
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Update Data Supplier Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function invFormInput(){
        if ($this->username != null) {
            $x['form_header'] = 'Input Data Asset';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'ma/inv';
            
            $x['listCat'] = $this->mAmodel->getListCat();
            $x['listSupplier'] = $this->mAmodel->listSuppForInv();
            
            $this->setTemplate($this->folderView . 'inputInv', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function invByID($param,$value){
        $hasilReturn = jsonFalseResponse();
        $x = $this->mAmodel->getInvInfo($param,$value);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x);
        }
        echo json_encode($hasilReturn);
    }
    
    function invSave(){
        $hasilReturn = jsonFalseResponse('Simpan Data Asset Gagal');
        if($this->form_validation->run('inv/asset') == TRUE) {
            $x = $this->mAmodel->saveInputInventory();
            if($x > 0){
                $hasilReturn = jsonTrueResponse($x, "Simpan Data Asset Sukses..!!");
            }
        }
        
        echo json_encode($hasilReturn);
    }
    
    function getListInv(){
        $x['listInvv'] = $this->mAmodel->getListInv();
        $this->load->view($this->folderView . 'listInv', $x);
    }
    
    function invUpdate(){
        $hasilReturn = jsonFalseResponse();
        $data = $this->input->post(NULL, TRUE);
        $qtyAvail = $this->mAmodel->cekQtyAvail($data['id_inventory']);
        $x = $this->mAmodel->saveUpdateInventory($qtyAvail[0]->qty);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Update Data Asset Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function invDelete($id){
        $hasilReturn = jsonFalseResponse('Hapus Data Asset Gagal');
        
        $x = $this->mAmodel->deleteInventory($id);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Hapus Data Asset Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function empFormInput(){
        if ($this->username != null) {
            $x['form_header'] = 'Input Data Karyawan';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'ma/employee';
            
            $x['listDivisi'] = $this->mAmodel->getListDiv();            
            $this->setTemplate($this->folderView . 'inputEmployee', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function employeeByID($param,$value){
        $hasilReturn = jsonFalseResponse();
        $x = $this->mAmodel->getEmplInfo($param,$value);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x);
        }
        echo json_encode($hasilReturn);
    }
    
    function employeeSave(){
        $hasilReturn = jsonFalseResponse('Simpan Data Karyawan Gagal');
        if($this->form_validation->run('empl/asset') == TRUE) {
            $y = $this->input->post(NULL, TRUE);
            $param = 'id_employee';
            $xx = $this->mAmodel->getEmplInfo($param,$y['id_employee']);
            if($xx > 0){
                $hasilReturn = jsonFalseResponse($x, "Simpan Data Karyawan Gagal..!!");
            }else{
                $x = $this->mAmodel->saveInputEmployee();
                if($x > 0){
                    $hasilReturn = jsonTrueResponse($x, "Simpan Data Karyawan Sukses..!!");
                }    
            }
        }
        echo json_encode($hasilReturn);
    }
    
    function getListEmploy(){
        $x['listEmpl'] = $this->mAmodel->getListEmp();
        $this->load->view($this->folderView . 'listEmp', $x);
    }
    
    function employUpdate(){
        $hasilReturn = jsonFalseResponse("Update Data Karyawan Gagal..!!");
        
        $x = $this->mAmodel->saveUpdateEmployee();
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Update Data Karyawan Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function employDelete($id){
        $hasilReturn = jsonFalseResponse('Hapus Data Karyawan Gagal');
        
        $x = $this->mAmodel->deleteEmployee($id);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Hapus Data Karyawan Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function mutasiInvForm(){
        if ($this->username != null) {
            $x['form_header'] = 'Input Mutasi Asset';
			$x['icon'] = 'icon-pencil';
			$x['form_reload'] = 'ma/mutasi/inv';
            
            $x['listEmpl'] = $this->mAmodel->getListEmp();
            $x['listInv'] = $this->mAmodel->getListInv1();
            $this->setTemplate($this->folderView . 'inputMutasiInv', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function mutasiInvSave(){
        $hasilReturn = jsonFalseResponse('Simpan Data Mutasi Gagal');
        if($this->form_validation->run('mutasi/asset') == TRUE) {
            $data = $this->input->post(NULL, TRUE);
            if($data['id_invent'] == null || $data['id_invent'] == ''){
                $idInventory = $data['id_inventory'];
            }else{
                $idInventory = $data['id_invent'];
            }
            //print_r($data);
            $ss = $this->mAmodel->cekQtyAvail($idInventory);
            $x = $this->mAmodel->saveInputMutasi($ss[0]->qty,$ss[0]->value,$ss[0]->qty_redemp);
            if($x > 0){
                $hasilReturn = jsonTrueResponse($x, "Simpan Data Mutasi Sukses..!!");
            }
        }
        
        echo json_encode($hasilReturn);
    }
    
    function getListMutasi(){
        $x['listMutasi'] = $this->mAmodel->getListMutasi();
        $this->load->view($this->folderView . 'listMutasi', $x);
    }
    
    function mutasiByID($param,$value){
        $hasilReturn = jsonFalseResponse();
        $x = $this->mAmodel->getMutasiInfo($param,$value);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x);
        }
        echo json_encode($hasilReturn);
    }
    
    function mutasiUpdate(){
        $hasilReturn = jsonFalseResponse("Update Data Mutasi Gagal..!!");
        $data = $this->input->post(NULL, TRUE);
        if($data['id_invent'] == null || $data['id_invent'] == '')
        {
            $idInventory = $data['id_inventory'];
        }else{
            $idInventory = $data['id_invent'];
        }
        $params = 'id';
        $cekInv =  $this->mAmodel->cekQtyAvail($idInventory);
        $cekMut =  $this->mAmodel->getMutasiInfo($params,$data['id_muts']);
        //echo "isinya ".$cekInv[0]->qty;
        //print_r($cekInv);
        $x = $this->mAmodel->saveUpdateMutasi($cekInv[0]->qty,$cekInv[0]->value,$cekInv[0]->qty_redemp,$cekMut[0]->qtyReq);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Update Data Mutasi Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function mutasiDelete($id){
        $hasilReturn = jsonFalseResponse('Hapus Data Mutasi Gagal');
        
        $x = $this->mAmodel->deleteMutasi($id);
        if($x > 0){
            $hasilReturn = jsonTrueResponse($x, "Hapus Data Mutasi Sukses..!!");
        }
        echo json_encode($hasilReturn);
    }
    
    function mutasiFrmRpt(){
        if ($this->username != null) {
            $x['form_header'] = 'Laporan Mutasi Asset';
			$x['icon'] = 'icon-list';
			$x['form_reload'] = 'ma/mutasi/report';
            
            $x['listEmpl'] = $this->mAmodel->getListEmp();
            $x['listInv'] = $this->mAmodel->getListInv1();
            $this->setTemplate($this->folderView . 'mutasiReport', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function mutasiRptAction($s){
        $dt = $this->input->post(NULL, TRUE);
        $x['tipe'] = $s;
        $x['results'] = $this->mAmodel->getListMutasiByDate($dt);
        $this->load->view($this->folderView.'mutasiRptList', $x);
    }
}
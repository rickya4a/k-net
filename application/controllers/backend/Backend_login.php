<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_login extends MY_Controller {
	
	
	public function __construct() {
	    parent::__construct();
		$this->load->service('backend/be_login_service');
	}
	
	//$route['backend'] = 'backend/login';
	public function index($err = null) {
		//echo "route['backend'] = 'backend/login';";
	    $data['error_message'] = $err;		
		$data['usr'] = "Admin";
		$data['formAction'] = base_url('backend/login');
		//$this->load->view('includes/header', $data);
		$this->load->view('includes/adm_login', $data);
		//$this->load->view('includes/footer', $data);
		
	}
	
	//$route['admin/login/err/(:any)'] = 'login/login_err/$1';
	public function login_err($err = "Username dan password harus diisi..!!") {
		$data['err'] = $err;
		$data['usr'] = "Admin";
		$data['formAction'] = base_url('backend/login');		
		$this->load->view('template/login/header', $data);
		$this->load->view('login', $data);
		$this->load->view('template/login/footer', $data); 
	}
	
	
	//$route['backend/login'] = 'backend/login/handleLogin';
	public function handleLogin() {
		try {
		    if($this->form_validation->run('admLogin') == FALSE) {
			   $this->index("Username dan password harus diisi..!!");
		    }
			
			$srvReturn = $this->be_login_service->handleLoginService();
			//print_r($srvReturn);
			$this->session->set_userdata('ecom_user', $srvReturn['ecom_user']);
			//$this->session->set_userdata('ecom_menu', $srvReturn['ecom_menu']);
			$this->session->set_userdata('ecom_groupid', $srvReturn['ecom_groupid']);
			$this->session->set_userdata('ecom_groupname', $srvReturn['ecom_groupname']);
			redirect('backend/main');
		} catch(Exception $e) {
			$this->index($e->getMessage());
		}	
	}
	
	private function buildMenu2($parent, $menu, $urut)
    {
       $ss = base_url();
       $html = "";
       if (isset($menu['parents'][$parent]))
       {
         foreach ($menu['parents'][$parent] as $itemId)
           {
              if(!isset($menu['parents'][$itemId]))
              {
                $html .= "<li><a rel=\"".$menu['items'][$itemId]['menu_id']."\" class=ss href=\"#\" id='$ss".$menu['items'][$itemId]['menu_url']."'>".$menu['items'][$itemId]['menu_desc']."</a></li>";
                //$html .= "<li><a rel=\"".$menu['items'][$itemId]['menu_id']."\" onclick=buatTabulasi() href=\"#\" id='$ss".$menu['items'][$itemId]['url']."'>".$menu['items'][$itemId]['menu_name']."</a></li>";
              }  
              if(isset($menu['parents'][$itemId]))
              {
                    if($urut != 0) {
                        $html .= "</ul>";
                    }
                    $html .= "
                      <div class=\"nav-header collapsed\" data-toggle=\"collapse\" data-target=\"#".$menu['items'][$itemId]['menu_id']."\"><i class=\"icon-dashboard\"></i>".$menu['items'][$itemId]['menu_desc']."</div>
                        <ul id=\"".$menu['items'][$itemId]['menu_id']."\" class=\"nav nav-list collapse\">";
                    //$html .= "<li><a href=".$menu['items'][$itemId]['menu_name'].">".$menu['items'][$itemId]['menu_name']."</a></li>";
                    $urut++;
                    $html .= $this->buildMenu2($itemId, $menu, $urut);
              }      
           }     
       } 
       return $html;
    } 

   //route['backend/main'] = 'backend/backend_login/mainMenu';
   public function mainMenu() {
   	  
   	  if($this->username != null) {
   	  	 $menu = $this->be_login_service->fetchingMenu($this->groupid);
   	     $data['menu'] = $this->buildMenu2(0, $menu, 0);
		 //print_r($this->session->all_userdata()) ;
		 $this->session->unset_userdata("ecom_menu");
		 $data['logout'] = base_url('backend/logout');		
		 $usr = $this->session->userdata('ecom_user');
		 if($usr == "DION") {
		    //$this->load->view('includes/headerDionDev', $data);
			$this->load->view('includes/header', $data);
		 } else {
			$this->load->view('includes/header', $data);
		 }
   	  }  else {
   	  	 redirect('backend');
   	  } 
   }
   //route['backend/main2'] = 'backend/backend_login/mainMenu2';
    public function mainMenu2() {
   	  
   	  if($this->username != null) {
   	  	 $menu = $this->be_login_service->fetchingMenu("13");
		 echo "<pre>";
   	     print_r($menu);
		 echo "<pre>";
   	  }  else {
   	  	 redirect('backend');
   	  } 
   }
	
	//$route['backend/logout'] = 'login/handleLogout';
	public function handleLogout() {
		//$this->session->sess_destroy();
		$this->session->sess_destroy();
		redirect('backend');
	}
	
	
}
	
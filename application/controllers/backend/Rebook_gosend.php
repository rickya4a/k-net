<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Rebook_gosend extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->folderView = "backend/rebook_gosend/";
        $this->load->model('backend/rebook/Rebook_model', 'm_rebookgsend');
        $this->load->model('webshop/ecomm_trans_model', 'mm_trans');
    }


    //$route['product/search'] = 'backend/product_search/productSearch';
    public function rebookSearch() {
        if($this->username != null) {
            $data['form_header'] = "Gosend Search";
            $data['form_action'] = base_url('shipping/rebook_gsend/list');
            $data['icon'] = "icon-search";
            $data['form_reload'] = 'shipping/rebook_gsend';
            $this->setTemplate($this->folderView.'rebookSearch', $data);
        } else {
            echo sessionExpireMessage(false);
        }
    }

    public function rebookSearchByID() {
        $data = $this->input->post(NULL,TRUE);
        $order = $data['paramValue'];

        $res['listnoConot'] = $this->m_rebookgsend->getReebookByID($order);

        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $getConote= $this->M_location->getConoteG($order);
        $noConote= $getConote[0]->conote;

        //echo $noConote;

        $dt1 = status_conoteG($user_key, $pass_key,$noConote); //get conote
        $result1= json_decode($dt1, TRUE);
        //$order_g= $result1['orderNo'];
        $order_g=$noConote;
        $res['status_order']= $result1['status'];

        //$res = jsonFalseResponse("Invalid Order No ..");
        $this->load->view($this->folderView.'rebookDetailByID', $res);

        /*if($result != null) {
            $res = jsonTrueResponse($result, "");
        }
        echo json_encode($res);*/
    }

    //$route['shipping/complain_gsend'] = 'backend/Rebook_gosend/orderSearch';
    public function orderSearch() {
        if($this->username != null) {
            $data['form_header'] = "Kirim Komplain ke GoSend";
            $data['form_action'] = base_url('shipping/complain_gsend/list');
            $data['icon'] = "icon-search";
            $data['form_reload'] = 'shipping/complain_gsend';
            $this->setTemplate($this->folderView.'orderSearch', $data);
        } else {
            echo sessionExpireMessage(false);
        }
    }

    //$route['complain_gsend/id'] = 'backend/Rebook_gosend/orderSearchByID';
    public function orderSearchByID() {
        $data = $this->input->post(NULL,TRUE);
        $noConote = $data['paramValue'];

        $res['listnoConot'] = $this->m_rebookgsend->getOrderByID($noConote);

        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $dt1 = status_conoteG($user_key, $pass_key,$noConote); //get conote
        $result1= json_decode($dt1, TRUE);
        $order_g=$noConote;
        $res['status_order']= $result1['status'];
        $res['driverName']= $result1['driverName'];
        $res['driverPhone']= $result1['driverPhone'];


        $res['noConote']= $noConote;
        $res['kendala'] = $data['pilih_kendala'];

        if($res['kendala']== 1){
            $res['ket']="Request Cancel : Pembeli ingin mengubah tipe pengiriman";
        }elseif($res['kendala']== 2){
            $res['ket']="Emergency Booking: Fake Complete - Driver menyelesaikan order tanpa mengambil barang, tak perlu Emergency Booking";
        }else{
            $res['ket']="Missing Goods: Pembeli belum menerima barang namun status order sudah selesai";
        }

        $this->load->view($this->folderView.'orderDetailByID', $res);

    }
    //$route['product/search/list'] = 'backend/product_search/productSearchByParam';
    public function productSearchByParam() {
        $data = $this->input->post(NULL,TRUE);
        $res['search'] = $data['param'];
        if($data['param'] == "prdcd") {
            $res['result'] = $this->m_product->getProductByID($data['paramValue']);
        } else if ($data['param'] == "prdnm") {
            $res['result'] = $this->m_product->getProductByName($data['paramValue']);
        } else if($data['param'] == "P") {
            $res['result'] = $this->m_product->getListProductBundling($data['paramValue']);
        } else if($data['param'] == "F") {
            $res['result'] = $this->m_product->getListFreeProduct($data['paramValue']);
        } else if($data['param'] == "dis") {
            $res['result'] = $this->m_product->getListIndenProduct($data['paramValue']);
        } else if($data['param'] == "non_knet") {
            $res['result'] = $this->m_product->getListPrdKnet($data['paramValue'], "0");
        } else if($data['param'] == "knet") {
            $res['result'] = $this->m_product->getListPrdKnet($data['paramValue'], "1");
        }
        $this->load->view($this->folderView.'productDetailByID', $res);
        //print_r($data);
    }

    //$route['product/id/(:any)/(:any)'] = 'backend/product_search/productSearchByID/$1/$2';


    //$route['product/status/update] = 'backend/product_search/updateProductStatus';
    function updateProductStatus() {
        $data = $this->input->post(NULL, TRUE);
        $res = jsonFalseResponse("update status produk gagal..");
        $result = $this->m_product->updateProductStatus($data['prdcd'], $data['param'], $data['value']);
        if($result > 0) {
            $res = jsonTrueResponse($result, "update status produk berhasil..");
        }
        echo json_encode($res);
    }

    function updtEConotG($orderno,$tipe,$token){

        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $dt1 = status_gsend($user_key, $pass_key,$orderno); //get conote
        $result1= json_decode($dt1, TRUE);
        $order_g= $result1['orderNo'];
        $status_g=$result1['status'];


        if($status_g == "Cancelled" || $status_g == "Driver not found" || $status_g == "Rejected"){
            $order_g="";
            $this->mm_trans->updateConotByOrderNo($orderno,$order_g); //update conote menjadi kosong

            $this->db = $this->load->database('db_ecommerce', true);
            $qryShpHdr = "SELECT a.*,b.total_pay
		              FROM ecomm_trans_shipaddr a
					  LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
		              WHERE a.orderno = '$orderno'";
            $queryShpHc = $this->db->query($qryShpHdr);

            if($queryShpHc->num_rows() > 0) {
                $row = $queryShpHc->result();
                $lat=$row[0]->lat_dest;
                $long=$row[0]->long_dest;

                $metode = $row[0]->service_type_name;
                $des_name = $row[0]->receiver_name;
                $des_phone = $row[0]->tel_hp1;
                $des_addr = cleanAddress($row[0]->addr1);
                $des_coord = $lat.",".$long;
                $price = $row[0]->total_pay;

                $warehouse=$row[0]->whcd;
                $stk=$row[0]->idstk;
            }


            $qryItem = "SELECT a.prdnm,a.qty
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
            $queryExec = $this->db->query($qryItem);

            if($queryExec->num_rows() > 0) {
                $no=0;
                foreach($queryExec->result() as $row1)
                {
                    $no++;
                    $prdnm= $row1->prdnm;
                    $qty= $row1->qty;
                    $concat_item= "(".$prdnm." : ".$qty." pc)";
                    $item_arr[]= $concat_item;
                    $item=implode(" ",$item_arr);
                }
            }

            //$qryItem1 = "SELECT * FROM V_ECOMM_GOSEND_SENDER_DET where whcd='$warehouse' AND loccd='$stk'";
            $qryItem1 = "SELECT * FROM V_ECOMM_GOSEND_SENDER_DET where loccd='$stk'";
            $queryExec1 = $this->db->query($qryItem1);

            if($queryExec1->num_rows() > 0) {
                foreach($queryExec1->result() as $rows)
                {
                    $fullnm= $rows-> fullnm;
                    $addr1= $rows->addr1;
                    $addr2= $rows->addr2;
                    $addr3= $rows->addr3;
                    $city= $rows->city;
                    $wh_tel_hp= $rows->tel_hp;
                    $wh_lat= $rows->latitude;
                    $wh_long= $rows->longitude;
                    $wh_head= $rows->WH_HEAD;
                    $hp_confirm= $rows->hp_to_confirm;

                }
            }

            if($hp_confirm == ""){
                $origin_kontak= $wh_tel_hp;
            }else{
                $origin_kontak=$hp_confirm;
            }

            $sendhp = explode(",", $origin_kontak);

            $senderName= $wh_head;
            $senderNote= $hp_confirm;
            $senderContact= $fullnm;
            $senderPhone= $sendhp[0];
            $senderCoord= $wh_lat.",".$wh_long;
            $senderAlamat= $addr1." ".$addr2." ".$addr3." ".$city;

            //get user key gosend
            $this->load->model('lokasi/M_location', 'M_location');
            $getKey= $this->M_location->getUserKey();
            $user_key= $getKey[0]->username;
            $pass_key= $getKey[0]->key_user;

            //mendapatkan jam buka gosend
            $day = date('D');
            $time= date('H:i:s');
            $now = new Datetime($time);
            //$now = new Datetime("now");
            $type_service=$row[0]->service_type_name;

            $hp= $row[0]->tel_hp1;
            $hp_val=str_replace("62","0",$hp);
            $hp_val=str_replace("+","",$hp_val);
            $hp_val=str_replace("-","",$hp_val);
            $hp_val=str_replace(" ","",$hp_val);

            /*$hp1="081388319355";
            $hp2="08568369705";*/

            $hp1= $sendhp[1];
            $hp2= $sendhp[0];

            $hp3="085717743438";

            //Instant
            $getInstant= $this->M_location->getSchedule($day,'Instant');
            $open_inst= $getInstant[0]->g_open;
            $closed_inst= $getInstant[0]->g_closed; //batas akhir konfirmasi pembayaran
            $begintime1 = new DateTime($open_inst);
            $endtime1 = new DateTime($closed_inst);

            $inst_open=date('H:i:s', strtotime($open_inst));
            $inst_closed=date('H:i:s', strtotime($closed_inst));

            //SameDay
            $getSame= $this->M_location->getSchedule($day,'SameDay');
            $open_same= $getSame[0]->g_open;
            $closed_same= $getSame[0]->g_closed; // batas akhir konfirmasi pembayaran
            $begintime2 = new DateTime($open_same);
            $endtime2 = new DateTime($closed_same);

            $same_open=date('H:i:s', strtotime($open_same));
            $same_closed=date('H:i:s', strtotime($closed_same));

            // CALL API GOSEND
            if($type_service == 'Instant'){
                if($time >= $inst_open && $time <= $inst_closed) {
                    $dt = booking_gsend($user_key,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
                    $result= json_decode($dt, TRUE);
                    //print_r($result);
                    if($result == null || $result ==''||  $result['errors'] !=''){

                        if($result['errors'] == "Sorry, you can only place a new order 86400 second after the last order you placed"){
                            $dt = status_gsend($user_key, $pass_key,$orderno);
                            $result1= json_decode($dt, TRUE);
                            $order_g= $result1['orderNo'];
                            print_r("masuk sini 1");
                        }else{
                            $order_g="";
                            print_r("masuk sini 2");
                        }

                    }else{
                        $order_g=$result['orderNo'];

                    }

                    if($order_g != ""){
                        //sms ke gudang
                        $txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
                        smsTemplate($hp1, $txt);
                        smsTemplate($hp2, $txt);
                    }else{
                        //sms ke vera
                        $txt = "error update conote:".$orderno;
                        smsTemplate($hp3, $txt);
                    }

                }else{
                    print_r("masuk sini 3");
                    print_r("<br>");
                    print_r($time.">=".$inst_open."<=".$inst_closed);

                    $order_g="";
                    $text = "Info pesanan gosend nomor order ".$orderno." melewati jam batas pengiriman dan akan dikirimkan di hari kerja besok.";
                    smsTemplate($hp_val, $text);
                }

            } else if($type_service == 'SameDay'){
                if($time >= $same_open && $time <= $same_closed){
                    $dt = booking_gsend($user_key, $pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price,$orderno);
                    $result= json_decode($dt, TRUE);


                    if($result == null || $result ==''||  $result['errors'] !=''){
                        if($result['errors'] == "Sorry, you can only place a new order 86400 second after the last order you placed"){
                            $dt = status_gsend($user_key, $pass_key,$orderno);
                            $result1= json_decode($dt, TRUE);
                            $order_g= $result1['orderNo'];
                            print_r("masuk sini 3");
                        }else{
                            $order_g="";
                            print_r("masuk sini 4");
                        }

                    }else{
                        $order_g=$result['orderNo'];
                    }

                    if($order_g != ""){
                        //sms ke gudang
                        $txt = "Info pesanan gosend ".$metode." yang akan dikirim hari ini: nomor order".$orderno.",penerima:".$des_name.",conote:".$order_g;
                        smsTemplate($hp1, $txt);
                        smsTemplate($hp2, $txt);
                    }else{
                        //sms ke vera
                        $txt = "error update conote:".$orderno;
                        smsTemplate($hp3, $txt);
                    }

                } else {
                    $order_g="";
                    $text = "Info pesanan gosend nomor order ".$orderno." melewati jam batas pengiriman dan akan dikirimkan di hari kerja besok.";
                    smsTemplate($hp_val, $text);
                }


            }else{
                $order_g="";
                $text = "Info pesanan gosend nomor order ".$orderno." mohon dicek metode pengiriman untuk nomor order tersebut.";
                smsTemplate($hp_val, $text);
            }

            $this->mm_trans->updateConotByOrderNo($orderno,$order_g);//update conote sesuai nomor order

            echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
            echo "<br><br>Rebooking Sukses! Conot Gosend Baru ".$order_g."<br><br>";




        }

        else{
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>Conot Gosend ".$order_g." Belum Tercancel! Silahkan Cancel Dulu ... <br><br>";

        }
    }


    function cancelEConotG($orderno,$tipe,$token){

        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $dt1 = status_gsend($user_key, $pass_key,$orderno); //get conote
        $result1= json_decode($dt1, TRUE);
        $order_g= $result1['orderNo'];
        $status_order= $result1['status'];

        $message="";

        if($status_order == "Driver not found"){
            $order_g="";
            $this->mm_trans->updateConotByOrderNo($orderno,$order_g); //update conote menjadi kosong

            echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
            echo "<br><br>Status Pengiriman ".$status_order.", Conote Berhasil Dihapus Dari Table!<br><br>";

        }elseif($status_order == "Rejected"){
            $order_g="";
            $this->mm_trans->updateConotByOrderNo($orderno,$order_g); //update conote menjadi kosong

            echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
            echo "<br><br>Status Pengiriman ".$status_order.", Conote Berhasil Dihapus Dari Table!<br><br>";

        }else{
            $dt2 = cancel_gsend($user_key, $pass_key,$order_g); //cancel conote
            $result2= json_decode($dt2, TRUE);
            $message=$result2['message'];
            if($message == "Booking cancelled") {
                //$order_g = $order_g;
                $this->mm_trans->cancelConotByOrderNo($orderno, $order_g);

                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>Cancel Conot Gosend Sukses,".$message."!<br><br>";
            }else{
                echo "<input class=\"btn btn-small btn-warning\" type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" value=\"<< Back\">";
                echo "<br><br>Cancel Conot Gosend Gagal!";
            }

        }


    }

}
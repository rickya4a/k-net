<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_pulsa extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/pulsa/";
		$this->load->service('backend/be_pulsa_service', 'pulsa');
		$this->backTo = 'backend';
		$this->sessExpMessage = "<script>alert('Session expired, please re-login..')</script>";
	}

	/*---------------------
	 * MODUL INPUT TRX PULSA
	 ---------------------*/
	//$route['be/pulsa'] = 'backend/backend_pulsa/formInputTrxPulsa';
	public function formInputTrxPulsa() {
		if($this->username != null) {
			$data['form_header'] = "Input Pulsa Transaction";
            $data['icon'] = "icon-pencil";
		    $data['form_reload'] = 'be/pulsa';
			$data['curdate'] = date("Y-m-d");
            $this->setTemplate($this->folderView.'formInputTrxPulsa', $data);
		} else {
			echo $this->sessExpMessage;
		}
	}

	//$route['be/pulsa/fullcode'] = 'backend/backend_pulsa/processTrxByFullcode';
	public function processTrxByFullcode() {
		if($this->username != null) {
			$data = $this->input->post(NULL, TRUE);
			$x['result'] = $this->pulsa->processTrxByFullcode($data['fullcode']);
			echo json_encode($x['result']);
		} else {
			echo $this->sessExpMessage;
		}
	}

	//$route['be/pulsa/save'] = 'backend/backend_pulsa/saveTrxPulsa';
	public function saveTrxPulsa() {
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->pulsa->saveTrxPulsa($data);
	    echo json_encode($arr);
	}

	//$route['be/pulsa/update'] = 'backend/backend_pulsa/updateTrxPulsa';
	public function updateTrxPulsa() {
		$data = $this->input->post(NULL, TRUE);
		$arr = $this->pulsa->updateTrxPulsa($data);
	    echo json_encode($arr);
	}

	//$route['be/pulsa/date'] = 'backend/backend_pulsa/searchTrxPulsaByDate';
	public function searchTrxPulsaByDate() {
		$data = $this->input->post(NULL, TRUE);
		try {
			$data['result'] = $this->pulsa->searchTrxPulsaByDate($data);
		} catch(Exception $e) {
			$data['error'] = $e->getMessage();
		}
		$this->load->view($this->folderView.'searchTrxPulsaByDate', $data);
	}

	//$route['be/pulsa/id/(:any)'] = 'backend/backend_pulsa/searchTrxPulsaByID/$1';
	public function searchTrxPulsaByID($id) {
		$arr = $this->pulsa->searchTrxPulsaByID($id);
	    echo json_encode($arr);
	}

	//$route['be/pulsa/delete/(:any)'] = 'backend/backend_pulsa/deletePulsaTrx/$1';
	public function deletePulsaTrx($id) {
		$arr = $this->pulsa->deletePulsaTrx($id);
	    echo json_encode($arr);
	}


	/*------------------------
	 * MODUL TRX PULSA SEARCH
	-------------------------*/

	//$route['be/pulsa/search'] = 'backend/backend_pulsa/formSearchTrxPulsa';
	public function formSearchTrxPulsa() {
		if($this->username != null) {
			$data['form_header'] = "Transaction Search";
            $data['icon'] = "icon-search";
		    $data['form_reload'] = 'be/pulsa/search';
            $this->setTemplate($this->folderView.'formSearchTrxPulsa', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

	//$route['be/pulsa/search/list'] = 'backend/backend_pulsa/listSearchTrxPulsa';
	public function listSearchTrxPulsa() {
		$form = $this->input->post(NULL, TRUE);
		$data['result'] = $this->pulsa->listSearchTrxPulsa($form);
		if($form['search_type'] == "cust") {
			$this->load->view($this->folderView.'listCellPhoneByCustName', $data);
		} elseif ($form['search_type'] == "hutang" || $form['search_type'] == "hutang_tanto") {
			$this->load->view($this->folderView.'listTrxOnDebt', $data);
		} elseif($form['search_type'] == "cell_phone") {
			$this->load->view($this->folderView.'listTrxByPhoneNumber', $data);
		} else {
			$this->load->view($this->folderView.'listTrxByClient', $data);
		}
	}

	/*-------------------------------
	 "L" => Order langsung,
	 "=" => Single Client Update,
	 "IN" => Multiple Client Update
	 -------------------------------*/

	//$route['be/pulsa/pay/(:any)'] = 'backend/backend_pulsa/payClientDebt/$1';
	public function payClientDebt($client_name) {
		$value = $this->pulsa->updatePayDebtClient("L", "=", $client_name);
		echo json_encode($value);
	}

	//$route['be/pulsa/pay'] = 'backend/backend_pulsa/paySeletectedClientDebt';
	public function paySeletectedClientDebt($client_name) {
		//$value = $this->pulsa->updateSelectedPayDebtClient("L", "=", $client_name);
		//echo json_encode($value);
	}

	//$route['be/pulsa/pay'] = 'backend/backend_pulsa/paySelectedClientDebt';
	public function paySelectedClientDebt() {
		$data = $this->input->post(NULL, TRUE);
		$value = $this->pulsa->updatePayDebtClient("L", "IN", $data);

		//print_r($data);
		//$arr = array("response" => "true", "data" => $data['client']);
		echo json_encode($value);
	}

	//$route['be/pulsa/pay/selected'] = 'backend/backend_pulsa/paySelectedID';
	public function paySelectedID() {
		$data = $this->input->post(NULL, TRUE);
		$value = $this->pulsa->updatePayDebtSelected("IN", $data);
		echo json_encode($value);
	}

	//$route['be/pulsa/tanto/pay/(:any)'] = 'backend/backend_pulsa/payTantoClientDebt/$1';
	public function payTantoClientDebt($client_name) {
		$value = $this->pulsa->updatePayDebtClient("V", "=", $client_name);
		echo json_encode($value);
	}

    //$route['be/pulsa/tanto/pay'] = 'backend/backend_pulsa/paySelectedTantoClientDebt';
    public function paySelectedTantoClientDebt() {
		$data = $this->input->post(NULL, TRUE);
		$value = $this->pulsa->updatePayDebtClient("V", "IN", $data);
		echo json_encode($value);
	}

	//$route['be/pulsa/debt/(:any)/(:any)'] = 'backend/backend_pulsa/detailClientDebt/$1/$2';
	public function detailClientDebt($client_name, $order_lgs) {
		$data['client']	= $client_name;
		$data['result'] = $this->pulsa->detailClientDebt($client_name, $order_lgs);
		$this->load->view($this->folderView.'detailClientDebt', $data);
	}


	/*------------------------
	 * MODUL TRX PULSA REPORT
	-------------------------*/
	//$route['be/pulsa/report'] = 'backend/backend_pulsa/formReportTrxPulsa';
	public function formReportTrxPulsa() {
		if($this->username != null) {
			$data['form_header'] = "Transaction Report";
            $data['icon'] = "icon-list";
		    $data['form_reload'] = 'be/pulsa/report';
			//$data['result'] = $this->pulsa->trxReport("2014");
            $this->setTemplate($this->folderView.'formReportTrxPulsa', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

	//$route['be/pulsa/report/list'] = 'backend/backend_pulsa/listReportTrxPulsa';
	public function listReportTrxPulsa() {
		$dataForm = $this->input->post(NULL, TRUE);
		$data['yearReport'] = $dataForm['yearReport'];
		$data['result'] = $this->pulsa->trxReport($dataForm['yearReport']);
		$this->load->view($this->folderView.'listReportTrxPulsa', $data);
	}

	//$route['be/pulsa/summary'] = 'backend/backend_pulsa/formSummaryReport';
	public function formSummaryReport() {
		if($this->username != null) {
			$data['form_header'] = "Summary Report";
            $data['icon'] = "icon-list";
		    $data['form_reload'] = 'be/pulsa/summary';
			//$data['result'] = $this->pulsa->trxReport("2014");
            $this->setTemplate($this->folderView.'formSummaryReport', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

    //$route['be/pulsa/summary/list'] = 'backend/backend_pulsa/listSummaryReport';
    public function listSummaryReport() {
		$dataForm = $this->input->post(NULL, TRUE);
		$data['range'] = $dataForm['yearReportFrom']." - ".$dataForm['yearReportTo'];
		$data['result'] = $this->pulsa->summaryReport($dataForm['yearReportFrom'], $dataForm['yearReportTo']);
		$this->load->view($this->folderView.'listSummaryReport', $data);
	}





	 //$route['be/pulsa/client'] = 'backend/backend_pulsa/formInputClient';
	 public function formInputClient() {
	 	if($this->username != null) {
			$data['form_header'] = "Input Customer";
            $data['icon'] = "icon-pencil";
		    $data['form_reload'] = 'be/pulsa/client';
			//$data['result'] = $this->pulsa->trxReport("2014");
            $this->setTemplate($this->folderView.'formInputClient', $data);
		} else {
			redirect('backend', 'refresh');
		}
	 }

	 //$route['be/pulsa/client/list'] = 'backend/backend_pulsa/getListClient';
	 public function getListClient() {
		$data['result'] = $this->pulsa->getListClient();
		$this->load->view($this->folderView.'listClient', $data);
	 }

	 //$route['be/pulsa/client/(:any)/(:any)'] = 'backend/backend_pulsa/getDataClient/$1/$2';
	 public function getDataClient($client, $no_tujuan) {
	 	$data['result'] = $this->pulsa->getDataClient($client, $no_tujuan);
		echo json_encode($data['result']);
	 }

     //$route['be/pulsa/client/no/(:any)'] = 'backend/backend_pulsa/getClientByNo/$1';
	 public function getClientByNo($no_tujuan) {
	 	$data['result'] = $this->pulsa->getDataClient($no_tujuan);
		echo json_encode($data['result']);
	 }

 	 //$route['be/pulsa/client/save'] = 'backend/backend_pulsa/saveNewClient';
	 public function saveNewClient() {
	 	$data = $this->input->post(NULL, TRUE);
		$arr = $this->pulsa->saveNewClient($data);
	    echo json_encode($arr);
	 }

	 //$route['be/pulsa/client/delete/(:any)'] = 'backend/backend_pulsa/deleteClientByPhoneNo/$1';
	 public function deleteClientByPhoneNo($no_tujuan) {
	 	$arr = $this->pulsa->deleteClientByPhoneNo($no_tujuan);
	    echo json_encode($arr);
	 }

	 //$route['be/pulsa/trx/export/excel']  = 'backend/backend_pulsa/exportToExcel';
	 public function exportToExcel() {
	 	//$this->load->library('Excel_generator');
	 	$xxx = $this->load->database("tes_newera4", TRUE);
		$query = $xxx->get('pulsa_no_client');
	 	print_r($query);
	 	/*$this->excel_generator->set_query($query);
        $this->excel_generator->set_header(array('Client', 'No Tujuan'));
        $this->excel_generator->set_column(array('client', 'no_tujuan'));
        $this->excel_generator->set_width(array(30, 30));
        $this->excel_generator->exportTo2007('Member Report'); */
	 }

	 /*-------------------------
	  API FOR ANDROID
	  ------------------------*/

	  //$route['api/pulsa/login']  = 'backend/backend_pulsa/postLoginPulsaUser';
	  public function postLoginPulsaUser() {
	  		$data = $this->input->post(NULL, TRUE);
			$srvReturn = $this->pulsa->apiUserPulsaLogin($data['username'], $data['password']);
			echo json_encode($srvReturn);
	  }

	  /*---------------------
	   * MONIFY
	   * -------------------*/
	   //$route['money']  = 'backend/backend_pulsa/formInputMonify';
	   public function formInputMonify() {
	   		$data['form_header'] = "Monify Input";
	        $data['icon'] = "icon-pencil";
			$data['form_reload'] = 'money';
			$data['curdate'] = date("Y-m-d");
			try {
				$this->checkSessionBE();
				$this->ifDion();
			} catch(Exception $e) {
				$data['err_msg'] = $e->getMessage();
			}
			$this->setTemplate($this->folderView.'monifyFormInput', $data);
	   }
 	   //$route['money/list']  = 'backend/backend_pulsa/getListMonify';

}

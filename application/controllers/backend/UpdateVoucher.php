<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class UpdateVoucher extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->folderView = "backend/update/";
            $this->load->model('backend/update/Update_model', 'mUpdate');

        }

        public function index(){

            if($this->username != null) {
                $data['form_header'] = "UPDATE CURRENCY VOUCHER FROM USD TO IDR";
                $data['icon'] = "icon-money";
                $data['form_reload'] = 'be/update/voucher/form';
                $this->setTemplate($this->folderView.'formUpdateVoucher', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        public function getDetailVoucher(){

            //$vch = $this->input->post('vch');
            $vch = $this->input->post('vch');

            $arr = $this->mUpdate->getVoucherKey($vch);

            if($arr == null){

                $arr['result'] = $this->mUpdate->getVoucherNo($vch);
            }else{

                $arr['result'] = $this->mUpdate->getVoucherKey($vch);
            }

            //echo json_encode($arr);
            $this->load->view($this->folderView.'detailVoucher', $arr);
        }

        public function actUpdate(){

            $voucherkey = $this->input->post('voucherkey');
            $curr = $this->input->post('voucheramt');

            $currRp = $curr * 13000;

            $result = $this->mUpdate->updateVoucher($voucherkey, $curr, $currRp);

            //print_r($result);
            if($result == 1){
                
                setSuccessMessage('update success');
            }else{

                setErrorMessage('fail, try again');
            }
        }
    }
<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Idc_promo extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = 'backend/klinkpromo/';
		$this->load->model("backend/klinkpromo/m_idc_promo", "m_idc_promo");	
	}
	
	//$route['be/promo/idc']  = 'backend/klinkpromo/idcPromo/idcPromoForm';
	public function idcPromoForm() {
		if($this->username != null) {
            $x['form_header'] = "Initiative Dynamic Challenge";
            $x['icon'] = "icon-search";
		    $x['form_reload'] = 'be/promo/idc';
            
            $this->setTemplate($this->folderView.'idcPromoForm', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
    //$route['be/promo/idc/result/(:any)'] = 'backend/klinkpromo/idc_promo/idcPromoResult/$1';
    public function idcPromoResult($idmember) {
    	$data = $this->input->post(NULL, TRUE);
		$data['result'] = $this->m_idc_promo->totalAkumulasiPoin($idmember);
		$data['bnsPeriod'] = $this->m_idc_promo->listBnsPeriod($idmember);
		$this->load->view($this->folderView.'idcPromoResult', $data);
    }
	
	//$route['be/promo/detail'] = 'backend/klinkpromo/idc_promo/idcPromoDetail';
	public function idcPromoDetail() {
		$data = $this->input->post(NULL, TRUE);
		$data['resultHeader'] = $this->m_idc_promo->checkValidIDC($data['memberid'], $data['bnsperiod']);
		$data['result'] = $this->m_idc_promo->getListIDCDetail($data['memberid'], $data['bnsperiod']);
		$this->load->view($this->folderView.'idcPromoDetail', $data);
	}
	
	//$route['be/promo/idc/report'] = 'backend/klinkpromo/idc_promo/formIdcReport';
	public function formIdcReport() {
		if($this->username != null) {
            $x['form_header'] = "Initiative Dynamic Challenge Report";
            $x['icon'] = "icon-search";
		    $x['form_reload'] = 'be/promo/idc/report';
            
            $this->setTemplate($this->folderView.'formIdcReport', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
	//$route['be/promo/idc/report/result'] = 'backend/klinkpromo/idc_promo/formIdcReportResult';
	public function formIdcReportResult() {
		$data = $this->input->post(NULL, TRUE);
		//print_r($data);
		$data['result'] = $this->m_idc_promo->getListIDCWinner($data['rep_bnsperiod'], $data['orderby']);
		//print_r($data['result']);
		$this->load->view($this->folderView.'formIdcReportResult', $data);
	}
	
	//$route['be/promo/idc/report/print'] = 'backend/klinkpromo/idc_promo/formIdcReportPrint';
	public function formIdcReportPrint() {
		
	}
}
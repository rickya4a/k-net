<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Iec_promo extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = 'backend/klinkpromo/';
		$this->load->model("backend/klinkpromo/m_iec_promo", "iec_promo");	
	}
    
    //$route['be/iec']  = 'backend/klinkpromo/idcPromo/idcPromoForm';
	function iecPromoForm() {
		if($this->username != null) {
            $x['form_header'] = "Initiative Elite Challenge";
            $x['icon'] = "icon-search";
		    $x['form_reload'] = 'be/iec';
            
            $this->setTemplate($this->folderView.'iecPromoForm', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
    
    function iecPromoResult($idmember) {
    	$data = $this->input->post(NULL, TRUE);
        $x['idmember'] = $idmember;
        $x['flag'] = '1';
        $x['header'] = $this->iec_promo->headerIEC($idmember);
		$x['result'] = $this->iec_promo->totalSummaryIEC($idmember);
		//$x['bnsPeriod'] = $this->iec_promo->listBnsPeriod($idmember);
		$this->load->view($this->folderView.'iecPromoResult',$x);
    }
    
    function iecPromoDetail($month,$year,$idmember,$flag){
        $x['idmember'] = $idmember;
        $x['bnsmonth'] = $month." / ".$year;
        $bnsperiod = $year." - ".$month." - 01";
        $x['header'] = $this->iec_promo->headerIEC($idmember);
        $x['detail'] = $this->iec_promo->detRecIEC($idmember,$bnsperiod);
        //echo "isinya ".$month." - ".$year." - ".$rekruit."<br>";
        //echo "<input type=\"button\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" class=\"btn btn-small btn-warning\" value=\"&lt;&lt; Back\">";
        //$x['detail'] = $this->iec_promo->getDetailIEC($month,$year,$rekruit);
        if($flag == '1'){
            $this->load->view($this->folderView.'iecDetResult',$x);    
        }else{
            $this->load->view('webshop/iecDetRes',$x);
        }
        
    }
 }
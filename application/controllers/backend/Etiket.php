<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Etiket extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = "backend/etiket/";
		$this->load->service('backend/be_product_service');
		$this->load->model('backend/be_etiket_model');
		$this->load->model("dtc/Mdtc_mbr",'mdtc');
		$this->dateTime = date("Y-m-d h:m:s");
	}
	
//$route['etiket/cat/input'] = 'backend/Etiket/inputEtiketCategory';
	public function inputEtiketCategory() {
	    
		
		if($this->username != null) {
			$data['form_header'] = "E-ticket Category Maintenane";
			$data['form_action'] = base_url('etiket/cat/input/save');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket/cat/input';
			//$data['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket(); 
			$this->setTemplate($this->folderView.'inputEtiketCategory', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
//$route['etiket/cat/input/save'] = 'backend/Etiket/saveEtiketCategory';
	public function saveEtiketCategory() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("Simpan Data gagal..");
		try {
			$checkDouble1 = $this->be_etiket_model->checkDouble("master_etiket_cat", "shortname", $data['shortname']);
			$checkDouble2 = $this->be_etiket_model->checkDouble("master_etiket_cat", "category_desc", $data['category_desc']);
			//insert form data to array
			$arrInput = array(
				"category_desc" => $data['category_desc'],
				"shortname" => $data['shortname'],
				"status" => $data['status'],
				"createnm" => $this->username
			);
			
			$dbqryx = $this->load->database("db_ecommerce", TRUE);
			$res = $dbqryx->insert("master_etiket_cat", $arrInput);
			if($res > 0) {
				$hasil = jsonTrueResponse(null, "Simpan Kategori E-Ticket $data[category_desc] berhasil..");
			}
		} catch(Exception $e) {
			$hasil = jsonFalseResponse($e->getMessage());
		}
		echo json_encode($hasil);
	}
//$route['etiket/cat/id/(:any)'] = 'backend/Etiket/getEtiketCategoryById/$1';
	public function getEtiketCategoryById($id) {
		$res = jsonFalseResponse("Data tidak ditemukan..");
		$hasil = $this->be_etiket_model->getEtiketCategoryById($id);	
		if($hasil != null) {
			$res = jsonTrueResponse($hasil);
		}
		echo json_encode($res);
	}
//$route['etiket/cat/update/save'] = 'backend/Etiket/updateEtiketCategory';	
	public function updateEtiketCategory() {
		$data = $this->input->post(NULL, TRUE);
		$hasil = jsonFalseResponse("Update Data gagal..");
		try {
			//$checkDouble1 = $this->be_etiket_model->checkDouble("master_etiket_cat", "shortname", $data['shortname']);
			//$checkDouble2 = $this->be_etiket_model->checkDouble("master_etiket_cat", "category_desc", $data['category_desc']);
			//insert form data to array
			$arrInput = array(
				"category_desc" => $data['category_desc'],
				"shortname" => $data['shortname'],
				"status" => $data['status'],
				"updatenm" => $this->username,
				"updatedt" => $this->dateTime,
				"last_log_activity" => "Update Data"
			);
			
			$dbqryx = $this->load->database("db_ecommerce", TRUE);
			$dbqryx->where('cat_id',$data['cat_id']);
            $res = $dbqryx->update('master_etiket_cat',$arrInput);
			if($res > 0) {
				$hasil = jsonTrueResponse(null, "Update Kategori E-Ticket $data[category_desc] berhasil..");
			}
		} catch(Exception $e) {
			$hasil = jsonFalseResponse($e->getMessage());
		}
		echo json_encode($hasil);
	}
//$route['etiket/cat/list'] = 'backend/Etiket/getListEtiketCategory';
	public function getListEtiketCategory() {
		$data['listCatEtiket'] = $this->be_etiket_model->getListAllEtiketCategory();
		$this->load->view($this->folderView.'getListEtiketCategory', $data);
		
	}


//$route['etiket'] = 'backend/Etiket/getInputEtiket';
	public function getInputEtiket() {
	    
		
		if($this->username != null) {
			$data['form_header'] = "Input E-ticket";
			$data['form_action'] = base_url('etiket/save');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket';
			$data['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket(); 
			$data['listKota'] = $this->be_etiket_model->listAllLocation();
			if($this->username != "DION") {
				//$this->setTemplate($this->folderView.'getInputEtiket', $data);
				$this->setTemplate($this->folderView.'getInputEtiketNew', $data);
			} else {
				$this->setTemplate($this->folderView.'getInputEtiketNew', $data);
			}
		} else {
			redirect('backend', 'refresh');
		}
	}
//	$route['etiket/save'] = 'backend/Etiket/saveInputEtiket';
	public function saveInputEtiket()
	{
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		$data = $this->input->post(NULL, TRUE);

		{
			try {
				$srvReturn = $this->be_etiket_model->saveInputEtiket();

			} catch (Exception $e) {
				$srvReturn = jsonFalseResponse($e->getMessage());
			}
		}

//		echo in_array(null, $data, true);
		echo json_encode($srvReturn);

	}
	public function submitAttendee()
	{
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		$data = $this->input->post(NULL, TRUE);

		{
			try {
				$srvReturn = $this->be_etiket_model->submitAttendee();

			} catch (Exception $e) {
				$srvReturn = jsonFalseResponse($e->getMessage());
			}
		}

//		echo in_array(null, $data, true);
		echo json_encode($srvReturn);

	}

	public function saveInputEtiketxd() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		try {
			if($this->form_validation->run() == TRUE) {
				$srvReturn = $this->be_etiket_model->saveInputEtiket();

			}
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
		}
		echo json_encode($srvReturn);
	}

//$route['etiket/list'] = 'backend/Etiket/getListAllTiket';
	public function getListAllTiket($type = "array") {
		$data['listPrd'] = null;
		try {
			$data['listPrd'] = $this->be_etiket_model->getlistAllEtiket();
		} catch(Exception $e) { }
		if($type == "array") {
			$data['username'] = $this->username;
			$this->load->view($this->folderView.'getListAllEtiket', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrd']));
		}
	}
//$route['etiket/getEditTiket/(:any)'] = 'backend/Etiket/getEditTiket/$1';
	public function getEditTiket( $value) {
		$srvReturn = jsonFalseResponse();
		try {
			/*if($this->username != "DION") {
				$srvReturn = $this->be_etiket_model->getData($value);
			} else {*/
				$srvReturn = $this->be_etiket_model->getDataBaru($value);
			//}
			//$srvReturn = $this->be_etiket_model->getDataBaru($value);
		} catch(Exception $e) {}
//		echo json_encode($srvReturn);
		echo json_encode(jsonTrueResponse($srvReturn));

	}
	
	//$route['etiket/status/update'] = 'backend/Etiket/updateStatusEtiket';
	public function updateStatusEtiket() {
	  $data = $this->input->post(NULL, TRUE);
	  
	  $dbqryx  = $this->load->database("db_ecommerce", TRUE);
	  $upd = array(
			'status' => $data['status']
		);
	  $dbqryx->where('id',$data['id']);
      $res = $dbqryx->update('db_ecommerce.dbo.master_etiket',$upd);
	  if($res > 0) {
	     $arr = jsonTrueResponse(null, "Status E-tiket berhasil diubah..");
	  } else {
		 $arr = jsonFalseResponse(null, "Status E-tiket gagal diubah..");
	  }
	  echo json_encode($arr);
	}
	
	//$route['etiket/updateNew'] = 'backend/Etiket/updateEtiketNew';
	public function updateEtiketNew()
	{
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		$data = $this->input->post(NULL, TRUE);

		{
			try {
				$data = $this->input->post(NULL, TRUE);

				$arr_data = array(
					'nama'=>$data['nm_event'],
					'lokasi'=>$data['loc_event'],
					'kode_lokasi'=>$data['loc_id'],
					'max_online'=> $data['max_online'],
					'max_kuota' =>$data['max_online'],
					'act_online'=>0,
					'max_offline'=>$data['max_offline'],
					'act_offline'=>0,
					'total'=>0,
					'act_total'=>0,
					'event_date'=>($data['event_date']),
					'pembicara'=>$data['pembicara'],
					'exp_date_online'=>($data['exp_date_online']),
					'exp_date_offline'=>($data['exp_date_offline']),
					'price_online'=>$data['price_online'],
					'price_offline'=>$data['price_offline'],
					'remark'=>$data['remark'],
					'createnm'=>$this->username,
					'earlybird_date'=>$data['bird_date'],
					'price_earlybird_online'=>$data['price_bird'],
					'cat_id' => $data['event_cat_id']
				);
				$id=$data['id'];

				$srvReturn = $this->be_etiket_model->updateEtiket($id,$arr_data);

			} catch (Exception $e) {
				$srvReturn = jsonFalseResponse($e->getMessage());
			}
		}
		echo json_encode(jsonTrueResponse($srvReturn));

	}
	
    //$route['etiket/update'] = 'backend/Etiket/updateEtiket';
	public function updateEtiket()
	{
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		$data = $this->input->post(NULL, TRUE);

		{
			try {
				$data = $this->input->post(NULL, TRUE);

				$arr_data = array(
					'nama'=>$data['nm_event'],
					'lokasi'=>$data['loc_event'],
					'kode_lokasi'=>$data['loc_id'],
					'max_online'=>$data['max_online'],
					'act_online'=>0,
					'max_offline'=>$data['max_offline'],
					'act_offline'=>0,
					'total'=>0,
					'act_total'=>0,
					'event_date'=>($data['event_date']),
					'pembicara'=>$data['pembicara'],
					'exp_date_online'=>($data['exp_date_online']),
					'exp_date_offline'=>($data['exp_date_offline']),
					'price_online'=>$data['price_online'],
					'price_offline'=>$data['price_offline'],
					'remark'=>$data['remark'],
					'createnm'=>$this->username,
					'earlybird_date'=>$data['bird_date'],
					'price_earlybird_online'=>$data['price_bird'],
					'cat_id' => $data['event_cat_id']
				);
				$id=$data['id'];

				$srvReturn = $this->be_etiket_model->updateEtiket($id,$arr_data);

			} catch (Exception $e) {
				$srvReturn = jsonFalseResponse($e->getMessage());
			}
		}
		echo json_encode(jsonTrueResponse($srvReturn));

//		echo in_array(null, $data, true);
//		echo json_encode($srvReturn);

	}


//$route['etiket/klaim'] = 'backend/Etiket/klaim';
	public function klaim()
	{
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		$data = $this->input->post(NULL, TRUE);

		{
			try {
				$data = $this->input->post(NULL, TRUE);

				$arr_data = array(
					'valid_dfno'=>$data['idmbr2'],
					'valid_fullnm'=>$data['display_info2'],
					'email'=>$data['email'],
					'notelp'=>$data['notelp'],
                    'remark'=>$data['remark'],

					'createnm'=>$this->username
				);
				$id=$data['id'];

				$srvReturn = $this->be_etiket_model->klaimEtiket($id,$arr_data);

			} catch (Exception $e) {
				$srvReturn = jsonFalseResponse($e->getMessage());
			}
		}
		echo json_encode(jsonTrueResponse($srvReturn));

//		echo in_array(null, $data, true);
//		echo json_encode($srvReturn);

	}

//$route['etiket/list2'] = 'backend/Etiket/getListAllTRXTiket';
	public function getListAllTRXTiket($type = "array") {
//		$data['listPrd'] = null;
//		$data['form_header'] = "Klaim E-ticket";
//		$data['form_action'] = base_url('etiket/save');
//		$data['icon'] = "icon-pencil";
//		$data['form_reload'] = 'etiket';
//		try {
//			$data['listPrd'] = $this->be_etiket_model->getListAllTRXTiket();
//		} catch(Exception $e) { }
//		if($type == "array") {
//			$this->setTemplate($this->folderView.'getListAllTRXEtiket', $data);
//
//
//		} else {
//			echo json_encode(jsonTrueResponse($data['listPrd']));
//		}

		if($this->username != null) {
			$data['form_header'] = "Klaim E-ticket";
			$data['form_action'] = base_url('etiket/klaim');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket/list2';
			$this->setTemplate($this->folderView.'formListing', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

//$route['etiket/list3'] = 'backend/Etiket/getListKlEtiket';
	public function getListKlEtiket($type = "array") {
		$data['listPrd'] = null;
		try {
			$data['listPrd'] = $this->be_etiket_model->getListAllTRXTiket();
		} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllTRXEtiket', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrd']));
		}
	}

//$route['etiket/delete/(:any)'] = 'backend/Etiket/deletetkt/$1';
	public function deletetkt($id) {
		try {
			$data['listPrd'] = $this->be_etiket_model->deletetiket($id);
		} catch(Exception $e) { }
		{
			echo json_encode(jsonTrueResponse());
		}
	}

//$route['etiket/klaimEtiket/(:any)'] = 'backend/Etiket/KlaimEtiket/$1';
	public function KlaimEtiket($value) {
		$srvReturn = jsonFalseResponse();
		try {
			$srvReturn = $this->be_etiket_model->getData2($value);
		} catch(Exception $e) {}
//		echo json_encode($srvReturn);
		echo json_encode(jsonTrueResponse($srvReturn));

	}

	function cetak2($id){

//		$srvReturn = $this->be_etiket_model->getData2($id);
//			$dt['x']=$this->get_web_page('https://chart.googleapis.com/chart?cht=qr&chl=ET0300009&chs=160x160&chld=L|0');

		$dt['res1'] = array(1);
//				$this->be_etiket_model->getData2($id);
		$this->load->view($this->folderView.'printTiket', $dt);


//			$this->load->view('webshop/printTiket',$dt);

	}
	function get_web_page( $url, $cookiesIn = '' ){
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => true,     //return headers in addition to content
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Cert
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_COOKIE         => $cookiesIn
		);

		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$rough_content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = implode("; ", $matches['cookie']);

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['headers']  = $header_content;
		$header['content'] = $body_content;
		$header['cookies'] = $cookiesOut;
		return $header;
	}










//$route['etiket/report'] = 'backend/Etiket/formInputReport';
	public function formInputReport() {
		if($this->username != null) {
			$data['form_header'] = "E-tiket Report";
			$data['form_action'] = base_url('etiket/cetak');
			$data['action'] = base_url('etiket/cetak');

			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket/report';
			$data['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket();
			$this->setTemplate($this->folderView.'formReportEtiket', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
	
//$route['etiket/listPesertaByAcara/(:any)'] = 'backend/Etiket/listPesertaByAcara/$1'
public function listPesertaByAcara($idacara) {
	$dt['report'] = $this->be_etiket_model->rekapTransaksiTiketByIdAcara($idacara);
	if($this->username == "DION" || $this->username == "DWI") {
		$this->load->view($this->folderView.'listPesertaByAcaraDion',$dt);
	} else {
		$this->load->view($this->folderView.'listPesertaByAcara',$dt);
	}
}	

//$route['etiket/pindahJadwal'] = 'backend/Etiket/pindahJadwal';
public function pindahJadwal() {
	$data = $this->input->post(NULL, TRUE);
	
	$data['res'] = $this->be_etiket_model->getAcaraOnlineByCategory($data['cat_id']);
	
	$data['listOrderno'] = set_list_array_to_string($data['order_no']);
	$data['prev_acara'] = $this->be_etiket_model->getAcaraByListOrderno($data['listOrderno']);
	$this->load->view($this->folderView.'pindahJadwal',$data);
	backToNextForm();
}
//$route['etiket/pindahJadwal/save'] = 'backend/Etiket/pindahJadwalSave';
public function pindahJadwalSave() {
	$data = $this->input->post(NULL, TRUE);
	//echo "<pre>";
	//print_r($data);
	//echo "<pre>";
	$upd = $this->be_etiket_model->pindahJadwal($data['opsi'],$data['list_orderno']);
	if($upd['response'] == "true") {
		echo setSuccessMessage($upd['message']);
	} else {
		echo setErrorMessage($upd['message']);
	}
	/*
	yg diubah :
	1. ecomm_trans_det_prd (prdcd, prdnm)
	2. trx_etiket (id_etiket)
	*/
}

//$route['etiket/resendsms/(:any)/(:any)'] = 'backend/Etiket/resendsms/$1/$2';
public function resendsms($param, $value) {
	if($param == "notiket") {
		$res = $this->mdtc->smsByNoTiket($value);
	} else if($param == "orderno") {
	    $res = $this->mdtc->sms2($value);
	}
	
	echo json_encode(jsonTrueResponse(null, "SMS sudah dikirim.."));
}

//$route['etiket/getReport'] = 'backend/Etiket/ReportEtiketData/0';
//$route['etiket/cetak'] = 'backend/Etiket/ReportEtiketData/1';
	function ReportEtiketData($id){
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;
		$category = $this->input->post('category');
		$tglawal =  $this->input->post('so_from');
		$tglakhir = $this->input->post('so_to');
		$search = $this->input->post('search');
		$jenis = $this->input->post('jenis');
		$cat_id = $this->input->post('cat_id');
		$id_acara = $this->input->post('opsi');

//			$year = $this->input->post('year');
		$dt['report'] = 1;
		//$dt['report'] = $this->be_etiket_model->getReportEtiket($tglawal,$tglakhir,$category,$search);
		//$dt2['report'] = $this->be_etiket_model->getReportEtiket2($tglawal,$tglakhir,$category,$search);
//		$dt['report6'] = $this->sm_office->getReportBonus2($idmembers);

//		$dt['free'] = $this->sm_office->isfree($idmembers);
//		echo 'dxdxdd '.$category;
//		echo $tglakhir;
		if($jenis==1){
			if($id==0){
			    if($category != "6") {
					//$dt['report'] = $this->be_etiket_model->getReportEtiket($tglawal,$tglakhir,$category,$search, $cat_id);
					$dt['report'] = $this->be_etiket_model->reportEtiketNew($tglawal,$tglakhir,$category,$search, $cat_id, $id_acara);
					$this->load->view($this->folderView.'getListReportEtiket',$dt);
				} else {
				    $dt['report'] = $this->be_etiket_model->rekapTransaksiTiket($cat_id, $id_acara, $id_acara, $tglawal,$tglakhir);
					if($dt['report'] == null) {
					   echo setErrorMessage("Data tidak ditemukan");
					} else {
					   $this->load->view($this->folderView.'rekapPenjualanTiket',$dt);	
					}
					//print_r($dt['report']);
				}
			}else{
				$dt2['report'] = $this->be_etiket_model->getReportEtiket2($tglawal,$tglakhir,$category,$search, $cat_id, $id_acara);
				$this->load->view($this->folderView.'excel',$dt2);
			}
		}else if($jenis==2){
			$dt3['report'] = $this->be_etiket_model->getReportFinance($tglawal,$tglakhir,$category,$search, $cat_id, $id_acara);
			$this->load->view($this->folderView.'excelfinance',$dt3);
		}

	}


	public function kirimEmail2()
	{
		$ci = get_instance();
		$ci->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "aldi.web16@gmail.com";
		$config['smtp_pass'] = "surabaya21";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";


		$ci->email->initialize($config);

		$ci->email->from('aldi.web16@gmail.com', 'Rinaldhi Cahyono');

//		$email = $this->input->post('email');
//		$subject = $this->input->post('subject');
		$pesan = ('hsahd');

		$ci->email->to('aldi_web16@yahoo.com');
		$ci->email->subject('test');
		$ci->email->message($pesan);
		if ($this->email->send()) {
			echo "<script>
		alert('Email sent!');
		</script>";
		} else {
			show_error($this->email->print_debugger());
		}
	}

	public function formCheckinx() {
		if($this->username != null) {
			$data['form_header'] = "Check-in Acara";
			$data['action'] = base_url('etiket/cek');
			$data['isiTabel']=$this->mdtc->tabelData();
			$data['alamat']=$this->mdtc->getAcara2();
			$data['bank']=$this->mdtc->getBank();
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket/cekin';
			$this->setTemplate($this->folderView.'checkin', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

	function formCheckin()
	{
		if ($this->username != null) {
			$x['form_header'] = "Check-in Acara";
			$x['icon'] = 'icon-list';
			$x['form_reload'] = 'etiket/cekin';
			$x['isiTabel'] = $this->mdtc->tabelData();
			$x['alamat'] = $this->mdtc->getAcara2();
			$x['bank'] = $this->mdtc->getBank();
			$x['key'] = "key";
			$x['payID'] = "payID";
			$x['action'] = base_url('etiket/cek');

			$this->setTemplate($this->folderView . 'checkin', $x);
		} else {
			redirect('backend', 'refresh');
		}
	}
	public function validasitiket(){
		$id = $this->input->post("idm");
		$acara = $this->input->post("acara");
		$opsi=$this->input->post("opsi");


		//	$ID = $this->Mdtc_mbr/checkID($data);
		//echo "Nandang";
		//	echo $id;
		//	$this->load->model("dtc/Mdtc_mbr",$id);
		if($opsi==0)
		{
		$nama=$this->be_etiket_model->validasitiket($id);
		}
		else
		{
		$nama=$this->be_etiket_model->validasitiket2($id, $acara);
		}
		//echo $nama[0]->fullnm;
		//print_r ($nama) ;
		//array("tes"=> "sdsds");
		echo json_encode($nama);
	}
	
	//$route['etiket/merchandise/set/(:any)'] = 'backend/Etiket/formSetMerchandise/$1';
	public function formSetMerchandise($etiket_id) {
		$data['id_etiket'] = $etiket_id;
		$data['listTipe'] = $this->be_etiket_model->getListTipeMerchandise();
		$data['detailAcara'] = $this->be_etiket_model->getDataAcaraByID($etiket_id);
		$data['listPrdBundling'] = $this->mdtc->getListPrdBundlingTiketById($etiket_id);
		$data['uangMakan'] = $this->be_etiket_model->getHargaProduct($etiket_id, "UM001");
		$this->load->view($this->folderView.'formSetMerchandise',$data);
	}
	
	//$route['etiket/merchandise/getPrice'] = 'backend/Etiket/getHargaProduct';
	function getHargaProduct() {
		$param = $this->input->post(NULL, TRUE);
		$hasil = $this->be_etiket_model->getHargaProduct($param['id_etiket'], $param['kode_produk']);
		
		echo json_encode($hasil);
	}
	
	//$route['etiket/merchandise/save'] = 'backend/Etiket/saveMerchandise';
	public function saveMerchandise() {
		$param = $this->input->post(NULL, TRUE);
		
		$jum = count($param['kode_produk']);
		
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		
		$dbqryx->trans_start();
		
		$dbqryx->where('etiket_id', $param['etiket_id']);
		$dbqryx->delete('master_etiket_merchandise_product');
		
		for($i=0; $i<$jum; $i++) {
		
			$array = array(
				"etiket_id" => $param['etiket_id'],
				"kode_produk" => $param['kode_produk'][$i],
				"nama" => $param['nama'][$i],
				"tipe" => $param['tipe'][$i],
				"harga" => $param['harga'][$i],
				"status" => $param['status'][$i],
			);
			
			$dbqryx->insert('master_etiket_merchandise_product', $array);
			
			/*echo "<pre>";
			print_r($array);
			echo "</pre>";*/
		}
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            $arrResult = array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            $arrResult = array(
              "response" => "true",
			  "message" => "Simpan merchandise berhasil.."
			);
        } 
		
		echo json_encode($arrResult);
		
	}
	
	//$route['etiket/merchandise/getlist/(:any)/(:any)'] = 'backend/Etiket/listUkuranWarna/$1/$2';
	public function listUkuranWarna($pilihan, $tipe_merchandise) {
		if($pilihan == "ukuran") {
			$table = "master_etiket_size_merchandise";
		} else if($pilihan == "warna") {
			$table = "master_etiket_ukuran_merchandise";
		}
	}
	
	//$route['etiket/location/input'] = 'backend/Etiket/formLocationInput';
	public function formLocationInput() {
		if($this->username != null) {
			$data['form_header'] = "E-ticket Location Maintenane";
			$data['form_action'] = base_url('etiket/cat/input/save');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket/location/input';
			//$data['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket(); 
			$this->setTemplate($this->folderView.'inputLocation', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
	//$route['etiket/location/check/(:any)/(:any)'] = 'backend/Etiket/checkDoubleLocation/$1/$2';
	public function checkDoubleLocation($param, $value) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select($param);
		$dbqryx->from('master_etiket_kota');
		$dbqryx->where($param, $value);
		$result = $dbqryx->get()->result();	
		
		if($result != null) {
			$arr = jsonTrueResponse(null, "Double kode/nama lokasi..");
		} else {
			$arr = jsonFalseResponse("ok..");
		}
		
		echo json_encode($arr);
	}
	//$route['etiket/location/save'] = 'backend/Etiket/saveLocation';
	public function saveLocation() {
		$data = $this->input->post(NULL, TRUE);
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		
		$dbqryx->select('kode_kota');
		$dbqryx->from('master_etiket_kota');
		$dbqryx->where('kode_kota', $data['kode_kota']);
		$dbqryx->or_where('nama_kota', $data['nama_kota']);
		$result = $dbqryx->get()->result();	
		
		if($result != null) {
			$arr = jsonFalseResponse("Nama kota / Kode Kota sudah ada di database..");
		} else {
			$detail = array(
				"kode_kota" => $data['kode_kota'],
				"nama_kota" => $data['nama_kota'],
			);
			$res = $dbqryx->insert('master_etiket_kota', $detail);
			$arr = jsonFalseResponse("Simpan data Kota/Lokasi gagal..");
			if($res > 0) {
				$arr = jsonTrueResponse("Simpan Kota/Lokasi berhasil");
			} else {
				$arr = jsonFalseResponse("Simpan Kota/Lokasi gagal");
			}
			
		}
		echo json_encode($arr);
	}
	
	//$route['etiket/location/list'] = 'backend/Etiket/listLocation';
	public function listLocation() {
		
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('*');
		$dbqryx->from('master_etiket_kota');
		$result = $dbqryx->get()->result();	
		//print_r($result); 
	    
		//echo "tes";
		$data['listLocation'] = $result;
		$this->load->view($this->folderView.'listLocation', $data);
	}
	
	//$route['etiket/integrity/check'] = 'backend/Etiket/checkIntegrity';
	public function checkIntegrity() {
		if($this->username != null) {
			$data['form_header'] = "E-ticket Integrity Check";
			$data['form_action'] = base_url('etiket/integrity/check/list');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'etiket/integrity/check';
			//$data['listTipeAcara'] = $this->be_etiket_model->getListCategoryTicket(); 
			$this->setTemplate($this->folderView.'checkIntegrity', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
	
	//$route['etiket/listEventByCat/(:any)'] = 'backend/Etiket/listEventByCat/$1';
	function listEventByCat($cat_id) {
		$mdReturn = jsonFalseResponse("Tidak ada acara yang aktif...");
		$res = $this->be_etiket_model->getAcaraOnlineByCategory($cat_id);
		//print_r($res);
		if($res != null) {
			$mdReturn = jsonTrueResponse($res);
		}
		echo json_encode($mdReturn);
	}
	
	//$route['etiket/integrity/check/list'] = 'backend/Etiket/checkIntegrityList';
	public function checkIntegrityList() {
		$data = $this->input->post(NULL, TRUE);
		if($data['tipe_check'] == "null_merchandise") {
			$data['result'] = $this->be_etiket_model->listTicketTanpaMerchandise();
			if($data['result'] == null) {
				echo setErrorMessage("Data tidak ditemukan..");
			} else {
				$this->load->view($this->folderView.'listTicketTanpaMerchandise', $data);
			}	
		} else if($data['tipe_check'] == "null_ip") {
			$data['result'] = $this->be_etiket_model->listTicketTanpaIP();
			if($data['result'] == null) {
				echo setErrorMessage("Data tidak ditemukan..");
			} else {
			    /*echo "<pre>";
				print_r($data['result']);
				echo "</pre>";*/
				$this->load->view($this->folderView.'listTicketTanpaIP', $data);
			}
		}
	}

}
?>
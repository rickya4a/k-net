<?php
/*
 create_by : suria atningsih
 create_date : April 02, 2018
 used for : listing voucher that have released or have claimed for event in April 11, 2018

*/
class voucher_list extends MY_Controller{
    public function __construct() {

        parent::__construct();
        $this->folderView = "backend/voucher_list/";
        $this->load->model('backend/be_voucher_list', 'm_vch');

    }

    public function listVch(){
        //$this->load->view('backend/voucher_list/v_voucher_list');
        //echo "test";
        $data['form_header'] = "Voucher List";
        $data['icon'] = "icon-list";
        $data['action'] = base_url('voucher_list/getList');
        $data['form_reload'] = 'voucher_list/listVch';
        $this->setTemplate($this->folderView.'v_voucher_list', $data);
    }

    function getList()
    {
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;

        $status = $this->input->post('status');


        if ($status == "Released") {
            $dt['list'] = $this->m_vch->getList($status);
            $this->load->view($this->folderView . 'ListVchReleased', $dt);
        }
        elseif ($status == "Identified") {
            $dt['list'] = $this->m_vch->getList($status);
            $this->load->view($this->folderView . 'ListVchIdentified', $dt);
        }
        elseif ($status == "Claimed") {
            $dt['list'] = $this->m_vch->getList($status);
            $this->load->view($this->folderView . 'ListVchClaimed', $dt);
        }

    }

    function getDetByID($id) {
        //$d = $this->m_vch->getDetByID($id);

        $dataH=$this->m_vch->getHeader($id);
        $orderno=$this->m_vch->getOrderNo($id);

        $dataD=$this->m_vch->getDetail($orderno[0]->orderno);

        $dataP=$this->m_vch->getPayment($orderno[0]->orderno);

        $dataS=$this->m_vch->getShipping($orderno[0]->orderno);

        //$data['header']=$this->m_vch->getHeader($id);

        $data['header']=$dataH;
//        foreach ($dataH as $row){
//            $data['header'][]=$row;
////            $data['detail'][]=$row;
////            $data['payment'][]=$row;
//        }

        $data['detail']=$dataD;
//        foreach($dataD as $row){
//            $data['detail'][]=$row;
//        }
        $data['payment']=$dataP;
//        foreach($dataP as $row){
//            $data['payment'][]=$row;
//        }
        $data['shipping']=$dataS;

        echo json_encode($data);
        //print_r($data);
    }


}//closing tag class


?>
<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');

class Be_promo extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if($this->username != null) {
			$this->folderView = "backend/promo/";
			$this->load->model("backend/be_promo_model", 'mPromo');
		} else {
			echo $this->sessExpMessage;
		}
	}
    
    //$route['be/promo/getSummary']  = 'backend/be_promo/getSummary';
    function getSummary(){
        if($this->username != null) {
            $x['form_header'] = "Print Summary Promo";
            $x['icon'] = "icon-print";
		    $x['form_reload'] = 'be/promo/getSummary';
            $x['stk'] = $this->mPromo->getIdstk();

            $this->setTemplate($this->folderView.'idc', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    //$route['be/promo/getSummary/act'] = 'backend/be_promo/getPrintSummaryAct';
    function getSummaryAct(){
        $dt = $this->input->post(NULL, TRUE);
        $x['sumPromo'] = $this->mPromo->getListPromo($dt);
        $x['from_date'] = date('Ymd',strtotime($dt['from_date']));
        $x['to_date'] = date('Ymd',strtotime($dt['to_date']));
        //if($x['sumPromo'] != null || $x['sumPromo'] != 0){
            $this->load->view($this->folderView.'summaryPromoRes', $x);
        //}
    }
    
    //$route['be/promo/details/(:any)/(:any)/(:any)'] = 'backend/be_promo/getSummaryDetails/$1/$2/$3';
    function getSummaryDetails($idstk,$from,$to){
        if($this->username != null) {
            $froms = date('Y/m/d',strtotime($from));
            $tos = date('Y/m/d',strtotime($to));
            $x['header'] = $this->mPromo->getHeadStk($idstk);
            $x['detSumm'] = $this->mPromo->getListPromoDet($idstk,$froms,$tos);
            
            $this->load->view($this->folderView.'promoDetsSummary', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    //$route['be/promo/printSummary/txt'] = 'backend/be_promo/printSummaryTxt';
    function printSummaryTxt(){
        $dt = $dt = $this->input->post(NULL, TRUE);
        //print_r($dt);
        $x['headSumm'] = $this->mPromo->getHeadPromoList($dt);
        $x['detSumm'] = $this->mPromo->getDetPromoList($dt);
        /*foreach($x['detSumm'][0]['detail'] as $row){
            echo "test ".$row->orderno;
            echo "<br>";
        }*/
        
        $this->load->view($this->folderView.'printSummTxt', $x);
    }
    
    //$route['be/promo/report']  = 'backend/Be_promo/getReport';
    function getReport(){
         if($this->username != null) {
            $x['form_header'] = "Report Claim Promo";
            $x['icon'] = "icon-file";
		    $x['form_reload'] = 'be/promo/report';
            $x['stk'] = $this->mPromo->getIdstk();

            $this->setTemplate($this->folderView.'getSummaryPromo', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }

	//$route['be/promo/rekrut'] = 'webshop/be_promo/checkPoinRewards';
	public function checkPoinRewards() {
		$x['form_header'] = "Check Point Promo Rekrut 100 BV";
        $x['icon'] = "icon-pencil";
	    $x['form_reload'] = 'be/promo/rekrut';
        $x['stk'] = $this->mPromo->getIdstk();
        $this->setTemplate($this->folderView.'checkPoinRewards', $x);
	}
	
	//$route['be/promo/rekrut/id/(:any)'] = 'backend/be_promo/getPointRekrutByID/$1';
	public function getPointRekrutByID($id) {
		$this->load->model("webshop/m_promo_rekrut", "promo");
		try {
			$res = $this->promo->checkValidMember($id);
			$data['promo'] = $this->promo->getListDataPromo("dfno", $id);
			$data['redemp'] =$this->promo->getListDataRedemptionRekrut("dfno", $id);	
			//print_r($data['promo']);
			$this->load->view($this->folderView."promoPoinResult", $data);
		} catch(Exception $e) {
			echo "<h4>".$e->getMessage()."<h4>";
		}
	}
    
    //REPORT PROMO POIN REWARD RECRUITER by ananech
    //$route['recruit/rptRedemp'] = 'backend/be_promo/getRptRecruitRedemp';
    function getRptRecruitRedemp(){
        if($this->username != null) {
			$x['form_header'] = "Redemption Point Recruiter Report ";
            $x['icon'] = "icon-list";
		    $x['form_reload'] = 'recruit/rptRedemp';
            $x['stk'] = $this->mPromo->getListIdstk();
            $this->setTemplate($this->folderView.'getRptRedempFrm', $x); 
		} else {
			redirect('backend', 'refresh');
		}	
     }
     
     //$route['recruit/redemp/list'] = 'backend/be_promo/listRecruitRedemp';
     function listRecruitRedemp(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['exportTo'] = $dt['exportto'];
            $x['listRedempRecruit'] = $this->mPromo->getlistRedempRecruit($dt);
            $x['from'] = date("d M Y",strtotime($dt['redemp_from']));
			$x['to'] = date("d M Y",strtotime($dt['redemp_to']));
            $x['froms'] = $dt['redemp_from'];
			$x['tos'] = $dt['redemp_to'];
            if($x['exportTo'] == '2'){
                $this->load->view($this->folderView.'redempListExc', $x);
            }else{
                $this->load->view($this->folderView.'recruitRedempList', $x);    
            }
			
        }else{
           redirect('backend', 'refresh'); 
        }
     }
     
     //$route['recruit/redemp/details/(:any)'] = 'backend/be_promo/detRecruitRedemp/$1';
     function detRecruitRedemp($cnno){
        if($this->username != null) {
            $x['cnno'] = $cnno;
			$x['form_header'] = "Redemption Point Recruiter Report ";
            $x['icon'] = "icon-list";
		    $x['form_reload'] = 'recruit/rptRedemp';
            $x['detail'] = $this->mPromo->detRedempRecruit($cnno);
            $this->load->view($this->folderView.'detRecruitRedemp', $x); 
		} else {
			redirect('backend', 'refresh');
		}	
     }
    
 	//$route['be/promo/anniv_kolagen'] = 'webshop/be_promo/checkUndianAnnivKolagen';
 	public function checkUndianAnnivKolagen() {
 		$x['form_header'] = "Check Undian Promo K-Collagen";
        $x['icon'] = "icon-pencil";
	    $x['form_reload'] = 'be/promo/anniv_kolagen';
        $x['stk'] = $this->mPromo->getIdstk();
        $this->setTemplate($this->folderView.'checkUndianAnnivKolagen', $x);
 	}
	
	//$route['be/promo/anniv_kolagen/id/(:any)'] = 'backend/be_promo/getPointAnnivKolagenByID/$1';
	public function getPointAnnivKolagenByID($id) {
		$this->load->model("webshop/m_promo_rekrut", "promo");
		try {
			$res = $this->promo->checkValidMember($id);
			$data['promo'] = $this->promo->checkUndianKolagen($id);	
			//print_r($data['promo']);
			$this->load->view($this->folderView."kolagenCheckUndianMember", $data);
		} catch(Exception $e) {
			echo "<h4>".$e->getMessage()."<h4>";
		}
	}
	
	 //$route['be/promo/anniv_kolagen/export'] = 'backend/be_promo/exportAnnivKolagen';
	 public function exportAnnivKolagen() {
	 	$x['form_header'] = "Export K-Collagen Anniv 2016";
        $x['icon'] = "icon-pencil";
	    $x['form_reload'] = 'be/promo/anniv_kolagen/export';
        //$x['stk'] = $this->mPromo->getIdstk();
        $this->setTemplate($this->folderView.'exportAnnivKolagen', $x);
	 }
      
     //$route['be/promo/anniv_kolagen/export/header'] = 'backend/be_promo/exportAnnivKolagenHeader';
	 public function exportAnnivKolagenHeader() {
	 	$listPromo = $this->mPromo->exportAnnivKolagenHeader();
		 header("Content-type: application/vnd.ms-excel");
	     header("Content-Disposition: attachment; filename=header.csv" );
	     header("Expires: 0");
	     header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	     header("Pragma: public");
		
		 $str = "idmember;nmmember;noktp;nohp;trx_date";
		 $str .= "\n";
		 foreach($listPromo as $dta) {
			$str .= $dta->dfno.";".$dta->fullnm.";".$dta->idno.";".$dta->tel_hp.";".$dta->etdt;
			$str .= "\n"; 
		 }
	     echo $str;
	 	 //$this->load->view($this->folderView.'exportAnnivKolagenHeader', $x);
	 }
     
     //$route['be/promo/anniv_kolagen/export/detail'] = 'backend/be_promo/exportAnnivKolagenDetail';
     public function exportAnnivKolagenDetail() {
	 	$listPromo = $this->mPromo->exportAnnivKolagenDetail();
		 header("Content-type: application/vnd.ms-excel");
	     header("Content-Disposition: attachment; filename=detail.csv" );
	     header("Expires: 0");
	     header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	     header("Pragma: public");
		
		 $str = "idmember;no_voucher;datetrx";
		 $str .= "\n";
		 foreach($listPromo as $dta) {
			$str .= $dta->dfno.";".$dta->vno.";".$dta->etdt;
			$str .= "\n";
		 }
	     echo $str;
	 	//$this->load->view($this->folderView.'exportAnnivKolagenDetail', $x);
	 }
	
	//$route['be/promo/config'] = 'backend/be_promo/promoConfig';
	public function promoConfig() {
		if($this->username != null) {
            $x['form_header'] = "Promo Configuration";
            $x['icon'] = "icon-pencil";
		    $x['form_reload'] = 'be/promo/config';
            $x['stk'] = $this->mPromo->getIdstk();
            $x['curdate'] = date("Y-m-d");
			$x['listPromo'] = $this->mPromo->getListPromoConfig();
			$x['view'] = $this->load->view($this->folderView.'promoConfigList', $x, TRUE);
            $this->setTemplate($this->folderView.'promoConfig', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
	//$route['be/promo/config/list'] = 'backend/be_promo/promoConfigList';
	public function promoConfigList() {
		$x['listPromo'] = $this->mPromo->getListPromoConfig();
		$this->load->view($this->folderView.'promoConfigList', $x);
	}
	//$route['be/promo/config/save'] = 'backend/be_promo/promoConfigSave';
	public function promoConfigSave() {
		$arr = jsonFalseResponse("Insert $form[name] Failed..");
		$form = $this->input->post(NULL, TRUE);
		$data = $this->mPromo->savePromoConfig($form);
		if($data > 0){
			$arr = jsonTrueResponse(null, "Insert $form[name] Success..");
		} 
		echo json_encode($arr);
	}
    
    //$route['be/promo/config/id/(:any)'] = 'backend/be_promo/getPromoById/$1';
	public function getPromoById($id) {
		$return = jsonFalseResponse("Invalid Kode..");
		$data = $this->mPromo->getListPromoConfigByID($id);
		if($data != null) {
			$return = jsonTrueResponse($data);
		}
		echo json_encode($return);
	}
    //$route['be/promo/config/update'] = 'backend/be_promo/updatePromoConfig';
	public function updatePromoConfig() {
		$arr = jsonFalseResponse("Update $form[name] Failed..");
		$form = $this->input->post(NULL, TRUE);
		$data = $this->mPromo->updatePromoConfig($form);
		if($data > 0){
			$arr = jsonTrueResponse(null, "Update $form[name] Success..");
		} 
		echo json_encode($arr);
	}
	
	//$route['be/promo/banner/config'] = 'backend/be_promo/inputBannerPromo';
	public function inputBannerPromo() {
		if($this->username != null) {
            $x['form_header'] = "Input Banner Promo K-Link";
            $x['icon'] = "icon-pencil";
		    $x['form_reload'] = 'be/promo/banner/config';
    		$this->setTemplate($this->folderView.'inputBannerPromo', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
	//$route['be/promo/banner/config/save'] = 'backend/be_promo/saveBannerPromo';
	public function saveBannerPromo() {
		$arr = jsonFalseResponse("Update Failed..");
		$form = $this->input->post(NULL, TRUE);
		$fileName = $_FILES["myfile"]['error'] != 0 ? "" : $_FILES["myfile"]["name"];
		$data = $this->mPromo->saveBannerPromo($form, $fileName);
		if($data > 0 && $_FILES["myfile"]['error'] == 0){
			$img_name = $this->uploadProductImage("assets/images/promo_image/");                 
	        $arr = jsonTrueResponse(null, "Update Success..");
		}
		//print_r($_FILES["myfile"]);
		echo json_encode($arr);
	}
	
	private function uploadProductImage($saveDir) {
        //$output_dir = "assets/product_img/original/";
        //$thumb_dir = "assets/product_img/thumbnail/";
        
        if(isset($_FILES["myfile"]))
        {
            $ret = array();
        
            $error =$_FILES["myfile"]["error"];
            
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData() 
            if(!is_array($_FILES["myfile"]["name"])) //single file
            {
                $fileName = $_FILES["myfile"]["name"];
                //$imageTitle = $_POST["imageTitle"];
                $imageTitle = "";  
                $dirfile = $saveDir.$fileName;
                move_uploaded_file($_FILES["myfile"]["tmp_name"],$dirfile);
                //$this->createThumbnails($dirfile, $thumb_dir, $fileName);
                //$this->m_admproduct->saveImagesProduct($idpict, $fileName, $imageTitle);
                $ret[]= $fileName;
            }
            
            return $ret;
         }   
   }
 	//$route['be/promo/banner/config/update'] = 'backend/be_promo/updateBannerPromo';
 	public function updateBannerPromo() {
 		$arr = jsonFalseResponse("Update Failed..");
		$form = $this->input->post(NULL, TRUE);
		$fileName = $_FILES["myfile"]['error'] != 0 ? "" : $_FILES["myfile"]["name"];
		$data = $this->mPromo->updateBannerPromo($form, $fileName);
		if($data > 0 && $_FILES["myfile"]['error'] == 0){
			$img_name = $this->uploadProductImage("assets/images/promo_image/");                 
	        $arr = jsonTrueResponse(null, "Update Success..");
		}
		echo json_encode($arr);
 	}
 	//$route['be/promo/banner/id/(:any)'] = 'backend/be_promo/getBannerPromoById/$1';
	public function getBannerPromoById($id) {
		$arr = jsonFalseResponse("No record found..");
		//$form = $this->input->post(NULL, TRUE);
		$data = $this->mPromo->getBannerPromoById($id);
		if($data != null){
			$arr = jsonTrueResponse($data);
		} 
		echo json_encode($arr);
	}
 	//$route['be/promo/banner/config/list'] = 'backend/be_promo/bannerList';
 	public function bannerList() {
 		 $x['listPromo'] = $this->mPromo->getListBannerPromo();
         $this->load->view($this->folderView.'bannerPromoList', $x);
 	}
    
	
	//$route['be/promo/kolagen400bv'] = 'backend/be_promo/formKolagen400Bv';
	public function formKolagen400Bv() {
		if($this->username != null) {
            $x['form_header'] = "Promo 400 BV (1 box Kolagen bayar Rp.50000)";
            $x['icon'] = "icon-pencil";
		    $x['form_reload'] = 'be/promo/kolagen400bv';
		    $x['listPayType'] = $this->mPromo->listPayType();
			$x['period'] = $this->mPromo->getPeriodBo();
            $this->setTemplate($this->folderView.'kolagen400bv', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
	//$route['be/promo/kolagen400bv/stockist/check/(:any)'] = 'backend/be_promo/checkDataStockist/$1';
	public function checkDataStockist($id) {
		$arr = jsonFalseResponse("No record found..");
		$data = $this->mPromo->checkDataStockist($id);
		if($data != null){
			$arr = jsonTrueResponse($data);
		} 
		echo json_encode($arr);
	}
	
	//$route['be/promo/kolagen400bv/price/(:any)/(:any)'] = 'backend/be_promo/checkDataProduct/$1/$2';
	public function checkDataProduct($prdcd, $pricecode) {
		$arr = jsonFalseResponse("No record found..");
		$data = $this->mPromo->checkDataProduct($prdcd, $pricecode);
		if($data != null){
			$arr = jsonTrueResponse($data);
		} 
		echo json_encode($arr);
	}
	
	//$route['be/promo/kolagen400bv/redemp/check'] = 'backend/be_promo/checkRedempRemain';
	public function checkRedempRemain() {
		$data = $this->input->post(NULL, TRUE);
		$arr = jsonFalseResponse("BV kurang dari 400 untuk periode $data[bnsperiod]");
		$member = $this->mPromo->checkRedempRemain($data['idmmember'], $data['bnsperiod']);	
		if($member != null) {
			$tot_bv = $member[0]->tbv;
			$max_qty = floor($tot_bv / 450);
			if($max_qty > 0) {
				$poin = $this->mPromo->checkPoinRedemp($data['idmmember'], $data['bnsperiod']);	
				if($poin != null) {
					$total_redemp = $poin[0]->total_redemp;
					
					if($total_redemp >= $max_qty) {
						$arr = jsonFalseResponse("Poin anda sudah di claim..");
					} else {
						$sisa_klaim = $max_qty - $total_redemp;
						$prd = $this->mPromo->checkDataProduct("IDHD014P16", $data['pricecode']);
					    $arr = array("response" => "true", "member" => $member, "product" => $prd, "max_qty_redemp" => $sisa_klaim);
					}
				} else {
					$prd = $this->mPromo->checkDataProduct("IDHD014P16", $data['pricecode']);
					$arr = array("response" => "true", "member" => $member, "product" => $prd, "max_qty_redemp" => $max_qty);
				}	
			} else {
				$arr = jsonFalseResponse("Jumlah klaim anda sudah habis..");
			}
		
			
		} 
		echo json_encode($arr);
	}
	
	//$route['be/promo/kolagen400bv/redemp/save'] = 'backend/be_promo/saveKolagenRedemp';
	public function saveKolagenRedemp() {
		$data = $this->input->post(NULL, TRUE);
		if($data['total_redemp'] > $data['total_remain']) {
			$arr = jsonFalseResponse("Total klaim melebihi batas maksimum : $data[total_remain]");
		} else {
			$cek_seQ = $this->mPromo->cek_seQ_promo();
			$idnoo = $this->mPromo->get_idno_promo();
			$save = $this->mPromo->saveKolagenRedemp($idnoo);
			
			if($save > 0){
				$arrx = array(
					"trcd" => $idnoo,
					"idmember" => $data['idmember'],
					"membernm" => $data['membernm'],
					"total_redemp" => $data['total_redemp'],
			    );
				$arr = array("response" => "true", "arraydata" => $arrx);
			} else {
				$arr = jsonFalseResponse("Simpan data gagal..");
			}
		}
		
		echo json_encode($arr);
	}
	
	//$route['be/promo/kolagen400bv/generate'] = 'backend/be_promo/formGenerateKolagenRedemp';
	public function formGenerateKolagenRedemp() {
		if($this->username != null) {
            $x['form_header'] = "Generate promo Kolagen 400 BV Rp.50000";
            $x['icon'] = "icon-pencil";
		    $x['form_reload'] = 'be/promo/kolagen400bv/generate';
		    $x['period'] = $this->mPromo->getPeriodBo();
            $this->setTemplate($this->folderView.'kolagen400Generate', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
	//$route['be/promo/kolagen400bv/generate/list'] = 'backend/be_promo/getListKolagenToGenerate';
	public function getListKolagenToGenerate() {
		$data = $this->input->post(NULL, TRUE);
		$X['bnsperiod'] = $data['bnsperiod'];
		$x['result'] = $this->mPromo->getGenerateByPvr($data['from'],$data['to'],$data['idstk'],$data['bnsperiod']);
		if($x['result'] == null) {
			echo setErrorMessage("Data tidak ada..");
		} else {
			$this->load->view($this->folderView."kolagen400GenerateList", $x);
		}
		//print_r($x['result']);
	}
	
    //$route['be/promo/kolagen400bv/generate/save'] = 'backend/be_promo/saveGenerateKolagen';
	public function saveGenerateKolagen() {
			$data['user'] = $this->session->userdata('username');
            $username = $this->session->userdata('username'); 
            $data['cek'] = $this->input->post('cek');
            $data['bnsperiod'] = $this->input->post('bnsperiod');
            $bnsperiod = date('d/m/Y',strtotime($data['bnsperiod']));
            
            $data['typess'] = $this->input->post('typee');
        
            
            $data['groupprod'] = $this->mPromo->getGroupProduct($data['cek'],$username);
			
			$data['grouppay'] = $this->mPromo->getGroupPayment($data['cek']);
        
            if($data['groupprod'] != null)
            {
                $data['form_action'] = site_url('/c_sales_subStockist/post_form_generate_sub');
                $this->load->view($this->folderView."kolagen400GenPreview", $data);	
            }
            else
            {
                print_r($data['groupprod']);
            }
	}

    //$route['be/promo/kolagen400bv/generate/save2'] = 'backend/be_promo/saveGenerateKolagen2';
    public function saveGenerateKolagen2() {
			//$data['user'] = $this->session->userdata('username');
            //$username = $this->session->userdata('username'); 
            $data['trcd'] = $this->input->post('trcd');
            $data['type'] = $this->input->post('subs');
            $data['bonusperiod'] = $this->input->post('bonusperiod');
       
           //$data['tampil'] = $this->menu_tes();
            
            $lastnoo = $this->mPromo->get_SSRno($data['type'],$data['bonusperiod']);
            if($lastnoo > 0)
            {
                foreach($lastnoo as $row)
                {
                    $new_id = $row->hasil;
                    $data['new_id'] = $row->hasil;
                    $generate = $this->mPromo->generate_sales($new_id,$data['trcd'],$data['bonusperiod'],$data['type']);
                    if($generate > 0)
                    { 
                        echo "<div class=\"row-fluid\">
							<div class=\"span12\">
							
								<h4>Generate sukses, ";
								if ($data['type'] == 'rdm') {
									echo "MM No : $new_id ";
								} else {
									echo "PVR No : $new_id ";
								}
								
							echo "</h4></div>
						</div>";
						
						$this->load->view($this->folderView."kolagen400GenerateResult", $data);	
                    }
                }
            }
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Simcard extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->folderView = "backend/simcard/";
        $this->load->service('backend/be_product_service');
        $this->load->model('backend/be_simcard_model');
        $this->load->model("dtc/Mdtc_mbr",'mdtc');

    }


//$route['simcard'] = 'backend/simcard';
    public function index() {
        if($this->username != null) {
            $data['form_header'] = "Release SIMCARD";
            $data['form_action'] = base_url('etiket/save');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard';
            $this->setTemplate($this->folderView.'getInputSIM', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }


    public function checkKW(){
        $id = $this->input->post("idm");

        //	$ID = $this->Mdtc_mbr/checkID($data);
        //echo "Nandang";
        //	echo $id;
        //	$this->load->model("dtc/Mdtc_mbr",$id);
        $data['header']=$this->be_simcard_model->checkKW($id);
        $data['detail']=$this->be_simcard_model->getDetailCard($id);

        //print_r($data);

        echo json_encode($data);

    }

    function cariKelipatan()
    {
        $sampai = $this->input->post('sampai');
        $dengan = $this->input->post('dengan');

        $produk = $this->be_simcard_model->getKelipatan($sampai,$dengan);


        $i=0;

        foreach($produk as $row)
        {
            if($row->status==0){
                $i++;
//					$rowx = array();

//					$rowx[] = $row->msidn;
                $data['arraydata'][] = $row;
            }
            else{
                $data['nomorbermasalah'][]=$row;
                break;
            }
        }

        $data['qty']=$i;
        if('IS_AJAX') {
            echo json_encode($data);
        }
    }
//	$route['etiket/save'] = 'backend/Etiket/saveInputEtiket';
    public function saveInputEtiket()
    {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

        {
            try {
                $srvReturn = $this->be_etiket_model->saveInputEtiket();

            } catch (Exception $e) {
                $srvReturn = jsonFalseResponse($e->getMessage());
            }
        }

//		echo in_array(null, $data, true);
        echo json_encode($srvReturn);

    }
    public function submitAttendee()
    {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

        {
            try {
                $srvReturn = $this->be_etiket_model->submitAttendee();

            } catch (Exception $e) {
                $srvReturn = jsonFalseResponse($e->getMessage());
            }
        }

//		echo in_array(null, $data, true);
        echo json_encode($srvReturn);

    }

    public function saveInputEtiketxd() {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        try {
            if($this->form_validation->run() == TRUE) {
                $srvReturn = $this->be_etiket_model->saveInputEtiket();

            }
        } catch(Exception $e) {
            $srvReturn = jsonFalseResponse($e->getMessage());
        }
        echo json_encode($srvReturn);
    }

//$route['etiket/list'] = 'backend/Etiket/getListAllTiket';
    public function getListAllTiket($type = "array") {
        $data['listPrd'] = null;
        try {
            $data['listPrd'] = $this->be_etiket_model->getlistAllEtiket();
        } catch(Exception $e) { }
        if($type == "array") {
            $this->load->view($this->folderView.'getListAllEtiket', $data);
        } else {
            echo json_encode(jsonTrueResponse($data['listPrd']));
        }
    }
//$route['etiket/getEditTiket/(:any)'] = 'backend/Etiket/getEditTiket/$1';
    public function getEditTiket( $value) {
        $srvReturn = jsonFalseResponse();
        try {
            $srvReturn = $this->be_etiket_model->getData($value);
        } catch(Exception $e) {}
//		echo json_encode($srvReturn);
        echo json_encode(jsonTrueResponse($srvReturn));

    }
//$route['etiket/update'] = 'backend/Etiket/updateEtiket';
    public function updateEtiket()
    {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

        {
            try {
                $data = $this->input->post(NULL, TRUE);

                $arr_data = array(
                    'nama'=>$data['nm_event'],
                    'lokasi'=>$data['loc_event'],
                    'max_online'=>$data['max_online'],
                    'act_online'=>0,
                    'max_offline'=>$data['max_offline'],
                    'act_offline'=>0,
                    'total'=>0,
                    'act_total'=>0,
                    'event_date'=>($data['event_date']),
                    'pembicara'=>$data['pembicara'],
                    'exp_date_online'=>($data['exp_date_online']),
                    'exp_date_offline'=>($data['exp_date_offline']),
                    'price_online'=>$data['price_online'],
                    'price_offline'=>$data['price_offline'],
                    'remark'=>$data['remark'],
                    'createnm'=>$this->username,
                    'earlybird_date'=>$data['bird_date'],
                    'price_earlybird_online'=>$data['price_bird']
                );
                $id=$data['id'];

                $srvReturn = $this->be_etiket_model->updateEtiket($id,$arr_data);

            } catch (Exception $e) {
                $srvReturn = jsonFalseResponse($e->getMessage());
            }
        }
        echo json_encode(jsonTrueResponse($srvReturn));

//		echo in_array(null, $data, true);
//		echo json_encode($srvReturn);

    }


//$route['etiket/klaim'] = 'backend/Etiket/klaim';
    public function klaim()
    {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

        {
            try {
                $data = $this->input->post(NULL, TRUE);

                $arr_data = array(
                    'valid_dfno'=>$data['idmbr2'],
                    'valid_fullnm'=>$data['display_info2'],
                    'email'=>$data['email'],
                    'notelp'=>$data['notelp'],

                    'createnm'=>$this->username
                );
                $id=$data['id'];

                $srvReturn = $this->be_etiket_model->klaimEtiket($id,$arr_data);

            } catch (Exception $e) {
                $srvReturn = jsonFalseResponse($e->getMessage());
            }
        }
        echo json_encode(jsonTrueResponse($srvReturn));

//		echo in_array(null, $data, true);
//		echo json_encode($srvReturn);

    }

//$route['etiket/list2'] = 'backend/Etiket/getListAllTRXTiket';
    public function getListAllTRXTiket($type = "array") {
//		$data['listPrd'] = null;
//		$data['form_header'] = "Klaim E-ticket";
//		$data['form_action'] = base_url('etiket/save');
//		$data['icon'] = "icon-pencil";
//		$data['form_reload'] = 'etiket';
//		try {
//			$data['listPrd'] = $this->be_etiket_model->getListAllTRXTiket();
//		} catch(Exception $e) { }
//		if($type == "array") {
//			$this->setTemplate($this->folderView.'getListAllTRXEtiket', $data);
//
//
//		} else {
//			echo json_encode(jsonTrueResponse($data['listPrd']));
//		}

        if($this->username != null) {
            $data['form_header'] = "Klaim E-ticket";
            $data['form_action'] = base_url('etiket/klaim');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'etiket/list2';
            $this->setTemplate($this->folderView.'formListing', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

//$route['etiket/list3'] = 'backend/Etiket/getListKlEtiket';
    public function getListKlEtiket($type = "array") {
        $data['listPrd'] = null;
        try {
            $data['listPrd'] = $this->be_etiket_model->getListAllTRXTiket();
        } catch(Exception $e) { }
        if($type == "array") {
            $this->load->view($this->folderView.'getListAllTRXEtiket', $data);
        } else {
            echo json_encode(jsonTrueResponse($data['listPrd']));
        }
    }

//$route['etiket/delete/(:any)'] = 'backend/Etiket/deletetkt/$1';
    public function deletetkt($id) {
        try {
            $data['listPrd'] = $this->be_etiket_model->deletetiket($id);
        } catch(Exception $e) { }
        {
            echo json_encode(jsonTrueResponse());
        }
    }

//$route['etiket/klaimEtiket/(:any)'] = 'backend/Etiket/KlaimEtiket/$1';
    public function KlaimEtiket($value) {
        $srvReturn = jsonFalseResponse();
        try {
            $srvReturn = $this->be_etiket_model->getData2($value);
        } catch(Exception $e) {}
//		echo json_encode($srvReturn);
        echo json_encode(jsonTrueResponse($srvReturn));

    }

    function cetak2($id){

//		$srvReturn = $this->be_etiket_model->getData2($id);
//			$dt['x']=$this->get_web_page('https://chart.googleapis.com/chart?cht=qr&chl=ET0300009&chs=160x160&chld=L|0');

        $dt['res1'] = array(1);
//				$this->be_etiket_model->getData2($id);
        $this->load->view($this->folderView.'printTiket', $dt);


//			$this->load->view('webshop/printTiket',$dt);

    }
    function get_web_page( $url, $cookiesIn = '' ){
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Cert
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE         => $cookiesIn
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookiesOut = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;
        return $header;
    }










//$route['etiket/report'] = 'backend/Etiket/formInputReport';
    public function formInputReport() {
        if($this->username != null) {
            $data['form_header'] = "E-tiket Report";
            $data['form_action'] = base_url('etiket/cetak');
            $data['action'] = base_url('etiket/cetak');

            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'etiket/report';
            $this->setTemplate($this->folderView.'formReportEtiket', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

//$route['etiket/getReport'] = 'backend/Etiket/ReportEtiketData/0';
//$route['etiket/cetak'] = 'backend/Etiket/ReportEtiketData/1';
    function ReportEtiketData($id){
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;
        $category = $this->input->post('category');
        $tglawal =  $this->input->post('so_from');
        $tglakhir = $this->input->post('so_to');
        $search = $this->input->post('search');
        $jenis = $this->input->post('jenis');

//			$year = $this->input->post('year');
        $dt['report'] = 1;
        //$dt['report'] = $this->be_etiket_model->getReportEtiket($tglawal,$tglakhir,$category,$search);
        //$dt2['report'] = $this->be_etiket_model->getReportEtiket2($tglawal,$tglakhir,$category,$search);
//		$dt['report6'] = $this->sm_office->getReportBonus2($idmembers);

//		$dt['free'] = $this->sm_office->isfree($idmembers);
//		echo 'dxdxdd '.$category;
//		echo $tglakhir;
        if($jenis==1){
            if($id==0){
                $dt['report'] = $this->be_etiket_model->getReportEtiket($tglawal,$tglakhir,$category,$search);
                $this->load->view($this->folderView.'getListReportEtiket',$dt);
            }else{
                $dt2['report'] = $this->be_etiket_model->getReportEtiket2($tglawal,$tglakhir,$category,$search);
                $this->load->view($this->folderView.'excel',$dt2);
            }
        }else if($jenis==2){
            $dt3['report'] = $this->be_etiket_model->getReportFinance($tglawal,$tglakhir,$category,$search);
            $this->load->view($this->folderView.'excelfinance',$dt3);
        }

    }


    public function kirimEmail2()
    {
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "aldi.web16@gmail.com";
        $config['smtp_pass'] = "surabaya21";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";


        $ci->email->initialize($config);

        $ci->email->from('aldi.web16@gmail.com', 'Rinaldhi Cahyono');

//		$email = $this->input->post('email');
//		$subject = $this->input->post('subject');
        $pesan = ('hsahd');

        $ci->email->to('aldi_web16@yahoo.com');
        $ci->email->subject('test');
        $ci->email->message($pesan);
        if ($this->email->send()) {
            echo "<script>
		alert('Email sent!');
		</script>";
        } else {
            show_error($this->email->print_debugger());
        }
    }

    public function formCheckinx() {
        if($this->username != null) {
            $data['form_header'] = "Check-in Acara";
            $data['action'] = base_url('etiket/cek');
            $data['isiTabel']=$this->mdtc->tabelData();
            $data['alamat']=$this->mdtc->getAcara2();
            $data['bank']=$this->mdtc->getBank();
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'etiket/cekin';
            $this->setTemplate($this->folderView.'checkin', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    function formCheckin()
    {
        if ($this->username != null) {
            $x['form_header'] = "Check-in Acara";
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'etiket/cekin';
            $x['isiTabel'] = $this->mdtc->tabelData();
            $x['alamat'] = $this->mdtc->getAcara2();
            $x['bank'] = $this->mdtc->getBank();
            $x['key'] = "key";
            $x['payID'] = "payID";
            $x['action'] = base_url('etiket/cek');

            $this->setTemplate($this->folderView . 'checkin', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }
    public function validasitiket(){
        $id = $this->input->post("idm");
        $acara = $this->input->post("acara");
        $opsi=$this->input->post("opsi");


        //	$ID = $this->Mdtc_mbr/checkID($data);
        //echo "Nandang";
        //	echo $id;
        //	$this->load->model("dtc/Mdtc_mbr",$id);
        if($opsi==0)
        {
            $nama=$this->be_etiket_model->validasitiket($id);
        }
        else
        {
            $nama=$this->be_etiket_model->validasitiket2($id, $acara);
        }
        //echo $nama[0]->fullnm;
        //print_r ($nama) ;
        //array("tes"=> "sdsds");
        echo json_encode($nama);
    }

    function saveData(){
        //script for saving the data
        $i=0;
        $no_kw = $this->input->post('nm_event');
        $msidn = $this->input->post('msidn');
        $date = $this->input->post('event_date');
        $stockis = $this->input->post('loccd');
        $tipe = $this->input->post('tipe');
        $dfno = $this->input->post('dfno');
        $now= date("Y-m-d H:i:s");
        $counter = 0;

        if(!empty($msidn))
        {
            //foreach pertama untuk ngecek di tabel
            foreach ($msidn as $k=>$v){
                $check = $this->be_simcard_model->checkSIMCard($msidn[$k]);
                if($check->num_rows() != 1 ){
                    $counter++;
                    $data['nomorbermasalah']=$msidn[$k];
                }
            }//closing tag foreach

            //kalau ada 1 saja angka bermasalah, semua tidak masuk, tidak tersimpan
            if($counter>0){
                echo "
                <script>
                    alert(' ".$data['nomorbermasalah']." Sudah Direlease, Pastikan Nomor yang Anda pilih belum terpakai');
                     All.reload_page('backend/Simcard');
                </script>
                ";
            }
            else
            {
                if($tipe==1){
                    foreach ($msidn as $k=>$v){
                        $arr_release = array(
                            'no_kw' => $no_kw,
                            'simcard' => $msidn[$k],
                            'createdt' => $now,
                            'loccd' => $stockis
                        );
                        $i++;
                        $this-> be_simcard_model->saveRelease($arr_release);//insert data to ppob_xl_release
                        //update data to table ppob_xl_msidn
                        $q=$this->be_simcard_model->checkMsidn($msidn[$k]);
                        if($q->num_rows()==1){
                            $up_msidn = array(
                                'msidn' => $msidn[$k],
                                'loccd'=> $stockis,
                                'createdt' => $date,
                                'loccddt' => $now,
                                'status' => "1",
                                'trx_loccd' => $no_kw
                            );

                            $this->be_simcard_model->updateMsidn($msidn[$k], $up_msidn);
                        }
                    }//closing tag foreach
                    echo "<br>";
                    echo "
            <script>
                alert('Data Tersimpan');
                //All.reload_page('backend/Simcard');
            </script>
            ";

                }
                else{
                    if($dfno!= '' ){
                        $arr_release = array(
                        'no_kw' => $no_kw,
                        'simcard' => $msidn[$k],
                        'createdt' => $now,
                        'loccd' => $stockis
                    );
                        $i++;
                        $this-> be_simcard_model->saveRelease($arr_release);//insert data to ppob_xl_release
                        //update data to table ppob_xl_msidn
                        $q=$this->be_simcard_model->checkMsidn($msidn[$k]);
                        if($q->num_rows()==1){
                            $up_msidn = array(
                                'msidn' => $msidn[$k],
                                'loccd'=> $stockis,
                                'dfno'=> $dfno,
                                'createdt' => $date,
                                'loccddt' => $now,
                                'status' => "2",
                                'trx_loccd' => $no_kw,
                                'trx_dfno' => $no_kw,
                                'dfnodt' => $now


                            );
                            $this->be_simcard_model->updateMsidn($msidn[$k], $up_msidn);
                        }
                        //closing tag foreach
                        echo "<br>";
                        echo "
                        <script>
                            alert('Data Tersimpan');
                            
                        </script>
                        ";
						//All.reload_page('backend/Simcard');
                    }
                    else{
                        echo "
                        <script>
                            alert('harap isi nomor member');
                             
                        </script>
                        ";
						//All.reload_page('backend/Simcard');
                    }




                }
            }


        }//closing tag main if
        else{
            echo
            "<script>
                alert('Data Gagal Disimpan');
             </script>";
        }

    }

    public function simCardSales() {
        if($this->username != null) {
            $data['form_header'] = "SIM CARD Sales";
            //$data['form_action'] = base_url('etiket/save');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard/simCardSales';
            $this->setTemplate($this->folderView.'simCardSales', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }
    public function simCardSalesMKT() {
        if($this->username != null) {
            $data['form_header'] = "SIM CARD Sales (MKT/BID06/AM)";
            //$data['form_action'] = base_url('etiket/save');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard/simCardSalesMKT';
            $this->setTemplate($this->folderView.'simCardSalesMKT', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    public function simCardInput() {
        if($this->username != null) {
            $data['form_header'] = "SIM CARD INPUT";
            //$data['form_action'] = base_url('etiket/save');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard/simCardSales';
            $this->setTemplate($this->folderView.'simCardInput', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }


    public function checkIdMemb(){
        $id = $this->input->post('idmemb');

        $data = $this->be_simcard_model->checkID($id);

        echo json_encode($data);

    }

    public function checkNomor(){
        $no = $this->input->post('nomor');

        $data = $this->be_simcard_model->checkNomor($no);

        echo json_encode($data);

    }

    public function saveSim(){
        //save SIM Card to table
        $msidn = $this->input->post('nomor');
        $dfno = $this->input->post('idmemb');
        $fullnm = $this->input->post('nm_memb');
        $remark = $this->input->post('remark');
        $createdt = date("Y-m-d H:i:s");
        $createnm = $this->username;

        $satu= substr($msidn,0,2);
        $dua = substr($msidn,2);
        $sim = str_replace("08", "628", $satu);
        $sim2=$sim.$dua;
        //print_r($sim);

        $save = array(
            'msidn' => $sim2,
            'dfno' => $dfno,
            'fullnm' => $fullnm,
            'createdt' => $createdt,
            'status' => 2,
            'createnm' => $createnm,
            'kodegenerate' =>'0'
        );

        $update= array(
            'loccd' => "BID06",
            'dfno' => $dfno,
            'fullnm' => $fullnm,
            'createdt' => $createdt,
            'loccddt' => $createdt,
            'dfnodt' => $createdt,
            'status' => 2,
            'trx_loccd' => $createdt,
            'Remark' => $remark,
            'trx_dfno' => $createdt
        );

        $this-> be_simcard_model->saveRegis($save);

        $this-> be_simcard_model->upStatusMsidn($sim2, $update);

        echo "
            <script>
                alert('Data Tersimpan');
                All.reload_page('backend/Simcard/simCardSales');
            </script>
            ";

    }

    public function saveSimMKT(){
        //save SIM Card to table
        $msidn = $this->input->post('nomor');
        $dfno = $this->input->post('idmemb');
        $fullnm = $this->input->post('nm_memb');
        $remark = $this->input->post('remark');
        $createdt = date("Y-m-d H:i:s");
        $createnm = $this->username;

        $satu= substr($msidn,0,2);
        $dua = substr($msidn,2);
        $sim = str_replace("08", "628", $satu);
        $sim2=$sim.$dua;
        //print_r($sim);

        $save = array(
            'msidn' => $sim2,
            'dfno' => $dfno,
            'fullnm' => $fullnm,
            'createdt' => $createdt,
            'status' => 2,
            'createnm' => $createnm,
            'kodegenerate' =>'0'
        );
        $orderno = $this->my_counter->getCounter2('KNXL', $this->uuid->v4());

        $update= array(
//            'loccd' => "BID06",
            'dfno' => $dfno,
            'fullnm' => $fullnm,
            'createdt' => $createdt,
//            'loccddt' => $createdt,
            'dfnodt' => $createdt,
            'status' => 2,
//            'trx_loccd' => $createdt,
            'Remark' => $remark,
            'trx_dfno' => $orderno
        );

        $this-> be_simcard_model->saveRegis($save);

        $this-> be_simcard_model->upStatusMsidn($sim2, $update);

        echo "
            <script>
                alert('Data Tersimpan');
                All.reload_page('backend/Simcard/simCardSalesMKT');
            </script>
            ";

    }

    public function reportsimCard() {
        if($this->username != null) {
            $data['form_header'] = "Report SIM CARD Sales";
            //$data['form_action'] = base_url('etiket/save');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard/report';
            $data['sql']= $this->be_simcard_model->getListReport();
            $this->setTemplate($this->folderView.'reportsimCardSales', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }

    public function generateReport($qty){
        $createdt = date("Y-m-d H:i:s");
        $user = $this->username;
        $now=gmdate("Y-m-d H:i:s", time()+60*60*5.6);
        $nom= 105000;
        $total= $nom * $qty;
        $pricecode="12W3";

        $x['tgl']=date('d-m-Y');
        $thn= date('y');
        $bln= date('m');

        //bonus period
        $bonusperiod ="";
        $bonus = $this-> be_simcard_model->getSelect2('syspref', 'currperiodSCO');
        foreach ($bonus as $bns) {
            $bonusperiod = $bns->currperiodSCO;
        }

        //Start Penomoran Otomatis
        $user= $this->session->userdata('username');
        $nogen = "";
        $sql_01="SELECT TOP 1 * from ppob_xl_register where substring(kodegenerate,3,2)='$thn' AND substring(kodegenerate,5,2)='$bln' order by kodegenerate DESC";
        //echo $this->db->last_query();
        $query_01 = $this->db->query($sql_01);
        if ($query_01->num_rows() == 1){
            foreach ($query_01->result() as $row_01)
            {
                $nogen= trim($row_01->kodegenerate);
            }
        }

        $sql_02="SELECT MAX(RIGHT('$nogen', 2)) as max_id from ppob_xl_register";
        $query_02 = $this->db->query($sql_02);
        if ($query_02->num_rows() == 1){
            foreach ($query_02->result() as $row_02)
            {
                $idmax= $row_02->max_id;
            }
        }

        $nomor=$idmax++;
        //$kode_auto=$thn.sprintf("%05s",$idmax);
        $kode_auto="XL".$thn.$bln.sprintf("%06s",$idmax);
        //END Penomoran Otomatis

        $update= array(
            'kodegenerate' => $kode_auto,
            'generatedt' => $createdt
        );

        $save1 = array(
            'trcd' => $kode_auto,
            'trtype' =>'SA0',
            'etdt' => $now,
            'trdt' => $now,
            'dfno' => '0000999',
            'loccd' => $this->username,
            'tdp' => $nom,
            'taxrate' => '0',
            'taxamt' => '0',
            'discamt' => '0',
            'shcharge' =>'0',
            'othcharge' =>'0',
            'tpv' => '0',
            'tbv' => '0',
            'npv' => '0',
            'nbv' => '0',
            'ndp' => $nom,
            'whcd' => 'WH001',
            'batchno' => '',
            'docno' => '',
            'branch' => 'B001',
            'pricecode' => $pricecode,
            'paytype1' => '01',
            'paytype2' => '01',
            'paytype3' => '03',
            'pay1amt' => '0',
            'pay2amt' => '0',
            'pay3amt' => '0',
            'totpay' => $nom,
            'paynote1' => '',
            'paynote2' => '',
            'paynote3' => '',
            'ship' => '1',
            'shipto' => '',
            'createnm' => $this->username,
            'updatenm' => $this->username,
            'updatedt' => $now,
            'shipnote' => '',
            'post' => '0',
            'sp' => '0',
            'sb' => '0',
            'sentby' => '',
            'filenm' => '',
            'taxable' => '0',
            'taxableamt' => '0',
            'stockdt' => '',
            'ordtype' => '5',
            'createdt' => $now,
            'cnid' => '',
            'orderno' => '',
            'sjno' => '',
            'type' => '0',
            'taxcd' => '',
            'scdiscrate' => '0',
            'scdiscamt' => '0',
            'fr_formno' => '',
            'to_formno' => '',
            'sctype' => '0',
            'scdisc' => '1',
            'generate' => '0',
            'statusbo' => '1',
            'syn2web' => '0',
            'regtype' => '',
            'n_bc'=> '0',
            'used_for' => '',
            'status' => '0',
            'placenode' => '',
            'applyto' => '',
            'autorecon' => '0',
            'first_trx' => '0',
            'origloccd' => '',
            'bc' => '1',
            'errlogs' => '',
            'PT_SVRID' => 'ID',
            'csno' => '',
            'sc_dfno' => '0000999',
            'sc_co' => $this->username,
            'bnsperiod' => $bonusperiod,
            'remarks' => '',
            'note' => '',
            'othdisc' => '0',
            'batchdt' => '',
            'flag_batch' => '',
            'batchstatus' => '0',
            'flag_recover' => '0',
            'system' => '0',
            'ttptype' => 'Memb',
            'filename' => '',
            'entrytype' => '4',
            'sc_co_offline' => '',
            'flag_show' => '1',
            'flag_approval' => '0',
            'trcd2' => '',
            'OLcsno' => '',
            'flag_editable' => '',
            'kitprdcd' => '',
            'no_deposit' => '',
            'id_deposit' => ''
        );

        $save2 = array(
            'trcd' => $kode_auto,
            'prdcd' => 'XLPerdana',
            'uom' => '',
            'qtyord' => $qty,
            'qtyship' => '0',
            'qtyremain' => '0',
            'dp' => $total,
            'pv' => '0',
            'bv' => '0',
            'taxrate' => '0',
            'sp' =>'0',
            'sb' =>'0',
            'sentby' => '0',
            'filenm' => '0',
            'taxcd' => '0',
            'scdisc' => '0',
            'seqno' => '0',
            'scdiscamt' => '0',
            'syn2web' => '',
            'qty_used' => '0',
            'qty_avail' => '0',
            'filename' => '',
            'PT_SVRID' => 'ID',
            'promocd' => '',
            'pricecode' => '',
            'indexfree' => ''
        );

        $exec1= $this-> be_simcard_model->updateRegister($update);
        $exec2= $this-> be_simcard_model->saveReportPDF1($save1);
        $exec3= $this-> be_simcard_model->saveReportPDF2($save2);

        if(!$exec1 && !$exec2 && !$exec3){
            $respon= "false";
            $msg= "Generate Laporan Gagal!";
        }else{
            $respon= "true";
            $msg= "Generate Laporan Berhasil!";
        }

        //$this->setTemplate($this->folderView.'reportsimCardTXT');

        $arr = array("response" => $respon, "message" => $msg,"kode" => $kode_auto);
        echo json_encode($arr);

        /*echo "<script>
                alert('Data Tersimpan');
                //All.reload_page('backend/Simcard/reportsimCard');
            </script>";*/
    }

    public function printTXT($kode) {
        if($this->username != null) {
            $data['form_header'] = "Report SIM CARD Sales";
            //$data['form_action'] = base_url('simcard/printtxt');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard/printtxt';
            /*
            //$msg= "Tes";
            //$arr = array("message" => $msg);
            //echo json_encode($arr);*/
            //$this->setTemplate($this->folderView.'reportsimCardTXT',$data);
            $data['sql']= $this->be_simcard_model->getListTXT($kode);
            foreach ($data['sql']->result() as $r) {
                $dfno[]= $r->idmemb;
            }


            $data['sql1'] = $this->be_simcard_model->getSelectTXT($kode);

            foreach ($dfno as $k => $v) {
                $sql1="select dfno,msidn from ppob_xl_register where dfno='$v' order by dfno ASC";
                $q1 = $this->db->query($sql1);
                if ($q1->num_rows() > 0) {
                    $no=0;
                    $ar[]=0;
                    foreach ($q1->result() as $row) {
                        $no++;
                        $idm = trim($row->dfno);
                        $msidn= trim($row->msidn);
                        $total[]=trim($row->msidn);
                        $n2=[$idm=>$msidn];

                        if (key_exists('EID548670', $n2))
                        {
                            $ar[]=$n2['EID548670'];
                        }
                    }
                }
            }
            $count= count($ar);
            /*$pop=array_diff($ar, ["6287784804910"]);
            echo implode("",$pop);*/
            //print_r($pop);
            //print_r($n2['EID548670']);
            //$data['n2']=$total;
            $qty=count($total);
            $total1= 105000 * $qty;
            $data['tot1']=$total1;
            $this->load->view($this->folderView . 'reportsimCardTXT2', $data);

        } else {
            redirect('backend', 'refresh');
        }
    }

    public function printTXT1() {
        if($this->username != null) {
            $data['form_header'] = "Report SIM CARD Sales";
            //$data['form_action'] = base_url('simcard/printtxt');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'simcard/printtxt';
            /*
            //$msg= "Tes";
            //$arr = array("message" => $msg);
            //echo json_encode($arr);*/
            //$this->setTemplate($this->folderView.'reportsimCardTXT',$data);
            $data['sql']= $this->be_simcard_model->getListTXT1();
            $this->load->view($this->folderView . 'reportsimCardTXT', $data);

        } else {
            redirect('backend', 'refresh');
        }
    }

    function getList()
    {
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;
        $data['list']= $this->be_simcard_model->getList();
        $this->load->view($this->folderView . 'getListSim', $data);


    }
}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class BankControl extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->folderView = ('backend/bank/');
        $this->load->model("backend/bank/Be_bank_model","mBank");
    }


    //$route['bank/tolak/form'] = 'backend/bank/BankControl/FormBank';
    public function FormBank() {
        if($this->username != null) {
            $data['form_header'] = "Input Tolakkan Bank";
            $data['form_action'] = base_url('bank/tolak/act');
            $data['icon'] = "icon-search";
            $data['form_reload'] = 'bank/tolak/form';
            $this->setTemplate($this->folderView.'formTolakkanBank', $data);
        } else {
            echo sessionExpireMessage(false);
        }
    }

    //$route['bank/tolak/act'] = 'backend/bank/Bank/actTolakanBank';
    public function actTolakanBank() {
        $data = $this->input->post(NULL,TRUE);
        $save= $this->mBank->savetolakbank($data);

        if($save != null){
            $arr = array("response" => "TRUE");
            echo json_encode($arr);

        } else {
            $arr = array("response" => "FALSE");
            echo json_encode($arr);
        }
    }

    public function listTolakanBank() {

        $res['listtolakbank'] = $this->mBank->getListTolak();

        $this->load->view($this->folderView.'listTolakBank', $res);

    }

}
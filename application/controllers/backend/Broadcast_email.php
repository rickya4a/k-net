<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Broadcast_email extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/transaction_mgm/";
        $this->load->model('backend/be_transaction_model', 'm_trans');
        //$this->load->model('webshop/ecomm_trans_model', 'mm_trans');
	}
    
    function getFrmSendBV(){
       if($this->username != null) {
			$data['form_header'] = "Get BonusMonth BV";
            $data['icon'] = "icon-list";
		    $data['form_reload'] = 'send/bv';
            //$data['bnsmonth'] = $this->m_trans->showBonusmonth();
            $this->setTemplate($this->folderView.'getBVByBnsMonth', $data); 
		} else {
			redirect('backend', 'refresh');
		}	 
    }
    
    function getListBVById(){
        $this->load->library('email');
        if($this->username != null) {
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $period = $month."/".$year;
                        
            //$personal = $this->m_trans->personalbv($month,$year);
            //print_r($personal);
            $personal[0] = array ( 
                        'dfno' => 'EID1052297',
                        'fullnm' => 'PUNGKY LESMANA', 
                        'email' => 'anna.nech49@gmail.com',
                        'level' => 9,
                        'ppv' => 77.00, 
                        'popv' => 167.00, 
                        'pgpv' => 167.00, 
                        'sfno' => 'EID169580', 
                        'year' => 2017, 
                        'month' => 1 
                        );
            $personal[1] = array ( 
                        'dfno' => 'EID1052298',
                        'fullnm' => 'INDRA LESMANA', 
                        'email' => 'dionrespati@gmail.com',
                        'level' => 9,
                        'ppv' => 77.00, 
                        'popv' => 168.00, 
                        'pgpv' => 168.00, 
                        'sfno' => 'EID169580', 
                        'year' => 2017, 
                        'month' => 1 );
                        
            $personal[2] = array ( 
                        'dfno' => 'EID1052291',
                        'fullnm' => 'SULE LESMANA', 
                        'email' => 'aribowo.susetyo@gmail.com',
                        'level' => 9,
                        'ppv' => 77.00, 
                        'popv' => 169.00, 
                        'pgpv' => 169.00, 
                        'sfno' => 'EID169580', 
                        'year' => 2017, 
                        'month' => 1 );
            
            
            
            foreach($personal as $list){
             //for($i=0;$i < count($personal);$i++){
                $char = "<html>
						<head>
						  <title>Personal BV</title>
						</head>
						
						<body>";
              
				/*$char .= "<table border=1><tr><td width='75%'>ID Member</td><td >".$list->dfno."</td></tr>";
				$char .= "<tr><td>Nama Member</td><td>".$list->fullnm."</td></tr>";
				$char .= "<tr><td>Period Bonus</td><td>".$period."</td></tr>";
				$char .= "<tr><td>Peringkat</td><td>".$list->level."</td></tr>";
				$char .= "<tr><td>PPV</td><td>".$list->ppv."</td></tr>";
				$char .= "<tr><td>GBV</td><td>".$list->popv."</td></tr>";
				$char .= "<tr><td>PGBV</td><td>".$list->pgpv."</td></tr>";
                $char .= "<tr><td>Email</td><td>".$list->email."</td></tr>";
				$char .= "</table><br />";
				$char .= "</body></html>";*/
                
                $char .= "<table border=1>";
                $char .= "<tr><td colspan=2>Personal BV ".$list['dfno']."</td></tr>";
                $char .= "<tr><td width='55%'>ID Member</td><td width='45%'>".$list['dfno']."</td></tr>";
				$char .= "<tr><td>Nama Member</td><td>".$list['fullnm']."</td></tr>";
				$char .= "<tr><td>Period Bonus</td><td>".$period."</td></tr>";
				$char .= "<tr><td>Peringkat</td><td>".$list['level']."</td></tr>";
				$char .= "<tr><td>PPV</td><td>".$list['ppv']."</td></tr>";
				$char .= "<tr><td>GBV</td><td>".$list['popv']."</td></tr>";
				$char .= "<tr><td>PGBV</td><td>".$list['pgpv']."</td></tr>";
                $char .= "<tr><td>Email</td><td>".$list['email']."</td></tr>";
				$char .= "</table><br />";
				$char .= "</body></html>";
                
                
                $senderEmail = 'knet.klink@gmail.com';
                $senderAlias = 'PT. K-Link Nusantara';
                //$sendCc
				//$email = strtolower($list->email);
                $email = strtolower($list['email']);
				//kirim email
				$this->email->from($senderEmail, $senderAlias);
                $this->email->to($email);
				$this->email->set_mailtype("html");
				$this->email->subject("Personal BV ".$period);
				$this->email->message($char);
                
                if ($this->email->send()){
                
                    echo "Mail Sent! sukses kirim bv by email";   
                }
                else{
                    echo "There is error in sending mail!";
                }
            }
        }else{
            redirect('backend', 'refresh');
        }
    }
        
    function getSendBnsReport(){
        if($this->username != null) {
			$data['form_header'] = "Get Bonus Report";
            $data['icon'] = "icon-list";
		    $data['form_reload'] = 'send/bnsreport';
            //$data['bnsmonth'] = $this->m_trans->showBonusmonth();
            $this->setTemplate($this->folderView.'getBnRptByMonth', $data); 
		} else {
			redirect('backend', 'refresh');
		}	
    }
    
    function sendBnsReport(){
        $this->load->library('email');
        if($this->username != null) {
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $period = $month."/".$year;
            
            
            
        }else{
            redirect('backend', 'refresh');
        }
        
            
    }
    
    function randomData(){
        $ss = $this->m_trans->randomsData();
        //echo json_encode($ss);
        $random_keys=array_rand($ss,3);
        echo $ss[$random_keys[0]]->dfno." - ".$ss[$random_keys[0]]->fullnm."<br>";
        echo $ss[$random_keys[1]]->dfno." - ".$ss[$random_keys[1]]->fullnm."<br>";
        echo $ss[$random_keys[2]]->dfno." - ".$ss[$random_keys[2]]->fullnm;
        
    }
}
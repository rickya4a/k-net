<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Hilal extends CI_Controller{
	
	public function __construct(){

     parent::__construct();

     // Load model
	$this->load->model("backend/M_hilal", "M_hilal");
  	}
		
	public function serviceMutasi2()
	{
		$token		= "654321";
      	$date_from  = "2019-03-20 00:00:00";
	  	$date_to    = "2019-04-22 23:59:59";
      	$status     = "all";
      	$platform   = "knet";
		$accno		= "1190000879898";
      	
		$serviceMutasi2=$this->serviceMutasi($token, $date_from, $date_to, $status, $platform, $accno);
		
		$i = 0;
		foreach ($serviceMutasi2 as $value) {
			$i++;
			
			$transaction_date = $value['transaction_date'];
			$description = $value['description'];
            $amount = $value['amount'];
            $kd_pay = $value['kd_pay'];
            $tot_bayar = $value['tot_bayar'];
            $ip_integration = $value['ip_integration'];
            $label = $value['label'];
            $tgl_order = $value['tgl_order'];
            $tgl_transfer = $value['tgl_transfer'];
            $status = $value['status'];
            $note = $value['note'];
			
			echo "$i --> $transaction_date -- $description -- $amount -- $kd_pay -- $tot_bayar -- $ip_integration -- $label -- $tgl_order -- $tgl_transfer -- $status -- $note</br>";
		}
		//echo "<pre>"; print_r($serviceMutasi2); echo "</pre>";
		
	}
	public function serviceMutasi($token, $date_from, $date_to, $status, $platform, $accno)
	{		
		$url = "https://service-mutasi.k-link.dev/api_v2/cek_mutasi";
		$method = "POST";
		//echo $token;
		$data_array =  array(
					      'token'		=> $token,
					      'date_from'   => $date_from,
		            	  'date_to'     => $date_to,
					      'status'      => $status,
					      'platform'    => $platform,
						  'no_rek'		=> $accno
						);
		
	    $profile = $this->http_request($url, $data_array);
		// ubah string JSON menjadi array
		$profile = json_decode($profile, TRUE);
		
		return $profile;
		
	}
		
	function http_request($url, $data){
	    // persiapkan curl
	    $ch = curl_init(); 
	
	    // set url 
	    curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		if ($data)
			$data = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    
	    // set user agent    
	    //curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	
	    // return the transfer as a string 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
	
	    // $output contains the output string 
	    $output = curl_exec($ch); 
	
	    // tutup curl 
	    curl_close($ch);      
	
	    // mengembalikan hasil curl
	    return $output;
	}
	
	
	// geolocation
	
	function getLatLong($code){
	 	 $mapsApiKey = 'AIzaSyAZUlMb5liLCtnBxcnjyhktsPq1GdvFXNc'; //hilal
	 	 //$mapsApiKey = 'AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU'; //vera
		  //$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + postcode + '&key=[[YOUR_API_KEY]]', function(data) {

		 //$query = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($code)."&output=json&key=".$mapsApiKey;
		 $query = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=jakarta+' . urlencode($code) . '&sensor=true&output=json&key='.$mapsApiKey);
		 $query = json_decode($query);
    	 print_r($query);
		 $data = file_get_contents($query);
		 print_r($data);
		 // if data returned
		 if($data){
			  // convert into readable format
			  $data = json_decode($data);
			  $long = $data->Placemark[0]->Point->coordinates[0];
			  $lat = $data->Placemark[0]->Point->coordinates[1];
			  return array('Latitude'=>$lat,'Longitude'=>$long);
		 }else{
		  		return false;
		 }
	}
	
	function postcode(){
		$d = $this->getLatLong('12950');
		print_r($d);
		//print_r(getLatLong('14460'));
	}
	
	
	function get_coordinates($city, $street, $province){
		$mapsApiKey = 'AIzaSyAZUlMb5liLCtnBxcnjyhktsPq1GdvFXNc';
	    $address = urlencode($city.','.$street.','.$province);
	    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false&region=Poland&key=$mapsApiKey";
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    $response_a = json_decode($response);
	    $status = $response_a->status;
		
		print_r($response_a);
	
	    if ( $status == 'ZERO_RESULTS' )
	    {
	        return FALSE;
	    }
	    else
	    {
	        $return = array('lat' => $response_a->results[0]->geometry->location->lat, 'long' => $long = $response_a->results[0]->geometry->location->lng);
	        return $return;
	    }
	}
	
	
	//Fungsi ini menghitung jarak mengemudi dan durasi waktu perjalanan:
	function GetDrivingDistance($lat1, $lat2, $long1, $long2)
	{
	    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    $response_a = json_decode($response, true);
	    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
	
	    return array('distance' => $dist, 'time' => $time);
	}
	
	function testCoordinate(){
		$coordinates1 = $this->get_coordinates('Tychy', 'Jana Pawła II', 'Śląskie');
		$coordinates2 = $this->get_coordinates('Lędziny', 'Lędzińska', 'Śląskie');
		if ( !$coordinates1 || !$coordinates2 )
		{
		    echo 'Bad address.';
		}
		else
		{
		    $dist = GetDrivingDistance($coordinates1['lat'], $coordinates2['lat'], $coordinates1['long'], $coordinates2['long']);
		    echo 'Distance: <b>'.$dist['distance'].'</b><br>Travel time duration: <b>'.$dist['time'].'</b>';
		}
	}
	
	function getPostCode(){
		echo "test-controller hilal";
		$loadPostCode = $this->M_hilal->getRecPostCode(null);
	}
	


	function hilalBCA(){
		header("Content-Type:text/plain");
		 
		$usernameKlikBca="USERNAME-KLIKBCA";  //username klikbca
		$passwordKlikBca="PASSWORD";  //password klikbca
		$tglCekMulai="16-8-2014";  //tgl mulai cek
		$tglCekBerakhir="17-8-2014"; //tgl akhir cek
		
		
		$VCurl = new VCurl; 
		$VCurl->setCurl();
		$VCurl->disableSSL(true);  //agar support https
		$VCurl->enableCookies(true,'klikbca'); //buat cookies bernama klikbca
		$VCurl->costumHeader(true,array( //manipulasi header
		'Host: ibank.klikbca.com',
		'User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0'));			
		$output=$VCurl->goCurl('https://ibank.klikbca.com/');
		if(strstr($output[0],"Please enter Your USER"))
		{
		$VCurl->disableSSL(true);
		$VCurl->enableCookies(true,'klikbca');
		$VCurl->costumHeader(true,array(
		'Host: ibank.klikbca.com',
		'User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0'));
		$data = array(
		'value(actions)' =>'login',
		'value(user_id)' => $usernameKlikBca,
		'value(user_ip)' => $_SERVER["REMOTE_ADDR"],
		'value(pswd)' => $passwordKlikBca,
		'value(Submit)' => 'LOGIN'
		);
		$VCurl->enablePost(true);
		$VCurl->dataPost($data);
		$output=$VCurl->goCurl('https://ibank.klikbca.com/authentication.do');
		}
		$tglStart=explode('-',$tglCekMulai); //pecah tanggal sesuai format yang dibutuhkan di klikbca
		$tglEnd=explode('-',$tglCekBerakhir); //pecah tanggal sesuai format yang dibutuhkan di klikbca
		$VCurl->enablePost(true);
		$VCurl->dataPost(str_replace(" ","+","value(D1)=0&value(r1)=1&value(startDt)=".$tglStart[0]."&value(startMt)=".$tglStart[1]."&value(startYr)=".$tglStart[2]."&value(endDt)=".$tglEnd[0]."&value(endMt)=".$tglEnd[1]."&value(endYr)=".$tglEnd[2]."&value(fDt)=&value(tDt)=&value(submit1)=Lihat Mutasi Rekening"));
		$output=$VCurl->goCurl('https://ibank.klikbca.com/accountstmt.do?value(actions)=acctstmtview');
		$output= trim(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output[0])); //karna output hasil curl banyak whitespace, jadi ane bersihkan seluruh whitespace menjadi 1 baris output
		preg_match_all('@<td  width="130" bgcolor="#(.*?)"><div align="left"><font  face="verdana" size="1"  color="#(.*?)">(.*?)</font></div></td><td  width="30" bgcolor="#(.*?)"><div align="center"><font  face="verdana" size="1"  color="#(.*?)">0000</font></div></td>@',  $output,$info);
		foreach($info[3] as $newInfo)
		{
		preg_match_all("#TRSF E-BANKING CR<br>(.*?)<br>(.*?)<br>(.*?)<br>(.*?)<br>(.*?)#",$newInfo,$data);
		if($data[2] && $data[3]){
		$dataTrf['id'][]=trim($data[1][0]);
		$dataTrf['total'][]=trim($data[2][0]);
		$dataTrf['berita'][]=trim($data[3][0]);
		$dataTrf['nama'][]=trim($data[4][0]);
		}
		}
		 
		print_r($dataTrf);
		
		}	
		
		
	function hilalBCA2(){
		$ch = false;	
		$url = "https://ibank.klikbca.com/authentication.do";
		$cookie_jar = tempnam('/tmp','klikbca');
		/*
		$fields = array(
			'actions' => urlencode($_POST['login']),
			'user_id' => urlencode($_POST['LUQITOMA2983']),
			'user_ip' => urlencode($_SERVER["REMOTE_ADDR"]),
			'pswd' => urlencode($_POST['210707']),
			'Submit' => urlencode($_POST['LOGIN'])
		);
		*/
		$fields = array(
			'actions' => urlencode('login'),
			'user_id' => urlencode('LUQITOMA2983'),
			'user_ip' => urlencode("REMOTE_ADDR"),
			'pswd' => urlencode('210707'),
			'Submit' => urlencode('LOGIN')
		);
		//print_r($fields);
		$fields_string = null;
		foreach($fields as $key=>$value) {
				$fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

	    $this->ch = curl_init();
	    curl_setopt($this->$ch, CURLOPT_URL, $url);
		curl_setopt($this->$ch, CURLOPT_COOKIEJAR, $cookie_jar); //create cookies
	    curl_setopt($this->$ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($this->$ch, CURLOPT_PROXYPORT, 3128);
	    curl_setopt($this->$ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($this->$ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($this->$ch, CURLOPT_COOKIEFILE, $cookie_jar); // put cookies value
		curl_setopt($this->$ch,CURLOPT_POST, count($fields));
		curl_setopt($this->$ch,CURLOPT_POSTFIELDS, $fields_string);
		
		$tglCekMulai="13-5-2019";  //tgl mulai cek
		$tglCekBerakhir="14-5-2019"; //tgl akhir cek
		
		$tglStart=explode('-',$tglCekMulai); //pecah tanggal sesuai format yang dibutuhkan di klikbca
		$tglEnd=explode('-',$tglCekBerakhir); //pecah tanggal sesuai format yang dibutuhkan di klikbca
		
		$val_mutasi = str_replace(" ","+","value(D1)=0&
										   value(r1)=1&
										   value(startDt)=".$tglStart[0].
										  "&value(startMt)=".$tglStart[1].
										  "&value(startYr)=".$tglStart[2].
										  "&value(endDt)=".$tglEnd[0].
										  "&value(endMt)=".$tglEnd[1].
										  "&value(endYr)=".$tglEnd[2].
										  "&value(fDt)=
										  &value(tDt)=
										  &value(submit1)=Lihat Mutasi Rekening");
		$url_mutasi = "https://ibank.klikbca.com/accountstmt.do?value(actions)=acctstmtview";
		//$output= trim(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output[0])); //karna output hasil curl banyak whitespace, jadi ane bersihkan seluruh whitespace menjadi 1 baris output
	
		define('URL_LOGIN', 'https://ibank.klikbca.com/authentication.do');
		define('URL_SALDO', 'https://ibank.klikbca.com/balanceinquiry.do');
		define('URL_MUTASI_INDEX', 'https://ibank.klikbca.com/accountstmt.do?value(actions)=acct_stmt'); 
		define('URL_MUTASI_VIEW', 'https://ibank.klikbca.com/accountstmt.do?value(actions)=acctstmtview');
		define('URL_MUTASI_DOWNLOAD','https://ibank.klikbca.com/stmtdownload.do?value(actions)=account_statement');
	
		
		
        $now = strtotime('Today');
        //list($day1, $month1, $year1) = explode(' ', date('d n Y', $tglCekMulai));
        //$minus_t = $now - (24 * 3600 * $minus);
        //list($day2, $month2, $year2) = explode(' ', date('d n Y', $tglCekBerakhir));

        $data = array(
            //'value(actions)' => 'acctstmtview',
            'value(D1)' => '0',
            'value(startDt)' => $tglStart[0],
            'value(startMt)' => $tglStart[1],
            'value(startYr)' => $tglStart[2],
            'value(endDt)' => $tglEnd[0],
            'value(endMt)' => $tglEnd[1],
            'value(endYr)' => $tglEnd[2],
            'value(submit1)' => 'Lihat Mutasi Rekening'
        );
        $data = http_build_query($data);
		
        //echo '<p>', urldecode($data), '</p>';
        $res = $this->my_curl_post(URL_MUTASI_VIEW, $data, URL_MUTASI_INDEX);
        $this->last_html = $res['response'];
        return true;
		
		/*
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_REFERER, $ref);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
	    $response = curl_exec($ch);
		//print_r($response);
	    curl_close($ch);
	    $response_a = json_decode($response);
	    $status = $response_a->status;
		
		print_r($response_a);
		*/
	/*
	    if ( $status == 'ZERO_RESULTS' )
	    {
	        return FALSE;
	    }
	    else
	    {
	        $return = array('lat' => $response_a->results[0]->geometry->location->lat, 'long' => $long = $response_a->results[0]->geometry->location->lng);
	        return $return;
	    }
	 * 
	 */
	}
 	function my_curl_post($url, $post_data, $ref = '') {
        if ($this->ch == false) {
            $this->my_curl_open();
        }
        $ssl = false;
        if (preg_match('/^https/i', $url)) {
            $ssl = true;
        }
        if ($ssl) {
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        }
        if ($ref == '') {
            $ref = $url;
        }
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_REFERER, $ref);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
        $res = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        return array(
            'response' => trim($res),
            'info' => $info
        );
    }
	
	
	
}

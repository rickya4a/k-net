<?php

    class Report_BV extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = "backend/report/";
            $this->load->model('backend/report/List_Report_BV', 'm_Report');
        }

        public function formReportBV(){

            if($this->username != null) {
                $data['form_header'] = "Report Distributor Sales By BV";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'report/summ_bv';
                $this->setTemplate($this->folderView.'formReport', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        public function getListingReportBV(){

            //$month = $this->input->post('month');
            $year = $this->input->post('year');
            $range = $this->input->post('range');

            if($range == '00-99'){

                $dt['result'] = $this->m_Report->get_detailreport0099($year);
                $this->load->view($this->folderView . 'tampilReportBV', $dt);
            }

            if($range == '01-00'){

                $dt['result'] = $this->m_Report->get_detailreport0100($year);
                $this->load->view($this->folderView . 'tampilReportBV2', $dt);
            }
        }

        public function printReportBVPdf(){

            $year = $this->input->post('year');
            $range = $this->input->post('range');

            if($range == '00-99'){

                $dt['result'] = $this->m_Report->get_detailreport0099($year);
                $this->load->view($this->folderView . 'printReportBVPDF', $dt);
            }

            if($range == '01-00'){

                $dt['result'] = $this->m_Report->get_detailreport0100($year);
                $this->load->view($this->folderView . 'printReportBVPDF2', $dt);
            }
        }

        public function printReportBVExcel(){

            $year = $this->input->post('year');
            $range = $this->input->post('range');

            if($range == '00-99'){

                $dt['result'] = $this->m_Report->get_detailreport0099($year);
                $this->load->view($this->folderView . 'printReportBVEXCEL', $dt);
            }

            if($range == '01-00'){

                $dt['result'] = $this->m_Report->get_detailreport0100($year);
                $this->load->view($this->folderView . 'printReportBVEXCEL2', $dt);
            }
        }

    }
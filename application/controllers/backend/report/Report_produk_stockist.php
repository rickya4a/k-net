<?php
class Report_produk_stockist extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->folderView = 'backend/report/';
		$this->load->model('backend/report/M_report_produk_stockist', 'm_report');
	}

	// $route['rekap/produk/stockist'] = 'backend/report/Report_produk_stockist/reportForm';
	public function reportForm() {
		if (!empty($this->username)) {
			$data['form_header'] = 'Report Produk Stockist';
			$data['icon'] = 'icon-pencil';
			$data['form_reload'] = 'rekap/produk/stockist';
			$data['stockist'] = 'BID06';
			$data['stockist_name'] = 'PT. K-LINK NUSANTARA';
			$this->setTemplate($this->folderView.'formReportProdukStockist', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}

	public function getList() {
		$data = $this->input->post(NULL, TRUE);
		if ($data['type'] == '0') {
			$data['result'] = $this->m_report->rekapProduk($data['stockist'], $data['status'], $data['date_from'], $data['date_to']);
			$this->load->view($this->folderView.'listRekapProduk', $data);
		} else {
			$data['result'] = $this->m_report->rekapTransaksi($data['stockist'], $data['status'], $data['date_from'], $data['date_to']);
			$this->load->view($this->folderView.'listRekapTransaksi', $data);
		}
	}
}


?>
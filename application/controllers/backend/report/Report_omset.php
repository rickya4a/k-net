<?php

    class Report_omset extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = "backend/report/";
            $this->load->model('backend/report/Model_report_omset', 'm_Report');
        }

        public function cariOmset(){

            if($this->username != null){

                $dt['form_header'] = "Daily Omset Report";
                $dt['icon'] = "icon-list";
                $dt['form_reload'] = 'be/daily_omset_report';

                $dt['bnsperiod'] = $this->m_Report->listBnsperiod();

//                foreach($dt['bnsprd'] as row){
//
//                    $bnsperiod = $row->bnsperiod;
//                }


                $this->setTemplate($this->folderView.'formReportOmset', $dt);
            }

            else{

                redirect('backend','refresh');
            }
        }

        public function listReport(){

            $bnsperiod = $this->input->post();
            $x = implode("|",$bnsperiod);

            $time = strtotime($x);
            $newformat = date('Y-m-d',$time);

            $dt['result'] = $this->m_Report->listReport($newformat);

            $this->load->view($this->folderView . 'tampilReportOmset', $dt);
            //print_r($dt['result']);
        }
    }
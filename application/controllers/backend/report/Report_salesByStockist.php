<?php

    class Report_salesByStockist extends MY_Controller{

        function __construct(){

            parent::__construct();

            $this->folderView = "backend/report/";
            $this->load->model('backend/report/Model_report_omset', 'mReport');
        }

        function searchReport(){

            if($this->username != null){

                $dt['form_header'] = "Report Sales By Stockist";
                $dt['icon'] = "icon-list";
                $dt['form_reload'] = 'be/report/sales/bystockist/form';

                $this->setTemplate($this->folderView.'formReportSalesByStockist', $dt);
            }

            else{

                redirect('backend','refresh');
            }
        }

        function listReport(){

            $dt = $this->input->post(NULL, TRUE);

            $dt['result'] = $this->mReport->listReportByStockist($dt);

            //print_r($dt['result']);

            $this->load->view($this->folderView.'listReportByStockist', $dt);
        }
    }
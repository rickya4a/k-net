<?php

		class FinanceTrx extends MY_Controller {
				public function __construct() {
						parent::__construct();

						$this->folderView = "backend/report/";
						$this->load->model('backend/be_transaction_model', 'm_trans');
				}

				public function home() {
						if ($this->username != null) {
								$data['form_header'] = "SGO Report";
								$data['icon'] = "icon-pencil";
								$data['form_reload'] = 'trx/finance';

								//$data['listBank'] = $this->m_trans->getListBank();
								$data['listBank'] = $this->m_trans->getListBankMaster();
								//$data['listBank'] = $this->m_trans->getListMerchantBank();

								$this->setTemplate($this->folderView.'financeFormReport', $data);
						} else {
								redirect('backend', 'refresh');
						}
				}

				//$route['trx/finance/report/act'] = 'backend/report/FinanceTrx/financeReportAct';
				public function financeReportAct() {
						$dt = $this->input->post(null, true);

						$x['exportTo'] = $dt['exportTo'];
						$exportTo = $dt['exportTo'];
						$x['from'] = $dt['from'];
						$x['to'] = $dt['to'];
						$x['listBank'] = $dt['listBank'];

						//echo $_POST['exportTo'];
						//print_r($x['listBank']);
						//print_r($exportTo);
						//print_r($x['listBank']);
						//print_r($dt['tipe_trx']);

						if ($exportTo == 0 or $exportTo == 2) {
								if ($dt['tipe_trx'] == "1") {
										//$x['rptSgo'] = $this->m_trans->getReportSgo2($dt);
										$x['rptSgo'] = $this->m_trans->getReportSgo3($dt);
										$this->load->view($this->folderView.'reportFinance', $x);
										/* if ($dt['listBank'] == '11') {
											$arr = array(
												'token' => '654321',
												'date_from' => '2019-03-20 00:00:00',
												'date_to' => '2019-04-22 23:59:59',
												'status' => '1',
												'platform' => 'knet'
											);
											$res = $this->mandiriAPI($arr);
											print_r($res);
										} */
								} elseif ($dt['tipe_trx'] == "0") {
										$x['rptSgo'] = $this->m_trans->getReportSgoticket($dt);
										$this->load->view($this->folderView.'reportFinanceTiket', $x);
								/*echo "<pre>";
								print_r($x['rptSgo']);
								echo "</pre>";*/
								} elseif ($dt['tipe_trx'] == "2") {
										//echo "masuk K-Wallet";
										$x['rptSgo'] = $this->m_trans->getReportSgoTopUpKwallet($dt);
										$this->load->view($this->folderView.'reportFinanceTopUp', $x);
										/*echo "<pre>";
										print_r($x['rptSgo']);
										echo "</pre>";*/
								}

								if ($x['listBank'] == "5" && $dt['tipe_trx'] == "2") { //k-wallet list topup
										//echo "masuk K-Wallet";
										//print_r($exportTo);
										$x['rptSgo'] = $this->m_trans->getReportSgoTopUpKwallet($dt);
										$this->load->view($this->folderView.'reportFinanceTopUp', $x);
								}
						} elseif ($exportTo == 1) {
								if ($dt['tipe_trx'] == "1") {
										//$x['rptSgo'] = $this->m_trans->getReportSgo2($dt);
										$x['rptSgo'] = $this->m_trans->getReportSgo3($dt);
										$x['totalSgo'] = $this->m_trans->getTotalSgo($dt);
										$this->load->view($this->folderView.'financeCsv', $x);
								} elseif ($dt['tipe_trx'] == "0") {
								}
						}
				}

				// $route['trx/finance/report/mandiri'] = 'backend/report/FinanceTrx/mandiriAPI';
				public function mandiriAPI() {
					$data = $this->input->post(null, true);
					$arr = json_encode($data);
					$curl = curl_init();
					curl_setopt_array($curl, array(
					CURLOPT_URL => 'https://service-mutasi.k-link.dev/api_v2/cek_mutasi',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 60,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => json_encode($data),
					));
					$response = curl_exec($curl);
					$info = curl_getinfo($curl);
					$err = curl_error($curl);

					curl_close($curl);
					if ($err) {
						echo "<pre>";
						print_r($arr);
						echo "<br />";
						print_r($info);
						echo "</pre>";
						return 0;
					} else {
						$res['result'] = json_decode($response);
						// return $res;
						$this->load->view($this->folderView.'mandiriMutasi', $res);
					}
				}
		}

<?php

    class Cetak_surat extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = "backend/report/";
            $this->load->model('backend/report/Model_memberLBC', 'mMemberLBC');
            
        }

        public function tampil_cetak(){

            if($this->username != null) {
                $data['form_header'] = "Cetak Surat Pemberitahuan";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'be/form_cetak';
                $this->setTemplate($this->folderView.'formCetakSurat', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        public function cetak(){

            $dt['tgl'] = $this->input->post('tgl_skrg');

            if($dt['tgl'] != null){

                $dt['date'] = date("d-m-Y", strtotime($dt['tgl']));
            }
            
            $this->load->view($this->folderView.'printSuratPDF', $dt);
        }
    }
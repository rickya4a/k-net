<?php

    class Report_SalesMember extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->folderView = 'backend/report/';
            $this->load->model('webshop/M_promo_lbc', 'm_report');

        }

        public function form(){

            if($this->username != null){

                $dt['form_header'] = "Cek Sales Member";
                $dt['icon'] = "icon-list";
                $dt['form_reload'] = 'be/ceksales/member';

                $this->setTemplate($this->folderView.'formReportSalesMember', $dt);
            }

            else{

                redirect('backend','refresh');
            }
        }

        public function act(){

            $data = $this->input->post(null, true);

            $dt['result'] = $this->m_report->getDetailLBC($data['dfno'], $data['bnsperiod']);

            //print_r($dt['result']);
            $this->load->view($this->folderView.'ReportSalesMember',$dt);
        }

        public function formBangkok(){

            if($this->username != null){

                $dt['form_header'] = "Cek Sales Promo Bangkok";
                $dt['icon'] = "icon-list";
                $dt['form_reload'] = 'be/ceksales/bangkok';

                $this->setTemplate($this->folderView.'formReportSalesBangkok', $dt);
            }

            else{

                redirect('backend','refresh');
            }
        }

        public function actBangkok(){

            $data = $this->input->post(null, true);

            $dt['result'] = $this->m_report->cekLBC3($data['dfno']);

            if($dt['result'] != null){

                foreach($dt['result'] as $abc){

                    $dt['fullnm'] = $abc->FULLNM;
                }
            //print_r($dt['result']);
                $this->load->view($this->folderView.'ReportPromoBangkok',$dt);

            }else{

                $dt['result2'] = $this->m_report->cekLBC2($data['dfno']);

                if($dt['result2'] != null){

                    foreach($dt['result2'] as $abc){

                        $dt['fullnm'] = $abc->fullnm;
                    }
                    $this->load->view($this->folderView.'ReportPromoBangkok2',$dt);
                }else{

                    setErrorMessage('datanya ga ada pak eko');
                }
            }
        }
    }
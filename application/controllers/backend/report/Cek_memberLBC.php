<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cek_memberLBC extends MY_Controller{

    public function __construct() {

        parent::__construct();
        $this->folderView = "backend/report/";
        $this->load->model('backend/report/Model_memberLBC', 'mMemberLBC');

    }

    public function cek_member(){

        if($this->username != null) {
            $data['form_header'] = "Search Data Member LBC";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'cek_LBC';
            $this->setTemplate($this->folderView.'cariMemberLBC', $data);
        }

        else {

            redirect('backend', 'refresh');
        }
    }

    public function tampil_memberLBC(){

        $dt = $this->input->post(null, true);
        $dari_tgl = substr($dt['from'],0,8).'01';
        $ke_tgl = substr($dt['to'],0,8).'01';

        $dt['tbl_lbc'] = $this->mMemberLBC->cekMemberLBC($dt['dfno']);

        $dt['detail'] = $this->mMemberLBC->detailMemberLBC($dt['dfno'],$dari_tgl,$ke_tgl);

        if($dt['tbl_lbc'] != null){

            $this->load->view($this->folderView . 'tampilMemberLBC', $dt);
        }else{

            $dt['tbl_msmemb'] = $this->mMemberLBC->cekMember($dt['dfno']);
                if($dt['tbl_msmemb'] != null){

                    foreach($dt['tbl_msmemb'] as $a){

                        $dfno = $a->dfno;
                        $nama = $a->fullnm;
                    }
                    echo "data ".$dfno."(".$nama.")"." belum ada dalam database member LBC";
                    $this->load->view($this->folderView . 'tampilMemberLBC', $dt);
                }else{

                    setErrorMessage("ID member salah!");
                }
        }

    }

    public function cetak_memberLBC(){

        $dt = $this->input->post(null, true);
        $dari_tgl = substr($dt['from'],0,8).'01';
        $ke_tgl = substr($dt['to'],0,8).'01';

        $dt['tbl_lbc'] = $this->mMemberLBC->cekMemberLBC($dt['dfno']);

        $dt['detail'] = $this->mMemberLBC->detailMemberLBC($dt['dfno'],$dari_tgl,$ke_tgl);

        if($dt['tbl_lbc'] != null){

            $this->load->view($this->folderView . 'excelMemberLBC', $dt);
        }else{

            $dt['tbl_msmemb'] = $this->mMemberLBC->cekMember($dt['dfno']);
            if($dt['tbl_msmemb'] != null){

                echo "data belum ada dalam database member LBC";
            }else{

                setErrorMessage("ID member salah!");
            }
        }
    }

    public function cek_salesLBC(){

        if($this->username != null) {
            $data['form_header'] = "Laporan Sales LBC";
            $data['icon'] = "icon-list";
            $data['form_reload'] = 'cek_LBC';
            $this->setTemplate($this->folderView.'cariLaporanLBC', $data);
        }

        else {

            redirect('backend', 'refresh');
        }
    }

    public function print_salesLBC(){

        $bnsperiod = $this->input->post('bns_period');

        $dt['tgl'] = date("Y/m/d", strtotime($bnsperiod));
        $dt['tgl_hdr'] = date("m-Y",strtotime($bnsperiod));

        $dt['hdr'] = $this->mMemberLBC->salesLBChdr($dt['tgl']);
        $dt['det'] = $this->mMemberLBC->salesLBCdet($dt['tgl']);
        //print_r($dt['hdr']);

        $this->load->view($this->folderView . 'tampilSalesLBC', $dt);
    }


}
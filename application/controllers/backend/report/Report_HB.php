<?php

    class Report_HB extends MY_Controller
    {

        public function __construct()
        {

            parent::__construct();
            $this->folderView = "backend/report/";
            $this->load->model('backend/report/List_Report_HB', 'm_Report');
        }

        public function formHB()
        {

            if ($this->username != null) {

                $data['form_header'] = "Report Promo Heboh Banget";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'be/report/hebohbanget';
                $this->setTemplate($this->folderView . 'formReportHB', $data);

            } else {

                redirect('backend', 'refresh');
            }
        }

        public function getListHB(){

            $idmember = $this->input->post('idmember');
            $bnsperiod = $this->input->post('bnsperiod');
            $tipe = $this->input->post('tipe');

            $dt['hdr'] = $this->m_Report->getHdrHB($idmember);

            if ($dt['hdr'] != null) {

                foreach ($dt['hdr'] as $a) {

                    $dt['nama'] = $a->fullnm;
                    $dt['total'] = $a->total;

                    echo "nama : " . $dt['nama'] . "<br>";
                    echo "total seluruh kupon hingga saat ini : " . $dt['total']. "<br>";
                }
            } else {

                $dt['hdr2'] = $this->m_Report->getHdr($idmember);

                foreach ($dt['hdr2'] as $a) {

                    $dt['nama'] = $a->fullnm;

                    echo "nama : " . $dt['nama'] . "<br>";
                    echo "total seluruh kupon hingga saat ini : tidak ada". "<br>";
                }
            }

            if($tipe == 'pribadi'){

                $dt['all_sales'] = $this->m_Report->getAllSales($idmember, $bnsperiod);
                $dt['all_sales_nonsuplemen'] = $this->m_Report->getAllSalesNonSupl($idmember, $bnsperiod);

                //TOTAL GRID ALL SALES
                $dt['tot_bv_all_sales'] = $this->m_Report->getBVAllSales($idmember, $bnsperiod);
                if ($dt['tot_bv_all_sales'] != null) {

                    foreach ($dt['tot_bv_all_sales'] as $x) {

                        $dt['tot_bv1'] = $x->tBV;
                    }

                }

                //TOTAL GRID ALL NONSUPLEMEN SALES
                $dt['tot_bv_all_sales_nonsupl'] = $this->m_Report->getBVAllSalesNonSupl($idmember, $bnsperiod);
                if ($dt['tot_bv_all_sales_nonsupl'] != null) {

                    foreach ($dt['tot_bv_all_sales_nonsupl'] as $x) {

                        $dt['tot_bv2'] = $x->tBV;
                    }

                }

                $this->load->view($this->folderView . 'tampilReportHBself', $dt);

            } else{

                $dt['all_sales_rekrutan'] = $this->m_Report->getAllSalesRekrutan($idmember,$bnsperiod);
                $dt['rekap_bv_all_nonsuplemen'] = $this->m_Report->getBVAllNonSupl($idmember,$bnsperiod);

                $this->load->view($this->folderView . 'tampilReportHBrekrutan', $dt);
            }

        }

        public function detRekrutanHB(){

            $idmember = $this->input->post('idmember');
            $bnsperiod = $this->input->post('bnsperiod');

            $bln = date("Y-m-d", strtotime($bnsperiod));

            $dt['all_sales_nonsuplemen_rekrutan'] = $this->m_Report->getAllSalesNonSuplRekrutan($idmember,$bln);

            $this->load->view($this->folderView . 'tampilDetReportHB', $dt);
        }
    }
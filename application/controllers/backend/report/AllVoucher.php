<?php

    class AllVoucher extends MY_Controller{

        public function __construct(){

            parent::__construct();
            $this->folderView = "backend/report/";
            $this->load->model('webshop/M_vch_membership','m_vch');
        }

        public function index(){

            if($this->username != null) {
                
                $data['form_header'] = "Voucher Free Membership";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'vch/free/membership/form';
                $this->setTemplate($this->folderView.'v_vch_form', $data);
            }
    
            else {
    
                redirect('backend', 'refresh');
            }
        }

        public function getDetail(){

            $dfno = $this->input->post('dfno');

            $dt['result'] = $this->m_vch->getVoucher($dfno);
              
            $this->load->view($this->folderView. 'v_getvchfreemembership', $dt);
        }
    }
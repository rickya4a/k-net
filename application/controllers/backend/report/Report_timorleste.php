<?php

    class Report_timorleste extends MY_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->model('backend/report/Model_report_timorleste','mLeste');
            $this->load->service('backend/report/Be_report_Service', 's_report');
            $this->folderView = "backend/report/";
        }

        public function index(){

            if($this->username != null) {
                $data['form_header'] = "Bonus Report Timor Leste";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'be/report/timor_leste';
                $this->setTemplate($this->folderView.'formReportTimorLeste', $data);
            }

            else {

                redirect('backend', 'refresh');
            }
        }

        public function getList(){

//            $data = $this->input->post(NULL, TRUE);
//            print_r($data);

            $data['result'] = $this->s_report->getListTL();
            //print_r($data['result']);
            echo json_encode($data['result']);

        }

        public function saveDetail(){

            //$data = $this->s_report->insertStat();
            //$this->load->view($this->folderView.'printKWToTxt', $data);

            $data = $this->input->post('idmemb');
            //print_r($data);
            foreach($data as $dt){

                //echo $dt."<br>";
                $inst = $this->mLeste->insertStatTL($dt);
            }

            if($inst){

                echo '<script language="javascript">';
                echo 'alert("Berhasil update database:)")';
                echo '</script>';
                echo '<script>window.history.go(-1);</script>';

            }else{

                echo '<script language="javascript">';
                echo 'alert("Terjadi kesalahan")';
                echo '</script>';
                //redirect('http://www.k-net.co.id/backend/be/report/timor_leste', 'refresh');
                echo '<script>window.history.go(-1);</script>';
            }
        }

        public function getDetail($id, $stat){


            $dt['result'] = $this->mLeste->getDetailReport($id, $stat);

            //print_r($dt['result']);

            $this->load->view($this->folderView.'getDetailTimorLeste',$dt);
        }

        public function prevSave(){

            $data = $this->input->post(NULL, TRUE);

            //print_r($data['idmemb']);

            $kirim = set_list_array_to_string($data['idmemb'], $roundby = "'" , $delimiter = ",");

            $dt['result'] = $this->mLeste->prevSave($kirim);

            //print_r($dt['result']);
            
            $this->load->view($this->folderView.'prevSaveTimorLeste',$dt);

        }

        public function insertDetail(){

            $data = $this->input->post(NULL, TRUE);
            //print_r($data);

            list($array1, $array2['idmembernya']) = array_chunk($data['idmember'], ceil(count($data['idmember']) / 2));
            //print_r($array1);

            list($ar1, $array2['bonus']) = array_chunk($data['bonus'], ceil(count($data['bonus']) / 2));
            //print_r($ar2);

            list($arr1, $array2['potongannya']) = array_chunk($data['potongan'], ceil(count($data['potongan']) / 2));
            //print_r($arr2);

            $array2['gabung1'] = array_combine($array2['idmembernya'],$array2['bonus']);
            $array2['gabung2'] = array_combine($array2['idmembernya'],$array2['potongannya']);

            //print_r($array2);

            /* update table pak toto QTWA_TLBNSsum2 dan QTWA_TLBNSdet2
            foreach($array2['idmembernya'] as $dt){

                $dt['update'] = $this->mLeste->updateStatus();
            }
            */

            foreach($array2['gabung1'] as $key => $value){

//                echo $key."<br>";
//                echo $value."<br>";

                $detail['hasil'] = $this->mLeste->getDetailReport2($key, 'P');
                //print_r($detail['hasil']);

                $dt['result1'] = $this->mLeste->insertStatusTrans($key, $value);

                foreach($detail['hasil'] as $key => $value){

//                    print_r($key)."<br>";
//                    print_r($value)."<br>";
                    $insertDetail['hasil'] = $this->mLeste->insertDetailTrans($value->idnumber, $value->bnsperiod, $value->total);

                    $dt['update'] = $this->mLeste->updateStatus($value->idnumber, $value->bnsperiod);
                }
            }

            foreach($array2['gabung2'] as $key => $value){

//                echo $key."<br>";
//                echo $value;
                $dt['result2'] = $this->mLeste->updateStatusTrans($key, $value);
            }

            foreach($array2['gabung1'] as $key1 => $value1){
//                echo $key1."<br>";
//                echo $value1."<br>";

                    foreach($array2['gabung2'] as $key2 => $value2) {
//                        echo $key2 . "<br>";
//                        echo $value2 . "<br>";
                    }
                $min = ($value1 * 1) - ($value2 * 1);
                //echo $key1." Hasil: " . $min. "<br>";
                $dt['result3'] = $this->mLeste->updateNettTrans($key1, $min);
            }



            echo "<script>alert(\"Berhasil update:)\");</script>";
            echo "<script>All.reload_page('be/report/timor_leste');</script>";

        }

    }
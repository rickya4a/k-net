<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Report_vt extends MY_Controller {
	public function __construct() {
	   parent::__construct();
       $this->folderView = "backend/report/";
       $this->load->model("backend/report/be_report_vt_trans_model", 'fReportVT');
	}
	
    function getReportVT(){
        if($this->username != null) {
            $x['form_header'] = 'Form Veritrans Sales Report';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'trans/report/VT';
            $this->setTemplate($this->folderView.'formReport_vt', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function getReportVTAct($param = 1){
    	//echo $param;
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['detReportVT'] = $this->fReportVT->getListVT($dt);
			$x['param']=$param;
			
			/*
			if($param == 2 ){
				$x['exportToXls']="	header('Content-type: application/vnd.ms-excel');
								    header('Content-Disposition: attachment; filename=ReportInstallmentMDR.xls' );
								    header('Expires: 0');
								    header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
								    header('Pragma: public');";
			}
			 */
            $this->load->view($this->folderView.'reportVT_res', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportPrintExcell(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            
			/*
			$x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['banklist'] = $dt['banklist'];
            
			 */ 
			//$x['headCicilan'] = $this->fUmroh->getBankInfo($dt);
            $x['detCicilan'] = $this->fUmroh->getListInstall($dt);
            //$this->load->view($this->folderView.'printReport_excell', $x);
            $this->load->view($this->folderView.'printReport_excell_edit', $x);
        }
        else{
            
        }
    }

	function reportPrintExcellMDR(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            
			/*
			$x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['banklist'] = $dt['banklist'];
            
			 */ 
			//$x['headCicilan'] = $this->fUmroh->getBankInfo($dt);
            $x['detCicilan'] = $this->fUmroh->getListInstallMDR($dt);
            //$this->load->view($this->folderView.'printReport_excell', $x);
            $this->load->view($this->folderView.'printReport_excell_editMDR', $x);
            //echo "masuk"
        }
        else{
            
        }
    }

}
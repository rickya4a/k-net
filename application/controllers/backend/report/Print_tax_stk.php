<?php
if(!defined('BASEPATH')) exit('NO DIRECT ACCESS ALLOWED');

    class Print_tax_stk extends MY_Controller{

        function __construct(){

            parent::__construct();
            $this->folderView = "backend/report/";

            $this->load->model("webshop/Sales_model",'salesM');
            //$this->load->model('backend/report/Model_memberLBC', 'mMemberLBC');


        }

        function formPrintTax(){

            if($this->username != null){

                $data['form_header'] = "Print Tax Stk";
                $data['icon'] = "icon-list";
                $data['form_reload'] = 'be/printtax/form';

                //$data['result'] = $this->salesM->getListTaxStk();

//                foreach($data['result'] as $dt){
//
//                    echo $dt->IDMEMBER;
//                }

                $this->setTemplate($this->folderView.'formPrintTax', $data);

            }else{

                redirect('backend', 'refresh');
            }
        }

        function printTax(){

            //$dfno = $this->input->post('dfno');
            $dfno = 'IDMSML4516';
            $thn = '2018';

            $dt['pjk'] = $this->salesM->getDetailPajak2($dfno, $thn);

            //print_r($dt['pjk']);
            $this->load->view('webshop/printPajakPdf2',$dt);
        }
    }
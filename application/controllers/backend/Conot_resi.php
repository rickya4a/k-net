<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Conot_resi extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("backend/m_conot_resi", "conot");
		$this->folderView = "backend/conot/";		
	}
	
	//$route['conote/update'] = 'backend/conot_resi';
	public function index() {
		if($this->username != null) {
            $x['form_header'] = "Update Resi Fisik";
            $x['icon'] = "icon-pencil";
		    $x['form_reload'] = 'conote/update';

            $this->setTemplate($this->folderView.'updateConotForm', $x);
        }else{
            redirect('backend', 'refresh');
        }
	}
	
	//$route['conote/update/list'] = 'backend/conot_resi/listResiFisikBelumDiupdate';
	public function listResiFisikBelumDiupdate() {
		$data = $this->input->post(NULL, TRUE);
		$wh = $this->usergroup;
		if($data['stt_resi_fisik'] == "0") {
			$res['result'] = $this->conot->listConoteResiFisikBlmUpdate($wh, $data['prt_form'], $data['prt_to'], $data['print_count']);
		} else {
			$res['result'] = $this->conot->listConoteResiFisikSudahUpdate($wh, $data['prt_form'], $data['prt_to'], $data['print_count']);
		}
		//echo "<pre>";
		//print_r($res);
		//echo "</pre>";
		
		$this->load->view($this->folderView.'updateConotList', $res);
	}
	
	//$route['conote/update/save'] = 'backend/conot_resi/updateResiFisik';
	public function updateResiFisik() {
		$data = $this->input->post(NULL, TRUE);
		$arr = set_list_array_to_string($data['orderno']);
		$hasil = $this->conot->updateResiFisik($arr);
		if($hasil > 0) {
			echo setSuccessMessage("Data Resi Fisik berhasil di update..");
		} else {
			echo setErrorMessage("Data Resi Fisik gagal di update..");
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_voucher extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->folderView = "backend/be_voucher/";
        //$this->load->service('backend/be_product_service');
        $this->load->model('backend/be_voucher_model', 'be_voucher_model');
    }

    /*--------------------------
     * PRODUCT  CATEGORY
     * ------------------------ */


    function getFormScan()
    {
        if($this->username != null)
        {



            $data['form_header'] = 'Klaim/Scan Voucher';
            $data['icon'] = 'icon-list';
            $data['form_reload'] = 'be_voucher';
            $data['action'] = site_url('be_voucher/submit');

            $data['user'] = $this->session->userdata('username');
            $data['form_action'] = site_url('/c_sales_subStockist/postFormTtpPvr');
            $data['header_form'] = "Scan/Klaim Voucher";
            $data['dateNow'] = date('d F Y');

            $tipe = 'sub';
            $mscode = '';

//            $data['listype'] = $this->m_sales_substockist->get_list_payment();
//            $data['stk'] = $this->m_sales_substockist->get_stockist_info($tipe,$data['user']);
//            $data['currentperiod'] = $this->m_sales_substockist->get_current_period();
//            $this->load->view($this->folderView.'form_scan_klaim',$data);
            $this->setTemplate($this->folderView.'form_scan_klaim', $data);

//            $this->load->view('sales/form_scan_klaim',$data);
            return $data['user'];
        }
        else {
            redirect('backend', 'refresh');
        }

    }

    public function cariID(){
        $id = $this->input->post("idm");

        //	$ID = $this->Mdtc_mbr/checkID($data);
        //echo "Nandang";
        //	echo $id;
        //	$this->load->model("dtc/Mdtc_mbr",$id);
        $nama=$this->be_voucher_model->checkID1($id);

        //echo $nama[0]->fullnm;
        //print_r ($nama) ;
        //array("tes"=> "sdsds");
        echo json_encode($nama);
    }

    function getVch()
    {

        $scan = $this->input->post('scan');
        $nilai = $this->be_voucher_model->getVch($scan)->row();
//        $status = $nilai->status;
        $kategori= strtolower($scan[0]);


//        print_r($nilai);
        if($nilai==null){
            $arr = array("response" => "false", "arraydata" => "g nemu", "scan"=>$scan);
        }
        else
        {
            if($nilai->status=='0' && $nilai->claimstatus=='0'){
                $sad=true;

                $exp=strtotime($nilai->YEY);
                $exd=strtotime($nilai->XD);


                if($exp < $exd)
                {
                    $sad=false;

                }

                $nilai->category="Voucher Cash";
                if($kategori=='p'){
                    $nilai->category="Voucher Product";
                }

                if($sad==true){
                    $arr = array("response" => "true", "arraydata" => $nilai, "scan"=>$scan, "expiri"=>$exp, "now"=>$exd, 'sad'=>$sad);
                }
                else{
                    $arr = array("response" => "expired", "arraydata" => $nilai, "scan"=>$scan, "expiri"=>date("d-M-Y",strtotime( $nilai->YEY )), "expiri2"=>date("d-M-Y",strtotime( $nilai->XD ))
                    ,"expiri3"=>$exp, "expiri4"=>$exd);
                }

            }
            else
            {
                $arr = array("response" => "claimed", "arraydata" => $nilai, "scan"=>$scan, "oleh"=>(isset($nilai->stokis) ? $nilai->stokis : $nilai->stokis2), "tgl"=>(isset($nilai->claim_date) ? date("d-M-Y",strtotime( $nilai->claim_date )) : date("d-M-Y",strtotime( $nilai->updatedt ))  )   );
            }

        }


        echo json_encode($arr);
    }


    function simpanScan(){
        $scan=$this->input->post('idpendaftar');
        $loccd=$this->input->post('substockistcode');



        if($loccd!= null && $scan != null){
            if(!empty($scan))
            {
                foreach($scan as $k=>$v)
                {
//                $aj=explode("|",$str[$k]);



                    $this->be_voucher_model->saveScan($scan[$k],$loccd);
                }
            }
            echo json_encode(true) ;
        }
        else
            echo json_encode(false) ;

        //		redirect('viewDO');

    }

    public function getListBonusPoin($type = "array") {
        $data['listPrd'] = null;
        try {
            $data['listPrd'] = $this->sm_office->getlistAllEtiket();
        } catch(Exception $e) { }
        if($type == "array") {
            $this->load->view($this->folderView.'getListAllEtiket', $data);
        } else {
            echo json_encode(jsonTrueResponse($data['listPrd']));
        }
    }

    function getReportCahyono(){
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;

        $idmembers = $this->input->post('dfno');
        $category = $this->input->post('category');

//			$year = $this->input->post('year');
        $dt['report'] = 1;
        $dt['reportHilal'] = null;
        if($category==1){
            $dt['reportHilal'] = $this->sm_office->getHilal($idmembers);
            $dt['report'] = $this->sm_office->getReportBonus($idmembers);
            $dt['report6'] = $this->sm_office->getReportBonus2($idmembers);
            $dt['free'] = $this->sm_office->isfree($idmembers);
        }
        else
        {
            $dt['report'] = 1;
            $dt['report6'] = 1;
            $dt['free'] = FALSE;
        }

        $this->load->view($this->folderView.'getListAllOffice',$dt);

    }

    function cetakReport(){
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;
        $category = $this->input->post('category');
        $tglawal =  $this->input->post('so_from');
        $tglakhir = $this->input->post('so_to');

//			$year = $this->input->post('year');
        $dt['report'] = 1;
        $dt['report'] = $this->sm_office->getReportLounge($tglawal,$tglakhir,$category);

        $this->load->view($this->folderView.'cetakReport',$dt);

    }


    function ReportLoungeData(){
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;
        $category = $this->input->post('category');
        $tglawal =  $this->input->post('so_from');
        $tglakhir = $this->input->post('so_to');

//			$year = $this->input->post('year');
        $dt['report'] = 1;
        $dt['report'] = $this->sm_office->getReportLounge($tglawal,$tglakhir,$category);
//		$dt['report6'] = $this->sm_office->getReportBonus2($idmembers);

//		$dt['free'] = $this->sm_office->isfree($idmembers);
//		echo 'dxdxdd '.$category;
//		echo $tglakhir;
        $this->load->view($this->folderView.'getListReportOffice',$dt);

    }


    public function saveInputOffice()
    {
//		$srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

//		{
//			try {
        $srvReturn = $this->sm_office->saveInputOffice();
//
//			} catch (Exception $e) {
//				$srvReturn = jsonFalseResponse($e->getMessage());
//			}
//		}

//		if($srvReturn!=0){
        $return['message']='<a class="btn btn-sm btn-primary" href="'.base_url("lounge/nota/".$srvReturn).'" title="View" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> Cetak</a>';
        $return['button']=  '<input id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" type="button">';

//		}
        //		echo in_array(null, $data, true);
        echo json_encode($return);
    }

    function saveInputOffice2(){
        print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;
        $dt['srvReturn'] = $this->sm_office->saveInputOffice2();
        $dt['action'] = base_url("lounge/nota/".$dt['srvReturn']);

        $this->load->view($this->folderView.'cetaknow',$dt);

    }

    public function saveInputEtiketxd() {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        try {
            if($this->form_validation->run() == TRUE) {
                $srvReturn = $this->be_etiket_model->saveInputEtiket();

            }
        } catch(Exception $e) {
            $srvReturn = jsonFalseResponse($e->getMessage());
        }
        echo json_encode($srvReturn);
    }

    public function getListAllAttendee($type = "array") {
        $data['listPrd'] = null;
        try {
            $data['listPrd'] = $this->sm_office->getlistAllInputOffice();
        } catch(Exception $e) { }
        if($type == "array") {
            $this->load->view($this->folderView.'getListAllTRXOffice', $data);
        } else {
            echo json_encode(jsonTrueResponse($data['listPrd']));
        }
    }





    function cetakNota($id){
//		print_r($this->input->post(NULL, TRUE)); // returns all POST items with XSS filter;

//			$year = $this->input->post('year');
        $dt['report'] = 1;
        $dt['report'] = $this->sm_office->getNota($id);

        $this->load->view($this->folderView.'cetakNota',$dt);

    }

    function getHilal($idmember){
        $dt['test'] = $this->sm_office->getHilal($idmember);

        //echo $dt;
    }








    public function updateEtiket()
    {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

        {
            try {
                $data = $this->input->post(NULL, TRUE);

                $arr_data = array(
                    'nama'=>$data['nm_event'],
                    'lokasi'=>$data['loc_event'],
                    'max_online'=>$data['max_online'],
                    'act_online'=>$data['act_online'],
                    'max_offline'=>$data['max_offline'],
                    'act_offline'=>$data['act_offline'],
                    'total'=>$data['total'],
                    'act_total'=>$data['act_total'],
                    'event_date'=>date("Y-m-d",strtotime($data['event_date'])),
                    'pembicara'=>$data['pembicara'],
                    'exp_date_online'=>date("Y-m-d",strtotime($data['exp_date_online'])),
                    'exp_date_offline'=>date("Y-m-d",strtotime($data['exp_date_offline'])),
                    'price_online'=>$data['price_online'],
                    'price_offline'=>$data['price_offline'],
                    'remark'=>$data['remark'],
                    'createnm'=>$this->username
                );
                $id=$data['id'];

                $srvReturn = $this->be_etiket_model->updateEtiket($id,$arr_data);

            } catch (Exception $e) {
                $srvReturn = jsonFalseResponse($e->getMessage());
            }
        }
        echo json_encode(jsonTrueResponse($srvReturn));

//		echo in_array(null, $data, true);
//		echo json_encode($srvReturn);

    }



    public function klaim()
    {
        $srvReturn = jsonFalseResponse(requiredFieldMessage());
        $data = $this->input->post(NULL, TRUE);

        {
            try {
                $data = $this->input->post(NULL, TRUE);

                $arr_data = array(
                    'valid_dfno'=>$data['idmbr2'],
                    'valid_fullnm'=>$data['display_info2'],

                    'createnm'=>$this->username
                );
                $id=$data['id'];

                $srvReturn = $this->be_etiket_model->klaimEtiket($id,$arr_data);

            } catch (Exception $e) {
                $srvReturn = jsonFalseResponse($e->getMessage());
            }
        }
        echo json_encode(jsonTrueResponse($srvReturn));

//		echo in_array(null, $data, true);
//		echo json_encode($srvReturn);

    }


    public function getListAllTRXTiket($type = "array") {
//		$data['listPrd'] = null;
//		$data['form_header'] = "Klaim E-ticket";
//		$data['form_action'] = base_url('etiket/save');
//		$data['icon'] = "icon-pencil";
//		$data['form_reload'] = 'etiket';
//		try {
//			$data['listPrd'] = $this->be_etiket_model->getListAllTRXTiket();
//		} catch(Exception $e) { }
//		if($type == "array") {
//			$this->setTemplate($this->folderView.'getListAllTRXEtiket', $data);
//
//
//		} else {
//			echo json_encode(jsonTrueResponse($data['listPrd']));
//		}

        if($this->username != null) {
            $data['form_header'] = "Klaim E-ticket";
            $data['form_action'] = base_url('etiket/klaim');
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'etiket/list2';
            $this->setTemplate($this->folderView.'formListing', $data);
        } else {
            redirect('backend', 'refresh');
        }
    }


    public function getListKlEtiket($type = "array") {
        $data['listPrd'] = null;
        try {
            $data['listPrd'] = $this->be_etiket_model->getListAllTRXTiket();
        } catch(Exception $e) { }
        if($type == "array") {
            $this->load->view($this->folderView.'getListAllTRXEtiket', $data);
        } else {
            echo json_encode(jsonTrueResponse($data['listPrd']));
        }
    }

    public function KlaimEtiket($value) {
        $srvReturn = jsonFalseResponse();
        try {
            $srvReturn = $this->be_etiket_model->getData2($value);
        } catch(Exception $e) {}
//		echo json_encode($srvReturn);
        echo json_encode(jsonTrueResponse($srvReturn));

    }

    function cetak2($id){

//		$srvReturn = $this->be_etiket_model->getData2($id);
//			$dt['x']=$this->get_web_page('https://chart.googleapis.com/chart?cht=qr&chl=ET0300009&chs=160x160&chld=L|0');

        $dt['res1'] = array(1);
//				$this->be_etiket_model->getData2($id);
        $this->load->view($this->folderView.'printTiket', $dt);


//			$this->load->view('webshop/printTiket',$dt);

    }
    function get_web_page( $url, $cookiesIn = '' ){
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Cert
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE         => $cookiesIn
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookiesOut = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;
        return $header;
    }

}
?>
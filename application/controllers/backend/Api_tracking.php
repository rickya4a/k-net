<?php if(!defined('BASEPATH')) exit('No Direct Script Access Allowed');

    class Api_tracking extends MY_controller{

        public function __construct(){

            parent::__construct();
            $this->load->model("backend/tracking_api_model", "m_api");
            $this->load->model('webshop/Ecomm_trans_model','trx');
            //$this->load->service("webshop/Cartshop_service",'cartshopService');
            $this->load->service("webshop/Product_service",'productService');
        }

        public function getAllDataTracking(){

            try {

                $record = $this->m_api->getAllTracking();
                $res = jsonTrueResponse($record);
            } catch(Exception $e) {

                $res = jsonFalseResponse($e->getMessage());
            }
            echo json_encode($res);
        }

        public function getDataTracking($idstk){

            try {

                $record = $this->m_api->getDataTracking($idstk);
                $res = jsonTrueResponse($record);
            } catch(Exception $e) {

                $res = jsonFalseResponse($e->getMessage());
            }
            echo json_encode($res);
        }

        public function getTrackingInfo() {

            if($this->_checkSessionStoreUser()){
                
                $dt['prodCat'] = $this->productService->getProductCategory();
                //$dt['listTrx'] = $this->trx->getEcommerceTrxByUserLogin(getUserID());
                $dt['listTrx'] = $this->m_api->getDataTracking(getUserID());
                
                $this->setTempWebShop('webshop/tracking/v_tracking',$dt);
            }

        }
    }
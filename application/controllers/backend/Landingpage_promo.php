<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Landingpage_promo extends MY_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('backend/m_landing_page', 'landingPage');
    $this->folderView = 'backend/landing_page/';
  }

  // $route['cashback/landing_page'] = 'backend/landingpage_promo/landingpage';
  public function landingpage() {
    if($this->username != NULL) {
      $data['form_header'] = "Landing Page Cashback";
      $data['icon'] = "icon-pencil";
      $data['form_reload'] = "cashback/landing_page";
      $this->setTemplate($this->folderView."formLandingPage", $data);
    } else {
      redirect('backend', 'refresh');
    }
  }

  // $route['cashback/landing_page/list'] = 'backend/landingpage_promo/getListCashback';
  public function getListCashback() {
    $data = $this->input->post(NULL, TRUE);
    if ($data['tipe'] == "blm_trf") {
			$data['result'] = $this->landingPage->listCashbackBelumTransfer();
      $this->load->view($this->folderView."listNotTransferred", $data);
    } elseif ($data['tipe'] == "tdk_lengkap") {
      $data['result'] = $this->landingPage->listCashbackNotComplete();
      $this->load->view($this->folderView."listNotCompleted", $data);
    } elseif ($data['tipe'] == "tolak") {
      $data['listTrf'] = $this->landingPage->listCashbackRejected($data['from'], $data['to']);
      $this->load->view($this->folderView."listRejected", $data);
    } elseif ($data['tipe'] == "sdh_trf") {
      $data['result'] = $this->landingPage->listCashbackTransferred($data['from'], $data['to'], "3");
      $this->load->view($this->folderView."listTransferred", $data);
    } elseif ($data['tipe'] == "batch") {
      $data['result'] = $this->landingPage->listCashbackBatch($data['from'], $data['to'], "1");
      $this->load->view($this->folderView."listBatch", $data);
    }
  }


  /**
   * Report Controller
   */
  // $route['cashback/landing_page/report'] = 'backend/landingpage_promo/cashbackReport'
  public function cashbackReport() {
    if ($this->username != NULL) {
      $data['form_header'] = "Landing Page Cashback Report";
      $data['icon'] = "icon-pencil";
      $data['form_reload'] = "cashback/landing_page/report";
      $this->setTemplate($this->folderView."formLandingPageReport", $data);
    } else {
      redirect('backend', 'refresh');
    }
  }

  // $route['cashback/landing_page/report/list'] = 'backend/landingpage_promo/cashbackReportList';
  public function cashbackReportList() {
    if ($this->username != NULL) {
      $data = $this->input->post(NULL, TRUE);
      if ($data['req_type'] == "batch") {
        $data['res'] = $this->landingPage->checkTransferByMemberID($data['idmember']);
        $this->load->view($this->folderView.'formReportResult', $data);
      } else {
        $data['res'] = $this->landingPage->rekapListCashbackByMember($data['idmember']);
        $this->load->view($this->folderView.'formReportResultList', $data);
      }
    } else {
      echo setErrorMessage('Silakan login kembali...');
    }
  }
/* end report controller */


  /**
   * Batch Controller
   */

  // $route['cashback/landing_page/batch/id'] = 'backend/landingpage_promo/listTransferByBatchNo';
  public function listTransferByBatchNo() {
    $data = $this->input->post(NULL, TRUE);
    $data['lstTrf'] = $this->landingPage->listTransferByBatchNo($data['no_batch']);
    $this->load->view($this->folderView."listTrfByBatch", $data);
  }

  // $route['cashback/landing_page/batch/ok'] = 'backend/landingpage_promo/TransferOk';
  public function batchOk() {
    $data = $this->input->post(NULL, TRUE);
    $listID = set_list_array_to_string($data['batchno']);
    $update = $this->landingPage->transferredComplete($listID, $data['transferdt']);
    echo json_encode($update);
  }

  // $route['cashback/landing_page/batch/process'] = 'backend/landingpage_promo/previewBatchCashback';
  public function previewBatchCashback() {
    $data = $this->input->post(NULL, TRUE);
    $listTrx = set_list_array_to_string($data['trcd']);
    $data['listTrx'] = $listTrx;
    $data['rekap'] = $this->landingPage->rekapCashbackByIdMember($listTrx);
    $this->load->view($this->folderView."landingPagePreviewBatch", $data);
  }

  // $route['cashback/landing_page/batch/save'] = 'backend/landingpage_promo/saveBatchCashback';
  public function saveBatchCashback() {
    $data = $this->input->post(NULL, TRUE);
    $save = $this->landingPage->saveBatchCashback($data);
    echo json_encode($save);
  }

  // $route['cashback/landing_page/batch/cancel'] = 'backend/landingpage_promo/cancelBatch';
  public function cancelBatch() {
    $data = $this->input->post(NULL, TRUE);
    $data['listTrf'] = $this->landingPage->listTransferByBatchNo($data['no_batch']);
    $this->load->view($this->folderView."listTrfByBatch", $data);
  }

  // $route['cashback/landing_page/batch/print'] = 'backend/landingpage_promo/printBatch';
  public function printBatch() {
    $data = $this->input->post(NULL, TRUE);
    $listID = set_list_array_to_string($data['batchno']);
    $data['res'] = $this->landingPage->listTransferByBatchNoPrint($listID);
    $this->load->view($this->folderView."listTrfByBatchExcel", $data);
  }

  // $route['cashback/landing_page/batch/reject'] = 'backend/landingpage_promo/rejectTransfer';
  public function rejectTransfer() {
    $data = $this->input->post(NULL, TRUE);
    $listID = set_list_array_to_string($data['trcd']);
    $update = $this->landingPage->rejectTransfer($data);
    echo json_encode($update);
  }

  // $route['cashback/landing_page/batch/reject_prev'] = 'backend/landingpage_promo/prevRejectTransfer';
  public function prevRejectTransfer() {
    $data = $this->input->post(NULL, TRUE);
    $listID = set_list_array_to_string($data['trcd']);
    $data['listTrf'] = $this->landingPage->rejectTransferPrev($listID, $data['batchno']);
    $this->load->view($this->folderView."listPrevRejected", $data);
  }
/* end batch controllers */

  // $route['cashback/landing_page/trfprocess/(:any)'] = 'backend/landingpage_promo/listTrfByBatch/$1';
  public function listTrfByBatch($batch) {
    $data['listTrf'] = $this->landingPage->listTransferByBatchID($batch);
    $this->load->view($this->folderView."listTrfByBatch", $data);
  }
}
?>
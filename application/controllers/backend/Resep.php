<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Resep extends MY_Controller {
	public function __construct() {
		parent::__construct();
        $this->folderView = "backend/resep/";
        $this->load->model('backend/be_resep_model');	
    }
    
    //$route['resep/view'] = 'backend/resep/view';
    public function view() {
		if($this->username != null) {
			$username = getUserID();
			$data['form_header'] = "Input Resep";
            $data['icon'] = "icon-pencil";
            $data['form_reload'] = 'resep/view';
            //get data product for option on view
            $data['products'] = $this->be_resep_model->getPrd();

            $this->setTemplate($this->folderView.'input_resep', $data); 
		} else {
			redirect('backend', 'refresh');
		}
    }

    public function save(){
        $data = $this->input->post(NULL, TRUE);

        $nama =  $data['nama'];
        $prdcd = $data['prd2'];
        $qty = $data['qty2'];
        $aturan = $data['aturan2'];

        //auto-generate koderesep
        
        $thn = date('Y').date('m');
        $kode = "";

        $query1= $this->db->query("SELECT TOP 1 koderesep FROM klink_mlm2010.dbo.ng_resep WHERE substring(koderesep,5,4)='$thn' ORDER BY koderesep DESC");
       
        if ($query1->num_rows() == 1){
            foreach ($query1->result() as $row1)
            {
                $kode= trim($row1->koderesep);
            }
        }

        $query2=$this->db->query("SELECT MAX(RIGHT('$kode', 2)) AS max_id FROM klink_mlm2010.dbo.ng_resep");
        
        if ($query2->num_rows() == 1){
            foreach ($query2->result() as $row2)
            {
                $idmax= $row2->max_id;
            }
        }

        $nomor = $idmax++;
        $kode_auto = "RS".$thn.sprintf("%04s",$idmax);
        //End Penomoran Otomatis

        $total = 0;
        foreach($prdcd as $k=>$v){
            //save to table ng_detail_resep
            $data['arr_det'] = array(
                'koderesep' => $kode_auto,
                'prdcd' => $prdcd[$k],
                'qty' => $qty[$k],
                'aturan' => $aturan[$k],
                'createdt' => date('Y-m-d H:i:s'),
                'createnm' => $this->username,
                'updatedt' => date('Y-m-d H:i:s'),
                'updatenm' => $this->username
            );

            $detail = $this->db->insert('klink_mlm2010.dbo.ng_detail_resep', $data['arr_det']);
            
            $total++;   
        }

        if($det > 0){
            //Save header to table ng_resep
            $data['arr_h'] =  array(
                'koderesep' => $kode_auto,
                'namakaryawan' => $nama,
                'totalprd' => $total,
                'createdt' => date('Y-m-d H:i:s'),
                'createnm' => $this->username,
                'updatedt' => date('Y-m-d H:i:s'),
                'updatenm' => $this->username
            );

            $header = $this->db->insert('klink_mlm2010.dbo.ng_resep', $data['arr_h']);
            
            if($header > 0){
				$message = "Data Berhasil Disimpan";
				$hasil = jsonTrueResponse($data, $message);
				echo json_encode($hasil);
			}
			else{
				$message = "Data Gagal Disimpan";
				$hasil = jsonFalseResponse($message);
				echo json_encode($hasil);
			}
        }

    }
    
}

?>
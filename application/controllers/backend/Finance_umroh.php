<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Finance_umroh extends MY_Controller {
	public function __construct() {
	   parent::__construct();
       $this->folderView = "backend/umroh/";
       $this->load->model("backend/be_finance_umroh_model", 'fUmroh');
	}
    
    
    
    function form_approve(){
        if($this->username != null) {
            $x['form_header'] = 'Approve Payment K-Smart';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be_umroh/listPayment';
            $this->setTemplate($this->folderView.'getListPayment', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function get_appPayment(){
        if($this->username != null) {
            $dt = $this->input->post(null,true);
            $x['reg_from'] = $dt['reg_from'];
            $x['reg_to'] = $dt['reg_to'];
            $x['form_reload'] = 'be_umroh/listPayment/app';
            $x['listPayment'] = $this->fUmroh->getListPayUmroh();
            if($x['listPayment'] != null){
                $this->load->view($this->folderView.'listPayUmroh', $x);
            }else{
                echo setErrorMessage();;
            }
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function approvePayment($regno,$from,$to){
        
        $alternate2 = $this->load->database('alternate2', true);
        
        if($this->username != null) {
            $x['form_reload'] = 'be_umroh/listPayment/appRes/(:any)';
            $appPayment = $this->fUmroh->setAppPayment($regno);
            
            //if($appPayment != 0){
                //echo "berhasil";
                $x['detPayment'] = $this->fUmroh->detPayUmroh($regno);
                
                if($x['detPayment'] != null){
                    
                    foreach($x['detPayment'] as $aa){
                        $pktUmrohA = $aa->departuredesc;
                        $idmember = $aa->dfno;
                        $nmmember = $aa->fullnm;
                        $voucherNo = $aa->voucherno;
                        $stk = $aa->loccd;
                        $flagtipe = $aa->flag_tipe;
                        $tel_hp = $aa->hpChild;
                        $telHp = $aa->telHp;
                        $secno = $aa->secno;
                        $cnno = $aa->CNno;
                        $pktUmroh = $aa->desc2;
                        $va = '88146'.$aa->novac;
						$nmmember2 = substr($nmmember, 0, 20);
                    }
                    
                    if($flagtipe == '1'){
                        $text = 'K-SMART RELIGI: Reg no: '.$regno.' / '.$pktUmroh.',ID: '.$idmember.' / '.$nmmember2.',VchNo: '.$voucherNo.',VA: '.$va.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$regno.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.'';
                        $resSms = smsTemplate($telHp, $text);
                    }elseif($flagtipe == '3'){
                        $text = 'K-SMART RELIGI: Reg no: '.$regno.' / '.$pktUmroh.',ID: '.$idmember.' / '.$nmmember2.',Stk:'.$stk.',';
                        $text .= ' VchNo:'.$voucherNo.',SK: '.$secno.',MM: '.$cnno.',VA: '.$va.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$regno.',Pkt Umroh: '.$pktUmroh.',Id Member: '.$idmember.'';
                        
                        /*$smsLnjt = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                           VALUES ('".$tel_hp."','".$textLanjut."','K-Smart Umroh Status')";
                        $query2 = $alternate2->query($smsLnjt);*/
                        
                        $resSms = smsTemplate($tel_hp, $text);
                    }else{
                        $text = 'K-SMART RELIGI: Reg no: '.$regno.' / '.$pktUmroh.',Nama:'.$nmmember2.',VchNo: '.$voucherNo.', VA : '.$va.'';
                        //$txtParent = 'K-SMART UMROH: Reg no: '.$regno.',Nama:'.$nama.',Pkt Umroh: '.$pktUmroh.'';
                        $resSms = smsTemplate($telHp, $text);
					}
                    
					
					
                    /*$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID) 
		                    VALUES ('".$tel_hp."','".$text."','K-Smart Umroh Status', 'MyPhone6')";
                    */
                    //$smsParent = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
    		        //            VALUES ('".$telHp."','".$txtParent."','K-Smart Umroh Status')";
    		        //echo "Ins SMS : $insSms";
    		        //$query = $alternate2->query($insSms);
                    //$query2 = $alternate2->query($smsParent);
                    
                    $x['from'] = $from;
                    $x['to'] = $to;
                    
                    $this->load->view($this->folderView.'resAppUmroh', $x);
                }
            /*}else{
                $this->load->view($this->folderView.'AppUmroh_failed', $x);
                //echo "update gagal";
            }*/
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function printKwSk(){
        if($this->username != null) {
    		//echo "masuk ke controller printKwSk username </br>";
            $dt = $this->input->post(NULL, TRUE);
            $x['dtKW'] = $this->fUmroh->getPrintKW($dt['regnos']);
            $x['from'] = $dt['from'];
            $x['to'] = $dt['to'];
            $x['dtVch'] = $this->fUmroh->getPrintVch($dt['regnos']);
            if($x['dtKW'] > 0 && $x['dtVch'] > 0){
                $this->load->view($this->folderView.'printKWtxt', $x);
                $this->load->view($this->folderView.'printVchtxt', $x);
            }
                      
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function getVoucherInfo(){
        if($this->username != null) {
            $x['form_header'] = 'Voucher K-Smart Religi';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_umroh/reprint/VoucherUmroh';
            $this->setTemplate($this->folderView.'getDtVoucher', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function voucherInfo(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['vchInfo'] = $this->fUmroh->getPrintVch($dt['regno']);
                if($x['vchInfo'] > 0){
                    $this->load->view($this->folderView.'dtVoucher', $x);
                }else{
                    echo setErrorMessage();
                }
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function reprintVoucher(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['dtVch'] = $this->fUmroh->getPrintVch($dt['regnos']);
            if($x['dtVch'] > 0){
                $this->load->view($this->folderView.'printVchtxt', $x);
            }else{
                echo "No data";
            }
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function form_installments(){
        if($this->username != null) {
            $x['form_header'] = 'Form Installments By Transfer';
            $x['icon'] = 'icon-pencil';
            $x['form_reload'] = 'be_umroh/installTrf';
            $x['bankList'] = $this->fUmroh->getBankList();
            $this->setTemplate($this->folderView.'formInstallTrf', $x);
            
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function helperRegnoInfo(){
        if($this->username != null) {
            //$this->load->model('webshop/Umroh_model','umrohModel');
            $dt = $this->input->post(null,true);
            $infoRegno = $this->fUmroh->showRegnoInfo(strtoupper($dt['regnos']));
            
            $res = jsonFalseResponse();
            if($infoRegno != null) {
                $res = jsonTrueResponse($infoRegno);  
            } 
            echo json_encode($res);
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function save_installments(){
        if($this->username != null) {
            if($this->form_validation->run('installments') == FALSE) {
                echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
            } else {
                $dt = $this->input->post(NULL, TRUE);
                $x['saveInstall'] = $this->fUmroh->saveInstallmentUmroh($dt);
				//$y['dtVch'] = $this->fUmroh->getPrintInst($dt['regnos']);
				
                //$this->load->view($this->folderView.'save_success', $x);
                if($x['saveInstall'] != null){
                 echo json_encode(jsonTrueResponse("Save Installments Success"));
				//$this->load->view($this->folderView.'printVchtxtInstall', $y);
				//	redirect('be_umroh/print/kw/txt', 'refresh');

                }else{
                    echo json_encode(jsonFalseResponse("Save Installments Failed"));
                    //cho "No Data";
                }
            }
        }else{
            redirect('backend', 'refresh');
        }
    }
	
	 function reprintKWx(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['dtVch'] = $this->fUmroh->getPrintInst($dt['regnose']);
            if($x['dtVch'] > 0){
                $this->load->view($this->folderView.'printVchtxtInstall', $x);
            }else{
                echo "No data";
            }
        }else{
			echo "No data";
            //redirect('backend', 'refresh');
        }
    }
    
    function getListVA(){
        if($this->username != null) {
            $x['form_header'] = 'Form Installments By Transfer';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_umroh/list/va';
            $this->setTemplate($this->folderView.'formListVA', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function getReportPost(){
        if($this->username != null) {
            $x['form_header'] = 'Posting Register Umroh';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'be_umroh/post';
            //$x['bankList'] = $this->fUmroh->getBankList();
            $this->setTemplate($this->folderView.'formReportPost', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    	
    function reportPostAct(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['status'] = $dt['status'];
            $dt['sort'] = '1';
            $dt['acdc'] = '0';
			//var_dump($dt);
            $x['detRes'] = $this->fUmroh->getListPost($dt);
            $this->load->view($this->folderView.'formReportPostRes', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    	
    function reportPostPrev(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
			//var_dump($dt);
			
            $x['data'] = $dt['registerno'];
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['status'] = $dt['status'];
			$this->load->view($this->folderView.'formReportPostPrev', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }

    function reportPostUpdate(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
			/*var_dump($dt);*/
			try {
				$param = set_list_array_to_string($dt['registerno']);
				$postVal = $dt['post'];
				$x['result'] = $this->fUmroh->getUpdatePost($param, $postVal, $this->username);
            	//$this->load->view($this->folderView.'formReportPostRes', $x);
            	$this->reportPostAct();
			} catch(Exception $e) {
				throw new Exception($e->getMessage(), 1);
			}
        }else{
           redirect('backend', 'refresh'); 
        }
			 
    }
    
    function getReportInstall(){
        if($this->username != null) {
            $x['form_header'] = 'Form Installments By Transfer';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'be_umroh/report/installment';
            //$x['bankList'] = $this->fUmroh->getBankList();
            $this->setTemplate($this->folderView.'formReportInstall', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportInstallAct(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['dataView'] = $dt['dataView'];
            $x['journeyType'] = $dt['journeyType'];
            $x['status'] = $dt['status'];
			$x['ordertype'] = $dt['ordertype'];
            $x['orderby'] = $dt['orderby'];
            $x['detCicilan'] = $this->fUmroh->getListInstall($dt);
            $this->load->view($this->folderView.'reportInstall_res_edit', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportPrintExcell(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['dataView'] = $dt['dataView'];
			/*
			$x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['banklist'] = $dt['banklist'];
            
			 */ 
			//$x['headCicilan'] = $this->fUmroh->getBankInfo($dt);
            $x['detCicilan'] = $this->fUmroh->getListInstall($dt);
            //$this->load->view($this->folderView.'printReport_excell', $x);
            $this->load->view($this->folderView.'printReport_excell_edit', $x);
        }
        else{
            
        }
    }

	function reportPrintExcellMDR(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            
			/*
			$x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['banklist'] = $dt['banklist'];
            
			 */ 
            $x['dataView'] = $dt['dataView'];
            $x['detCicilan'] = $this->fUmroh->getListInstallMDR($dt);
            //$this->load->view($this->folderView.'printReport_excell', $x);
            $this->load->view($this->folderView.'printReport_excell_editMDR', $x);
            //echo "masuk"
        }
        else{
            
        }
    }



    /*
        ===== @DionRespati =====
    */
    
	function getListRegUmroh() {
		if($this->username != null) {
		    $x['form_header'] = 'K-Smart Registration Listing';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_umroh/reg/list';
            $this->setTemplate($this->folderView.'getListRegUmroh', $x);	
		} else {
           redirect('backend', 'refresh');
        }	
	}
	
	function postListRegUmroh() {
		if($this->username != null) {
		   $res = jsonFalseResponse("Data tidak ditemukan");	
		   $arr = $this->fUmroh->getListRegUmroh();	
		   if($arr != null || !empty($arr)) {
		   		$res = jsonTrueResponse($arr);
		   }	
		   echo json_encode($res);
		} else {
           redirect('backend', 'refresh');
        }	
	}
	
	//$route['be_umroh/reg/list/print']='backend/finance_umroh/printListRegUmroh';
	function printListRegUmroh() {
		if($this->username != null) {
			$data['result'] = $this->fUmroh->getListRegUmroh();
			
			$this->load->view($this->folderView.'printListRegUmroh', $data);
		} else {
           redirect('backend', 'refresh');
        }	
	}
    
    
    function testUrlShorten(){
        $result = $this->fUmroh->getListRegUmroh();
        print_r($result);
        
        /*object(stdClass)#38 (3) 
        { ["kind"]=> string(16) "urlshortener#url" 
            ["id"]=> string(20) "http://goo.gl/0714Jj" 
            ["longUrl"]=> string(66) "http://www.lawankolesterol.com/index.php/home/affiliasi/HRRuLM5QVP" } */
    }
    
    function testSequenceNUmber(){
        
    }
	
	//========================hilal====================
	function getlistUploadToWeb(){
        if($this->username != null) {
            $x['form_header'] = 'Form Transfer Data To Website';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'be_umroh/listUpload/journey';
            //$x['bankList'] = $this->fUmroh->getBankList();
            $this->setTemplate($this->folderView.'formReportToWeb', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportUmrToWebAct(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['journey_type'] = $dt['journey_type'];
            //$x['headCicilan'] = $this->fUmroh->getBankInfo($dt);
            
            if($x['date_from'] != "" && $x['date_to'] != ""){
            	$x['detListUmroh'] = $this->fUmroh->getListUmrToWeb($dt);
            	$this->load->view($this->folderView.'reportUplUmrToWeb_res', $x);
			}else{
				echo "Please fill all date";
			}
        }else{
           redirect('backend', 'refresh'); 
        }
    }
    
    function reportUmrohToXlsWeb(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['detListUmroh'] = $this->fUmroh->getListUmrToWebCSV($dt);
            $this->load->view($this->folderView.'reportUplUmrToWebXls_res', $x);
        }
        else{
            
        }
    }

    function getCancel(){
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('alasan', 'alasan', 'trim|required|min_length[5]|max_length[12]');
        if($this->username != null) {
            $x['form_header'] = 'Form Cancel K-Smart';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'be_umroh/cancel';
            $x['bankList'] = $this->fUmroh->getBankList();

            $this->setTemplate($this->folderView.'formCancelKSmart2', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }

    function getListCancel(){
        $dt['result'] = $this->fUmroh->getListCancel();
        $this->load->view($this->folderView. 'formListCancelUmroh' ,$dt);
    }

    /** 
     * @Author: Ricky
     * @param: string $id
     * @var: $res $x
     * @Date: 2019-01-28 17:48:12 
     * @Desc: fetch data member by ID
     */    
    // $route['be_umroh/cancel/getmemberinfo/(:any)'] = 'backend/finance_umroh/getMemberDataById/$1';
    function getMemberDataById($id){
       $res = jsonFalseResponse("No result found");
		$x = $this->fUmroh->getMemberInfoCancel($id);
		if($x != null) {
			$res = jsonTrueResponse($x);
		}
		echo json_encode($res);
    }

    /** 
     * @Author: Ricky
     * @param string $id
     * @var $data
     * @Date: 2019-01-29 14:41:54 
     * @Desc: get member mutation detail  by ID
     */    
    // $route['be_umroh/cancel/getmutation/(:any)'] = 'backend/finance_umroh/getMutationDetail/$1';
    function getMutationDetail($id) {
        $data['result'] = $this->fUmroh->getMutation($id);
        $this->load->view($this->folderView. 'detailMutation', $data);
    }

    // $route['be_umroh/cancel/addmutation'] = 'backend/finane_umroh/addMutation';
    function addMutation() {
        $data = $this->input->post(null, true);
        $data['result'] = $this->fUmroh->getMemberInfoCancel($data['idmember']);
        $this->form_validation->set_data($data);
        if (empty($data['result'])) {
            echo "<script>alert('Tidak ada data')</script>";
        } else {
            $this->load->view($this->folderView. 'addCancelation', $data);
        }
    }

    // $route['be_umroh/cancel/savemutation'] = 'backend/finance_umroh/saveMutation';
    function saveMutation() {
        $data = $this->input->post(null, true);
        $count = count($data['registerno']);
		
		//BUAT NO BATCH PEMBATALAN CNTH FORMAT UMROH/CANCEL1901-001
		$batchno = $this->fUmroh->getBatchNo("UMROH/CANCEL");
        
		for($i=0; $i<$count; $i++) {
			$checkCancel = $this->fUmroh->checkCancel($data['registerno'][$i]);
			if($checkCancel != null) {
				//AKUMULASI DATA 
				$dt = array(
					'id' => $checkCancel[0]->id,
					'registerno' => $data['registerno'][$i],
					'dfno' => $data['dfno'][$i],
					'fullnm' => $data['fullnm'][$i],
					'alasan' => strtoupper($data['alasan'][$i]),
					'tanggal' => $data['tanggal'][$i],
					'banknm' => $data['banknm'][$i],
					'bankaccno' => $data['bankaccno'][$i],
					'installment_c' => $data['installment_c'][$i],
					'installment_d' => $data['installment_d'][$i],
					'sisa' => $data['sisa'][$i],
					'nama_rekening' => $data['nama_rekening'][$i]
				);
				$res = $this->fUmroh->saveCancelUmroh2($batchno, $dt);
				
				echo "<pre>";
				print_r ($dt);
				echo "</pre>";
				
			} else {
			}
        }
		$data['res'] = $this->fUmroh->getListCancelByParam("batchno", $batchno);
        $this->load->view($this->folderView.'cancelConfirmation', $data);
        backToMainForm();
        /* foreach ($data as $key ) {
            print_r($key[0]);
            if ($key['departure_status'] == '0') {
            }
        } */
    }
    
    // $route['be_umroh/cancel/print'] = 'backend/finance_umroh/printCancelUmroh';
	function printCancelUmroh() {
		$data = $this->input->post(NULL, TRUE);
		$data['result'] = $this->fUmroh->getListCancelByParam('batchno', $data['batchno']);
        $this->load->view($this->folderView.'printCancelUmrohPDF', $data);
	}

    function prosesCancelUmroh(){

        $idmember = $this->input->post('idmember');
        $idmember2 = $this->input->post('idmember2');

        $dt['tanggal'] = $this->input->post('tanggal');
        $dt['alasan'] = $this->input->post('alasan');
        $dt['norek'] = $this->input->post('norek');
        $dt['tgl'] = date("Ym", strtotime($dt['tanggal']));

        $dt['result'] = $this->fUmroh->cancelUmroh($idmember);
        $dt['result2'] = $this->fUmroh->cancelUmroh($idmember2);

//        if($dt['result'] != null){
//            $dt['no'] = $this->fUmroh->saveCancelUmroh($idmember,$dt['alasan']);
//        }

        //print_r($dt['result']);
        $this->load->view($this->folderView. 'formCancelPDF' ,$dt);
    }

	//START broadcast sms umroh
    function getListSMSPost(){
        if($this->username != null) {
            $x['form_header'] = 'List SMS Broadcast For Umroh';
            $x['icon'] = 'icon-search';
            $x['form_reload'] = 'be_umroh/listsmspost';
            //$x['bankList'] = $this->fUmroh->getBankList();
            $this->setTemplate($this->folderView.'formListSMSPost', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }

  	function reportSMSBCAct(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['status'] = $dt['status'];
            $x['sort'] = $dt['sort'];
            $x['acdc'] = $dt['acdc'];
			//var_dump($dt);
            $x['detRes'] = $this->fUmroh->getListPost($dt);
            $this->load->view($this->folderView.'formListSMSPostRes', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
  
  	
    function reportSMSBCPrev(){
        if($this->username != null) {
        	//echo "masuk sini ya";
            $dt = $this->input->post(NULL, TRUE);
			//var_dump($dt);
			
            $x['data'] = $dt['registerno'];
            $x['date_from'] = $dt['date_from'];
            $x['date_to'] = $dt['date_to'];
            $x['status'] = $dt['status'];
            $x['sort'] = $dt['sort'];
            $x['acdc'] = $dt['acdc'];
			$this->load->view($this->folderView.'formListSMSBCPrev', $x);
        }else{
           redirect('backend', 'refresh'); 
        }
    }
	//=======================end hilal=================


    
  
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Warehouse extends MY_Controller {
		
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/warehouse/";
		$this->load->service('backend/be_warehouse_service', 's_warehouse');
		$this->load->model('backend/be_transaction_model', 'm_trans');
	}
	
	/*--------------------------------------------
	 * HARUS DIPINDAH KE CONTROLLER TRANSACTION
	 --------------------------------------------*/
	 
	 //$route['trans/integrity/check'] = 'backend/warehouse/formCheckIntegrityTransaction';
	 public function formCheckIntegrityTransaction() {
	 	$data['form_header'] = "Transaction Integrity Check";
        $data['icon'] = "icon-list";
		$data['form_reload'] = 'trans/integrity/check';
		try {			
			$this->checkSessionBE();		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'shippingSummaryForm', $data); 
	 }
	 /*-------
	  * END
	  --------*/
	
	//$route['wh/shipping/summary'] = 'backend/warehouse/formShippingSummary';
	public function formShippingSummary() {
		$data['form_header'] = "Shipping Summary";
        $data['icon'] = "icon-list";
		$data['form_reload'] = 'wh/shipping/summary';
		try {			
			$this->checkSessionBE();
			$data['list_wh'] = $this->s_warehouse->getWHList();		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'shippingSummaryForm', $data); 
	}
	
	//$route['wh/shipping/summary/list'] = 'backend/warehouse/getShippingSummary';
	public function getShippingSummary() {
		try {			
			$this->checkSessionBE();
			$data['result'] = $this->s_warehouse->getShippingList();		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->load->view($this->folderView.'shippingSummaryPreview', $data);
	}
	
	//$route['wh/shipping/detail/(:any)'] = 'backend/warehouse/getDetailProductShipping/$1';
	public function getDetailProductShipping($orderno) {
		try {			
			$this->checkSessionBE();
			$data['result'] = $this->s_warehouse->getDetailProductShipping($orderno);		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->load->view($this->folderView.'shippingDetailPrd', $data);
	}
	
	//$route['wh/shipping/summary/print'] = 'backend/warehouse/printShippingSummary';
	public function printShippingSummary() {
		try {			
			$this->checkSessionBE();
			$data['form'] = $this->input->post(NULL, TRUE);
			$data['result'] = $this->s_warehouse->getPrintShippingSummary($data['form']);		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->load->view($this->folderView.'shippingSummaryPrintPdf', $data);
	}
	
	//$route['wh/shipping/label'] = 'backend/warehouse/formShippingLabel';
	function formShippingLabel() {
		$data['form_header'] = "Print Shipping Label BE";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'wh/shipping/label';
		$data['list_shipping'] = $this->m_trans->getListCargo();
		try {			
			$this->checkSessionBE();
			$data['list_wh'] = $this->s_warehouse->getWHList();	
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'shippingLabelForm', $data);
	}
	
	//$route['wh/shipping/label/list'] = 'backend/warehouse/getListShippingLabel';
	function getListShippingLabel() {
		
		try {			
			$this->checkSessionBE();
			$data['result'] = $this->s_warehouse->getListShippingLabel();		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
			//$data['result'] = null;
		}
		$this->load->view($this->folderView.'shippingLabelResult', $data);
		//$this->load->view('backend/transaction/getPrintShippingLabelResult', $data);
	}
	
	//$route['wh/releasevcr'] = 'backend/warehouse/getReleaseVcrSK';
	function getReleaseVcrSK() {
		$data['form_header'] = "Release Voucher Starterkit";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'wh/releasevcr';		
		try {			
			$this->checkSessionBE();	
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'releaseVcrSKForm', $data);
	}

 	//$route['wh/releasevcr/act'] = 'backend/warehouse/getReleaseVcrSKact';
	function getReleaseVcrSKact(){
		$dt = $this->input->post(NULL, TRUE);
		$searchby = $dt['searchby']; //dfno, cnno, batchno, trcd, orderno
		$paramValue = $dt['paramValue'];
		$from = $dt['trx_from'];
		$to = $dt['trx_to'];
				
 		if(($searchby == 'trcd'|| $searchby == 'receiptno') && 
 			($paramValue != null and $paramValue != "")){
 			$data['result'] = $this->s_warehouse->getListVcrByParam(0, $paramValue, $from, $to, $searchby);		
			$this->load->view($this->folderView.'releaseVcrSKresult', $data);		
		}  else if($searchby == 'vcno' && $paramValue != "") {
				$data['result'] = $this->s_warehouse->getDetailVoucher($paramValue);
				//$this->load->view($this->folderView.'voucherDetailByFormNo', $data);
				$this->load->view($this->folderView.'detailVoucher', $data);
			}
		
		elseif($searchby == 'trxdt' && $from != null && $to != null){
			$data['result'] = $this->s_warehouse->getListVcrByParam(1, $paramValue, $from, $to, $searchby);
			$this->load->view($this->folderView.'releaseVcrSKresult', $data);
		}else{
			redirect('backend', 'refresh');
		}
		//var_dump($data);
		
		
	}
	
	//$route['wh/releasevcr/det/(:any)'] = 'backend/warehouse/getReleaseVcrSKdet/$1';
	function getReleaseVcrSKdet($orderno) {
		if($this->username != null) {
			//echo $orderno;
			$param = explode("**", $orderno);
			$count = count($param);
			$data['result'] = null;
			
			if($count > 0){
				$data['trcd'] = $param[0];
				$data['receiptno'] = $param[1];
				$data['trdt'] = $param[2];
				$data['receiptdt'] = $param[3];
				$data['stockist'] = $param[4];
				
				$data['result'] = $this->s_warehouse->getListProductForSK($data['trcd']);
				//$data['voucherno'] = $this->s_warehouse->getStarterkitBrOrderno($data['trcd']);
				
			}
			
			if($data['result'] == null) {
				echo "false";
			} else {
				//$this->load->view($this->folderView.'releaseVcrSKdetail', $data);
				$this->load->view($this->folderView.'releaseVcrSKdetailByDion', $data);
			}
		} else {
			redirect('backend', 'refresh');
		}	
	}
	
	//$route['wh/releasevcr/product/(:any)/(:any)'] = 'backend/warehouse/detailReleasedSK/$1/$2';
	public function detailReleasedSK($trxno, $prdcd) {
		$data['voucherno'] = $this->s_warehouse->getStarterkitBrOrderno($trxno, $prdcd);
		//print_r($data['voucherno']);
		$this->load->view($this->folderView.'releasedVoucherList', $data);
	}
	
	//$route['wh/releasevcr/check/formno/(:any)/(:any)'] = 'backend/warehouse/checkFormNo/$1/$2';
	public function checkFormNo($voucherStart, $qty) {
		$res = $this->s_warehouse->getValidVoucher($voucherStart, $qty);
		echo json_encode($res);
		//print_r($data['voucherno']);
		//$this->load->view($this->folderView.'releasedVoucherList', $data);
	}
	
	//$route['wh/releasevcr/upd/(:any)/(:any)/(:any)'] = 'backend/warehouse/getReleaseVcrSKupd/$1/$2/$3';
	function getReleaseVcrSKupd($vchno, $trcd, $prdcd){
		if($this->username != null) {
			//echo '$vchno = '.$vchno.'<br/>'.$trcd;
			$usrname = $this->username;
			$srvReturn = jsonFalseResponse();
			$data['result'] = $this->s_warehouse->getCheckvoucher($vchno);
			
			$param = explode("**", $prdcd);
			$count = count($param);
			
			if($count > 0){
				$data['prdcd'] = $param[0];
			}
			
			//var_dump($data['result']);
			//echo $data['prdcd'];
			
			
			if($data['result'] != null) {
				$srvReturn = $this->s_warehouse->getReleaseVcrSKupd($vchno, $usrname, $trcd, $data['prdcd']);
				$srvReturn = jsonTrueResponse($srvReturn, null);	
				//var_dump($srvReturn);
			} /*else {
				$this->load->view($this->folderView.'releaseVcrSKdetail', $data);
			}*/
			echo json_encode($srvReturn); 
		} else {
			redirect('backend', 'refresh');
		}	
	}
	
	//$route['wh/releasevcr/save'] = 'backend/warehouse/saveReleaseVch';
	public function saveReleaseVch() {
		$data = $this->input->post(NULL, TRUE);
		try {			
			$this->checkSessionBE();	
			//$res = jsonTrueResponse(null, "ok");
			$res = $this->s_warehouse->updateReleaseVoucher($data);
			echo json_encode($res);
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
			$res = jsonFalseResponse($e->getMessage());
			echo json_encode($res);
		}
		//$this->setTemplate($this->folderView.'releaseVcrSKForm', $data);
		
	}
	
	//$route['wh/kgp/stk/update] = 'backend/warehouse/updateNullStockistKgp';
	public function updateNullStockistKgp() {
		$data['form_header'] = "Update Stockist for KGP Cargo";
        $data['icon'] = "icon-edit";
		$data['form_reload'] = 'wh/kgp/stk/update';
		try {			
			$this->checkSessionBE();
			$this->load->model('backend/be_warehouse_model', 'm_wh');		
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'updateStkForKgp', $data); 
	}
	
	//$route['wh/kgp/province] = 'backend/warehouse/listProvinceKgp';
	public function listProvinceKgp() {
		$data = $this->input->post(NULL, TRUE);
		$this->load->model('backend/be_warehouse_model', 'm_wh');		
		if($data['stkType'] == "1") {
			$res = $this->m_wh->getListProvinceWithEmptyStockist();
		} else if($data['stkType'] == "2") {
			$res = $this->m_wh->getListProvinceKgp();
		}
		echo json_encode($res);
	}
	
	//$route['wh/kgp/stk/list] = 'backend/warehouse/listStkByProvinceKgp';
	public function listStkByProvinceKgp() {
		$data = $this->input->post(NULL, TRUE);
		$this->load->model('backend/be_warehouse_model', 'm_wh');		
		if($data['stkType'] == "1") {
			$data['result'] = $this->m_wh->getListEmptyStkByProvinceKgp($data['provinsiNama']);
		} else if($data['stkType'] == "2") {
			$data['result'] = $this->m_wh->getListStkByProvinceKgp($data['provinsiNama']);
		}
		//print_r($res);
		$this->load->view($this->folderView.'listStkForKgp', $data); 
	}
	
	//$route['wh/kgp/stk/saveupd] = 'backend/warehouse/updateStkByProvinceKgp';
	function updateStkByProvinceKgp() {
		$data = $this->input->post(NULL, TRUE);
		$count = count($data['loccd']);
		$arr = array();
		for($i=0; $i<$count; $i++) {
			if($data['loccd'][$i] !== "") {
				$s['loccd'] = $data['loccd'][$i];
				$s['stkfullnm'] = $data['stkfullnm'][$i];
				$s['KotaKode'] = $data['KotaKode'][$i];
				$s['pricecode'] = $data['pricecode'][$i];
				array_push($arr, $s);
			}
		}
		$this->load->model("backend/be_warehouse_model", 'm_warehouse');
		$hasil = $this->m_warehouse->updateStkByProvinceKgp($arr);
		if($hasil['response'] == "true") {
			setSuccessMessage($hasil['message']);
		} else {
			setErrorMessage($hasil['message']);
		}
		
		//print_r($arr);
		
	}
	
	
}
	
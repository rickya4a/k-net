<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_umroh extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/umroh/";
		$this->load->service('backend/be_umroh_service', 's_umroh');
        
	}
	
    
    
    /*
        ===== @DionRespati =====
    */
    
	//$route['be_umroh'] = 'backend/backend_umroh/index';
	public function index($err = null) {
	   if($this->username != null) {
           $data['form_header'] = "Registrasi Umroh & Ziarah";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'be_umroh';
		   $data['jadwal'] = $this->s_umroh->getJadwalKeberangkatan();
		   $data['idstk'] = $this->s_umroh->getListStokist();
           $this->setTemplate($this->folderView.'getInputUmroh', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}	
	
	//$route['be_umroh/helper/journey/{:any}']='backend/backend_umroh/getTipePerjalanan/$1';
	public function getTipePerjalanan($tipe) {
		if($tipe == "1") {
			$arr = $this->s_umroh->getJadwalKeberangkatan();
		} else {
			$arr = $this->s_umroh->getJadwalZiarah();
		}
		echo json_encode($arr);
	}
	
	//$route['be_umroh/helper/distInfo/{:any}']='backend/backend_umroh/getDistributorInfo/$1';
	public function getDistributorInfo($idmember) {
	    if($this->username != null) {
           $arr = $this->s_umroh->getDistributorInfo(strtoupper($idmember));
		   echo json_encode($arr);
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['be_umroh/helper/ktp/(:any)']='backend/backend_umroh/getCheckNoKTP/$1';
	public function getCheckNoKTP($noktp) {
		  if($this->username != null) {
           $arr = $this->s_umroh->getCheckNoKTP(strtoupper($noktp));
		   echo json_encode($arr);
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['be_umroh/helper/hp/(:any)']='backend/backend_umroh/getCheckNoHP/$1';
	public function getCheckNoHP($noktp) {
		  if($this->username != null) {
           $arr = $this->s_umroh->getCheckNoHP($noktp);
		   echo json_encode($arr);
        } else {
           redirect('backend', 'refresh');
        } 
	}

    //$route['be_umroh/preview']='backend/backend_umroh/getPreviewInputUmroh';
	public function getPreviewInputUmroh() {
		if($this->username != null) {
            if($this->form_validation->run('umroh') == FALSE) {
                echo json_encode(jsonFalseResponse("Silahkan isi field yang di perlukan.."));
            } else {
            	$arr = array();
            	$dt = $this->input->post(null,true);
                if($dt != null) {
                	/*$cekKtp = $this->s_umroh->getCheckNoKTP($dt['idno']);
                    $cekHp = $this->s_umroh->getCheckNoKTP($dt['tel_hp']);
					$err = "data valid";
					$validUmur = date('Y') - $dt['thnlhr'];   
                        
                    //if($cekKtp > 0 && $dt['tipeJamaah'] != '2'){
                    if($cekKtp['response'] == "true" && $dt['tipeJamaah'] == '3'){	
                        $err = "Ktp Sudah Terdaftar";
						echo json_encode(jsonFalseResponse($err));
                    //} elseif($cekHp > 0){
                    } elseif($cekHp ['response'] == "true" && $dt['tipeJamaah'] == '3'){	
                        $err = "No Hp Sudah Terdaftar";
						echo json_encode(jsonFalseResponse($err));
                    } elseif($dt['tipeJamaah'] != '3' && $dt['idstk'] == '0'){
                        $err = "Silahkan Pilih Stockist";
						echo json_encode(jsonFalseResponse($err));
                    } elseif($validUmur < 18 && ($dt['statusJamaah'] == '2' ||$dt['statusJamaah'] == '3'||$dt['statusJamaah'] == '4' )){
                        $err = "Umur Kurang dari 18 Tahun,jika Hubungan Keluarga Istri/Suami/Saudara";
						echo json_encode(jsonFalseResponse($err));
                    } else { */
                    	
						$save = $this->s_umroh->saveDataUmroh($dt);
						echo json_encode($save);
					//}	
				}	
			}	
        } else {
           redirect('backend', 'refresh');
        } 
	}

	//$route['be_umroh/print/notaUmroh/(:any)']='backend/backend_umroh/printNotaUmroh/$1';
    function printNotaUmroh($regno) {
    	if($this->username != null) {
            //$regnos = $this->input->post('regnos');
			$this->load->model('webshop/Umroh_model','umrohModel');
            $dt['dtUmroh'] = $this->umrohModel->pdfUmroh($regno);
            if($dt['dtUmroh'] > 0){
                //$this->load->view('webshop/umroh/pdfUmroh',$dt);
				$this->load->view('webshop/rohani/pdfUmroh',$dt);
            }
        } else {
           redirect('backend', 'refresh');
        } 
    }
	
	function formListingRegUmroh(){
        if($this->username != null) {
            $x['form_header'] = 'Listing K-Smart Registration';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_umroh/listingReg';
            $this->setTemplate($this->folderView.'formListingRegUmroh', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
	
	function getListingRegUmroh(){
		if($this->username != null) {	
			$data = $this->input->post(null,true);
			$data['result'] = $this->s_umroh->getListingRegUmroh($data['reg_from'],$data['reg_to']);
			//print_r($data);
			$this->load->view($this->folderView.'getListingRegUmroh', $data);
		}
		else
        {
           redirect('backend', 'refresh');
        }
    }
	
	//$route['be_umroh/regno/(:any)'] = 'backend/backend_umroh/getDataUmrohByRegno/$1';
	function getDataUmrohByRegno($regno) {
		if($this->username != null) {				
			$data['result'] = $this->s_umroh->getDataUmrohByRegno($regno);
			$this->load->view($this->folderView.'getDataUmrohByRegno', $data);
		} else {
           redirect('backend', 'refresh');
        }
	}
    
    /*
        ===== @Ananech =====
    */
    
    function getDtSmsUmroh(){
        if($this->username != null) {
            $this->load->model("backend/be_umroh_model", 'bUmroh');
            $x['form_header'] = 'Listing Jamaah K-Smart Religi';
            $x['icon'] = 'icon-list';
            $x['form_reload'] = 'be_umroh/resendSms';
            $x['dtUmroh'] = $this->bUmroh->getDtUmroh();
            $this->setTemplate($this->folderView.'getListSmsUmroh', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function resendSmsUmroh($regno){
        //$alternate2 = $this->load->database('alternate2', true);
        if($this->username != null) {
            
            
            $this->load->model("backend/be_umroh_model", 'bUmroh');
            $x['dtJamaah'] = $this->bUmroh->getDtSmsUmroh($regno);
            if($x['dtJamaah'] != null)
            {
                foreach($x['dtJamaah'] as $row){
                    $noreg = $row->registerno;
                    $pktUmroh = $row->departuredesc;
                    $idmember = $row->dfno;
                    $nmmember = str_replace("`", "'", $row->fullnm);
                    $voucherNo = $row->voucherno;
                    $isVoucher = $row->is_voucher;
                    $flagtipe = $row->flag_tipe;
                    $telHp = $row->tel_hp;
//                    $telHp = "082370386796";
                    $secno = $row->secno;
                    $cnno = $row->CNno;
                    $kdbank = '88146';
                    $va = $kdbank.$row->novac;
					//tambahan dr Dion 31/8/2015
					$stk = $row->loccd;
                }
                $telHpTanto = "087780441874";
//                $telHpTanto = "085717743438";

				//$telHp = "087780441874";
                if($flagtipe == '1'){
                    $text = 'K-SMART RELIGI: Reg no: '.$noreg.' / '.$pktUmroh.',Id Member: '.$idmember.' / '.$nmmember.',VchNo: '.$voucherNo.',VA: '.$va.'';
                    $resSms = smsTemplate($telHp, $text);
					$resSms2 = smsTemplate($telHpTanto, $text);
                }elseif($flagtipe == '3'){
                    $text = 'K-SMART RELIGI: Reg no: '.$regno.' / '.$pktUmroh.',Id Member: '.$idmember.' / '.$nmmember.',Stockist:'.$stk.',';
                    
                    $text .= 'LANJUTAN: VchNo:'.$voucherNo.',SK: '.$secno.',MM: '.$cnno.',VA: '.$va.'';
                    $resSms = smsTemplate($telHp, $text);
					$resSms2 = smsTemplate($telHpTanto, $text);
					//$resSms = smsTemplate($telHp, $textLanjut);    
                    /*$smsLnjt = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                           VALUES ('".$telHp."','".$textLanjut."','K-Smart Umroh Status')";
                    $query2 = $alternate2->query($smsLnjt);*/
                }else{
                    $text = 'K-SMART RELIGI: Reg no: '.$noreg.' /  '.$pktUmroh.',Nama:'.$nmmember.',VchNo: '.$voucherNo.',VA: '.$va.'';
                    $resSms = smsTemplate($telHp, $text);
                }
				
				//echo $resSms;
                
                /*$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, senderid) 
		                    VALUES ('".$telHp."','".$text."','K-Smart Religi Status', 'MyPhone6')";
                
                //echo $insSms;
                $query = $alternate2->query($insSms);*/
                $this->load->view($this->folderView.'suksesSms', $x);
            }
        } 
        else
        {
           redirect('backend', 'refresh');
        }
    }
    

//$route['be_umroh/be_mt940'] = 'backend/backend_umroh/getUploadMT940';
	public function getUploadMT940($err = null) {
	   if($this->username != null) {
           $data['form_header'] = "Upload File MT940";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'be_umroh/be_mt940';
           $data['form_action'] = 'be_umroh/save_upload';
           $this->setTemplate($this->folderView.'getUploadMT', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}	
	
//$route['product/save'] = 'backend/product/saveInputProduct';
	public function actUploadMT940() {
	  /* 	
            $this->load->library('csvreader');
    		
            $fileName = $_FILES["fileUpload"]["tmp_name"];  
    		$this->csvreader->set_separator(":");     
            $data['csvData'] = $this->csvreader->parse_file($fileName);
            
            
            var_dump(array_shift($data['csvData']));
            echo "<br>";
	   */ 
      $lokasi_file = $_FILES["fileUpload"]["tmp_name"];
	
	  if(isset($lokasi_file))
	  {
        $txfufile = file($lokasi_file);
		    $arr = array();
			$i = 0;
			//print_r($txfufile);
            foreach($txfufile as $row) 
    		{
    		      $element = trim($row);
				  //DELIMITER
                  $txfuslice = explode(":",$element);
				  array_push($arr, $txfuslice);
				  $i++;	  
				  
    		}
			//print_r($arr);
			
			$axx = array();
			foreach($arr as $dta) {
				$i = 0;
				$jml = count($dta);
				for($x=0;$x<$jml;$x++) {
					if($dta[$x] == "61" || $dta[$x] == "86") {
						$nilai = $x + 1;
                        $xx = array("kode" => $dta[$x], "nilai" => $dta[$nilai]);
						$ax = array_push($axx, $xx);
					}
				}
				
			}
			//print_r($axx);
			
			$jxx = count($axx);
			for($s=0; $s < $jxx; $s++) {
				if(substr($axx[$s]['nilai'],0,3) == "UBP") {
                    $prevS = $s-1;
					echo "Kode : ".$axx[$prevS]['kode']." : Nilai : ".$axx[$prevS]['nilai']."<br />";
                    echo "Kode : ".$axx[$s]['kode']." : Nilai : ".$axx[$s]['nilai']."<br />";
				}
			}
      }
		
	}


    function sendBC(){
        if($this->username != null) {
            //echo "masuk sini ya";
            $this->load->model("backend/be_umroh_model", 'bUmroh');
            $dt = $this->input->post(NULL, TRUE);
            //var_dump($dt);

            //Start Penomoran Otomatis
            $bln= date('m');
            $thn= date('y');

            $query_01 = $this->bUmroh->getbatchBC1();
            foreach($query_01 as $row1) {
                $batch= $row1->batch;
            }

            $query_02 = $this->bUmroh->getbatchBC2($batch);
            foreach($query_02 as $row2) {
                $idmax = $row2->max_id;
            }

            $idmax++;
            //$nomor=$idmax++;
            $kode_batch=$thn.$bln.sprintf("%05s",$idmax);
            //END Penomoran Otomatis

            $telHp="081388995258"; //nomor purchasing umroh
            $telHp2="081807278131"; //nomor pak hilal
            $text1 = $dt['sms1'];
            $text2 = $dt['sms2'];
            $text3 = $dt['sms3'];
            $total=count($dt['registerno']);

            $this->bUmroh->insertsendBChdr($kode_batch,$total,$text1,$text2,$text3); //insert header


            foreach($dt['registerno'] as $k=>$v) {
                $query_03 = $this->bUmroh->getBChp($v);
                foreach($query_03 as $row3) {
                    $tel_Hp = $row3->tel_hp;
                }


                if($text1 != ""){
                    smsTemplate($tel_Hp, $text1);
                    //smsTemplate($telHp, $text1);
                }

                if($text2 != ""){
                    smsTemplate($tel_Hp, $text2);
                    //smsTemplate($telHp, $text2);
                }

                if($text3 != ""){
                    smsTemplate($tel_Hp, $text3);
                    //smsTemplate($telHp, $text3);
                }

                $this->bUmroh->insertsendBCdet($v,$kode_batch,$tel_Hp); //insert detail
                /*smsTemplate($telHp, $text1." ".$k." ".$tel_Hp);
                smsTemplate($telHp, $text2." ".$k." ".$tel_Hp);
                smsTemplate($telHp, $text3." ".$k." ".$tel_Hp);*/
            }


            //kirim sms ke pak hilal
           if($text1 != ""){smsTemplate($telHp, $text1);smsTemplate($telHp2, $text1);}

           if($text2 != ""){smsTemplate($telHp, $text2);smsTemplate($telHp2, $text2);}

           if($text3 != ""){smsTemplate($telHp, $text3);smsTemplate($telHp2, $text3);}

           /* if($text1 != ""){
                smsTemplate($telHp, $text1." ".$tel_Hp);
            }

            if($text2 != ""){
                smsTemplate($telHp, $text2);
            }

            if($text3 != ""){
                smsTemplate($telHp, $text3);
            }*/

            //$this->load->view($this->folderView.'formListSMSBCPrev', $x);
        }else{
            redirect('backend', 'refresh');
        }
    }

}
	
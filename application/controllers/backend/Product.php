<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product extends MY_Controller {
	public function __construct() {
	    parent::__construct();
		$this->folderView = "backend/product/";
		$this->load->service('backend/be_product_service');
		//$this->load->model('backend/be_product_model');
	}
	
	/*--------------------------
	 * PRODUCT  CATEGORY
	 * ------------------------ */
	 
	//$route['product/cat'] = 'backend/product/getInputProductCat';
	public function getInputProductCat() {
		if($this->username != null) {
           $data['form_header'] = "Input Product Category";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'product/cat';
           $this->setTemplate($this->folderView.'getInputProductCat', $data);
        } else {
           redirect('backend', 'refresh');
        } 
	}
	
	//$route['product/cat/list'] = 'backend/product/getListAllProductCat';
	//$route['product/cat/list/json'] = 'backend/product/getListAllProductCat/$1';
	public function getListAllProductCat($type = "array") {
		$data['listPrdCat'] = null;
    	try {
    		$data['listPrdCat'] = $this->be_product_service->listAllProductCat();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllProductCat', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrdCat']));
		}	
	}
	
	//$route['product/cat/id/(:any)'] = 'backend/product/getListProductCatByID/$1';	
	
	//$route['product/cat/list/(:any)/(:any)'] = 'backend/product/getListProductCat/$1/$2';
	public function getListProductCat($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_product_service->listUserProductCatService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	
	//$route['product/cat/save'] = 'backend/product/saveInputProductCat';
	public function saveInputProductCat() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product/cat') == TRUE) {
    			$srvReturn = $this->be_product_service->inputProductCatService();	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	//$route['product/cat/update'] = 'backend/product/saveUpdateProductCat';
	public function saveUpdateProductCat() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product/cat') == TRUE) {
    			$srvReturn = $this->be_product_service->updateProductCatService();	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	//$route['product/cat/delete/(:any)'] = 'backend/product/deleteProductCat/$1';
	public function deleteProductCat($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_product_service->deleteProductCatService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	
	/*--------------------
	 * PRODUCT
	 *-------------------*/
	//$route['product'] = 'backend/product/product/getInputProduct';
	public function getInputProduct() {		
        if($this->username != null) {
           $data['form_header'] = "Input Product";
           $data['form_action'] = base_url('user/group');
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'product';
		   $data['listPrdCat'] = $this->be_product_service->listAllProductCat();
		   $data['listPrd'] = $this->be_product_service->listAllProductService(false);
           $this->setTemplate($this->folderView.'getInputProduct', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	}
	public function getInputProductANP() {
		if($this->username != null) {
			$data['form_header'] = "Input Product (ANP)";
			$data['form_action'] = base_url('user/group');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'productANP';
			$data['listPrdCat'] = $this->be_product_service->listAllProductCat();
			$data['listPrd'] = $this->be_product_service->listAllProductService(false);
			$this->setTemplate($this->folderView.'getInputProductANP', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
	public function getInputBannerANP() {
		if($this->username != null) {
			$data['form_header'] = "Maintenance Banner (ANP)";
			$data['form_action'] = base_url('user/group');
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'bannerANP';
//			$data['listPrdCat'] = $this->be_product_service->listAllProductCat();
//			$data['listPrd'] = $this->be_product_service->listAllProductService(false);
			$this->setTemplate($this->folderView.'getInputBannerANP', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
	public function saveInputBanner() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		try {
			$this->form_validation->set_rules('hdr_desc', 'hdr_desc', 'required');
			if($this->form_validation->run() == TRUE) {
				$srvReturn = $this->be_product_service->inputBannerService();
				if($srvReturn['response'] == "true" && $_FILES["myfile"]['error'] == 0) {
					$img_name = $this->uploadBannerImage();
				}
			}
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
		}
		echo json_encode($srvReturn);
	}
	public function getListAllBanner($type = "array") {
		$data['listPrd'] = null;
		try {
			$data['listPrd'] = $this->be_product_service->listAllBannerService();
		} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllBanner', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrd']));
		}
	}
	public function saveUpdateBanner() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
		try {
			$this->form_validation->set_rules('hdr_desc', 'hdr_desc', 'required');
			if($this->form_validation->run() == TRUE) {
				$srvReturn = $this->be_product_service->updateBannerService();
				//print_r($_FILES["myfile"]);
				if($srvReturn['response'] == "true" && isset($_FILES["myfile"]) && $_FILES["myfile"]['name'] != "") {
					$img_name = $this->uploadBannerImage();
				}
			}
		} catch(Exception $e) {
			$srvReturn = jsonFalseResponse($e->getMessage());
		}
		echo json_encode($srvReturn);
	}
	//$route['product/list'] = 'backend/product/getListAllProduct';
	//$route['product/list/json'] = 'backend/product/getListAllProduct/$1';
	public function getListAllProduct($type = "array") {
		$data['listPrd'] = null;
    	try {
    		$data['listPrd'] = $this->be_product_service->listAllProductService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllProduct', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrd']));
		}	
	}
	
	//$route['product/id/(:any)'] = 'backend/product/getListProductByID/$1';
	public function getListProductByID($id) {
		$this->load->model('backend/be_product_model');
		$srvReturn = $this->be_product_model->getListPrdBundlingHeader($id);
		echo json_encode($srvReturn);
	}
	//$route['product/list/(:any)/(:any)'] = 'backend/product/getListProduct/$1/$2';
	public function getListProduct($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_product_service->listProductService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	//$route['product/save'] = 'backend/product/saveInputProduct';
	public function saveInputProduct() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product') == TRUE) {
    			$srvReturn = $this->be_product_service->inputProductService();
				if($srvReturn['response'] == "true" && $_FILES["myfile"]['error'] == 0) {
	                $img_name = $this->uploadProductImage();                 
	            }	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}

	private function uploadProductImage() {
        //$output_dir = "assets/product_img/original/";
        $output_dir = "assets/images/products/thumb/";
        $thumb_dir = "assets/product_img/thumbnail/";
        
        if(isset($_FILES["myfile"]))
        {
            $ret = array();
        
            $error =$_FILES["myfile"]["error"];
            
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData() 
            if(!is_array($_FILES["myfile"]["name"])) //single file
            {
                $fileName = $_FILES["myfile"]["name"];
                //$imageTitle = $_POST["imageTitle"];
                $imageTitle = "";  
                $dirfile = $output_dir.$fileName;
                move_uploaded_file($_FILES["myfile"]["tmp_name"],$dirfile);
                //$this->createThumbnails($dirfile, $thumb_dir, $fileName);
                //$this->m_admproduct->saveImagesProduct($idpict, $fileName, $imageTitle);
                $ret[]= $fileName;
            }
            
            return $ret;
         }   
   }
	private function uploadBannerImage() {
		//$output_dir = "assets/product_img/original/";
		$output_dir = "assets/images/banner_home/";
		$thumb_dir = "assets/product_img/thumbnail/";

		if(isset($_FILES["myfile"]))
		{
			$ret = array();

			$error =$_FILES["myfile"]["error"];

			//You need to handle  both cases
			//If Any browser does not support serializing of multiple files using FormData()
			if(!is_array($_FILES["myfile"]["name"])) //single file
			{
				$fileName = $_FILES["myfile"]["name"];
				//$imageTitle = $_POST["imageTitle"];
				$imageTitle = "";
				$dirfile = $output_dir.$fileName;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$dirfile);
				//$this->createThumbnails($dirfile, $thumb_dir, $fileName);
				//$this->m_admproduct->saveImagesProduct($idpict, $fileName, $imageTitle);
				$ret[]= $fileName;
			}

			return $ret;
		}
	}
	public function getListBanner($param, $value) {
		$srvReturn = jsonFalseResponse();
		try {
			$srvReturn = $this->be_product_service->listBannerService($param, $value);
		} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	//$route['product/update'] = 'backend/product/saveUpdateProduct';
	public function saveUpdateProduct() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product') == TRUE) {
    			$srvReturn = $this->be_product_service->updateProductService();	
				//print_r($_FILES["myfile"]);
				if($srvReturn['response'] == "true" && isset($_FILES["myfile"]) && $_FILES["myfile"]['name'] != "") {
	                $img_name = $this->uploadProductImage();                 
	            }	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	//$route['product/delete/(:any)'] = 'backend/product/deleteProduct/$1';
	public function deleteProduct($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_product_service->deleteProductService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	
	/*--------------------
	 * PRICE CODE
	 *-------------------*/
	 //$route['pricecode'] = 'backend/product/getInputPriceCode';
	 public function getInputPriceCode() {
	 	if($this->username != null) {
           $data['form_header'] = "Input Price Code";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'pricecode';
           $this->setTemplate($this->folderView.'getInputPriceCode', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	 }
	 //$route['pricecode/list'] = 'backend/product/getListAllPriceCode';
	 //$route['pricecode/list/json'] = 'backend/product/getListAllPriceCode/$1';
	 public function getListAllPriceCode($type = "array") {
		$data['listPriceCode'] = null;
    	try {
    		$data['listPriceCode'] = $this->be_product_service->listAllPriceCodeService();		
    	} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAllPriceCode', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPriceCode']));
		}	
	}
	 //$route['pricecode/id/(:any)'] = 'backend/product/getListPriceCodeByID/$1';
	 //$route['pricecode/list/(:any)/(:any)'] = 'backend/product/getListPriceCode/$1/$2';
	 public function getListPriceCode($param, $value) {
		$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_product_service->listPriceCodeService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	}
	 
	 //$route['pricecode/postlist/(:any)'] = 'backend/product/postListPriceCode/$1';
	 public function postListPriceCode($param) {
	 	$value = $this->input->post('param');
	 	$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_product_service->listPriceCodeService($param, $value);		
    	} catch(Exception $e) {}
		echo json_encode($srvReturn);
	 }
	 
	 //$route['pricecode/save'] = 'backend/product/saveInputPriceCode';
	 public function saveInputPriceCode() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('pricecode') == TRUE) {
    			$srvReturn = $this->be_product_service->inputPriceCodeService();	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	 
	 //$route['pricecode/update'] = 'backend/product/saveUpdatePriceCode';
	 public function saveUpdatePriceCode() {
		$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('pricecode') == TRUE) {
    			$srvReturn = $this->be_product_service->updatePriceCodeService();	
    		}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	 
	 //$route['pricecode/delete/(:any)'] = 'backend/product/deletePriceCode/$1';
	 public function deletePriceCode($id) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->be_product_service->deletePriceCodeService($id);		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	}
	 
	 /*--------------------
	 * PRODUCT PRICE
	 *-------------------*/
	 //$route['product/price'] = 'backend/product/getInputProductPrice';
	 public function getInputProductPrice() {
	 	if($this->username != null) {
           $data['form_header'] = "Input Product Price";
           $data['icon'] = "icon-pencil";
		   $data['form_reload'] = 'product/price';
		   $data['listPriceCode'] = $this->be_product_service->listPriceCodeService("status", "1");
     
           $this->setTemplate($this->folderView.'getInputProductPrice', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	 }
	 //$route['product/price/check'] = 'backend/product/checkProductPriceCode';
	 public function checkProductPriceCode() {
	 	$srvReturn = jsonFalseResponse();
    	try {
    		$srvReturn = $this->be_product_service->checkProductPriceCodeService();		
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	 }
	 //$route['product/price/list'] = 'backend/product/getListAllProductPrice';
	 //$route['product/price/list/json'] = 'backend/product/getListAllProductPrice/$1';
	 //$route['product/price/id/(:any)'] = 'backend/product/getListProductPriceByID/$1';
	 //$route['product/price/list/(:any)/(:any)'] = 'backend/product/getListProductPrice/$1/$2';
	 
	 //$route['product/price/save'] = 'backend/product/saveInputProductPrice';
	 public function saveInputProductPrice() {
	 	$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product/price') == TRUE) {
    			$srvReturn = $this->be_product_service->inputProductPriceService();	
    		} 
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	 }
	 //$route['product/price/update'] = 'backend/product/saveUpdateProductPrice';
	 public function saveUpdateProductPrice() {
	 	$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		if($this->form_validation->run('product/price') == TRUE) {
    			$srvReturn = $this->be_product_service->updateProductPriceService();	
    		} 
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	 }
	 
	 //$route['product/price/delete/(:any)'] = 'backend/product/deleteProductPrice/$1';
	 
	 /*--------------------------
	 * PRODUCT  BUNDLING
	 * ------------------------ */
	 //$route['product/bundling'] = 'backend/product/getInputProductBundling';
	 public function getInputProductBundling() {
	 	if($this->username != null) {
           $data['form_header'] = "Input Product Bundling";
		   $data['form_reload'] = 'product/bundling';
           $data['icon'] = "icon-pencil";
		   $data['list'] = $this->be_product_service->getListAllProductBundling();
           $this->setTemplate($this->folderView.'getInputProductBundling', $data); 
        } else {
           redirect('backend', 'refresh');
        } 
	 }
	 //$route['product/bundling/list'] = 'backend/product/getListAllProductBundling';
	 //$route['product/bundling/list/json'] = 'backend/product/getListAllProductBundling/$1';
	 //$route['product/bundling/id/(:any)'] = 'backend/product/getListProductBundlingByID/$1';
	 public function getListProductBundlingByID($id) {
	 	$srvReturn = $this->be_product_service->getListProductBundlingByID($id);
		echo json_encode($srvReturn);
	 }
	 
	 //$route['product/bundling/id2'] = 'backend/product/getListProductBundlingByIDPost';
	 public function getListProductBundlingByIDPost() {
		$data = $this->input->post(NULL, TRUE); 
	 	$srvReturn = $this->be_product_service->getListProductBundlingById_v2($data['cat_inv_id_parent']);
		echo json_encode($srvReturn);
	 }
	 
	 //$route['product/bundling/list/(:any)/(:any)'] = 'backend/product/getListProductBundling/$1/$2';
	 public function getListProductBundling($param, $id) {
	 	$srvReturn = $this->be_product_service->getListProductBundling($param, $id);
		echo json_encode($srvReturn);
	 }
	 //$route['product/bundling/save'] = 'backend/product/saveInputProductBundling';
	 public function saveInputProductBundling() {
	 	$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		//if($this->form_validation->run('pricecode') == TRUE) {
    			$srvReturn = $this->be_product_service->saveInputProductBundling();	
    		//}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	 }
	 //$route['product/bundling/update'] = 'backend/product/saveUpdateProductBundling';
	 public function saveUpdateProductBundling() {
	 	$srvReturn = jsonFalseResponse(requiredFieldMessage());
    	try {
    		//if($this->form_validation->run('pricecode') == TRUE) {
    			$srvReturn = $this->be_product_service->saveUpdateProductBundling();	
    		//}
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn);
	 }
	 
	 //$route['product/bundling/delete/(:any)'] = 'backend/product/deleteProductBundling/$1';
	 
	 /*--------------------------
	 * PRODUCT  CLAIM
	 * ------------------------ */
	 //$route['product/claim'] = 'backend/product/formProductClaim';
	 public function formProductClaim() {   
	    $data['form_header'] = "Employee Product Claim Trx";
        $data['icon'] = "icon-list";
		$data['form_reload'] = 'product/claim';
		try {			
			$this->checkSessionBE();	
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'productClaimForm', $data); 
	 }	

     //$route['product/claim/list'] = 'backend/product/getProductClaim';
	 public function getProductClaim() {   
		try {			
			$this->checkSessionBE();
			$data['result'] = $this->be_product_service->getProductClaim();	
			//print_r($data['result']);
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->load->view($this->folderView.'productClaimResult', $data); 
	 }	

	//$route['product/claim/detail/(:any)'] = 'backend/product/getProductClaimDetail/$1';
	public function getProductClaimDetail($id) {
		try {			
			$this->checkSessionBE();
			$data['result'] = $this->be_product_service->getProductClaimDetail($id);	
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->load->view($this->folderView.'productClaimDetail', $data); 
	}
	
	/*----------------------------------------------
	 * PRODUCT IMPORT FROM PINEAPPLE TO DB COMMERCE
	 ----------------------------------------------*/
	 
	 //$route['product/pine/import'] = 'backend/product/formImportProductPine';
	 public function formImportProductPine() {
	 	$data['form_header'] = "Import Product from Pineapple";
        $data['icon'] = "icon-pencil";
		$data['form_reload'] = 'product/pine/import';
		try {			
			$this->checkSessionBE();	
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		$this->setTemplate($this->folderView.'importProductPineForm', $data);
	 }
	 //$route['product/pine/list/id/(:any)'] = 'backend/product/getListProductPineByID/$1';
	 public function getListProductPineByID() {
	 	$data = $this->input->post(NULL, TRUE);	
		try {			
			$this->checkSessionBE();
			$data['result'] = $this->be_product_service->getListProductPineByID($data['prdcd']);	
		} catch(Exception $e) {
			$data['err_msg'] = $e->getMessage();
		}
		//print_r($data);
		$this->load->view($this->folderView.'importProductPineListByID', $data);
	 }
	 //$route['product/pine/import/save'] = 'backend/product/saveImportProductFromPine';
	 public function saveImportProductFromPine() {
	 	$data = $this->input->post(NULL, TRUE);
		//print_r($data);
		$srvReturn = null;
    	try {
    		$this->checkSessionBE();
    		$srvReturn = $this->be_product_service->saveImportProductFromPine($data);			
    	} catch(Exception $e) {
    		$srvReturn = jsonFalseResponse($e->getMessage());
    	}
		echo json_encode($srvReturn); 
	 }

	public function getInputProductNon() {
		if($this->username != null) {
			$data['form_header'] = "Input Product Non Available";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'product/nonavailable';

			$type = "array";

			$data['listPrd'] = null;
			$data['listPrd2'] = null;
			try {
				$data['listPrd'] = $this->be_product_service->listAllProductService2();
				//$data['listPrd2'] = $this->be_product_service->cekProductAllNon();
			} catch(Exception $e) { }
			if($type == "array") {
				$this->setTemplate($this->folderView.'getInputProductNon', $data);
			} else {
				echo json_encode(jsonTrueResponse($data['listPrd2']));
			}


		} else {
			redirect('backend', 'refresh');
		}
	}

	public function getListAll($type = "array") {

		$res = $this->input->post(NULL,TRUE);
		$loccd=$res['paramValue'];

		$data['search'] = $res['paramValue'];
		$data['searchName'] = $res['paramName'];
		$data['listPrd'] = null;

		try {
			$data['listPrd'] = $this->be_product_service->listAllProductService2($loccd);
		} catch(Exception $e) { }

		if($type == "array") {
			$this->load->view($this->folderView.'getListAll', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrd']));
		}
	}

	public function getListAll2($type = "array") {
		$data['listPrd'] = null;
		try {
			$data['listPrd'] = $this->be_product_service->listAllProductService();
		} catch(Exception $e) { }
		if($type == "array") {
			$this->load->view($this->folderView.'getListAll', $data);
		} else {
			echo json_encode(jsonTrueResponse($data['listPrd']));
		}
	}

	public function cek_stokist() {
		$data = $this->input->post(NULL, TRUE);
		$loccd= $data['kd_stk'];

		$dts = $this->be_product_service->cekStokist($loccd);
		if($dts != null){
			foreach($dts as $row){
				$nmstk=$row->fullnm;
				//echo "kode".$nmstk;
			}
			//print_r($dts);
			$response="true";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);

		}else{
			$nmstk="";
			$response="false";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);
		}
	}

	public function cek_nonavailable() {
		$data = $this->input->post(NULL, TRUE);
		$loccd= $data['kd_stk'];
		$prdcd= $data['kd_brg'];

		$dts = $this->be_product_service->cekNonAvailable($loccd,$prdcd);
		if($dts != null){
			foreach($dts as $row){
				$nmstk=$row->prdnm;
				//echo "kode".$nmstk;
			}
			//print_r($dts);
			$response="true";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);

		}else{
			$nmstk="";
			$response="false";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);
		}
	}

	public function save_nonavailable() {
		$data = $this->input->post(NULL, TRUE);
		$loccd= $data['kd_stk'];
		$prdcd= $data['kd_brg'];

		$dt = $this->be_product_service->cekProduct($prdcd);
		if($dt != null){
			foreach($dt as $row){
				$prdnm=$row->prdnm;
			}
		}else{
			$prdnm="";
		}

		$this->be_product_service->saveNonAvailable($loccd,$prdcd,$prdnm);
		$this->be_product_service->updtNonAvailable($prdcd);

		$dts = $this->be_product_service->cekNonAvailable($loccd,$prdcd);
		if($dts != null){
			foreach($dts as $row){
				$nmstk=$row->prdcd;
				//echo "kode".$nmstk;
			}
			//print_r($dts);
			$response="true";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);

		}else{
			$nmstk="";
			$response="false";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);
		}
	}

	public function del_nonavailable() {
		$data = $this->input->post(NULL, TRUE);
		$loccd= $data['kd_stk'];
		$prdcd= $data['kd_brg'];

		$dt = $this->be_product_service->cekProduct($prdcd);
		if($dt != null){
			foreach($dt as $row){
				$prdnm=$row->prdnm;
			}
		}else{
			$prdnm="";
		}

		$this->be_product_service->delNonAvailable($loccd,$prdcd,$prdnm);
		$this->be_product_service->updtNonAvailable2($prdcd);

		$dts = $this->be_product_service->cekNonAvailable($loccd,$prdcd);
		$nmstk="Berhasil";
		$response="true";
		$arr = array("response" =>$response,"message" =>$nmstk);
		echo json_encode($arr);

		/*if($dts == null){
			foreach($dts as $row){
				$nmstk=$row->prdcd;
				//echo "kode".$nmstk;
			}
			//print_r($dts);
			$response="true";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);

		}else{
			$nmstk="";
			$response="false";
			$arr = array("response" =>$response,"message" =>$nmstk);
			echo json_encode($arr);
		}*/
	}

}	
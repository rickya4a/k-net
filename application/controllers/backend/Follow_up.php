<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Follow_up extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if($this->username != null) {
			$this->folderView = "backend/followup/";
		    $this->load->model("backend/m_follow_up", "follow_up");	
			
		} else {
			echo $this->sessExpMessage;
		}
	}	
	
	//$route['followup/mkdirtest'] = 'backend/follow_up/tesCreateDir';
	function tesCreateDir() {
		//mkdir("/asset/images/los", 0777);
		/*if (!mkdir("/asset/images/los/FU100001/", 0, true)) {
		    die('Failed to create folders...');
		}*/
		
		$folderName = "asset/images/los/F009898_1";
        //rmdir($folderName);
		if ( !file_exists($folderName) ) {
		    mkdir($folderName);
			chmod($folderName, 0777);
			
		}
	}	
	
	//$route['followup/edit'] = 'backend/follow_up/followupEditForm';
	public function followupEditForm() {	
		$data['form_header'] = "Update Follow Up Complaint";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/edit';
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForSelect());
		$data['cust_type'] = selectArray($this->follow_up->getDataCustTypeForSelect());
		//$data['status_list'] = selectArray($this->follow_up->getListStatusForSelect());
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/edit/list')";
        $this->setTemplate($this->folderView.'followupEditForm', $data);		
	}	
	//$route['followup/edit/list'] = 'backend/follow_up/getListUpdateFollowup';
	public function getListUpdateFollowup() {
		$form = $this->input->post(NULL, TRUE);
		$data['listFollowUp'] = $this->follow_up->getListUnprocesedFollowup($form);
		$data['listStatus'] = $this->follow_up->getListStatusForSelect();
		$this->load->view($this->folderView.'followupEditList', $data);
	}    
	
	
	//$route['followup/memb/(:any)/(:any)'] = 'backend/follow_up/getMemberInfo/$1/$2';
	
	//$route['followup/edit/save'] = 'backend/follow_up/saveEditFollowup';
	public function saveEditFollowup() {
		$data = $this->input->post(NULL, TRUE);
		$save = $this->follow_up->saveEditFollowup($data);
		echo json_encode($save);
	}
	
	//$route['followup/delete/id/(:any)'] = 'backend/follow_up/deleteFollowUp/$1';
	public function deleteFollowUp($id) {
		$data = $this->input->post(NULL, TRUE);
		$save = $this->follow_up->deleteFollowUp($id);
		echo json_encode($save);
	}
	
	//$route['followup/edit/id/(:any)'] = 'backend/follow_up/formEditFollowUp/$1';
	public function formEditFollowUp($id) {
		 
		$data['fid'] = $id;
		$data['header'] = $this->follow_up->getDetailFollowUpforUpdate($id);
		if($data['header'][0]->followup_type == "7") {
			$data['footer'] = $this->follow_up->getDataDetailFollowUpForUpdateBV($id);
		} else {
			$data['footer'] = $this->follow_up->getDataDetailFollowUpForUpdate($id);	
		}
		
		$data['comm_method'] = selectArray($this->follow_up->getListCommMethod());
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForSelect());
		$data['cust_type'] = selectArray($this->follow_up->getDataCustTypeForSelect());
		$data['listLampiran'] = $this->follow_up->getListAllAttachment();
		$this->load->view($this->folderView.'followupEditForm', $data);
	}
	
	public function getMemberInfo($param, $value){
		$arr = $this->follow_up->getMemberInfo($param, $value);
		$res = $arr != null ? jsonTrueResponse($arr, "OK") : jsonFalseResponse("Data member tidak ada di database");
		echo json_encode($res);
	}
	
	//$route['followup'] = 'backend/follow_up/followupInputForm';
	public function followupInputForm() {	
		$data['form_header'] = "Input Follow Up Complaint";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup';
		$data['comm_method'] = selectArray($this->follow_up->getListCommMethod());
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForSelect());
		$data['cust_type'] = selectArray($this->follow_up->getDataCustTypeForSelect());
		$data['listLampiran'] = $this->follow_up->getListAllAttachment();
        $this->setTemplate($this->folderView.'followupInputForm', $data); 			
	}	
	
	//$route['followup/bv/form'] = 'backend/follow_up/transferBVForm';
	public function transferBVForm() {
		$this->load->view($this->folderView.'followupTrfBVForm');
	}
	
	//$route['followup/bv/ttp/add/(:any)'] = 'backend/follow_up/transferBvCheckTTP';
	public function transferBvCheckTTP($id) {
		$arr = $this->follow_up->checkSalesByOrderNo($id);
		echo json_encode($arr);
	}
	
	//$route['followup/save'] = 'backend/follow_up/saveFollowupInput';
	public function saveFollowupInput() {
		$data = $this->input->post(NULL, TRUE);
		$save = $this->follow_up->saveFollowupInput($data);
		echo json_encode($save);
	}
	
	//$route['followup/list'] = 'backend/follow_up/followupListForm';
	public function followupListForm() {
		$data['form_header'] = "Follow Up List";
        $data['icon'] = "icon-list";
	    $data['form_reload'] = 'followup/list';
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForSelect());
		$data['cust_type'] = selectArray($this->follow_up->getDataCustTypeForSelect());
		$data['status_list'] = selectArray($this->follow_up->getListStatusForSelect());
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/list/get')";
        $this->setTemplate($this->folderView.'followupListForm', $data);
	}
	
	//$route['followup/detail/id/(:any)'] = 'backend/follow_up/getFollowUpDetailByID/$1';
 	public function getFollowUpDetailByID($id) {
 		$data['followup'] = $this->follow_up->getDetailFollowUpByID($id);
		//print_r($data['followup']);
		//$data['pic'] = $this->follow_up->getListPic($id);
		$this->load->view($this->folderView.'followupDetail', $data);
 	}
	
	//$route['followup/update/id/(:any)'] = 'backend/follow_up/getUpdFollowUpDetailByID/$1';
 	public function getUpdFollowUpDetailByID($id) {
 		$data['followup'] = $this->follow_up->getDetailFollowUpByID($id);
		//print_r($data['followup']['los']);
		$this->load->view($this->folderView.'followupDetailToUpdate2', $data);
 	}
	
	//$route['followup/update/bv/(:any)/(:any)/(:any)'] = 'backend/follow_up/getUpdateBV/$1/$2/$3';
	public function getUpdateBV($id, $followup_id, $trx_no) {
 		//$data['followup'] = $this->follow_up->getDetailFollowUpByID($id);
 		$data['rec_id'] = $id;
 		$data['formno'] = $followup_id;
		$data['no_ttp'] = $trx_no;
		$data['trf_bv'] = $this->follow_up->getDetailTrfBV($followup_id, $trx_no);
		$data['detailTrx'] = $this->follow_up->getDetailTrxByTrcd("orderno", $trx_no);
		//print_r($data['detailTrx']);
		$this->load->view($this->folderView.'followupBvUpdateForm', $data);
 	}
	
	//$route['followup/trx_klink/(:any)'] = 'backend/follow_up/getDetailTrxByTrcd/$1';
	function getDetailTrxByTrcd($trx_no) {
		$arr = jsonFalseResponse("Trx No atau Order No tidak valid.. ");
		$res = $this->follow_up->getDetailTrxByTrcd("orderno", $trx_no);
		if($res != null) {
			$arr = array("response" => "true", "arrayData" => $res['hasil'], "tipetrx" => $res['tipe'], 
			  "batas_update" => $res['batas_update'], "product" => $res['product']);
		}
		echo json_encode($arr);
	}
	
	//$route['followup/update/bv/save'] = 'backend/follow_up/saveUpdateBv';
	function saveUpdateBv() {
		
		$data = $this->input->post(NULL, TRUE);
		$return = jsonFalseResponse("Data Trx $data[trcd] gagal di update..");
		$res = $data['tipetrx'] == "inv" ? $this->follow_up->updateInvoice($data) : $this->follow_up->updateCnMs($data);
		$return =  $res ? jsonTrueResponse(null, "Data Trx $data[trcd] berhasil di update..") : jsonFalseResponse("Data Trx $data[trcd] gagal di update..");
		echo json_encode($return);
	}
	
	//$route['followup/process'] = 'backend/follow_up/followupProcess';
	public function followupProcess() {
		$data['form_header'] = "Proses Follow Up";
        $data['icon'] = "icon-list";
	    $data['form_reload'] = 'followup/process';
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForSelect());
		$data['cust_type'] = selectArray($this->follow_up->getDataCustTypeForSelect());
		$data['status_list'] = selectArray($this->follow_up->getListStatusForSelect());
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/process/list')";
        $this->setTemplate($this->folderView.'followupListForm', $data);
	}
	
	//$route['followup/status/update'] = 'backend/follow_up/updateStatusFollowUp';
	function updateStatusFollowUp() {
		$arr = jsonFalseResponse("Status gagal di update");
		$data = $this->input->post(NULL, TRUE);
		$res = $this->follow_up->updateStatusFollowUp($data);
		if($res) {
			$arr = jsonTrueResponse(null, "Status berhasil diupdate");
		}
		echo json_encode($arr);
	}

    //$route['followup/process/list'] = 'backend/follow_up/getListFollowUpToProcess';
	public function getListFollowUpToProcess() {
		$form = $this->input->post(NULL, TRUE);
		$data['listFollowUp'] = $this->follow_up->getListFollowupAdis($form);
		$data['listStatus'] = $this->follow_up->getListStatusForSelect();
		$this->load->view($this->folderView.'followupInputProcessList', $data);
	}    
	
	//$route['followup/process/excel'] = 'backend/follow_up/ExportFUtoExcel';
	public function ExportFUtoExcel() {
		$data['form'] = $this->input->post(NULL, TRUE);
		$data['listFollowUp'] = $this->follow_up->getListDataToExcel($data['form']);
		$this->load->view($this->folderView.'ExportFUtoExcel', $data);
	}  
	//$route['followup/resign-reactive/(:any)'] = 'backend/follow_up/followupResignReactive/$1';
	public function followupResignReactive($tipe) {
		//$data['title'] = $tipe == "1" ? "Pengunduran diri" : "Reactivation ID";	
		$data['followup_type'] = $tipe;
		$data['title'] = $this->follow_up->getDataFollowTypeByID($tipe);
		$this->load->view($this->folderView.'followupResignReactive', $data);
	}
	
	//$route['followup/form/resign/(:any)/(:any)/(:any)'] = 'backend/follow_up/formResignation/$1/$2/$3';
	public function formResignation($id, $followup_id, $idmember) {
		$data['id'] = $id;
		$data['followup_id'] = $followup_id;
		$data['idmember'] = $idmember;
		$data['legend'] = true;
		//$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
		$xx = $this->follow_up->getDetailResign($id, $idmember);
		if($xx != null) {
			$data['detailMember'] = $xx;
			$data['update'] = 1;
		} else {
			$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
			$data['update'] = 0;
		}

		$data['backButton'] = true;
		$data['dataFollow'] = null;
		$data['errMsg'] = true;
		$this->load->view($this->folderView.'followupResignForm', $data);
	}
	
	//$route['followup/form/resign'] = 'backend/follow_up/formResignation2';
	public function formResignation2() {
		$data['form_header'] = "Resignation / Termination";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/form/resign';
		$data['legend'] = false;
		$data['id'] = "";
		$data['followup_id'] = "";
		$data['idmember'] = "";
		$data['detailMember'] = null;
		$data['update'] = 0;
		$data['backButton'] = false;
		$data['dataFollow'] = $this->follow_up->listUnprocesedFollowUpByType("1");
		$data['errMsg'] = false;
		if($data['dataFollow'] != null) {
			$data['errMsg'] = true;
		}
		$this->setTemplate($this->folderView.'followupResignForm', $data);
	}
	
	//$route['followup/resign/save']= 'backend/follow_up/saveResignTerminate';
	public function saveResignTerminate() {
		$data = $this->input->post(NULL, TRUE);
		if($data['followup_id'] != "") {
			$arr = $this->follow_up->updateStatResignReactive($data['followup_id'], $data);
		} else {
			$arr = $this->follow_up->insertPengunduranDiri($data['fid'], $data);
		} 
		echo json_encode($arr);
	}
	
	//$route['followup/exchange/acc/(:any)/(:any)/(:any)'] = 'backend/follow_up/exchangeAccForm/$1/$2/$3';
	public function exchangeAccForm($id, $followup_id, $idmember) {
		$data['id'] = $id;
		$data['followup_id'] = $followup_id;
		$data['idmember'] = $idmember;
		$data['legend'] = true;
		//$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
		//$data['footer'] = $this->follow_up->getDetailExcAcc($id);
		
		$xx = $this->follow_up->getDetailExcAcc($id);
		if($xx != null) {
			$data['detailMember'] = $xx;
			$data['update'] = 1;
		} else {
			$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
			$data['update'] = 0;
		}
		
		$data['backButton'] = true;
		$data['dataFollow'] = null;
		$data['errMsg'] = true;
		$this->load->view($this->folderView.'exchangeAccForm', $data);
	}
 
	//$route['followup/exchange/acc'] = 'backend/follow_up/exchangeAccForm2';
	public function exchangeAccForm2() {
		$data['form_header'] = "Exchange Account";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/exchange/acc';
		$data['legend'] = false;
		$data['id'] = "";
		$data['followup_id'] = "";
		$data['idmember'] = "";
		$data['detailMember'] = null;
		$data['backButton'] = false;
		$data['update'] = 0;
		$data['dataFollow'] = $this->follow_up->listUnprocesedFollowUpByType("4");
		$data['errMsg'] = false;
		if($data['dataFollow'] != null) {
			$data['errMsg'] = true;
		}
		$this->setTemplate($this->folderView.'exchangeAccForm', $data);
	}

    //$route['followup/exchange/acc/save']= 'backend/follow_up/saveExchangeAcc';
	public function saveExchangeAcc() {
		$data = $this->input->post(NULL, TRUE);
		if($data['followup_id'] != "") {
			$arr = $this->follow_up->updateStatExchangeAcc($data['followup_id'], $data);
		} else {
			$arr = $this->follow_up->insertExchangeAcc($data['fid'], $data);
		} 
		echo json_encode($arr);
	}

    
	
    //$route['followup/form/reactive/(:any)/(:any)/(:any)'] = 'backend/follow_up/formReactive/$1/$2/$3';
	
	//$route['followup/resign-reactive/u(:any)'] = 'backend/follow_up/followupResignReactive/$1';
	
	//$route['followup/transfer/line/(:any)/(:any)/(:any)'] = 'backend/follow_up/tranferLineForm/$1/$2/$3';
	public function tranferLineForm($id, $followup_id, $idmember) {
		$data['id'] = $id;
		$data['followup_id'] = $followup_id;
		$data['idmember'] = $idmember;
		$data['legend'] = true;
		//$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
		
		$xx = $this->follow_up->getDetailTrfName($id, $idmember);
		if($xx != null) {
			$data['detailMember'] = $xx;
			$data['update'] = 1;
		} else {
			$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
			$data['update'] = 0;
		}
		
		$data['backButton'] = true;
		$data['dataFollow'] = null;
		$data['errMsg'] = true;
		$data['detailTrfLine'] = $this->follow_up->getDetFollowupNonBV($id);
		$this->load->view($this->folderView.'tranferLineForm', $data);
	}
	
	//$route['followup/transfer/line'] = 'backend/follow_up/tranferLineForm2';
	public function tranferLineForm2() {
		$data['form_header'] = "Transfer Line / Pindah Sponsor";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/transfer/line';
		$data['legend'] = false;
		$data['id'] = "";
		$data['followup_id'] = "";
		$data['idmember'] = "";
		$data['detailMember'] = null;
		$data['backButton'] = false;
		$data['update'] = 0;
		$data['dataFollow'] = $this->follow_up->listUnprocesedFollowUpByType("3");
		$data['errMsg'] = false;
		if($data['dataFollow'] != null) {
			$data['errMsg'] = true;
		}
		$this->setTemplate($this->folderView.'tranferLineForm', $data); 	
	}
    //$route['followup/transfer/line/save'] = 'backend/follow_up/tranferLineSave';
    public function tranferLineSave() {
    	$data = $this->input->post(NULL, TRUE);
		$this->form_validation->set_rules('dfno', 'ID Member', 'required');
		$this->form_validation->set_rules('new_sponsorid', 'Sponsor/Reqruiiter', 'required');
		if ($this->form_validation->run() == FALSE) {
			$arr = jsonFalseResponse("Silahkan lengkapi data yang di perlukan..");
		} else {
			if($data['followup_id'] != "") {
				//$arr = $this->follow_up->updateStatTrfLine($data['followup_id'], $data);
				$arr = $this->follow_up->updateStatTrfLine2($data['rec_id'], $data);
				//$arr = $this->follow_up->insertTrfLine($data['followup_id'], $data);
			} else {
				$arr = $this->follow_up->insertTrfLine($data['fid'], $data);
			} 
		}
		echo json_encode($arr);
    }
	
	//$route['followup/transfer/recruiter/(:any)/(:any)/(:any)'] = 'backend/follow_up/tranferRecruiterForm/$1/$2/$3';
	public function tranferRecruiterForm($id, $followup_id, $idmember) {
		$data['id'] = $id;
		$data['followup_id'] = $followup_id;
		$data['idmember'] = $idmember;
		$data['legend'] = true;
		//$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
		
		$xx = $this->follow_up->getDetailTrfName($id, $idmember);
		if($xx != null) {
			$data['detailMember'] = $xx;
			$data['update'] = 1;
		} else {
			$data['detailMember'] = $this->follow_up->getMemberInfoRecruiter("dfno", $idmember);
			$data['update'] = 0;
		}
		
		$data['backButton'] = true;
		$data['dataFollow'] = null;
		$data['errMsg'] = true;
		$data['detailTrfLine'] = $this->follow_up->getDetFollowupNonBV($id);
		$this->load->view($this->folderView.'tranferRecruiterForm', $data);
	}
	
	//$route['followup/transfer/recruiter'] = 'backend/follow_up/tranferRecruiterForm';
	public function tranferRecruiterForm2() {
		$data['form_header'] = "Update Recruiter";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/transfer/recruiter';
		$data['legend'] = false;
		$data['id'] = "";
		$data['followup_id'] = "";
		$data['idmember'] = "";
		$data['detailMember'] = null;
		$data['backButton'] = false;
		$data['update'] = 0;
		$data['dataFollow'] = $this->follow_up->listUnprocesedFollowUpByType("8");
		$data['errMsg'] = false;
		if($data['dataFollow'] != null) {
			$data['errMsg'] = true;
		}
		$this->setTemplate($this->folderView.'tranferRecruiterForm', $data); 	
	}
    //$route['followup/transfer/recruiter/save'] = 'backend/follow_up/tranferRecruiterSave';
    public function tranferRecruiterSave() {
    	$data = $this->input->post(NULL, TRUE);
		$this->form_validation->set_rules('dfno', 'ID Member', 'required');
		$this->form_validation->set_rules('new_sponsorid', 'Sponsor/Reqruiiter', 'required');
		if ($this->form_validation->run() == FALSE) {
			$arr = jsonFalseResponse("Silahkan lengkapi data yang di perlukan..");
		} else {
			if($data['followup_id'] != "") {
			    $arr = $this->follow_up->updateStatTrfRecruiter2($data['rec_id'], $data);
			} else {
				$arr = $this->follow_up->insertTrfRecruiter($data['fid'], $data);
			} 
		}
		
		echo json_encode($arr);
    }
	
	
    //$route['followup/transfer/name/(:any)/(:any)/(:any)'] = 'backend/follow_up/tranferNameForm/$1/$2/$3';
    public function tranferNameForm($id, $followup_id, $idmember) {
		$data['id'] = $id;
		$data['followup_id'] = $followup_id;
		$data['idmember'] = $idmember;
		$data['legend'] = true;
		//$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
		
		$xx = $this->follow_up->getDetailTrfName($id, $idmember);
		if($xx != null) {
			$data['detailMember'] = $xx;
			$data['update'] = 1;
		} else {
			$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
			$data['update'] = 0;
		}
		
		$data['backButton'] = true;
		$data['dataFollow'] = null;
		$data['errMsg'] = true;
		$this->load->view($this->folderView.'transferNameForm', $data);
	}
    
    //$route['followup/transfer/name'] = 'backend/follow_up/tranferNameForm2';
    public function tranferNameForm2() {
		$data['form_header'] = "Transfer Name";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/transfer/name';
		$data['legend'] = false;
		$data['id'] = "";
		$data['followup_id'] = "";
		$data['idmember'] = "";
		$data['update'] = 0;
		$data['detailMember'] = null;
		$data['backButton'] = false;
		$data['dataFollow'] = $this->follow_up->listUnprocesedFollowUpByType("2");
		$data['errMsg'] = false;
		if($data['dataFollow'] != null) {
			$data['errMsg'] = true;
		}
		$this->setTemplate($this->folderView.'transferNameForm', $data);	
			
	}
    
    //$route['followup/transfer/name/save'] = 'backend/follow_up/tranferNameSave';
	public function tranferNameSave() {
		$data = $this->input->post(NULL, TRUE);
		$this->form_validation->set_rules('dfno', 'Username', 'trim|required');
		$this->form_validation->set_rules('receiver_name', 'Nama Penerima', 'trim|required');
		//$this->form_validation->set_rules('idno', 'No KTP', 'trim|required');
		$this->form_validation->set_rules('dd', 'Date', 'trim|required');
		$this->form_validation->set_rules('mm', 'Date', 'trim|required');
		$this->form_validation->set_rules('yy', 'Date', 'trim|required');
		$this->form_validation->set_rules('stockist', 'Stockist', 'trim|required');
		if ($this->form_validation->run() == TRUE)  {
	    	if($data['followup_id'] != "") {
				$arr = $this->follow_up->updateStatTrfName($data['followup_id'], $data);
			} else {
				$arr = $this->follow_up->insertTrfName($data['fid'], $data);
			}
		} else {
			$arr = jsonFalseResponse("Isi field yang di perlukan..");
		}	 
		echo json_encode($arr);
	}


	//$route['followup/correction/name/(:any)/(:any)/(:any)'] = 'backend/follow_up/correctionNameForm/$1/$2/$3';
	public function correctionNameForm($id, $followup_id, $idmember) {
		$data['id'] = $id;
		$data['followup_id'] = $followup_id;
		$data['idmember'] = $idmember;
		$data['legend'] = true;
		//$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
		
		$xx = $this->follow_up->getDetailTrfName($id, $idmember);
		if($xx != null) {
			$data['detailMember'] = $xx;
			$data['update'] = 1;
		} else {
			$data['detailMember'] = $this->follow_up->getMemberInfo("dfno", $idmember);
			$data['update'] = 0;
		}
		
		$data['backButton'] = true;
		$data['dataFollow'] = null;
		$data['errMsg'] = true;
		$this->load->view($this->folderView.'correctionNameForm', $data);
	}
    //$route['followup/correction/name'] = 'backend/follow_up/correctionNameForm2';
    //$route['followup/correction/name/save'] = 'backend/follow_up/correctionNameSave';
    public function correctionNameSave() {
		$data = $this->input->post(NULL, TRUE);
		$this->form_validation->set_rules('dfno', 'Username', 'trim|required');
		$this->form_validation->set_rules('receiver_name', 'Nama Penerima', 'trim|required');
		//$this->form_validation->set_rules('idno', 'No KTP', 'trim|required');
		if ($this->form_validation->run() == TRUE)  {
	    	if($data['followup_id'] != "") {
				$arr = $this->follow_up->updateStatCorrectionName($data['followup_id'], $data);
			} else {
				$arr = $this->follow_up->insertCorrectionName($data['fid'], $data);
			}
		} else {
			$arr = jsonFalseResponse("Isi field yang di perlukan..");
		}	 
		echo json_encode($arr);
	}
	
	//$route['followup/type'] = 'backend/followup/followupTypeInputForm';
 	public function followupTypeInputForm() {
			$data['form_header'] = "Follow Up Type Maintenance";
            $data['icon'] = "icon-pencil";
		    $data['form_reload'] = 'followup/type';
			$this->setTemplate($this->folderView.'followupTypeInputForm', $data); 	
	}
	
	//$route['followup/list/get'] = 'backend/followup/getListFollowup';	
	public function getListFollowup() {
		$form = $this->input->post(NULL, TRUE);
		$data['listFollowUp'] = $this->follow_up->getListFollowup($form);
		$data['listStatus'] = $this->follow_up->getListStatusForSelect();
		$this->load->view($this->folderView.'followupInputList', $data);
	}
	
	//$route['followup/type/save'] = 'backend/followup/saveFollowUpType';
	public function saveFollowUpType() {
		$this->form_validation->set_rules('followup_type', 'Follow Up Type', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$data = $this->input->post(NULL, TRUE);
			
			$ins = $this->follow_up->saveFollowUpType($data);
			if($ins) {
				echo setSuccessMessage("Save Follow Up Type success..");
			} else {
				echo setErrorMessage("Save Follow Up Type failed..");
			}
        } else {
            echo setErrorMessage("Follow Up Type can not be empty..");
        }		
	}

	//$route['followup/type/update'] = 'backend/follow_up/updateFollowUpType';
	public function updateFollowUpType() {
		$this->form_validation->set_rules('followup_type', 'Follow Up Type', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$data = $this->input->post(NULL, TRUE);
			
			$ins = $this->follow_up->updateFollowUpType($data);
			if($ins) {
				$arr = jsonTrueResponse(null, "Update Follow Up Type success..");
			} else {
				$arr = jsonFalseResponse("Update Follow Up Type failed..");
			}
        } else {
            $arr = jsonFalseResponse("Follow Up Type can not be empty..");
        }		
		echo json_encode($arr);	
	}
	
 	//$route['followup/type/list'] = 'backend/follow_up/followupTypeList';
 	public function followupTypeList() {
 		$data['list'] = $this->follow_up->getListFollowUpType();
		$this->load->view($this->folderView.'followupTypeList', $data);
 	}
 	
	//$route['followup/los'] = 'backend/follow_up/createLOS';
	public function createLOS() {
		$data['form_header'] = "Generate LOS";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/los';
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForSelect());
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/los/type/list')";
		$data['los_act'] = "Followup.previewGenerateLOS(this.form.id,'followup/los/save')";
		$this->setTemplate($this->folderView.'LosCreate', $data); 
	}
 	//$route['followup/los/type/list'] = 'backend/follow_up/getListFollowupLOS';
 	public function getListFollowupLOS() {
 		$form = $this->input->post(null, true);
 		//if($form['followup_type'] == "1") {
 			$data['listData'] = $this->follow_up->listPengunduranDiri($form);
			//print_r($data);
			$this->load->view($this->folderView.'LosCreateShowList', $data);
 		//}
 		
 	}
 	
 	
 	//$route['followup/los/list'] = 'backend/follow_up/listLOS';
 	public function listLOS() {
 		$data['form_header'] = "LOS Generated List";
        $data['icon'] = "icon-list";
	    $data['form_reload'] = 'followup/los/list';
		$data['followup_type'] = selectArray($this->follow_up->getDataFollowTypeForlos());
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/los/list/get')";
		$data['los_act'] = "Followup.previewGenerateLOS(this.form.id,'followup/los/save')";
		$this->setTemplate($this->folderView.'LosList', $data); 
 	}

    //$route['followup/los/save'] = 'backend/follow_up/saveLOS';
 	public function saveLOS() {
 		$form = $this->input->post(null, true);
 		$arr = $this->follow_up->saveLOS($form);
 		echo json_encode($arr);
 	}
	
	//$route['followup/los/list/get'] = 'backend/follow_up/getListLOS';
	public function getListLOS() {
		$form = $this->input->post(null, true);
 		$data['result'] = $this->follow_up->getListLOS($form);
		//print_r($data['result']);
		$this->load->view($this->folderView.'LosListResult', $data);
	}

    //$route['followup/los/print/namecorrection/(:any)'] = 'backend/follow_up/printCorrectionNameLos/$1';
	public function printCorrectionNameLos($id) {
		$data['result'] = $this->follow_up->getDetailDataLos($id);
		$this->load->view($this->folderView.'LosPrintCorrName_pdf', $data);
	}
	//$route['followup/los/print/trfname/(:any)'] = 'backend/follow_up/printTransferName/$1';
	public function printTransferName($id) {
		$data['result'] = $this->follow_up->getDetailDataLos($id);
		$this->load->view($this->folderView.'LosPrintTrfName_pdf', $data);
	}
    //$route['followup/los/print/trfline/(:any)'] = 'backend/follow_up/printTransLine/$1';
    public function printTransLine($id) {
		//$data['result'] = $this->follow_up->getDetailDataLos($id);
		$data['result'] = $this->follow_up->getDataTrfLinebyLosNo($id);
		//print_r($data['result']);
		$this->load->view($this->folderView.'LosPrintTrfLine_pdf', $data);
	}
	
	//$route['followup/los/print/trfrecruiter/(:any)'] = 'backend/follow_up/printTransRecruiter/$1';
    public function printTransRecruiter($id) {
		$res = $this->follow_up->getDetailDataLos($id);
		$res2 = $this->follow_up->getDetailDataLosByFid($res[0]->followup_id);
		if(count($res2 > 1)) {
			$data['result'] = $res2;
			//print_r($res2);
		} else {
			$data['result'] = $res;
			
		}
        //print_r($data['result']);
		$this->load->view($this->folderView.'LosPrintTrfRecruiter_pdf', $data);
	}
	
    //$route['followup/los/print/excacc/(:any)'] = 'backend/follow_up/printExchangeAccount/$1';
    public function printExchangeAccount($id) {
		$data['result'] = $this->follow_up->getDetailDataLos($id);
		$this->load->view($this->folderView.'LosPrintExchangeAcc_pdf', $data);
	}
	
	//$route['followup/los/print/resign/(:any)'] = 'backend/follow_up/printResignation/$1';
	public function printResignation($id) {
		$data['result'] = $this->follow_up->getDataLOSbyLosNo($id);
		$this->load->view($this->folderView.'LosPrintResign_pdf', $data);
	}
	
	//$route['followup/los/print/resign'] = 'backend/follow_up/printResignation2';
	public function printResignation2() {
		$form = $this->input->post(NULL, TRUE);
		$arr = set_list_array_to_string($form['followup_id']);
		$data['result'] = $this->follow_up->getDetailResignationBySelectedID($arr);
		//print_r($data['result']);
		$this->load->view($this->folderView.'LosPrintResign_pdf', $data);
	}
	
	//$route['followup/los/print/reactive/(:any)'] = 'backend/follow_up/printReactivation/$1';
	public function printReactivation($id) {
		$data['result'] = $this->follow_up->getDataLOSbyLosNo($id);
		//print_r($data['result']);
		$this->load->view($this->folderView.'LosPrintReactivation_pdf', $data);
	}
	
	//$route['followup/los/print/reactive'] = 'backend/follow_up/printReactivation2';
	public function printReactivation2() {
		$form = $this->input->post(NULL, TRUE);
		$arr = set_list_array_to_string($form['followup_id']);
		$data['result'] = $this->follow_up->getDetailDataLosBySelectedID($arr);
		//print_r($data['result']);
		$this->load->view($this->folderView.'LosPrintReactivation_pdf', $data);
	}
	
	// $route['followup/los/batch'] = 'backend/follow_up/formBatchLos';
	public function formBatchLos() {
		$data['form_header'] = "Generate Batch LOS";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/los/batch';
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/los/batch/list')";
		$data['save'] = "Followup.saveBatchLos(this.form.id,'followup/los/batch/save')";
		$this->setTemplate($this->folderView.'LosBatchForm', $data); 
 	}
	
    //$route['followup/los/batch/list'] = 'backend/follow_up/getListLosByDate';
	public function getListLosByDate() {
		$form = $this->input->post(NULL, TRUE);
		$data['listData'] = $this->follow_up->getListGeneratedLos($form);
		//print_r($data['listData']);
		$this->load->view($this->folderView.'LosGeneratedList', $data);
	}
    //$route['followup/los/batch/save'] = 'backend/follow_up/saveBatchLos';
    public function saveBatchLos() {
    	$form = $this->input->post(null, true);
 		$arr = $this->follow_up->saveBatchLos($form);
 		echo json_encode($arr);
    }
    
	//$route['followup/batch/list'] = 'backend/follow_up/formListBatchLos';
	public function formListBatchLos() {
		$data['form_header'] = "View Batch LOS";
        $data['icon'] = "icon-list";
	    $data['form_reload'] = 'followup/batch/list';
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/batch/list/get')";
		//$data['save'] = "Followup.saveBatchLos(this.form.id,'followup/los/batch/save')";
		$this->setTemplate($this->folderView.'LosBatchListForm', $data); 
	}
	
	//$route['followup/batch/list/get'] = 'backend/follow_up/getListBatchLos';
	public function getListBatchLos() {
		$form = $this->input->post(NULL, TRUE);
		$data['listAcc'] = $this->follow_up->getListFollowupAcc();
		$data['listData'] = $this->follow_up->getListBatchLos($form);
		//print_r($data['listData']);
		$this->load->view($this->folderView.'LosBatchList', $data);
	}
	
	//$route['followup/batch/id/(:any)'] = 'backend/follow_up/getDetailBatchById/$1';
	public function getDetailBatchById($id) {
		$data['listData'] = $this->follow_up->getDetailBatchById($id);
		//print_r($data['listData']);
		$this->load->view($this->folderView.'LosBatchDetail', $data);
	}

    //$route['followup/batch/acc/update'] = 'backend/follow_up/updateReqAccBatch';
    public function updateReqAccBatch() {
    	$arr = jsonFalseResponse("Update Acc Status failed..");
    	$form = $this->input->post(NULL, TRUE);
    	$res = $this->follow_up->updateReqAccBatch($form);
		if($res > 0) {
			$arr = jsonTrueResponse(null, "Update Acc Status success..");
		}
		echo json_encode($arr);
    }
	
	//$route['followup/los/upload'] = 'backend/follow_up/losUploadForm';
	public function losUploadForm() {
		$data['form_header'] = "Upload Scan LOS";
        $data['icon'] = "icon-pencil";
	    $data['form_reload'] = 'followup/los/upload';
		$data['fu_from'] = date("Y")."-".date("m")."-"."01";
		$data['fu_to'] = date("Y-m-d");
		$data['act'] = "All.postListData(this.form.id,'followup/los/upload/get')";
		//$data['save'] = "Followup.saveBatchLos(this.form.id,'followup/los/batch/save')";
		$this->setTemplate($this->folderView.'losUploadForm', $data); 
	}
	
	//$route['followup/los/upload/get'] = 'backend/follow_up/getListFU';
	public function getListFU() {
		$data['listData'] = $this->follow_up->listFollowUpForUpload();
		//print_r($data['listData']);
		$this->load->view($this->folderView.'losUploadFormResult', $data);
	}
	
	//$route['followup/los/upload/form/(:any)'] = 'backend/follow_up/getFormUpload/$1';
	public function getFormUpload($id) {
		$data['listData'] = $this->follow_up->getDataFollowForUpload($id);
		//print_r($data['listData']);
		$this->load->view($this->folderView.'uploadForm', $data);
	}
	
 	//$route['followup/los/upload/save'] = 'backend/follow_up/saveLosUpload';
	public function saveLosUpload() {
		$data = $this->input->post(NULL, TRUE);
		//print_r($data);
		$folder = $data['idfo']."_".$data['id']."/";	
		$folderName = "asset/images/los/".$folder;
        //rmdir($folderName);
		if ( !file_exists($folderName) ) {
		    mkdir($folderName);
			chmod($folderName, 0777);		
		}
		$success = 0;
		$error = 0;
		$jum = count($_FILES["files"]["name"]);
		for($i = 0; $i < $jum; $i++) {
			$fileName = $_FILES["files"]["name"][$i];
            //$imageTitle = $_POST["imageTitle"];
            $imageTitle = "";  
            $dirfile = $folderName.$fileName;
            if($this->follow_up->insertDataImage($dirfile, $fileName, $data['idfo'], $data['id']) > 0) {
            	if(move_uploaded_file($_FILES["files"]["tmp_name"][$i], $dirfile)) {
            		$success++;
					//$this->resizeImageSmaller($dirfile, $folderName."/tumb/", "tesss.jpg");
            	} else {
            		$error++;
            	}
			}		
		}
		
        $str = "Files uploaded : $success";
		echo json_encode(array("message" => $str));
	}

    //$route['followup/img/(:any)/(:any)'] = 'backend/follow_up/imgDet/$1/$2';
    public function imgDet($fid, $id) {
    	$data['result'] = $this->follow_up->getListPic($fid, $id);
		//print_r($data['result']);
		$this->load->view($this->folderView.'losImageViewList', $data);
    }
	
	//$route['followup/img-resize'] = 'backend/follow_up/resizeImg';
	public function resizeImg() {
		$this->load->libraries('img_resize');
		$resizeObj = new resize('asset/images/los/FU16110001_1/Jellyfish.jpg');

	    // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
	    $resizeObj -> resizeImage(200, 350, 'portrait');
	
	    // *** 3) Save image
	    $resizeObj -> saveImage('sample-resizeda.jpg', 1000);
	}
	
	private function resizeImageSmaller($vfile_upload, $thumb_dir, $img_name) {
        //identitas file asli
                $im_src = imagecreatefromjpeg($vfile_upload);
                $src_width = imageSX($im_src);
                $src_height = imageSY($im_src);
                
                //Simpan dalam versi small 320 pixel
                //set ukuran gambar perbandingan perubahan
                $dst_width = 700;
                $dst_height = 1000;
                
                
                //resize height
                $ratio = $dst_height / $src_height;
                $width = $src_width * $ratio;
                //$this->resize($width,$height);
                
                //resize width
                $ratio = $dst_width / $src_width;
                $height = $src_height * $ratio;
                //$this->resize($width,$height);
                 
                //scale
                $scale = 50;       
                $width = $src_width * $scale/100;
                $height = $src_height * $scale/100;
                //$this->resize($width,$height);
                
                //proses perubahan ukuran
                $im = imagecreatetruecolor($width,$height);
                imagecopyresampled($im, $im_src, 0, 0, 0, 0, $width, $height, $src_width, $src_height);
                
                
                //Simpan gambar
                imagejpeg($im, $thumb_dir .  $img_name);
                
                imagedestroy($im_src);
                imagedestroy($im);
    }	

	private function Img_Resize($path) {

           $x = getimagesize($path);            
           $width  = $x['0'];
           $height = $x['1'];

           $rs_width  = $width / 2;//resize to half of the original width.
           $rs_height = $height / 2;//resize to half of the original height.

           switch ($x['mime']) {
              case "image/gif":
                 $img = imagecreatefromgif($path);
                 break;
              case "image/jpeg":
                 $img = imagecreatefromjpeg($path);
                 break;
              case "image/png":
                 $img = imagecreatefrompng($path);
                 break;
           }

           $img_base = imagecreatetruecolor($rs_width, $rs_height);
           imagecopyresized($img_base, $img, 0, 0, 0, 0, $rs_width, $rs_height, $width, $height);

           $path_info = pathinfo($path);    
           switch ($path_info['extension']) {
              case "gif":
                 imagegif($img_base, $path);  
                 break;
              case "jpeg":
                 imagejpeg($img_base, $path);  
                 break;
              case "png":
                 imagepng($img_base, $path);  
                 break;
           }

        }

	public function temp_surat() {
		$data['form_header'] = "Template Surat";
		$data['icon'] = "icon-pencil";
		$data['form_reload'] = 'followup/temp_surat';
		$data['legend'] = false;
		$data['id'] = "";
		$data['followup_id'] = "";
		$data['idmember'] = "";
		$data['detailMember'] = null;
		$data['backButton'] = false;
		$data['update'] = 0;
		$data['dataFollow'] = $this->follow_up->listUnprocesedFollowUpByType("8");
		$data['errMsg'] = false;
		if($data['dataFollow'] != null) {
			$data['errMsg'] = true;
		}
		$this->setTemplate($this->folderView.'temp_surat_form', $data);
	}
}
	
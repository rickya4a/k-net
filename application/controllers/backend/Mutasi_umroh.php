<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi_umroh extends MY_Controller {
	public function __construct() {
	   parent::__construct();
       $this->folderView = "backend/umroh/";
       $this->load->model("backend/be_mutasi_model", 'mUmroh');
	}
	
	function form_mutasi(){
        if($this->username != null) {
            $x['form_header'] = 'Mutasi';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be_umroh/mutasi/umroh';
            $this->setTemplate($this->folderView.'getListMutasi', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
	function list_mutasi(){
		if($this->username != null) {	
			$x = $this->input->post(null,true);
			$datefrom = $x['reg_from'];
			
			$x['mutasi'] = $this->mUmroh->get_data($x['reg_from'],$x['reg_to'], $x['status'], $x['appstatus']);
			$this->load->view($this->folderView.'getListMutasi2', $x);
		}
		else
        {
           redirect('backend', 'refresh');
        }
    }
	
	function mutasi_details($id){
		if($this->username != null) {
			$x['dataum'] = $this->mUmroh->get_head($id);	
			$x['mutdetails'] = $this->mUmroh->get_details($id);
			$this->load->view($this->folderView.'mutasiDetails', $x);
		}
		else
        {
           redirect('backend', 'refresh');
        }
	}

    //tambahamn print Mutasi (CAHYONO)
    function reprintKWx(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['dataum'] = $this->mUmroh->get_head($dt['regnose']);
            $x['mutdetails'] = $this->mUmroh->get_details($dt['regnose']);
            $x['no'] = ($dt['nokw']);

            if($x['dataum'] > 0){
                $this->load->view($this->folderView.'printMutasiKW', $x);
            }else{
                echo "woop";
            }
        }else{
            echo "smack";
            //redirect('backend', 'refresh');
        }
    }
    //tambahamn print Mutasi Detail (CAHYONO)
    function printDetail(){
        if($this->username != null) {
            $dt = $this->input->post(NULL, TRUE);
            $x['dataum'] = $this->mUmroh->getKWDetail($dt['child']);

            if($x['dataum'] > 0){
                $this->load->view($this->folderView.'printMutasiKWDetail', $x);
            }else{
                echo "no data";
            }
        }else{
            echo "no data";
            //redirect('backend', 'refresh');
        }
    }
	//REMARKED BY DION 28/10/2015
	/*function form_listing(){
        if($this->username != null) {
            $x['form_header'] = 'Listing Reg Umroh';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be_umroh/listReg';
            $this->setTemplate($this->folderView.'listReg1', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
	
	function listing_reg(){
		if($this->username != null) {	
			$x = $this->input->post(null,true);
			$datefrom = $x['reg_from'];
			
			$x['mutasi'] = $this->mUmroh->get_data($x['reg_from'],$x['reg_to']);
			$this->load->view($this->folderView.'listReg2', $x);
		}
		else
        {
           redirect('backend', 'refresh');
        }
    } */
	
}
<?php
 class klikbca_by_semprot extends CI_Controller{
    var $ch = false;
    var $ip = '';
    var $last_html = '';
    var $logged_in = false;
    var $password = '';
    var $username = '';

    function klikbca_by_semprot() {
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->logged_in = false;
    }

    // Argumen pertama = berapa hari yg lalu?
    function cek_mutasi($minus = 30) {
        if ($this->logged_in == false) {
            //trigger_error('LOGIN FIRST', E_USER_WARNING);
            //return false;
        }
        $minus = intval($minus);
        if ($minus > 30) {
            trigger_error('MAX MINUS IS 30 DAYS', E_USER_WARNING);
            $minus = 30;
        }
        $now = strtotime('Today');
        list($day1, $month1, $year1) = explode(' ', date('d n Y', $now));
        $minus_t = $now - (24 * 3600 * $minus);
        list($day2, $month2, $year2) = explode(' ', date('d n Y', $minus_t));

        $data = array(
            //'value(actions)' => 'acctstmtview',
            'value(D1)' => '0',
            'value(startDt)' => $day2,
            'value(startMt)' => $month2,
            'value(startYr)' => $year2,
            'value(endDt)' => $day1,
            'value(endMt)' => $month1,
            'value(endYr)' => $year1,
            'value(submit1)' => 'Lihat Mutasi Rekening'
        );
        $data = http_build_query($data);
        //echo '<p>', urldecode($data), '</p>';
        $res = $this->my_curl_post(URL_MUTASI_VIEW, $data, URL_MUTASI_INDEX);
        $this->last_html = $res['response'];
        return true;
    }

    function cek_saldo() {
        if ($this->logged_in == false) {
            //trigger_error('LOGIN FIRST', E_USER_WARNING);
            //return false;
        }
        $res = $this->my_curl_get(URL_SALDO);
        $this->last_html = $res['response'];
        preg_match_all('/color=\"\#0000bb\"\>([ ]+)?([0-9\,]+)/i', $res['response'], $match);
        echo '<pre>';print_r($match);echo '</pre>';
        return true;
    }

    function login() {
        $this->logged_in = false;
        $data = array(
            'value(actions)' => 'login',
            'value(user_id)' => $this->username,
            'value(user_ip)' => $this->ip,
            'value(pswd)' => $this->password,
            'value(Submit)' => 'LOGIN'
        );
        $data = http_build_query($data);
        $res = $this->my_curl_post(URL_LOGIN, $data);
        $this->last_html = $res['response'];
        if (preg_match('/value\(user_id\)/i', $res['response'])) {
            trigger_error('CAN NOT LOGIN TO KLIKBCA', E_USER_WARNING);
            return false;
        }
        $this->logged_in = true;
        return true;
    }

    function my_curl_close() {
        if ($this->ch != false) {
            curl_close($this->ch);
        }
    }

    function my_curl_get($url, $ref = '') {
        if ($this->ch == false) {
            $this->my_curl_open();
        }
        $ssl = false;
        if (preg_match('/^https/i', $url)) {
            $ssl = true;
        }
        if ($ssl) {
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        }
        if ($ref == '') {
            $ref = $url;
        }
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_REFERER, $ref);
        $res = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        return array(
            'response' => trim($res),
            'info' => $info
        );
    }

    function my_curl_open() {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
        @curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 2);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, dirname(__FILE__).'/curl-cookie.txt');
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/curl-cookie.txt');
        curl_setopt($this->ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    }

    function my_curl_post($url, $post_data, $ref = '') {
        if ($this->ch == false) {
            $this->my_curl_open();
        }
        $ssl = false;
        if (preg_match('/^https/i', $url)) {
            $ssl = true;
        }
        if ($ssl) {
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        }
        if ($ref == '') {
            $ref = $url;
        }
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_REFERER, $ref);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
        $res = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        return array(
            'response' => trim($res),
            'info' => $info
        );
    }
}?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Be_SCO_recover extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->folderView = "backend/stockist/";
        $this->load->model("backend/stockist/be_stockist_model", 'fStockist');
    }


    public function index(){

        if($this->username != null) {
            $x['form_header'] = 'Recover Incoming Payment';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'recover_ip';
//            $x['location'] = $this->be_ticket->getListTicketLocation();
            $this->setTemplate($this->folderView.'FormRecover', $x);
        } else {
            redirect('backend', 'refresh');
        }
    }
    public function getSSR() {

        $category=$this->input->post('category');
        $search=$this->input->post('search');


        $data['result'] = $this->fStockist->getSSR($category,$search);
        $this->load->view($this->folderView.'getListRecoverResult', $data);
    }

    public function getDetail() {

        $param=$this->input->post('param');
//        $search=$this->input->post('search');


        $data['arraydata'] = $this->fStockist->getSSRDetail($param);
//        $this->load->view($this->folderView.'getListRecoverResult', $data);

        echo json_encode($data);
    }


    public function recover($ssr) {

        $param=$this->input->post('param');
//        $search=$this->input->post('search');


//        $this->fStockist->recover($ssr);
//        $this->load->view($this->folderView.'getListRecoverResult', $data);

        redirect('recover_ip', 'refresh');

//        echo json_encode(true);
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Be_backpack extends MY_Controller {
	public function __construct() {
	   parent::__construct();
       $this->folderView = "backend/stockist/backpack/";
       $this->load->service("backend/be_backpack_service", 's_backpack');
	}
	//$route['be/backpack']='backend/stockist/be_backpack/formInputTrfBackpack';
	public function formInputTrfBackpack() {
		if($this->username != null) {
			$data['form_header'] = "Input Transfer Backpack";
            $data['icon'] = "icon-pencil";
		    $data['form_reload'] = 'be/backpack';
			//$data['curdate'] = date("Y-m-d");
            $this->setTemplate($this->folderView.'formInputTrfBackpack', $data); 
		} else {
			echo $this->sessExpMessage;
		}	
	}
	
	
   //$route['be/backpack/member/(:any)']='backend/stockist/be_backpack/getMemberInfo/$1';
	public function getMemberInfo($idmember) {
		if($this->username != null) {
	    	$arr = $this->s_backpack->getMemberInfoSrv($idmember);
	    } else {
	    	$arr = jsonFalseResponse($this->sessExpMessage);			
		}	
		echo json_encode($arr);
	}
	
	//$route['be/backpack/stockist/(:any)']='backend/stockist/be_backpack/getStockistInfo/$1';
	public function getStockistInfo($idstk) {
		if($this->username != null) {
	    	$arr = $this->s_backpack->getStockistInfoSrv($idstk);
	    } else {
	    	$arr = jsonFalseResponse($this->sessExpMessage);			
		}	
		echo json_encode($arr);
	}
	
	//$route['be/backpack/save']='backend/stockist/be_backpack/postSaveTrfBackpack';
	public function postSaveTrfBackpack() {
		if($this->username != null) {
			$data = $this->input->post(NULL, TRUE);
	    	$arr = $this->s_backpack->saveTrfBackpack($data);
	    } else {
	    	$arr = jsonFalseResponse($this->sessExpMessage);			
		}	
		echo json_encode($arr);
	}
	
	//$route['be/backpack/report']='backend/stockist/be_backpack/formReportTrfBackpack';
	public function formReportTrfBackpack() {
		if($this->username != null) {
			$data['form_header'] = "Transfer Backpack Report";
            $data['icon'] = "icon-list";
		    $data['form_reload'] = 'be/backpack/report';
			//$data['curdate'] = date("Y-m-d");
            $this->setTemplate($this->folderView.'formReportTrfBackpack', $data); 
		} else {
			echo $this->sessExpMessage;
		}	
	}
    
	//$route['be/backpack/report/list']='backend/stockist/be_backpack/getReportTrfBackpack';
	public function getReportTrfBackpack() {
		if($this->username != null) {
			$data = $this->input->post(NULL, TRUE);
			$data['result'] = $this->s_backpack->getReportTrfBackpackSrv($data);
            //print_r($arr);
            if($data['search_type'] == "id_stockist") {
              $this->load->view($this->folderView.'getReportTrfBackpackByStockist', $data);
			} else {
			  
			  $this->load->view($this->folderView.'getReportTrfBackpackByMemb', $data);	
			}	 
		} else {
			echo $this->sessExpMessage;
		}	
	}
	
	//$route['be/backpack/update/status']='backend/stockist/be_backpack/updateStatusPakai';
	public function updateStatusPakai() {
		if($this->username != null) {
			$data = $this->input->post(NULL, TRUE);
	    	$arr = $this->s_backpack->updateStatusPakai($data);
	    } else {
	    	$arr = jsonFalseResponse($this->sessExpMessage);			
		}	
		echo json_encode($arr);
	}
	
	//$route['be/backpack/detail/(:any)']='backend/stockist/be_backpack/getDetailTrf/$1';
	public function getDetailTrf($id) {
		if($this->username != null) {
			$data['result'] = $this->s_backpack->getDetailTrf($id);
            $this->load->view($this->folderView.'getDetailTrf', $data);		 
		} else {
			echo $this->sessExpMessage;
		}	
	}

}
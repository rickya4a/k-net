<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Be_stockist extends MY_Controller {
	public function __construct() {
	   parent::__construct();
       $this->folderView = "backend/stockist/";
       
	}
    
   
    
    function form_addpromo100(){
        if($this->username != null) {
            $x['form_header'] = 'Open Promo Free Application';
            $x['icon'] = 'icon-ok';
            $x['form_reload'] = 'be_stockist/addpromo100';
            $x['listLoccd'] = $this->fStockist->getListStockist();
            $this->setTemplate($this->folderView.'getListStockistPromo', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function getListStockistPromo(){
        if($this->username != null) {
            $dt = $this->input->post(null,true);
            $x['reg_from'] = $dt['reg_from'];
            $x['reg_to'] = $dt['reg_to'];
			$x['loccd'] = $dt['loccd'];
            $x['form_reload'] = 'be_stockist/listPromo';
           // print_r($x['listScPromo']);
			$this->load->model("backend/stockist/be_stockist_model", 'fStockist');
           //echo "Tanggal to : ".date('Y-m-d', strtotime($x['reg_to']))."</br>";
		   //echo "Tanggal From : ".date('Y-m-d', strtotime($x['reg_from']))."</br>";
			if(date('Y-m-d', strtotime($x['reg_to'])) < date('Y-m-d', strtotime($x['reg_from']))){
				echo "Error date range!";
			}else{
				
            	$x['listScPromo'] = $this->fStockist->getListStockistPromo($x['reg_from'], $x['reg_to'],$x['loccd']);
	            if($x['listScPromo'] != null){
	                $this->load->view($this->folderView.'listingStockistPromo', $x);
	            }else{
	                echo "No Data";
	            }
			}
			 
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function saveStockistPromo() {
		$dt = $this->input->post(null,true);
        $x['reg_from'] = $dt['reg_from'];
        $x['reg_to'] = $dt['reg_to'];
		$x['loccd'] = $dt['loccd'];
		
		$srvReturn = null;
		if(date('Y-m-d', strtotime($x['reg_to'])) < date('Y-m-d', strtotime($x['reg_from']))){
			echo "Error date range!";
		}else{
	    	try {
	    		$srvReturn = $this->fStockist->saveStockistPromo($x['reg_from'], $x['reg_to'], $x['loccd']);
				//$this->getListStockistPromo();
				if($srvReturn == 1){
					$srvReturn = "Success insert new data";
				}
	    	} catch(Exception $e) {
	    		$srvReturn = "Error Insert New Data";
	    	}	
		}
		echo $srvReturn;
		//echo json_encode($srvReturn);
	}
	
	
	//$route['be_stockist/deletePromo'] = 'backend/stockist/be_stockist/deleteStockistPromo';
	function deleteStockistPromo($loccd, $start_date, $expired_date) {
		$srvReturn = null;
    	try {
    		$srvReturn = $this->fStockist->deleteStockistPromo($loccd, $start_date, $expired_date);
			//$this->getListStockistPromo();
    	} catch(Exception $e) {
    		$srvReturn = "No Data";
    	}
		//echo json_encode($srvReturn);
	}
    
    //$route['be/stockist/maintenance'] = 'backend/stockist/be_stockist/getDtStockist';
    function getDtStockist(){
        if($this->username != null) {
            $x['form_header'] = 'Update Stockist Ecommerce';
            $x['icon'] = 'icon-pencil';
            $x['form_reload'] = 'be/stockist/maintenance';
            $x['listStk'] = $this->fStockist->getListStkEcomm();
            $x['listProv'] = $this->fStockist->getListProvince();
            //$x['listKab'] = $this->fStockist->getListKabupaten();
            //$x['listKec'] = $this->fStockist->getListKecamatan();
            $this->setTemplate($this->folderView.'getListStkEcommerce', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
    }
    
    function getListKab($kab){
        if($this->username != null) {
            $res = $this->fStockist->getListKabupaten($kab);
            
            if($res != null){
                echo "<option value=\"kabb\">--Select Here--</option>";
                foreach($res as $listKabs){
                echo "<option value=\"$listKabs->Kota_Kabupaten\">".$listKabs->Kota_Kabupaten."</option>";
                }
            }else{
                echo "<option value=\"\">Data Tidak Di temukan</option>";
            }
            
	        //echo json_encode($res);      
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function getListKecamatan(){
        if($this->username != null) {
            $dt = $this->input->post(null,true);
            //if($dt['idstk']=="" && $dt['newstkk'] == ""){
              if($dt['idstk']==""){
                setErrorMessage("Silahkan Pilih salah satu Stockist..");
            }elseif($dt['prov']==""){
                setErrorMessage("Silahkan Pilih salah satu Provinsi..");
            }else{
                $x['stks'] = $dt['idstk'];
                //$x['newstkk'] = $dt['newstkk'];
                //$x['listStk'] = $this->fStockist->getListStkEcomm();
                //$x['kecByStk'] = $this->fStockist->kecByStks($dt);
                $x['listKec'] = $this->fStockist->getListKecamatan($dt);
                $this->load->view($this->folderView.'listKecEcomm', $x);
            }
        }else{
            redirect('backend', 'refresh');
        }
    }
    
    function updateStkEcommm(){
        if($this->username != null) {
            $dt = $this->input->post(null,true);
            //print_r($dt);
            if(array_key_exists('stkMaintenance', $dt)) {
                $cekStk = $this->fStockist->cekStkExist($dt);
                if(empty($cekStk)){
                    $res = $this->fStockist->updtNewStkEcomm($dt);
                }else{
                    $res = $this->fStockist->updtStkEcomm($dt);
                }
            } else {
              $res = array('response'=>'false','message'=>'Silahkan pilih salah satu area');
            }
            echo json_encode($res);
        }
        else{
            redirect('backend', 'refresh');
        }
    }
    
    function unsetStockist(){
        if($this->username != null) {
		    $this->load->model("backend/stockist/be_stockist_model", 'fStockist');
            $dt = $this->input->post(null,true);
            if($dt['idstk'] == "" || $dt['idstk'] == " "){
                $unset = array('response'=>'false','message'=>'Silahkan pilih Salah Satu Stockist');
            }else{
                $unset = $this->fStockist->unsetAllByIdstk($dt);
            }
            echo json_encode($unset);
        }else{
            redirect('backend', 'refresh');
        }
    }
	
	//$route['be_stockist/ship/reff'] = 'backend/stockist/be_stockist/updateStkReff';
	public function updateStkReff() {
		if($this->username != null) {
		    $this->load->service("webshop/Cartshop_service",'cartshopService');
            $x['form_header'] = 'Update Stockist Ship Refference';
            $x['icon'] = 'icon-pencil';
            $x['form_reload'] = 'be_stockist/ship/reff';
            //$x['listLoccd'] = $this->fStockist->getListStockist();
			$x['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
			//$x['show_provinsi'] = $this->cartshopService->showListProvinsi();
			$this->load->model('webshop/shared_module', 'shared');
			$x['listProv'] = $this->shared->getListProvinsi2();
            $this->setTemplate($this->folderView.'updateStkReff', $x);
        }
        else
        {
           redirect('backend', 'refresh');
        }
	}
	//$route['be_stockist/ship/reff/update'] = 'backend/stockist/be_stockist/saveUpdateStkReff';
	public function saveUpdateStkReff() {
	
	}
}
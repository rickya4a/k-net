<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Trxintegrity extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->folderView = "backend/integrity/";
		$this->load->model('backend/be_transaction_model', 'm_trans');
	}

	// $route['be/trans/integrity'] = 'backend/trxintegrity/form';
	public function form() {
		if (!empty($this->username)) {
			$data['form_header'] = 'Transaction Integrity Check';
			$data['icon'] = 'icon-pencil';
			$data['form_reload'] = 'be/trans/integrity';
			$this->setTemplate($this->folderView.'formTrxIntegrityCheck', $data);
		} else {
			redirect('backend', 'refresh');
		}
	}
	//$route['be/trans/integrity/check'] = 'backend/trxintegrity/getList';
	public function getList() {
		if(!empty($this->username)) {
			$data = $this->input->post(NULL, TRUE);
			$data['bnsperiod'] = $data['bnsyear']. '-' .$data['bnsmonth']. '-' ."01";
			$yearFormat = $data['bnsmonth'].'/'.$data['bnsyear'];
			$data['result'] = $this->m_trans->checkHeaderDetail($data['tipe'], $data['bnsperiod']);
			if ($data['tipe'] == "1" || $data['tipe'] == "2") {
				$this->load->view($this->folderView.'listTrxIntegrityCheck', $data);
			} elseif ($data['tipe'] == "5") {
				$data['result'] = $this->m_trans->failTrhTrx();
				$this->load->view($this->folderView.'listTrxFailedTrh', $data);
			} elseif ($data['tipe'] == "6") {
				$data['result'] = $this->m_trans->checkHeaderDetail($data['tipe'], $yearFormat);
				$this->load->view($this->folderView.'listDiffTrx', $data);
			} elseif ($data['tipe'] == "7") {
				$data['result'] = $this->m_trans->checkHeaderDetail($data['tipe'], $yearFormat);
				$this->load->view($this->folderView.'listMemberRecord', $data);
			} elseif ($data['tipe'] == "8") {
				$this->load->view($this->folderView.'listEmptyQty', $data);
			} elseif ($data['tipe'] == "9") {
				$this->load->view($this->folderView.'listDpBv', $data);
			} else {
				$this->load->view($this->folderView.'stockistListIntegrityCheck', $data);
			}
		} else {
			echo setErrorMessage("Session expire, silahkan login kembali");
		}
	}

	// $route['be/trans/integrity/reconcile/(:any)'] = 'backend/trxintegrity/getDetail/$1';
	public function getDetail($orderno) {
		$data['result'] = $this->m_trans->getDetail($orderno);
		$this->load->view($this->folderView.'detailListTrh', $data);
	}

	// $route['be/trans/integrity/reconcileTrx/newtrh'] = 'backend/trxintegrity/reconcileNewtrh';
	public function reconcileNewtrh() {
		$data = $this->input->post(NULL, TRUE);
		$check = $this->m_trans->checkingNewtrh($data['orderno']);
		$res = jsonFalseResponse("Orderno sudah ada / sudah di recover");
		if(empty($check)) {
			$return = $this->m_trans->reconcileNewtrh($data['orderno']);
			if($return > 0) {
				$res = jsonTrueResponse(NULL, "Data sudah di recon ke tabel Newtrh");
			}
		}
		echo json_encode($res);
	}

	// $route['be/trans/integrity/reconcileTrx/billivhdr'] = 'backend/trxintegrity/reconcileBillivhdr';
	public function reconcileBillivhdr() {
		$data = $this->input->post(NULL, TRUE);
		$check = $this->m_trans->checkingBillivhdr($data['kwno']);
		$res = jsonFalseResponse("Nomor order sudah ada / sudah di-recover");
		if(empty($check)) {
			$return = $this->m_trans->reconcileBillivhdr($data['orderno']);
			if($return > 0) {
				$res = jsonTrueResponse(NULL, "Data sudah di-reconcile ke table Billivhdr");
			}
		}
		echo json_encode($res);
	}

	// $route['be/trans/integrity/reconcileTrx/bbhdr'] = 'backend/trxintegrity/reconcileBbhdr';
	public function reconcileBbhdr() {
		$data = $this->input->post(NULL, TRUE);
		$check = $this->m_trans->checkingBbhdr($data['ipno']);
		$res = jsonFalseResponse("Nomor order sudah ada / sudah di-recover");
		if(empty($check)) {
			$return = $this->m_trans->reconcileBbhdr($data['ipno'], $data['orderno']);
			if ($return > 0) {
				$res = jsonTrueResponse(NULL, "Data sudah di-reconcile ke table Bbhdr");
			}
		}
		echo json_encode($res);
	}

	// $route['be/trans/integrity/reconcileTrx/newivtrh'] = 'backend/trxintegrity/reconcileNewivtrh';
	public function reconcileNewivtrh() {
		$data = $this->input->post(NULL, TRUE);
		$check =$this->m_trans->checkingNewivtrh($data['cnno']);
		$res = jsonFalseResponse("Nomor order sudah ada / sudah di-recover");
		if(empty($check)) {
			$return = $this->m_trans->reconcileNewivtrh($data['orderno']);
			if ($return > 0) {
				$res = jsonTrueResponse(NULL, "Data sudah di-reconcile ke table Newivtrh");
			}
		}
		echo json_encode($res);
	}

	// $route['be/trans/integrity/reconcileTrx/bonusmonth'] = 'backend/trxintegrity/reconcileBonusMonth';
	public function reconcileBonusMonth() {
		$data = $this->input->post(NULL, TRUE);
		$bns = explode("/", $data['bonusmonth']);
		$bnsperiod = $bns[1]."-".$bns[0]."-"."01";
		// print_r($data);
		$res = $this->m_trans->reconcileBonusMonth($bnsperiod, $data['registerno'], $data['cnno'], $data['orderno'], $data['bonusmonth']);
		echo json_encode($res);
	}

	//$route['be/trans/integrity/reconcileTrx/idmember'] = 'backend/trxintegrity/reconcileIdMember';
	public function reconcileIdMember() {
		$data = $this->input->post(NULL, TRUE);
		$res = $this->m_trans->reconcileIdMember($data['idmember'], $data['nmmember'], $data['orderno']);
		echo json_encode($res);
	}
//
	// $route['be/trans/integrity/reconcileTrx/detailmember/(:any)'] = 'backend/trxintegrity/memberRecordDetail/$1'
	public function memberRecordDetail($orderno) {
		$data['result'] = $this->m_trans->memberRecordDetail($orderno);
		$this->load->view($this->folderView. 'memberDetailRecord', $data);
	}
}
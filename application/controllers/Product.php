<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product extends MY_Controller {
    	
    public function __construct() {
	    parent::__construct();
        $this->load->driver('cache', array('adapter' => 'redis', 'backup' => 'file'));
		$this->load->service("webshop/Product_service",'productService');
        $this->load->service("webshop/Redis_service",'redisService');
        $this->load->service("webshop/Login_service",'loginService');
		$this->folderPrd = base_url()."assets/images/products/thumb/";
		$this->folderView = "webshop/sales_non_member/";
	}
    
	function redirectGetProduct() {
		// this controller executes only set template from webshop source. 
		// $this->setTempWebShop('webshop/shop_home_empty');
		$dt['prodCat'] = $this->productService->getProductCategoryCache();
		$this->session->unset_userdata('sales_lp');
		$username = getUserID();
		//echo "ok";
		if(!isset($username) || $username == "") {
			$this->session->sess_destroy();
			$this->setTempWebShop22('webshop/shop_home_empty', $dt);
		} else {
			$this->setTempWebShop('webshop/shop_home_empty', $dt);
		}
	}
	
	function redirectGetProductTest() {
		// this controller executes only set template from webshop source. 
		// $this->setTempWebShop('webshop/shop_home_empty');
		$dt['prodCat'] = $this->productService->getProductCategoryCache();
		$this->session->unset_userdata('sales_lp');
		$username = getUserID();
		//echo "ok";
		if(!isset($username) || $username == "") {
			$this->session->sess_destroy();
			$this->setTempWebShop22('webshop/shop_home_empty_test', $dt);
		} else {
			$this->setTempWebShop('webshop/shop_home_empty_test', $dt);
		}
	}
	
	//$route['shop/product'] = 'webshop/product/getProduct';
    function getProduct() {
    	//echo "masuk sini";session_destroy();
        $username = getUserID();
        if($username == "IDSPAAA91433"){

            $this->loginService->insertLogoutData();
            $this->session->sess_destroy();
            //redirect('loginmember');
            redirect('');

        }else{

            $dt['prdnm'] = $this->input->post("name");
            $dt['formAction'] = "".site_url('cart/addtocart')."";
         

        if($redis1 = $this->redisService->getRedisProduct()){  // Masuk Redis
            $dt['prodCache'] = $redis1;
            $dt['bannerCache'] = $this->redisService->getRedisMainBanner();
            $dt['prodCat'] = $this->redisService->getRedisProductCat();
            $dt['prodjhon'] = $this->redisService->getRedisProductJohn();
            $dt['prodv'] = $this->redisService->getRedisProductVera();
         }else {   // Keluar redis ambil file Cache kalau tidak ada baru ke DB
                $dt['prodCat'] = $this->productService->getProductCategoryCache();
                $dt['bannerCache'] = $this->productService->getHeaderBannerCache();
                $dt['prodCache'] = $this->productService->getProductShopCache();


                $dt['prodv'] = $this -> productService -> getProductShopVera();
                $dt['prodjhon'] = $this -> productService -> getProductShopVera1();
         }
                $dt['prod1'] = $this -> productService -> getProductShopVeraD1();
                $dt['prod2'] = $this -> productService -> getProductShopVeraD2();
                $dt['prod3'] = $this -> productService -> getProductShopVeraD3();


                $dt['p1'] = $this -> productService -> getProductShopVeraP1();
                $dt['p2'] = $this -> productService -> getProductShopVeraP2();
                $dt['p3'] = $this -> productService -> getProductShopVeraP3();
                $dt['p4'] = $this -> productService -> getProductShopVeraP4();
                $dt['p5'] = $this -> productService -> getProductShopVeraP5();
                $dt['p6'] = $this -> productService -> getProductShopVeraP6();
                $dt['p7'] = $this -> productService -> getProductShopVeraP7();

    			
                $dt['T1'] = $this -> productService -> getProductShopVeraT1();
                $dt['T2'] = $this -> productService -> getProductShopVeraT2();

        

            $dt['folderPrd'] = $this->folderPrd;
			//echo $this->folderPrd;
            $this->session->unset_userdata('sales_lp');
            $username = getUserID();
            //echo "ok";
            if(!isset($username) || $username == "") {
                //$this->setTempWebShop('webshop/shop_home_guest', $dt);
                //$this->setTempWebShop($this->folderView.'shop_home_guest1', $dt);
                $this->session->sess_destroy();
                $this->setTempWebShop22($this->folderView.'shop_home_guest1_iman', $dt);
                //$this->setTempWebShop22($this->folderView.'shop_home_guest1', $dt);
            } else {
                //$this->setTempWebShop('webshop/shop_home', $dt);
                if($username == "IDSPAAA66834") {
                    //$this->setTempWebShop22('webshop/shop_home_iman', $dt);
                    $this->setTemplateDevSGO('webshop/shop_home_iman_jhondoe1', $dt);

                } else {
                    //$this->setTempWebShop22('webshop/shop_home_iman', $dt);
                    $this->setTempWebShop('webshop/shop_home_iman', $dt);
                }

            }
        }
    }
	
	//$route['shop/productdev'] = 'product/getProductDev';
    function getProductDev() {
			$dt['prdnm'] = $this->input->post("name");
            $dt['formAction'] = "".site_url('cart/addtocart')."";
            $dt['banner'] = $this->productService->getHeaderBanner();
			$dt['prod'] = $this->productService->getProductShop();
            $dt['prodCat'] = $this->productService->getProductCategory();
			$this->session->unset_userdata('sales_lp');
			$dt['folderPrd'] = $this->folderPrd;
			$username = getUserID();
			/*if(!isset($username) || $username == "") {
				//$this->setTempWebShop('webshop/shop_home_guest', $dt);
				$this->setTempWebShop($this->folderView.'shop_home_guest1', $dt);
			} else {*/
            	//$this->setTempWebShop22('webshop/shop_home_iman', $dt);
				$this->setTempWebShop22($this->folderView.'shop_home_guest1_iman', $dt);
            //}
        
    }
	
	//$route['shop/product2'] = 'product/getProduct2';
	function getProduct2() {
			$dt['banner'] = $this->productService->getHeaderBanner();
			$dt['prdnm'] = $this->input->post("name");
            $dt['formAction'] = "".site_url('cart/addtocart')."";
            $dt['prod'] = $this->productService->getProductShop();
            $dt['prodCat'] = $this->productService->getProductCategory();
			$dt['folderPrd'] = $this->folderPrd;
            $this->setTempWebShop('webshop/shop_home', $dt);
    }
    
	//$route['shop/product/cat'] = 'webshop/product/getProductCategory';
    function getProductCategory() {
        //if($this->_checkSessionStoreUser())
            $dt['prodCat'] = $this->productService->getProductCategory();
			$dt['folderPrd'] = $this->folderPrd;
            $this->setTempWebShop('webshop/product_category',$dt);
        
    }
    
	//$route['shop/product/cat/id/(:any)'] = 'webshop/product/getProdByCat/$1';
    /*function getProdByCat($id) {
         //if($this->_checkSessionStoreUser())
        
         
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $data['totData']= $this->productService->getCountProduct();
        
        $config['base_url'] = site_url("shop/product/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';

        $this->ajax_pagination->initialize($config);

        $dt['halaman'] = $this->ajax_pagination->create_links();
        
        $data['per_page'] = $config['per_page'];
        
        
        //echo "jumlah ".$data['totData']['jml']."<br>";
        $dt['prodCat'] = $this->productService->getProdByCat($id,$limit,$this->uri->segment(3));   
            
         $this->load->view('webshop/prodByCat',$dt);
    } */
    
    //$route['shop/product/name'] = 'product/getProdByName';
    function getProdByName() {
    	 /*$dt['prdnm'] = $this->input->post("name");
		 //print_r($dt['prdnm']);  
		 $dt['formAction'] = "".site_url('cart/addtocart')."";
         $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
		 //print_r($dt['prod']);  
		 $dt['prodCat'] = $this->productService->getProductCategory();   
         $this->setTempWebShop('webshop/shop_home', $dt);
		 */
           // $dt['banner'] = $this->productService->getHeaderBannerCache();

            $namaprod = $this->input->post("name");
            $nama = preg_replace('#[^a-zA-Z- ]#', '', $namaprod);
		    $dt['prdnm'] = $nama;

            $dt['formAction'] = "".site_url('cart/addtocart')."";

            if ($redis1 = $this->redisService->getRedisProductByName($dt['prdnm'])){
                $dt['prod'] = $redis1;
                $dt['prodCat'] = $this->redisService->getRedisProductCat();
            }else {
            $dt['prod'] = $this->productService->getProdByNameCache($dt['prdnm']);
            $dt['prodCat'] = $this->productService->getProductCategoryCache();
        }
           // $dt['prodCache'] = $this->productService->getProdByNameCache($dt['prdnm']);
            
			$dt['folderPrd'] = $this->folderPrd;
			$username = getUserID();
			if(!isset($username) || $username == "") {
				//$this->setTempWebShop('webshop/shop_home_guest', $dt);
                //echo "test";
				$this->setTempWebShop($this->folderView.'shop_home_guest1', $dt);
			} else {
            	$this->setTempWebShop('webshop/shop_home', $dt);
            }
	}
    function getProdByName2($ID) {
        /*$dt['prdnm'] = $this->input->post("name");
        //print_r($dt['prdnm']);
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        //print_r($dt['prod']);
        $dt['prodCat'] = $this->productService->getProductCategory();
        $this->setTempWebShop('webshop/shop_home', $dt);
        */
        $dt['banner'] = $this->productService->getHeaderBanner();
        $dt['prdnm'] = $ID;
        $dt['formAction'] = "".site_url('cart/addtocart')."";
        $dt['prod'] = $this->productService->getProdByName($dt['prdnm']);
        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['folderPrd'] = $this->folderPrd;
        $username = getUserID();
        if(!isset($username) || $username == "") {
            //$this->setTempWebShop('webshop/shop_home_guest', $dt);
            $this->setTempWebShop($this->folderView.'shop_home_guest1', $dt);
        } else {
            $this->setTempWebShop('webshop/shop_home', $dt);
        }
    }


    function getProdByCat($id) {
         //if($this->_checkSessionStoreUser())
         
        $limit = 9;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $data['totData']= $this->productService->getCountProductByID($id);
        
        $config['base_url'] = site_url("shop/product/cat/id/$id/");
		//$config['base_url'] = "tes";
        //$config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
		$config['total_rows'] =  $data['totData'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';*/
		$config['div'] = 'listprd';
		$config['param_prd'] = $id;

        $this->jquery_pagination->initialize($config);

        $dt['halaman'] = $this->jquery_pagination->create_links();
        
        $data['per_page'] = $config['per_page'];
        
        
        //echo "jumlah ".$data['totData']['jml']."<br>";
        //echo $limit;
        $dt['prodCat'] = $this->productService->getProdByCat($id,$limit,$this->uri->segment(3));   
        $dt['folderPrd'] = $this->folderPrd;
		$username = getUserID();
		if(!isset($username) || $username == "") {
			$this->load->view('webshop/prodByCat_guest', $dt);
		} else {
        	$this->load->view('webshop/prodByCat', $dt);
        }    
        //$this->load->view('webshop/prodByCat',$dt);
    }

	function getProdByCat2($id, $offset) {
         //if($this->_checkSessionStoreUser())
         
        $limit = 9;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $data['totData']= $this->productService->getCountProductByID($id);
        
        $config['base_url'] = site_url("shop/product/cat/id/$id/");
		//$config['base_url'] = "tes";
        //$config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
		$config['total_rows'] =  $data['totData'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/
		$config['div'] = 'listprd';
		$config['param_prd'] = $id;

        $this->jquery_pagination->initialize($config);

        $dt['halaman'] = $this->jquery_pagination->create_links();
        
        $data['per_page'] = $config['per_page'];
        $dt['folderPrd'] = $this->folderPrd;
        
        //echo "jumlah ".$data['totData']['jml']."<br>";
        $dt['prodCat'] = $this->productService->getProdByCat($id,$limit, $offset);   
            
        $username = getUserID();
		if(!isset($username) || $username == "") {
			$this->load->view('webshop/prodByCat_guest', $dt);
		} else {
        	$this->load->view('webshop/prodByCat', $dt);
        }   
    }
    
   function getProdByCatHeader($cat){
        $id = preg_replace('#[^0-9]#', '', $cat);
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        //$data['totData']= $this->productService->getCountProduct();
        $data['totData']= $this->productService->getCountProductByID($id);
        /*$config['base_url'] = site_url("shop/productHeader/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/

        /*$this->pagination->initialize($config);

        $dt['halaman'] = $this->pagination->create_links();
        
        $data['per_page'] = $config['per_page'];*/
     $result = $this->redisService->getRedisProdByCatHeader($id);
            if($result['response'] != 'false'){
           
            $dt['listProd'] =  $result;
      }else{          
            $dt['listProd'] = $this->productService->getProdByCatHeaderCache($id);
           // $dt['listProd'] =  $this->redisService->getRedisProdByCatHeader($id);
        
       }
        $dt['prodCat'] = $this->productService->getProductCategory();
        
		$dt['bannerCat'] = $this->productService->getHeaderBannerCat();
        $dt['folderPrd'] = $this->folderPrd;
            //$this->setTempWebShop('webshop/product_category1',$dt);
       
	   $username = getUserID();
		if(!isset($username) || $username == "") {
			$this->setTempWebShop('webshop/product_category1_guest', $dt);
		} else {
        	$this->setTempWebShop('webshop/product_category1', $dt);
        }  
    }
    
	//$route['shop/prodDet'] = 'webshop/product/getProdDetails';
    function getProdDetails($prdcd){
        $dt['prodDetail'] = $this->productService->getProductDetails($prdcd);
        //if($this->_checkSessionStoreUser())
        //$this->setTempWebShop('webshop/product_detail');
		$this->load->view('webshop/product_detail');
    }
	
	//$route['promo/listing'] = 'product/listPromo';
	public function listPromo() {
		$dt['month'] = date("m");
		$dt['year'] = date("Y");
		$dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();	
		$dt['listPromo'] = $this->productService->getListCurrentPromo($dt['year'], $dt['month']);
		$this->setTempWebShop('webshop/promoListing', $dt);
	}

    //cahyono
    function getProdByCahyono($id){
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        //$data['totData']= $this->productService->getCountProduct();
        $data['totData']= $this->productService->getCountProductByID($id);
        /*$config['base_url'] = site_url("shop/productHeader/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/

        /*$this->pagination->initialize($config);

        $dt['halaman'] = $this->pagination->create_links();

        $data['per_page'] = $config['per_page'];*/

        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['listProd'] = $this->productService->getProdByCahyono($id);
        $dt['bannerCat'] = $this->productService->getHeaderBannerCat();
        $dt['folderPrd'] = $this->folderPrd;
        //$this->setTempWebShop('webshop/product_category1',$dt);

        $username = getUserID();
        if(!isset($username) || $username == "") {
            $this->setTempWebShop('webshop/product_category1_guest', $dt);
        } else {
            $this->setTempWebShop('webshop/product_category1', $dt);
        }
    }
    function getProdByCahyono1(){
        $limit = 2;
        $data['offset'] = $this->uri->segment(3);
        $data['limit'] = $limit;
        $id= $this->input->post("nm");
        //$data['totData']= $this->productService->getCountProduct();
        $data['totData']= $this->productService->getCountProductByID($id);
        /*$config['base_url'] = site_url("shop/productHeader/cat/id/$id/");
        $config['total_rows'] =  $data['totData']['jml'];//$this->pagingM->getAllDt();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        /*$config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';*/

        /*$this->pagination->initialize($config);

        $dt['halaman'] = $this->pagination->create_links();

        $data['per_page'] = $config['per_page'];*/

        $dt['prodCat'] = $this->productService->getProductCategory();
        $dt['listProd'] = $this->productService->getProdByCahyono($id);
        $dt['bannerCat'] = $this->productService->getHeaderBannerCat();
        $dt['folderPrd'] = $this->folderPrd;
        //$this->setTempWebShop('webshop/product_category1',$dt);

        $username = getUserID();
        if(empty($dt['listProd']['arrayData'])){
            redirect('http://www.k-net.co.id/');
        }
        else{
            if(!isset($username) || $username == "") {
                $this->setTempWebShop('webshop/product_category1_guest', $dt);
            } else {
                $this->setTempWebShop('webshop/product_category1', $dt);
            }
        }
    }

}
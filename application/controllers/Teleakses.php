<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teleakses extends MY_Controller {

	public function __construct() {
		 parent::__construct();
		$this->usr_api = "k-net.apps";
		$this->pwd_api = "apps@2017";
		$this->salt = "knet";
		$this->dataForm = $this->input->post(NULL, TRUE);	
	}
	
	function checkAuthAPI($data) {
		$return = false;
		if(array_key_exists("api_usr", $data) && array_key_exists("api_pwd", $data)) {
			if($data['api_usr'] == $this->usr_api && $data['api_pwd'] == $this->pwd_api) {
				$return = true;
			} 
		} 
		return $return;
	}
	
	//$route['tele/api/bonus'] = 'teleakses/getBonusReportMember'; 
	 function getBonusReportMember() {
	 	$data = $this->dataForm;
		  	//$auth = $this->checkAuthAPI2($this->dataForm);	
		  	$auth = $this->checkAuthAPI($this->dataForm);
			//echo json_encode($auth);
			if($auth['response'] == "false") {
				echo json_encode($auth);
			} else {
				$idm = $auth['arrayData'];
			   //rint_r($idm[0]);
				//$res = jsonFalseResponse("Parameter idmember dan data token tidak sesuai..");
				//if($idm[0] == $data['idmember']) {
			  		$this->load->model("webshop/Sales_model",'salesM');
					//$res = jsonFalseResponse("Update info member gagal..");
					$idmember = $data['idmember'];
		            $month = $data['month_period'];
		            $year = $data['year_period'];
		            $dt['month'] = $month;
		            $dt['year'] = $year;

				    $dt['res1'] = $this->salesM->showDataMember($idmember,$month,$year);
					$dt['tableBaru'] = $this->salesM->showTableHilal($idmember,$month,$year);

					if($dt['res1'] == 0)
		    	   	{
		    		   echo "Data Belum Ada";
		    		}
		            else
		            {
		        		  if($this->salesM->showBonusMember($idmember,$month,$year) == 0)
		        		  {
		        		  	echo "Data statement bonus ".$idmember." tidak ada";
		        		  }
		                  else
		                  {
		       			      //$dt['res2'] = $this->salesM->m_showBonusSummary();
		        			  // ---tambahan dr ana---
		        			  $dt['currency1'] = $this->salesM->getCurrency();
		                      $dt['currency'] = $dt['currency1'][0]->exchangerate;
		                      //print_r($dt['currency']);
		        			  $dt['hasil1'] = $this->salesM->cekmonthly($idmember,$month,$year);
		        			  $dt['hasil2'] = $this->salesM->downline($idmember,$month,$year);
		        			  $dt['hasil3'] = $this->salesM->selfBonus($idmember,$month,$year);
		        			  $cc = $this->salesM->showBonusMember($idmember,$month,$year);
		        			  if(isset($cc))
		        			  {
		        				  foreach($cc as $dd)
		        				  {
		        				  	  if($dd->netincome > 0)
		        				  	  {
		        				  	  	$dt['res2'] = $this->salesM->showBonusMember($idmember,$month,$year);
		                                $dt['initiateSUm'] = $this->salesM->showSumInitiative($idmember,$month,$year);
		        				  	  }
		        				  	  if($dd->gpincome > 0)
		        					  {
		        						$dt['devresult'] = $this->salesM->developmentBonus($idmember,$month,$year);
		        					  }
		        					  if($dd->ldbincome > 0)
		        					  {
		        						$dt['ldbresult'] = $this->salesM->leadershipBonus($idmember,$month,$year);
		        					  }

		                              $unilevel = $dd->planbincome + $dd->addinfinityincome;

		                                if($dt['year'] < 2017){
		                                    if($dd->infinityincome > 0)
		            					  	{
		            							$dt['hasil4'] = $this->salesM->infinityBonus($idmember,$month,$year);
		            					  	}
		                                }else{
		                                    $dt['hasil4'] = $this->salesM->initiativeBonus($idmember,$month,$year);
		                                }

		        					  	if($unilevel  > 0)
		        					  	{
		        							$dt['hasil5'] = $this->salesM->unilevelBonus($idmember,$month,$year);
		        							$dt['hasil6'] = $this->salesM->levelMember($idmember,$month,$year);
		        					  	}
		        				  }
		        		      }
		        			  if($this->salesM->showBonusVoucher($idmember,$month,$year) != 0)
		        			  {
		        			  	$dt['voucher'] = $this->salesM->showBonusVoucher($idmember,$month,$year);
		        			  }
		    		          $this->load->view('webshop/printBonusReportPdf',$dt);
		        		  }
		    		}
				} 
			    //echo json_encode($res);
			//} 
	 }	

     //$route['tele/api/listbonus'] = 'mobile_api/ListBonus';
	 function ListBonus() {
		$data = $this->dataForm;
    
        $auth = $this->checkAuthAPI3($this->dataForm);
		if($auth['response'] == "false") {
			echo json_encode($auth);
		} else {
			$res = jsonFalseResponse("File not Found!");
			//$this->load->model('webshop/product_model', 'product');
			 {
				$this->load->model('M_mobile_api', 'm_mobile_api');
				$idmember = $auth['arrayData']['idmember'];
				$arr = $this->m_mobile_api->getReportBonus($idmember, $data['token'],$data['year'],$data['api_usr'],$data['api_pwd']);
			}

			if($arr != null) {
				$res = jsonTrueResponse($arr);
			}
			echo json_encode($res);
		}
	}
	
}		
		
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_xl_encrypt extends MY_Controller {

	//$route['xl/generateEncrypt/(:any)'] = 'api_xl_encrypt/createEncryptKey/$1';
	function createEncryptKey($waktu) {
		
		$double = 0;
		$ins = 0;
		for($i=1;$i<=24;$i++) {
			$num_padded = sprintf("%02d", $i);
			$tgl = date($waktu).$num_padded;
			
			
			/*$resEnc = $this->XlEncrypt($tgl);
			$hasil = json_decode($resEnc);
			if(array_key_exists("key", $hasil)) {
			    //echo "response dari API - $tgl : ".$hasil->key;
				//echo "<br />";
				if($this->checkTglEncrypt($tgl)) {
					$this->insertEncrypt($tgl, $hasil->key);
					$ins++;
				} else {
					//echo $tgl."<br />";
					$double++;
				}
			}*/
			
			if($this->checkTglEncrypt($tgl)) {
				$resEnc = $this->XlEncrypt($tgl);
				$hasil = json_decode($resEnc);
				if(array_key_exists("key", $hasil)) {
					$this->insertEncrypt($tgl, $hasil->key);
					$ins++;
				} else {
					$api_req_fail++;
				}
			} else {
				//echo $tgl."<br />";
				$double++;
			}
			
		}
		
		echo "Double : ".$double;
		echo "<br />Insert : ".$ins;
		
		
		
	}
	
	//$route['xl/encrypt/generate'] = 'api_xl_encrypt/createEncryptKeyCRON/$1';
	function createEncryptKeyCRON() {
		/*$tempWaktu = date("Ym");
		$tempTgl = date("d");
		$pengurang = $tempTgl - 4;
		$pembulatan_pengurang = sprintf("%02d", $pengurang);
		$tglDelete = $tempWaktu.$pembulatan_pengurang."24";
		echo "Tgl Delete : ".$tglDelete;
		echo "<br />";
		
		$prosesDeleteExpKey = $this->deleteExpireEncryptKey($tglDelete);
		*/
		$waktu = date("Ymd"); 
		$double = 0;
		$ins = 0;
		$api_req_fail = 0;
		
		
		for($i=1;$i<=24;$i++) {
			$num_padded = sprintf("%02d", $i);
			$tgl = date($waktu).$num_padded;
			
			
			
			    //echo "response dari API - $tgl : ".$hasil->key;
				//echo "<br />";
				if($this->checkTglEncrypt($tgl)) {
				    echo "tgl : ".$tgl;
					echo "<br />";
				    $resEnc = $this->XlEncrypt($tgl);
					$hasil = json_decode($resEnc);
					if(array_key_exists("key", $hasil)) {
						$this->insertEncrypt($tgl, $hasil->key);
						$ins++;
					} else {
						$api_req_fail++;
					}
				} else {
					//echo $tgl."<br />";
					$double++;
				}
			
			
		}
		
		
		
		if($double < 24) {
			$this->createEncryptKeyCRON();
		} /*else {
			
			$tempWaktu = date("Ym");
			$tempTgl = date("d");
			$pengurang = $tempTgl - 4;
			$pembulatan_pengurang = sprintf("%02d", $pengurang);
			$tglDelete = $tempWaktu.$pembulatan_pengurang."24";
			echo "Tgl Delete : ".$tglDelete;
			echo "<br />";
			
			$prosesDeleteExpKey = $this->deleteExpireEncryptKey($tglDelete);
		}*/
		
		echo "Double : ".$double;
		echo "<br />Insert : ".$ins;
		echo "<br />API Encrypt Request Failed : ".$api_req_fail;
	}
	
	function deleteExpireEncryptKey($tgl) {
		//$qry = "DELETE FROM ppob_xl_encrypt WHERE encrypt_time < '$tgl'";
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		
		$dbqryx->where('encrypt_time <=', $tgl);
		$dbqryx->delete('ppob_xl_encrypt');
	}
	
	function checkTglEncrypt($tgl) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('encrypt_time');
		$dbqryx->from('ppob_xl_encrypt');
		$dbqryx->where('encrypt_time', $tgl);
		$query = $dbqryx->get();	
				
		if($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	function getEncryptFromDB($tgl) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->select('*');
		$dbqryx->from('ppob_xl_encrypt');
		$dbqryx->where('encrypt_time', $tgl);
		$query = $dbqryx->get();	
				
		if($query->num_rows() > 0) {
			return $query->result();
		} else {
			return 0;
		}
	}
	
	function insertEncrypt($time, $code) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$arr = array(
			"encrypt_time" => $time,
			"encrypt_key" => $code,
		);
		$dbqryx->insert('ppob_xl_encrypt', $arr);
	}
	
	
	
	public function XlEncrypt($waktu)
	{
		$inpString = $this->adv_id . "|" . $this->pass_xl . "|" . date($waktu);
		$arr = array(
			'inputString' => $inpString,
			'encryptKey' => $this->key_xl
		);
		$urlx = $this->xl_get_encrypt;
		//echo $urlx;
		//$urlx = "https://112.215.105.108:443/digitalreward/v1/encrypt";
		$curl = curl_init();
		//echo "<pre>";
		//print_r($arr);
		//echo "</pre>";
		curl_setopt_array($curl, array(
			CURLOPT_PORT => "8243",
			CURLOPT_URL => $urlx,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($arr),
			CURLOPT_HTTPHEADER => $this->headerXL,
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		/*echo "<pre>";
		print_r($arr);
		print_r($info);
		echo "</pre>";
		*/
		if ($err) {
            //echo "Err when request encryptkey ";
			//echo "<br />";
			//print_r($err);
			
			echo "Err when request encryptkey ";
			echo "<br />";
			echo "<pre>";
			echo "param : ";
			echo "<br />";
			print_r($arr);
			echo "<br />";
			echo "error : ";
			echo "<br />";
			print_r($err);
			echo "<br />";
			echo "curl info : ";
			echo "<br />";
			print_r($info);
			echo "<br />";
			echo "response : ";
			echo "<br />";
			print_r($response);
			echo "</pre>";
			
			return 0;
		} else {
			//echo "dekripnya = ".$inpString."\n";
			//echo "enkrip key = ".$this->key_xl."\n";
			//print_r($err);
			//print_r($response);
			//curl -v -k https://112.215.105.53:8243/digitalreward/v1/encrypt -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Postman-Token: dc9a1b01-a655-49ed-b546-06e9fd1bb075' -H 'Channel: MyXL' -H 'Request-Id: 90812307518' -d '{"inputString": "2370|klinkTest123|2018111217","encryptKey": "ThisTestKey"}'
			return $response;
		}
	}
	
	
	//$route['xl/redeem/status/(:any)'] = 'api_xl_encrypt/checkXlRedeemStatus/$1';
	function checkXlRedeemStatus($trx_id) {
		$this -> load -> model("backend/be_hifi_model", "hifi");
		$cust_no = null;
		$checkDataReal = $this->hifi->getDetailPPOBTrxByID($trx_id);
		if($checkDataReal == null) {
		   $checkDataTemp = $this->hifi->getDetailTempPPOBTrxByID($trx_id);
		   if($checkDataTemp != null) {
			$cust_no = $checkDataTemp[0]->cust_no;
		   }
		} else {
			$cust_no = $checkDataReal[0]->cust_no;
		}  
		
		if($cust_no != null && $cust_no != "" && $cust_no != " ") {
			$arrXL = array(
				"transaction" => $trx_id,
				"msisdn" => $cust_no
			);
			
			$hasil = $this->curlXlRedeemStatus($arrXL);
			//echo json_encode($hasil);
		}
	}
	
	function curlXlRedeemStatus($arr) {
		$time = date("YmdH");
		$keyRes = $this->getEncryptFromDB($time);
		if($keyRes != null) {
			$final_key = str_replace("+", "%2B", $keyRes[0]->encrypt_key);
			$arr['key'] = $final_key; 
			$param = $this->setFieldPost($arr);
			//$ds ="http://apigw.xl.co.id/digitalreward/dev/v1/transaction?".$param;
			$ds ="https://api.xl.co.id:8243/digitalreward/v1/transaction?".$param;
			$urlx = $ds;
			$header = array(
				"Accept: application/json",
				"Content-Type: application/json",
				"channel: MyXL",
				"request-id: 908765789",
			);	
			
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_PORT => "8243",
				CURLOPT_URL => $urlx,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 120,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($arr),
				CURLOPT_HTTPHEADER => $header,
			));

			//print_r($this->headerXL);
			//echo $final_key;

			$response = curl_exec($curl);
			//curl_setopt($curl, CURLINFO_HEADER_OUT, true);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);
			
			
			if ($err) {
				//echo "Err when request encryptkey ";
				//echo "<br />";
				//print_r($err);
				
				echo "<pre>";
				echo "error : ";
				echo "<br />";
				print_r($err);
				echo "<br />";
				echo "curl info : ";
				echo "<br />";
				print_r($info);
				echo "<br />";
				echo "response : ";
				echo "<br />";
				print_r($response);
				echo "</pre>";
				
				//return jsonFalseResponse("sdsd");
			} else {
				//echo $response;
				//return $response;
				echo "<pre>";
				print_r($err);
				echo "<br />";
				echo "curl info : ";
				echo "<br />";
				print_r($info);
				echo "<br />";
				echo "response : ";
				echo "<br />";
				print_r($response);
				echo "</pre>";
			} 
		} else {
			return jsonFalseResponse("Encrypt Key not generated");
		}
	} 

	// $route['xl/generatekey'] = 'api_xl_encrypt/generateEncryptionXL';
	function generateEncryptionXL() {
		// initialize model
		$this->load->model('backend/xl_encrypt', 'xl');

		if ($this->username != NULL) {
			$data['result'] = $this->xl->getEncryptTime();

			// Turn $data['result'] into date format YYYY-MM-DD
			$data['year'] = substr($data['result'][0]->latest, 0, -6);
			$data['month'] = substr($data['result'][0]->latest, 4, -4);
			$data['day'] = substr($data['result'][0]->latest, 6, 2);
			$data['res'] = $data['year']."-".$data['month']."-".$data['day'];

			// fetch data to be a param
			$x = substr($data['result'][0]->latest, 0, -2);
			$data['rec'] = $this->xl->getRecordEncryption($x);

			$data['form_header'] = 'Generate Encryption Key';
			$data['icon'] = 'icon-pencil';
			$data['form_reload'] = 'xl/generatekey';
			$this->setTemplate('backend/hifi/generateKey', $data);
		} else {
			// throw error if there's no data to fetch
			echo setErrorMessage();
		}
	}

	// $route['xl/getencryptiondate'] = 'api_xl_encrypt/getEncryptionDate';
	function getEncryptionDate() {
		$data = $this->input->post(NULL, TRUE);
		$from = str_replace('-', '', $data['from']);
		$to = str_replace('-', '', $data['to']);
		$data['hasil'] = array();
		for($x = $from;$x <= $to;$x++){
			$res = $this->createEncryptionKey($x);
			array_push($data['hasil'], $res);
		}
		$this->load->view('backend/hifi/listGenKey', $data);
	}

	function createEncryptionKey($waktu) {
		
		$double = 0;
		$ins = 0;
		for($i=1;$i<=24;$i++) {
			$num_padded = sprintf("%02d", $i);
			$tgl = date($waktu).$num_padded;

			if($this->checkTglEncrypt($tgl)) {
				$resEnc = $this->XlEncrypt($tgl);
				$hasil = json_decode($resEnc);
				if(array_key_exists("key", $hasil)) {
					$this->insertEncrypt($tgl, $hasil->key);
					$ins++;
				} else {
					$api_req_fail++;
				}
			} else {
				// echo $tgl."<br />";
				$double++;
			}
		}
		return array(
			"waktu" => $waktu,
			"ins_sukses" => $ins,
			"ins_double" => $double,
		);
	}

	function checkEncrypt() {
		$this->load->model('backend/xl_encrypt', 'xl');
		$data = $this->input->post(NULL, TRUE);
		$from = str_replace('-', '', $data['from']);
		$to = str_replace('-', '', $data['to']);
		$data['result'] = $this->xl->chekLastEncrypt($from, $to);
		$this->load->view('backend/hifi/listEncryptDate', $data);
	}
}
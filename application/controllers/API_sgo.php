<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentExample
 */
class Api_sgo extends MY_Controller {

	//put your code here
	public function __construct() {
		parent::__construct();
		$this->load->service("webshop/Knet_payment_service",'paymentService');
		$this->sgoPasswordAccess = 'k-net181183';
		$this->folderView = 'backend/api_sgo/';
	}

	//$route['sgo/payment/check/status'] = 'API_sgo/checkUpdateStatus';
	function checkUpdateStatus() {
		$data = $this->input->post(NULL, TRUE);

		$rr_uuid = $this->sgo_usrname . "-CST-" . date("ymd-his");
		$rq_datetime = date("Y-m-d h:i:s");

		$arr[0] = strtoupper("signa7u12ekLINK");
		$arr[1] = $rq_datetime;
		$arr[2] = $data['order_id'];
		$arr[3] = "CHECKSTATUS";
		//print_r($arr);


		$sign = ppob_generateSignature($arr);

		$fields = array(
			'uuid' => urlencode($rr_uuid),
			'rq_datetime' => urlencode($rq_datetime),
			'comm_code' => urlencode($data['comm_code']),
			'order_id' => urlencode($data['order_id']),
			'is_paymentnotif' => urlencode($data['is_paymentnotif']),
			'signature' => urlencode($sign)
		);
		//print_r($fields);
		$postData = setFieldPost($fields);

		//SET URL CURL AND DATA
		$sendRequest = array(
			"url" => "https://api.espay.id/rest/merchant/status",
			"postdata" => $postData
		);

		$hasil = $this->sgo_curl($sendRequest);

		echo $hasil;
	}

	/**
	 * Sort data wether Success or Suspect
	 *
	 * @return  array
	 * 
	 * $route['sgo/payment/update/get/statusfailed'] = 'API_sgo/failedUpdate';
	 */
	function failedUpdate() {
		$this->load->library('csvreader');
		$fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != "") {
			$this->csvreader->set_separator(",");
			$data['csvData'] = $this->csvreader->parse_file($fileName);
			$i = 1;
			$berhasil_update = 0;
			$gagal_update = 0;
			$tidakDiCek = 0;
			$arrBerhasil = array();
			$arrGagal = array();
			foreach ($data['csvData'] as $list) {
				$tx_id = $list[0];
				$order_id = $list[1];
				$merchant_name = $list[2];
				$status = str_replace(' ','',$list[9]);
				$notif = "N";
				if (!empty($order_id) && $status == "Suspect" && $merchant_name != "TOP UP KLINK") {
					$check = $this->getCheckUpdateStatus($order_id, $i, $notif);
					$res = json_decode($check);
					if($res->tx_status == "S") {
						$berhasil_update++;
						$text = $res->tx_id." - ".$res->order_id;
						array_push($arrBerhasil, $text);
					} else {
						$gagal_update++;
						$text = $res->tx_id." - ".$res->order_id;
						array_push($arrGagal, $text);
					}
					$i++;
				} else {
					//echo "$order_id - $merchant_name -  $status<br />";
					$text = $tx_id." - ".$order_id." - ".$merchant_name;
					$tidakDiCek++;
				}
			}
			
			$data = array(
				"arrBerhasil" => $arrBerhasil,
				"arrGagal" => $arrGagal,
				"success" => $berhasil_update,
				"fail" => $gagal_update,
			);
			
			echo "Berhasil update : ".$berhasil_update."<br />";
			echo "Gagal update : ".$gagal_update."<br />";
			echo "Status success / Trx TOP UP : ".$tidakDiCek."<br />";
		} else {
			echo "No Data Uploaded!";
		}
	}

	/**
	 * Check status update
	 *
	 * @param   mixed  $order_id  
	 *
	 * @return  array
	 * 
	 * $route['sgo/payment/update/get/check/(:any)'] = 'API_sgo/getCheckUpdateStatus/$1';
	 */
	function getCheckUpdateStatus($order_id, $param, $notif) {
		$rr_uuid = $this->sgo_usrname . "-CST-" . date("ymd-his") .$param;
		$rq_datetime = date("Y-m-d h:i:s");
		$arr[0] = strtoupper("signa7u12ekLINK");
		$arr[1] = $rq_datetime;
		$arr[2] = $order_id;
		$arr[3] = "CHECKSTATUS";
		$sign = ppob_generateSignature($arr);
		$fields = array(
			'uuid' => urlencode($rr_uuid),
			'rq_datetime' => urlencode($rq_datetime),
			'comm_code' => urlencode('SGWKLINKMA'),
			'order_id' => urlencode($order_id),
			'is_paymentnotif' => urlencode($notif),
			'signature' => urlencode($sign)
		);
		$postData = setFieldPost($fields);
		//SET URL CURL AND DATA
		$sendRequest = array(
			"url" => "https://api.espay.id/rest/merchant/status",
			"postdata" => $postData
		);
		$hasil = $this->sgo_curl($sendRequest);
		return $hasil;
	}

	/**
	 * Show Update Form
	 *
	 * $route['sgo/payment/update/form'] = 'API_sgo/formUpdateSukses';
	 */
	function formUpdateSukses() {
		if(!empty($this->username)) {
			$this->load->library('csvreader');
			$data['form_header'] = "Import SGO Successfully Updated File to Database";
			$data['icon'] = "icon-pencil";
			$data['form_reload'] = 'sgo/payment/update/form';
			$this->setTemplate($this->folderView.'sgoImportForm', $data);
		 } else {
			redirect('backend', 'refresh');
		 }
	}

	/**
	 * Preview .csv files
	 * 
	 * $route['sgo/payment/update/form/preview'] = 'API_sgo/updateSukses';
	 */
	function updateSukses() {
		$this->load->library('csvreader');
		$fileName = $_FILES['myfile']['tmp_name'];
		if ($fileName != "") {
			$this->csvreader->set_separator(",");
			$data['csvData'] = $this->csvreader->parse_file($fileName);
			$this->load->view($this->folderView.'sgoPreviewFile', $data);
		} else {
			echo "No data uploaded";
		}
	}

	// $route['sgo/payment/update/form/save'] = 'API_sgo/saveToDB';
	function saveToDB() {
		$this->load->library('csvreader');
		$fileName = $_FILES["myfile"]["tmp_name"];
		if($fileName != ""){
			$this->csvreader->set_separator(",");
			$data['csvData'] = $this->csvreader->parse_file($fileName);
			//var_dump($data['csvData']);
			$save = $this->s_trans->saveImportToDB($data);
			//var_dump($save);
			$this->load->view($this->folderView.'sgoImportResult', $save);
		}else{
			echo "No Data Uploaded!";
		}
	}
}
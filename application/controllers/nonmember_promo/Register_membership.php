<?php

class Register_membership extends MY_Controller{
    public function __construct() {
        parent::__construct();

        $this -> folderView = "nonmember_promo/";
        $this -> load -> service("webshop/Product_service", 'productService');
        $this->load->model('M_mobile_api', 'm_mobile_api');
        $this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
		$this->load->model('webshop/Member_model');
        $this->load->service("webshop/Cartshop_service",'cartshopService');

    }

    public function index()
    {
        $dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
		$dt['resM'] = $this->checkTglBatasMampirkak();
        $data = $this->input->post(NULL, TRUE);
		//print_r($data);
		if ($data != null) {
            if ($this->cartshopService->checkShippingData($data)) {
            $dt['cart'] = $this->cart->contents();
			    if (array_key_exists('no_hp_konfirmasi', $data)) {
					$this->session->set_userdata('no_hp_konfirmasi', $data['no_hp_konfirmasi']);
				}
            
                $eerr = $this->updateCartBeforeProceed();
                $prdx = set_list_array_to_stringCart($dt['cart'], "id");

                
                $reg = $this->cartshopService->sessionRegPersonalInfoPromoMember($data);
                //$ship = $this->cartshopService->setShippingInfoBaru();
				$checkExistingFreePrd = $this->cartshopService->IfDoubleInputCart2($data['free_prdcd']);
				if(!$checkExistingFreePrd) {
					$this->session->unset_userdata('promo_free_prd');
					$free_product = array(
	                  "prdcd" => $data['free_prdcd'],
	                  "prdnm" => $data['free_prdnm'],
	                  "qty" => $data['free_qty'],
	                  "bv" => $data['free_bv'],
	                  "westPrice" => $data['free_westPrice'],
	                  "eastPrice" => $data['free_eastPrice'],
	                  "weight" => $data['free_weight'],
					);
					
					$this->session->set_userdata('promo_free_prd', $free_product);
					//$this->cartshopService->addToCartXXX($free_product);
					
				} 

                $personal_info = $this->session->userdata("personal_info");
				$dt['stockist'] = $personal_info['stockist'];
				if($personal_info['delivery'] == "2") {
	                
	                $dt['nama'] = $personal_info['nama_penerima'];
	                $dt['email'] = $personal_info['email'];
	                $dt['notlp'] = $personal_info['notlp'];
	                $dt['alamat'] = $personal_info['alamat'];
                } else {
                	$dt['nama'] = "";
	                $dt['email'] = "";
	                $dt['notlp'] = "";
	                $dt['alamat'] = "";
                }
                $dt['idno'] = "";
				$dt['errMessage'] = "";
                $dt['state'] = $this->M_nonmember_promo->getState($dt['stockist']);
                $state = $dt['state'][0]->state;
				$dt['showsponsor'] = $this->m_mobile_api->getListUplineByArea($state);
				$dt['memberlama']=false;
				$dt['bank'] = $this->Member_model->getListAffiliateBank();
                $this->setTempWebShopNonMember($this->folderView . 'registerMembership2', $dt);
//				$this->setTempWebShopNonMember($this->folderView . 'registerMembership', $dt);
            }
        }
/**
		$new_member_reg = $this->session->userdata("new_member_reg");
		if($new_member_reg != null) {
			$personal_info = $this->session->userdata("personal_info");
			    $dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
				$dt['stockist'] = $personal_info['stockist'];
				$dt['idno'] = $new_member_reg['noktp'];
				$dt['nama'] = $new_member_reg['membername'];
				$dt['alamat'] = $new_member_reg['addr'];
				$dt['email'] = $new_member_reg['email'];
				$dt['notlp'] = $new_member_reg['no_hp'];
				$dt['errMessage'] = "No HP / No KTP sudah terdaftar atau data sponsor belum diisi..";
                $dt['state'] = $this->M_nonmember_promo->getState($dt['stockist']);
                $state = $dt['state'][0]->state;
				$dt['showsponsor'] = $this->m_mobile_api->getListUplineByArea($state);
                $this->setTempWebShopNonMember($this->folderView . 'registerMembership2', $dt);
		}
 * **/
    }

	function backToRegisterMember() {
		$dt['prod'] = $this->productService->getProductShop();
        $dt['prodCat'] = $this->productService->getProductCategory();
		$dt['bank'] = $this->Member_model->getListAffiliateBank();
		$new_member_reg = $this->session->userdata("new_member_reg");
		if($new_member_reg != null) {
			$personal_info = $this->session->userdata("personal_info");
				$dt['stockist'] = $personal_info['stockist'];
				$dt['idno'] = $new_member_reg['noktp'];
				$dt['nama'] = $new_member_reg['membername'];
				$dt['alamat'] = $new_member_reg['addr'];
				$dt['email'] = $new_member_reg['email'];
				$dt['notlp'] = $new_member_reg['no_hp'];
				$dt['errMessage'] = "";
                $dt['state'] = $this->M_nonmember_promo->getState($dt['stockist']);
                $state = $dt['state'][0]->state;
				$dt['showsponsor'] = $this->m_mobile_api->getListUplineByArea($state);

			if($new_member_reg['sponsorid']=='IS_MEMBER'){
				$dt['memberlama']=true;
			}else
				$dt['memberlama']=false;
                $this->setTempWebShopNonMember($this->folderView . 'registerMembership2', $dt);
		}
	}

    function updateCartBeforeProceed() {

        if($this->_checkSessionStoreUser()) {
            $arr = jsonFalseResponse(NULL, "Update Cart gagal..!");
            $data = $this->input->post(NULL, TRUE);
            $upd = $this->cartshopService->updateCart($data);
            $this->session->set_userdata('pricecode', $data['pricecode']);
            if($upd) {
                $arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
            }
            //echo json_encode($arr);
            return $arr;
        }
    }

    function getSponsorName()
    {

        $sponsorid = $this->input->post('sponsoridinput');

        //print_r($id);

        $return = $this->M_nonmember_promo->getSponsorName($sponsorid);
//        print_r($return);
        /*$data['sponsorname']= $return[0]->fullnm;

        if('IS_AJAX') {
            echo json_encode($data);
        }*/
		
		if($return == null) {
			$arr = jsonFalseResponse("ID Member salah/tidak terdaftar..");	
		} else {
			$arr = jsonTrueResponse($return);
		}
		echo json_encode($arr);

    }

	function getSponsorName2()
	{

		$sponsorid = $this->input->post('idsponsor');

		//print_r($sponsorid);

		$return = $this->M_nonmember_promo->getSponsorName($sponsorid);
//        print_r($return);
		/*$data['sponsorname']= $return[0]->fullnm;

        if('IS_AJAX') {
            echo json_encode($data);
        }*/

		if($return == null) {
			$arr = jsonFalseResponse("ID Member salah/tidak terdaftar..");
		} else {
			$arr = jsonTrueResponse($return);
		}
		echo json_encode($arr);

	}

}

?>
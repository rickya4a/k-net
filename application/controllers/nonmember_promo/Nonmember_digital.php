<?php

class Nonmember_digital extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this -> load -> model("dtc/Mdtc_mbr", 'mdtc');
		//$this -> load -> service("webshop/Member_service", 'memberService');
		$this -> load -> service("webshop/Product_service", 'productService');
		$this -> load -> service("webshop/Login_service", 'loginService');
		$this->load->service("webshop/Cartshop_service",'cartshopService');
		$this -> folderPrd = base_url() . "assets/images/products/thumb/";
		$this->load->model('nonmember_promo/M_nonmember_promo', 'M_nonmember_promo');
		$this->load->service("webshop/Payment_service",'paymentService');
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');

		$this -> folderView = "nonmember_promo/";

		// di jadikan constructor agar langsung di jalankan saat di akses dan bisa di akses ke semua fungction
	}

	//$route['nonmember'] = 'nonmember_promo/nonmember_promo/home';
	public function home() {

		//		$data = $this->input->post(NULL, TRUE);
		$login = $this -> mdtc -> checkAuthLoginKnet();
		//		print_r($login);
		if ($login != null) {
			$telhp = getUserPhone();
			$email = getUserEmail();
			$this -> session -> set_userdata('store_info', $login);
			$this -> session -> set_userdata('pricecode', "12W3");

			//if(getUserID() == "IDSPAAA96407") {
			//print_r($promo);
			$promo = $this -> loginService -> checkListPromo();

			$this -> session -> set_userdata('promo', $promo);
			//}
		}

		$dt['prodCat'] = $this -> cartshopService -> getProductCategory();

		$dt['prdnm'] = $this -> input -> post("name");
		$dt['formAction'] = "" . site_url('cart/addtocart') . "";
		$dt['banner'] = $this -> productService -> getHeaderBanner();
		$dt['prod'] = $this -> productService -> getProductShop();
		//$dt['prodCat'] = $this -> productService -> getProductCategory();
		$dt['folderPrd'] = $this -> folderPrd;
		$dt['listProd'] = $this->productService->getProdByCatHeader('14');
		$dt['bannerCat'] = $this->productService->getHeaderBannerCat();

		$this -> setTempWebShopNonMemberDigital($this -> folderView.'product_category1Digital', $dt);

	}

	//$route['nonmember/cart/list'] = 'nonmember_promo/nonmember_promo/listCart';
	public function listCart()
	{
		if($this->_checkSessionStoreUser()) {
			$dt['formAction'] = "".site_url('shipping/postShipping')."";
			$dt['prodCat'] = $this->cartshopService->getProductCategory();
			//$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();    
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			//print_r($dt['bns']);
			$dt['cart'] = $this->cart->contents();
			$personal_info = $this->session->userdata('personal_info');
			if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAA96407") {
				//$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
				$dt['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
			} else {
				$dt['listCargo'] = $this->cartshopService->getListCargo();
			}

			$dt['listFreeProduct'] = $this->M_nonmember_promo->getListFree();

//			if($personal_info != null) {
//				//ubah dion @ 21/05/2017
//				if(array_key_exists("shipper", $personal_info)) {
//					if($personal_info['shipper'] == "1") {
//						$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
//						$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
//					} else {
//						$dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb();
//						$dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);
//					}
//				} else {
//					$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
//						$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
//				}
//
//
//				//print_r($dt['shipping']);
//				//echo "c";
//				$this->setTempWebShopNonMember($this -> folderView.'shippingWithCartData2Digital',$dt);
//			} else
			{
				$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();
				$res = $this->cartshopService->getAddressReferenceByID(getUserID());
				if($res != null) {
					$personal_infox = array(
						"provinsi" => $res[0]->provinsi,
						"kota" => $res[0]->kota,
						"kecamatan" => $res[0]->kecamatan,
					);
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);
					$this->cartshopService->setAutomaticPersonalInfo($res);
					//print_r($dt['shipping'])
					//echo "e";
					$this->setTempWebShopNonMemberDigital($this -> folderView.'shippingAutoAddrReff',$dt);
					//$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
				} else {
					//echo "v";
					$this->setTempWebShopNonMemberDigital($this -> folderView.'shippingWithCartData2Digital',$dt);
				}
			}
		}
	}
	function CheckoutCartSGODigital() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			{
				$dt['cart'] = $this->cart->contents();
				$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
				if($data != null) {
					$eerr = $this->updateCartBeforeProceed();
					$prdx = set_list_array_to_stringCart($dt['cart'], "id");
					$reg = $this->cartshopService->sessionRegPersonalInfoBaruDigital($data);
//					$ship = $this->cartshopService->setShippingInfoBaru();
					$dt['prodCat'] = $this->cartshopService->getProductCategory();


					if(getUserID() == "IDSPAAA66834") {
						$dt['listBank'] = $this->cartshopService->getBankJohnDoe();
					} else {
						$dt['listBank'] = $this->cartshopService->getBank();
					}

					$dt['key'] = '0df5835ee198d49944c372ead860c241';

					if(getUserID() == "IDSPAAA96407") {
						$dt['biaya'] = getTotalPayNetAndShipCostTerbaru();
					}
					/*}
                }*/
					/*------------------------------------------------------------
					 * END PROMO 17 AGUSTUS CODE
					 * -----------------------------------------------------------*/

					$dt['payID'] = "ECD".randomNumber(8);
					$this->session->unset_userdata('pay_sgo_id');
					$this->session->set_userdata("pay_sgo_id", $dt['payID']);
//					$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
//					$insTempTrx = $this->M_nonmember_promo->insertEcommerceTrxSGO($dt['payID']);
//					$dt['backURL'] = "http://www.k-net.co.id/pay/sgo/finish/".$dt['payID'];
				}
				{
					$this->setTempWebShopNonMemberDigital($this->folderView.'payment_sgo_vch',$dt);
				}

			}
		}
	}


	function submitDIGITAL() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);
			{
				$dt['cart'] = $this->cart->contents();
				$dt['listVoucher'] = $this->cartshopService->getListValidVoucher();
//				{
//					$this->setTempWebShopNonMemberDigital($this->folderView.'payment_sgo_vch',$dt);
//				}

			}
		}
		$dtx['payID'] = "ED".randomNumber(8);

		$insTempTrx = $this->M_nonmember_promo->insertEcommerceTrxSGO($dtx['payID']);


		$x = jsonTrueResponse($dtx);
		echo json_encode($x) ;


	}
	//$route['nonmember/checkout'] = 'nonmember_promo/nonmember_promo/CheckoutCart';
	function CheckoutCart() {
		if($this->_checkSessionStoreUser()) {
			$data = $this->input->post(NULL, TRUE);

			$personal_info = $this->session->userdata('personal_info');
			//if($this->cartshopService->checkShippingData($arrShip)) {
			$dt['cart'] = $this->cart->contents();
			if($data != null) {
				//$eerr = $this->updateCartBeforeProceed();
				$prdx = set_list_array_to_stringCart($dt['cart'], "id");
				$berat = $this->cart->total_weight();
				$promo_free_prd = $this->session->userdata('promo_free_prd');
				$dt['promo_free_prd'] = $promo_free_prd;
				$dt['berat_total'] = $berat + ($promo_free_prd['qty'] * $promo_free_prd['weight']);
				$ship = $this->cartshopService->setShippingInfoNonMember($dt['berat_total']);
				$dt['prodCat'] = $this->cartshopService->getProductCategory();

				$dt['listBank'] = $this->cartshopService->getBank();
				//production
				//$dt['key'] = 'f1d0e2ac8f5ae5ea96c9f09e3f055f85';
				//development 0df5835ee198d49944c372ead860c241
				$dt['key'] = '0df5835ee198d49944c372ead860c241';

				if($data['sponsor'] == "input") {
					$sponsorid = $data['sponsoridinput'];
				} else {
					$sponsorid = $data['sponsoridpilih'];
				}

				$arrMember = array(
					"noktp" => $data['idno'],
					"membername" => $data['membername'],
					"tgllhr" => $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'],
					"sex" => $data['sex'],
					"addr" => $data['addr1'],
					"no_hp" => $data['tel_hp'],
					"email" => $data['memb_email'],
					"sponsorid" => $sponsorid,
					"sponsorname" => $data['sponsorname'],
					"recruiterid" => $sponsorid,
					"rekruitername" => $data['sponsorname'],
				);

				$this->session->set_userdata('new_member_reg', $arrMember);
			}

			if($personal_info['delivery'] == "2" && $ship != 1 && $ship == null) {
				echo "ship : ".$ship;
			} else {
				//$insTempTrx = $this->cartshopService->insertEcommerceTrxSGO($dt['payID']);
				//$this->setTemplateDevSGO('webshop/payment_sgo',$dt);
				$this->setTempWebShopNonMember($this->folderView . 'payment_sgo',$dt);
			}

			/*} else {
				//$reg = $this->cartshopService->sessionRegPersonalInfo($data);
				//echo "<script>alert('Mohon data dilengkapi dahulu..')</script>";
				//redirect('cart/list');
				echo "<script>
						window.location.href='list';
						alert('Mohon data dilengkapi dahulu..');
						</script>";
			}	*/
		}
	}

	function updateCartBeforeProceed() {

		if($this->_checkSessionStoreUser()) {
			$arr = jsonFalseResponse(NULL, "Update Cart gagal..!");
			$data = $this->input->post(NULL, TRUE);
			$upd = $this->cartshopService->updateCart($data);
			$this->session->set_userdata('pricecode', $data['pricecode']);
			if($upd) {
				$arr = jsonTrueResponse(NULL, "Update Cart berhasil..!");
			}
			//echo json_encode($arr);
			return $arr;
		}
	}


	public function afterPaymentWithSGODev($order_id) {
		$this->load->model("webshop/Payment_model",'pmodel');

		$dta['trxInsertStatus'] = "fail";
		$dta['prodCat'] = $this->paymentService->getProductCategory();
		$dta['trans'] = $this->ecomm_trans_model->getInsertTrxOrderIDNonMemberDigital($order_id);
		$dta['catalog']=null;
		//print_r($dta['trans']);
		if ($dta['trans'] > 0 || $dta['trans'] != null) {
			$dta['trxInsertStatus'] = "ok";
			$dta['jne'] = $dta['trans'][0]->conoteJNE;
			$data_catalog=$this->pmodel->getKATALOG($order_id);
			if($data_catalog != null){
				$dta['catalog']=$data_catalog;
				$dta['token']=$order_id;
//				emailSend('Katalog Digital K-Link', getUserEmail(), KatalogEmail($order_id, $data_catalog));

				foreach($data_catalog as $row){
					$link='Download katalog di : '.base_url('catalogue/download/'.$order_id.'/'.$row->cat_inv_id);
//					smsTemplate(getUserPhone(), $link);
				}

			}

		} else {

			//$sgo = $this->paymentService->getDataPaymentSGOByOrderID($order_id);
			$sgo = $this->paymentService->getDataPaymentSGOByOrderIDDev($order_id);
			if($sgo[0]->bank_code_payment == "16") {
				$dta['trxInsertStatus'] = "pending";
			}
		}

//		$this->_destroy_cart_session();
		session_destroy();
		$this->setTempWebShopNonMemberDigital($this->folderView.'pay_ecommerce_resultDigital',$dta);

//		$this->setTempWebShopNonMemberDigital('webshop/payment_sgo/pay_ecommerce_result',$dta);
		//print_r($dt['prodCat']);
	}

}

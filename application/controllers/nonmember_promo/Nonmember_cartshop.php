<?php

class Nonmember_promo extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this -> load -> model("dtc/Mdtc_mbr", 'mdtc');
		//$this -> load -> service("webshop/Member_service", 'memberService');
		$this -> load -> service("webshop/Product_service", 'productService');
		$this -> load -> service("webshop/Login_service", 'loginService');

		$this -> folderPrd = base_url() . "assets/images/products/thumb/";

		$this -> folderView = "backend/klaimbonus/";

		// di jadikan constructor agar langsung di jalankan saat di akses dan bisa di akses ke semua fungction
	}

    //$route['nonmember'] = 'nonmember_promo/nonmember_promo/home';
	public function home() {

		//		$data = $this->input->post(NULL, TRUE);
		$login = $this -> mdtc -> checkAuthLoginKnet();
		//		print_r($login);
		if ($login != null) {
			$telhp = getUserPhone();
			$email = getUserEmail();
			$this -> session -> set_userdata('store_info', $login);
			$this -> session -> set_userdata('pricecode', "12W3");

			//if(getUserID() == "IDSPAAA96407") {
			//print_r($promo);
			$promo = $this -> loginService -> checkListPromo();

			$this -> session -> set_userdata('promo', $promo);
			//}
		}

		$dt['prodCat'] = $this -> productService -> getProductCategory();

		$dt['prdnm'] = $this -> input -> post("name");
		$dt['formAction'] = "" . site_url('cart/addtocart') . "";
		$dt['banner'] = $this -> productService -> getHeaderBanner();
		$dt['prod'] = $this -> productService -> getProductShop();
		//$dt['prodCat'] = $this -> productService -> getProductCategory();
		$dt['folderPrd'] = $this -> folderPrd;

		$this -> setTempWebShopNonMember('webshop/shop_home_iman', $dt);

	}

    
    //$route['nonmember/cart/list'] = 'nonmember_promo/nonmember_promo/listCart';
    public function listCart() 
    {    
       /* if($this->_checkSessionStoreUser()) {
            $dt['formAction'] = "".site_url('shipping/postShipping')."";
            $dt['prodCat'] = $this->cartshopService->getProductCategory();
			//$dt['show_provinsi'] = $this->cartshopService->showListProvinsi();    
			$dt['bns'] = $this->cartshopService->getCurrentPeriod();
			//print_r($dt['bns']);
			$dt['cart'] = $this->cart->contents(); 
            $personal_info = $this->session->userdata('personal_info');
			if(getUserID() == "IDSPAAA66834" || getUserID() == "IDSPAAA96407") {
				//$dt['listCargo'] = $this->cartshopService->getListCargoJNE();
				$dt['listCargo'] = $this->cartshopService->getListCargoJhonDoe();
			} else {
				$dt['listCargo'] = $this->cartshopService->getListCargo();
			}
			
			if($personal_info != null) {
				//ubah dion @ 21/05/2017
				if(array_key_exists("shipper", $personal_info)) {
					if($personal_info['shipper'] == "1") {
						$dt['show_provinsi'] = $this->cartshopService->showListProvinsi(); 
						$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);	
					} else {
						$dt['show_provinsi'] = $this->cartshopService->showListProvinsiKgb(); 
						$dt['shipping'] = $this->cartshopService->getShippingDataKgb($personal_info);	
					}	
				} else {
					$dt['show_provinsi'] = $this->cartshopService->showListProvinsi(); 
						$dt['shipping'] = $this->cartshopService->getShippingData2($personal_info);
				}	
				
					
				//print_r($dt['shipping']);
				//echo "c";
				$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
			} else {
				$dt['show_provinsi'] = $this->cartshopService->showListProvinsi(); 
				$res = $this->cartshopService->getAddressReferenceByID(getUserID());
				if($res != null) {
					$personal_infox = array(
					    "provinsi" => $res[0]->provinsi,
	                    "kota" => $res[0]->kota,
	                    "kecamatan" => $res[0]->kecamatan,
					);
					$dt['shipping'] = $this->cartshopService->getShippingData2($personal_infox);	
					$this->cartshopService->setAutomaticPersonalInfo($res);
					//print_r($dt['shipping'])
					//echo "e";
					$this->setTempWebShopNonMember('webshop/shippingAutoAddrReff',$dt);
					//$this->setTempWebShop('webshop/shippingWithCartData2',$dt);
				} else {
					//echo "v";
					$this->setTempWebShopNonMember('webshop/shipping2',$dt);
				} 
			}
		} */
    }

}

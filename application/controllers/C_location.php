<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class C_location extends MY_Controller{

    public function __construct(){

        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        $this->usr_api = "k-net.apps";
        $this->pwd_api = "apps@2017";
        $this->salt = "knet";
        $this->dataForm = $this->input->post(NULL, TRUE);
        $this->output->set_header( "Access-Control-Allow-Origin: *" );
        $this->output->set_header( "Access-Control-Allow-Credentials: true" );
        $this->output->set_header( "Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS" );
        $this->output->set_header( "Access-Control-Max-Age: 604800" );
        $this->output->set_header( "Access-Control-Request-Headers: x-requested-with" );
        $this->output->set_header( "Access-Control-Allow-Headers: x-requested-with, x-requested-by" );

        $this->load->model('lokasi/M_location');
        $this->load->model("webshop/Sales_model",'salesM');
        $this->load->model('webshop/ecomm_trans_model', 'mm_trans');
    }

    public function index($a1,$a2){
        $data['page']=$a1;

        //cek sudah ada atau belum
        $res= $this->M_location->cekLokasi($a1);
        $qty= $res[0]->qty;

        print_r("cek lokasi =>".$qty);
        print_r("a3 =>".$a1);

        //update koordinat
        $this->M_location->updateAddress1($a1,$a2); //update mssc
        if($qty != 0){
            //update koordinat
            $this->M_location->updateAddress2($a1,$a2);
        }

        $this->load->view('lokasi/stokist', $data);
    }

    public function verifikasi_sms(){

        $a1= $this->input->post('lat');
        $a2 = $this->input->post('long');
        $a3 = $this->input->post('loccd');
        $telp="";

        //echo "loccd: ".$a3;
        //print_r($a3);

        //get nomor hp stokist
        $dt= $this->M_location->getInfo($a3);
        $hp=$dt[0]->tel_hp;

        //print_r($hp);
        //echo "nomor hp: ".$hp;

        $hp_val=str_replace("62","0",$hp);
        $hp_val=str_replace("+","",$hp_val);
        $hp_val=str_replace("-","",$hp_val);
        $hp_val=str_replace(" ","",$hp_val);

        $kode = rand(111111,999999);
        $text = "Pastikan nomor handphone stokist sudah terdaftar, jika tidak yakin bisa hubungi stokist koordinator / CS K-Link. Kode Verifikasi OTP Lokasi GPS Anda adalah: ".$kode.". OTP Anda akan kadaluarsa dalam waktu 5 menit.";
        //$telp = "081807278131";
        $telp= $hp_val;
        smsTemplate($telp, $text);

        //empty otp ke tabel
        $this->M_location->emptyOTP($a3);

        //insert otp ke tabel
        $this->M_location->insertOTP($a3,$kode,$telp);

        $data['lat']=$a1;
        $data['long']=$a2;
        $data['page']=$a3;
        $this->load->view('lokasi/v_otp', $data);
    }

    public function verifikasi_sms2($a1,$a2,$a3){

        /*$a1= $this->input->post('lat');
        $a2 = $this->input->post('long');
        $a3 = $this->input->post('loccd');*/

        //echo "loccd: ".$a3;

        $telp="";
        //get nomor hp stokist
        $dt= $this->M_location->getInfo($a3);
        $hp=$dt[0]->tel_hp;

        $hp_val=str_replace("62","0",$hp);
        $hp_val=str_replace("+","",$hp_val);
        $hp_val=str_replace("-","",$hp_val);
        $hp_val=str_replace(" ","",$hp_val);

        $kode = rand(111111,999999);
        $text = "Pastikan nomor handphone stokist sudah terdaftar, jika tidak yakin bisa hubungi stokist koordinator / CS K-Link. Kode Verifikasi OTP Lokasi GPS Anda adalah: ".$kode.". OTP Anda akan kadaluarsa dalam waktu 5 menit.";
        //$telp = "081807278131";
        $telp= $hp_val;
       // smsTemplate($telp, $text); //ditutup sementara per tgl 15.02.2019

        //echo "nomor hp: ".$telp;

        //empty otp ke tabel
        $this->M_location->emptyOTP($a3);

        //insert otp ke tabel
        $this->M_location->insertOTP($a3,$kode,$telp);

        $data['lat']=$a1;
        $data['long']=$a2;
        $data['page']=$a3;
       // $this->load->view('lokasi/v_otp', $data);
        redirect('http://www.k-net.co.id/otp_proses2/'.$a1.'/'.$a2.'/'.$a3, 'refresh');
    }

    public function verifikasi_sms3(){
        $a1= $this->input->post('lat');
        $a2 = $this->input->post('long');
        $a3 = $this->input->post('loccd');
        /*$a1= $this->input->post('lat');
        $a2 = $this->input->post('long');
        $a3 = $this->input->post('loccd');*/

        //echo "loccd: ".$a3;

        $telp="";
        //get nomor hp stokist
        $dt= $this->M_location->getInfo($a3);
        $hp=$dt[0]->tel_hp;

        $hp_val=str_replace("62","0",$hp);
        $hp_val=str_replace("+","",$hp_val);
        $hp_val=str_replace("-","",$hp_val);
        $hp_val=str_replace(" ","",$hp_val);

        $kode = rand(111111,999999);
        $text = "Pastikan nomor handphone stokist sudah terdaftar, jika tidak yakin bisa hubungi stokist koordinator / CS K-Link. Kode Verifikasi OTP Lokasi GPS Anda adalah: ".$kode.". OTP Anda akan kadaluarsa dalam waktu 5 menit.";
        //$telp = "081807278131";
        $telp= $hp_val;
        // smsTemplate($telp, $text); //ditutup sementara per tgl 15.02.2019

        //echo "nomor hp: ".$telp;

        //empty otp ke tabel
        $this->M_location->emptyOTP($a3);

        //insert otp ke tabel
        $this->M_location->insertOTP($a3,$kode,$telp);

        $data['lat']=$a1;
        $data['long']=$a2;
        $data['page']=$a3;
        // $this->load->view('lokasi/v_otp', $data);
        redirect('http://www.k-net.co.id/otp_proses2/'.$a1.'/'.$a2.'/'.$a3, 'refresh');
    }

    public function otp_verifikasi($a1,$a2,$a3){
        $data['lat']=$a1;
        $data['long']=$a2;
        $data['page']=$a3;
        //redirect('http://www.k-net.co.id/otp_send/'.$a1.'/'.$a2.'/'.$a3, 'refresh');
        $this->load->view('lokasi/v_otp', $data);
    }

    public function otp_proses($a1,$a2,$a3){

        $kode = $this->input->post('otp');
        $result= $this->M_location->getSelect($a3);
        $kode_otp= $result[0]->otp;


        //get nomor hp stokist
        $dt= $this->M_location->getInfo($a3);
        $sctype=$dt[0]->sctype;

        if($kode == $kode_otp){
            $srvReturn = $this->M_location->updateOTP($a3);
            if($srvReturn){

                //cek sudah ada atau belum
                $res= $this->M_location->cekLokasi($a3);
                $qty= $res[0]->qty;

                print_r($qty);

                //update koordinat
                $this->M_location->updateCoord($a1,$a2,$a3); //update mssc
                $this->M_location->updateCoord2($a1,$a2,$a3); //update master_warehouse_list

                /*if($qty > 0){
                    //update koordinat
                    $this->M_location->updateCoord($a1,$a2,$a3);
                }else{
                    //insert koordinat
                    $this->M_location->insertCoord($a1,$a2,$a3);
                }*/

                //kembali menu awal
                if($sctype == 3){
                    echo '<script language="javascript">';
                    echo 'alert("Lokasi Berhasil Disimpan, silahkan kembali ke Menu Utama!")';
                    echo '</script>';
                    redirect('http://www.k-linkmember.co.id/transaction/lokasi/listms', 'refresh');

                }else{
                    echo '<script language="javascript">';
                    echo 'alert("Lokasi Berhasil Disimpan, silahkan kembali ke link sebelumnya!")';
                    echo '</script>';
                    echo '<script>window.history.go(-1);</script>';
                    echo '<script>window.history.go(-2);</script>';
                }

            }else{
                //kembali menu awal
                if($sctype == 3){
                    echo '<script language="javascript">';
                    echo 'alert("Kode OTP gagal diproses, silahkan kembali ke Menu Utama!")';
                    echo '</script>';
                    redirect('http://www.k-linkmember.co.id/transaction/lokasi', 'refresh');

                }else{
                    echo '<script language="javascript">';
                    echo 'alert("Kode OTP gagal diproses, silahkan kembali ke link sebelumnya!")';
                    echo '</script>';
                    echo '<script>window.history.go(-1);</script>';
                    echo '<script>window.history.go(-2);</script>';
                }
            }

        }else{
            //kembali menu awal
            if($sctype == 3){
                echo '<script language="javascript">';
                echo 'alert("Kode OTP gagal diproses, silahkan kembali ke Menu Utama!")';
                echo '</script>';
                redirect('http://www.k-linkmember.co.id/transaction/lokasi', 'refresh');

            }else{
                echo '<script language="javascript">';
                echo 'alert("Kode OTP gagal diproses, silahkan kembali ke link sebelumnya!")';
                echo '</script>';
                echo '<script>window.history.go(-1);</script>';
                echo '<script>window.history.go(-2);</script>';
            }
        }



    }

    public function otp_proses2($a1,$a2,$a3){

        //$kode = $this->input->post('otp');
        $result= $this->M_location->getSelect($a3);
        $kode_otp= $result[0]->otp;


        //get nomor hp stokist
        $dt= $this->M_location->getInfo($a3);
        $sctype=$dt[0]->sctype;

        //cek sudah ada atau belum
        $res= $this->M_location->cekLokasi($a3);
        $qty= $res[0]->qty;

        print_r("cek lokasi =>".$qty);
        print_r("a3 =>".$a3);

        //update koordinat
        $this->M_location->updateCoord($a1,$a2,$a3); //update mssc
        //$this->M_location->updateCoord2($a1,$a2,$a3); //update master_warehouse_list

        if($qty == 0){
        //insert koordinat
            $this->M_location->insertCoord($a1,$a2,$a3);
        }else{
            //update koordinat
            $this->M_location->updateCoord2($a1,$a2,$a3);

        }

        //kembali menu awal
        if($sctype == 3){
            echo '<script language="javascript">';
            echo 'alert("Lokasi Berhasil Disimpan, silahkan kembali ke Menu Utama!")';
            echo '</script>';
            redirect('http://www.k-linkmember.co.id/transaction/lokasi/listms', 'refresh');

        }else{
            echo '<script language="javascript">';
            echo 'alert("Lokasi Berhasil Disimpan, silahkan kembali ke link sebelumnya!")';
            echo '</script>';
            echo '<script>window.history.go(-1);</script>';
            echo '<script>window.history.go(-2);</script>';
        }



    }


    public function get_route(){
        $this->load->view('lokasi/tes_direction');
    }

    public function setFieldPost($fields) {
        //$data_string = json_encode($fields);
        //url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        //rtrim($fields_string, '&');
        $fields_string = rtrim($fields_string, '&');
        return $fields_string;
    }

    //START GOSEND
    public function price_send(){
        //get user key gosend
        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $x= $this->input->post('lat');
        $y= $this->input->post('long');

        $dt =price_gsend($user_key,$pass_key,$x,$y);
        //$result= json_decode($dt, TRUE);
        echo($dt);
    }

    public function price_send_dev(){
        //get user key gosend
        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKeyDev();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $x= $this->input->post('lat');
        $y= $this->input->post('long');

        $dt = price_gsend_dev($user_key,$pass_key,$x,$y);
        //$result= json_decode($dt, TRUE);
        echo($dt);
    }

    public function booking_send(){
        $metode = $this->input->post('metode');
        $des_name = $this->input->post('penerima');
        $des_phone = $this->input->post('tlp');
        $des_coord = $this->input->post('coord');
        $des_add = $this->input->post('alamat');
        $item = $this->input->post('item');
        $price = $this->input->post('harga');

        //get user key gosend
        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        //insert to table
        //$this->M_location->insertBooking($metode,$des_name,$des_phone,$des_add,$des_X,$des_Y,$item,$price);

        $dt = booking_gsend($user_key,$pass_key,$metode,$des_name,$des_phone,$des_add,$des_coord,$item,$price);
        //$result= json_decode($dt, TRUE);
        echo($dt);
    }

    public function status_send(){
        //get user key gosend
        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $orderno = $this->input->post('orderno');
        $dt =status_gsend($user_key,$pass_key,$orderno); //ada di dion helper
        echo($dt);
    }

    public function cek_warehouse(){ //$route['cek_warehouse'] = 'c_location/cek_warehouse';
        $kode_stk= $this->input->post('kode_stk');
        $kode_wh= $this->input->post('kode_wh');
        $dt = $this->cek_wh($kode_wh,$kode_stk);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function cek_gs_product2(){ //$route['cek_gs_product'] = 'c_location/cek_gs_product';
        $kode_wh= $this->input->post('wh');
        $kode_loccd= $this->input->post('loccd');
        $kode_prdcd= $this->input->post('kd_prd');

        $dt = $this->cek_prd_stok($kode_wh,$kode_loccd,$kode_prdcd);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function cek_gs_product(){ //$route['cek_gs_product'] = 'c_location/cek_gs_product';
        $kode_wh= $this->input->post('kd_wh');
        $kode_loccd= $this->input->post('kd_stk');
        $kode_prdcd= $this->input->post('kd_prd');

        $dt = $this->cek_prd_stok2($kode_wh,$kode_loccd,$kode_prdcd);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function cek_jarak(){
        $latitude1= $this->input->post('lat');
        $longitude1 = $this->input->post('long');

        $dt = $this->cek_distance($latitude1,$longitude1);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function cek_jarak1(){
        $lat1= $this->input->post('lat1');
        $long1= $this->input->post('long1');

        $lat2= $this->input->post('lat2');
        $long2= $this->input->post('long2');

        $dt = $this->cek_distance1($lat1,$long1,$lat2,$long2);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function cek_jarak2($latitude1,$longitude1){ //$route['gosend/cek_jarak/(:any)/(:any)'] = 'c_location/cek_jarak2/$1/$2';
        //$latitude1= $this->input->post('lat');
        //$longitude1 = $this->input->post('long');

        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $dt = $this->cek_distance2($user_key,$pass_key,$latitude1,$longitude1); //ada di core > My Controller
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function cek_jarak3($latitude1,$longitude1){ //$route['gosend/cek_jarak2/(:any)/(:any)'] = 'c_location/cek_jarak3/$1/$2';
        //$latitude1= $this->input->post('lat');
        //$longitude1 = $this->input->post('long');

        $this->load->model('lokasi/M_location', 'M_location');
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        $dt = $this->cek_distance3($user_key,$pass_key,$latitude1,$longitude1);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function status_update() {
        //get authorization
        $qry= $this->M_location->getAuth();
        $auth= $qry[0]->authorization;
        $content="application/json";
        $accept="application/json";

        //cek authorization
       // $auth= "gojek1234";
        $myHeaders = $this->input->request_headers();

        $headers_content= $myHeaders["Content-Type"];
        $headers_accept= $myHeaders["Accept"];
        $headers_auth= $myHeaders["Authorization"];

        //get request data
        $json = file_get_contents('php://input');
        $result= json_decode($json, TRUE);

        //echo $result;
        /*$jsonIterator = new RecursiveIteratorIterator(
                  new RecursiveArrayIterator(json_decode($json, TRUE)),
                  RecursiveIteratorIterator::SELF_FIRST);*/

        $entity_id=$result["entity_id"];
        $type= $result["type"];
        $status= $result["status"];
        $cancel= $result["cancelled_by"];
        $booking_type= $result["booking_type"];
        $driver_name= $result["driver_name"];
        $driver_phone = $result["driver_phone"];
        $receiver_name  =$result["receiver_name"];
        $total_km= $result["total_distance_in_kms"];
        $pickup_eta= $result["pickup_eta"]; //epoch format
        $delivery_eta= $result["delivery_eta"]; // epoch format
        $price= $result["price"];
        $cancel_reason= $result["cancellation_reason"];
        $live_tracking= $result["liveTrackingUrl"];
        $shipper_id='';

        $pickup_time=date("Y-m-d H:i:s", substr($pickup_eta, 0, 10));
        $delivery_time =date("Y-m-d H:i:s", substr($delivery_eta, 0, 10));

        //get orderno
        $res= $this->M_location->getOrderNo($entity_id);
        //$orderno= $res[0]->orderno;
        if($res != null){
            foreach($res as $row1){
                $orderno=$row1->orderno;
            }
            
           // $exec= $this->M_location->insertTrackingWebhook($orderno,$entity_id,$type,$status,$cancel,$booking_type,$driver_name,$driver_phone,$receiver_name,$total_km,$pickup_time,$delivery_time,$price,$cancel_reason,$live_tracking,$shipper_id);

        }

        $exec= $this->M_location->insertTrackingWebhook($orderno,$entity_id,$type,$status,$cancel,$booking_type,$driver_name,$driver_phone,$receiver_name,$total_km,$pickup_time,$delivery_time,$price,$cancel_reason,$live_tracking,$shipper_id);


        /*if($status == "no_driver"){
         //call make booking
            $dts = $this->salesM->getProductOrder($orderno);
            if($dts != null){
                foreach($dts as $row){
                    $orderdate=$row->datetrans;
                    $price = $row->total_pay;
                    $des_name = $row->receiver_name;
                    $des_phone =$row->lat_dest;
                    $des_addr = $row->long_dest;
                    $lat=$row->lat_dest;
                    $long=$row->long_dest;
                    $des_coord = $lat.",".$long;
                    $metode=$row->service_type_name;
                }
            }

            $qryItem = "SELECT a.prdnm,a.qty
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
            $queryExec = $this->db->query($qryItem);

            if($queryExec->num_rows() > 0) {
                $no=0;
                foreach($queryExec->result() as $row1)
                {
                    $no++;
                    $prdnm= $row1->prdnm;
                    $qty= $row1->qty;

                    $concat_item= "(".$prdnm." : ".$qty." pc)";
                    $item_arr[]= $concat_item;
                    $item=implode(" ",$item_arr);
                }

            }
            //menggunakan API k-net

            //hapus tabel history gosend
            $this->salesM->delTracking1($orderno);

            $dt = booking_gsend($metode,$des_name,$des_phone,$des_addr,$des_coord,$item,$price);
            $result= json_decode($dt, TRUE);
            $order_g=$result['orderNo'];

            //update tabel
            $exec= $this->salesM->updateShippaddr($orderno,$des_name,$des_addr,$des_phone,$order_g,$lat,$long);

        }*/

        if($status == "no_driver"){

            //$hp3="085717743438"; //nohp vera
            $hp3="08568369705"; //nohp myna
            $txt = "Driver Not Found:".$orderno." Reason:".$cancel_reason;
            smsTemplate($hp3, $txt);

        }elseif($status == "on_hold"){
            //$hp3="085717743438"; //nohp vera
            $hp3="08568369705"; //nohp myna
            $txt = "On Hold Order:".$orderno." Reason:".$cancel_reason;
            smsTemplate($hp3, $txt);

        }elseif($status == "rejected"){
            //$hp3="085717743438"; //nohp vera
            $hp3="08568369705"; //nohp myna
            $txt = "Rejected Order:".$orderno." Reason:".$cancel_reason;
            smsTemplate($hp3, $txt);
        }else{
            $arr = array("status" =>401);
        }

        if($headers_auth != $auth){
            $arr = array("status" =>401);

        }

        if($exec){
            $arr = array("status" =>200);
        }else{
            $arr = array("status" =>401);
        }
        echo json_encode($arr);

    }

    public function status_tracking() {
        //get authorization
        $qry= $this->M_location->getAuthDev();
        $auth= $qry[0]->authorization;
        $content="application/json";
        $accept="application/json";

        //get user key gosend
        $getKey= $this->M_location->getUserKey();
        $user_key= $getKey[0]->username;
        $pass_key= $getKey[0]->key_user;

        //cek authorization
        // $auth= "gojek1234";
        $myHeaders = $this->input->request_headers();

        $headers_content= $myHeaders["Content-Type"];
        $headers_accept= $myHeaders["Accept"];
        $headers_auth= $myHeaders["Authorization"];

        //get request data
        $json = file_get_contents('php://input');
        $result= json_decode($json, TRUE);

        //echo $result;

        $entity_id=$result["entity_id"];
        $type= $result["type"];
        $status= $result["status"];
        $cancel= $result["cancelled_by"];
        $booking_type= $result["booking_type"];
        $driver_name= $result["driver_name"];
        $driver_phone = $result["driver_phone"];
        $receiver_name  =$result["receiver_name"];
        $total_km= $result["total_distance_in_kms"];
        $pickup_eta= $result["pickup_eta"]; //epoch format
        $delivery_eta= $result["delivery_eta"]; // epoch format
        $price= $result["price"];
        $cancel_reason= $result["cancellation_reason"];
        $live_tracking= $result["liveTrackingUrl"];
        //$live_tracking= "https://gosend-livetracking.gojek.co.id/";
        $shipper_id='';

        $pickup_time=date("Y-m-d H:i:s", substr($pickup_eta, 0, 10));
        $delivery_time =date("Y-m-d H:i:s", substr($delivery_eta, 0, 10));

        //get orderno
        $res= $this->M_location->getOrderNo($entity_id);
        if($res != null){
            foreach($res as $row1){
                $orderno=$row1->orderno;
            }
            //$exec= $this->M_location->insertTrackingWebhook($orderno,$entity_id,$type,$status,$cancel,$booking_type,$driver_name,$driver_phone,$receiver_name,$total_km,$pickup_time,$delivery_time,$price,$cancel_reason,$live_tracking,$shipper_id);
        }

        $exec= $this->M_location->insertTrackingWebhook($orderno,$entity_id,$type,$status,$cancel,$booking_type,$driver_name,$driver_phone,$receiver_name,$total_km,$pickup_time,$delivery_time,$price,$cancel_reason,$live_tracking,$shipper_id);

        //call url tracking
        $dt1 =status_gsend($user_key, $pass_key,$entity_id);
        $result1= json_decode($dt1, TRUE);
        $url_track=$result1['liveTrackingUrl'];

        //update tabel url
        $this->salesM->updateShippaddr_url($orderno,$url_track);

       /* if($status == "no_driver"){

            $dt = cancel_gsend($user_key, $pass_key,$orderno);
            $result1= json_decode($dt, TRUE);
            if($result1 != ""){
                $entity_id="";
                $this->mm_trans->updateConotByOrderNo($orderno,$entity_id);

                $hp3="085717743438"; //nohp vera
                $txt = "No Driver & Cancel No Order:".$orderno;
                smsTemplate($hp3, $txt);

            }else{
                $this->mm_trans->updateConotByOrderNo($orderno,$entity_id);
            }
            

        }*/
        if($status == "no_driver"){
            //$hp3="085717743438"; //nohp vera
            $hp3="08568369705"; //nohp myna
            $txt = "Driver Not Found:".$orderno." Reason:".$cancel_reason;
            smsTemplate($hp3, $txt);

        }elseif($status == "on_hold"){
            //$hp3="085717743438"; //nohp vera
            $hp3="08568369705"; //nohp myna
            $txt = "On Hold Order:".$orderno." Reason:".$cancel_reason;
            smsTemplate($hp3, $txt);

        }elseif($status == "rejected"){
            //$hp3="085717743438"; //nohp vera
            $hp3="08568369705"; //nohp myna
            $txt = "Rejected / Retur Order:".$orderno." Reason:".$cancel_reason;
            smsTemplate($hp3, $txt);

        }else{
            $arr = array("status" =>401);
        }

        if($headers_auth != $auth){
            $arr = array("status" =>401);
        }

        if($exec){
            $arr = array("status" =>200);
        }else{
            $arr = array("status" =>401);
        }
        echo json_encode($arr);

    }

    public function sk_gosend(){
        $this->load->view('SK_Gosend/index');
    }

    public function getTracking(){
        $dt['tracking']="";
        $this->load->view('webshop/TrackingGosend',$dt);
    }

    public function getSalesTracking($orderno){

        $dts = $this->salesM->getProductOrder($orderno);
        if($dts != null){
            foreach($dts as $row){
                $service=$row->conoteJNE;
                $date=$row->datetrans;
                $jenis=$row->service_type_name;
            }
        }

        $arr = array("message" =>$service,"tglorder"=>$date,"jenis"=>$jenis);
        echo json_encode($arr);

    }

    public function getWebhook($orderno){

        $dts = $this->salesM->getTrackingWebhook($orderno);
        if($dts != null){
            foreach($dts as $row){

                $status[]=$row->status."+".$row->created;

            }
        }
        $arr = array("message" =>$status);
        echo json_encode($arr);

        //print_r($arr);

    }

    public function getRekening(){ //$route['getRek'] = 'c_location/getRek';
        $idbank= $this->input->post('bankid');

        $dt = $this->cek_rek($idbank);
        $result= json_decode($dt, TRUE);
        echo($result);
    }


    public function getUnique(){//$route['getUnique'] = 'c_location/getUnique';
        $nominal= $this->input->post('nominal');
        $norek= $this->input->post('no_rek_tujuan');
        $nmpemilik= $this->input->post('nama_rek_tujuan');
        $type= $this->input->post('type');
        $payship= $this->input->post('payShip');

        $dt = $this->cek_unik($nominal,$norek,$nmpemilik,$type,$payship);
        $result= json_decode($dt, TRUE);
        echo($result);
    }

    public function getKodePay(){ //$route['getKodePay'] = 'c_location/getKodePay';
        $token = $this->input->post('token');
        $nominal = $this->input->post('nominal');
        $payship = $this->input->post('pay_ship');
        $norek = $this->input->post('no_rek_tujuan');
        $nmrek = $this->input->post('nama_rek_tujuan');
        $type = $this->input->post('type');


        $dt = get_api_payment($token,$nominal,$payship,$norek,$nmrek,$type); //ada di dion helper
        echo($dt);
    }

    public function getValidate(){ //$route['getValidate'] = 'c_location/getValidate';
        $token = $this->input->post('token');
        $nominal = $this->input->post('nominal_belanja');
        $payship = $this->input->post('pay_ship');
        $kode_unik = $this->input->post('kode_unik');
        $kd_pay = $this->input->post('kd_pay');
        $total_bayar = $this->input->post('total_bayar');
        $norek = $this->input->post('no_rekening');
        $nmrek = $this->input->post('nama_rekening');
        $type = $this->input->post('type');


        $dt = get_api_validatepayment($token,$nominal,$payship,$kode_unik,$kd_pay,$total_bayar,$norek,$nmrek,$type); //ada di dion helper
        echo($dt);
    }

    public function getIpaymu(){ //$route['getIpaymu'] = 'c_location/getIpaymu';
        $token = $this->input->post('token');
        $product = $this->input->post('product');
        $quantity = $this->input->post('quantity');
        $price = $this->input->post('price');
        $weight = $this->input->post('weight');
        $dimension = $this->input->post('dimension');
        $postal_code_from = $this->input->post('postal_code_from');
        $name_to = $this->input->post('name_to');
        $email_to = $this->input->post('email_to');
        $phone_to = $this->input->post('phone_to');
        $address_to = $this->input->post('address_to');
        $provinsi_to = $this->input->post('provinsi_to');
        $kecamatan_to = $this->input->post('kecamatan_to');
        $kelurahan_to = $this->input->post('kelurahan_to');
        $postal_code_to = $this->input->post('postal_code_to');


        $dt = get_api_ipaymu($token,$product,$quantity,$price,$weight,$dimension,$postal_code_from,$name_to,$email_to,$phone_to,$address_to,$provinsi_to,$kecamatan_to,$kelurahan_to,$postal_code_to); //ada di dion helper
        echo($dt);
    }

    public function getDistanceKP(){ //$route['getDistanceKP'] = 'c_location/getDistanceKP';
        $kel= $this->input->post('kel');
        $kodepos1 = $this->input->post('kodepos1');
        $this->load->model("lokasi/M_location", "wh_model");
        $sql = $this->wh_model->getWH('master_warehouse_list');
        $array_wh=array();
        $array_dist=array();
        foreach($sql as $row) {
            $loccd = trim($row->loccd);
            $fullnm = trim($row->fullnm);
            $city = trim($row->city);
            $state= trim($row->state);
            $city = str_replace(" ", "+", $city);

            $postcd = trim($row->postcd);

            $dt = $this->get_api_distance_matrix($kel,$kodepos1,$city,$postcd,$loccd); //ada di My_Controller

            $json = json_decode($dt, true);
            $jrows=$json['rows'];

            //print_r($json['rows']);
            foreach($jrows as $i){

                $e=$i['elements'];

                foreach($e as $a){
                    /*echo "<pre>";
                        print_r($a['distance']['text']);
                    echo"</pre>";*/

                    $text_dist= $a['distance']['text'];
                    $dist= str_replace(",","",$text_dist);
                    $dist= str_replace(" mi","",$dist);
                    $dist_km= 2 * (int)$dist;
                    //$dist_km=number_format($dist_km);

                    /*if($dist_km <= 40){
                        $dist_km= $dist_km;
                    }else{
                        $dist_km=0;
                    }*/
                    $list[]=array($dist_km=>$loccd.'|'.$postcd.'|'.$fullnm.'|'.$dist_km."|".$state);
                    //array_push($array_dist,$dist_km);
                    /*if($dist_km <= 45){
                        array_push($array_dist,$dist_km);
                    }*/

                }
            }

        }

        foreach ($list as $key => $val) {
            foreach ($val as $k => $v) {
                $k= (int)$k;
                if($k > 40){
                    unset($val[$k]); //menghapus nilai > 40
                }else{
                    $namelist1[$k]=$v;
                    foreach ($namelist1 as $m => $n) {
                        array_push($array_dist,$n);
                    }
                }
            }
        }

        $n_dist=array();
        $array_unik=array_unique($array_dist);
        foreach ($array_unik as $a => $b) {
            array_push($n_dist,$b);
        }

        $arr = array("distance" => $n_dist);
        echo json_encode($arr);
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Muammar Qathafi
 */

class Api extends CI_Controller
{

    public function __construct()
    {
        # code...
        parent:: __construct();
        //  $this->load->controller('child_controller');
    }

    public function cek_mutasi(){
        $this->klink_apps->CheckAuth();
        $this->load->model('Api_model');
        header("Content-type:application/json");
        echo json_encode($this->Api_model->cek_mutasi());
    }
    public function cek_account(){
        $this->klink_apps->CheckAuth();
        $this->load->model('Api_model');
        header("Content-type:application/json");
        echo json_encode($this->Api_model->cek_account());
    }
    public function list_transaction(){
        if($this->input->get('token') != ''){
            if($this->klink_apps->CheckTokenApi()){
                $this->load->model('Api_model');
                header("Content-type:application/json");
                echo json_encode($this->Api_model->cek_mutasi());
            }
            //echo $this->klink_apps->CheckTokenApi();
        }else{
            $this->klink_apps->CheckAuth();
            $this->load->model('Api_model');
            header("Content-type:application/json");
            echo json_encode($this->Api_model->cek_mutasi());
        }
    }
    public function list_account(){
        if($this->input->get('token') != ''){
            if($this->klink_apps->CheckTokenApi()){
                $this->load->model('Api_model');
                header("Content-type:application/json");
                echo json_encode($this->Api_model->cek_account());
            }
        }else{
            $this->klink_apps->CheckAuth();
            $this->load->model('Api_model');
            header("Content-type:application/json");
            echo json_encode($this->Api_model->cek_account());
        }
        //print_r($this->klink_apps->CheckTokenApi());
    }

    public function callback($url, $data_array){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,//"https://{{your_url}}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data_array),//"{\r\n\t\"api_key\":\"UmRTOGQ1ckU3QUY4c3VidnBCaFYxbzI4djR5MEo2YVFud3B2NTNrUkpZRjFCSG76e115db7cdVZT3Z6MU51YmZhMnAy5b\",\r\n\t\"account_id\": 12,\r\n\t\"module\": \"bri\",\r\n\t\"account_name\": \"PT Amanah Karya Indonesia\",\r\n\t\"account_number\": null,\r\n\t\"balance\": 429090,\r\n\t\"data_mutasi\": [{\r\n\t\t\"transaction_date\": \"2018-08-10 00:00:00\",\r\n\t\t\"description\": \"TRANSFER EDC  MUHLISIN        TO PT AMANAH KARYA\",\r\n\t\t\"type\": \"CR\",\r\n\t\t\"amount\": 30097,\r\n\t\t\"balance\": 427099\r\n\t}, {\r\n\t\t\"transaction_date\": \"2018-08-10 00:00:00\",\r\n\t\t\"description\": \"TRANSFER EDC  JONI            TO PT AMANAH KARYA\",\r\n\t\t\"type\": \"DB\",\r\n\t\t\"amount\": 20002,\r\n\t\t\"balance\": 429099\r\n\t}]\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }


    // function write_mysql_log($message, $db)
    // {
    // 	  // Check database connection
    // 	  if( ($db instanceof MySQLi) == false) {
    // 	    return array(status => false, message => 'MySQL connection is invalid');
    // 	  }
    // 	 
    // 	  // Check message
    // 	  if($message == '') {
    // 	    return array(status => false, message => 'Message is empty');
    // 	  }
    // 	 
    // 	  // Get IP address
    // 	  if( ($remote_addr = $_SERVER['REMOTE_ADDR']) == '') {
    // 	    $remote_addr = "REMOTE_ADDR_UNKNOWN";
    // 	  }
    // 	 
    // 	  // Get requested script
    // 	  if( ($request_uri = $_SERVER['REQUEST_URI']) == '') {
    // 	    $request_uri = "REQUEST_URI_UNKNOWN";
    // 	  }
    // 	 
    // 	  // Escape values
    // 	  $message     = $db->escape_string($message);
    // 	  $remote_addr = $db->escape_string($remote_addr);
    // 	  $request_uri = $db->escape_string($request_uri);
    // 	 
    // 	  // Construct query
    // 	  $sql = "INSERT INTO my_log (remote_addr, request_uri, message) VALUES('$remote_addr', '$request_uri','$message')";
    // 	 
    // 	  // Execute query and save data
    // 	  $result = $db->query($sql);
    // 	 
    // 	  if($result) {
    // 	    return array(status => true);
    // 	  }
    // 	  else {
    // 	    return array(status => false, message => 'Unable to write to the database');
    // 	  }
    // }

    function request_order(){
        // format data json : /{"api_key":"UmRTOGQ1ckU3QUY4c3VidnBCaFYxbzI4djR5MEo2YVFud3B2NTNrUkpZRjFCSG76e115db7cdVZT3Z6MU51YmZhMnAy5b", "data_order":[{order_id:001,nominal:100000,kode_bank:014,no_rek:0700007269421,type:Cr},{order_id:002,nominal:200000,kode_bank:014,no_rek:0700007269421,type:Cr}]}

        $data = json_decode(file_get_contents('php://input'), true);
        file_put_contents('./result.html', json_encode($data));
        $api_token = "bWdYdnFZb0p6d2RHV0pybXBIcVJzUTJCWjk5dHJobzdrQjlGNXAxeENVek02bzRDUWVQdllpTE1iR0Qw5c624d281a00b";
        $token = $data['api_key'];
        if ($api_token != $token) {
            echo "invalid api token";
            exit;
        }else{
            $this->db->insert_batch('mutasi_proc_order', $data_insert);
        }
    }


    function request_dana_transfer(){
        // input data
        $data = json_decode(file_get_contents('php://input'), true);
        file_put_contents('./result.html', json_encode($data));
        $api_token = "bWdYdnFZb0p6d2RHV0pybXBIcVJzUTJCWjk5dHJobzdrQjlGNXAxeENVek02bzRDUWVQdllpTE1iR0Qw5c624d281a00b";
        $token = $data['api_key'];
        if ($api_token != $token) {
            echo "invalid api token";
            exit;
        }else{
            // $this->db->insert_batch('mutasi_proc_order', $data_insert);
            // check data available unique id MIN
            // mysql num rows
            $this->load->model('Pay_unique_model');

        }
    }

    public function api_payment(){
        $this->load->model('Api_model');
        // {no_order:001,nominal:100000,kode_bank:014,no_rek:0700007269421,type:Cr}
        $data = json_decode(file_get_contents('php://input'), true);

        //file_put_contents('./result.html', json_encode($data));
        $nominal = isset($data['nominal']) ? $data['nominal'] : '';
        $kode_unik = rand(0001,2000);
        $total_bayar = $nominal + $kode_unik;
        $kd_pay = rand(000001,100000);
        $kd_ref = rand(0000001,1000000);

        /*$kd_pay = $this->Api_model->kd_pay();
        $kd_ref =  $this->Api_model->kd_ref();*/
        $no_rek_tujuan = isset($data['no_rek_tujuan']) ? $data['no_rek_tujuan'] : '';
        $nama_rek_tujuan = isset($data['nama_rek_tujuan']) ? $data['nama_rek_tujuan'] : '';
        $type = isset($data['type']) ? $data['type'] : '';

       /* if($this->Api_model->save_payment($kd_pay, $kd_ref, $nominal, $no_rek_tujuan, $nama_rek_tujuan, $type, $total_bayar, $kode_unik)){
            $koneksi = true;
            $response = array('kd_pay'=>$kd_pay,'kd_ref'=>$kd_ref,'total_bayar'=>$total_bayar, 'kode_unik'=>$kode_unik, 'total_belanja'=>$nominal, 'koneksi'=>$koneksi);

        }else{
            $koneksi = false;
            $response = array('kd_pay'=>'','kd_ref'=>'','total_bayar'=>'', 'kode_unik'=>'', 'total_belanja'=>'', 'koneksi'=>false);
        }*/

        $koneksi = true;
        $response = array('kd_pay'=>$kd_pay,'kd_ref'=>$kd_ref,'total_bayar'=>$total_bayar, 'kode_unik'=>$kode_unik, 'total_belanja'=>$nominal, 'koneksi'=>$koneksi);
        //return json_encode($response);
        echo json_encode($response);
        //file_put_contents('./response.html', json_encode($response));
    }

    public function receive_no_order(){
        // {kd_pay:00345,no_order:001}
        $data = json_decode(file_get_contents('php://input'), true);
        $kd_pay = $data['kd_pay'];
        $no_order = $data['no_order'];
        if($this->Api_model->receive_no_order($kd_pay, $no_order)){
            $response = array('success' => true);
        }
        else{
            $response = array('success'=>false);
        }
        return json_encode($response);
    }
    // public function country(){
    // 	header("Content-type:application/json");
    // 	echo $this->Api_model->country();
    // }
    // public function data_santri(){
    // 	header("Content-type:application/json");
    // 	echo $this->Api_model->data_santri();
    // }
    // public function data_karyawan(){
    // 	header("Content-type:application/json");
    // 	echo $this->Api_model->data_karyawan_query();
    // }
    // public function get_nomor_induk(){
    // 	header("Content-type:application/json");
    // 	echo $this->Api_model->get_nomor_induk();
    // }
    // public function get_keterangan(){
    // 	header("Content-type:application/json");
    // 	echo $this->Api_model->get_keterangan();
    // }
    // public function update_status_fix(){
    // 	echo $this->Api_model->update_status_fix();
    // }
}

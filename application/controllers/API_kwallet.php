<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API_kwallet extends MY_Controller {

	public function __construct() {
		// Call the Model constructor
        parent::__construct();
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		
		$this->usr_api = "k-net.apps";
		$this->pwd_api = "apps@2017";
		//$this->salt = "knet";
		$this->dataForm = $this->input->post(NULL, TRUE);
	}
	
	function checkAuthAPI($data) {
		$return = false;
		if(array_key_exists("api_usr", $data) && array_key_exists("api_pwd", $data)) {
			if($data['api_usr'] == $this->usr_api && $data['api_pwd'] == $this->pwd_api) {
				$return = true;
			} 
		} 
		return $return;
	}
	
	//$route['api/member/membRegister/kwallet'] = 'mobile_api/kwalletRegMember';
	public function kwalletRegMember() {
		if(!$this->checkAuthAPI($this->dataForm)) {
			echo json_encode($this->apiAuthResponse(false));
		} else {
			$idmember = $data['userlogin'];
			$amount = (double) $data['pay_amount'];
			$this -> load -> model("backend/be_hifi_model", "hifi");
			$saldo = $this->hifi->getSaldoVa($idmember);
			//print_r($saldo);
			if($saldo != null) {
				$sisaSaldo = (int)$saldo[0]->amount;
				//print_r($sisaSaldo);
				if ($amount <= $sisaSaldo) {
				
				} else {
					$resJson = jsonFalseResponse("Saldo VA anda tidak cukup : " . number_format($sisaSaldo, 0, ",", ".") . ", total pembayaran : " . number_format($amount, 0, ",", "."));
				}
			} else {
				$resJson = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
			}
			
			echo json_encode($resJson);
		}
	}
	
	public function kwalletSales() {
		if(!$this->checkAuthAPI($this->dataForm)) {
			echo json_encode($this->apiAuthResponse(false));
		} else {
			$idmember = $data['userlogin'];
			$amount = (double) $data['pay_amount'];
			$this -> load -> model("backend/be_hifi_model", "hifi");
			$saldo = $this->hifi->getSaldoVa($idmember);
			//print_r($saldo);
			if($saldo != null) {
				$sisaSaldo = (int)$saldo[0]->amount;
				//print_r($sisaSaldo);
				if ($amount <= $sisaSaldo) {
					//orderid dikirim oleh web k-net baru
					if(!array_key_exists("orderid", $data)) {
						echo json_encode($this->apiAuthResponse(false));
						return;
					}
					
					$orderid = $data['orderid'];
					
					$res = $this->saveTempData($data, $orderid);
					if($res['response'] == "true") {
						$this->load->service("webshop/Knet_payment_service",'paymentService');
						$arrayData = $res['arrayData'][0];
						//$xx['order_id'] = $arrayData->orderno;
						$xx['order_id'] = $orderid;
						$double = $this->paymentService->checkDoubleTokenTransHdr($xx['order_id']);
						if($double == null) {
							$this->load->model("webshop/Payment_model",'pmodel');
							//$updtTabel = $this->paymentService->updateTrxSGO_baru($xx['order_id']);
							$updtTabel = $this->paymentService->update_trx_knetbaru($xx['order_id'], $data['noresi']);
							//harus ada tambahan utk insert ke ecomm-trans-ship-addr
							
							$resultInsert = $this->paymentService->getInsertTrxOrderID($xx['order_id']);
							
							if($resultInsert != null) {
								$this->paymentService->sendTrxSMS2($resultInsert);
								sendNotifSalesTransaction($resultInsert);
								
								//potong saldo k-wallet
								$dbqryx = $this->load->database("db_ecommerce", TRUE);
								
								//QUERY FOR CHECK DOUBLE TRANSACTION ID
								
								$arr = array(
									"trcd" => $resultInsert[0]->orderno,
									//TRS = Transaksi K-NET Sales
									"trtype" => "TRS",
									"effect" => "-",
									"effect_acc" => "+",
									"custtype" => "M",
									"novac" => $saldo[0]->novac,
									//"dfno"    => $header['memberid'],
									"dfno" => $idmember,
									"docno" => "",
									"refno" => "",
									"amount" => $amount,
									"createnm" => $idmember,
									"tipe_dk" => "D",
									"tipe_dk_acc" => "K"
								);
								$dbqryx->insert('va_cust_pay_det', $arr);
								
								$resJson = jsonTrueResponse($resultInsert, "Transaksi $xx[order_id] berhasil di simpan..");
							} else {
								$resJson = jsonFalseResponse("Trx $xx[order_id] gagal disimpan..");
							}
							
							
						} else {
							$resJson = jsonFalseResponse("Gagal, Trx $xx[order_id] sudah ada di database..");
						}
					} else {
						$resJson = $res;
					}
				} else {
					$resJson = jsonFalseResponse("Saldo VA anda tidak cukup : " . number_format($sisaSaldo, 0, ",", ".") . ", total pembayaran : " . number_format($amount, 0, ",", "."));
				}	
			} else {
				$resJson = jsonFalseResponse("Saldo tidak cukup atau belum ada transaksi top up saldo..");
			}
			
			echo json_encode($resJson);
		}
	}
}	
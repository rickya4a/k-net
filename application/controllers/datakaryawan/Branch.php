<?php

class branch extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('M_branch');
        $this->load->database();

    }

    function index(){
        $r= $this->M_branch->getBranch('kantorcabang');
        $data= array('data' => $r);

        $data['title']="DATA KANTOR CABANG";
        $data['content']='master/kantorcabang/listBranch';

        $this->load->view('index', $data);
    }

    function add(){
        $data['title']="TAMBAH DATA KANTOR CABANG";
        $data['content']='master/kantorcabang/addBranch';

        $this->load->view('index', $data);
    }

    function insert(){

        $data = array(
            'kodekantor' => $this->input->post('ckode'),
            'namakantor' => $this->input->post('cnama'),
        );
        //print_r($data);

        $data = $this->M_branch->Insert('kantorcabang', $data);

        redirect(base_url()."branch");

    }

    function delete($id){
        $id = array('idkantor' => $id);
        $this->load->model('M_branch');
        $this->M_branch->Delete('kantorcabang', $id);
        redirect(base_url()."branch", 'refresh');
    }

    function edit($id){
        $branch = $this->M_branch->GetWhere('kantorcabang', array('idkantor' => $id));
        $data = array(
            'id' => $branch[0]['idkantor'],
            'kode' => $branch[0]['kodekantor'],
            'nama' => $branch[0]['namakantor']
        );

        $data['title']="EDIT DATA KANTOR CABANG";
        $data['content']='master/kantorcabang/editBranch';

        $this->load->view('index', $data);
    }

    public function update(){
        $id = $_POST['cid'];
        $kode = $_POST['ckode'];
        $nama = $_POST['cnama'];
        $data = array(
            'kodekantor' => $kode,
            'namakantor' => $nama,
        );
        $where = array(
            'idkantor' => $id
        );

        $res = $this->M_branch->Update('kantorcabang', $data, $where);

        //print_r($res);

        if ($res>0) {
            redirect(base_url()."branch");
        }
    }

}
?>
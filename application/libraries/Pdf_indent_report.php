<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_indent_report extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$data_kwit=array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->kwit = $data_kwit;
			$this->as = $as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'INDENT REPORT' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(5);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Stockies' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".(isset($mst->NAMA_STOCKIES) ? $mst->NAMA_STOCKIES : $mst->NAMA) , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(350);$this->Cell(0, $h, 'Alamat :' , 0, 0,'L',false );
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->MultiCell(155, $h,''.(isset($mst->ALAMAT_STOCKIES) ? $mst->ALAMAT_STOCKIES : $mst->ALAMAT1)  , 0);
		$this->Ln(12);


		$this->Ln(30);

		$this->HeaderTable();
	//	$this->indent();
	//	$this->bawah();


	}
	function BasicTable()
	{
		$table= $this->data;
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			$this->Cell(80,20,'No',1,0,'C');
			$this->Cell(80,20,'No Kwitansi',1,0,'C');
			$this->Cell(80,20,'Product Code',1,0,'C');
			$this->Cell(170,20,'Product Name',1,0,'C');
			$this->Cell(50,20,'Qty',1,0,'C');
			//$this->Cell(50,20,'Qty Ship',1,0,'C');
			//$this->Cell(50,20,'B/O',1,0,'C');

			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$terkirim=0;$sisa=0;
			foreach($table as $row) {
				$i++;
				$this->Cell(80,20,$i,1);
				$this->Cell(80,20,$row->KWITANSI_NO,1);
				$this->Cell(80,20,$row->PRODUK_ALIAS_ID,1);
				$this->Cell(170,20,$row->ALIAS_NAME,1);
				$this->Cell(50,20,$row->QTY_SISA,1,0,'R');

				$this->Ln();
				$total=$total+$row->QTY_SISA;

			}



			// Data



			$this->Cell(410,20,'Sub Total',1);
			$this->Cell(50,20,$total,1,0,'R');

		}
		$this->Ln(50);


	}

	function indent()
	{

		$indent = $this->indent;

		if($indent!=null){

		$this->SetFont("", "B", 10);
		$this->Cell(20,13,'Indent : ' ,0,0,'L' ,false);
		$this->Ln();

		// Header
		$this->Cell(80,20,'No',1);
		$this->Cell(80,20,'Product',1);
		$this->Cell(170,20,'Product Name',1);
		$this->Cell(50,20,'Qty',1);


		$this->Ln();
		$this->SetFont("", "", 10);
		$i=0;$total=0;
		foreach($indent as $row) {
			$i++;
			$this->Cell(80,20,$i,1);
			$this->Cell(80,20,$row->PRODUK_ALIAS_ID,1);
			$this->Cell(170,20,$row->ALIAS_NAME,1);
			$this->Cell(50,20,$row->total,1,0,'R');
			$total=$total+$row->total;
			$this->Ln();
		}

		// Data

		$this->Cell(330,20,'Sub Total',1);
		$this->Cell(50,20,$total,1,0,'R');
		$this->Ln(30);
		}
	}


	function bawah()
	{
		$kwit = $this->kwit;
		$mst = $this->mst;

		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(70);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, '1....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4....................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Ln();

		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
//		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Ln();
		$this->Ln();
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Ln();
		$this->Cell(20, 13, 'DO Parent ', 0, 0, 'L', false);
		$this->SetX(150); $this->Cell(150, 13, ": ".$mst->NO_DO , 0, 0, 'L' ,false);

		$this->Ln();
		$this->Cell(20, 13, 'Receipt Number ', 0, 0, 'L', false);
		$this->SetFont("", "B", 10);
		$this->SetX(150);
		$this->Cell(5, 13, ": ", 0, 0, 'L' ,false);
		foreach($kwit as $row) {

			$this->Cell(0, 13,(isset($row->NO_KWITANSI) ? $row->NO_KWITANSI : $row->ID_KWITANSI).", " , 0, 0, 'L' ,false);

		}
		$this->Ln();

	}

	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(80,26,'No' ,1,0,'C' );
		$this->SetXY($left += 80,$Z); $this->Cell(80, 26, 'No Kwintasi' , 1, 0, 'C');
		$this->SetXY($left += 80,$Z); $this->Cell(105, 26, 'Product Code' , 1, 0, 'C');
		$this->SetXY($left += 105,$Z); $this->Cell(150, 26, 'Product Name' , 1, 0, 'C' );
		$this->SetXY($left += 150,$Z); $this->MultiCell(50, 26, 'Qty' , 1 );



	}

	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(80,80,105,150,50));
		$this->SetAligns(array('C','C','C','C','R'));
		$no = 1;

		foreach ($this->data as $row) {
			$this->Row(
				array($no++,
					$row['KWITANSI_NO'],
					$row['PRODUK_ALIAS_ID'],
					$row['ALIAS_NAME'],

					number_format($row['QTY_SISA']) ,



				), 15, 15
			);
			$awal=$awal+($row['QTY_SISA']);

		}
		$this->SetFont("", "", 10);
		$this->Cell(415,26,'Total',1);
//
//		$this->Cell(80,20,$i,1);
//		$this->Cell(80,20,$row->KWITANSI_NO,1);
//		$this->Cell(80,20,$row->PRODUK_ALIAS_ID,1);
//
//		$this->Cell(170,20,$row->ALIAS_NAME,1);
//		$this->Cell(50,20,$row->QTY_SISA,1,0,'R');
//
//		$this->Ln();
//		$total=$total+$row->QTY_SISA;

		$this->Cell(50,26,number_format($awal),1,0,'R');


		$this->Ln();
	}



	public function Footer() {
		$mst = $this->mst;

		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');


		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont("Courier", "", 10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}

}
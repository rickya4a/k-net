<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_do_closing2 extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'Letter' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$tgl_awal='',$tgl_akhir='', $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->tgl_awal=$tgl_awal;
			$this->tgl_akhir=$tgl_akhir;
			$this->as=$as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'CLOSING STOCK REPORT' , 0, 1,'C' );
		$this->Ln(30);


		$this->HeaderMst();
	}


	function bawah()
	{

		$this->SetFont("", "B", 10);

		$this->Ln();



		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(175, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
	}
	public function HeaderMst()
	{

		$mst = $this->mst;
		$awal=$this->tgl_awal;
		$akhir=$this->tgl_akhir;
		$data=$this->data;
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(30);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Tanggal' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(100, $h, ": ".$awal." s/d ".$akhir , 0, 0, 'L' ,false);
		$this->ln(15);

		//$this->bawah();
		if($data!=null)
		$this->HeaderTable();
		//$this->Ln();

	}


	function BasicTable()
	{

		$table= $this->data;
		//print_r($table);
		if($table!=null){
			$this->SetFont("", "B", 8);

			// Header
			$this->Cell(20,20,'No',1,0,'C');
			$this->Cell(170,20,'Product Name',1,0,'C');
			$this->Cell(50,20,'Awal',1,0,'C');
			$this->Cell(50,20,'In',1,0,'C');
			$this->Cell(50,20,'Out',1,0,'C');
			$this->Cell(100,20,'Ending Balance',1,0,'C');


			$this->Ln();
			$this->SetFont("", "", 8);

			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
				foreach($table as $row) {

				$sat=' pcs';

				$i++;
				$this->Cell(20,20,$i,1);
				$this->Cell(170,20,$row->PRODUCT_NAME,1);
				$this->Cell(50,20,number_format($row->AWAL),1,0,'R');
				$this->Cell(50,20,number_format($row->MASUK),1,0,'R');
				$this->Cell(50,20,number_format($row->KELUAR),1,0,'R');
				$this->Cell(100,20,number_format($row->TOTAL),1,0,'R');


				$this->Ln();
				$masuk=$masuk+($row->MASUK);
				$awal=$awal+($row->AWAL);

				$KELUAR=$KELUAR+($row->KELUAR);
				$total=$total+($row->TOTAL);
			//	$sisa=$sisa+$row->SISA;
			}



			// Data



			$this->Cell(190,20,'Total',1);
			$this->Cell(50,20,number_format($awal),1,0,'R');
			$this->Cell(50,20,number_format($masuk),1,0,'R');
			$this->Cell(50,20,number_format($KELUAR),1,0,'R');

			$this->Cell(100,20,number_format($total),1,0,'R');

		}
		$this->Ln();


	}

	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(20,26,'No' ,1,0,'C' );
		$this->SetXY($left += 20,$Z); $this->Cell(170, 26, 'Product Name' , 1, 0, 'C');
		$this->SetXY($left += 170,$Z); $this->Cell(90, 26, 'Awal' , 1, 0, 'C' );
		$this->SetXY($left += 90,$Z); $this->Cell(90, 26, 'In' , 1, 0, 'C' );
		$this->SetXY($left += 90,$Z); $this->Cell(90, 26, 'Out' , 1, 0, 'C' );
		$this->SetXY($left += 90,$Z); $this->MultiCell(90, $h, 'Ending Balance (Pcs)' , 1);

	}


	function rptDetailData()
	{

		if($this->data!=null){
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true,60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table= $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
			$this->SetFont('' ,'' ,10);
			$this->SetWidths(array(20,170,90,90,90,90));
			$this->SetAligns(array('C','C','R','R','R','R'));
			$no = 1;

			foreach ($this->data as $row) {
				$sat=' pcs';

				$this->Row(
					array($no++,
						$row['PRODUCT_NAME'],
						number_format($row['AWAL']) ,
						number_format($row['MASUK']) ,
						number_format($row['KELUAR']),
						number_format($row['TOTAL']) ,

					), 15, 15
				);
				$masuk=$masuk+($row['MASUK']);
				$awal=$awal+($row['AWAL']);
				$KELUAR=$KELUAR+($row['KELUAR']);
				$total=$total+($row['TOTAL']);
			}
			$this->SetFont("", "", 10);
			$this->Cell(460,26,'Ending Balance',1);

			$this->Cell(90,26,number_format($total),1,0,'R');


			$this->Ln();
			$this->bawah();

		}

	}



	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont("Courier", "", 10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
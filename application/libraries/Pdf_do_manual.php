<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_do_manual extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->as = $as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'REPORT DO MANUAL' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(5);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Do Number' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->NO_MANUAL_DO , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(350);$this->Cell(0, $h, 'To' , 0, 0,'L',false );
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h,': '.$mst->CODE_STOCKIES , 0, 0, 'L' ,false);
		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Create Date' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->TANGGAL_DO , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h,'  '.$mst->NAMA_STOCKIES , 0, 0, 'L' ,false);


		$this->Ln(12);



		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Warehouse' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->WAREHOUSE_NAME , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h,' ' , 0, 0, 'L' ,false);
		$this->Ln(12);
		$this->Ln();




		$this->HeaderTable();
		//$this->indent();



	}
	function BasicTable()
	{
		$this->SetFont("", "B", 10);

		// Header
		$this->Cell(80,20,'No',1);
		$this->Cell(80,20,'Product',1);
		$this->Cell(270,20,'Product Name',1);
		$this->Cell(50,20,'Qty',1);

		$this->Ln();
		$this->SetFont("", "", 10);

		$table= $this->data;
		$i=0;$total=0;$terkirim=0;$sisa=0;
		foreach($table as $row) {
			$i++;
			$this->Cell(80,20,$i,1);
			$this->Cell(80,20,$row->ID_PRODUCT,1);
			$this->Cell(270,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->total,1,0,'R');

			$this->Ln();
			$total=$total+$row->total;

		}



		// Data



		$this->Cell(430,20,'Sub Total',1);
		$this->Cell(50,20,$total,1,0,'R');

		$this->Ln();

		$this->Ln();
		$mst = $this->mst;
		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Remarks :'.$mst->REMARK, 0, 0, 'L', false);
		$this->Ln(70);

	}

	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(20,26,'No' ,1,0,'C' );
		$this->SetXY($left += 20,$Z); $this->Cell(80, 26, 'Product' , 1, 0, 'C');
		$this->SetXY($left += 80,$Z); $this->Cell(170, 26, 'Product Name' , 1, 0, 'C');
		$this->SetXY($left += 170,$Z); $this->MultiCell(50, 26, 'Qty' , 1);



	}

	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(20,80,170,50));
		$this->SetAligns(array('C','C','C','R'));
		$no = 1;

		$terkirim=0;
		foreach ($this->data as $row) {
			$i++;


			$this->Row(
				array($no++,
					$row['ID_PRODUCT'],
					$row['PRODUCT_NAME'],
					number_format($row['total'])

				), 15, 15
			);
			$total=$total+$row['total'];
		}
		$this->SetFont("", "", 10);
		$this->Cell(270,26,'Sub Total',1);


		$this->Cell(50,26,number_format($total),1,0,'R');


		$this->Ln();
		$this->bawah();
	}

	function indent()
	{

		$indent = $this->indent;


		$this->SetFont("", "B", 10);
		$this->Cell(20,13,'Indent : ' ,0,0,'L' ,false);
		$this->Ln();

		// Header
		$this->Cell(80,20,'No',1);
		$this->Cell(80,20,'Product',1);
		$this->Cell(170,20,'Product Name',1);
		$this->Cell(50,20,'Qty',1);


		$this->Ln();
		$this->SetFont("", "", 10);
		$i=0;$total=0;
		foreach($indent as $row) {
			$i++;
			$this->Cell(80,20,$i,1);
			$this->Cell(80,20,$row->ID_PRODUCT,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->total,1);
			$total=$total+$row->total;
			$this->Ln();
		}

		// Data

		$this->Cell(330,20,'Sub Total',1);
		$this->Cell(50,20,$total,1);
		$this->Ln(30);

	}


	function bawah()
	{
//		$kwit = $this->kwit;
//		$mst = $this->mst;
		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, '1.................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2.................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3.................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4.................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Ln();


		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
//		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Ln();
		$this->Ln();
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Ln();

//		$this->Ln();
//		$this->Cell(20, 13, 'Receipt Number ', 0, 0, 'L', false);
//		$this->SetFont("", "B", 10);
//		$this->SetX(150);
//		$this->Cell(5, 13, ":  ", 0, 0, 'L' ,false);
//		foreach($kwit as $row) {
//
//			$this->Cell(0, 13," ".(isset($row->NO_KWITANSI) ? $row->NO_KWITANSI : $row->ID_KWITANSI)."," , 0, 0, 'L' ,false);
//
//		}
		$this->Ln();

	}



	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-35);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');


		$this->SetY(-25);
		$this->SetFont('Courier','I',10);

		$this->Cell(0,10,$mst->NO_MANUAL_DO,0,0,'L');

		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
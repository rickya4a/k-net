<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    Class My_counter
    {
		function __construct()
		{
		}

    	function Counter($DOC)
		{
			$CI =&get_instance();

			$this->db = $CI->load->database('db_ecommerce',TRUE);

			$query = 'SELECT VALUE FROM MASTER_COUNTER WHERE DOC_CODE = ?';
			$result = $this->db->query($query,array($DOC));
			$row = $result->row();
			
			$row->VALUE;
			
			$no = $DOC.'-'.str_pad($row->VALUE,5,"0000000",STR_PAD_LEFT);
			
			return $no;
		}
			
    	function getCounter($DOC)
		{
			$CI =&get_instance();

			$this->db = $CI->load->database('db_ecommerce',TRUE);

			$query = 'SELECT VALUE FROM MASTER_COUNTER WHERE DOC_CODE = ?';
			$result = $this->db->query($query,array($DOC));
			$row = $result->row();
			
			$row->VALUE;
			
			$no = $DOC.'-'.str_pad($row->VALUE,5,"0000000",STR_PAD_LEFT);
			
			$value = $row->VALUE + 1;
			
			$this->db->where(array('DOC_CODE'=>$DOC));
			$this->db->update('MASTER_COUNTER', array('VALUE'=>$value));
			
			return $no;
		}
		
		function Counter2($DOC)
		{
			$CI =&get_instance();
			
			$waktu = date('y').date('m').date('d');
			
			$this->db = $CI->load->database('db_ecommerce',TRUE);
			
			$this->db->where(array('DOC_CODE'=>$DOC,'DATE'=>date('d'),'MONTH'=>date('m'),'YEAR'=>date('y')));
			$count = $this->db->count_all_results('MASTER_COUNTER');
			
			if($count == 0) {
				$value = 1;
				$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
			}else{
				$query = 'SELECT VALUE FROM MASTER_COUNTER WHERE DOC_CODE = ? AND DATE = ? AND MONTH = ? and YEAR = ?';
				$result = $this->db->query($query,array($DOC,date('d'),date('m'),date('y')));
				$row = $result->row();
				
				$value = $row->VALUE+1;
				
				$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
			}
			return $no;
		}
		
		function getCounter2($DOC,$uuid = '')
		{
			$CI =&get_instance();
			$waktu = date('y').date('m').date('d');
			
			$this->db = $CI->load->database('db_ecommerce',TRUE);
			$this->db->where(array('DOC_CODE'=>$DOC,'DATE'=>date('d'),'MONTH'=>date('m'),'YEAR'=>date('y')));
			$count = $this->db->count_all_results('MASTER_COUNTER');
			if($count == 0) {
				$value = 1;
				$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
			
				$this->db->insert('MASTER_COUNTER',array('ID_COUNTER' =>$uuid,'VALUE'=>$value,'DOC_CODE'=>$DOC,'DATE'=>date('d') ,'MONTH'=>date('m'),'year'=>date('y'),'CRT_NM'=>'SYSTEM'));
			}else{
				$query = 'SELECT VALUE FROM MASTER_COUNTER WHERE DOC_CODE = ? AND DATE= ? AND MONTH = ? AND YEAR = ?';
				$result = $this->db->query($query,array($DOC,date('d'),date('m'),date('y')));
				$row = $result->row();


				$value = $row->VALUE+1;
				
				$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
				
				$this->db->where(array('DOC_CODE'=>$DOC,'DATE'=>date('d'),'MONTH'=>date('m'),'year'=>date('y')));
				$this->db->update('MASTER_COUNTER', array('VALUE'=>$value));
			}

			return $no;
		}


		function Counter3($DOC, $YEAR, $MONTH)
		{
			$CI =&get_instance();

//			$waktu = date('y').date('m').date('d');

			$this->db = $CI->load->database('db_ecommerce',TRUE);

			$this->db->where(array('DOC_CODE'=>$DOC,'DATE'=>'-','MONTH'=>'-','YEAR'=>$YEAR));
			$count = $this->db->count_all_results('MASTER_COUNTER');

			if($count == 0) {
				$value = 1;
				$no = str_pad($value,3,"00",STR_PAD_LEFT)."/K-link/".$DOC."/".$MONTH."/".$YEAR;
			}else{
				$query = 'SELECT VALUE FROM MASTER_COUNTER WHERE DOC_CODE = ? AND DATE= ? AND MONTH = ? AND YEAR = ?';
				$result = $this->db->query($query,array($DOC,'-','-',$YEAR));
				$row = $result->row();

				$value = $row->VALUE+1;

				$no = str_pad($value,3,"00",STR_PAD_LEFT)."/K-link/".$DOC."/".$MONTH."/".$YEAR;
			}
			return $no;
		}

		function getCounter3($DOC,$uuid = '',$YEAR, $MONTH)
		{
			$CI =&get_instance();
//			$waktu = date('y').date('m').date('d');

			$this->db = $CI->load->database('db_ecommerce',TRUE);
			$this->db->where(array('DOC_CODE'=>$DOC,'DATE'=>'-','MONTH'=>'-','YEAR'=>$YEAR));
			$count = $this->db->count_all_results('MASTER_COUNTER');
			if($count == 0) {
				$value = 1;
				$no =str_pad($value,3,"00",STR_PAD_LEFT)."/K-link/".$DOC."/".$MONTH."/".$YEAR;

				$this->db->insert('MASTER_COUNTER',array('ID_COUNTER' =>$uuid,'VALUE'=>$value,'DOC_CODE'=>$DOC,'DATE'=>'-' ,'MONTH'=>'-','year'=>$YEAR,'CRT_NM'=>'SYSTEM'));
			}else{
				$query = 'SELECT VALUE FROM MASTER_COUNTER WHERE DOC_CODE = ? AND DATE= ? AND MONTH = ? AND YEAR = ?';
				$result = $this->db->query($query,array($DOC,'-','-',$YEAR));
				$row = $result->row();


				$value = $row->VALUE+1;

				$no = str_pad($value,3,"00",STR_PAD_LEFT)."/K-link/".$DOC."/".$MONTH."/".$YEAR;

				$this->db->where(array('DOC_CODE'=>$DOC,'DATE'=>'-','MONTH'=>'-','year'=>$YEAR));
				$this->db->update('MASTER_COUNTER', array('VALUE'=>$value));
			}

			return $no;
		}

		function counterByFendi(){

			$CI =& get_instance();

			$this->db = $CI->load->database('db_ecommerce',TRUE);
			$count = $this->db->count_all_results('fendi_tes_listvoucher');

			if($count < 20000){

				$no = $this->getCounter2('FV', $this->v4());
				$status = 'Released';

				$dt = array(
						'vch_no' => $no,
						'status' => $status
				);
				$this->db->insert('fendi_tes_listvoucher', $dt);

			}else{

				$no = false;
			}

			return $no;

		}


		public function v4($trim = false)
		{

			$format = ($trim == false) ? '%04x%04x-%04x-%04x-%04x-%04x%04x%04x' : '%04x%04x%04x%04x%04x%04x%04x%04x';

			$uuid = sprintf($format,

				// 32 bits for "time_low"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),

				// 16 bits for "time_mid"
				mt_rand(0, 0xffff),

				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand(0, 0x0fff) | 0x4000,

				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand(0, 0x3fff) | 0x8000,

				// 48 bits for "node"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
			);

			return str_replace('-','',substr($uuid, 0,13));
		}


	}
?>

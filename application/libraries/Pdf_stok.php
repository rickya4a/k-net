<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_stok extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$tgl_awal='',$tgl_akhir='', $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->tgl_awal=$tgl_awal;
			$this->tgl_akhir=$tgl_akhir;
			$this->as=$as;
	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'REPORT INVENTORY' , 0, 1,'C' );
		$this->Ln(30);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		$awal=$this->tgl_awal;
		$akhir=$this->tgl_akhir;
		if($akhir==0){
			$akhir='Rack';
		}else
			$akhir='Location';

		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(30);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Warehouse' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(100, $h, ": ".$awal , 0, 0, 'L' ,false);
		$this->ln();
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Category' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(100, $h, ": ".$akhir , 0, 0, 'L' ,false);
		$this->ln();
		$this->HeaderTable();

		//$this->bawah();


	}
	function BasicTable()
	{

		$table= $this->data;
		//print_r($table);
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			//$this->Cell(20,20,'No',1);
			$this->Cell(70,20,'Rack/Location',1);
			$this->Cell(170,20,'Product Name',1);
			$this->Cell(50,20,'Max',1);
			$this->Cell(50,20,'Min',1);
			$this->Cell(50,20,'Qty',1);


			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
			foreach($table as $row) {


				$i++;
			//	$this->Cell(20,20,$i,1);
				$this->Cell(70,20,$row->RACK_NAME,1);
				$this->Cell(170,20,$row->PRODUCT_NAME,1);
				$this->Cell(50,20,number_format($row->MAX),1,0,'R');
				$this->Cell(50,20,number_format($row->MIN),1,0,'R');
				$this->Cell(50,20,number_format($row->QTY),1,0,'R');
				$this->Ln();
			}
		}
		$this->Ln();


	}



	function bawah()
	{
		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(70);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, '1....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4....................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'DO Parent: ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'Receipt Number: ', 0, 0, 'L', false);
		$this->Ln();

	}


	public function HeaderTable()
	{
		if($this->data!=null) {

			$h = 13;
			$left = 10;
			$top = 80;
			$Z = $this->getY();
			#tableheader
			$this->SetFont("", "B", 10);
			//$this->SetFillColor(200,200,200);
			$left = $this->GetX();


			$this->Cell(20, 26, 'No', 1, 0, 'C');
			$this->SetXY($left += 20, $Z);
			$this->MultiCell(60, 13, 'Rack/ Location', 1);
			$this->SetXY($left += 60, $Z);
			$this->Cell(130, 26, 'Product Name', 1, 0, 'C');
			$this->SetXY($left += 130, $Z);
			$this->Cell(50, 26, 'Max', 1, 0, 'C');
			$this->SetXY($left += 50, $Z);
			$this->Cell(50, 26, 'Min', 1, 0, 'C');
			$this->SetXY($left += 50, $Z);
			$this->MultiCell(50, 26, '	Qty', 1);
			$this->SetXY($left += 50, $Z);
			$this->MultiCell(90, 26, '	Exp Pabrik', 1);
			$this->SetXY($left += 90, $Z);
			$this->MultiCell(90, 26, '	Exp Gudang', 1);
		}
	}

	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$total2 = 0;
			$masuk = 0;
			$awal = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(20, 60, 130, 50, 50, 50,90,90));
			$this->SetAligns(array('C', 'C', 'C', 'R', 'R', 'R','R','R'));
			$no = 1;


			foreach ($this->data as $row) {


				$this->Row(
					array(
						$no++,
						$row['RACK_NAME'],
						$row['PRODUCT_NAME'],
						number_format($row['MAX']),
						number_format($row['MIN']),
						number_format($row['QTY']),
						$row['EXP_PABRIK'],
						$row['EXP_GUDANG'],
					), 15, 15
				);
				//$total=$total+$row['QTY'];
				//$total2=$total2+($row['QTY']*$BOX);
				//$awal=$awal+($row->QTY*$BOX);

			}

			$this->Ln();
		}
		//$this->bawah();
	}

	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
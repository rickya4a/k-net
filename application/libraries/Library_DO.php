<?php

/**
 * Created by PhpStorm.
 * User: Rinaldhi Cahyono
 * Date: 3/8/2017
 * Time: 17:20
 */
class library_DO
{




    function jarak($origin, $produk,$berat, $qty,$uuidz){


        $satuan= $berat/$qty;
        $CI =&get_instance();
        $CI2 =&get_instance();
        $this->db = $CI->load->database('default',TRUE);
        $CI2->load->library('uuid');
        $query2 ='SELECT LATITUDE, LONGITUDE, KODE_JNE from r_kota where ID_KOTA = ?;';
        $result2 = $this->db->query($query2,array($origin));

        $row2 = $result2->row();

        $lat= $row2->LATITUDE;
        $lng= $row2->LONGITUDE;
        $kode_jne=$row2->KODE_JNE;
        $query = '
        SELECT a.ID_SUPPLIER, a.NAMA_SUPPLIER, a.ALAMAT, g.NAMA_KOTA, g.ID_KOTA, g.KODE_KOTA, g.KODE_JNE,hey.distance, z.QTY , p.ID_PRODUK from r_supplier a
        LEFT JOIN inventory z ON a.ID_SUPPLIER = z.ID_SUPPLIER
        LEFT JOIN r_produk p ON z.ID_PRODUK = p.ID_PRODUK
        LEFT JOIN r_kota g ON a.ID_KOTA = g.ID_KOTA
        LEFT JOIN(
        SELECT
          ID_KOTA,(
            6371 * acos (
              cos ( radians(?/*input latitude*/) )
              * cos( radians( LATITUDE ) )
              * cos( radians( LONGITUDE ) - radians(?/* input longitude*/) )
              + sin ( radians(?/*input latitude*/) )
              * sin( radians( LATITUDE ) )
            )
          ) AS distance
        FROM r_kota
        where LATITUDE IS NOT NULL
        ORDER BY distance
        LIMIT 0 , 20
        ) hey
        on a.ID_KOTA = hey.ID_KOTA
        where hey.distance is not null AND z.ID_PRODUK = ? AND z.QTY >0/*input kode produk*/

        ORDER BY distance
        ';
        $sisa = $qty;
        $result = $this->db->query($query,array($lat,$lng,$lat,$produk));
        //echo $this->db->last_query();
        //var_dump($result);
        //	$uuid=$CI2->uuid->v4();
        foreach($result->result() as $row) {

            if ($row->QTY == $sisa) {
                $qty_p = $sisa;
            } else {
                if ($row->QTY > $sisa)
                    $qty_p = $sisa;
                else
                    $qty_p = $row->QTY;
            }
            $sisa = $sisa - $qty_p;

            $arraySupplier[] = array('kode' => $row->ID_KOTA, 'qty_p' => $qty_p, 'produk' => $row->ID_PRODUK, 'jne' => $row->KODE_JNE);
            $method = array();
            $query3 = 'SELECT LATITUDE, LONGITUDE, KODE_JNE from r_kota where KODE_KOTA = ?;';
            $result3 = $this->db->query($query3, array($row->KODE_KOTA));
            $row3 = $result3->row();
            $kode_jne2 = $row3->KODE_JNE;
            $query4 = 'INSERT INTO  temp_transaksi (ID,UUID,ID_SUPPLIER,ID_PRODUK,QTY,NO_PO,IS_ACTIVE,ID_KOTA) VALUES (?,?,?,?,?,?,?,?);';
            $result4 = $this->db->query($query4, array($CI2->uuid->v4(),$uuidz,$row->ID_SUPPLIER,$row->ID_PRODUK,$qty_p,null,'0',$origin ));


            $harga_trf = $this->JNE($kode_jne, $kode_jne2, $berat*$qty_p);

            for ($i = 0; $i < count($harga_trf['price']); $i++) {
                $trf['price'][] = $harga_trf['price'][$i];
            }


            if ($sisa <= '0')
                break;
        }

        return $trf;
    }



}
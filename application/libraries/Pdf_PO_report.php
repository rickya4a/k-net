<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_PO_report extends Autowraptable
{

	public $options = array(
	  		'filename'  => 'qwewqewq' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P'
	  	);

    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(), $as='',$count=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->as = $as;
			$this->count = $count;
	}

	public function Header()
	{
		$mst = $this->mst;
		$h = 13;
		$this->Ln(5);
		$this->SetFont("Courier", "", 10);
		$this->Cell(550,$h,'' ,0,0,'R' ,false);
		$this->Ln();
		$this->SetFont("", "", 10);
		$this->Cell(550,$h,'' ,0,0,'R' ,false);
		$this->Ln();
		$this->SetFont("", "", 10);
		$this->Cell(550,$h,'' ,0,0,'R' ,false);
		$this->Ln();
		$this->SetFont("", "", 10);
		$this->Cell(550,$h,'' ,0,0,'R' ,false);
		$this->Ln();
		$this->SetFont("", "", 10);
		$this->Cell(550,$h,'' ,0,0,'R' ,false);
		$this->Ln();
		$this->SetFont("", "", 10);
		$this->Cell(550,$h,'' ,0,0,'R' ,false);
		$this->Ln();
		$this->SetFont("", "B", 11);
		$this->Cell(20,$h,'' ,0,0,'L' ,false);
		$this->Ln();
		$this->Ln();
		$this->Ln();
		$this->Ln();
		$this->Ln();

		$this->SetFont("", "B", 16);
		$this->SetX(0);
		$this->Cell(0, 0, 'PURCHASE ORDER' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		$table= $this->data;

		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(5);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'No. PO' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(80); $this->Cell(150, $h, ": ".$mst->NO_PO , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(420);$this->Cell(0, $h, 'Date' , 0, 0,'L',false );
		$this->SetFont("", "", 10);
		$this->SetX(480); $this->Cell(150, $h,': '.date("d-m-Y",strtotime($mst->woop)) , 0, 0, 'L' ,false);
		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'To' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(80); $this->Cell(150, $h, ": ".$mst->NAMA_SUPPLIER , 0, 0, 'L' ,false);
		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Address' ,0,0,'L' ,false);
		$this->SetX(80); $this->Cell(150, $h, ":", 0, 0, 'L' ,false);

		$this->SetFont("", "", 10);

		$this->SetX(92); $this->MultiCell(300, 13, "".$mst->ALAMAT , 0 );

//		$this->SetFont("", "B", 10);
//		$this->SetX(350);$this->Cell(0, $h, 'To' , 0, 0,'L',false );
//		$this->SetFont("", "", 10);
//		$this->SetX(400); $this->Cell(150, $h,': '.$mst->CODE_STOCKIES , 0, 0, 'L' ,false);
		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Attn' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(80); $this->Cell(150, $h, ": ".$mst->ATN , 0, 0, 'L' ,false);

		$kurs= $this->data;
		
		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Kurs' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(80); $this->Cell(150, $h, ": ".$kurs[0]['CURRENCY'] , 0, 0, 'L' ,false);




		$this->Ln(30);

		$this->HeaderTable();
//		$this->indent();


	}
	function BasicTable()
	{
		$this->SetFont("", "B", 10);

		// Header
		$this->Cell(20,20,'No',1,0,'C');
		$this->Cell(50,20,'Product',1,0,'C');
		$this->Cell(170,20,'Product Name',1,0,'C');
		$this->Cell(80,20,'Qty ',1,0,'C');
		$this->Cell(80,20,'Unit Price',1,0,'C');
		$this->Cell(80,20,'Total Amount',1,0,'C');


		$this->Ln();
		$this->SetFont("", "", 10);

		$table= $this->data;
		$i=0;$total=0;$terkirim=0;$sisa=0;
		foreach($table as $row) {
			$i++;
			$this->Cell(20,20,$i,1);
			$this->Cell(50,20,$row->PRODUCT_CODE,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(80,20,$row->QTY,1,0,'R');
			$this->Cell(80,20,$row->PRICE." ".$row->CURRENCY,1,0,'R');
			$this->Cell(80,20,$row->TOTAL." ".$row->CURRENCY,1,0,'R');

			$this->Ln();
			$total=$total+$row->TOTAL;

		}

		$this->Cell(400,20,'Sub Total',1);
		$this->Cell(80,20,$total." ".$row->CURRENCY,1,0,'R');

		$this->Ln();


	}



	function bawah()
	{
		$mst = $this->mst;


		$this->SetFont("", "B", 10);
		$this->Cell(55, 13, 'Remarks :', 0, 0, 'L', false);
		$this->multiCell(320,13,$mst->REMARK,0,'L');

		$this->Ln(70);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, 'Approved By ', 0, 0, 'L', false);
		$this->Ln(70);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, "".$mst->TTD."", 0, 0, 'L', false);
		$this->Ln();

		$this->Cell(150, 13, 'Adm & Purchasing Manager',0, 0, 'L', false);
		$this->Ln();

	}

	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();

		$this->Cell(20, 26, 'No' , 1, 0, 'C');
		$this->SetXY($left += 20,$Z); $this->Cell(70, 26, 'Code' , 1, 0, 'C' );
		$this->SetXY($left += 70,$Z); $this->Cell(220, 26, 'Product Name' , 1, 0, 'C' );
		$this->SetXY($left += 220,$Z); $this->Cell(50, 26, 'Qty' ,1, 0, 'C' );
		$this->SetXY($left += 50,$Z); $this->Cell(80, 26, 'Price' , 1, 0, 'C' );
		$this->SetXY($left += 80,$Z); $this->MultiCell(100, 26, 'Total Amount' , 1,'C' );



	}

	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(20,70,220,50,80,100));
		$this->SetAligns(array('L','L','L','R','R','R'));
		$no = 1;


		if($this->count==1)
			$gede=70;
		else
			$gede=15;
		foreach ($this->data as $row) {


			$this->Row(

				array(
					$no++,

				isset($row['PO_PRODUCT_CODE']) ? $row['PO_PRODUCT_CODE'] : $row['PRODUCT_CODE']
				,
					isset($row['PO_PRODUCT_NAME']) ? $row['PO_PRODUCT_NAME'] : $row['PRODUCT_NAME']
				,
					number_format($row['QTY']) ,
					number_format((float)$row['PRICE'], 2, '.', ',') ,
					number_format((float)$row['TOTAL'], 2, '.', ',')

				), $gede, 15
			);
			$total=$total+$row['TOTAL'];

		}
		$this->SetFont("", "", 10);
		$this->Cell(440,26,'Total',1);
		$this->Cell(100,26,number_format((float)$total, 2, '.', ',')."",1,0,'R')
		
							
;

		$this->Ln();

		$this->bawah();
	}

	public function Footer() {
//		$mst = $this->mst;
//		$as = $this->as;
//		//$as=$this->session->userdata('NAME');
//		$this->SetY(-35);
//		$this->SetFont("Courier", "I", 10);
//
//
//		//foreach($name as $row) {
//		$this->Cell(0,10,($as),0,0,'L');
//
//		$this->SetY(-35);
//		$this->SetFont('Courier','I',10);
//
//		$this->Cell(0,10,$mst->NO_PO,0,0,'L');
//
//		//Position at 1.5 cm from bottom
//		$this->SetY(-15);
//		//Arial italic 8
//		$this->SetFont('Courier','I',10);
//		// Document Time
//		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
//		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
//		//Page number
//		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}

}
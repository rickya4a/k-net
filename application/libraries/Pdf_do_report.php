<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_do_report extends Autowraptable
{
	
	public $payheaderCek = true;
	public $pindentHeaderCek = false;
	public $indentHeaderCek = false;

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'Letter' ,
	  		'orientation' =>'P'
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$data_kwit=array(), $data_pindent=array(), $username=array()){

			$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->kwit = $data_kwit;
			$this->pindent=$data_pindent;
			$this->username = $username;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'DELIVERY ORDER REPORT' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();

		$YY = $this->GetY();

		$this->Ln(5);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Do Number' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->NO_DO , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(350);$this->Cell(0, $h, 'To' , 0, 0,'L',false );
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h,': '.(isset($mst->CODE_STOCKIES) ? $mst->CODE_STOCKIES : $mst->kd)  , 0, 0, 'L' ,false);
		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Create Date' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->TANGGAL_DO , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h,'  '.$mst->REAL_NAMA , 0, 0, 'L' ,false);


		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Branch' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': PT. K-link Nusantara' , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(350);$this->Cell(0, $h, 'Address :' , 0, 0,'L',false );
		$this->SetFont("", "", 10);
		$this->SetX(410); $this->MultiCell(150, $h,''.$mst->REAL_ALAMAT , 0);
		$this->Ln(12);

		$this->SetXY($left,140);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Warehouse' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->WAREHOUSE_NAME , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(410); $this->MultiCell(150, 13,''.$mst->REAL_ALAMAT2 , 0);
		$this->Ln(12);


		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Delivery By ' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->NAMA_COURIER , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(410); $this->MultiCell(150, 13,''.$mst->REAL_ALAMAT3 , 0);
		$this->Ln(12);


		$this->SetFont("", "", 10);
		$this->Cell(20,$h,'' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);

		$this->SetFont("", "B", 10);
		$this->SetX(350);$this->Cell(0, $h, 'Telpon' , 0, 0,'L',false );
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h,': '.$mst->PHONE , 0, 0, 'L' ,false);
		$this->Ln(30);

		if($this->payheaderCek == true){
		   $this->HeaderTable();
		}
		
		if($this->pindentHeaderCek == true){
		   $this->pindentHeader();
		}

		if($this->indentHeaderCek == true){
		   $this->indentHeader();
		}

	}
	
	public function HeaderTable()
	{
		$this->SetFont("", "B", 10);

		$this->Cell(20,13,'Payment : ' ,0,0,'L' ,false);
		$this->Ln();


		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);


		$left = $this->GetX();
		$this->Cell(30,26,'No' ,1,0,'C' );
		$this->SetXY($left += 30,$Z); $this->Cell(80, 26, 'Product ' , 1, 0, 'C');
		$this->SetXY($left += 80,$Z); $this->Cell(200, 26, 'Product Name' , 1,0,'C');
		$this->SetXY($left += 200,$Z); $this->Cell(50, 26, 'Qty' , 1, 0, 'C');
		$this->SetXY($left += 50,$Z); $this->MultiCell(50, 13, 'Qty Ship' , 1);
		$this->SetXY($left += 50,$Z); $this->MultiCell(50, 26, 'B/O' , 1);

	}
	
	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$KELUAR = 0;
			$masuk = 0;
			$awal = 0;
			$sisa = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(30, 80, 200, 50, 50, 50));
			$this->SetAligns(array('L', 'L', 'L', 'R', 'R', 'R'));
			$no = 1;
			$k = 0;
			$terkirim = 0;
			foreach ($this->data as $row) {

				$this->Row(
					array($no++,
						$row['ID_PRODUCT'],
						$row['PRODUCT_NAME'],
						number_format($row['total']),
						number_format($row['TERKIRIM']),
						number_format($row['SISA']),
					), 15, 15
				);
				$total = $total + $row['total'];
				$terkirim = $terkirim + $row['TERKIRIM'];
				$sisa = $sisa + $row['SISA'];
				$k++;
			}

			$this->SetFont("", "", 10);
			$this->Cell(310, 26, 'Sub Total', 1);


			$this->Cell(50, 26, number_format($total), 1, 0, 'R');
			$this->Cell(50, 26, number_format($terkirim), 1, 0, 'R');
			$this->Cell(50, 26, number_format($sisa), 1, 0, 'R');
		}

		{
			$this->payheaderCek = false;
		}

		$this->Ln();
		$this->Ln();

		$this->pindentTable();
		$this->indent();
		$this->bawah();

	}
	/* Enggak Ke Pake
	function BasicTable()
	{
		$table= $this->data;
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			$this->Cell(80,20,'No',1,0,'C');
			$this->Cell(80,20,'Product',1,0,'C');
			$this->Cell(200,20,'Product Name',1,0,'C');
			$this->Cell(50,20,'Qty',1,0,'C');
			$this->Cell(50,20,'Qty Ship',1,0,'C');
			$this->Cell(50,20,'B/O',1,0,'C');

			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$terkirim=0;$sisa=0;
			foreach($table as $row) {
				$i++;
				$this->Cell(80,20,$i,1);
				$this->Cell(80,20,$row->ID_PRODUCT,1);
				$this->Cell(200,20,$row->PRODUCT_NAME,1);
				$this->Cell(50,20,$row->total,1,0,'R');
				$this->Cell(50,20,$row->TERKIRIM,1,0,'R');
				$this->Cell(50,20,$row->SISA,1,0,'R');
				$this->Ln();
				$total=$total+$row->total;
				$terkirim=$terkirim+$row->TERKIRIM;
				$sisa=$sisa+$row->SISA;
			}



			// Data



			$this->Cell(360,20,'Sub Total',1);
			$this->Cell(50,20,$total,1,0,'R');
			$this->Cell(50,20,$terkirim,1,0,'R');
			$this->Cell(50,20,$sisa,1,0,'R');
		}
		$this->Ln(50);
	}
	 */
	
	function pindentHeader(){
		$table= $this->pindent;
		if($table!=null){
			$this->SetFont("", "B", 10);

			$this->Cell(20,13,'Payment Indent : ' ,0,0,'L' ,false);
			$this->Ln();
			// Header
			$this->Cell(30,20,'No',1,0,'C');
			$this->Cell(80,20,'Product',1,0,'C');
			$this->Cell(250,20,'Product Name',1,0,'C');
			$this->Cell(100,20,'DO Parent',1,0,'C');
			//$this->Cell(50,20,'Qty Ship',1,0,'C');
			$this->Cell(50,20,'Qty',1,0,'C');

			$this->Ln();
		}
	}
	 
	function pindentTable()
	{
		$table= $this->pindent;
		if($table!=null){
			$this->pindentHeader();
			
			$this->pindentHeaderCek = true;
			
			$this->SetFont("", "", 10);

			$i=0;$total=0;$terkirim=0;$sisa=0;
			foreach($table as $row) {
				$i++;
				$this->Cell(30,20,$i,1);
				$this->Cell(80,20,$row->ID_PRODUCT,1);
				$this->Cell(250,20,$row->PRODUCT_NAME,1);
				$this->Cell(100,20,$row->NO_DO,1);
				$this->Cell(50,20,$row->QTY,1,0,'R');

				$this->Ln();
				$total=$total+$row->QTY;
			}

			$this->Cell(460,20,'Sub Total',1);
			$this->Cell(50,20,$total,1,0,'R');
		//	$this->Cell(50,20,$terkirim,1,0,'R');
			//$this->Cell(50,20,$sisa,1,0,'R');
			
			if(count($table) == $i)
			{
				$this->pindentHeaderCek = false;
			}
			
			$this->Ln(50);	
		}else{
				$this->pindentHeader = false;
		}
	}
	
	function indentHeader()
	{
		$indent = $this->indent;

		if($indent!=null){
			$this->SetFont("", "B", 10);
			$this->Cell(20,13,'Indent : ' ,0,0,'L' ,false);
			$this->Ln();
	
			// Header
			$this->Cell(30,20,'No',1,0,'C');
			$this->Cell(80,20,'Product',1,0,'C');
			$this->Cell(350,20,'Product Name',1,0,'C');
			$this->Cell(50,20,'Qty',1,0,'C');
		}

		$this->Ln();
	}
	
	function indent()
	{

		$indent = $this->indent;

		if($indent!=null){
			$this->indentHeader();
				
			$this->indentHeaderCek = true;
				
			$this->SetFont("", "", 10);
			$i=0;$total=0;
			foreach($indent as $row) {
				$i++;
				$this->Cell(30,20,$i,1);
				$this->Cell(80,20,$row->ID_PRODUCT,1);
				$this->Cell(350,20,$row->PRODUCT_NAME,1);
				$this->Cell(50,20,$row->total,1,0,'R');
				$total=$total+$row->total;
				$this->Ln();
			}
	
			// Data
			$this->Cell(460,20,'Sub Total',1);
			$this->Cell(50,20,$total,1,0,'R');
			
			if(count($indent) == $i)
			{
				$this->indentHeaderCek = false;
			}
			
			$this->Ln(30);
		}else{
			$this->indentHeaderCek = false;
		}
	}


	function bawah()
	{
		$kwkw ="";
		$kwit = $this->kwit;
		$mst = $this->mst;
		$this->AddPage();

		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, '1.................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2.................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3.................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4.................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Ln();


		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
//		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Ln();
		$this->Ln();
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Ln();
		$this->Cell(20, 13, 'DO Parent ', 0, 0, 'L', false);
		$this->SetX(150); $this->Cell(150, 13, ": ".$mst->NO_DO , 0, 0, 'L' ,false);

		$this->Ln();
		$this->Cell(20, 13, 'Receipt Number ', 0, 0, 'L', false);
		$this->SetFont("", "B", 10);
		$this->SetX(150);
		$this->Cell(5, 13, ":  ", 0, 0, 'L' ,false);
		foreach($kwit as $row) {
			$kwkw .= $row->y . ", ";
			//	$this->Cell(0, 13," ".(isset($row->NO_KWITANSI) ? $row->NO_KWITANSI : $row->ID_KWITANSI)."," , 0, 0, 'L' ,false);
		}

		$this->MultiCell(200, 13,' '.$kwkw , 0);
		$this->Ln();

	}

	public function Footer() {

		$name = $this->username;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($name->NAME),0,0,'L');




		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont("Courier", "", 10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}

}
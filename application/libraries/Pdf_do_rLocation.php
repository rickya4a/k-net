<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_do_rLocation extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$data_KW=array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->kw =$data_KW;
			$this->as =$as;
	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'PICKING LIST (LOCATION) - DELIVERY ORDER' , 0, 1,'C' );
		$this->Ln(30);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();

		$this->Ln(30);

		$this->HeaderTable();
	//	$this->indent();
		//$this->bawah();



	}

//	function BasicTable()
//	{
//		$table= $this->data;
//		if($table!=null){
//			$this->SetFont("", "B", 10);
//
//			// Header
//			$this->Cell(80,20,'No',1);
//			$this->Cell(170,20,'Product Name',1);
//			$this->Cell(50,20,'Rack',1);
//			$this->Cell(50,20,'Qty',1);
//			$this->Cell(50,20,'SubTotal',1);
//
//			$this->Ln();
//			$this->SetFont("", "", 10);
//
//			$i=0;$total=0;$terkirim=0;$sisa=0;
//			foreach($table as $row) {
//				if($row->CATEGORY==1){
//					$X= 'Pcs';
//					$tots=$row->total;
//				}else{
//					$X='Box';
//					$tots=$row->total*$row->BOX;
//				}
//
//				$i++;
//				$this->Cell(80,20,$i,1);
//				$this->Cell(170,20,$row->PRODUCT_NAME,1);
//				$this->Cell(50,20,$row->RACK_NAME,1);
//				$this->Cell(50,20,$row->total.$X,1,0,'R');
//				$this->Cell(50,20,$tots.'Pcs',1,0,'R');
//
//				$this->Ln();
//			//	$total=$total+$row->total;
//				$terkirim=$terkirim+($tots);
//			//	$sisa=$sisa+$row->SISA;
//			}
//
//
//
//			// Data
//
//
//
//			$this->Cell(350,20,'Total',1);
//			//$this->Cell(50,20,$total,1);
//			$this->Cell(50,20,$terkirim.'Pcs',1,0,'R');
//			//$this->Cell(50,20,$sisa,1);
//		}
//		$this->Ln();
//
//
//	}

	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);


		$left = $this->GetX();
		$this->Cell(20,26,'No' ,1,0,'C' );
		$this->SetXY($left += 20,$Z); $this->Cell(170, 26, 'Product Name' , 1, 0, 'C');
		$this->SetXY($left += 170,$Z); $this->Cell(170, 26, 'Product Rack' , 1);
		$this->SetXY($left += 170,$Z); $this->Cell(50, 26, 'Qty' , 1, 0, 'C');
		$this->SetXY($left += 50,$Z); $this->MultiCell(70, 13, 'SubTotal (Pcs)' , 1);
	}



	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(20,170,170,50,'70'));
		$this->SetAligns(array('L','L','L','R','R'));
		$no = 1;

		$terkirim=0;
		foreach ($this->data as $row) {
//			if($row['CATEGORY']==1){
//				$X= 'Pcs';
//				$tots=$row['total'];
//			}else{
//				$X='Box';
//				$tots=$row['total']*$row['BOX'];
//			}
			if($row['CATEGORY']==1){
				$x='pcs';
				$tots=$row['total'];
			}
			else{
				$x='box';
				$tots=$row['total'] * $row['BOX']; ;
			}
			$this->Row(
				array($no++,
					$row['PRODUCT_NAME'],
					$row['RACK_NAME'],
					number_format($row['total']).$x,
					number_format($tots),

//				$this->Cell(80,20,$i,1);
//				$this->Cell(170,20,$row->PRODUCT_NAME,1);
//				$this->Cell(50,20,$row->RACK_NAME,1);
//				$this->Cell(50,20,$row->total.$X,1,0,'R');
//				$this->Cell(50,20,$tots.'Pcs',1,0,'R');
				), 15, 15
			);
			$total=$total+$tots;
		}
		$this->SetFont("", "", 10);
		$this->Cell(410,26,'Sub Total',1);


		$this->Cell(70,26,number_format($total),1,0,'R');


		$this->Ln();
		$this->bawah();
	}

	function bawah()
	{
		$kw = $this->kw;
		$mst = $this->mst;

		$this->SetFont("", "B", 10);

		$this->Ln();
		$this->Cell(20, 13, 'DO Parent: ', 0, 0, 'L', false);
		$this->SetFont("", "", 10);
		$this->SetX(150);
		$arr="";
		foreach($mst as $row) {
			$arr.= $row->NO_DO.",";
		}
		$this->MultiCell(400, 13,$arr , 0);

		$this->Ln();
		$this->SetFont("", "B", 10);

		$this->Cell(20, 13, 'Receipt Number: ', 0, 0, 'L', false);
		$this->SetFont("", "", 10);
		$this->SetX(150);
		$arr2="";

		foreach($kw as $row) {
			$arr2.= (isset($row->NO_KWITANSI) ? $row->NO_KWITANSI : $row->ID_KWITANSI).",";
		}
		$this->MultiCell(400, 13,$arr2 , 0);

		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(175, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
	}

	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-35);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');



		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
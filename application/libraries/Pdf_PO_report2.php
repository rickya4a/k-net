<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_PO_report2 extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$tgl_awal='',$tgl_akhir='',$as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->tgl_awal=$tgl_awal;
			$this->tgl_akhir=$tgl_akhir;
			$this->as=$as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 16);
		$this->SetX(0);
		$this->Cell(0, 0, 'REPORT SUMMARY PO' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		$awal=$this->tgl_awal;
		$akhir=$this->tgl_akhir;

		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(30);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Tanggal' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(100); $this->Cell(100, $h, ": ".$awal." s/d ".$akhir , 0, 0, 'L' ,false);
		$this->ln();
		$this->HeaderTable();

		//$this->bawah();


	}
	function BasicTable()
	{

		$table= $this->data;
		//print_r($table);
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			$this->Cell(60,20,'No PO',1);
			$this->Cell(60,20,'Atas Nama',1);
			$this->Cell(80,20,'Supplier',1);
			//$this->Cell(50,20,'Awal',1);

			$this->Cell(150,20,'Produk',1);
			$this->Cell(50,20,'Harga',1);
			$this->Cell(50,20,'Qty',1);
			$this->Cell(55,20,'Total',1);


			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
			foreach($table as $row) {


				$i++;

				$this->Cell(60,20,$row->NO_PO,1);
				$this->Cell(60,20,$row->ATN,1);
				$this->Cell(80,20,$row->NAMA_SUPPLIER,1);

				//	$this->Cell(50,20,$row->AWAL.$sat,1);

				$this->Cell(150,20,$row->PRODUCT_NAME,1);
				$this->Cell(50,20,$row->PRICE." ".$row->CURRENCY ,1);

				$this->Cell(50,20,$row->QTY." Pcs",1);
				$this->Cell(55,20,$row->TOTAL." ".$row->CURRENCY ,1);


				$this->Ln();
				$masuk=$masuk+($row->QTY);
				$awal=$awal+($row->TOTAL);

			//	$KELUAR=$KELUAR+($row->KELUAR*$row->SATUAN);
			//	$total=$total+($row->TOTAL*$row->SATUAN);
			//	$sisa=$sisa+$row->SISA;
			}



			// Data



			$this->Cell(400,20,'Total',1);
		//	$this->Cell(50,20,$awal.' Pcs',1);
		//	$this->Cell(50,20,$masuk.' Pcs',1);
		//	$this->Cell(50,20,$KELUAR.' Pcs',1);
		//	$this->Cell(50,20,'',1);

			$this->Cell(50,20,$masuk.' Pcs',1);
		//	$this->Cell(55,20,$awal." ".$row->CURRENCY,1);

		}
		$this->Ln();


	}



	function bawah()
	{
		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(70);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, '1....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4....................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'DO Parent: ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'Receipt Number: ', 0, 0, 'L', false);
		$this->Ln();

	}

	public function HeaderTable()
	{
		if($this->data!=null) {
			$h = 13;
			$left = 10;
			$top = 80;
			$Z = $this->getY();
			#tableheader
			$this->SetFont("", "B", 10);
			//$this->SetFillColor(200,200,200);
			$left = $this->GetX();

			$this->Cell(20, 26, 'No', 1, 0, 'C');
			$this->SetXY($left += 20, $Z);
			$this->Cell(60, 26, 'No PO', 1, 0, 'C');
			$this->SetXY($left += 60, $Z);
			$this->Cell(60, 26, 'Atas Nama', 1, 0, 'C');
			$this->SetXY($left += 60, $Z);
			$this->Cell(80, 26, 'Supplier', 1, 0, 'C');
			$this->SetXY($left += 80, $Z);
			$this->Cell(150, 26, 'Produk', 1, 0, 'C');
			$this->SetXY($left += 150, $Z);
			$this->Cell(50, 26, 'Harga', 1, 0, 'C');
			$this->SetXY($left += 50, $Z);
			$this->Cell(50, 26, 'Qty', 1, 0, 'C');

			$this->SetXY($left += 50, $Z);
			$this->MultiCell(80, 26, 'Total Harga', 1);
		}

	}

	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$KELUAR = 0;
			$masuk = 0;
			$awal = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(20, 60, 60, 80, 150, 50, 50, 80));
			$this->SetAligns(array('C', 'C', 'C', 'C', 'C', 'R', 'R', 'R'));
			$no = 1;


			foreach ($this->data as $row) {


				$this->Row(

					array(
						$no++,
						$row['NO_PO'],
						$row['ATN'],
						$row['NAMA_SUPPLIER'],
						$row['PRODUCT_NAME'],
						number_format($row['PRICE']) . " " . $row['CURRENCY'],
						number_format($row['QTY']),

						number_format($row['TOTAL']) . " " . $row['CURRENCY']

					), 15, 15
				);
				$total = $total + $row['QTY'];

			}


			$this->Ln();
		}
	}


	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');



		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
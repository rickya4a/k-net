<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_MS_report2 extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$tgl_awal='',$tgl_akhir='', $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->tgl_awal=$tgl_awal;
			$this->tgl_akhir=$tgl_akhir;
			$this->as=$as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'REPORT SUMMARY MOVING STOCK' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		$awal=$this->tgl_awal;
		$akhir=$this->tgl_akhir;

		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(30);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Tanggal' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(100); $this->Cell(100, $h, ": ".$awal." s/d ".$akhir , 0, 0, 'L' ,false);
		$this->ln();
		$this->HeaderTable();

		//$this->bawah();


	}
	function BasicTable()
	{

		$table= $this->data;
		//print_r($table);
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			$this->Cell(60,20,'No MS',1);
			$this->Cell(105,20,'Warehouse',1);
			$this->Cell(70,20,'Asal',1);
			$this->Cell(70,20,'Tujuan',1);

			$this->Cell(120,20,'Produk',1);
			$this->Cell(50,20,'Qty box',1);

			$this->Cell(50,20,'Total',1);


			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
			foreach($table as $row) {


				$i++;

				$this->Cell(60,20,$row->NO_MOVING_STOCK,1);
				$this->Cell(105,20,$row->WAREHOUSE_NAME,1);
				$this->Cell(70,20,$row->ASAL,1);
				$this->Cell(70,20,$row->TUJUAN,1);

				$this->Cell(120,20,$row->PRODUCT_NAME,1);

				$this->Cell(50,20,$row->QTY_BOX." Box" ,1);

				$this->Cell(50,20,$row->QTY_PIECE." Pcs",1);


				$this->Ln();
				$masuk=$masuk+($row->QTY_BOX);
				$awal=$awal+(($row->QTY_PIECE)." Pcs");


			}



			// Data



			$this->Cell(455,20,'Total',1);

			$this->Cell(50,20,$masuk.' Pcs',1);
			$this->Cell(50,20,$awal." Pcs",1);

		}
		$this->Ln();


	}



	function bawah()
	{
		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(70);

		$this->SetFont("", "B", 10);
		$this->Cell(150, 13, '1....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4....................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
		$this->Ln();

		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'DO Parent: ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'Receipt Number: ', 0, 0, 'L', false);
		$this->Ln();

	}

	public function HeaderTable()
	{
		if($this->data!=null) {
			$h = 13;
			$left = 10;
			$top = 80;
			$Z = $this->getY();
			#tableheader
			$this->SetFont("", "B", 10);


			$left = $this->GetX();

			$this->Cell(60, 26, 'No MS', 1, 0, 'C');
			$this->SetXY($left += 60, $Z);
			$this->Cell(105, 26, 'Warehouse', 1, 0, 'C');
			$this->SetXY($left += 105, $Z);
			$this->Cell(70, 26, 'Asal', 1, 0, 'C');
			$this->SetXY($left += 70, $Z);
			$this->Cell(70, 26, 'Tujuan', 1, 0, 'C');
			$this->SetXY($left += 70, $Z);
			$this->Cell(120, 26, 'Produk', 1, 0, 'C');
			$this->SetXY($left += 120, $Z);
			$this->Cell(50, 26, 'Qty box', 1, 0, 'C');
			$this->SetXY($left += 50, $Z);
			$this->MultiCell(70, 26, 'Total', 1);

		}
	}

	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$KELUAR = 0;
			$masuk = 0;
			$awal = 0;
			$terkirim = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(60, 105, 70, 70, 120, 50, 70));
			$this->SetAligns(array('C', 'C', 'C', 'R', 'R', 'R', 'R'));
			$no = 1;
			foreach ($this->data as $row) {
				$this->Row(
					array(
						$row['NO_MOVING_STOCK'],
						$row['WAREHOUSE_NAME'],
						$row['ASAL'],
						$row['TUJUAN'],
						$row['PRODUCT_NAME'],

						number_format($row['QTY_BOX']) . 'Box',
						number_format($row['QTY_PIECE']) . 'Pcs',


					), 15, 15
				);
				$total = $total + $row['QTY_BOX'];
				$terkirim = $terkirim + $row['QTY_PIECE'];
			}
			$this->SetFont("", "", 10);
			$this->Cell(425, 26, 'Total', 1);
//			$this->Cell(50,26,number_format($awal).' Pcs',1,0,'R');
//			$this->Cell(50,26,number_format($masuk).' Pcs',1,0,'R');
//			$this->Cell(50,26,number_format($KELUAR).' Pcs',1,0,'R');
			//$this->Cell(70,26,'',1);
			$this->Cell(50, 26, number_format($total) . 'Box', 1, 0, 'R');

			$this->Cell(70, 26, number_format($terkirim) . 'Pcs', 1, 0, 'R');


			$this->Ln();
		}

	}


	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');

		//$this->SetY(-25);
		//$this->SetFont('Courier','I',10);

		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');

		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
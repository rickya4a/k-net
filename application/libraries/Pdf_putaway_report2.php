<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_putaway_report2 extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$tgl_awal='',$tgl_akhir='', $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->tgl_awal=$tgl_awal;
			$this->tgl_akhir=$tgl_akhir;
			$this->as=$as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'REPORT SUMMARY PUTAWAY' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		$awal=$this->tgl_awal;
		$akhir=$this->tgl_akhir;

		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(30);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Tanggal' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(100); $this->Cell(100, $h, ": ".$awal." s/d ".$akhir , 0, 0, 'L' ,false);
		$this->ln();
		$this->HeaderTable();

		//$this->bawah();


	}
	function BasicTable()
	{

		$table= $this->data;
		//print_r($table);
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			$this->Cell(60,20,'No Putaway',1,0,'C');
			$this->Cell(80,20,'Tanggal Transaksi',1,0,'C');
			$this->Cell(60,20,'Rack',1,0,'C');
			$this->Cell(50,20,'No Batch',1,0,'C');
			$this->Cell(50,20,'Tgl Batch',1,0,'C');
			$this->Cell(50,20,'EXP. Pabrik',1,0,'C');
			$this->Cell(55,20,'EXP. Gudang',1,0,'C');
			$this->Cell(50,20,'Qty',1,0,'C');
			$this->Cell(50,20,'Total (Pcs)',1,0,'C');

			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
			foreach($table as $row) {

				if($row->CATEGORY==0)
				{$sat=' Box'; $BOX=$row->BOX;}
				else
				{$sat=' Pcs';$BOX=1;}

				$i++;

				$this->Cell(60,20,$row->PUTAWAY_CODE,1);
				$this->Cell(80,20,$row->PUTAWAY_DATE,1);
				$this->Cell(60,20,$row->RACK_NAME,1);
				$this->Cell(50,20,$row->NO_BATCH,1);
				$this->Cell(50,20,$row->EXP_BATCH,1);
				$this->Cell(50,20,$row->EXP_PABRIK,1);
				$this->Cell(55,20,$row->EXP_GUDANG,1);
				$this->Cell(50,20,number_format($row->QTY).$sat,1,0,'R');
				$this->Cell(50,20,number_format($row->QTY*$BOX)." Pcs",1,0,'R');


				$this->Ln();
				$awal=$awal+($row->QTY*$BOX);

			//	$KELUAR=$KELUAR+($row->KELUAR*$row->SATUAN);
			//	$total=$total+($row->TOTAL*$row->SATUAN);
			//	$sisa=$sisa+$row->SISA;
			}



			// Data



			$this->Cell(455,20,'Total',1);
		//	$this->Cell(50,20,$awal.' Pcs',1);
		//	$this->Cell(50,20,$masuk.' Pcs',1);
		//	$this->Cell(50,20,$KELUAR.' Pcs',1);
		//	$this->Cell(50,20,'',1);

			//$this->Cell(50,20,$masuk.$sat,1);
			$this->Cell(50,20,number_format($awal).' Pcs',1,0,'R');

		}
		$this->Ln();


	}



		function bawah()
	{

		$this->SetFont("", "B", 10);

		$this->Ln();



		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(175, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
	}

	public function HeaderTable()
	{
		if($this->data!=null){

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();

		$this->Cell(20, 26, 'No' , 1, 0, 'C');
		$this->SetXY($left += 20,$Z); $this->MultiCell(50, 13, 'No Putaway' , 1 );
		$this->SetXY($left += 50,$Z); $this->MultiCell(70, 13, 'Tanggal Transaksi' , 1 );
		$this->SetXY($left += 70,$Z); $this->Cell(60, 26, 'Rack' , 1, 0, 'C' );
		$this->SetXY($left += 60,$Z); $this->MultiCell(50, 13, 'No Batch' ,1 );
		$this->SetXY($left += 50,$Z); $this->MultiCell(70, 13, 'Tanggal Batch' , 1 );
		$this->SetXY($left += 70,$Z); $this->MultiCell(70, 26, 'EXP Pabrik' , 1);
		$this->SetXY($left += 70,$Z); $this->MultiCell(70, 26, 'EXP Gudang' , 1);
		$this->SetXY($left += 70,$Z); $this->Cell(50, 26, 'Qty' , 1, 0, 'C' );
		$this->SetXY($left += 50,$Z); $this->MultiCell(60, 13, 'Total (Pcs)' , 1 );
		}
	}

	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$total2 = 0;
			$masuk = 0;
			$awal = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(20, 50, 70, 60, 50, 70, 70, 70, 50, 60));
			$this->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'R', 'R'));
			$no = 1;


			foreach ($this->data as $row) {
				if ($row['CATEGORY'] == 0) {
					$sat = ' Box';
					$BOX = $row['BOX'];
				} else {
					$sat = ' Pcs';
					$BOX = 1;
				}

				$this->Row(


					array(
						$no++,
						$row['PUTAWAY_CODE'],
						$row['PUTAWAY_DATE'],
						$row['RACK_NAME'],

						$row['NO_BATCH'],
						$row['EXP_BATCH'],
						$row['EXP_PABRIK'],
						$row['EXP_GUDANG'],

						number_format($row['QTY']) . $sat,
						number_format($row['QTY']) * $BOX . ' Pcs'
					), 15, 15
				);
				//$total=$total+$row['QTY'];
				$total2 = $total2 + ($row['QTY'] * $BOX);
				//$awal=$awal+($row->QTY*$BOX);

			}
			$this->SetFont("", "", 10);
			//$this->Cell(450,26,'Total',1);
			//$this->Cell(80,26,number_format($total)." ".$row['CURRENCY'],1,0,'R');
			$this->Cell(510, 20, 'Sub Total', 1);
			//$this->Cell(50,20,$total.' box',1);
			$this->Cell(60, 20, $total2 . ' pcs', 1, 0, 'R');
			$this->Ln();
		}
		$this->bawah();
	}

	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}

}
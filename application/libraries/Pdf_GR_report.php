<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_GR_report extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->as = $as;
	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'GOOD RECEIVE REPORT' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(5);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'GR Number' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->NO_GR , 0, 0, 'L' ,false);
		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'PL Number' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->NO_PL , 0, 0, 'L' ,false);

		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Create Date' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->GR_DATE , 0, 0, 'L' ,false);
//		$this->SetFont("", "", 10);
//		$this->SetX(400); $this->Cell(150, $h,' '.$mst->NAMA_STOCKIES , 0, 0, 'L' ,false);


		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Consignee' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': PT. K-link Nusantara' , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);

		$this->Ln(12);


		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Warehouse' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->WAREHOUSE_NAME , 0, 0, 'L' ,false);

		$this->Ln(12);


		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Exporter ' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->EXPORTER , 0, 0, 'L' ,false);

		$this->Ln(12);



		$this->Ln(30);

		$this->HeaderTable();
//		$this->indent();
//		$this->bawah();


	}
	function bawah()
	{

		$this->SetFont("", "B", 10);

		$this->Ln();



		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(175, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
	}

	function BasicTable()
	{
		$this->SetFont("", "B", 10);

		// Header
		$this->Cell(20,40,'No',1,0,'C');
		$this->Cell(50,40,'Product',1,0,'C');
		$this->Cell(170,40,'Product Name',1,0,'C');
		$this->Cell(150,20,'Ship',1,0,'C');
		$this->Cell(150,20,'Recieve',1,0,'C');
		$this->Ln();
		$this->Cell(240,20,'',0);
		$this->Cell(50,20,'Qty(Box)',1,0,'C');
		$this->Cell(50,20,'Qty(Pcs)',1,0,'C');
		$this->Cell(50,20,'Total(Pcs)',1,0,'C');
		$this->Cell(50,20,'Qty(Box)',1,0,'C');
		$this->Cell(50,20,'Qty(Pcs)',1,0,'C');
		$this->Cell(50,20,'Total(Pcs)',1,0,'C');


		$this->Ln();
		$this->SetFont("", "", 10);

		$table= $this->data;
		$i=0;$total=0;$terkirim=0;$sisa=0;
		foreach($table as $row) {
			$i++;
			$this->Cell(20,20,$i,1);
			$this->Cell(50,20,$row->ID_PRODUCT,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->QTY_PL,1,0,'R');
			$this->Cell(50,20,$row->QTY_PCS,1,0,'R');
			$this->Cell(50,20,($row->QTY_PL*$row->BOX)+$row->QTY_PCS,1,0,'R');

			$this->Cell(50,20,$row->QTY_GR,1,0,'R');
			$this->Cell(50,20,$row->PCS,1,0,'R');
			$this->Cell(50,20,($row->QTY_GR*$row->BOX)+$row->PCS,1,0,'R');

			$this->Ln();

		}
		$this->Ln();
		$mst = $this->mst;
		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Remarks :'.$mst->REMARK, 0, 0, 'L', false);
		$this->Ln(70);


	}



	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(20,40,'No',1,0,'C');
		$this->Cell(50,40,'Product',1,0,'C');
		$this->Cell(120,40,'Product Name',1,0,'C');
		$this->Cell(180,20,'Ship',1,0,'C');
		$this->Cell(180,20,'Recieve',1,0,'C');
		$this->Ln();
		$this->Cell(190,20,'',0);
		$this->Cell(60,20,'Qty(Box)',1,0,'C');
		$this->Cell(60,20,'Qty(Pcs)',1,0,'C');
		$this->Cell(60,20,'Total',1,0,'C');
		$this->Cell(60,20,'Qty(Box)',1,0,'C');
		$this->Cell(60,20,'Qty(Pcs)',1,0,'C');
		$this->multiCell(60,20,'Total',1,0);


	}

	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(20,50,120,60,60,60,60,60,60));
		$this->SetAligns(array('C','C','C','R','R','R','R','R','R'));
		$no = 1;
		foreach ($this->data as $row) {
			$this->Row(
				array($no++,
					$row['ID_PRODUCT'],
					$row['PRODUCT_NAME'],
					number_format($row['QTY_PL']) ,
					number_format($row['QTY_PCS']) ,
					number_format($row['QTY_PL']*$row['BOX']+$row['QTY_PCS']),
					number_format($row['QTY_GR']),
					number_format($row['PCS']),
					number_format($row['QTY_GR']*$row['BOX']+$row['PCS'])

				), 15, 15
			);

		}
		$this->Ln();
		$mst = $this->mst;
		$this->SetFont("", "B", 10);
		$this->Cell(20, 13, 'Remarks :'.$mst->REMARK, 0, 0, 'L', false);
		$this->Ln(70);
		$this->bawah();


	}




	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-35);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');

		$this->SetY(-25);
		$this->SetFont('Courier','I',10);

		$this->Cell(0,10,$mst->NO_GR,0,0,'L');

		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_do_gudang extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'Letter' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(),$data_KW=array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->kw =$data_KW;
			$this->as =$as;
	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'DELIVERY ORDER ANTAR GUDANG' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();


		$this->Ln(5);
		$this->SetFont("", "B", 10);

		$this->Cell(20,$h,'Do Number' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->NO_DO_GUDANG , 0, 0, 'L' ,false);
//		$this->SetFont("", "B", 10);
//		$this->SetX(350);$this->Cell(0, $h, 'To' , 0, 0,'L',false );
//		$this->SetFont("", "", 10);
//		$this->SetX(400); $this->Cell(150, $h,': '.$mst->CODE_STOCKIES , 0, 0, 'L' ,false);
		$this->Ln(12);
//
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Create Date' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->TANGGAL_GUDANG , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Warehouse asal' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->ASAL , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Warehouse tujuan' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->TUJUAN , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);

		$this->Ln(30);

		$this->HeaderTable();
	//	$this->indent();



	}
	function BasicTable()
	{
		$table= $this->data;
		if($table!=null){
			$this->SetFont("", "B", 10);

			// Header
			$this->Cell(80,20,'No',1);
			$this->Cell(170,20,'Product Name',1);
			$this->Cell(50,20,'Rack',1);
			$this->Cell(50,20,'Qty',1);
			$this->Cell(50,20,'SubTotal',1);

			$this->Ln();
			$this->SetFont("", "", 10);

			$i=0;$total=0;$terkirim=0;$sisa=0;
			foreach($table as $row) {


				$i++;
				$this->Cell(80,20,$i,1);
				$this->Cell(170,20,$row->PRODUCT_NAME,1);

				$this->Cell(50,20,$row->QTY." Pcs",1,0,'R');


				$this->Ln();
			//	$total=$total+$row->total;
				$terkirim=$terkirim+($row->QTY);
			//	$sisa=$sisa+$row->SISA;
			}



			// Data



			$this->Cell(350,20,'Total',1);
			//$this->Cell(50,20,$total,1);
			$this->Cell(50,20,$terkirim.'Pcs',1,0,'R');
			//$this->Cell(50,20,$sisa,1);
		}
		$this->Ln();


	}

	function indent()
	{

		$indent = $this->indent;

		if($indent!=null){

		$this->SetFont("", "B", 10);
		$this->Cell(20,13,'Indent : ' ,0,0,'L' ,false);
		$this->Ln();

		// Header
		$this->Cell(80,20,'No',1);
		$this->Cell(80,20,'Product',1);
		$this->Cell(170,20,'Product Name',1);
		$this->Cell(50,20,'Qty',1);


		$this->Ln();
		$this->SetFont("", "", 10);
		$i=0;$total=0;
		foreach($indent as $row) {
			$i++;
			$this->Cell(80,20,$i,1);
			$this->Cell(80,20,$row->ID_PRODUCT,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->QTY,1,0,'L');
			$terkirim=$terkirim+($tots);
			$this->Ln();
		}

		// Data

		$this->Cell(350,20,'Sub Total',1);
			$this->Cell(50,20,$total.'Pcs',1,0,'R');
		$this->Ln(30);
		}
	}


	function bawah()
	{

		$this->SetFont("", "B", 10);

		$this->Ln();



		$this->Ln(100);

		$this->SetFont("", "B", 10);
		$this->Cell(175, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
		$this->Cell(185, 13, '(....................) ', 0, 0, 'L', false);
	}

	public function HeaderTable()
	{

		if($this->data!=null){
		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();
		$this->Cell(20,26,'No' ,1,0,'C' );
		$this->SetXY($left += 20,$Z); $this->Cell(300, 26, 'Product Name' , 1, 0, 'C');
		$this->SetXY($left += 300,$Z); $this->MultiCell(90, 26, 'Qty' , 1);

		}

	}

	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$KELUAR = 0;
			$masuk = 0;
			$awal = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(20, 300, 90));
			$this->SetAligns(array('L', 'L', 'R'));
			$no = 1;

			$terkirim = 0;
			foreach ($this->data as $row) {
				$this->Row(
					array($no++,
						$row['PRODUCT_NAME'],

						number_format($row['QTY']) .' Pcs',


					), 15, 15
				);
				$terkirim = $terkirim + ($row['QTY']);
			}
			$this->SetFont("", "", 10);
			$this->Cell(320, 26, 'Ending Balance', 1);
//			$this->Cell(50,26,number_format($awal).' Pcs',1,0,'R');
//			$this->Cell(50,26,number_format($masuk).' Pcs',1,0,'R');
//			$this->Cell(50,26,number_format($KELUAR).' Pcs',1,0,'R');
			//$this->Cell(70,26,'',1);

			$this->Cell(90, 26, number_format($terkirim)." Pcs", 1, 0, 'R');


			$this->Ln();


		}
		$mst = $this->mst;

		$this->SetFont("", "B", 10);
		$this->Cell(20,26,'Remark' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, 26,': '.$mst->REMARK , 0, 0, 'L' ,false);
		$this->SetFont("", "", 10);
		$this->Ln(12);

		$this->bawah();
	}



	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-35);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');
		$this->SetY(-25);
		$this->SetFont('Courier','I',10);

		$this->Cell(0,10,$mst->NO_DO_GUDANG,0,0,'L');

		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
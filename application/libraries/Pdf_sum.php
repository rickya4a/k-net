<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_sum extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->as = $as;
	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("", "", 16);
		$this->SetX(0);
		$this->Cell(0, 0, 'REPORT SUMMARY' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		//$awal=$this->tgl_awal;
		//$akhir=$this->tgl_akhir;

		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 8);
		$left = $this->GetX();


		$this->Ln(30);
		$this->SetFont("", "B", 8);

//		$this->Cell(20,$h,'Tanggal' ,0,0,'L' ,false);
		$this->SetFont("", "", 8);
		//$this->SetX(150); $this->Cell(100, $h, ": ".$awal." s/d ".$akhir , 0, 0, 'L' ,false);
		$this->ln();
		$this->HeaderTable();

		//$this->bawah();


	}
	function BasicTable()
	{

		$table= $this->data;
		//print_r($table);
		if($table!=null){
			$this->SetFont("", "B", 8);

			// Header
			$this->Cell(100,20,'Product Code',1);
			$this->Cell(170,20,'Product Name',1);
			$this->Cell(50,20,'Qty(pcs)',1);

			$this->Ln();
			$this->SetFont("", "", 8);

			$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
			foreach($table as $row) {
//				if($row->CATEGORY==0)
//				{$sat=' box';}
//				else
//				{$sat=' pcs';}

				//$i++;
				$this->Cell(100,20,$row->ID_PRODUCT,1);
				$this->Cell(170,20,$row->PRODUCT_NAME,1);
				$this->Cell(50,20,number_format($row->QTY),1,0,'R');

				$this->Ln();
				//$masuk=$masuk+($row->MASUK*$row->satuan);
			//	$awal=$awal+($row->AWAL*$row->SATUAN);

			//	$KELUAR=$KELUAR+($row->KELUAR*$row->SATUAN);
			//	$total=$total+($row->TOTAL*$row->SATUAN);
			//	$sisa=$sisa+$row->SISA;
			}



			// Data



			//$this->Cell(310,20,'Total',1);
		//	$this->Cell(50,20,$awal.' Pcs',1);
		//	$this->Cell(50,20,$masuk.' Pcs',1);
		//	$this->Cell(50,20,$KELUAR.' Pcs',1);
		//	$this->Cell(50,20,'',1);

			//$this->Cell(50,20,$masuk.' Pcs',1);

		}
		$this->Ln();


	}



	function bawah()
	{
		$this->SetFont("", "B", 8);
		$this->Cell(20, 13, 'Branch/Stockies must examine the products at the time of delivery ', 0, 0, 'L', false);
		$this->Ln(70);

		$this->SetFont("", "B", 8);
		$this->Cell(150, 13, '1....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '2....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '3....................... ', 0, 0, 'L', false);
		$this->Cell(150, 13, '4....................... ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(150, 13, '	Disiapkan', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Dipacking', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Diperiksa', 0, 0, 'L', false);
		$this->Cell(150, 13, '	Penerima', 0, 0, 'L', false);
		$this->Ln();

		$this->SetFont("", "B", 8);
		$this->Cell(20, 13, '*Putih: Stockiest/Penerima *Merah: Ekspedisi *Kuning: Gudang ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'DO Parent: ', 0, 0, 'L', false);
		$this->Ln();
		$this->Cell(20, 13, 'Receipt Number: ', 0, 0, 'L', false);
		$this->Ln();

	}


	public function HeaderTable()
	{
	if($this->data!=null) {
		$h = 13;
		$left = 10;
		$top = 80;
		$Z = $this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();


		$this->Cell(20, 13, 'No', 1, 0, 'C');
		$this->SetXY($left += 20, $Z);
		$this->Cell(100, 13, 'Product Code', 1, 0, 'C');
		$this->SetXY($left += 100, $Z);
		$this->Cell(250, 13, 'Product Name', 1, 0, 'C');

		$this->SetXY($left += 250, $Z);
		$this->MultiCell(70, 13, 'Qty', 1);
	}
	}

	function rptDetailData()
	{
		if($this->data!=null) {
			$border = 0;
			$this->AddPage();
			$this->SetAutoPageBreak(true, 60);
			$this->AliasNbPages();
			$left = 25;

			//header

			//$this->Ln();
			$table = $this->data;
			//print_r($table);

			$this->SetFont("Courier", "", 10);
			$i = 0;
			$total = 0;
			$total2 = 0;
			$masuk = 0;
			$awal = 0;
			$this->SetFont('', '', 10);
			$this->SetWidths(array(20, 100, 250, 70));
			$this->SetAligns(array('C', 'C', 'C', 'R'));
			$no = 1;


			foreach ($this->data as $row) {


				$this->Row(
					array(
						$no++,
						$row['ID_PRODUCT'],
						$row['PRODUCT_NAME'],

						number_format($row['QTY'])
					), 15, 15
				);
				$total = $total + $row['QTY'];
				//$total2=$total2+($row['QTY']*$BOX);
				//$awal=$awal+($row->QTY*$BOX);

			}
			$this->Cell(370, 20, ' Total', 1);
			//$this->Cell(50,20,$total.' box',1);
			$this->Cell(70, 20, number_format($total), 1, 0, 'R');

			$this->Ln();
		}
		//$this->bawah();
	}

	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-25);
		$this->SetFont("Courier", "", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
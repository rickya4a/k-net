<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_MS_report extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->as = $as;
	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'MOVING STOCK REPORT' , 0, 1,'C' );
		$this->Ln(30);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "", 10);
		$left = $this->GetX();




		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'No Moving Stock' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->NO_MOVING_STOCK , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Nama Warehouse" ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h, ": ".$mst->WAREHOUSE_NAME , 0, 0, 'L' ,false);

		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Tanggal Moving Stock' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->MOVING_STOCK_DATE , 0, 0, 'L' ,false);




		$this->Ln(30);

		$this->HeaderTable();


//		$this->indent();
	//	$this->bawah();


	}
	function BasicTable()
	{
		$this->SetFont("", "B", 10);

		// Header
		$this->Cell(75,20,'Rack Asal',1);
		$this->Cell(75,20,'Location Tujuan',1);
		$this->Cell(150,20,'Produk',1);

		$this->Cell(50,20,'Qty',1);
		$this->Cell(50,20,'Total',1);


		$this->Ln();
		$this->SetFont("", "", 10);

		$table= $this->data;
		$i=0;$total=0;$terkirim=0;$sisa=0;
		foreach($table as $row) {
			$i++;
			$this->Cell(75,20,$row->ASAL,1);
			$this->Cell(75,20,$row->TUJUAN,1);
			$this->Cell(150,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->QTY_BOX." Box",1,0,'R');
			$this->Cell(50,20,$row->QTY_PIECE." Pcs",1,0,'R');


			$this->Ln();
			$total=$total+$row->QTY_BOX;
			$terkirim=$terkirim+$row->QTY_PIECE;
//			$sisa=$sisa+$row->SISA;
		}



		// Data

//
//
		$this->Cell(300,20,'Sub Total',1);
		$this->Cell(50,20,$total.' Box',1,0,'R');
		$this->Cell(50,20,$terkirim.' Pcs',1,0,'R');
//		$this->Cell(50,20,$sisa,1);

		$this->Ln();


	}

	function indent()
	{

		$indent = $this->indent;


		$this->SetFont("", "B", 10);
		$this->Cell(20,13,'Indent : ' ,0,0,'L' ,false);
		$this->Ln();

		// Header
		$this->Cell(80,20,'No',1);
		$this->Cell(80,20,'Product',1);
		$this->Cell(170,20,'Product Name',1);
		$this->Cell(50,20,'Qty',1);


		$this->Ln();
		$this->SetFont("", "", 10);
		$i=0;$total=0;
		foreach($indent as $row) {
			$i++;
			$this->Cell(80,20,$i,1);
			$this->Cell(80,20,$row->ID_PRODUCT,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->total,1);
			$total=$total+$row->total;
			$this->Ln();
		}

		// Data

		$this->Cell(330,20,'Sub Total',1);
		$this->Cell(50,20,$total,1);
		$this->Ln(30);

	}


	function bawah()
	{
		$mst = $this->mst;
		$this->Ln(70);
		$h = 13;

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Gross Weight (Kg)' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h, ": ".$mst->GROSS , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Shipping Marks" ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h, ": " , 0, 0, 'L' ,false);

		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Net Weight (Kg)' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->NET, 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Address" ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h, ": " , 0, 0, 'L' ,false);


		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Total Measurement (m3)' ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(150); $this->Cell(150, $h,': '.$mst->TOTAL, 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Signature" ,0,0,'L' ,false);
		$this->SetFont("", "", 10);
		$this->SetX(400); $this->Cell(150, $h, ": " , 0, 0, 'L' ,false);


		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h, date("Y-m-d h:i:sa") ,0,0,'L' ,false);

	}


	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);


		$left = $this->GetX();

		$this->Cell(20,26,'No' ,1,0,'C' );
		$this->SetXY($left += 20,$Z); $this->Cell(100, 26, 'Rack Asal' , 1, 0, 'C');
		$this->SetXY($left += 100,$Z); $this->Cell(100, 26, 'Location Tujuan' , 1);
		$this->SetXY($left += 100,$Z); $this->Cell(150, 26, 'Produk' , 1, 0, 'C');
		$this->SetXY($left += 150,$Z); $this->Cell(50, 26, 'Qty' , 1);
		$this->SetXY($left += 50,$Z); $this->MultiCell(50, 26, 'Total' , 1);


	}

	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;$terkirim=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(20,100,100,150,50,50));
		$this->SetAligns(array('C','C','C','C','R','R'));
		$no = 1;
		foreach ($this->data as $row) {
			$this->Row(
				array(
					$no++,
					$row['ASAL'],
					$row['TUJUAN'],
					$row['PRODUCT_NAME'],

					number_format($row['QTY_BOX']).'Box' ,
					number_format($row['QTY_PIECE']).'Pcs' ,


				), 15, 15
			);
			$total=$total+$row['QTY_BOX'];
			$terkirim=$terkirim+$row['QTY_PIECE'];
		}

		$this->Ln();


	}


	public function Footer() {
		$mst = $this->mst;

		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-35);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');



		$this->SetY(-25);
		$this->SetFont('Courier','I',10);

		$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');

		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_MOVING_STOCK,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
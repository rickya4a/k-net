<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//untuk Header Report Driver Management
class Pdf_PL_report extends Autowraptable
{

	public $options = array(
	  		'filename'  => '' ,
	  		'destinationfile'  => '' ,
	  		'paper_size' =>'F4' ,
	  		'orientation' =>'P' 
	  	);
		
    function __construct($data_mst = array(), $data_pdf = array(),$data_indent = array(), $options = array(), $as=''){
	    	$this->data = $data_pdf;
	    	$this->options = $options;
			$this->mst = $data_mst;
			$this->indent = $data_indent;
			$this->as = $as;

	}
	
	public function Header()
	{
		$mst = $this->mst;	
		$left = 0;
		$this->SetFont("Courier", "", 24);
		$this->SetX(0);
		$this->Cell(0, 0, 'PACKING LIST REPORT' , 0, 1,'C' );
		$this->Ln(50);


		$this->HeaderMst();
	}
	
	public function HeaderMst()
	{

		$mst = $this->mst;
		
		$h = 13;
		$left = 10;
		$left2 = 10;
		$left3 = 10;
		$left4 = 10;
		$top = 80;
		#tableheader
		$this->SetFont("", "B", 10);
		$left = $this->GetX();




		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Exporter' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(100); $this->Cell(200, $h, ": ".$mst->EXPORTER , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Buyer's Order" ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(400); $this->Cell(150, $h, ": ".$mst->BUYER_ORDER , 0, 0, 'L' ,false);

		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Consignee' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(100); $this->Cell(200, $h,': K-link Indonesia', 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"No Packing List" ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(400); $this->Cell(150, $h, ": ".$mst->NO_PL , 0, 0, 'L' ,false);


		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Invoice Date' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(100); $this->Cell(200, $h,': '.$mst->INVOICE_DATE , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Carriage By" ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(400); $this->Cell(150, $h, ": ".$mst->CARRIAGE_BY , 0, 0, 'L' ,false);


		$this->Ln(12);


		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Invoice No' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(100); $this->Cell(150, $h,': '.$mst->NO_INVOICE , 0, 0, 'L' ,false);

		$this->Ln(12);





		$this->Ln(30);

		$this->HeaderTable();

//		$this->indent();



	}
	function BasicTable()
	{
		$this->SetFont("", "B", 10);

		// Header
		$this->Cell(30,20,'No CTN',1,0,'C');
		$this->Cell(60,20,'No PO',1,0,'C');
		$this->Cell(50,20,'Product',1,0,'C');
		$this->Cell(170,20,'Product Name',1,0,'C');
		$this->Cell(35,20,'Qty PO',1,0,'C');
		$this->Cell(35,20,'Qty/box',1,0,'C');
		$this->Cell(30,20,'(box)',1,0,'C');
		$this->Cell(30,20,'(pcs)',1,0,'C');
		$this->Cell(30,20,'TOTAL',1,0,'C');

		$this->Ln();
		$this->SetFont("", "B", 10);

		$table= $this->data;
		$i=0;$total=0;$terkirim=0;$sisa=0;
		foreach($table as $row) {
			$i++;
			$this->Cell(30,20,$row->CARTON,1);
			$this->Cell(60,20,$row->NO_PO,1);
			$this->Cell(50,20,$row->PRODUCT_CODE,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(35,20,$row->QTY,1,0,'R');
			$this->Cell(35,20,$row->BOX,1,0,'R');
			$this->Cell(30,20,$row->QTY_BOX,1,0,'R');
			$this->Cell(30,20,$row->QTY_PCS,1,0,'R');
			$this->Cell(30,20,($row->QTY_BOX*$row->BOX)+$row->QTY_PCS,1,0,'R');

			$this->Ln();

		}



		// Data

//
//
//		$this->Cell(330,20,'Sub Total',1);
//		$this->Cell(50,20,$total,1);
//		$this->Cell(50,20,$terkirim,1);
//		$this->Cell(50,20,$sisa,1);

		$this->Ln();


	}

	function indent()
	{

		$indent = $this->indent;


		$this->SetFont("", "B", 10);
		$this->Cell(20,13,'Indent : ' ,0,0,'L' ,false);
		$this->Ln();

		// Header
		$this->Cell(80,20,'No',1);
		$this->Cell(80,20,'Product',1);
		$this->Cell(170,20,'Product Name',1);
		$this->Cell(50,20,'Qty',1);


		$this->Ln();
		$this->SetFont("", "B", 10);
		$i=0;$total=0;
		foreach($indent as $row) {
			$i++;
			$this->Cell(80,20,$i,1);
			$this->Cell(80,20,$row->ID_PRODUCT,1);
			$this->Cell(170,20,$row->PRODUCT_NAME,1);
			$this->Cell(50,20,$row->total,1);
			$total=$total+$row->total;
			$this->Ln();
		}

		// Data

		$this->Cell(330,20,'Sub Total',1);
		$this->Cell(50,20,$total,1);
		$this->Ln(30);

	}


	function bawah()
	{
		$mst = $this->mst;
		$this->Ln(70);
		$h = 13;

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Gross Weight (Kg)' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(150); $this->Cell(150, $h, "  : ".number_format($mst->GROSS) , 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Shipping Marks" ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(400); $this->Cell(150, $h, ": " , 0, 0, 'L' ,false);

		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Net Weight (Kg)' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(150); $this->Cell(150, $h,'  : '.number_format($mst->NET), 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Address" ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(400); $this->Cell(150, $h, ": " , 0, 0, 'L' ,false);


		$this->Ln(12);

		$this->SetFont("", "B", 10);
		$this->Cell(20,$h,'Total Measurement (m3)  ' ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(150); $this->Cell(150, $h,'  : '.$mst->TOTAL, 0, 0, 'L' ,false);
		$this->SetFont("", "B", 10);
		$this->Cell(350,$h,"Signature" ,0,0,'L' ,false);
		$this->SetFont("", "B", 10);
		$this->SetX(400); $this->Cell(150, $h, ": " , 0, 0, 'L' ,false);


		$this->Ln(12);
		$this->SetFont("", "B", 10);
		$this->Cell(20,$h, date("Y-m-d h:i:sa") ,0,0,'L' ,false);

	}

	public function HeaderTable()
	{

		$h = 13;
		$left = 10;
		$top = 80;
		$Z=$this->getY();
		#tableheader
		$this->SetFont("", "B", 10);
		//$this->SetFillColor(200,200,200);
		$left = $this->GetX();

		$this->Cell(35, 26, 'No CTN' , 1, 0, 'C');
		$this->SetXY($left += 35,$Z); $this->Cell(80, 26, 'No PO' , 1, 0, 'C');
		$this->SetXY($left += 80,$Z); $this->Cell(60, 26, 'Produk' , 1, 0, 'C' );
		$this->SetXY($left += 60,$Z); $this->Cell(140, 26, 'Product Name' , 1, 0, 'C' );
		$this->SetXY($left += 140,$Z); $this->Cell(50, 26, 'Qty PO' ,1, 0, 'C' );
		$this->SetXY($left += 50,$Z); $this->Cell(50, 26, 'Qty/box' , 1, 0, 'C' );
		$this->SetXY($left += 50,$Z); $this->Cell(45, 26, '(box)' , 1, 0, 'C' );
		$this->SetXY($left += 45,$Z); $this->Cell(45, 26, '(pcs)' , 1, 0, 'C' );
		$this->SetXY($left += 45,$Z); $this->MultiCell(50, 26, 'Total' , 1);




	}

	function rptDetailData()
	{
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header

		//$this->Ln();
		$table= $this->data;
		//print_r($table);

		$this->SetFont("Courier", "", 10);
		$i=0;$total=0;$KELUAR=0;$masuk=0;$awal=0;
		$this->SetFont('' ,'' ,10);
		$this->SetWidths(array(35,80,60,140,50,50,45,45,50));
		$this->SetAligns(array('C','C','C','C','R','R','R','R','R'));
		$no = 1;



		foreach ($this->data as $row) {
			$QTYBOX=$row['QTY_BOX'];
			$QTYPCS=$row['QTY_PCS'];
			$this->Row(

				array(
					$row['CARTON'],
					$row['NO_PO'],
					$row['PRODUCT_CODE'],
					$row['PRODUCT_NAME'],

					number_format($row['QTY'])  ,
					number_format($row['BOX']) ,
					number_format($row['QTY_BOX']) ,
					number_format($row['QTY_PCS']) ,
					number_format(($row['QTY_BOX']*$row['BOX'])+$row['QTY_PCS'])
				), 15, 15
			);
			$awal=$awal+(($row['QTY_BOX']*$row['BOX'])+$row['QTY_PCS']);

		}
		$this->SetFont("", "B", 10);
		$this->Cell(505,26,'Total',1);
		$this->Cell(50,26,number_format($awal),1,0,'R');

		$this->Ln();

		$this->bawah();
	}
	public function Footer() {
		$mst = $this->mst;
		$as = $this->as;
		//$as=$this->session->userdata('NAME');
		$this->SetY(-35);
		$this->SetFont("Courier", "I", 10);


		//foreach($name as $row) {
		$this->Cell(0,10,($as),0,0,'L');

		$this->SetY(-25);
		$this->SetFont('Courier','I',10);

		$this->Cell(0,10,$mst->NO_PL,0,0,'L');

		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Courier','I',10);
		// Document Time
		$this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
		//$this->Cell(0,10,$mst->NO_PL,0,0,'L');
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   class Autowraptable extends FPDF {

	function __construct()
	{
		$this->ones = array( "", " one", " two", " three", " four", " five", " six", " seven", " eight", " nine", " ten", " eleven", " twelve", " thirteen", " fourteen", " fifteen", " sixteen", " seventeen", " eighteen", " nineteen" );
		$this->tens = array( "", "", " twenty", " thirty", " forty", " fifty", " sixty", " seventy", " eighty", " ninety" );
		$this->triplets = array( "", " thousand", " million", " billion", " trillion", " quadrillion", " quintillion", " sextillion", " septillion", " octillion", " nonillion" );
	}

	public function Footer() {
      //Position at 1.5 cm from bottom
      $this->SetY(-15);
      //Arial italic 8
      $this->SetFont('Arial','I',8);
      // Document Time
      $this->Cell(0,10,'Generated on '. date("Y/m/d H:i:s"),0,0,'L');
      //Page number
      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
  	}

	public function printPDF ($js = '') {

		if ($this->options['paper_size' ] == "F4") {
			$a = 8.3 * 72; //1 inch = 72 pt
			$b = 13.0 * 72;
			$this->FPDF($this->options['orientation' ], "pt", array($a,$b));
		} else {
			$this->FPDF($this->options['orientation' ], "pt", $this->options['paper_size' ]);
		}

	    $this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("helvetica", "B", 10);
	    //$this->AddPage();

	    $this->rptDetailData();

	    if($js != ''){
	 		$this->AutoPrint(true);
		    $this->Output();
	    }else{
	    	$this->Output($this->options['filename' ],$this->options['destinationfile' ]);
	    }

  	}

  	private $widths;
	private $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data,$t=12,$l=14)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=$t*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L' ;
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,$l,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw' ];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'' ,$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ' )
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}

	var $javascript;
    var $n_js;

	function IncludeJS($script) {
        $this->javascript=$script;
    }

    function _putjavascript() {
        $this->_newobj();
        $this->n_js=$this->n;
        $this->_out('<<');
        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
        $this->_out('>>');
        $this->_out('endobj');
        $this->_newobj();
        $this->_out('<<');
        $this->_out('/S /JavaScript');
        $this->_out('/JS '.$this->_textstring($this->javascript));
        $this->_out('>>');
        $this->_out('endobj');
    }

    function _putresources() {
        parent::_putresources();
        if (!empty($this->javascript)) {
            $this->_putjavascript();
        }
    }

    function _putcatalog() {
        parent::_putcatalog();
        if (isset($this->javascript)) {
            $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
        }
    }

	function AutoPrint($dialog=false)
	{
	    //Embed some JavaScript to show the print dialog or start printing immediately
	    $param=($dialog ? 'true' : 'false');
	    $script="print($param);";
	    $this->IncludeJS($script);
	}

	function terbilang_get_valid($str,$from,$to,$min=1,$max=9){
		$val=false;
		$from=($from<0)?0:$from;
		for ($i=$from;$i<$to;$i++){
			if (((int) $str{$i}>=$min)&&((int) $str{$i}<=$max)) $val=true;
		}
		return $val;
	}

	function terbilang_get_str($i,$str,$len){
		$numA=array("","SATU","DUA","TIGA","EMPAT","LIMA","ENAM","TUJUH","DELAPAN","SEMBILAN");
		$numB=array("","SE","DUA ","TIGA ","EMPAT ","LIMA ","ENAM ","TUJUH ","DELAPAN ","SEMBILAN ");
		$numC=array("","SATU ","DUA ","TIGA ","EMPAT ","LIMA ","ENAM ","TUJUH ","DELAPAN ","SEMBILAN ");
		$numD=array(0=>"PULUH",1=>"BELAS",2=>"RATUS",4=>"RIBU", 7=>"JUTA", 10=>"MILYAR", 13=>"TRILIUN");
		$buf="";
		$pos=$len-$i;
		switch($pos){
			case 1:
					if (!$this->terbilang_get_valid($str,$i-1,$i,1,1))
						$buf=$numA[(int) $str{$i}];
				break;
			case 2:	case 5: case 8: case 11: case 14:
					if ((int) $str{$i}==1){
						if ((int) $str{$i+1}==0)
							$buf=($numB[(int) $str{$i}]).($numD[0]);
						else
							$buf=($numB[(int) $str{$i+1}]).($numD[1]);
					}
					else if ((int) $str{$i}>1){
							$buf=($numB[(int) $str{$i}]).($numD[0]);
					}
				break;
			case 3: case 6: case 9: case 12: case 15:
					if ((int) $str{$i}>0){
							$buf=($numB[(int) $str{$i}]).($numD[2]);
					}
				break;
			case 4: case 7: case 10: case 13:
					if ($this->terbilang_get_valid($str,$i-2,$i)){
						if (!$this->terbilang_get_valid($str,$i-1,$i,1,1))
							$buf=$numC[(int) $str{$i}].($numD[$pos]);
						else
							$buf=$numD[$pos];
					}
					else if((int) $str{$i}>0){
						if ($pos==4)
							$buf=($numB[(int) $str{$i}]).($numD[$pos]);
						else
							$buf=($numC[(int) $str{$i}]).($numD[$pos]);
					}
				break;
		}
		return $buf;
	}

	function toTerbilang($nominal){
		$buf="";
		$str=$nominal."";
		$len=strlen($str);
		for ($i=0;$i<$len;$i++){
			$buf=trim($buf)." ".$this->terbilang_get_str($i,$str,$len);
		}
		return trim($buf);
	}

	function convertNumber($number)
	{
	    list($integer, $fraction) = explode(".", (string) $number);

	    $output = "";

	    if ($integer{0} == "-")
	    {
	        $output = "negative ";
	        $integer    = ltrim($integer, "-");
	    }
	    else if ($integer{0} == "+")
	    {
	        $output = "positive ";
	        $integer    = ltrim($integer, "+");
	    }

	    if ($integer{0} == "0")
	    {
	        $output .= "ZERO";
	    }
	    else
	    {
	        $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
	        $group   = rtrim(chunk_split($integer, 3, " "), " ");
	        $groups  = explode(" ", $group);

	        $groups2 = array();
	        foreach ($groups as $g)
	        {
	            $groups2[] = $this->convertThreeDigit($g{0}, $g{1}, $g{2});
	        }

	        for ($z = 0; $z < count($groups2); $z++)
	        {
	            if ($groups2[$z] != "")
	            {
	                $output .= $groups2[$z] . $this->convertGroup(11 - $z) . (
	                        $z < 11
	                        && !array_search('', array_slice($groups2, $z + 1, -1))
	                        && $groups2[11] != ''
	                        && $groups[11]{0} == '0'
	                            ? " AND "
	                            : " "
	                    );
	            }
	        }

	        $output = rtrim($output, ", ");
	    }

	    if ($fraction > 0)
	    {
	        $output .= " point";
	        for ($i = 0; $i < strlen($fraction); $i++)
	        {
	            $output .= " " . $this->convertDigit($fraction{$i});
	        }
	    }

	    return $output;
	}

	function convertGroup($index)
	{
	    switch ($index)
	    {
	        case 11:
	            return " DECILION";
	        case 10:
	            return " NONILLION";
	        case 9:
	            return " OCTILLION";
	        case 8:
	            return " SEPTILLION";
	        case 7:
	            return " SEXTILLION";
	        case 6:
	            return " QUINTRILLION";
	        case 5:
	            return " QUADRILLION";
	        case 4:
	            return " TRILLION";
	        case 3:
	            return " BILLION";
	        case 2:
	            return " MILLION";
	        case 1:
	            return " THOUSAND";
	        case 0:
	            return "";
	    }
	}

	function convertThreeDigit($digit1, $digit2, $digit3)
	{
	    $buffer = "";

	    if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
	    {
	        return "";
	    }

	    if ($digit1 != "0")
	    {
	        $buffer .= $this->convertDigit($digit1) . " HUNDRED";
	        if ($digit2 != "0" || $digit3 != "0")
	        {
	            $buffer .= " ";
	        }
	    }

	    if ($digit2 != "0")
	    {
	        $buffer .= $this->convertTwoDigit($digit2, $digit3);
	    }
	    else if ($digit3 != "0")
	    {
	        $buffer .= $this->convertDigit($digit3);
	    }

	    return $buffer;
	}

	function convertTwoDigit($digit1, $digit2)
	{
	    if ($digit2 == "0")
	    {
	        switch ($digit1)
	        {
	            case "1":
	                return "TEN";
	            case "2":
	                return "TWENTY";
	            case "3":
	                return "THIRTY";
	            case "4":
	                return "FORTY";
	            case "5":
	                return "FIFTY";
	            case "6":
	                return "SIXTY";
	            case "7":
	                return "SEVENTY";
	            case "8":
	                return "EIGHTY";
	            case "9":
	                return "NINETY";
	        }
	    } else if ($digit1 == "1")
	    {
	        switch ($digit2)
	        {
	            case "1":
	                return "ELEVEN";
	            case "2":
	                return "TWELVE";
	            case "3":
	                return "THIRTEEN";
	            case "4":
	                return "FOURTEEN";
	            case "5":
	                return "FIFTEEN";
	            case "6":
	                return "SIXTEEN";
	            case "7":
	                return "SEVENTEEN";
	            case "8":
	                return "EIGHTEEN";
	            case "9":
	                return "NINETEEN";
	        }
	    } else
	    {
	        $temp = $this->convertDigit($digit2);
	        switch ($digit1)
	        {
	            case "2":
	                return "TWENTY $temp";
	            case "3":
	                return "THIRTY $temp";
	            case "4":
	                return "FORTY $temp";
	            case "5":
	                return "FIFTY $temp";
	            case "6":
	                return "SIXTY $temp";
	            case "7":
	                return "SEVENTY $temp";
	            case "8":
	                return "EIGHTY $temp";
	            case "9":
	                return "NINETY $temp";
	        }
	    }
	}

	function convertDigit($digit)
	{
	    switch ($digit)
	    {
	        case "0":
	            return "ZERO";
	        case "1":
	            return "ONE";
	        case "2":
	            return "TWO";
	        case "3":
	            return "THREE";
	        case "4":
	            return "FOUR";
	        case "5":
	            return "FIVE";
	        case "6":
	            return "SIX";
	        case "7":
	            return "SEVEN";
	        case "8":
	            return "EIGHT";
	        case "9":
	            return "NINE";
	    }
	 }

	function amount($nilai)
	{
		$split = explode('.',number_format($nilai,2,'.',''));
 		$whole = $this->convertNumber($split[0].".0");
 		$cents = $this->convertNumber($split[1].".0");

		if($cents != 'ZERO')
		   $cents = ' AND'.$cents.' ';
		else
		   $cents = '';

		$money = $whole.$cents;

		return $money;
	}
  }
?>
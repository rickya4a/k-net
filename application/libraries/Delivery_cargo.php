<?php

class Delivery_cargo {
	var $tipe_cargo = "";
	var $username = "";
	var $password = "";
	var $price_list_url = "";
	var $konos_url = "";
	var $tracking_url = "";
	
	var $olshopCustId = "80075400";
	
	var $provinsi;
	var $kabupaten;
	var $kecamatan;
	
	var $from;
	var $to;
	var $weight;
	var $totalItem;
	var $totalGoodsValue;
	var $fullAddress;
	var $receiverName;
	var $receiverPhone;
	var $senderPhone;
	
	var $stockistId;
	var $stockistName;
	var $kodeArea;
	
	var $serviceCode;
	var $serviceName;
	var $shippingPrice;
	var $branch;
	
	var $transactionId;
	var $noResi;
	
	var $tokenId;
	var $errorMsg;
	var $errorKonosLog;
	
	//1 = DATA SUDAH ADA DI ECOMM_TRANS_HDR ( UPDATE KONOT DI  ECOMM_TRANS_HDR )
	//2 = DATA BELUM ADA DI ECOMM_TRANS_HDR ( INSERT DATA KE ECOMM_TRANS_HDR DARI ECOMM_TRANS_HDR_SGO )
	var $updateKonosStatus;
	
	public function getErrorKonosLog() {
		return $this->errorKonosLog;
		//return $this;
	}
	
	public function getKonosStatus() {
		return $this->updateKonosStatus;
		//return $this;
	}
	
	public function getNoResi() {
		return $this->noResi;
	}
	
	public function setGoodsValue($newGoodsValue) {
		$this->totalGoodsValue = $newGoodsValue;
		return $this;
	}
	
	public function setSenderPhone($senderPhone) {
		$this->senderPhone = $senderPhone;
		return $this;
	}
	
	public function setTokenId($tokenId) {
		$this->tokenId = $tokenId;
		return $this;
	}
	
	public function setReceiverName($receiverName) {
		$this->receiverName = $receiverName;
		return $this;
	}
	
	public function setTransactionId($transactionId) {
		$this->transactionId = $transactionId;
		return $this;
	}
	
	public function setTotalItem($totalItem) {
		$this->totalItem = $totalItem;
		return $this;
	}
	
	public function setFullAddress($fullAddress) {
		$this->fullAddress = $fullAddress;
		return $this;
	}
	
	public function setReceiverPhone($receiverPhone) {
		$this->receiverPhone = $receiverPhone;
		return $this;
	}
	
	public function getTotalItem() {
		return $this->totalItem;
	}
	
	public function getServiceCode() {
		return $this->serviceCode;
	}
	
	public function getShippingPrice() {
		return $this->shippingPrice;
	}
	
	public function setStockist($newStockist) {
		$this->stockistId = $newStockist;
		return $this;
	}
	
	public function setStockistName($newStockist) {
		$this->stockistName = $newStockist;
		return $this;
	}
	
	public function getStockist() {
		return $this->stockistId;
		
	}
	
	public function getErrorMessage() {
		return $this->errorMsg;
	}
	
	public function setKodeArea($newKodeArea) {
		$this->kodeArea = $newKodeArea;
		return $this;
	}
	
	public function getKodeArea() {
		return $this->kodeArea;
	}
	
	public function setFrom($newFrom = null) {
		$this->from = $newFrom;
		return $this;
	}
	
	public function setTo($newFrom = null) {
		$this->to = $newFrom;
		return $this;
	}
	
	public function setWeight($newFrom = null) {
		$this->weight = $newFrom;
		return $this;
	}
	
	public function setTipeCargo($newTipe) {
		$this->tipe_cargo = $newTipe;
		//echo "OKS : ".$this->tipe_cargo;
		//Jika Tipe Cargo = JNE
		if($this->tipe_cargo == "1") {
		    $this->tipe_cargo = "1";
			$this->username = "KLINK";
			$this->password = "76270305bef5d402220c96d59ac61977";
			$this->price_list_url = pricing_url("1"); //"http://apiv2.jne.co.id:10101/tracing/api/pricedev";
			//$this->konos_url = "http://api.jne.co.id:8889/tracing/klink/generateCnote";
			$this->konos_url = conot_url("1"); //"http://apiv2.jne.co.id:10101/tracing/api/generatecnote";
			$this->tracking_url = tracking_konot_url("1"); //"http://apiv2.jne.co.id:10101/tracing/api/list/cnote/";
		} else if($this->tipe_cargo == "2") {
		    $this->tipe_cargo = "2";
			$this->username = "k-link2";
			$this->password = "pusat@59A";
			$this->price_list_url = pricing_url("2");
			$this->konos_url = conot_url("2");
		}
		return $this;
	}
	
	public function getTipeCargo() {
		return $this->tipe_cargo;
	}
	
	public function getUsername() {
		return $this->username;
	}
	
	public function getPassword() {
		return $this->password;
	}
	
	public function getPricelistUrl() {
		return $this->price_list_url;
	}
	
	public function getKonosUrl() {
		return $this->konos_url;
	}
	
	public function printShippingConfig() {
	    echo "Cargo          : ".$this->tipe_cargo;
		echo "<br />sender   : ".$this->from;
		echo "<br />dest     : ".$this->to;
		echo "<br />branch   : ".$this->branch;
		echo "<br />Stockist   : ".$this->stockistId;
		echo "<br />Stockist Name   : ".$this->stockistName;
		echo "<br />Price   : ".$this->shippingPrice;
	}
	
	public function curlPriceListJNE() {
			$err = 0;	
			$from = "";
			$to = "";
			if(($this->from != "" || $this->from != null) && ($this->from != "" || $this->from != null)) {
				$from = $this->from;
				$to = $this->to;
			} else if(($this->stockistId != "" || $this->stockistId != null) && ($this->kodeArea != "" || $this->kodeArea != null)) {
				 $CI = & get_instance();
				 $CI->load->service("webshop/Shipping_service",'shippingService');
				 $resP = $CI->shippingService->showPricecodeByStockistIDX($this->stockistId, $this->kodeArea);
				 //print_r($resP);
				 if($resP['response'] == "true") {
				    $result = $resP['arrayData'];
					$from = $result[0]->kode_kec_JNE_Origin;
					$this->from = $result[0]->kode_kec_JNE_Origin;
				    $to = $result[0]->KEC_JNE;
					$this->to = $result[0]->KEC_JNE;
					$this->branch = $result[0]->jne_branchcd;  
					$this->stockistName = $result[0]->fullnm;
					$this->provinsi = $result[0]->provinsi;
					$this->kabupaten = $result[0]->kabupaten;
					$this->kecamatan = $result[0]->kecamatan;
				 }
			} else {
				$err++;
			}
	
	
	
			if($err == 0) {	
				$curl_post_data = array(
					"username" => urlencode($this->username),
					"api_key" => urlencode($this->password),
					"from" => urlencode($from),
					"thru" => urlencode($to),
					"weight" => urlencode($this->weight)
				);		
				
				$postData = setFieldPost($curl_post_data);
			    
				$curl = curl_init();
				$arx = array(
				  CURLOPT_URL => $this->price_list_url, 
				  CURLOPT_RETURNTRANSFER => true, 
				  CURLOPT_SSL_VERIFYPEER => true,
				  CURLOPT_CUSTOMREQUEST => "POST", 
				  CURLOPT_POSTFIELDS => $postData, 
				  CURLOPT_HTTPHEADER => array( 
					"Content-Type: application/x-www-form-urlencoded")
				  );
				curl_setopt_array($curl, $arx);
				$curl_response = curl_exec($curl);
				curl_close($curl);
				return $curl_response;
			} else {
				return array("error" => "1", "message" => "Param from / to / stockist ID / area should exist..");
			}
	}
	
	function getPriceCargo() {
	    if($this->tipe_cargo == "1") {
			$curl_post_data = array(
				"username" => urlencode($this->username),
				"api_key" => urlencode($this->password),
				"from" => urlencode($this->from),
				"thru" => urlencode($this->to),
				"weight" => urlencode($this->weight)
			);		
			
			$postData = setFieldPost($curl_post_data);
				
			$curl = curl_init();
			$arx = array(
			  CURLOPT_URL => $this->price_list_url, 
			  CURLOPT_RETURNTRANSFER => true, 
			  CURLOPT_SSL_VERIFYPEER => true,
			  CURLOPT_CUSTOMREQUEST => "POST", 
			  CURLOPT_POSTFIELDS => $postData, 
			  CURLOPT_HTTPHEADER => array( 
				"Content-Type: application/x-www-form-urlencoded")
			  );
			curl_setopt_array($curl, $arx);
			$curl_response = curl_exec($curl);
			curl_close($curl);
			return $curl_response;
		}
	}
	
	function setLowestJnePrice($arr) {
		$test = array();
		$arrx = null;
			foreach($arr->price as $dta) {
				if($dta->service_code != "OKE15" && $dta->service_code != "CTCOKE15") {
						$arrx = array(
						  "service_code" => $dta->service_code,
						  "service_display" => $dta->service_display,
						  "origin_name" => $dta->origin_name,
						  "destination_name" => $dta->destination_name,
						  "price" => $dta->price,
						  "etd_from" => $dta->etd_from,
						  "etd_thru" => $dta->etd_thru,
						  "times" => $dta->times,
						);
					array_push($test, $arrx);
				}
			}

			$lowest = $test[0]['price'];
			//echo "Harga pertama : $lowest<br />";
			foreach($test as $dta) {
				if($dta['price'] <= $lowest) {
					$lowest = $dta['price'];
					$arrx = array(
					  "service_code" => $dta['service_code'],
					  "service_display" => $dta['service_display'],
					  "origin_name" => $dta['origin_name'],
					  "destination_name" => $dta['destination_name'],
					  "price" => $dta['price'],
					  "etd_from" => $dta['etd_from'],
					  "etd_thru" => $dta['etd_thru'],
					  "times" => $dta['times']
					);
				}
			}

			return $arrx;
	}
	
	public function getPriceListShipping() {
		if($this->tipe_cargo == "1") {
			$resPrice = json_decode($this->curlPriceListJNE());
			//print_r($resPrice);
			   $arrx = array();
			   if($resPrice != null) {
					if(array_key_exists("status", $resPrice)) {
						return jsonFalseResponse("Error");
					} else {
						//if(getUserID() == "IDSPAAA66834") {
						$arrx = $this->setLowestJnePrice($resPrice);
						$this->shippingPrice = $arrx['price'];
						$this->serviceCode = $arrx['service_code'];
						return jsonTrueResponse($arrx);

					}
			   } else {
				return jsonFalseResponse("Pricelist area Ekspedisi JNE tidak tersedia");
			   }
		} else if($this->tipe_cargo == "2") {
		
		}
		
	}
	
	function updateProvKabupaten($temp_orderno) {
	    $CI = &get_instance();
		$qryTrx = $CI->load->database('db_ecommerce', true);
		
		$qry = "UPDATE b
				   set b.kabupaten_name = a.Kota_Kabupaten,
					   b.province_name = a.ProvinsiNama
				FROM ecomm_trans_shipaddr_sgo b
				INNER JOIN jne_Tabel_Zona_Induk a ON (b.kab_code = a.kode_kabupaten) 
				WHERE b.orderno = '$temp_orderno'
				and (a.ProvinsiNama is not null OR a.ProvinsiNama != '')
				and (Kota_Kabupaten is not null OR Kota_Kabupaten != '')";
		//echo $qry;
		$queryShpHc = $qryTrx->query($qry);
		if($queryShpHc > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function conoteGenerateFromDB() {
		/*if($this->tokenId == null || $this->tokenId == "") {
		    $this->errorMsg = "Token ID /  No trx sementara harus diisi..";
			return false;
		}*/
		
		if($this->transactionId == null || $this->transactionId == "") {
			$this->errorMsg = "Transaction ID harus diisi..";
			return false;
		}
		
		$CI = &get_instance();
		$qryTrx = $CI->load->database('db_ecommerce', true);
	    $qryShpHdr = "SELECT a.*, b.orderno as trx_id, b.conote_new, c.conoteJNE as noResi
						FROM ecomm_trans_shipaddr_sgo a 
						LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.token)
						LEFT JOIN ecomm_trans_shipaddr c ON (b.orderno = c.orderno)
						WHERE b.orderno = '".$this->transactionId."'";
		//echo $qryShpHdr;
		$queryShpHc = $qryTrx->query($qryShpHdr);
		$row = $queryShpHc->result();
		//print_r($row);
		
		
		if($row != null) {
		    if($row[0]->kabupaten_name == null || $row[0]->kabupaten_name == "" || $row[0]->province_name == null || $row[0]->province_name == "") {
					$upd = $this->updateProvKabupaten($row[0]->orderno);
					if($upd == true) {
						$this->conoteGenerateFromDB();
					} else {
						$this->errorMsg = "Nama Kota / Kabupaten / Provinsi belum ada";
						return false;
					}
				}
				
				/*if($row[0]->province_name == null || $row[0]->province_name == "") {
					$this->errorMsg = "Nama provinsi belum ada";
					return false;
				}*/
				
				if($row[0]->addr1 == null || $row[0]->addr1 == "") {
					$this->errorMsg = "Alamat Penerima belum ada";
					return false;
				}
				
				if($row[0]->receiver_name == null || $row[0]->receiver_name == "") {
					$this->errorMsg = "Nama Penerima barang belum ada";
					return false;
				}
				
				if($row[0]->shipper_telhp == null || $row[0]->shipper_telhp == "") {
					$this->errorMsg = "No Telp/HP Pengirim barang belum ada";
					return false;
				}
				
				if($row[0]->stockist_name == null || $row[0]->stockist_name == "") {
					$this->errorMsg = "Nama Stockist belum ada";
					return false;
				}
				
				if($row[0]->tel_hp1 == null || $row[0]->tel_hp1 == "") {
					$this->errorMsg = "No Telp/HP Penerima belum ada";
					return false;
				}
		
			    if($row[0]->noResi == null  || $row[0]->noResi == "") {

					$addr1 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 0, 19));
					$addr2 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 19, 19));
					$addr3 = str_replace(array("\n","\r"), '', substr($row[0]->addr1, 38, 19));
					
					$xhp = str_replace(array("\n","\r"), '', substr($row[0]->tel_hp1, 0, 14));
					$telhp = str_replace(array("-","+", " "), "", $xhp);
					
					$shipper_xhp = str_replace(array("\n","\r"), '', substr($row[0]->shipper_telhp, 0, 14));
					$shipper_telhp = str_replace(array("-","+", " "), '', $shipper_xhp);
					
					//$stockstr_replace(array(",","."), '', substr($row[0]->stockist_name)
					$stk_name = str_replace(array("\n","\r", ",", "."), '', substr($row[0]->stockist_name, 0, 19));
					$prov_name = str_replace(array("\n","\r"), '', substr($row[0]->province_name, 0, 19));
					$kabu = strtoupper("Kab.");
					$namaKota = str_replace($kabu, "", $row[0]->kabupaten_name);
					
					
								
					$curl_post_data = array(
						"username" => urlencode($this->username),
						"api_key" => urlencode($this->password),
						"OLSHOP_BRANCH" => urlencode($row[0]->jne_branch),
						"OLSHOP_CUST" => urlencode($this->olshopCustId),
						"OLSHOP_ORIG" => urlencode($row[0]->sender_address),
						//"OLSHOP_ORDERID" => urlencode($this->transactionId),
						"OLSHOP_ORDERID" => urlencode($this->transactionId),
						"OLSHOP_SHIPPER_NAME" => urlencode($stk_name),
						"OLSHOP_SHIPPER_ADDR1" => urlencode($stk_name),
						"OLSHOP_SHIPPER_ADDR2" => urlencode($stk_name),
						"OLSHOP_SHIPPER_ADDR3" => urlencode($stk_name),
						"OLSHOP_SHIPPER_CITY" => urlencode($stk_name),
						"OLSHOP_SHIPPER_REGION" => urlencode($stk_name),
						"OLSHOP_SHIPPER_ZIP" => urlencode("12620"),
						"OLSHOP_SHIPPER_PHONE" => urlencode($shipper_telhp),
						"OLSHOP_RECEIVER_NAME" => urlencode($row[0]->receiver_name),
						"OLSHOP_RECEIVER_ADDR1" => urlencode($addr1),
						"OLSHOP_RECEIVER_ADDR2" => urlencode($addr2),
						"OLSHOP_RECEIVER_ADDR3" => urlencode($addr3),
						//"OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
						"OLSHOP_RECEIVER_CITY" => urlencode($namaKota),
						"OLSHOP_RECEIVER_REGION" => urlencode($prov_name),
						"OLSHOP_RECEIVER_ZIP" => urlencode("12620"),
						"OLSHOP_RECEIVER_PHONE" => urlencode($telhp),
						"OLSHOP_DEST" => urlencode($row[0]->dest_address),
						"OLSHOP_SERVICE" => urlencode($row[0]->service_type_id),
						"OLSHOP_QTY" => urlencode($row[0]->total_item),
						"OLSHOP_WEIGHT" => urlencode(ceil($row[0]->total_weight)),

						"OLSHOP_GOODSTYPE" =>  urlencode("2"),
						"OLSHOP_GOODSDESC" =>  urlencode("SUPLEMENT"),
						"OLSHOP_INST" =>  urlencode("FRAGILE"),
						"OLSHOP_GOODSVALUE" =>  urlencode($row[0]->total_pay_net),
						//"OLSHOP_INSURANCE" => "Y",
						"OLSHOP_INS_FLAG" =>  urlencode("N"),
						"OLSHOP_COD_FLAG" =>  urlencode("N"),
						"OLSHOP_COD_AMOUNT" =>  urlencode($row[0]->total_pay_net),
					);		
					
					//$postData = setFieldPost($curl_post_data);
					
					$curl = curl_init();
					$arx = array(
					  CURLOPT_URL => $this->konos_url, 
					  CURLOPT_RETURNTRANSFER => true, 
					  CURLOPT_SSL_VERIFYPEER => true,
					  CURLOPT_CUSTOMREQUEST => "POST", 
					  CURLOPT_POSTFIELDS => http_build_query($curl_post_data), 
					  CURLOPT_HTTPHEADER => array( 
						"Content-Type: application/x-www-form-urlencoded")
					  );
					curl_setopt_array($curl, $arx);
					$curl_response = curl_exec($curl);
					curl_close($curl);
					
				
					$resKonos = json_decode($curl_response);
					//echo "<pre>";
					//print_r($curl_post_data);
					//print_r($resKonos);
				    //echo "</pre>";
					
					if($resKonos->detail == null) {
					     $this->updateKonosStatus =0;
						 $this->errorMsg = "Create No resi gagal";
						 return false;
					} else if($resKonos->detail[0]->status == "sukses") {
					    $this->updateKonosStatus = 1;
						$this->noResi = $resKonos->detail[0]->cnote_no;
						return true;
					} else if($resKonos->detail[0]->status == "Error" || $resKonos->detail[0]->status == "error") {
					     $this->updateKonosStatus =0;
						 $this->errorMsg = $resKonos->detail[0]->reason;
						 $this->noResi = null;
						 return false;
					}
					
					
					//return $curl_response;
				} else {
				   $this->updateKonosStatus = 0;
				   $this->noResi = $row[0]->conoteJNE;
				   $this->errorMsg = "No resi sudah ada : ".$this->noResi;
				   return false;
				} 
		} else {
		    //RECONCILE DATA ECOMM_TRANS_SHIPADDR
			$this->updateKonosStatus = 2;
			$this->errorMsg = "Data shipping temporary harus di reconcile dulu..";
			return false;
		}
		
		
	}
	
	
	public function getKonosJne() {
		$addr1 = str_replace(array("\n","\r"), '', substr($this->fullAddress, 0, 19));
					$addr2 = str_replace(array("\n","\r"), '', substr($this->fullAddress, 19, 19));
					$addr3 = str_replace(array("\n","\r"), '', substr($this->fullAddress, 38, 19));
					$telhp = str_replace(array("-","+", " "), "", $this->receiverPhone);
					//$telhp = str_replace("+", "", $telhp);
					//$telhp = str_replace(" ", "", $telhp);
					$stk_name = str_replace(array("\n","\r"), '', substr($this->stockistName, 0, 19));
					$prov_name = str_replace(array("\n","\r"), '', substr($this->provinsi, 0, 19));
	            	$kabu = strtoupper("Kab.");
		            $namaKota = str_replace($kabu, "", $this->kabupaten);
					$curl_post_data = array(
	                            "username" => $this->username,
	                            "api_key" => $this->password,
	                            "OLSHOP_BRANCH" => $this->branch,
	                            "OLSHOP_CUST" => $this->olshopCustId,
	                            "OLSHOP_ORIG" => $this->from,
	                            "OLSHOP_ORDERID" => $this->transactionId,
	                            "OLSHOP_SHIPPER_NAME" => $stk_name,
	                            "OLSHOP_SHIPPER_ADDR1" => $stk_name,
	                            "OLSHOP_SHIPPER_ADDR2" => $stk_name,
	                            "OLSHOP_SHIPPER_ADDR3" => $stk_name,
	                            "OLSHOP_SHIPPER_CITY" => $stk_name,
	                            "OLSHOP_SHIPPER_REGION" => $stk_name,
	                            "OLSHOP_SHIPPER_ZIP" => "12620",
	                            "OLSHOP_SHIPPER_PHONE" => $this->senderPhone,
	                            "OLSHOP_RECEIVER_NAME" => $this->receiverName,
	                            "OLSHOP_RECEIVER_ADDR1" => $addr1,
	                            "OLSHOP_RECEIVER_ADDR2" => $addr2,
	                            "OLSHOP_RECEIVER_ADDR3" => $addr3,
	                            //"OLSHOP_RECEIVER_CITY" => $row->kabupaten_name,
	                            "OLSHOP_RECEIVER_CITY" => $namaKota,
	                            "OLSHOP_RECEIVER_REGION" => $prov_name,
	                            "OLSHOP_RECEIVER_ZIP" => "12620",
	                            "OLSHOP_RECEIVER_PHONE" => $telhp,
	                            "OLSHOP_DEST" => $this->from,
	                            "OLSHOP_SERVICE" => $this->serviceCode,
	                            "OLSHOP_QTY" => $this->totalItem,
	                            "OLSHOP_WEIGHT" => $this->weight,

	                            "OLSHOP_GOODSTYPE" => "2",
	                            "OLSHOP_GOODSDESC" => "SUPLEMENT",
	                            "OLSHOP_INST" => "FRAGILE",
	                            "OLSHOP_GOODSVALUE" => $this->totalGoodsValue,
	                            //"OLSHOP_INSURANCE" => "Y",
								"OLSHOP_INS_FLAG" => "N",
								"OLSHOP_COD_FLAG" => "N",
								"OLSHOP_COD_AMOUNT" => $this->totalGoodsValue,
	                            );

			        $curl = curl_init($this->konos_url);

			        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($curl, CURLOPT_POST, true);
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			        $curl_response = curl_exec($curl);
					
					return $curl_response;
					
	}
	
	public function getKonosKgp() {
		$curl_data = array(
				 //"usernama"	    => "relas",
				 //"passw"		=> "123456",
				 "usernama"	    => $this->username,
				 "passw"		=> $this->password,
				 "transaksi_id" => $this->transactionId,
				 "kota"		    => $this->to,
				 "penerima"	    => $this->receiverName,
				 "alamat"		=> $this->fullAddress,
				 "telepon"		=> $this->receiverPhone,
				 "berat"	    => $this->weight);
				 
	    $data_string = json_encode($curl_data);                                                                                   
																													 
		$ch = curl_init($this->konos_url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
																														 
	   $curl_response = curl_exec($ch);
	   return $curl_response;
	}
	
	public function updateResi($notrx, $noresi) {
	    $resReturn = jsonFalseResponse("Conote gagal digenerate");
		$CI = &get_instance();
	    $qryTrx = $CI->load->database("db_ecommerce", true);
	    $data = array(
			'conoteJNE' => $noresi,
		);

		$qryTrx->where('orderno', $notrx);
		$resUpd = $qryTrx->update('ecomm_trans_shipaddr', $data);
		if($resUpd > 0) {
			$qryTrx->select('orderno, conoteJNE');
			$qryTrx->where('orderno', $notrx);
			$qryTrx->from('ecomm_trans_shipaddr');
			$query = $qryTrx->get();
			$sel = $query->result();
			if($sel != null) {
				if($sel[0]->conoteJNE != null || $sel[0]->conoteJNE != "") {
					$resReturn = jsonTrueResponse($sel, "Conote berhasil di generate : ".$sel[0]->conoteJNE);
				}
			} 
		}
		
		return $resReturn;
	}
	
	public function getShippingKonos() {
	    //echo "cargo : ".$this->username;
		if($this->tipe_cargo == "1") {
			$res = $this->conoteGenerateFromDB();
			//Jika Data shipping sudah ada di trans_shipaddr dan No resi telah digenerate
			//Update konot ditrans_shipaddr
			$hasil = jsonFalseResponse($this->errorMsg);
			if($res== true && $this->updateKonosStatus == 1 && $this->noResi != null) {
				$notrx = $this->transactionId;
				$noresi = $this->noResi;
			    $hasil = $this->updateResi($notrx, $noresi);
				
			} 
			return $hasil;
			/*$resKonos = json_decode($res);
			
			if($resKonos->detail == null) {
				 $this->noResi = null;
			} else if($resKonos->detail[0]->status == "sukses") {
				$this->noResi = $arr->detail[0]->cnote_no;
			} else if($resKonos->detail[0]->status == "Error") {
				 $this->noResi = null;
			}*/
		} else if($this->tipe_cargo == "2") {
			$res = $this->getKonosKgp();
			$resKonos = json_decode($res);
		
		    if($resKonos->status == "999") {
				$this->noResi = null;
		    } else {
				$this->noResi = $arr->KonosemenNo;
		    }
		}
		
		return $this->noResi;
	}
	
	public function trackingKonosJNE($konos = null) {
		//$url = "http://apiv2.jne.co.id:10101/tracing/api/list/cnote/$conot";
		//$url = "http://api.jne.co.id:8889/tracing/klink/list/cnote/$awb";
		
		$noresi = $this->noResi;
		if($konos != null) {
			$noresi = $konos;
		}
		$url = $this->tracking_url."".$noresi;
		
        $curl_post_data = array(
			"username" => $this->username,
			"api_key" => $this->password
		);
		$postData = setFieldPost($curl_post_data);					
		
		$curl = curl_init();
        $arx = array(
		  CURLOPT_URL => $url, 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_SSL_VERIFYPEER => true,
		  CURLOPT_CUSTOMREQUEST => "POST", 
		  CURLOPT_POSTFIELDS => $postData, 
		  CURLOPT_HTTPHEADER => array( 
		    "Content-Type: application/x-www-form-urlencoded")	 
		  );
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		//echo "<pre>";
		//print_r($arx);
		//print_r($response);
		//echo "</pre>";
		if ($err) {
			echo $err;
		} else {
           return $response;
		}
	}
	
	public function trackingKonos($konos = null) {
		if($this->tipe_cargo == "1") {
			return $this->trackingKonosJNE($konos);
		} else if($this->tipe_cargo == "2") {
		
		}
	}
}
<html>
<head>
    <meta charset="UTF-8">
    <title>Generate Voucher</title>

    <!-- CSS
   ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/base.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/vendor.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/main.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/normalize.css')?>">


    <style>
        .wrapper {
            padding-top: 250px;
            text-align: center;
        }

        .button {
            position: absolute;
            top: 50%;
        }
    </style>

</head>
<body>

<form action="<?php echo base_url('get_voucher')?>" method="post">
    <input type="hidden" name="link" value="http://www.k-net.co.id/simulasi">
    <input type="hidden" name="voucher_no" value="<?php echo $voucher_no; ?>">

    <div class="wrapper">
        <h2 class="subhead">Click This Button To The Next Step</h2>
        <br>
        <button type="submit" class="btn--primary">Get Your Voucher Here</button>
    </div>


</form>

</body>
</html>
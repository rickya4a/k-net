
<script src="<?php echo base_url('template-landing-page/js/jquery-3.2.1.min.js')?>"></script>

<script>
    $(document).ready(function() {
        // window.onbeforeunload = function() {
        //     return "Are you sure you want to leave?";
        // }


        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    })
</script>


<script>
    function checkStatus(that) {
        if (that.value == "Member") {
            //alert("check");
            document.getElementById("memb").style.display = "block";
        } else {
            document.getElementById("memb").style.display = "none";
            document.getElementById('nama').readOnly = false;
            $('#nama').val('');
        }
    }

</script>

<script>
    $(document).ready(function(){
        $('#idmemb').change(function(){
                if($('#idmemb').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo base_url('test_landing_page/getNamaMemb');?>",
                        dataType : 'json',
                        data : {idmemb:$('#idmemb').val()},
                        success: function(result){
                            if(result!=null){
                               // alert('ID Member Tidak Terdaftar')
                                $("#nama").val(result[0].fullnm);
                                $("#email").val(result[0].email);
                                $("#telp").val(result[0].tel_hp);
                                console.log(result);
                                console.log(result[0]);
                                document.getElementById('nama').readOnly = true;
                            }
                            else{
                                alert('ID Member Tidak Terdaftar');
                                $('#idmemb').val('');
                                $('#nama').val('');
                            }

                        }
                    });
                }
            }
        );



    });

</script>


<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="no-js oldie" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>K-Link Indonesia</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/base.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/vendor.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/main.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('template-landing-page/css/normalize.css')?>">

    <!-- script
    ================================================== -->
    <script src="<?php echo base_url('template-landing-page/js/modernizr.js')?>"></script>
    <script src="<?php echo base_url('template-landing-page/js/pace.min.js')?>"></script>

    <!-- favicons
    ================================================== -->
<!--    <link rel="shortcut icon" href="--><?php //echo base_url('template-landing-page/favicon.ico')?><!--" type="image/x-icon">-->
<!--    <link rel="icon" href="--><?php //echo base_url('template-landing-page/favicon.ico')?><!--" type="image/x-icon">-->

</head>

<body id="top">

<!-- header
================================================== -->
<header class="s-header">

    <div class="header-logo">
<!--        <a class="site-logo" href="index.html">-->
<!--            <img src="--><?php //echo base_url('template-landing-page/images/logo.png')?><!--" alt="Homepage">-->
<!--        </a>-->
    </div>

    <nav class="header-nav">
        <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>

        <div class="header-nav__content">
            <h3>Navigation</h3>

            <ul class="header-nav__list">
                <li class="current"><a class="smoothscroll"  href="#home" title="home">Home</a></li>
<!--                <li><a class="smoothscroll"  href="#about" title="about">About</a></li>-->
                <li><a class="smoothscroll"  href="#profile" title="works">Profile</a></li>
                <li><a class="smoothscroll"  href="#voucher" title="contact">Voucher</a></li>
                <li><a class="smoothscroll"  href="#benefit" title="services">Benefit</a></li>

<!--                <li><a class="smoothscroll"  href="#clients" title="clients">Clients</a></li>-->

            </ul>


            <ul class="header-nav__social">
                <li>
                    <a href="https://www.facebook.com/pages/AP-K-Link-Indonesia/154582434691756" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                    <a href="https://twitter.com/official_klink" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                    <a href="http://instagram.com/klink_indonesia_official" target="_blank"><i class="fa fa-instagram"></i></a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UCZ8j6S-8smC8G2u2TXiDUYg" target="_blank"><i class="fa fa-youtube"></i></a>
                </li>

            </ul>

        </div> <!-- end header-nav__content -->

    </nav>  <!-- end header-nav -->

    <a class="header-menu-toggle" href="#0">
        <span class="header-menu-text">Menu</span>
        <span class="header-menu-icon"></span>
    </a>

</header> <!-- end s-header -->


<!-- home
================================================== -->
<section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="<?php echo base_url('template-landing-page/images/bg_copy.png')?>" data-natural-width=3000 data-natural-height=2000 data-position-y=center>

    <div class="overlay"></div>
    <div class="shadow-overlay"></div>

    <div class="home-content">

        <div class="row home-content__main">


<!--            <h1>-->
<!--                We are a creative group <br>-->
<!--                of people who design <br>-->
<!--                influential brands and <br>-->
<!--                digital experiences.-->
<!--            </h1>-->
            <h2 class="display-2 display-2--light">K-Link Indonesia</h2>

            <div class="home-capt">

                <p style="color: azure; text-align: justify;">
                    Dengan lebih dari 2 juta mitra, tempat dimana setiap orang bisa meningkatkan pengetahuan mengenai konsep kesehatan dan membantu para mitra mencapai kesuksesan secara mental maupun financial.
                    K-Link Indonesia hadir untuk menjadi solusi bisnis yang sangat prospektif terhadap tantangan yang muncul untuk meningkatkan kemampuan dan kualitas hidup masyarakat dalam menjalankan bisnis.
                    Kami yakin dan bersemangat karena Kami terus mengembangkan dan menjaga kualitas produk serta menawarkan peluang bisnis sebagai solusi terbaik secara nasional maupun global di masa yang akan datang.

                    Bergabunglah menjadi salah satu mitra K-Link!
                </p>

                <div class="row about-stats stats block-1-3 block-m-1-2 block-mob-full" data-aos="fade-up">

                    <div class="col-block stats__col ">
                        <div class="stats__count">15</div>
                        <h5 style="color: white;">Years</h5>
                    </div>
                    <div class="col-block stats__col">
                        <div class="stats__count">2</div>
                        <h5 style="color: white;">Juta Mitra</h5>
                    </div>
                    <div class="col-block stats__col">
                        <div class="stats__count">250</div>
                        <h5 style="color: white;">Stockist</h5>
                    </div>
                    <!--        <div class="col-block stats__col">-->
                    <!--            <div class="stats__count">102</div>-->
                    <!--            <h5>Happy Clients</h5>-->
                    <!--        </div>-->

                </div> <!-- end about-stats -->

            </div>






        </div>

        <div class="home-content__scroll">
            <a href="#about" class="scroll-link smoothscroll">
                <span>Scroll Down</span>
            </a>
        </div>

        <div class="home-content__line"></div>

    </div> <!-- end home-content -->


    <ul class="home-social">
        <li>
            <a href="https://www.facebook.com/pages/AP-K-Link-Indonesia/154582434691756" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>
                <span>Facebook</span></a>
        </li>
        <li>
            <a href="https://twitter.com/official_klink" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a>
        </li>
        <li>
            <a href="http://instagram.com/klink_indonesia_official" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i><span>Instagram</span></a>
        </li>
        <li>
            <a href="https://www.youtube.com/channel/UCZ8j6S-8smC8G2u2TXiDUYg" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i><span>YouTube</span></a>
        </li>
        
    </ul>
    <!-- end home-social -->

</section> <!-- end s-home -->

<!-- works
================================================== -->
<section id='profile' class="s-works">

    <div class="intro-wrap">

        <div class="row section-header has-bottom-sep light-sep" data-aos="fade-up">
            <div class="col-full">

                <div class="row services-list block-1-2 block-tab-full">
                    <div class="col-block service-item" data-aos="fade-up">
                        <div class="service-text">
                            <h3 class="subhead">President Directors</h3>
                            <h3 class="subhead">Dato’ DR.H.Md. Radzi Saleh</h3>

                            <img src="<?php echo base_url('template-landing-page/images/pak_radzi2.jpg')?>" alt="" height="200" width="350">

                        </div>
                    </div>

                    <div class="col-block service-item" data-aos="fade-up">
                        <div class="service-text">
                            <br>
                            <p style="padding-top: 70px; color: white;">Pemimpin yang memiliki pengalaman lebih dari 25 tahun dalam industri penjualan langsung.
                                Dengan kemampuan manajemen sumber daya manusia yang luar biasa, keyakinan tak terkalahkan dan antusiasme,
                                serta pandangan yang visioner telah membantu K-Link Indonesia tumbuh menjadi salah satu perusahaan
                                multi level marketing terbesar di Indonesia.</p>
                        </div>
                    </div>
                </div>





            </div>

        </div> <!-- end section-header -->

    </div> <!-- end intro-wrap -->

</section> <!-- end s-works -->




<!-- contact
================================================== -->
<section id="voucher" class="s-contact" style="background-image: url(<?php echo base_url('template-landing-page/images/bg2.png')?>);">

    <div class="overlay"></div>
    <div class="contact__line"></div>

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h2 class="subhead">Voucher Number</h2>
            <h2 class="subhead"><?php echo $voucher_no; ?></h2>
            <h1 class="display-2 display-2--light">Verify Your Voucher Here</h1>

        </div>
    </div>

    <div class="row contact-content" data-aos="fade-up">

        <div class="contact-primary">

            <h3 class="h6">Silahkan Isi Data Diri Anda</h3>

            <form method="post" action="<?php echo base_url('tes_voucher/produk'); ?> ">
                <fieldset>

                    <div class="form-field">
                        <input name="vch" type="text" id="vch" value="<?php echo $voucher_no; ?>" class="full-width" readonly>
                    </div>
                    <div class="cl-custom-select">

                        <select class="full-width" id="status" name="status" onchange="checkStatus(this)" required="" aria-required="true">
                            <option value="Non-Member">Non-Member</option>
                            <option value="Member">Member</option>

                        </select>
                    </div>

                    <div class="form-field" style="display: none;" id="memb">
                        <label for="" class="form-control">Silahkan Isi ID Anda</label>
                        <input name="idmemb"  type="text" id="idmemb" value="" class="full-width">
                    </div>

                    <div class="form-field">
                        <label for="" class="form-control">Silahkan Isi Nama Anda</label>
                        <input name="nama" type="text" id="nama"  value="" required="" aria-required="true" class="full-width">
                    </div>

                    <div class="form-field">
                        <label for="" class="form-control">Silahkan Isi Email Anda</label>
                        <input name="email" type="email" id="email"  value="" class="full-width" required="" aria-required="true">
                    </div>

                    <div class="form-field">
                        <label for="" class="form-control">Silahkan Isi No. Telpon Anda</label>
                        <input name="telp" type="text" id="telp"  pattern="\d{1,15}" title="Input Angka Saja" value="" class="full-width" required="" aria-required="true">
                    </div>

                    <div class="form-field">
                        <button class="full-width btn--primary" type="submit">Submit</button>

                    </div>

                </fieldset>
            </form>


        </div> <!-- end contact-primary -->

        <div class="contact-secondary">
            <div class="contact-info">

                <h3 class="h6 hide-on-fullwidth">Keterangan</h3>

                <div class="cinfo">
                    <h5>Jika Anda Member</h5>
                    <p>
                        Silahkan Isi : <br>
                        ID Anda<br>
                        Nama Anda<br>
                        Email<br>
                        No Handphone
                    </p>
                </div>

                <div class="cinfo">
                    <h5>Jika Anda Non-Member</h5>
                    <p>
                        Silahkan Isi : <br>
                        Nama Anda<br>
                        Email<br>
                        No Handphone
                    </p>
                </div>

                <div class="cinfo">
                    <h5>Setelah Submit</h5>
                    <p>
                        Silahkan Cek <br>
                        Email dan SMS di telp Anda<br>
                        Terima Kasih
                    </p>
                </div>

            </div> <!-- end contact-info -->
        </div> <!-- end contact-secondary -->

    </div> <!-- end contact-content -->

</section> <!-- end s-contact -->


<!-- services
================================================== -->
<section id='benefit' class="s-services" style="padding-top: 30px;">

    <div class="row section-header has-bottom-sep" data-aos="fade-up">
        <div class="col-full">
<!--            <h3 class="subhead">What We Do</h3>-->
            <h3 class="h1">Keuntungan Bergabung Bersama Kami</h3>
        </div>
    </div> <!-- end section-header -->

    <div class="row services-list block-1-2 block-tab-full">

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-user"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Upline/Pembimbing</h3>
                <p>Setiap anggota memiliki upline yang akan memberikan pendampingan, pelatihan dan
                    mendukung setiap langkah anda menuju sukses dengan kami.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-group"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Modul Pelatihan</h3>
                <p>Tersedia modul pelatihan standar yang dijelaskan secara sederhana,
                    mudah dipelajari dan dimengerti, dan mudah dilakukan.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-megaphone"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Rencana pemasaran (Marketing Plan)</h3>
                <p>Dengan rencana pemasaran (Marketing Plan) kami sangat bermanfaat dalam menguraikan potensi
                    yang ditawarkan kepada seluruh Distributor K-link.
                    Tidak hanya distributor mendapatkan uang dari penjualan produk,
                    juga mendapatkan kesempatan memiliki penghasilan yang tak terbatas.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon"><i class="icon-lego-block"></i></div>
            <div class="service-text">
                <h3 class="h2">Terdaftar</h3>
                <p>K-LINK Indonesia telah memiliki Surat Izin Usaha Penjualan Langsung (SIUPL) dan telah menjadi anggota dari
                    Asosiasi Penjualan Langsung Indonesia (APLI) serta memiliki sertifikat dari MUI.
                </p>
            </div>
        </div>


        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-clock"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Waktu Yang Fleksibel</h3>
                <p>Anda bebas kapan saja menjalankan bisnis ini.
                    Jika anda memiliki waktu yang terbatas maka K-LINK pilihan tepat untuk anda.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-dollar"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Modal Kecil</h3>
                <p>Bangun bisnis K-Link anda sendiri dengan modal awal yang kecil untuk memulai potensi bisnis yang besar.
                </p>
            </div>
        </div>



    </div> <!-- end services-list -->

</section> <!-- end s-services -->






<!-- footer
================================================== -->
<footer style="height: 100px; padding: 10px;">

    <div class="row footer-bottom">
        <div class="col-twelve">
            <div class="copyright">
                <span>&copy; Copyright K-Link Indonesia 2018</span>
                <span>Site Template by <a href="https://www.colorlib.com/">Colorlib</a></span>
            </div>

            <div class="go-top">
                <a class="smoothscroll" title="Back to Top" href="#top"><i class="icon-arrow-up" aria-hidden="true"></i></a>
            </div>
        </div>

    </div> <!-- end footer-bottom -->

</footer> <!-- end footer -->


<!-- photoswipe background
================================================== -->
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                "Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
            "Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>

    </div>

</div> <!-- end photoSwipe background -->


<!-- preloader
================================================== -->
<!--<div id="preloader">-->
<!--    <div id="loader">-->
<!--        <div class="line-scale-pulse-out">-->
<!--            <div></div>-->
<!--            <div></div>-->
<!--            <div></div>-->
<!--            <div></div>-->
<!--            <div></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


<!-- Java Script
================================================== -->
<script src="<?php echo base_url('template-landing-page/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('template-landing-page/js/plugins.js')?>"></script>
<script src="<?php echo base_url('template-landing-page/js/main.js')?>"></script>

</body>

</html>
<div class="mainForm">
	<form class="form-horizontal" id="promoCashback" enctype="multipart/form-data">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Tipe</label>
				<div class="controls">
					<select id="trx_type" name="trx_type" id="trx_type" class="span4" onchange="changeLabel(this)">
						<option value="recruiter">Cashback Recruiter Rp.20000</option>
						<option value="rekap">Rekap Batch Transfer Cashback</option>
						<option value="import">Import Rekap Transfer</option>
					</select>

				</div>
				
				<label class="control-label" for="typeahead" id="tgl">Rekruiter Join Date</label>
				<div class="controls">
					<input id="jdtfroms" class="dtpicker" type="text" required="required" placeholder="From" name="jdtfroms">
					<input id="jdttos" class="dtpicker" type="text" required="required" placeholder="To" name="jdttos">
				</div>
				<label class="control-label" for="typeahead">File Import</label>
				<div class="controls">
					<input type="file" id="fileCSV" name="myfileX" class="span7 typeahead" />
					
				</div>
				<?php
				//echo datepickerFromTo("Trx Date", "jdtfroms", "jdttos");
				?>

				<label class="control-label" for="typeahead">&nbsp</label>
				<div id="bpro" class="controls">
					<input tabindex="3" type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="Find" onclick="All.ajaxFormPost(this.form.id,'be/promo/cashback/list')" />
					<input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
					<input tabindex="5" type="button" id="btn_proses" class="btn btn-primary .submit" name="proses" value="Proses Batch" onclick="All.ajaxFormPost(this.form.id,'be/promo/cashback/save')" />
					
				</div>
				<div id="imp" style="display: none;">
					<input tabindex="3" type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="Find" onclick="All.ajaxFormPost(this.form.id,'be/promo/cashback/list')" />
					<input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
					<input tabindex="6" type="button" id="btn_import" class="btn btn-primary .submit" name="proses" value="Import Data" onclick="importData()" />
				</div>
			</div>
		</fieldset>
	</form>
	<div class="result"></div>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy/mm/dd',
		}).datepicker("setDate", new Date());
		
		
	}); 
	
	function changeLabel(nilai) {
		var x = nilai.value;
		if(x === "recruiter") {
			$(All.get_active_tab() + " label#tgl").text(null);
			$(All.get_active_tab() + " label#tgl").text("Rekruiter Join Date");
			$(All.get_active_tab() + " #imp").css("display", "none");
			$(All.get_active_tab() + " #bpro").css("display", "block");
		} else if(x === "rekap") {
			$(All.get_active_tab() + " label#tgl").text(null);
			$(All.get_active_tab() + " label#tgl").text("Batch Date");
			$(All.get_active_tab() + " #imp").css("display", "none");
			$(All.get_active_tab() + " #bpro").css("display", "block");
		} else {
			$(All.get_active_tab() + " #imp").css("display", "block");
			$(All.get_active_tab() + " #bpro").css("display", "none");
		}
	}
	
	function importData() {
		var formData = new FormData($(All.get_active_tab() + " #promoCashback")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('be/promo/cashback/import'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});

	}
	
	function saveDataBatchTrf() {
		All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url("be/promo/cashback/import") , $(All.get_active_tab() + " #saveDataBatch").serialize(), function(data)
        {  
            All.set_enable_button();
            alert(data.message);
            
        }, "json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
	}
</script>
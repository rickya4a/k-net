<?php
if(empty($listtolakbank)){
    echo setErrorMessage();
}else{

    ?>

    <div class="overflow-auto">
        <!-- <form id="summPromo" method="post" >-->
        <table width="100%" id="DataTable" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <tr >
                <th colspan="11" >Daftar Penolakan Bank</th>
            </tr>
            <tr>
                <th>No</th>
                <th>ID Member</th>
                <th>Nama Member</th>
                <th>Tgl Tolak</th>
                <th>Alasan</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $no=1;
            foreach($listtolakbank as $row){

                echo "<tr>";
                echo "<td align='center'>&nbsp;$no</td>";
                echo "<td align='center'>&nbsp;".$row ->idmember."</td>";
                echo "<td align='center'>&nbsp;".$row->nama."</td> ";
                echo "<td align='right'>&nbsp;".date("Y-m-d", strtotime($row->tgl_tolak))."</td> ";
                echo "<td align='center'>&nbsp;".$row->alasan_tolak."</td> ";
                echo"</tr>";

                $no++;
            }
            /*echo "<tr>
                      <td colspan=\"4\" align='center'>Total</td>
                    <td align='center'>$totQty</td>
                  </tr>";*/
            ?>
            <!--<tr>
            <td colspan="6"></td>
            </tr>-->
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->

        <!--</form>-->
        <br />
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        sortTable();
        All.set_datatable();
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

    function sortTable() {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("dataTable");
        switching = true;
        /*Make a loop that will continue until
         no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.getElementsByTagName("TR");
            /*Loop through all table rows (except the
             first, which contains table headers):*/
            for (i = 0; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                 one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[0];
                y = rows[i + 1].getElementsByTagName("TD")[0];
                //check if the two rows should switch place:
                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                 and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }
</script>

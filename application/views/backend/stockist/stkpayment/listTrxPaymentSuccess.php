<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!--<meta content="text/html;charset=UTF-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">-->
    <title>K-LINK Back End Apps</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

	<!-- tambahan Hilal -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/img_caption.css'); ?>"/>
    <!-- End tambahan Hilal -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap/css/bootstrap-cerulean.css'); ?>"/>
    <!-- link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap/css/bootstrap-responsive.css'); ?>"/>
    
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap/css/theme.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap/font-awesome/css/font-awesome.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap/css/DT_bootstrap.css'); ?>"/>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/backend.css'); ?>"/>
	<!--<script src="<?php echo base_url('asset/css/bootstrap/js/jquery-1.8.1.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/css/bootstrap/js/jquery-ui-1.8.21.custom.min.js')?>"></script> -->
    
     
     <link rel="stylesheet" href="<?php echo base_url('asset/css/jquery-ui.css'); ?>" />
     <!--<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">-->
     <script src="<?php echo base_url('asset/css/bootstrap/js/jquery-1.9.1.min.js')?>"></script>
     <!--<script src="<?php echo base_url('asset/js/highcharts.js')?>"></script>
     <script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>-->
     <!--<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
     <script src="<?php echo base_url('asset/css/bootstrap/js/jquery-ui-1.10.3.min.js')?>"></script>
     
     
     
	 
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">

	  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
	  <!--[if IE 7 ]> <body class="ie ie7"> <![endif]-->
	  <!--[if IE 8 ]> <body class="ie ie8"> <![endif]-->
	  <!--[if IE 9 ]> <body class="ie ie9"> <![endif]-->
	  <!--[if (gt IE 9)|!(IE)]>
    <script type="text/javascript" src="<?php echo base_url('asset/css/bootstrap/js/bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('asset/css/bootstrap/js/jquery.dataTables.js')?>"></script>
    <script src="<?php echo base_url('asset/css/bootstrap/js/DT_bootstrap.js')?>"></script>   
    
    <script src="<?php echo base_url('asset/js_module/global.js')?>"></script>
    <script src="<?php echo base_url('asset/js_module/paystk.js')?>"></script>-->

 </head>
 <!--<body onLoad="document.form.value.focus()"> -->
 <body>
  <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <ul class="nav pull-right">
                 <li><a tabindex="-1" href="<?php echo base_url('stk/logout'); ?>">Logout</a></li>
                </ul>
                <a class="brand"><span class="first">K-NET Payment Transaction Stockist</a>
               
            </div>
        </div>
  </div>
	
  <div class="container-fluid">
    <div class="row-fluid">
       <div class="span">
       	    <?php
				if ($trxInsertStatus == "ok") {
					echo "<h3 align=center>Pembayaran MSR/SSR anda berhasil..</h3>";
				} else if ($trxInsertStatus == "fail") {
					echo "<h3>Pembayaran MSR/SSR anda gagal..</h3>";
				} else {
					echo "<h3>Pembayaran MSR/SSR anda masih pending..</h3>";
				}
			?>
			<table width="50%" align="center" class="table table-bordered">
				
					<thead>
					   <tr>	
						<th colspan="2">
							Online MSR/SSR Payment
						</th>
					   </tr>	
					</thead>
					<tbody>
						<?php
						if($trxInsertStatus == "ok" || $trxInsertStatus == "pending") {
						?>
						<tr>
							<td width="40%">Metode Pembayaran</td>
							<td align="right"><?php echo $header[0] -> bankDisplayNm; ?></td>
						</tr>
						<tr>
							<td width="40%">No. KW</td>
							<td align="right"><?php echo $header[0] -> receipt_no; ?></td>
						</tr>
						<tr>
							<td>Total Pembayaran SSR/MSR</td>
							<td align="right"><?php echo number_format($header[0] -> total_pay_ssr, 0, ".", "."); ?></td>
						</tr>
						<tr>
							<td>Biaya Connectivity</td>
							<td align="right"><?php echo number_format($header[0] -> charge_connectivity, 0, ".", "."); ?></td>
						</tr>
						<tr>
							<td>Biaya Administrasi</td>
							<td align="right"><?php echo number_format($header[0] -> charge_admin, 0, ".", "."); ?></td>
						</tr>
						<tr>
							<td>Total Biaya di Bayar</td>
							<td align="right">
								<?php
								$total_all = $header[0] -> total_pay_ssr + $header[0] -> charge_connectivity + $header[0] -> charge_admin;
								echo number_format($total_all, 0, ".", ".");
								?>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</tr>
			</table>
			<?php
			  if($trxInsertStatus == "ok" || $trxInsertStatus == "pending") {
			?>
			  <table width="50%" align="center" class="table table-bordered">
					<thead>
					  <tr>	
						<th colspan="4">
						   Daftar MSR/SSR yang dibayar	
						</th>
					  </tr>
					  <tr>	
						<th>No</th>
						<th>SSR/MSR No</th>
						<th>Total DP</th>
						<th>Total BV</th>
					  </tr>	
					</thead>				
					<tbody>
						<?php
						 $i = 1;
						 $tot_dp;
						 $tot_bv;
						 foreach($detail as $dta) {
						 	echo "<tr>";
							echo "<td align=right>$i</td>";
							echo "<td align=center>$dta->ssr_no</td>";
							echo "<td align=right>".number_format($dta->total_dp, 0, ".", ".")."</td>";
							echo "<td align=right>".number_format($dta->total_bv, 0, ".", ".")."</td>";
							echo "</tr>"; 
							$i++;
						 }
						?>
				    </tbody>		
			</table>	    
			<?php
			  }
			?>
			
			<h3 align="center"><a href="<?php echo base_url('stk/trx') ?>">Klik disini untuk ke halaman utama..</a></h3>
			
	 <div class="nextForm1"></div>
      <div class="nextForm2"></div>
     </div>	
    </div><!-- end row-fluid -->
  </div><!-- end container-fluid -->  
  
 </body>
</html>
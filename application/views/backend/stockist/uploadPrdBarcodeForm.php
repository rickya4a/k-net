<?php
   $formID = "frmUploadBarcode";
   echo opening_form(array("id" => $formID, "enctype" => true));
   
   $onchange = "onchange=be_stockist.checkValidDO()";
   $do = array(
	 	"labelname" => "DO Number",
	 	"fieldname" => "do_number",
	 	"placeholder" => placeholderCheck(),
	 
	 	"event" => $onchange
   );
   echo inputText($do);
   
   $onchange = "onchange=be_stockist.getDataStockistBarcode(this.value)";
   $idstk = array(
	 	"labelname" => "ID Stockist",
	 	"fieldname" => "idstk",
	 	//"placeholder" => placeholderCheck(),
	 	"readonly" => true,
	 	"event" => $onchange
   );
   echo inputText($idstk);
   
   $nmstk = array(
	 	"labelname" => "WH Code",
	 	"fieldname" => "whcd",
	 	"readonly" => true
   );
   echo inputText($nmstk);
   
   
   
   $arr = array(
   	"HD006A" => "K-Chlorophyl",
   	"IDHF001A" => "K-Omega Squa",
   );
   echo inputSelectArray("Product Code", "prdcd", $arr);
   echo setSingleUploadFile("File List Barcode");
   //echo inputHidden("whcd");
   echo inputHidden("ins_do");
   $input  = "All.postAjaxFormResetField('$formID','barcode/upload/save')";
   //$update = "All.updateFormData('menu/group/update', 'formInputGroupMenu', 'menu/group/list')";
   $view   = "All.postAjaxForm('$formID','barcode/preview')";
   echo button_set($input, null, $view);
   
   echo closing_form();	 
?>
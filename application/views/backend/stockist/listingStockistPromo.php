<?php
    if(empty($listScPromo)){
        echo "no Data";
    }else{
?>
	<form id="listStockistPromo">
	    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
		    <thead>
		    <tr>
		        
		        <th>No</th>
		        <th>Stockist Code</th>
		        <th>Stockist Name</th>
		        <th>Password SCO</th>
		        <th>Start Date</th>
		        <th>Expire Date</th>
		        <th>Action</th>
		    </tr>
		    </thead>
		    <?php
		        $no = 1;
		        foreach($listScPromo as $row){
		    ?>
		    
		    <tr>
		        <td><?php echo $no;?>
		        	<input type="hidden" value="<?php echo $no;?>" id="<?php echo "$no" ?>" ></td>
		        <td><?php echo $row->loccd;?>
		        	<input type="hidden" value="<?php echo $row->loccd;?>" id="<?php echo "loccd-$no" ?>" >
		        </td>
		        <td><?php echo $row->fullnm;?></td>
		        <td><?php echo $row->password;?></td>
		        <td style="text-align: center;"><?php echo date("d-m-Y",strtotime($row->start_date));?>
		        	<input type="hidden" value="<?php echo date("d-m-Y",strtotime($row->start_date));?>" id="<?php echo "start_date-$no" ?>"></td>
		        <td style="text-align: center;"><?php echo date("d-m-Y",strtotime($row->expired_date));?>
		        	<input type="hidden" value="<?php echo date("d-m-Y",strtotime($row->expired_date));?>" id="<?php echo "expired_date-$no" ?>"</td>
		        <td style="text-align: center;">
					<div align="center">
						<?php echo "
								<a class=\"btn btn-mini btn-danger\" onclick=\"be_stockist.deleteStockistPromo($no)\">
									<i class=\"icon-trash icon-white\"></i>
								</a>";?>
					</div>           
		        </td>
		    </tr>
		    <?php
		        $no++;
		        }
		    ?>
		</table>
	</form>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
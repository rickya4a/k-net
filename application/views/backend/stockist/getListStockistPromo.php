<div class="mainForm">
    <div id="frmScPromoApp">
        <form class="form-horizontal" id="frmScPromo" name="frmScPromo" method="post">
            <fieldset>
            	<div class="control-group"> 
                    <label class="control-label" for="typeahead">Stockist</label>
                    <div class="controls">
                        <select id="loccd" name="loccd" style="width: 400px !important; min-width: 400px; max-width: 400px;">
                        	<?php
						        foreach($listLoccd as $row){
						        	echo "<option value='$row->loccd'>$row->fullnm -- $row->loccd</option>";
						    	}
						    ?>
                        </select>
                    </div>
                    <label class="control-label" for="typeahead">Range Promo Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="reg_from" name="reg_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="reg_to" name="reg_to" />
                    </div>
                    <label class="control-label" for="typeahead">&nbsp;</label> 
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_search" class="btn btn-primary .submit" name="search" value="Search" onclick="be_stockist.getListStockistPromo()"/>
                        <input tabindex="3" type="button" id="btn_save" class="btn btn-success .submit" name="save" value="Save" onclick="be_stockist.saveSCPromo()"/>
                    </div>
                </div>
            </fieldset>
            <div id="listingStockistPromo"></div>
        </form>
    </div>
    
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>
<div class="mainForm">
    <div id="frmApproval">
        <form class="form-horizontal" id="listPaymentTicket" name="listPaymentTicket" method="post">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="typeahead">Search By</label>
                    <div class="controls">
                        <select id="category" name="category" tabindex="3">
                            <option value="batchno">No SSR</option>
                            <option value="trcd">No TTP</option>
                        </select>
                    </div>


                    <div class="controls">
                        <input type="text" class="span5"  id="search" name="search" />
                    </div>
                    <label class="control-label" for="typeahead">&nbsp;</label>
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'recover_ip/getSSR')"/>
                        <!-- input tabindex="3" type="button" id="btn_reconcile" class="btn btn-primary .submit" name="save" value="Refresh List" onclick="be_umroh.getReconcile()"/ -->
                    </div>
                </div>
            </fieldset>
            <div class="result"></div>
        </form>
    </div>

</div>
<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>
<div class="mainForm">
    <form class="form-horizontal" id="formListStk" method="post">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">Pilih Stockist</label>                             
                <div class="controls">
                	<select id="idstk" name="idstk" class="span5">
                		<option value="">--Select Here--</option>
                        <!--<option value="newstk">New Stockist</option>-->
                		<?php 
                            foreach($listStk as $listStks){
                		      echo "<option value=\"$listStks->loccd\">".$listStks->loccd." - ".$listStks->fullnm."</option>";
                		    }
                        ?>
                	</select>
                </div>
                
                <!--<label class="control-label" for="typeahead">New Stockist</label>
                <div class="controls">
                    <input type="text" id="newstkk" name="newstkk" class="span5" placeholder="Kosongkan field jika stockist ada di list"/>
                </div>-->
                
                <label class="control-label" for="typeahead">Pilih Provinsi</label>                             
                <div class="controls">
                	<select id="prov" name="prov" onchange="be_stockist.show_kab(this.value,'#kabupaten')" >
                		<option value="">--Select Here--</option>
                		<?php 
                            foreach($listProv as $listProvs){
                		      echo "<option value=\"$listProvs->PROPINSI\">".$listProvs->PROPINSI."</option>";
                              
                		    }
                        ?>
                	</select>
                </div>
                <label class="control-label" for="typeahead">Pilih Kabupaten</label>                             
                <div class="controls">
                	<select id="kabupaten" name="kabupaten" disabled="yes">
                		<option value="kabb">--Select Here--</option>
                		<?php 
                            /*foreach($listKab as $listKabs){
                		      echo "<option value=\"$listKabs->Kota_Kabupaten\">".$listKabs->Kota_Kabupaten."</option>";
                		    }*/
                        ?>
                	</select>
                </div>
                <div id="inp_btn" class="controls">
                    <input id="btn_input_user" class="btn btn-primary .submit" type="button" onclick="All.ajaxFormPost(this.form.id,'be/stockist/getListKecamatan')" value="Submit" name="submit"/>
                    <input id="btn_input_user" class="btn btn-danger .submit" type="button" onclick="be_stockist.testSave('be/stockist/unsetStk',this.form.id)" value="Unset Stockist" name="submit"/>
                </div>
                
            </div>
        </fieldset>
        
    </form>
    <div class="result"></div>
</div>
<script>
    
</script>
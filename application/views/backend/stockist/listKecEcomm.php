<?php
    if(empty($listKec)){
        setErrorMessage("No Data");
    }else{
?>
<!--<form id="updateStk22" method="post" action="<?php //echo site_url('be/stockist/updateStkEcomm');?>">-->
    <form id="updateStk22">    
        <input id="btn_upd_stk" class="btn btn-success .submit" type="button" value="Update" name="submit" onclick="be_stockist.testSave('be/stockist/updateStkEcomm',this.form.id)"/>
        <!--<input id="btn_upd_stk" class="btn btn-success .submit" type="button" value="Update" name="submit" onclick="be_stockist.testSave()"/>-->
        <input type="hidden" id="idstkk" name="idstkk" value="<?php echo $stks;?>"/>
        <!--<input type="hidden" id="newstk" name="newstk" value="<?php //echo $newstkk;?>"/>-->
    
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
		    <tr>
                <th>No</th>
                <th><input type="checkbox" name="checkall" onclick="All.checkUncheckAll(this)"/></th>
                <th>Provinsi</th>
		        <th>Kabupaten</th>
		        <th>Kecamatan</th>
                <th>Stockist</th>
            </tr>
        </thead>
        <tbody>
        <?php
	        $no = 1;
	        foreach($listKec as $row){
        ?>
            <tr>
               <?php
                    //if($stks == $row->loccd || $newstkk == $row->loccd ){
                      if($stks == $row->loccd){
               ?>
                <td style="text-align: center;"><?php echo $no?></td>
                <td style="text-align: center;"><input id="stkMaintenance" type="checkbox" value="<?php echo $row->propinsi."-".$row->Kota_Kabupaten."-".$row->Kecamatan?>" name="stkMaintenance[]" checked="checked" /></td>
                <td><?php echo $row->propinsi?>&nbsp;</td>
                <td><?php echo $row->Kota_Kabupaten?>&nbsp;</td>
                <td><?php echo $row->Kecamatan?>&nbsp;</td>
                <td><?php echo $row->loccd?>&nbsp;</td>
                <?php
                    }else{
                ?>
                <td style="text-align: center;"><?php echo $no?></td>
                <td style="text-align: center;"><input id="stkMaintenance" type="checkbox" value="<?php echo $row->propinsi."-".$row->Kota_Kabupaten."-".$row->Kecamatan?>" name="stkMaintenance[]" /></td>
                <td><?php echo $row->propinsi?>&nbsp;</td>
                <td><?php echo $row->Kota_Kabupaten?>&nbsp;</td>
                <td><?php echo $row->Kecamatan?>&nbsp;</td>
                <?php if ($row->loccd == 'NO STK'){ ?>
                <td style="color: red;"><?php echo $row->loccd?>&nbsp;</td>
                <?php }else{ ?>
                <td><?php echo $row->loccd?>&nbsp;</td>
                <?php }?>
            </tr>
        <?php
                }
                $no++;
            }
        ?>
        </tbody>
    </table>
</form>
<?php }?>
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>
<div class="mainForm">
    <form class="form-horizontal" id="frmUpdStkReff" method="post" action="#" target="_blank">
        <fieldset>
                <label class="control-label" for="typeahead">Ekspedisi</label>
               <div class="controls">
                    <select name="shipper" id="shipper" class="span5" onchange="javascript:All.listProvinsi(this)">
                        <option value="">--Select Here--</option>
                      <?php
                        //print_r($listCargo);
                        foreach($listCargo as $row){
                            echo "<option value=\"".$row->shipper_id."\">".strtoupper($row->shipper_name)."</option>";
                       }
                     ?>
                   </select>

                </div>

                <div id="provDiv">
                    <label class="control-label" for="typeahead">Provinsi</label>
                    <div class="controls">
					    <input type="hidden" id="shipper" name="shipper" value="1" />
                        <select class="span5" name="provinsi" id="provinsi" onchange="javascript:All.tampilkanKota('#kota')">
							
                        </select>
                        <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
                    </div>
                </div>

                <div id="kotaDiv">
                    <label class="control-label" for="typeahead">Kota / Kabupaten</label>
                    <div class="controls">
<!--                        <select class="span5" name="kota" id="kota" onchange="javascript:All.show_kecamatan(this,'#kecamatan')">-->
                        <select class="span5" name="kota" id="kota" onchange="javascript:All.tampilkanKecamatan(this,'#kecamatan')">

                        </select>
                        <input type="hidden" id="nama_kota" name="nama_kota" value="" />
                    </div>
                </div>

                <div id="KecDiv" style="display: none;">
                    <label class="control-label" for="typeahead">Kecamatan</label>
                    <div class="controls">
                        <select class="span5" name="kecamatan" id="kecamatan" onchange="javascript:All.listStockistByArea(this)">

                        </select>
                        <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
                    </div>
                </div>

                <div id="stockDiv">
                    <label class="control-label" for="typeahead">Stockist Referensi</label>
                    <div class="controls">
                        <input type="text" id="idstockistreff" name="idstockistreff" class="span5" placeholder="Masukan Kode Stockist" />
                    </div>
					<label class="control-label" for="typeahead">Stockist Name</label>
                    <div class="controls">
                        <input readonly="readonly" type="text" id="stkname" name="stkname" class="span5"  />
                    </div>
                </div>


                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" disabled name="save" value="Submit" onclick="All.ajaxFormPost('frmNullShipping','backend/transaction/InsertNullShipping')"/>
                    <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->

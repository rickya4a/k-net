<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>


        <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>FORM KONFIRMASI</h3>
                    </div>
                    <div class="modal-body">
                        Anda yakin akan mengubah data ini?
                        <form action="<?php echo base_url('subdept/update')?>" method="post">
                            <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
                                <thead>
                                <tr>

                                    <th>Member</th>
                                    <th>TRCD</th>
                                    <th>Date</th>
                                    <th>TDP</th>
                                    <th>TBV</th>
                                </tr>
                                </thead>
                                <tbody id="hasiltable">
                                </tbody>

                            </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-info" onClick="confirmDialog()">Recover</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>



    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        
        <th>Batch no</th>
        <th>Batch date</th>
        <th>Stokis code</th>
        <th>Stokis Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <?php
        //$no = 1;
        foreach($result as $row){
    ?>
    
    <tr>
       
        <td align="center"><?php echo $row->batchno;?></td>
        <td><?php echo $row->batchdt;?></td>
        <td><?php echo $row->loccd;?></td>
        <td><?php echo $row->fullnm;?></td>
        <td style="text-align: center;">
            <button class="btn btn-success" value="<?php echo $row->batchno;?>" id="submitBtn" data-toggle="modal" data-target="#"> Recover</button>

        </td>
    </tr>
    <?php
        //$no++;
        }
    ?>
</table>
<?php
    }
?>

<!--JS untuk confirm data yang diinput-->
<script type="text/javascript">
    $('#submitBtn').click(function() {
        console.log($(this).val());
        var rowshtml="";
        var no=0;
        $('#hasiltable').html(null);


//        var kode = document.getElementById("kode").value;
//        var nama = document.getElementById("nama").value;
//        var dept = document.getElementById("department").value;
//        var namadept = document.getElementById("namadept").value;
//
//        if (kode !="" && nama !="" && dept!=""){
//            $('#cid').val($('#id').val());
//            $('#ckode').val($('#kode').val());
//            $('#cnama').val($('#nama').val());
//            $('#cdepartment').val($('#department').val());
//            $('#cnamadept').val($('#namadept').val());
        $.ajax({
            url: All.get_url('recover_ip/getDetail'),
            type: 'post',
            data: {param: $(this).val()},
            dataType: 'json',
            success:
                function(data){
                    console.log(data);

                    $.each(data.arraydata, function(key, value) {
                        rowshtml += "<tr data-status=\"I\" id='tr"+no+"'>";
                        rowshtml += "<td>"+value.dfno+"</td>";
                        rowshtml += "<td>"+value.trcd+"</td>";
                        rowshtml += "<td>"+value.trdt+"</td>";
                        rowshtml += "<td>"+value.tdp+"</td>";
                        rowshtml += "<td>"+value.tbv+"</td>";
                        rowshtml += "</tr>";


                        no++;
                    });

                    $('#hasiltable').append(rowshtml);
//                    alert(masuk+" dari "+(y-x+1)+" nomor yang valid");
//				$("<tr data-status=\"I\" id='"+no+"'>"
//					+ "<td><input type=\"hidden\" name=\"idpendaftar[]\" id=\"idpendaftar\" value=\""+$('#idmbr2').val()+"\" /><input type=\"hidden\" name=\"jumlah[]\" id=\"jumlah\" readonly value=\""+$('#qty').val()+"\"  />"+$('#idmbr2').val()+"</td>"
//					+ "<td><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"hidden\" name=\"nmpendaftar[]\" id=\"nmpendaftar\" value=\""+$('#display_info2').val()+"\" />"+$('#display_info2').val()+"</td>"
//
//					+ "<td><input type=\"hidden\" name=\"emailrow[]\" id=\"emailrow\" value=\""+$('#email').val()+"\" />"+$('#email').val()+"</td>"
//					+ "<td><input type=\"hidden\" name=\"notelprow[]\" id=\"notelprow\" value=\""+$('#notelp').val()+"\" />"+$('#notelp').val()+"</td>"
//
//					+ "<td><input type=\"hidden\" name=\"event_db[]\" id=\"event_db\" value=\""+$('#opsi').val()+"\" /><input type=\"hidden\" name=\"event_nm[]\" id=\"event_nm\" value=\""+acara+"\" />"+acara+"</td>"
//					+ "<td>"+1+"</td>"
//					+ "<td><input type=\"hidden\" name=\"harga[]\" id=\"harga\" value=\""+$('#price').val()+"\"  />"+$('#price').val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
//					+ "<td>"+(num2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
//					+ "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\" /></td>"
//					+" </tr>").appendTo(".main tbody");



//				if(result[0].fullnm!=null){
//					$("#display_info").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
//					$("#display_info2").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
//					$("#idmbr2").val( idnya);
//				}
//				else
//				{
//					$("#display_info").val( "ID Member tidak valid");
//					$("#display_info2").val( "ID Member tidak valid");
//					$("#idmbr2").val( idnya);
//
//				}
                }
//            ,
//            error: function (xhr, ajaxOptions, thrownError) {
//                alert(thrownError + ':' +xhr.status);
//                All.set_enable_button();
//            }

        });
            //modal muncul setelah data yg diisi lengkap
            $("#confirm-submit").modal();
//
//        }
//        else
//        {
//            alert('Anda harus mengisi data dengan lengkap!');
//        }


    });

</script>
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>

<script>
    function confirmDialog() {
        alert('Data Berhasil Diupdate');
    }
</script>

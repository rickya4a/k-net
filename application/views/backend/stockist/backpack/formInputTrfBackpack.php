<?php
  if(!empty($sess_exp)) {
  	   setErrorMessage($sess_exp);
  } else {
?>
<div class="mainForm">
	<form class="form-horizontal" id="formInpTrfBackpack">
	   
       <div class="control-group"> 
       	  
       	  
          <label class="control-label">ID Stockist</label>  
            <div class="controls">
              <input tabindex="2"  class="required uppercase span3	" type="text" id="id_stockist" name="id_stockist" onchange="be_backpack.checkIDStockist(this.value, ' #nm_stockist')" />
            </div>
          
          <label class="control-label">Stockist Name</label>  
            <div class="controls">
              <input tabindex="3" readonly="readonly" class="uppercase span8" type="text" id="nm_stockist" name="nm_stockist" />
            </div>
          <label class="control-label">Total Transfer</label>  
            <div class="controls">
              <input tabindex="4" style="text-align: right"  class="uppercase span3" type="text" id="total_trf" name="total_trf" value="0" />
              <input type="hidden" id="total_trf_real" name="total_trf_real" value="0" />
            </div>
           <label class="control-label">&nbsp;</label>  
            <div class="controls" >
            	<table width="100%" class="table table-striped table-bordered" >
            	  <thead>
            	  	<tr>	
	            		<th width="25%">ID Member</th>
	            		<th>Member Name</th>
	            		<th width="15%">Trf Amount</th>
            		</tr>
            	  </thead>
            	  <tbody id="tbl_member">
            	  	<tr>
            	  		<td><input id="idmember1" name="idmember[]" type="text" class="span20" onchange="be_backpack.checkIDMember(this.value, '1')" /></td>
            	  		<td><input id="nmmember1" name="nmmember[]" type="text" class="span20" readonly="readonly" /></td>
            	  		<td><input style="text-align: right" type="text" name="trf[]"  class="span20" value="150000" readonly="readonly" /></td>
            	  	</tr>
            	  </tbody>	
            	</table>
            </div>
    	  
           <div class="controls"  id="inp_btn">
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Save" onclick="be_backpack.saveTrfBackpak(this.form.id)" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            
            <input type="hidden" id="jum" value="0" />
          </div>  
          <div class="controls"  id="upd_btn" style="display: none;"> 
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Update" onclick="be_pulsa.saveUpdatePulsaTrx(this.form.id)" />
            <input id="cancelupd" class="btn btn-danger" type="button" onclick="All.cancelUpdateForm()" value="Cancel Update">
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View Trx By Date" onclick="All.ajaxFormPost(this.form.id,'be/pulsa/date')" />
          </div>                                               
        </div>  
	   <div class="result"></div>	
	</form>
</div>

<?php
   }
?>
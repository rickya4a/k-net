<?php
  if(!empty($sess_exp)) {
  	   setErrorMessage($sess_exp);
  } else {
?>
<div class="mainForm">
	<form class="form-horizontal" id="formReportTrcBackpack">
	   
       <div class="control-group"> 
       	  
       	 
           <label class="control-label"><font color="red">&nbsp;*</font>Search Type</label>  
            <div class="controls">
              <select tabindex="6"  class="form-list required" id="search_type" name="search_type">
	            	<option value="id_stockist">ID Stockist</option>
					<option value="id_member">ID Member</option>
	          </select> 
            </div> 
            <label class="control-label">ID Stockist/Member</label>  
            <div class="controls">
              <input tabindex="2"  class="required uppercase span5	" type="text" id="param" name="param" />
            </div>
          <label class="control-label"><font color="red">&nbsp;*</font>Status</label>  
            <div class="controls">
              <select tabindex="6"  class="form-list required" id="status" name="status">
	            <option value="0">Belum Terpakai</option>
				<option value="1">Sudah Terpakai</option>
				<option value="all">All</option>
	          </select> 
            </div> 
    	  
           <div class="controls"  id="inp_btn">
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/backpack/report/list')" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            
          </div>  
                                                      
        </div>  
	   <div class="result"></div>	
	</form>
</div>
<script>
  $(document).ready(function($){
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
  });
</script>
<?php
   }
?>
<?php
	if($result['response'] == "false") {
		setErrorMessage($result['message']);
	} else {
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="6">List Transfer ID Stockist <?php echo $param; ?></th>
    </tr>	
    <tr>
        <th width="5%">No.</th>
        <th width="10%">Trf ID</th>
        <th>ID Stockist</th>
        <th width="20%">Tgl Input</th>
        <th width="15%">Total Transfer</th>
        <th width="15%">Status</th>
        <th width="15%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result['arrayData'] as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs	
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->id;?></td>
        <td align="center"><a href = "#" id=" <?php echo $row->id;?>" onclick="All.ajaxShowDetailonNextForm('be/backpack/detail/<?php echo $row->id;?>')"><?php echo $row->id_stockist;?></a></td>
        <td align="center"><?php echo $row->tgl." ".$row->jam;?></td>
        <td style="text-align: right;"><?php echo number_format($row->total_trf,0,".",".");?></td>
        
        <?php
          $status = "Terpakai";
          if($row->status == "0") {
          	 
          	 echo "<td align=\"center\"><select id=updstt$no name=updstt class=\"span20\">";
			 echo "<option value=\"1\">Terpakai</option>";
			 echo "<option value=\"0\" selected=selected>Belum Terpakai</option>";
          	 echo "</select></td>";
          } else {
          	 echo "<td align=\"center\"><select id=updstt$no name=updstt class=\"span20\">";
			 echo "<option value=\"1\" selected=selected>Terpakai</option>";
			 echo "<option value=\"0\">Belum Terpakai</option>";
          	 echo "</select></td>";
          }
        ?>
          
        
        <td align="center">
        	<?php echo "<input type=\"hidden\" id=\"idx$no\" value=\"$row->id\" />"; ?>
        	<a onclick="javascript:be_backpack.getUpdateStatus(<?php echo $no; ?>)" class="btn btn-mini btn-primary">Update Status</a>
        	
        </td>
   </tr>
    <?php
        
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
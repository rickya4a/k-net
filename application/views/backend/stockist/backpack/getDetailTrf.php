<?php
    if($result['response'] == "false"){
        setErrorMessage();
    } else {
?>
	
	<table width="80%" class="table table-striped table-bordered">
    <tr>
    	<th colspan=4>Detail Transfer</th>
    </tr>  
    <tr>
        
        <th width="10%">No.</th>
        <th width="25%">ID member</th>
        <th>Nama Member</th>
        <th width="20%">Transfer</th>
    </tr>

    <?php
        $no = 1;
        $total = 0;
        foreach($result['arrayData'] as $row) {
    ?>
    
    <tr>    
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->idmember; ?></td>
        <td align="center"><?php echo $row->nmmember;?></td>
        <td align="right"><?php echo number_format($row->trf,0,".",".");?></td>
    </tr>
        
        
    <?php
        $total += $row->trf;
        $no++;
        }
    ?>
    
    <tr>
        <td colspan="3" align="center"><b>Total Debt</b></td>
        <td align="right"><?php echo number_format($total,0,".",".");?></td>
    </tr>
</table>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
    </div>
	<p></p>
<?php
    }
?>

<!--<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });
</script>-->
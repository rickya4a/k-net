<?php
	if($result['response'] == "false") {
		setErrorMessage($result['message']);
	} else {
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="6">List Transfer <?php echo $result['arrayData'][0]->idmember. " / " .$result['arrayData'][0]->nmmember; ?></th>
    </tr>	
    <tr>
        <th width="5%">No.</th>
        <th width="10%">Trf ID</th>
        <th width="20%">ID Stockist</th>
        <th width="30%">Tgl Input</th>
        <th width="10%">Total Transfer</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result['arrayData'] as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs	
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->trf_id;?></td>
        <td align="center"><?php echo $row->id_stockist;?></td>
        <td align="center"><?php echo $row->tgl." ".$row->jam;?></td>
        <td style="text-align: right;"><?php echo number_format($row->trf,0,".",".");?></td>
        
       
   </tr>
    <?php
        
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<?php
$pdf=new FPDF('P','mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)

$pdf->AddPage();
//$pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
//$pdf->Cell(50,10,$img,'L',0,0,'R');
$pdf->SetFont('Courier','', 8);
$thnCheck = '2017';

function NumberToMonth($bln)
{
    switch($bln)
    {
        case 1 :
            $jum = "january";
            break;
        case 2 :
            $jum = "february";
            break;
        case 3 :
            $jum = "march";
            break;
        case 4 :
            $jum = "april";
            break;
        case 5 :
            $jum = "may";
            break;
        case 6 :
            $jum = "june";
            break;
        case 7 :
            $jum = "july";
            break;
        case 8 :
            $jum = "august";
            break;
        case 9 :
            $jum = "september";
            break;
        case 10 :
            $jum = "october";
            break;
        case 11 :
            $jum = "november";
            break;
        case 12 :
            $jum = "december";
            break;
    }
    return $jum;
}

$lebarCell = 4;
$pdf->Ln();
$pdf->SetXY(6,6);
$titleCol1 = 30;
$titleCol2 = 100;
$titleCol3 = 35;
$titleCol4 = 35;


$pdf->SetFont('Arial','B', 8);

{
    //        $pdf->Cell(198,5,"BONUS STATEMENT REPORT $bulan $dta2->bonusyear",1,0,'C', true);
    $pdf->SetFont('Arial','B', 20);
    $pdf->SetFillColor(255,255,255); // white

    $pdf->Cell(200,9,"Dynamic Youth Lounge Report",0,0,'C', true);
    $pdf->Ln();

    $pdf->SetFont('Arial','B', 12);

}
$pdf->SetLeftMargin(6);
$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);


$pdf->SetTextColor(0,0,0);

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','B', 8);

$pdf->Ln();
$pdf->Ln();
$pdf->Cell(10,$lebarCell,"No ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(30,$lebarCell,"ID Member ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(50,$lebarCell,"Nama Member ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(10,$lebarCell,"Rank ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(30,$lebarCell,"Tanggal ",1,0,'L',true); 	// its similar with TD in HT

$pdf->Cell(50,$lebarCell,"Remark ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(20,$lebarCell,"Pembayaran ",1,0,'L',true); 	// its similar with TD in HT
$total=0;
$i=0;
$pdf->Ln();
$pdf->SetFont('Courier','', 8);

foreach($report as $dxd)
{
    $i++;

    $pdf->Cell(10,$lebarCell,$i,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(30,$lebarCell,$dxd->dfno,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,$dxd->name,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(10,$lebarCell,$dxd->rank,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(30,$lebarCell,date("d-m-Y", strtotime($dxd->createdt)),1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,$dxd->remark,1,0,'L',true); 	// its similar with TD in HT
    if($dxd->payment!='FREE' && $dxd->payment!= null){

//if($dxd->payment!='FREE'){
    $total+=$dxd->payment;
    $pdf->Cell(20,$lebarCell,number_format($dxd->payment),1,0,'L',true); 	// its similar with TD in HT
}
    else
    {
        $pdf->Cell(20,$lebarCell,($dxd->payment),1,0,'L',true); 	// its similar with TD in HT

    }
    $pdf->Ln();
}
$pdf->SetFont('Courier','B', 8);

$pdf->Cell(180,$lebarCell,'Total',1,0,'L',true);
$pdf->Cell(20,$lebarCell,number_format($total),1,0,'L',true);


$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);
$pdf->SetTextColor(0,0,255);



$title = "ReportLounge.pdf";
$pdf->SetTitle($title);
$pdf->Output();

?>
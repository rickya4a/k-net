<?php
if($report == null) {
    echo emptyResultDiv();
} else {
    ?>

    <form id=listoffice>
        <?php
        echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
        echo "<thead><tr><th colspan=6>Dynamic Youth Report</th></tr>";
        echo "<tr bgcolor=#f4f4f4><th>No Member</th><th>Nama Member</th><th>Rank</th><th>Remark</th><th>Tanggal</th><th>Pembayaran</th></thead></tr>";
        echo "<tbody>";
        $i = 1;
        $total=0;
        foreach ($report as $list) {
            echo "<tr id=\"$i\">";
            echo "<td>$list->dfno</td>";
            echo "<td><div align=center>$list->name</div></td>";
            echo "<td><div align=center>$list->rank</div></td>";
            echo "<td><div align=center>$list->remark</div></td>";
            echo "<td><div align=center>".date("d-M-Y", strtotime($list->createdt))."</div></td>";
            if($list->payment!='FREE' && $list->payment!= null){
                $total+=$list->payment;
                echo "<td><div align=center>".number_format($list->payment)."</div></td>";
            }else{
                echo "<td><div align=center>$list->payment</div></td>";

            }
//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);

            echo "</tr>";
            $i++;
        }
        echo "</tbody>
        <tfoot>
        <tr><th colspan=5>Total</th><th>".number_format($total)."</th></tr>
        </tfoot>
        </tr>";
        echo "</table>";




	?>


         </form>


	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

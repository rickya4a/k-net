<?php
$pdf = new FPDF('P','mm',array(70,150));
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)

$pdf->AddPage();
//$pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
//$pdf->Cell(50,10,$img,'L',0,0,'R');
$pdf->SetFont('Courier','', 8);
$thnCheck = '2017';

function NumberToMonth($bln)
{
    switch($bln)
    {
        case 1 :
            $jum = "January";
            break;
        case 2 :
            $jum = "February";
            break;
        case 3 :
            $jum = "March";
            break;
        case 4 :
            $jum = "April";
            break;
        case 5 :
            $jum = "May";
            break;
        case 6 :
            $jum = "June";
            break;
        case 7 :
            $jum = "July";
            break;
        case 8 :
            $jum = "August";
            break;
        case 9 :
            $jum = "September";
            break;
        case 10 :
            $jum = "October";
            break;
        case 11 :
            $jum = "November";
            break;
        case 12 :
            $jum = "December";
            break;
    }
    return $jum;
}

$lebarCell = 4;
$pdf->Ln();
$pdf->SetXY(6,6);
$titleCol1 = 30;
$titleCol2 = 100;
$titleCol3 = 35;
$titleCol4 = 35;


$pdf->SetFont('Arial','B', 8);

{
    //        $pdf->Cell(198,5,"BONUS STATEMENT REPORT $bulan $dta2->bonusyear",1,0,'C', true);
    $pdf->SetFont('Arial','B', 8);
    $pdf->SetFillColor(255,255,255); // white

    $pdf->Cell(55,3,"Nota",0,0,'C', true);
    $pdf->Ln();
    $pdf->Cell(50,3,"Dynamic Youth Lounge",0,0,'C', true);
    //$pdf->Ln();
    //$pdf->Cell(50,3,"Lounge",0,0,'C', true);

    $pdf->SetFont('Arial','B', 8);

}
$pdf->SetLeftMargin(6);
$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);


$pdf->SetTextColor(0,0,0);

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','B', 8);


foreach($report as $dxd)
{
    $pdf->Ln();

    $pdf->Cell(40,$lebarCell,"Date ",0,0,'R',true);$pdf->Cell(40,$lebarCell,": ".$dxd->createdt,0,0,'L',true);

    $pdf->Ln();
// its similar with TD in HT
    $pdf->Cell(150,$lebarCell,"Created by ",0,0,'R',true);
    $pdf->Cell(40,$lebarCell,": ".$dxd->createnm,0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell(30,$lebarCell,"ID Member ",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,": ".$dxd->dfno,0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell(30,$lebarCell,"Nama Member ",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,": ".$dxd->name,0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell(30,$lebarCell,"Jumlah Peserta ",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,": ".$dxd->qty,0,0,'L',true);
    $pdf->Ln();
// its similar with TD in HT
    $pdf->Cell(30,$lebarCell,"Pembayaran ",0,0,'L',true); 	// its similar with TD in HT

    if($dxd->payment!='FREE'){
        $pdf->Cell(50,$lebarCell,": ".number_format($dxd->payment),0,0,'L',true); 	// its similar with TD in HT
    }
    else
    {
        $pdf->Cell(50,$lebarCell,": ".($dxd->payment),0,0,'L',true); 	// its similar with TD in HT

    }

    $pdf->Ln();
	$pdf->Cell(30,$lebarCell,"Catatan ",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,": ".$dxd->remark,0,0,'L',true); 	// its similar with TD in HT
    $pdf->Ln();
	$pdf->Cell(30,$lebarCell,"Petugas ",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(50,$lebarCell,": ".$dxd->createnm,0,0,'L',true);
    $pdf->Ln();
    
    $total=0;
    $i=0;
    $pdf->Ln();
    $pdf->SetFont('Courier','', 8);



}
$pdf->SetFont('Courier','B', 8);


$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);
$pdf->SetTextColor(0,0,255);



$title = "notaLounge.pdf";
$pdf->SetTitle($title);
$pdf->Output();

?>
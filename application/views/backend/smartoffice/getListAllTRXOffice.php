<?php
if($listPrd == null) {
    echo emptyResultDiv();
} else {
    echo "<form id=listPrd><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
    echo "<thead><tr><th colspan=7>List E-Ticket</th></tr>";
    echo "<tr bgcolor=#f4f4f4><th width=6%>No</th><th width=15%>No Member</th><th width=15%>Nama</th><th>Rank</th><th width=20%>Payment</th><th width=10%>Remark</th><th width=10%>Created Date</th></thead></tr>";
    echo "<tbody>";
    $i = 1;

    foreach($listPrd as $list) {
        echo "<tr id=\"$i\">";
        echo "<td>$i</td>";
        echo "<td><div align=center>$list->dfno</div></td>";
        echo "<td><div align=center>$list->name</div></td>";
        echo "<td><div align=center>$list->rank</div></td>";
        echo "<td><div align=center>$list->payment</div></td>";
        echo "<td><div align=center>$list->remark</div></td>";
        echo "<td><div align=center>$list->createdt</div></td>";
        echo "</tr>";
        $i++;
    }
    echo "</tbody></tr>";
    echo "</table></form>";
    ?>
    <script>
        $( document ).ready(function() {
            All.set_datatable();
        });
    </script>
    <?php
}
?>

<script>
    $(document).ready(function() {
            var bayar =$("#bayar").val();

            $("#qty").change(function() {
                    if(bayar!="FREE"){
                        $("#bayar").val($("#qty").val()* bayar);
                    }

                }
            );

        }

    );
</script>

<?php
if($report == null) {
    echo emptyResultDiv();
} else {
    if ($report != 1) {
        foreach ($report as $list) {
        }
        $fullnm = $list->fullnm;
        $readonly = 'readonly';
        $shortnm = $list->shortnm;
        $distributorcode = $list->distributorcode;

        if ($free) {
            $bayar = 'FREE';
        } else
            $bayar = 50000;

    } else {
        $fullnm = '';
        $readonly = '';
        $shortnm = '';
        $distributorcode = 'Non-Member';

        if ($free) {
            $bayar = 'FREE';
        } else
            $bayar = 50000;
    }

    ?>

    <form id=listoffice>
        <label class="control-label showDt" for="typeahead">NAMA MEMBER</label>

        <div class="controls">
            <input type="text" class="span5" <?php echo $readonly ?> id="fullnm" name="fullnm" required="required"
                   value="<?php echo $fullnm ?>">
            <input type="hidden" class="span5" readonly="readonly" id="distributorcode" name="distributorcode"
                   required="required" value=<?php echo $distributorcode ?>>

        </div>
        <label class="control-label showDt" for="typeahead">PERINGKAT</label>

        <div class="controls">
            <input type="text" class=" span5" readonly="readonly" id="shortnm" name="shortnm" required="required"
                   value=<?php echo $shortnm ?>>
        </div>


        <label class="control-label showDt" for="typeahead">QTY</label>

        <div class="controls">
            <input type="number" class=" span5"  id="qty" name="qty" required="required" value="1">
        </div>



        <label class="control-label showDt" for="typeahead">PEMBAYARAN</label>

        <div class="controls">
            <input type="text" class=" span5" readonly="readonly" id="bayar" name="bayar" required="required"
                   value=<?php echo $bayar ?>>
        </div>
        <label class="control-label showDt" for="typeahead">REMARK</label>

        <div class="controls">
            <input type="text" class=" span5" id="remark" name="remark">
        </div>
        <input id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit"
               onclick="All.inputFormDataCahyono('lounge/saveInputOffice','listoffice')" type="button">


        <?php
        if ($report != 1)
        {
            if ($list->adjustedrank <= 6) {
                echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
                echo "<thead><tr><th colspan=6>PSharing Points</th></tr>";
                echo "<tr bgcolor=#f4f4f4><th>Bulan</th><th>Point</th><th>Status</th></thead></tr>";
                echo "<tbody>";
                $i = 1;

                foreach ($report as $list) {
                    echo "<tr id=\"$i\">";
                    echo "<td>$list->bonusmonth</td>";
                    echo "<td><div align=center>$list->psharingpoints</div></td>";
                    if ($list->psharingpoints > 0) {
                        echo "<td><div align=center>Aktif</div></td>";
                    } else {
                        echo "<td><div align=center>Non-Aktif</div></td>";
                    }
//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);

                    echo "</tr>";
                    $i++;
                }
                echo "</tbody></tr>";
                echo "</table>";
            } else {
                echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
                echo "<thead><tr><th colspan=6>PSharing Points</th></tr>";
                echo "<tr bgcolor=#f4f4f4><th>Bulan</th><th>F/Qualified</th><th>Status</th></thead></tr>";
                echo "<tbody>";
                $i = 1;

                foreach ($report6 as $list) {
                    echo "<tr id=\"$i\">";
                    echo "<td>$list->bonusmonth</td>";
                    echo "<td><div align=center>$list->TOT400_LEG</div></td>";
                    if ($list->TOT400_LEG > 3) {
                        echo "<td><div align=center>Aktif</div></td>";
                    } else {
                        echo "<td><div align=center>Non-Aktif</div></td>";
                    }
//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);

                    echo "</tr>";
                    $i++;
                }
                echo "</tbody></tr>";
                echo "</table>";

            }

        }



        ?>


    </form>


    <script>
        $( document ).ready(function() {
            All.set_datatable();

        });
    </script>
    <?php
}
?>

<!--
    create_by : Suria Atningsih
    create_date : April 04, 2018
    create_for : event on April 11, 2018
-->

<?php
if($list == null) {
    echo emptyResultDiv();
} else {
    ?>

    <form id=listVch>
        <?php
        echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
        echo "<thead><tr><th colspan=9>List Voucher Identified</th></tr>";
        echo "<tr bgcolor=#f4f4f4>";
        echo "<th>No Voucher</th>";
        echo "<th>Id Member</th>";
        echo "<th>Nama</th>";
        echo "<th>Email</th>";
        echo "<th>Telp</th>";
        echo "<th>Status</th>";
        echo "</thead></tr>";
        echo "<tbody>";
        //$i = 1;
        $total=0;
        foreach ($list as $r) {
            //echo "<tr id=\"$i\">";
            echo "<td><div align=center>$r->vch_no</div></td>";
            echo "<td><div align=center>$r->dfno</div></td>";
            echo "<td><div align=center>$r->nama</div></td>";
            echo "<td><div align=center>$r->email</div></td>";
            echo "<td><div align=center>$r->no_telp</div></td>";
            echo "<td><div align=center>$r->status</div></td>";
            echo "</tr>";
            // $i++;
        }
        echo "</tbody>
        <tfoot>
        </tfoot>
        </tr>";
        echo "</table>";

        ?>


    </form>


    <script>
        $( document ).ready(function() {
            All.set_datatable();
        });
    </script>
    <?php
}
?>


<div class="mainForm">
    <form class="form-horizontal" id="formVch" action="<?php echo $action; ?>" method="post"  enctype="multipart/form-data">

        <div class="control-group">
            <fieldset>
                <label class="control-label" for="typeahead">Status Voucher</label>
                <div class="controls">
                    <select name="status" id="status" class="form-control">
                        <option value="">Select Option</option>
                        <option value="Released">Released</option>
                        <option value="Identified">Identified</option>
                        <option value="Claimed">Claimed</option>
                    </select>
                </div>

                <br>

                <div class="controls">
                    <button class="btn btn-primary" type="button" name="save" value="Submit" onclick="All.sanListVch('voucher_list/getList','formVch')">Submit</button>
                </div>

            </fieldset>

        </div>

</form>

<div id="listReleased"></div>
<div id="listClaimed"></div>
<div id="hasilPencarian1xd"></div>
<div id="result" class="result"></div>

</div>

<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>

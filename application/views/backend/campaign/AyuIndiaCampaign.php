<div class="mainForm">
    <div id="frmPend">
        <form class="form-horizontal" id="frmAyuListIndia" name="frmAyuListIndia" 
        	  method="post" action="<?php echo site_url('campaign/listUploadAyu/act/2');?>">
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Period</label>
                	<div class="controls">
                    	<select id="period" name="period">
                    		<option value="0">All</option>
                    		
                    		<?php 
								 foreach($getAyuPeriod as $list){
								 	$period = date('Y-m-d', strtotime($list->period));
								 	$period1 = date('M-Y', strtotime($list->period));
							    	echo "<option value='$period'>$period1</option>";
							    } 
							?>
							
                    	</select>
                    </div>	
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="be_campaign.getListAyuIndia(1)"/>
                        <input type="submit" id="printTxt" name="printTxt" value="Export" class="btn btn-primary"/> 
                    </div>
                    
                </div>
            </fieldset>
        </form>
        <div id="listAyuIndia"></div>
    </div>
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'dd/mm/yy',
		});
	});	
</script>
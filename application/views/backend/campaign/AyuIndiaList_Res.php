<?php
    if(empty($detListAyuIndia)){
        echo "no Data";
    }else{
    	if($param == 1){
?>
			<div class="overflow-auto">
			 	<table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable ">
			 	
			 	<?php
			 		$totIns = 7;
			    	echo "<tr style='height: 50px;text-align: center;font-size: 16px;text-shadow: none;'>
					        <td colspan='$totIns'>Listing Ayurveda India</td>
					     </tr>
			    
					     <tr style='height: 30px;text-align: center;text-shadow: none;' >
					        <th>No</th>
					        <th>ID Member</th>
					        <th>Name</th>
					        <th>Period</th>
					        <th>Join Date</th>
					        <th>Total DP</th>
					        <th>Total BV</th>
						  </tr>";
				    
			        $no = 1;
			        foreach($detListAyuIndia as $list){
			        	$periodx = date('M-Y', strtotime($list->period));
			        	$jointdt = date('d M Y', strtotime($list->jointdt));
			        	//BB.dfno, BB.fullnm, BB.period, BB.jointdt, BB.totaldp, BB.totalbv
			        	echo "<tr style='height: 30px;text-align: center;text-shadow: none;'>
					        	<td align='right'>".$no."</td>
						        <td align='left'>".$list->dfno."</td>
						        <td align='left'>".$list->fullnm."</td>
						        <td>$periodx</td>
						        <td>$jointdt</td>
						        <td align='right'>".$list->totaldp."</td>
						        <td align='right'>".$list->totalbv."</td>
						      </tr>";
						$no++;
			        }
			    ?>
				</table>
			</div>
<?php
    	}elseif($param == 2){
    		header("Content-Type: application/csv");
			header("Content-Disposition: attachment;Filename=ListAyuIndiaToWeb.csv");
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");
			
			$no = 1;
			foreach($detListAyuIndia as $list){
				//BB.dfno, BB.fullnm, BB.period, BB.jointdt, BB.totaldp, BB.totalbv
				$dfno = $list->dfno;
				$fullnm = $list->fullnm;
				//$period = date('M-Y', strtotime($list->period));
				//$jointdt = date('d-M-Y', strtotime($list->jointdt));
				//Request ubah format tanggal by Kristi @ 19/11/2015
				$period = date('Y-m-d', strtotime($list->period));
				$jointdt = date('Y-m-d', strtotime($list->jointdt));
				//End Request
		    	$totaldp = $list->totaldp;
		    	$totalbv = $list->totalbv;
				
				echo   "$dfno;$fullnm;$period;$jointdt;$totaldp;$totalbv;;\n";
		        $no++;
		    }
    	}
    }
?>
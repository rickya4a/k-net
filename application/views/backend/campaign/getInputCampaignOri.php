<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="frmInputCampaignPic2" method="post" action=<?php echo base_url('campaign/save'); ?>>
    <fieldset>      
      <div class="control-group">
      	<label class="control-label" for="typeahead">Distributor Code</label>
		<div class="controls ">
			<input type="text" name="dfno" id="dfno" 
				   class="setReadOnly span4"
				   placeholder="Press TAB after typing" required>
		</div> 	
		<label class="control-label" for="typeahead">Distributor Name</label>
		<div class="controls ">
			<input id="fullnm" class="label label-primary span4" type="text" readonly="readonly" name="fullnm">
		</div> 	
      	<label class="control-label" for="typeahead">Picture Description</label>
		<div class="controls">
			<input type="text" name="pic_desc" id="pic_desc" 
				   class="span5 typeaheadspan9 typeahead setReadOnly" 
				   placeholder="Describe the picture" required>
		</div> 
      	<label class="control-label" for="typeahead">Status</label>
		<div class="controls">
			<select id="status" name="status">
        		<option value="1">Active</option>
        		<option value='2'>Post</option>
        		<option value='0'>Disable/Reject</option>
        	</select>
		</div> 
		
	    <label class="control-label" for="typeahead">File</label>
		<div class="controls">
			<?php 
				foreach ($error as $data) {
					echo $data;
				}
	
			 	echo form_open_multipart('campaign/save');?>
			<input type="file" name="userfile" accept="image/*" class="span7"/>
		</div> 
      </div> <!-- end control-group -->
	    
	    <div class="controls"  id="inp_btn">
	        <input tabindex="3" type="button" id="btn_search" class="btn btn-primary .submit" name="search" value="Search" onclick="be_stockist.getListStockistPromo()"/>
	        <input tabindex="3" type="button" id="btn_save" class="btn btn-success .submit" name="save" value="Save" onclick="be_campaign.saveInputCampaignPic()"/>
	        <input type="submit" value="submit"/>
	    </div>
	          
     </fieldset>
     
     <div id="listingCampaignPic"></div>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->	  

<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>
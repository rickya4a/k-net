<?php
  if($listCampaign == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listCampaign><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=7>Campaign Listing</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=6%>No</th>
       							 <th width=15%>Date</th>
       							 <th width=15%>ID</th>
       							 <th width=15%>Name</th>
       							 <th>Description</th>
       							 <th>Status</th>
       							 <th width=10%>Action</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listCampaign as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
                //echo "<td><div align=center><input type=\"hidden\" id=\"camp_id$i\" value=\"$list->id\" />$list->id</div></td>";
                echo "<td><div align=center>".date('d-m-Y H:i:s', strtotime($list->createdt))."</div></td>";  
				echo "<td><div align=center>$list->dfno</div></td>";
				echo "<td><div align=center>$list->fullnm</div></td>";
				echo "<td><div align=center>$list->ft_desc</div></td>";
				echo "<td><div align=center>$list->status</div></td>";
               
                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
                $arr = array(
				    "update" => "Product.getUpdateProduct($i)",
				    "delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

<div class="mainForm">
	<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" id='frmInputCampaignPic'>
		<fieldset> 
			<div class="control-group">
		      	<label class="control-label" for="typeahead">Distributor Code</label>
				<div class="controls ">
					<input type="text" name="dfno" id="dfno" tabindex="1"
						   class="setReadOnly span4"
						   placeholder="Press TAB after typing" required>
				</div> 	
				
				<label class="control-label" for="typeahead">Distributor Name</label>
				<div class="controls ">
					<input id="fullnm" class="label label-primary span4" type="text" name="fullnm">
				</div> 	
				
		      	<label class="control-label" for="typeahead">Picture Description</label>
				<div class="controls">
					<input type="text" name="pic_desc" id="pic_desc" tabindex="2"
						   class="span5 typeaheadspan9 typeahead setReadOnly" 
						   placeholder="Describe the picture" required>
				</div> 
				
		      	<label class="control-label" for="typeahead">Status</label>
				<div class="controls">
					<select id="status" name="status" tabindex="3">
		        		<option value="1">Active</option>
		        		<option value='2'>Post</option>
		        		<option value='0'>Disable/Reject</option>
		        	</select>
				</div> 
				
				<label class="control-label" for="typeahead">Select File</label>
				<div class="controls">
					<input type="file" name="userfile" id="userfile" tabindex="4"/>
				</div> 
				
				<div class="controls">
					</br>
					<input tabindex="5" type="button" id="btn_save" class="btn btn-primary .submit" 
			    	   	   name="save" value="Save" onclick="be_campaign.saveInputCampaign('campaign/save', this.form.id)"/>
			    	<input tabindex="6" type="button" id="btn_reset" class="btn btn-reset .submit" name="reset" value="Reset" />
			    	<input tabindex="7" type="button" id="btn_list" class="btn btn-success .submit" 
			    		   name="view_list" value="View List" onclick="be_campaign.getListCampaignPic('campaign/list')"/>
					<!-- input tabindex="3" type="button" id="btn_delete" class="btn btn-danger .submit" 
			    	   	   name="delete" value="Delete" onclick="be_campaign.deleteCampaign('campaign/save', this.form.id)"/ -->
			    	 
				</div> 
      		</div> <!-- end control-group -->
		</fieldset>
	</form>
	
<div class="result"></div>
</div>
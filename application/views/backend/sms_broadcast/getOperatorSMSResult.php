<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
				  <thead>
					  <tr>
						  <th width='3%'>No</th>
						  <th width='30%'>Operator Name</th>
						  <th width='5%'>Status</th>
						  <th width='5%'>Action</th>
					  </tr>
				  </thead>
			  <tbody>";
		$i = 1;
		
		foreach($result as $data) {
			$status = "Active";
			if($data->is_activated == "0"){
				$status = "Incative";
			}
			
			echo "<tr>
				  <td align=right>$i</td>
				  <td align=left>$data->operator_desc</td>
				  <td align=left>$status</td>
				  <td align='center'>
				  	<a class=\"btn btn-mini btn-info\" onclick=\"All.getUpdateOperator($data->id)\"><i class=\"icon-edit icon-white\"></i></a>
				  	<a id='$data->id' onclick=\"javascript:All.deleteData('trans/delete/$data->id','delTrans','trans/delete/list')\" class=\"btn btn-mini btn-danger\"><i class=\"icon-trash icon-white\"></i></a>
				  </td>
				 </tr>";
			$i++;
		}
	}
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
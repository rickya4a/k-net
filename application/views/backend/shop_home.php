<script type="text/javascript">
// To conform clear all data in cart.
function clear_cart() {
var result = confirm('Are you sure want to clear all bookings?');

if (result) {
window.location = "<?php echo base_url(); ?>index.php/shopping/remove/all";
} else {
return false; // cancel button
}
}
</script>

<!--slider -->
<div class="slider">
	<div class="callbacks_container">
		<ul class="rslides" id="slider">
	    	<li><img src="<?php echo base_url() ?>assets/images/banner.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="<?php echo base_url() ?>assets/images/banner1.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="<?php echo base_url() ?>assets/images/banner4.jpg" class="img-responsive" alt=""/></li>
	    </ul>
	 </div>
</div>
<script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	nav: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
</script>	
<!--slider -->

<!--Stripe -->
<div class="column_center">
	<div class="container">
    	<!-- newsletter-->
		<div class="search">
	  		<div class="stay">Sign Up For Our Newsletter</div>
	  		<div class="stay_right">
		 		<input type="text" value="Your email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your email address';}">
		  		<input type="submit" value=""/>
	 		</div>
	  		<div class="clearfix"></div>
		</div>
        <!-- newsletter-->
        
        <!-- Social Media-->        
        <ul class="social">
        	<li class="find">Find us here</li>
          	<li><a href="#"> <i class="fb"> </i> </a></li>
          	<li><a href="#"> <i class="tw"> </i> </a></li>
          	<li><a href="#"> <i class="gl"> </i></a></li>
            <li><a href="#"> <i class="ig"> </i> </a></li>
          	<li><a href="#"> <i class="yt"> </i> </a></li>
         	<div class="clearfix"> </div>
        </ul>
		<div class="clearfix"></div> 
        <!-- Social Media-->       
  	</div>
</div>
<!--Stripe -->

<!--promo -->
<div class="row">
	<div class="container">
    	<div class="col-lg-6">
        	<a href="#"><img src="<?php echo base_url() ?>assets/images/promo.jpg" class="img-responsive margin0" alt=""/></a>
        </div>
        <div class="col-lg-6">
        	<a href="#"><img src="<?php echo base_url() ?>assets/images/tracking.jpg" class="img-responsive margin0" alt=""/></a>
        </div>		
 	</div>
</div>
<!--promo -->

<!--product -->
<div class="clearence">
	<div class="content_bottom">
        <div class="container">
            <div class="m_3"><span class="left_line"> </span><h3>Best Seller</h3><span class="right_line"> </span></div>
            <?php 
                    $n = 1;
                    foreach($prod as $row)
                    { 
                        $prdcd = $row->prdcd;
                        $imgName = $row->img_url;
                        $prdcdnm = $row->prdnm;
                        $prdcdcat = $row->prdcdcat;
                        $prdcdcatnm = $row->prdnmcatnm;
                        $westPrice = $row->price_w;
                        $eastPrice = $row->price_e;
                        $bv = $row->bv;                        
            ?>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 voffset4">
                        <div class="view view-first">
                            <a id="<?php echo $prdcd;?>" onclick="Shop.get_detailProds(<?php echo $n;?>)">
                                <img src="<?php echo "".base_url()."assets/images/".$imgName."";?>" class="img-responsive" alt=""/>
                            </a>                   
                            <div class="tab_desc">
                                <h3><a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)"><?php echo $prdcdnm;?></a></h3>
                                <h5 class="green">WIL.A IDR <?php echo "".number_format($westPrice,0,".",".");?></h5>
                                <h5 class="green">WIL.B IDR <?php echo "".number_format($eastPrice,0,".",".");?></h5>
                                <h5 class="green">BV <?php echo "".number_format($bv,0,".",".");?></h5>
                                <form method="post" action="<?php echo $formAction; ?>">
                                    <input type="hidden" name="prdcd" id="prdcd" value="<?php echo $prdcd;?>"/>
                                    <input type="hidden" name="prdnm" id="prdnm" value="<?php echo $prdcdnm;?>"/>
                                    <input type="hidden" name="bv" id="bv" value="<?php echo $bv;?>"/>
                                    <input type="hidden" name="westPrice" id="westPrice" value="<?php echo $westPrice;?>"/>
                                    <input type="hidden" name="eastPrice" id="eastPrice" value="<?php echo $eastPrice;?>"/>
                                    <button type="submit" class="btn1 btn-primary1">Add To Cart  <i class="fa fa-plus"></i></button>
                                </form>
                                <br />
                                <!--<a href="cart.php" class="btn1 btn-primary1">
                                        <span>Add To Cart</span><i class="fa fa-plus"></i>
                                </a>-->
                            </div>
                        </div>                
                        <div class="sale-box1"> </div>      
                    </div>
                       
                <?php
                    $n++;
                    }
                ?>
                <div class="col-md-12 voffset4 text-center">
                    <a href="<?php echo "".site_url()."";?>product-categories.php" class="btn1 btn2 btn-primary1"><span>View More Product</span><i class="fa fa-list"></i></a>
                </div>  
        </div>
    </div>
</div>
<!--product --> 

   
<!--brand -->        
<div class="brands">
	<div class="m_3 voffset3"><span class="left_line2"> </span><h3>Product Category</h3><span class="right_line2"> </span></div>
    <div class="container">
       	<div class="col-md-2">
            <a href="#"><img src="<?php echo base_url() ?>assets/images/category-1.jpg" class="img-responsive category margin0" alt=""/> </a>
        </div>
        <div class="col-md-2">
            <a href="#"><img src="<?php echo base_url() ?>assets/images/category-2.jpg" class="img-responsive category margin0" alt=""/> </a>
        </div>
        <div class="col-md-2">
            <a href="#"><img src="<?php echo base_url() ?>assets/images/category-3.jpg" class="img-responsive category margin0" alt=""/> </a>
        </div>
        <div class="col-md-2">
            <a href="#"><img src="<?php echo base_url() ?>assets/images/category-4.jpg" class="img-responsive category margin0" alt=""/> </a>
        </div>
        <div class="col-md-2">
            <a href="#"><img src="<?php echo base_url() ?>assets/images/category-5.jpg" class="img-responsive category margin0" alt=""/> </a>
        </div>
        <div class="col-md-2">
            <a href="#"><img src="<?php echo base_url() ?>assets/images/category-6.jpg" class="img-responsive category margin0" alt=""/> </a>
        </div>
	</div>
</div>
<!--brand --> 

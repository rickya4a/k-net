<?php
if(empty($listnoConot)){
    echo setErrorMessage();
}else{

    ?>

    <div class="overflow-auto">
        <!-- <form id="summPromo" method="post" >-->
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <tr >
                <th colspan="11" >Gosend Maintenance</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Orderno</th>
                <th>Cargo</th>
                <th>Resi Fisik</th>
                <th>Status Tracking</th>
                <th>ID Member</th>
                <th>Trx Date</th>
                <th>Total Pay</th>
                <!--<th>Qty Product</th>-->
                <th>Total Weight</th>
                <th>&nbsp;Cancel<a ></a></th>
                <th>&nbsp;Rebooking<a ></a></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            //print_r($listnoConot);
            foreach($listnoConot as $row){
                $cargo= $row->cargo_id;
                if($row->service_type_id != null){
                    $type = '1';
                }else{
                    $type = '0';
                }

                $conote=substr($row->conoteJNE,0,6);
                if($conote == "CANCEL"){
                    $status_conote="";
                }else{
                    $status_conote=$row->conoteJNE;
                }

                if($status_conote == ""){
                    $hide1= "hidden";
                    $hide2= "";
                }elseif($status_order == "Completed"){
                    $hide1= "hidden";
                    $hide2= "hidden";
                }elseif($status_order == "Driver not found"){
                    $hide1= "hidden";
                    $hide2= "";
                }else{
                    $hide1= "";
                    $hide2= "hidden";
                }

                if($status_order == "Cancelled" || $status_order == "Driver not found"){
                    $color="#FF0000";
                }else{
                    $color="#00FF00";
                }

                echo "<tr>
                <td align='center'>&nbsp;".$no."</td>
                <td>&nbsp;<a id=\"$row->orderno\" onclick=\"javascript:be_trans.getDetailTrx(this)\" href=\"#\">$row->orderno</a></td>";
                echo "<td align='center'>&nbsp;$row->service_type_name</td>";
                echo "<td align='center'>&nbsp;".$status_conote."</td>
                <td align='center'><span style='color:$color'>&nbsp;".$status_order."</span></td>
                <td align='center'>&nbsp;".$row->id_memb."</td>
                <td align='center'>&nbsp;".date('d-m-Y',strtotime($row->datetrans))."</td>
                <td align='right'>&nbsp;".number_format($row->total_pay,0,'.','.')."</td> ";
                //<td align='right'>&nbsp;".number_format($row->total_Items,0,'.','.')."</td>
                echo "<td align='right'>&nbsp;".$row->total_weight."</td>
                <td align='center'>";
                echo "<a class=\"btn btn-mini btn-danger\" onclick=\"be_trans.setCancelEConotG('$row->orderno','$type','$row->token','$cargo')\" title=\"Update Conot\" style='visibility: $hide1'>
                <i class=\"icon-edit icon-white\"></i></a></td>
                <td align='center'>";
                echo "<a class=\"btn btn-mini btn-success\" onclick=\"be_trans.setRebookEConotG('$row->orderno','$type','$row->token','$cargo')\" title=\"Update Conot\" style='visibility: $hide2'>
                <i class=\"icon-edit icon-white\"></i></a></td>
                             </tr>";
                $no++;
            }
            /*echo "<tr>
                      <td colspan=\"4\" align='center'>Total</td>
                    <td align='center'>$totQty</td>
                  </tr>";*/
            ?>
            <!--<tr>
            <td colspan="6"></td>
            </tr>-->
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->

        <!--</form>-->
        <br />
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        All.set_datatable();
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

    function updateCargo(param, pilihan) {
        var order = $(All.get_active_tab() + " #orderno" +param).val();
        //var pilcargo = $(All.get_active_tab() + " #pilcargo").val();
        //alert("Isi orderno : " +order+ " cargo : " +pilihan);

        $.ajax({
            url: All.get_url('trans/cargo/upd/') + order + "/" +pilihan,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    alert(data.message);
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                $("input[type=button]").removeAttr('disabled');
            }
        });
    }
</script>

<div class="mainForm">
    <form class="form-horizontal" id="formTrxReport" method="post" target="_blank">
        <fieldset>
            <div class="control-group">

                <label class="control-label" for="typeahead">Parameter Value</label>
                <div class="controls">
                    <input type="text" id="paramValue" name="paramValue" class="span4" placeholder="Masukkan Nomor IDEC" />
                </div>


                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'rebook_gsend/id')" />
                    <!--<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListTrxRebook()" />-->
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->

<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>

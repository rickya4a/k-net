<?php
if(empty($listnoConot)){
    echo setErrorMessage();
}else{

    ?>

    <div class="overflow-auto">
        <!-- <form id="summPromo" method="post" >-->
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <tr >
                <th colspan="11" >Detail Gosend</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Orderno</th>
                <th>Cargo</th>
                <th>Resi Fisik</th>
                <th>Status Tracking</th>
                <th>ID Member</th>
                <th>Trx Date</th>
                <th>Total Pay</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            //print_r($listnoConot);
            foreach($listnoConot as $row){
                $cargo= $row->cargo_id;
                if($row->service_type_id != null){
                    $type = '1';
                }else{
                    $type = '0';
                }

                $conote=substr($row->conoteJNE,0,6);
                if($conote == "CANCEL"){
                    $status_conote="";
                }else{
                    $status_conote=$row->conoteJNE;
                }

                if($status_conote == ""){
                    $hide1= "hidden";
                    $hide2= "";
                }elseif($status_order == "Completed"){
                    $hide1= "hidden";
                    $hide2= "hidden";
                }elseif($status_order == "Driver not found"){
                    $hide1= "hidden";
                    $hide2= "";
                }else{
                    $hide1= "";
                    $hide2= "hidden";
                }

                if($status_order == "Cancelled" || $status_order == "Driver not found"){
                    $color="#FF0000";
                }else{
                    $color="#00FF00";
                }

                echo "<tr>
                <td align='center'>&nbsp;".$no."</td>
                <td>&nbsp;<a id=\"$row->orderno\" onclick=\"javascript:be_trans.getDetailTrx(this)\" href=\"#\">$row->orderno</a></td>";
                echo "<td align='center'>&nbsp;$row->service_type_name</td>";
                echo "<td align='center'>&nbsp;".$status_conote."</td>
                <td align='center'><span style='color:$color'>&nbsp;".$status_order."</span></td>
                <td align='center'>&nbsp;".$row->id_memb."</td>
                <td align='center'>&nbsp;".date('d-m-Y',strtotime($row->datetrans))."</td>
                <td align='right'>&nbsp;".number_format($row->total_pay,0,'.','.')."</td></tr>";
                $no++;

            /*echo "<tr>
                      <td colspan=\"4\" align='center'>Total</td>
                    <td align='center'>$totQty</td>
                  </tr>";*/
            ?>
            <!--<tr>
            <td colspan="6"></td>
            </tr>-->
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->

        <!--</form>-->
        <br />

        <form class="form-horizontal" id="formTrxReport" method="post" target="_blank">
            <table width="100%" border="0">
                <tr>
                    <td>Nomor Order</td>
                    <td><?php echo $noConote;?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td><?php echo $ket;?></td>
                </tr>
                <tr>
                    <td>Nama Pembeli</td>
                    <td><?php echo $row->nmmember;?></td>
                </tr>
                <tr>
                    <td>Nama Penjual</td>
                    <td>K-Link Indonesia</td>
                </tr>
                <tr>
                    <td>Nama Driver</td>
                    <td><?php echo $driverName;?></td>
                </tr>
                <tr>
                    <td>No. Telp Pembeli</td>
                    <td><?php echo $row->tel_hp1;?></td>
                </tr>
                <tr>
                    <td>No. Telp Penjual</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>No. Telp Driver</td>
                    <td><?php echo $driverPhone;?></td>
                </tr>
                <tr>
                    <td>Nomor Order</td>
                    <td><?php echo $row->orderno;?></td>
                </tr>
                <tr>
                    <td>Detail Barang</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Nominal</td>
                    <td><?php echo number_format($row->total_pay,0,'.','.');?></td>
                </tr>
                <tr>
                    <td>Tanggal Order</td>
                    <td><?php echo date('d-m-Y',strtotime($row->datetrans));?></td>
                </tr>
                <tr>
                    <td>Waktu Order</td>
                    <td><?php echo date('H:i:s',strtotime($row->datetrans));?> WIB</td>
                </tr>
                <tr>
                    <td>Kronologi</td>
                    <td><textarea></textarea></td>
                </tr>
            </table>

            <input type="hidden" id="paramValue" name="paramValue" class="span4" value="<?php echo $ket;?>"/>
            <div class="result"></div>
        </form>
       <?php
            }//end foreach
       ?>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        All.set_datatable();
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

    function updateCargo(param, pilihan) {
        var order = $(All.get_active_tab() + " #orderno" +param).val();
        //var pilcargo = $(All.get_active_tab() + " #pilcargo").val();
        //alert("Isi orderno : " +order+ " cargo : " +pilihan);

        $.ajax({
            url: All.get_url('trans/cargo/upd/') + order + "/" +pilihan,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    alert(data.message);
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                $("input[type=button]").removeAttr('disabled');
            }
        });
    }
</script>

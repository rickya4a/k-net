<?php
if($listTrf == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
  //<input type=button name=\"proses_batch\" value=\"Recover Tolakan Transfer\" onclick=\"tolakanBatchAction(this.form)\" class='btn btn-primary' />
	echo "<form id=\"tolakanTransfer\">

	    <table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"9\">List Tolakan Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		//echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		echo "<th width='3%'>No</th>";
		echo "<th>Member</th>";
		echo "<th>Nama Member</th>";
		echo "<th width='4%'>Bank</th>";
		echo "<th width='15%'>No Rek</th>";
		echo "<th width='15%'>Pemilik Rek</th>";
		echo "<th width='10%'>No Batch</th>";
		echo "<th width='8%'>Cashback</th>";
		echo "<th width='10%'>Alasan Ditolak</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($listTrf as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			if($data->status == "1") {
			  $stt = "Proses";
			} else if($data->status == "2") {
			  $stt = "Tolak";
			} else {
			  $stt = "Blm Proses";
			}

			echo "<tr>";
			//echo "<td align=center><input type=checkbox id=pil$i name=trcd[] value=\"$data->dfno\" /></td>";
			echo "<td align=right>$i</td>";
			//echo "<td align=center><a id=\"$data->CNno\" onclick=\"javascript:be_trans.getDetailShipProduct(this)\">$data->CNno</a></td>";
			echo "<td align=center>$data->dfno</td>";
			echo "<td align=center>$data->fullnm</td>";
			echo "<td align=center>$data->bank</td>";
			echo "<td align=center>$data->bank_acc</td>";
			echo "<td align=center>$data->bank_accname</td>";
			echo "<td align=center>$data->BatchProses</td>";
			echo "<td align=right>".number_format($data->total_cashback, 0, ",", ".")."</td>";
			echo "<td align=left>$data->remarkTolakan";

			echo"</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table></form>";
		//echo "<input type=\"button\" class='btn btn-warning' value=\"<< Kembali\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" />";
}
?>
<script type="text/javascript">
$(document).ready(function()
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
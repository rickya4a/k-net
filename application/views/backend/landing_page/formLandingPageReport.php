<div class="mainForm">
  <form class="form-horizontal" id="formLandingPageReport" method="POST" target="_BLANK">
    <fieldset>
      <div class="control-group">
        <label class="control-label" for="typeahead">ID Member</label>
        <div class="controls">
          <input type="text" class="span4" id="idmember" name="idmember"
            onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/msmemb/dfno','#nmmember')">
        </div>
        <label class="control-label" for="typeahead">Nama Member</label>
        <div class="controls">
          <input type="text" class="span6" readonly="readonly" id="nmmember" name="nmmember" />
        </div>
        <label class="control-label" for="typeahead">Report Type</label>
        <div class="controls">
          <select id="rep_type" name="rep_type">
            <option value="batch">Batch Proses</option>
            <option value="listTrx">List Transaksi</option>
          </select>
        </div>
        <div class="controls" id="inp_btn">
          <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save"
            value="Submit" onclick="All.ajaxFormPost(this.form.id,'cashback/landing_page/report/list')" />
          <input tabindex="5" type="reset" class="btn btn-reset" value="Reset" />
        </div>
      </div>
      <!-- end control-group -->
    </fieldset>
    <div class="result"></div>
  </form>
</div>
<!--/end mainForm-->

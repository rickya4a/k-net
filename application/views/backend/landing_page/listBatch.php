<?php
if($result == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
	echo "<input  type=submit value=\"Export To Excel\" class='btn btn-success' onclick=\"javascript: form.action='".base_url('cashback/hydrowater/batch/print')."';\" />&nbsp;";
	echo "<input  type=button value=\"Transfer Selesai\" class='btn btn-primary' onclick=\"TransferOK()\" />
	    &nbsp;Tgl Transfer&nbsp;<input type=\"text\" class=\"dtpicker\" id=\"transferdt\" name=\"transferdt\" value=\"".date('Y-m-d')."\" />
	    <table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"8\">List Batch Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		echo "<th width='5%'>No</th>";
		echo "<th>No Batch Transfer</th>";
		echo "<th width='10%'>Tgl</th>";
		echo "<th width='15%'>Total Cashback</th>";
		echo "<th width='10%'>Diproses oleh</th>";
		echo "<th width='8%'>Batal</th>";
		echo "<th width='10%'>Tolakan</th>";
		//echo "<th width='6%'>Transfer OK</th>";

		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($result as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";

			echo "<tr id=\"$data->BatchProses\">";
			echo "<td align=center><input type=checkbox id=batchno$i name=batchno[] value=\"$data->BatchProses\" /></td>";
			echo "<td align=right>$i</td>";
			//echo "<td align=center><a id=\"$data->BatchProses\" onclick=\"javascript:be_trans.getDetailShipProduct(this)\">$data->BatchProses</a></td>";
			echo "<td align=center>$data->BatchProses</td>";
			echo "<td align=center>$data->batchdt</td>";
			echo "<td align=right>".number_format($data->total_cashback, 0, ",", ".")."</td>";
			echo "<td align=center>$data->processBy</td>";
			echo "<td align=center><input type=button value=\"Batal Proses\" onclick=\"cancelBatch('$data->BatchProses')\" class='btn btn-mini btn-danger' /></td>";
			echo "<td align=center><input type=button value=\"Tolakan Transfer\" onclick=\"tolakanBatch('$data->BatchProses')\" class='btn btn-mini btn-primary' /></td>";
			//echo "<td align=center><input  type=submit value=\"Print\" class='btn btn-mini btn-success' onclick=\"javascript: form.action='".base_url('cashback/hydrowater/batch/print')."';\" /></td>";
			//echo "<td align=center>$data->nominal_cashback</td>";

			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
}
?>
<script type="text/javascript">
$(document).ready(function()
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

 $(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
		}).datepicker("setDate", new Date());

</script>

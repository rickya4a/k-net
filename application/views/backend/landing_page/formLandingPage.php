<div class="mainForm">
  <form class="form-horizontal" id="formLandingPage" method="POST" target="_BLANK">
    <fieldset>
      <div class="control-group">
        <label class="control-label" for="typeahead">Tipe / Category</label>
        <div class="controls">
          <select id="tipe" name="tipe" onchange="setTgl()">
            <option value="blm_trf">Cashback yg belum ditransfer</option>
            <option value="tdk_lengkap">Cashback yg belum ada No rek / No HP</option>
            <option value="tolak">Transfer yg ditolak</option>
            <option value="sdh_trf">Cashback yg sudah di transfer</option>
            <option value="batch">List Batch Proses Transfer</option>
          </select>
        </div>
        <label style="display: none;" class="control-label dt_show" for="typeahead" id="title_tgl">Tgl Proses
          Batch</label>
        <div style="display: none;" class="controls dt_show">
          <input id="btdt_from" class="dtpicker" type="text" required="required" placeholder="From" name="from">
          <input id="btdt_to" class="dtpicker" type="text" required="required" placeholder="To" name="to">
        </div>
        <label class="control-label" for="typeahead">&nbsp</label>
        <div class="controls" id="inp_btn">
          <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save"
            value="Submit" onclick="All.ajaxFormPost(this.form.id,'cashback/landing_page/list')" />
          <input tabindex="5" type="reset" class="btn btn-reset" value="Reset" />
          <!--<input type="submit" value="Print" />-->
          <input tabindex="8" type="button" id="btn_updbank" class="btn btn-success .submit" name="save"
            value="Update Admin Bank" onclick="All.ajaxFormPost(this.form.id,'cashback/update/adminbank')" />
          <input tabindex="5" type="button" id="btn_sms" class="btn btn-warning .submit" name="save"
            value="Kirim SMS Notif" onclick="All.ajaxFormPost(this.form.id,'cashback/landing_page/sendsms')" />
        </div>
        <!-- end control-group -->
    </fieldset>
    <div class="result"></div>
  </form>
</div>
<!--/end mainForm-->
<script>
  $(document).ready(function () {
    $(All.get_active_tab() + " .dtpicker").datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: 'yy-mm-dd',
    }).datepicker("setDate", new Date());
  });

  function setTgl() {
    var pilih = $(All.get_active_tab() + ' #tipe').val();
    if (pilih == "blm_trf" || pilih == "tdk_lengkap") {
      $(All.get_active_tab() + ' .dt_show').css("display", "none");
    } else {
      $(All.get_active_tab() + ' .dt_show').css("display", "block");
    }
  }

  function processBatch() {
    All.set_disable_button();
    $.ajax({
      url: All.get_url('cashback/landing_page/batch/process'),
      type: 'POST',
      data: $(All.get_active_tab() + ' #hydroPromo').serialize(),
      success: function (data) {
        All.set_enable_button();
        $(All.get_active_tab() + " .mainForm").hide();
        All.clear_div_in_boxcontent(" .nextForm1");
        $(All.get_box_content() + " .nextForm1").html(data);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + ':' + xhr.status);
        All.set_enable_button();
      }
    });
  }

  function processPreviewBatch() {
    All.set_disable_button();
    $.ajax({
      url: All.get_url('cashback/landing_page/batch/save'),
      type: 'POST',
      data: $(All.get_active_tab() + ' #processBatch').serialize(),
      dataType: "json",
      success: function (data) {
        All.set_enable_button();
        alert(data.message);
        if (data.response == "true") {
          All.clear_div_in_boxcontent(" .nextForm1");
          $(All.get_active_tab() + " .mainForm").show();
          All.ajaxFormPost('hydroPromo', 'cashback/landing_page/list');
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + ':' + xhr.status);
        All.set_enable_button();
      }
    });
  }

  function TransferOK() {
    All.set_disable_button();
    $.ajax({
      url: All.get_url('cashback/landing_page/batch/ok'),
      type: 'POST',
      data: $(All.get_active_tab() + ' #hydroPromo').serialize(),
      dataType: "json",
      success: function (data) {
        All.set_enable_button();
        alert(data.message);
        if (data.response == "true") {
          All.clear_div_in_boxcontent(" .nextForm1");
          $(All.get_active_tab() + " .mainForm").show();
          All.ajaxFormPost('hydroPromo', 'cashback/landing_page/list');
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + ':' + xhr.status);
        All.set_enable_button();
      }
    });
  }

  function cancelBatch(noBatch) {
    var r = confirm("Anda yakin ingin membatalkan Batch Transfer ini?");
    if (r == true) {
      All.set_disable_button();
      $.ajax({
        url: All.get_url('cashback/landing_page/batch/cancel'),
        type: 'POST',
        data: {
          no_batch: noBatch
        },
        dataType: "json",
        success: function (data) {
          All.set_enable_button();
          alert(data.message);
          if (data.response == "true") {
            All.ajaxFormPost('hydroPromo', 'cashback/landing_page/list');
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(thrownError + ':' + xhr.status);
          All.set_enable_button();
        }
      });
    }
  }

  function tolakanBatch(param) {
    All.set_disable_button();
    $.ajax({
      url: All.get_url('cashback/landing_page/batch/id'),
      type: 'POST',
      data: {
        no_batch: param
      },
      success: function (data) {
        All.set_enable_button();
        $(All.get_active_tab() + " .mainForm").hide();
        All.clear_div_in_boxcontent(" .nextForm1");
        $(All.get_box_content() + " .nextForm1").html(data);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + ':' + xhr.status);
        All.set_enable_button();
      }
    });
  }

  function tolakanBatchActionPrev(form) {
    var batchno = form.batchno.value;

    All.set_disable_button();
    $.ajax({
      url: All.get_url('cashback/landing_page/batch/reject_prev'),
      type: 'POST',
      data: $(All.get_active_tab() + ' #tolakanTransfer').serialize(),
      success: function (data) {
        All.set_enable_button();
        $(All.get_active_tab() + " .nextForm1").hide();
        All.clear_div_in_boxcontent(" .nextForm2");
        $(All.get_box_content() + " .nextForm2").html(data);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + ':' + xhr.status);
        All.set_enable_button();
      }
    });
  }

  function tolakanBatchAction(form) {
    var batchno = form.batchno.value;
    All.set_disable_button();
    $.ajax({
      url: All.get_url('cashback/landing_page/batch/reject'),
      type: 'POST',
      data: $(All.get_active_tab() + ' #tolakanTransfer').serialize(),
      dataType: 'json',
      success: function (data) {
        All.set_enable_button();
        if (data.response == "true") {
          alert(data.message);
          tolakanBatch(batchno);
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + ':' + xhr.status);
        All.set_enable_button();
      }
    });
  }
</script>
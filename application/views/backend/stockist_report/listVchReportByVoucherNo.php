<?php
    if(empty($result)){
        echo setErrorMessage();
    }else{
?>
<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable">
	<tr>
		<th colspan="2">Detail Data Voucher Cash/Product</th>
	</tr>
	<?php
		foreach($result as $row){
				$claimstatus = $row->claimstatus;
				$dfno = $row->DistributorCode;
				$fullnm = $row->fullnm;
				$vchno = $row->VoucherNo;
				$vchtype = $row->vchtype;
				$voucherkey = $row->voucherkey;
				$amount = $row->VoucherAmt;
				$expDate = $row->ExpireDate;
				$tglklaim = $row->tglklaim;
				$nowDate = $row->nowDate;
				$status_expire = $row->status_expire;
				$trcd = $row->trcd;
				$batchno = $row->batchno;
				$loccd = $row->loccd;
		}
	?>
	<tr>
		<td style="width: 20%">No Voucher</td>
		<td id='vchno'><?php echo $vchno;?></td>
	</tr>
	<tr>
		<td style="width: 20%">Voucher Key</td>
		<td id='vchkey'><?php echo $voucherkey;?></td>
	</tr>
	<tr>
		<td>Tipe Voucher</td>
		<td>
			<?php
				if($vchtype == "C") {
					echo "VOUCHER CASH";
				} else if($vchtype == "P") {
					echo "VOUCHER PRODUCT";
				} else {
					echo "--";
				}
			?>
		</td>
	</tr>
	<tr>
		<td style="width: 20%">ID Member</td>
		<td><?php echo $dfno. " / ".$fullnm;?></td>
	</tr>
	<tr>
		<td style="width: 20%">Nilai Voucher</td>
		<td><?php echo number_format($amount,2,",",".");?></td>
	</tr>
	<tr>
		<td>Expire Date</td>
		<td>
			<?php
				if($status_expire == "1") {
					echo "<font color=red>* Voucher sudah expired</font>";
				}
				echo " ".$expDate;
				$today = date("Y-m-d");
				if ($today<=$expDate && $claimstatus == "0") { ?>
					<input type="text" class="dtpicker" id="date">
					<input type="button" class="btn btn-primary" value="Extend" onclick="extendExpiration()">
	<?php } ?>
		</td>
	</tr>
	<tr>
		<td>Status Klaim</td>
		<td>
			<?php
				//echo $updatedt. " ";
				if($claimstatus == "1") {
					echo "<font color=red>* Voucher sudah di klaim</font>";
					echo " ".$tglklaim;
				} else {
					echo "Belum di klaim";
				}
			?>
		</td>
	</tr>
	<tr>
		<td>No Trx</td>
		<td>
			<?php echo $trcd; ?>
		</td>
	</tr>
	<tr>
		<td>No Batch</td>
		<td>
			<?php echo $batchno; ?>
		</td>
	</tr>
	<tr>
		<td>ID Stockist</td>
		<td>
			<?php echo $loccd; ?>
		</td>
	</tr>

	<?php
		if($result[0]->no_trx != null || $result[0]->no_trx != "") {
			echo "<tr>";
			echo "<td>VC Deposit</td>";
			echo "<td>".$result[0]->no_trx." - ".$result[0]->createnm. " - ".$result[0]->tgldeposit."</td>";
			echo "</tr>";
		}
	?>

</table>

<script>
	$(document).ready(function() {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());

		All.set_datatable();
	});

	function extendExpiration() {
		var date = $('#date').val(),
				vchno = $('#vchno').html(),
				vchkey = $('#vchkey').html();
		$.ajax({
				type: "POST",
				url: All.get_url('cekvoucher/extend'),
				data: {
					date: date,
					vchno: vchno,
					vchkey: vchkey
		},
		success: function (data) {
			alert(data.message);
			// console.log(data)
			All.set_enable_button();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
			});
	}
</script>

<?php
echo backToMainForm();
    }
?>
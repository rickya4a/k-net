<div class="mainForm">
<!--<div id="formDOO">-->
  <form class="form-horizontal" id="hifiTrxReport" >
    <fieldset>      
      <div class="control-group">        		      
       <label class="control-label" for="typeahead">Transaction Date</label>
       <div class="controls">
           <input type="text" class="dtpicker typeahead" name="from" />&nbsp;to&nbsp;
		   <input type="text"  class="dtpicker typeahead" name="to" />
       </div>
	   <label class="control-label" for="typeahead">PPOB Type</label>
       <div class="controls">
           <select id="ppob_type" name="ppob_type" onchange="getListTrxTypeByPpob(this.value)">
		    <option value="">--Select here--</option>
           	<option value="1">PPOB Hifi</option>
           	<option value="2">XL SIM Card </option>
			<option value="4">XL Paket Data</option>
			<option value="3">PPOB SGO</option>
			
           	?>
           </select>
       </div>
        <label class="control-label" for="typeahead">Transaction Type</label>
       <div class="controls">
           <select id="hifi_trx_type" name="hifi_trx_type">
           	<option value="">--Select here--</option>
           	<?php
			/*
           	foreach($listTrxType as $dta) {
           		echo "<option value=\"$dta->trx_type\">$dta->trx_type</option>";
           	}
			*/
           	?>
           </select>
       </div>
       <label class="control-label" for="typeahead">ID Member</label>
       <div class="controls">
           <input type="text" class="span4" id="hifi_memberid" name="hifi_memberid" />
       </div>
	   <label class="control-label" for="typeahead">Customer No</label>
       <div class="controls">
           <input type="text" class="span4" id="hifi_custno" name="hifi_custno" />
       </div>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'hifi/trx/report/list')" />
            
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="5" type="button" id="cek_kuota_xl" class="btn btn-primary .submit" name="cek_kuota" value="Cek Kuota XL" onclick="All.ajaxFormPost(this.form.id,'xl/stock/report/acc')" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
	
	function getListTrxTypeByPpob(nilai) {
		if(nilai !== "") {
    	   
	    	All.set_disable_button();
			All.get_wait_message();
			$.ajax({
	            url: All.get_url("sgo_ppob/trxtype/list"+"/" +nilai),
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
					var rowhtml = "<option value=''>--Select here--</option>";
	                if(data.response == "true") {
					    var arrayData = data.arrayData;
						$(All.get_active_tab() + " #hifi_trx_type").html(null);	
						$.each(arrayData, function(key, value) {
							rowhtml += "<option  value='"+value.trx_type+"'>"+value.trx_name+"</option>";
						});
						$(All.get_active_tab() + " #hifi_trx_type").append(rowhtml);
					} else {
						$(All.get_active_tab() + " #hifi_trx_type").html(null);
						$(All.get_active_tab() + " #hifi_trx_type").append(rowhtml);						
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	    } else {
	    	alert("Please fill the field..");
	    }  
	}
</script>

<div class="mainForm">
  <form class="form-horizontal" id="sgoPpobTrx">
    <fieldset>      
      <div class="control-group">
      	
      	<label class="control-label" for="typeahead">Kategori PPOB</label>                             
        <div class="controls">
        	<select id="cat" name="cat" style="width: 350px">
        		<option value="">--Select here--</option>
        		<?php
        		 foreach ($listCat as $dta) {
					 echo "<option value=\"$dta->id\">$dta->cat_name</option>";
				 }
        		?>
        	</select>
        </div>
       
        <label class="control-label" for="typeahead">Tipe Transaksi PPOB</label>                             
        <div class="controls">
        	<select id="prd_id" name="prd_id" style="width: 350px">
        		<option value="">--Select here--</option>
        		
        	</select>
        	<input type="hidden" name="prd_id" id="prd_id" value=""  />
			<input type="hidden" name="product_name" id="product_name" value=""  />
        </div>
        <label class="control-label" for="typeahead">Order / Customer ID</label>                             
        <div class="controls">
        	<input type="text" class="span6" id="order_id" name="order_id" placeholder="No Pelanggan/No HP" />
        </div>
        <label class="control-label" for="typeahead">Nominal Pembayaran</label>                             
        <div class="controls">
        	<input type="text" class="span6" id="amount" name="amount" placeholder="" />
        	<input type="hidden" class="span6" id="amountx" name="amountx" placeholder="" />
        </div>
        <label class="control-label" for="typeahead">Pilih Pembayaran</label>
        <div class="controls">
        	<select class="form-control"  id="bank" name="bank" onchange="setSelectPay()">
				<?php
				echo "<option value=\"\">--Pilih disini--</option>";
	
				foreach($listBank as $dta) {
					echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
				}
				?>
			</select>
			<input type="hidden" name="totalx" value="" />
			<input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
			<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
			<input type="hidden" name="bankid" id="bankid" value=""  />
			<input type="hidden" name="bankCode" id="bankCode" value=""  />
			<input type="hidden" name="bankDesc" id="bankDesc" value=""  />
			<input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
			<input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
			<input type="hidden" name="charge_admin" id="charge_admin" value=""  />
        </div>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-warning .submit" name="save" value="Cek Tagihan" onclick="cekTagihan()" />
            <input tabindex="4" type="button" id="btn_input_user" class="btn btn-primary" name="save" value="Lanjut Pembayaran" onclick="submitData()" />
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
        
     </fieldset>
    
    <div class="result"></div>
    <div>
		  <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
		</div>
  </form> 
</div><!--/end mainForm-->
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script>
    function setSelectPay() {
		var x = $("#bank").val();
		var bankDetail = $("#bank option:selected").text();
		//alert("isi : " +x);
		//var bankDesc = $("#bank option:selected").text();
		var str = x.split("|");
		$("#bankid").val(str[0]);
		$("#bankCode").val(str[1]);
		//$("#bankDesc").val(bankDesc);
		$("#bankDesc").val(str[4]);
		$("#charge_connectivity").val(str[2]);
		$("#charge_admin").val(str[3]);
		$("#bankDescDetail").val(bankDetail);


	}
	
	function cekTagihan() {
		All.set_disable_button();
        //All.get_image_load2();
        $.post(All.get_url('sgo_ppob/inquiry/show') , $(All.get_active_tab() + " #sgoPpobTrx").serialize(), function(data)
        {  
            All.set_enable_button();
            if(data.error_code == "0000") {
            	var x = parseInt(data.amount);
            	x = x/100;
            	x = numeral(x).format('0.00');
            	$(All.get_active_tab() + " #amount").val(x);
            	$(All.get_active_tab() + " #amountx").val(data.amount);
            }  else {
            	alert(data.error_desc);
            	$(All.get_active_tab() + " #amount").val(null);
            	$(All.get_active_tab() + " #amountx").val(null);
            } 
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
	}

	$(document).ready(function() { 
		$("#cat").change(function() {
			var nilai = $(All.get_active_tab() + " #cat").val();
		    All.set_disable_button();
			$.ajax({
	            url: All.get_url('sgo_ppob/trx/listByCat') + "/" +nilai,
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){
	            	All.set_enable_button();
	            	var arrayData = data.arrayData;
	            	if(data.response == "true") {
	            		//All.clear_div_in_boxcontent("#ppob_trx_type");
	            		$(All.get_active_tab() + " #prd_id").html(null);  
	            		var htmlx = "";
	            		$.each(arrayData, function(key, value) {
	            			var kodex = value.prd_id+"|"+value.inqui;
							htmlx += "<option value='"+kodex+"'>"+value.product_name+"</option>";
                        });
                        
                        $(All.get_active_tab() + " #prd_id").append(htmlx);
	            	} else {
	            		alert(data.message);
	            		//All.clear_div_in_boxcontent("#ppob_trx_type");
	            		$(All.get_active_tab() + " #prd_id").html(null);
	            		var htmlx = "";
	            		htmlx += "<option value=''>--Select here--</option>";
	            		$(All.get_active_tab() + " #prd_id").append(htmlx);
	            	}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
		});
    });
</script>    
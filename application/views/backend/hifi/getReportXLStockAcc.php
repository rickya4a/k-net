<table width="100%" align="center" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
	<tr>
	    <th colspan="7">Laporan Stock Paket Data XL</th>
	</tr>
	<tr>
		<th rowspan="2" width="10%">Kode</th>
		<th rowspan="2">Nama Paket</th>
		<th rowspan="2" width="10%">Top Up Kuota</th>
		<th rowspan="2" width="10%">Qty Trx</th>
		<!--<th rowspan="2" width="10%">Sisa Quota API XL</th>-->
		<th rowspan="2" width="10%">Sisa Quota XL</th>
		<!--<th rowspan="2" width="8%">Selisih</th>
		<th rowspan="2" width="10%">Selisih</th>-->
		<th colspan="2">Penjualan</th>
		<!--<th rowspan="2" width="10%">Exp Date</th>-->
	</tr>
	<tr>
		<th width="10%">K-Mart</th>
		<th width="10%">K-Net</th>
	</tr>
	<?php
		//if(property_exists($xl_rekap_stock, "stock")) {
		//print_r($rekap_xl);
		foreach($rekap_xl as $stock) {
			echo "<tr>";
			echo "<td align=center>$stock[prod_id]</td>";
			echo "<td align=center>$stock[nama_paket]</td>";
			echo "<td align=right>".number_format($stock['kuota_db'], 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($stock['qty_record'], 0, ",", ".")."</td>";
			//echo "<td align=right>".number_format($stock['kuota_xl'], 0, ",", ".")."</td>";
			$jum_trx = $stock['kuota_db'] - $stock['qty_record'];

			 //$selisih = $stock['kuota_xl'] - $jum_trx;
			 echo "<td align=right>".number_format($jum_trx, 0, ",", ".")."</td>";
			//echo "<td align=right>".number_format($selisih, 0, ",", ".")."</td>";
			
			if(array_key_exists("kmart", $stock)) {
				echo "<td align=right>".number_format($stock['kmart'], 0, ",", ".")."</td>";
			} else {
				echo "<td align=right>0</td>";
			}
			if(array_key_exists("knet", $stock)) {
				echo "<td align=right>".number_format($stock['knet'], 0, ",", ".")."</td>";
			} else {
			    echo "<td align=right>0</td>";
			}
			//echo "<td align=center>$stock[exp_date]</td>";
			echo "</tr>";
		
		}
		//}
	?>
</table>
<br />

<br />
<?php
if($xl_last_trx != null) {
?>
<table width="80%" border="1">
	<tr>
	    <th colspan="4">Transaksi Terakhir Paket Data XL</th>
	</tr>
	<tr>
		<th width="15%">Trx ID</th>
		<th width="25%">Nama Paket</th>
		<th>Pembeli</th>
		<th width="15%">Tgl</th>
		<!--<th width="15%">Sisa Kuota</th>
		<th width="15%">Selisih</th>-->
	</tr>
	<?php
		//$kuota_xl = $xl_rekap_stock->stock;
		foreach($xl_last_trx as $ppobx) {
		    //$sisa_kuota = $ppob->kuota - $ppob->qty_record;
			echo "<tr>";
			echo "<td align=center>$ppobx->trx_id</td>";
			echo "<td align=center>$ppobx->nama_paket</td>";
			echo "<td align=center>".$ppobx->memberid." - ".$ppobx->membername."</td>";
			echo "<td align=center>$ppobx->createdt</td>";
			
			echo "</tr>";
		
		}
	?>
</table>


<?php
}
if($xl_ppob_not_success != null) {
?>
<table width="100%" border="1">
	<tr>
	    <th colspan=5>Data PPOB XL sudah masuk tapi belum ada reff_id/Invalid No HP</th>
	</tr>
	<tr>
		<th>Trx ID</th>
		<th>ID Member</th>
		<th>Nama Paket</th>
		<th>No HP</th>
		<th>No Reff</th>
	</tr>
	<?php
		foreach($xl_ppob_not_success as $data) {
		
			echo "<tr>";
			echo "<td align=center>$data->trx_id</td>";
			echo "<td align=center>".$data->memberid." / ".$data->membername."</td>";
			echo "<td align=right>$data->nama_paket</td>";
			echo "<td align=center>$data->cust_no</td>";
			echo "<td align=center>$data->reff_pay_id</td>";
			echo "</tr>";
		
		}
	?>
</table>
<?php
}
?>
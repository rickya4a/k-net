	
<div class="row vpullset4">
    <div class="container">
    <!--right-->
    	<div class="col-md-12" id="mainDiv">
        <!-- Track Order -->
            <!-- tracking content header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                    Rekap Transaksi Virtual Account	
                </div>            
            </div>
            <form id="listVaTrx">
            <div class="col-lg-12 col-xs-12 cart_content_noborder" style="font-size:14px;">  
                
                <!--line 1-->
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID()." / ".getUserName(); ?> 
                </div> 
                <!--line 1-->
                <!--line 2-->
                <div class="col-lg-3 col-xs-3">
                    Sisa Saldo VA  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $saldo[0]->amount; ?>
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Rekap Trx Harian  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <select id="tgl" name="tgl">
                   	 <?php
                   	  $tglaktif = date("d");
                   	  for($i = 1;$i <= 31; $i++) {
				    		$val = sprintf("%02s", $i);	
						    if($tglaktif == $val) {
						    	$selected = "selected=selected";
								echo "<option $selected value=\"$val\">$val</option>";
						    } else {
				    			echo "<option value=\"$val\">$val</option>";
							}
						}	
                   	 ?>
                   </select>
                   <select id="bln" name="bln">
                   	 <?php
                   	  $blnaktif = date("m");
                   	  for($i = 1;$i <= 12; $i++) {
				    		$val = sprintf("%02s", $i);	
						    if($blnaktif == $val) {
						    	$selected = "selected=selected";
								echo "<option $selected value=\"$val\">$val</option>";
						    } else {
				    			echo "<option value=\"$val\">$val</option>";
							}
				    		
						}	
                   	 ?>
                   </select>
                   <select id="thn" name="thn">
                   	 <?php
                   	 $date = date("Y");
					 $dateAkhir = $date - 2;
                   	  for($i = $date;$i >= $dateAkhir; $i--) {
				    		$val = sprintf("%02s", $i);	
				    		echo "<option value=\"$val\">$val</option>";
						}	
                   	 ?>
                   </select>	
                   <input type="button" id="btn_recruit" onclick="getListVaTrx()" value="Lihat Rekap Trx" />
                </div>
                <!--line 2-->                
            </div>
            </form>
            <!-- table result-->
            <!-- result header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
            	<div class="col-lg-1 col-xs-12">No</div>
                <div class="col-lg-3 col-xs-12">Trx ID</div>
                <div class="col-lg-2 col-xs-12">Jenis Trx</div>
                <div class="col-lg-2 col-xs-12">D/K</div> 
                <div class="col-lg-2 col-xs-12">Nominal</div> 
                <div class="col-lg-2 col-xs-12">Tgl Trx</div>
            </div>
            <div id="listTrx" class="col-lg-12 col-xs-12 cart_content_noborder" style="height:300px; overflow-y: auto;">
            	
            </div>
        
        </div>
       
    </div>
    <div class="col-md-12" id="divUpd">
    	
    </div>
 </div>	
<script>
	function getListVaTrx() {
		$.ajax({
			            //url: All.get_url('sgo_ppob/saveTemp'),
			            url: All.get_url('va/history/trx/list'),
			            type: 'POST',
			            data: $("#listVaTrx").serialize(),
						dataType: 'json',
			            success:
			            function(data){
			            	$("input[type=button]").removeAttr('disabled');
			            	if(data.response == 'true') {
								var arrayData = data.arrayData;
								var xhtml = "";
								$.each(arrayData, function(key, value) {
									xhtml += "<div class='col-lg-1 col-xs-1'>"+(key+1)+"</div>";
					                xhtml += "<div class='col-lg-3 col-xs-2'><a class='btn btn-small' href='javascript:getDetailTrxPpobById('"+value.trcd+"')'>"+value.trcd+"</a></div>";
					                xhtml += "<div class='col-lg-2 col-xs-2'>"+value.shortdescription+"</div>";
					                xhtml += "<div class='col-lg-2 col-xs-2'>"+value.tipe_dk+"</div>"; 
					                xhtml += "<div class='col-lg-2 col-xs-2'>"+All.num(parseInt(value.amount))+"</div>"; 
					                xhtml += "<div class='col-lg-2 col-xs-2'>"+value.createdt+"</div>";
								});
								$("#listTrx").html("<h3>Mohon menunggu, data sedang di proses..</h3>");
								$("#listTrx").html(null);
								$("#listTrx").html(xhtml);
							} else {
		                        alert(data.message);
		                        $("#listTrx").html(null);
							}	
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                 alert(thrownError + ':' +xhr.status);
							 $("input[type=button]").removeAttr('disabled');
			            }
			        });				
	}
	
	function getDetailTrxPpobById(param) {
		alert(param);
	}
</script>
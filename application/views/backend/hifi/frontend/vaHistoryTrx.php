<style>
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		padding: 3px;
	}	
	
	.btn {
		padding: 0 12px;
	}
</style>	
<div class="row vpullset4">
    <div class="container">
    <!--right-->
    	<div class="col-md-12" id="mainDiv">
        <!-- Track Order -->
            <!-- tracking content header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                    Rekap Transaksi Virtual Account	
                </div>            
            </div>
            <form id="listVaTrx">
            <div class="col-lg-12 col-xs-12 cart_content_noborder" style="font-size:14px;">  
                
                <!--line 1-->
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID()." / ".getUserName(); ?> 
                </div> 
                <!--line 1-->
                <!--line 2-->
                <div class="col-lg-3 col-xs-3">
                    Sisa Saldo VA  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php 
                   if($saldo == null) {
                   	  echo "Belum ada rekap top up saldo";
                   }else {
                   	  echo $saldo[0]->amount;	
                   }
                    ?>
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Pilih Tipe Transaksi
                </div>
                <div class="col-lg-9 col-xs-9">
                   <select id="trx_type" name="trx_type">
                   	<option value="all">--ALL TRANSACTION--</option>
                   	<?php
                   	  foreach($listTrtype as $listX) {
                   	  	echo "<option value=\"$listX->trxtype\">$listX->description</option>";
                   	  }
                   	  //print_r($listTrtype);
                   	?>
                   	
                   </select>
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Tgl Transaksi    
                </div>
                <div class="col-lg-9 col-xs-9">
                   <select id="tglf" name="tglf">
                   	 <?php
                   	  $tglaktif = date("d");
                   	  for($i = 1;$i <= 31; $i++) {
				    		$val = sprintf("%02s", $i);	
						    if($tglaktif == $val) {
						    	$selected = "selected=selected";
								echo "<option $selected value=\"$val\">$val</option>";
						    } else {
				    			echo "<option value=\"$val\">$val</option>";
							}
						}	
                   	 ?>
                   </select>
                   <select id="blnf" name="blnf">
                   	 <?php
                   	  $blnaktif = date("m");
                   	  for($i = 1;$i <= 12; $i++) {
				    		$val = sprintf("%02s", $i);	
						    if($blnaktif == $val) {
						    	$selected = "selected=selected";
								echo "<option $selected value=\"$val\">$val</option>";
						    } else {
				    			echo "<option value=\"$val\">$val</option>";
							}
				    		
						}	
                   	 ?>
                   </select>
                   <select id="thnf" name="thnf">
                   	 <?php
                   	 $date = date("Y");
					 $dateAkhir = $date - 2;
                   	  for($i = $date;$i >= $dateAkhir; $i--) {
				    		$val = sprintf("%02s", $i);	
				    		echo "<option value=\"$val\">$val</option>";
						}	
                   	 ?>
                   </select>&nbsp;-&nbsp;	
                   <select id="tgl" name="tgl">
                   	 <?php
                   	  $tglaktif = date("d");
                   	  for($i = 1;$i <= 31; $i++) {
				    		$val = sprintf("%02s", $i);	
						    if($tglaktif == $val) {
						    	$selected = "selected=selected";
								echo "<option $selected value=\"$val\">$val</option>";
						    } else {
				    			echo "<option value=\"$val\">$val</option>";
							}
						}	
                   	 ?>
                   </select>
                   <select id="bln" name="bln">
                   	 <?php
                   	  $blnaktif = date("m");
                   	  for($i = 1;$i <= 12; $i++) {
				    		$val = sprintf("%02s", $i);	
						    if($blnaktif == $val) {
						    	$selected = "selected=selected";
								echo "<option $selected value=\"$val\">$val</option>";
						    } else {
				    			echo "<option value=\"$val\">$val</option>";
							}
				    		
						}	
                   	 ?>
                   </select>
                   <select id="thn" name="thn">
                   	 <?php
                   	 $date = date("Y");
					 $dateAkhir = $date - 2;
                   	  for($i = $date;$i >= $dateAkhir; $i--) {
				    		$val = sprintf("%02s", $i);	
				    		echo "<option value=\"$val\">$val</option>";
						}	
                   	 ?>
                   </select>	
                   <input type="button" id="btn_recruit" onclick="getListVaTrx()" value="Lihat Rekap Trx" />
                </div>
                <!--line 2-->                
            </div>
            </form>
            <!-- table result-->
            <!-- result header-->
            <table class="table table-bordered table-striped">
            	<thead>
            		<tr>
            		  <th width="5%">No</th>
            		  <th width="15%">Trx ID</th>
            		  <th>Jenis Transaksi</th>
            		  <th width="5%">D/K</th>
            	      <th width="10%">Nominal</th>
            		  <th width="15%">Tgl Trx</th>
            		  <th width="8%">Detail</th></tr>
            	</thead>
            	<tbody id="listTrx">
            		<?php
	            	 if($res != null) {
	            	 	$i = 1;
	            	 	foreach($res as $trx) {
	            	 		echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>$trx->trcd</td>";
							echo "<td>$trx->shortdescription</td>";
							echo "<td>$trx->tipe_dk</td>";
							echo "<td>".number_format($trx->amount, 0, ",", ".")."</td>";
							echo "<td>$trx->createdt</td>";
							echo "<td><a class=\"btn btn-mini btn-success\" id=\"$trx->trcd\" onclick=\"javascript:getDetailTrxPpobById(this)\">Detail<a/></td>";
							echo "</tr>";
							$i++;
						}
					 }		
	            	?>
            	</tbody>
            </table>
            <!--
            <div class="col-lg-12 col-xs-12 cart_header">  
            	<div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-3 col-xs-2">Trx ID</div>
                <div class="col-lg-2 col-xs-2">Tipe</div>
                <div class="col-lg-2 col-xs-2">D/K</div> 
                <div class="col-lg-2 col-xs-2">Nominal</div> 
                <div class="col-lg-2 col-xs-2">Tgl Trx</div>
            </div>
            
            <div id="listTrx" class="col-lg-12 col-xs-12 cart_content_noborder" style="height:300px; overflow-y: auto;">
            	<?php
            	 if($res != null) {
            	 	$i = 1;
            	 	foreach($res as $trx) {
            	?>
            	<div class="col-lg-1 col-xs-1"><?php echo $i;?></div>
                <div class="col-lg-3 col-xs-2"><?php echo $trx->trcd;?></div>
                <div class="col-lg-2 col-xs-2"><?php echo $trx->shortdescription;?></div>
                <div class="col-lg-2 col-xs-2"><?php echo $trx->tipe_dk;?></div> 
                <div class="col-lg-2 col-xs-2"><?php echo number_format($trx->amount, 0, ",", ".");?></div> 
                <div class="col-lg-2 col-xs-2"><?php echo $trx->createdt;?></div>
            	<?php
					}
            	 }
            	?>
            </div>
            -->
        </div>
       
    </div>
    <div class="col-md-12" id="divUpd">
    	
    </div>
 </div>	
<script>
	function getListVaTrx() {
		$("input[type=button]").attr('disabled', 'disabled');
		$("#listTrx").html(null);
		$("#listTrx").html("<tr align=center><td colspan=7><b>Mohon menunggu, data sedang di proses..</b></td></tr>");
		$.ajax({
			            //url: All.get_url('sgo_ppob/saveTemp'),
			            url: All.get_url('va/history/trx/list'),
			            type: 'POST',
			            data: $("#listVaTrx").serialize(),
						dataType: 'json',
			            success:
			            function(data){
			            	$("input[type=button]").removeAttr('disabled');
			            	if(data.response == 'true') {
								var arrayData = data.arrayData;
								var xhtml = "";
								$.each(arrayData, function(key, value) {
									var link = "javascipt:getDetailTrxPpobById('"+value.trcd+"')";
									xhtml += "<tr><td>"+(key+1)+"</td>";
					                xhtml += "<td>"+value.trcd+"</td>";
					                xhtml += "<td>"+value.shortdescription+"</td>";
					                xhtml += "<td>"+value.tipe_dk+"</td>"; 
					                xhtml += "<td>"+All.num(parseInt(value.amount))+"</td>"; 
					                xhtml += "<td>"+value.createdt+"</td>";
					                xhtml += "<td><a class='btn btn-mini btn-success' id='"+value.trcd+"' onclick=getDetailTrxPpobById(this)>Detail</a></td></tr>";
								});
								
								$("#listTrx").html(null);
								$("#listTrx").html(xhtml);
							} else {
		                        alert(data.message);
		                        $("#listTrx").html(null);
							}	
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                 alert(thrownError + ':' +xhr.status);
							 $("input[type=button]").removeAttr('disabled');
			            }
			        });				
	}
	
	function getDetailTrxPpobById(paramx) {
		var param = paramx.id
		$.ajax({
			
            //url: All.get_url('sgo_ppob/saveTemp'),
            url: All.get_url('sgo_ppob/detail/') +param,
            type: 'GET',
            success:
            function(data){
            	$("#mainDiv").hide();
            	$("#divUpd").html(null);
            	$("#divUpd").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 $("input[type=button]").removeAttr('disabled');
            }
        });			
	}
	
	function back_to_form() {
		$("#divUpd").html(null);
		$("#mainDiv").show();
	}
</script>
 <div align="center">
  <form class="form-horizontal" id="sgoPpobTrx">
    <fieldset>      
      <div >
      
        <label  >ID Member</label>                             
        <div class="controls">
        	<input type="text" style="width: 400px" id="memberid" name="memberid" readonly="readonly" value="<?php echo getUserID(); ?>" />
        </div>
        
         <label  >Nama Member</label>                             
        <div class="controls">
        	<input type="text" style="width: 400px" id="membername" name="membername" value="<?php echo getUsername(); ?>" />
        </div>
        
        <label  >Email</label>                             
        <div class="controls">
        	<input type="text" style="width: 400px" id="email" name="email" readonly="readonly" value="<?php echo getUserEmail(); ?>" />
        </div>
        
        <label  >Pilih Bank</label>
        <div class="controls">
        	<select style="width: 400px" id="bank" name="bank">
				<?php
				echo "<option value=\"\">--Pilih disini--</option>";
	
				foreach($listBank as $dta) {
					echo "<option value=\"$dta->bankCode\">$dta->bankName</option>";
				}
				?>
			</select>
			<input type="hidden" id="novac" name="novac" value="<?php echo getUserNovac(); ?>" />
        </div>
        
        <label  >&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <!--<input tabindex="3" type="button" id="btn_cek_tagihan" class="btn btn-success" name="save" value="Cek Tagihan" onclick="cekTagihan()" />-->
            <input tabindex="4" type="button" id="btn_next_pay" class="btn btn-primary" name="save" value="Lanjut Registrasi" onclick="submitDataVaReg()" />
            
         </div>
        </div> <!-- end control-group -->
        
     </fieldset>
    
    <div class="result">
		<?php
		//print_r($listVa);
		if($listVa != null) {
		?>
		<table class="table table-bordered table-striped">
			<thead>
			  <tr>
				  <th colspan="3">List Virtual Account</th>
			  </tr>
			  <tr>
				  <th>No</th>
				  <th>Bank</th>
				  <th>No Virtual Account</th>
			  </tr>
			</thead>
			<tbody>
			<?php
				$i=1;
				foreach($listVa as $dtax) {
					echo "<tr>";
					echo "<td>$i</td>";
					echo "<td>$dtax->nama_bank</td>";
					echo "<td>$dtax->va_number</td>";
					echo "</tr>";
					$i++;
				}
			?>
			</tbody>
		</table>
		<?php
		}
		?>
	</div>
    <div class="result2"></div>
    <div>
		  <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
		</div>
  </form> 
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script>
    
	
	function submitDataVaReg() {
		
		var bank= $('#bank').val();
		//  var nama_bank= $('#bank option:selected"').text();
		var nama_bank= $("#bank option:selected").text();
		if(bank==''){
			alert("Silahkan pilih bank..");
		} else {
			$("input[type=button]").attr('disabled', 'disabled');
			$(".result").html(null);
			$(".result").html("<h3><font color=red>Permintaan sedang diproses..</font></h3>");
			$.ajax({
	            url: All.get_url('va/invoice/send/post'),
	            type: 'POST',
	            data: $("#sgoPpobTrx").serialize(),
				dataType: 'json',
	            success:
	            function(data){
	            	$(".result").html(null);
	            	$("input[type=button]").removeAttr('disabled');
	            	if(data.error_code == '0000') {
						alert("Nomor Virtual Account anda : " +data.va_number);
						console.log("Kode Bank : " +bank);
						console.log("Nama Bank : " +nama_bank);
						console.log("NO VA : " +data.va_number);
						console.log("fee : " +data.fee);
						$.ajax({
							url: All.get_url('va/member/save'),
							type: 'POST',
							data: {va_number: data.va_number, fee: data.fee, kode_bank: bank, nama_bank: nama_bank},
							dataType: 'json',
							success:
								function(data){

								},
							error: function (xhr, ajaxOptions, thrownError) {
								alert(thrownError + ':' +xhr.status);
								$("input[type=button]").removeAttr('disabled');
							}
						});
					} else {
                        alert(data.error_message);
					}	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 $("input[type=button]").removeAttr('disabled');
	            }
	        });		
			
			
		}	
	}

	
</script>    
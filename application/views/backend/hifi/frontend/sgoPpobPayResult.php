<?php
 if($res != null) {
 echo show_info_ppob_payresult($res, $param_data);
 } else {
 	echo "Detail transaksi TOP UP tidak tersedia..";
 }	 
?>
<div id="showStatus"></div>
<script>
	function checkPpobStatus(paramx) {
		var ref_pay_id = paramx.id;
		$("input[type=button]").attr('disabled', 'disabled');
		$(".btn").attr('disabled', 'disabled');
		$.ajax({
			
            //url: All.get_url('sgo_ppob/saveTemp'),
            url: All.get_url('sgo_ppob/inquiry/bill/status'),
            data: {ref_id: ref_pay_id},
            type: 'POST',
            dataType: 'json',
            success:
            function(data){
            	$("input[type=button]").removeAttr('disabled');
		        $(".btn").removeAttr('disabled');
            	//$("#mainDiv").hide();
            	if(data.error_code == "0000") {
            		alert('Transaksi : ' +data.error_desc+ ' '+data.status_desc);
            	} else {
            		alert('Transaksi gagal: ' +data.error_desc+ ' '+data.status_desc);
            	}
            	
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 $("input[type=button]").removeAttr('disabled');
            }
        });			
	}
</script>

 <div align="center">
  <form class="form-horizontal" id="sgoPpobTrx">
    <fieldset>      
      <div >
      	<label  >ID Member</label>
      	<div class="controls">
        	<input type="text"  style="width: 400px" name="idmember" id="idmember" value="<?php echo getUserID(); ?>" onchange="Shopping.getMemberNameByID(this.form.idmember, '#membername')"  />
        	<!--<input type="button" id="checkIDMember" value="Check ID" class="btn1 btn2 btn-primary1" onclick="Shopping.getMemberNameByID(this.form.idmember, '#membername')" />-->
        	
        </div>
        <label  >Nama Member</label>
        <div class="controls">
        	<input type="text" style="width: 400px" name="membername" readonly="readonly" id="membername" value="<?php echo getUsername(); ?>"  />
        </div>	
      	<label  >Kategori PPOB</label>                             
        <div class="controls">
        	<select id="cat" name="cat" style="width: 400px" onchange="pilTrx(this.value)">
        		<option value="">--Select here--</option>
        		<?php
        		 foreach ($listCat as $dta) {
					 echo "<option value=\"$dta->id\">$dta->cat_name</option>";
				 }
        		?>
        	</select>
        </div>
       
        <label  >Tipe Transaksi PPOB</label>                             
        <div class="controls">
        	<select id="prd_id" name="prd_id" style="width: 400px" onchange="pilProdPpob(this.value)">
        		<option value="">--Select here--</option>
        		
        	</select>
        	<input type="hidden" name="product_code" id="product_code" value=""  />
			<input type="hidden" name="product_name" id="product_name" value=""  />
			<input type="hidden" name="inq_status" id="inq_status" value=""  />
        </div>
        <label  >Order / Customer ID</label>                             
        <div class="controls">
        	<input class="boleh_reset" type="text" style="width: 400px" id="order_id" name="order_id" placeholder="No Pelanggan/No HP" onchange="lanjutCekTagihan()" />
        </div>
        <label  >Nominal Pembayaran</label>                             
        <div class="controls">
        	<input class="boleh_reset" type="text" style="width: 400px" id="amount" name="amount" placeholder="" onchange="setAmountX(this.value)" />
        	<input class="boleh_reset" type="hidden" class="span6" id="amountx" name="amountx" placeholder="" />
        </div>
        <label  >Pilih Pembayaran</label>
        <div class="controls">
        	<select style="width: 400px" id="bank" name="bank" onchange="setSelectPay()">
				<?php
				echo "<option value=\"\">--Pilih disini--</option>";
	
				foreach($listBank as $dta) {
					echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
				}
				?>
			</select>
			<input type="hidden" name="totalx" value="" />
			<input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
			<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
			<input class="boleh_reset" type="hidden" name="bankid" id="bankid" value=""  />
			<input class="boleh_reset" type="hidden" name="bankCode" id="bankCode" value=""  />
			<input class="boleh_reset" type="hidden" name="bankDesc" id="bankDesc" value=""  />
			<input class="boleh_reset" type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
			<input class="boleh_reset" type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
			<input class="boleh_reset" type="hidden" name="charge_admin" id="charge_admin" value=""  />
			
			
			<input type="hidden" name="telp_hp" id="telp_hp" value="<?php echo getUserPhone(); ?>"  />
			<input class="boleh_reset" type="hidden" name="inq_data" id="inq_data" value=""  />
			<input class="boleh_reset" type="hidden" name="token" id="token" value=""  />
			
        </div>
        <label style="display: none;" class="sal_va">Saldo Virtual Account</label>                             
        <div style="display: none;" class="sal_va" class="controls">
        	<input type="text" readonly="readonly" style="width: 400px" id="saldo_va" name="saldo_va"/>
        </div>
        <label  >&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_cek_tagihan" class="btn btn-success" name="save" value="Cek Tagihan" onclick="cekTagihan()" />
            <input tabindex="4" type="button" id="btn_next_pay" class="btn btn-primary" name="save" value="Lanjut Pembayaran" onclick="submitData()" />
            <input tabindex="5"  type="button" class="btn btn-reset" value="Reset" onclick="resetField()" />
         </div>
        </div> <!-- end control-group -->
        
     </fieldset>
    
    <div class="result"></div>
    <div class="result2"></div>
    <div>
		  <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
		</div>
  </form> 
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script>
    
    function setSelectPay() {
		var x = $("#bank").val();
		var bankDetail = $("#bank option:selected").text();
		//alert("isi : " +x);
		//var bankDesc = $("#bank option:selected").text();
		var str = x.split("|");
		var bankid = str[0];
		$("#bankid").val(bankid);
		$("#bankCode").val(str[1]);
		//$("#bankDesc").val(bankDesc);
		$("#bankDesc").val(str[4]);
		$("#charge_connectivity").val(str[2]);
		$("#charge_admin").val(str[3]);
		$("#bankDescDetail").val(bankDetail);
       //alert("bankid : " +bankid);
        if(bankid === "25") {
        	$(".sal_va").css('display', 'block');
        	var idmember = "<?php echo getUserID(); ?>";
        	var password = "<?php echo getUserPassword(); ?>";
        	
        	var token = getEncryptToken(idmember, password);
        	//console.log(token);
        	/*if(token.response === "true") {
        	  var a = cekSaldoVa(idmember, token.arrayData);	
        	  if(a.response === "true") {
        	  	var arr = a.arrayData;
        	  	$("#saldo_va").val(arr[0].amount);
        	  } else {
        	  	alert("Belum ada saldo untuk virtual account anda..")
        	  }
        	}*/
        } else {
        	$("#saldo_va").val(null);
        	$(".sal_va").css('display', 'none');
        }

	}
	
	function cekSaldoVa(idmember, token) {
		var api_user = "k-net.apps";
        var api_key = "apps@2017";
		$.ajax({
            url: All.get_url('api/saldo/va'),
            type: 'POST',
            data: {api_usr: api_user, api_pwd: api_key, idmember: idmember, token: token},
			dataType: 'json',
			success: function(data) {
				$("input[type=button]").removeAttr('disabled');
				if(data.response == "true") {
					var arr = data.arrayData;
    	  	        $("#saldo_va").val(arr[0].amount);
    	  	        
				} else {
					alert("Saldo anda kosong..silahkan top up terlebih dahulu..");
					 $("#saldo_va").val(null);
				}
			}, error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 $("input[type=button]").removeAttr('disabled');
            }
            
        });    	
	}
	
	function getEncryptToken(idmember, password) {
		$("input[type=button]").attr('disabled','disabled');
		$.ajax({
            url: All.get_url('encrypt') +"/"+ idmember +"/"+ password,
            type: 'GET',
			dataType: 'json',
			success: function(data) {
				if(data.response == "true") {
					var tokenx = data.arrayData;
					$("#token").val(tokenx);
					cekSaldoVa(idmember, tokenx);
					
				}
			},
			
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 $("input[type=button]").removeAttr('disabled');
            }
			
			
        });   
	}
	
	function pilProdPpob(nilai) {
		var str = nilai.split("|");
	    var inq_status = str[2];
		$("#product_code").val(str[0]);
		$("#product_name").val(str[1]);
		$("#inq_status").val(str[2]);
		
		if(inq_status == "0") {
			//alert("Tipe transaksi ini tidak dapat mengecek jumlah nominal tagihan/pembayaran..");
			$(".result").html(null);
			$(".result").html("<h4><font color=red>Tipe transaksi ini tidak dapat mengecek jumlah nominal tagihan/pembayaran</font></h4>");
			$("#amount").removeAttr("readonly");
			$("#btn_cek_tagihan").attr("disabled", "disabled");
			$("#btn_next_pay").removeAttr("disabled");
		} else {
			$(".result").html(null);
			$(".result").html("<h4><font color=red>silahkan klik tombol cek tagihan untuk mendapatkan nominal pembayaran</font></h4>");
			//alert("silahkan klik tombol cek tagihan untuk mendapatkan nominal pembayaran..");
			$("#amount").attr("readonly", "readonly");
			$("#btn_cek_tagihan").removeAttr("disabled");
			$("#btn_next_pay").attr("disabled", "disabled");
		}
	}	
	
	function setAmountX(nilai) {
		var x = parseInt(nilai);
		//x = x/100;
    	x = numeral(x).format('0.00');
    	$("#amount").val(x);
    	xn = nilai + "00";
    	$("#amountx").val(xn);
	}
	
	function lanjutCekTagihan() {
		var inq_status = $("#inq_status").val();
		if(inq_status == "1") {
			alert("Silahkan klik tombol Cek Tagihan..");
			//cekTagihan();
		}
	}
	
	function cekTagihan() {
	    
        var order_id = $("#order_id").val();
        if(order_id === '') {
        	alert("Silahkan isi No Pelanggan/No HP terlebih dahulu");
        	return;
        } 
        $("input[type=button]").attr('disabled','disabled');
        $.ajax({
	            url: All.get_url('sgo_ppob/inquiry/show'),
	            type: 'POST',
	            data: $("#sgoPpobTrx").serialize(),
				dataType: 'json',
	            success:
	            function(data){
	                $("input[type=button]").removeAttr('disabled');
		            if(data == null) {
		            	alert('Permintaan tidak dapat diproses, coba tipe transaksi yang lain..');
		            } else {
			            if(data.error_code == "0000") {
			            	var x = parseInt(data.amount);
			            	x = x/100;
			            	x = numeral(x).format('0.00');
			            	$("#amount").val(x);
			            	$("#amountx").val(data.amount);
			            	$("#inq_data").val(JSON.stringify(data.data));
			            	var product_code = $("#product_code").val();
			            	var htmlx = "<table align=center border=0 width=50%>";
			            	htmlx += setInfoPpob(data.data);
			            	htmlx += "</table>";
			            	$(".result2").html(null);
			            	$(".result2").append(htmlx);
			            	$("#btn_next_pay").removeAttr("disabled");
			            }  else {
			            	alert(data.error_desc);
			            	$("#amount").val(null);
			            	$("#amountx").val(null);
			            }
		            } 
	             },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 $("input[type=button]").removeAttr('disabled');
	            }
	        });	
	}
	
	function setInfoPpob(data) {
		var tablex = "";
		for(var key in data) {
			tablex += "<tr><td>"+key+"</td><td>"+data[key]+"</td></tr>";
		}
		return tablex;
	}
	
	function pilTrx(nilai) {
		$("input[type=button]").attr('disabled','disabled');
		$.ajax({
	            url: All.get_url('sgo_ppob/trx/listByCat') + "/" +nilai,
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){
	            	$("input[type=button]").removeAttr('disabled');
	            	var arrayData = data.arrayData;
	            	if(data.response == "true") {
	            		//All.clear_div_in_boxcontent("#ppob_trx_type");
	            		$("#prd_id").html(null);  
	            		var htmlx = "<option value=''>--Pilih disini--</option>";
	            		$.each(arrayData, function(key, value) {
	            			var kodex = value.product_code+"|"+value.product_name+"|"+value.inquiry;
							htmlx += "<option value='"+kodex+"'>"+value.product_name+"</option>";
                        });
                        
                        $("#prd_id").append(htmlx);
	            	} else {
	            		alert(data.message);
	            		//All.clear_div_in_boxcontent("#ppob_trx_type");
	            		$("#prd_id").html(null);
	            		var htmlx = "";
	            		htmlx += "<option value=''>--Pilih disini--</option>";
	            		$("#prd_id").append(htmlx);
	            	}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 $("input[type=button]").removeAttr('disabled');
	            }
	        });
	}
	
	function resetField() {
		$(".boleh_reset").val(null);
		$("#saldo_va").val(null);
        $(".sal_va").css('display', 'none');
        $("#bank").val(null);
        $(".result").html(null);
        $(".result2").html(null);
	}
	
	function submitData() {
		var bankid= $('#bankid').val();
		if(bankid==''){
			alert("Silahkan pilih tipe pembayaran..");
		} else {
				
			if(bankid !== '25') {
				$.ajax({
		            url: All.get_url('sgo_ppob/saveTemp'),
		            type: 'POST',
		            data: $("#sgoPpobTrx").serialize(),
					dataType: 'json',
		            success:
		            function(data){
		            	if(data.response == 'true') {
							var payment=data.arrayData;
							var orderno=payment[0].trx_id;
							var total_pay=payment[0].nominal;
	                        
								var data = {
										key : "<?php echo $keySgoPayment; ?>",
										paymentId : orderno,
										paymentAmount : total_pay,
										backUrl : "http://www.k-net.co.id/sgo_ppob/trx/id/"+orderno,
										bankCode : $('#bankCode').val(),
										bankProduct: $('#bankDesc').val()
									},
									sgoPlusIframe = document.getElementById("sgoplus-iframe");
		
								if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
								SGOSignature.receiveForm();
							
						} else {
	                        alert("Problem insert data..");
						}	
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 //$("input[type=button]").removeAttr('disabled');
		            }
		        });		
			} else {
				$("input[type=button]").attr("disabled", "disabled");
				$.ajax({
			            //url: All.get_url('sgo_ppob/saveTemp'),
			            url: All.get_url('sgo_ppob/save/va'),
			            type: 'POST',
			            data: $("#sgoPpobTrx").serialize(),
						dataType: 'json',
			            success:
			            function(data){
			            	$("input[type=button]").removeAttr('disabled');
			            	if(data.response == 'true') {
								alert("Transaksi Berhasil..");
								resetField();
								var arrayData = data.arrayData;
								var xhtml = "<table border=0 width=50%>";
								xhtml += "<tr><td width=25%>No Trx</td><td>:</td><td>"+arrayData[0].trx_id+"</td></tr>";
								xhtml += "<tr><td>Nominal</td><td>:</td><td>"+arrayData[0].nominal+"</td></tr>";
								//xhtml += "<tr><td>Reff No</td><td>:</td><td>"+arrayData[0].reff_pay_id+"</td></tr>";
								xhtml += "<table>";
								$(".result").html(null);
								$(".result").html(xhtml);
							} else {
		                        alert(data.message);
							}	
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                 alert(thrownError + ':' +xhr.status);
							 $("input[type=button]").removeAttr('disabled');
			            }
			        });				
			} 
			
		}	
	}
  
    $( document ).ready(function() {  
	   $("input[type=text]").keyup(function(){
		 $(this).val( $(this).val().toUpperCase() );
	   });
   });

	
</script>    
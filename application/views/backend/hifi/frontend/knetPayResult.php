<?php
if($res == null) {
	echo "<div class='alert alert-error'>Transaksi tidak ditemukan..</div>";
} else {
//print_r($res);
?>
<table class="table table-bordered table-striped table-bordered">
	<tr>
		<th colspan="2">Transaksi K-Net</th>
	</tr>
	<tr>
		<td  width="25%">Order No</td>
		<td><?php echo $res[0]->orderno; ?></td>
	</tr>
	<tr>
		<td>Member</td>
		<td><?php echo $res[0]->id_memb. " / ".$res[0]->nmmember; ?></td>
	</tr>
	<tr>
		<td>Total Pay / BV</td>
		<td><?php echo number_format($res[0]->total_pay, 0, ",", "."). " / ". number_format($res[0]->total_bv, 0, ",", ".") ?></td>
	</tr>
</table>

<?php
}
?>
<input type="button" class="btn btn-mini btn-warning" value="<< Kembali" onclick="back_to_form()"  />
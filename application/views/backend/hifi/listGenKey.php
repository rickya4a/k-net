<?php if ($hasil == NULL) {
    echo setErrorMessage();
} else { ?>

    <div class="table-responsive">
        <table class="table table-hover table-bordered bootstrap-datatable datatable" width="50%">
            <thead>
                <tr>
                    <th colspan='3'>List Generated Key</th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Success</th>
                    <th>Insert Double</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($hasil as $key ) { ?>
                <tr>
                    <td style='text-align: center;'><?php echo $key['waktu']; ?></td>
                    <td style='text-align: center;'><?php echo $key['ins_sukses']; ?></td>
                    <td style='text-align: center;'><?php echo $key['ins_double']; ?></td>
                </tr>
            <?php
            }; ?>
            </tbody>
        </table>
    </div>

<?php
}
; ?>
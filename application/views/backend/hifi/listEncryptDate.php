<?php if ($result == NULL) {
    echo setErrorMessage();
} else { ?>

<div class="table-responsive">
    <table class="table table-bordered bootstrap-datatable datatable">
        <thead>
            <tr>
                <th colspan='2'>List Encrypt</th>
            </tr>
            <tr>
                <th>Jumlah Record</th>
                <th>Tanggal</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $key ) { ?>
            <tr>
                <?php if ($key->jumlah_record < 24) { ?>
                <td style="color: red"><?php echo $key->jumlah_record; ?></td>
                <?php } else { ?>
                <td><?php echo $key->jumlah_record; ?></td>
                <?php }; ?>
                <td><?php echo $key->encrypt_date; ?></td>
            </tr>
        <?php }; ?>
        </tbody>
    </table>
</div>

<?php
}
; ?>
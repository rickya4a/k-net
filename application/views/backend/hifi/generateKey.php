<?php
if ($res && $rec != NULL) {
?>
<div class="mainForm">

    <form class="form-horizontal" role="form" id="genForm">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">Latest Encryption Time</label>
                <div class="controls">
                    <input
                        type="text"
                        class="typeahead"
                        value="<?php echo $res; ?>"
                        disabled="disabled"/>
                </div>
                <label for="typehead" class="control-label">Record</label>
                <div class="controls">
                    <input
                        type="text"
                        class="typehead"
                        value="<?php echo $rec[0]->jumlah_record;?>"
                        disabled="disabled"/>
                </div>
                <label class="control-label" for="typeahead">Date</label>
                <div class="controls">
                    <input type="text" class="dtpicker typeahead" name="from"/>&nbsp;To&nbsp;
                    <input type="text" class="dtpicker typeahead" name="to"/>
                </div>
            </div>

            <div class="control-group" id="inp_btn">
                <div class="controls col-sm-10 col-sm-offset-2">
                    <input
                        tabindex="3"
                        type="button"
                        id="btn_input_user"
                        class="btn btn-primary .submit"
                        name="save"
                        value="Submit"
                        onclick="All.ajaxFormPost(this.form.id,'xl/getencryptiondate')"/>
                    <input
                        tabindex="3"
                        type="button"
                        id="btn_input_user"
                        class="btn btn-success .submit"
                        name="save"
                        value="Check List"
                        onclick="All.ajaxFormPost(this.form.id,'xl/checkencrypt')"/>
                </div>
            </div>
        </fieldset>
    </form>
    <div class="result"></div>

</div>
<script>
    $(document).ready(function () {
        $(All.get_active_tab() + " .dtpicker")
            .datepicker(
                {changeMonth: true, numberOfMonths: 1, dateFormat: 'yy-mm-dd'}
            )
            .datepicker("setDate", new Date());;
    });
</script>

<?php } else {
    setErrorMessage();
}
; ?>
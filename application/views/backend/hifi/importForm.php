<div class="mainForm">
<!--<div id="formDOO">-->
  <form class="form-horizontal" id="hifiTrx" >
    <fieldset>      
      <div class="control-group">        		      
       <label class="control-label" for="typeahead">Transaction Date</label>
       <div class="controls">
           <input type="text" class="dtpicker typeahead" id="hf_date_from" name="from" />&nbsp;to&nbsp;
		   <input type="text"  class="dtpicker typeahead" id="hf_date_to" name="to" />
       </div>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-warning .submit" name="save" value="Get List" onclick="All.ajaxFormPost(this.form.id,'hifi/trx/import/preview')" />
            
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="5" type="button" class="btn btn-primary .submit" name="save" value="Import to Database" onclick="All.ajaxFormPost(this.form.id,'hifi/trx/import/save')" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

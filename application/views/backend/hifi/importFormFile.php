
<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="post" id="formImportHifiFile" action="<?php echo base_url('be/member/voucherkey/print'); ?>">
    <fieldset>      
      <div class="control-group">
                 
        <label class="batch control-label" for="typeahead">File CSV to upload</label>
            <div class="batch controls" >
              <input type="file" id="hifiFile" name="hifiFile" class="span7 typeahead" />
              
            </div>
         <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-warning .submit" name="save" value="Preview List" onclick="All.readFromFile(this.form.id,'hifi/trx/importFile/preview')" />
            
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="5" type="button" class="btn btn-primary .uploadButton" name="save" value="Import to Database" onclick="All.readFromFile(this.form.id,'hifi/trx/importFile/save')" />
         </div> 
        
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div id="editor"></div>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
$(document).ready(function()
{
   
   $(All.get_active_tab() + " #hifiFile").change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'csv':
            case 'txt':
            
                $(All.get_active_tab() + ' #uploadButton').attr('disabled', false);
                break;
            default:
                alert('This is not an allowed file type, only CSV/TXT file..!!');
                this.value = '';
        }
   });
   
   
   
});
</script>

<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>
    <table align="center" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="7">List Hi-Fi Transaction</th>
    </tr>	
    <tr>
    	<!--<th width="5%"><input type="checkbox" name="checkall" onclick="All.checkUncheckAll(this)"></th>--> 
        <th width="5%">No.</th>
        <th width="25%">Trx ID</th>
        <th >Customer Name</th>
		<th >Customer No</th>
        <th width="10%">Trx Type</th>
        <th width="9%">Nominal</th>
        <th width="15%">Trx Date</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result as $row){
        // a.id, a.client, count(id) as jum_trx, sum(a.harga_jual) as harga_jual	
    ?>
    
    <tr>
        <!--<th align="center"><input id="pil1" type="checkbox" value="<?php echo $row->client;?>" name="client[]"></th>-->
        <td align="right"><?php echo $no;?></td>
        <td align="center"><a href='#' id='<?php echo $row->trx_id?>' onclick='javascript:be_voucher.getDetailHifi(this)'><?php echo $row->trx_id; ?></a></td>
        <td align="left"><?php echo $row->memberid."/".strtoupper($row->membername);?></td>
		<td align="center"><?php echo $row->cust_no;?></td>
        <td align="center"><?php echo strtoupper($row->trx_type); ?></td>
        <td align="right"><?php echo $row->nominal;?></td>
        <td align="center"><?php echo $row->trx_date;?></td>
        
   </tr>
    <?php
        
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

    function test(){
        alert('tes');
    }
</script>
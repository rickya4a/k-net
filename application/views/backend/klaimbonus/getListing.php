<?php
if(empty($result)) {
    setErrorMessage("No result found, try another date..");
} else {
    ?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
        <tr>
            <th colspan="8">List Klaim Bonus</th>
        </tr>
        <tr>
            <th width="5%">No.</th>
            <th width="10%">Period</th>
            <th width="15%">Paket Ladies</th>
            <th width="15%">Paket Umum</th>
            <th width="10%">TBV</th>
            <th width="10%">Voucher</th>
            <th width="10%">Status</th>
            <th width="10%">Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $no = 1;

        foreach($result as $row){

            //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs
            ?>

            <tr>

                <td align="right"><?php echo $no;?></td>

                <td  align="center">
                    <a id="<?php echo $row->bnsperiod;?>" onclick="All.ajaxShowDetailonNextForm('be_klaimbonus/xvc/<?php echo ($row->bnsperiod);?>/<?php echo $row->dfno;?>')">
                    <?php echo date("d-M-Y",strtotime($row->bnsperiod));?>
                    </a>
                </td>
                <td align="center"><?php echo $row->jmlPaket_Ladies;?></td>
                <td align="center"><?php echo $row->jmlPaket_Umum;?></td>
                <td align="center"><?php echo $row->ttbv;?></td>
                <td align="center"><?php echo $row->VoucherNo;?></td>
                <?php if($row->Claim_Status==0) {

                    ?>
                    <td align="center">Belum diklaim</td>
                    <?php
                }
                else{
                    ?><td align="center">Sudah diklaim</td>
                <?php }?>
                <td align="center"><a id="<?php echo $row->VoucherNo;?>" onclick="All.ajaxShowDetailonNextForm('be_klaimbonus/vc/<?php echo $row->VoucherNo;?>')">View</a></td>




            </tr>
            <?php

            $no++;
        }
        ?>
        </tbody>
    </table>

    <?php
}
?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

</script>
<?php
    if(empty($bns)){
        setErrorMessage();
    }else{
?>
<?php
	//print_r($result);
?>
<table width="100%" class="table table-striped table-bordered">
 <thead>
 <tr>
     <th colspan=5>DATA TRANSAKSI PERIODE <?php echo date("d-M-Y",strtotime($bns));?></th>
 </tr>


 <tr>
     <th width="10%">No.</th>
     <th width="10%">Nomor Transaksi</th>
     <th width="10%">Kode Produk</th>
     <th width="15%">Nama Produk</th>
     <th width="15%">Qty</th>

 </tr>

 </thead>
    <tbody>
    <?php
    $no = 1;

    foreach($result as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs
        ?>

        <tr>

            <td align="center"><?php echo $no;?></td>
            <td align="center"><?php echo $row->Trcd;?></td>
            <td align="center"><?php echo $row->prdcd;?></td>
            <td align="center"><?php echo $row->prdnm;?></td>
            <td align="center"><?php echo $row->qty;?></td>

        </tr>
        <?php

        $no++;
    }
    ?>
    </tbody>
</table>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
    </div>
	<p></p>
<?php
    }
?>

<!--<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });
</script>-->
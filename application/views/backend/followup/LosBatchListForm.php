<div class="mainForm">
  <form class="form-horizontal" id="batchList" method="post">
    <fieldset>      
      <div class="control-group">
      	<label class="control-label" for="typeahead">Batch Create Date</label>
        <div class="controls">
        	<input type="text" class="TabOnEnter dtpicker" id="batch_list_from" name="batch_list_from" tabindex="5" value="<?php echo $fu_from; ?>"/>&nbsp;
        	<input type="text" class="TabOnEnter dtpicker" id="batch_list_to" name="batch_list_to" tabindex="6" value="<?php echo $fu_to; ?>" />
        	<input type="button" class="btn btn-success" name="save" value="Search" onclick="<?php echo $act; ?>" />
        	
        </div>	
       
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

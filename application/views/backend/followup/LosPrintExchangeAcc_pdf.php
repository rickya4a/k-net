<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
		
		$settingLayout = settingLOSLayout($result);
		$sisaKeterangan = $settingLayout['totalWidth']-$settingLayout['widthKopTitle']-$settingLayout['doubleDot'];
		//print header
		LosPrintHeader($pdf, $settingLayout);		
		
		//Isi / Maksud penyampaian
		$alasan = "Dengan ini kami beritahukan bahwa adanya permintaan perpindahan jaringan karena ".strtoupper($result[0]->keterangan)."";
		$pdf->MultiCell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		
		//$pdf->SetFont('Helvetica','',$settingLayout['contentFontSize']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->fullnm),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->dfno),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		//$pdf->SetFont('Helvetica','B',$settingLayout['contentFontSize']);
		$alasan = "Untuk ditukar posisi/exchange dibawah distributor berikut ini :";
		$pdf->Cell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		//$pdf->SetFont('Helvetica','',$settingLayout['contentFontSize']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_nm),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID Member",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_id),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
			
		//Print footer
		LosPrintFooter($pdf, $settingLayout);
		$pdf->Output();
				
?>		
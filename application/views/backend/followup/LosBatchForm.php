<div class="mainForm">
  <form class="form-horizontal" id="batchLosCreate" method="post">
    <fieldset>      
      <div class="control-group">
      	<label class="control-label" for="typeahead">LOS Create Date</label>
        <div class="controls">
        	<input type="text" class="TabOnEnter dtpicker" id="batch_from" name="batch_from" tabindex="5" value="<?php echo $fu_from; ?>"/>&nbsp;
        	<input type="text" class="TabOnEnter dtpicker" id="batch_to" name="batch_to" tabindex="6" value="<?php echo $fu_to; ?>" />
        	<input type="button" class="btn btn-success" name="save" value="Search" onclick="<?php echo $act; ?>" />
        	<input type="button" class="btn btn-primary" name="save" value="Generate Batch LOS" onclick="<?php echo $save; ?>" />
        </div>	
       
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

<?php
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=document_name.doc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>LOS transfer Jaringan</title>
<style type="text/css">
	body {
		font-family: Verdana, Geneva, Arial, helvetica, sans-serif;
		word-spacing: normal;
		position: absolute;
		font-size: 24px;
	}
	font {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 16px;
	}

	td {
		font-family: Verdana, Geneva, Arial, helvetica, sans-serif;
		text-align: left;
		font-weight: normal;
		font-size: 12px;
		font-style: normal;
	}

	th {
		font-family: Verdana, Geneva, Arial, helvetica, sans-serif;
		text-align: left;
		font-size: 12px;
		font-weight: normal;
		font-style: normal;
	}
	h1 {
		text-align: center;
	}
	.style1 {
		font-size: 12px
	}

</style>
</head>

<body>


<table width="706" height="697" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <th height="21" colspan="6" scope="row"><div align="center">LETTERS OF STATEMENT </div></th>
  </tr>
  <tr>
    <th width="139" height="15" scope="row">To</th>
    <td width="8">:</td>
    <td width="173">Mr. Poa </td>
    <td width="179">&nbsp;</td>
    <td colspan="2">Reff:<?php echo $result[0] -> los_no; ?></td>
  </tr>
  <tr>
    <th height="17" scope="row">CC</th>
    <td>:</td>
    <td>Ms. Sharon </td>
    <td colspan="2">&nbsp;</td>
    <td width="205" >Date : <?php echo date("d/m/Y"); ?></td>
  </tr>
  <tr>
    <th height="17" scope="row">From</th>
    <td>:</td>
    <td>Hilal</td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th height="17" scope="row">Subject</th>
    <td>:</td>
    <td>Member Account </td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th height="17"colspan="6" scope="row">=============================================================================</th>
  </tr>
  <tr>
    <th colspan="6" height="15" scope="row">      
      <p>Dengan hormat,</p>
      <p>Dengan ini kami beritahukan bahwa adanya penggantian nama karena
    <?php
	echo $result[0] -> keterangan;
	?></th>
  </tr>
  <tr>
    <th height="15" colspan="6"  scope="row"><div align="left">
      <p>Nama :
        <?php

		echo $result[0] -> fullnm;
	?>
      </p>
      <p>ID : 
        <?php

		echo $result[0] -> dfno;
	?>
      </p>
    </div></th>
  </tr>
   <tr>
    <th height="30" colspan="6" scope="row">Untuk digantikan dengan member berikut ini</th>
  </tr>
  <tr>
    <th height="15" colspan="6" scope="row">Nama :
      <?php
	echo $result[0]->trf_to_nm;
	?> 
      <p align="justify">KTP :
        <?php
		echo $result[0]->idno;
	?>
      </p>
      <p align="justify">Address: 
        <?php
		echo $result[0]->address;
	?>
      </p>
      <p align="justify">Sex :
        <?php

		echo $result[0]->sex;
	?>
      </p>
      <p align="justify">Bank Info:
        <?php
		echo $result[0]->bank_code;
		echo "-";
		echo $result[0]->acc_no;
	?>
      </p>
      <p align="justify">D.O.B :
        <?php
		echo $result[0]->dob;
	?>
      </p>      <p align="justify">S.Code:
        <?php
		echo $result[0]->stockist;
	?>
      </p></th>
  </tr>
  
  <tr>
    <th height="30" scope="row">Terima kasih </th>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td colspan="2" >&nbsp;</td>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <th height="29" scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <th height="29" colspan="3" scope="row">&nbsp;</th>
    <td colspan="3" >&nbsp;</td>
  </tr>
  <tr>
    <th height="31" scope="row">Luqito M Hilal</th>
    <th>&nbsp;</th>
    <td colspan="1" >Bayu R </td>
    <td>Ir. Joko Komara </td>
  </tr>
  <tr>
    <th height="31" scope="row">(EDP Manager ) </th>
    <th >&nbsp; </th>
    <td colspan="1" >(C.Aff Manager) </td>
    <td>(GM)</td>
  </tr>
  <tr>
    <th height="29" colspan="3" scope="row">&nbsp;</th>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <th height="29" colspan="3" scope="row">&nbsp;</th>
    <td colspan="3">Approved By</td>
  </tr>
  <tr>
    <th height="29" colspan="3" scope="row">&nbsp;</th>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <th height="29" colspan="3"  scope="row">&nbsp;</th>
    <td colspan="3" >&nbsp;</td>
  </tr>
  <tr>
    <th height="29" colspan="3"  scope="row">&nbsp;</th>
    <td colspan="3" >Dato' Dr. H. Md. Radzi Saleh</td>
  </tr>
  <tr>
    <th height="25" colspan="3"  scope="row"><p>&nbsp;</p>    </th>
    <td colspan="3" >(President Director K-Link Indonesia)</td>
  </tr>
</table>
</body>
</html>

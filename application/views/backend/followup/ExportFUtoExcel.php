<?php
    header("Content-type: application/vnd.ms-excel; name='excel'");
    header("Content-Disposition: Attachment; filename=list.xls");
	header("Pragma: no-cache");
    header("Expires: 0");
    date_default_timezone_set("Asia/Jakarta");
	
	if($form['followup_type'] == "1" || $form['followup_type'] == "6") {
		echo "<table border=1>";
	    echo "<tr><th>No</th><th>LOS No</th><th>ID Member</th><th>Nama Member</th><th>Keterangan</th><th>Rank</th></tr>";
		$no = 1;
		foreach($listFollowUp as $dta) {
			if($dta->memberstatus == "2" || $dta->memberstatus == "4") {
				echo "<tr>"; 
				echo "<td>$no</td>";
				echo "<td>$dta->los_no</td>";

				echo "<td>$dta->dfno</td>";

				echo "<td>$dta->fullnm</td>";
				echo "<td>$dta->folup_type</td>";
				echo "<td>$dta->dfno_rank</td>";
				echo "</tr>";
				
			} else {
				echo "<tr>"; 
				echo "<td>$no</td>";
				echo "<td>$dta->los_no</td>";

				echo "<td>$dta->dfno</td>";

				echo "<td>$dta->fullnm</td>";
				echo "<td>$dta->folup_type</td>";
				echo "<td>$dta->dfno_rank</td>";
				echo "</tr>";
			}
			$no++;
		}
	    echo "</table>";
	} else if($form['followup_type'] == "3" || $form['followup_type'] == "8") {
		echo "<table border=1>";
	    echo "<tr><th>No</th>";
	    echo "<th>LOS No</th>";
	    echo "<th>ID Member</th>";
	    echo "<th>Member Name</th>";
	    if($listFollowUp[0]->folup_type == "TRANSFER LINE") {
		    //echo "<th>Tgl Join</th>";
		    echo "<th>ID Sponsor</th>";
		    echo "<th>Sponsor Name</th>";
	    } else {
		    echo "<th>ID Recruiter</th>";
		    echo "<th>Recruiter Name</th>";
		}
	    echo "<th>Keterangan</th></tr>";
		$no = 1;
		foreach($listFollowUp as $dta) {
			if($dta->folup_type == "TRANSFER LINE") {
				echo "<tr>"; 
				echo "<td>$no</td>";
				echo "<td>$dta->los_no</td>";
				echo "<td>$dta->dfno</td>";
				echo "<td>$dta->fullnm</td>";
				//echo "<td>$dta->jointdt</td>";
				echo "<td>$dta->trf_to_id</td>";
				echo "<td>$dta->trf_to_nm</td>";
				//echo "<td>&nbsp;</td>";
				//echo "<td>&nbsp;</td>";
				echo "<td>$dta->keterangan</td>";
				echo "</tr>";
				
			} else {
				echo "<tr>"; 
				echo "<td>$no</td>";
				echo "<td>$dta->los_no</td>";
				echo "<td>$dta->dfno</td>";
				echo "<td>$dta->fullnm</td>";
				//echo "<td>$dta->jointdt</td>";
				//echo "<td>&nbsp;</td>";
				//echo "<td>&nbsp;</td>";
				echo "<td>$dta->trf_to_id</td>";
				echo "<td>$dta->trf_to_nm</td>";
				
				echo "<td>$dta->keterangan</td>";
				echo "</tr>";
			}
			$no++;	
		}	
		echo "</table>";			
	} else if($form['followup_type'] == "2" || $form['followup_type'] == "9") {
		echo "<table border=1>";
	    echo "<tr><th>No</th>";
	    echo "<th>LOS No</th>";
	    echo "<th>ID Member</th>";
	    echo "<th>Member Name</th>";
	    echo "<th>Dialihkan ke nama</th>";
	    echo "<th>Keterangan</th></tr>";
		$no = 1;
		foreach($listFollowUp as $dta) {
			
				echo "<tr>"; 
				echo "<td>$no</td>";
				echo "<td>$dta->los_no</td>";
				echo "<td>$dta->dfno</td>";
				echo "<td>$dta->fullnm</td>";
				echo "<td>$dta->trf_to_nm</td>";
				echo "<td>$dta->keterangan</td>";
				echo "</tr>";
			
			$no++;	
		}	
		echo "</table>";
	}			
			
		
	
?>
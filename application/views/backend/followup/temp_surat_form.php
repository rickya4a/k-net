<style>
    .tab {
        display:inline-block;
        margin-left: 40px;
    }
</style>
<script src="<?php echo base_url()?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script type='text/javascript'>
tinymce.init({
//selector:'textarea', menubar:'', theme: 'modern'
    /* replace textarea having class .tinymce with tinymce editor */
    selector: "textarea",
    content_style: ".mce-content-body {font-size:12px;font-family:Arial,sans-serif;}",

    /* theme of the editor */
    theme: "modern",
    skin: "lightgray",

    /* width and height of the editor */
    width: "100%",
    height: 300,

    /* display statusbar */
    statubar: true,

    /* plugin */
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor"
    ],

    /* toolbar */
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",

    /* style */
    /*style_formats: [
        {title: "Headers", items: [
            {title: "Header 1", format: "h1"},
            {title: "Header 2", format: "h2"},
            {title: "Header 3", format: "h3"},
            {title: "Header 4", format: "h4"},
            {title: "Header 5", format: "h5"},
            {title: "Header 6", format: "h6"}
        ]},
        {title: "Inline", items: [
            {title: "Bold", icon: "bold", format: "bold"},
            {title: "Italic", icon: "italic", format: "italic"},
            {title: "Underline", icon: "underline", format: "underline"},
            {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
            {title: "Superscript", icon: "superscript", format: "superscript"},
            {title: "Subscript", icon: "subscript", format: "subscript"},
            {title: "Code", icon: "code", format: "code"}
        ]},
        {title: "Blocks", items: [
            {title: "Paragraph", format: "p"},
            {title: "Blockquote", format: "blockquote"},
            {title: "Div", format: "div"},
            {title: "Pre", format: "pre"}
        ]},
        {title: "Alignment", items: [
            {title: "Left", icon: "alignleft", format: "alignleft"},
            {title: "Center", icon: "aligncenter", format: "aligncenter"},
            {title: "Right", icon: "alignright", format: "alignright"},
            {title: "Justify", icon: "alignjustify", format: "alignjustify"}
        ]}
    ]*/

    /*init_instance_callback : function(editor) {
     var data = document.getElementById('ptr_content_holder').innerHTML;
     tinymce.get('ptr_content').setContent(data);
     }*/

 });


</script>
<?php
if($errMsg == true) {
    $dfno = ""; $fullnm = ""; $idno = ""; $addr = ""; $stockist = "";
    $rec_id = ""; $fol_id = ""; $memberstatus = ""; $jointdt = "";
    $sponsorid = "";
    $sponsorname = "";
    $keterangan = "";
    $trf_to_nm = "";

    /*if($detailMember != "" || $detailMember != null) {
        $dfno = $detailMember[0]->dfno;
        $fullnm = $detailMember[0]->fullnm;
        $jointdt = $detailMember[0]->jointdt;
        $idno = $detailMember[0]->idno;
        $addr = $detailMember[0]->addr1." ".$detailMember[0]->addr2." ".$detailMember[0]->addr3;
        $stockist = $detailMember[0]->loccd;
        $sponsorid = $detailMember[0]->sponsorid;
        $sponsorname = $detailMember[0]->sponsorname;
        $rec_id = $id;
        $fol_id = $followup_id;
        $memberstatus = $detailMember[0]->memberstatus;

    }*/

    if($update == 1) {
        $dfno = $detailMember[0]->dfno;
        $fullnm = $detailMember[0]->fullnm;
        $trf_to_nm = $detailMember[0]->trf_to_nm;
        $rec_id = $id;
        $fol_id = $followup_id;
        $keterangan = $detailMember[0]->keterangan;
        $sponsorid = $detailMember[0]->trf_to_id;
        $sponsorname = $detailMember[0]->trf_to_nm;
    } else {
        if($detailMember != "" || $detailMember != null) {
            $dfno = $detailMember[0]->dfno;
            $fullnm = $detailMember[0]->fullnm;
            $trf_to_nm = $detailMember[0]->sponsorname;
            $rec_id = $id;
            $fol_id = $followup_id;
            $sponsorid = $detailMember[0]->sponsorid;
            $sponsorname = $detailMember[0]->sponsorname;
            $keterangan = "";
            //$keterangan = $detailMember[0]->keterangan;
        }
    }
    //print_r($detailMember)
    ?>

    <form id="updateTL" class="form-horizontal">
        <fieldset>

            <?php
            /*if($legend == true) {
                echo "<legend style=\"margin-bottom: 5px;\">Update Recruiter</legend>";
            }
            if(isset($dataFollow)) {
                echo "<label class=\"control-label\" for=\"typeahead\">Claim</label>";
                echo "<div class=\"controls\">";
                echo "<select id=\"fid\" name=\"fid\" required=\"required\">";
                echo "<option value=\"\">--Pilih disini--</option>";
                foreach($dataFollow as $listdtas) {
                    echo "<option value=\"$listdtas->id\">".$listdtas->id." - ".$listdtas->formno."</option>";
                }
                echo "</select></div>";
            }*/
            ?>
            <div class="control-group">
                <!--<label class="control-label" for="typeahead">No Form</label>
			<div class="controls">
				<input type="text" id="no_form" name="no_form" />&nbsp;Bulan
				<select id="month" name="month" style="width: 50px;">
					<?php
                /*$curmonth = date('m');
                $prev = $curmonth - 1;
                for($i=$curmonth;$i>=$prev;$i--) {
                    echo "<option value=\"$i\">".sprintf("%'.02d", $i)."</option>";
                }*/
                ?>
				</select>
			</div>-->

                <label class="control-label" for="typeahead">No Ref Order</label>
                <div class="controls">
                    <input id="refno" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="refno"  />
                </div>

                <label class="control-label" for="typeahead">Nominal</label>
                <div class="controls">
                    <input id="nominal" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="nominal"/>
                </div>
                <br>

                <label class="control-label" for="typeahead"><strong><u>Data Pribadi</u> :</strong></label>
                <br><br>

                <label class="control-label" for="typeahead">Nama Pemohon</label>
                <div class="controls">
                    <input id="fullnm" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="fullnm" />
                </div>

                <label class="control-label" for="typeahead">No. KTP</label>
                <div class="controls">
                    <input id="noktp" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="noktp" />
                </div>

                <label class="control-label" for="typeahead">Alamat</label>
                <div class="controls">
                    <input id="alamat" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="alamat" />
                </div>

                <label class="control-label" for="typeahead">No. Hp</label>
                <div class="controls">
                    <input id="nohp" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="nohp" />
                </div>
                <br>

                <label class="control-label" for="typeahead"><strong><u>Data Bank</u> :</strong></label>
                <br><br>

                <label class="control-label" for="typeahead">No. Rekening</label>
                <div class="controls">
                    <input id="norek" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="norek" />
                </div>

                <label class="control-label" for="typeahead">Nama Bank</label>
                <div class="controls">
                    <input id="nm_bank" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="nm_bank"/>
                </div>

                <label class="control-label" for="typeahead">Atas Nama</label>
                <div class="controls">
                    <input id="pemilik_rek" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value=""  name="pemilik_rek" />
                </div>
                <br>

                <label class="control-label" for="typeahead">Jenis Claim</label>
                <div class="controls">
                    <select id="claim" name="claim" required="required">
                        <option value="0">--Pilih disini--</option>
                        <option value="1">Surat Claim Refund</option>

                    </select>

                </div>

                <label class="control-label" for="typeahead">Isi Surat</label>
                <div class="controls">
                    <textarea id="keterangan" name="keterangan" class="form-control" rows="15" placeholder="Keterangan atau spesifikasi"></textarea>
                </div>

                <label class="control-label" for="typeahead">&nbsp;</label>
                <div id="inp_btn" class="controls">
                    <?php
                    if($backButton) {
                        ?>
                        <input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm2',' .nextForm1')" value="<< Kembali" name="save">
                    <?php } ?>
                    <input tabindex="4" id="btn_input_user" class="btn btn-primary TabOnEnter" type="button" onclick="Followup.saveDataLOS(this.form.id,'followup/transfer/recruiter/save')" value="Submit" name="save">
                    <input class="btn btn-reset" type="reset" value="Reset" name="reset">

                </div>
            </div>
        </fieldset>
    </form>
    <?php
} else { echo setErrorMessage("Belum ada follow up Transfer Line");}
?>

<script>

    $(function() {
        $("#claim").change(function() {
            var arrbulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
            var date = new Date();
            var tanggal = date.getDate();
            var bulan = date.getMonth();
            var tahun = date.getFullYear();

            var s = $(this).val();
            var ref = document.getElementById("refno").value;
            var nominal = document.getElementById("nominal").value;
            var nama = document.getElementById("fullnm").value;
            var noktp = document.getElementById("noktp").value;
            var alamat = document.getElementById("alamat").value;
            var nohp = document.getElementById("nohp").value;
            var norek = document.getElementById("norek").value;
            var nama_bank = document.getElementById("nm_bank").value;
            var atas_nama = document.getElementById("pemilik_rek").value;
            //alert(s);

            if(s == 1){
                var judul ="<div align='center'>SURAT PERNYATAAN DAN PERMOHONAN <br> PENARIKAN KEMBALI KELEBIHAN PEMBAYARAN <br><br><br></div>";
                var head =
                    "<p align='left'>PT. K-Link Indonesia <br>" +
                    "Jl. Jend. Gatot Subroto Kav. 59A Jakarta Selatan <br>" +
                    "Di Tempat </p>";

                var p1 ="<p align='left'>Saya yang bertanda tangan dibawah ini: </p>";
                var p2 ="<p align='left'>Nama : "+ nama +"</p>";
                var p3 ="<p align='left'>No. KTP : "+ noktp +"</p>";
                var p4 ="<p align='left'>Alamat : "+ alamat +"</p>";
                var p4_1 ="<p align='left'>No. Hp : " + nohp +"</p>";
                var p5 ="<p align='left'>Mengajukan permohonan penarikan kembali kelebihan pembayaran pembelanjaan melalui www.k-net.com," +
                    "alasan kelebihan pembayaran pada transaksi dengan nomor order " + ref+
                    " dikarenakan kekeliruan saya bukan pihak manapun.</p>";
                var p6 ="<p align='left'>Dan saya harapkan agar kelebihan bayar tersebut dapat dikembalikan kerekening saya sebagai berikut:</p>";
                var p7 ="<p align='left'>Nomor Rekening : " + norek +"</p>";
                var p8 ="<p align='left'>Nama Bank : "+ nama_bank +"</p>";
                var p9 ="<p align='left'>Atas Nama : "+ atas_nama +"</p>";
                var p10 ="<p align='left'>Dana Yang Dikembalikan : " + nominal +"</p>";
                var p11 ="<p align='left'>Demikianlah surat permohonan ini saya buat, atas bantuan dan kerjasamanya saya ucapkan terima kasih</p>";
                var tempat ="<p align='right'>Jakarta, "+ tanggal+" "+arrbulan[bulan]+" "+tahun +"</p>";

                var foot ="<p align='center'>Mengetahui, " +
                    "&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; " +
                    "Pemohon,</p>";


                var tab ="<p align='left'><br><br><br><br></p>";
                var br= "<p align='left'><br><br></p>";

                var ttd ="<p align='center'>(.................................) " +
                    "&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;" +
                    "(.................................)</p>";
                var content = judul + head + br + p1 + p2 + p3 + p4 + p4_1 + p5 + br + p6 + p7 + p8 + p9 + p10 + br+ p11 + br + tempat + foot + tab + ttd;
            }else{
                var content= "";
            }

            tinyMCE.activeEditor.setContent(content);
        });
    });

    var dengan_rupiah = document.getElementById('nominal');
    dengan_rupiah.addEventListener('keyup', function(e)
    {
        dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>


<div class="mainForm">
  <form class="form-horizontal" id="listFollowUp" method="post" target="_blank" action="<?php echo site_url("followup/process/excel");?>">
    <fieldset>      
      <div class="control-group">
      	<label class="control-label" for="typeahead">No Follow Up</label>
      	<div class="controls">
      		<input  type="text" class="TabOnEnter" id="formno" style="width: 150px;" name="formno" placeholder="No Form Follow Up" />
      		
      	</div>
      	<label class="control-label" for="typeahead">Cust ID</label>
      	<div class="controls">
      		<input tabindex="1" type="text" class="TabOnEnter" id="cust_idxx" style="width: 150px;" name="cust_id" />
      		<input tabindex="2" type="text" class="TabOnEnter"  id="cust_name" style="width: 350px;" placeholder="Nama member/stockist/lainnya" name="cust_name" />
      	</div>	  
      	
      	<label class="control-label" for="typeahead">Jenis Follow Up</label>                             
        <div class="controls">
        	<select class="TabOnEnter span4" id="followup_typexx" name="followup_type" tabindex="3">
        		<option value="">--Pilih disini--</option>
  				<?php
  				foreach($followup_type as $key => $value) {
  					echo "<option value=\"$key\">$value</option>";
  				}
  				?>
      		</select>
        </div>
        <label class="control-label" for="typeahead">Status</label>
        <div class="controls">
        	<select class="TabOnEnter span4" id="status_followupxx" name="status_followup" tabindex="4">
        		<option value="">--Pilih disini--</option>
  				<?php
  				foreach($status_list as $key => $value) {
  					echo "<option value=\"$key\">$value</option>";
  				}
  				?>
      		</select>
        </div>      
        <?php
          //echo datepickerFromTo("Tgl Follow Up", "fu_from", "fu_to");
		  
        ?>
        <label class="control-label" for="typeahead">Tgl Input</label>
        <div class="controls">
        	<input type="text" class="TabOnEnter dtpicker" name="fu_from" tabindex="5" />&nbsp;
        	<input type="text" class="TabOnEnter dtpicker" name="fu_to" tabindex="6"  />
        </div>	
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="7" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="<?php echo $act; ?>" />
            <input type="submit" class="btn btn-success" name="export" value="Export To Excel" />
            <!--<input tabindex="4"  type="submit" class="btn btn-success" value="To Excel" />-->
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result">
    	
    </div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

<?php 
   $resx = $detailTrx['hasil'];
   $tipetrx = $detailTrx['tipe'];
   $batas_update = $detailTrx['batas_update'];
   $product = $detailTrx['product'];
   if($tipetrx == "inv") {
   	 $title = "Transaction Type : Invoice";
	 $inpx = "<input id=\"orderno\" tabindex=\"1\" class=\"TabOnEnter\" type=\"text\" onchange=\"Followup.getTrxByTrxNo(this.value)\" value=\"\" name=\"orderno\" style=\"width:200px;\">";
   } else {
   	 $title = "Transaction Type : CN/MS";
	 $nilai = $resx[0]->orderno;
	 //echo "DIff Date : ".$resx[0]->diffdate;
   	 $inpx = $inpx = "<input id=\"orderno\" tabindex=\"1\" class=\"TabOnEnter\" type=\"text\" onchange=\"Followup.getTrxByTrxNo(this.value)\" value=\"$nilai\" name=\"orderno\" style=\"width:200px;\">";
   }				
				
				
?>
<style>
	legend {
		margin-bottom: 5px;
	}
</style>
<form id="updateDistTrx" class="form-horizontal">
	<fieldset>
		<legend>
			<?php echo $title; ?>
		</legend>
		<div class="control-group">
			<label class="control-label" for="typeahead">Order No</label>
			<div class="controls">
				<input type="hidden" id="formnoXXX" name="formno" value="<?php echo $formno; ?>" />
				<input type="hidden" id="rec_id" name="rec_id" value="<?php echo $rec_id; ?>" />
				<input type="hidden" id="trcdx" name="trcdx" value="<?php echo $resx[0]->trcd; ?>" />
				<input type="hidden" id="tipetrx" name="tipetrx" value="<?php echo $tipetrx; ?>" />
				<?php echo $inpx; ?>
				Total BV
				<input id="nbv" type="text" readonly="readonly" value="<?php echo $resx[0]->nbv; ?>" name="nbv" style="width:150px;">
			</div>
			<label class="control-label" for="typeahead">Transaction ID</label>
			<div class="controls">
				<input id="trcd" tabindex="2" class="TabOnEnter" type="text" onchange="Followup.getTrxByTrxNo(this.value)" value="<?php echo $resx[0]->trcd; ?>" name="trcd" style="width:200px;">
				Total DP
				<input id="ndp" type="text" readonly="readonly" value="<?php echo $resx[0]->ndp; ?>" name="ndp" style="width:150px;">
			</div>
			<label class="control-label" for="typeahead">Bonus Period</label>
			<div class="controls">
				<input id="bnsperiod" type="text" readonly="readonly" value="<?php echo $resx[0]->tglbns; ?>" name="bnsperiod" style="width:200px;">
				Current Date
				<input id="curdate" type="text" readonly="readonly" value="<?php echo $resx[0]->tglnow; ?>" name="curdate" style="width:120px;">
			</div>
			<label class="control-label" for="typeahead">Stockist Code</label>
			<div class="controls">
				<input id="stk" type="text" readonly="readonly" value="<?php echo $resx[0]->sc_dfno; ?>" name="stk" style="width:200px;">
				<input id="stkname" type="text" readonly="readonly" value="<?php echo $resx[0]->stkname	; ?>" name="stkname" style="width:450px">
			</div>
			<label class="control-label" for="typeahead">C/O Stockist Code</label>
			<div class="controls">
				<input id="co_stk" type="text" readonly="readonly" value="<?php echo $resx[0]->scco; ?>" name="co_stk" style="width:200px;">
				<input id="co_stk_name" type="text" readonly="readonly" value="<?php echo $resx[0]->costkname; ?>" name="co_stk_name" style="width:450px">
			</div>
			<label class="control-label" for="typeahead">Main Stockist Code</label>
			<div class="controls">
				<input id="main_stk" type="text" readonly="readonly" value="<?php echo $resx[0]->loccd; ?>" name="main_stk" style="width:200px;">
				<input id="main_stk_name" type="text" readonly="readonly" value="<?php echo $resx[0]->mainstkname; ?>" name="main_stk_name" style="width:450px">
			</div>
			<label class="control-label" for="typeahead">Create By</label>
			<div class="controls">
				<input id="createby" type="text" readonly="readonly" value="<?php echo $resx[0]->createnm; ?>" name="createby" style="width:200px;">
				Create Date
				<input id="createdt" type="text" readonly="readonly" value="<?php echo $resx[0]->tglinp; ?>" name="createdt" style="width:125px;">
			</div>
			<label class="control-label" for="typeahead">ID Member</label>
			<div class="controls">
				<input id="memberid" type="text" readonly="readonly" value="<?php echo $resx[0]->dfno; ?>" name="memberid" style="width:200px;">
				<input id="membername" type="text" readonly="readonly" value="<?php echo $resx[0]->nmmember; ?>" name="membername" style="width:450px">
			</div>
			<label class="control-label" for="typeahead">Update ID Member</label>
			<div class="controls">
				<input id="new_memberid" type="text" value="<?php echo $trf_bv[0]->updateTo_dfno; ?>" name="new_memberid" style="width:200px;" onchange="All.getFullNameByID(this.value,'db2/get/fullnm/from/msmemb/dfno','#new_membername')">
				<input id="new_membername" readonly="readonly" type="text" value="<?php echo $trf_bv[0]->fullnm; ?>" name="new_membername" style="width:450px">
			</div>
			<label class="control-label" for="typeahead">Update By</label>
			<div class="controls">
				<input tabindex="3" class="TabOnEnter" id="update_by"  type="text" readonly="readonly" name="update_by" style="width:200px;" value="<?php echo $resx[0]->updatenm; ?>" />
				Update Date
				<input id="update_dt" type="text" readonly="readonly" name="update_dt" style="width:125px;" value="<?php echo $resx[0]->tglupd; ?>" />
			</div>
			<label class="control-label" for="typeahead">Remarks</label>
			<div class="controls">
				<?php
					$remarks = "";
					if($tipetrx == "inv") {
						if($resx[0]->remarka != null) {
							$remarks = $resx[0]->remarka;
						} else {
							$remarks = $resx[0]->remarkb;
						}
					} else {
						$remarks = $resx[0]->remarks;
					}
				?>
				<input id="remarks" class="span9 typeahead" type="text" name="remarks" value="<?php echo $remarks; ?>">
			</div>
			<label class="control-label" for="typeahead"> </label>
			<div id="inp_btn" class="controls">
			<?php
			    /*if($resx[0]->diffdate > 0 ) {
					$disable = "disabled=disabled";
			   	    $errs = "<font color=red>*Please Check Bonus Period and Current Date</font>";
				} else {*/
					//uupdate dion @6/1/2017
					$disable = "";
					$errs = "";
					if($resx[0]->diffdate == 0 || $resx[0]->tglnow <= $batas_update) {
						$disable = "";
					    $errs = "";
					} else {
						$disable = "disabled=disabled";
			   	    	$errs = "<font color=red> *Please Check Bonus Period and Current Date</font>";
						//$err = "Diff date: ".$resx[0]->diffdate.", batas update : ".$batas_update.", tgl now : ".$resx[0]->tglnow; 
					}
				//}
			
			?>	
				<input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm2',' .nextForm1')" value="<< Kembali" name="save">
				<input tabindex="4" id="btn_input_user" class="btn btn-primary TabOnEnter" <?php echo $disable; ?> type="button" onclick="Followup.updateBvTrx()" value="Submit" name="save">
				<input class="btn btn-reset" type="reset" value="Reset" name="reset">
				
				<span id="errMsg"><?php echo $errs; ?></span>
			</div>
		</div>
	</fieldset>
</form>
<div id="detailPrd">
	<table width="100%" class="table table-bordered table-striped datatable">
	  <thead>	
		<tr>
			<th colspan="8">Detail Product</th>
		</tr>
		<tr>
			<th>No</th>
			<th>ID</th>
			<th>Product name</th>
			<th>Qty</th>
			<th>BV</th>
			<th>DP</th>
			<th>Total BV</th>
			<th>Total DP</th>
		</tr>
      </thead>
      <tbody id="detListPrd">
      	<?php
      	  $i = 1;	
      	  $total_bv = 0;
      	  $total_harga = 0;
      	  foreach($product as $datax) {
      	  	echo "<tr>";
      	  	echo "<td>$i</td>";
      	  	echo "<td>$datax->prdcd</td>";
			echo "<td>$datax->prdnm</td>";
			echo "<td align=right>".number_format($datax->qtyord, 0, ",",".")."&nbsp;</td>";
			echo "<td align=right>".number_format($datax->pv, 0, ",",".")."&nbsp;</td>";
			echo "<td align=right>".number_format($datax->dp, 0, ",",".")."&nbsp;</td>";
			echo "<td align=right>".number_format($datax->sumbv, 0, ",",".")."&nbsp;</td>";
			echo "<td align=right>".number_format($datax->sumharga, 0, ",",".")."&nbsp;</td>";
      	  	echo "</tr>";
			$total_bv += $datax->sumbv;
			$total_harga += $datax->sumharga;
			$i++;
      	  }
      	  echo "<tr><td colspan=6 align=center>TOTAL</td><td align=right>".number_format($total_bv, 0, ",",".")."&nbsp;</td><td align=right>".number_format($total_harga, 0, ",",".")."&nbsp;</td></tr>"
      	?>
      </tbody>		
	</table>
</div>
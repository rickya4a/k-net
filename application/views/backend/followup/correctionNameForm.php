<?php
if($errMsg == true) {
    $dfno = ""; $fullnm = ""; $idno = ""; $addr = ""; $stockist = ""; 
    $rec_id = ""; $fol_id = ""; $memberstatus = ""; $jointdt = "";
	$trf_to_nm = "";
	$dob = "";
	$acc_no = "";
	$bank_code = "";
	$stockist = "";
	$keterangan = "";
	/*$sponsorid = "";
	$sponsorname = "";
	if($detailMember != "" || $detailMember != null) {
		$dfno = $detailMember[0]->dfno;
		$fullnm = $detailMember[0]->fullnm;
		$jointdt = $detailMember[0]->jointdt;
		$idno = $detailMember[0]->idno;
		$addr = $detailMember[0]->addr1." ".$detailMember[0]->addr2." ".$detailMember[0]->addr3;
		$stockist = $detailMember[0]->loccd;
		$sponsorid = $detailMember[0]->sponsorid;
		$sponsorname = $detailMember[0]->sponsorname;
		$rec_id = $id;
		$fol_id = $followup_id;
		$memberstatus = $detailMember[0]->memberstatus;
	}*/
	
	if($update == 1) {
			$dfno = $detailMember[0]->dfno;
			$fullnm = $detailMember[0]->fullnm;
			$jointdt = $detailMember[0]->jointdt;
			$idno = $detailMember[0]->idno;
			$addr = $detailMember[0]->address;
			$stockist = $detailMember[0]->loccd;
			$trf_to_nm = $detailMember[0]->trf_to_nm;
			$rec_id = $id;
			$fol_id = $followup_id;
			$memberstatus = $detailMember[0]->memberstatus;
			$addr = $detailMember[0]->address;
			$dob = $detailMember[0]->dob;
			$acc_no = $detailMember[0]->acc_no;
			$bank_code = $detailMember[0]->bank_code;
			$stockist = $detailMember[0]->stockist;
			$keterangan = $detailMember[0]->keterangan;
	} else {
	   if($detailMember != "" || $detailMember != null) {
		   $dfno = $detailMember[0]->dfno;
			$fullnm = $detailMember[0]->fullnm;
			$jointdt = $detailMember[0]->jointdt;
			$idno = "";
			$addr = $detailMember[0]->addr1." ".$detailMember[0]->addr2." ".$detailMember[0]->addr3;
			$stockist = $detailMember[0]->loccd;
			//$sponsorid = $detailMember[0]->sponsorid;
			//$sponsorname = $detailMember[0]->sponsorname;
			$rec_id = $id;
			$fol_id = $followup_id;
			$memberstatus = $detailMember[0]->memberstatus;
	   }	
	}
	//print_r($detailMember)
?>
<form id="updateCorrName" class="form-horizontal">
	<fieldset>
		<?php if($legend == true) { echo "<legend style=\"margin-bottom: 5px;\">Name Correction</legend>"; } 
		if(isset($dataFollow)) {
			echo "<label class=\"control-label\" for=\"typeahead\">Follow Up</label>";
			echo "<div class=\"controls\">";
			echo "<select id=\"fid\" name=\"fid\">";
			//echo "<option value=\"\">--Pilih disini--</option>";
			foreach($dataFollow as $listdtas) {
				echo "<option value=\"$listdtas->id\">".$listdtas->id." - ".$listdtas->formno."</option>";
			}
			echo "</select></div>";
		}
		?>
		<div class="control-group">
			<!--<label class="control-label" for="typeahead">No Form</label>
			<div class="controls">
				<input type="text" id="no_form" name="no_form" />&nbsp;Bulan
				<select id="month" name="month" style="width: 50px;">
					<?php
					  $curmonth = date('m');
					  $prev = $curmonth - 1;
					  for($i=$curmonth;$i>=$prev;$i--) {
					  	echo "<option value=\"$i\">".sprintf("%'.02d", $i)."</option>";
					  }
					?>
				</select>
			</div>-->	
			<label class="control-label" for="typeahead">ID Member</label>
			<div class="controls">
				<?php
				 
				  	echo "<input type=\"hidden\" id=\"rec_id\" name=\"rec_id\" value=\"$rec_id\" />";
				  
				?>
				<input type="hidden" id="followup_id" name="followup_id" value="<?php echo $fol_id; ?>" />
				<input id="dfno" placeholder="Harus diisi" tabindex="1" class="span5 TabOnEnter" type="text" value="<?php echo $dfno; ?>"  name="dfno" onchange="Followup.getDataMemberExcUpline()" />
			</div>
			<label class="control-label" for="typeahead">Member Name</label>
			<div class="controls">
				<input id="fullnm" class="span10 TabOnEnter" value="<?php echo $fullnm; ?>" type="text" value="" name="fullnm" />
			</div>
			<label class="control-label" for="typeahead">New Member Name</label>
			<div class="controls">
				<input placeholder="Harus diisi" id="receiver_name" tabindex="2" class="span10 TabOnEnter" type="text" value="<?php echo $trf_to_nm; ?>" name="receiver_name" />
			</div>
			
			<label class="control-label" for="typeahead">Keterangan</label>
			<div class="controls">
				<textarea id="keterangan" tabindex="12" class="span10 TabOnEnter" name="keterangan"><?php echo $keterangan; ?></textarea>
			</div>
			<label class="control-label" for="typeahead">&nbsp;</label>
			<div id="inp_btn" class="controls">
				<?php
				if($backButton) {
				?>	
			    <input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm2',' .nextForm1')" value="<< Kembali" name="save">
			    <?php } ?>
				<input tabindex="13" id="btn_input_user" class="btn btn-primary TabOnEnter" type="button" onclick="Followup.saveDataLOS(this.form.id,'followup/correction/name/save')" value="Submit" name="save">
				<input class="btn btn-reset" type="reset" value="Reset" name="reset">
				
			</div>
		</div>
	</fieldset>
</form>
<script>
  $(document).ready(function() {
     $("#bank_code").val("<?php echo $bank_code; ?>");
  });
</script>
<?php
} else { echo setErrorMessage("Belum ada follow up Name Correction");}
?>

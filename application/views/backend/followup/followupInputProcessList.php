<?php
  if($listFollowUp == null) {
  	 echo setErrorMessage();
  } else {
  	 echo "<strong>Kode Warna : </strong>";
  	 	   $i = 1;
  	 	   foreach($listStatus as $kode) {
  	 	   	 //echo "<span class='bagde' bgcolor=\"$kode->warna\">$i.</span>";
  	 	   	 //echo "<input type=\"button\" style=\"background-color:$kode->warna; width:150px; display:inline-block;\" value=\"$kode->selectValue\" />&nbsp;";
  	 	   	 echo "<span style=\"background-color:$kode->warna2; width:150px; display:inline-block; text-align=center;\">&nbsp;&nbsp;$kode->selectValue</span>&nbsp;";
			 //echo $kode->selectValue;  
			 $i++;
  	 	   }
  	 
  	 echo "<form id=listFollowUp>";
  	 echo "<table width=\"100%\" align=center class=\"table table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr>";
       echo "<th width=\"5%\">No</th>";
	   echo "<th>Form No</th>";
       echo "<th>Customer</th>";
	   //echo "<th>Status</th>";
       echo "<th width=\"25%\">Deskripsi</th>";
       echo "<th>Tgl</th>";
	   echo "<th width=\"25%\">Penjelasan</th>";
	   echo "<th width=\"8%\">Update Status</th>";
       echo "</thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listFollowUp as $list) {
       			$url = "'followup/update/id/$list->id'";
				$upd = "All.ajaxShowDetailonNextForm($url)";
				$cust = substrwords("$list->cust_id/$list->cust_name", 25);
				
                echo "<tr bgcolor=\"$list->color\" id=\"$i\">";
		        echo "<td><div align=center>$i<div></td>";
                echo "<td><div align=center>";
                echo "<input type=\"hidden\" id=\"id$i\" value=\"$list->id\" />";
				echo "<input type=\"hidden\" id=\"status$i\" value=\"$list->status_followup\" />";
                echo "<a href=\"#\" onclick=\"Javascript:$upd\">$list->formno</a></div></td>";
				echo "<td><div align=left>$cust</div></td>";
				//echo "<td><div align=center>$list->status</div></td>";
				/*if($list->followup_type == 7) {
					echo "<td><div align=center>$list->followup_detail2</div></td>";
				} else { 
					echo "<td><div align=center>$list->followup_detail</div></td>";	
				}*/
				echo "<td><div align=left>$list->followup_detail</div></td>";	
				echo "<td><div align=center>$list->createdt</div></td>";
				echo "<td><div align=left>&nbsp;$list->feedback</div></td>";
				echo "<td><div align=center>";
				 
				echo "<button type=\"button\" class=\"btn btn-mini btn-primary\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"Followup.getIdFollowup($i)\">Update</button>";	
				echo "</div></td>";
                /*$ondelete = "All.deleteFormData('followup/type/delete/', '$list->id', 'followup/type/list')";
				$arr = array(
				    "view" => "All.ajaxShowDetailonNextForm($url)",
				);
                echo btnUpdateDelete($arr); */
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	
	<?php
  }
?>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Status Follow Up</h4>
      </div>
      
      <div class="modal-body">
      	
		    <fieldset>      
		      <div class="control-group">
		      	<label class="control-label" for="typeahead">Status</label>
		      	<div class="controls">
		      		<input type="hidden" id="fid" name="fid" />
		      		<?php
        				echo "<select id=\"updStatus\" name=\"updStatus\" class=\"span5\">";
						 foreach($listStatus as $kode) {
						 	echo "<option value=\"$kode->selectID\">$kode->selectValue</option>";
						 }
						echo "</select>";
		            ?>
		      	</div>
		      	<label class="control-label" for="typeahead">Penjelasan/Alasan</label>
		      	<div class="controls">
		      		<textarea id="penjelasan" name="penjelasan" style="width: 500px;"></textarea>
		      	</div>
		      </div> 	
		    </fieldset>
		      		  
      			
      </div>
      <div class="modal-footer">
      	<button data-dismiss="modal" class="btn btn-primary" onclick="Followup.updateStatusFollowUp()">Update Follow Up</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
   
</div>

  </div>
</div>

<script>
  $( document ).ready(function() {
   All.set_datatable();
  });
</script>

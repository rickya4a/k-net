<?php
if($errMsg == true) {
    $dfno = ""; $fullnm = ""; $idno = ""; $addr = ""; $stockist = ""; 
    $rec_id = ""; $fol_id = ""; $memberstatus = ""; $jointdt = "";
	$sponsorid = "";
	$sponsorname = "";
	$keterangan = "";
	$trf_to_nm = "";
	
	/*if($detailMember != "" || $detailMember != null) {
		$dfno = $detailMember[0]->dfno;
		$fullnm = $detailMember[0]->fullnm;
		$jointdt = $detailMember[0]->jointdt;
		$idno = $detailMember[0]->idno;
		$addr = $detailMember[0]->addr1." ".$detailMember[0]->addr2." ".$detailMember[0]->addr3;
		$stockist = $detailMember[0]->loccd;
		$sponsorid = $detailMember[0]->sponsorid;
		$sponsorname = $detailMember[0]->sponsorname;
		$rec_id = $id;
		$fol_id = $followup_id;
		$memberstatus = $detailMember[0]->memberstatus;
		
	}*/
	
	if($update == 1) {
			$dfno = $detailMember[0]->dfno;
			$fullnm = $detailMember[0]->fullnm;
			$trf_to_nm = $detailMember[0]->trf_to_nm;
			$rec_id = $id;
			$fol_id = $followup_id;
			$keterangan = $detailMember[0]->keterangan;
			$sponsorid = $detailMember[0]->trf_to_id;
	        $sponsorname = $detailMember[0]->trf_to_nm;
	} else {
	   if($detailMember != "" || $detailMember != null) {
		    $dfno = $detailMember[0]->dfno;
			$fullnm = $detailMember[0]->fullnm;
			$trf_to_nm = $detailMember[0]->sponsorname;
			$rec_id = $id;
			$fol_id = $followup_id;
			$sponsorid = $detailMember[0]->sponsorid;
	        $sponsorname = $detailMember[0]->sponsorname;
			$keterangan = "";
			//$keterangan = $detailMember[0]->keterangan;
	   }	
	}
	//print_r($detailMember)
?>

<form id="updateTL" class="form-horizontal">
	<fieldset>
		<?php if($legend == true) { echo "<legend style=\"margin-bottom: 5px;\">Update Recruiter</legend>"; } 
		if(isset($dataFollow)) {
			echo "<label class=\"control-label\" for=\"typeahead\">Follow Up</label>";
			echo "<div class=\"controls\">";
			echo "<select id=\"fid\" name=\"fid\" required=\"required\">";
			echo "<option value=\"\">--Pilih disini--</option>";
			foreach($dataFollow as $listdtas) {
				echo "<option value=\"$listdtas->id\">".$listdtas->id." - ".$listdtas->formno."</option>";
			}
			echo "</select></div>";
		}
		?>
		<div class="control-group">
			<!--<label class="control-label" for="typeahead">No Form</label>
			<div class="controls">
				<input type="text" id="no_form" name="no_form" />&nbsp;Bulan
				<select id="month" name="month" style="width: 50px;">
					<?php
					  $curmonth = date('m');
					  $prev = $curmonth - 1;
					  for($i=$curmonth;$i>=$prev;$i--) {
					  	echo "<option value=\"$i\">".sprintf("%'.02d", $i)."</option>";
					  }
					?>
				</select>
			</div>-->	
			<label class="control-label" for="typeahead">ID Member</label>
			<div class="controls">
				<?php
				 
				  	echo "<input type=\"hidden\" id=\"rec_id\" name=\"rec_id\" value=\"$rec_id\" />";
				  
				?>
				<input type="hidden" id="followup_id" name="followup_id" value="<?php echo $fol_id; ?>" />
				<input id="dfno" tabindex="1" placeholder="Required" required="required" class="span5 TabOnEnter" type="text" value="<?php echo $dfno; ?>"  name="dfno" onchange="Followup.getDataMemberExcUpline()" />
			</div>
			<label class="control-label" for="typeahead">Member Name</label>
			<div class="controls">
				<input id="fullnm" readonly="readonly" class="span10 TabOnEnter" value="<?php echo $fullnm; ?>" type="text" value="" name="fullnm" />
			</div>
			<!--<label class="control-label" for="typeahead">ID Recruiter</label>
			<div class="controls">
				<input id="sponsorid" readonly="readonly" class="span10 TabOnEnter" type="text" value="<?php echo $sponsorid; ?>" name="sponsorid" />
			</div>
			<label class="control-label" for="typeahead">Recruiter Name</label>
			<div class="controls">
				<input id="sponsorname" readonly="readonly" class="span10 TabOnEnter" type="text" value="<?php echo $sponsorname; ?>" name="sponsorname" />
			</div>-->
			<label class="control-label" for="typeahead">New ID Recruiter</label>
			<div class="controls">
				<input id="new_sponsorid" placeholder="Required" required="required" tabindex="2" class="span10 TabOnEnter" type="text" value="<?php echo $sponsorid ?>" name="new_sponsorid" onchange="All.getFullNameByID(this.value,'followup/memb/dfno',' #new_sponsorname')" />
			</div>
			<label class="control-label" for="typeahead">New Recruiter Name</label>
			<div class="controls">
				<input id="new_sponsorname" readonly="readonly" class="span10 TabOnEnter" value="<?php echo $sponsorname; ?>" type="text" value="" name="new_sponsorname" />
			</div>
			<label class="control-label" for="typeahead">Keterangan</label>
			<div class="controls">
				<textarea id="keterangan" tabindex="3" class="span10 TabOnEnter" name="keterangan"><?php echo $keterangan; ?></textarea>
			</div>
			<label class="control-label" for="typeahead">&nbsp;</label>
			<div id="inp_btn" class="controls">
				<?php
				if($backButton) {
				?>	
			    <input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm2',' .nextForm1')" value="<< Kembali" name="save">
			    <?php } ?>
				<input tabindex="4" id="btn_input_user" class="btn btn-primary TabOnEnter" type="button" onclick="Followup.saveDataLOS(this.form.id,'followup/transfer/recruiter/save')" value="Submit" name="save">
				<input class="btn btn-reset" type="reset" value="Reset" name="reset">
				
			</div>
		</div>
	</fieldset>
</form>
<?php
} else { echo setErrorMessage("Belum ada follow up Transfer Line");}
?>

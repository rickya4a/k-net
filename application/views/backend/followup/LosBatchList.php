<?php
  if($listData == null) {
  	 echo setErrorMessage();
  } else { ?>
  	 
<?php
  	 //echo "<form id=losPengunduranDiri>";
  	 echo "<table width=\"100%\" align=center class=\"table table-bordered bootstrap-datatable datatable\">";
      echo "<thead><tr><th colspan=5>Batch LOS List</th></tr>";
			   echo "<tr>";
			   
			   echo "<th>No</th>";
		       echo "<th>Batch LOS No</th>";
			   echo "<th>Batch Date</th>";
			   echo "<th>Create by</th>";
		       echo "<th width=\"20%\">Acc Status</th>";
			   echo "</tr></thead><tbody>";
       $i = 1;
	   //$header = $listData[0]->keterangan;
       foreach($listData as $list) {
       	
       		   echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
				echo "<td><div align=center><input type=\"hidden\" id=\"batchno$i\" name=\"batchno[]\" value=\"$list->batch_no\"/><a href=\"#\" onclick=\"Javascript:All.ajaxShowDetailonNextForm('followup/batch/id/".$list->batch_no."')\">$list->batch_no</a></div></td>";
				echo "<td><div align=center>$list->batch_createdt</div></td>";
				echo "<td><div align=center>$list->batch_createby</div></td>";
				echo "<td><div align=center>";
				echo "<select id=\"acc$i\" name=\"acc\" onchange=\"Followup.updateAccStatus($i)\">";
				foreach($listAcc as $acc) {
					$cv = "";
					if($acc->id == $list->sign_id) {
						$cv = "selected=selected";		
					} 
					echo "<option value=\"$acc->id\" $cv>$acc->acc_by</option>";
				}
				echo "</select>";
				echo "</div></td>";
				
               echo "</tr>";
              $i++; 
       
       	}
               
      
    //echo "</tr>";
    echo "</tbody></table>";
    //echo "</form>";
	?>
	
	<?php
  }
?>

<script>
  $( document ).ready(function() {
   All.set_datatable();
  });
</script>

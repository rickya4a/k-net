<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
		
		$settingLayout = settingLOSLayout($result);
		$sisaKeterangan = $settingLayout['totalWidth']-$settingLayout['widthKopTitle']-$settingLayout['doubleDot'];
		//print header
		LosPrintHeader($pdf, $settingLayout);		
		
		//Isi / Maksud penyampaian
		$alasan = "Bersama surat ini kami beritahukan adanya permohonan perubahan recruiter karena ".strtoupper($result[0]->keterangan)."";
		$pdf->MultiCell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$alasan = "Menyatakan adanya perubahan recruiter, adapun data nya adalah : ";
		$pdf->Cell($settingLayout['totalWidth'] - 30,$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		
		foreach($result as $data) {	
		
		$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan - 30,$settingLayout['heightCell'], strtoupper($data->dfno),$settingLayout['border'],0,'L',true);
        
        $pdf->Ln();
		
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan - 30,$settingLayout['heightCell'], strtoupper($data->fullnm),$settingLayout['border'],0,'L',true);
        
        $pdf->Ln();
		
		$pdf->Ln();
		
		}
		//$pdf->SetFont($settingLayout['font'],'B',$settingLayout['contentFontSize']);
		$alasan = "Perubahan recruiter menjadi : ";
		
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['totalWidth'] - 30,$settingLayout['heightCell'], $alasan,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_id),$settingLayout['border'],0,'L',true);
        
		$pdf->Ln();
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_nm),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		
		
		//Print footer
		LosPrintFooter($pdf, $settingLayout);
		$pdf->Output();
				
?>		
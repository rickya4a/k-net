<?php
   if($result == null) {
   	  echo setErrorMessage("No result found");
   } else {
   	    $no = 1;
   	    echo "<table width=\"100%\" align=center class=\"table table-bordered bootstrap-datatable datatable\">"; 
	    //Tipe pengunduran diri
        if($result[0]->followup_type == 1) {
        	
			 echo "<thead><tr><th colspan=4>Pengunduran Diri</th></tr>";
			   echo "<tr>";
			   //echo "<th><input type=\"checkbox\" name=\"checkall\" onclick=\"All.checkUncheckAll(this)\"></th>";
			   echo "<th>No</th>";
			   echo "<th>LOS No</th>";
		       //echo "<th>ID Member</th>";
			   //echo "<th>Nama Member</th>";
			   echo "<th>LOS Create Date</th>";
		       echo "<th>Create by</th>";
			   echo "</tr><thead>";
			   echo "<tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 //echo "<td><div align=center><input id=\"pil1\" type=\"checkbox\" value=\"$data->id\" name=\"followup_id[]\"></div></td>";
				 echo "<td align=right>$no</td>";
				 //echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/resign/')."/$data->id"."\" >$data->los_no</a></td>";
				 echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/resign/')."/$data->los_no"."\" >$data->los_no</a></td>";
				 //echo "<td>$data->dfno</td>";
				 //echo "<td>$data->fullnm</td>";
				 
				 /*if($data->memberstatus == "4") {
				 	$stt = "TERMINATION";
				 } else if($data->memberstatus == "2") {
				 	$stt = "RESIGNATION";
				 } else {
				 	$stt = "";
				 }*/
				 
				 echo "<td align=center>$data->los_createdt</td>";
				 echo "<td align=center>$data->los_createby</td>";
				 //echo "<td align=center>$stt</td>";  
				 echo "</tr>";  
				 $no++;
			   }
			   echo "</tbody>";
			   //echo "<button class=\"btn btn-success\" type=\"submit\" formaction=\"".base_url('followup/los/print/resign')."\" >Print Selected LOS</button>";
        //Tipe Transfer Name
		} else if($result[0]->followup_type == 2) {
        	echo "<thead><tr><th colspan=6>Transfer Name</th></tr>";
			   echo "<tr>";
			   echo "<th>No</th>";
			   echo "<th>LOS No</th>";
		       echo "<th>ID Member</th>";
			   echo "<th>Nama Member</th>";
			   echo "<th>Transfer To</th>";
		       echo "<th>Reason/Detail</th>";
			   echo "</tr><thead>";
			   echo "<tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 echo "<td align=right>$no</td>";
				 echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/trfname/')."/$data->id"."\" >$data->los_no</a></td>";
				 echo "<td>$data->dfno</td>";
				 echo "<td>$data->fullnm</td>";
				 echo "<td>$data->trf_to_nm</td>";
				 echo "<td>$data->keterangan</td>";  
				 echo "</tr>";  
				 $no++;
			   }
			   echo "</tbody>";
		
		//Tipe Transfer Line	   
        }  else if($result[0]->followup_type == 3) {
        	echo "<thead><tr><th colspan=4>Transfer Line</th></tr>";
			   echo "<tr>";
			   echo "<th>No</th>";
			   echo "<th>LOS No</th>";
		       /*echo "<th>ID Member</th>";
			   echo "<th>Nama Member</th>";
			   echo "<th>Transfer To</th>";
		       echo "<th>Reason/Detail</th>";*/
		       echo "<th>LOS Create Date</th>";
		       echo "<th>Create by</th>";
			   echo "</tr><thead>";
			   echo "<tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 echo "<td align=right>$no</td>";
				  echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/trfline/')."/$data->los_no"."\" >$data->los_no</a></td>";
				 //echo "<td>$data->dfno</td>";
				 //echo "<td>$data->fullnm</td>";
				 //echo "<td>$data->trf_to_id/$data->trf_to_nm</td>";
				 //echo "<td>$data->keterangan</td>";  
				  echo "<td align=center>$data->los_createdt</td>";
				 echo "<td align=center>$data->los_createby</td>";
				 echo "</tr>";  
				 $no++;
			   }
			   echo "</tbody>";
			//Tipe Update Recruiter	    
			}   else if($result[0]->followup_type == 8) {
        	   echo "<thead><tr><th colspan=6>Update Recruiter</th></tr>";
			   echo "<tr>";
			   echo "<th>No</th>";
			   echo "<th width=15%>LOS No</th>";
		       echo "<th>ID Member</th>";
			   echo "<th width=20%>Nama Member</th>";
			   echo "<th width=20%>Transfer To</th>";
		       echo "<th>Reason/Detail</th>";
			   echo "</tr><thead>";
			   echo "<tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 echo "<td align=right>$no</td>";
				  echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/trfrecruiter/')."/$data->id"."\" >$data->los_no</a></td>";
				 echo "<td>$data->dfno</td>";
				 echo "<td>$data->fullnm</td>";
				 echo "<td>$data->trf_to_id/$data->trf_to_nm</td>";
				 echo "<td>$data->keterangan</td>";  
				 echo "</tr>";  
				 $no++;
			   }
			   echo "</tbody>";
		
		//Tipe Exchange Account	   	   
        } else if($result[0]->followup_type == 4) {
        	echo "<thead><tr><th colspan=6>Exchange Account</th></tr>";
			   echo "<tr>";
			   echo "<th>No</th>";
			   echo "<th>LOS No</th>";
		       echo "<th>ID Member</th>";
			   echo "<th>Nama Member</th>";
			   echo "<th>Transfer To</th>";
		       echo "<th>Reason/Detail</th>";
			   echo "</tr><thead>";
			   echo "<tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 echo "<td align=right>$no</td>";
				  echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/excacc/')."/$data->id"."\" >$data->los_no</a></td>";
				 echo "<td>$data->dfno</td>";
				 echo "<td>$data->fullnm</td>";
				 echo "<td>$data->trf_to_id/$data->trf_to_nm</td>";
				 echo "<td>$data->keterangan</td>";  
				 echo "</tr>";  
				 $no++;
			   }
			   echo "</tbody>";
		
			   
		//Tipe Reactivation ID   
		} else if($result[0]->followup_type == 6) {	
			  echo "<thead><tr><th colspan=4>Reactive ID</th></tr>";
			   echo "<tr>";
			   //echo "<th><input type=\"checkbox\" name=\"checkall\" onclick=\"All.checkUncheckAll(this)\"></th>";
			   echo "<th>No</th>";
			    //echo "<th>ID Member</th>";
			   //echo "<th>Nama Member</th>";
			   echo "<th>LOS No</th>";
			   echo "<th>LOS Create Date</th>";
		       echo "<th>Create by</th>";
			   echo "</tr><thead>";
			   /*echo "<th>LOS No</th>";
		       echo "<th>ID Member</th>";
			   echo "<th>Nama Member</th>";
			   echo "<th>Peringkat</th>";
		       
			   echo "</tr>";
			   echo "<thead>";*/
			   echo "<tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 //echo "<td><div align=center><input id=\"pil1\" type=\"checkbox\" value=\"$data->id\" name=\"followup_id[]\"></div></td>";
				 echo "<td align=right>$no</td>";
				 echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/reactive/')."/$data->los_no"."\" >$data->los_no</a></td>";
				 //echo "<td>$data->dfno</td>";
				 //echo "<td>$data->fullnm</td>";
				 //echo "<td>$data->dfno_rank</td>";
				 //echo "<td>$data->keterangan</td>";  
				  echo "<td align=center>$data->los_createdt</td>";
				 echo "<td align=center>$data->los_createby</td>";
				 echo "</tr>";  
				 $no++;
			   }
			   //echo "<tr><td colspan=6></td></tr>";
			   echo "</tbody>";	
			   //echo "<button class=\"btn btn-success\" type=\"submit\" formaction=\"".base_url('followup/los/print/reactive')."\" >Print Selected LOS</button>";
		} else if($result[0]->followup_type == 9) {	
			  echo "<thead><tr><th colspan=5>Name Correction</th></tr>";
			   echo "<tr>";
			   //echo "<th><input type=\"checkbox\" name=\"checkall\" onclick=\"All.checkUncheckAll(this)\"></th>";
			   echo "<th>No</th>";
			   echo "<th>LOS No</th>";
		       echo "<th>ID Member</th>";
			   echo "<th>Nama Member</th>";
			   echo "<th>Koreksi Nama</th>";
		       
			   echo "</tr>";
			   echo "<thead><tbody>";
			   
			   foreach($result as $data) {
			   	 echo "<tr>";
				 // echo "<td><div align=center><input id=\"pil1\" type=\"checkbox\" value=\"$data->id\" name=\"followup_id[]\"></div></td>";
				 echo "<td align=right>$no</td>";
				  echo "<td align=center><a target=\"_BLANK\" href=\"".base_url('followup/los/print/namecorrection/')."/$data->id"."\" >$data->los_no</a></td>";
				 echo "<td>$data->dfno</td>";
				 echo "<td>$data->fullnm</td>";
				 echo "<td>$data->trf_to_nm</td>";
				 //echo "<td>$data->keterangan</td>";  
				 echo "</tr>";  
				 $no++;
			   }
			   //echo "<tr><td colspan=6></td></tr>";
			   echo "</tbody>";	
			   //echo "<button class=\"btn btn-success\" type=\"submit\" formaction=\"".base_url('followup/los/print/namecorrection')."\" >Print Selected LOS</button>";
		}
        
		echo "</table>";
		
   }
?>


  <?php $formid = "followUpEdit"; ?>	
  <form class="form-horizontal" enctype="multipart/form-data" id="<?php echo $formid; ?>">
    <fieldset>
    	<legend>
    	  Follow Up ID : <?php echo $fid; ?>
    	</legend>      
      <div class="control-group">
      	<table width="100%" border="0" id="frmInput">
      		<tr>
      			<td width="15%" align="right">Follow Up No&nbsp;</td>
      			<td width="35%">
      				<input type="text" value="<?php echo $header[0]->formno ?>" tabindex="1" class="span20 TabOnEnter" placeholder="<?php echo placeholderCheck(); ?>" id="formno" name="formno" onchange="All.checkDoubleInput('db2/get/formno/from/DION_followup/','formno',this.value)" />
      			</td>
      			<td width="15%" align="right">Email&nbsp;</td>
      			<td width="35%">
      				<input type="text" value="<?php echo $header[0]->email ?>" tabindex="7" class="span20 TabOnEnter"  id="email" name="email" />
      			</td>
      		</tr>
      		<tr>
      			<td align="right">Person Type</td>
      			<td>
      				<select style="width: 340px;" class="TabOnEnter" name="cust_type" id="cust_type" tabindex="2">
      					<?php
	      				foreach($cust_type as $key => $value) {
	      					$selected ="";
	      					if($key == $header[0]->cust_type) {
	      						$selected = "selected=selected";
								
	      					}
	      					echo "<option value=\"$key\" $selected>$value</option>";
	      				}
	      				?>
      				</select>
      				
      			</td>
      			<td align="right">Cara Penyampaian&nbsp;</td>
      			<td>
      				<select style="width: 340px;" class="TabOnEnter" name="comm_method" id="comm_method" tabindex="8">
      					<?php
	      				foreach($comm_method as $key => $value) {
	      					$selected ="";
	      					if($key == $header[0]->cust_type) {
	      						$selected = "selected=selected";
								
	      					}
	      					echo "<option value=\"$key\" $selected>$value</option>";
	      				}
	      				?>
      				</select> 
      				<input type="hidden" id="sent_to" name="sent_to" value="ADIS" />
      			</td>
      		</tr>
      		<tr>
      			<td align="right">ID Member/Stockist&nbsp;</td>
      			<td>
      				<input type="text" value="<?php echo $header[0]->cust_id; ?>" tabindex="3" class="span20 TabOnEnter"  id="cust_id" name="cust_id" onchange="Followup.checkIDCust()" />
      			</td>
      			<td align="right">Ada Lampiran/Bukti&nbsp;</td>
      			<td>
      				<select style="width: 340px;" name="attached_app" id="attached_app" tabindex="9" class="TabOnEnter">
      					<?php
      					foreach($listLampiran as $dta) {
      						$selected ="";
	      					if($dta->id == $header[0]->attached_app) {
	      						$selected = "selected=selected";
								
	      					}
      						echo "<option value=\"$dta->id\" $selected>$dta->lampiran</option>";
      					}
      					?>
      				</select> 
      				
      			</td>
      			<!---->
      		</tr>
      		<tr>
      			<td align="right">Nama&nbsp;</td>
      			<td>
      		       <input type="text" value="<?php echo $header[0]->cust_name; ?>" tabindex="4" class="span20 TabOnEnter"  id="cust_name" name="cust_name" />
      		    </td>
      			<!--<td align="right">Bukti Berupa&nbsp;</td>
      			<td>
      				<input type="text" tabindex="10" class="span20 TabOnEnter"  id="attaced_form" name="attaced_form" />
      			</td>
      			
      			<td align="right">Tgl Follow Up&nbsp;</td>
      			<td>
      				<input type="text" class="TabOnEnter span20" id="createdt" tabindex="10" name="createdt" value="<?php echo date("Y-m-d"); ?>" />
      			</td>-->
      			<td align="right">Jenis Follow Up&nbsp;</td>
      			<td>
      				<select class="TabOnEnter" style="width: 340px;" id="followup_type" name="followup_type" tabindex="10" onchange="Followup.setFollowupType()">
      			    <option value="">--Silahkan pilih</option>
      				<?php
      				foreach($followup_type as $key => $value) {
      					$selected ="";
	      					if($key== $header[0]->followup_type) {
	      						$selected = "selected=selected";
								
	      					}
      					echo "<option value=\"$key\" $selected>$value</option>";
      				}
      				?>
      				</select>
      			</td>
      		</tr>
      		<tr>
      			<td align="right">Telp Rumah/Kantor&nbsp;</td>
      			<td>
      			  <input type="text" value="<?php echo $header[0]->tel_no; ?>" tabindex="5" class="span20 TabOnEnter"  id="tel_no" name="tel_no" />
      			</td>
      			<td align="right" valign="top">Detail Follow Up&nbsp;</td>
      			<td rowspan="2">
      				<textarea tabindex="11" type="text" class="span20 TabOnEnter" id="followup_detail" name="followup_detail"><?php echo $header[0]->followup_detail; ?></textarea> 
      			</td>
      			<!---->
      		</tr>
			<tr>
      			<td align="right">Telp Celluler&nbsp;</td>
      			<td>
      			  <input type="text" value="<?php echo $header[0]->tel_hp; ?>"  tabindex="6" class="span20 TabOnEnter"  id="tel_hp" name="tel_hp" />
      			</td>     			
      			
      			
      			<!---->
      		</tr>
      		
      			
      			
      		
      		
      			 <?php
		    	    if($header[0]->followup_type == 7) {
		    	    	//echo "sdsds";	
		    	  ?>
		    	  <tr id="isBV">
		    	  	 <td>&nbsp;</td>
						<td colspan="3">
						 <table class="table table-bordered" width="100%">
						  <thead>
							<tr><th colspan="6">Pelimpahan BV kepada member di bawah ini :</th></tr>
							<tr>
								<td width="20%">ID Member</td>
								<td colspan="5">
								 <?php $url = "db2/get/fullnm/from/msmemb/dfno"; ?>	
								 <input type="text" tabindex="12" id="updateTo_dfno" name="updateTo_dfno" class="TabOnEnter span5" onchange="All.getFullNameByID(this.value,'<?php echo $url; ?>','#updateToNm')" />
								</td>
							</tr>
							<tr>
								<td>Nama Member</td>
								<td colspan="5"><input readonly="readonly" type="text" id="updateToNm" name="updateToNm" class="span20 TabOnEnter" /></td>
							</tr>
						    <tr>
						    	<td>No. TTP/No. Trx</td>
						    	<td colspan="5">
						    		<input tabindex="13" class="TabOnEnter" style="width: 200px;" type="text" id="ttpAdd" name="ttpAdd" />&nbsp;
									<input tabindex="20" type="button" name="check" class="TabOnEnter btn btn-mini btn-warning" value="Cek TTP & Tambah" onclick="Followup.addTTP()" />
								</td>
							</tr>
							<tr>
							  <td colspan="6">	
								<input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
								<input value="Save" type="button" class="btn btn-small btn-primary" />
							  </td>	
							</tr>
							
						  </thead>
						  <tbody id="listTTP">
						  	<tr>
						  		<th>No TTP</th>
						  		<th>No Trx</th>
						  		<th>Total DP</th>
						  		<th>Total BV</th>
						  		<th>ID Member</th>
						  		<th>Act</th>
						  	</tr>
						  	<?php
						  	foreach($footer as $dta) {
						  		echo "<tr>";
								echo "<td align=center><input type=text class='span20' name=ttp[] value=\"$dta->no_ttp\" /></td>";
								echo "<td align=center><input readonly=readonly type=text class='span20 readonly' name=trcd[] value=\"$dta->trcd\" /></td>";
								echo "<td align=right><input readonly=readonly type=text class='span20 readonly' name=ndp[] value=\"$dta->ndp\" /></td>";
								echo "<td align=right><input readonly=readonly type=text class='span20 readonly' name=nbv[] value=\"$dta->ndp\" /></td>";
								echo "<td align=center><input readonly=readonly type=text class='span20 readonly' name=dfno[] value=\"$dta->dfno\" /></td>";
								echo "<td align=center><a class=\"btn btn-mini btn-danger\"><i class=\"icon-trash icon-white\"></i></a></td>";
								echo "</tr>";
						  	}
						  	?>
						  </tbody>
						 </table>
						</td>
		    	  </tr>
		    	  <tr id="nonBV"></tr>	
		    	  <?php
		    	    } else {
		    	?> 
		    	<tr id="isBV"></tr>
		    	<tr id="nonBV">
		    	  <td>&nbsp;</td>
					<td colspan="3">
					 <table class="table table-bordered" width="100%">
					  <thead>
						<tr><th colspan="2"><?php echo $header[0]->nama_followup; ?></th></tr>
						<tr>
							<td width="20%">ID Member</td>
							<td>
							 <?php $url = "db2/get/fullnm/from/msmemb/dfno"; ?>	
							 <input type="text" value="<?php echo $footer[0]->dfno; ?>" tabindex="12" id="dfno" name="dfno" class="TabOnEnter span5" onchange="Followup.setMemberInfoForResignation()" />
							</td>
						</tr>
						<tr>
							<td>Nama Member</td>
							<td>
								<input type="text" value="<?php echo $footer[0]->fullnm; ?>" tabindex="13" class="TabOnEnter span5" id="fullnm" name="fullnm" class="span20 TabOnEnter" />
								<input type="hidden" id="idno" name="idno" value="<?php echo $footer[0]->idno; ?>" />
								<input type="hidden" id="memberstatus" name="memberstatus" value="" />
								
							</td>
						</tr>
						<tr>
							<td>Peringkat</td>
							<td>
								<input type="text"  tabindex="13" class="TabOnEnter span5" id="dfno_rank" name="dfno_rank" class="span20 TabOnEnter" />
								
							</td>
						</tr>
						<tr>
			      			<td>&nbsp;</td>
			      			<td>
			      				<input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
			      				<input id="btnSubmit" type="button" class="btn btn-small btn-primary" value="Simpan Data" onclick="Followup.updateFollowUp()" />
			      				<input type="hidden" id="fid" name="fid" value="<?php echo $footer[0]->id; ?>" />
											<input type="hidden" id="followup_id" name="followup_id" value="<?php echo $fid; ?>" />
			      			</td>
			      		</tr>
					 </table>
					</td>
					</tr>
		    	<?php   	
		    	    }
		    	?>
      		</tr>
      		
	     </table>
     	 
		 
      </div> 
     </fieldset>
  </form>   
  
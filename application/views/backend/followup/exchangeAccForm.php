<?php
if($errMsg == true) {
    $dfno = ""; $fullnm = ""; $idno = ""; $addr = ""; $stockist = ""; $rec_id = ""; $fol_id = ""; $memberstatus = ""; $jointdt = "";
	$trf_sponsorid_to = "";
	$trf_sponsornm_to = "";
	$keterangan = "";
	if($update == 1) {
		$dfno = $detailMember[0]->dfno;
			$fullnm = $detailMember[0]->fullnm;
			$trf_sponsorid_to = $detailMember[0]->trf_to_id;
		    $trf_sponsornm_to = $detailMember[0]->trf_to_nm;
			$rec_id = $id;
			$fol_id = $followup_id;
			$keterangan = $detailMember[0]->keterangan;
	} else {
		if($detailMember != "" || $detailMember != null) {
			$dfno = $detailMember[0]->dfno;
			$fullnm = $detailMember[0]->fullnm;
			$rec_id = $id;
			$fol_id = $followup_id;
			
		//$memberstatus = $detailMember[0]->memberstatus;
	   }
	}		
	
	
	
?>

<form id="updateExcAcc" class="form-horizontal">
	<fieldset>
		<?php if($legend == true) { echo "<legend style=\"margin-bottom: 5px;\">Exchange Account</legend>"; } 
		if(isset($dataFollow)) {
			echo "<label class=\"control-label\" for=\"typeahead\">Follow Up</label>";
			echo "<div class=\"controls\">";
			echo "<select id=\"fid\" name=\"fid\">";
			echo "<option value=\"\">--Pilih disini--</option>";
			foreach($dataFollow as $listdtas) {
				echo "<option value=\"$listdtas->id\">".$listdtas->id." - ".$listdtas->formno."</option>";
			}
			echo "</select></div>";
		}
		?>
		
		<div class="control-group">
			<label class="control-label" for="typeahead">ID Member</label>
			<div class="controls">
				<input type="hidden" id="idx" name="idx" value="<?php echo $rec_id; ?>" />
				<input type="hidden" id="followup_id" name="followup_id" value="<?php echo $fol_id; ?>" />
				<input id="dfno" class="span5 TabOnEnter" type="text" value="<?php echo $dfno; ?>" name="dfno" onchange="Followup.getDataMemberTermination()" />
			</div>
			<label class="control-label" for="typeahead">Member Name</label>
			<div class="controls">
				<input id="fullnm" class="span10 TabOnEnter" type="text" value="<?php echo $fullnm; ?>" name="fullnm" />
			</div>
			<label class="control-label" for="typeahead">Transfer ID To</label>
			<div class="controls">
				<input id="trf_to_id" class="span10 TabOnEnter" type="text" value="<?php echo $trf_sponsorid_to; ?>" name="trf_to_id" onchange="All.getFullNameByID(this.value,'followup/memb/dfno',' #trf_to_nm')" />
			</div>
			<label class="control-label" for="typeahead">Transfer Name</label>
			<div class="controls">
				<input id="trf_to_nm" class="span10 TabOnEnter" type="text" value="<?php echo$trf_sponsornm_to; ?>" name="trf_to_nm" />
			</div>
			<label class="control-label" for="typeahead">Keterangan</label>
			<div class="controls">
				<textarea id="keterangan" name="keterangan"><?php echo $keterangan; ?></textarea>
			</div>
			<label class="control-label" for="typeahead"> </label>
			<div id="inp_btn" class="controls">
				<?php
				if($backButton) {
				?>	
			    <input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm2',' .nextForm1')" value="<< Kembali" name="save">
			    <?php } ?>
				<input tabindex="4" id="btn_input_user" class="btn btn-primary TabOnEnter" type="button" onclick="Followup.saveDataLOS(this.form.id,'followup/exchange/acc/save')" value="Submit" name="save">
				<input class="btn btn-reset" type="reset" value="Reset" name="reset">
				
			</div>
		</div>
	</fieldset>
</form>
<?php
} else { echo setErrorMessage("Belum ada follow up Exchange Account");}
?>
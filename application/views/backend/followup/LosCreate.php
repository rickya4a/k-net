
<div class="mainForm">
  <form class="form-horizontal" id="losCreate" method="post" target="_blank" action="<?php echo site_url("trans/report/toExcel");?>">
    <fieldset>      
      <div class="control-group">
      	<label class="control-label" for="typeahead">Tgl Input</label>
        <div class="controls">
        	<input type="text" class="TabOnEnter dtpicker" id="los_from" name="los_from" tabindex="5" value="<?php echo $fu_from; ?>"/>&nbsp;
        	<input type="text" class="TabOnEnter dtpicker" id="los_to" name="los_to" tabindex="6" value="<?php echo $fu_to; ?>" />
        </div>	
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="Search" onclick="<?php echo $act; ?>" />
            <input type="button" id="generateLOS" class="btn btn-primary" value="Generate LOS" onclick="<?php echo $los_act; ?>" />
            <!--<input tabindex="4"  type="submit" class="btn btn-success" value="To Excel" />-->
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

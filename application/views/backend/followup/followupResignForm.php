<?php
if($errMsg == true) {
    $dfno = ""; $fullnm = ""; $idno = ""; $addr = ""; $stockist = ""; $rec_id = ""; $fol_id = ""; $memberstatus = ""; $jointdt = "";
	if($update == 1) {
		$dfno = $detailMember[0]->dfno;
		$fullnm = $detailMember[0]->fullnm;
		$jointdt = $detailMember[0]->jointdt;
		$idno = $detailMember[0]->idno;
		$addr = $detailMember[0]->address;
		$stockist = $detailMember[0]->loccd;
		$rec_id = $id;
		$fol_id = $followup_id;
		$memberstatus = $detailMember[0]->memberstatus;
	} else {
	   if($detailMember != "" || $detailMember != null) {
		$dfno = $detailMember[0]->dfno;
		$fullnm = $detailMember[0]->fullnm;
		$jointdt = $detailMember[0]->jointdt;
		$idno = $detailMember[0]->idno;
		$addr = $detailMember[0]->addr1." ".$detailMember[0]->addr2." ".$detailMember[0]->addr3;
		$stockist = $detailMember[0]->loccd;
		$rec_id = $id;
		$fol_id = $followup_id;
		$memberstatus = $detailMember[0]->memberstatus;
	   }	
	}
	
?>

<form id="updateResignTerminate" class="form-horizontal">
	<fieldset>
		<?php if($legend == true) { echo "<legend style=\"margin-bottom: 5px;\">Resignation / Termination Member</legend>"; } 
		if(isset($dataFollow)) {
			echo "<label class=\"control-label\" for=\"typeahead\">Follow Up</label>";
			echo "<div class=\"controls\">";
			echo "<select id=\"fid\" name=\"fid\">";
			echo "<option value=\"\">--Pilih disini--</option>";
			foreach($dataFollow as $listdtas) {
				echo "<option value=\"$listdtas->id\">".$listdtas->id." - ".$listdtas->formno."</option>";
			}
			echo "</select></div>";
		}
		?>
		
		<div class="control-group">
			<label class="control-label" for="typeahead">ID Member</label>
			<div class="controls">
				<input type="hidden" id="idx" name="idx" value="<?php echo $rec_id; ?>" />
				<input type="hidden" id="followup_id" name="followup_id" value="<?php echo $fol_id; ?>" />
				<input id="dfno" class="span5 TabOnEnter" type="text" value="<?php echo $dfno; ?>" name="dfno" onchange="Followup.getDataMemberTermination()" />
			</div>
			<label class="control-label" for="typeahead">Member Name</label>
			<div class="controls">
				<input id="fullnm" class="span10 TabOnEnter" type="text" value="<?php echo $fullnm; ?>" name="fullnm" />
			</div>
			<label class="control-label" for="typeahead">Join Date</label>
			<div class="controls">
				<input id="jointdt" class="span10 TabOnEnter" type="text" value="<?php echo $jointdt; ?>" name="jointdt" />
			</div>
			<label class="control-label" for="typeahead">ID No</label>
			<div class="controls">
				<input id="idno" class="span10 TabOnEnter" type="text" value="<?php echo $idno; ?>" name="idno" />
			</div>
			<label class="control-label" for="typeahead">Address</label>
			<div class="controls">
				<input id="addr" class="span10 TabOnEnter" type="text" value="<?php echo $addr; ?>" name="addr" />
			</div>
			<label class="control-label" for="typeahead">Stockist</label>
			<div class="controls">
				<input id="stockist" class="span10 TabOnEnter" type="text" value="<?php echo $stockist; ?>" name="stockist" />
			</div>
			<label class="control-label" for="typeahead">Action</label>
			<div class="controls">
				<select id="act" name="act">
					<?php
					  if($update == 1) {
					  	if($memberstatus == "4") {
					  		echo "<option value=\"\">--Pilih disini--</option>
					        <option value=\"4\" selected=selected>Termination</option>
					        <option value=\"2\">Resignation</option>
					        <option value=\"1\">Active</option>";
					  	} else if($memberstatus == "2") {
					  		echo "<option value=\"\">--Pilih disini--</option>
					        <option value=\"4\" selected=selected>Termination</option>
					        <option value=\"2\" selected=selected>Resignation</option>
					        <option value=\"1\">Active</option>";
					  	} 
					?>  	
					  	  
					<?php     
					  } else {
					?>  	
					<option value="">--Pilih disini--</option>
					<option value="4">Termination</option>
					<option value="2">Resignation</option>
					<option value="1">Active</option>
					<?php  	
					  }	
					?>
					
				</select>
			</div>
			<label class="control-label" for="typeahead"> </label>
			<div id="inp_btn" class="controls">
				<?php
				if($backButton) {
				?>	
			    <input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm2',' .nextForm1')" value="<< Kembali" name="save">
			    <?php } ?>
				<input tabindex="4" id="btn_input_user" class="btn btn-primary TabOnEnter" type="button" onclick="Followup.saveDataLOS(this.form.id,'followup/resign/save')" value="Submit" name="save">
				<input class="btn btn-reset" type="reset" value="Reset" name="reset">
				
			</div>
		</div>
	</fieldset>
</form>
<?php
} else { echo setErrorMessage("Belum ada follow up Exchange Account");}
?>
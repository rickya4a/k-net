<?php
  if($listData == null) {
  	 echo setErrorMessage();
  } else { ?>
  	 
<form id="uploadSaveImgLos" class="form-horizontal" enctype="multipart/form-data">
	<fieldset>
	   <legend style="margin-bottom: 5px;">Upload Image LOS Follow Up</legend>
	   <div class="control-group">
			<label class="control-label" for="typeahead">Follow Up ID</label>
			<div class="controls">
				<input type="text" readonly="readonly" id="idfo" name="idfo" value="<?php echo $listData[0]->id; ?>" />
				<input type="hidden" readonly="readonly" id="id" name="id" value="<?php echo $listData[0]->fid; ?>" />
			</div>
		
			<label class="control-label" for="typeahead">Follow Form ID</label>
			<div class="controls">
				<input type="text" readonly="readonly" id="formno" name="formno" value="<?php echo $listData[0]->formno; ?>" />
				
			</div>
			<label class="control-label" for="typeahead">Image</label>
			<div class="controls">
				<input type="file" id="file" name="files[]" multiple="multiple" accept="image/*" />
				
			</div>
		</div>
		<div id="inp_btn" class="controls">
				
			    <input id="back_button" class="btn btn-warning" type="button" onclick="All.back_to_form(' .nextForm1',' .mainForm')" value="<< Kembali" name="save">
			
				<input tabindex="4" id="btn_input_user" class="btn btn-primary TabOnEnter" type="button" onclick="Followup.saveUploadLOS()" value="Submit" name="save">
				<input class="btn btn-reset" type="reset" value="Reset" name="reset">
		</div>
		<div id="resUpload"></div>				
	</fieldset>	
</form>	
<?php
  }

?>

<!--
<script>
  $( document ).ready(function() {
   All.set_datatable();
  });
</script>-->
	
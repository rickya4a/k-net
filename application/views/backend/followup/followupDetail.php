<?php
    $header = $followup['followup'];
	$log = $followup['log'];
	$los = $followup['los'];
	$bv = $followup['bv'];
    if($header == null){
        echo setErrorMessage("No result found");
    }else{
?>
   <form id="updMember">
	<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr>
        	<th colspan="4">Detail Data Follow Up</th>
        </tr>	
       <tr>
            <td style="width: 13%">ID</td>
            <td style="width: 38%"><?php echo $header[0]->id; ?></td>
             
            <td style="width: 13%">Penyampaian via</td>
            <td style="width: 38%"><?php echo $header[0]->comm_method; ?></td>
       </tr>
		<tr>
            <td>No Form</td>
            <td><?php echo $header[0]->formno;?></td>
        	 
        	<td>Lampiran</td>
        	<td><?php echo $header[0]->lampiran;?></td>
        </tr>
		<tr>
            <td valign="top">Cust ID</td>
            <td valign="top"><?php echo $header[0]->cust_id;?></td>
			 
        	<td valign="top">Ditujukan ke</td>
            <td valign="top"><?php echo $header[0]->sent_to;?></td>
        </tr>
		<tr>
            <td>Nama</td>
            <td><?php echo $header[0]->cust_name; ?></td>
			 
        	<td>Tipe Follow Up</td>
            <td><?php echo $header[0]->follow_type;?></td>
        </tr>
		<tr>
            <td>Email</td>
            <td><?php echo $header[0]->email;?></td>
             
        	<td>User/Tgl Input</td>
        	<td><?php echo $header[0]->createby." @ ".$header[0]->createdt;?></td>
        </tr>
		<tr>
            <td>Telp Rmh/Kantor</td>
            <td><?php echo $header[0]->tel_no;?></td>
			 
        	<td>Status</td>
            <td><?php echo $header[0]->status;?></td>
        </tr>
        <tr>
            <td>Telp HP</td>
            <td><?php echo $header[0]->tel_hp;?></td>
			 
        	<td>Detail</td>
            <td><?php echo $header[0]->followup_detail;?></td>
        </tr>
        <tr>
            <td>LOS No / Tgl</td>
            <td><?php echo $header[0]->los_no." / ".$header[0]->los_createdt;?></td>
			 
        	<td>Batch No / Tgl </td>
            <td><?php echo $header[0]->batch_no." / ".$header[0]->batch_createdt;?></td>
        </tr>
        <tr>
            <td>Status Acc</td>
            <td><?php echo $header[0]->acc_by2;?></td>
			 
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        
            
</table>

<?php
	if($bv != null) {
?>
<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th colspan="8">List untuk Pelimpahan BV</th>
		</tr>
		<tr>
			<th>No</th>			
			<th>Trx No</th>
			<th>TTP No</th>
			<th>DP</th>
			<th>BV</th>
			<th>ID Member</th>
			<th>Dipindah ke</th>
			<th>Scan</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		    $i=1;
			foreach($bv as $dta) {
				echo "<tr>";
				echo "<td align=right>$i</td>";
				echo "<td align=center>$dta->trcd</td>";
				echo "<td align=center>$dta->no_ttp</td>";
				
				echo "<td align=right>".number_format($dta->ndp, 0, ".", ",")."</td>";
				echo "<td align=right>".number_format($dta->nbv, 0, ".", ",")."</td>";
				echo "<td align=center>$dta->dfno  / $dta->fullnm</td>";
				echo "<td align=center>$dta->updateTo_dfno / $dta->updateTo_nm</td>";
				$urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
				echo "</tr>";
				$i++;
			}
		?>
	</tbody>
</table>
<?php
	} else {
		//print_r($los);
		if($los != null) {
		echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
		        if($los[0]->followup_type == "1") {
					$title = "Pengunduran Diri";
					$url = "'followup/form/resign/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";
					echo "<tr><th colspan=6>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member</th>
							<th>Member Status</th>
							
							<th>Info</th>
							<th>&nbsp;</th>
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno</td>";
						if($dta->memberstatus == "2") {
							echo "<td align=center>RESIGNATION</td>";
						} else if($dta->memberstatus == "4") {
							echo "<td align=center>TERMINATION</td>";
						} else {
							echo "<td align=center>Blm ditentukan</td>";
						}
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							
							//$url = "'followup/update/resign-reactive/$dta->id/$dta->followup_id/$dta->dfno'";
			                /*$arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); */
						echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>";
							
							/*$arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); */
		                }
		                $urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					} 	  
				} else if($los[0]->followup_type == "4") {
					$title =  "Exchange Account";
					$url = "'followup/exchange/acc/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";	
					
					echo "<tr><th colspan=6>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member/Nama</th>
							<th>Tukar ke ID/ Nama</th>
							<th>Keterangan</th>
							<th>&nbsp;</th>
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno / $dta->fullnm</td>";
						echo "<td align=center>$dta->trf_to_id / $dta->trf_to_nm</td>";
						echo "<td align=center>$dta->keterangan</td>";
						
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>"; 
		                }
						$urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					} 
					
				} else if($los[0]->followup_type == "3") {
					$title =  "Transfer Line";
					$url = "'followup/transfer/line/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";	
					
					echo "<tr><th colspan=6>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member/Nama</th>
							<th>Transfer Ke</th>
							<th>Keterangan</th>
							<th>&nbsp;</th>
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno / $dta->fullnm</td>";
						echo "<td align=center>$dta->trf_to_id / $dta->trf_to_nm</td>";
						echo "<td align=center>$dta->keterangan</td>";
						
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>"; 
		                }
						$urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					} 
				} else if($los[0]->followup_type == "2") {
					$title = "Transfer Name";
					$url = "'followup/transfer/name/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";
					
					echo "<tr><th colspan=7>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member</th>
							<th>Nama Member</th>
							<th>Transfer Ke</th>
							<th>Keterangan</th>
							<th>&nbsp;</th>
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno</td>";
						echo "<td align=center>$dta->fullnm</td>";
						echo "<td align=center>$dta->trf_to_nm</td>";
						echo "<td align=center>$dta->keterangan</td>";
						
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							 
						echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>";
							 
		                }
						$urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				       echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					} 	
						
				} else if($los[0]->followup_type == "5") {
					$title = "Exchange Statement Bonus";
					
					$url = "'followup/form/reactive/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";
					
					echo "<tr><th colspan=4>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member</th>
							<th>Nama Member</th>
							<th>&nbsp;</th>
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno</td>";
						echo "<td align=center>$dta->fullnm</td>";
						
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							
							//$url = "'followup/update/resign-reactive/$dta->id/$dta->followup_id/$dta->dfno'";
			                $arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
						echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>";
							
							$arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
		                }
		                $urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					}
					$url = "";
				} else if($los[0]->followup_type == "8") {
					$title = "Update Recruiter";
					
					$url = "'followup/correction/name/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";
					
					echo "<tr><th colspan=5>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member</th>
							<th>Nama Member</th>
							<th>Data Rekruiter yang benar</th>
							<th>scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno</td>";
						echo "<td align=center>$dta->fullnm</td>";
						echo "<td align=center>$dta->trf_to_id / $dta->trf_to_nm</td>";
						/*
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							
							//$url = "'followup/update/resign-reactive/$dta->id/$dta->followup_id/$dta->dfno'";
			                $arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
						echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>";
							
							$arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
		                }*/
		                $urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					}
					$url = "";
				} else if($los[0]->followup_type == "9") {
					$title = "Name Correction";
					
					$url = "'followup/form/reactive/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";
					
					echo "<tr><th colspan=5>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member</th>
							<th>Nama Member</th>
							<th>Nama yang Benar</th>
							
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno</td>";
						echo "<td align=center>$dta->fullnm</td>";
						echo "<td align=center>$dta->trf_to_nm</td>";
						//echo "<td align=center>$dta->keterangan</td>";
						/*if($dta->status == "0") {
							
							//$url = "'followup/update/resign-reactive/$dta->id/$dta->followup_id/$dta->dfno'";
			                $arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
						echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>";
							
							$arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
		                }*/
		                $urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					}
					$url = "";
				} else {
					$title = "Reactive ID";
					$url = "'followup/form/reactive/".$los[0]->id."/".$los[0]->followup_id."/".$los[0]->dfno."'";
					
					echo "<tr><th colspan=7>$title</th></tr>
					      <tr>
							<th>No</th>
							<th>ID Member</th>
							<th>Nama Member</th>
							<th>Transfer Ke</th>
							<th>Keterangan</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>Scan</th>
						  </tr>";
						  
					$i=1;
					//print_r($los);
					foreach($los as $dta) {
						echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$dta->dfno</td>";
						echo "<td align=center>$dta->fullnm</td>";
						echo "<td align=center>$dta->trf_to_nm</td>";
						echo "<td align=center>$dta->keterangan</td>";
						
						//echo "<td align=center>$dta->keterangan</td>";
						if($dta->status == "0") {
							
							//$url = "'followup/update/resign-reactive/$dta->id/$dta->followup_id/$dta->dfno'";
			                $arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
						echo "<td>&nbsp;</td>";	
		                } else {
		                	echo "<td align=center><font color=green>Updated @$dta->updatedt by $dta->updateby</font></td>";
							
							$arr = array(
							    "update" => "All.ajaxShowDetailonNextForm2($url)",
							);
			                echo btnUpdateDelete($arr); 
		                }
						$urlxc = base_url('followup/img')."/".$header[0]->id."/".$dta->id;
				        echo "<td align=center><a href=\"$urlxc\"  target=\"_BLANK\">View</a></td>";
						echo "</tr>";
						$i++;
					}
				}
		echo "</table>";
		}       
	}
?>

<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th colspan="5">Follow Up History</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Status</th>
			<th>Tgl</th>
			<th>User</th>
			<th width="50%">Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		    $i=1;
			foreach($log as $dta) {
				echo "<tr>";
				echo "<td align=right>$i</td>";
				echo "<td align=center>$dta->status</td>";
				echo "<td align=center>$dta->createdt</td>";
				echo "<td align=center>$dta->createby</td>";
				echo "<td>$dta->detail</td>";
				echo "</tr>";
				$i++;
			}
		?>
	</tbody>
</table>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
		 <!--<input type="button" value="Save Changes" class="btn btn-small btn-primary" onclick="be_member.updateDataMember()" />-->
    </div>
	<p></p>
	</form>
<?php
    }
?>
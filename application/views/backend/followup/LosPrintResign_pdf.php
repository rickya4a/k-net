<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
		
		$settingLayout = settingLOSLayout($result, "Ms. Sharon / Mr. Cheong");
		$sisaKeterangan = $settingLayout['totalWidth']-$settingLayout['widthKopTitle']-$settingLayout['doubleDot'];
		//print header
		LosPrintHeader($pdf, $settingLayout);		
		
		//Isi / Maksud penyampaian
		$alasan = "Dengan ini kami beritahukan adanya permintaan mengundurkan diri yang kami kirimkan bersama laporan berikut ini : ";
		//$alasan .= ", mohon bantuannya untuk memprosesnya di bonus month ini"; 
		$pdf->MultiCell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		
		$kolomNo = 10;
		$kolomID = 60;
		$kolomName = 100;
		$kolomRank = 0;
		$heightHeader = 5;
		$total = $kolomNo + $kolomID + $kolomName + $kolomRank;
		
		
		$i = 1;
		
		$header = $result[0]->memberstatus;
		foreach($result as $data) {
			if($i == 1) {
				    if($header == "4") {
						$stt = "TERMINATION ".strtoupper(convertMonthName($result[0]->bulan))." ".$result[0]->tahun;
					} else {
						$stt = "RESIGNATION ".strtoupper(convertMonthName($result[0]->bulan))." ".$result[0]->tahun;
					}
				    $pdf->SetFont($settingLayout['font'],'B',$settingLayout['contentFontSize']);
					$pdf->Cell($total,$heightHeader,"PT K-LINK INDONESIA",1,0,'C',true);
					$pdf->Ln();
					$pdf->Cell($total,$heightHeader,$stt,1,0,'C',true);
					$pdf->Ln();
					$pdf->Cell($kolomNo,$heightHeader,"No",1,0,'C',true);
					$pdf->Cell($kolomID,$heightHeader,"ID MEMBER",1,0,'C',true);
					$pdf->Cell($kolomName,$heightHeader,"NAMA MEMBER",1,0,'C',true);
					//$pdf->Cell($kolomRank,$heightHeader,"STT",1,0,'C',true); 	
					$pdf->Ln();
					$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
			} else {
				if($header != $data->memberstatus) {
					if($data->memberstatus == "4") {
						$stt = "TERMINATION ".strtoupper(convertMonthName($result[0]->bulan))." ".$result[0]->tahun;
					} else {
						$stt = "RESIGNATION ".strtoupper(convertMonthName($result[0]->bulan))." ".$result[0]->tahun;
					}
					$header = $data->memberstatus;
					$pdf->Ln();
					$pdf->Ln();
				    $pdf->SetFont($settingLayout['font'],'B',$settingLayout['contentFontSize']);
					$pdf->Cell($total,$heightHeader,"PT K-LINK INDONESIA",1,0,'C',true);
					$pdf->Ln();
					$pdf->Cell($total,$heightHeader,$stt,1,0,'C',true);
					$pdf->Ln();
					$pdf->Cell($kolomNo,$heightHeader,"No",1,0,'C',true);
					$pdf->Cell($kolomID,$heightHeader,"ID MEMBER",1,0,'C',true);
					$pdf->Cell($kolomName,$heightHeader,"NAMA MEMBER",1,0,'C',true);
					//$pdf->Cell($kolomRank,$heightHeader,"STT",1,0,'C',true); 	
					$pdf->Ln();
					$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
				}	
			}	
			$pdf->Cell($kolomNo,$settingLayout['heightCell'], $i,1,0,'R',true);
			$pdf->Cell($kolomID,$settingLayout['heightCell'], strtoupper($data->dfno),1,0,'L',true);
			$pdf->Cell($kolomName,$settingLayout['heightCell'], strtoupper($data->fullnm),1,0,'L',true);
			//$pdf->Cell($kolomRank,$settingLayout['heightCell'], strtoupper($data->memberstatus),1,0,'C',true);
			$pdf->Ln();
			$i++;
		}
        
		$pdf->Ln();
		//Print footer
		LosPrintFooter2sign($pdf, $settingLayout);
		$pdf->Output();
				
?>		
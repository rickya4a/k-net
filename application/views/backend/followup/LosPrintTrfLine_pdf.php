<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
		$jum = count($result);
		if($jum > 4) {
			$sentCC = null;
			$edp = "Luqito Masdar Hilal";
			$cc = $sentCC == null ? "Ms. Sharon" : $sentCC;
			$settingLayout = array(
				"border"              => 0,
				"contentFontSize"     => 11,
				"edpManager" 	      => $edp,
				"totalWidth"          => 190,
				"widthKopTitle" 	  => 40,
				"widthKopName"        => 70,
				"doubleDot"           => 2,
				"heightCell" 		  => 5,
				"heightCellSignature" => 16,
				"widthCellSignature"  => 63,		
				"result"              => $result,
				"font"                => "Times",
				"sentTo"              => "Mr. Poa",
				"sentCC"              => $cc,
				"subject"             => "Member Account"
				//"sentFrom"            => $edp
			);
		}	else {
			$settingLayout = settingLOSLayout($result);
		}
		
		$sisaKeterangan = $settingLayout['totalWidth']-$settingLayout['widthKopTitle']-$settingLayout['doubleDot'];
		//print header
		LosPrintHeader($pdf, $settingLayout);		
		
		//Isi / Maksud penyampaian
		/*$alasan = "Dengan ini kami beritahukan bahwa adanya permintaan perpindahan jaringan karena ".strtoupper($result[0]->keterangan)."";
		$pdf->MultiCell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		
		$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->fullnm),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->dfno),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		//$pdf->SetFont($settingLayout['font'],'B',$settingLayout['contentFontSize']);
		$alasan = "Untuk ditransfer dibawah distributor berikut ini : ";
		$pdf->Cell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_nm),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID Member",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_id),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		*/
		//Isi / Maksud penyampaian
		$alasan = "Bersama surat ini kami beritahukan adanya permohonan perubahan jaringan karena ".strtoupper($result[0]->keterangan)."";
		$pdf->MultiCell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		
		
			$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
			$alasan = "Menyatakan adanya perubahan jaringan, adapun data nya adalah : ";
			$pdf->Cell($settingLayout['totalWidth'] - 30,$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			
		foreach($result as $data) {	
			$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
			$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
			$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$pdf->Cell($sisaKeterangan - 30,$settingLayout['heightCell'], strtoupper($data->dfno),$settingLayout['border'],0,'L',true);
	        
	        $pdf->Ln();
			
			$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
			$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$pdf->Cell($sisaKeterangan - 30,$settingLayout['heightCell'], strtoupper($data->fullnm),$settingLayout['border'],0,'L',true);
	        
	        $pdf->Ln();
			if($jum < 5) {
				$pdf->Ln();
			}
			//$pdf->Ln();
		}
		//$pdf->SetFont($settingLayout['font'],'B',$settingLayout['contentFontSize']);
		if($jum > 4) {
				$pdf->Ln();
			}
		$alasan = "Untuk ditransfer di bawah distributor berikut ini : ";
		
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['totalWidth'] - 30,$settingLayout['heightCell'], $alasan,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
		
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "SPONSOR :",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID      :  ".strtoupper($result[0]->trf_to_id),$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        //$pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		//$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_id),$settingLayout['border'],0,'L',true);
        
		$pdf->Ln();
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama :  ".strtoupper($result[0]->trf_to_nm),$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        //$pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
		//$pdf->Cell($sisaKeterangan,$settingLayout['heightCell'], strtoupper($result[0]->trf_to_nm),$settingLayout['border'],0,'L',true);
        $pdf->Ln();
		$pdf->Ln();
		/*
		$pdf->Ln();
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "RECRUITER :",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "ID      :  ".strtoupper($result[0]->recruiterid),$settingLayout['border'],0,'L',true); 	
        
		$pdf->Ln();
		$pdf->Cell(30,$settingLayout['heightCell'], "",$settingLayout['border']);
		$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Nama :  ".strtoupper($result[0]->recruitername),$settingLayout['border'],0,'L',true); 	
        $pdf->Ln();
		*/
		//Print footer
		LosPrintFooter($pdf, $settingLayout);
		$pdf->Output();
				
?>		
<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
		
		$settingLayout = settingLOSLayout($result, "Ms. Sharon / Mr. Cheong");
		$sisaKeterangan = $settingLayout['totalWidth']-$settingLayout['widthKopTitle']-$settingLayout['doubleDot'];
		//print header
		LosPrintHeader($pdf, $settingLayout);		
		
		//Isi / Maksud penyampaian
		$alasan = "Dengan ini kami beritahukan bahwa member berikut telah meminta kami untuk mengaktifkan kembali kememberan mereka";
		$alasan .= ", mohon bantuannya untuk memprosesnya di bonus month ini :"; 
		$pdf->Ln();
		$pdf->MultiCell($settingLayout['totalWidth'],$settingLayout['heightCell'], $alasan,$settingLayout['border']); 	// its similar with TD in HT$pdf->Ln();
		$pdf->Ln();
		
		$kolomNo = 10;
		$kolomID = 60;
		$kolomName = 100;
		$kolomRank = 20;
		$pdf->Ln();	
		$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
		$pdf->Cell($kolomNo,7,"No",1,0,'C',true);
		$pdf->Cell($kolomID,7,"Distributor Code",1,0,'C',true);
		$pdf->Cell($kolomName,7,"Name",1,0,'C',true);
		$pdf->Cell($kolomRank,7,"Rank",1,0,'C',true); 	
		$pdf->Ln();
		
		$i = 1;
		foreach($result as $data) {
			$pdf->Cell($kolomNo,$settingLayout['heightCell'], $i,1,0,'R',true);
			$pdf->Cell($kolomID,$settingLayout['heightCell'], strtoupper($data->dfno),1,0,'C',true);
			$pdf->Cell($kolomName,$settingLayout['heightCell'], strtoupper($data->fullnm),1,0,'L',true);
			$pdf->Cell($kolomRank,$settingLayout['heightCell'], strtoupper($data->dfno_rank),1,0,'C',true);
			$pdf->Ln();
			$i++;
		}
        
		
		//Print footer
		LosPrintFooter2sign($pdf, $settingLayout);
		$pdf->Output();
				
?>		
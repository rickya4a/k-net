<div class="mainForm">
  <form class="form-horizontal" id="losList" method="post" target="_blank">
    <fieldset>      
      <div class="control-group">
      	
      	
      	<label class="control-label" for="typeahead">Jenis Follow Up</label>                             
        <div class="controls">
        	<select class="TabOnEnter span4" id="followup_type" name="followup_type" tabindex="3">
        		<!--<option value="">--Pilih disini--</option>-->
  				<?php
  				foreach($followup_type as $key => $value) {
  					echo "<option value=\"$key\">$value</option>";
  				}
  				?>
      		</select>
        </div>
        <label class="control-label" for="typeahead">Seach By</label>                             
        <div class="controls">
        	<select class="TabOnEnter span4" id="searchBy" name="searchBy" tabindex="3">
        		<option value="">--Pilih disini--</option>
  				<option value="los_no">LOS Number</option>
  				<option value="dfno">ID member</option>
  				<option value="fullnm">Full Name</option>
  				<option value="trf_to_id">Trf to ID member</option>
  				<option value="trf_to_nm">Transfer to Name</option>
      		</select>
        </div>
        <label class="control-label" for="typeahead">Parameter Value</label>                             
        <div class="controls">
        	<input type="text" class="span5" id="paramValue" name="paramValue" />
        </div>  
        <?php
          //echo datepickerFromTo("Tgl Follow Up", "fu_from", "fu_to");
		  
        ?>
        <label class="control-label" for="typeahead">LOS Create Date</label>
        <div class="controls">
        	<input type="text" class="TabOnEnter dtpicker" id="los_create_from" name="los_create_from" tabindex="5" value="<?php echo $fu_from; ?>"/>&nbsp;
        	<input type="text" class="TabOnEnter dtpicker" id="los_create_to" name="los_create_to" tabindex="6" value="<?php echo $fu_to; ?>" />
        </div>	
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="7" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="<?php echo $act; ?>" />
            
            <!--<input tabindex="4"  type="submit" class="btn btn-success" value="To Excel" />-->
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>



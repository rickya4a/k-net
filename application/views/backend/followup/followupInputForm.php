<div class="mainForm">
  <?php $formid = "followUpInput"; ?>	
  <form class="form-horizontal" enctype="multipart/form-data" id="<?php echo $formid; ?>">
    <fieldset>      
      <div class="control-group">
      	<table width="100%" border="0" id="frmInput">
      		<tr>
      			<td width="15%" align="right">Follow Up No&nbsp;</td>
      			<td width="35%">
      				<input type="text" tabindex="1" class="span20 TabOnEnter" placeholder="<?php echo placeholderCheck(); ?>" id="formno" name="formno" onchange="All.checkDoubleInput('db2/get/formno/from/DION_followup/','formno',this.value)" />
      			</td>
      			<td width="15%" align="right">Email&nbsp;</td>
      			<td width="35%">
      				<input type="text" tabindex="7" class="span20 TabOnEnter"  id="email" name="email" />
      			</td>
      		</tr>
      		<tr>
      			<td align="right">Person Type</td>
      			<td>
      				<select style="width: 340px;" class="TabOnEnter" name="cust_type" id="cust_type" tabindex="2">
      					<?php
	      				foreach($cust_type as $key => $value) {
	      					echo "<option value=\"$key\">$value</option>";
	      				}
	      				?>
      				</select>
      				
      			</td>
      			<td align="right">Cara Penyampaian&nbsp;</td>
      			<td>
      				<select style="width: 340px;" class="TabOnEnter" name="comm_method" id="comm_method" tabindex="8">
      					<?php
	      				foreach($comm_method as $key => $value) {
	      					echo "<option value=\"$key\">$value</option>";
	      				}
	      				?>
      				</select> 
      				<input type="hidden" id="sent_to" name="sent_to" value="ADIS" />
      			</td>
      		</tr>
      		<tr>
      			<td align="right">ID Member/Stockist&nbsp;</td>
      			<td>
      				<input type="text" tabindex="3" class="span20 TabOnEnter"  id="cust_id" name="cust_id" onchange="Followup.checkIDCust()" />
      			</td>
      			<td align="right">Ada Lampiran/Bukti&nbsp;</td>
      			<td>
      				<select style="width: 340px;" name="attached_app" id="attached_app" tabindex="9" class="TabOnEnter">
      					<?php
      					foreach($listLampiran as $dta) {
      						echo "<option value=\"$dta->id\">$dta->lampiran</option>";
      					}
      					?>
      				</select> 
      				
      			</td>
      			<!---->
      		</tr>
      		<tr>
      			<td align="right">Nama&nbsp;</td>
      			<td>
      		       <input type="text" tabindex="4" class="span20 TabOnEnter"  id="cust_name" name="cust_name" />
      		    </td>
      			<!--<td align="right">Bukti Berupa&nbsp;</td>
      			<td>
      				<input type="text" tabindex="10" class="span20 TabOnEnter"  id="attaced_form" name="attaced_form" />
      			</td>
      			
      			<td align="right">Tgl Follow Up&nbsp;</td>
      			<td>
      				<input type="text" class="TabOnEnter span20" id="createdt" tabindex="10" name="createdt" value="<?php echo date("Y-m-d"); ?>" />
      			</td>-->
      			<td align="right">Jenis Follow Up&nbsp;</td>
      			<td>
      				<select class="TabOnEnter" style="width: 340px;" id="followup_type" name="followup_type" tabindex="10" onchange="Followup.setFollowupType()">
      			    <option value="">--Silahkan pilih</option>
      				<?php
      				foreach($followup_type as $key => $value) {
      					echo "<option value=\"$key\">$value</option>";
      				}
      				?>
      				</select>
      			</td>
      		</tr>
      		<tr>
      			<td align="right">Telp Rumah/Kantor&nbsp;</td>
      			<td>
      			  <input type="text" tabindex="5" class="span20 TabOnEnter"  id="tel_no" name="tel_no" />
      			</td>
      			<td align="right" valign="top">Detail Follow Up&nbsp;</td>
      			<td rowspan="2">
      				<textarea tabindex="11" type="text" class="span20 TabOnEnter" id="followup_detail" name="followup_detail"></textarea> 
      			</td>
      			<!---->
      		</tr>
			<tr>
      			<td align="right">Telp Celluler&nbsp;</td>
      			<td>
      			  <input type="text" tabindex="6" class="span20 TabOnEnter"  id="tel_hp" name="tel_hp" />
      			</td>     			
      			
      			
      			<!---->
      		</tr>
      		<tr >
      			<td colspan="4" id="nonBV"></td>
      			
      		</tr>
      		<tr >
      			 <td colspan="4" id="isBV" style="display: none;"></td>
      		</tr>
      		<tr>
      			<td>&nbsp;</td>
      			<td colspan="3">
      				<input id="btnSubmit" type="button" class="btn btn-primary" value="Simpan Data" onclick="Followup.saveFollowUp()" />
      			</td>
      		</tr>
	     </table>
     	 
		 
      </div> 
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->	  
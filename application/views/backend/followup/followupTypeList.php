<?php
  if($list == null) {
  	 echo setErrorMessage();
  } else {
  	 echo "<form id=listFollowUp><table width=\"70%\" align=center class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=3>List Follow Up Type</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=6%>No</th>";
       echo "<th>Description</th><th>Action</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($list as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
                echo "<td><div align=center><input type=\"hidden\" id=\"id$i\" value=\"$list->id\" />$list->follow_type</div></td>";
                 $ondelete = "All.deleteFormData('followup/type/delete/', '$list->id', 'followup/type/list')";
                $arr = array(
				    "update" => "Followup.getUpdateFollowUpType($i)",
				    "delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	
	<?php
  }
?>

<script>
  $( document ).ready(function() {
   All.set_datatable();
  });
</script>

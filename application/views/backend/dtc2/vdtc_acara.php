<style>
.form-group {
	margin-bottom: 3px;
}
</style>
<script type="text/javascript">
	function setSelectPay() {
		var x = $("#bank").val();
		var bankDetail = $("#bank option:selected").text();
		var str = x.split("|");
		var bankid = str[0];
		$("#bankid").val(bankid);
		$("#bankCode").val(str[1]);
		$("#bankDesc").val(str[4]);
		$("#charge_connectivity").val(str[2]);
		$("#charge_admin").val(str[3]);
		$("#bankDescDetail").val(bankDetail);

		if(bankid === "25") {
			$(".sal_va").css('display', 'block');
			var idmember = "<?php echo getUserID(); ?>";
			var password = "<?php echo getUserPassword(); ?>";

			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('va/balance/'.getUserID());?>",
				dataType : 'json',
				success: function(data){
					$("#saldo_va").val(data.arrayData[0].amount);
				}
			});
		} else {
			$("#saldo_va").val(null);
			$(".sal_va").css('display', 'none');
		}
	}
	
	function pilihjenisAcara() {
		var pil_event = $("#pil_event").val();
		res = pil_event.split("|");
		$("#event_cat_id").val(res[1]);
		$("#nm_event").val(res[0]);
		
		var cat_id = res[1];
		if(pil_event != '') {
			$.ajax({
				type : "GET",
				url  : "<?php echo site_url('dtc/listEvent/');?>" + cat_id,
				dataType : 'json',
				success: function(data){
					if(data.response == "true") {
						var arrayData = data.arrayData;
						var rowhtml = "";
						$("#opsi").html(null);
						rowhtml += "<option value=''>--Pilih Lokasi dan Waktu--</option>";
						$.each(arrayData, function(key, value) {
							rowhtml += "<option value='"+value.id+"'>"+value.nama+" - "+value.lokasi+" ("+value.event_date2+")</option>";	
						});
						$("#opsi").html(rowhtml);
					} else  {
						alert("Acara tidak tersedia..");
						$("#opsi").html(null);
					}
				}
			});
		} else {
			$("#opsi").html(null);
		}
	}

	function getDataEvent(){
		if($('#opsi').val() != '')
		{
			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('be_dtc/getDetailTiketV2');?>",
				dataType : 'json',
				data : {opsi:$('#opsi').val()},
				success: function(data){
					$("#price").val(data.price_online);
					$("#stok").val(data.sisa_kuota);
					$("#note").val(data.early_note);
					$("#early_status").val(data.earlystatus);
					$("#tglearly").val(data.earlybird_date);
					$("#tglnow").val(data.tglnow);
					document.getElementById('note').innerHTML = data.early_note;
					
					var rowhtml = "";
					if(data.merchandise !== null) {
						var prdPilihan = data.merchandise.prdPilihan;
						var prdPasti = data.merchandise.prdPasti;
						var ix = 1;
						$("#pilihProduk").html(null);
						if(prdPilihan != null) {
							Object.keys(prdPilihan).forEach((item) => {
							  rowhtml += "<div class='form-group'>";
							  rowhtml += "<label class='control-label col-sm-4' >"+item+"</label>";
							  rowhtml += "<div class='col-sm-3'>";
							  rowhtml += "<select class='form-control inp_array' name=free[]>";
							  prdPilihan[item].forEach((opt) => {
								var setWarna = "";
								if(opt.warna !== null && opt.warna !== "" && opt.warna !== " ") {
								   setWarna = " - "+opt.warna;
								}
								
								var setUkuran = "";
								if(opt.ukuran !== null && opt.ukuran !== "" && opt.ukuran !== " ") {
								   setUkuran = " - "+opt.ukuran;
								}
								
								 rowhtml += "<option value='"+opt.kode_produk+"'>"+opt.nama+setWarna+setUkuran+"</option>";	
								// jangan lupa option yang sudah dibuat di append ke select
							  });
							  rowhtml += "</select>";
							  rowhtml += "</div>";
							  rowhtml += "</div>";
							});
						}
						if(prdPasti != null) {
							Object.keys(prdPasti).forEach((item) => {
							  rowhtml += "<div class='form-group'>";
							  rowhtml += "<label class='control-label col-sm-4' >"+item+"</label>";
							  rowhtml += "<div class='col-sm-3'>";
							  rowhtml += "<select class='form-control inp_array' name=free[]>";
							  prdPasti[item].forEach((opt) => {
								 rowhtml += "<option value='"+opt.kode_produk+"'>"+opt.nama+"</option>";	
								// jangan lupa option yang sudah dibuat di append ke select
							  });
							  rowhtml += "</select>";
							  rowhtml += "</div>";
							  rowhtml += "</div>";
							});
						}
					  $("#pilihProduk").append(rowhtml);
					} else {
						$("#pilihProduk").append(rowhtml);
					}
				}
			});
		}
	}

	function DataKeTable(){
		var acara = $('#opsi option:selected').text();
		var x = document.getElementById("idmbr");
		var qty =parseInt( $('#qty').val());
		var stok=parseInt ($('#stok').val());
		var qtytot =parseInt( $('#qtytot').val());
		var harga =parseInt( $('#price').val());
		var duit=parseInt( $('#duit').val());
		
		var free_prd = "";
		$('select.inp_array').each(function() {
			free_prd +=  $(this).val() + ",";
		});
		console.log(free_prd);
		var i =0;
		var counter=0;
		for(i=0; i< $('.main tr').length;i++ ){
			var nama =$("tr#"+i).find('input').val();
			if($('#idmbr').val() == nama ){
				counter++;
			}
		}
		if(counter>0) {
			alert('Peserta hanya boleh mendaftar satu kali')
		} else {
			if(stok<qty) {
				alert('Pemesanan tiket melebihi stok')
			} else {
				if(($('#idmbr').val()!= '')&&($('#qty').val()!= '')&&($('#email').val()!= '')&&($('#display_info').val()!= 'ID Member tidak valid/Sudah Terdaftar') ){
					var rowCount = $('.main tr').length-1;
					var no = rowCount + 1;
					var num2 = parseFloat(Math.round(($('#price').val()*$('#qty').val()) * 100) / 100);
					$("<tr data-status=\"I\" id='"+no+"' >"
						+ "<td><input type=\"hidden\" name=\"idpendaftar[]\" id=\"idpendaftar\" value=\""+$('#idmbr').val()+"\" /><input type=\"hidden\" name=\"jumlah[]\" id=\"jumlah\" readonly value=\""+$('#qty').val()+"\"  />"+$('#idmbr').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"hidden\" name=\"nmpendaftar[]\" id=\"nmpendaftar\" value=\""+$('#display_info').val()+"\" />"+$('#display_info').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"emailrow[]\" id=\"emailrow\" value=\""+$('#email').val()+"\" />"+$('#email').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"notelprow[]\" id=\"notelprow\" value=\""+$('#notelp').val()+"\" />"+$('#notelp').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"event_db[]\" id=\"event_db\" value=\""+$('#opsi').val()+"\" /><input type=\"hidden\" name=\"event_nm[]\" id=\"event_nm\" value=\""+acara+"\" />"+acara+"</td>"
						+ "<td>"+$('#qty').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"harga[]\" id=\"harga\" value=\""+$('#price').val()+"\"  />"+$('#price').val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"<input type=\"hidden\" name=\"prd_bundling[]\" value="+free_prd+" /></td>"
						+ "<td>"+(num2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
						+ "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\"  onclick=\"DelTable('"+no+"')\"   /></td>"
						+" </tr>").appendTo(".main tbody");
					$('#opsi').attr("disabled", true);
					$("#stok").val( ($('#stok').val()) -qty);
					$("#qtytot").val( qtytot +qty);
					$("#duit").val( duit +(qty*harga));
				} else {
					alert('Data harus diisi');
				}
			}
		}
	}

	function loaddata()
	{
		var idnya = document.getElementById( "idmbr" ).value;
		var acara = document.getElementById( "opsi" ).value;
		if (idnya)
		{
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url();?>dtc/cariID2",
				data : {idm: idnya, acara: acara},
				dataType: "json",

				success : function(result)
				{
					if(result[0].fullnm!=null) {
						$("#display_info").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
						$("#email").val(result[0].email);
						$("#notelp").val(result[0].tel_hp);
					} else {
						$("#display_info").val( "ID Member tidak valid/Sudah Terdaftar");
						$("#email").val(null);
						$("#notelp").val(null);
					}
				}
			});
		} else {
			$( '#display_info' ).html("Please Enter Some Words");
		}
	}

	function DelTable(no){
		var harga = parseInt( $('#price').val());
		var duit = parseInt( $('#duit').val());

		var stok = parseInt ($('#stok').val());
		var qty = parseInt($("tr#"+no).find('input').val() );
		var row = $("tr#"+no).find("input[id=row]").val();
		$("#stok").val( stok+1 );
		$("#duit").val( duit -(1*harga));

		var rowCount = $('.main tr').length;

		$("tr#"+no).remove();
	}
</script>
<div class="mainForm">
	<div class="row vpullset4" style="min-height: 410px;">
		<div class="container-fluid">
			<div class="row-fluid">
				<form class="form-horizontal" action="<?php echo $action; ?>" method="post" id="frmListReg" name="frmListReg">
					<div class="col-lg-12 col-xs-12 cart_header">
						<div class="col-lg-12">
							<h2>Entry Data Pembelian Tiket Acara Power Net </h2>
							<?php
							if($listTipeAcara == null) {
							?>
							  <h4><font color="red">Saat ini belum ada tiket acara yang bisa dijual</font></h4>
							<?php
							}
							?>
						</div>
						
					</div>
					<div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
						<div class="form-group">
							<label class="control-label col-sm-4" >Jenis Event</label>
							<div class="col-sm-4">	
								<select class="form-control" data-live-search="true" id="pil_event" name="pil_event" onchange="pilihjenisAcara()">
									<?php
									echo "<option value=\"\">--Pilih Jenis Acara--</option>";
									foreach($listTipeAcara as $dta) {
										echo "<option value=\"$dta->shortname|$dta->cat_id\">$dta->category_desc</option>";
									}
									?>
								</select>
								<input type="hidden" class=" span20" id="event_cat_id" name="event_cat_id" value=""  />
								<input type="hidden" class=" span20" id="nm_event" name="nm_event" value=""  />
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-sm-4" >Lokasi</label>
							<div class="col-sm-4">
								<select class="form-control" data-live-search="true" id="opsi" name="opsi" onchange="getDataEvent()">
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >ID Member</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="idmbr" placeholder="Enter id" onchange="loaddata()">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" >Nama Member</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="display_info"  readonly >
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Email</label>
							<div class="col-sm-3">
								<input type="email" class="form-control" id="email" name="email"  >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" >No. HP</label>
							<div class="col-sm-3">
								<input type="email" class="form-control" id="notelp" name="notelp"  >
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Harga</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="price" name="price" readonly style="width: 300px;">
								<input type="hidden" class="form-control" id="early_status" name="early_status" value="" />
							</div>
							<div class="col-sm-3">
								<label class="control-label" id="note"></label>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Tiket Tersedia</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="stok" name="stok" readonly style="width: 500px;">
							</div>
						</div>
						<span id="pilihProduk">
						</span>	
						<input type="hidden" class="form-control" id="qty" value="1"  style="width: 500px;">
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a class="btn btn-default" id="addItemxd" onclick="DataKeTable()"> Tambahkan </a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-10">

								<table width="90%" class="main table table-hover" id="box-table-b">
									<thead>
									<tr>
										<th >ID Member</th>
										<th >Nama Member</th>
										<th >Email</th>
										<th>No Telp</th>
										<th >Event</th>

										<th >Quantity </th>
										<th >Harga</th>
										<th >Total amount</th>

										<th >Action</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div class="col-sm-1"></div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" >Total Bayar</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="duit" value="0" readonly name="duit" style="width: 500px;">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" >Bank:</label>
							<div class="col-sm-4">
								<select class="form-control"  id="bank" name="bank" required onchange="setSelectPay()">
									<?php
									echo "<option value=\"\">--Pilih disini--</option>";

									foreach($listBank as $dta) {
										echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
									}
									?>
								</select>
							</div>

						</div>
						<div class="form-group">
							<label style="display: none;" class="sal_va control-label col-sm-4">Saldo Virtual Account</label>
							<div style="display: none;" class="sal_va col-sm-3" >
								<input type="text" class="form-control" readonly="readonly" style="width: 400px" id="saldo_va" name="saldo_va"/>
							</div>
						</div>
						<input type="hidden" name="totalx" value="" />
						<input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
						<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
						<input type="hidden" name="bankid" id="bankid" value=""  />
						<input type="hidden" name="bankCode" id="bankCode" value=""  />
						<input type="hidden" name="bankDesc" id="bankDesc" value=""  />
						<input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
						<input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
						<input type="hidden" name="charge_admin" id="charge_admin" value=""  />



						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="button" class="btn btn-default" name="Input"  id="save" onclick="simpandata()">Pembayaran</button>
							</div>
						</div>
					</div>
				</form>
			</div><!--/fluid-row-->
		</div>
	</div>

</div>

<div>
	<iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
</div>


<script>

	function simpandata(){
		var bank= ($('#bankid').val());
		if(bank==''){
			alert("Mohon lengkapi data");
		}
		else if (bank=='25'){
			document.getElementById("save").disabled = true;

			$.post(All.get_url('<?php echo $submit_url; ?>') , $("#frmListReg").serialize(), function(data)
				{
					if(data.response == 'true')
					{
						var payment=data.arrayData;
						var orderno=payment[0].orderno;
						var total_pay=payment[0].total_pay;

						$.ajax({
							url: All.get_url('dtc/submit/va'),
							type: 'POST',
							data: {
								memberid : "<?php echo getUserID();?>",
								paymentId : orderno,
								paymentAmount : total_pay,
							},
							dataType: 'json',
							success:
								function(data){
									$("input[type=button]").removeAttr('disabled');
									if(data.response == 'true') {
										window.location = "https://www.k-net.co.id/dtc/done/"+orderno;

									} else {
										alert(data.message);
									}
								},
							error: function (xhr, ajaxOptions, thrownError) {
								alert(thrownError + ':' +xhr.status);
								$("input[type=button]").removeAttr('disabled');
							}
						});



					}
					else
					{
//			alert('keluar');
					}


				},"json");
            /*
			).fail(function() {
					alert("Error requesting page");
					All.set_enable_button();
				}); */
		}
		else
		{
			document.getElementById("save").disabled = true;
			$.post(All.get_url('<?php echo $submit_url; ?>') , $("#frmListReg").serialize(), function(data)
				{
					if(data.response == 'true')
					{
//			alert('masuk');
						var payment=data.arrayData;
						var orderno=payment[0].orderno;
						var total_pay=payment[0].total_pay;
						var data = {
								key : "51edf5e8117da341a8be702d9bc18de5",
								paymentId : orderno,
								paymentAmount : total_pay,
								backUrl : "https://www.k-net.co.id/dtc/done/"+orderno,
								bankCode : $('#bankCode').val(),
								bankProduct: $('#bankDesc').val()
							},
							sgoPlusIframe = document.getElementById("sgoplus-iframe");
						if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
						SGOSignature.receiveForm();
					}
					else
					{
						alert(data.message);
						document.getElementById("save").disabled = false;
//			alert('keluar');
					}
				},"json"); /*.fail(function() {
					alert("Error requesting page");
					All.set_enable_button();
				});*/
		}
	}
</script>
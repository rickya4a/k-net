<?php
    if(empty($detRes)){
        echo "no Data";
    }else{
 		$totIns = 5 ;
		$totInsHead = $totIns + ($totIns * 2) + 6 + 2;
    	echo "
 				<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
	 			 <thead><tr style='height: 30px;text-align: center;text-shadow: none;' >
			        <th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>
			        <th>No</th>
			        <th>Reg. No</th>
			        <th>Member ID</th>
			        <th>Member Name</th>
			        <th>VA No.</th>
			        <th>Cell Phone</th>
					<th>Total Saldo All</th>
				  </tr> </thead><tbody>";
	    /*
 		<th>DP</th>
        <th>Voucher Amt</th>
        <th>Starterkit</th>
        <th>DP - (Voucher + SK)</th>
		 */
		//detail of installment
		
        $no 			= 	1;
        $totFund 		= 	0;
		$saldoTempTot	=	0;
		$saldoTotX		=	0;
		$DPUmrTot		=	0;
		$vchUmrTot		=	0;
		$vchSKTot		=	0;
		$saldoTempTot	=	0;
		$installmentTot	=	0;
		$saldoTotTot	=	0;
		$saldoTotGrand	=	0;
        foreach($detRes as $list){
        	
			$saldoPerDist	=	0;
			
			$vcrAmt = explode('#', $list->installment_d);
			if(count($vcrAmt) > 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= $vcrAmt[1];
			}elseif(count($vcrAmt) == 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= 0;
			}else{
				$vchUmr = 0;
				$vchSK 	= 0;
			}
			//$instTD;
			
			$instAmt 	= explode('#', $list->installment_c); // data DP & installment
			$instDt 	= explode('#', $list->installment_createdt); // data DP & installment
			$DPUmr 		= $instAmt[0];
			$DPUmrTot 	= $DPUmrTot + $DPUmr;
			$saldoTemp	= $DPUmr - ($vchUmr + $vchSK);
			$saldoTot 	= $saldoTemp;
			$vchUmrTot	= $vchUmrTot + $vchUmr;
			$vchSKTot	= $vchSKTot + $vchSK;
			$saldoTempTot	= $saldoTempTot + $saldoTemp;
			/*
			echo   "<td align='right'>".number_format($DPUmr,0,".",".")."</td>
					<td align='right'>".number_format($vchUmr,0,".",".")."</td>
					<td align='right'>".number_format($vchSK,0,".",".")."</td>
					<td align='right'>".number_format($saldoTemp,0,".",".")."</td>";
*/
			if(count($instAmt) > 1){
				$instTD = "";
				for ($i=0; $i < count($instAmt)  ; $i++) {  //iteration for installment data
					if($i > 0){
						$installment 	= $instAmt[$i];
						$installmentDt 	= $instDt[$i];
						$installmentTot = $installmentTot + $installment;
						$saldoPerDist = $saldoPerDist + $installment;
					}
				}
				
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
				}
				
			}elseif(count($instAmt) == 1){
				$installment 	= 0;
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
				}
				
			}else{
				$installment 	= 0;
			}
				
			$saldoPerDist1	=	$saldoPerDist + $DPUmr;
			$saldoTot = $saldoTemp + $installment;
			
			$fullnm = $list->fullnm;
			$param = $list->registerno."###".$list->dfno."###$fullnm###".$list->novac."###".$list->tel_hp."###".$saldoTot."###".$saldoPerDist1;
	
			echo   "<tr style='height: 30px;text-align: center;text-shadow: none;'>
			        <td align=center><input type=checkbox id=pil$no name=registerno[] value=\"$param\" /></td>
		        	<td align=\"right\">".$no."</td>
			        <td align=\"left\">".$list->registerno."</td>
			        <td align=\"left\">".$list->dfno."</td>
			        <td align=\"left\">".$list->fullnm."</td>
			        <td align=\"left\">".$list->novac."</td>
			        <td align=\"left\">".$list->tel_hp."</td>
				    <td align='right'>".number_format($saldoPerDist1,0,".",".")."</td>
				    </tr>";
			$saldoTotTot = $saldoTotTot + $saldoTot;
			$saldoTotGrand = $saldoTotGrand + $saldoPerDist1;
				  
	        //$totFund += $list->tot_fund;
	        $no++;
        }
	echo "</tbody>
		 </table>
		 ";
		 }
    ?>

<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>
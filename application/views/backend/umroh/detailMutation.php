<?php
if ($result == null) {
    echo setErrorMessage();
} else { ?>

<div class="table-responsive">

    <table class="table table-bordered" width="80%">
        <thead>
		    <tr>
			    <th colspan="4">Rincian Mutasi</th>
			</tr>
            <tr>
                <th style="text-align: left">No Registrasi</th>
                <td colspan="3"><?php echo $result[0]->registerno; ?></td>
            </tr>
			<tr>
                <th style="text-align: left">ID Member</th>
                <td colspan="3"><?php echo $result[0]->dfno; ?></td>
            </tr>
            <tr>
                <th style="text-align: left">Nama</th>
                <td colspan="3"><?php echo $result[0]->fullnm; ?></td>
            </tr>
            <tr>
                <th style="text-align: left">No. KTP</th>
                <td colspan="3"><?php echo $result[0]->idno; ?></td>
            </tr>
            
            <tr>
                <th style="text-align: left">Status</th>
                <td colspan="3">
                    <?php 
                    switch ($result[0]->depart_status) {
                        case '1':
                            echo "Sudah Melakukan Keberangkatan";
                            break;
                        case '2';
                            echo "Sudah Melakukan Pembatalan";
                            break;
                        case '0';
                            echo "Masih Terdaftar";
                            break;
                    }
                    ?>
                </td>
            </tr>
        </thead>
        
        <thead>
            <tr>
                <th>Voucher No.</th>
                <th>Description</th>
                <th>Created At</th>
                <th>Total Fund</th>
            </tr>
        </thead>

    <?php
            $sum = 0;
            $sum2 = 0;
            foreach ($result as $list) { 
                if ($list->mutate_flag == '2') {
                    $sum += $list->tot_fund;
                } else if ($list->mutate_flag == '1') {
                    $sum2 += $list->tot_fund;
                }
            $total = $sum - $sum2;
            ?>
        <tbody>
            <tr>
                <td><?php echo $list->voucherno; ?></td>
                <td><?php echo $list->description; ?></td>
                <td style="text-align: center;"><?php echo date("d F Y H:i", strtotime($list->createdt)); ?></td>
                <td style="text-align: right;"><?php echo number_format($list->tot_fund, "0", ".", ","); ?></td>
            </tr>
            <?php }; ?>
            <tr>
                <td colspan="3" style="text-align: right">
                    <strong>Total DP & Cicilan</strong>
                </td>
                <td style="text-align: right"><?php echo number_format($sum, "0", ".", ","); ?></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right">
                    <strong>Total Potongan</strong>
                </td>
                <td style="text-align: right">-
                    <?php echo number_format($sum2, "0", ".", ","); ?></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right">
                    <strong>Total Yang Harus Dikembalikan</strong>
                </td>
                <td style="text-align: right">
                    <strong><?php echo number_format($total, "0", ".", ","); ?></strong>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<?php } ; ?>
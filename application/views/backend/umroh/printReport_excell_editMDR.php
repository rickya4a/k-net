<?php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=ReportInstallmentMDR.xls" );
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
?>
<?php
    if(empty($detCicilan)){
        echo "no Data";
    }else{
    	
		if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
 			$dataColAdd = "";
 		}else{
 			$dataColAdd = "<th>Address</th>
 						   <th>Contact No</th>
 						   <th>Pax</th>";
 		}
		
		echo "<style> .str{ mso-number-format:\@; } </style>
				<table border='1' style='width: 100%;' class='table table-striped table-bordered bootstrap-datatable datatable '> 
			<tr style='height: 30px;text-align: center;text-shadow: none;' >
		        <th>Nomor MVA</th>
				<th>Key2</th>
				<th>Key3</th>
				<th>currency</th>
				<th>NAMA</th>
				<th>ID MEMBER</th>
				<th>Saldo</th>
				<th>bill info 4</th>
				<th>Bill Info 05</th>
				<th>Bill Info 06</th>
				<th>Bill Info 07</th>
				<th>Bill Info 08</th>
				<th>Bill Info 09</th>
				<th>Bill Info 10</th>
				<th>Bill Info 11</th>
				<th>Bill Info 12</th>
				<th>Bill Info 13</th>
				<th>Bill Info 14</th>
				<th>Bill Info 15</th>
				<th>Bill Info 16</th>
				<th>Bill Info 17</th>
				<th>Bill Info 18</th>
				<th>Bill Info 19</th>
				<th>Bill Info 20</th>
				<th>Bill Info 21</th>
				<th>Bill Info 22</th>
				<th>Bill Info 23</th>
				<th>Bill Info 24</th>
				<th>Bill Info 25</th>
				<th>Periode Open</th>
				<th>Periode Close</th>
				<th>SubBill 01</th>
				<th>SubBill 02</th>
				<th>SubBill 03</th>
				<th>SubBill 04</th>
				<th>SubBill 05</th>
				<th>SubBill 06</th>
				<th>SubBill 07</th>
				<th>SubBill 08</th>
				<th>SubBill 09</th>
				<th>SubBill 10</th>
				<th>SubBill 11</th>
				<th>SubBill 12</th>
				<th>SubBill 13</th>
				<th>SubBill 14</th>
				<th>SubBill 15</th>
				<th>SubBill 16</th>
				<th>SubBill 17</th>
				<th>SubBill 18</th>
				<th>SubBill 19</th>
				<th>SubBill 20</th>
				<th>SubBill 21</th>
				<th>SubBill 22</th>
				<th>SubBill 23</th>
				<th>SubBill 24</th>
				<th>SubBill 25</th>
				<th>end record</th>
				$dataColAdd
		    </tr>";
	    
		//detail of installment
		
		//'\\\' AS BACKSLASH, '~' AS ENDRPT, '20150708' AS STARTDT, '20250708' AS ENDDT, A.Nomor_MVA, A.NAMA, A.ID_MEMBER, SUM(A.Saldo) AS SALDO
		$no 		= 0;
		$subBill 	= 9;
      foreach($detCicilan as $list){
			$no 		= $no + 1;
			$subBill	= $subBill + 1;
			$Nomor_MVA 	= $list->Nomor_MVA;
			$NAMA		= $list->NAMA;
			$ID_MEMBER	= $list->ID_MEMBER;
			$SALDO		= $list->SALDO;
			$STARTDT	= $list->STARTDT;
			$ENDDT		= $list->ENDDT;
			$BACKSLASH	= $list->BACKSLASH;
			$ENDRPT		= $list->ENDRPT;
			$headerVDMDR= "88146";
			$VAno		= strval($headerVDMDR).strval($Nomor_MVA);
			$subBil1 	= "01\\TOTA\\TOTAL\\$subBill";
			$totalPax	= $list->totalDep;
			
			echo "<tr>
					<td align='left' class='str'>$VAno</td>";
			
			for ($i=0; $i<2; $i++) {
					echo "<td style='text-align: right;font-weight: '>&nbsp;</td>";
				}
			
			echo   "<td align='left'>IDR</td>
				    <td align='left'>$NAMA</td>
					<td align='left'>$ID_MEMBER</td>
					<td align='right'>$SALDO</td>";
			
			for ($i=0; $i<22; $i++) {
					echo "<td style='text-align: right;font-weight: '>&nbsp;</td>";
				}
				
			echo   "<td align='right'>$STARTDT</td>
					<td align='right'>$ENDDT</td>";
			
			echo "<td align='left' class='str'>$subBil1</td>";
			
			for ($i=0; $i<24; $i++) {
					echo "<td align='left' class='str'>$BACKSLASH</td>";
				}
			
			if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
	 			$dataColField = "";
	 		}else{
	 			$addr1 = $list->addr1;
				$kelurahan = $list->kelurahan;
				$kecamatan = $list->kecamatan;
				$kota = $list->kota;
				
				if($addr1 != ""){
					$addr1 = "$addr1, ";
				}else{
					$addr1 = "";
				}
				
				if($kelurahan != ""){
					$kelurahan = "$kelurahan, ";
				}else{
					$kelurahan = "";
				}
				
				if($kecamatan != ""){
					$kecamatan = "$kecamatan, ";
				}else{
					$kecamatan = "";
				}
				
				if($kota != ""){
					$kota = "$kota, ";
				}else{
					$kota = "";
				}
				
				$dataColField = "<td>$addr1$kelurahan$kecamatan$kota</td>
	 						     <td class='str'>$list->tel_hp</td>
								 <td align='right'>$totalPax</td>";
	 		}
			
			
			
			echo "	<td align='left'>$ENDRPT</td>
					$dataColField
				  </tr>";
        }

    echo "</tr></table>";
    }
?>
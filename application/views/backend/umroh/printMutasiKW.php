<?php
	header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=KwitansiMutasi.txt");
	header("Pragma: no-cache");

    date_default_timezone_set("Asia/Jakarta");

    $tgl = date("d F Y");
    $waktu = date("d/m/y h:i:s");

    function tmbh_spaceHeaderxxx($value)
	{
    	  $kosong = '';
          for($x = 1; $x <= $value; $x++)
    	  {
    	    $kosong .= " ";
    	  }

	  echo $kosong;
	}

    function titleHeader()
	{
	    echo "-   No.    -  ";
    	tmbh_spaceHeaderxxx(8);
    	echo " Tanggal        - ";
    	tmbh_spaceHeaderxxx(11);
        echo "Deskripsi            - ";
    	tmbh_spaceHeaderxxx(3);
        echo "Kredit    - ";
        echo "\n";

    	//garisStripSum();
	}

    function titleKosong(){
        echo "-";
        tmbh_spaceHeaderxxx(19);
        echo "-";
        tmbh_spaceHeaderxxx(27);
        echo "-";
        tmbh_spaceHeaderxxx(14);
        echo "-";
        tmbh_spaceHeaderxxx(10);
        echo "-";
    }

//	foreach($dtVch as $dtax) {
//        $STATUS='';
//                    if($dtax->dprtstatus == '0' && $dtax->postStatus == '0'){
//                        $STATUS= "Belum Berangkat";
//                    }
//                    elseif($dtax->dprtstatus == '1' && $pdtax->ostStatus == '1') {
//                        $STATUS= "Sudah Berangkat";
//                    }else{
//                        $STATUS= "Batal Berangkat";
//                    };
//
//			echo "\n\n\n";
//    		echo "            PT.K-Link Indonesia\n\n";
//            echo "                          DATA MUTASI UMROH \n\n\n";
//            echo "REGISTRASI NO.  : ".$dtax->regno."\n";
//
//            echo "ID/NAME         : ".$dtax->dfno."/".$dtax->fullnm."\n";
//            echo "STATUS          : ".$STATUS."\n";
//            echo "DP (RP)         : Rp. ".number_format($dtax->tot_fund,0,",",",")."\n";
//            echo "PRINT DATE      : ".date("d/m/Y",strtotime($dtax->now))."\n";
//			echo "PRINT BY        : ".$this->username."  \n";
//			echo "INPUTED BY      : ".$dtax->createnm."\n";
//            echo "\n\n";
//
//	}


            foreach($dataum as $row){
                $dfno = $row->dfno;
                $fullnm = $row->fullnm;
                $dprtstatus = $row->depart_status;
                $postStatus = $row->post_status;
                $dprtdesc = $row->departuredesc;
                $dprtdt = $row->departuredt;
                $regno = $row->registerno;
                $postDt = $row->post_dt;
                $novac = $row->novac;

            }
                    if($dprtstatus == '0' && $postStatus == '0'){
                        $STATUS= "Belum Berangkat";
                    }
                    elseif($dprtstatus == '1' && $postStatus == '1') {
                        $STATUS= "Sudah Berangkat";
                    }else{
                        $STATUS= "Batal Berangkat";
                    };

                    if($dprtstatus == '0' && $postStatus == '0'){
                        $berangkat= "".date ("d-m-Y h:ia",strtotime($dprtdt))." / ".$dprtdesc;
                    }elseif($dprtstatus == '1' && $postStatus == '1'){
                        $berangkat= "".date ("d-m-Y h:ia",strtotime($postDt))." / ".$dprtdesc;
                    }else{
                        $berangkat= "";
                    }

            echo "\n\n\n";
            echo "            PT.K-Link Indonesia\n\n";
            echo "                          DATA MUTASI UMROH \n\n\n";
            echo "REGISTRASI NO.                : ".$regno."\n";
            echo "ID/NAME                       : ".$dfno."/".$fullnm."\n";
            echo "STATUS                        : ".$STATUS."\n";
            echo "JADWAL KEBERANGKATAN          : ".$berangkat."\n";
            echo "VIRTUAL ACCOUNT               : 88146 -".$novac."\n";

            echo "\n\n";


          //  garisStrip();

//
//            echo '
//        	<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
//            <tr style="height: 50px;text-align: center;font-size: 16px;text-shadow: none;">
//                    <td colspan="4">Details Mutasi</td>
//                </tr>
//                <tr style="height: 30px;text-align: center;text-shadow: none;">
//
//                    <th>No.</th>
//                    <th>Tanggal</th>
//                    <th>Deskripsi</th>
//                    <th>Kredit</th>
//                </tr>
//            ';
//
//                    $nox = 1;
//                    $total = 0;
//                    foreach($mutdetails as $row){
//
//                        echo' <tr style="height: 30px;text-align: center;text-shadow: none;">
//                            <td>'.$nox.'</td>
//                            <td>'.date ("d/m/Y h:ia",strtotime($row->createdt)).'</td>
//                            <td>'.$row->description.'</td>
//                            <td style="text-align: right;">Rp.'.number_format($row->total_mutasi,0,".",".").'</td>
//                        </tr>';
//
//                        $total += $row->total_mutasi;
//                        $no++;
//                    }
//            echo '
//            <tr style="height: 30px;text-align: center;font-size: 14px;text-shadow: none;">
//                <td colspan="3">Saldo</td>
//                <td style="text-align: right;font-weight: bold;">Rp.'.number_format($total,0,".",".").'</td>
//            </tr>
//            </table>
//            ';
            titleHeader();
            echo "\n";
            $nox = 1;
            $total = 0;
            $totqty = 0;
            $totdp = 0;
            foreach($mutdetails as $row) {
                echo "$nox";
                tmbh_spaceHeaderxxx(15);

                echo date ("d/m/Y h:ia",strtotime($row->createdt));
                tmbh_spaceHeaderxxx(8);
                echo "$row->description";
                tmbh_spaceDetailPersonal(2,$row->description);
                echo "Rp. ".number_format($row->total_mutasi,0,".",".");
                tmbh_spaceHeaderxxx(15);
//
//                tmbh_spaceDetailPersonal(5, $dta2->dpr);
//                echo number_format($dta2->dpr,0,".",",");
//                tmbh_spaceDetailPersonal(7, $dta2->dpr);
//
//                tmbh_spaceDetailPersonal(6,$total_dp2);
//                echo $total_dp2;
//
               echo "\n";
                $totdp += $row->total_mutasi;
                $nox++;
//                $totqty += $dta2->qty;
            }
            echo "\n";

            garisStrip();
            echo "-------------";
            echo "\n";
            //echo "isi : $totdp";
//            $space = '';
//            for($v = 1;$v <= 11;$v++)
//            {
//                $space .= " ";
//            }
//            $totDPx = number_format($totdp,0,".",",");
//            $usrname = $this->session->userdata('ecomm_user');
           //echo $space;
            echo "T O T A L       ";
            tmbh_spaceHeaderxxx(15);
            tmbh_spaceHeaderxxx(15);
            tmbh_spaceHeaderxxx(13);
            TotQty(29, "Rp. ".number_format($totdp,0,".",","));
//            TotQty(27, $totDPx);
//            echo "\n";
//            garisStrip();
//            echo "\n\n\n";
//            garisStrip3();
//            echo "\n Type Payment";
//            tmbh_spaceHeaderProduct(23);
//            echo "Amount\n";
//            //tmbh_spaceHeaderProduct(15);
//            //echo "Bank\n";
//            garisStrip3();
//            echo "\n ".$dtax['payment'][0]->paytype." ";
//            //tmbh_spaceHeaderProduct(15);
//            //TotQty(16, $totDPx);
//            //tmbh_spaceHeaderProduct(15);
//            //echo "Sinarmas \n";
//            tmbh_spaceDetailPersonal(2,$dtax['payment'][0]->paytype);
//
//            tmbh_spaceDetailPersonal(3, $totDPx);
//            echo "$totDPx\n";
            echo "\n\n\n\n\n";

			echo "\n";
           /* titleHeader();
            echo "\n";
            garisStrip();
			echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            tmbh_spaceHeaderxxx(47);

            echo "\n";
            tmbh_spaceHeaderxxx(65);
            echo "==========";
            echo "\n";
            echo "For HQ/Branch/Stockist use only";
            echo "\n";
            echo "PAYMENT MODE :";
            echo "\n";
            echo "*Total DP of K-link products redeemed : ";
            echo "\n";
            echo "Less : The K-Link cash voucher amount : Rp. ".number_format($dtax->tot_fund,0,",",",")."";
            echo "\n";
            echo "       The difference paid by cash    :";
            echo "\n";
            garisSambung();
            echo "\n";
            echo "I certify that the products redeemed is correct and in a good condition";*/
            echo "\n\n\n\n\n";
            echo "================";
            tmbh_spaceHeaderxxx(10);
            echo "================";
            tmbh_spaceHeaderxxx(10);

            echo "\n";
            echo "Distributor";
            tmbh_spaceHeaderxxx(15);
            echo "Cashier";
            tmbh_spaceHeaderxxx(8);


?>
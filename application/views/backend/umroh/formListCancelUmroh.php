<?php

    if(empty($result)){
        setErrorMessage();
    }else {
        ?>

<table
    width="100%"
    class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
        <tr>
            <th colspan='7'>List Pembatalan</th>
        </tr>
        <tr>
            <th>No.</th>
            <th>ID Member</th>
            <th>Nama</th>
            <th>Alasan</th>
            <th>Batch No.</th>
            <th>Tanggal</th>
            <th>Cetak</th>
        </tr>
    </thead>

    <tbody>
        <?php
            $no = 1;
            foreach ($result as $dt) {
				?>
        <tr>

            <td align="center"><?php echo $no; ?></td>
            <td><?php echo $dt->dfno ?></td>
            <td><?php echo $dt->fullnm; ?></td>
            <td><?php echo $dt->ket ?></td>
            <td><?php echo $dt->batchno; ?></td>
            <td align="center"><?php echo date("d-m-Y", strtotime($dt->date)); ?></td>
            <td>
                <form
                    action="<?php echo site_url('be_umroh/cancel/print') ?>"
                    method="post"
                    target="_blank">
                    <input type="hidden" name="batchno" value="<?php echo $dt->batchno; ?>">
                    <input type="submit" class="btn btn-success" value="Cetak">
                </form>
            </td>
        </tr>
        <?php
                $no++;
            }
            ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {

        $(All.get_active_tab() + " .datatable").dataTable({
            "aLengthMenu": [
                [
                    10, 25, 50, 100, -1
                ],
                [
                    10, 25, 50, 100, 'All'
                ]
            ],
            "sPaginationType": "bootstrap",
            "oLanguage": {},
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });
</script>

<?php
    }
?>
<?php
    if(empty($detListUmroh)){
        echo "no Data";
    }else{
?>

<!-- <table>
	<tr>
		<td>
			<form method="post" action="<?php echo site_url('be_umroh/listUpload/journey/xls');?>" target="_blank">
			    <input type="hidden" name="date_from" id="date_from" value="<?php echo $date_from;?>"/>
			    <input type="hidden" name="date_to" id="date_to" value="<?php echo $date_to;?>"/>
			    <input type="hidden" name="journey_type" id="journey_type" value="<?php echo $journey_type;?>"/>
			    <input type="submit" id="printTxt" name="printTxt" value="Export" class="btn btn-primary"/> 
			</form>
		</td>
	</tr>
</table> -->
<div class="overflow-auto">
 <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable ">
 	
 	<?php
 	//".$headCicilan[0]->bankDesc."
 		$totIns = 15;
		$totInsHead = $totIns;
    	echo "<tr style='height: 50px;text-align: center;font-size: 16px;text-shadow: none;'>
		        <td colspan='$totInsHead'>Report Upload Data Umroh To Web</td>
		     </tr>
    
		     <tr style='height: 30px;text-align: center;text-shadow: none;' >
		        <th>No</th>
		        <th>ID member</th>
		        <th>Full Name</th>
		        <th>IC No</th>
		        <th>Sex</th>
		        <th>Birth Place</th>
		        <th>Birth Date</th>
		        <th>Address</th>
		        <th>Relational</th>
		        <th>HP No.</th>
		        <th>Telp. No.</th>
		        <th>Reg. Date</th>
		        <th>Dep. Date</th>
		        <th>Passport No.</th>
		        <th>Passport Name</th>
		      </tr>";
        
		$no = 1;
		foreach($detListUmroh as $list){

        	$dfno 		= $list->dfno;
			$fullnm 	= $list->fullnm;
			$idno 		= $list->idno;
			$sex 		= $list->sex;
			$birthplace	= $list->birthplace;
			$birthdt 	= $list->birthdt;
			$addr		= $list->addr;
			$relation	= $list->relation;
			$tel_hp		= $list->tel_hp;
			$tlp		= $list->tlp;
			$createdt	= $list->createdt;
			$departuredt= $list->departuredt;
			$passportno	= $list->passportno;
			$passportnm	= $list->passportnm;
			
			echo   "<tr>
				    <td align='right'>".number_format($no,0,".",".")."</td>
					<td align='left'>$dfno</td>
					<td align='left'>$fullnm</td>
					<td align='left'>$idno</td>
					<td align='left'>$sex</td>
					<td align='left'>$birthplace</td>
					<td align='left'>$birthdt</td>
					<td align='left'>$addr</td>
					<td align='left'>$relation</td>
					<td align='left'>$tel_hp</td>
					<td align='left'>$tlp</td>
					<td align='left'>$createdt</td>
					<td align='left'>$departuredt</td>
					<td align='left'>$passportno</td>
					<td align='left'>$passportnm</td>
					</tr>";
	        $no++;
        }
    ?>
</table>
</div>
<?php
    }
?>
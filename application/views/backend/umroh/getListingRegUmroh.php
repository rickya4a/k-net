<?php
	if($result['response'] == "false") {
		setErrorMessage("No result found, try another date..");
	} else {
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="7">List Registration Umroh from <?php echo $reg_from; ?> - <?php echo $reg_to; ?></th>
    </tr>	
    <tr>
        <th width="5%">No.</th>
        <th width="10%">Register No</th>
        <th width="15%">ID Member</th>
        <th>Name</th>
        <th width="15%">Cell No</th>
        <th width="10%">Date</th>
        <th width="10%">Input</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		
        foreach($result['arrayData'] as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs	
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        
        <td align="center"><a id="<?php echo $row->registerno;?>" onclick="All.ajaxShowDetailonNextForm('be_umroh/regno/<?php echo $row->registerno;?>')"><?php echo $row->registerno;?></a></td>
        <td align="center"><?php echo $row->dfno;?></td>
        <td ><?php echo $row->fullnm; ?></td>
        <td align="center"><?php echo $row->tel_hp;?></td>
        <td align="center"><?php echo $row->createdt;?></td>
        <td align="center"><?php echo $row->createnm;?></td>
        
   </tr>
    <?php
       
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
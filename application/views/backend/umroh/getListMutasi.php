<div class="mainForm">
	<div id="frmMutasi">
	<form class="form-horizontal" id="frmMut" name="frmMut" method="post">
            <fieldset>
                <div class="control-group"> 
			       <label class="control-label" for="typeahead">Status</label>
			         <div class="controls">
			           <select id="status" name="status">
			            	<option value = "all">All</option>
			            	<option value = "0">Unposted</option>
			            	<option value = "1">Posted</option>
			            	<option value = "2">Cancel/Reject</option>
			          </select>
					 </div>
					
					 <label class="control-label" for="typeahead">Approval Status</label>
			         <div class="controls">
			           <select id="appstatus" name="appstatus">
			            	<option value = "all">All</option>
			            	<option value = "0">Pending</option>
			            	<option value = "1">Approved</option>
			            	<option value = "2">Cancel/Reject</option>
			          </select>
					 </div>
                     
                     <label class="control-label" for="typeahead">Search By</label>
			         <div class="controls">
			           <select id="searchBy" name="searchBy">
                            <option value = "x">Silahkan Pilih</option>
			            	<option value = "id">Id Member</option>
			            	<option value = "nmmember">Nama Member</option>
			            	<option value = "regno">Register No</option>
                            <option value = "va">Virtual Account</option>
			          </select>
					 </div>
                     
                     <label class="control-label" for="typeahead">Parameter Value</label>
					 <div class="controls">
                        <input type="text" id="paramValue" name="paramValue" class="uppercase span5" placeholder="Silahkan Isi dengan Id / Reg No / Nama"/>
                     </div> 
                    
                    <label class="control-label" for="typeahead">Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="reg_from" name="reg_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="reg_to" name="reg_to" />
                    </div>
                    
                    <label class="control-label" for="typeahead">&nbsp;</label> 
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="be_umroh.getListMutasi()"/>
                    </div>
                </div>
            </fieldset>
            <div id="listMutasiUmroh"></div>
        </form>
       </div>       
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker("setDate", new Date());;
	});	
</script>
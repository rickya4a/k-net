<?php
	header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=VoucherUmroh.txt");
	header("Pragma: no-cache");
    
    date_default_timezone_set("Asia/Jakarta");
	
    $tgl = date("d F Y");
    $waktu = date("d/m/y h:i:s");
	
    function tmbh_spaceHeaderxxx($value)
	{
    	  $kosong = '';
          for($x = 1; $x <= $value; $x++)
    	  {
    	    $kosong .= " ";
    	  }

	  echo $kosong;
	}
    
    function titleHeader()
	{
	    echo "-   Product Code    -  ";
    	tmbh_spaceHeaderxxx(1);
    	echo "Product Description     - ";
    	tmbh_spaceHeaderxxx(2);
        echo "Quantity   - ";
    	tmbh_spaceHeaderxxx(2);
        echo "Amount - ";
        echo "\n";
	    
    	//garisStripSum();
	} 
    
    function titleKosong(){
        echo "-";
        tmbh_spaceHeaderxxx(19);
        echo "-";
        tmbh_spaceHeaderxxx(27);
        echo "-";
        tmbh_spaceHeaderxxx(14);
        echo "-";
        tmbh_spaceHeaderxxx(10);
        echo "-";
    }
    
	foreach($dtVch as $dtax) {
            if($dtax->claimstatus =='1'){
                $y = 'VOUCHER SUDAH DI CLAIM';
            }else{
                $y = 'VOUCHER BELUM DI CLAIM';
            }
		
			echo "\n\n\n";
    		echo "            PT.K-Link Indonesia\n\n";
            echo "                          K-SMART UMROH CASH VOUCHER\n\n\n";
			echo "VOUCHER NO    : ".$dtax->voucherno."\n";
            echo "DATE          : ".date("d/m/Y",strtotime($dtax->createdt))."\n";
            echo "MEMBER CODE   : ".$dtax->dfno."\n";
            echo "NAME          : ".$dtax->fullnm."\n";
            echo "AMOUNT (RP)   : Rp. ".number_format($dtax->tot_fund,0,",",",")."\n";
            echo "STATUS        : ".$y."\n";
            echo "\n\n";
            
	}
    
            garisStrip();
			echo "\n";
            titleHeader();
            echo "\n";
            garisStrip();
			echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            titleKosong();
            echo "\n";
            garisStrip();
            echo "\n";
            tmbh_spaceHeaderxxx(47); 
            echo "*Total DP Amount : ";
            echo "\n";
            tmbh_spaceHeaderxxx(65);
            echo "==========";
            echo "\n";
            echo "For HQ/Branch/Stockist use only";
            echo "\n";
            echo "PAYMENT MODE :";
            echo "\n";
            echo "*Total DP of K-link products redeemed : ";
            echo "\n";
            echo "Less : The K-Link cash voucher amount : Rp. ".number_format($dtax->tot_fund,0,",",",")."";
            echo "\n";
            echo "       The difference paid by cash    :";
            echo "\n";
            garisSambung();
            echo "\n";
            echo "I certify that the products redeemed is correct and in a good condition";
            echo "\n\n\n\n\n";
            echo "================";
            tmbh_spaceHeaderxxx(3);
            echo "================";
            tmbh_spaceHeaderxxx(7);
            echo "====================================";
            echo "\n";
            echo "Distributor's";
            tmbh_spaceHeaderxxx(5);
            echo "Distributor's TelNo";
            tmbh_spaceHeaderxxx(4);
            echo "Stockist's/Officer's Stamp & Signature";
            
?>
<style>
	.birthdt {
		width: 80px;
	}
	
	.stk {
		width: 450px;
	}
</style>
<div class="mainForm">
	<form class="form-horizontal" id="formUmroh">
	   
       <div class="control-group">
       	  <label  class="control-label">Tipe Perjalanan<font color="red">&nbsp;*</font></label>
            <div class="controls">
	            <select tabindex="1"  class="form-list required" id="type_ks" name="type_ks" onchange="be_umroh.getTipePerjalanan(this.value)">
	            	<option value="1">Umroh</option>
	            	<option value="2">Ziarah</option>
	            </select>     
    	    </div> 
       	  <label class="control-label" for="typeahead">Tipe Calon Jamaah</label>
       	     <div class="controls"> 
       	     	<div class="radio">
       	     		<input name="tipeJamaah" id="tipeJamaah1" type="radio" value="1" checked="checked"/> Member
       	     	</div>
       	     	<div class="radio">
       	     		<input name="tipeJamaah" id="tipeJamaah2" type="radio" value="2" />  Bukan Member kurang dari 18 tahun
       	     	</div>
       	     	<div class="radio">
       	     		<input name="tipeJamaah" id="tipeJamaah3" type="radio" value="3" /> Bukan Member diatas 18 tahun
       	     	</div>
       	     </div>	
       	  <label class="control-label" for="typeahead" id="lbl-idmemb">ID Member</label>
	        <div class="controls">  
	       	  <input tabindex="2" style="text-transform:uppercase" placeholder="Kosongkan jika bukan member k-link" class="uppercase span5" type="text" id="idmember" name="idmember" onchange="be_umroh.get_distributor_info(this.value)" />
            </div>
          <label class="control-label">Nama Lengkap<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="3" style="text-transform:uppercase" class="required uppercase span10" type="text" id="fullnm" name="fullnm" readonly="true"/>
            </div>
          <label class="control-label">ID Recruiter<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="4" style="text-transform:uppercase" class="uppercase span5" type="text" id="idrecruiter" name="idrecruiter" onchange="be_umroh.get_recruiter_info(this.value)" />
            </div>
           <label class="control-label">Nama Recruiter</label>  
            <div class="controls">
              <input tabindex="5" style="text-transform:uppercase" class="uppercase span10" type="text" id="nmrecruiter" name="nmrecruiter" />
            </div> 
          <label class="control-label">ID Sponsor<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="6" style="text-transform:uppercase" class="uppercase span5" type="text" id="idsponsor" name="idsponsor" onchange="be_umroh.get_sponsor_info(this.value)" />
            </div>
          <label class="control-label">Nama Sponsor</label>  
            <div class="controls">
              <input tabindex="7" style="text-transform:uppercase" class="uppercase span10" type="text" id="nmsponsor" name="nmsponsor" />
            </div>
         
          <label class="control-label">No KTP<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="8" placeholder="Untuk member/non member di atas usia 17 tahun" class="uppercase span10" type="text" id="idno" name="idno" readonly="true" onchange="be_umroh.cekNoKtp(this.value)" />
            </div> 
          <label  class="control-label">Jenis Kelamin<font color="red">&nbsp;*</font></label>
            <div class="controls">
	            <select tabindex="9"  class="form-list required" id="sex" name="sex">
	            	<option value="M">Pria</option>
	            	<option value="F">Wanita</option>
	            </select>     
    	    </div>
    	  <label class="control-label">Tempat Lahir<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="10" style="text-transform:uppercase" class="uppercase span5" type="text" id="birthplace" name="birthplace"  />
            </div>
        
          <label class="control-label">Tgl Lahir<font color="red">&nbsp;*</font></label>
            <div class="controls">
             <?php echo datebirth_combo(0, 80, 'birthdt'); ?>
            </div>           
    	  <label class="control-label">Nama Ayah Kandung<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="12" style="text-transform:uppercase" class="uppercase span10" type="text" id="fathersnm" name="fathersnm" />
            </div>
          <label class="control-label">Email<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="13" class="uppercase span10" type="text" id="email" name="email" />
            </div>  
          <label class="control-label">Alamat<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="14" style="text-transform:uppercase" placeholder="Sertakan Kecamatan dan Kelurahan" type="text"  class="uppercase span10" id="addr1" name="addr1" />
            </div>
          <label class="control-label">Alamat Stt Bonus<font color="red">&nbsp;*</font></label>  
            <div class="controls">
              <input tabindex="15" style="text-transform:uppercase" type="text"  class="uppercase span10" id="addr2" name="addr2" />
            </div>  
          <label class="control-label">Kode Pos</label>  
            <div class="controls">
              <input tabindex="16" type="text"  class="only-number span4" id="zipcode" name="zipcode" />
            </div> 
          <label  class="control-label">Kartu & Stt Bonus<font color="red">&nbsp;*</font></label>
            <div class="controls">
	            <select tabindex="17" class="form-list required stk" id="idstk" name="idstk">
	            	<!-- option value="0">--Pilih Stockist--</option -->
                            <?php
                                foreach($idstk as $row){
                                    echo "<option value = \"$row->loccd\">".$row->loccd." - ".$row->fullnm." </option>";    
                                }
                                
                            ?>
	            </select>     
    	    </div> 
    	  <label class="control-label">No HP<font color="red">&nbsp;*</font></label>  
            <div class="controls">
            	
              <input tabindex="18" class="uppercase span5" type="text" id="tel_hp" name="tel_hp"  /> <!-- onchange="be_umroh.cekNoHp(this.value)"-->
            </div>  
          <label class="control-label">No. Telp Rmh</label>  
            <div class="controls">
              <input tabindex="19" class="uppercase span5" type="text" id="tel_hm" name="tel_hm" />
            </div>
          <label class="control-label">Hubungan Keluarga<font color="red">&nbsp;*</font></label>
            <div class="controls">
	            <select tabindex="20" class="form-list required" id="statusJamaah" name="statusJamaah">
	            	<option value="1">Anak</option>
                    <option value="2">Istri</option>
                    <option value="3">Saudara</option>
                	<option value="4">Suami</option>
                    <option value="5">Lainnya</option>
	            </select>     
    	    </div>
    	  <label class="control-label">Jadwal Keberangkatan<font color="red">&nbsp;*</font></label>
            <div class="controls">
	            <select tabindex="21" class="form-list required stk" id="jdwlbrkt" name="jdwlbrkt">
	            	<?php 
                        foreach($jadwal as $row){
                            //$tglbrkt = date("d-m-Y",strtotime($row->departuredt));
                            echo "<option value=\"".$row->id."-".$row->departuredesc."\">".$row->departuredt." ".$row->departuredesc."</option>";
                        }
                    ?>
	            </select>     
    	    </div>
    	  <label class="control-label">Nama Sesuai Passport</label>  
            <div class="controls">
              <input tabindex="22" style="text-transform:uppercase" type="text"  class="uppercase span10" id="passportnm" name="passportnm" />
            </div>
          <label class="control-label">Nomor Passport</label>  
            <div class="controls">
              <input tabindex="23" type="text"  class="uppercase span10" id="passportno" name="passportno" />
            </div>  
           <div class="controls"  id="inp_btn">
            <input tabindex="24" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_umroh.getPreviewInpUmroh()" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            
          </div>                                                   
        </div>  
	   <div class="result"></div>	
	</form>
</div>
<script>
  jQuery(document).ready(function($){
		if($(All.get_active_tab() + " #formUmroh").length) {
			 be_umroh.getTipeJamaah();
		
        }
  });
</script>
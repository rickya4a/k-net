<div class="mainForm">
    <div id="frmPend">
        <form class="form-horizontal" id="frmReportInstall" name="frmReport" method="post">
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Journey Type</label>
                    <div class="controls">  
			       	  <select id="journeyType" name="journeyType">
                        	echo "<option value = "0">All</option>";
                        	echo "<option value = "1">Umroh</option>";
                        	echo "<option value = "2">Ziarah</option>";
		              </select>
		              <select id="dataView" name="dataView">
                        	echo "<option value = "0">No Address</option>";
                        	echo "<option value = "1">With Address</option>";
		              </select>
		            </div>
		            
		            <label class="control-label" for="typeahead">Status</label>
			         <div class="controls">
			           <select id="status" name="status">
			            	<option value = "all">All</option>
			            	<option value = "0">Pending</option>
			            	<option value = "1">Posted</option>
			            	<option value = "2">Cancel/Reject</option>
			          </select>
					 </div>
					 
                    <label class="control-label" for="typeahead">Transaction Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="date_to" name="date_to" />
                    </div>
                    
                    
                    <label class="control-label" for="typeahead">Order By</label>
                    <div class="controls">
                        <select id="ordertype" name="ordertype">
			            	<option value = "fullnm">Distributor Name</option>
			            	<option value = "dfno">Distributor Code</option>
			            	<option value = "total">Total</option>
			          	</select>
			          	
			          	<select id="orderby" name="orderby">
                        	echo "<option value = "asc">Ascending</option>";
                        	echo "<option value = "desc">Descending</option>";
		              </select>
                    </div>
                    
                    
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="be_umroh.getListInstall()"/>
                    </div>
                </div>
            </fieldset>
        </form>
        <div id="listInstallUmroh"></div>
    </div>
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'dd/mm/yy',
		}).datepicker("setDate", new Date());;
	});	
</script>
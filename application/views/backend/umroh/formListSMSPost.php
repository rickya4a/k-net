<?php
   if(isset($err_msg)) {
   	  echo setErrorMessage($err_msg);
   } else {
?>
<div class="mainForm">
  <!--<form class="form-horizontal" id="formPrintShippingLabel" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" > -->
  <form class="form-horizontal" id="formListSMSPost">
    <fieldset>      
      <div class="control-group">       
       <label class="control-label" for="typeahead">Status</label>
         <div class="controls">
           <select id="status" name="status">
            	<option value = "0">Unposted</option>
            	<option value = "1">Posted</option>
            	<option value = "2">Cancel/Reject</option>
          </select>
		 </div>
		 
         <label class="control-label" for="typeahead">Sort</label>
	     <div class="controls">
	     	<select id="sort" name="sort">
	        	<option value = "0">ID Member</option>
	        	<option value = "1">Name</option>
	        	<option value = "2">Saldo</option>
	      	</select>
	      	<select id="acdc" name="acdc">
	        	<option value = "0">Ascending</option>
	        	<option value = "1">Descending</option>
	      	</select>
	      </div>
		 
	   	 <label class="control-label" for="typeahead">Date</label>
         <div class="controls">
            <input type="text" class="dtpicker typeahead" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
            <input type="text"  class="dtpicker typeahead" id="date_to" name="date_to" />
         </div>
	        
	   	 <label class="control-label" for="typeahead">&nbsp;</label>
         <div class="controls">	  
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="search" value="Search" onclick="All.ajaxFormPost(this.form.id,'be_umroh/smsbcpost/act')" />
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="process" value="Process" onclick="be_umroh.getSMSBCPostPrev()"/>
       </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'dd/mm/yy',
		}).datepicker("setDate", new Date());;
	});	
	
	function checkValidationShippingSum() {
		var atLeastOneIsChecked = $(All.get_active_tab() + ' input[name="registerno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			alert("Please select at least one transaction..");
			return false;
		} 
	}
</script>
<?php
}
?>
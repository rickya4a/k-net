
<?php
    if(empty($detCicilan)){
        echo "no Data";
    }else{
?>

<table>
	<tr>
		<td>
			<form method="post" action="<?php echo site_url('be_umroh/report/printExcell');?>" target="_blank">
			    <input type="hidden" name="date_from" id="date_from" value="<?php echo $date_from;?>"/>
			    <input type="hidden" name="date_to" id="date_to" value="<?php echo $date_to;?>"/>
			    <input type="hidden" name="status" id="status" value="<?php echo $status;?>"/>
			    <input type="hidden" name="dataView" id="dataView" value="<?php echo $dataView;?>"/>
			    <input type="hidden" name="journeyType" id="journeyType" value="<?php echo $journeyType;?>"/>
			    <input type="hidden" name="ordertype" id="ordertype" value="<?php echo $ordertype;?>"/>
			    <input type="hidden" name="orderby" id="orderby" value="<?php echo $orderby;?>"/>
			    <input type="submit" id="printTxt" name="printTxt" value="Print Report" class="btn btn-primary"/> 
			</form>
		</td>
		<td>
			<form method="post" action="<?php echo site_url('be_umroh/report/printExcellMDR');?>" target="_blank">
			    <input type="hidden" name="date_from" id="date_from" value="<?php echo $date_from;?>"/>
			    <input type="hidden" name="date_to" id="date_to" value="<?php echo $date_to;?>"/>
			    <input type="hidden" name="status" id="status" value="<?php echo $status;?>"/>
			    <input type="hidden" name="dataView" id="dataView" value="<?php echo $dataView;?>"/>
			    <input type="hidden" name="journeyType" id="journeyType" value="<?php echo $journeyType;?>"/>
			    <input type="hidden" name="ordertype" id="ordertype" value="<?php echo $ordertype;?>"/>
			    <input type="hidden" name="orderby" id="orderby" value="<?php echo $orderby;?>"/>
			    <input type="submit" id="printVAMDR" name="printVAMDR" value="Print Report VA To Mandiri" class="btn btn-primary"/> 
			</form>
		</td>
	</tr>
</table>
<div class="overflow-auto">
 <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable ">
 	
 	<?php
 	//".$headCicilan[0]->bankDesc."
 		if($dataView == "0"){
 			$dataCol = 0;
 		}else{
 			$dataCol = 2;
 		}
		
 		$totIns = 10 + $dataCol;
		$totInsHead = $totIns + ($totIns * 2) + 6 + 2;
		//echo $totInsHead;
    	echo "<tr style='height: 50px;text-align: center;font-size: 16px;text-shadow: none;'>
		        <td colspan='33'>Report Setoran Cicilan K-Smart Umroh</td>
		     </tr>
    
		     <tr style='height: 30px;text-align: center;text-shadow: none;' >
		        <th>No</th>
				<th>Action</th>
		        <th>Reg. Date</th>
		        <th>Reg. No</th>
		        <th>Member ID</th>
		        <th>Member Name</th>
		        <th>VA No.</th>
		        <th>DP</th>
		        <th>Voucher Amt</th>
		        <th>Starterkit</th>
		        <th>DP - (Voucher + SK)</th>
		        <th>IP No</th>";
			   
	   if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
 			echo "";
 		}else{
 			echo "<th>Voucher No</th>";
 		}
    
        for ($i=0; $i<$totIns; $i++) {
        	$ii = $i+1; 
			echo "<th>Inst. Date $ii</th>
				  <th>Installment $ii</th>";
		}
			  
		if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
 			$dataColAdd = "";
 		}else{
 			$dataColAdd = "<th>Address</th>
 						   <th>Contact No</th>";
 		}
			
		echo "	<th>Total Saldo - (SK+Voucher)</th>
				<th>Total Saldo All</th>
				$dataColAdd
			  </tr>";
	    
		//detail of installment
		
        $no 			= 	1;
        $totFund 		= 	0;
		$saldoTempTot	=	0;
		$saldoTotX		=	0;
		$DPUmrTot		=	0;
		$vchUmrTot		=	0;
		$vchSKTot		=	0;
		$saldoTempTot	=	0;
		$installmentTot	=	0;
		$saldoTotTot	=	0;
		$saldoTotGrand	=	0;
        foreach($detCicilan as $list){
        	
			$saldoPerDist	=	0;
			
			echo "<tr style='height: 30px;text-align: center;text-shadow: none;'>
		        	<td align='right'>".$no."</td>
					<td>
                    <form id='printTxt' name='printTxt' method='post' target='_blank' action='http://www.k-net.co.id/be_umroh/print/kw/txt'>
						<input id='regnose' name='regnose' type='hidden' value='".$list->registerno."'>
                        <input id='submitss' name='submit' class='btn btn-primary' value='Print Kwitansi' type='submit'>
                    </form>
					</td>
			        <td>".$list->createdt2."</td>					
			        <td align='left'>".$list->registerno."</td>
			        <td align='left'>".$list->dfno."</td>
			        <td align='left'>".$list->fullnm."</td>
			        <td align='left'>".$list->novac."</td>";
			        			
			$vcrAmt = explode('#', $list->installment_d);
			if(count($vcrAmt) > 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= $vcrAmt[1];
			}elseif(count($vcrAmt) == 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= 0;
			}else{
				$vchUmr = 0;
				$vchSK 	= 0;
			}
			//$instTD;
			
			$instAmt 	= explode('#', $list->installment_c); // data DP & installment
			$instDt 	= explode('#', $list->installment_createdt); // data DP & installment
			$DPUmr 		= $instAmt[0];
			$DPUmrTot 	= $DPUmrTot + $DPUmr;
			$saldoTemp	= $DPUmr - ($vchUmr + $vchSK); //ini dia
			$saldoTot 	= $saldoTemp;
			$vchUmrTot	= $vchUmrTot + $vchUmr;
			$vchSKTot	= $vchSKTot + $vchSK;
			$saldoTempTot	= $saldoTempTot + $saldoTemp;
			
			echo   "<td align='right'>".number_format($DPUmr,0,".",".")."</td>
					<td align='right'>".number_format($vchUmr,0,".",".")."</td>
					<td align='right'>".number_format($vchSK,0,".",".")."</td>
					<td align='right'>".number_format($saldoTemp,0,".",".")."</td>
					<td>".$list->IPno."</td>";
			if($dataView == "1"){ // voucher no
	 			echo "<td>".$list->voucherno."</td>";
	 		}

			if(count($instAmt) > 1){
				$instTD = "";
				for ($i=0; $i < count($instAmt)  ; $i++) {  //iteration for installment data
					if($i > 0){
						$installment 	= $instAmt[$i];
						$installmentDt 	= $instDt[$i];
						echo "<td>$installmentDt</td>
							  <td align='right'>".number_format($installment,0,".",".")."</td>";
						//$saldoTot = $saldoTemp + $installment;
						$installmentTot = $installmentTot + $installment;
						$saldoPerDist = $saldoPerDist + $installment;
					}
				}
				
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
					echo "<td>&nbsp;</td>
						  <td align='right'>".number_format(0,0,".",".")."</td>";
				}
				
			}elseif(count($instAmt) == 1){
				$installment 	= 0;
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
					echo "<td>&nbsp;</td>
						  <td align='right'>".number_format(0,0,".",".")."</td>";
				}
				
			}else{
				$installment 	= 0;
			}
			
			if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
	 			$dataColField = "";
	 		}else{
	 			$addr1 = $list->addr1;
				$kelurahan = $list->kelurahan;
				$kecamatan = $list->kecamatan;
				$kota = $list->kota;
				
				if($addr1 != ""){
					$addr1 = "$addr1, ";
				}else{
					$addr1 = "";
				}
				
				if($kelurahan != ""){
					$kelurahan = "$kelurahan, ";
				}else{
					$kelurahan = "";
				}
				
				if($kecamatan != ""){
					$kecamatan = "$kecamatan, ";
				}else{
					$kecamatan = "";
				}
				
				if($kota != ""){
					$kota = "$kota, ";
				}else{
					$kota = "";
				}
				
				$dataColField = "<td>$addr1$kelurahan$kecamatan$kota</td>
	 						     <td>$list->tel_hp</td>";
	 						    // <td>$list->voucherno</td>
	 		}
				
			$saldoPerDist1	=	$saldoPerDist + $DPUmr;
			$saldoTot = $saldoTemp + $installment;
			echo   "<td align='right'>".number_format($saldoTot,0,".",".")."</td>
				    <td align='right'>".number_format($saldoPerDist1,0,".",".")."</td>
				    $dataColField
				    </tr>";
			$saldoTotTot = $saldoTotTot + $saldoTot;
			$saldoTotGrand = $saldoTotGrand + $saldoPerDist1;
				  
	        //$totFund += $list->tot_fund;
	        $no++;
        }
    ?>
    <tr style="height: 30px;text-align: center;font-size: 14px;text-shadow: none;"> 
        <td colspan="6">Total</td>
        <td style="text-align: right;font-weight: " align="right"><?php echo number_format($DPUmrTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: " align="right"><?php echo number_format($vchUmrTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: " align="right"><?php echo number_format($vchSKTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: " align="right"><?php echo number_format($saldoTempTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: ">&nbsp;</td>
        <?php 
	        for ($i=0; $i<$totIns; $i++) {
		        	$ii = $i+1; 
					echo "<td style='text-align: right;font-weight: '>&nbsp;</td>
						  <td style='text-align: right;font-weight: '>&nbsp</td>";
				}
		?>    
        <td style="text-align: right;font-weight: " align="right"><?php echo number_format($saldoTotTot,0,".",".");?></td>    
        <td style="text-align: right;font-weight: " align="right"><?php echo number_format($saldoTotGrand,0,".",".");?></td>    
    </tr>
</table>
</div>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

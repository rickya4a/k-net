
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
            <tr>
                <th colspan="2" bgcolor="#lightgrey">Data Voucher Umroh</th>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach($vchInfo as $xxx){
                $regno = $xxx->registerno;
                $dfno = $xxx->dfno;
                $fullnm = $xxx->fullnm;
                $fund = $xxx->tot_fund;
                $vchno = $xxx->voucherno;
                $status = $xxx->claimstatus;
            }
            if($status == '1'){
                $y = 'Claimed Voucher';
            }else{
                $y = 'Not Yet Claimed Voucher';
            }
        ?>
            <tr bgcolor="#f4f4f4">
                <td>
                    Register No
                </td>
                <td>
                    <?php echo $regno;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    ID Member
                </td>
                <td>
                    <?php echo $dfno;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    Nama Member
                </td>
                <td>
                    <?php echo $fullnm;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    Voucher No
                </td>
                <td>
                    <?php echo $vchno;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    Pembayaran
                </td>
                <td>
                    <?php echo $fund;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td colspan="2" style="text-align: center;color: red;">Approved Payment & <?php echo $y;?></td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td colspan="2">
                    <form id="printTxt" name="printTxt" method="post" target="_blank" action="<?php echo site_url('be_umroh/reprint/voucher/txt');?>">
                        <input type="hidden" id="regnos" name="regnos" value="<?php echo $regno;?>"/>
                        <input id="submitss" type="submit" name="submit" class="btn btn-primary" value="Reprint Voucher Umroh"/>
                    </form>
                </td>
               
            </tr>
        </tbody>
    </table>
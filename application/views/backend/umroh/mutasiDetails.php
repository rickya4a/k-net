<?php
    if(empty($mutdetails)){
        echo "no Data";
    }else{
?>
<div class="mdet">
	<label class="control-label" for="typeahead">Data Mutasi Umroh</label>
</div>	
	<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <tr>
        <?php
        //print_r($dataum);
            foreach($dataum as $row){
                $dfno = $row->dfno;
                $fullnm = $row->fullnm;
				$tel_hp = $row->tel_hp;
                $dprtstatus = $row->depart_status;
                $postStatus = $row->post_status;
                $dprtdesc = $row->departuredesc;
                $dprtdt = $row->departuredt;
                $regno = $row->registerno;
                $postDt = $row->post_dt;
                $novac = $row->novac;  
            
	       
				$voucherno = $row->voucherno;  
            }
        ?>
                <td>Register/Voucher No</td>
                <td colspan="4"><?php echo "$regno / $voucherno";?></td>
                
            </tr>
            <tr>
                <td>ID Member</td>
                <td colspan="4"><?php echo $dfno." / ".$fullnm;?></td>
            </tr>
            <tr>
                <td>Nama Member</td>
                <td colspan="4"><?php echo $fullnm;?></td>
            </tr>
            <tr>
                <td>Contact No.</td>
                <td colspan="4"><?php echo $tel_hp;?></td>
            </tr>
            <tr>
               <td>Status Keberangkatan</td>
                <td colspan="4"><?php
                    if($dprtstatus == '0' && $postStatus == '0'){
                    	echo "Belum Berangkat";
					}
					elseif($dprtstatus == '1' && $postStatus == '1') {
					   echo "Sudah Berangkat";
					}else{
					   echo "Batal Berangkat";
					};?>
				</td>
            </tr>
			<tr>
                <td>Jadwal Keberangkatan</td>
                <td colspan="4"><?php
                        if($dprtstatus == '0' && $postStatus == '0'){
                            echo "".date ("d-m-Y h:ia",strtotime($dprtdt))." / ".$dprtdesc;
                        }elseif($dprtstatus == '1' && $postStatus == '1'){
                            echo "".date ("d-m-Y h:ia",strtotime($postDt))." / ".$dprtdesc;
                        }else{
                            echo "";
                        }
                ?></td>
            </tr>
            <tr>
                <td>Virtual Account</td>
                <td colspan="4"><?php echo "88146 - ".$novac;?></td>
            </tr>
			
    <tr style="height: 50px;text-align: center;font-size: 16px;text-shadow: none;">
        <td colspan="5">Detail Mutasi</td>
    </tr>
    <tr style="height: 30px;text-align: center;text-shadow: none;">
        
        <th>No.</th>
        <th width="5%">Action</th>
        <th>Tanggal</th>
        <th>Deskripsi</th>
        <th>Kredit</th>

    </tr>

    <?php
        $no = 1;
        $total = 0;
        foreach($mutdetails as $row){
    ?>
    
    <tr style="height: 30px;text-align: center;text-shadow: none;">    
        <td><?php echo $no;?></td>
        <td>
            <form class='printTxt' name='printMutDetail' method='post' target='_blank' action='https://www.k-net.co.id/be_umroh/mutasi/kw/txt2'>
                <input id='child' name='child' type='hidden' value=<?php echo $row->child;?>>
                <input id='submitss' name='submit' class='btn btn-success' value='Print ' type='submit'>
            </form>
        </td>
        <td><?php echo date ("d/m/Y h:ia",strtotime($row->createdt));?></td>
        <td><?php echo $row->description;?></td>
        <td style="text-align: right;">Rp. <?php echo number_format($row->total_mutasi,0,".",".");?></td>

    </tr>
        
        
    <?php
        $total += $row->total_mutasi;
        $no++;
        }
    ?>
    
    <tr style="height: 30px;text-align: center;font-size: 14px;text-shadow: none;">
        <td colspan="4">Saldo</td>
        <td style="text-align: right;font-weight: bold;">Rp. <?php echo number_format($total,0,".",".");?></td>
    </tr>
</table>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
    </div>
	<p></p>
<?php
    }
?>

<!--<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });
</script>-->
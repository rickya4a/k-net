<?php
    if(empty($regum)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th>No.</th>
        <th>No. Registrasi</th>
        <th>Tanggal</th>
        <th>ID</th>
        <th>Total DP</th>
        <th>No. Voucher</th>
        <th>No. KW</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($regum as $row){
    ?>
    
    <tr>
       
        <td><?php echo $no;?></td>
        <td><?php echo $row->regno;?></td>
        <td><?php echo $row->creatdt;?></td>
        <td><?php echo $row->dfno;?></td>
        <td><?php echo $row->tot_fund;?></td>
        <td><?php echo $row->voucherno;?></td>
        <td><?php echo $row->kwno;?></td>
        <td style="text-align: right;">Rp. <?php echo number_format($row->tot_fund,0,".",".");?>&nbsp;&nbsp;</td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<div class="mainForm">
    <div id="frmPend">
        <form class="form-horizontal" id="frmReportToWeb" name="frmReportToWeb" method="post"
        	  action="<?php echo site_url('be_umroh/listUpload/journey/xls');?>" >
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Journey Type</label>
                	<div class="controls">
                    	<select id="journey_type" name="journey_type">
                    		<option value="0">All</option>
                    		<option value="1">Umroh</option>
                    		<option value="2">Ziarah</option>
                    	</select>
                    </div>	
                    <label class="control-label" for="typeahead">Transaksi Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="date_to" name="date_to" />
                    </div>
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="be_umroh.getListUmrToWeb()"/>
                        <input type="submit" id="printTxt" name="printTxt" value="Export" class="btn btn-primary"/> 
                    </div>
                    
                </div>
            </fieldset>
        </form>
        <div id="listUmrohToWeb"></div>
    </div>
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'dd/mm/yy',
		});
	});	
</script>
<?php
    /*header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=ReportInstallmentPerBank.xls");
    header("Pragma: no-cache");
    header("Expires: 0");*/
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=ReportInstallmentPerBank.xls" );
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
?>
<table width="100%" border="1">
    <tr>
        <th colspan="6" style="font-size: 16px;">Report Setoran Cicilan K-Smart Umroh melalui Bank <?php echo $headCicilan[0]->bankDesc;?></th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
    
    <tr>
        <th>No</th>
        <th>Reg. No</th>
        <th>Member ID</th>
        <th>Member Name</th>
        <th>Virtual Account</th>
        <th>Amount</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
    
    <?php
        $no = 1;
        $totFund = 0;
        foreach($detCicilan as $list){
    ?>
    
    <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo $list->registerno;?></td>
        <td><?php echo $list->dfno;?></td>
        <td><?php echo $list->fullnm;?></td>
        <td><?php echo $list->novac;?></td>
        <td style="text-align: right;">Rp. <?php echo number_format($list->tot_fund,0,".",".");?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <?php
        $totFund += $list->tot_fund;
        $no++;
        }
    ?>
    <tr>
        <td colspan="5" style="text-align: center;">Total</td>
        <td style="text-align: right;font-weight: bold;">Rp. <?php echo number_format($totFund,0,".",".");?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <?php 
        for($i=1;$i <= 50;$i++){
    ?>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <?php }?>
</table>
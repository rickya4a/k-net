<?php
	header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=VoucherUmroh.txt");
	header("Pragma: no-cache");
    
    date_default_timezone_set("Asia/Jakarta");
	
    $tglPengambilan = date("d F Y");
    $waktu = date("d/m/y h:i:s");
	
	foreach($dtVch as $dtax) {
		
			echo "\n\n\n";
    		echo "    PT.K-Link Indonesia\n";
            echo "K-SMART UMROH CASH VOUCHER\n\n\n";
			echo "VOUCHER NO    : ".$dtax->voucherno."\n";
            echo "DATE          : ".$dtax->createdt."\n";
            echo "MEMBER CODE   : ".$dtax->dfno."\n";
            echo "NAME          : ".$dtax->fullnm."\n";
            echo "AMOUNT (RP)   : ".$dtax->tot_fund."\n";
            echo "\n\n";
            
			garisStrip();
			echo "\n";
			titleHeaderData();
			echo "\n";
			
			$totqty = 0;
	        $totdp = 0;
			foreach($dtax['detail'] as $dta2) {
					 $total_dp = $dta2->qty * $dta2->dpr;
				     $total_dp2 = number_format($total_dp,0,".",",");	
					 echo "$dta2->prdcd";
					 tmbh_spaceDetailPersonal(1,$dta2->prdcd);
					 
					 echo nama($dta2->prdnm,30);
					 tmbh_spaceDetailPersonal(2,nama($dta2->prdnm,30));
					 
					 tmbh_spaceDetailPersonal(3, $dta2->qty);
					 echo "$dta2->qty";
					 
					 tmbh_spaceDetailPersonal(5, $dta2->dpr);
					 echo number_format($dta2->dpr,0,".",",");
                     tmbh_spaceDetailPersonal(7, $dta2->dpr);
					 
					 tmbh_spaceDetailPersonal(6,$total_dp2);
					 echo $total_dp2;

					 echo "\n";
					 $totdp += $total_dp;
				     $totqty += $dta2->qty;
			}
			
			garisStrip();
			echo "\n";
			//echo "isi : $totdp";
			$space = '';
			for($v = 1;$v <= 11;$v++)
			{
			  $space .= " ";
			}
			$totDPx = number_format($totdp,0,".",",");
			$usrname = $this->session->userdata('ecomm_user');
			echo $space;
			echo "T O T A L       ";
			TotQty(20, number_format($totqty,0,".",","));
			TotQty(27, $totDPx);
			echo "\n"; 
			garisStrip();
			echo "\n\n\n";
			garisStrip3();
			echo "\n Type Payment";
			tmbh_spaceHeaderProduct(23);
			echo "Amount\n";
			//tmbh_spaceHeaderProduct(15);
			//echo "Bank\n";
			garisStrip3();
			echo "\n ".$dtax['payment'][0]->paytype." ";
			//tmbh_spaceHeaderProduct(15);
			//TotQty(16, $totDPx);
			//tmbh_spaceHeaderProduct(15);
			//echo "Sinarmas \n";
			tmbh_spaceDetailPersonal(2,$dtax['payment'][0]->paytype);
					 
			tmbh_spaceDetailPersonal(3, $totDPx);
			echo "$totDPx\n";
			echo "\n\n\n\n\n";
			garisStrip2();
			tmbh_spaceHeaderProduct(35);
			garisStrip2();
			echo "\n";
			echo "Recipient's Chop & Sign";
			tmbh_spaceHeaderProduct(32);
			echo "".$usrname." \n";
			tmbh_spaceHeaderProduct(55);
			echo "K-LINK INDONESIA";
			//$xx++;
	}
?>
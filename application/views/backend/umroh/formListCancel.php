<script>
    function getCancelUmroh(param) {

        var dfno = $("#dfno").val();
        var date1 = $("#from" +param).val();
        var date2 = $("#to" +param).val();
        //All.set_disable_button();
        $.ajax({
            url : All.get_url('sales/recruiter/detailinfo'),
            type : 'POST',
            data : {dfno : dfno, date1 : date1, date2 : date2},

            success : function(data) {

                All.set_enable_button();
                //$(All.get_active_tab() + " #hasilPencarian1").hide();
                //$(All.get_active_tab() + " #nextForm1").html(null);
                //$(All.get_active_tab() + " #nextForm1").html(data);

                $("#formCekDownline").hide();
                //$("#formCekDownline").html(null);
//                $("#nextForm1").show();
//                $("#nextForm1").html(null);
//                $("#nextForm1").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    }
</script>

<?php

    if(empty($result)){

        echo "no data!";
    }else{
        //print_r($result);
?>

<div class="overflow-auto">

    <form id="formInputReport" action="http://www.k-net.co.id/be_umroh/cancel/act2" target="_blank" method="POST">
    <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable ">

        <tr>
            <th colspan="14" align="center">Detail Data Cancel Umroh</th>
        </tr>

        <tr>
            <th style="width: 15%">Action</th>
            <th>No. Registrasi</th>
            <th>ID</th>
            <th>Nama Lengkap</th>
            <th>No. VAC</th>
            <th>Potongan Vch Umroh</th>
            <th>Potongan Staterkit</th>
            <th>Total</th>
            <th>No. Surat</th>
            <th>Keterangan</th>
            <th>Lampiran</th>
            <th>Bank</th>
            <th>No. Rekening</th>
            <th>Atas Nama</th>
        </tr>

<?php

        $no=1;
        foreach($result as $a){

            $registerno = $a->registerno;
            $dfno = $a->dfno;
            $fullnm = $a->fullnm;
            $novac = $a->novac;
            $installment_d = $a->installment_d;
            $total = $a->total;

            $vcrAmt = explode('#', $installment_d);
            if(count($vcrAmt) > 1){
                $vchUmr = $vcrAmt[0];
                $vchSK 	= $vcrAmt[1];
            }elseif(count($vcrAmt) == 1){
                $vchUmr = $vcrAmt[0];
                $vchSK 	= 0;
            }else{
                $vchUmr = 0;
                $vchSK 	= 0;
            }
?>


        <tr>
                <td align="center">
<!--                <button type="submit" class="btn btn-success" >Cetak Pdf</button>-->
                    <input id="submit" type="submit" name="submit" value="Cetak Pdf" class="btn btn-primary1" onclick="getCancelUmroh($no)" method="POST"/>
                </td>
                <td align="center"><?php echo $registerno;?></td>
                <td align="center">
                    <input type="hidden" id="dfno" name="dfno" value="<?php echo $dfno;?>">
                    <?php echo $dfno;?>
                </td>
                <td align="center"><?php echo $fullnm;?></td>
                <td align="center"><?php echo $novac;?></td>
                <td align="center"><?php echo $vchUmr;?></td>
                <td align="center"><?php echo $vchSK;?></td>
                <td align="center"><?php echo $total;?></td>
                <td><input tabindex="3" type="text" id="nosurat"<?php echo $no;?> name="nosurat" style="width: 100px;"/></td>
                <td><input tabindex="3" type="text" id="keterangan"<?php echo $no;?> name="keterangan" style="width: 100px;"/></td>
                <td><input tabindex="3" type="text" id="lampiran"<?php echo $no;?> name="lampiran" style="width: 100px;"/></td>
                <td><input tabindex="3" type="text" id="bank"<?php echo $no;?> name="bank" style="width: 100px;"/></td>
                <td><input tabindex="3" type="text" id="nama"<?php echo $no;?> name="nama" style="width: 100px;"/></td>
                <td><input tabindex="3" type="text" id="norek"<?php echo $no;?> name="norek" style="width: 100px;"/></td>
        </tr>

            <?php
            $no++; }}
            ?>


    </table>
    </form>
    </div>


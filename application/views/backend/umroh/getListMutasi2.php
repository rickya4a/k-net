<?php
    if(empty($mutasi)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th>No.</th>
        <th>ID</th>
        <th>Register No</th>
        <th>Nama</th>
        <th>Saldo Akhir</th>
        <th width="5%">Action </th>

    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($mutasi as $row){
    ?>
    
    <tr>

        <td><?php echo $no;?></td>

        <td><a href = "#" id=" <?php echo $row->id;?>" onclick="All.ajaxShowDetailonNextForm('be_umroh/mutasi/details/<?php echo $row->id;?>')"><?php echo $row->dfno;?></a></td>
        <td><?php echo $row->registerno;?></td>
        <td><?php echo $row->fullnm;?></td>
        <td style="text-align: right;">Rp. <?php echo number_format($row->tot_mutasi,0,".",".");?></td>
        <td>
            <form class='printTxt' name='printTxt' method='post' target='_blank' action='https://www.k-net.co.id/be_umroh/mutasi/kw/txt'>
                <input id='regnose' name='regnose' type='hidden' value=<?php echo $row->id;?>>
                <input id='nokw' name='nokw' type='hidden' value=<?php echo $no;?>>
                <input id='submitss' name='submit' class='btn btn-success' value='Print Mutasi' type='submit'>
            </form>
        </td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
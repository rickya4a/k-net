<?php

require APPPATH."libraries/Code128.php";
$pdf = new Code128('P', 'mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.3); // border thickness = 0.3 (maybe in e.m)

$pdf->AddPage();
$pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
$pdf->SetFont('Courier','', 9);

$pdf->Text(5, 40, 'Kepada      :  Presiden Direktur Bpk. Radzi Saleh');
$pdf->Text(5, 45, 'CC          :  SFAM. Ibu Lanny Wijaya, Purchasing Mgr. Holydia');
$pdf->Text(5, 50, 'Perihal     :  Pembatalan Peserta Umroh');
$pdf->Text(5, 55, 'Tanggal     :  '.$tanggal);
$pdf->Text(5, 60, 'Lampiran    :  ');
$pdf->Line(5, 65, 200, 65);

//$pdf->Text(75, 70, 'No : UMROH/C/'.$tgl.'/00'.$no);
$pdf->Text(75, 70, 'No : UMROH/C/'.$tgl.'/005');

$pdf->Text(5, 75, 'Dengan hormat,');
$pdf->Text(5, 85, 'Bersama ini terjadi pembatalan peserta K-Smart Umroh atas permintaan dari peserta sendiri.');
$pdf->Text(5, 90, 'Alasan yang diajukan adalah : '.$alasan);
$pdf->Text(5, 95, 'Keterangan : ');

$pdf->SetY(100);
//$pdf->SetX(3);

$x = 55;
$y = 10;

//$pdf->Cell(150,$y,"Total",1,0,'C',true);
//$pdf->Ln();
$pdf->Cell(10,$y,"No.",1,0,'C',true);
$pdf->Cell(25,$y,"ID Member",1,0,'C',true);
$pdf->Cell(35,$y,"Nama",1,0,'C',true);
$pdf->Cell(25,$y,"Tabungan",1,0,'C',true);
$pdf->Cell(25,$y,"Potongan",1,0,'C',true);
$pdf->Cell(25,$y,"Saldo",1,0,'C',true);
$pdf->Cell(25,$y,"Keterangan",1,0,'C',true);
$pdf->Ln();

if($result != null){

    $no = 1;
    foreach($result as $dt){

            $tabungan = substr($dt->installment_c, 0, 7);
            $potongan = substr($dt->installment_d, 0, 7);

            $pdf->Cell(10, $y, $no, 1, 0, 'C', true);
            $pdf->Cell(25, $y, $dt->dfno, 1, 0, 'C', true);
            $pdf->Cell(35, $y, $dt->fullnm, 1, 0, 'C', true);
            $pdf->Cell(25, $y, number_format($tabungan, 0, ',', '.'), 1, 0, 'C', true);
            $pdf->Cell(25, $y, number_format($potongan, 0, ',', '.'), 1, 0, 'C', true);
            $pdf->Cell(25, $y, number_format($dt->sisa, 0, ',', '.'), 1, 0, 'C', true);
            $pdf->Cell(25, $y, " ", 1, 0, 'C', true);

//            $pdf->Ln();
//            $pdf->Cell(70, $y, "TOTAL NOMINAL", 1, 0, 'R', true);
//            $pdf->Cell(25, $y, number_format($tabungan, 0, ',', '.'), 1, 0, 'C', true);
//            $pdf->Cell(25, $y, number_format($potongan, 0, ',', '.'), 1, 0, 'C', true);
//            $pdf->Cell(25, $y, number_format($dt->sisa, 0, ',', '.'), 1, 0, 'C', true);
//            $pdf->Cell(25, $y, "", 1, 0, 'C', true);

            $pdf->Text(5, 165, 'No rek : ' . $norek . ' (Mandiri) '.' '. $dt->bankaccnm);

            $no++;

    }
}

if($result2 != null){

    $no2 = 2;
    foreach($result2 as $dt2){


        $tabungan2 = substr($dt2->installment_c, 0, 7);
        $potongan2 = substr($dt2->installment_d, 0, 7);

        $dfno2 = $dt2->dfno;
        $fullnm2 = $dt2->fullnm;

        $tabung = number_format($tabungan2, 0, ',', '.');
        $potong = number_format($potongan2, 0, ',', '.');

        $sisa2 = number_format($dt2->sisa, 0, ',', '.');

        $pdf->Ln();
        $pdf->Cell(10, $y, $no2, 1, 0, 'C', true);
        $pdf->Cell(25, $y, $dfno2, 1, 0, 'C', true);
        $pdf->Cell(35, $y, $fullnm2, 1, 0, 'C', true);
        $pdf->Cell(25, $y, $tabung, 1, 0, 'C', true);
        $pdf->Cell(25, $y, $potong, 1, 0, 'C', true);
        $pdf->Cell(25, $y, $sisa2, 1, 0, 'C', true);
        $pdf->Cell(25, $y, " ", 1, 0, 'C', true);

    }
}

            $pdf->Ln();
            $pdf->Cell(70, $y, "TOTAL NOMINAL", 1, 0, 'R', true);
            $pdf->Cell(25, $y, "7.000.000", 1, 0, 'C', true);
            $pdf->Cell(25, $y, "4.000.000", 1, 0, 'C', true);
            $pdf->Cell(25, $y, "3.000.000", 1, 0, 'C', true);
            $pdf->Cell(25, $y, "", 1, 0, 'C', true);


//$pdf->Text(5, 150, 'Dana total tabungan umroh sebesar Rp. '.number_format($dt->sisa,0,".",",").' ('.convertAngkaKeHuruf($dt->sisa).' Rupiah )');
$pdf->Text(5, 150, 'Dana total tabungan umroh sebesar Rp. 3,000,000 (tiga juta rupiah)');

$pdf->Text(5, 155, 'tersebut mohon dapat disetorkan kembali ke :');
//$pdf->Text(5, 165, 'No rek : ');
$pdf->Text(5, 185, 'Demikian disampaikan dan terima kasih.');
$pdf->Text(25, 195, 'Hormat kami,');
$pdf->Text(150, 195, 'Mengetahui,');

$pdf->SetFont('Courier','U', 9);
$pdf->Text(18, 225, 'Luqito Masdar Hilal');
$pdf->Text(143, 225, 'Nandang Hermansyah');

$pdf->SetFont('Courier','', 9);
$pdf->Text(25, 230, 'EDP Manager');
$pdf->Text(144, 230, 'Senior IT Manager');


$title = "form_cancel_umroh.pdf";
$pdf->SetTitle($title);
$pdf->Output();

//if($month < 10){
//    $title = $year."0".$month."-".$idmember.".pdf";
//    $pdf->SetTitle($title);
//    $pdf->Output($title, 'I');
//}else{
//    $title = $year.$month."-".$idmember.".pdf";
//    $pdf->SetTitle($title);
//    $pdf->Output($title, 'I');
//}

?>
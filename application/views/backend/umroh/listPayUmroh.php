<?php
    if(empty($listPayment)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        
        <th>Reg. No</th>
        <th>Member ID</th>
        <th>Member Name</th>
        <th>Amount</th>
        <th>Create Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <?php
        //$no = 1;
        foreach($listPayment as $row){
    ?>
    
    <tr>
       
        <td><?php echo $row->registerno;?></td>
        <td><?php echo $row->dfno;?></td>
        <td><?php echo $row->fullnm;?></td>
        <td style="text-align: right;">Rp. <?php echo number_format($row->tot_fund,0,".",".");?></td>
        <td style="text-align: right;"><?php echo date("d-m-Y",strtotime($row->createdt));?></td>
        <td style="text-align: center;">
            <a class="btn btn-mini btn-info" onclick="be_umroh.approvePayment('<?php echo $row->registerno;?>','<?php echo $reg_from;?>','<?php echo $reg_to;?>','<?php echo $row->tipe_perjalanan;?>')">
            <i class="icon-ok icon-white"></i>
        </td>
    </tr>
    <?php
        //$no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
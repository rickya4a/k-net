<?php
    if(empty($dtUmroh)){
        echo "no Data";
    }else{
?>
<div id="formDtUmrohh">
    <div class="control-group"> 
        <label class="control-label" for="typeahead">Listing Data Umroh</label>
    </div>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        
        <th>Reg. No</th>
        <th>Member ID</th>
        <th>Member Name</th>
        <th>Ktp No.</th>
        <th>Create Date</th>
        <th>Hp Number</th>
        <th>Action</th>
    </tr>
    </thead>
    <?php
        //$no = 1;
        foreach($dtUmroh as $row){
    ?>
    
    <tr>
       
        <td><?php echo $row->registerno;?></td>
        <td><?php echo $row->dfno;?></td>
        <td><?php echo $row->fullnm;?></td>
        <td><?php echo $row->idno;?></td>
        <td style="text-align: center;"><?php echo date("d-m-Y",strtotime($row->createdt));?></td>
        <td><?php echo $row->tel_hp;?></td>
        <td style="text-align: center;">
            <a class="btn btn-mini btn-info" onclick="be_umroh.resendSms('<?php echo $row->registerno;?>')">
            <i class="icon-envelope icon-white"></i>
        </td>
    </tr>
    <?php
        //$no++;
        }
    ?>
    </table>
</div>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>

<!--<div class="container-fluid">
    <div class="row-fluid">
        <div class="block">
            <div id="page-stats" class="block-body collapse in">
            <form class="form-horizontal" method="post" id="frm_pch" name="frm_pch">
                <fieldset>
                    <legend><?php echo $form_info['header_form']; ?></legend>
                    
                    <label class="control-label" for="typeahead">List Purchases By</label>
                    <div class="controls">
                        <select class="span3 typeahead" id="searchs"  name="searchs">
                            <option value="">=== Select here ===</option>
                            <option value="idmember">ID Member</option>
                            <option value="orderno">Orderno</option>
                        </select>
                   </div>
                   
                    <span id="idmemb" style="display: none;">
                    <label class="control-label" for="typeahead">ID Member</label>
                        <div class="controls">
                        <input type="text" class="span3 typeahead" id="idmembers"  name="idmembers" />
                        </div>
                    </span>
                    
                    <span id="order_no" style="display: none;">
                    <label class="control-label" for="typeahead">Orderno</label>
                        <div class="controls">
                        <input type="text" class="span3 typeahead" id="ordernumber"  name="ordernumber" />
                        </div>
                    </span>                    
                        <div class="controls">
                            <input type="submit" class="btn btn-primary" onClick="load_SearchPurchase(); return false;" name="submit" value="View"/>
                        </div>
                        <div id="hasil"></div>
                </fieldset>
            </form>
            </div>
        </div>
    </div>
</div>-->
<div class="mainForm">
        <form class="form-horizontal" id="formCancelKSmart" name="formCancelKSmart" method="post">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="typeahead">Journey Type</label>
                    <div class="controls">
                        <select id="journeyType" name="journeyType">
<!--                            <option value = "0">All</option>-->
                            <option value = "1">Umroh</option>
                            <option value = "2">Ziarah</option>
                        </select>

                    </div>

                    <label class="control-label" for="typeahead">Status</label>
                    <div class="controls">
                        <select id="status" name="status">
<!--                        <option value = "all">All</option>-->
                            <option value = "0">Pending</option>
                            <option value = "1">Posted</option>
                            <option value = "2">Cancel/Reject</option>
                        </select>
                    </div>

                    <label class="control-label" for="typeahead">Transaction Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="date_to" name="date_to" />
                    </div>

                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be_umroh/cancel/act')"/>
                    </div>
                </div>
            </fieldset>
        </form>
        <div class="result">

        </div>
        <div class="nextForm1">

        </div>
    </div>
<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'dd-mm-yy',
        }).datepicker("setDate", new Date());;
    });
</script>
<style>
	.xlText {
	    mso-number-format: "\@";
	}
</style>

<?php
    /*header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=ReportInstallmentPerBank.xls");
    header("Pragma: no-cache");
    header("Expires: 0");*/
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=ReportInstallmentPerBank.xls" );
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
?>
<?php
    if(empty($detCicilan)){
        echo "no Data";
    }else{
?>
 <table border="1" style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable ">
 	
 	<?php
 	//".$headCicilan[0]->bankDesc."
 		$totIns = 10;
		$totInsHead = $totIns + ($totIns * 2) + 7 + 1;
		//echo $totInsHead;
    	echo "<tr style='height: 50px;text-align: center;font-size: 16px;text-shadow: none;'>
		        <td colspan='$totInsHead'>Report Setoran Cicilan K-Smart Umroh</td>
		        
		     </tr>
    
		     <tr style='height: 30px;text-align: center;text-shadow: none;' >
		        <th>No</th>
		        <th>Reg. Date</th>
		        <th>Reg. No</th>
		        <th>Voucher. No</th>
		        <th>Member ID</th>
		        <th>Member Name</th>
		        <th>VA No.</th>
		        <th>DP</th>
		        <th>Voucher Amt</th>
		        <th>Starterkit</th>
		        <th>DP - (Voucher + SK)</th>
		        <th>IP No</th>
		    ";
        
	        for ($i=0; $i<$totIns; $i++) {
	        	$ii = $i+1; 
				echo "<th>Inst. Date $ii</th>
					  <th>Installment $ii</th>";
			}
			
		if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
 			$dataColAdd = "";
 		}else{
 			$dataColAdd = "<th>Address</th>
 						   <th>Contact No</th>";
						   //<th>Pax</th>";
 		}
		
		echo "	<th>Total Saldo - (SK+Voucher)</th>
				<th>Total Saldo All</th>
				$dataColAdd
			  </tr>";
	    
		//detail of installment
		
        $no 			= 	1;
        $totFund 		= 	0;
		$saldoTempTot	=	0;
		$saldoTotX		=	0;
		$DPUmrTot		=	0;
		$vchUmrTot		=	0;
		$vchSKTot		=	0;
		$saldoTempTot	=	0;
		$installmentTot	=	0;
		$saldoTotTot	=	0;
		$saldoTotGrand	=	0;
        foreach($detCicilan as $list){

			$saldoPerDist	=	0;
			
			echo "<tr style='height: 30px;text-align: center;text-shadow: none;'>
		        	<td align='right'>".$no."</td>
			        <td align='left'>".$list->createdt2."</td>
			        <td align='left'>".$list->registerno."</td>
			        <td align='left'>".$list->voucherno."</td>
			        <td align='left'>".$list->dfno."</td>
			        <td align='left'>".$list->fullnm."</td>
			        <td align='left' class='xlText'>".$list->novac."</td>";
			        			
			$vcrAmt = explode('#', $list->installment_d);
			if(count($vcrAmt) > 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= $vcrAmt[1];
			}elseif(count($vcrAmt) == 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= 0;
			}else{
				$vchUmr = 0;
				$vchSK 	= 0;
			}
			//$instTD;
			
			$instAmt 	= explode('#', $list->installment_c); // data DP & installment
			$instDt 	= explode('#', $list->installment_createdt); // data DP & installment
			$DPUmr 		= $instAmt[0];
			$DPUmrTot 	= $DPUmrTot + $DPUmr;
			$saldoTemp	= $DPUmr - ($vchUmr + $vchSK);
			$saldoTot 	= $saldoTemp;
			$vchUmrTot	= $vchUmrTot + $vchUmr;
			$vchSKTot	= $vchSKTot + $vchSK;
			$saldoTempTot	= $saldoTempTot + $saldoTemp;
			//$totalPax		= $list->totalDep;
			
			echo   "<td align='right'>$DPUmr</td>
					<td align='right'>$vchUmr</td>
					<td align='right'>$vchSK</td>
					<td align='right'>$saldoTemp</td>
					<td align='left'>".$list->IPno."</td>";

			if(count($instAmt) > 1){
				$instTD = "";
				for ($i=0; $i < count($instAmt)  ; $i++) {  //iteration for installment data
					if($i > 0){
						$installment 	= $instAmt[$i];
						$installmentDt 	= $instDt[$i];
						echo "<td align='left'>$installmentDt</td>
							  <td align='right'>$installment</td>";
						//$saldoTot = $saldoTemp + $installment;
						$installmentTot = $installmentTot + $installment;
						$saldoPerDist = $saldoPerDist + $installment;
					}
				}
				
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
					echo "<td>&nbsp;</td>
						  <td align='right'>0</td>";
				}
				
			}elseif(count($instAmt) == 1){
				$installment 	= 0;
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
					echo "<td>&nbsp;</td>
						  <td align='right'>0</td>";
				}
				
			}else{
				$installment 	= 0;
			}
			
			if($dataView == "0"){ // jika alamat ingin ditampilkan maka tambah 2 kolom
	 			$dataColField = "";
	 		}else{
	 			$addr1 = $list->addr1;
				$kelurahan = $list->kelurahan;
				$kecamatan = $list->kecamatan;
				$kota = $list->kota;
				
				if($addr1 != ""){
					$addr1 = "$addr1, ";
				}else{
					$addr1 = "";
				}
				
				if($kelurahan != ""){
					$kelurahan = "$kelurahan, ";
				}else{
					$kelurahan = "";
				}
				
				if($kecamatan != ""){
					$kecamatan = "$kecamatan, ";
				}else{
					$kecamatan = "";
				}
				
				if($kota != ""){
					$kota = "$kota, ";
				}else{
					$kota = "";
				}
				
				$dataColField = "<td>$addr1$kelurahan$kecamatan$kota</td>
	 						     <td class='str'>$list->tel_hp</td>";
								 //<td align='right'>$totalPax</td>";
	 		}
			
			$saldoPerDist1	=	$saldoPerDist + $DPUmr;
			$saldoTot = $saldoTemp + $installment;
			echo   "<td align='right'>$saldoTot</td>
				    <td align='right'>$saldoPerDist1</td>
				    $dataColField
				    </tr>";
			$saldoTotTot = $saldoTotTot + $saldoTot;
			$saldoTotGrand = $saldoTotGrand + $saldoPerDist1;
				    
	        //$totFund += $list->tot_fund;
	        $no++;
        }
    ?>
    <tr style="height: 30px;text-align: center;font-size: 14px;text-shadow: none;"> 
        <td colspan="7" align="right">Total</td>
        <td style="text-align: right;font-weight: " align="right"><?php echo $DPUmrTot;?></td>
        <td style="text-align: right;font-weight: " align="right"><?php echo $vchUmrTot;?></td>
        <td style="text-align: right;font-weight: " align="right"><?php echo $vchSKTot;?></td>
        <td style="text-align: right;font-weight: " align="right"><?php echo $saldoTempTot;?></td>
        <!-- td style="text-align: right;font-weight: ">&nbsp;</td -->
        <?php 
	        for ($i=0; $i<$totIns; $i++) {
		        	$ii = $i+1; 
					/* echo "<td style='text-align: right;font-weight: '>&nbsp;</td>
						  <td style='text-align: right;font-weight: '>&nbsp</td>";
					 * 
					 */
				}
		?>    
        <td style="text-align: right;font-weight: " align="right"><?php echo $saldoTotTot;?></td>    
        <td style="text-align: right;font-weight: " align="right"><?php echo $saldoTotGrand;?></td>   
    </tr>
</table>
<?php
    }
?>
<div class="mainForm">
    <div id="frmApproval">
        <form class="form-horizontal" id="frmApp" name="frmApp" method="post">
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Register Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="reg_from" name="reg_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="reg_to" name="reg_to" />
                    </div>
                    <label class="control-label" for="typeahead">&nbsp;</label> 
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="be_umroh.getListPayUmroh()"/>
                        <!-- input tabindex="3" type="button" id="btn_reconcile" class="btn btn-primary .submit" name="save" value="Refresh List" onclick="be_umroh.getReconcile()"/ -->
                    </div>
                </div>
            </fieldset>
            <div id="listingPayUmroh"></div>
        </form>
    </div>
    
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>
<?php
    if(empty($detCicilan)){
        echo "no Data";
    }else{
?>
<form method="post" action="<?php echo site_url('be_umroh/report/printExcell');?>" target="_blank">
    <input type="hidden" name="date_from" id="date_from" value="<?php echo $date_from;?>"/>
    <input type="hidden" name="date_to" id="date_to" value="<?php echo $date_to;?>"/>
    <input type="hidden" name="banklist" id="banklist" value="<?php echo $banklist;?>"/>
   <input type="submit" id="printTxt" name="printTxt" value="Print Report" class="btn btn-primary"/> 
</form>
 <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr style="height: 50px;text-align: center;font-size: 16px;text-shadow: none;">
        <td colspan="6">Report Setoran Cicilan K-Smart Umroh melalui Bank <?php echo $headCicilan[0]->bankDesc;?></td>
    </tr>
    
    <tr style="height: 30px;text-align: center;text-shadow: none;" >
        <th>No</th>
        <th>Reg. No</th>
        <th>Member ID</th>
        <th>Member Name</th>
        <th>Virtual Account</th>
        <th>Amount</th>
    </tr>
    
    <?php
        $no = 1;
        $totFund = 0;
        foreach($detCicilan as $list){
    ?>
    
    <tr style="height: 30px;text-align: center;text-shadow: none;">
        <td><?php echo $no; ?></td>
        <td><?php echo $list->registerno;?></td>
        <td><?php echo $list->dfno;?></td>
        <td><?php echo $list->fullnm;?></td>
        <td><?php echo $list->novac;?></td>
        <td style="text-align: right;">Rp. <?php echo number_format($list->tot_fund,0,".",".");?></td>
    </tr>
    <?php
        $totFund += $list->tot_fund;
        $no++;
        }
    ?>
    <tr style="height: 30px;text-align: center;font-size: 14px;text-shadow: none;">
        <td colspan="5">Total</td>
        <td style="text-align: right;font-weight: bold;">Rp. <?php echo number_format($totFund,0,".",".");?></td>
    </tr>
</table>
<?php
    }
?>
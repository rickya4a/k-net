
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
            <tr>
                <th colspan="2" bgcolor="#lightgrey">Payment Data</th>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach($detPayment as $xxx){
                $regno = $xxx->registerno;
                $dfno = $xxx->dfno;
                $fullnm = $xxx->fullnm;
                $fund = $xxx->tot_fund;
            }
        ?>
            <tr bgcolor="#f4f4f4">
                <td>
                    Register No
                </td>
                <td>
                    <?php echo $regno;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    ID Member
                </td>
                <td>
                    <?php echo $dfno;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    Nama Member
                </td>
                <td>
                    <?php echo $fullnm;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td>
                    Pembayaran
                </td>
                <td>
                    <?php echo $fund;?>
                </td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td colspan="2" style="text-align: center;color: red;">Approved Payment</td>
            </tr>
            <tr bgcolor="#f4f4f4">
                <td colspan="2">
                    <form id="printTxt" name="printTxt" method="post" target="_blank" action="<?php echo site_url('be_umroh/printKwSk');?>">
                        <input type="button" id="back_btn" class="btn btn-primary .submit" name="back" onclick="be_umroh.backListPayUmroh()" value="Back To Listing"/>
                        <input type="hidden" name="from" id="from" value="<?php echo $from;?>"/>
                        <input type="hidden" name="to" id="to" value="<?php echo $to;?>"/>
                        <input type="hidden" id="regnos" name="regnos" value="<?php echo $regno;?>"/>
                        <input id="submitss" type="submit" name="submit" class="btn btn-primary" value="Print KW SK & Voucher"/>
                    </form>
                </td>
               
            </tr>
        </tbody>
    </table>
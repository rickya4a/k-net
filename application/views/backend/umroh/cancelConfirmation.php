<?php if (empty($res)) {
    echo setErrorMessage();
} else { ?>

<div class="table-responsive">
    <h2>Konfirmasi Pembatalan</h2>
    <form action="<?php echo site_url("be_umroh/cancel/print") ?>" method="post" target="_blank">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan='11'><?php echo $res[0]->batchno; ?></th>
                </tr>
                <tr>
                    <th>ID Member</th>
                    <th>Keterangan</th>
                    <th>Tanggal</th>
                    <th>No. Register</th>
                    <th>Bank</th>
                    <th>No. Rek</th>
                    <th>Total DP & Cicilan</th>
                    <th>Total Voucher & SK</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <?php foreach ($res as $key) { ?>
            <tbody>
                <tr>
                    <td><?php echo $key->dfno; ?></td>
                    <td><?php echo $key->ket; ?></td>
                    <td><?php echo date("d F Y H:i", strtotime($key->date)); ?></td>
                    <td><?php echo $key->registerno; ?></td>
                    <td><?php echo $key->bank; ?></td>
                    <td><?php echo $key->bankaccno; ?></td>
                    <td style="text-align: right;"><?php echo number_format($key->total_pay, "0", ".", ","); ?></td>
                    <td style="text-align: right;"><?php echo number_format($key->total_vch_sk, "0", ".", ","); ?></td>
                    <td style="text-align: right;"><?php echo number_format($key->pengembalian, "0", ".", ","); ?></td>
                </tr>
            </tbody>
            <?php }; ?>
        </table>
        <input type="hidden" name="batchno" value="<?php echo $res[0]->batchno ?>">
        <input id="savebtn" type="submit" value="Cetak PDF" class="btn btn-primary">
    </form>
</div>

<?php
}
; ?>
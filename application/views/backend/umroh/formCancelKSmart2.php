<div class="mainForm">
    <form
        class="form-horizontal"
        id="formCancelKSmart"
        name="formCancelKSmart"
        method="post"
        action="<?php echo site_url("be_umroh/cancel/act2")?>"
        target="_blank">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">ID Member</label>
                <input
                    tabindex="3"
                    type="text"
                    id="idmember"
                    name="idmember"
                    onchange="getInfoMember(this.value)"
                    style="width: 150px;"
                    required="required"/>
                <input
                    type="button"
                    class="btn btn-primary"
                    onclick="getMutation()"
                    value='Check Mutation'>
                <br>
                <label class="control-label" for="typeahead">Nama Lengkap</label>
                <input
                    tabindex="3"
                    type="text"
                    id="fullname"
                    name="fullname"
                    style="width: 300px;"
                    required="required"/>
                <br>
                <label class="control-label" for="typeahead">Alasan</label>
                <input
                    tabindex="3"
                    type="text"
                    id="alasan"
                    name="alasan"
                    style="width: 400px;"
                    required="required"/>
                <br>
                <label class="control-label" for="typeahead">Tanggal</label>
                <div class="controls">
                    <input type="text" class="dtpicker typeahead" id="tanggal" name="tanggal"/>
                </div>
                <label class="control-label" for="typeahead">No. Rek</label>
                <input
                    tabindex="3"
                    type="text"
                    id="norek"
                    name="norek"
                    style="width: 150px;"
                    required="required"/>
                <br>
				<label class="control-label" for="typeahead">Nama Rek.</label>
                <input
                    tabindex="3"
                    type="text"
                    id="nama_rekening"
                    name="nama_rekening"
                    style="width: 150px;"
                    required="required"/>
				<br>
                <label class="control-label" for="typeahead">Bank</label>
                <div class="controls">
                    <select
                        id="banklist"
                        name="banklist"
                        class="span3"
                        tabindex="3"
                        required="required">
                        <option value="" disabled="disabled" selected="selected">--Pilih Bank--</option>
                        <?php
                        foreach($bankList as $list){
                            echo "<option value = \"$list->bankCode\">".$list->bankDesc."</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="controls" id="inp_btn">
                    <!-- <input tabindex="3" type="button" id="btn_input_user" class="btn
                    btn-primary .submit" name="save" value="Search"
                    onclick="All.ajaxFormPost(this.form.id,'be_umroh/cancel/act')"/>-->
                    <!-- <input id="printPdf" class="btn btn-primary .submit" type="submit"
                    value="Cetak ke PDF" name="printPdf"> -->
                    <input type="button" value="&#x2b Add" class="btn btn-success" id="add">
                    <input
                        class="btn btn-warning"
                        type="button"
                        onclick="All.ajaxFormPost2(this.form.id,'be_umroh/cancel/act')"
                        value="View List">
                </div>
            </div>
        </fieldset>
    </form>
    <div class="addMutasi" style="display: none;">
        <div class="table-responsive">
            <h3>List Data Pembatalan</h3>
            <form id="saveCancel" action="" method="">
                <table class="table table-bordered" id="myTable">
                    <thead>
                        <tr>
                            <th>No. Reg.</th>
                            <th>ID Member</th>
                            <th>Total DP & Cicilan</th>
                            <th>Total Voucher & Starter</th>
                            <th>Sisa</th>
                            <th>Alasan</th>
                            <th>Tanggal</th>
                            <th>Nama Bank</th>
                            <th>No. Rekening</th>
							<th>Nama Rekening</th>
                            <th>Pilih</th>
                        </tr>
                    </thead>
                    <tbody id="data">
                        <!-- data goes here -->
                    </tbody>
                </table>
                <input
                    id="savebtn"
                    value="&#10003 Simpan"
										type="hidden"
                    class="btn btn-primary"
                    onclick="saveCancel()">
                <input
                    id="delete-row"
                    value="&#10005 Batal"
										type="hidden"
                    class="btn btn-danger">
            </form>
        </div>
    </div>
    <div class="hasil">
        <!-- <div class="nextForm1">-->
        <!-- </div>-->
    </div>
</div>
<script>
    $(document).ready(function () {
        $(All.get_active_tab() + " .dtpicker")
            .datepicker(
                {changeMonth: true, numberOfMonths: 1, dateFormat: 'dd-mm-yy'}
            )
            .datepicker("setDate", new Date());
    });

    function getInfoMember(value) {
        All.set_enable_button();
        $.ajax({
            url: All.get_url('be_umroh/cancel/getmemberinfo/') + value,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                All.set_enable_button();
                var arrayData = data.arrayData;
                if (data.response == "true") {
                    if (arrayData[0].depart_status == "0" && arrayData[0].approval_status == "1") {
                        $(All.get_active_tab() + " #fullname").val(arrayData[0].fullnm);
                        $(All.get_active_tab() + " #norek").val(arrayData[0].bankaccno);
                        $(All.get_active_tab() + " #banklist").val(arrayData[0].banknm);
                        $("#fullname, #alasan, #tanggal, #norek, #banklist, #printpdf").prop(
                            'disabled',
                            false
                        );
                    } else if (arrayData[0].depart_status == '0' && arrayData[0].approval_status == "0") {
                        alert('Pembayaran belum approved...');
                        $("#fullname, #alasan, #tanggal, #norek, #banklist, #printPdf, #add").prop(
                            'disabled',
                            true
                        );
												All.set_enable_button();
                    } else if (arrayData[0].depart_status == '1') {
                        alert('Member sudah melakukan keberangkatan...');
                        $("#fullname, #alasan, #tanggal, #norek, #banklist, #printPdf, #add").prop(
                            'disabled',
                            true
                        );
												All.set_enable_button();
                    } else if (arrayData[0].depart_status == '2') {
                        alert('Member sudah melakukan pembatalan...');
                        $("#fullname, #alasan, #tanggal, #norek, #banklist, #printPdf, #add").prop(
                            'disabled',
                            true
                        );
												All.set_enable_button();
                    }
                } else {
                    alert('Data tidak ditemukan')
                }
            }
        })
    }

    function getMutation() {
        let idmember = document
            .getElementById('idmember')
            .value;
        $.ajax({
            url: All.get_url('be_umroh/cancel/getmutation/') + idmember,
            type: 'GET',
            success: function (data) {
                All.set_enable_button();
                $(All.get_active_tab() + " .hasil").html(null);
                $(All.get_active_tab() + " .hasil").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' + xhr.status);
                All.set_enable_button();
            }
        })
    }

    /**
     * @Author: Ricky
     * @Date: 2019-02-01 17:19:56
     * @Desc: form validation for adding member to cancel
     * this feature is under dev
     * ** Known issues:
     *      Requirements field don't show any messages
     */
    var idmember = document
        .getElementById('idmember')
        .value;
    $(document).ready(function () {
        $('#add').on('click', function (event) {
            var isvalidate = $("#formCancelKSmart")[0].checkValidity();
            if (isvalidate) {
			     All.set_disable_button();
                event.preventDefault();
                All.get_wait_message();
                $.post(All.get_url('be_umroh/cancel/addmutation/'),
                $(All.get_active_tab() + " #formCancelKSmart")
                .serialize(), function (data) {
                    All.set_enable_button();
                    $(All.get_active_tab() + " #data").append("<tr>" + data + "</tr>");
                    $("#delete-row").prop("type", "button");
                    $("#savebtn").prop("type", "button");
                    $(".addMutasi").removeAttr('style');
                })
                .fail(function () {
                    alert("Error requesting page");
                    All.set_enable_button();
                });
            } else {
                alert('data tidak lengkap')
            }
        });
    });

    function saveCancel() {
        All.set_disable_button();
        $
            .post(All.get_url('be_umroh/cancel/savemutation'), $(
                All.get_active_tab() + " #saveCancel"
            ).serialize(), function (data) {
                All.set_enable_button();
                $(All.get_active_tab() + " .mainForm").hide();
                All.clear_div_in_boxcontent(".nextForm1");
                $(All.get_active_tab() + " .nextForm1").html(data);
            })
            .fail(function () {
                alert("Error requesting page");
                All.set_enable_button();
            });
    }

    $("#delete-row").click(function () {
        $("table tbody")
            .find('input[name="record"]')
            .each(function () {
                if ($(this).is(":checked")) {
                    $(this)
                        .parents("tr")
                        .remove();
                }
            });
    });
</script>
<?php

require APPPATH."libraries/Code128.php";
$pdf = new Code128('P', 'mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$pdf->SetDrawColor(0,0,0);
$pdf->SetLineWidth(.3);

$pdf->AddPage();
$pdf->Image("assets/images/logo.jpg",9,5, 85, 25);
$pdf->SetFont('Courier','', 9);

$pdf->Text(9, 40, 'Kepada      :  Presiden Direktur Bpk. Radzi Saleh');
$pdf->Text(9, 45, 'CC          :  SFAM. Ibu Lanny Wijaya, Purchasing Mgr. Holydia');
$pdf->Text(9, 50, 'Perihal     :  Pembatalan Peserta Umroh');
$pdf->Text(9, 55, 'Tanggal     :  '.date("d/M/Y", strtotime($result[0]->date)));
$pdf->Text(9, 60, 'Lampiran    :  1 lembar');
$pdf->Line(9, 65, 200, 65);

//$pdf->Text(75, 70, 'No : UMROH/C/'.$tgl.'/00'.$no);
$pdf->Text(75, 70, 'No : '.$result[0]->batchno);

$pdf->Text(9, 75, 'Dengan hormat,');
$pdf->Text(9, 85, 'Bersama ini terjadi pembatalan peserta K-Smart Umroh atas permintaan dari peserta sendiri.');
$pdf->Text(9, 90, 'Alasan yang diajukan adalah : '.$result[0]->ket);
// $pdf->Text(9, 95, 'Keterangan : ');

$pdf->SetY(100);
//$pdf->SetX(3);

$x = 55;
$y = 8;

//$pdf->Cell(150,$y,"Total",1,0,'C',true);
//$pdf->Ln();
$pdf->Cell(10,$y,"No.",1,0,'C',true);
$pdf->Cell(25,$y,"ID Member",1,0,'C',true);
$pdf->Cell(60,$y,"Nama",1,0,'C',true);
$pdf->Cell(25,$y,"DP + Cicilan",1,0,'C',true);
$pdf->Cell(25,$y,"Voucher + SK",1,0,'C',true);
$pdf->Cell(25,$y,"Saldo",1,0,'C',true);
$pdf->Ln();

if($result != null){

    $no = 1;
	$totalTabungan = 0;
	$totalPotongan = 0;
	$totalRefund = 0;
    foreach($result as $dt){

            $tabungan = $dt->total_pay;
            $potongan = $dt->total_vch_sk;
			$refund = $dt->pengembalian;

            $pdf->Cell(10, $y, $no, 1, 0, 'C', true);
            $pdf->Cell(25, $y, $dt->dfno, 1, 0, 'C', true);
            $pdf->Cell(60, $y, $dt->fullnm, 1, 0, 'C', true);
            $pdf->Cell(25, $y, number_format($tabungan, 0, ',', '.'), 1, 0, 'R', true);
            $pdf->Cell(25, $y, number_format($potongan, 0, ',', '.'), 1, 0, 'R', true);
			$pdf->Cell(25, $y, number_format($dt->pengembalian, 0, ',', '.'), 1, 0, 'R', true);
			$pdf->Ln();
            $no++;
			$totalTabungan += $tabungan;
			$totalPotongan += $potongan;
			$totalRefund += $refund;
    }
            $pdf->Text(9, 165, 'No rek : ' . $result[0]->bankaccno . ' '. $result[0]->bank . ' a/n '. $result[0]->nama_rekening);
}
            $pdf->Ln();
            $pdf->Cell(95, $y, "TOTAL NOMINAL", 1, 0, 'R', true);
            $pdf->Cell(25, $y, number_format($totalTabungan, 0, ',', '.'), 1, 0, 'R', true);
            $pdf->Cell(25, $y, number_format($totalPotongan, 0, ',', '.'), 1, 0, 'R', true);
            $pdf->Cell(25, $y, number_format($totalRefund, 0, ',', '.'), 1, 0, 'R', true);
$pdf->Text(9, 150, 'Dana total tabungan umroh sebesar Rp. '.number_format($totalRefund, 0, ',', '.').',-');
$pdf->Text(9, 155, 'tersebut mohon dapat disetorkan kembali ke :');
$pdf->Text(9, 185, 'Demikian disampaikan dan terima kasih.');
$pdf->Text(25, 195, 'Mengetahui,');

$pdf->SetFont('Courier','U', 9);

$pdf->Text(18, 225, 'Nandang Hermansyah');

$pdf->SetFont('Courier','', 9);

$pdf->Text(19, 230, 'Senior IT Manager');


$title = "Report_".$result[0]->batchno.".pdf";
$pdf->SetTitle($title);
$pdf->Output();
?>
<style>
	.control-label {
		width: 150px;
		text-align: left;
	}
</style>
<div class="formInstallments">
    <form class="form-horizontal" id="formCicilan" method="post">
        <div class="control-group">
            <label class="control-label" for="typeahead" style="">Register No</label>
            <div class="controls">  
	       	  <input tabindex="1" style="text-transform:uppercase" placeholder="contoh : U0000001" class="uppercase span7" type="text" id="regnos" name="regnos" onchange="be_umroh.get_registerno_info(this.value)" />
            </div>
            
            <label class="control-label" for="typeahead" >ID Member</label>
            <div class="controls">  
	       	  <input style="text-transform:uppercase" class="uppercase span7" type="text" id="idmember" name="idmember" readonly="yes"/>
            </div>
            
            <label class="control-label" for="typeahead" >Nama Member</label>
            <div class="controls">  
	       	  <input style="text-transform:uppercase" class="uppercase span7" type="text" id="nmmember" name="nmmember" readonly="yes"/>
            </div>
            
            <label class="control-label" for="typeahead" >Paket K-SMART</label>
            <div class="controls">  
	       	  <input style="text-transform:uppercase" class="uppercase span7" type="text" id="pktUmroh" name="pktUmroh" readonly="yes" />
            </div>
            
            <label class="control-label" for="typeahead" >Saldo Tabungan</label>
            <div class="controls">  
	       	  <input style="text-transform:uppercase" class="uppercase span7" type="text" id="saldo" name="saldo" readonly="yes"/>
            </div>
            
            <label class="control-label" for="typeahead" >Cicilan</label>
            <div class="controls">  
	       	  <input tabindex="2" style="text-transform:uppercase" placeholder="* tanpa titik (Contoh 1500000)" class="uppercase span7" type="text" id="amtUmroh" name="amtUmroh"/>
            </div>
            
            <label class="control-label" for="typeahead" >Payment Reff</label>
            <div class="controls">  
	       	  <input tabindex="3" style="text-transform:uppercase" class="uppercase span7" type="text" id="paymentreff" name="paymentreff"/>
            </div>
            
            <label class="control-label" for="typeahead" >Bank</label>
            <div class="controls">  
	       	  <select id="banklist" name="banklist" class="span7" tabindex="4">
                <?php
                    foreach($bankList as $list){
                        echo "<option value = \"$list->bankCode\">".$list->bankDesc."</option>";    
                    }
                ?>
              </select>
            </div>
            <br />
            <div class="controls"  id="inp_btn">
                <input tabindex="20" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Approve" onclick="be_umroh.saveInstallment()" />
                <input type="reset" class="btn btn-reset" value="Reset" />
          </div>

			
               
               
            
		  
        </div>
    </form>
	

    <div id="result"></div>
		<div id="kwprint" hidden="true">
					<td colspan="2">
                    <form id="printTxt" name="printTxt" method="post" target="_blank" action="http://www.k-net.co.id/be_umroh/print/kw/txt">
						<input id="regnose" name="regnose" type="hidden">                       
                        <input id="submitss" name="submit" class="btn btn-primary" value="Print Kwitansi" type="submit">
                    </form>
                </td>
		</div>
	
</div>
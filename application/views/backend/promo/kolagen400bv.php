<style>
	.idnm {
		width: 180px;
	}
	
	.nm {
		width: 350px;
	}
</style>
<div class="mainForm">
	<form class="form-horizontal" id="kolagen400bv">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Check BV Bns Period </label>
				<div class="controls">
					<select tabindex="1" id="bnsperiod" name="bnsperiod">
						<?php
						  foreach($period as $dra) {
						  	echo "<option value=\"$dra->period\">$dra->period</option>";
						  }
						?>
					</select>					
					
				</div>
				<label class="control-label" for="typeahead">ID Stockist</label>
				<div class="controls">
					<input type="text" class="idnm" id="idstk" name="idstk" tabindex="2" onchange="be_promo.checkDataStockist(this.value)" />
					<input type="text" class="nm" id="loccdnm" placeholder="Nama Stockist" readonly="readonly" name="loccdnm" />
					<input type="text" readonly="readonly" id="pricecode" name="pricecode" placeholder="Pricecode" value="" />					
				</div>
				<label class="control-label" for="typeahead">ID Member</label>
				<div class="controls">
					<input type="text" class="idnm"  id="idmember" name="idmember" tabindex="3" onchange="be_promo.getDataRedemp()" />					
					<input type="text" class="nm" id="membernm" placeholder="Nama Member" readonly="readonly" name="membernm" 	/>
					<input type="text" readonly="readonly" id="pbv" name="pbv" placeholder="BV" value="" />
				</div>
				<!--<label class="control-label" for="typeahead">Order No</label>
				<div class="controls">
					<input type="text" class="idnm"  id="orderno" name="orderno" tabindex="4" onchange="be_promo.checkDoubleOrderNo(this.value)" />					
					
				</div>-->
				<label class="control-label" for="typeahead">Max Qty</label>
				<div class="controls">
					<input type="text" class="span5" readonly="readonly"  id="total_remain" name="total_remain" placeholder="Total produk yang bisa di klaim" />					
					
					<input type="hidden"  id="sc_dfno" name="sc_dfno" value="" />
					<input type="hidden"  id="sc_co" name="sc_co" value="" />
					<input type="hidden"  id="total_redemp" name="total_redemp" value="" />
					<input type="hidden" id="costkname" name="costkname" />
				</div>
				
			</div>
			<!-- end control-group -->
		</fieldset>

		
		<table width="100%" class="table table-bordered table-striped">
		 	<thead>
		 		<tr>
		 			<th colspan="6">Detail Produk</th>
		 		</tr>
		 		<tr>
		 			<th width="10%">Kode Produk</th>
		 			<th>Nama Produk</th>
		 			<th width="6%">Qty</th>
		 			<th width="15%">DP</th>
		 			<th width="10%">BV</th>
		 			<th width="15%">Total BV</th>
		 		</tr>
		 	</thead>
		 	<tbody id="addRow">
		 		<tr>
		 			<td><input type="text" readonly="readonly" class="span20" id="prdcd1" name="prdcd[]" onchange="be_promo.checkDataProduct('1')"  /></td>
		 			<td><input readonly="readonly" type="text" class="span20" id="prdnm1" name="prdnm[]" /></td>
		 			<td><input type="text" tabindex="5" class="span20" id="qty1" name="qty[]" onkeyup="be_promo.checkPrdMaxQty('1')" /></td>
		 			<td><input readonly="readonly" type="text" class="span20" id="dp1" name="dp[]" /></td>
		 			<td><input readonly="readonly" type="text" class="span20" id="bv1" name="bv[]" /></td>
		 			<td><input readonly="readonly" type="text" class="span20" id="total_dp1" name="total_dp[]" /></td>
		 		</tr>
		 	</tbody>
		 	
		</table>
		<table>
			<tr>
				<td>Total DP&nbsp;</td>
				<td><input type="text" readonly="readonly" name="tdp" id="tdp" /></td>
				<td>&nbsp;Payment&nbsp;</td>
				<td>
					<select id="payType" name="payType">
						<?php
						foreach($listPayType as $dta) {
							echo "<option value=\"$dta->id\">$dta->description</option>";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Total BV&nbsp;</td>
				<td><input type="text" readonly="readonly" name="tbv" id="tbv" /></td>
				<td>&nbsp;Nominal&nbsp;</td>
				<td><input type="text" readonly="readonly" id="nominal" name="nominal" /></td>
			</tr>
			<tr>
				<td colspan="4"><input tabindex="6" class="btn btn-mini btn-primary span20" type="button" value="Save" onclick="be_promo.saveKolagenRedemp()" /></td>
			</tr>
		</table>
		
	</form>
	<div class="result"></div>
</div><!--/end mainForm-->

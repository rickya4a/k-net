<?php
	header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=summaryPromo.txt  ");
	header("Pragma: no-cache");
    
    date_default_timezone_set("Asia/Jakarta");
    $waktu = date("d/m/y h:i:s");
    
    $promo = "CALENDAR";
    $a=0;
    foreach($headSumm as $row){
        echo "\n\n\n";
  		echo "    PT.K-Link                    SUMMARY PROMO $promo \n\n\n";
        echo "ID Stockist           : ".$row->loccd ." / ".$row->fullnm."\n";
        echo "Address               : ".$row->addr1 .",".$row->addr2."\n";
        echo "Address2              : ".$row->addr3."\n";
        echo "Phone No / Mobile No  : ".$row->tel_hm.",".$row->tel_hp."\n";
        echo "Promo                 : ".$promo."\n";
        echo "Print Date            : ".$waktu."\n";
        echo "\n\n";
        garisStripDO(12);
        echo "\n";
        titleHeaderPromo();
        echo "\n";
        
        $no = 1;
        $totQtyy = 0;
        foreach($detSumm as $list){
            if($list->idstk == $row->loccd){
                if(strlen($list->nmmember) > 20){
                    $nmmember = substr($list->nmmember, 0, 20);
                 }
                 else
                 {
                    $nmmember = $list->nmmember;
                 }
                 
                 if(strlen($list->prdnm) > 15){
                    $prdnm = substr($list->prdnm, 0, 15);
                 }else{
                    $prdnm = $list->prdnm;
                 }
                
                echo "$no";
                tmbh_spaceDetailDO(2,$no,5);
                echo "".$list->orderno."";
                tmbh_spaceDetailDO(3,$list->orderno,14);
                echo "".$list->id_memb."";
                tmbh_spaceDetailDO(2,$list->id_memb,14);
                
                 echo $nmmember;
                 tmbh_spaceDetailDO(4,$nmmember,22);
                 echo $list->prdcd;
                 tmbh_spaceDetailDO(5,$list->prdcd,11);
                 echo $prdnm;
                 tmbh_spaceDetailDO(6,$list->prdcd,3);
                 tmbh_spaceDetailDO(7,$list->qty,3);
                 echo $list->qty;
                 
                 echo "\n";
                 $no++;
                 $totQtyy += $list->qty;
            }
            
        }
       
        garisStripDO(12);
        //tmbh_spaceHeader(20);
        echo "\n                  T O T A L      ";
        TotQty(51, $totQtyy);
        echo "\n";
        garisStripDO(12);
        echo "\n";
        echo "\n\n\n\n";
        echo "--------------------\n";
        echo "Authorized Signature";
        echo "\n";
        
       if($a == 0)
        {
            //echo "\n";
            echo "\f";
            
        }
        else
        {
           $a++;
        } 
    } 
 ?>
<?php
  if(!empty($sess_exp)) {
  	   setErrorMessage($sess_exp);
  } else {
?>
<div class="mainForm">
	<form class="form-horizontal" id="formPromoConfig">
	   
       <div class="control-group"> 
       	  
       	 
          <label class="control-label">ID Promo</label>  
            <div class="controls">
              <input tabindex="2"  class="required uppercase span5	setReadOnly" type="text" id="id" name="id" onchange="All.checkDoubleInput('db1/get/id/from/promo_period/','id',this.value)" />
            </div>
          
          <label class="control-label">Promo Name</label>  
            <div class="controls">
              <input tabindex="3"  class="uppercase span5" type="text" id="name" name="name" />
            </div>
          <label class="control-label">Maximum Discount</label>  
            <div class="controls">
              <input tabindex="4"  class="uppercase span5" type="text" id="max_discount" name="max_discount" />
            </div>
         
           <label class="control-label"><font color="red">&nbsp;*</font>Status</label>  
            <div class="controls">
              <select tabindex="6"  class="form-list required" id="status" name="status">
	            	<option value="1">Active</option>
					<option value="0">Inactive</option>
	          </select> 
            </div> 
            <label  class="control-label"><font color="red">&nbsp;*</font>Period Start Date</label>
            <div class="controls">
	            <input tabindex="7" type="text"  class="dtpicker typeahead" id="period_start" name="period_start" value="<?php echo $curdate; ?>" >  
    	    </div>
    	    
    	    <label  class="control-label"><font color="red">&nbsp;*</font>Period End Date</label>
            <div class="controls">
	            <input tabindex="7" type="text"  class="dtpicker typeahead" id="period_end" name="period_end" value="<?php echo $curdate; ?>" >  
    	    </div>
    	  
           <div class="controls"  id="inp_btn">
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Save" onclick="All.updateFormData('be/promo/config/save','formPromoConfig','be/promo/config/list')" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View List Promo" onclick="All.getListData('be/promo/config/list')" />
          </div>  
          <div class="controls"  id="upd_btn" style="display: none;"> 
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Update" onclick="All.updateFormData('be/promo/config/update','formPromoConfig','be/promo/config/list')" />
            <input id="cancelupd" class="btn btn-danger" type="button" onclick="All.cancelUpdateForm()" value="Cancel Update">
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View List Promo" onclick="All.getListData('be/promo/config/list')" />
          </div>                                               
        </div>  
	   <div class="result">
	   	<?php
	   	echo $view;
		?>
	   </div>	
	</form>
</div>
<script>
  $(document).ready(function($){
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
  });
</script>
<?php
   }
?>
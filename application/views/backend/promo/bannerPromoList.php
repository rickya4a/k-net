<?php
	if($listPromo != null) {
	//print_r($listPromo);	 
?>
 <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th colspan="5">List Banner Promo</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Banner Promo Name</th>
			<th>Status</th>
			<th>Period</th>
			<th>&nbsp;</th>		
		</tr>
	</thead>
	<tbody>
		<?php
		  $i = 1;
		  foreach($listPromo as $dtax) {
		  	echo "<tr>";
			echo "<td align=right>$i</td>";
			//echo "<td align=center>$dtax->id</td>";
			echo "<td>$dtax->promo_name</td>";
			echo "<td align=center>$dtax->status</td>";
			echo "<td align=center>$dtax->month - $dtax->year</td>";
			$ondelete = "All.deleteFormData('product/delete/', '$dtax->id', 'product/list')";
                $arr = array(
				    "update" => "be_promo.getUpdateBannerPromo('$dtax->id')",
				    //"delete" => $ondelete
				);
                echo btnUpdateDelete($arr);  
			echo "</tr>";  
			$i++;
		  }
		?>
	</tbody>
</table>

<?php
	} else {
		echo setErrorMessage("No record found");
	}
?>
<script type="text/javascript">
$(document).ready(function() 
{
	All.set_datatable();
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<?php
    if(empty($promo)){
        setErrorMessage();
        //echo "data tidak ada!!!";
    }else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="8" align="center">Detail Data Promo</th>
    </tr>

    <tr>
        <th style="width: 15%">ID Member</th>
        <th>Nama Lengkap</th>
        <th>Nama Stokies</th>
        <th>No. Voucher</th>
        <th>Lokasi Stokies</th>
        <th>Tanggal Klaim</th>
        <th>Periode Bonus</th>
        <th>Action</th>
    </tr>

    <?php
    $no=1;
    foreach($promo as $a){

        $dfno = $a->dfno;
        $fullnm = $a->fullnm;
        $nmSTK = $a->nmSTK;
        $VoucherNo = $a->VoucherNo;
        $bnsperiod = $a->bnsperiod;
        $b = $bnsperiod;
        $bnsprd = date("Y-d-m", strtotime($b));
        $loccd = $a->loccd;
        $claim_date = $a->claim_date;
        ?>

    <tr>
        <td align="center"><?php echo $dfno;?></td>
        <td align="center"><?php echo $fullnm;?></td>
        <td align="center"><?php echo $nmSTK;?></td>
        <td align="center"><?php echo $VoucherNo;?></td>
        <td align="center"><?php echo $loccd;?></td>
        <td align="center"><?php echo $claim_date;?></td>
        <td align="center">
            <input type="hidden" id="bnsperiod<?php echo $no;?>" value="<?php echo $bnsprd;?>">
            <?php echo $bnsperiod;?>
        </td>
        <td align="center"><input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" value="Lihat Detail" onclick="javascript:be_trans.getDetailBonusAyu(<?php echo $no;?>)" href="#" /></td>
    </tr>

        <?php
        $no++; }}
        ?>

</table>

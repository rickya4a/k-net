<?php
    if(empty($sumPromo)){
        echo setErrorMessage();
    }else{
        
    
?>
<div class="overflow-auto">
   <!-- <form id="summPromo" method="post" >-->
   <input type='submit' id='printTxt' name='printTxt' value='Print Summary' class='btn btn-success'/><br />
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            	
            <tr >
                <th colspan="6" >List Summary Promo Calendar</th>
             </tr>
                <tr>
                    <th><input type="checkbox" name="checkall" onclick="All.checkUncheckAll(this);"/></th>
                    <th>No</th>
                    <th>ID Stockist</th>
                    <th>PRODUCT</th>
                    <th>PROD NAME</th>
                    <th>Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
					$totQty = 0;
                    foreach($sumPromo as $row){
                        
                        echo "<tr>
                                <td align=\"center\"><input type=\"checkbox\" id=\"ceks[]\" name=\"ceks[]\" value=\"$row->idstk\"></td>
                                <td align='center'>".$no."</td>
                                <td><a href=\"#\" id=\"$row->idstk\" onclick=\"All.ajaxShowDetailonNextForm('be/promo/details/$row->idstk/$from_date/$to_date')\">".$row->idstk."</a></td>
                                <td>".$row->prdcd."</td>
                                <td>".$row->prdnm."</td>
                                <td align='center'>".$row->qty."</td>                              
                             </tr>";
                             $no++;
                        $totQty += $row->qty;
                    }
                ?>
                <!--<tr>
                <td colspan="6"></td>
                </tr>-->
            </tbody>
        </table>        
    <!--</form>-->
    <br />
</div>
<?php } ?>
<script type="text/javascript">
$(document).ready(function() 
{
	All.set_datatable();
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
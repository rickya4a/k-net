<div class="row-fluid">
<div class="span12">
<span id="prod_gen">
<form id="generatesubs22" method="post" >
        <table width="92%" class="table table-striped table-bordered bootstrap-datatable datatable" align="center">
            <thead>
    		  <tr bgcolor="#f4f4f4">
                <th>NO</th>
    			<th>PROD CODE</th>
                <th>PROD NAME</th>
                <th>QTY</th>
                <th>DP</th>
                <th>TOTAL DP</th>
    		  </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            $totdp = 0;
            $totdp1 = 0;
            foreach($groupprod as $row)
            {
                $totdp = $row->qty * $row->dp;
                //$trxdate = date('d/m/Y', strtotime($row->etdt));
                echo "
                <tr>
                    <td>".$no++."</td>
                   <td align=\"right\">$row->prdcd</td>
                   <td>$row->prdnm</td>
                   <td><div align=right>".number_format($row->qty,0,"",",")."</div></td>
                   <td><div align=right>".number_format($row->dp,0,"",",")."</div></td>
                   <td><div align=right>".number_format($totdp,0,"",",")."</div></td>
                </tr>";
                $totdp1 += $totdp;
            }
            ?>
            <tr>
                <td colspan="5">Total</td>
                <td><?php echo "<div align=right>".number_format($totdp1,0,"",",")."</div>";?></td>
            </tr>
            <tr>
                <td colspan="6"><b>Payment Detail</b></td>
            </tr>
            <tr>
            	<?php
				    $no2 = 1;
				    $payamt = 0;
				    $payamt1 = 0;
				    foreach($grouppay as $pay){
				    	$payamt = $pay->payamt;
				        echo "
				        <tr>
				            <td>".$no2++."
				           <td colspan=\"4\" align=\"right\">$pay->description</td>
				           <td ><div align=right>".number_format($pay->payamt,0,"",",")."</div></td>
				        </tr>";
				        $payamt1 += $payamt;
				    }
			    ?>
            </tr>
            <tr>
                <td colspan="5">Total Payment</td>
                <td><?php echo "<div align=right>".number_format($payamt1,0,"",",")."</div>";?></td>
            </tr>
            
            <tr><td colspan="6">
            <input type="button" class="btn btn-success" name="back" value="Back" onclick="All.back_to_form(' .nextForm1', ' .mainForm')"  />&nbsp;&nbsp;
            <!-- <input type="submit" class="btn btn-success" name="submit" value="Generate"/>-->
            <input type="button" class="btn btn-success" onClick="be_promo.saveGenerate();" name="submit" value="Generate" id="checkss"/>
            </td></tr>
            </tbody>
        </table>
        <input type="hidden" name="subs" value="<?php echo $typess;?>"/>
        <input type="hidden" name="bonusperiod" value="<?php echo $bnsperiod;?>"/>
        <?php
        
        for($i=0;$i < count($cek);$i++)
        {
            echo "<input type=hidden name=trcd[] value=$cek[$i]>";
        }
        ?>
    </form>
</span>
</div>
</div>

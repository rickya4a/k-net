<style>
	.idnm {
		width: 180px;
	}
	
	.nm {
		width: 350px;
	}
</style>
<div class="mainForm">
	<form class="form-horizontal" id="kolagen400Generate">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Check BV Bns Period </label>
				<div class="controls">
					<select tabindex="1" id="bnsperiod" name="bnsperiod">
						<?php
						  foreach($period as $dra) {
						  	echo "<option value=\"$dra->period\">$dra->period</option>";
						  }
						?>
					</select>					
					
				</div>
				<label class="control-label" for="typeahead">ID Stockist</label>
				<div class="controls">
					<input type="text" class="idnm" id="idstk" name="idstk" tabindex="2" onchange="be_promo.checkDataStockist(this.value)" />
					<input type="text" class="nm" id="loccdnm" placeholder="Nama Stockist" readonly="readonly" name="loccdnm" />					
				</div>
				
				<label  class="control-label"><font color="red">&nbsp;*</font>Trx Date</label>
	            <div class="controls">
	            	<?php
	            	$curdate = date("Y-m-d");
	            	?>
		            <input tabindex="3" type="text"  class="dtpicker typeahead" id="tgl_trans_f" name="from" value="<?php echo $curdate; ?>" >  
		            <input tabindex="4" type="text"  class="dtpicker typeahead" id="tgl_trans_t" name="to" value="<?php echo $curdate; ?>" >
	    	    </div>
				<div class="controls"  id="inp_btn">
					<input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit"
					onclick="be_promo.getListRedempToGenerate()" />
					<input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
				</div>
				
			</div>
			<!-- end control-group -->
		</fieldset>

		
		
		
	</form>
	<div class="result"></div>
</div><!--/end mainForm-->
<script>
  $(document).ready(function($){
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
  });
</script>
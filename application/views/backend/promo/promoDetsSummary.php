<?php
    if(empty($detSumm)){
        echo setErrorMessage("No result found");
    }else{
?>
    
    <table style="width: 100%;border: 1;" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr><th colspan="4">SUMMARY PROMO CALENDAR</th></tr>
        <?php
            foreach($header as $row){
                $loccd = $row->loccd;
                $fullnm = $row->fullnm;
                $addr1 = $row->addr1;
                $addr2 = $row->addr2;
                $addr3 = $row->addr3;
                $telHm = $row->tel_hm;
     			$telHp = $row->tel_hp;
          	}
        ?>	
        <tr>
            <td style="width: 30%;">ID STOCKIST</td>
            <td><?php echo $loccd."-".$fullnm;?></td>
        </tr>
        <tr>
            <td>ADDRESS </td>
            <td><?php echo $addr1.",".$addr2.",".$addr3;?></td>
        </tr>
        <tr>
            <td>Phone No / Mobile No</td>
            <td><?php echo $telHm ." / ".$telHp ;?> </td>
        </tr>
    </table><br />
    <table style="width: 100%;border: 1;" class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
            <tr >
                <th colspan="7" >List Summary Promo Calendar</th>
             </tr>
             <tr>
                <th>NO</th>
                <th>ORDERNO</th>
                <th>IDMEMBER</th>
                <th>MEMBER NAME</th>
                <th>PRODUCT</th>
                <th>PROD NAME</th>
                <th>QTY</th>
             </tr>
        </thead>
        <tbody>
            <?php 
                $no = 1;
                foreach($detSumm as $list){ ?>
                <tr>
                    <td style="text-align: center;"><?php echo $no;?></td>
                    <td><?php echo $list->orderno;?></td>
                    <td><?php echo $list->id_memb;?></td>
                    <td><?php echo $list->nmmember;?></td>
                    <td><?php echo $list->prdcd;?></td>
                    <td><?php echo $list->prdnm;?></td>
                    <td style="text-align: center;"><?php echo $list->qty;?></td>
                </tr>
                    
            <?php
                $no++;}
            ?>
        </tbody>
    </table>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
    </div>
	<p></p>
<?php
    }
?>
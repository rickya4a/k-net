<div class="mainForm">
  <form class="form-horizontal" id="frmSumPromo" method="post" action="<?php echo site_url("be/promo/printSummary/txt")?>" target="_blank">
    <fieldset>      
      <div class="control-group">       
       <label class="control-label" for="typeahead">Transaction Date</label>
               <div class="controls">
	               <input type="text" class="dtpicker typeahead" id="from_date" name="from_date" tabindex="1"/>&nbsp;to&nbsp;
				   <input type="text"  class="dtpicker typeahead" id="to_date" name="to_date" tabindex="2" />
			 	</div>
			    <label class="control-label" for="typeahead">Id Stockist</label>
	        	<div class="controls">
	            	<select id="idstkk" name="idstkk" tabindex="3">
	            		<option value="">--Select Here--</option>
                		<?php 
                            foreach($stk as $list){
                                
                                echo "<option value=\"$list->idstk\">".$list->idstk."</option>" ;    
                            }
                        ?>
	            	</select>
	            </div>
                <label class="control-label" for="typeahead">Print Status</label>
	        	<div class="controls">
                    <select id="printst" name="printst" tabindex="4">
	            	  <option value="0">Unprinted</option>
                        <option value="1">Printed</option>
	            	</select>
	            </div>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" 
        	   onclick="All.ajaxFormPost(this.form.id,'be/promo/getSummary/act')" />
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

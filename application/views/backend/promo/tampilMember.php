<?php
    if(empty($memberdt)){
        setErrorMessage();
    }else{
?>

<?php
    $no=1;
    foreach($memberdt as $a){
        $sfno_reg = $a->sfno_reg;
        $fullnm = $a->fullnm;
        $bankaccno = $a->bankaccno;
        $bankaccnm = $a->bankaccnm;
        $banknm = $a->banknm;
?>

    <p>
        <br>
        <b>ID</b> : <?php echo $sfno_reg;?> - <?php echo $fullnm;?> <br>
        <b>No. Rekening</b> : <?php echo $bankaccno;?> - <?php echo $bankaccnm;?> <br>
        <b>Kode Bank</b> : <?php echo $banknm;?> <br>
    </p>

<?php
$no++; }}
?>

<?php
if($detail != null){
?>
	<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr>
        	<th colspan="4">Detail Data Member</th>
        </tr>

        <tr>
            <th style="width: 13%">ID Member</th>
            <th style="width: 20%">Full Name</th>
            <th>Date Join</th>
            <th>Status Transfer</th>
            <th>Bonus</th>
            <th>Transfer Date</th>
            <th>Tax</th>
            <th>Total</th>
        </tr>

        <?php
            $no=1;
            foreach($member as $row) {
                $dfno = $row->dfno;
                $fullnm = $row->fullnm;
                //$joindt = $row->joindt;
                $status_transfer = $row->status_transfer;
                $bonusrec = $row->bonusrec;
                $transfer_dt = $row->transfer_dt;
                $tax_decrease = $row->tax_decrease;
                $total = $row->total;
                $join=date("d-m-Y",strtotime( $row->joindt ));
        ?>

            <tr>
                <td><?php echo $dfno;?></td>
                <td><?php echo $fullnm;?></td>
                <td><?php echo $join;?></td>
                <td><?php echo $status_transfer;?></td>
                <td><?php echo $bonusrec;?></td>
                <td><?php echo $transfer_dt;?></td>
                <td><?php echo $tax_decrease;?></td>
                <td><?php echo $total; ?></td>
            </tr>

	
<?php
    $no++; }}
?>
    </table>

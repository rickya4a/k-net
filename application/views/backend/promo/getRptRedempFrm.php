<div class="mainForm">
  <form class="form-horizontal" id="frmRptRedemp" method="post" target="_blank" action="<?php echo site_url("recruit/redemp/list");?>">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Trx Type</label>                             
        <div class="controls">
        	<select id="idstk" name="idstk" tabindex="1">
        		<option value="">--Select Here--</option>
        		<?php
                    foreach($stk as $list){
                ?>
                <option value="<?php echo $list->loccd;?>"><?php echo $list->loccd." - ".$list->fullnm;?></option>     
                <?php    
                    }
                ?>
        	</select>
        </div>        		      
        <?php
          echo datepickerFromTo("Transaction Date", "redemp_from", "redemp_to");
		  //echo bonusPeriod();
        ?>
        <label class="control-label" for="typeahead">Eksport To</label>
        <div class="controls">
        	<select id="exportto" name="exportto" tabindex="3">
        		<option value="">--Select Here--</option>
        		<option value="1">View List</option>
                <option value="2">Excell</option>
        	</select>
        </div>   
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="4" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'recruit/redemp/list')" />
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="6"  type="submit" class="btn btn-success" value="To Excel" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

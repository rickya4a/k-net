<?php
if(empty($header)){
    setErrorMessage();
}else{

    //$no=1;
    foreach($header as $a){

//        echo"Dist. Code     : ".$a->dfno."</br>";
        echo"Dist. Name     : ".$a->fullnm."</br>";
        echo"Sponsor Code   : ".$a->sponsorcode."</br>";
        echo"Sponsor Name   : ".$a->sp_nm."</br>";
        echo"Adjusted Rank  : ".$a->perc_adj."%"."(".$a->adjust.")";
    }
}
?>

<?php
if(empty($report)){
    setErrorMessage();
    //echo "data tidak ada!!!";
}else {
    ?>

    <table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr>
            <th colspan="6" align="center">HASIL BONUS REPORT</th>
        </tr>

        <tr>
            <th>BULAN</th>
            <th>TAHUN</th>
            <th width="15%">TOTAL BONUS</th>
            <th width="15%">TOTAL POTONGAN</th>
            <th width="15%">NET BONUS</th>
            <th><form class="formInputxyz" action="https://www.k-net.co.id/promo/printreportstatement" target="_blank" method="POST">ACTION</form></th>
        </tr>

        <?php
        $no = 1;
        foreach ($report as $a) {

            $bonusyear = $a->bonusyear;
            $bonusmonth = $a->bonusmonth;
            $totBonus = $a->totBonus;
            $tax = $a->tax;
            $totBonus_Nett = $a->totBonus_Nett;
            $idnya = $a->distributorcode;
            $nama = $a->nama;
            ?>

            <tr>
                <td align="center">
                    <?php echo $bonusmonth; ?>
                </td>
                <td align="center">
                    <?php echo $bonusyear; ?>
                </td>
                <td align="right">
                    <?php echo number_format($totBonus, 0, ",", "."); ?>
                </td>
                <td align="right">
                    <?php echo $tax; ?>
                </td>
                <td align="right">
                    <?php echo number_format($totBonus_Nett, 0, ",", "."); ?>
                </td>

                <td align="center">
                    <form class="formInputxyz" action="https://www.k-net.co.id/promo/printreportstatement" target="_blank" method="POST">
                        <input type="hidden" id="idnya" name="idnya" value="<?php echo $idnya; ?>">
                        <input type="hidden" id="month" name="month" value="<?php echo $bonusmonth; ?>">
                        <input type="hidden" id="year" name="year" value="<?php echo $bonusyear; ?>">
                        <button type="submit" class="btn btn-success">Cetak</button>
                    </form>
                </td>

            </tr>

            <?php
            $no++;
        }
        ?>

    </table>

    <?php
}
    ?>


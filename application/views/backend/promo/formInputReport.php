<div class="mainForm" xmlns="http://www.w3.org/1999/html">
    <form class="form-horizontal" id="formInputReport" action="http://www.k-net.co.id/promo/printreportstatement" target="_blank" method="POST">
        <fieldset>
            <div class="control-group">
                <div class="controls">
                    <input type="hidden" class="span5" id="id" name="id"  />
                </div>
                <label class="control-label" for="typeahead">Pilih Bulan dan Tahun</label>
                <div class="controls">
                    <select id="month" name="month" tabindex="12">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                    <select id="year" name="year" tabindex="3">
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                    </select>
                </div>
                <label class="control-label" for="typeahead">ID Member</label>
                <input tabindex="3" type="text" id="idmember" name="idmember" style="width: 250px;"/>

                <div class="controls"  id="inp_btn">
                    </br>
                    <input tabindex="7" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.searchOfficeCahyono('loungereport/getReport','frmLounge')" />
                    <button type="submit" class="btn btn-success" >Cetak</button>
                </div>
            </div>
        </fieldset>
    </form>

    <div> </div>
</div><!--/end mainForm-->

<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());
    });
</script>
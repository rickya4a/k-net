<?php
    if(empty($bonus)){
        setErrorMessage();
    }else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="11" align="center">BONUS AYUARTIS OIL</th>
    </tr>

    <tr>
        <th style="width: 5%">NO</th>
        <th>TGL JOIN</th>
        <th>NAMA</th>
        <th>JUMLAH REKRUTAN</th>
        <th>NAMA REKENING</th>
        <th>NO REKENING</th>
        <th>BANNK</th>
        <th>BONUS</th>
        <th>TOTAL TRANSFER</th>
        <th>TGL TRANSFER</th>
        <th>STATUS</th>
    </tr>

<?php
    $no=1;
    foreach($bonus as $x){
        $tgl1 = date("d-m-Y",strtotime( $x->tgl1 ));
        $tgl2 = date("d-m-Y",strtotime( $x->tgl2 ));
        $datel = $x->tgl1;
        $date2 = $x->tgl2;
        $fullnm = $x->fullnm;
        $jml_rekrutan = $x->jml_rekrutan;
        $NAMAREK = $x->NAMAREK;
        $REKENING = $x->REKENING;
        $BANK = $x->BANK;
        $NOMINAL = number_format($x->NOMINAL, 0, ".", ",");
        $NOMINAL_TRF = number_format($x->NOMINAL_TRF, 0, ".", ",");
        $status = $x->status;
        
        if($x->tglFINANCE == null){
            $tglFINANCE = "";
        }else{
            $tglFINANCE = date("d-m-Y",strtotime( $x->tglFINANCE ));
        }

?>

    <tr>
        <td align="center"><?php echo $no;?></td>
        <td align="center">
            <input type="hidden" id="from<?php echo $no;?>" value="<?php echo $datel;?>">
            <input type="hidden" id="to<?php echo $no;?>" value="<?php echo $date2;?>">
            <?php echo $tgl1;?> hingga <?php echo $tgl2;?>
        </td>
        <td align="center"><?php echo $fullnm;?></td>
        <td align="center"><a id="<?php echo $jml_rekrutan;?>" onclick="javascript:be_trans.getDetailBonus(<?php echo $no;?>)" href="#"><?php echo $jml_rekrutan;?></a></td>
        <td align="center"><?php echo $NAMAREK;?></td>
        <td align="center"><?php echo $REKENING;?></td>
        <td align="center"><?php echo $BANK;?></td>
        <td align="center"><?php echo $NOMINAL;?></td>
        <td align="center"><?php echo $NOMINAL_TRF;?></td>
        <td align="center"><?php echo $tglFINANCE;?></td>
        <td align="center"><?php echo $status;?></td>
    </tr>

        <?php
        $no++; }}
?>
</table>

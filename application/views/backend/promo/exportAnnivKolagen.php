<div class="mainForm">
	<form class="form-horizontal" id="exportPromoAnniv" target="_blank">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Export to File CSV</label>
				
				<div class="controls"  id="inp_btn">
					 <input class="btn btn-mini btn-primary" type="submit" formaction="<?php echo base_url('be/promo/anniv_kolagen/export/header') ?>" value="Header Data" />
      				 <input class="btn btn-mini btn-primary" type="submit" formaction="<?php echo base_url('be/promo/anniv_kolagen/export/detail') ?>" value="Detail Data" />
				</div>
			</div>
			<!-- end control-group -->
		</fieldset>

		<div class="result"></div>
	</form>
</div><!--/end mainForm-->


<form id="generatesub" method="post" >
        <table width="92%" class="table table-striped table-bordered bootstrap-datatable datatable" align="center">
            <thead>
    		  <tr bgcolor="#f4f4f4">
              <th><input type="checkbox" name="checkall" onclick="checkAll(this);"/></th>
                <th>ORDERNO</th>
    			<th>ID MEMBER</th>
                <th>MEMBER NAME</th>
                <th>TOTAL PAY</th>
                <th>BONUS PERIOD</th>
                <th>TRX DATE</th>
    		  </tr>
            </thead>
            <tbody>
            <?php
            foreach($result as $row)
            {
                $trxdate = date('d/m/Y', strtotime($row->etdt));
                $bnsperiod = date('d/m/Y', strtotime($row->bnsperiod));
                echo "
                <tr>
                    <td align=\"center\">
                    <input type=checkbox id=cek[] name=cek[] value=\"$row->trcd\" />
                    </td>
                   <td align=\"right\">$row->trcd</td>
                   <td>$row->dfno</td>
                   <td>$row->fullnm</td>
                   <td><div align=right>".number_format($row->totpay,0,"",",")."</div></td>
                   <td><div align=center>$bnsperiod</div></td>
                   <td><div align=center>$trxdate</div></td>
                </tr>";   
            }
            ?>
            <tr>
                <td colspan="7">
                    <input type="button" class="btn btn-success" onclick="be_promo.previewGenerate()" name="submit" value="Process" id="checkss"/>
                </td>
            </tr>
            </tbody>
        </table>
        <input type="hidden" name="typee" value="rdm"/>
        <input type="hidden" name="bnsperiod" value="<?php echo $bnsperiod;?>"/>
    </form>
    
<script type="text/javascript">

/*$(document).ready(function() 
{
	$('.datatable').dataTable( {
		
		"sPaginationType": "bootstrap",
		"oLanguage": {
			
		}
	});
 });*/

function checkAll(theElement) 
{
    var theForm = theElement.form;
    for(z=0; z<theForm.length;z++)
    {
        if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkall')
        {
            theForm[z].checked = theElement.checked;
        }

    }
}
</script>
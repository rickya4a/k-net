<?php
  if(!empty($sess_exp)) {
  	   setErrorMessage($sess_exp);
  } else {
?>
<div class="mainForm">
	<form class="form-horizontal" id="formBannerPromo" enctype="multipart/form-data">
	   
       <div class="control-group"> 
       	  
          <input tabindex="2"  class="required uppercase span5	setReadOnly" type="hidden" id="id" name="id" />  
          <?php echo monthYearPeriod2("Period"); ?>
          
          <label class="control-label">Promo Name</label>  
            <div class="controls">
              <input tabindex="4"  class="uppercase span5" type="text" id="promo_name" name="promo_name" />
            </div>
         
           <label class="control-label"><font color="red">&nbsp;*</font>Status</label>  
            <div class="controls">
              <select tabindex="6"  class="form-list required" id="status" name="status">
	            	<option value="1">Active</option>
					<option value="0">Inactive</option>
	          </select> 
            </div> 
            
    	    
    	    <?php echo setSingleUploadFile("file"); 
    	      $list = "be/promo/banner/config/list";
    	    ?>
           <div class="controls"  id="inp_btn">
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Save" onclick="be_promo.saveInputBannerPromo('be/promo/banner/config/save','formBannerPromo','be/promo/banner/config/list')" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View List Banner Promo" onclick="All.getListData('be/promo/banner/config/list')" />
          </div>  
          <div class="controls"  id="upd_btn" style="display: none;"> 
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Update" onclick="be_promo.saveInputBannerPromo('be/promo/banner/config/update','formBannerPromo','be/promo/banner/config/list')" />
            <input id="cancelupd" class="btn btn-danger" type="button" onclick="All.cancelUpdateForm()" value="Cancel Update">
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View List Banner Promo" onclick="All.getListData('be/promo/banner/config/list')" />
          </div>                                               
        </div>  
	   <div class="result">
	   
	   </div>	
	</form>
</div>

<?php
   }
?>
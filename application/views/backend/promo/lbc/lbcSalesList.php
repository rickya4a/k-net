<?php
	//echo "<font color=red>MASIH BLM BISA DIPAKE YA..</font>";
?>
<table width="100%" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Produk</th>
			<th>Nama Produk</th>
			<th>Qty</th>
			<th>BV</th>
			<th>Total BV</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		$total_bv = 0;
		  foreach($mdReturn as $dtax) {
			echo "<tr>";
			echo "<td align=center>$no</td>";
			echo "<td>$dtax[prdcd]</td>";
			echo "<td>$dtax[prdnm]</td>";
			echo "<td align=\"right\">".number_format($dtax['qtyord'],0,".",",")."</td>";
			echo "<td align=\"right\">".number_format($dtax['bv'],0,".",",")."</td>";
			echo "<td align=\"right\">".number_format($dtax['total_bv'],0,".",",")."</td>";
			echo "</tr>"; 
			$total_bv += $dtax['total_bv'];			
			$no++;
		  }
		  
		?>
		<tr><td colspan="5">TOTAL BV</td><td align="right"><?php echo number_format($total_bv,0,".",","); ?></td></tr>
	</tbody>
	</table>
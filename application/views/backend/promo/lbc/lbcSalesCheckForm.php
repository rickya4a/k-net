<form class="form-horizontal" id="lbcSalesForm" >
    <fieldset>      
      <div class="control-group"> 
      	
        <label class="control-label" for="typeahead">ID Member</label>
        <div class="controls">
         <input style="width: 150px;" type="text" id="memberid" name="memberid" onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/msmemb/dfno','#nmmember')" />
         <input style="width: 300px;" type="text" id="nmmember" name="nmmember" readonly="readonly" />   
        </div>
        <label class="control-label" for="typeahead">Bonus Period</label>
        <div class="controls">
         <input type="text" class="dtpicker" id="bnsp" name="bnsp" placeholder="From" required="required" />
            
        </div>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
			<button class="btn btn-primary" type="button" name="save"  onclick="All.ajaxFormPost(this.form.id,'lbc/sales/check/list')">Submit</button>
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
	 <div class="result"></div>
  </form>
 
 <script type="text/javascript">
$(document).ready(function() 
{	
	$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());
 });

</script>
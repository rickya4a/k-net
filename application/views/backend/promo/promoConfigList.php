<?php
	if($listPromo != null) {
	//print_r($listPromo);	 
?>
 <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th colspan="7">List Promo</th>
		</tr>
		<tr>
			<th>No</th>
			<th>ID</th>
			<th>Promo Name</th>
			<th>Status</th>
			<th>Period</th>
			<th>Max Discount</th>
			<th>&nbsp;</th>		
		</tr>
	</thead>
	<tbody>
		<?php
		  $i = 1;
		  foreach($listPromo as $dtax) {
		  	echo "<tr>";
			echo "<td align=right>$i</td>";
			echo "<td align=center>$dtax->id</td>";
			echo "<td>$dtax->name</td>";
			echo "<td align=center>$dtax->status</td>";
			echo "<td align=center>$dtax->period_start - $dtax->period_end</td>";
			echo "<td align=center>$dtax->max_discount</td>";
			$ondelete = "All.deleteFormData('product/delete/', '$dtax->id', 'product/list')";
                $arr = array(
				    "update" => "be_promo.getUpdatePromoConfig('$dtax->id')",
				    //"delete" => $ondelete
				);
                echo btnUpdateDelete($arr);  
			echo "</tr>";  
			$i++;
		  }
		?>
	</tbody>
</table>

<?php
	} else {
		echo setErrorMessage("No record found");
	}
?>
<script type="text/javascript">
$(document).ready(function() 
{
	All.set_datatable();
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<?php
 if($promo == null) {
 	echo setErrorMessage("Belum ada point terakumulasi saat ini..");
 } else {
 	//echo setErrorMessage("Masih dalam pengembangan menggunakan data contoh..");
?>
<table width="70%" class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Data Poin Reward Rekrut</th>
	</tr>
	<tr>
		<td width="30%">ID Member</td>
		<td><?php echo $promo[0]->dfno ?></td>
	</tr>
	<tr>
		<td>Nama Member</td>
		<td><?php echo $promo[0]->fullnm ?></td>
	</tr>
	<tr>
		<td>Akumulasi Point</td>
		<td><?php echo $promo[0]->total_poin ?></td>
	</tr>
	<tr>
		<td>Total Poin sudah di klaim</td>
		<td><?php echo $promo[0]->total_redemp ?></td>
	</tr>
	<tr>
		<td>Sisa Poin</td>
		<td><?php echo $promo[0]->total_remain ?></td>
	</tr>
	<tr>
		<td>Kode Penukaran</td>
		<td><?php echo $promo[0]->sec_code ?></td>
	</tr>
</table>

<?php
    //print_r($redemp);
	
 } 
?>
<?php
if($redemp != null) {
	?>
	<table width="100%" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Trx Code</th>
				<th>Tgl Klaim</th>
				<th>Poin</th>
				<th>Stockist</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			  foreach($redemp as $dtax) {
			  	echo "<tr>";
				echo "<td align=center>$no</td>";
				echo "<td>$dtax->trx_code</td>";
				echo "<td>$dtax->trx_date</td>";
				echo "<td align=right>$dtax->point_redemp</td>";
				echo "<td>$dtax->stockist</td>";  
				echo "</tr>";  
				$no++;
			  }
			?>
		</tbody>
	</table>
	<?php	
	} else {
 	echo "tidak ada klaim";
    }
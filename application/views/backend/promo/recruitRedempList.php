<?php
    if(empty($listRedempRecruit)){
        echo setErrorMessage();
        
    }else{
        if($exportTo == '2'){
	    	header("Content-type: application/vnd.ms-excel; charset=utf-8");
		    header("Content-Disposition: attachment; filename=ReportRedempRecruit.xls" );
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");
			
			//$rptHead = "<tr><th>Report From Date </th><th>$from to $to</th></tr><tr></tr>";
			$border = 'border="1" ';
		}else{
			//$rptHead = '';
			$border = '';
		}
        //$rptHead = "<tr><th colspan = \"5\">Redemption Poin Recruit From Date $from to $to</th></tr><tr></tr>";
?>
<div class="overflow-auto">
    <form id="listRedemp" method="post" >
        <table <?php echo $border;?> width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th colspan="5" >Redemption Poin Recruit From Date <?php echo $from." to ".$to;?></th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>CN No</th>
                    <th>CN Date</th>
                    <th>ID Stk</th>
                    <th>Total Poin Redemp</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    foreach($listRedempRecruit as $list){
                ?>
                <tr>
                    <td style="text-align: center;"><?php echo $no;?></td>
                    <?php 
                        if($exportTo == '2'){
                            echo "<td>".$list->csno."</td>";
                        }else{
                            echo "<td><a id=\"$list->csno\" href=\"#\" onclick = \"All.ajaxShowDetailonNextForm('recruit/redemp/details/$list->csno')\">$list->csno</a></td>";
                        }
                    ?>
                        
                    <!--<td><a id="<?php //echo $list->csno;?>" href="#" onclick="All.ajaxShowDetailonNextForm('recruit/redemp/details/<?php //echo $list->csno;?>')"><?php //echo $list->csno;?></a></td>-->
                    <td style="text-align: center;"><?php echo date("d-m-Y",strtotime($list->trdt));?></td>
                    <td style="text-align: center;"><?php echo $list->loccd;?></td>
                    <td style="text-align: right;"><?php echo number_format($list->totPoin, 0, ',', ',');?></td>
                </tr>
                <?php $no++;} ?>
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->
    </form>
    <br />
</div>
  <?php }?> 
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>

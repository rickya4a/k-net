
<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="post" id="formImportPromo" action="<?php echo base_url('be/promo/importayu'); ?>">
    <fieldset>      
      <div class="control-group">
       
         <label class="control-label" for="typeahead">File Excel to upload</label>
         <div class="controls" >
            <input type="file" id="file" name="file" class="span7 typeahead" />
         </div>
         
         
          
         <label class="control-label" for="typeahead">&nbsp</label>                             
         <div class="controls"  id="inp_btn">
          <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Import File" onclick="be_promo.importPromoAyuartisToDB()" />
            <input tabindex="6"  type="reset" class="btn btn-reset" value="Reset" />
             <a href="<?php echo base_url("assets/excel/format.xlsx"); ?>">Download Format</a>
             <!--<input type="button" class="btn btn-success" value="Preview Content File" onclick="be_trans_klink.readFromFile()" />-->
         </div>
         
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div id="editor"></div>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
$(document).ready(function()
{
   
   $(All.get_active_tab() + " #fileXls").change(function () {
      
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'xls':
            case 'xlsx':
            case 'csv':
            case 'txt':
            
                $(All.get_active_tab() + ' #uploadButton').attr('disabled', false);
                break;
            default:
                alert('This is not an allowed file type, only XLS/CSV/TXT file..!!');
                this.value = '';
        }
   });
   
   
   
});
</script>

<?php
    if(empty($detail)){
        echo setErrorMessage();
        
    }else{
?>
<div class="overflow-auto">
    <form id="listRedemp" method="post" >
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable">
            <thead>
                <tr>
                    <th colspan="7" >Redemption Poin Recruit From <?php echo $cnno;?></th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Trx No</th>
                    <th>Trx Date</th>
                    <th>Member ID</th>
                    <th>Member Name</th>
                    <th>ID Stk</th>
                    <th>Poin Redemp</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $totPoin = 0;
                    foreach($detail as $det){
                ?>
                <tr>
                    <td style="text-align: center;"><?php echo $no;?></td>
                    <td><?php echo $det->trcd;?></td>
                    <td><?php echo date("d-m-Y",strtotime($det->createdt));?></td>
                    <td><?php echo $det->dfno;?> </td>
                    <td><?php echo $det->fullnm;?> </td>
                    <td><?php echo $det->loccd;?> </td>
                    <td style="text-align: right;"><?php echo number_format($det->tdp, 0, ',', ',');?></td>
                </tr>
                <?php 
                        $totPoin += $det->tdp;
                        $no++;} ?>
                <tr>
                    <td colspan="6" style="text-align: center;">Total Poin Redemption</td>
                    <td style="text-align: right;"><?php echo number_format($totPoin, 0, ',', ',');?></td>
                </tr>
                <tr>
                    <td colspan="7"><input class="btn btn-small btn-warning" value="<< Back" onclick="All.back_to_form(' .nextForm1',' .mainForm')" type="button"></td>
                </tr>
            </tbody>
        </table>
    </form>
    <br />
</div>
  <?php }?> 
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>

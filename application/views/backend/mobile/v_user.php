<style>
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }

    .example-modal .modal {
        background: transparent !important;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box box-primary">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                    <li><a href="#">Data Master</a></li>
                    <li class="active">User</li>

                    <!--header title -->
                    <h1>
                        <i class="fa fa-user fa-lg"></i>
                        List
                        <b>User</b>
                    </h1>
                </ol>
            </div>
        </div>
    </div>


    <!-- Main content -->
    <div class="result1">
        <section class="content">
            <div class="form-group">
                <!-- <form action="<?php echo base_url(); ?>user/add" target="_blank">
          <button type="submit" class="btn btn-primary btn-lg">
            <i class="fa fa-plus"> </i> Add
          </button>
        </form> -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-plus"> </i> Add
                </button>

                <div class="modal fade" id="myModal">
                    <form method="POST" action="<?php echo base_url(); ?>add_user">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Create User</h4>
                                </div>
                                <div class="modal-body">
                                    <!-- <p>One fine body&hellip;</p> -->

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" id="username" name="username" placeholder="username" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="text" class="form-control" id="password" name="password" placeholder="password" required>
                                        </div>
                                        <div class="form-group">
                                            <label>User role</label>
                                            <select class="form-control" id="user_role" name="user_role">
                                                <option>-- select option --</option>
                                                <option>123</option>
                                                <option>321</option>
                                                <option>666</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </form>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>

            <table id="datatableM" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>
                        no
                    </th>
                    <th>
                        username
                    </th>
                    <th>
                        password
                    </th>
<!--                    <th>-->
<!--                        user role-->
<!--                    </th>-->
                    <th>
                        action
                    </th>
                </tr>
                </thead>

                <tbody>

                <?php

                $no = 1;
                foreach($list_user as $dt){

                    echo "<tr>";
                    echo "<td>".$no."</td>";
                    echo "<td>".$dt->username."</td>";
                    echo "<td>".md5($dt->password)."</td>";
//                    echo "<td>".$dt->user_role."</td>";
                    echo "<td>
                                <a class=\"btn btn-success\" onclick=\"getDetailUser($no)\">edit</a>
                                <input type=\"hidden\" name=\"id_user\" id=\"id_user$no\" value=\"$dt->id\">

                                <a class=\"btn btn-danger\" onclick=\"deleteUser($no)\">delete</a>
                                <input type=\"hidden\" name=\"id_user\" id=\"id_user$no\" value=\"$dt->id\">

                              </td>";
                    echo "</tr>";
                    $no++;
                }
                ?>

                </tbody>

            </table>


        </section>
    </div>
    <!-- div detail -->
    <div id="detailuser"></div>
    <!-- /.content-wrapper -->
</div>

<script type="text/javascript">

    function getDetailUser(param){

        var asd = $("#id_user" +param).val()

        //console.log('idnya : '+asd);

        $.ajax({

            url : All.get_url("detail_user"),
            type : 'POST',
            data : {id_user : asd},

            success :

                function(data){

                    $(".result1").hide();
                    $("#detailuser").html(null);
                    $("#detailuser").html(data);
                }
        });
    }

    function deleteUser(param){

        var asd = $("#id_user" +param).val();
        var btn = confirm("are you sure want to delete?");

        if(btn == true){

            $.ajax({

                url : All.get_url("delete_user"),
                type : 'POST',
                data : {id_user : asd},

                //console.log(data);
                success :

                    function(data){

                        if(data == "success"){
                            alert("delete success");
                            location.reload();
                        }else{
                            alert("delete failed");
                        }
                    }

            });
        }
    }

    function backToForm(){

        $("#detailuser").html(null);
        $(".result1").show();
    }
</script>



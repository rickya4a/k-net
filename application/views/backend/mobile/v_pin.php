<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box box-primary">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                    <li><a href="#">Event</a></li>
                    <li class="active">Pin Eksklusif</li>

                    <!--header title -->
                    <h1>
                        <i class="fa fa-map-pin fa-lg"></i>
                        Pin
                        <b>Member</b>
                    </h1>
                </ol>
            </div>
        </div>
    </div>

    <div class="result1">
        <section class="content">
            <div class="form-group">
                <div class="box-body">
                    <div class="form-group">
                        <label>ID Member</label>
                        <input type="text" class="form-control" id="idmember" name="idmember" placeholder="masukkan id member" onchange="getName()" required>
                    </div>
                    <div class="form-group">
                        <label>Nama Member</label>
                        <input type="text" class="form-control" id="nmmember" name="nmmember" placeholder="nama member" readonly>
                    </div>
                    <button class="btn btn-primary" id="btn">Submit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <input type="submit" id="hit" value="Search" onclick="getTotalBV()" class="btn"/>
                </div>
            </div>
        </section>
    </div>

    <div id="detMember">

    </div>
</div>



<script type="text/javascript">
    function getName() {

        var idmember = document.getElementById("idmember").value;

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('be_mobile/getname');?>",
            data: {idmember: idmember},
            dataType: 'json',

            success: function (result) {

                if (result.response == "TRUE") {

                    document.getElementById("nmmember").value = result.nama;
                } else {

                    alert("ID Member Tidak Ditemukan!");
                }
            }
        });
    }

    function getTotalBV(){

        var idmemb = document.getElementById("idmember").value;

        $.ajax({

            url : All.get_url("be_mobile/event/getBV"),
            type : 'POST',
            data : {idmemb : idmemb},

            success :

                function(data){

                    $("#detMember").html(null);
                    $("#detMember").html(data);
                }
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("table.main").on("click", "#btn", function (event) {
            {
                var idmemb = document.getElementById("idmember").value;

                $.ajax({

                    url : All.get_url("be_mobile/event/getBV"),
                    type : 'POST',
                    data : {idmemb : idmemb},

                    success :

                        function(data){

                            $("#detMember").html(null);
                            $("#detMember").html(data);
                        }
                });
            }
        });
    });
</script>
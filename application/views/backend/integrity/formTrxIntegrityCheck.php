<div class="mainForm">
	<form class="form-horizontal" id="formTrxReport" method="post" target="_blank"
		action="<?php echo site_url("trans/report/toExcel");?>">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Trx Type</label>
				<div class="controls">
					<select id="tipe" name="tipe" class="span7">
						<option value="">--Select Here--</option>
						<option value="1">Header dan Detail berbeda dengan DP/BV</option>
						<option value="2">Header ada dan Detail tidak ada</option>
						<option value="3">Kode Stockist Kosong</option>
						<option value="4">Inputan stokis yang loccd dan createnm nya berbeda</option>
						<option value="5">Trx K-Net yang tidak masuk ke table NEWTRH</option>
						<option value="6">Trx K-Net Header dan Detail berbeda</option>
						<option value="7">Trx K-net Reg. Member Record memb_OK Double</option>
						<option value="8">Tabel newtrd / sc_newtrd yang mengandung qty produk bernilai 0 / null</option>
						<option value="9">Check Field DP dan BV berbeda</option>
					</select>
				</div>
				<div class="control-group">
					<label class="control-label" for="typeahead">Bonus Period</label>
					<div class="controls">
						<select id="bnsmonth" name="bnsmonth">
							<option value="">--All--</option>
							<option value="01">January</option>
							<option value="02">February</option>
							<option value="03">March</option>
							<option value="04">April</option>
							<option value="05">May</option>
							<option value="06">June</option>
							<option value="07">July</option>
							<option value="08">August</option>
							<option value="09">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
						<input type="text" id="bnsyear" name="bnsyear" placeholder="Year"
							value="<?php echo date("Y"); ?>" autocomplete="off" />
					</div>
				</div>

				<label class="control-label" for="typeahead">&nbsp</label>
				<div class="controls" id="inp_btn">
					<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save"
						value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/trans/integrity/check')" />
					<input tabindex="4" type="reset" class="btn btn-reset" value="Reset" />
				</div>
			</div> <!-- end control-group -->
		</fieldset>
		<div class="result"></div>
	</form>
</div>
<!--/end mainForm-->
<!-- <script src="<?php echo base_url('asset/js/yearpicker.js')?>"></script> -->
<?php
if (empty($result)) {
	echo setErrorMessage();
?>
<input value="<< Back " type="button" class="btn btn-small btn-warning"
	onclick="All.back_to_form(' .nextForm1',' .mainForm')" />
<?php } else { ?>
<form id="detailOrder" method="POST">
	<div class="col-xs-6">
		<table class="table table-striped table-bordered" width="100%">
			<thead>
				<tr>
					<th colspan='9'>Detail Order</th>
				</tr>
				<tr>
					<th style="width:10%">Order No</th>
					<td><?php echo $result[0]->orderno; ?></td>
					<input type="hidden" name="orderno" id="orderno" value="<?php echo $result[0]->orderno; ?>">
					<th style="width:10%">CN No</th>
					<td colspan="3"><?php echo $result[0]->CNno; ?></td>
					<input type="hidden" name="cnno" id="cnno" value="<?php echo $result[0]->CNno; ?>">
				</tr>
				<tr>
					<th>ID Member</th>
					<?php if (empty($result[0]->dfno)) { ?>
						<td>
							<input type="text" name="dfno" id="dfno" style="width: 150px;" onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/msmemb/dfno','#membername')">
							<input type="text" name="membername" id="membername" style="width: 300px;" readonly>
							<input class="btn btn-primary" type="button" value="Save" onclick="reconcileIdMember()">
						</td>
					<?php } else { ?>
					<td><?php echo $result[0]->dfno; ?></td>
					<?php } ?>
					<th>Register No</th>
					<td colspan="3"><?php echo $result[0]->REGISTERno; ?></td>
					<input type="hidden" name="registerno" id="registerno" value="<?php echo $result[0]->REGISTERno; ?>">
				</tr>
				<tr>
					<th>Token</th>
					<td><?php echo $result[0]->token; ?></td>
					<th>KW No</th>
					<td colspan="3"><?php echo $result[0]->KWno; ?></td>
					<input type="hidden" name="kwno" id="kwno" value="<?php echo $result[0]->KWno; ?>">
				</tr>
				<tr>
					<th>SSR No</th>
					<td><?php echo $result[0]->SSRno; ?></td>
					<th>IP No.</th>
					<td colspan="3"><?php echo $result[0]->IPno; ?></td>
					<input type="hidden" name="ipno" id="ipno" value="<?php echo $result[0]->IPno; ?>">
				</tr>
				<tr>
					<th>Total Pay</th>
					<td><?php echo $result[0]->total_pay; ?></td>
					<th>Total BV</th>
					<td colspan="3"><?php echo $result[0]->total_bv; ?></td>
				</tr>
				<?php if ($result[0]->idstk == "" || $result[0]->idstk == NULL) { ?>
					<tr>
							<th>Stockist</th>
							<td colspan="3">
								<input type="text" name="newidstk" id="newidstk">
								<input type="button" value="Set ID Stockist" class="btn btn-mini btn-primary" onclick="be_trans.recoverStkCodeTrx()">
							</td>
					</tr>
				<?php } ?>
			</thead>
		</table>
		<table class="table table-striped table-bordered" width="100%">
			<thead>
			<tr>
					<th colspan='9'>Status data di database klink_mlm2010</th>
			</tr>
			<tr>
					<th style="width:10%">webol_trans_ok</th>
					<td><?php echo $result[0]->webol_trans_ok_id; ?></td>
					<th style="width:10%">webol_log_trans</th>
					<td colspan="3"><?php echo $result[0]->webol_log_trans_id; ?></td>
				</tr>
				<tr>
					<th>ordivhdr</th>
					<td><?php echo $result[0]->ordivhdr_id; ?></td>
					<th>ordivtrh</th>
					<td colspan="3"><?php echo $result[0]->ordivtrh_id; ?></td>
				</tr>
				<tr>
					<th>bbhdr</th>
					<?php if (empty($result[0]->bbhdr)) { ?>
					<td class="bbhdr_rec"><a class="btn btn-success" onclick="reconcileBbhdr('<?php echo $result[0]->IPno; ?>', '<?php echo $result[0]->orderno; ?>')">Reconcile</a></td>
					<?php } else { ?>
					<td class="bbhdr_rec"><?php echo $result[0]->bbhdr_id; ?></td>
					<?php } ?>
					<th>custpaydet</th>
					<td colspan="3"><?php echo $result[0]->custpaydet_cn; ?> </td>
				</tr>
				<tr>
					<th>sc_newtrh</th>
					<td><?php echo $result[0]->sc_newtrh_id; ?></td>
					<th>newtrh</th>
					<?php if (empty($result[0]->newtrh_id)) { ?>
					<td class="newtrh_rec"><a class="btn btn-success" onclick="reconcileNewtrh('<?php echo $result[0]->orderno; ?>')">Reconcile</a></td>
					<?php } else { ?>
					<td class="newtrh_rec"><?php echo $result[0]->newtrh_id; ?></td>
					<?php } ?>
				</tr>
				<tr>
					<th>billivhdr</th>
					<?php if (empty($result[0]->billivhdr_id)) { ?>
					<td class="billivhdr_rec"><a class="btn btn-success" onclick="reconcileBillivhdr('<?php echo $result[0]->KWno; ?>', '<?php echo $result[0]->orderno; ?>')">Reconcile</a></td>
					<?php } else { ?>
					<td class="billivhdr_rec"><?php echo $result[0]->billivhdr_id; ?></td>
					<?php } ?>
					<th>newivtrh</th>
					<?php if (empty($result[0]->newivtrh_id)) { ?>
					<td class="newivtrh_rec"><a class="btn btn-success" onclick="reconcileNewivtrh('<?php echo $result[0]->CNno; ?>', '<?php echo $result[0]->orderno; ?>')">Reconcile</a></td>
					<?php } else { ?>
					<td class="newivtrh_rec"><?php echo $result[0]->newivtrh_id; ?></td>
					<?php } ?>
				</tr>
				<tr>
					<th>ordivtrp</th>
					<td><?php echo $result[0]->ordivtrp_id; ?></td>
					<th>ordivdetp</th>
					<td><?php echo $result[0]->ordivdetp_id; ?></td>
				</tr>
				<tr>
					<th>billivdetp</th>
					<td><?php echo $result[0]->billivdetp_id; ?></td>
					<th>newivtrp</th>
					<td><?php echo $result[0]->newivtrp_id; ?></td>
				</tr>
				<tr>
					<th>bonusmonth</th>
					<td colspan="3" class="bonus_rec">
						<input type="text" id="bonusmonth" name="bonusmonth" value="<?php echo $result[0]->bonusmonth; ?>">
						<a class="btn btn-success" onclick="reconcileBonusMonth()">Simpan</a>
					</td>
				</tr>
			</thead>
		</table>
	</div>

	<input value="<< Back" type="button" class="btn btn-small btn-warning"
		onclick="All.back_to_form(' .nextForm1',' .mainForm')" />
</form>

<script>
function reconcileNewtrh(orderno) {
	All.set_disable_button();
	$.ajax({
		url: All.get_url('be/trans/integrity/reconcileTrx/newtrh'),
		type: 'POST',
		data: {
			orderno: orderno
		},
		dataType: 'json',
		success: function (data) {
			All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				$(All.get_active_tab() + " .newtrh_rec").html(null);
				$(All.get_active_tab() + " .newtrh_rec").html("<font color=red>STATUS OK</font>");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	});
}

function reconcileBillivhdr(kwno, orderno) {
	All.set_disable_button();
	$.ajax({
		url: All.get_url('be/trans/integrity/reconcileTrx/billivhdr'),
		type: 'POST',
		data: {
			kwno: kwno,
			orderno: orderno
		},
		dataType: 'json',
		success: function (data) {
			All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				$(All.get_active_tab() + " .billivhdr_rec").html(null);
				$(All.get_active_tab() + " .billivhdr_rec").html("<font color=red>STATUS OK</font>");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	});
}

function reconcileBbhdr(ipno, orderno) {
	$.ajax({
		url: All.get_url('be/trans/integrity/reconcileTrx/bbhdr'),
		type: 'POST',
		data: {
			ipno: ipno,
			orderno: orderno
		},
		dataType: 'json',
		success: function (data) {
			All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				$(All.get_active_tab() + " .bbhdr_rec").html(null);
				$(All.get_active_tab() + " .bbhdr_rec").html("<font color=red>STATUS OK</font>");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	});
}

function reconcileNewivtrh(cnno, orderno) {
	$.ajax({
		url: All.get_url('be/trans/integrity/reconcileTrx/newivtrh'),
		type: 'POST',
		data: {
			cnno: cnno,
			orderno: orderno
		},
		dataType: 'json',
		success: function (data) {
			All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				$(All.get_active_tab() + " .newivtrh_rec").html(null);
				$(All.get_active_tab() + " .newivtrh_rec").html("<font color=red>STATUS OK</font>");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	});
}

function reconcileBonusMonth() {
	var bonusmonth = $(All.get_active_tab() + ' #bonusmonth').val();
	var registerno = $(All.get_active_tab() + ' #registerno').val();
	var cnno = $(All.get_active_tab() + ' #cnno').val();
	var orderno = $(All.get_active_tab() + ' #orderno').val();
  console.log("bonusmonth : " +bonusmonth);
	console.log("registerno : " +registerno);
	console.log("cnno : " +cnno);
	console.log("orderno : " +orderno);

	$.ajax({
		url: All.get_url('be/trans/integrity/reconcileTrx/bonusmonth'),
		type: 'POST',
		data: {
			bonusmonth: bonusmonth,
			registerno: registerno,
			cnno: cnno,
			orderno: orderno
		},
		dataType: 'json',
		success: function (data) {
			All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				$(All.get_active_tab() + " .bonus_rec").html(null);
				$(All.get_active_tab() + " .bonus_rec").html("<font color=red>STATUS OK</font>");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	})


}

function reconcileIdMember() {
	var dfno = $(All.get_active_tab() + ' #dfno').val();
	var orderno = $(All.get_active_tab() + ' #orderno').val();
	var membername = $(All.get_active_tab() + ' #membername').val();

  //console.log("bonusmonth : " +bonusmonth);
	//console.log("registerno : " +registerno);
	console.log("dfno : " +dfno);
	console.log("orderno : " +orderno);

	$.ajax({
		url: All.get_url('be/trans/integrity/reconcileTrx/idmember'),
		type: 'POST',
		data: {
			idmember: dfno,
			nmmember: membername,
			orderno: orderno
		},
		dataType: 'json',
		success: function (data) {
			/*All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				$(All.get_active_tab() + " .bonus_rec").html(null);
				$(All.get_active_tab() + " .bonus_rec").html("<font color=red>STATUS OK</font>");
			}*/
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	})


}
</script>
<?php
}
?>
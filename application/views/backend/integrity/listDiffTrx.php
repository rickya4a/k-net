<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx Integrity Check</th>
		</tr>
		<tr>
			<th>Order No.</th>
			<th>CN No.</th>
			<th>Transaction Date</th>
			<th>Total Payment</th>
			<th>Total Product Price</th>
			<th>Total BV</th>
			<th>Total Product BV</th>
			<th>Stockist</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
		<?php if ($key->orderno_prd == NULL) { ?>
			<td style="text-align: center;"><a href="#" id="<?php echo $key->orderno; ?>" class="btn btn-warning" onclick="All.ajaxShowDetailonNextForm('trans/reconcile/id/<?php echo $key->orderno; ?>')"><?php echo $key->orderno; ?></a></td>
		<?php } else { ?>
			<td style="text-align: center;"><a href="#" id="<?php echo $key->orderno; ?>" onclick="be_trans.getDetailTrx(this)"><?php echo $key->orderno; ?></a></td>
		<?php } ?>
			<td><?php echo $key->CNno; ?></td>
			<td><?php echo date("Y-m-d", strtotime($key->datetrans)); ?></td>
			<td style="text-align: right"><?php echo number_format($key->total_pay, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->jum_nilai_produk, 0,".", ","); ?></td>
			<td style="text-align: right"><?php echo number_format($key->total_bv, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->jum_bv_produk, 0,".", ","); ?></td>
			<td><?php echo $key->idstk; ?></td>
		</tr>
	<?php
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx K-Net Reg. Member Record Check</th>
		</tr>
		<tr>
			<th>Order No.</th>
			<th>Token No.</th>
			<th>Transaction Date</th>
			<th>Total Payment</th>
			<th>ID No.</th>
			<th>Member Name</th>
			<th>Record(s)</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
			<td style="text-align: center;"><a href="#" id="<?php echo $key->orderno; ?>" onclick="be_trans.getDetailTrx(this)"><?php echo $key->orderno; ?></a></td>
			<td><?php echo $key->token; ?></td>
			<td><?php echo date("Y-m-d", strtotime($key->datetrans)); ?></td>
			<td style="text-align: right"><?php echo number_format($key->total_pay, 0,".", "."); ?></td>
			<td><?php echo $key->idno; ?></td>
			<td><?php echo $key->membername; ?></td>
			<td><?php echo $key->jum_record_memb; ?></td>
			<td style="text-align: center;"><input type="button" value="Detail" class="btn btn-warning" onclick="All.ajaxShowDetailonNextForm('be/trans/integrity/reconcileTrx/detailmember/<?php echo $key->orderno; ?>')"></td>
		</tr>
	<?php
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
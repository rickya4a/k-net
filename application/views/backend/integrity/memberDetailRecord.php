<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%" id="memberForm">
	<thead>
		<tr>
			<th colspan="9">List Trx K-Net Reg. Member Record Check</th>
		</tr>
		<tr>
			<th rowspan="2" style="text-align: center; vertical-align: middle;">Token No.</th>
			<th rowspan="2" style="text-align: center; vertical-align: middle;">Transaction No.</th>
			<th rowspan="2" style="text-align: center; vertical-align: middle;">ID No.</th>
			<th rowspan="2" style="text-align: center; vertical-align: middle;">Member ID #1</th>
			<th rowspan="2" style="text-align: center; vertical-align: middle;">ID Member</th>
			<th rowspan="2" style="text-align: center; vertical-align: middle;">Member Name</th>
			<th colspan='2'>Status</th>
		</tr>
		<tr>
			<th>SGO</th>
			<th>K-Wallet</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
			<td id="token"><?php echo $key->token; ?></td>
			<td><?php echo $key->trx_no; ?></td>
			<td><?php echo $key->idno; ?></td>
			<td><?php echo $key->memberid; ?></td>
			<?php if ($key->id_memb == NULL && ($key->sgo != NULL || $key->kwallet != NULL)) { ?>
					<td style="text-align: center;"><input class="btn btn-primary" type="button" value="Reconcile" onclick="postReq(
						'<?php echo $key->token; ?>',
						'<?php echo $key->trx_no; ?>',
						'<?php echo $key->idno; ?>',
						'<?php echo $key->memberid; ?>'
						)"></td>
<?php } else { ?>
				<td><?php echo $key->id_memb; ?></td>
<?php } ?>
			<td><?php echo $key->membername; ?></td>
			<td><?php echo $key->sgo; ?></td>
			<td><?php echo $key->kwallet; ?></td>
		</tr>
	<?php
	$i++;
	}; ?>
	</tbody>
</table>
<script>
function postReq(token, trx_no, idno, memberid) {
		All.set_disable_button();
		$.ajax({
				type: "POST",
				url: All.get_url('member/transaction_only/recon'),
				dataType:"json",
				data: {
			token: token,
			trx_no: trx_no,
			idno: idno,
			memberid: memberid
		},
		success: function (data) {
			alert(data.message);
			All.set_enable_button();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	});
}
</script>
<?php backToMainForm(); } ?>
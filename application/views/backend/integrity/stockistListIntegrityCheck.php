<?php 
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx Integrity Check</th>
		</tr>
		<tr>
			<th>Created By</th>
			<th>Stockist</th>
			<th>Trx. No.</th>
			<th>Order No.</th>
			<th>Updated</th>
			<th>ID Member</th>
			<th>Header BV</th>
			<th>Header DP</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
			<td style="text-align: center;"><?php echo $key->createnm ?></td>
			<td style="text-align: center;"><?php echo $key->loccd; ?></td>
			<td><?php echo $key->orderno; ?></td>
			<td><a href="#" onclick="javascript:be_trans_klink.getDetailProduct(this)" id="<?php echo $key->trcd; ?>"><?php echo $key->trcd; ?></a></td>
			<td><?php echo date("d F Y", strtotime($key->updatedt)); ?></td>
			<td><?php echo $key->dfno; ?></td>
			<td style="text-align: right"><?php echo number_format($key->tbv, 0,".", ","); ?></td>
			<td style="text-align: right"><?php echo number_format($key->tdp, 0,".", "."); ?></td>
		</tr>
	<?php 
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
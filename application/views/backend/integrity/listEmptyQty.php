<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx Integrity Check</th>
		</tr>
		<tr>
			<th>TRCD</th>
			<th>Member ID</th>
			<th>Total Payment</th>
			<th>Total BV</th>
			<th>Table</th>
			<th>Total TRCD</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
			<td><?php echo $key->trcd; ?></td>
			<td><?php echo $key->dfno; ?></td>
			<td style="text-align: right"><?php echo number_format($key->totpay, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->tbv, 0,".", "."); ?></td>
			<td><?php echo $key->tbl; ?></td>
			<td><?php echo $key->jum; ?></td>
		</tr>
	<?php
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
<?php
if (empty($result)) {
	echo setErrorMessage();
} else {
?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan = "11">List Data Trx Yang Tidak Masuk Ke Table NewTrh</th>
		</tr>
		<tr>
			<th>TRCD</th>
			<th>Token</th>
			<th>ETDT</th>
			<th>ID Member</th>
			<th>TBV</th>
			<th>Total</th>
			<th>CN</th>
			<th>Bill. KW</th>
			<th>Bonus Period</th>
			<th>TRCD_NewTrh</th>
			<th>Reconcile</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$i = 1;
		foreach ($result as $key) { ?>
			<tr>
				<?php
				echo "<td align=left><a href=# id=\"$key->trcd\" onclick=\"javascript:be_trans.getDetailTrx(this)\">$key->trcd</a></td>";
				?>
				<td><?php echo $key->token; ?></td>
				<td style="text-align: center;"><?php echo $key->etdt; ?></td>
				<td><?php echo $key->dfno; ?></td>
				<td style="text-align: right"><?php echo number_format($key->tbv, 0,".", ","); ?></td>
				<td style="text-align: right"><?php echo number_format($key->totpay, 0,".", ","); ?></td>
				<td><?php echo $key->CNno; ?></td>
				<td><?php echo $key->bill_kw; ?></td>
				<td style="text-align: center;"><?php echo $key->bnsperiod; ?></td>
				<td><?php echo $key->trcd_newtrh; ?></td>
				<td style="text-align: center"><a class="btn btn-success" onclick="All.ajaxShowDetailonNextForm('be/trans/integrity/reconcile/<?php echo $key->trcd; ?>')">Reconcile</a></td>
			</tr>
		<?php $i++; } ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
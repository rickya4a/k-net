<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx Integrity Check</th>
		</tr>
		<tr>
			<th>TRCD</th>
			<th>Member ID</th>
			<th>Bonus Period</th>
			<th>TDP</th>
			<th>Total Payment</th>
			<th>NDP</th>
			<th>NBV</th>
			<th>NPV</th>
			<th>TBV</th>
			<th>TPV</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
			<td><?php echo $key->trcd; ?></td>
			<td><?php echo $key->dfno; ?></td>
			<td><?php echo date("Y-m-d", strtotime($key->bnsperiod)); ?></td>
			<td style="text-align: right"><?php echo number_format($key->tdp, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->totpay, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->ndp, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->nbv, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->npv, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->tbv, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->tpv, 0,".", "."); ?></td>
		</tr>
	<?php
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
<?php 
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx Integrity Check</th>
		</tr>
		<tr>
			<th>Price Code/Stockist</th>
			<th>Order No.</th>
			<th>CN No.</th>
			<th>Updated</th>
			<th>ID Member</th>
			<th>Header BV</th>
			<th>Header DP</th>
			<th>Detail DP</th>
			<th>Detail BV</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$i = 1;
	foreach ($result as $key) { ?>
		<tr>
			<td><?php echo $key->pricecode. "/" .$key->loccd; ?></td>
			<?php
			$substr = substr($key->trcd, 0, 4);
			if($substr == "IDEC") {
			?>	
			<td><a href="#" id="<?php echo $key->trcd; ?>" onclick="be_trans.getDetailTrx(this)"><?php echo $key->trcd; ?></a></td>
			<?php
			} else {
			?>
			<td><?php echo $key->trcd; ?></td>
			<?php
			}
			?>
			<td><?php echo $key->trcd2; ?></td>
			<td><?php echo date("d F Y", strtotime($key->updatedt)); ?></td>
			<td><?php echo $key->dfno; ?></td>
			<td style="text-align: right"><?php echo number_format($key->tbv, 0,".", ","); ?></td>
			<td style="text-align: right"><?php echo number_format($key->tdp, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->xDP, 0,".", "."); ?></td>
			<td style="text-align: right"><?php echo number_format($key->xbv, 0,".", ","); ?></td>
		</tr>
	<?php 
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
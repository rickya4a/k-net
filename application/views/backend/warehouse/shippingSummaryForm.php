<?php
   if(isset($err_msg)) {
   	  echo setErrorMessage($err_msg);
   } else {
?>
<style>
	button {
		margin-top: 2px;
	}
</style>
<div class="mainForm">
  <!--<form class="form-horizontal" id="formPrintShippingLabel" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" > -->
  <form class="form-horizontal" id="formShippingSummary" action="<?php echo base_url('wh/shipping/summary/print') ?>" method="POST" target="_BLANK" onsubmit="return All.checkMultipleCheckbox('orderno[]')" >
    <fieldset>      
      <div class="control-group">       
       <label class="control-label" for="typeahead">Transaction Date</label>
         <div class="controls">
           <input type="text" class="dtpicker typeahead" id="ship_form" name="from" >&nbsp;to&nbsp;
		   <input type="text"  class="dtpicker typeahead" id="ship_to" name="to" >
		 </div>
		 
	   <label class="control-label" for="typeahead">Warehouse</label>
         <div class="controls">
           <select id="whcd" name="whcd">
           	  <?php
           	    foreach($list_wh as $dta) {
           	    	echo "<option value=\"$dta->whcd\">$dta->description</option>";
           	    }
           	  ?>
           </select>
		 </div>	 
	   <label class="control-label" for="typeahead">&nbsp;</label>
         <div class="controls">	  
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'wh/shipping/summary/list')" />
            <button tabindex="3" type="submit" class="btn btn-success" name="printlabel" value="1">Print Label</button>
            
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
            <input type="hidden" name="CNStatus" value="0" />
          </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
	
	function checkValidationShippingSum() {
		var atLeastOneIsChecked = $(All.get_active_tab() + ' input[name="orderno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			alert("Please select at least one transaction..");
			return false;
		} 
	}
</script>
<?php
}
?>
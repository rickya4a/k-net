<?php
    $pdf=new FPDF('P','mm', 'A4');
    $pdf->Open();
    $pdf->SetFillColor(255,255,255); // background = biru muda
    $pdf->SetTextColor(0,0,0);	 //	font color = black
    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
    $pdf->SetLineWidth(.1);
    //$pdf->AddPage();
	
	//$pdf->SetXY(10,50);
        
    $titleCol2 = 155;
    $lebarCell = 5;
	$left = 30;
	$border = 0;
	
	 //setting header product column width
    $no_urut = 10;
    $product_id = 30;
    $product_name = 120;
    $qty = 10;
    //$dp = 23;
   // $bv = 8;
    $kolom_total = $no_urut + $product_id + $product_name + $qty ;
    //$total_dp = 23;
    //$total_bv = 15;
	
    $pdf->SetFillColor(255,255,255);
    $pdf->SetFont('Arial','',10);
	
	    $pdf->AddPage();	
	    $pdf->Cell(185,$lebarCell,"SHIPPING SUMMARY",1,1,'C',true); 
	    $pdf->Ln();
	    
		$pdf->Cell($left,$lebarCell,"Warehouse :",$border,0,'R',true);
	    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", strtoupper($form['whcd'])),$border,1,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell,"Order No :",$border,0,'R',true);
	    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", $result[0]->orderno),$border,1,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell,"Conote No :",$border,0,'R',true);
	    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", $result[0]->conoteJNE),$border,1,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell,"Trx Date :",$border,0,'R',true);
	    $pdf->MultiCell($titleCol2,$lebarCell,$form['from']." - ".$form['to'],$border,1,'L',true);
		$pdf->Ln();
		$pdf->Ln();
			
			
		$kolom_header = $no_urut + $product_id + $product_name + $qty;
		//$pdf->Ln();
		$kiri = $no_urut + $product_id;
		$pdf->Cell($kolom_header,$lebarCell,"DETAIL PRODUCT",1,0,'C',true);
		$pdf->Ln();
	    $pdf->Cell($no_urut,$lebarCell,"No",1,0,'C',true);
	    $pdf->Cell($product_id,$lebarCell,"ID Product",1,0,'C',true);
	    $pdf->Cell($product_name,$lebarCell,"Product Name",1,0,'C',true);
	    $pdf->Cell($qty,$lebarCell,"Qty",1,0,'C',true);
	    $pdf->Ln();
	    $x = 1;
		   
		    $total_qty = 0;
		    
		    foreach($result as $dta2)
		    {
		        $pdf->Cell($no_urut, $lebarCell,$x,1,0,'R',true);
		        $pdf->Cell($product_id, $lebarCell,"$dta2->prdcd",1,0,'C',true);
		        $pdf->Cell($product_name, $lebarCell,$dta2->prdnm,1,0,'L',true);
		        $pdf->Cell($qty, $lebarCell,number_format($dta2->qty,0,".",","),1,0,'R',true);
		        $pdf->Ln();
		       
		        $total_qty += $dta2->qty;
		       $x++;
		    }
		    $tot = $no_urut + $product_id + $product_name;
			
		    $pdf->Cell($tot, $lebarCell,"T O T A L",1,0,'C',true);
	        $pdf->Cell($qty, $lebarCell,number_format($total_qty,0,".",","),1,0,'R',true);
	        //$pdf->Cell($tot2, $lebarCell,"",1,0,'R',true);
	        $pdf->Ln();
			$pdf->Ln();
		    $pdf->Ln();
		
	    $pdf->Cell(60,$lebarCell,"Printed By",0,0,'L',true);
	    $pdf->Cell(60,$lebarCell,"Shipped By",0,0,'L',true);
	    $pdf->Cell(60,$lebarCell,"Approved By",0,0,'L',true);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(60,$lebarCell,"(".$this->username.")",0,0,'L',true);
	    $pdf->Cell(60,$lebarCell,"( Courir Company )",0,0,'L',true);
	    $pdf->Cell(60,$lebarCell,"( Warehouse Manager/Spv )",0,0,'L',true);
	$pdf->Output();
?>
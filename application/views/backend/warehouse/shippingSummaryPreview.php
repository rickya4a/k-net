<?php
	if(isset($err_msg)) {
		echo $err_msg;
	} else {
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr><th colspan=7>Shipping List</th></tr>";
		echo "<tr>";
		echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		echo "<th width='5%'>No</th>";
		
		echo "<th width='15%'>Order No</th>";
		echo "<th width='15%'>Conote JNE</th>";
		echo "<th width='15%'>Pickup No</th>";
		echo "<th>Stockist</th>";
		echo "<th width='10%'>Date</th>";
		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($result as $data) {
			echo "<tr>";
			echo "<td align=center><input type=checkbox id=pil$i name=orderno[] value=\"$data->orderno\" /></td>";
			echo "<td align=right>$i</td>";
			echo "<td align=center><a id=\"$data->orderno\" onclick=\"All.ajaxShowDetailonNextForm('wh/shipping/detail/$data->orderno')\">$data->orderno</a></td>";
			echo "<td align=center>$data->conoteJNE</td>";
			echo "<td align=center>$data->pickup_number</td>";
			echo "<td align=left>$data->idstk - $data->scnm</td>";
			echo "<td align=center>$data->datetrans</td>";
			
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
	}
?>
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>
<?php
	if(isset($err_msg)) {
		echo $err_msg;
	} else {
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr><th colspan=4>Detail List Product</th></tr>";
		echo "<tr>";
		echo "<th width='8%'>No</th>";
		echo "<th width='20%'>Product Code</th>";
		echo "<th>Product Name</th>";
		echo "<th width='20%'>Qty</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($result as $data) {
			echo "<tr>";
			echo "<td align=right>$i</td>";
			echo "<td align=center>$data->prdcd</td>";
			echo "<td align=left>$data->prdnm</td>";
			echo "<td align=right>$data->qty</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
	}
	echo "<input class='btn btn-small btn-warning' type='button' value='<< Back to Form' onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" />"
?>

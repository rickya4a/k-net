<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
				  <thead>
					  <tr>
						  <th width='3%'>No</th>
						  <th width='6%'>CN/MSN/MM#</th>
						  <th width='20%'>Receipt#</th>
						  <th width='5%'>CN Date</th>
						  <th width='5%'>Rec. Date</th>
			  			  <th width='20%'>Sockist</th>
					  </tr>
				  </thead>
			  <tbody>";
		$i = 1;
		foreach($result as $data) {
			$etdt = date("d-m-Y", strtotime($data->invoicedt));
			$createdt = date("d-m-Y", strtotime($data->createdt));
			echo "<tr>
				  <td align=right>$i</td>";
			/* .category, a.ordtype, a.invoiceno, a.invoicedt, a.dfno, c.fullnm, b.trcd, b.createdt, b.applyto 
			 */
			 $fullnm = str_replace(",", "", $data->fullnm);
			$param = "$data->invoiceno**$data->trcd**$etdt**$createdt**$data->dfno - $fullnm";
			echo "<td align=left><a id='$param' onclick=\"javascript:All.ajaxShowDetailonNextForm('wh/releasevcr/det/$param')\">$data->invoiceno</a></td>
				  <td align=left>$data->trcd</td>
				  <td align=left>$etdt</td>
				  <td align=left>$createdt</td>
				  <td align=left>$data->dfno - $data->fullnm</td>
				  </tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
	}
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
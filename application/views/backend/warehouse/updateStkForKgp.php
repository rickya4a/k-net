<div class="mainForm">
  <form class="form-horizontal" id="updStkKgp">
    <fieldset>      
      <div class="control-group">
      	
      	<label class="control-label" for="typeahead">Update Type</label>                             
        <div class="controls">
        	<select id="stkType" name="stkType" onchange="be_trans.listProvinceKgp()">
        		<option value="">--Select here--</option>
        		<option value="1">Empty Stockist</option>
        		<option value="2">Set New Stockist</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead">Province</label>                             
        <div class="controls">
        	<select id="provinsiNama" name="provinsiNama" class="span5">
        		
        	</select>
        </div>
        <span id="trcdno" style="display: none;">
          	<label class="control-label" for="typeahead">Value</label>                             
	        <div class="controls">
	        	<input type="text" id="paramValue" name="paramValue" class="span4" />
	        </div>
        </span>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id, 'wh/kgp/stk/list')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->

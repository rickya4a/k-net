<?php
if(empty($listbonusmemb)){
    echo setErrorMessage();
}else{

    ?>

    <div class="overflow-auto">
        <!-- <form id="summPromo" method="post" >-->
        <table width="100%" id="DataTable" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <tr >
                <th colspan="11" >Daftar Bonus Bulanan: <?php echo $idmemb." - ".$nama;?></th>
            </tr>
            <tr>
                <th>No</th>
                <th>Bulan</th>
                <th>Bonus Gross</th>
                <th>Potongan</th>
                <th>Bonus Nett</th>
                <th>Rekening</th>
                <th>Bank</th>
                <th>Tgl Transfer</th>
                <th>Tgl Tolakkan</th>
                <th>Alasan Tolakkan</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach($listbonusmemb as $row){

                $bonus_month= $row->bulan;
                if($bonus_month == 1){
                    $month = 'Jan';
                }elseif($bonus_month == 2){
                    $month = 'Feb';
                }elseif($bonus_month == 3){
                    $month = 'Mar';
                }elseif($bonus_month == 4){
                    $month = 'Apr';
                }elseif($bonus_month == 5){
                    $month = 'Mei';
                }elseif($bonus_month == 6){
                    $month = 'Juni';
                }elseif($bonus_month == 7){
                    $month = 'Juli';
                }elseif($bonus_month == 8){
                    $month = 'Agust';
                }elseif($bonus_month == 9){
                    $month = 'Sept';
                }elseif($bonus_month == 10){
                    $month = 'Okt';
                }elseif($bonus_month == 11){
                    $month = 'Nov';
                }elseif($bonus_month == 12){
                    $month = 'Des';
                }else{
                    $month = '';
                }

                $tax= $row->tax;
                $adm_bank= $row-> adm_bank;

                $tot_net= $row-> tot_net;
                $tot_gross= $row-> tot_gross;

                $potongan= $tot_gross - $tot_net;

                echo "<tr>";
                echo "<td align='center'>&nbsp;$bonus_month</td>";
                echo "<td align='center'>&nbsp;$month</td>";
                echo "<td align='right'>&nbsp;Rp ".number_format($row->tot_gross,0,'.','.')."</td> ";
                echo "<td align='right'>&nbsp;Rp ".number_format($potongan,0,'.','.')."</td> ";
                echo "<td align='right'>&nbsp;Rp ".number_format($row->tot_net,0,'.','.')."</td> ";
                echo "<td align='center'>&nbsp;$rek_num</td> ";
                echo "<td align='center'>&nbsp;$bank</td> ";
                echo "<td align='right'>&nbsp;</td> ";
                echo "<td align='right'>&nbsp;</td> ";
                echo "<td align='right'>&nbsp;</td> ";
                echo"</tr>";
            }
            /*echo "<tr>
                      <td colspan=\"4\" align='center'>Total</td>
                    <td align='center'>$totQty</td>
                  </tr>";*/
            ?>
            <!--<tr>
            <td colspan="6"></td>
            </tr>-->
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->

        <!--</form>-->
        <br />
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        sortTable();
        All.set_datatable();
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

    function sortTable() {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("dataTable");
        switching = true;
        /*Make a loop that will continue until
         no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.getElementsByTagName("TR");
            /*Loop through all table rows (except the
             first, which contains table headers):*/
            for (i = 0; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                 one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[0];
                y = rows[i + 1].getElementsByTagName("TD")[0];
                //check if the two rows should switch place:
                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                 and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }
</script>

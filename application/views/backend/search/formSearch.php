<div class="mainForm">
    <form class="form-horizontal" id="formSearch">
        <fieldset>
            <label class="control-label showDt" for="typeahead">LIPSTIK D'FLORA 1</label>
            <div class="controls showDt">
                <select id="prod1_color" name="prod1_color">
                    <option value="#">- warna -</option>
                    <option value="IDBC014-1">NUDE</option>
                    <option value="IDBC014-2">RED BLOOD</option>
                    <option value="IDBC014-3">RED HEART</option>
                    <option value="IDBC014-4">DEEP PINK</option>
                    <option value="IDBC014-5">RED PINK</option>
                    <option value="IDBC014-6">PEACH</option>
                </select>
                <br />
            </div>

            <label class="control-label showDt" for="typeahead">LIPSTIK D'FLORA 2</label>
            <div class="controls showDt">
                <select id="prod2_color" name="prod2_color">
                    <option value="#">- warna -</option>
                    <option value="IDBC014-1F">NUDE</option>
                    <option value="IDBC014-2F">RED BLOOD</option>
                    <option value="IDBC014-3F">RED HEART</option>
                    <option value="IDBC014-4F">DEEP PINK</option>
                    <option value="IDBC014-5F">RED PINK</option>
                    <option value="IDBC014-6F">PEACH</option>
                </select>
                <br />
            </div>

            <label class="control-label showDt" for="typeahead">CC CREAM</label>
            <div class="controls showDt">
                <select id="prod3_color" name="prod3_color">
                    <option value="#">- warna -</option>
                    <option value="IDBC015N">NATURAL</option>
                    <option value="IDBC015V">IVORY</option>
                </select>
                <br />
            </div>

            <label class="control-label showDt" for="typeahead">LOOSE POWDER</label>
            <div class="controls showDt">
                <select id="prod4_color" name="prod4_color">
                    <option value="#">- warna -</option>
                    <option value="IDBC016B">BEIGE</option>
                    <option value="IDBC016GB">GOLDEN BEIGE</option>
                </select>
                <br />
            </div>

            <label class="control-label showDt" for="typeahead">COMPACT POWDER</label>
            <div class="controls showDt">
                <select id="prod5_color" name="prod5_color">
                    <option value="#">- warna -</option>
                    <option value="IDBC016L">LIGHT</option>
                    <option value="IDBC016N">NATURAL</option>
                </select>
                <br />
            </div>

            <label class="control-label showDt" for="typeahead">BRUSH POWDER</label>
            <div class="controls showDt">
                <select id="prod6_color" name="prod6_color">
                    <option value="IDBC017BRF">ALL SIZE</option>
                </select>
                <br />
            </div>

            <label class="control-label showDt" for="typeahead">POUCH POWDER </label>
            <div class="controls showDt">
                <select id="prod7_color" name="prod7_color">
                    <option value="IDBC017PC">ALL SIZE</option>
                </select>
                <br />
                <br />
                <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/getPrdcd/act')" />
                <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            </div>
        </fieldset>
        <div class="result"></div>
    </form>
</div>
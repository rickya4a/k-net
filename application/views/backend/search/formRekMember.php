<div class="mainForm">
    <form class="form-horizontal" id="formSearch">
        <fieldset>
            <div class="control-group">
                <label class="control-label showDt" for="typeahead">ID Member / Name </label>
                    <div class="controls showDt">
                        <input tabindex="3" type="text" id="idname" name="idname" style="width: 250px;"/>
                        <br />
<!--                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'search/rekening_member/act')" />-->
                        <input type="button" name="btn" id="btn" value="Submit" class="btn btn-primary .submit">
                        <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
                    </div>
            </div>
        </fieldset>
        <div id="myTable">
<!--            <table width='100%' class='main table table-bordered'>-->
<!--                <thead>-->
<!--                    <tr>-->
<!--                        <td style="text-align: center"><b>ID Member</b></td>-->
<!--                        <td style="text-align: center"><b>Nama Member</b></td>-->
<!--                        <td style="text-align: center"><b>Rekening</b></td>-->
<!--                        <td style="text-align: center"><b>Nomor Rekening</b></td>-->
<!--                        <td style="text-align: center"><b>Bank</b></td>-->
<!--                        <td style="text-align: center"><b>Tanggal Rekening</b></td>-->
<!--                    </tr>-->
<!--                </thead>-->
<!--                <tbody>-->
<!---->
<!--                </tbody>-->
<!--            </table>-->
        </div>
    </form>
</div>

<script>
    $('#btn').click(function(){

        if($('#idname').val() != ''){

            $.ajax({
                type: "POST",
                url: All.get_url('search/rekening_member/act'),
                dataType: 'json',
                data: {idname: $('#idname').val()},
                success: function (data) {

                    if(data.response == "false"){
                        All.set_error_message(" .result", "No record found..!!");
                    }else{

                        $("#myTable").html(null);
                        var arrayData = data.arrayData;
                        var rowshtml = "";
                        rowshtml += "<table width='100%' class='main table table-bordered'>";
                        rowshtml += "<thead>";
                        rowshtml += "<th colspan='6'>LIST REKENING MEMBER</th>";
                        rowshtml += "<tr>";
                        rowshtml += "<td align=center><b>ID Member</b></td>";
                        rowshtml += "<td align=center><b>Nama Member</b></td>";
                        rowshtml += "<td align=center><b>Rekening</b></td>";
                        rowshtml += "<td align=center><b>Nomor Rekening</b></td>";
                        rowshtml += "<td align=center><b>Bank</b></td>";
                        rowshtml += "<td align=center><b>Tanggal Rekening</b></td>";
                        rowshtml += "</tr>";
                        rowshtml += "</thead>";

                        rowshtml += "<tbody>";
                        $.each(arrayData, function(key, value){

                            rowshtml += "<tr>";
                            rowshtml += "<td align=center>" + value.IDMEMBER + "</td>";
                            rowshtml += "<td align=center>" + value.NMMEMBER + "</td>";
                            rowshtml += "<td align=center>" + value.REKNAME + "</td>";
                            rowshtml += "<td align=center>" + value.REKNUM + "</td>";
                            rowshtml += "<td align=center>" + value.BANKCODE + "</td>";
                            rowshtml += "<td align=center>" + value.tgl + "</td>";

                            rowshtml += "</tr>";
                        });
                        rowshtml += "</tbody></table>";
                        $("#myTable").html(rowshtml);
                    }

                }
            });
        }else{

            alert("Input ID Member/Member Name please...");
        }
    });
</script>

<?php
$thn_before=date('Y', strtotime('-1 year'));
?>

<div class="mainForm">
    <form class="form-horizontal" id="formTrxReport" method="post" target="_blank">
        <fieldset>
            <div class="control-group">

                <label class="control-label" for="typeahead">ID Member</label>
                <div class="controls">
                    <input type="text" id="id_member" name="id_member" class="span4" placeholder="Masukkan ID Member" onchange="getName()"/>
                </div>

                <label class="control-label" for="typeahead">Nama Member</label>
                <div class="controls">
                    <input type="text" id="nm_member" name="nm_member" class="span4" placeholder="Otomatis"  readonly/>
                </div>

                <label class="control-label" for="typeahead">Periode</label>
                <div class="controls">
                    <select id="tahun" name="tahun">
                        <option value="<?php echo date("Y");?>"><?php echo date("Y");?></option>
                        <option value="<?php echo $thn_before;?>"><?php echo $thn_before;?></option>
                    </select>
                </div>


                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'search/bonus_member/act')" />
                    <!--<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListTrxRebook()" />-->
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->

<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });

    function getName(){
        var idmember = document.getElementById("id_member").value;

        //alert(idmember);
        $.ajax({
            type: "POST",
            url: "https://www.k-net.co.id/search/getNameMemb",
            data: {idmemb: idmember},
            dataType: 'json',
            success: function(result){

                if(result.response == "TRUE"){
                    //alert("Nama Member: "+result.nama);
                    document.getElementById("nm_member").value= result.nama;
                }else{
                    alert("ID Member Tidak Ditemukan!");
                }

            }
        });
    }
</script>

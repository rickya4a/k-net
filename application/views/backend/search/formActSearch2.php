<?php

if($result == null){

    setErrorMessage();
}else {
?>

<table width='100%' class='main table table-bordered'>
    <thead>
    <tr>
        <td style="text-align: center"><b>Product Code</b></td>
        <td style="text-align: center"><b>Product Name</b></td>
    </tr>
    </thead>
    <tbody>

    <?php

    foreach ($result as $dt) {

    ?>

    <tr>
        <td align="center">
            <?php echo $dt->prdcd; ?>
        </td>
        <td align="left">
            <?php echo $dt->prdnm; ?>
        </td>
    </tr>
    </tbody>
</table>

<?php
}
?>

<table width='100%' class='main table table-bordered'>
    <thead>
    <tr>
        <th colspan="2">DETAIL</th>
    </tr>
    <tr>
        <td align="center"><b>Product Code</b></td>
        <td align="center"><b>Product Name</b></td>
    </tr>
    </thead>
    <tbody>

    <?php

    foreach ($detail as $dx) {

        ?>

        <tr>
            <td align="center">
                <?php echo $dx->prdcdDet; ?>
            </td>
            <td align="left">
                <?php echo $dx->prdcdNmDet; ?>
            </td>
        </tr>

        <?php
    }
    }
    ?>

    </tbody>
</table>
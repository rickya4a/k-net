<?php

if($result == null){

    setErrorMessage();
}else {
    ?>

    <table width='100%' class='main table table-bordered'>
    <thead>
    <tr>
        <td style="text-align: center"><b>Stockist</b></td>
        <td style="text-align: center"><b>ID Member</b></td>
        <td style="text-align: center"><b>Nama Member</b></td>
        <td style="text-align: center"><b>Nama Rekening</b></td>
        <td style="text-align: center"><b>Nomor Rekening</b></td>
        <td style="text-align: center"><b>Bank</b></td>
        <td style="text-align: center"><b>Tanggal Rekening</b></td>
    </tr>
    </thead>
    <tbody>

    <?php


    foreach ($result as $a) {

        ?>

        <tr>
            <td align="center"><?php echo $a->STOCKIST ?></td>
            <td align="center"><?php echo $a->IDMEMBER ?></td>
            <td align="center"><?php echo $a->NAME ?></td>
            <td align="center"><?php echo $a->REKNAME ?></td>
            <td align="center"><?php echo $a->REKNUM ?></td>
            <td align="center"><?php echo $a->BANKCODE ?></td>
            <td align="center"><?php echo date("d-m-Y", strtotime($a->ADDDATE)); ?></td>
        </tr>

        <?php

    }
}
?>
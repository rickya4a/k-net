<script type="text/javascript">
	function validate(){
		var name = document.forms["regMember"]["fullnm"].value;
		var ktp = document.forms["regMember"]["ktpno"].value;
		var phone = document.forms["regMember"]["tel_hp"].value;
		var numbers = /^[0-9]+$/;
			if(name == null || name == "" || ktp == null || ktp == "" || phone == null || phone == ""){
				alert("Tidak Boleh Kosong");
				return false;
			}
			
			if(!ktp.match(numbers) || !phone.match(numbers)){
				alert("No. KTP & No. Hp Harus Angka");
				return false;
			}
		}
</script>
<div class="mainForm">
    	  <div class="control-group">                      	  	
           <?php 
				$attr = array("class" => "form-horizontal", "name" => "regMember", "autocomplete" => "off", "onsubmit" => "return validate();");
				$hidden = array("regMember" => "regMember");
				echo form_open("be/member/insert", $attr, $hidden);
			?>             
                        
                        <label class="control-label">Nama<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="1" style="width: 20%;" class="uppercase" type="text" id="fullnm" name="fullnm" />
    	            </div>
				
				         <label class="control-label">No. KTP<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
                        <input tabindex="2" style="width: 20%;" class="required only-number" type="text" id="ktpno" name="ktpno" onchange="be_member.cekKtp(this.value)"/>
                    </div>
                    
                        <label  class="control-label">Jenis Kelamin<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <select tabindex="3" class="form-list required" id="sex" name="sex">
    	                	<option value="M">Pria</option>
    	                	<option value="F">Wanita</option>
    	                </select>
    	            </div>
                                        
    	                <label class="control-label">No Telp<font color="red">&nbsp;* </font></label>  
    	            <div class="controls">
    	                <input tabindex="4" style="width: 20%" class="required only-number" type="text" id="tel_hp" name="tel_hp" placeholder="Handphone" onchange="be_member.cekTlp(this.value)"/>&nbsp;
    	            </div>

    	            <p></p>
    	            
    	            	<div id="inp_btn" class="controls">
							<input id="btn_input_user" class="btn btn-primary .submit" type="submit" value="Submit" name="save" tabindex="20">
							<input class="btn btn-reset" type="reset" value="Reset">
						</div>
					</div>
			<?php
				echo form_close();
			?>
	</form>
</div>

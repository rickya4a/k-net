<div class="mainForm">
	<form class="form-horizontal" id="formRecruitMember" method="POST" action="<?php echo base_url('be/member/recruit/list/excell') ?>">
		<fieldset>
			<div class="control-group">
				<label class="control-label showDt" for="typeahead">Join Date</label>
				<div class="controls showDt">
					<input type="text" class="dtpicker" id="rekrut_from" name="from" tabindex="1" />
					&nbsp;&nbsp;to&nbsp;
					<input type="text"  class="dtpicker" id="rekrut_to" name="to" tabindex="1" />
                </div>
			
				<label class="control-label showDt" for="typeahead">Recruiter ID</label>
				<div class="controls showDt">
					<input tabindex="3" type="text" id="recruiterid" style="width: 250px;" name="recruiterid" onchange="All.getFullNameByID(this.value,'followup/memb/dfno',' #recruitername')" />
					
					<input type="text" readonly="readonly" style="width: 450px;" id="recruitername" name="recruitername" />
				</div>
			
				<label class="control-label showDt" for="typeahead">&nbsp;</label>
				<div class="controls">
					<input tabindex="4" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/member/recruit/list')" />
						<input type="submit" class="btn btn-success" value="Print to Excel" />
				    <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
			    </div>
			</div>
			<!-- end control-group -->
		</fieldset>

		<div class="result"></div>
	</form>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() {
		$(".dtpicker").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
		});
		
	}); 
</script>

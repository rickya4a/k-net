<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=reportMemberListReconBangkok.xls");
header("Pragma: no-cache");
header("Expires: 0");

if (empty($member)) {
    echo setErrorMessage();
} else { ?>

<table width="100%" border="1" class="table table-striped">
    <thead>
    <tr>
        <th>No</th>
        <th>No Trx</th>
        <th>No Token</th>
        <th>Nama Member</th>
        <th>No KTP</th>
        <th>Tgl Trx</th>
        <th>KTP Sudah Terdaftar</th>
        <th>Konfirmasi Member</th>
    </tr>
    </thead>
    <?php

        $no = 1;
        foreach($member as $list){
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td  align="center"><?php echo $list->orderno;?></td>
        <td><?php echo $list->token;?></td>
        <td align="center"><?php echo $list->membername; ?></td>
        <td align="center"><?php echo "'".$list->idno;?></td>
        <td align="center"><?php echo $list->datetrans;?></td>
        <td align="center"><?php echo $list->ktp_sudah_terdaftar;?></td>
        <td align="center"><?php echo $list->konfirmasi;?></td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>
<?php
    if(empty($member)){
        setErrorMessage();
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th>No.</th>
        <th>ID</th>
        <th>Name</th>
        <th>Join Date</th>
        <th>Stockist</th>
        <th>Sponsor ID</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($member as $row){
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td  align="center"><a href = "#" id=" <?php echo $row->dfno;?>" onclick="All.ajaxShowDetailonNextForm('be/member/details/<?php echo $row->dfno;?>')"><?php echo $row->dfno;?></a></td>
        <td><?php echo $row->fullnm;?></td>
        <td align="center"><?php echo $row->jointdt;?></td>
        <td align="center"><?php echo $row->loccd;?></td>
        <td align="center"><?php echo $row->sponsorid;?></td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
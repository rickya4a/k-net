<?php
    if(empty($memberLP)){
        setErrorMessage();
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th>No.</th>
        <th>ID</th>
        <th>Name</th>
        <th>Join Date</th>
        <th>Stockist</th>
        <th>Sponsor ID</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($memberLP as $row){
    ?>
    <tr>
        <td align="right"><?php echo $no;?></td>
        <td  align="center"><a href = "#" id=" <?php echo $row->memberid;?>" onclick="All.ajaxShowDetailonNextForm('be/member/details/<?php echo $row->memberid;?>')"><?php echo $row->memberid;?></a></td>
        <td><?php echo $row->membername;?></td>
        <td align="center"><?php echo date('d M Y',strtotime($row->joindt));?></td>
        <td align="center"><?php echo $row->stk_code;?></td>
        <td align="center"><?php echo $row->sponsorid;?></td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
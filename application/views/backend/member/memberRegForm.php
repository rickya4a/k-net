<div class="mainForm">
    <div class="control-group">                      	  	
        <!--<form class="form-horizontal" id="formRegs" name="regMember" method="post" action="<?php //echo site_url('be/member/insert');?>">--> 
        <form class="form-horizontal" id="formRegs" name="regMember" method="post">
        <label class="control-label">Token ID<font color="red">&nbsp;* </font></label>
        <div class="controls">
    	       <!--<input tabindex="1" class="uppercase span7" type="text" id="orderid" name="orderid" onchange="be_member.getDtByOrderid(this.value);" placeholder="*Contoh RM15010001"/>-->
               <input tabindex="1" class="uppercase span7" type="text" id="tokenid" name="tokenid" placeholder="*Contoh RM160100011" onchange="be_member.checkExistIDmember(this.value)"/>
               <input type="hidden" id="orderid" name="orderid" />
        </div>
        
        <label class="control-label">Id Member<font color="red">&nbsp;* </font></label>
    	<div class="controls">
           <input class="uppercase span7" type="text" id="idmember" name="idmember" readonly="yes"/>
         </div>
         
         <label class="control-label">Nama Member<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <input tabindex="2" class="uppercase span7" type="text" id="membername" name="membername" />
        </div>
        
        <label class="control-label">Userlogin<font color="red">&nbsp;* </font></label>
    	<div class="controls">
           <input tabindex="3" class="uppercase span7" type="text" id="userlogin" name="userlogin" onchange="be_member.checkUserlogin(this.value)"/>
         </div>
                    
        <label class="control-label">Nama userlogin<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <input class="uppercase span7" type="text" id="userloginnm" name="userloginnm" readonly="yes"/>
        </div>
                    
        <label class="control-label">ID Sponsor<font color="red">&nbsp;* </font></label>
    	<div class="controls">
    	   
           <input tabindex="4" class="uppercase span7" type="text" id="sponsorid" name="sponsorid" onchange="be_member.checkSponsor(this.value,this.form.userlogin.value)"/>
 	    
         </div>
                    
        <label class="control-label">Nama Sponsor<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <input class="uppercase span7" type="text" id="sponsornm" name="sponsornm" readonly="yes"/>
        </div>
        
        <label class="control-label">ID Rekruiter<font color="red">&nbsp;* </font></label>
    	<div class="controls">
    	   
           <input tabindex="4" class="uppercase span7" type="text" id="rekruiterid" name="rekruiterid" onchange="be_member.checkSponsor1(this.value,this.form.userlogin.value)"/>
 	    
         </div>
                    
        <label class="control-label">Nama Rekruiter<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <input class="uppercase span7" type="text" id="rekruiternm" name="rekruiternm" readonly="yes"/>
        </div>
        		
	    <label class="control-label">No. KTP<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <input tabindex="5" class="required only-number span7" type="text" id="ktpno" name="ktpno" onchange="be_member.cekKtp(this.value)"/>
            <input type="hidden" name="prdcd" id="prdcd"/>
        </div>
        
        <label class="control-label">Tgl. Lahir<font color="red">&nbsp;* </font></label>
         <div class="controls">
             <?php echo datebirth_combo1(17, 80, 'birthdt','6','7','8'); ?>
         </div> 
        
        <label class="control-label">Alamat<font color="red">&nbsp;* </font></label>  
        <div class="controls">
            <input tabindex="9" class="uppercase span10" type="text" id="addr1" name="addr1" />
        </div>
              
       <label  class="control-label">Jenis Kelamin<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <select tabindex="10" class="form-list required" id="sex" name="sex">
            	<option value="M">Pria</option>
            	<option value="F">Wanita</option>
            </select>
        </div>
                                        
        <label class="control-label">No Hp<font color="red">&nbsp;* </font></label>  
        <div class="controls">
            <input tabindex="11" class="required only-number span7" type="text" id="tel_hp" name="tel_hp" onchange="be_member.cekTlp(this.value)"/>
        </div>
                    
        <label class="control-label">No Telp Rmh</label>  
        <div class="controls">
            <input tabindex="12" class="required only-number span7" type="text" id="tel_hm" name="tel_hm" />
        </div>
        
        <label class="control-label">Email</label>  
        <div class="controls">
            <input tabindex="13" class="required span7" type="text" id="email" name="email" />
        </div>
        
        <label class="control-label">Kartu & Stt Bonus<font color="red">&nbsp;*</font></label>
            <div class="controls">
	            <select tabindex="14" class="form-list required stk span7" id="idstk" name="idstk">
	            	<option value="0">--Pilih Stockist--</option>
                            <?php
                                foreach($idstk as $row){
                                    echo "<option value = \"$row->loccd\">".$row->loccd." - ".$row->fullnm." </option>";    
                                }
                                
                            ?>
	            </select>     
    	    </div>
        
        <label class="control-label">Rekening Bank</label>
            <div class="controls">
                <select tabindex="15" class="form-list" id="pilBank" name="pilBank">
               	    <option value="">--Pilih disini--</option>
	                 <?php
	                    foreach($listBank as $dta) {
	                    	echo "<option value=\"$dta->bankid\">$dta->description</option>";
	                    }
	                 ?>
                </select>
            </div>
        
         <label class="control-label">No Rekening</label>  
        <div class="controls">
            <input tabindex="16" class="required only-number span5" type="text" id="norek" name="norek" />
        </div>  
        	<div id="inp_btn" class="controls">
				<input id="btn_input_user" class="btn btn-primary .submit" type="button" value="S A V E" name="save" tabindex="17" onclick="All.ajaxPostResetField(this.form.id,'be/member/insert')"/>
				<input type="hidden" id="tipe_upd" name="tipe_upd" value="0"/>
				<input class="btn btn-reset" type="reset" value="Reset"/>
			</div>
            </form>
		</div>
    <div class="result"></div>
</div>
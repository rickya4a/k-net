<?php
    if(empty($member)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th>No.</th>
        <th>ID</th>
        <th>Nama</th>
        <th>No.KTP</th>
        <th>Alamat</th>
        <th>No. Handphone</th>
        <th>Password</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($member as $row){
    ?>
    
    <tr>
       
        <td><?php echo $no;?></td>
        <td><a href = "#" id=" <?php echo $row->dfno;?>" onclick="All.ajaxShowDetailonNextForm('be/member/details/<?php echo $row->dfno;?>')"><?php echo $row->dfno;?></a></td>
        <td><?php echo $row->fullnm;?></td>
        <td><?php echo $row->idno;?></td>
        <td><?php echo $row->addr1;?></td>
        <td><?php echo $row->tel_hp;?></td>
        <td><?php echo $row->password;?></td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
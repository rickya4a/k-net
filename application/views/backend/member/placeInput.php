<div class="mainForm">
	<form class="form-horizontal" id="formPlaceInput">
		<div class="control-group">
			<label class="control-label">Tempat</label>
    	            <div class="controls">
    	                <input tabindex="1" style="width: 20%;" class="uppercase" type="text" id="tempat" name="tempat" value="<?php echo $tempat;?>"/>
    	            </div>

			<label class="control-label">ID Distributor<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="2" style="width: 25%;" class="uppercase" type="text" id="dfno" name="dfno" placeholder="ID" onchange="be_member.get_id_info(this.value)"/>
    	            </div>

			<label class="control-label">Rank</label>
    	            <div class="controls">
    	                <input tabindex="3" style="width: 20%;" class="uppercase" type="text" id="ranknm" name="ranknm" placeholder="Rank"/>
    	                <input tabindex="4" type="hidden" id="currentrank" name="currentrank"/>
    	            </div>

			<label class="control-label">Period</label>
    	            <div class="controls">
    	                <input tabindex="5" style="width: 10%;" class="uppercase" type="text" id="period" name="period" placeholder="Bulan"/>&nbsp;/&nbsp;
    	                <input tabindex="6" style="width: 7%;" class="uppercase" type="text" id="year1" name="year1" placeholder="Tahun"/>
    	            </div>
			
			<label class="control-label">Tanggal</label>                             
        		<div class="controls">
        			<input tabindex="7" type="text" class="dtpicker typeahead" id="tanggal" name="tanggal" value="<?php echo $tanggal;?>"/>
       			 </div>

			<p></p>

		   	<div class="controls"  id="inp_btn">
	            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_member.saveInput()"/>
	            <input tabindex="9" type="button" id="btn_input_user" class="btn btn-primary .submit" name="newplace" value="New Place" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
				<input type="reset" class="btn btn-reset" value="Reset" />
			</div>
		</div>
		<div class="result"></div>
	</form>
</div>
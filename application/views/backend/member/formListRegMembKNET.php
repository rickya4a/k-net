<div class="mainForm">
  <form class="form-horizontal" id="formMembKnet" method="POST" action="<?php echo base_url('member/knet/print') ?>">
    <fieldset>      
      <div class="control-group">       
        <label class="control-label showDt" for="typeahead">Join Date</label>
            <div class="controls showDt">
                <input type="text" class="dtpicker" id="joindt_from" name="from" />&nbsp;&nbsp;to&nbsp;
                <input type="text"  class="dtpicker" id="joindt_to" name="to" />
            
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/member/knet/list')" />
            <input type="submit" class="btn btn-success" value="Print to Excel" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(".dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

<?php
    if(empty($memdetails)){
        echo setErrorMessage("No result found");
        ?>

        <input value="<< Back " type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
        <br>

        <?php
    }else{
?>
   <form id="updMember">
	<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr>
        	<th colspan="4">Detail Data Member</th>
        </tr>	
        <?php
            foreach($memdetails as $row){
                $dfno = $row->dfno;
                $fullnm = $row->fullnm;
                $idno = $row->idno;
                $addr1 = $row->addr1;
                $addr2 = $row->addr2;
                $addr3 = $row->addr3;
                $tel_hp = $row->tel_hp;
				$tel_hm = $row->tel_hm;
                $sex = $row->sex;
				$birthdt = $row->birth;
				$join = $row->datejoin;
				$country = $row->country;
				$idsp = $row->idsponsor;
				$spnm = $row->sponsorname;
				$stckst = $row->stockist;
				$email = $row->email;
				$pass = $row->password;
                $novac = "88146-".$row->novac;
                $userinput = $row->createnm;
                $recruiter = $row->recruiter;
                $recruitnm = $row->recruiternm;			
          	}
        ?>	
        	
        	
			<tr>
                <td style="width: 13%">ID Member</td>
                <td style="width: 38%"><?php echo $dfno;?></td>
                 
                <td style="width: 13%">ID No</td>
                <td style="width: 38%"><?php echo $idno;?></td>
            </tr>
			<tr>
                <td>Member Name</td>
                <td><?php echo $fullnm;?></td>
            	 
            	<td>Sex</td>
            	<td><?php echo $sex;?></td>
            </tr>
			<tr>
                <td valign="top">Address</td>
                <td valign="top"><?php echo $addr1." ".$addr2." ".$addr3;?></td>
				 
            	<td valign="top">Cell Phone</td>
                <td valign="top"><?php echo $tel_hp;?></td>
            </tr>
			<tr>
                <td>ID Sponsor</td>
                <td><?php echo $idsp." / ".$spnm;?></td>
				 
            	<td>Birth Date</td>
                <td><?php echo $birthdt;?></td>
            </tr>
			<tr>
                <td>ID Recruiter</td>
                <td><?php echo $recruiter." / ".$recruitnm;?></td>
                 
            	<td>Join Date</td>
            	<td><?php echo $join;?></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td><?php echo $tel_hm;?></td>
                
                <td>City</td>
                <td><?php echo $country;?></td>
            </tr>
            <tr>
                <td>Registered in</td>
                <td id="upd_loccd"><?php echo $stckst;?></td>
				 
            	<td>Card & Bonus Stt</td>
                <td id="upd_bnsstmt"><?php 
                $stockist = $row->bnsstmsc." - ".$row->sc_fullnm;
                if($row->sc_fullnm != null and $row->sc_fullnm != ""){
                	$stockist = "$stockist <br />$row->sc_addr";
                }
                
                if($row->sc_city != null and $row->sc_city != ""){
                	$stockist = "$stockist <br />$row->sc_city";
                }
                
                echo $stockist; //$row->bnsstmsc." - ".$row->sc_fullnm." <br />$row->sc_addr<br />$row->sc_city";
                ?></td>
            </tr>
			<tr>
                <td>Password</td>
                <td><?php echo $pass;?></td>
                <td>Email</td>
                <td><?php echo $email;?></td>
            </tr>
            <tr>
                <td>No. VA</td>
                <td><?php echo $novac;?></td>
                <td>Input By</td>
                <td><?php echo $userinput;?></td>
            </tr>
             <tr>
                <td>Voucher No</td>
                <td><?php echo $memdetails[0]->formno;?></td>
                <td>Voucher Key</td>
                <td><?php echo $memdetails[0]->vchkey;?></td>
            </tr>
             <tr>
                <td>Starterkit Code</td>
                <td><?php echo $memdetails[0]->prdcd;?> ** Price (<?php echo number_format($memdetails[0]->dp);?>)</td>
                <td>Starterkit Desc</td>
                <td><?php echo $memdetails[0]->prdnm;?></td>
            </tr>
            <tr>
                <td>Registered using</td>
                <td><?php echo $memdetails[0]->keteranganx;?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
</table>
<?php

if(!empty($rekening)) {
?>
<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
     <tr>
        <tH colspan="5">Data Rekening</tH>
     </tr>      
     <tr>
        <td>Bank</td>
        <td>Account Name</td>
        <td>Account Number</td>
        <td>Update Date</td>
        <td width="30%">Remarks</td>
     </tr>  
     <?php
     foreach($rekening as $datarek) {
         echo "<tr>";
         echo "<td>$datarek->BANKCODE</td>";
         echo "<td>$datarek->REKNAME</td>";
         echo "<td>$datarek->REKNUM</td>";
         echo "<td>$datarek->ADDDATE</td>";
         echo "<td>$datarek->REMARK</td>";
         echo "</tr>";
     }
     ?>
</table>
<?php
}
?>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
		 <!--<input type="button" value="Save Changes" class="btn btn-small btn-primary" onclick="be_member.updateDataMember()" />-->
    </div>
	<p></p>
	</form>
<?php
    }
?>
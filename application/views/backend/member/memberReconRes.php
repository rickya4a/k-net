<?php 
    if(empty($getDt)){
        echo setErrorMessage('Input Gagal');
    }else{
?>
<div class="overflow-auto">
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        
        <tr>
            <td>ID Member</td>
            <td><?php echo $getDt[0]->dfno;?>&nbsp;</td>
            <td>Nama Member</td>
            <td><?php echo $getDt[0]->fullnm;?>&nbsp;</td>
        </tr>
        <tr>
            <td>ID Sponsor</td>
            <td><?php echo $getDt[0]->sponsorid;?>&nbsp;</td>
            <td>Nama Sponsor</td>
            <td><?php echo $getDt[0]->sponsorname;?>&nbsp;</td>
        </tr>
        <tr>
            <td>Statement Bonus</td>
            <td><?php echo $getDt[0]->loccd ." / ". $getDt[0]->loccd_name;?>&nbsp;</td>
            <td>Password</td>
            <td><?php echo $getDt[0]->password;?>&nbsp;</td>
        </tr>
    </table>
    <br />
</div>
<?php } ?>
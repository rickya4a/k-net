<div class="mainForm">
  <form class="form-horizontal" id="formMemberUpdate" method="post" >
    <fieldset>      
      <div class="control-group">
       <label class="control-label" for="typeahead">Orderno</label>
         <div class="controls">
           <input type="text" class="span5" id="orderno" name="orderno" onchange="be_member.getDetailMember(this.value)" placeholder="Contoh E1601000001" tabindex="1"/>
	       <input type="hidden" id="token" name="token"/>
         </div>
         
       <label class="control-label" for="typeahead">ID Sponsor</label>
         <div class="controls">
            <input type="hidden" name="userlogin" id="userlogin" value=""/>
           <input type="text" class="span5" id="sponsorid" name="sponsorid" onchange="be_member.checkSponsor(this.value,this.form.userlogin.value)" tabindex="2"/>
	     </div>
         
	   <label class="control-label" for="typeahead">Nama Sponsor</label>
         <div class="controls">
           <input readonly="readonly" type="text" class="span9" id="sponsornm" name="sponsorname" />
	     </div>
	     
	   <label class="control-label" for="typeahead">ID Rekruiter</label>
         <div class="controls">
           <input type="text" class="span5" id="rekruiterid" name="rekruiterid" onchange="be_member.checkSponsor1(this.value,this.form.userlogin.value)" tabindex="3"/>
	     </div>
         
	   <label class="control-label" for="typeahead">Nama Rekruiter</label>
         <div class="controls">
           <input readonly="readonly" type="text" class="span9" id="rekruiternm" name="rekruiternm" />
	     </div> 
       
       <label class="control-label" for="typeahead">ID Member</label>
         <div class="controls">
           <input type="text" class="span5" id="memberid" readonly="readonly" name="memberid"/>
	     </div>
         
	   <label class="control-label" for="typeahead">Nama Member</label>
         <div class="controls">
           <input type="text" class="span9" id="membername" name="membername" tabindex="3"/>
	     </div>
	   
        <label class="control-label" for="typeahead">No Ktp</label>
         <div class="controls">
           <input type="text" class="span9" id="ktpno" name="ktpno" tabindex="4" onchange="be_member.cekKtp(this.value)"/>
           <input type="hidden" name="prdcd" id="prdcd"/>
	     </div>
         
        <label class="control-label">Tgl. Lahir<font color="red">&nbsp;* </font></label>
         <div class="controls">
             <?php echo datebirth_combo1(17, 80, 'birthdt','5','6','7'); ?>
         </div> 
        
         <label class="control-label">Alamat<font color="red">&nbsp;* </font></label>  
        <div class="controls">
            <input tabindex="8" class="uppercase span10" type="text" id="addr1" name="addr1" tabindex="5"/>
        </div>
        
        <label  class="control-label">Jenis Kelamin<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <select tabindex="9" class="form-list required" id="sex" name="sex">
            	<option value="M">Pria</option>
            	<option value="F">Wanita</option>
            </select>
        </div>
        
        <label class="control-label">No Hp<font color="red">&nbsp;* </font></label>  
        <div class="controls">
            <input tabindex="10" class="required only-number span7" type="text" id="tel_hp" name="tel_hp" onchange="be_member.cekTlp(this.value)"/>
        </div>
                    
        <label class="control-label">No Telp Rmh</label>  
        <div class="controls">
            <input tabindex="11" class="required only-number span7" type="text" id="tel_hm" name="tel_hm" />
        </div>
        
        <label class="control-label">Email</label>  
        <div class="controls">
            <input tabindex="12" class="required span7" type="text" id="email" name="email" />
        </div>
        
       <label class="control-label" for="typeahead">Registered in</label>
         <div class="controls">
           <input readonly="readonly"  type="text" id="loccd" name="loccd" />
           <input readonly="readonly" type="text" id="loccd_name" name="loccd_name" style="width: 500px" />
	     </div>
	   <label class="control-label" for="typeahead">Card & Bonus Statement</label>
         <div class="controls">
           <input type="text" tabindex="13" id="bnsstmt" name="bnsstmt" onchange="be_member.getDetailStockist(this.value,'bnsstmt_name')"/>
           <input readonly="readonly" type="text" id="bnsstmt_name" name="bnsstmt_name" style="width: 500px" />
	     </div>
      
      <label class="control-label">Rekening Bank</label>
            <div class="controls">
                <select tabindex="14" class="form-list" id="pilBank" name="pilBank">
               	    <option value="">--Pilih disini--</option>
	                 <?php
	                    foreach($listBank as $dta) {
	                    	echo "<option value=\"$dta->bankid\">$dta->description</option>";
	                    }
	                 ?>
                </select>
            </div>
        
         <label class="control-label">No Rekening</label>  
        <div class="controls">
            <input tabindex="15" class="required only-number span5" type="text" id="norek" name="norek" />
        </div>  
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="16" type="button" id="btn_input_user" class="btn btn-primary .submit" name="Update" value="Submit" 
            	   onclick="All.ajaxPostResetField(this.form.id,'be/member/update/save')" />
            <input type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  </form> 
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

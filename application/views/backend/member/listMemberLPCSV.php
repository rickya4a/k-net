<?php
    if(empty($memberLP)){
        echo setErrorMessage();
    }else{
        $filenm = "report".$from."-".$to.".csv";
    
        
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filenm.'');
        
        $output = fopen('php://output', 'r');
        
        fputcsv($output, array('No','ID Sponsor', 'ID Member', 'Nama Member', 'No KTP', 'Tgl Join', 'Alamat', 'Tgl Lahir', 
        					   'No Hp', 'Email', 'ID Stockist', 'TRX No', 'User Login', 'ID Rekruiter', 'ID LandingPage', 'Propinsi'),';',' ');
							   
        $num = 1;
        foreach($memberLP as $row){
        	$nmmember = str_replace(",","",$row->membername);
            if($row->sponsorid == '0000999'){
                $sponsorid = "'".$row->sponsorid;
            }else{
                $sponsorid = $row->sponsorid;
            }
            $x = array('No' =>$num,
                        'Sponsor_id' => $sponsorid,
                        'Member_ID' =>$row->memberid,
                        'Member_Name' => $row->membername,
                        'No_KTP' => $row->idno,
                        'Date_Join' => date("d/m/y",strtotime($row->joindt)),
                        'Alamat' => $row->addr1,
                        'Birthday' => date("d/m/y",strtotime($row->birthdt)),
                        'No_Hp' => "'".$row->tel_hp,
                        'Email' => $row->email,
                        'Stockist' => $row->stk_code,
                        'Trx_No' => $row->trx_no,
                        'Userlogin' => $row->userlogin,
                        'Rekruiter_ID' => $row->recruiterid,
                        'LP_ID' => $row->id_landingpage,
                        'Propinsi' => $row->description,
                        );
            $num++;
            fputcsv($output,$x,';',' ');
            
        }
        fclose($output);
    }
?>
<?php
  if($listReconciledBangkok == null) {
  	 echo setErrorMessage();
  } else { ?>
  	 
<?php
  	 //echo "<form id=losPengunduranDiri>";
  	 echo "<table width=\"100%\" align=center class=\"table table-bordered bootstrap-datatable datatable\">";
      echo "<thead><tr><th colspan=10>Recruit Member List</th></tr>";
			   echo "<tr>";
               
            //    echo "<th>No</th>";
			   echo "<th>No Trx</th>";
		       echo "<th>No Token</th>";
			   echo "<th>Nama Member</th>";
			   echo "<th>No KTP</th>";
			   echo "<th>Sponsor</th>";
			   echo "<th>Recruiter</th>";
		       echo "<th>Tgl Trx</th>";
               echo "<th>KTP Sudah Terdaftar</th>";
			   echo "<th>Terdaftar di No Trx</th>";
               echo "<th>Konfirmasi Member</th>";
               echo "</tr></thead><tbody>";
?>
    <?php
       $i = 1;
	   //$header = $listData[0]->keterangan;
       foreach($listReconciledBangkok as $list) {
        $url = 'member/reconcile/bangkok/list/detail/'.$list->dfno;
        $urlToken = 'member/reconcile/bangkok/update/'.$list->token;
		
		$warna = "";
		if($list->idno != null && $list->idno != '' && $list->sponsorid != null && $list->sponsorid != ''
		   && $list->recruiterid != null && $list->recruiterid != '' && $list->dfno == NULL) {
		   $warna = " bgcolor=lightgreen";	   
		   //$classBtn = "btn btn-mini btn-success";
	    } 
		echo "<tr $warna>";
    ?>
	           
       		   
				<td><div align=right><?php echo $list->orderno; ?></div></td>
				<td><div align=center><a href="#" onclick="javascript:All.ajaxShowDetailonNextForm('<?php echo $urlToken; ?>')"><?php echo $list->token; ?></div></td>
                <td><div align=left><?php echo $list->membername;?></div></td>
                <td><div align=center><?php echo $list->idno; ?></div></td>
				<td><div align=center><?php echo $list->sponsorid; ?></div></td>
				<td><div align=center><?php echo $list->recruiterid; ?></div></td>
                <td><div align=center><?php echo $list->datetrans; ?></div></td>
                <!-- <a href="#" onclick="javascript:All.ajaxShowDetailonNextForm('<?php echo $url; ?>')"> -->

                <?php 
                if ($list->dfno != NULL) {
                    echo "<td><a href=\"#\" onclick=\"javascript:All.ajaxShowDetailonNextForm('$url')\"><div align=center>$list->ktp_sudah_terdaftar</div></td>";
                } else {
                    echo "<td align=\"center\"><input type=\"button\" class=\"btn btn-mini btn-success\" value=\"Reconcile\" onclick=\"All.ajaxJsonGetResponse('trans/sgo/reconcile/saveMemb/2/$list->id_memb/$list->token');\"></td>";
                }
                ?>
                <td><div align=center><a id="<?php echo $list->no_transaksi; ?>" onclick="javascript:be_trans.getDetailTrx(this)"><?php echo $list->no_transaksi; ?></a></div></td>
				<td><div align=center><?php echo $list->konfirmasi; ?></div></td>
               </tr>
       <?php
       $i++;
       	}
        ?>
      
    <!-- </tr> -->
    </tbody>
    </table>
    <!-- </form> -->

	
	<?php
  }
?>

<script>
	$(document).ready(function() {
		
		$(All.get_active_tab() + " .datatable").dataTable( { 
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
			"sPaginationType": "bootstrap",
			"oLanguage": {
			},
	        "bDestroy": true
	    });
    	$(All.get_active_tab() + " .datatable").removeAttr('style');
	}); 
</script>
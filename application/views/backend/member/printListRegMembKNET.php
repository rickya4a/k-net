<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=LapProductSalesPerState.xls");
header("Pragma: no-cache");
header("Expires: 0");

    if(empty($member)){
        setErrorMessage();
    }else{
?>
    <table width="100%" border="1" class="table table-striped">
    <thead>
    <tr>
        <th>No.</th>
        <th>ID</th>
        <th>Name</th>
        <th>Join Date</th>
        <th>Stockist</th>
        <th>Sponsor ID</th>
    </tr>
    </thead>
    <?php
        $ssx = "";
        $no = 1;
        foreach($member as $row){
        	$ssx += "'".$row->dfno."', ";
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td  align="center"><?php echo $row->dfno;?></td>
        <td><?php echo $row->fullnm;?></td>
        <td align="center"><?php echo $row->jointdt; ?></td>
        <td align="center"><?php echo $row->loccd;?></td>
        <td align="center"><?php echo $row->sponsorid;?></td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
    
    echo $ssx;
?>
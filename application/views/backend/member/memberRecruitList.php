<?php
  if($listRecruit == null) {
  	 echo setErrorMessage();
  } else { ?>
  	 
<?php
  	 //echo "<form id=losPengunduranDiri>";
  	 echo "<table width=\"100%\" align=center class=\"table table-bordered bootstrap-datatable datatable\">";
      echo "<thead><tr><th colspan=6>Recruit Member List</th></tr>";
			   echo "<tr>";
			   
			   echo "<th>No</th>";
		       echo "<th>ID</th>";
			   echo "<th>Member Name</th>";
			   echo "<th>Tgl Join</th>";
		       echo "<th>Sponsor ID</th>";
			   echo "<th>Sponsor Name</th>";
			   echo "</tr></thead><tbody>";
       $i = 1;
	   //$header = $listData[0]->keterangan;
       foreach($listRecruit as $list) {
       	
       		   echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
				echo "<td><div align=center><input type=\"hidden\" id=\"dfno$i\" name=\"dfno[]\" value=\"$list->dfno\"/><a href=\"#\" onclick=\"Javascript:All.ajaxShowDetailonNextForm('followup/batch/id/".$list->dfno."')\">$list->dfno</a></div></td>";
				echo "<td><div align=center>$list->fullnm</div></td>";
				echo "<td><div align=center>$list->jointdt</div></td>";
				echo "<td><div align=center>$list->sponsorid</div></td>";
				echo "<td><div align=center>$list->sponsorname</div></td>";
				
				
               echo "</tr>";
              $i++; 
       
       	}
               
      
    //echo "</tr>";
    echo "</tbody></table>";
    //echo "</form>";
	?>
	
	<?php
  }
?>

<script>
	$(document).ready(function() {
		
		$(All.get_active_tab() + " .datatable").dataTable( { 
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
			"sPaginationType": "bootstrap",
			"oLanguage": {
			},
	        "bDestroy": true
	    });
    	$(All.get_active_tab() + " .datatable").removeAttr('style');
	}); 
</script>
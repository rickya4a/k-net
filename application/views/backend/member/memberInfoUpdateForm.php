<style>
	.fullname {
		width: 500px;
	}

	.idx {
		width: 200px
	}

	.small_select, .birthdt {
		width: 80px
	}

</style>
<div class="mainForm">
  <form class="form-horizontal" id="formMemberInfoUpdate" method="post" >
    <fieldset>
      <div class="control-group">
      <label class="control-label" for="typeahead">Member ID</label>
         <div class="controls">
           <input type="text" class="idx" id="memberid" name="memberid" onchange="be_member.getDataMemberInfo(this.value)" />
           <input type="text" class="fullname" id="membername" name="membername" tabindex="3" placeholder="Member Name"/>
           <input type="hidden" id="status_expire" name="status_expire" value="" />
           <input type="hidden" id="lbcmember" name="lbcmember" value="" />
	     </div>
	  <label class="control-label" for="typeahead">ID Number</label>
         <div class="controls">
           <input type="text" class="span5" id="ktpno" name="ktpno" readonly="readonly" >

	     </div>
      <label class="control-label" for="typeahead">Address</label>
         <div class="controls">
           <input type="text" class="span6" id="addr1" name="addr1" >

	     </div>
	  <label class="control-label" for="typeahead">&nbsp;</label>
         <div class="controls">
           <input type="text" class="span6" id="addr2" name="addr2" >

	     </div>

	   <label class="control-label" for="typeahead">&nbsp;</label>
         <div class="controls">
           <input type="text" class="span6" id="addr3" name="addr3" >

	     </div>
       <label class="control-label" for="typeahead">Birth Date</label>
         <div class="controls">
           <?php echo datebirth_combo(17, 80, 'birthdt'); ?>
	     </div>
	    <label class="control-label" for="typeahead">Sex</label>
         <div class="controls">
           <select id="sex" name="sex" class="small_select">
           	  <option value="M">Male</option>
           	  <option value="F">Female</option>
           </select>&nbsp;

	     </div>
       <label class="control-label" for="typeahead">Sponsor ID</label>
         <div class="controls">
           <input type="text" id="sponsorid" class="idx" name="sponsorid" readonly="readonly" />
           <input type="text" id="sponsorname" class="fullname" name="sponsorname" readonly="readonly" placeholder="Sponsor Name" />
	     </div>

	    <label class="control-label" for="typeahead">Registered In</label>
         <div class="controls">
           <input type="text" class="idx" id="loccd" name="loccd" onchange="be_member.getDetailStockist(this.value,'loccd_name')" readonly="readonly"/>
           <input readonly="readonly" class="fullname" type="text" id="loccd_name" name="loccd_name" readonly="readonly" />
	     </div>
	   <label class="control-label" for="typeahead">Card & Bonus Statement</label>
         <div class="controls">
           <input type="text" class="idx" id="bnsstmt" name="bnsstmt" onchange="be_member.getDetailStockist(this.value,'bnsstmt_name')"/>
           <input readonly="readonly" class="fullname" type="text" id="bnsstmt_name" name="bnsstmt_name" />
	     </div>
	   <label class="control-label" for="typeahead">Cellular Phone No</label>
         <div class="controls">
           <input type="text" class="idx" id="tel_hp" name="tel_hp" onchange="All.checkDoubleInput('db2/get/tel_hp/from/msmemb/','tel_hp',this.value)"/>

	     </div>
	    <span id="lbc_area" style="display: none;">
        <label class="control-label" for="typeahead">LBC Register Date </label>
         <div class="controls">
           <input type="text" class="idx" id="register_dt" name="register_dt" readonly="readonly"/>
         </div>
	     <label class="control-label" for="typeahead">LBC Expire Date </label>
         <div class="controls">
           <input type="text" class="idx" id="expired_dt" name="expired_dt" readonly="readonly" />
	     </div>
	     <label class="control-label" for="typeahead">Status</label>
         <div class="controls">
           <input type="text" class="idx" id="status_lbc" name="status_lbc" readonly="readonly" />
	     </div>
	     </span>
        <label class="control-label" for="typeahead">&nbsp</label>
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="Update" value="Update Member"
            	   onclick="All.ajaxPostResetField(this.form.id,'be/member/info/update')" />
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
            <input onclick="be_member.clear_expire_lbc()" tabindex="6" disabled="disabled" type="button" class="btn btn-warning" id="del_exp_lbc" name="del_exp_lbc" value="Clear Expired LBC Data" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div class="result"></div>
</div><!--/end mainForm-->


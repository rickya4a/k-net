<?php
    if(empty($repmember)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
  	  <thead>
			<tr>
				<th>No.</th>
				<th>No. Register</th>
				<th>Nama</th>
				<th>No. KTP</th>
				<th>No. Hp</th>
				<th>Jenis Kelamin</th>
				<th><button onclick="location.href='<?php echo base_url('be/member/excel');?>'">Print Excel</button></th>
				<th></th>
			</tr>		
  	  </thead>
			<?php
					$no=1;
					foreach($repmember as $row){
			?>
					<tr>
						<td><?php echo $no;?></td>
						<td><?php echo $row->registerno;?></td>
						<td><?php echo $row->fullnm;?></td>
						<td><?php echo $row->ktpno;?></td>
						<td><?php echo $row->tel_hp;?></td>
						<td><?php echo $row->sex;?></td>
						<td><a href="<?php echo base_url('be/member/edit/'.$row->registerno.'');?>">Edit</a></td>
						<td><a href="<?php echo base_url('be/member/delete/'.$row->registerno.'');?>">Delete</a></td>
					</tr>
			<?php
					$no++;
					}
			?>
	</table>
<?php
   }
?>  
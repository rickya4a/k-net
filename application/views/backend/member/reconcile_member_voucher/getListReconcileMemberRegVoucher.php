<?php
    if(empty($listMember)){
        setErrorMessage();
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th width="10%">Reconcile</th>
        <th>ID</th>
        <th>Name</th>
        <th>Join Date</th>
        <th>Stockist</th>
        <th>Sponsor ID</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($listMember as $row){
    ?>
    <tr id="<?php echo $row->memberid;?>">
        <td align="center"><input type="button" class="btn btn-mini btn-primary" value="Reconcile" onclick="be_member.reconcileMemberRegVoucher('<?php echo $row->memberid; ?>')" /></td>
        <td  align="center"><a href = "#" id=" <?php echo $row->memberid;?>" onclick="All.ajaxShowDetailonNextForm('be/member/details/<?php echo $row->memberid;?>')"><?php echo $row->memberid;?></a></td>
        <td><?php echo $row->membername;?></td>
        <td align="center"><?php echo $row->joindt;?></td>
        <td align="center"><?php echo $row->stk_code;?></td>
        <td align="center"><?php echo $row->sponsorid;?></td>
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
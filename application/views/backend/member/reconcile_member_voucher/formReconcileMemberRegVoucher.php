<div class="mainForm">
	<form class="form-horizontal" id="formListRegVoucherFail" method="POST" action="<?php echo base_url('be/member/recruit/list/excell') ?>">
		<fieldset>
			<div class="control-group">
				<label class="control-label showDt" for="typeahead">Join Date</label>
				<div class="controls showDt">
					<input type="text" class="dtpicker" id="jn_fail_from" name="from" tabindex="1" />
					&nbsp;&nbsp;to&nbsp;
					<input type="text"  class="dtpicker" id="jn_fail_to" name="to" tabindex="2" />
					<input tabindex="4" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/member/voucher/reconcile/list')" />
						<input type="submit" class="btn btn-success" value="Print to Excel" />
				    <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
			    </div>
			</div>
			
			<!-- end control-group -->
		</fieldset>

		<div class="result"></div>
	</form>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() {
		$(".dtpicker").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
		});
		
	}); 
</script>

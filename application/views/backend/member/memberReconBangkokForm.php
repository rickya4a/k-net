<div class="mainForm">
  <form class="form-horizontal" id="formMemberUpdate" method="post" >
    <fieldset>      
      <div class="control-group">
       <label class="control-label" for="typeahead">No Token</label>
         <div class="controls">
           <input type="text" id="orderno" style="width: 200px;" name="orderno" value="<?php echo $token; ?>" tabindex="1"/>&nbsp;
		   <!--<input type="button" value="Mampirkak Check Token" class="btn btn-mini btn-primary" onclick="checkNoToken()" />-->
		   &nbsp;<input type="button" value="Promo Bangkok Check Token" class="btn btn-mini btn-primary" onclick="checkNoToken2()" />&nbsp;<input type="button" value="Check Registration Member" class="btn btn-mini btn-primary" onclick="checkNoToken3()" />
	       <input type="hidden" id="token" name="token"/>
		   <input type="hidden" id="trx_id" name="trx_id" value=""/>
         </div>
         
       <label class="control-label" for="typeahead">ID Sponsor</label>
         <div class="controls">
            <input type="hidden" name="userlogin" id="userlogin" value=""/>
           <input type="text" class="span5" id="sponsorid" name="sponsorid" onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/msmemb/dfno','#sponsorname')" tabindex="2"/>
	     </div>
         
	   <label class="control-label" for="typeahead">Nama Sponsor</label>
         <div class="controls">
           <input readonly="readonly" type="text" class="span9" id="sponsorname" name="sponsorname" />
	     </div>
	     
	   <label class="control-label" for="typeahead">ID Rekruiter</label>
         <div class="controls">
           <input type="text" class="span5" id="rekruiterid" name="rekruiterid" onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/msmemb/dfno','#rekruiternm')" tabindex="3"/>
	     </div>
         
	   <label class="control-label" for="typeahead">Nama Rekruiter</label>
         <div class="controls">
           <input readonly="readonly" type="text" class="span9" id="rekruiternm" name="rekruiternm" />
	     </div> 
       
       <!--<label class="control-label" for="typeahead">ID Member</label>
         <div class="controls">
           <input type="text" class="span5" id="memberid" readonly="readonly" name="memberid"/>
	     </div>-->
         
	   <label class="control-label" for="typeahead">Nama Member</label>
         <div class="controls">
           <input type="text" class="span9" id="membername" name="membername" tabindex="3"/>
	     </div>
	   
        <label class="control-label" for="typeahead">No Ktp</label>
         <div class="controls">
           <input type="text" style="width: 200px;" id="idno" name="idno" tabindex="4" onchange="be_member.cekKtp(this.value)"/>
		   &nbsp;<input type="button" value="Check No KTP" class="btn btn-mini btn-primary" onclick="be_member.cekKtp(this.value)" />
           <input type="hidden" name="prdcd" id="prdcd"/>
	     </div>
         
        <label class="control-label">Tgl. Lahir<font color="red">&nbsp;* </font></label>
         <div class="controls">
             <?php echo datebirth_combo1(17, 80, 'birthdt','5','6','7'); ?>
         </div> 
        
         <label class="control-label">Alamat<font color="red">&nbsp;* </font></label>  
        <div class="controls">
            <input tabindex="8" class="uppercase span10" type="text" id="addr1" name="addr1" tabindex="5"/>
        </div>
        
        <label  class="control-label">Jenis Kelamin<font color="red">&nbsp;* </font></label>
        <div class="controls">
            <select tabindex="9" class="form-list required" id="sex" name="sex">
            	<option value="M">Pria</option>
            	<option value="F">Wanita</option>
            </select>
        </div>
        
        <label class="control-label">No Hp<font color="red">&nbsp;* </font></label>  
        <div class="controls">
            <input tabindex="10" class="required only-number span7" type="text" id="tel_hp" name="tel_hp" onchange="be_member.cekTlp(this.value)"/>
        </div>
                    
        <!--<label class="control-label">No Telp Rmh</label>  
        <div class="controls">
            <input tabindex="11" class="required only-number span7" type="text" id="tel_hm" name="tel_hm" />
        </div>-->
        
        <label class="control-label">Email</label>  
        <div class="controls">
            <input tabindex="12" class="required span7" type="text" id="email" name="email" />
        </div>
        
	   <label class="control-label" for="typeahead">Stockist Info</label>
         <div class="controls">
           <input type="text" tabindex="13" id="stk_code" name="stk_code" onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/mssc/loccd','#stk_code_name')" />
           <input readonly="readonly" type="text" id="stk_code_name" name="stk_code_name" style="width: 500px" />
		   <input type="hidden" id="tgljoin" name="tgljoin" value="" />
		   <input type="hidden" id="insert_type" name="insert_type" value="" />
		   <input type="hidden" id="state" name="state" value="" />
	     </div>
      
      <!--<label class="control-label">Rekening Bank</label>
            <div class="controls">
                <select tabindex="14" class="form-list" id="pilBank" name="pilBank">
               	    <option value="">--Pilih disini--</option>
	                 <?php
	                    foreach($listBank as $dta) {
	                    	echo "<option value=\"$dta->bankid\">$dta->description</option>";
	                    }
	                 ?>
                </select>
            </div>
        
         <label class="control-label">No Rekening</label>  
        <div class="controls">
            <input tabindex="15" class="required only-number span5" type="text" id="norek" name="norek" />
        </div> --> 
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <!--<input tabindex="14" type="button" id="btn_input_user" class="btn btn-primary .submit" name="Update" value="Submit" 
            	   onclick="All.ajaxFormPost(this.form.id,'trx/reconcile/null_member/save')" />-->
			<input tabindex="14" type="button" id="btn_input_user" class="btn btn-primary .submit" name="Update" value="Save" 
            	   onclick="ajaxFormPostRecon(this.form.id,'trx/reconcile/null_member/save')" />	   
            <input type="reset" class="btn btn-reset" value="Reset" />
            <?php backToMainForm(); ?>
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  <div class="resultRecon"></div>
</div><!--/end mainForm-->
<script>
function ajaxFormPostRecon(formID, url) {
		All.set_disable_button();
        //All.get_image_load2();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .resultRecon").html(null);
            $(All.get_active_tab() + " .resultRecon").html(data);   
        }).fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
}
 /*
 function checkNoToken() {
	var notoken = $(All.get_active_tab() + " #orderno").val();
	console.log(notoken);
	All.set_disable_button();
		$.ajax({
            url: All.get_url('trx/reconcile/token/check'),
            type: 'POST',
			dataType: "json",
			data: {notoken: notoken},	
            success:
            function(data){
            	All.set_enable_button();
				if(data.response == "true") {
					var arrayData = data.arrayData;
					$(All.get_active_tab() + " #membername").val(arrayData[0].nmmember);
					$(All.get_active_tab() + " #idno").val(arrayData[0].id_memb);
					$(All.get_active_tab() + " #tel_hp").val(arrayData[0].no_hp_konfirmasi);
					$(All.get_active_tab() + " #stk_code").val(arrayData[0].idstk);
					$(All.get_active_tab() + " #stk_code_name").val(arrayData[0].namastk);
					$(All.get_active_tab() + " #tgljoin").val(arrayData[0].tgljoin);
					$(All.get_active_tab() + " #prdcd").val("X");
					$(All.get_active_tab() + " #state").val(arrayData[0].state);
					$(All.get_active_tab() + " #trx_id").val(arrayData[0].orderno);
					
				} else {
					alert(data.message);
				}
            	
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
        }); 
 }
 */
 
 function checkNoToken2() {
	console.log("masuk sini..");
	var notoken = $(All.get_active_tab() + " #orderno").val();
	console.log(notoken);
	All.set_disable_button();
		$.ajax({
            url: All.get_url('trx/reconcile/token/check'),
            type: 'POST',
			dataType: "json",
			data: {notoken: notoken},	
            success:
            function(data){
            	All.set_enable_button();
				if(data.response == "true") {
					var arrayData = data.arrayData;
					var total_pay = parseInt(arrayData[0].total_pay);
					var generate_idmemb = arrayData[0].generate_idmemb;
					var generate_nmmemb = arrayData[0].generate_nmmemb;
					var temp_idmemb = arrayData[0].temp_idmemb;
					var temp_nmmemb = arrayData[0].temp_nmmemb;
					 
					if(total_pay > 700000 && generate_idmemb != null && generate_idmemb != "") {
						alert("Free Membership sudah di generate : " +generate_idmemb+ " / " +generate_nmmemb);
					} else if(total_pay > 700000 && temp_idmemb != null && temp_nmmemb != "") {
						alert("Data Free Membership ada..");
						$(All.get_active_tab() + " #sponsorid").val(arrayData[0].sponsorid);
						$(All.get_active_tab() + " #sponsorname").val(arrayData[0].sponsorname);
						$(All.get_active_tab() + " #rekruiterid").val(arrayData[0].recruiterid);
						$(All.get_active_tab() + " #rekruiternm").val(arrayData[0].recruitername);
						$(All.get_active_tab() + " #addr1").val(arrayData[0].addr1);
						$(All.get_active_tab() + " #sex").val(arrayData[0].sex);
						$(All.get_active_tab() + " #tgllhr").val(arrayData[0].tgllhr);
						$(All.get_active_tab() + " #blnlhr").val(arrayData[0].blnlhr);
						$(All.get_active_tab() + " #thnlhr").val(arrayData[0].thnlhr);
						$(All.get_active_tab() + " #membername").val(arrayData[0].temp_nmmemb);
						$(All.get_active_tab() + " #idno").val(arrayData[0].idno);
						$(All.get_active_tab() + " #tel_hp").val(arrayData[0].tel_hp);
						$(All.get_active_tab() + " #stk_code").val(arrayData[0].stk_code);
						$(All.get_active_tab() + " #stk_code_name").val(arrayData[0].stk_name);
						$(All.get_active_tab() + " #tgljoin").val(arrayData[0].tgljoin);
						$(All.get_active_tab() + " #prdcd").val("X");
						$(All.get_active_tab() + " #state").val(arrayData[0].state);
						$(All.get_active_tab() + " #trx_id").val(arrayData[0].orderno);
						$(All.get_active_tab() + " #insert_type").val("update");
					} else if(total_pay > 700000) {
						alert("Data Free Membership masih kosong..");
						$(All.get_active_tab() + " #insert_type").val("insert");
						$(All.get_active_tab() + " input[type=text]").val(null);
					} else {
						alert("Minimum pembelanjaan untuk free membership adalah 700000");
					}
					
				} else {
					alert(data.message);
				}
            	
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
        });
 }
 
 /*
 function checkNoToken3() {
	console.log("masuk sini..");
	var notoken = $(All.get_active_tab() + " #orderno").val();
	console.log(notoken);
	All.set_disable_button();
		$.ajax({
            url: All.get_url('trx/reconcile/token/check'),
            type: 'POST',
			dataType: "json",
			data: {notoken: notoken},	
            success:
            function(data){
            	All.set_enable_button();
				if(data.response == "true") {
					var arrayData = data.arrayData;
					var total_pay = parseInt(arrayData[0].total_pay);
					var generate_idmemb = arrayData[0].generate_idmemb;
					var generate_nmmemb = arrayData[0].generate_nmmemb;
					var temp_idmemb = arrayData[0].temp_idmemb;
					var temp_nmmemb = arrayData[0].temp_nmmemb;
					 
					if(generate_idmemb != null && generate_idmemb != "") {
						alert("Membership sudah di generate : " +generate_idmemb+ " / " +generate_nmmemb);
					} else if(temp_idmemb != null && temp_nmmemb != "") {
						alert("Data Free Membership ada..");
						$(All.get_active_tab() + " #sponsorid").val(arrayData[0].sponsorid);
						$(All.get_active_tab() + " #sponsorname").val(arrayData[0].sponsorname);
						$(All.get_active_tab() + " #rekruiterid").val(arrayData[0].recruiterid);
						$(All.get_active_tab() + " #rekruiternm").val(arrayData[0].recruitername);
						$(All.get_active_tab() + " #addr1").val(arrayData[0].addr1);
						$(All.get_active_tab() + " #sex").val(arrayData[0].sex);
						$(All.get_active_tab() + " #tgllhr").val(arrayData[0].tgllhr);
						$(All.get_active_tab() + " #blnlhr").val(arrayData[0].blnlhr);
						$(All.get_active_tab() + " #thnlhr").val(arrayData[0].thnlhr);
						$(All.get_active_tab() + " #membername").val(arrayData[0].temp_nmmemb);
						$(All.get_active_tab() + " #idno").val(arrayData[0].idno);
						$(All.get_active_tab() + " #tel_hp").val(arrayData[0].tel_hp);
						$(All.get_active_tab() + " #stk_code").val(arrayData[0].stk_code);
						$(All.get_active_tab() + " #stk_code_name").val(arrayData[0].stk_name);
						$(All.get_active_tab() + " #tgljoin").val(arrayData[0].tgljoin);
						$(All.get_active_tab() + " #prdcd").val("X");
						$(All.get_active_tab() + " #state").val(arrayData[0].state);
						$(All.get_active_tab() + " #trx_id").val(arrayData[0].orderno);
						$(All.get_active_tab() + " #insert_type").val("update");
					} else {
						alert("Data Free Membership masih kosong..");
						$(All.get_active_tab() + " #insert_type").val("insert");
						$(All.get_active_tab() + " input[type=text]").val(null);
					} 
					
				} else {
					alert(data.message);
				}
            	
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
        });
 } */
 
 function checkNoToken3() {
	console.log("masuk sini..");
	var notoken = $(All.get_active_tab() + " #orderno").val();
	console.log(notoken);
	All.set_disable_button();
		$.ajax({
            url: All.get_url('trx/reconcile/token/check2'),
            type: 'POST',
			dataType: "json",
			data: {notoken: notoken},	
            success:
            function(data){
            	All.set_enable_button();
				
				/*var arrayData = data.arrayData;
				if(data.response == "true") {
					
					var total_pay = parseInt(arrayData[0].total_pay);
					var generate_idmemb = arrayData[0].generate_idmemb;
					var generate_nmmemb = arrayData[0].generate_nmmemb;
					var temp_idmemb = arrayData[0].temp_idmemb;
					var temp_nmmemb = arrayData[0].temp_nmmemb;
					 
					if(generate_idmemb != null && generate_idmemb != "") {
						alert("Membership sudah di generate : " +generate_idmemb+ " / " +generate_nmmemb);
						$(All.get_active_tab() + " #insert_type").val("update");
					} else if(temp_idmemb != null) {
						alert("Data Free Membership ada..");
						
						$(All.get_active_tab() + " #insert_type").val("update");
					} else {
						alert("Data Free Membership masih kosong..");
						
						//$(All.get_active_tab() + " input[type=text]").val(null);
					} 
					
					
					
				} else {
					alert(data.message);
				} */
				
				alert(data.message);
				if(data.response == "blh_ganti") {
				    var arrayData = data.arrayData;  
					$(All.get_active_tab() + " #sponsorid").val(arrayData[0].sponsorid);
					$(All.get_active_tab() + " #sponsorname").val(arrayData[0].sponsorname);
					$(All.get_active_tab() + " #rekruiterid").val(arrayData[0].recruiterid);
					$(All.get_active_tab() + " #rekruiternm").val(arrayData[0].recruitername);
					$(All.get_active_tab() + " #addr1").val(arrayData[0].addr1);
					$(All.get_active_tab() + " #sex").val(arrayData[0].sex);
					$(All.get_active_tab() + " #tgllhr").val(arrayData[0].tgllhr);
					$(All.get_active_tab() + " #blnlhr").val(arrayData[0].blnlhr);
					$(All.get_active_tab() + " #thnlhr").val(arrayData[0].thnlhr);
					$(All.get_active_tab() + " #membername").val(arrayData[0].temp_nmmemb);
					$(All.get_active_tab() + " #idno").val(arrayData[0].idno);
					$(All.get_active_tab() + " #tel_hp").val(arrayData[0].tel_hp);
					$(All.get_active_tab() + " #stk_code").val(arrayData[0].stk_code);
					$(All.get_active_tab() + " #stk_code_name").val(arrayData[0].stk_name);
					$(All.get_active_tab() + " #tgljoin").val(arrayData[0].tgljoin);
					$(All.get_active_tab() + " #prdcd").val("X");
					$(All.get_active_tab() + " #state").val(arrayData[0].state);
					$(All.get_active_tab() + " #trx_id").val(arrayData[0].orderno);
					$(All.get_active_tab() + " #insert_type").val("update");
				} else {
					$(All.get_active_tab() + " #insert_type").val("invalid");
				}
				
				
            	
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
        });
 }
</script>

<div class="mainForm">
  <form class="form-horizontal" id="formListRank">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Search By</label>                             
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Select Here--</option>
        		<option value="memberid">ID Member</option>
        		<option value="period">Period</option>
        		<option value="rank">Peringkat</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead" id="param">Parameter Value</label>                             
        <div class="controls">
        	<input type="text" id="paramValue1" name="paramValue1" class="span4" placeholder="Masukan ID Member"/>
        </div>
        
		
    		<div class="controls">
    	    	<select tabindex="9" class="form-list" id="paramValue2" name="paramValue2" style="width: 150px">
	            <option value="0">--Pilih Period--</option>
	            		<?php
	            			for($bulan=1; $bulan<=12; $bulan+=1){
  								echo "<option value=$bulan> $bulan </option>";
							}
	            		?>
		           </select>
		        <input type="text" id="paramValue3" name="paramValue3" placeholder="Tahun"/>
	       </div>        

		
    	   	<div class="controls">
    		    <select tabindex="7" class="form-list" id="paramValue4" name="paramValue4" onchange="be_member.get_level_info()">
	          	<option value="0">--Pilih Rank--</option>
					<?php
                           foreach($rank as $row){
            	               echo "<option value = \"$row->ranknm\">".$row->ranknm."</option>";				
							}
                   ?>
		            </select>
		        </div>
		<p></p>
		<p></p>
		<p></p>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
             <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/member/rank/list/details')"/>
         </div>
        </div>
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("select[name=searchBy]").change(function(){
			$("select option:selected").each(function(){
				if($(this).attr("value") == "period"){
					$('#param').show("slow");
					$('#paramValue1').hide("slow");
					$('#paramValue2').show("slow");
					$('#paramValue3').show("slow");
					$('#paramValue4').hide("slow");
				}else if($(this).attr("value") == "rank"){
					$('#param').show("slow");
					$('#paramValue1').hide("slow");
					$('#paramValue2').hide("slow");
					$('#paramValue3').hide("slow");
					$('#paramValue4').show("slow");
				}else if($(this).attr("value") == "memberid"){
					$('#param').show("slow");
					$('#paramValue1').show("slow");
					$('#paramValue2').hide("slow");
					$('#paramValue3').hide("slow");
					$('#paramValue4').hide("slow");
				}else if($(this).attr("value") == ""){
					$('#param').hide("slow");
					$('#paramValue1').hide("slow");
					$('#paramValue2').hide("slow");
					$('#paramValue3').hide("slow");
					$('#paramValue4').hide("slow");
				}
			});
		}).change();
	});
</script>
<div class="mainForm">
  <form class="form-horizontal" id="formSearchMember">
    <fieldset>
      <div class="control-group">
      	<label class="control-label" for="typeahead">Search By</label>
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Select Here--</option>
        		<option value="memberid">ID Member</option>
        		<option value="membernm">Nama Member</option>
        		<option value="idno">No. KTP</option>
						<option value="cell_no">HP/Telepon</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead">Parameter Value</label>
        <div class="controls">
        	<input type="text" id="paramValue" name="paramValue" class="span4" onchange="return validate()" tabindex="1"/>
        </div>

        <label class="control-label" for="typeahead">&nbsp</label>
        <div class="controls"  id="inp_btn">
             <input tabindex="2" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/member/list')"/>
         </div>
        </div>
     </fieldset>

    <div class="result"></div>
  </form>
</div>

<script type="text/javascript">
	function validate(){
		var param = document.forms["formSearchMember"]["paramValue"].value;
			if(param == null || param == ""){
				alert("Tidak Boleh Kosong");
				return false;
			}
	}
</script>

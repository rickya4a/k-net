<div class="mainForm">
	<form class="form-horizontal" id="formRank">
		<div class="control-group">
			<label class="control-label">ID<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="1" style="width: 20%;" class="uppercase" type="text" id="dfno" name="dfno" placeholder="ID" onchange="be_member.get_id_info(this.value)"/>
    	            </div>

			<label class="control-label">Nama<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="2" style="width: 25%;" class="uppercase" type="text" id="fullnm" name="fullnm" placeholder="Nama" />
    	            </div>

			<label class="control-label">Rank<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="3" style="width: 20%;" class="uppercase" type="text" id="ranknm" name="ranknm" placeholder="Rank"/>
    	                <input tabindex="4" type="hidden" id="currentrank" name="currentrank"/>
    	            </div>

			<label class="control-label">Period<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="5" style="width: 10%;" class="uppercase" type="text" id="period" name="period" placeholder="Bulan"/>&nbsp;/&nbsp;
    	                <input tabindex="6" style="width: 7%;" class="uppercase" type="text" id="year1" name="year1" placeholder="Tahun"/>
    	            </div>
			
			<p></p>
			
			<label class="control-label">New Rank<font color="red">&nbsp;* </font></label>
    	    	<div class="controls">
    	            <select tabindex="7" class="form-list" id="newrank" name="newrank" onchange="be_member.get_level_info()">
	            	<option value="0">--Pilih Rank--</option>
						<?php
                                foreach($rank as $row){
                                    echo "<option value = \"$row->level\">".$row->ranknm."</option>";
										
								}
                           ?>
		            </select>
    	            <input tabindex="8" type="hidden" id="newranknm" name="newranknm"/>
	
		        </div>

			<label class="control-label">New Period<font color="red">&nbsp;* </font></label>
    	         <div class="controls">
    	            <select tabindex="9" class="form-list" id="newperiod" name="newperiod" style="width: 150px">
	            	<option value="0">--Pilih Bulan--</option>
	            		<?php
	            			for($bulan=1; $bulan<=12; $bulan+=1){
  								echo "<option value=$bulan> $bulan </option>";
							}
	            		?>
		            </select>
		            &nbsp;/&nbsp;
		           	<select tabindex="10" class="form-list required stk" id="year2" name="year2" style="width: 90px">
	            	<option value="0">--Tahun--</option>
		            	<?php
		            		$now = date('Y');
							for($a=$now-5; $a<=$now+5; $a++){
								echo '<option value="'.$a.'">'.$a.'</option>';
							}
		            	?>
		            </select>
		         </div>

			<p></p>

		   	<div class="controls"  id="inp_btn">
	            <input tabindex="11" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_member.saveRank()"/>
	            <input type="reset" class="btn btn-reset" value="Reset" />
			</div>
		</div>
		<div class="result"></div>
	</form>
</div>
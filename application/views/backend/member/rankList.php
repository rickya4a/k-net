<?php
    if(empty($ranklist)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
  	  <thead>
			<tr>
				<th>No.</th>
				<th>ID Member</th>
				<th>Nama</th>
				<th>Tahun</th>
				<th>Bulan</th>
				<th>Peringkat</th>
			</tr>		
  	  </thead>
			<?php
					$no=1;
					foreach($ranklist as $row){
			?>
					<tr>
						<td align="center"><?php echo $no;?></td>
						<td><?php echo $row->idmember;?></td>
						<td><?php echo $row->nama;?></td>
						<td align="center"><?php echo substr($row->bulan, 0,4);?></td>
						<td align="center"><?php echo $row->bulan1;?></td>
						<td><?php echo $row->lvldesk;?></td>
					</tr>
			<?php
					$no++;
					}
			?>
	</table>
<?php
   }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
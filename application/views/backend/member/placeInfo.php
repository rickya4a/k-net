<div class="mainForm">
  <form class="form-horizontal" id="formPlace" name="formPlace">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Tempat</label>                             
        <div class="controls">
        	<input type="text" id="tempat" name="tempat" class="span4"/>
        </div>
        <label class="control-label" for="typeahead" id="param">Tanggal</label>                             
        <div class="controls">
        	<input type="text" class="dtpicker typeahead" id="tanggal" name="tanggal" />
        </div>
		
		<p></p>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
             <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Next" onclick="All.ajaxFormPost(this.form.id,'be/member/place/input')"/>
         </div>
        </div>
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div>

<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		});
	});	
</script>
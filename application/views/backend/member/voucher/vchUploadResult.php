<input type="button" class="btn btn-mini btn-danger" value="Clear Preview" onclick="be_member.clear()" />
<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Update Voucher Key Status</th>
   </tr>
   </thead>
   <tbody>
   <tr>
   	 <td width="70%">Record Found</td><td align="right"><?php echo $update['jumRec']; ?></td>
   </tr>
   <tr>	 
   	 <td>Updated Success</td><td align="right"><?php echo $update['success']; ?></td>
   </tr>
   <tr>	 
   	 <td>Updated Failed</td><td align="right"><?php echo $update['fail']; ?></td>
   </tr>
   
   </tbody>
</table>


<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="post" id="formUploadVchError" action="<?php echo base_url('be/member/voucherkey/print'); ?>">
    <fieldset>      
      <div class="control-group">
        <label class="control-label" for="typeahead">Action Type</label>	
        	<div class="controls">
        		<select id="pilih" name="pilih">
        			<option value="3">Update Voucher Key</option>
        			<option value="1">Get Voucher Key</option>
        			<option value="2">Import Voucher Key</option>
        		</select>
        	</div>
        <label class=" manual control-label" for="typeahead">Voucher No</label>
            <div class="manual controls" >
              <input type="text" id="vchno" name="vchno" class="span7 typeahead" />
              
            </div>
        <label class="manual control-label" for="typeahead">Voucher Key</label>
            <div class="manual controls" >
              <input type="text" id="vchkey" name="vchkey" class="span7 typeahead" />
              
            </div>
        	         
        <label class="batch control-label" for="typeahead">File CSV to upload</label>
            <div class="batch controls" >
              <input type="file" id="fileCSV" name="myfile" class="span7 typeahead" />
              
            </div>
          
          
         <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Import Update Voucher Key" onclick="be_member.readFromFile('be/member/voucherkey/update/save')" />
            <input type="button" class="btn btn-success" value="Preview Content File" onclick="be_member.readFromFile('be/member/voucherkey/list')" />
            <input type="submit" class="btn btn-success" value="Print To Excel"/>
         </div>
        
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div id="editor"></div>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
$(document).ready(function()
{
   $(All.get_active_tab() + " .batch").css("display", "none");	 
   $(All.get_active_tab() + " #pilih").on("change", function() {
   	  var pil = $(All.get_active_tab() + " #pilih").val();
   	  if(pil !== "3") {
   	  	$(All.get_active_tab() + " .batch").css("display", "block");
   	  	$(All.get_active_tab() + " .manual").css("display", "none");
   	  } else {
   	  	$(All.get_active_tab() + " .manual").css("display", "block");
   	  	$(All.get_active_tab() + " .batch").css("display", "none");
   	  }
   });	
   
   $(All.get_active_tab() + " #vchno").on("change", function() {
   	    All.set_disable_button();
   	    var vchno = $(All.get_active_tab() + " #vchno").val();
   		$.ajax({
	            url: All.get_url("be/member/vchno") + "/" +vchno,
	            type: 'GET',
	            success:
	            function(data){
	                All.set_enable_button();
		            $(All.get_active_tab() + " .result").html(null);
		            $(All.get_active_tab() + " .result").html(data);   
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
   });	
   
   $(All.get_active_tab() + " #fileCSV").change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'csv':
            case 'txt':
            
                $(All.get_active_tab() + ' #uploadButton').attr('disabled', false);
                break;
            default:
                alert('This is not an allowed file type, only CSV/TXT file..!!');
                this.value = '';
        }
   });
   
   
   
});
</script>

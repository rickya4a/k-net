<script type="text/javascript">
	function validate(){
		var name = document.forms["edit"]["fullnm"].value;
		var ktp = document.forms["edit"]["ktpno"].value;
		var phone = document.forms["edit"]["tel_hp"].value;
		var numbers = /^[0-9]+$/;
			if(name == null || name == "" || ktp == null || ktp == "" || phone == null || phone == ""){
				alert("Tidak Boleh Kosong");
				return false;
			}
			
			if(!ktp.match(numbers) || !phone.match(numbers)){
				alert("Harus Angka");
				return false;
			}
		}
</script>
	
<div class="mainForm">
	<div class="control-group">                      	  	
           <?php 
				$attr = array("class" => "form", "name" => "edit", "autocomplete" => "off", "onsubmit" => "return validate();");
				$hidden = array("registerno" => "$repmember->registerno");
				echo form_open("be/member/edit2", $attr, $hidden);
			?>             
                        
                        <label class="control-label">Nama<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
    	                <input tabindex="1" style="width: 20%;" class="uppercase" type="text" id="fullnm" name="fullnm" value="<?=$repmember->fullnm?>"/>
    	            </div>
				
				         <label class="control-label">No. KTP<font color="red">&nbsp;* </font></label>
    	            <div class="controls">
                        <input tabindex="2" style="width: 20%;" class="required only-number" type="text" id="ktpno" name="ktpno" value="<?=$repmember->ktpno?>"/>
                    </div>
                                        
    	                <label class="control-label">No Telp<font color="red">&nbsp;* </font></label>  
    	            <div class="controls">
    	                <input tabindex="4" style="width: 20%" class="required only-number" type="text" id="tel_hp" name="tel_hp" value="<?=$repmember->tel_hp?>"/>&nbsp;
    	            </div>

    	            <p></p>
    	            
    	            <div id="inp_btn" class="controls">
						<input id="btn_input_user" class="btn btn-primary .submit" type="submit" value="Update" name="submit" tabindex="20" >
						<input class="btn btn-reset" type="reset" value="Reset">
					</div>
			</div>
			<?php
				echo form_close();
			?>
	</form>
</div>
<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="formInputUser">
    <fieldset>      
      <div class="control-group">                      
         <?php
         //username
		 $onchange = "onchange=All.checkDoubleInput('user/list/','username',this.value)";
         $usrname = array(
		 	"labelname" => "User Name",
		 	"fieldname" => "username",
		 	"placeholder" => placeholderCheck(),
		 	"addClass" => "setReadOnly",	
		 	"event" => $onchange
 		 );
		 echo inputText($usrname);
		 //password
         $usrpwd = array(
		 	"labelname" => "Password",
		 	"fieldname" => "password",
		 	"placeholder" => "required"
 		 );
		 echo inputText($usrpwd);
		 //status
		 echo selectFlagActive("Status", "status");
		 //branch
         $branch = array(
		 	"labelname" => "Branch ID",
		 	"fieldname" => "branchid",
 		 );
		 echo inputText($branch);
		 //department
         $department = array(
		 	"labelname" => "Department ID",
		 	"fieldname" => "departmentid",
 		 );
		 echo inputText($department);
		 //user group
		 $opt = "";
		 foreach($listUserGroup as $list) {
		 		
		 	$opt .= "<option value=\"$list->groupid\">$list->groupname</option>";
		 }
         $usergroup = array(
		 	"labelname" => "User Group",
		 	"fieldname" => "groupid",
		 	"optionlist" => $opt,
		 	"refresh" => "Userconfig.refreshListUserGroup(' #groupid')"
 		 );
		 echo inputSelect($usergroup);
         $input  = "All.inputFormData('user/save', 'formInputUser')";
		 $update = "All.updateFormData('user/update', 'formInputUser', 'user/list')";
		 $view   = "All.getListData('user/list')";
         echo button_set($input, $update, $view);
		 ?>    
        </div> <!-- end control-group -->
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->

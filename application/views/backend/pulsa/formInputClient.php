
<div class="mainForm">
	<form class="form-horizontal" id="formInpCust">
	   
       <div class="control-group"> 
       	  
       	 
          <label class="control-label">Phone No</label>  
            <div class="controls">
              <input tabindex="2"  class="required uppercase span5	" type="text" id="no_tujuan" name="no_tujuan" onchange="be_pulsa.checkDoubleNoClient(this.value)" />
              <input type="hidden" id="flag_upd" name="flag_upd" value="0" />
            </div>
          
          <label class="control-label">Customer Name</label>  
            <div class="controls">
              <input tabindex="3"  class="uppercase span5" type="text" id="client" name="client" />
            </div>
         
           <div class="controls"  id="inp_btn">
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Save" onclick="be_pulsa.saveInputClient(this.form.id)" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View List Customer" onclick="All.ajaxFormPost(this.form.id,'be/pulsa/client/list')" />
          </div>  
          <div class="controls"  id="upd_btn" style="display: none;"> 
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Update" onclick="be_pulsa.saveUpdateClient(this.form.id)" />
            <input id="cancelupd" class="btn btn-danger" type="button" onclick="All.cancelUpdateForm()" value="Cancel Update">
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View List Customer" onclick="All.ajaxFormPost(this.form.id,'be/pulsa/client/list')" />
          </div>                                               
        </div>  
	   <div class="result"></div>	
	</form>
</div>
<script>
  $(document).ready(function($){
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
  });
</script>
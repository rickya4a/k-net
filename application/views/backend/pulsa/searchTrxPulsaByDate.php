<?php
	if(isset($error)) {
		setErrorMessage($error);
	} else if($result['rowCount'] < 1) {
        setErrorMessage();
    } else {
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="7">List Transaction per <?php echo $tgl_trans; ?></th>
    </tr>	
    <tr>
        <th width="5%">No.</th>
        <th>Cell Phone</th>
        <th width="10%">Client</th>
        <th width="15%">Price</th>
        <th width="20%">Trx Date</th>
        <th width="10%">Lunas</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result['arrayData'] as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs	
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->no_tujuan;?></td>
        <td align="center"><?php echo $row->client;?></td>
        <td style="text-align: right;"><?php echo number_format($row->harga_jual,0,".",".");?></td>
        <td align="center"><?php echo $row->tgl_trans." ".$row->jam;?></td>
        <td align="center"><?php echo $row->lunas;?></td>
        <td align="center">
        	<a onclick="javascript:be_pulsa.getUpdateTrx(<?php echo $row->id; ?>)" class="btn btn-mini btn-primary"><i class="icon-edit icon-white"></i></a>
        	<a onclick="javascript:be_pulsa.deleteTrx(<?php echo $row->id; ?>)" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
        </td>
   </tr>
    <?php
        $tot_price += $row->harga_jual;
        $no++;
        }
    ?>
    </tbody>
</table>
<table width="100%"  class="table table-striped table-bordered"> 
	<tr>
		 <td colspan="3" align="right"><b>TOTAL INCOME&nbsp;&nbsp;</b></td>
		 <td width="15%"  align="right"><?php echo number_format($tot_price,0,".",".");?></td>
		 <td colspan="2" width="40%">&nbsp;</td></tr>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
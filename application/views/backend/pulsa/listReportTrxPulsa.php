<?php
  $total_cost = 0;
  $total_revenue = 0;
  $total_profit = 0;
  foreach($result as $data) {
  	 $belix = (int) $data->beli;	
  	 $cost[] = $belix;
	 (int) $total_cost += $belix;
  }
  
  foreach($result as $data) {
  	 $jualx = (int) $data->jual;	
  	 $revenue[] = $jualx;
	 (int) $total_revenue += $jualx;
  }
  
  foreach($result as $data) {
  	 $profitx = (int) $data->profit;	
  	 $profit[] = $profitx;
	 (int) $total_profit += $profitx; 
  }
  
  $cost1 = json_encode($cost);
  $revenue1 = json_encode($revenue);
  $profit1 = json_encode($profit);
  
  $total_cost1 = json_encode($total_cost);
  $total_revenue1 = json_encode($total_revenue);
  $total_profit = json_encode($total_profit);
  
   
?>

<script>
	$(function () { 
		Highcharts.setOptions({
	    global: {
	      useUTC: false,
	    },
	    lang: {
	      decimalPoint: ',',
	      thousandsSep: '.'
	    }
	  });	
  
	  $(All.get_active_tab() + ' .result').highcharts({
	        chart: {
	            type: 'line'
	        },
	        title: {
	            text: 'Sales Transaction Report <?php echo $yearReport;?>'
	        },
	        xAxis: {
	            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
	        },
	        yAxis: {
	            title: {
	                text: 'Nominal'
	            }
	        },
	        
	        tooltip: {
	        		
	        		shared: true,
            		crosshairs: true
            		
	    	},

           legend: { labelFormatter: function() { 
		           		var total = 0; 
		           		for(var i=this.yData.length; i--;) { 
		           			total += this.yData[i]; 
		           		}; 
		           		return this.name + ' - Total : ' + All.num(total); 
           	         } 
           	},
	        series: [{
	            name: 'Cost',
	            
	            data: <?php echo $cost1; ?>
	        }, {
	            name: 'Revenue',
	            
	            data: <?php echo $revenue1; ?>
	        }, {
	        	name: 'Profit',
	        	
	            data: <?php echo $profit1; ?>
	        }]
	    });
    
    
    
});
</script>
<div></div>

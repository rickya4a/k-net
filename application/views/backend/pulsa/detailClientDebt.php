<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>
	<form id="formPaySelectedDebtClient">
	Total Selected Transaction :&nbsp;	
    <input type="text" style="text-align: right" id="total_selected_pricedet" name="total_selected_pricedet" value="0" />
    <input type="hidden" id="total_selected_price_realdet" name="total_selected_price_realdet" value="0" />
    <input type="button" name="btn-selected-pay" class="btn btn-mini btn-primary" value="Pay Selected" onclick="be_pulsa.paySelectedDebtDet()" />	
	<table width="100%" class="table table-striped table-bordered">
    <tr>
    	<th colspan=4>Data Transaksi <?php echo $client; ?></th>
    </tr>  
    <tr>
        <th width="5%"><input type="checkbox"  name="checkall" onclick="All.checkUncheckAll(this)"></th>
        <th>No.</th>
        <th>Date</th>
        <th>Cell Phone</th>
        <th>Price</th>
    </tr>

    <?php
        $no = 1;
        $total = 0;
        foreach($result as $row) {
    ?>
    
    <tr>    
    	<td align="center"><input id="<?php echo "pilDet".$no; ?>" type="checkbox" value="<?php echo $row->id;?>" name="idtrx[]" onchange="be_pulsa.addSelectedPriceDetail(<?php echo $no; ?>)"></td>
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->tgl_trans." ".$row->jam; ?></td>
        <td align="center"><?php echo $row->no_tujuan;?></td>
        <td align="right"><input type="hidden" id="<?php echo "pricedet".$no; ?>" value="<?php echo $row->harga_jual; ?>" /><?php echo number_format($row->harga_jual,0,".",".");?></td>
    </tr>
        
        
    <?php
        $total += $row->harga_jual;
        $no++;
        }
    ?>
    
    <tr>
        <td colspan="4" align="center"><b>Total Debt</b></td>
        <td align="right"><?php echo number_format($total,0,".",".");?></td>
    </tr>
</table>
</form>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
    </div>
	<p></p>
<?php
    }
?>

<!--<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });
</script>-->
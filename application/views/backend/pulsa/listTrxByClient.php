<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>
    <table align="center" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="6">List Transaction By Client</th>
    </tr>	
    <tr>
    	<!--<th width="5%"><input type="checkbox" name="checkall" onclick="All.checkUncheckAll(this)"></th>--> 
        <th width="10%">No.</th>
        <th>Code</th>
        <th width="20%">Trx Date</th>
        <th width="20%">Cell Phone</th>
        <th width="20%">Price</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result as $row){
        // a.id, a.client, count(id) as jum_trx, sum(a.harga_jual) as harga_jual	
    ?>
    
    <tr>
        <!--<th align="center"><input id="pil1" type="checkbox" value="<?php echo $row->client;?>" name="client[]"></th>-->
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->kode; ?></a></td>
        <td align="center"><?php echo $row->tgl_trans." ".$row->jam;?></td>
        <td align="center"><?php echo $row->no_tujuan;?></td>
        <td style="text-align: right;"><?php echo number_format($row->harga_jual,0,".",".");?></td>
        <!--<td align="center"><a href="#" id="<?php echo $row->client;?>" class="btn btn-mini btn-primary" onclick="javascript:All.ajaxPostUpdate('formPulsa','be/pulsa/pay/<?php echo $row->client; ?>','be/pulsa/search/list')">Pay Now</a></td>-->
        
        
   </tr>
    <?php
        $tot_price += $row->harga_jual;
        $no++;
        }
    ?>
    </tbody>
</table>
<table align="center" width="100%"  class="table table-striped table-bordered"> 
	<tr>
		 <td align="right">Total Price&nbsp;&nbsp;</td>
		
		 <td width="20%" align="right"><?php echo number_format($tot_price,0,".",".");?></td>
		 
    </tr>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>
    <form id="formPaySelectedDebt">
    Total Selected Transaction :&nbsp;	
    <input type="text" style="text-align: right" id="total_selected_price" name="total_selected_price" value="0" />
    <input type="hidden" id="total_selected_price_real" name="total_selected_price_real" value="0" />
    <input type="button" name="btn-selected-pay" class="btn btn-mini btn-primary" value="Pay Selected" onclick="be_pulsa.paySelectedDebt()" />	
    <table align="center" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="6">Transaction on Debt</th>
    </tr>	
    <tr>
    	<th width="5%"><input type="checkbox"  name="checkall" onclick="All.checkUncheckAll(this)"></th> 
        <th width="10%">No.</th>
        <th>Customer</th>
        <th width="10%">Trx Qty</th>
        <th width="20%">Price</th>
        <th width="15%">Pay</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result as $row){
        // a.id, a.client, count(id) as jum_trx, sum(a.harga_jual) as harga_jual	
    ?>
    
    <tr>
        <td align="center"><input id="<?php echo "pil".$no; ?>" type="checkbox" value="<?php echo $row->client;?>" name="client[]" onchange="be_pulsa.addSelectedPrice(<?php echo $no; ?>)"></td>
        <td align="right"><?php echo $no;?></td>
        <td align="center"><a href = "#" id=" <?php echo $row->client;?>" onclick="All.ajaxShowDetailonNextForm('be/pulsa/debt/<?php echo $row->client;?>/<?php echo $row->order_lgs;?>')"><?php echo $row->client;?></a></td>
        <td align="right"><?php echo $row->jum_trx;?></td>
        <td style="text-align: right;"><input type="hidden" id="<?php echo "price".$no; ?>" value="<?php echo $row->harga_jual; ?>" /><?php echo number_format($row->harga_jual,0,".",".");?></td>
        <!--<td align="center"><a href="#" id="<?php echo $row->client;?>" class="btn btn-mini btn-primary" onclick="javascript:All.ajaxPostUpdate('formPulsa','be/pulsa/pay/<?php echo $row->client; ?>','be/pulsa/search/list')">Pay Now</a></td>-->
        <td align="center"><a href="#" id="<?php echo $row->client;?>" class="btn btn-mini btn-primary" onclick="javascript:be_pulsa.payDebtClient(this)">Pay now</a></td>
        
   </tr>
    <?php
        $tot_price += $row->harga_jual;
        $no++;
        }
    ?>
    </tbody>
</table>
</form>
<table align="center" width="100%"  class="table table-striped table-bordered"> 
	<tr>
		 <td align="right">Total Income in Debt&nbsp;&nbsp;</td>
		
		 <td width="20%" align="right"><?php echo number_format($tot_price,0,".",".");?></td>
		 <td width="15%">&nbsp;</td>
    </tr>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
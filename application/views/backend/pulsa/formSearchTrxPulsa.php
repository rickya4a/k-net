
<div class="mainForm">
	<form class="form-horizontal" id="formPulsaSearch">
	   
       <div class="control-group"> 
       	 
          <label class="control-label"><font color="red">&nbsp;*</font>Search Type</label>  
            <div class="controls">
              <select tabindex="1"  class="span5" id="search_type" name="search_type" onchange="be_pulsa.showDateonTrxSearch(this.value)">
	            	<option value="hutang">Hutang Pulsa</option>
					<option value="hutang_tanto">Hutang Pulsa Tanto</option>
					<option value="cust">List Phone Number by Customer Name</option>
					<option value="cell_phone">List Trx by Phone Number</option>
					<option value="trx_client">List Trx by Client</option>
	          </select> 
            </div> 
          <label  class="control-label">Param Value</label>
            <div class="controls">
	            <input tabindex="2" type="text" class="span5" id="param" name="param" >  
    	    </div>
    	  <label class="control-label showDt" style="display: none;" for="typeahead">Trx Date</label>
            <div class="controls showDt" style="display: none;">
                <input type="text" class="dtpicker typeahead" id="pulsa_trx_src_from" name="from" />&nbsp;&nbsp;to&nbsp;
                <input type="text"  class="dtpicker typeahead" id="pulsa_trx_src_to" name="to" />
            </div>
           <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" class="btn btn-success" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/pulsa/search/list')" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            
          </div>                                                   
        </div>  
	   	
	</form>
	<div class="result"></div>
</div>
<script>
  $(document).ready(function($){
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
  });
</script>
<?php
  $total_cost = 0;
  $total_revenue = 0;
  $total_profit = 0;
  
  foreach($result as $data) {
  	 $thnx = (int) $data->thn;	
  	 $thn[] = $thnx;
	 
  }
  
  foreach($result as $data) {
  	 $belix = (int) $data->beli;	
  	 $cost[] = $belix;
	 (int) $total_cost += $belix;
  }
  
  foreach($result as $data) {
  	 $jualx = (int) $data->jual;	
  	 $revenue[] = $jualx;
	 (int) $total_revenue += $jualx;
  }
  
  foreach($result as $data) {
  	 $profitx = (int) $data->profit;	
  	 $profit[] = $profitx;
	 (int) $total_profit += $profitx; 
  }
  
  $thn1 = json_encode($thn);
  $cost1 = json_encode($cost);
  $revenue1 = json_encode($revenue);
  $profit1 = json_encode($profit);
  
  $total_cost1 = json_encode($total_cost);
  $total_revenue1 = json_encode($total_revenue);
  $total_profit = json_encode($total_profit);
  
  $average_cost1 = json_encode($total_cost/12);
  $average_revenue1 = json_encode($total_revenue/12);
  $average_profit = json_encode($total_profit/12);
  
   
?>

<script>
	$(function () { 
		Highcharts.setOptions({
	    global: {
	      useUTC: false,
	    },
	    lang: {
	      decimalPoint: ',',
	      thousandsSep: '.'
	    }
	  });	
  
	  $(All.get_active_tab() + ' .result').highcharts({
	        chart: {
	            type: 'line'
	        },
	        title: {
	            text: 'Profit Summary Sales Report <?php echo $range;?>'
	        },
	        xAxis: {
	            categories: <?php echo $thn1; ?>
	        },
	        yAxis: {
	            title: {
	                text: 'Nominal'
	            }
	        },
	        
	        tooltip: {
	        		
	        		shared: true,
            		crosshairs: true
            		
	    	},

           
	        series: [{
	        	name: 'Profit',
	        	
	            data: <?php echo $profit1; ?>
	        }]
	    });
    
      /* 
       {
	            name: 'Cost',
	            
	            data: <?php echo $cost1; ?>
	        }, {
	            name: 'Revenue',
	            
	            data: <?php echo $revenue1; ?>
	        }, 
       * */
    
});
</script>
<div></div>

<?php
  if(!empty($sess_exp)) {
  	   setErrorMessage($sess_exp);
  } else {
?>
<div class="mainForm">
	<form class="form-horizontal" id="formPulsa">
	   
       <div class="control-group"> 
       	  
       	  <label class="control-label" for="typeahead" id="lbl-trx_id"><font color="red">&nbsp;*</font>Trx Code</label>
	        <div class="controls">  
	       	  <input tabindex="1"  class="uppercase span5" type="text" id="trx_id" name="trx_id" onchange="be_pulsa.searchTrxByID(this.value)" />
	       	  <input type="hidden" id="code" name="code" />
	       	  <input type="hidden" id="harga_beli" name="harga_beli" />
	       	  <input type="hidden" id="jam" name="jam" />
	       	  <input type="hidden" id="idx" name="idx" />
            </div>
          <label class="control-label">Client Phone No</label>  
            <div class="controls">
              <input tabindex="2"  class="required uppercase span5	" type="text" id="no_tujuan" name="no_tujuan" />
            </div>
          
          <label class="control-label">Customer Name</label>  
            <div class="controls">
              <input tabindex="3"  class="uppercase span5" type="text" id="client" name="client" />
            </div>
          <label class="control-label">Price</label>  
            <div class="controls">
              <input tabindex="4"  class="uppercase span5" type="text" id="harga_jual" name="harga_jual" />
            </div>
         
          <label class="control-label"><font color="red">&nbsp;*</font>Order Type</label>  
            <div class="controls">
              <select tabindex="6"  class="form-list required" id="order_lgs" name="order_lgs" onchange="be_pulsa.setOrderType(this.value)">
	            	<option value="L">Langsung</option>
					<option value="V">Via Tanto</option>
	          </select> 
            </div>
           <label class="control-label"><font color="red">&nbsp;*</font>Pay Now</label>  
            <div class="controls">
              <select tabindex="6"  class="form-list required" id="lunas" name="lunas">
	            	<option value="Y">Yes</option>
					<option value="">No</option>
	          </select> 
            </div> 
          <label  class="control-label"><font color="red">&nbsp;*</font>Trx Date</label>
            <div class="controls">
	            <input tabindex="7" type="text"  class="dtpicker typeahead" id="tgl_trans" name="tgl_trans" value="<?php echo $curdate; ?>" >  
    	    </div>
    	  
           <div class="controls"  id="inp_btn">
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Save" onclick="be_pulsa.savePulsaTrx(this.form.id)" />
            <input type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View Trx By Date" onclick="All.ajaxFormPost(this.form.id,'be/pulsa/date')" />
          </div>  
          <div class="controls"  id="upd_btn" style="display: none;"> 
            <input tabindex="8" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Update" onclick="be_pulsa.saveUpdatePulsaTrx(this.form.id)" />
            <input id="cancelupd" class="btn btn-danger" type="button" onclick="All.cancelUpdateForm()" value="Cancel Update">
            <input tabindex="9" type="button" class="btn btn-success" name="save" value="View Trx By Date" onclick="All.ajaxFormPost(this.form.id,'be/pulsa/date')" />
          </div>                                               
        </div>  
	   <div class="result"></div>	
	</form>
</div>
<script>
  $(document).ready(function($){
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
  });
</script>
<?php
   }
?>
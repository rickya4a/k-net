<div class="mainForm">
	<form class="form-horizontal" id="summaryReport">
	   
       <div class="control-group">
       	<label class="control-label" for="typeahead" id="lbl-trx_id"><font color="red">&nbsp;*</font>Year from</label>
	        <div class="controls">
	         <select id="yearReportFrom" name="yearReportFrom" class="forYear">
	       	  	 <?php 
	       	  	    $year = date("Y");
					$show = $year - 5;
	       	  	    $select = ($year - 5) + 1;
					$set = "";
					for($i = $year; $i > $show; $i--) {
						if($i == $select) {
							echo "<option value=\"$i\" selected=\"selected\">$i</option>";
						} else {
							echo "<option value=\"$i\">$i</option>";
						}
						
						
					}
	       	  	 ?>
	       	  </select>
	       	  	 &nbsp;to&nbsp;
	         <select id="yearReportTo" name="yearReportTo" class="forYear">
	       	  	 <?php 
	       	  	    
					
					for($i = $year; $i > $show; $i--) {
						echo "<option value=\"$i\">$i</option>";
					}
	       	  	 ?>
	       	  </select>	 
	       	  
	       	  
	       	  <input type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="View Report" onclick="All.ajaxFormPost(this.form.id, 'be/pulsa/summary/list')" />
            </div>
       </div>
    </form>        
    <div class="result" style="width:100%; height:400px;"></div>
 </div>   

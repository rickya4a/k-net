<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>
    <table align="center" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="4">List Customer</th>
    </tr>	
    <tr>
    	<!--<th width="5%"><input type="checkbox" name="checkall" onclick="All.checkUncheckAll(this)"></th>--> 
        <th width="10%">No.</th>
        <th width="40%">Customer Name</th>
        <th>Phone Number</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result as $row){
        // a.id, a.client, count(id) as jum_trx, sum(a.harga_jual) as harga_jual	
    ?>
    
    <tr>
        <!--<th align="center"><input id="pil1" type="checkbox" value="<?php echo $row->client;?>" name="client[]"></th>-->
        <td align="right"><?php echo $no;?></td>
        <td align="center"><?php echo $row->client; ?></a></td>
        <td align="center"><?php echo $row->no_tujuan;?></td>
        
        <td align="center">
        	<a id="<?php echo $row->no_tujuan; ?>" onclick="javascript:be_pulsa.getUpdateClient(this)" class="btn btn-mini btn-primary"><i class="icon-edit icon-white"></i></a>
        	<a id="<?php echo $row->no_tujuan; ?>" onclick="javascript:All.deleteFormData('be/pulsa/client/delete/',<?php echo "'$row->no_tujuan'"; ?>,'be/pulsa/client/list')" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
        </td>
        
        
   </tr>
    <?php
        
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>

<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="post" id="formUploadSGO" action="<?php echo base_url('trans/sgo/import/preview'); ?>">
    <fieldset>      
      <div class="control-group">
       
         <label class="control-label" for="typeahead">File CSV to upload</label>
            <div class="controls" >
              <input type="file" id="fileCSV" name="myfile" class="span7 typeahead" />
              
            </div>
          
          
         <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Export To DB" onclick="be_trans.saveSGOFileToDatabase()" />
            <input tabindex="6"  type="reset" class="btn btn-reset" value="Reset" />
          
            <input type="button" class="btn btn-success" value="Preview Content File" onclick="be_trans.readFromFile()" />
            
         </div>
         
         <?php
         /*
         	$desc = "Order ID = RM160217675986 Remark = K-NET Registration Member";
         	echo "$desc<br/>";
			$ordid = strpos($desc, "Order ID ="); 
			$rmk = strpos($desc, "Remark"); 
			echo "$ordid<br/>";
			echo "$rmk<br/>";
			$ss = explode(" ", $desc);
			$jml = count($ss)-1;
			//echo $jml;
			for ($i=0; $i <= $jml; $i++) {
				echo "No. $i ** ".$ss[$i]."<br/>";
			}
		  * */
         ?>
         
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div id="editor"></div>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
$(document).ready(function()
{
   
   $(All.get_active_tab() + " #fileCSV").change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'csv':
            case 'txt':
            
                $(All.get_active_tab() + ' #uploadButton').attr('disabled', false);
                break;
            default:
                alert('This is not an allowed file type, only CSV/TXT file..!!');
                this.value = '';
        }
   });
   
   
   
});
</script>

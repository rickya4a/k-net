<?php

    //$pdf=new FPDF('P','mm', 'A4');
	//$pdf=new PDF_Code128('P','mm','A4'); 
	$pdf=new PDF_MC_Table(); 
    $pdf->Open();
    $pdf->SetFillColor(255,255,255); // background = biru muda
    $pdf->SetTextColor(0,0,0);	 //	font color = black
    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
    $pdf->SetLineWidth(.1);
    //$pdf->AddPage();
	
	//$pdf->SetXY(10,50);
	
		
        
    $titleCol2 = 155;
    $lebarCell = 5;
	$left = 40;
	$border = 0;
	
	 //setting header product column width
    $no_urut = 7;
    $product_id = 25;
    $product_name = 90;
    $qty = 9;
    $dp = 23;
    $bv = 8;
    $kolom_total = $no_urut + $product_id + $product_name + $qty + $dp + $bv;
    $total_dp = 23;
    $total_bv = 15;
	
    $pdf->SetFillColor(255,255,255);
    
	$xpage = 0;
	foreach($result as $row) {
		$xpage = $xpage + 1;
		if($row['cargo_id'] == "2") {
			$pdf -> AddPage();
			$pdf->SetX(40);
			$pdf->Image("assets/kgb_logo.jpeg",13,5, 21, 21);
			$pdf->Image("assets/klink_logo.jpeg",177,8, 22, 22);
			
			 $pdf->Ln();
			 $pdf->SetY(10);
			 $pdf->SetX(70);
			 $pdf -> SetFont('Arial', 'B', 12);
			 $pdf->Cell(70,7,"CONSIGNMENT NOTE",0,1,'C'); 
			 $pdf->Ln();
			 
			 $pdf->SetY(15);
			 $pdf->SetX(70);
			 $pdf -> SetFont('Arial', '', 9);
			 $pdf->Cell(70,7,"Ecommerce Transaction",0,1,'C'); 
			 $pdf->Ln();
			 
			 $pdf->SetY(24);
			 $pdf->SetX(9);
			 $pdf -> SetFont('Times', 'B', 8);
			 $pdf->Cell(20,7,"PT KERTA GAYA PUSAKA",0,1,'L');
			 $pdf->SetY(28);
			 $pdf->Line(8,30,200,30); 
			 $pdf->Ln();
					 
			$pdf->SetFont('Arial','',9);
		    $printDate = date("d-m-Y");    
		    $pdf->SetX(10);
		    $pdf->Cell($left,$lebarCell,"Transaction Date :",$border,0,'R',true);
			$pdf->Cell($titleCol2,$lebarCell,$row['datetrans2'],$border,0,'L',true);
		    $pdf->SetX(120);
		    $pdf->Cell($left,$lebarCell,"Print Date :",$border,0,'R',true);
			$pdf->Cell($titleCol2,$lebarCell, $printDate, $border,0,'L',true);
			$pdf->Ln();
			
			$pdf->Cell($left,$lebarCell,"Transaction No :",$border,0,'R',true);
			$pdf->Cell($titleCol2,$lebarCell, $row['orderno'], $border,0,'L',true);
			$pdf->Code39(80,40,$row['orderno'],1,10);
			$pdf->Ln();
		 	$pdf->Cell($left,$lebarCell,"Delivered By :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,"KGP",$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell,"KGP Conote :",$border,0,'R',true);
		    //$pdf->Cell($titleCol2,$lebarCell,$row['conoteJNE'],$border,0,'L',true);
		    
		    $pdf->Ln();
		    //============================START CONVERT CONOTE NUMBER TO BARCODE============================
		    //Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
			$pdf->Code39($left+13,$lebarCell+46,trim($row['conoteJNE']),1,7);
			//$pdf->Code39($left+13,65+24,trim($row['conoteJNE']),1,7);
			//$pdf1->Output();
		    $pdf->SetFillColor(255,255,255); // background = biru muda
		    $pdf->SetTextColor(0,0,0);	 //	font color = black
		    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
		    $pdf->SetLineWidth(.1);
			$pdf->Ln();
			$pdf->Ln();
			//============================END CONVERT CONOTE NUMBER TO BARCODE============================
			
			$pdf->Cell($left,$lebarCell,"Total Weight :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,$row['total_weight']." kg",$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell,"Consignee Name :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,$row['receiver_name'],$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell,"Contact :",$border,0,'R',true);
			
			$contactNo = trim($row['contactno']);
			$contactNoCheck = substr($contactNo, -1);
			$pjg = strlen($contactNo) - 1;
			if($contactNoCheck == "/"){
				$contactNo = trim(substr($contactNo, 0, $pjg));
				//$contactNo = $contactNoCheck;
			}
			
		    //$pdf->Cell($titleCol2,$lebarCell, $pjg, $border,0,'L',true);
			//$pdf->Ln();
		    $pdf->Cell($titleCol2,$lebarCell, $contactNo, $border,0,'L',true);
			$pdf->Ln();
			/*
			$pdf->Cell($left,$lebarCell,"ADDRESS :",0,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 0, 50),0,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 50, 50),0,0,'L',true);
			$pdf->Ln(); */
			$pdf->Cell($left,$lebarCell,"Address :",$border,0,'R',true);
		    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['addr']))),$border,1,'L',true);
			/*tambahan ana 7/4/2016*/
	        if($row['propinsi'] != null && $row['kec'] != null){
			  $pdf->Cell($left,$lebarCell,"",$border,0,'R',true);
	          $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords($row['kec'])))),$border,1,'L',true);
			}
			$pdf->Cell($left,$lebarCell,"Shipper Name :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,"PT K-Link Indonesia / ".$row['wh_name'],$border,0,'L',true);
			$pdf->Ln();
			/*$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,$this->username,0,0,'L',true);
		    $pdf->Ln(); */
			$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr1'])),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr2'])),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower(trim($row['whcd_addr3']))),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Ln();
		    
			$pdf->SetFont('Arial','',7);
			$pdf->Cell($left,$lebarCell,"Received Date :",$border,0,'R',true);
			$pdf->Cell($titleCol2,$lebarCell,"----------/----------/-----------------",$border,0,'L',true);
			$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();
			$colAlign = array('C', 'C', 'C');
			$pdf->SetAligns($colAlign);
			$pdf -> SetWidths(array(50, 90, 50));
			
			$pdf -> Row(
			   array(
			     "------------------------------------------", 
			     "------------------------------------------", 
			     "------------------------------------------"
			   ), false
			);
			
			$pdf -> Row(
			   array(
			     "Received By", 
			     "Delivered By", 
			     "W/H K-Link (Shipper)"
			   ), false
			);
			
			
		    $pdf->SetFont('Arial','',10);
			$pdf->Ln();
            $pdf->Cell($left,$lebarCell,"** Produk dapat di klaim bila ada kerusakan dalam jangka waktu 1 x 24 jam setelah barang itu diterima **",$border,0,'L',true);
			$pdf->SetFont('Arial','',10);
/*		//start ori dion

  					$pdf->Image("assets/kgb_logo.jpeg",10,5, 25, 25); 
					
					$pdf->SetX(33);
					$pdf -> SetFont('Times', 'B', 12);
					$pdf->Cell(70,7,"PT KERTA GAYA PUSAKA",0,1,'L'); 
					 $pdf->Ln();
 					$pdf->SetY(17);
					 $pdf->SetX(33);
					 $pdf -> SetFont('Times', 'B', 9);
					 $pdf->Cell(70,7,$row['kode_kabupaten'],0,1,'L'); 
					$pdf -> SetFont('Times', '', 9); 			
					$pdf->Code39(105,20,$row['conoteJNE'],1,7);
					//Table with 20 rows and 4 columns
					$pdf -> SetFont('Times', '', 9);
					$colAlign = array('L', 'L', 'L', 'L', 'C');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(20, 45, 20, 50, 50));
					$pdf->SetY(35);
					
					$pdf -> Row(
					   array(
					     "Pengirim", 
					     $row['wh_name'], 
					     "Penerima", 
					     $row['receiver_name'],
					     "Jumlah"
					   )
					);
					$colAlign = array('L', 'L', 'L', 'L', 'C', 'C');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(20, 45, 20, 50, 25, 25));
					
					$pdf -> Row(
					   array(
					     "", 
					     $row['whcd_addr1']." ".$row['whcd_addr2']. " " .$row['whcd_addr3'], 
					     "", 
					     $row['addr'],
					     "Satuan : ".$row['total_item']."",
					     "Berat : ".$row['total_weight']." (Kg)"
					   )
					);
					
					
					$addY = 0;
					$addY2 = 0;
					if(strlen($row['addr']) > 70){
						$addY = 20;
						$addY2 = 10;
					}
					
					
					
					$colAlign = array('L', 'L', 'L', 'L', 'C', 'C');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(20, 45, 20, 50, 25, 25));
					
					$pdf -> Row(
					   array(
					     "Telp", 
					     "", 
					     "Telp", 
					     $row['contactno'],
					     "",
					     ""
					   )
					); 
					
					$colAlign = array('L', 'R', 'R');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(25, 8, 32));
					
					$pdf -> Row(
					   array(
					     "Porto",
					     "Rp.",   
					     "".number_format($row['payShip'], 0,",", ".").""
					   )
					);
					$colAlign = array('L', 'R', 'R');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(25, 8, 32));
					
					$pdf -> Row(
					   array(
					     "PPn 1%",
					     "Rp.",  
					     ""
					   )
					);
					
					$colAlign = array('L', 'R', 'R');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(25, 8, 32));
					
					$pdf -> Row(
					   array(
					     "Premi Asuransi",  
					     "Rp.",
					     ""
					   )
					);
					
					$colAlign = array('L', 'R', 'R');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(25, 8, 32));
					
					$pdf -> Row(
					   array(
					     "Packing", 
					     "Rp.", 
					     ""
					   )
					);
					$colAlign = array('L', 'R', 'R');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(25, 8, 32));
					
					$pdf -> Row(
					   array(
					     "Jumlah", 
					     "Rp.", 
					     "".number_format($row['payShip'], 0,",", ".").""
					   )
					);
					
					 $pdf->SetY(65 + $addY2);
					 $pdf->SetX(75);
					 $pdf -> SetFont('Times', '', 9);
					 $pdf->MultiCell(120,25,"Isi menurut pengakuan : Produk suplemen herbal",1,1,'L');
					 $pdf->MultiCell(50,5,"Diterima tanggal",0,1,'L');
					 $pdf->SetY(90 + $addY2);
					 $pdf->SetX(60);
					  $pdf -> SetFont('Times', '', 5);
					 $pdf->MultiCell(85,5,"PENGIRIM MENYATAKAN SETUJU DENGAN ISIAN DAN SYARAT-SYARAT PENGIRIMAN YANG TERTERA PADA HALAMAN SEBALIKNYA",0,1,'C');  
					 $pdf->SetY(90 + $addY2);
					 $pdf->SetX(145);
					  $pdf -> SetFont('Times', '', 9);
					 //$pdf->MultiCell(50,5,"Trx Date : ".$row['datetrans2']."",0,1,'C');
					 $printDate = date("d-m-Y");    
					 $pdf->MultiCell(50,5,"Trx Date : ".$row['datetrans2']."\nPrint Date : ".$printDate."",0,1,'C');
					
					$pdf->SetY(120 + $addY2); 
					$colAlign = array('L', 'L', 'L');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(50, 90, 50));
					
					$pdf -> Row(
					   array(
					     "_____________________________", 
					     "_____________________________", 
					     "_____________________"
					   ), false
					);
					$colAlign = array('L', 'L', 'L');
					$pdf->SetAligns($colAlign);
					$pdf -> SetWidths(array(50, 90, 50));
					
					$pdf -> Row(
					   array(
					     "Tanda Tangan dan Nama Jelas", 
					     "Tanda Tangan dan Nama Jelas", 
					     "Bagian Pengiriman"
					   ), false
					);
					
*/ //end ori dion
					
					
					
					
					
					
					
					$pdf->SetFont('Arial','',10);
				    $pdf->AddPage();	
				    $pdf->Cell(185,$lebarCell,"Print Out Shipping",0,1,'C'); 
				    $pdf->Ln();
				    //$pdf->Ln();
					$pdf->Cell($left,$lebarCell,"To :",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['receiver_name'])),$border,0,'L');
					$pdf->Ln();
					$pdf->Cell($left,$lebarCell,"Contact :",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,$row['contactno'],$border,0,'L');
					$pdf->Ln();
					/*
					$pdf->Cell($left,$lebarCell,"ADDRESS :",0,0,'R',true);
				    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 0, 50),0,0,'L',true);
					$pdf->Ln();
					$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
				    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 50, 50),0,0,'L',true);
					$pdf->Ln(); */
					$pdf->Cell($left,$lebarCell,"Address :",$border,0,'R');
				    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['addr']))),$border,1,'L');
					/*tambahan ana 7/4/2016*/
			        if($row['propinsi'] != null && $row['kec'] != null){
					  $pdf->Cell($left,$lebarCell,"Provinsi & Kecamatan :",$border,0,'R');
			          $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords($row['kec'])))),$border,1,'L',true);
					}
					$pdf->Cell($left,$lebarCell,"From :",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,"PT K-Link Nusantara",$border,0,'L');
					$pdf->Ln();
					/*$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
				    $pdf->Cell($titleCol2,$lebarCell,$this->username,0,0,'L',true);
				    $pdf->Ln(); */
					$pdf->Cell($left,$lebarCell," ",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr1'])),$border,0,'L');
					$pdf->Ln();
					$pdf->Cell($left,$lebarCell," ",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr2'])),$border,0,'L');
					$pdf->Ln();
					$pdf->Cell($left,$lebarCell," ",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr3'])),$border,0,'L');
					$pdf->Ln();
					$pdf->Cell($left,$lebarCell,"Ship By :",$border,0,'R');
				    $pdf->Cell($titleCol2,$lebarCell,"KGP",$border,0,'L');
					$pdf->Ln();
					$pdf->Cell($left,$lebarCell,"KGP Conote :",$border,0,'R');
				    //$pdf->Cell($titleCol2,$lebarCell,$row['conoteJNE'],$border,0,'L',true);
				    //============================START CONVERT CONOTE NUMBER TO BARCODE============================
					$pdf->Code39($left+12,65,$row['conoteJNE'],1,7);
					//$pdf1->Output();
				    $pdf->SetFillColor(255,255,255); // background = biru muda
				    $pdf->SetTextColor(0,0,0);	 //	font color = black
				    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
				    $pdf->SetLineWidth(.1);
					$pdf->Ln();
					$pdf->Ln();
					//============================END CONVERT CONOTE NUMBER TO BARCODE============================
					
					
					    $pdf->Ln();
						$pdf->Ln();
						$kolom_header = $no_urut + $product_id + $product_name + $qty + $dp + $bv + $total_dp + $total_bv;
						//$pdf->Ln();
						$kiri = $no_urut + $product_id;
						$pdf->Cell($kolom_header,$lebarCell,"Detail Product",0,0,'C',true);
						$pdf->Ln();
					    $pdf->Cell($kiri,$lebarCell,"Order No :",0,0,'R',true);
				        $pdf->Cell($titleCol2,$lebarCell,$row['orderno'],0,0,'L',true); 
						$pdf->Ln();
						
					    $pdf->Cell($no_urut,$lebarCell,"No",1,0,'C',true);
					    $pdf->Cell($product_id,$lebarCell,"ID Product",1,0,'C',true);
					    $pdf->Cell($product_name,$lebarCell,"Product Name",1,0,'C',true);
					    $pdf->Cell($qty,$lebarCell,"Qty",1,0,'C',true);
					    //$pdf->Cell($dp,$lebarCell,"DP",1,0,'C',true);
					    $pdf->Cell($bv,$lebarCell,"BV",1,0,'C',true);
					    //$pdf->Cell($total_dp,$lebarCell,"Total DP",1,0,'C',true);
					    $pdf->Cell($total_bv,$lebarCell,"Total BV",1,0,'C',true);
					    $pdf->Ln();
					    $x = 1;
					   
					    $total_qty = 0;
					    $total_dpR = 0;
					    $total_bvR = 0;
						if($row['detPrd'] != null) {
					    foreach($row['detPrd'] as $dta2)
						    {
						        $dpR = $dta2->dpr * $dta2->qty;
						        $bvR = $dta2->bvr * $dta2->qty;
						       
						        $pdf->Cell($no_urut, $lebarCell,$x,1,0,'C');
						        $pdf->Cell($product_id, $lebarCell,"$dta2->prdcd",1,0,'L');
						        $pdf->Cell($product_name, $lebarCell,ucwords(strtolower($dta2->prdnm)),1,0,'L');
						        $pdf->Cell($qty, $lebarCell,number_format($dta2->qty,0,".",","),1,0,'R');
						        //$pdf->Cell($dp, $lebarCell,number_format($dta2->dpr,0,".",","),1,0,'R',true);
						        $pdf->Cell($bv, $lebarCell,number_format($dta2->bvr,0,".",","),1,0,'R');
						        //$pdf->Cell($total_dp, $lebarCell,number_format($dpR,0,".",","),1,0,'R',true);
						        $pdf->Cell($total_bv, $lebarCell,number_format($bvR,0,".",","),1,0,'R');
						        $pdf->Ln();
						       
						        $total_qty += $dta2->qty;
						        $total_dpR += $dpR;
						        $total_bvR += $bvR;
						        $x++;
						    }
					    $tot = $no_urut + $product_id + $product_name;
						$tot2 = $dp + $bv;
					    $pdf->Cell($tot, $lebarCell,"T o t a l",1,0,'C');
				        $pdf->Cell($qty, $lebarCell,number_format($total_qty,0,".",","),1,0,'R');
				        //$pdf->Cell($tot2, $lebarCell,"",1,0,'R',true);
				        //$pdf->Cell($total_dp, $lebarCell,number_format($total_dpR,0,".",","),1,0,'R',true);
				        $pdf->Cell($total_bv + $bv, $lebarCell,number_format($total_bvR,0,".",","),1,0,'R');
				        $pdf->Ln(); 
				        }	
		} else {
		
			$pdf->SetFont('Arial','',10);
			$pdf->AddPage();	
			
			/*
			$pdf->SetX(40);
			$pdf->Image("assets/kgb_logo.jpeg",13,5, 21, 21);
			$pdf->Image("assets/klink_logo.jpeg",177,8, 22, 22);
			
			 $pdf->Ln();
			 $pdf->SetY(10);
			 $pdf->SetX(70);
			*/			 
			 //============================START CONVERT CONOTE NUMBER TO BARCODE============================
			//$pdf->Code39($left+30,65,$row['conoteJNE'],1,7);
			
		    //$pdf->Ln();
			//$pdf->Ln();
			if($xpage == 1){
				$pdf->Ln();
				$pdf->Ln();
			}
		    $pdf->Cell(185,$lebarCell,"Destination",0,1,'C',true); 
		    $pdf->Ln();
			
			$pdf->Cell($left,$lebarCell,"Order No :",$border,0,'R',true);
		    //============================START CONVERT CONOTE NUMBER TO BARCODE============================
		    //Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
			$pdf->Code39($left+15,$lebarCell + 13,trim($row['orderno']),1,7);
			//$pdf1->Output();
		    $pdf->SetFillColor(255,255,255); // background = biru muda
		    $pdf->SetTextColor(0,0,0);	 //	font color = black
		    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
		    $pdf->SetLineWidth(.1);
			$pdf->Ln();
			$pdf->Ln();
			//============================END CONVERT CONOTE NUMBER TO BARCODE============================

		    $pdf->SetFont('Arial','',10);
			
			$pdf->Cell($left,$lebarCell,"To :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['receiver_name'])),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell,"Contact :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,$row['contactno'],$border,0,'L',true);
			$pdf->Ln();
			/*
			$pdf->Cell($left,$lebarCell,"ADDRESS :",0,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 0, 50),0,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 50, 50),0,0,'L',true);
			$pdf->Ln(); */
			$pdf->Cell($left,$lebarCell,"Address :",$border,0,'R',true);
		    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['addr']))),$border,1,'L',true);
			/*tambahan ana 7/4/2016*/
	        if($row['propinsi'] != null && $row['kec'] != null){
			  $pdf->Cell($left,$lebarCell,"Provinsi & Kecamatan :",$border,0,'R',true);
	          $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords($row['kec'])))),$border,1,'L',true);
			}
			$pdf->Cell($left,$lebarCell,"From :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,"PT K-Link Nusantara",$border,0,'L',true);
			$pdf->Ln();
			/*$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,$this->username,0,0,'L',true);
		    $pdf->Ln(); */
			$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr1'])),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr2'])),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr3'])),$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell,"Ship By :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell, $row['shipper_code'],$border,0,'L',true);
			$pdf->Ln();
			$pdf->Cell($left,$lebarCell, $row['shipper_code']." Conote :",$border,0,'R',true);
		    //$pdf->Cell($titleCol2,$lebarCell,$row['conoteJNE'],$border,0,'L',true);
		    
		    //============================START CONVERT CONOTE NUMBER TO BARCODE============================
		    //Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
			$pdf->Code39($left+30,65+10,trim($row['conoteJNE']),1,7);
			//$pdf1->Output();
		    $pdf->SetFillColor(255,255,255); // background = biru muda
		    $pdf->SetTextColor(0,0,0);	 //	font color = black
		    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
		    $pdf->SetLineWidth(.1);
			$pdf->Ln();
			//============================END CONVERT CONOTE NUMBER TO BARCODE============================
			
			//$pdf->Code39($titleCol2,$lebarCell,$row['conoteJNE'],$border,0,'L',true);
			//$pdf->Code39(80,40,$row['conoteJNE'],1,10);
			/*
			$pdf->Cell($left,$lebarCell,"JNE BARCODE :",$border,0,'R',true);
			$conoteno = $row['conoteJNE'];
			$pdf->Code128($titleCol2,$lebarCell,$row['conoteJNE'],$border,0,'L',true);
			*/
		    $pdf->Ln();
		    $pdf->Ln();
		    $pdf->Cell($left,$lebarCell,"Service Type :",$border,0,'R',true);
		    $pdf->Cell($titleCol2,$lebarCell, $row['service_type_name'],$border,0,'L',true);
			$pdf->Ln();
			
		    $pdf->SetFont('Arial','',10);
	    $pdf->AddPage();	
	    $pdf->Cell(185,$lebarCell,"Print Out Shipping Data",0,1,'C',true); 
	    $pdf->Ln();
	    //$pdf->Ln();
		$pdf->Cell($left,$lebarCell,"To :",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,$row['receiver_name'],$border,0,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell,"Contact :",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,$row['contactno'],$border,0,'L',true);
		$pdf->Ln();
		/*
		$pdf->Cell($left,$lebarCell,"ADDRESS :",0,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 0, 50),0,0,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,substr(ucwords(strtolower($row['addr'])), 50, 50),0,0,'L',true);
		$pdf->Ln(); */
		$pdf->Cell($left,$lebarCell,"Address :",$border,0,'R',true);
	    $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['addr']))),$border,1,'L',true);
		/*tambahan ana 7/4/2016*/
        if($row['propinsi'] != null && $row['kec'] != null){
		  $pdf->Cell($left,$lebarCell,"Provinsi & Kecamatan :",$border,0,'R',true);
          $pdf->MultiCell($titleCol2,$lebarCell,str_replace("\n", "", ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords($row['kec'])))),$border,1,'L',true);
		}
		$pdf->Cell($left,$lebarCell,"From :",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,"PT K-Link Nusantara",$border,0,'L',true);
		$pdf->Ln();
		/*$pdf->Cell($left,$lebarCell," ",0,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,$this->username,0,0,'L',true);
	    $pdf->Ln(); */
		$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr1'])),$border,0,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr2'])),$border,0,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell," ",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell,ucwords(strtolower($row['whcd_addr3'])),$border,0,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell,"Ship By :",$border,0,'R',true);
	    $pdf->Cell($titleCol2,$lebarCell, $row['shipper_code'],$border,0,'L',true);
		$pdf->Ln();
		$pdf->Cell($left,$lebarCell, $row['shipper_code']." Conote :",$border,0,'R',true);
	    //$pdf->Cell($titleCol2,$lebarCell,$row['conoteJNE'],$border,0,'L',true);
	    //============================START CONVERT CONOTE NUMBER TO BARCODE============================
		$pdf->Code39($left+30,65,$row['conoteJNE'],1,7);
		//$pdf1->Output();
	    $pdf->SetFillColor(255,255,255); // background = biru muda
	    $pdf->SetTextColor(0,0,0);	 //	font color = black
	    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
	    $pdf->SetLineWidth(.1);
		$pdf->Ln();
		$pdf->Ln();
		//============================END CONVERT CONOTE NUMBER TO BARCODE============================
		
		
		    $pdf->Ln();
			$pdf->Ln();
			$kolom_header = $no_urut + $product_id + $product_name + $qty + $dp + $bv + $total_dp + $total_bv;
			//$pdf->Ln();
			$kiri = $no_urut + $product_id;
			$pdf->Cell($kolom_header,$lebarCell,"Detail Product",0,0,'C',true);
			$pdf->Ln();
		    $pdf->Cell($kiri,$lebarCell,"Order No :",0,0,'R',true);
	        $pdf->Cell($titleCol2,$lebarCell,$row['orderno'],0,0,'L',true); 
			$pdf->Ln();
			
		    $pdf->Cell($no_urut,$lebarCell,"No",1,0,'C',true);
		    $pdf->Cell($product_id,$lebarCell,"ID Product",1,0,'C',true);
		    $pdf->Cell($product_name,$lebarCell,"Product Name",1,0,'C',true);
		    $pdf->Cell($qty,$lebarCell,"Qty",1,0,'C',true);
		    //$pdf->Cell($dp,$lebarCell,"DP",1,0,'C',true);
		    $pdf->Cell($bv,$lebarCell,"BV",1,0,'C',true);
		    //$pdf->Cell($total_dp,$lebarCell,"Total DP",1,0,'C',true);
		    $pdf->Cell($total_bv,$lebarCell,"Total BV",1,0,'C',true);
		    $pdf->Ln();
		    $x = 1;
		   
		    $total_qty = 0;
		    $total_dpR = 0;
		    $total_bvR = 0;
		    foreach($row['detPrd'] as $dta2)
		    {
		        $dpR = $dta2->dpr * $dta2->qty;
		        $bvR = $dta2->bvr * $dta2->qty;
		       
		        $pdf->Cell($no_urut, $lebarCell,$x,1,0,'C',true);
		        $pdf->Cell($product_id, $lebarCell,"$dta2->prdcd",1,0,'L',true);
		        $pdf->Cell($product_name, $lebarCell,ucwords(strtolower($dta2->prdnm)),1,0,'L',true);
		        $pdf->Cell($qty, $lebarCell,number_format($dta2->qty,0,".",","),1,0,'R',true);
		        //$pdf->Cell($dp, $lebarCell,number_format($dta2->dpr,0,".",","),1,0,'R',true);
		        $pdf->Cell($bv, $lebarCell,number_format($dta2->bvr,0,".",","),1,0,'R',true);
		        //$pdf->Cell($total_dp, $lebarCell,number_format($dpR,0,".",","),1,0,'R',true);
		        $pdf->Cell($total_bv, $lebarCell,number_format($bvR,0,".",","),1,0,'R',true);
		        $pdf->Ln();
		       
		        $total_qty += $dta2->qty;
		        $total_dpR += $dpR;
		        $total_bvR += $bvR;
		        $x++;
		    }
			    $tot = $no_urut + $product_id + $product_name;
				$tot2 = $dp + $bv;
			    $pdf->Cell($tot, $lebarCell,"T o t a l",1,0,'C',true);
		        $pdf->Cell($qty, $lebarCell,number_format($total_qty,0,".",","),1,0,'R',true);
		        //$pdf->Cell($tot2, $lebarCell,"",1,0,'R',true);
		        //$pdf->Cell($total_dp, $lebarCell,number_format($total_dpR,0,".",","),1,0,'R',true);
		        $pdf->Cell($total_bv + $bv, $lebarCell,number_format($total_bvR,0,".",","),1,0,'R',true);
		        $pdf->Ln();
		    
		} 

		
	}
	
	$pdf->Output(); 
	
	

?>
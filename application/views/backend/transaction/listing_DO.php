<?php
    if(empty($listDO)){
        echo setErrorMessage();
    }else{
?>
<div class="overflow-auto">
    <form id="groupingDO" method="post" >
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th><input type="checkbox" name="checkall" onclick="All.checkUncheckAll(this);"/></th>
                    <th>Order No</th>
                    <th>CN No</th>
                    <th>KW No</th>
                    <th>Qty Prd</th>
                    <th>Date Trans</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach($listDO as $row){
                        echo "<tr>
                                <td align=\"center\"><input type=\"checkbox\" id=\"ceks[]\" name=\"ceks[]\" value=\"$row->KWno\"></td>
                                <td>".$row->orderno."</td>
                                <td>".$row->CNno."</td>
                                <td>".$row->KWno."</td>
                                <td align=\"center\">".$row->qty_prd."</td>
                                <td align=\"center\">".date("d/m/y",strtotime($row->datetrans))."</td>
                             </tr>";
                    }
                ?>
                <!--<tr>
                <td colspan="6"></td>
                </tr>-->
            </tbody>
        </table>
        <input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>
    </form>
    <br />
</div>

  <?php }?> 
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>
<?php
    $pdf = new LABELPDF('5161');
	
	$pdf->AddPage();
	
	$pdf->Set_Font_Size("9");

	foreach($result as $row) {
		$text = sprintf("%s\n%s\n%s\n%s", "From : PT K-Link Indonesia", "To : ".$row['nmmember'], "HP : ".$row['contactno'], ucwords(strtolower($row['addr'])));
	    $pdf->Add_Label($text);
	}
	
	$pdf->Output();
?>
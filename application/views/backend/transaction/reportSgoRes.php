<style>
	.xlText {
	    mso-number-format: "\@";
	}
</style>


<?php
    if(empty($rptSgo)){
        echo setErrorMessage();
    }else{
    	
		if($exportTo == 0){
	    	header("Content-type: application/vnd.ms-excel");
		    header("Content-Disposition: attachment; filename=ReportSGORes.xls" );
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");
			
			$rptHead = "<tr><th>Report From Date </th><th>$from to $to</th></tr><tr></tr>";
			$border = 'border="1" ';
		}else{
			$rptHead = '';
			$border = '';
		}
		
?>
<div class="overflow-auto">
    <form id="groupingDO" method="post" >
        <table <?php echo $border; ?> width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            	<?php echo $rptHead; ?>
            <!--<tr >
                <th colspan="31" style="font-weight: bold;text-align: center;background-color:#f4f4f4 ;">List Trx SGO</th>
             </tr>-->
                <tr>
                    <th>No</th>
                    <th>Trx ID</th>
                    <!--<th>Anchor</th>
                    <th>Bussiness Scheme</th>
                    <th>Community</th>
                    <th>Member</th>
                    <th>CCY</th>-->
                    <th>Amount</th>
                    <th>Trx Date</th>
                    <!--<th>Status</th>-->
                    <th>Bank Prod.</th>
                    <!--<th>Prdcd Value</th>
                    <th>Pay Reff</th>
                    <th>Created Date</th>
                    <th>Last Updt</th>
                    <th>Desc</th>-->
                    <th>Order ID</th>
                    <!--<th>Flag</th>-->
                    <th>CN No.</th>
                    <!--<th>KW No.</th>
                    <th>IP No.</th>-->
                    <th>Dist. Code</th>
                    <th>Name</th>
                    <th>Bank Code</th>
					<th>No VA</th>
                    <!--<th>Bank Desc.</th>-->
                    <th>Pay Sales</th>
                    <th>Pay IPG</th>
                    <th>Pay Ship</th>
                    <th>Trf Amount</th>
					<th>Voucher K-NET</th>
                    <th>Trx Type</th>
                    <th>Selisih Harga</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
					$TotAmount = 0;
					$TotPaySales = 0;
					$TotPayIPG = 0;
					$TotPayShip = 0;
					$TotPayTotal = 0;
					$TotPayVch = 0;
                    $TotPayCP = 0;
					
                    foreach($rptSgo as $row){
                    	if($exportTo == 0){ //jika export ke excel, maka format
                    		//echo "MASUK KE 0000";
                    		$Amount 	= number_format($row->Amount,0,"","");
							$PaySales	= number_format($row->total_pay,0,"","");
							$PayIPG		= number_format($row->payAdm,0,"","");
							$PayShip	= number_format($row->payShip,0,"","");
							$PayTotal	= number_format($row->payamt,0,"","");
							$PayVch	= number_format($row->P_VOUCHER,0,"","");
                            $PayCP      = number_format($row->selisihCustPrice,0,"","");
						}else{
							//echo "MASUK KE 1111";
							$Amount 	= number_format($row->Amount,0,"",",");
							$PaySales	= number_format($row->total_pay,0,"",",");
							$PayIPG		= number_format($row->payAdm,0,"",",");
							$PayShip	= number_format($row->payShip,0,"",",");
							$PayTotal	= number_format($row->payamt,0,"",",");
							$PayVch	= number_format($row->P_VOUCHER,0,"",",");
                            $PayCP      = number_format($row->selisihCustPrice,0,"","");
						}
                        
                        if($row->tipeLogin == '0'){
                            $typess = 'Trx Umroh';
                        }elseif($row->tipeLogin == '1' || $row->tipeLogin == Null){
                            $typess = 'Trx from Member Price';
                        }elseif($row->tipeLogin == '2'){
                            $typess = 'Trx from Consument Price';
                        }else{
                            $typess = 'Trx from Landing Page';
                        }

                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$row->Transaction_ID."</td>";
                                //<td>".$row->Anchor."</td>";
                                //<td>".$row->Business_Scheme."</td>
                                //<td>".$row->Community."</td>
                                //<td>".$row->Member."</td>
                                //<td>".$row->CCY."</td>
                                echo "<td align='right'>$Amount</td>
                                <td>".date("d/m/y",strtotime($row->Transaction_Date))."</td>";
                                //<td>".$row->Status."</td>
                                echo "<td>".$row->Bank_Product."</td>";
                                echo "<td align='left' class='xlText'>".$row->Product_Value."</td>";
                                //<td>".$row->Payment_Ref."</td>
                                //<td>".date("d/m/y",strtotime($row->Created))."</td>
                                //<td>".date("d/m/y",strtotime($row->Lastupdated))."</td>
                                //<td>".$row->Description."</td>
                                echo "<td class='str'>".$row->Order_ID."</td>";
                                //<td>".$row->flagRecover."</td>
                                
                                 echo "<td>".$row->CNno."</td>";
                                //<td>".$row->KWno."</td>
                                //<td>".$row->IPno."</td>
                                echo "<td>".$row->id_memb."</td>
                                <td>".$row->nmmember."</td>
                                <td>".$row->bankCode."</td>";
								
                                //<td>".$row->bankDesc."</td>
                                echo "<td align='right'>$PaySales</td>
                                <td align='right'>$PayIPG</td>
                                <td align='right'>$PayShip</td>
                                <td align='right'>$PayTotal</td>
								<td align='right'>$PayVch</td>
                                <td align='right'>".$typess."</td>
                                <td align='right'>".$PayCP."</td>                                  
                             </tr>";
                             $no++;
							 
							$TotAmount 		= $TotAmount + $row->Amount;
							$TotPaySales 	= $TotPaySales + $row->total_pay;
							$TotPayIPG 		= $TotPayIPG + $row->payAdm;
							$TotPayShip 	= $TotPayShip + $row->payShip;
							$TotPayTotal 	= $TotPayTotal + $row->payamt;
							$TotPayVch 	=     $TotPayVch + $row->P_VOUCHER;
                            $TotPayCP      += $row->selisihCustPrice;
                    }
					if($exportTo == 0){ //jika export ke excel, maka format
                		//echo "MASUK KE 0000";
                		$TotAmount 	 = number_format($TotAmount,0,"","");
                		$TotPaySales = number_format($TotPaySales,0,"","");
                		$TotPayIPG 	 = number_format($TotPayIPG,0,"","");
                		$TotPayShip  = number_format($TotPayShip,0,"","");
                		$TotPayTotal = number_format($TotPayTotal,0,"","");
						$TotPayVch    = number_format($TotPayVch,0,"","");
                        $TotPayCP    = number_format($TotPayCP,0,"","");
					}else{
						//echo "MASUK KE 1111";
                		$TotAmount 	 = number_format($TotAmount,0,"",",");
                		$TotPaySales = number_format($TotPaySales,0,"",",");
                		$TotPayIPG 	 = number_format($TotPayIPG,0,"",",");
                		$TotPayShip  = number_format($TotPayShip,0,"",",");
                		$TotPayTotal = number_format($TotPayTotal,0,"",",");
						$TotPayVch = number_format($TotPayVch,0,"",",");
                        $TotPayCP    = number_format($TotPayCP,0,"","");
					}
					echo "<tr>
		                  	<td colspan=\"2\">Total</td>
		                  	<td align='right'>$TotAmount</td>
		                  	<td colspan=\"7\"></td>
                            <td align='right'>$TotPaySales</td>
                            <td align='right'>$TotPayIPG</td>
                            <td align='right'>$TotPayShip</td>
                            <td align='right'>$TotPayTotal</td>
							<td align='right'>$TotPayVch</td>
                            <td>Total Selisih</td> 
                            <td align='right'>".$TotPayCP."</td> 
		                  </tr>";
                ?>
                <!--<tr>
                <td colspan="6"></td>
                </tr>-->
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->
    </form>
    <br />
</div>

  <?php }?> 

<script type="text/javascript">
/*$(document).ready(function() 
{
	All.set_datatable();
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });*/

</script>
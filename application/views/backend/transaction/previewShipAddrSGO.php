<?php
    if(empty($result)){
        setErrorMessage();
    }else{
?>
	<form id="updateShipAddr">
	<table width="100%" class="table table-striped table-bordered">
	 <thead>
	 	<th colspan="2">Data Shipping from Temporary Table SGO</th>
	 </thead>
	 <tbody>	
    <?php
        $no = 1;
        $total = 0;
        foreach($result as $row) {
    ?>
	    <tr>
	      <td width="25%">Order No</td>
	      <td><?php echo $orderno; ?></td>	
	    </tr>
        <tr>
	      <td>Token No</td>
	      <td><?php echo $row->orderno; ?></td>	
	    </tr>
	    
	    <tr>
	      <td>ID Stockist</td>
	      <td><?php echo $row->idstk; ?></td>	
	    </tr>
	    <tr>
	      <td>Receiver Name</td>
	      <td><?php echo $row->receiver_name; ?></td>	
	    </tr>
	    <tr>
	      <td>Cell No</td>
	      <td><?php echo $row->tel_hp1; ?></td>	
	    </tr>
	    <tr>
	      <td>Address</td>
	      <td><?php echo $row->addr1; ?></td>	
	    </tr>
	    <tr>
	      <td>Kabupaten</td>
	      <td><?php echo $row->kabupaten_name; ?></td>	
	    </tr>
	    <tr>
	      <td>Province</td>
	      <td><?php echo $row->province_name; ?></td>	
	    </tr>
	    <tr>
	      <td>Total Item / Total Weight</td>
	      <td><?php echo $row->total_item." / ".$row->total_weight; ?></td>	
	    </tr>
    <?php
		} 
	?>
    
  </tbody>  
</table>
</form>
	<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
		 <input value="Insert Data Shipping" type="button" class="btn btn-small btn-primary" onclick="be_trans.updateShipAddr('<?php echo $orderno; ?>','<?php echo $result[0]->orderno; ?>')"/>
    </div>
	<p></p>
<?php
    }
?>

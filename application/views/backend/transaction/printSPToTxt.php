<?php
$time = date("dmyhis");
header("Content-Type: plain/text");
header("Content-Disposition: Attachment; filename=Product_Sales_Summary_".$time.".txt");
header("Pragma: no-cache");

date_default_timezone_set("Asia/Jakarta");

$tgl = date("d F Y");
$waktu = date("d/m/y h:i:s");

function tmbh_spaceHeaderxxx($value)
{
    $kosong = '';
    for($x = 1; $x <= $value; $x++)
    {
        $kosong .= " ";
    }

    echo $kosong;
}

function titleHeader()
{
    echo "No";
    tmbh_spaceHeaderxxx(2);
    echo "Kode Produk";
    tmbh_spaceHeaderxxx(10);
    echo "Nama Produk";
    tmbh_spaceHeaderxxx(35);
    /*echo "Qty";
    tmbh_spaceHeaderxxx(12);*/
    echo "Qty";
    echo "\n";

    //garisStripSum();
}

function titleKosong(){
    echo "-";
    tmbh_spaceHeaderxxx(4);
    echo "-";
    tmbh_spaceHeaderxxx(17);
    echo "-";
    tmbh_spaceHeaderxxx(20);
    echo "-";
    tmbh_spaceHeaderxxx(20);
    echo "-";
    tmbh_spaceHeaderxxx(10);
    echo "-";
}

/*foreach($dtVch as $dtax) {
    if($dtax->claimstatus =='1'){
        $y = 'VOUCHER SUDAH DI CLAIM';
    }else{
        $y = 'VOUCHER BELUM DI CLAIM';
    }
}*/

echo "\n\n\n";
echo "                                 PT.K-Link Indonesia\n\n";
echo "                              LIST PRODUCT SALES SUMMARY\n\n\n";

garisStrip();
echo "\n";
titleHeader();
echo "\n";
garisStrip();
echo "\n";

$no=0;
/*foreach ($sql->result() as $r) {
}*/
    $no++;
    $qty=999;
    $nk1 = 4;
    $nk2 = 25;
    $nk3 = 71;
    //$nk4 = 68;

    $nama="K-LIQUID CHLOROPHYLL (500ML)";
    $nomor="6287784804998";

    $str1= strlen($no);
    $str2= strlen("HD006B");
    $str3= strlen($nama);
    $str4= strlen($qty);
    //$str5= strlen($qty);

    if($str3 > 24){
        $str3= strlen(substr($nama,0,24));
    }else{
        $str3= $str3;
    }

    $kolom1= ($nk1 - ($str1)); // ok
    $kolom2= ($nk2 - ($nk1 + $str2)); //ok
    $kolom3= ($nk3 - ($str1 + $kolom1 + $str2 + $kolom2 + $str3));
    //$kolom4= ($nk4 - ($str1 + $kolom1 + $str2 + $kolom2 + $str3 + $kolom3 + $str4));

    echo $no;

    tmbh_spaceHeaderxxx($kolom1);
    echo "HD006B";

    tmbh_spaceHeaderxxx($kolom2);
   // echo substr($nama,0,24);
    echo $nama;

    /*if ($sql1->num_rows() > 0) {
        foreach ($sql1->result() as $row) {
            $idm = trim("HD006B");
            $msidn= trim("HD006B");
            $n2=[$idm=>$msidn];

            if (key_exists("HD006B", $n2))
            {
                $col3=$n2["HD006B"];

            }
        }
    }*/
    /*tmbh_spaceHeaderxxx($kolom3);
    echo "HD006B";*/

    tmbh_spaceHeaderxxx($kolom3);
    echo number_format(999,0,".",",");

    echo "\n";
    //garisStrip();
    //echo "\n";

    // tmbh_spaceHeaderxxx($nk3);
   /* if ($sql1->num_rows() > 0) {
        foreach ($sql1->result() as $row) {
            $idm = trim($row->dfno);
            $msidn= trim($row->msidn);
            $n2=[$idm=>$msidn];

            if (key_exists($r->idmemb, $n2))
            {
                tmbh_spaceHeaderxxx($nk3);
                $rep=str_replace($col3,"",$n2[$r->idmemb]);
                echo $rep;

                if($rep == ''){
                    $nom='';
                }else{
                    $nom=number_format(105000,0,".",",");
                }
                tmbh_spaceHeaderxxx($kolom4);
                echo $nom."\n";

            }
        }
    }*/

    garisStrip();
    echo "\n";
//}
/*titleKosong();
echo "\n";
garisStrip();
echo "\n";*/
//$tot1= array_sum($nsum);
tmbh_spaceHeaderxxx(49);
echo "*Total DP Amount :  ".number_format(999,0,".",",");
echo "\n";
tmbh_spaceHeaderxxx(67);
echo "==========";
echo "\n";

echo $kolom1." ".$kolom2." ".$kolom3;

?>
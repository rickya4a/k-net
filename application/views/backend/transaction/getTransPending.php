<div class="mainForm">
  <form class="form-horizontal" id="formTrxPending" method="post" target="_blank" action="<?php echo site_url("trans/report/toExcel");?>">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Trx Type</label>                             
        <div class="controls">
        	<select id="trx_type" name="trx_type">
        		<option value="">--Select Here--</option>
        		<option value="EC">E-Commerce</option>
        		<option value="RM">Member Registration</option>
        		
        	</select>
        </div>
      	<label class="control-label" for="typeahead">Search By</label>                             
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Select Here--</option>
        		<option value="id_memb">ID Member</option>
        		<option value="idstk">ID Stockist</option>
        		<option value="log_usrlogin">User Login</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead">Parameter Value</label>                             
        <div class="controls">
        	<input type="text" id="paramValue" name="paramValue" class="span4" />
        </div>
        <label class="control-label" for="typeahead">Sent To</label>                             
        <div class="controls">
        	<select id="sentTo" name="sentTo">
        		<option value="">--Select Here--</option>
        		<option value="1">Stockist</option>
        		<option value="2">Address</option>
        	</select>
        </div>
        		      
        <?php
          echo datepickerFromTo("Transaction Date", "trx_froms", "trx_tos");
		  echo bonusPeriod();
        ?>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'trans/report/pending/list')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="4"  type="submit" class="btn btn-success" value="To Excel" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

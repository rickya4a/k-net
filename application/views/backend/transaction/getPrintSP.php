<div class="mainForm">
    <form class="form-horizontal" id="formSP" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" >
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">Transaction Date</label>
                <div class="controls">
                    <input type="text" class="dtpicker typeahead" id="cn_from" name="cn_from" >&nbsp;to&nbsp;
                    <input type="text"  class="dtpicker typeahead" id="cn_to" name="cn_to" >
                </div>
                <label class="control-label" for="typeahead">Status Kirim</label>
                <div class="controls">
                    <select id="statusKirim" name="statusKirim">
                        <option value="1">Pembayaran Sukses</option>
                        <option value="2">Menunggu Pembayaran <=4 Jam</option>
                        <option value="3">Menunggu Pembayaran >4 Jam</option>
                        <option value="0">Menunggu Pembayaran All</option>
                    </select>
                </div>
                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListSP()" />
                    <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
                    <input type="hidden" name="SPStatus" value="0" />
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->
<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());;
    });
</script>

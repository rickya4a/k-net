<form action="" id="formSave" name="formSave" method="post" class="form-horizontal">
    <div class="control-group">
        <h5 align="center">TRANSACTION DATA</h5><br>
        <label class="control-label" for="typeahead">No. Token</label>
        <div class="controls">
            <input type="text" class="span4" name="token" id="token" value="<?php echo $token?>" readonly>
        </div>

        <label class="control-label" for="typeahead">ID Member</label>
        <div class="controls">
            <input type="text" style="width: 150px;" name="idmemb" id="idmemb" value="<?php echo $idmemb?>" onchange="All.getFullNameByID(this.value,'be/memberinfo/msmemb/dfno','#nmmemb')">
            <input type="text" style="width: 250px;" name="nmmemb" id="nmmemb" value="<?php echo $nmmemb?>" readonly>
        </div>

        <label class="control-label" for="typeahead">ID Stockist</label>
        <div class="controls">
            <input type="text" style="width: 150px;" name="idstk" name="idstk" value="<?php echo $idstk?>" onchange="All.getFullNameByID(this.value,'be/memberinfo/mssc/loccd','#nmstk')">
            <input type="text" style="width: 300px;" name="nmstk" id="nmstk" value="<?php echo $nmstk?>" readonly>
        </div>

        <label class="control-label" for="typeahead">Send To</label>
        <div class="controls">
            <select name="sent_to" id="sent_to" class="span4">
                <option value="1">Stockist</option>
                <option value="2">Alamat</option>
            </select>
        </div>

        <label class="control-label" for="typeahead">Discount Shipping</label>
        <div class="controls">
            <input type="text" class="span2" name="disc" id="disc" value="<?php echo $disc?>">
        </div>

        <label class="control-label" for="typeahead">Bonus Month</label>
        <div class="controls">
            <input type="text" class="span2" name="bonus" id="bonus" value="<?php echo $bonus?>">
        </div>

        <div class="controls">
            <input type="button" class="btn btn-warning" value="BACK" onclick="be_trans.back_to_form()">
            <input type="button" class="btn btn-danger" value="SUBMIT" onclick="All.jsonPostRequest('formSave','backend/transaction/updateHdrSgo')">
            <input type="button" class="btn btn-info" value="RECONSILE" onclick="reconcileSgoTrx('<?php echo $token?>')">
        </div>
    </div>
</form>

<script>
    function reconcileSgoTrx(orderid) {
        All.set_disable_button();
        $.ajax({
            url: All.get_url('api/sgo/renotif'),
            type: 'POST',
            data: {order_id: orderid},
            dataType: 'json',
            success:
                function(data){
                    All.set_enable_button();
                    alert(data.message);

                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    }
</script>

<div class="mainForm">
    <div class="control-group">
        <form id="reInputTrx" class="form-horizontal" name="reInputTrx">
            <table style="width: 100%;" class="table table-bordered" >
                <tr>
                    <td colspan="2" style="text-align: center;font-size: medium;" >TRANSACTION DATA</td>
                </tr>
                <tr>
                    <td style="width:50%;">
                        <table style="width: 100%;" class="table table-bordered">
                            <tr>
                                <td>TokenID</td>
                                <td><input type="text" id="tokenid" name="tokenid" tabindex="1" class="span11 uppercase" placeholder="contoh RM1601000001"/></td>
                            </tr>
                            <tr>
                                <td>Userlogin</td>
                                <td><input type="text" id="userlogin" name="userlogin" tabindex="2" class="span11 uppercase" onchange="be_member.checkUserlogin(this.value)"/></td>
                            </tr>
                            <tr>
                                <td>Nama Userlogin</td>
                                <td><input type="text" id="userloginnm" name="userloginnm" readonly="yes" class="span11 uppercase"/></td>
                            </tr>
                            <tr>
                                <td>ID Member</td>
                                <td><input type="text" id="idmember" name="idmember" tabindex="3" class="span11 uppercase" onchange="All.getFullNameByID(this.value,'be/member/info/id/','#membername')"/></td>
                            </tr>
                            <tr>
                                <td>Nama Member</td>
                                <td><input type="text" id="membername" name="membername" readonly="yes" class="span11 uppercase"/></td>
                            </tr>
                            <tr>
                                <td>Stockist Terdekat</td>
                                <td>
                                    <input type="text" id="idstk" name="idstk" tabindex="4" class="span11 uppercase" onchange="All.getFullNameByID(this.value,'be/member/stk/id/','#stkname')"/>
                                    <!--<input type="text" id="idstk" name="idstk" tabindex="4" class="span11 uppercase" onchange="All.getFullNameByID(this.value,'klinklmlm/get/fullnm/from/mssc/loccd','#stkname')"/>-->
                                
                                </td>
                            </tr>
                            <tr>
                                <td>Nama Stockist</td>
                                <td><input type="text" id="stkname" name="stkname" readonly="yes" class="uppercase span11"/></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width:50%;">
                        <table style="width: 100%;" class="table table-bordered">
                            <tr>
                                <td>No. Trx</td>
                                <td><input type="text" readonly="yes" value=">> Auto <<" id="trxnoo" name="trxnoo" class="uppercase span11"/></td>
                            </tr>
                            <tr>
                                <td>Orderno</td>
                                <td><input type="text" id="ordernoo" name="ordernoo" class="uppercase span11" value=">> Auto <<" readonly="yes"/></td>
                            </tr>
                            <tr>
                                <td>Tanggal Trx</td>
                                <td><input type="text" readonly="yes" value="<?php echo $dateNow;?>" id="trxdate" name="trxdate" class="uppercase span11"/></td>
                            </tr>
                            <tr>
                                <td>Price Code</td>
                                <td><input type="text" readonly="yes" value="12W3" id="pricecode" name="pricecode" class="uppercase span5"/></td>
                            </tr>
                            <tr>
               					<td class="form_title">Bonus Period&nbsp;</td>
                    			<td>
                    				<select name="bnsperiod" id="bnsperiod" class="form-list span10" tabindex="5">
                                      <?php
                                        
                                        $xx = $currentperiod['arrayData'];
                                       for($i=0; $i <= $xx[0]->rangeperiod; $i++) {
                						 	$x = addRangeOneMonth($xx[0]->lastperiod, $i);
                							echo $x."<br />";
                							echo "<option value=\"$x\">$x</option>";
                						 }
                                        
                                      ?>
                                     </select>
            				    </td>
                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td><input type="text" id="remarks" name="remarks" tabindex="6" class="uppercase span11" /></td>
                            </tr>
                            <tr>
                                <td>Ship To</td>
                                <td><input type="radio" id="delivery" name="delivery" value="1" checked="checked" onclick="be_trans.getDelChoice(this.value)"/>Stockist <input type="radio" id="delivery" name="delivery" value="2" tabindex="8"  onclick="be_trans.getDelChoice(this.value)"/>Address</td>
                            </tr>
                            
                        </table>
                    </td>  
                </tr>
            </table>
            <table class="table table-striped table-bordered" style="width: 100%;">
                <tr><td colspan="2" style="text-align: center;font-size: medium;" >SHIPPING INFO</td></tr>
                    <tr>
                        <td>Nama Penerima</td><td><input type="text" class="uppercase span10 shipto" id="receivername" name="receivername" readonly="yes"/></td>
                    </tr>
                    <tr>
                        <td>Alamat Penerima</td><td><input type="text" class="uppercase span10 shipto" id="addressDelivery" name="addressDelivery" readonly="yes"/></td>
                    </tr>
                    <tr>
                        <td>Tel Hp Penerima</td><td><input type="text" class="uppercase span10 numeric-input shipto" id="hpReceiver" name="hpReceiver" readonly="yes"/></td>
                    </tr>
                    <tr>
                        <td>Email Penerima</td><td><input type="text" class="span10 shipto" id="emailReceiver" name="emailReceiver" readonly="yes"/></td>
                    </tr>
            </table>
            <table class="table table-striped table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Prd Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>BV</th>
                        <th>Total</th>
                        <th>Act</th>
                    </tr>
                <tbody id="dataPrd">  
                </tbody>
                <tbody id="SS">
                    <tr>
                        <td align="right"><input type="button" class="btn btn-danger" name="new_record" id="new_record" value="Add 1 row" onclick="be_trans.add_new_row()" tabindex="7"/></td>
                        <td colspan="3" align="right">T O T A L</td>
                        <td><input readonly="yes" type="text" style="text-align:right;" class="span12 typeahead" id="total_all_bv"  name="total_all_bv" /></td>
                        <td>
                            <input readonly="yes" type="text" style="text-align:right;" class="span12 typeahead" id="total_all"  name="total_all" />
                            <input type="hidden" id="total_all_real" name="total_all_real" />
                            <input type="hidden" id="total_all_real_bv" name="total_all_real_bv" />
                            <input type="hidden" id="totals_all_bv" name="totals_all_bv" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                        <input type="hidden" id="action" name="action" value="<?php echo $form_action; ?>" />
                        <input type="hidden" name="amount" id="amount" value="1" />
                        <input type="hidden" name="tabidx" id="tabidx" value="1" />
                        <input type="hidden" name="amt_record" id="amt_record" value="0" />
                </tbody> 
                </thead>
            </table>
        </form>
    </div>
</div>
<script>
$(function() {
    $(All.get_active_tab() + ' .uppercase').bind('change', function() {
	  $(this).val($(this).val().toUpperCase().replace(/'/g,"`"));
	});
    
    $('.numeric-input').keydown(function (e) {
	    // Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
		return;
		}
	    // Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
		}
	});
});

</script>
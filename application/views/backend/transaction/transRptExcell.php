<?php
    if(empty($arr)){
        echo setErrorMessage();
    }else{
        $filenm = "report.xls";
        header("Content-type: application/vnd.ms-excel");
        header('Content-Disposition: attachment; filename='.$filenm.'');
        header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	    header("Pragma: public");
        
       echo "<table width=\"100%\" border = \"1\">";
       echo "<thead><tr><th colspan=\"9\" bgcolor=\"#lightgrey\">List Transaction</th></tr>";
       echo "<tr>";
       echo "<th>No</th>";
       echo "<th>Order No</th>";
       echo "<th>Token No</th>";
       echo "<th>Member Name</th>";
	   echo "<th>Stockist</th>";
	   echo "<th>Trx Date</th>";
	   echo "<th>Qty</th>";
	   echo "<th>DP</th>";
       echo "<th>BV</th>";
	   echo "<th>WH</th>";
	   echo "<th>Conote</th>";
       echo "</tr></thead>";
       echo "<tbody>";
       $i = 1;
       foreach($arr as $list) {
            echo "<tr id=\"$i\">";
                echo "<td align=\"right\">$i</td>";
                echo "<td>".$list->orderno."</td>";
                echo "<td>".$list->token."</td>";
                echo "<td>".$list->nmmember."</td>";
                echo "<td align=\"left\">".$list->idstk."</td>";
                echo "<td align=\"right\">".$list->datetrans1."</td>";
                echo "<td align=\"right\">".$list->sumofPrd."</td>";
                echo "<td align=\"right\">".$list->total_pay."</td>";
                echo "<td align=\"right\">".$list->total_bv."</td>";
				echo "<td align=\"right\">".$list->wh_name."</td>";
				echo "<td align=\"right\">'".$list->conoteJNE."</td>";
            echo "</tr>";
            $i++;
       }
    }
?>
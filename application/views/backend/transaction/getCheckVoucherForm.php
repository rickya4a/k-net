<div class="mainForm">
  <form class="form-horizontal" id="formVcrSearch">
    <fieldset>      
      <div class="control-group">
      	
      	<label class="control-label" for="typeahead">Search By</label>                             
        <div class="controls">
        	<select id="searchby" name="searchby">
        		<option value="ip">Incoming No.</option>
        		<option value="vocuherno">Voucher No.</option>
        		<option value="stockist">Stockist</option>
        		
        	</select>
        </div>
       
        <span id="vcrno" style="display: none;">
          	<label class="control-label" for="typeahead">Value</label>                             
	        <div class="controls">
	        	<input type="text" id="paramValue" name="paramValue" class="span4" />
	        </div>
        </span>
        
        <span id="trxdate" style="display: none;"> <?php echo datepickerFromTo("Transaction Date", "trx_from", "trx_to"); ?></span>
 
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListVcr()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		if( $("#searchby").val() == "vocuherno" || $("#searchby").val() == "ip"){
		        $("#vcrno").css('display', 'block');
		        $("#trxdate").css('display', 'none');
		     }
		
		$("#searchby").change(function() {
		     if( $("#searchby").val() == "vocuherno" || $("#searchby").val() == "ip"){
		        $("#vcrno").css('display', 'block');
		        $("#trxdate").css('display', 'none');
		     }else if($("#searchby").val() == "stockist"){
		        $("#vcrno").css('display', 'block');
		        $("#trxdate").css('display', 'block');
		     }
		 });
		 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

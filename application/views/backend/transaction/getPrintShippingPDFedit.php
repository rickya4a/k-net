<?php
//$pdf=new PDF_Code128('P','mm','A4');
$pdf=new PDF_Code39('P','mm','A4');
$pdf->Open();
//$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown	
$pdf->SetLineWidth(.1);
$pdf->SetMargins(0,0,0,0);
//$pdf->AddPage();

//$pdf->SetXY(10,50);
     
    $titleCol2 = 155;
    $lebarCell = 5;
	$left = 30;
	$border = 0;
	
	 //setting header product column width
    $no_urut = 7;
    $product_id = 20;
    $product_name = 80;
    $qty = 9;
    $dp = 23;
    $bv = 8;
    $kolom_total = $no_urut + $product_id + $product_name + $qty + $dp + $bv;
    $total_dp = 23;
    $total_bv = 15;

$x_pos = 0;
$y_pos = 0;

$pdf->SetMargins(0,0,0,0);

foreach ($result as $key) {   
    
    if($x_pos == 0 && $y_pos ==0) {
        $pdf->AddPage();
    }
    
    $pdf->Line(70 + $x_pos, 5 + $y_pos, 5 + $x_pos, 5 + $y_pos); // garis hor atas
    $pdf->Line(70 + $x_pos, 35 + $y_pos, 5 + $x_pos, 35 + $y_pos); // garis hor bawah
    $pdf->Line(5 + $x_pos, 35 + $y_pos, 5 + $x_pos, 5 + $y_pos); // garis ver kiri
    $pdf->Line(70 + $x_pos, 5 + $y_pos, 70 + $x_pos, 35 + $y_pos); // garis ver kanan


    $pdf->SetFont('Arial', 'B', 13); 
    $pdf->Text(7 + $x_pos, 10 + $y_pos, "Conote JNE Test");

    $pdf->SetFont('Arial', '', 8);
    $pdf->Text(7 + $x_pos, 15 + $y_pos, 'Nama Penerima : '.$key['receiver_name']);
    $pdf->Text(7 + $x_pos, 20 + $y_pos, 'No. HP : '.$key['contactno']);
    $pdf->Text(7 + $x_pos, 25 + $y_pos, 'Conote JNE : '.$key['conoteJNE']);
    //$pdf->Code39(7 + $x_pos,28 + $y_pos,$key['conoteJNE'],40,5);
	$conoteno = $key['conoteJNE'];
	$pdf->Code39(80,40,$conoteno,1,10);
	//$pdf->Code128($titleCol2,$lebarCell,$key['conoteJNE'],$border,0,'L',true);
	//$pdf->Code39($titleCol2,$lebarCell,$key['conoteJNE'],$border,0,'L',true);
	
    
    $x_pos = $x_pos + 70;
    
    if($x_pos >= 150 ) {
        $x_pos = 0;
        $y_pos = $y_pos + 35;
        if($y_pos >= 340){
            $y_pos=0;
        }
    }            

}     
$pdf->Output();
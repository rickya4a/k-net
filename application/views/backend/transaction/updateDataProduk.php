<?php
	$header = $hasil['header'];
	$produk = $hasil['produk'];
?>
<form id="frmUpdPrd">
<table width="100%" class="table table-striped table-bordered">
	<tr>
		<th colspan="4">Data Summary Transaction</th>
	</tr>
	<tr>
		<td width="15%">Orderno</td>
		<td colspan="3"><?php echo $header[0]->orderno ?><input type="hidden" id="orderno" name="orderno" value="<?php echo $header[0]->orderno ?>" /></td>
	</tr>
	<tr>
		<td>KW No</td>
		<td colspan="3"><?php echo $header[0]->KWno ?><input type="hidden" id="KWno" name="KWno" value="<?php echo $header[0]->KWno ?>" /></td>
	</tr>
	<tr>
		<td>CN No</td>
		<td colspan="3"><?php echo $header[0]->CNno ?>
		<input type="hidden" id="CNno" name="CNno" value="<?php echo $header[0]->CNno ?>" />
		<input type="hidden" id="total_pay" name="total_pay" value="<?php echo $header[0]->total_pay ?>" />
		<input type="hidden" id="total_bv" name="total_bv" value="<?php echo $header[0]->total_bv ?>" />
		<input type="hidden" id="pricecode" name="pricecode" value="<?php echo $header[0]->pricecode ?>" />
		<input type="hidden" id="gdo" name="gdo" value="<?php echo $header[0]->GDO ?>" />
		<input type="hidden" id="sentToStt" name="sentToStt" value="<?php echo $header[0]->sentTo ?>" />
		<input type="hidden" id="conoteJNE" name="conoteJNE" value="<?php echo $header[0]->conoteJNE ?>" />
		</td>
	</tr>
	<tr>
		<td>Total Pay / BV</td>
		<td colspan="3"><?php echo $header[0]->total_pay." / ".$header[0]->total_bv ?></td>
	</tr>
	<tr>
		<td>Sent To</td>
		<td><?php 
		 if($header[0]->sentTo == "1") {
			echo $header[0]->nmstkk;
		 } else {
			echo $header[0]->conoteJNE. " / ".$header[0]->receiver_name;
		 } 	
		?></td>
		<td width="15%">No DO</td>
		<td width="30%">&nbsp;<?php echo $header[0]->GDO; ?></td>
	</tr>
</table>
<table width="100%" class="table table-striped table-bordered">
    <thead>
		<tr>
			<th colspan="7">Data Product</th>
		</tr>
		<tr>
			<th width="12%">Product Code</th>
			<th>Product Name</th>
			<th width="5%">Qty</th>
			<th width="8%">BV</th>
			<th width="13%">Harga</th>
			<th width="10%">Total BV</th>
			<th width="15%">Total Harga</th>
		</tr>
	</thead>
	<tbody id="TmbhPrd">
		<?php
			$i=1;
			$tot_all_bvr = 0;
			$tot_all_dpr = 0;
		    foreach($produk as $dta) {
			    $tot_bv = $dta->qty * $dta->bvr;
				$tot_dpr = $dta->qty * $dta->dpr;
				
				echo "<tr id=\"$i\">";
				echo "<td><input class=span20 type=\"text\" id=\"prdcd$i\" name=\"prdcd[]\" onchange=\"searchProduct('$i')\" value=\"$dta->prdcd\" /></td>";
				echo "<td><input readonly=\"readonly\" class=span20 type=\"text\" id=\"prdnm$i\" name=\"prdnm[]\" value=\"$dta->prdnm\" /></td>";
				echo "<td><input style=\"text-align:right\" class=span20 type=\"text\" onchange=\"searchProduct('$i')\" id=\"qty$i\" name=\"qty[]\" value=\"$dta->qty\" /></td>";
				echo "<td><input style=\"text-align:right\" class=span20 readonly=\"readonly\" type=\"text\" id=\"bvr$i\" name=\"bvr[]\" value=\"$dta->bvr\" /></td>";
				echo "<td><input style=\"text-align:right\" class=span20 readonly=\"readonly\" type=\"text\" id=\"dpr$i\" name=\"dpr[]\" value=\"$dta->dpr\" /></td>";
				echo "<td><input style=\"text-align:right\" class=span20 readonly=\"readonly\" type=\"text\" id=\"total_bvr$i\" name=\"total_bvr[]\" value=\"$tot_bv\" /></td>";
				echo "<td><input style=\"text-align:right\" readonly=\"readonly\" type=\"text\" id=\"total_dpr$i\" name=\"total_dpr[]\" value=\"$tot_dpr\" />
				<a class='btn btn-mini btn-danger' onclick=\"javascript:delProduct($i)\"><i class='icon-trash icon-white'></i></a>
				</td>";
				echo "</tr>";
				
				$tot_all_bvr += $tot_bv;
				$tot_all_dpr += $tot_dpr;
				$i++;
			}
		?>
	</tbody>
	<tbody id="divTotal">
		<tr>
			<td colspan="5">T O T A L</td>
			<td><input type="text" readonly=\"readonly\" style="text-align:right" id="totBVR" name="totBVR" value="<?php echo $tot_all_bvr; ?>" /></td>
			<td><input type="text" readonly=\"readonly\" style="text-align:right" id="totDPR" name="totDPR" value="<?php echo $tot_all_dpr; ?>" /></td>
		</tr>
	</tbody>
	<tbody>
		<tr><td colspan="8"><input type="button" id="btnUpdPrd" onclick="simpanProduk()" value="Update Product" class="btn btn-mini btn-primary" /></td></tr>
	</tbody>
</table>
</form>
<?php
backToNextForm();
?>
<script>
	function simpanProduk() {
		var form = $(All.get_active_tab() + " #frmUpdPrd").serialize();
		//console.log(form);
		var sentToStt = $(All.get_active_tab() + " #sentToStt").val();
		var gdo = $(All.get_active_tab() + " #gdo").val();
		var conoteJNE = $(All.get_active_tab() + " #conoteJNE").val();
		var totDPR = parseInt($(All.get_active_tab() + " #totDPR").val());
		var total_pay = parseInt($(All.get_active_tab() + " #total_pay").val());
		var err = 0;
		
		/*if(sentToStt == "1" && gdo !== "") {
			alert("Transaksi sudah di generate DO nya...");
			err++;
		} else {
			alert("ok : " +sentToStt);
		}*/
		
		if(totDPR > total_pay) {
			alert("Total nilai produk yang akan di update lebih besar dari yang sudah dibayar..");
			err++;
		}
		
		if(err == 0) {
			$.ajax({
                dataType: 'json',
                url: All.get_url("trans/updPrd/upd/save"),
                type: 'POST',
				data: form,
                success:
                function(data){
                    if(data.response == "true") {
					
					} else {
					
					}
				},
                error: function (xhr, ajaxOptions, thrownError) {
                     alert(thrownError + ':' +xhr.status);
                }
            }); 
		}
		
	}
	
	function searchProduct(frm) {
		var prdcd = $(All.get_active_tab() + " #prdcd" +frm).val();
		var pricecode = $(All.get_active_tab() + " #pricecode").val();
       $.ajax({
                dataType: 'json',
                url: All.get_url("product/id/") +prdcd,
                type: 'GET',
                success:
                function(data){
                    if(data !== null)
                    {
                           $(All.get_active_tab() + " #prdnm" +frm).val(data[0].prdnm);
						   if(pricecode == "12W3" || pricecode == "12W4") {
								var dp = parseInt(data[0].price_w);
						   } else if(pricecode == "12E3" || pricecode == "12E4") {
								var dp = parseInt(data[0].price_e);
						   }
						
                           var bv = parseInt(data[0].bv);
						   //var weight = parseFloat(data[0].weight);
                           var qty = parseInt($(All.get_active_tab() + " #qty" +frm).val());
						   console.log('qty : '+qty);
                           if(isNaN(qty)) 
                           {
                                console.log('masuk disini');
								qty = 1;
								$(All.get_active_tab() + " #qty" +frm).val('1');
                           } else {
								$(All.get_active_tab() + " #qty" +frm).val(qty);
						   }
                           var tot_dp = dp * qty;
                           var tot_bv = bv * qty;
						   //var tot_weight = weight * qty;
                           
                           
                            
                            $(All.get_active_tab() + " #qty" +frm).focus();
							$(All.get_active_tab() + " #bvr" +frm).val(bv);
							$(All.get_active_tab() + " #dpr" +frm).val(dp);
							$(All.get_active_tab() + " #total_bvr" +frm).val(tot_bv);
							$(All.get_active_tab() + " #total_dpr" +frm).val(tot_dp);
							
							hitungTotalPrd();
                    }
                    else
                    {
                        alert('product does not exist');
                        $(All.get_active_tab() + " #prdcd" +frm).val(null);
                        $(All.get_active_tab() + " #qty" +frm).val(null);
                        $(All.get_active_tab() + " #bvr" +frm).val(null);
						$(All.get_active_tab() + " #dpr" +frm).val(null);
						$(All.get_active_tab() + " #total_bvr" +frm).val(null);
						$(All.get_active_tab() + " #total_dpr" +frm).val(null);
                        $(All.get_active_tab() + " #prdcd" +frm).focus();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                     alert(thrownError + ':' +xhr.status);
                }
            }); 
	}
	
	function hitungTotalPrd() {
		var sentTo = $(All.get_active_tab() + " #sentTo").val();
		var gdo = $(All.get_active_tab() + " #gdo").val();
		
		if(sentTo == "1" && gdo != null) {
		    alert("Transaksi sudah di buat DO..");
			$(All.get_active_tab() + " #btnUpdPrd").attr("disabled", "disabled");
		} else {
			$(All.get_active_tab() + " #btnUpdPrd").removeAttr("disabled");
			var theForm = document.getElementById("frmUpdPrd");
			//var total_bvr = document.forms[0].elements["total_bvr[]"];
			//var total_dpr = document.forms[0].elements["total_dpr[]"];
			//alert(total_bvr.length);
			//console.log(theForm);
			
			var dpr = document.querySelectorAll("#frmUpdPrd input[name='dpr[]']");
			var bvr = document.querySelectorAll("#frmUpdPrd input[name='bvr[]']");
			var qty = document.querySelectorAll("#frmUpdPrd input[name='qty[]']");
			//alert(inputs.length);
			var sub_total_bvr = 0;
			var sub_total_dpr = 0;
			var tot_bvr = 0;
			var tot_dpr = 0;
			for (i = 0; i < dpr.length; i++) {
				sub_total_bvr = parseInt(qty[i].value) * parseInt(bvr[i].value);
				sub_total_dpr = parseInt(qty[i].value) * parseInt(dpr[i].value);
				tot_bvr += sub_total_bvr;
				tot_dpr += sub_total_dpr;
				//console.log(sub_total_bvr);
				//console.log(sub_total_dpr);
				$(All.get_active_tab() + " input[name='total_bvr["+i+"]']").val(tot_bvr);
				$(All.get_active_tab() + " input[name='total_dpr["+i+"]']").val(tot_dpr);
			}
			
			
			$(All.get_active_tab() + " #totBVR").val(tot_bvr);
			$(All.get_active_tab() + " #totDPR").val(tot_dpr);
			var total_pay  = parseInt($(All.get_active_tab() + " #total_pay").val());
			if(tot_dpr > total_pay) {
				alert("Total Nilai Produk lebih dari total yang sudah dibayarkan...");
				$(All.get_active_tab() + " #btnUpdPrd").attr("disabled", "disabled");
			}
		}		
		
    }
	
	function delProduct(param) {
		 $(All.get_active_tab() + " tr#" +param).remove();
		 hitungTotalPrd();
	}
</script>
<div class="mainForm">
  <form class="form-horizontal" id="formKW" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" >
    <fieldset>      
      <div class="control-group">       
        
        <label class="control-label" for="typeahead">Transaction Date</label>
            <div class="controls">
               <input type="text" class="dtpicker typeahead" id="kw_from" name="kw_from" >&nbsp;to&nbsp;
			   <input type="text"  class="dtpicker typeahead" id="kw_to" name="kw_to" >
			  </div>
       <label class="control-label" for="typeahead">Print Status</label>
            <div class="controls">
              <select id="kwPrintStt" name="kwPrintStt" >
                <option value="0">Unprinted</option>
                <option value="1">Printed</option>
              </select>    
            </div>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListKW()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(".dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

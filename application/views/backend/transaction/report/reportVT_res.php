<?php
	if($param == 1){
	        echo "<table>
					<tr>
						<td>
							<form method='post' action='".site_url('trans/report/VT/act/2')."' target='_blank'>
							    <input type='hidden' name='date_from' id='date_from' value='$date_from'/>
							    <input type='hidden' name='date_to' id='date_to' value='$date_to'/>
							    <input type='submit' id='printTxt' name='printTxt' value='Print Report' class='btn btn-primary'/> 
							</form>
						</td>
					</tr>
				</table>"; // jika param = 1 maka jangan export ke excel
	    }else{
	    	//echo $exportToXls; // jika param = 2 maka export ke excel
	    	header("Content-type: application/vnd.ms-excel");
		    header("Content-Disposition: attachment; filename=ReportInstallmentMDR.xls" );
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");
	    }

    if(empty($detReportVT)){
        echo "no Data";
    }else{
?>


<div class="overflow-auto">
 <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable " 
 	<?php if($param == 2){ echo "border='1'"; } ?> >
 	
 	<?php
		$totInsHead = 14;
    	echo "<tr style='height: 50px;text-align: center;font-size: 16px;text-shadow: none;'>
		        <td colspan='$totInsHead'>Report Consolidation Veritrans VS K-Link</td>
		        <td colspan='6'>&nbsp;</td>
		     </tr>
    
		     <tr style='height: 30px;text-align: center;text-shadow: none;' >
		        <th>No</th>
		        <th>Trx. Date</th>
		        <th>Trx No.</th>
		        <th>Member ID</th>
		        <th>Member Name</th>
		        <th>DP</th>
		        <th>Register No</th>
		        <th>CN No</th>
		        <th>Receipt No</th>
		        <th>IP NO</th>
		        <th>Bonus Month</th>
		        <th>VT-ID</th>
		        <th>VT-Amount</th>
		        <th>VT-Paid Date</th>";
			
//datetrans,orderno,id_memb,fullnm,REGISTERno,CNno,KWno,IPno,total_paybonusmonth,token,Order_ID,sett_Amount,sett_Req_Date,sett_Paid_Date
      	$no 			= 	1;
        foreach($detReportVT as $list){
			$dtTrans = date('d/m/Y',strtotime($list->datetrans));
        	
        	if($param == 1){
	        	$dp = number_format($list->total_pay,0,".",".");
	        	$dpVT = number_format($list->sett_Amount,0,".",".");
        	}else{
        		$dp = $list->total_pay;
	        	$dpVT = $list->sett_Amount;
        	}
        	
        	if($list->sett_Paid_Date == "" ){
        		$dtVTTrans = "";
        	}else{
        		$dtVTTrans = date('d/m/Y',strtotime($list->sett_Paid_Date));
        	}
			
			echo "<tr style='height: 30px;text-align: center;text-shadow: none;'>
		        	<td>".$no."</td>
			        <td>".$dtTrans."</td>
			        <td>".$list->orderno."</td>
			        <td>".$list->id_memb."</td>
			        <td>".$list->fullnm."</td>
			        <td>".$dp."</td>
			        <td>".$list->REGISTERno."</td>
			        <td>".$list->CNno."</td>
			        <td>".$list->KWno."</td>
			        <td>".$list->IPno."</td>
			        <td>".$list->bonusmonth."</td>
			        <td>".$list->token."</td>
			        <td>".$dpVT."</td>
			        <td>".$dtVTTrans."</td>
			      </tr>";
	        $no++;
        }
    ?>  
    </tr>
</table>
</div>
<?php
    }
?>
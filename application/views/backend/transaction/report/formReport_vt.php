<div class="mainForm">
    <div id="frmPend">
        <form class="form-horizontal" id="frmReportVT" name="frmReport" method="post">
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Transaction Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="date_to" name="date_to" />
                    </div>
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="be_trans.getListVTrpt()"/>
                    </div>
                </div>
            </fieldset>
        </form>
        <div id="listReportVT"></div>
    </div>
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'dd/mm/yy',
		});
	});	
</script>
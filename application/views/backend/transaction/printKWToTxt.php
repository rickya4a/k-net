<?php
header("Content-Type: plain/text");
//header("Content-type: application/vnd.ms-word"); 
header("Content-Disposition: Attachment; filename=KWReport.txt ");
header("Pragma: no-cache");

date_default_timezone_set("Asia/Jakarta");
$tglPengambilan=date("d-m-Y");
$waktu = date("h:i:s A");



if($printKW == null)
{
    echo "no data";
}
else
{
        	
        $datesFrom = date('d F Y',strtotime($this->input->post('kw_from')));
        $datesTo = date('d F Y',strtotime($this->input->post('kw_to')));
        
            echo "\n\n\n";
            echo "  PT. K-LINK          MEMBER RECEIPT                   Print Date : $tglPengambilan\n";
            echo "                 DATE FROM : $datesFrom          Time       : $waktu\n";
            echo "                 DATE TO   : $datesTo                      \n\n\n";
       
        garisStripKW();
        echo "\n";
        titleHeaderDataKW();
        echo "\n";   
    
	    $page = 0;
	    $rec = 1;
	    $f = 1;
	    $totAmount = 0;
	    $y = 1;
	    foreach($printKW as $dta2)
	    {
	        if($rec == 42)
	        {
	            $page++;
	            echo "\n";
	            echo "\n";
	            echo "Page ".$page."\n\n";
	            echo "\f";
	            titleHeaderDataKW();
	           	echo "\n";
	            $rec = 1;
	        }
	        $ipno = substr($dta2->IPno,0,13);
	        //$etdt = date('d-m-Y',strtotime($dta2->etdt));
	        
	        //$i++;
	         echo "$y";
	         tmbh_spaceDetailPersonalKW(2,$y);
	         //tmbh_spaceDetailProduct(1, $dta2->level);
	         echo "$dta2->KWno";
	         tmbh_spaceDetailPersonalKW(3,$dta2->KWno);
	    	 
	         echo "$ipno";
	         tmbh_spaceDetailPersonalKW(4, $ipno);
	         
	         echo "$dta2->CNno";
	         tmbh_spaceDetailPersonalKW(5,$dta2->CNno);
	         
	         echo substr($dta2->id_memb,0,15);
	         tmbh_spaceDetailPersonalKW(6,substr($dta2->id_memb,0,15));
	         
	         echo "$dta2->idstk";
	         tmbh_spaceDetailPersonalKW(7,$dta2->idstk);
	         
	         tmbh_spaceDetailPersonalKW(7,number_format($dta2->total_pay,0,".","."));
	         echo number_format($dta2->total_pay,0,".",".");
	         
	         echo "\n";
	          
	         $totAmount += $dta2->total_pay; 
	          $y++;
	          $rec++;
	         $f++;
	      }   
    
	    garisStripKW();
	    echo "\n";
	    $usrname = $this->session->userdata('ecom_user');
	    $space = '';
		for($v = 1;$v <= 30;$v++)
		{
		  $space .= " ";
		}
		echo $space;
		echo "T O T A L       ";
	    TotQty(34, number_format($totAmount,0,".","."));
	    echo "\n"; 
		garisStripKW();
	    echo "\n\n\n\n\n\n\n";
	    tmbh_spaceHeaderIP(58);
	    garisStrip2();
	    echo "\n";
	    tmbh_spaceHeaderIP(58);
	    echo "$usrname \n";
	    tmbh_spaceHeaderIP(58);
	    echo "Authorized Signature";  
   
 }  
?>
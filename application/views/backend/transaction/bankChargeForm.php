<?php	 
         $idform = "formBankCharge";
         $form = array("id" => $idform);
         echo opening_form($form);
      	 echo inputHidden("id");
		 //Bank Code
         $onchange = "onchange=All.checkDoubleInput('trans/bank/get/','bankCode',this.value)";
         $bankCode = array(
		 	"labelname" => "Bank Code",
		 	"fieldname" => "bankCode",
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($bankCode);
		 
		 //Bank DESC
         $onchange = "onchange=All.checkDoubleInput('trans/bank/get/','bankDesc',this.value)";
         $bankDesc = array(
		 	"labelname" => "Bank Description",
		 	"fieldname" => "bankDesc",
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($bankDesc);
		 
		 //Bank Display Name
         $onchange = "onchange=All.checkDoubleInput('trans/bank/get/','bankDisplayNm',this.value)";
         $bankDisplayNm = array(
		 	"labelname" => "Bank Display Name",
		 	"fieldname" => "bankDisplayNm",		 	
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($bankDisplayNm);
		 
		 //Bank charge connectivity
         $onkeyup = "onchange=All.numericOnly(this.value,'charge_admin')";
         $charge_connectivity = array(
		 	"labelname" => "Connectivity Charge",
		 	"fieldname" => "charge_connectivity",
		 	"addClass" => "span3",
		 	"placeholder" => "numeric only",
		 	"event" => $onkeyup
 		 );
         echo inputText($charge_connectivity);
         
		 //Bank charge connectivity
         $onkeyup = "onchange=All.numericOnly(this.value,'status')";
         $charge_connectivity = array(
		 	"labelname" => "Admin Charge",
		 	"fieldname" => "charge_admin",
		 	"addClass" => "span3",
		 	"placeholder" => "numeric only",
		 	"event" => $onkeyup
 		 );
         echo inputText($charge_connectivity);
	 		 
		 //Display Status
		 echo selectFlagActive("Display Status", "status");
		 //Backend Status
		 echo selectFlagActive("Backend Status", "status_be");
		 
		 //$input = "All.inputFormData('product/save', this.form.id)";
		 $formListURL = "'trans/bank/charge/list'";
		 $input = "All.ajaxFormPost('$idform','trans/bank/charge/save')";
		 $update = "All.updateFormData('trans/bank/charge/update','$idform',$formListURL)";
		 $view = "All.ajaxFormPost('$idform',$formListURL)";
         echo button_set($input, $update, $view);
		 echo closing_form();
?>	 
      
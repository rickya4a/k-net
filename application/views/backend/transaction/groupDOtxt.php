<?php
	header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=$gdono.txt");
	header("Pragma: no-cache");
    
    date_default_timezone_set("Asia/Jakarta");
    
    $a=0;
    foreach($hdrDO as $head){
        $sentTo = $head->shipto;
        $desc_addr = $head->desc_addr;
        $addr1 = $head->addr1;
        $addr2 = $head->addr2;
        $addr3 = $head->addr3;
        $telp = $head->telp;
        $gdo = $head->trcd;
        $shipBy = $head->shipby;
        $wh = $head->whcd;
        $createdt = date("m/d/Y", strtotime($head->createdt));
    }
        $pjg_shipby = strlen($shipBy);
        $pjg_shipto = strlen($sentTo);
        $pjg_addr1 = strlen($addr1);
        $pjg_addr2 = strlen($addr2);
        $pjg_addr3 = strlen($addr3);
        $pjg_fullnm = strlen($desc_addr);
        
        $AD1 = substr($addr1, 0, 37);
        if(strlen($AD1) == 37)
        {
    	  $AD1 .= " ";
    	}
        $AD2 = substr($addr2, 0, 37);
        if(strlen($AD2) == 37)
        {
    	  $AD2 .= " ";
    	}
        $AD3 = substr($addr3, 0, 37);
        if(strlen($AD3) == 37)
        {
    	  $AD3 .= " ";
    	}
    
	    
            echo "\n";
    		echo "                        GROUPING DELIVERY ORDER SUMMARY\n\n\n";
            echo "To       : $sentTo";
        	tmbh_spaceHeader($pjg_shipto);					   
            echo "GDO No    : $gdo\n";
        	echo "Receiver : $desc_addr";
        	tmbh_spaceHeader($pjg_fullnm); 						   
        	echo "Date      : $createdt\n";
        	echo "           $AD1";
        	tmbh_spaceHeader($pjg_addr1);
        	echo "Branch    : PT K-LINK NUSANTARA\n";
        	echo "           $AD2";
        	tmbh_spaceHeader($pjg_addr2);
        	echo "Warehouse : GUDANG SENTUL\n";
        	echo "           $AD3";
        	tmbh_spaceHeader($pjg_addr3);
        	echo "Ship by   : $shipBy\n";
        	echo "Tel      : $telp\n";  
            
            garisStripDO(12);
            echo "\n";
            title_headerDO();
            echo "\n";
       
        
        $no = 1;
        $jum = $jums;
        $totQty = 0;
        $totShip = 0;
        foreach($detDO as $row){
            echo "$no";
            tmbh_spaceDetailDO(2,$no,5);
            echo "".$row->prdcd."";
            tmbh_spaceDetailDO(3,$row->prdcd,11);
            if(strlen($row->prdnm) > 42)
             {
                $prdnms = substr($row->prdnm, 0, 42);
             }
             else
             {
                $prdnms = $row->prdnm;
             }
            echo "".$prdnms."";
            tmbh_spaceDetailDO(4,$prdnms,40);
            tmbh_spaceDetailDO(5,number_format($row->qty,0,".",","),7);
            echo "".number_format($row->qty,0,".",",")."";
            tmbh_spaceDetailDO(5,number_format($row->qty,0,".",","),7);
            echo "".number_format($row->ship,0,".",",")."";
            tmbh_spaceDetailDO(6,number_format($row->ship,0,".",","),6);
            tmbh_spaceDetailDO(7,number_format($row->bo,0,".",","),3);
            echo "".number_format($row->bo,0,".",",")."";
            $totQty += $row->qty;
            $totShip += $row->ship;
            echo "\n";
            $no++;
            
            if($no == 31 or $no == 61 or $no == 91 or $no == 121 or $no == 151 or $no == 181 or $no == 211 or $no == 241)
        	 {
                if($no < $jum)
        	    {
        		   garisStripDO(12);
                   echo "\n";
                   echo "                                   Page : $no               ";
                   echo "\n";
        		   garisStripDO(12);
                   $no++;	
        		}
        	
                	  for($i = 1;$i<=26;$i++)
                    	{
                    	  echo "\n";
                    	}
                    	
                	    echo "\n";
                		echo "                        GROUPING DELIVERY ORDER SUMMARY\n\n\n";
                        echo "To       : $sentTo";
                    	tmbh_spaceHeader($pjg_shipto);					   
                        echo "GDO No    : $gdo\n";
                    	echo "Receiver : $desc_addr";
                    	tmbh_spaceHeader($pjg_fullnm); 						   
                    	echo "Date      : $createdt\n";
                    	echo "           $AD1";
                    	tmbh_spaceHeader($pjg_addr1);
                    	echo "Branch    : PT K-LINK NUSANTARA\n";
                    	echo "           $AD2";
                    	tmbh_spaceHeader($pjg_addr2);
                    	echo "Warehouse : GUDANG SENTUL\n";
                    	echo "           $AD3";
                    	tmbh_spaceHeader($pjg_addr3);
                    	echo "Ship by   : $shipBy\n";
                    	echo "Tel      : $telp\n";  
                        
                        garisStripDO(12);
                        echo "\n";
                        title_headerDO();
                        echo "\n";
        	  }
            
        }
        echo "\n";
        garisStripDO(12);
        echo "\n";
        tmbh_spaceHeader(20);
        echo "T O T A L      ";
        
        TotQty(30, number_format($totQty,0,".",","));
        TotQty(7, number_format($totShip,0,".",","));
        TotQty(8, 0);
        echo "\n";
        garisStripDO(12);
        echo "\n";
        
        echo "Note : Branch/Stockist must examine the products at the time of delivery.";
        echo "\n\n\n\n\n\n";
    	echo "-------------------------  ";
    	echo "-----------------------  ";
    	echo "----------------------------\n";
    	echo "Transportation Authorized  Authorized Signature &   Authorized Signature &\n";
    	echo "Signature & Chop           Chop by Stockist         Chop\n";	   	
        echo "                                                    K-LINK INTERNATIONAL SDN.BHD";
        echo "\n\n\n";
        echo "Name     :                 Name  :\n";
        echo "I/C      :                 I/C   :\n";
        echo "Lorry    :                 Date  :\n";
        echo "Tel      :                        \n\n";
        echo "White:Return to HQ *Blue:Stockist/Mobile *Pink:Warehouse *Yellow:Transporter";
        
        	 
	
 ?>
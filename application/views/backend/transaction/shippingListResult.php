<?php
 if($result == null) {
 	echo "<div class='alert alert-error'>$result[message]</div>";
 } else {
 	   //echo "<form id=\"savePostTrx\" method=\"POST\" target=\"_BLANK\" action=\"".base_url('trans/posting/save')."\">";
 	   //echo "<form id=\"savePostTrx\">";
 	   //echo "<input class='btn btn-small btn-primary' type=button value=\"Posting Data\" onclick=\"All.postListData('listTrxToPost','trans/posting/save')\" />";
 	   echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=\"9\" bgcolor=\"#lightgrey\">List Transaction</th></tr>";
       echo "<tr bgcolor=\"#f4f4f4\">";
	   //echo "<th><input type=\"checkbox\" onclick=\"All.checkUncheckAll(this)\" name=\"checkall\" /></th>";
       echo "<th width=\"5%\">No</th>";
       echo "<th>Order No</th>";
	   echo "<th>Trx Date</th>";
	   echo "<th>Shipper</th>";
       echo "<th>Receiver Name</th>";
	   echo "<th>Free Ship</th>";
	   echo "<th>Cost Ship</th>";
	   echo "<th>Weight</th>";
	   echo "<th>Conote</th>";
       echo "</tr></thead>";
       echo "<tbody>";
       $i = 1;
       foreach($result as $list) {
                echo "<tr id=\"$i\">";
		        //echo "<td><div align=\"center\"><input type=\"checkbox\" id=\"pil$i\" name=\"orderno[]\" value=\"$list->orderno\" /></div></td>";
                echo "<td><div align=\"right\">$i</div></td>";
                echo "<td><div align=\"left\"><a href=\"#\" onclick=\"All.ajaxShowDetailonNextForm('trans/ship/invoice/id/$list->orderno')\">$list->orderno</a></div></td>";
				echo "<td><div align=\"center\">$list->datetrans</div></td>";
				echo "<td><div align=\"left\">$list->shipper_name - $list->service_type_name</div></td>";
                echo "<td><div align=\"left\">$list->receiver_name</div></td>";
				echo "<td><div align=\"center\">$list->free_shipping</div></td>";
                echo "<td><div align=\"right\">".number_format($list->payShip, 0, ",", ".")."</div></td>";
				echo "<td><div align=\"right\">".number_format($list->total_weight, 2, ",", ".")."</div></td>";
				echo "<td><div align=\"left\">$list->conote_new</div></td>";
               
                echo "</tr>";
              $i++; 
        }
	$act = "onclick=All.postListData('formPostTrx','trans/posting/save')";
    echo "</tbody></tr>";
	//echo "<tr><td colspan=6>";
	//echo "<input type=\"button\" class=\"btn btn-small btn-primary\" value=\"Posting Data\" $act />";
	//echo "</td></tr>";
    echo "</table>";
	
    //echo "<input class=\"btn btn-small btn-primary\" type=\"submit\" value=\"Posting Data\"  />";
    
    //echo "</form>"; 
 }

?>
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>

<?php
	echo "<form>";
	echo "<table class='table table-striped table-bordered' width='100%'>";
	echo "<thead><tr>";
	echo "<th colspan=8>DETAIL PRODUCT PICK UP : $pickup_no</th>";
	echo "</tr>";
	echo "<tr>";
	echo "<th width=5%>No</th>";
	echo "<th width=15%>ID</th>";
	echo "<th>Product Name</th>";
	echo "<th width=5%>Qty</th>";
	echo "<th width=10%>DP</th>";
	echo "<th width=8%>BV</th>";
	echo "<th width=13%>Tot DP</th>";
	echo "<th width=10%>Tot BV</th>";
	echo "</tr></thead><tbody>";
	$i = 1;
	$tot_dpr = 0;
	$tot_bvr = 0;
	$tot_qty = 0;
	foreach($result as $data) {
		$dpr = $data->qty * $data->dpr;
		$bvr = $data->qty * $data->bvr;
		echo "<tr>";
		echo "<td align=right>$i</td>";
		echo "<td align=center>$data->prdcd</td>";
		echo "<td align=left>$data->prdnm</td>";
		echo "<td align=right>".number_format($data->qty,0,".",",")."</td>";
		echo "<td align=right>".number_format($data->dpr,0,".",",")."</td>";
		echo "<td align=right>".number_format($data->bvr,0,".",",")."</td>";
		echo "<td align=right>".number_format($dpr,0,".",",")."</td>";
		echo "<td align=right>".number_format($bvr,0,".",",")."</td>";
		echo "</tr>";
		$tot_dpr = $tot_dpr + $dpr;
		$tot_bvr = $tot_bvr + $bvr;
		$tot_qty = $tot_qty + $data->qty;
		$i++;
	}
	echo "<tr><td colspan=3>&nbsp;</td>";
	echo "<td align=right>".number_format($tot_qty,0,".",",")."</td>";
	echo "<td>&nbsp;</td>";
	echo "<td>&nbsp;</td>";
	echo "<td align=right>".number_format($tot_dpr,0,".",",")."</td>";
	echo "<td align=right>".number_format($tot_bvr,0,".",",")."</td>";
	echo "</tr>";
	echo "<tr><td colspan=2><input class=\"btn btn-mini btn-warning span20\" type=\"button\" onclick=\"be_trans.back_to_form()\" value=\"<< Back\"></td><td colspan=6>&nbsp;</td></tr>";
	echo "</tbody></table><br />";
	
	echo "</form>";
?>
<style>
	.xlText {
	    mso-number-format: "\@";
	}
</style>


<?php
 if($result == null) {
 	echo "<div class='alert alert-error'>$result[message]</div>";
 } else {
 	   //echo "<form id=\"savePostTrx\" method=\"POST\" target=\"_BLANK\" action=\"".base_url('trans/posting/save')."\">";
 	   //echo "<form id=\"savePostTrx\">";
 	   //echo "<input class='btn btn-small btn-primary' type=button value=\"Posting Data\" onclick=\"All.postListData('listTrxToPost','trans/posting/save')\" />";
 	   echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=\"11\" bgcolor=\"#lightgrey\">List Transaction</th></tr>";
       echo "<tr bgcolor=\"#f4f4f4\">";
	   //echo "<th><input type=\"checkbox\" onclick=\"All.checkUncheckAll(this)\" name=\"checkall\" /></th>";
       echo "<th>No</th>";
       echo "<th>Order No</th>";
	   echo "<th>Trx Date</th>";
	   echo "<th>ID Member</th>";
       echo "<th>Member Name</th>";
	   echo "<th>Free Ship</th>";
	   echo "<th>Cost Ship</th>";
	   echo "<th>Weight</th>";
	   echo "<th>Shipper</th>";
	   echo "<th>Conote</th>";
	   echo "<th>Service Type</th>";
	   echo "<th>Receiver Name</th>";
       echo "</tr></thead>";
       echo "<tbody>";
       $i = 1;
       foreach($result as $list) {
                echo "<tr id=\"$i\">";
		        //echo "<td><div align=\"center\"><input type=\"checkbox\" id=\"pil$i\" name=\"orderno[]\" value=\"$list->orderno\" /></div></td>";
                echo "<td><div align=\"right\">$i</div></td>";
                echo "<td><div align=\"left\">$list->orderno</div></td>";
				echo "<td><div align=\"center\">$list->datetrans</div></td>";
				echo "<td><div align=\"left\">$list->id_memb</div></td>";
                echo "<td><div align=\"left\">$list->nmmember</div></td>";
				echo "<td><div align=\"right\">$list->free_shipping</div></td>";
                //echo "<td><div align=\"right\">".number_format($list->payShip, 0, ",", ".")."</div></td>";
				echo "<td><div align=\"right\">$list->payShip</div></td>";
				echo "<td><div align=\"right\">$list->total_weight</div></td>";
				echo "<td><div align=\"left\">$list->shipper_name</div></td>";
				//echo "<td><div align=\"right\" class=\"xlText\">$list->conote_new</div></td>";
				echo "<td align='left' class='xlText'>$list->conote_new</td>";
                echo "<td><div align=\"left\">$list->service_type_name</div></td>";
				echo "<td><div align=\"left\">$list->receiver_name</div></td>";
                echo "</tr>";
              $i++; 
        }
	$act = "onclick=All.postListData('formPostTrx','trans/posting/save')";
    echo "</tbody></tr>";
	//echo "<tr><td colspan=6>";
	//echo "<input type=\"button\" class=\"btn btn-small btn-primary\" value=\"Posting Data\" $act />";
	//echo "</td></tr>";
    echo "</table>";
	
    //echo "<input class=\"btn btn-small btn-primary\" type=\"submit\" value=\"Posting Data\"  />";
    
    //echo "</form>"; 
 }

?>

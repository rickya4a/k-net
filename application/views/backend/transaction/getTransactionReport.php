<div class="mainForm">
  <form class="form-horizontal" id="formTrxReport" method="post" target="_blank" action="<?php echo site_url("trans/report/toExcel");?>">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Trx Type</label>                             
        <div class="controls">
        	<select id="trx_type" name="trx_type" class="span7">
        		<option value="">--Select Here--</option>
        		<option value="IDEC">E-Commerce</option>
        		<option value="E">Member Registration</option>
        		<option value="IDTA">DTC Ticket</option>
        		<option value="PRD">Report Product Summary</option>
				<option value="TOP">Top Up Virtual Account</option>
				<option value="double">Double Transaction Input</option>
				<option value="failRegMampirkak">Mampirkak yang mengisi ID Member dgn NO KTP</option>
				<option value="TDKLENGKAP">Transaksi dengan Data Member/Stokis kosong</option>
				<option value="vchbel">Transaksi dengan Voucher Belanja</option>
        	</select>
        </div>
      	<label class="control-label" for="typeahead">Search By</label>                             
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Select Here--</option>
        		<option value="id_memb">ID Member</option>
        		<option value="idstk">ID Stockist</option>
        		<option value="log_usrlogin">User Login</option>
				<option value="token">Token No / Temp Trx No</option>
				<option value="orderno">Order No / Trx No</option>
				<option value="CNno">CNE No</option>
				<option value="KWNo">KWE No</option>
				<option value="conoteJNE">No Resi Konot</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead">Parameter Value</label>                             
        <div class="controls">
        	<input type="text" id="paramValue" name="paramValue" class="span4" />
        </div>
        <label class="control-label" for="typeahead">Sent To</label>                             
        <div class="controls">
        	<select id="sentTo" name="sentTo">
        		<option value="">--Select Here--</option>
        		<option value="1">Stockist</option>
        		<option value="2">Address</option>
        	</select>
        </div>
        		      
        <?php
          echo datepickerFromTo("Transaction Date", "trx_from", "trx_to");
		  //echo bonusPeriod();
        ?>
    	<div class="control-group">
	        <label class="control-label" for="typeahead">Bonus Period</label>
    		<div class="controls">
		        <select id="bnsmonth" name="bnsmonth">
		    		<option value="">--All--</option>
		    		<option value="01">January</option>
		    		<option value="02">February</option>
		    		<option value="03">March</option>
					<option value="04">April</option>
					<option value="05">May</option>
					<option value="06">June</option>
					<option value="07">July</option>
					<option value="08">August</option>
					<option value="09">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
		    	</select>
    			
    			<input type="text" id="bnsyear" name="bnsyear" placeholder="Year" value="<?php echo date("Y"); ?>"/>
				<!-- label class="control-label" for="typeahead">Payment Type</label -->
	        	&nbsp &nbsp Payment Type &nbsp 
	        	<select id="pay_type" name="pay_type">
	        		<option value="">--Select Here--</option>
	        		<?php
					foreach($payType as $dtas) {
						echo "<option value=\"$dtas->id\">$dtas->bankDisplayNm</option>";
					}
					?>
	        	</select>
	        </div>
	    </div>
	    
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListTrxReport()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="4"  type="submit" class="btn btn-success" value="To Excel" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->

<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());
		
		All.set_datatable();
	});	
</script>

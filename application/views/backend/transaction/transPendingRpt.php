<?php if(empty($results)){
    echo "<div class = alert alert-error>No Record</div> ";
}else{
?>

<table style="width: 100%;" class='table table-striped table-bordered bootstrap-datatable datatable'>
    <thead>
        <tr>
            <th colspan='10' style="height: 45px;vertical-align: middle;font-size: medium;">List Transaction</th>
        </tr>
		<tr style="background-color: #f5f4f4;">
            <th style="width: 6%;">No</th>
			<th style="width: 14%;">Order No</th>
			<th style="width: 14%;">Token No</th>
			<th>ID Member / Name</th>
			<th style="width: 10%;">Stockist</th>
			<th style="width: 10%;">Trx Date</th>
            <th style="width: 6%;">Qty</th>
			<th style="width: 8%;">DP</th>
			<th style="width: 6%;">BV</th>
            <th style="width: 6%;">Status</th>
		</tr>
    </thead>
	<tbody>
        <?php
            $no = 1;
            foreach($results as $list){
                if($list->CNstatus == '1'){
                    $status = "Posted";
                }else{
                    $status = "Unposted";
                }
        ?>
        <tr>
            <td><?php echo $no;?></td>
            <td><a href="#" id="<?php echo $list->orderno;?>" onclick="All.ajaxShowDetailonNextForm('trans/pending/det/<?php echo $list->orderno;?>')"> <?php echo $list->orderno;?></a></td>
            <td><?php echo $list->token;?></td>
            <td><?php echo $list->id_memb." / ".$list->nmmember;?></td>
            <td style="text-align: center;"><?php echo $list->idstk;?></td>
            <td style="text-align: center;"><?php echo date("d/m/Y",strtotime($list->datetrans));?></td>
            <td style="text-align: right;"><?php echo $list->qty;?></td>
            <td style="text-align: right;"><?php echo $list->total_pay;?></td>
            <td style="text-align: right;"><?php echo $list->total_bv;?></td>
            <td style="text-align: right;"><?php echo $status;?></td>
        </tr>
        <?php $no++; }?>
    </tbody>
</table>
<script type="text/javascript">
    All.set_datatable();
</script>
<?php }?>
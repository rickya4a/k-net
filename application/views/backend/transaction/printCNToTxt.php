<?php
	header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=CN_ONLINE.txt  ");
	header("Pragma: no-cache");
    
    date_default_timezone_set("Asia/Jakarta");
	
    $tglPengambilan = date("d F Y");
    $waktu = date("d/m/y h:i:s");
	$a=0;
	foreach($printCN as $dtax) {
		$prefix1 = substr($dtax['header'][0]->CNno,0,3);
        if($prefix1 == 'MME')
        {
            $numberr = 'MM No';
        }
        else
        {
            $numberr = 'CN No';
        }
		if($dtax['header'][0]->nmstkk != null || $dtax['header'][0]->nmstkk != "") {
			//$stk = explode(" - ", $dtax['header'][0]->nmstkk);
			//$namastk = $stk[0]." / ".substr($stk[1],0,35);
			
			$namastk = substr($dtax['header'][0]->nmstkk,0,40);
		} else {
			$namastk = "";
		}
        if($dtax['header'][0]->sentTo == '1'){
            $ship = 'STOCKIST DELIVERY';
        }else{
            $ship = 'ADDRESS DELIVERY';
        }
			echo "\n\n\n";
    		echo "    PT.K-Link                    STOCKIEST RECEIPT CN\n\n\n";
			echo "".$numberr."         : ".$dtax['header'][0]->CNno."\n";
            echo "Orderno       : ".$dtax['header'][0]->orderno."\n";
            echo "ID Member     : ".$dtax['header'][0]->id_memb." / ".substr($dtax['header'][0]->nmmember,0,35)."\n";
            echo "ID Stockist   : ".$namastk."\n";
            
            echo "Bonus Period  : ".$dtax['header'][0]->bonusmonth." \n";
            echo "Print Date    : $waktu\n";
            echo "Ref           :  \n";
            echo "Shipping Type : ".$ship."\n";
            echo "\n\n";
            
			garisStrip();
			echo "\n";
			titleHeaderData();
			echo "\n";
			
			$totqty = 0;
	        $totdp = 0;
			foreach($dtax['detail'] as $dta2) {
					 $total_dp = $dta2->qty * $dta2->dpr;
				     $total_dp2 = number_format($total_dp,0,".",",");	
					 echo "$dta2->prdcd";
					 tmbh_spaceDetailPersonal(1,$dta2->prdcd);
					 
					 echo nama($dta2->prdnm,30);
					 tmbh_spaceDetailPersonal(2,nama($dta2->prdnm,30));
					 
					 tmbh_spaceDetailPersonal(3, $dta2->qty);
					 echo "$dta2->qty";
					 
					 tmbh_spaceDetailPersonal(5, $dta2->dpr);
					 echo number_format($dta2->dpr,0,".",",");
                     tmbh_spaceDetailPersonal(7, $dta2->dpr);
					 
					 tmbh_spaceDetailPersonal(6,$total_dp2);
					 echo $total_dp2;

					 echo "\n";
					 $totdp += $total_dp;
				     $totqty += $dta2->qty;
			}
			
			garisStrip();
			echo "\n";
			//echo "isi : $totdp";
			$space = '';
			for($v = 1;$v <= 11;$v++)
			{
			  $space .= " ";
			}
			$totDPx = number_format($totdp,0,".",",");
			$usrname = $this->session->userdata('ecomm_user');
			echo $space;
			echo "T O T A L       ";
			TotQty(20, number_format($totqty,0,".",","));
			TotQty(27, $totDPx);
			echo "\n"; 
			garisStrip();
			echo "\n\n\n";
			garisStrip3();
			echo "\n Type Payment";
			$batas_header = 25;
			$batas_biaya_tambahan = 20;
			tmbh_spaceHeaderProduct($batas_header);
			echo "Amount\n";
			//tmbh_spaceHeaderProduct(15);
			//echo "Bank\n";
			garisStrip3();
			
			$paytipe = "\n ".$paytipe = $dtax['payment'][0]->paytype;
			echo $paytipe;
			echo addSpacePersonal($batas_header, $paytipe);		 
			echo addSpacePersonal($batas_biaya_tambahan, $totDPx);
		    echo $totDPx;
			
			$adm = "\n "."Admin Cost";
			$adm_cost = number_format($dtax['header'][0]->payAdm,0,".",",");	
			echo $adm;
			echo addSpacePersonal($batas_header, $adm);		 
			echo addSpacePersonal($batas_biaya_tambahan, $adm_cost);
			echo $adm_cost;
			
			$ship = "\n "."Shipping Cost";
			$ship_cost = number_format($dtax['header'][0]->payShip,0,".",",");	
			echo $ship;
			echo addSpacePersonal($batas_header, $ship);
			echo addSpacePersonal($batas_biaya_tambahan, $ship_cost);
			echo $ship_cost."\n";
			echo addLine(64);
			
			$total_pay_all = $totdp + $dtax['header'][0]->payAdm + $dtax['header'][0]->payShip;
			$footer_total = "\n "."TOTAL";
			$footer_tot_pay = number_format($total_pay_all,0,".",",");	
			echo $footer_total;
			echo addSpacePersonal($batas_header, $footer_total);
			echo addSpacePersonal($batas_biaya_tambahan, $footer_total);
			echo $footer_tot_pay."\n";
			
			echo "\n\n\n\n\n";
			garisStrip2();
			tmbh_spaceHeaderProduct(35);
			garisStrip2();
			echo "\n";
			echo "Recipient's Chop & Sign";
			tmbh_spaceHeaderProduct(32);
			echo "".$usrname." \n";
			tmbh_spaceHeaderProduct(55);
			echo "K-LINK INDONESIA";
			//$xx++;
            if($a == 0)
            {
                //echo "\n";
                echo "\f";
                
            }
            else
            {
               $a++;
            }
		
	}
?>
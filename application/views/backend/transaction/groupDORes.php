<div class="overflow-auto">
    <form method="post" action="<?php echo site_url('wh/do/txt');?>" target="_blank">
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <?php
                foreach($hdrDO as $head){
                    $noDO = $head->trcd;
                    $sentTO = $head->shipto;
                    $addr1 = $head->addr1;
                    $addr2 = $head->addr2;
                    $telp = $head->telp;
                    $shipBy = $head->shipby;
                    $wh = $head->whcd;
                    $desc = $head->desc_addr;
                }
            ?>
            <tr>
                <td>&nbsp;No DO</td>
                <td>&nbsp;<?php echo $noDO;?></td>
            </tr>
            <tr>
                <td>&nbsp;Sent To</td>
                <td>&nbsp;<?php echo $sentTO;?></td>
            </tr>
            <tr>
                <td>&nbsp;Alamat</td>
                <td>&nbsp;<?php echo $addr1.", ".$addr2;?></td>
            </tr>
            <tr>
                <td>&nbsp;No telp</td>
                <td>&nbsp;<?php echo $telp;?></td>
            </tr>
            <tr>
                <td>&nbsp;Ship By </td>
                <td>&nbsp;<?php echo $shipBy;?></td>
            </tr>
            <tr>
                <td>&nbsp;Warehouse </td>
                <td>&nbsp;<?php //echo $wh." / ".$desc;?><?php echo "Gudang Sentul";?></td>
            </tr>
        </table>
        
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <tr>
                <th>NO</th>
    			<th>PRODUCT</th>
                <th>PRODUCT NAME</th>
                <th>QTY</th>
                <th>Ship</th>
                <th>B/O</th>
            </tr>
           <?php
                $no = 1;
                
                foreach($detDO as $row)
                {
                    echo "
                    <tr>
                        <td>".$no++."</td>
                       <td align=\"right\">$row->prdcd</td>
                       <td>$row->prdnm</td>
                       <td><div align=right>".number_format($row->qty,0,"",",")."</div></td>
                       <td><div align=right>".number_format($row->ship,0,"",",")."</div></td>
                       <td><div align=right>".number_format($row->bo,0,"",",")."</div></td>
                    </tr>";
                }
           ?>
            <tr>
                <td colspan="6">
                    <input type="hidden" name="doNo" id="doNo" value="<?php echo $noDO;?>"/>
                    <input type="submit" value="Print To Text" name="submit" class="btn-success"/>
                    <input type="button" class="btn btn-warning" name="back" value="Back" onclick="All.back_to_form(' .nextForm1',' .mainForm')"  />
                </td>
            </tr>
        </table>
    </form>
</div>
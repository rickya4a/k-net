<?php
	if($result['response'] == "false") {
		setErrorMessage("No record found, try another parameter..");
	} else {
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="9">List Transaction</th>
    </tr>	
    <tr>
        <th width="5%">No.</th>
        <th width="15%">Order No</th>
        <th width="15%">Token</th>
        <th>Member Name</th>
        <th width="10%">Stockist</th>
        <th width="10%">Date</th>
        <th width="8%">DP</th>
        <th width="6%">BV</th>
        <th width="5%">Act</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result['arrayData'] as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs	
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td align="center"><a href = "#" id=" <?php echo $row->orderno;?>" onclick="be_trans.getDetailTrx(this)"><?php echo $row->orderno;?></a></td>
        <td align="center"><?php echo $row->token;?></td>
        <td><?php 
        		if(strlen($row->nmmember)>30){
        			$nmmember = substrwords($row->nmmember, 30);
				}else{
					$nmmember = $row->nmmember;
				} 
        		echo $nmmember;
        	?></td>
        <td align="center"><?php echo $row->idstk;?></td>
        <td align="center"><?php echo $row->datetrans1;?></td>
        <td style="text-align: right;"><?php echo number_format($row->total_pay,0,".",".");?></td>
        <td style="text-align: right;"><?php echo number_format($row->total_bv,0,".",".");?></td>
        
        <td align="center">
        	<a id=" <?php echo $row->orderno;?>" onclick="javascript:All.deleteData('trans/delete/<?php echo $row->orderno;?>','delTrans','trans/delete/list')" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
        </td>
   </tr>
    <?php
        
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<div class="mainForm">
    <form class="form-horizontal" id="frmNullShipping" method="post" action="#" target="_blank">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">Order No</label>
                <div class="controls">
                    <input type="text" class="span4" id="orderno" name="orderno" onchange="cekOrderNo(this)">
                </div>

                <label class="control-label" for="typeahead">Nama Penerima</label>
                <div class="controls">
                    <input type="text" class="span4" id="namapenerima" name="namapenerima">
                </div>

                <label class="control-label" for="typeahead">No Telp/Hp</label>
                <div class="controls">
                    <input type="text" class="span3" id="telp" name="telp">
                </div>

                <label class="control-label" for="typeahead">Email Address</label>
                <div class="controls">
                    <input type="text" class="span5" id="email" name="email">
                </div>

                <label class="control-label" for="typeahead">Alamat</label>
                <div class="controls">
                    <textarea name="alamat" id="alamat" cols="30" rows="5" class="span8"></textarea>
                    <input type="hidden" id="destination_address" name="destination_address" value="" />
                </div>

                <label class="control-label" for="typeahead">Ekspedisi</label>
                <div class="controls">
                    <select name="shipper" id="shipper" class="span5" onchange="javascript:All.listProvinsi(this)">
                        <option value="">--Select Here--</option>
                      <?php
                        //print_r($listCargo);
                        foreach($listCargo as $row){
                            echo "<option value=\"".$row->shipper_id."\">".strtoupper($row->shipper_name)."</option>";
                       }
                     ?>
                   </select>

                </div>

                <div id="provDiv">
                    <label class="control-label" for="typeahead">Provinsi</label>
                    <div class="controls">
                        <select class="span5" name="provinsi" id="provinsi" onchange="javascript:All.tampilkanKota('#kota')">

                        </select>
                        <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
                    </div>
                </div>

                <div id="kotaDiv">
                    <label class="control-label" for="typeahead">Kota / Kabupaten</label>
                    <div class="controls">
<!--                        <select class="span5" name="kota" id="kota" onchange="javascript:All.show_kecamatan(this,'#kecamatan')">-->
                        <select class="span5" name="kota" id="kota" onchange="javascript:All.tampilkanKecamatan(this,'#kecamatan')">

                        </select>
                        <input type="hidden" id="nama_kota" name="nama_kota" value="" />
                    </div>
                </div>

                <div id="KecDiv" style="display: none;">
                    <label class="control-label" for="typeahead">Kecamatan</label>
                    <div class="controls">
                        <select class="span5" name="kecamatan" id="kecamatan" onchange="javascript:All.show_kelurahan(this,'#stockistref')">

                        </select>
                        <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
                    </div>
                </div>

                <div id="stockDiv">
                    <label class="control-label" for="typeahead">Stockist Referensi</label>
                    <div class="controls">
                        <select id="stockistref" name="stockistref" class="span5" onchange="javascript:All.tampilkanPriceCode()">

                        </select>

                        <input type="hidden" id="nama_stockistref" name="nama_stockistref" value="" />
                        <input type="hidden" id="sender_address" name="sender_address" value="" />
                        <input type="hidden" id="jne_branch" name="jne_branch" value="" />
                        <input type="hidden" id="pricecode" name="pricecode" value="" />
                    </div>
                    <div class="controls">
                        <input type="button" name="cekHarga" id="cekHarga" value="Cek Ongkir" class="btn btn-danger btn-xs">
                    </div>

                </div>
                <div>
                    <label class="control-label" for="typeahead">Ongkos Kirim</label>
                    <div class="controls">
                        <input type="text" class="span4" id="ongkir" name="ongkir" disabled>
                    </div>
                </div>



                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" disabled name="save" value="Submit" onclick="All.ajaxFormPost('frmNullShipping','backend/transaction/InsertNullShipping')"/>
                    <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
<!--                    <input tabindex="5"  type="button" class="btn btn-success" value="View List" onclick="be_voucher.getListNullShipping('transaction/getListNullShipping','formVch')" />-->
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->
<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        $('#cekHarga').click(function() {
            if($('#stockistref').val() != '' && $('#orderno').val() != '' && $('#shipper').val() != ''
                && $('#sender_address').val() != '' && $('#destination_address').val() != ''){
                console.log("Ada");

                $.ajax({
                    type : "POST",
                    url: All.get_url("trans/get_ongkir"),
                    dataType : 'json',
                    data : {stk:$('#stockistref').val(), orderno:$('#orderno').val(), shipper:$('#shipper').val(),
                            sender:$('#sender_address').val(), destination:$('#destination_address').val() },
                    success: function(data){
                        $("#ongkir").val(data.ongkir);
                    }
                });
            }
            else{
                alert("Data belum lengkap");
            }
        });
    });

    function cekOrderNo(){

        var orderno = $("#orderno").val();
        console.log(orderno);

        $.ajax({
            url: All.get_url("trans/null_shipping/cekOrderNo/") +orderno,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    All.set_enable_button();
                    if(data.response == "true" && data.arrayData != null) {
                        document.getElementById("btn_input_user").disabled = false;
                        //JIKA DATA ADA, MUNCULKAN DETAIL
                        $.ajax({
                            url : All.get_url("trans/null_shipping/cekTransShipAddrSgo/")+ orderno,
                            type: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                if(data.response == "true" && data.arrayData != null) {

                                    var detail = data.arrayData;

                                    $("#namapenerima").val(detail[0].receiver_name);
                                    $("#telp").val(detail[0].tel_hp1);
                                    $("#email").val(detail[0].email);
                                    $("#alamat").val(detail[0].addr1);
                                    $("#shipper").val(detail[0].cargo_id);

                                    $.ajax({
                                        url : All.get_url("trans/null_shipping/cekTransShipAddr/")+ orderno,
                                        type: 'GET',
                                        dataType: 'json',
                                        success: function (data) {
                                            if(data.response == "true" && data.arrayData != null) {
                                                alert('Data Sudah ada di tabel ecomm_trans_shipaddr');
                                            }
                                            else{
                                                alert('Data belum ada di tabel ecomm_trans_shipaddr');
                                            }
                                        }
                                    });
                                }
                                else{
                                    alert("Data Tidak Ditemukan");
                                }
                            }
                        });

                    }
                    else {
                        alert("Order No yang diinput salah");

                        document.getElementById("btn_input_user").disabled = true;
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    }

</script>

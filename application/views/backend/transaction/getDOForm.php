<div class="mainForm">
<!--<div id="formDOO">-->
  <form class="form-horizontal" id="formDo" >
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Trx Type</label>                             
        <div class="controls">
        	<select id="trx_type" name="trx_type" id="trx_type">
        		<option value="">--Select Here--</option>
        		<option value="stk">Stockist</option>
        		<option value="addr">Address</option>
        	</select>
        </div>
        <span id="destList" style="display: none;">
            <label class="control-label" for="typeahead">Destination</label>
            <div class="controls">
            	<select id="destTo" name="destTo">
            		<option value="">--Select Here--</option>
            		<option value="stockist">Stockist</option>
            		<option value="warehouse">Warehouse</option>
                    <option value="person">Personal</option>
            	</select>
            </div>
        </span>
        <span id="whList" style="display: none;">
            <label class="control-label" for="typeahead">Sent To</label>
            <div class="controls">
            	<select id="sentToWH" name="sentToWH" class="span5">
                    <option value="">--Select Here--</option>
            		<?php 
                        foreach($wh as $list){
                            
                            echo "<option value=\"$list->whcd\">".$list->whcd." - ".$list->description."</option>" ;    
                        }
                        
                    
                    ?>
            	</select>
            </div>
        </span>
        <span id="stkList" style="display: none;">
            <label class="control-label" for="typeahead">Sent To</label>
            <div class="controls" >
            	<select id="sentToStk" name="sentToStk" class="span4">
                    <option value="">--Select Here--</option>
            		<?php 
                        foreach($stk as $list){
                            echo "<option value=\"$list->idstk\">".$list->idstk." - ".$list->fullnm."</option>" ;    
                        }
                        
                    
                    ?>
            	</select>
            </div>
        </span>
        		      
        <?php
          echo datepickerFromTo("Transaction Date", "from", "to");
        ?>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'wh/doList')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div class="result"></div>
  <!--</div>
  <div id="listDO"></div>
  <div id="groupDO"></div>-->
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker("setDate", new Date());;
	    
        $(All.get_active_tab() + " #trx_type").change(function() {
             if( $(All.get_active_tab() + " #trx_type").val() == "addr")
             {
                $(All.get_active_tab() + " #whList").css('display', 'block');
                $(All.get_active_tab() + " #stkList").css('display', 'none');
                $(All.get_active_tab() + " #destList").css('display', 'none');
             }
             else if($(All.get_active_tab() + " #trx_type").val() == "stk" )
             {
                $(All.get_active_tab() + " #destList").css('display', 'block');
                
             }else{
                $(All.get_active_tab() + " #stkList").css('display', 'none');
                $(All.get_active_tab() + " #whList").css('display', 'none');
             }
        });
        
        $(All.get_active_tab() + " #destTo").change(function() {
             if( $(All.get_active_tab() + " #destTo").val() == "stockist")
             {
                $(All.get_active_tab() + " #whList").css('display', 'none');
                $(All.get_active_tab() + " #stkList").css('display', 'block');
               
             }
             else{
                $(All.get_active_tab() + " #stkList").css('display', 'none');
                $(All.get_active_tab() + " #whList").css('display', 'block');
             }
        });
    });
</script>

<?php
    if(empty($rptSgo)){
        echo setErrorMessage();
    }else{
        $filenm = "report".$from."-".$to.".csv";
    
        
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filenm.'');
        
        $output = fopen('php://output', 'r');
        
        fputcsv($output, array('No','Trx ID', 'Anchor', 'Bussiness Scheme', 'Community', 'Member', 'CCY', 'Amount', 
        					   'Trx Date', 'Status', 'Bank Prdcd', 'Prdcd Value', 'Pay Reff', 'Created Date', 'Last Updt', 'Desc', 
        					   'Order ID', 'Flag', 'CN No.', 'KW No.', 'IP No.','Dist. Code', 'Name', 'Bank Code','Bank Desc.', 'Pay Sales',
							   'Pay IPG', 'Pay Ship', 'Pay Total', 'Trx Type', 'Selisih Harga'),';',' ');
							   
        //print_r($rptSgo);
        $jum = 0;
        $num = 1;
        $TotPaySales = 0;
		$TotPayIPG = 0;
		$TotPayShip = 0;
		$TotPayTotal = 0;
        $TotPayCP = 0;
        foreach($rptSgo as $row){
        	$nmmember = str_replace(",","",$row->nmmember);
            $PaySales	= number_format($row->total_pay,0,"","");
			$PayIPG		= number_format($row->payAdm,0,"","");
			$PayShip	= number_format($row->payShip,0,"","");
			$PayTotal	= number_format($row->payamt,0,"","");
            $PayCP      = number_format($row->selisihCustPrice,0,"","");
            
            if($row->tipeLogin == '0'){
                $typess = 'Trx Umroh';
            }elseif($row->tipeLogin == '1' || $row->tipeLogin == Null){
                $typess = 'Trx from Member Price';
            }elseif($row->tipeLogin == '2'){
                $typess = 'Trx from Consument Price';
            }else{
                $typess = 'Trx from Landing Page';
            }
            
            $x = array('No' =>$num,
                        'Transaction_ID' => $row->Transaction_ID,
                        'Anchor' =>$row->Anchor,
                        'Business_Scheme' => $row->Business_Scheme,
                        'Community' => $row->Community,
                        'Member' => $row->Member,
                        'CCY' => $row->CCY,
                        'Amount' => ''.number_format($row->Amount,0,"","").'',
                        //'Amount' => $row->Amount,
                        'Transaction_Date' => date("d/m/y",strtotime($row->Transaction_Date)),
                        'Status' => $row->Status,
                        'Bank_Product' => $row->Bank_Product,
                        'Product_Value' => $row->Product_Value,
                        'Payment_Ref' => $row->Payment_Ref,
                        'Created' => date("d/m/y",strtotime($row->Created)),
                        'Lastupdated' => date("d/m/y", strtotime($row->Lastupdated)),
                        'Description' => $row->Description,
                        'Order_ID' => $row->Order_ID,
                        'flagRecover' => $row->flagRecover,
                        'CNno' => $row->CNno, 
                        'KWno' => $row->KWno, 
                        'IPno' => $row->IPno, 
                        'id_memb' => $row->id_memb,
                        'nmmember' => $nmmember,
                        'bankCode' => $row->bankCode,
                        'bankDesc' => $row->bankDesc,
                        'Pay_Sales' => $PaySales,
                        'Pay_IPG' => $PayIPG,
                        'Pay_Ship' => $PayShip,
                        'Pay_Total' => $PayTotal,
                        'Trx_typess ' => $typess,
                        'Pay_CP' => $PayCP,
                        /* 
                        'total_pay' => ''.number_format($row->total_pay,0,"",",").'',
                        'payAdm' => ''.number_format($row->payAdm,0,"",",").'',
                        'payShip' => ''.number_format($row->payShip,0,"",",").'',
                        'payamt' => ''.number_format($row->payamt,0,"",",").'', */
                        );
            //$y = implode( "\r\n" , $x );
            //print_r($y);
            $jum += $row->Amount;
            $TotPaySales += $row->total_pay;
    		$TotPayIPG += $row->payAdm;
    		$TotPayShip += $row->payShip;
    		$TotPayTotal += $row->payamt;
            $TotPayCP += $row->selisihCustPrice;
            $num++;
            fputcsv($output,$x,';',' ');
            
        }
        fputcsv($output,array('','','','','','','Total',$jum,'','','','','','','','','','','','','','','','','Total',$TotPaySales,$TotPayIPG,$TotPayShip,$TotPayTotal,'Total Selisih',$TotPayCP),';',' ');
        echo "\n";
        fputcsv($output,array('No','Bank Product','Status','Amount'),';',' ');
        
        $no = 1;
        $tot = 0;
        foreach($totalSgo as $list){
            $y = array('No' => $no,
                        'Bank_Product' => $list->Bank_Product,
                        'Status' => $list->Status,
                        'Amount' => ''.number_format($list->Amount,0,"","").'');
            $no++;
            $tot += $list->Amount;
            fputcsv($output,$y,';',' ');
        }
        fputcsv($output,array('','','Total',$tot),';',' ');
        
        fclose($output);
    }
?>
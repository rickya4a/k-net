<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
			  <thead>
			  <tr>
			  <th width='3%'>No</th>
			  <th width='5%'>Date</th>
			  <th width='6%'>Order#</th>
			  <th width='20%'>Sockist</th>
			  <th width='6%'>Voucher#</th>
			  <th width='3%'>Status</th>
			  <th width='5%'>Exp. Date</th>
			  <th width='20%'>Distibutor</th>
			  <th width='10%'>IP#</th>
			  <th width='10%'>CN#</th>
			  </tr>
			  </thead>
			  <tbody>";
		$i = 1;
		foreach($result as $data) {
			$etdt = date("d-m-Y", strtotime($data->etdt));
			$ExpireDate = date("d-m-Y", strtotime($data->ExpireDate));
			echo "<tr>
				  <td align=right>$i</td>";
			/*a.VoucherNo, a.claimstatus, a.IssueDate, a.ExpireDate, a.BonusMonth, a.BonusYear,
   						a.updatedt, a.trcd, a.etdt, a.loccd, a.fullnm, a.createnm, 
   						A.DistributorCode, E.fullnm AS distname
			 * 
			 */
			echo "<td align=left>$etdt</td>
				  <td align=left>$data->trcd</td>
				  <td align=left>$data->loccd - $data->SCNM</td>
				  <td align=left>$data->VoucherNo</td>
				  <td align=left>$data->claimstatus</td>
				  <td align=left>$ExpireDate</td>
				  <td align=left>$data->dfno - $data->fullnm</td>
				  <td align=left>$data->IPNO</td>
				  <td align=left>$data->CNNO</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
	}
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
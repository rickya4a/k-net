<div class="mainForm">
    <form class="form-horizontal" id="formReconcileSGO" method="post"
          action="<?php echo site_url("trans/sgo/reconcile/manual/1")?>" target="_blank">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">Transaction Date</label>
                <div class="controls">
                    <input type="text" class="dtpicker typeahead" id="sgo_trx_from" name="from" >&nbsp;to&nbsp;
                    <input type="text"  class="dtpicker typeahead" id="sgo_trx_to" name="to" >
                </div>
                <label class="control-label" for="typeahead">Status</label>
                <div class="controls">
                    <select id="rptType" name="rptType">
                        <option value="0">--All--</option>
                        <option value='1'>Success</option>
                        <option value='2'>Reconcile</option>
                        <option value='3'>Menunggu Pembayaran</option>
                    </select>
                </div>

                <!--<label class="control-label" for="typeahead">Payment Type</label>
                <div class="controls">
                    <select id="bank" name="bank">
                        <option value="all">All</option>
                        <?php
/*                        foreach ($bankList as $bank) {
                            echo "<option value=\"".$bank->id."\">".$bank->bankDisplayNm."</option>";
                        }
                        */?>
                    </select>
                </div>-->

                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit"
                           onclick="All.ajaxFormPost(this.form.id,'trans/sgo/reconcile/manual/1')" />
                    <!--input tabindex="4" type="submit" id="btn_input_user" class="btn btn-success .submit" name="toXls" value="To Excel"
                        onclick="All.ajaxFormPost(this.form.id,'trans/sgo/reconcile/list/0')" / -->
                    <!--<input type='submit' id='printXls' name='printXls' value='To Excel' class='btn btn-success'/>
                    <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />-->
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->
<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());;
    });
</script>

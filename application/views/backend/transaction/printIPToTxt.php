<?php
date_default_timezone_set("Asia/Jakarta");
$tglPengambilan=date("d M Y");
$waktu = date("h:i:s A");
if($printIP == null)
{
    echo "no data";
}
else
{
    header("Content-Type: plain/text");
    header("Content-Disposition: Attachment; filename=IncomingPaymentAll.txt ");
	header("Pragma: no-cache");
    
    $dateFrom = date('d-m-Y',strtotime($inp['ip_from']));
    $dateTo = date('d-m-Y',strtotime($inp['ip_to']));
    
    $a=0;
    $xx = 1;
    foreach($printIP as $dta2)
    {
        //if ($xx == 2)
        //break;
        echo "\n\n\n";
        echo "PT. K-LINK        DETAIL Of INCOMING PAYMENT        Print Date: $tglPengambilan\n";
        echo "                                                    Time      : $waktu\n\n\n";
        echo "ID Stockist     : $dta2->nmstkk\n";
        echo "Amount          : ".number_format($dta2->amount,0,".",",")." \n";
        echo "Bank            : $dta2->bankaccnm \n";
        echo "FROM            : $dateFrom  TO : $dateTo \n\n";
        //$xx++;
      
        garisStrip();
		echo "\n";
		echo "No";
		tmbh_spaceHeaderProduct(1);
		echo "ID MEMBER";
		tmbh_spaceHeaderProduct(6);
		echo "NAMA MEMBER";
		tmbh_spaceHeaderProduct(6);
		echo "REF NO";
	    tmbh_spaceHeaderProduct(13);
	    echo "DATE";
	    tmbh_spaceHeaderProduct(10);
	    echo "AMOUNT\n";
		//tmbh_spaceHeaderProduct(5);
		    
	    garisStrip();
		echo "\n";
        
        //$totAmount = 0;
        
        echo "1";
		echo addSpacePersonal(3,"1");
		
		echo $dta2->id_memb;
		echo addSpacePersonal(15, $dta2->id_memb);
		
		$nama = nama($dta2->nmmember,15);
		echo $nama;
		echo addSpacePersonal(17, $nama);
		
		echo $dta2->trcd;
		echo addSpacePersonal(19, $dta2->trcd);
		
		$dateIP = date('d-m-Y',strtotime($dta2->dateIP));
		echo $dateIP;
		echo addSpacePersonal(11, $dateIP);
		
		$amount = number_format($dta2->amount,0,".",",");
		echo addSpacePersonal(11, $amount);
	    echo $amount;
		
		echo "\n";
		
		echo "2";
		echo addSpacePersonal(3,"2");
		
		echo $dta2->id_memb;
		echo addSpacePersonal(15, $dta2->id_memb);
		
		$nama = nama($dta2->nmmember,15);
		echo $nama;
		echo addSpacePersonal(17, $nama);
		
		echo $dta2->CNno;
		echo addSpacePersonal(19, $dta2->CNno);
		
		$dateIP = date('d-m-Y',strtotime($dta2->dateIP));
		echo $dateIP;
		echo addSpacePersonal(11, $dateIP);
		
		$amount2 = number_format($dta2->amount,0,".",",");
		$amount2a = "(".$amount2.")";
		echo addSpacePersonal(11, $amount2a);
	    echo $amount2a;
		
		echo "\n";
		
            garisStrip();
            echo "\n";
            $space = '';
        	for($v = 1;$v <= 25;$v++)
        	{
        	  $space .= " ";
        	}
        	echo $space;
        	echo "T O T A L       ";
            TotQty(35, number_format("0",0,".","."));
            echo "\n"; 
        	garisStrip();
            echo "\n\n";
            tmbh_spaceHeaderIP(22);
			
            echo "END OF REPORT - PRINTED BY : $username";
            
            if($a == 0)
            {
                //echo "\n";
                echo "\f";
                
            }
            else
            {
               $a++;
            }
    }
}
 ?>
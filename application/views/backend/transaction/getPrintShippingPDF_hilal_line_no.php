<?php
	
	$pdf=new PDF_MC_Table(); // PDF_MC_Table();//
	
	
	$pdf->Open();
    $pdf->SetFillColor(255,255,255); // background = biru muda
    $pdf->SetTextColor(0,0,0);	 //	font color = black
    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
    $pdf->SetLineWidth(.1);
        
    $titleCol2 = 155;
    $lebarCell = 5;
	$left = 40;
	$border = 0;
	
	 //setting header product column width
    $no_urut = 7;
    $product_id = 25;
    $product_name = 90;
    $qty = 9;
    $dp = 23;
    $bv = 8;
    $kolom_total = $no_urut + $product_id + $product_name + $qty + $dp + $bv;
    $total_dp = 23;
    $total_bv = 15;
	
	$bcWidth = 50;
	$bcHeigth = 4;
	
    $pdf->SetFillColor(255,255,255);
    
	$xpage = 0;
	$y = 20;
	$xpos = $pdf->GetX();
	$ypos = $pdf->GetY();
	$yposadd = 3.5;
	$xminus = 0.5;
	$leftAlign = -3;
	foreach($result as $row) {	
			$xpage = $xpage + 1;
			$leftAlign = - 3;
			//============================RESI/CONOTE JNE DLL============================
			//print_r($result);
			$pdf->SetFont('Arial','',10);
			$pdf->AddPage();

			$pdf->SetMargins(60, 0, 1);
			//$pdf->SetMargins(7, 0, 1); //ori
			$tambahBawah = 0;
			if($xpage > 1){
				$tambahBawah = 2;
			}
			/*
			if($xpage == 6 || $xpage == 7 || $xpage == 8 || $xpage == 13 || $xpage == 14 || $xpage == 16 || $xpage == 17 || $xpage == 21 || $xpage == 23 || 
			   $xpage == 31 || $xpage == 33 || $xpage == 38 || $xpage == 40 || $xpage == 44 || $xpage == 45 || $xpage == 46 || $xpage == 48 ||
			   $xpage == 49 || $xpage == 58 || $xpage == 59 || $xpage == 60 || $xpage == 63 || $xpage == 76 || $xpage == 85 || $xpage == 87 || 
			   $xpage == 94 || $xpage == 95 || $xpage == 97 || $xpage == 102 || $xpage == 104 || $xpage == 106 || $xpage == 107 || $xpage == 111 ||
			   $xpage == 113 || $xpage == 114 || $xpage == 115){
				//$tambahBawah = 5;
				$tambahBawah =  0;
			}
			 * 
			 */
			
			// ==========================Start Coba Hilal dengan menggunakan table==========================
			$currDate=date('d/M/Y');
			//$currDate= date_format($currDate,"d/M/Y");
			$pdf->SetFont('Arial','B',7.5);
			//$pdf->SetWidths(array(90));
			srand(microtime()*1000000); 
			$pdf->SetXY(61 - $xminus, 3);
			//$pdf->SetXY(7, 3); //ori    
			
			//Start untuk Shipper
			// cuma untuk pancingan
			//$pdf->SetFont('Arial','B',1);                 
			$pdf->Code39_old(95, 0,trim(""),0, 2, 2); 
			// end pancingan
			
			$pdf->SetFont('Arial','',7.5);
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$ypos = $ypos + $tambahBawah;
			$pdf->SetXY(61 - $xminus, $ypos + $yposadd);
			$logoKlink = 0;
			if($xpage == 1){
				$logoKlink = $ypos + 3.5;
			}else{
				$logoKlink = $ypos + 1;
			}
			
			$bcKlink = 0;
			if($xpage == 1){
				$bcKlink = $ypos - 1;
				$ordernoY = -1;
			}else{
				$bcKlink = $ypos - 2.5;
				$ordernoY = 0;
			}
			$pdf->MultiCell(15, 8, "", '', 'l', '');
			$pdf->SetXY((73 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(83, 4 + $ordernoY, "                        ".trim($row['orderno'])."\n\n".
							$pdf->Code128(($left + 39) + $leftAlign - 1, $bcKlink + 7,trim($row['orderno']), $bcWidth, $bcHeigth), '', 'L', '');	
			//$pdf->Ln(0);
			//$pdf->SetXY(17, $ypos+6);
			$logoExp = 0;
			if($xpage == 1){
				$logoExp = $ypos + 14;
				$ordernoY = -1;
			}else{
				$logoExp = $ypos + 11;
				$ordernoY = 0;
			}
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos +8 + $yposadd);
			$pdf->MultiCell(15 , 8, $pdf->Image($row['logo_url'],62 + $leftAlign, $logoExp, 13, 3.5), '', 'l', '');
			
			$pdf->SetXY((73 - $xminus) + $leftAlign, $ypos +7.5 + $yposadd );
			
			$bcExp = 0;
			if($xpage == 1){
				$bcExp = $ypos - 1;
				$ordernoY = -1.5;
			}else{
				$bcExp = $ypos - 2.5;
				$ordernoY = 0;
			}
			
			if(trim($row['conoteJNE']) != null and trim($row['conoteJNE']) != ''){
				$conoteexp = $pdf->Code128(($left + 38) + $leftAlign, $bcExp + 14.5, trim($row['conoteJNE']), $bcWidth, $bcHeigth);	
			}else{
				$conoteexp = null;
			}
			$pdf->MultiCell(83 + $leftAlign, 4.5 + $ordernoY, "                        ".trim($row['conoteJNE'])."\n\n".
							$conoteexp, '', 'L', '');	
			
		
			$pdf->SetFont('Arial','B',6.5);
			//$pdf->SetWidths(array(10, 65, 15,0)); //NO, PRD DESC, QTY
			//$pdf->Row(array("No", "Product Desc", "Qty"));
			
			$yposadd = 0;
			if($xpage == 1){
				$yposadd = 2;
			}
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(15, 3.5, "", '', 'C', '');
			$pdf->SetXY((76 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(30, 3.5, strtoupper($row['service_type_name']), '', 'L', '');
			$pdf->SetXY((106 - $xminus) + $leftAlign, $ypos + $yposadd);
			//$pdf->MultiCell(25, 3.5, number_format($row['payShip']), 'RB', 'L', '');
			$pdf->MultiCell(25, 3.5, '-', '', 'L', '');	
			$pdf->SetXY((131 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(25, 3.5, '-', '', 'L', '');	
			$pdf->Ln(0);
			
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 3.7 + $yposadd);
		 	$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95 + $leftAlign, 3.2, "                     ".strtoupper("PT K-Link Nusantara"), '', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 6.9 + $yposadd);
			$pdf->SetFont('Arial','',7.5);
			$pdf->Cell(95 + $leftAlign, 3.2, ucwords(strtolower($row['whcd_addr1'])), '', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 10 + $yposadd);
			$pdf->Cell(95 + $leftAlign, 3.2, ucwords(strtolower($row['whcd_addr2'])), '', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 13 + $yposadd);
			$pdf->Cell(95 + $leftAlign, 3.2, ucwords(strtolower($row['whcd_addr3'])), '', 1, 'L', '', '');
			
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 16 + $yposadd);
			$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95 + $leftAlign, 3.2, "                     ".strtoupper($row['receiver_name'])." / ".strtoupper($row['contactno']), 
						'', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 19 + $yposadd);
			$pdf->SetFont('Arial','',7.5);
			$addr = ucwords(strtolower(str_replace("\n", " ", $row['addr']))).
				    ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords(strtolower($row['kec']))));
			//echo "$addr</br>";
			
			$countAddr = 0;
			$split = 70;
			$yposnew = 0; 
			if(strlen($addr) > $split){
				$addsplit = null;
				$countAddr = round(strlen($addr)/$split);
				if($countAddr < 4) $countAddr = 4;
				//echo "countAddr = $countAddr</br>";
				for ($i=1; $i <= $countAddr; $i++) {
					if($i == 1){
						$start = 0;
						$yposnew = 0;
					}else{
						$yposnew = $yposnew + 3.3;
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<$countAddr) {
						$border = '';
					}elseif($i == $countAddr){
						$border = '';
					}
					$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 19 + $yposnew + $yposadd);
					$pdf->Cell(95 + $leftAlign, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}else{
				//$pdf->Cell(95, 3.2, $addr, $border, 1, 'L', '', '');
				for ($i=1; $i <= 4; $i++) {
					if($i == 1){
						$start = 0;
						$yposnew = 0;
					}else{
						$yposnew = $yposnew + 3.3;
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<4) {
						$border = '';
					}elseif($i == 4){
						$border = '';
					}
					$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 19 + $yposnew + $yposadd);
					$pdf->Cell(95 + $leftAlign, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}
			
		 	//Start untuk Shipper
			//$pdf->Cell(95, 2.2, "x-------------------------------------------------------------------------------------------------------x", '', 1, 'L', '', '');
			$pdf->Cell(95 + $leftAlign, 2.2, "", '', 1, 'L', '', '');
			// ==========================End Coba Hilal dengan menggunakan table==========================
         	$pdf->SetFont('Arial','B',7.5);    
			$pdf->Cell(95 + $leftAlign, 3.2, "                                                                                              $currDate", '', 1, 'L', '', '');
			
			
			//$pdf->SetXY(61, $ypos + $yposnew + 3);
			$pdf->SetFont('Arial','',7.5);
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$yposaddlogo2 = 4.5;
			
			$yposadd = 1;
			
			$leftAlign = $leftAlign;
			
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(15, 8, "", '', 'l', '');
			$pdf->SetXY((73 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(83, 4, "                        ".trim($row['orderno'])."\n\n".
							$pdf->Code128(($left + 38) + $leftAlign, $ypos + 4.5,trim($row['orderno']), $bcWidth, $bcHeigth), '', 'L', '');	
			
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 8 + $yposadd);
			$pdf->MultiCell(15, 8, $pdf->Image($row['logo_url'],62 + $leftAlign,$ypos + 12, 13, 3.5), '', 'l', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos+ 7 + $yposadd);
			
			if(trim($row['conoteJNE']) != null and trim($row['conoteJNE']) != ''){
				$conoteexp = $pdf->Code128(($left + 38) + $leftAlign, $ypos + 12, trim($row['conoteJNE']), $bcWidth, $bcHeigth);	
			}else{
				$conoteexp = null;
			}
			$pdf->SetXY((73 - $xminus) + $leftAlign, $ypos + 7.3 + $yposadd);
			$pdf->MultiCell(83, 4.5, "                        ".trim($row['conoteJNE'])."\n\n".
							$conoteexp, '', 'L', '');	

			$pdf->SetFont('Arial','B',6.5);
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$yposadd = 0;
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(15, 3.3, "", '', 'C', '');
			$pdf->SetXY((76 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(30, 3.3, strtoupper($row['service_type_name']), '', 'L', '');
			$pdf->SetXY((106 - $xminus) + $leftAlign, $ypos + $yposadd);
			//$pdf->MultiCell(25, 2.8, number_format($row['payShip']), 'RB', 'L', '');	
			$pdf->MultiCell(25, 3.3, '-', '', 'L', '');
			$pdf->SetXY((131 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(25, 3.3, '-', '', 'L', '');	
			$pdf->Ln(0);
			
			$pdf->SetFont('Arial','',7.5);
			$pdf->SetWidths(array(95,0));
			
			$yposadd = 0.5;
			
		 	$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 3.0 + $yposadd);
		 	$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95 + $leftAlign, 3.2, "                     ".strtoupper("PT K-Link Nusantara"), '', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 6.4 + $yposadd);
			$pdf->SetFont('Arial','',7.5);
			$pdf->Cell(95 + $leftAlign, 3.2, ucwords(strtolower($row['whcd_addr1'])), '', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 9.5 + $yposadd);
			$pdf->Cell(95 + $leftAlign, 3.2, ucwords(strtolower($row['whcd_addr2'])), '', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 13 + $yposadd);
			$pdf->Cell(95 + $leftAlign, 3.2, ucwords(strtolower($row['whcd_addr3'])), '', 1, 'L', '', '');
			
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 16 + $yposadd);
			$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95 + $leftAlign, 3.2, "                     ".strtoupper($row['receiver_name'])." / ".strtoupper($row['contactno']), 
						'', 1, 'L', '', '');
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 19 + $yposadd);
			$pdf->SetFont('Arial','',7.5);
			$addr = ucwords(strtolower(str_replace("\n", " ", $row['addr']))).
				    ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords(strtolower($row['kec']))));
			//echo "$addr</br>";
			$countAddr = 0;
			$split = 70;
			$yposnew = 0;
			if(strlen($addr) > $split){
				$addsplit = null;
				$countAddr = round(strlen($addr)/$split);
				if($countAddr < 4) $countAddr = 4;
				//echo "countAddr = $countAddr</br>";
				for ($i=1; $i <= $countAddr; $i++) {
					if($i == 1){
						$start = 0;
						$yposnew = 0;
					}else{
						$yposnew = $yposnew + 3.3;
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<$countAddr) {
						$border = '';
					}elseif($i == $countAddr){
						$border = '';
					}
					
					$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 19 + $yposnew + $yposadd);
					$pdf->Cell(95 + $leftAlign, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}else{
				//$pdf->Cell(95, 3.2, $addr, $border, 1, 'L', '', '');
				for ($i=1; $i <= 4; $i++) {
					if($i == 1){
						$start = 0;
						$yposnew = 0;
					}else{
						$yposnew = $yposnew + 3.3;
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<4) {
						$border = '';
					}elseif($i == 4){
						$border = '';
					}
					
					$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + 19 + $yposnew + $yposadd);
					$pdf->Cell(95 + $leftAlign, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}
			//$pdf->Row(array("Delivery By/Service", strtoupper($row['shipper_code'])."/".strtoupper($row['service_type_name'])));
			
			//==============================DETAIL PRODUCT================================================
			
			$x = 1;
		   
		    $total_qty = 0;
		    $total_dpR = 0;
		    $total_bvR = 0;
			$arrPrdcd = null;
			$no = null;
			$arrPrdDesc = null;
			$arrQty = null;
			$arrDP = null;
			$arrBV = null;
			$arrTotBV = null;
		    $arrTtotal_qty = null;
		    $arrTtotal_dpR = null;
		    $arrTtotal_bvR = null;
			
			$pdf->SetFont('Arial','B',6.5);
			$pdf->SetWidths(array(90)); //HEADER FOR PRODUCT DETAIL
			$transDate=date_create($row['datetrans2']);
			$transDate = date_format($transDate,"d/M/Y");
			//$pdf->Row(array("---------INVOICE DETAIL--------- Transaction Date : ".$transDate), FALSE);
			$pdf->Cell(95 + $leftAlign, 3.2, "                                                                                            ".$transDate, '', 1, 'L', '', '');
			
			$pdf->SetFont('Arial','B',6.5);
			//$pdf->SetWidths(array(10, 65, 15,0)); //NO, PRD DESC, QTY
			//$pdf->Row(array("No", "Product Desc", "Qty"));
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$yposadd = 1;
			
			$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(10, 2.5, "", '', 'R', '');
			$pdf->SetXY((71 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(70, 2.5, "", '', 'L', '');
			$pdf->SetXY((138 - $xminus) + $leftAlign, $ypos + $yposadd);
			$pdf->MultiCell(18, 2.5, "", '', 'R', '');	
			$pdf->Ln(0);
			
			$pdf->SetFont('Arial','',6.5);
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$yposnew = 0;	
		    foreach($row['detPrd'] as $dta2)
		    {
		        $dpR = $dta2->dpr * $dta2->qty;
		        $bvR = $dta2->bvr * $dta2->qty;

		        $total_qty += $dta2->qty;
		        $total_dpR += $dpR;
		        $total_bvR += $bvR;
				
				$no = $no.$x."\n";
			   	$arrPrdcd = $arrPrdcd.$dta2->prdcd."\n";
				$arrPrdDesc = $arrPrdDesc.$dta2->prdnm."\n";
				$arrQty = $arrQty.number_format($dta2->qty,0,".",",")."\n";
				$arrDP = $arrDP.number_format($dta2->qty,0,".",",")."\n";
				$arrBV = $arrBV.number_format($dta2->bvr,0,".",",")."\n";
				$arrTotBV = $arrTotBV.number_format($bvR,0,".",",")."\n";
				
				//MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false)
				//$xpos = $pdf->GetX();
				//$ypos = $pdf->GetY();
				if($xpage == 1){
					$leftAlign = $leftAlign - 5;
					$leftAlignQty = $leftAlign - 2;
				}else{
					$leftAlign = $leftAlign - 5;
					$leftAlignQty = $leftAlign - 2;
				}
				
				$pdf->SetXY((61 - $xminus) + $leftAlign, $ypos + $yposnew );
				$pdf->MultiCell(10, 2.5, $x, '', 'R', '');;
				$pdf->SetXY((71 - $xminus) + $leftAlign, $ypos );
				$pdf->MultiCell(70, 2.5, ucwords(strtolower($arrPrdDesc)), '', 'L', '');
				$pdf->SetXY((138 - $xminus) + $leftAlignQty, $ypos );
				$pdf->MultiCell(18, 2.5, $arrQty, '', 'R', '');	
				$pdf->Ln(0);
				
				if($x > 10){
					$tambahBawah = $tambahBawah + 2.3;
				}
				
		        $x++;
				$yposnew = $yposnew + 2.5;
		    }
			
			$x = $x - 1;			
			if($x < 10){
				for($i = 1; $i <= 10 - $x; $i++){
					//echo "nilai baru $i</br>";
					$xpos = $pdf->GetX();
					$ypos = $pdf->GetY();
					$pdf->SetXY(($xpos + 1 - $xminus) + $leftAlign, $ypos );
					$pdf->MultiCell(10, 2.5, ".", '', 'R', '');
					$pdf->SetXY(71 - $xminus, $ypos );
					$pdf->MultiCell(70, 2.5, "\n", '', 'L', '');
					$pdf->SetXY(133.0 - $xminus, $ypos );
					$pdf->MultiCell(23, 2.5, "\n", '', 'R', '');	
					$pdf->Ln(0);
					if($i > 8){
						$tambahBawah = $tambahBawah + 2;
					}
				}				
			}
			
		    $tot = $no_urut + $product_id + $product_name;
			$tot2 = $dp + $bv;
			
			//$pdf->Row(array($no, ucwords(strtolower($arrPrdDesc)), $arrQty, "\n\n\n\n"));
			$pdf->SetWidths(array(75, 15)); //NO, PRD DESC, QTY
			$pdf->SetFont('Arial','B',6.5);
			//$pdf->Row(array("Total", $total_qty));
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			if($xpage == 1){
				$leftAlign = $leftAlign - 3;
				$leftAlignQty = $leftAlign + 1;
			}else{
				$leftAlign = $leftAlign - 3;
				$leftAlignQty = $leftAlign + 1;
			}
			
			$pdf->SetXY(($xpos + 1 - $xminus) + $leftAlign, $ypos );
			$pdf->MultiCell(80, 2.5, "", '', 'C', '');
			$pdf->SetXY((141 - $xminus) + $leftAlign, $ypos );
			$pdf->MultiCell(15, 2.5, $total_qty, '', 'R', '');	
			$pdf->Ln(0);
			//=============================END DETAIL PRODUCT=============================================
			// ==========================End Coba Hilal dengan menggunakan table==========================
			
			$pdf->Ln(0);
	}
       	
	$pdf->Output(); 
	//$pdf1->Output(); 
	

?>
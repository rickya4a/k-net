<form id="groupPrd" method="post" >
    
        <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable">
            <?php 
                foreach($headDO as $row){
                    $kode = $row->kode;
                    $desc = $row->nama;
                    $addr1 = $row->alamat1;
                    $addr2 = $row->alamat2;
                    $tel_hp = $row->tel_hp;
                }
                
                $kw = "";
                for($i=0;$i < count($arrayCek['cekArray']);$i++)
                {
                    $kw .= "".$arrayCek['cekArray'][$i].", ";
                }
                    $kw = substr($kw, 0, -2);
            ?>
            <tr>
                <td style="width: 15%;">&nbsp;<?php echo $headNM;?></td>
                <td>&nbsp;<?php echo $kode ." / ".$desc;?></td>
            </tr>
            <tr>
                <td>&nbsp;Alamat</td>
                <td>&nbsp;<?php echo $addr1;?></td>
            </tr>
            <tr>
                <td>&nbsp;No Telp</td>
                <td>&nbsp;<?php echo $tel_hp;?></td>
            </tr>
            <tr>
                <td>&nbsp;List KW</td>
                <td>&nbsp;<?php echo $kw;?></td>
            </tr>
            <tr>
                <td style="color: red;">&nbsp;Ship By</td>
                <td>&nbsp; <select id="courier" name="courier"><?php foreach($kurir as $listt){ echo "<option value=\"$listt->code\">$listt->description</option>";}?></select></td>
            </tr>
        </table>
        
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
             <tr >
                <th colspan="4" style="font-weight: bold;text-align: center;background-color:#f4f4f4 ;">SUMMARY PRODUCT GROUPING</th>
             </tr>
    		  <tr bgcolor="#f4f4f4">
                <th>NO</th>
    			<th>PROD CODE</th>
                <th>PROD NAME</th>
                <th>QTY</th>
    		  </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            $totQty = 0;
            foreach($groupDO as $row)
            {
                echo "
                <tr>
                    <td>".$no++."</td>
                   <td align=\"right\">$row->prdcd</td>
                   <td>$row->prdnm</td>
                   <td><div align=right>".number_format($row->qty_prd,0,"",",")."</div></td>
                </tr>";
                $totQty += $row->qty_prd;
            }
            ?>
            <tr>
                <td colspan="3">TOTAL</td>
                <td style="font-weight: bold;"><?php echo "<div align=right>".number_format($totQty,0,"",",")."</div>";?></td>
            </tr>
            <tr><td colspan="6">
            <input type="button" class="btn btn-warning" name="back" value="Back" onclick="All.back_to_form(' .nextForm1',' .mainForm')"  />&nbsp;&nbsp;
            
            <input type="button" class="btn btn-primary" onClick="be_trans.generateDO();" name="submit" value="Grouping" id="checkss"/>
            </td></tr>
            </tbody>
        </table>
    </form>
    <div class="result"></div>
    <?php
        /*$head = "";
        foreach($detDO as $y){
            if($head== "" or $head == $y->KWno){
                echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">
                  <tr>
                    <td colspan=\"4\">DETAIL PRODUCT GROUPING ".$y->KWno."</td>
                  </tr>";
            }
            echo "</table>";
        }*/
    
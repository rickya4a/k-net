<?php
	if($result['response'] == "false") {
		setErrorMessage("No record found, try another parameter..");
	} else {
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
    	<th colspan="7">List Bank Charge</th>
    </tr>	
    <tr>
        <th width="5%">No.</th>
        <th width="10%">Bank Code</th>
        <th width="20%">Bank Desc</th>
        <th>Bank Display Name</th>
  		<th width="10%">Charge Admin</th>
  		<th width="10%">Charge Connectivity</th>
        <th width="10%">Act</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no = 1;
		$tot_price = 0;
        foreach($result['arrayData'] as $row){
        //no_tujuan, harga_jual, tgl_trans, jam, client, lunas, order_lgs	
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
        <td align="center"><a href = "#" id=" <?php echo $row->id;?>"><?php echo $row->bankCode;?></a></td>
        <td align="center"><?php echo $row->bankDesc;?></td>
        <td align="center"><?php echo $row->bankDisplayNm;?></td>
        <td style="text-align: right;"><?php echo number_format($row->charge_admin,0,".",".");?></td>
        <td style="text-align: right;"><?php echo number_format($row->charge_connectivity,0,".",".");?></td>
        
        <td align="center">
        	<a id="<?php echo $row->id;?>" onclick="javascript:be_trans.getUpdateChargeBank(<?php echo $row->id;?>)" class="btn btn-mini btn-primary"><i class="icon-edit icon-white"></i></a>
        	<a id="<?php echo $row->id;?>" onclick="javascript:All.deleteData('trans/delete/<?php echo $row->id;?>','delTrans','trans/delete/list')" class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
        </td>
   </tr>
    <?php
        
        $no++;
        }
    ?>
    </tbody>
</table>

<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
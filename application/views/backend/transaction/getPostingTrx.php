<div class="mainForm">
  <form class="form-horizontal" id="formPostTrx">
    <fieldset>      
      <div class="control-group">       
        <?php
          echo datepickerFromTo("Transaction Date");
        ?>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListTrxToPost()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(".dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

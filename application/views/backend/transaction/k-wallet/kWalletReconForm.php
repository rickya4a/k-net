<div class="mainForm">
		<form class="form-horizontal" id="frmReportVT" name="frmReport" method="post">
				<fieldset>
						<div class="form-group">
								<label for="input" class="col-sm-2 control-label">Type</label>
								<div class="col-sm-2">
										<select name="type" id="input" class="form-control" required="required">
												<option value="">-- Select One --</option>
												<option value="1">Potong Saldo Sukses, Trx Gagal</option>
										<?php
											if($username == "DION" || $username == "HILAL") {	
										?>		
												<option value="2">Potong Saldo Gagal, Trx Masuk</option>
										<?php
										}	
										?>		
										<!-- <option value="3">Cari Transaksi Top UP</option> -->
										</select>
								</div>
						</div>
						<div class="form-group">
								<label for="input" class="col-sm-2 control-label">Transaction</label>
								<div class="col-sm-10">
										<select name="trx" id="input" class="form-control" required="required">
												<option value="">-- Select One --</option>
												<option value="1">K-Net Sales & Reg. Member</option>
												<option value="2">PPOB & XL</option>
										</select>
								</div>
						</div>
						<div class="control-group">
								<label class="control-label" for="typeahead">Date</label>
								<div class="controls">
										<input type="text" class="dtpicker typeahead" id="date_from" name="date_from"/>&nbsp;&nbsp;to&nbsp;
										<input type="text" class="dtpicker typeahead" id="date_to" name="date_to"/>
								</div>
								<div class="controls" id="inp_btn">
										<input
												tabindex="3"
												type="button"
												id="btn_input_user"
												class="btn btn-primary .submit"
												name="save"
												value="Submit"
												onclick="All.ajaxFormPost(this.form.id,'k-wallet/recon/trx/list')"/>
										<input
												type="reset"
												tabindex="3"
												class="btn btn-danger"
												name="reset"
												value="Reset"/>
								</div>
						</div>
				</fieldset>
				<div class="result"></div>
		</form>
</div>
<script>
		$(document).ready(function () {
				$(All.get_active_tab() + " .dtpicker").datepicker(
						{changeMonth: true, numberOfMonths: 1, dateFormat: 'yy-mm-dd'}
				);
		});
</script>
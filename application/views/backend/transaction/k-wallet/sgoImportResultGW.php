<input type="button" class="btn btn-mini btn-danger" value="Clear Preview" onclick="be_trans.clear()" />
<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Import Data Status</th>
   </tr>
   </thead>
   <tbody>
   <tr>
   	 <td width="70%">Record Found</td><td align="right"><?php echo $totalRecord; ?></td>
   </tr>
   <tr>	 
   	 <td>Inserted Success</td><td align="right"><?php echo $insSuccess; ?></td>
   </tr>
   <tr>	 
   	 <td>Inserted Failed</td><td align="right"><?php echo $insFail; ?></td>
   </tr>
   <tr>	 
   	 <td>Double Record on DB</td><td align="right"><?php echo $double; ?></td>
   </tr>
   <tr>	 
   	 <td>Member Not Found</td><td align="right"><?php echo $rec_member_not_found; ?></td>
   </tr>
   <tr>	 
   	 <td>Non Top Up Transaction</td><td align="right"><?php echo $rec_nontop; ?></td>
   </tr>
   
   </tbody>
</table>

<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Member not found</th>
   </tr>
   <tr>
   	 <th width="15%">No</th>
   	 <th>Transaction ID</th>
   </tr>
   </thead>
   <tbody>
   <?php 
   $i = 0;
   foreach($member_not_found as $arr) {
   $i++;
   ?>	
   <tr>
     
   	 <td align="right"><?php echo $i; ?></td><td align="center"><?php echo $arr; ?></td>
   </tr>
   <?php } ?>
   </tbody>	
</table>
<?php
if($success != null) {
?>
<table width="80%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="6">Insert Success</th>
   </tr>
   <tr>
   	 <th width="5%">No</th>
   	 <th width="30%">Transaction ID</th>
	 <th>Member ID</th>
	 <th width="10%">Total Amount</th>
	 <th width="10%">Saldo Top Up</th>
   </tr>
   </thead>
   <tbody>
   <?php 
   $i = 0;
   foreach($success as $arr) {
   $i++;
   ?>	
   <tr>
   	 <td width="10%" align="right"><?php echo $i; ?></td>
	 <td align="center"><?php echo $arr['payment_ref']; ?></td>
	 <td align="center"><?php echo $arr['member_id']." / ".$arr['member_cust_name'];?></td>
	 <td align="right"><?php echo $arr['total_amount']; ?></td>
	 <td align="right"><?php echo $arr['amount']; ?></td>
   </tr>
   <?php } ?>
   </tbody>	
</table>
<?php
}
?>

<?php
if($member_not_found != null) {
?>
<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Member Not Found</th>
   </tr>
   <tr>
   	 <th width="15%">No</th>
   	 <th>Transaction ID</th>
   </tr>
   </thead>
   <tbody>
   <?php 
   $i = 0;
   foreach($member_not_found as $arr) {
   $i++;
   ?>	
   <tr>
   	 <td width="10%" align="right"><?php echo $i; ?></td><td align="center"><?php echo $arr; ?></td>
   </tr>
   <?php } ?>
   </tbody>	
</table>
<?php
}
?>
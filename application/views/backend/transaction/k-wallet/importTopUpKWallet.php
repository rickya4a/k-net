<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="post" id="formImportTopUp" action="<?php echo base_url(''); ?>">
    <fieldset>      
      <div class="control-group">
	     <label class="control-label" for="typeahead">Tipe Download</label>
         <div class="controls">
		   <select id="download_type" name="download_type">
				<!--<option value="goworld">Go World</option>-->
				<option value="espay">Espay</option>
		   </select>
		 </div>
         <label class="control-label" for="typeahead">File CSV to upload</label>
            <div class="controls" >
              <input type="file" id="fileCSV" name="myfile" class="span7 typeahead" />
            </div>
         <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Export To DB" onclick="saveSGOFileToDatabaseGW()" />
            <input tabindex="6"  type="reset" class="btn btn-reset" value="Reset" />
            <input id="btn-read-file" type="button" class="btn btn-success" value="Preview Content File" onclick="readFromFile()" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div id="editor"></div>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
$(document).ready(function()
{
   
   $(All.get_active_tab() + " #fileCSV").change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'csv':
            case 'txt':
                $(All.get_active_tab() + ' #uploadButton').attr('disabled', false);
                break;
            default:
                alert('This is not an allowed file type, only CSV/TXT file..!!');
                this.value = '';
        }
   });
   
   /*$(All.get_active_tab() + " #download_type").change(function () {
		if(this.value == "espay") {
			$(All.get_active_tab() + " #btn-read-file").removeAttr("onclick");
			$(All.get_active_tab() + " #btn-read-file").attr("onclick", "be_trans.readFromFile()");
		} else {
			$(All.get_active_tab() + " #btn-read-file").removeAttr("onclick");
			$(All.get_active_tab() + " #btn-read-file").attr("onclick", "be_trans.readFromFileGw()");
		}
		console.log(this.value);
   });*/
   
   
});

function saveSGOFileToDatabaseGW() {
		var formData = new FormData($(All.get_active_tab() + " #formImportTopUp")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('k-wallet/topup/import/save'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			error: function (data) {
				All.set_enable_button();
			},
			cache : false,
			contentType : false,
			processData : false
		});
   }
   
   function readFromFile() {
		var formData = new FormData($(All.get_active_tab() + " #formImportTopUp")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('k-wallet/topup/import/preview'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			error: function (data) {
				All.set_enable_button();
			},
			cache : false,
			contentType : false,
			processData : false
		});
   }
</script>

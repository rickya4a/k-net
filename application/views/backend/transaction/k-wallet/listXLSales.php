<?php if ($result == NULL) {
	echo setErrorMessage();
} else { ?>

<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="8">List XL Failed Transaction</th>
		</tr>
		<tr>
			<th>No.</th>
			<th>TRCD</th>
			<th>Date Trx</th>
			<th>Create By</th>
			<th>Trx Type</th>
			<th>Ref. Pay ID</th>
			<th>Phone</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody id="xlTbody">
		<?php
				$i = 1;
				foreach ($result as $key) { ?>
					<tr id="<?php echo $key->trcd; ?>">
						<td style="text-align: center;"><?php echo $i; ?></td>
						<td><?php echo $key->trcd; ?></td>
						<input
							type="hidden"
							name="va_trcd"
							class="orderid"
							id="va_trcd<?php echo $i; ?>"
							value="<?php echo $key->trcd; ?>">
						<!--<td style="text-align: right"><?php echo number_format($key->amount, 2, ".", ",");?></td>-->
						<td><?php echo $key->datetrans; ?></td>
						<td><?php echo $key->memberid. " - " .$key->membername; ?></td>
						<!--<td><?php echo $key->createnm; ?></td>-->
						<td><?php echo $key->trx_type; ?></td>
						<td><?php echo $key->reff_pay_id; ?></td>
						<td><?php echo $key->cust_no; ?></td>
						<td style="text-align: center">
							<a class="btn btn-success" onclick="javascript:refundSaldoKwallet('<?php echo $i;  ?>')">Refund</a>
							<a class="btn btn-primary" onclick="be_trans.reconcileXlPaketData('<?php echo $key->trcd; ?>')">Reconcile</a>
							<a class="btn btn-warning" onclick="javascript:reconcile('<?php echo $i; ?>')">Recon Trx</a>
						</td>
					</tr>
		<?php $i++;
		}; ?>
	</tbody>
</table>

<script>
	function getBlue(param) {
		var trcd = $('#va_trcd' + param).val();
		var order_number = $('#order_number' + param).val();
		// alert(trcd + order_number)
		$.ajax({
			type: 'POST',
			url: All.get_url('k-wallet/recon/token/update'),
			dataType: 'json',
			data: {
				trcd: trcd,
				order_number: order_number
			},
			success: function (data) {
				alert(data.message);
				All.set_enable_button();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	}

	function refundSaldoKwallet(param) {
		var trcd = $('#va_trcd' + param).val();
		$.ajax({
			type: 'POST',
			url: All.get_url('k-wallet/recon/xl/balance/recover'),
			dataType: 'json',
			data: {
				trcd: trcd
			},
			success: function (data) {
				alert(data.message);
				All.set_enable_button();
				if(data.response == "true") {
					$(All.get_active_tab() + " tbody#xlTbody tr#" +trcd).remove();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	}

	function reconcile(param) {
		var orderid = $('#va_trcd' + param).val()
		$.ajax({
			type: "POST",
			url: All.get_url('k-wallet/recon/trx/only'),
			dataType: "json",
			data: { orderid: orderid },
			success: function (data) {
				alert(data.message);
				console.log(data)
				All.set_enable_button();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	}

	// datatables
	$(document).ready(function () {

		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>

<?php
}
; ?>
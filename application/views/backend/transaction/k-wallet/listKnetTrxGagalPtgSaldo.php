<?php if ($result == NULL) {
	echo setErrorMessage();
} else { ?>

<table
		class="table table-bordered bootstrap-datatable datatable"
		width="100%">
		<thead>
				<tr>
						<th colspan="7">List K-Net & Reg. Member Gagal Potong Saldo K-Wallet</th>
				</tr>
				<tr>
						<th>Order No</th>
						<th>Token</th>
						<th>Total Trx</th>
						<th>Tgl</th>
						<th>Member</th>
						<th>Sisa Saldo</th>
				</tr>
		</thead>
		<tbody>
		<?php
		$i = 1;
		foreach($result as $dta) {
			echo "<tr>";
			echo "<td align=center><a onclick=\"javascript:reconSaldo('$dta->orderno','$dta->token', '$dta->userlogin')\" class='btn btn-mini btn-primary'>$dta->orderno</a></td>";
			echo "<td align=center><a href=# id=\"$dta->orderno\" onclick=javascript:be_trans.getDetailTrx(this)>$dta->token</a></td>";
			echo "<td align=right>".number_format($dta->total_trx, 0, ".", ",")."</td>";
			echo "<td align=center>$dta->datetrans</td>";
			echo "<td>$dta->userlogin / $dta->username ($dta->tel_hp)</td>";
			echo "<td align=right>".number_format($dta->sisa_saldo, 0, ".", ",")."</td>";
			echo "</tr>";
		}	
		?>		
		</tbody>
</table>

<script>

	// datatables
	$(document).ready(function() {
		
		$(All.get_active_tab() + " .datatable").dataTable( { 
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
			"sPaginationType": "bootstrap",
			"oLanguage": {
			},
					"bDestroy": true
			});
			$(All.get_active_tab() + " .datatable").removeAttr('style');
	}); 
	
	function reconSaldo(ordernox, tokenx, userloginx) {
		All.set_disable_button();
		//All.get_image_load();
		$.ajax({
            url: All.get_url('k-wallet/recon/insert/failtrx') ,
            type: 'post',
			dataType: 'json',
			data: {
				orderno: ordernox,
				token: tokenx,
				userlogin: userloginx
			},
            success:
            function(data){
                alert(data.message);
				All.set_enable_button();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	}
</script>

<?php
}
; ?>
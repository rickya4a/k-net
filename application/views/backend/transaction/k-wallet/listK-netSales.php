<?php if ($result == NULL) {
	echo setErrorMessage();
} else { ?>

<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="8">List K-Net & Reg. Member</th>
		</tr>
		<tr>
			<th>TRCD</th>
			<th>Delete Order No.</th>
			<th>Order Number</th>
			<th>Created At</th>
			<th>Penerima BV</th>
			<th>Stk/Alamat</th>
			<th>Created By</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody id="knetTbody">
		<?php 
				$i = 1;
				foreach ($result as $key) { 
			if ($key->delete_orderno != NULL) { ?>
		<tr id="<?php echo $key->trcd; ?>" style="background: lightgreen;">
			<td><?php echo $key->trcd; ?></td>
			<input
				type="hidden"
				name="va_trcd"
				id="va_trcd<?php echo $i; ?>"
				value="<?php echo $key->trcd; ?>">
			<td><?php echo $key->delete_orderno; ?></td>
			<td style="text-align: center">
				<a class="btn btn-success" onclick="javascript:recoverSaldo('<?php echo $i;  ?>')">Refund</a>
			</td>
			<td><?php echo $key->createdt; ?></td>
			<td><?php echo $key->penerima_bv; ?>&nbsp;</td>
			<td><?php echo $key->nama_penerima; ?>&nbsp;</td>
			<td><?php echo $key->createnm."-".$key->fullnm; ?></td>
			<td style="text-align: right;"><?php echo number_format($key->amount, 2, ".", ","); ?></td>
		</tr>
	<?php } elseif($key->delete_orderno == NULL && $key->order_number == NULL) { ?>
		<tr id="<?php echo $key->trcd; ?>" style="background: #ff7070;">
		    <?php
			   $link = "trans/pending/det/".$key->trcd;
			?>
			<td><a id="<?php echo $key->trcd; ?>" onclick="All.ajaxShowDetailonNextForm('<?php echo $link; ?>')" href="#"><?php echo $key->trcd; ?></a></td>
			<input
				type="hidden"
				name="va_trcd"
				id="va_trcd<?php echo $i; ?>"
				value="<?php echo $key->trcd; ?>">
			<td style="text-align: center">
				<a class="btn btn-success" onclick="javascript:recoverSaldo('<?php echo $i;  ?>')">Refund</a>
			</td>
			<td style="text-align: center">
				<a class="btn btn-danger" onclick="javascript:reconcileSgoTrx('<?php echo $key->trcd; ?>')" >Recon Trx</a>
			</td>
			<td><?php echo $key->createdt; ?></td>
			<td><?php echo $key->penerima_bv; ?>&nbsp;</td>
			<td><?php echo $key->nama_penerima; ?>&nbsp;</td>
			<td><?php echo $key->createnm."-".$key->fullnm; ?></td>
			<td style="text-align: right;"><?php echo number_format($key->amount, 2, ".", ","); ?></td>
		</tr>
	<?php
			} else { ?>
		<tr id="<?php echo $key->trcd; ?>" style="background: #70a6ff;">
			<td><?php echo $key->trcd; ?></td>
			<input
				type="hidden"
				name="va_trcd"
				id="va_trcd<?php echo $i; ?>"
				value="<?php echo $key->trcd; ?>">
			<input
				type="hidden"
				name="va_trcd"
				id="order_number<?php echo $i; ?>"
				value="<?php echo $key->order_number; ?>">
			<td style="text-align: center"><input
				type="button"
				class="btn btn-primary"
				onclick="getBlue('<?php echo $i;  ?>')"
				value="Update"></td>
			<td><?php echo $key->order_number; ?></td>
			<td><?php echo $key->createdt; ?></td>
			<td><?php echo $key->penerima_bv; ?>&nbsp;</td>
			<td><?php echo $key->nama_penerima; ?>&nbsp;</td>
			<td><?php echo $key->createnm."-".$key->fullnm; ?></td>
			<td style="text-align: right;"><?php echo number_format($key->amount, 2, ".", ","); ?></td>
		</tr>
		<?php
			}
			?>
		<?php
			$i++;
		}; ?>
	</tbody>
</table>

<script>
	function getBlue(param) {
		var trcd = $('#va_trcd' + param).val();
		var order_number = $('#order_number' + param).val();
		// alert(trcd + order_number)
		$.ajax({
			type: 'POST',
			url: All.get_url('k-wallet/recon/token/update'),
			dataType: 'json',
			data: {
				trcd: trcd,
				order_number: order_number
			},
			success: function (data) {
				alert(data.message);
				All.set_enable_button();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	}

	function recoverSaldo(param) {
		var trcd = $('#va_trcd' + param).val();
		$.ajax({
			type: 'POST',
			url: All.get_url('k-wallet/recon/knet/balance/recover'),
			dataType: 'json',
			data: {
				trcd: trcd
			},
			success: function (data) {
				alert(data.message);
				All.set_enable_button();
				if(data.response == "true") {
					$(All.get_active_tab() + " tbody#knetTbody tr#" +trcd).remove();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	}

	// datatables
	$(document).ready(function () {

		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>

<?php
}
; ?>
<style>
	button {
		margin-top: 2px;
	}
</style>
<div class="mainForm">
  <!--<form class="form-horizontal" id="formPrintShippingLabel" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" > -->
  <form class="form-horizontal" id="formPrintShippingLabel" method="POST" target="_BLANK" onsubmit="return All.checkMultipleCheckbox('orderno[]')" action="<?php echo base_url('print/shipping/manifest/pdf'); ?>">
    <fieldset>      
      <div class="control-group">       
       <label class="control-label" for="typeahead">Transaction Date</label>
         <div class="controls">
           <input type="text" class="dtpicker typeahead" id="prt_form" name="prt_form" >&nbsp;to&nbsp;
		   <input type="text"  class="dtpicker typeahead" id="prt_to" name="prt_to" >
		 </div>
	   <label class="control-label" for="typeahead">Print Status</label>
         <div class="controls">
            <select id="print_count" name="print_count" class="span4">
            	<option value="0">New Order</option>
            	<option value="1">Reprint</option>
            </select>
		 </div>
		  <label class="control-label" for="typeahead">Shipping</label>
		  <div class="controls">
			  <select id="list_shipping" name="list_shipping" class="span4">
				  <option value="">--Select Here--</option>
				  <?php
				  foreach($list_shipping as $dt){

					  echo "<option value=\"$dt->shipper_id\">".$dt->shipper_name."</option>";
				  }
				  ?>
			  </select>
		  </div>
	   <label class="control-label" for="typeahead">&nbsp;</label>
         <div class="controls">	  
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListShippingCommerce()" />
            <!--<button tabindex="3" type="submit" class="btn btn-success" name="printlabel" formaction="<?php echo base_url('print/shipping/label/pdf'); ?>" value="1">Print Label</button>-->
            <button tabindex="4" type="submit" class="btn btn-success" name="printmanifest" value="2">Print Manifest</button>
            <button tabindex="5" type="submit" class="btn btn-success" name="printlabel" value="2" formaction="<?php echo base_url('print/shipping/manifest/pdfTesnew'); ?>">Print Label</button>
            <input tabindex="6"  type="reset" class="btn btn-reset" value="Reset" />
            <input type="hidden" name="CNStatus" value="0" />
          </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});
	
	/*function checkValidationShipping() {
		var atLeastOneIsChecked = $('input[name="orderno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			alert("Please select at least one transaction..");
			return false;
		} 
	}*/
</script>

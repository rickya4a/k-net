<?php
 if(!isset($result) || $result == null) {
 	echo setErrorMessage();
 } else {
 	echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
	echo "<thead>";
	echo "<tr><th colspan=5>Pick Up Listing</th></tr>";
	echo "<tr><th>No</th>";
	echo "<th>Pick Up Number</th>";
	echo "<th>Pick Up Date</th>";
	echo "<th>Order No</th>";
	echo "<th>Warehouse</th></tr>";
	echo "</thead><tbody>";
	$no = 1;
	foreach($result as $dta) {
		$pickup_datetime = NULL;
		if($dta->pickup_datetime != NULL){
			$pickup_datetime = date("d-m-Y h:i:s", strtotime($dta->pickup_datetime));	
		}else{
			$pickup_datetime = date("d-m-Y h:i:s", strtotime($dta->pickup_date));	
		}
		
		/*
		if($dta->pickup_date != NULL){
			$pickup_datetime = date("d-m-Y h:i:s", strtotime($dta->pickup_date));	
		}else{
			$pickup_datetime = date("d-m-Y h:i:s", strtotime($dta->pickup_datetime));	
		}*/
		echo "<tr>";
		echo "<td align=right>$no</td>";
		echo "<td align=left><a href=# id=\"$dta->pickup_number\" onclick=\"All.ajaxShowDetailonNextForm('trans/email/pickup/id/$dta->pickup_number')\">$dta->pickup_number</a></td>";
		echo "<td align=left>$pickup_datetime</td>";
		
		$orderno = $dta->ORDERNO;
		$ordernox = explode(" ", $orderno);
		$countArr = count($ordernox);
		$i = 1;
		$noOrder = "";
		foreach ($ordernox as $order) {
			$order = str_replace(",", "", $order);
			$comma = "";
			if($i < $countArr){
				$comma = ", ";
			}
			
			$noOrder =  $noOrder."<a href=# id=\"$order\" onclick=\"be_trans.getDetailTrx(this)\">$order</a>$comma";
			$i++;
		}
		
		echo "<td>$noOrder</td>";
		//echo "<td>$pickup_datetime</td>";
		$wh = str_replace("WAREHOUSE", "", $dta->fullnm);
		echo "<td align=left>$dta->WHCD - $wh</td>";
		echo "</tr>";
		$no++;
	}
	echo "</tbody></table>";
 }
?>
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<div class="mainForm">
  <form class="form-horizontal" id="frmReportSgo" method="post" 
  		action="<?php echo site_url("trans/sgo/report/act")?>" target="_blank">
    <fieldset>      
      <div class="control-group">       
       <label class="control-label" for="typeahead">Transaction Date</label>
               <div class="controls">
	               <input type="text" class="dtpickers typeahead" id="sgoReportFrom" name="from" />&nbsp;to&nbsp;
				   <input type="text"  class="dtpickers typeahead" id="sgoReportTo" name="to" />
			 	</div>
			    <label class="control-label" for="typeahead">Status</label>
	        	<div class="controls">
	            	<select id="status" name="status">
	            		<option value="0">All Excluded Failed</option>
	            		<option value='1'>Success</option>
	            		<option value='2'>Suspect</option>
	            		<option value='3'>Failed</option>
	            	</select>
	            </div>
	            <label class="control-label" for="typeahead">Export To</label>
				<div class="controls">
					<select id="exportTo" name="exportTo">
						<option value="2">View List</option>
						<option value="0">Excel</option>
						<option value="1">CSV</option>
					</select>
				</div>
       
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" 
        	   onclick="All.ajaxFormPost(this.form.id,'trans/sgo/report/act')" />
            <input type='submit' id='printXls' name='printXls' value='Export Data' class='btn btn-success'/>
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpickers").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

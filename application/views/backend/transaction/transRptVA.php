<?php
    if(empty($arr)){
        echo setErrorMessage();
    }else{
        $filenm = "reportTopUpVA.xls";
        header("Content-type: application/vnd.ms-excel");
        header('Content-Disposition: attachment; filename='.$filenm.'');
        header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	    header("Pragma: public");
        
       echo "<table width=\"100%\" border = \"1\">";
       echo "<thead><tr><th colspan=\"10\" bgcolor=\"#lightgrey\">List Top Up VA</th></tr>";
       echo "<tr>";
       echo "<th>No</th>";
       echo "<th>Trx ID</th>";
	   echo "<th>ID Member</th>";
       echo "<th>Member Name</th>";
	   echo "<th>Trx Date</th>";
	   echo "<th>Bank Desc</th>";
	   echo "<th>Total Transfer</th>";
	   echo "<th>Potongan</th>";
	   echo "<th>Jumlah yg Masuk</th>";	   
	   echo "<th>Credit To</th>";
       echo "<th>Credit To Name</th>";
       echo "</tr></thead>";
       echo "<tbody>";
       $i = 1;
       foreach($arr as $list) {
            echo "<tr id=\"$i\">";
                echo "<td align=\"right\">$i</td>";
                echo "<td>".$list->trcd."</td>";
                echo "<td>".$list->dfno."</td>";
				echo "<td>".$list->fullnm."</td>";
                echo "<td align=\"center\">".$list->createdt."</td>";
				echo "<td align=\"center\">".$list->bankDesc."</td>";
				echo "<td align=\"right\">".$list->total_amount."</td>";
                echo "<td align=\"right\">".$list->potongan."</td>";
				echo "<td align=\"right\">".$list->amount."</td>";
                echo "<td align=\"right\">'".$list->credit_to."</td>";
                echo "<td align=\"right\">".$list->credit_to_name."</td>";
                
            echo "</tr>";
            $i++;
       }
    }
?>
<?php
 /*$pdf = new PDF_MC_Table();
 $pdf->Open();
 $pdf->SetFillColor(255,255,255); // background = biru muda
 $pdf->SetTextColor(0,0,0);	 //	font color = black
 $pdf->SetDrawColor(0,0,0); // border 	   = brown
 $pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)
 $pdf->SetFont('Courier','', 8);
 $pdf->AddPage();

 $pdf->Image("assets/kgb_logo.jpeg",6,5, 25, 25);
 $pdf->Ln();
 //$pdf->Rect(29,26,26,26);
 $pdf->SetX(30);
 $pdf -> SetFont('Times', '', 10);
 $pdf->Cell(50, 5, "PT Kerta Gaya Pusaka", 1, 'L');
 //$pdf->SetX(80);
 $pdf->Cell(18, 30, "", 1, 'L');
 $pdf->MultiCell(20, 10, "Jumlah kiriman", 1, 'C');
 $pdf->SetX(98);
 $pdf->MultiCell(20, 10, "", 1, 'C');
 //$pdf->MultiCell(20, 10, "", 1, 'L');
 //$pdf->Cell(50, 5, "Dengan ini kami beritahukan", 1);
 $pdf->Ln();

 $pdf->Output();
*/


$pdf = new PDF_MC_Table();
$pdf -> AddPage();
//$pdf->SetX(56);
$pdf->Image("assets/kgb_logo.jpeg",10,5, 25, 25);

$pdf->SetX(33);
$pdf -> SetFont('Times', 'B', 12);
$pdf->Cell(70,7,"PT KERTA GAYA PUSAKA",0,1,'L'); 
 $pdf->Ln();
 $pdf->SetY(17);
 $pdf->SetX(33);
 $pdf -> SetFont('Times', 'B', 9);
 $pdf->Cell(70,7,$result[0]['wh_name'],0,1,'L'); 
$pdf -> SetFont('Times', '', 9);

$pdf->Code39(120,20,$result[0]['conoteJNE'],1,7);
//Table with 20 rows and 4 columns
$pdf -> SetFont('Times', '', 9);
$colAlign = array('L', 'L', 'L', 'L', 'C');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(20, 45, 20, 50, 50));
$pdf->SetY(35);
$pdf -> Row(
   array(
     "Pengirim", 
     "PT K-Link Indonesia", 
     "Penerima", 
     $result[0]['receiver_name'],
     "Jumlah"
   )
);
$colAlign = array('L', 'L', 'L', 'L', 'C', 'C');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(20, 45, 20, 50, 25, 25));

$pdf -> Row(
   array(
     "", 
     "", 
     "", 
     $result[0]['addr'],
     "Satuan : ".$result[0]['total_item']."",
     "Berat : ".$result[0]['total_weight']." (Kg)"
   )
);

$colAlign = array('L', 'L', 'L', 'L', 'C', 'C');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(20, 45, 20, 50, 25, 25));

$pdf -> Row(
   array(
     "Telp", 
     "", 
     "Telp", 
     $result[0]['contactno'],
     "",
     ""
   )
); 

$colAlign = array('L', 'R', 'R');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(25, 8, 32));

$pdf -> Row(
   array(
     "Porto",
     "Rp.",   
     "".number_format($result[0]['payShip'], 0,",", ".").""
   )
);
$colAlign = array('L', 'R', 'R');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(25, 8, 32));

$pdf -> Row(
   array(
     "PPn 1%",
     "Rp.",  
     ""
   )
);

$colAlign = array('L', 'R', 'R');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(25, 8, 32));

$pdf -> Row(
   array(
     "Premi Asuransi",  
     "Rp.",
     ""
   )
);

$colAlign = array('L', 'R', 'R');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(25, 8, 32));

$pdf -> Row(
   array(
     "Packing", 
     "Rp.", 
     ""
   )
);
$colAlign = array('L', 'R', 'R');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(25, 8, 32));

$pdf -> Row(
   array(
     "Jumlah", 
     "Rp.", 
     "".number_format($result[0]['payShip'], 0,",", ".").""
   )
);

 $pdf->SetY(60);
 $pdf->SetX(75);
 $pdf -> SetFont('Times', '', 9);
 $pdf->MultiCell(120,25,"Isi menurut pengakuan : Produk suplemen herbal",1,1,'L');
 $pdf->MultiCell(50,5,"Diterima tanggal",0,1,'L');
 $pdf->SetY(85);
 $pdf->SetX(60);
  $pdf -> SetFont('Times', '', 5);
 $pdf->MultiCell(85,5,"PENGIRIM MENYATAKAN SETUJU DENGAN ISIAN DAN SYARAT-SYARAT PENGIRIMAN YANG TERTERA PADA HALAMAN SEBALIKNYA",0,1,'C');  
 $pdf->SetY(85);
 $pdf->SetX(145);
  $pdf -> SetFont('Times', '', 9);
 $pdf->MultiCell(50,5,"".date("d-M-Y")."",0,1,'C');    

$pdf->SetY(120); 
$colAlign = array('L', 'L', 'L');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(50, 90, 50));

$pdf -> Row(
   array(
     "_____________________________", 
     "_____________________________", 
     "_____________________"
   ), false
);
$colAlign = array('L', 'L', 'L');
$pdf->SetAligns($colAlign);
$pdf -> SetWidths(array(50, 90, 50));

$pdf -> Row(
   array(
     "Tanda Tangan dan Nama Jelas", 
     "Tanda Tangan dan Nama Jelas", 
     "Bagian Pengiriman"
   ), false
);
 
 /*$pdf->Cell(20,5,"Porto",1,1,'L');
 $pdf->SetY(60);
 $pdf->SetX(30);
 $pdf->Cell(5,5," : ",1,1,'L');
 $pdf->SetY(89);
 $pdf->SetX(30);
 $pdf->Cell(20,5,"sds",1,1,'L');
 $pdf->SetX(30);
 $pdf->SetY(65);
 $pdf->Cell(20,5,"Ppn 1%",1,1,'L');
 
 $pdf->SetY(65);
 $pdf->SetX(30);
 $pdf->Cell(5,5," : ",1,1,'L');
 //$pdf->Ln();
 //$pdf->MultiCell(65,5,"DESTINATION",1,1,'C'); 
*/
$pdf -> Output();
 
?>
<div class="mainForm">
  <form class="form-horizontal" id="delTrans">
    <fieldset>      
      <div class="control-group">
      	<label class="control-label" for="typeahead">Trx Type</label>                             
        <div class="controls">
        	<select id="trx_type" name="trx_type">
        		<option value="">--Select Here--</option>
        		<option value="IDEC">E-Commerce</option>
        		<option value="E">Member Registration</option>
        		
        	</select>
        </div> 
      	<label class="control-label" for="typeahead">Search By</label>                             
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Select Here--</option>
        		<option value="id_memb">ID Member</option>
        		<option value="idstk">ID Stockist</option>
        		<option value="log_usrlogin">User Login</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead">Parameter Value</label>                             
        <div class="controls">
        	<input type="text" id="paramValue" name="paramValue" class="span4" />
        </div>
        <label class="control-label" for="typeahead">Sent To</label>                             
        <div class="controls">
        	<select id="sentTo" name="sentTo">
        		<option value="">--Select Here--</option>
        		<option value="1">Stockist</option>
        		<option value="2">Address</option>
        	</select>
        </div>		      
        <label class="control-label" for="typeahead">Transaction Date</label>
        <div class="controls">
        <input type="text" class="dtpicker" id="del_trx_from" name="from" placeholder="From" required="required" />
        &nbsp;<input type="text" class="dtpicker" id="del_trx_to" name="to" placeholder="To" required="required" />
        </div>
        <?php
         echo bonusPeriod();
        ?>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'trans/delete/list')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

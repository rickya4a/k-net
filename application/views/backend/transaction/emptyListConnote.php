<?php
    if(empty($listnoConot)){
        echo setErrorMessage();
    }else{
?>

<div class="overflow-auto">
   <!-- <form id="summPromo" method="post" >-->
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <tr >
                <th colspan="6" ><?php echo $title; ?></th>
             </tr>
                <tr>
                    <th>Order No.</th>
                    <th>Token</th>
										<th>HP</th>
                </tr>
            </thead>
            <tbody>
						<?php foreach ($listnoConot as $key) { ?>
							<tr>
								<td><a href="#" id="<?php echo $key->orderno; ?>" onclick="javascript:be_trans.getDetailTrx(this)"><?php echo $key->orderno; ?></a></td>
								<td><?php echo $key->token; ?></td>
								<td><?php echo $key->no_hp_konfirmasi; ?></td>
							</tr>
						<?php } ?>
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->

    <!--</form>-->
    <br />
</div>

<script type="text/javascript">
$(document).ready(function()
{
	All.set_datatable();
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<?php } ?>
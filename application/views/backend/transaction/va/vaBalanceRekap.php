<?php
    if(empty($vaRekap)){
        setErrorMessage("Tidak ada transaksi..");
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th>No.</th>
        <th width="20%">ID Member</th>
        <th>Nama Member</th>
        <th width="10%">Top Up Va</th>
        <th width="10%">Total Transaksi</th>
        <th width="10%">Sisa Saldo</th>
		<th width="10%">Selisih</th>
    </tr>
    </thead>
    <?php
        $no = 1;
		$selisih = 0;
        foreach($vaRekap as $row){
		 $sisa = $row->top_up - $row->terpakai;
		 if($sisa != $row->balance) {
			$selisih = $sisa - $row->balance;
		 }
		 
		 $url = 'va/rekapTrx/member/'.$row->dfno;
    ?>
    
			<tr>
				<td align="right"><?php echo $no;?></td>
				<td align="center"><a onclick="javascript:All.ajaxShowDetailonNextForm('<?php echo $url; ?>')"><?php echo $row->dfno;?></a></td>
				<td align="left"><?php echo $row->fullnm;?></td>
				<td align="right"><?php echo number_format($row->top_up,"0",".", ",");?></td>
				<td align="right"><?php echo number_format($row->terpakai,"0",".", ",");?>&nbsp;</td>
				<td align="right"><?php echo number_format($row->balance,"0",".", ",");?>&nbsp;</td>
				<td align="right"><?php echo number_format($selisih,"0",".", ",");?>&nbsp;</td>
				
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>
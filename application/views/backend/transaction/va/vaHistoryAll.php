<?php
    if(empty($res)){
        setErrorMessage("Tidak ada transaksi..");
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
	<tr>
		<th colspan="7">Rekap Mutasi Virtual Account</th>
	</tr>
    <tr>
        <th>No.</th>
        <th>No Trx</th>
		<th>Tipe Trx</th>
		<th>Member</th>
        
        <th>Debet</th>
		<th>Kredit</th>
        <th>Tgl Trx</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($res as $row){
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
		<?php
			if($row->trtype == "TRM" || $row->trtype == "TRS") {
				//$link = "onclick='javascript:be_trans.getDetailTrx(this)'";
				echo "<td align=\"center\"><a href = \"#\" id=\"$row->trcd\" onclick=\"javascript:be_trans.getDetailTrx(this)\">$row->CNno</td>";
			} else if($row->trtype == "PX2") {
				//$link = "onclick='javascript:be_trans.getDetailTrx(this)'";
				echo "<td align=\"center\"><a href = \"#\" id=\"$row->trcd\" onclick=\"javascript:be_voucher.getDetailHifi(this)\">$row->trcd</td>";
			} else {
				echo "<td align=\"center\"><a href=\"#\">$row->trcd</a></td>";
			}
		?>
        
        <td align="center"><?php echo $row->shortdescription;?></td>
		<td align="left"><?php echo $row->dfno." / ".$row->fullnm;?></td>
		<?php
			if($row->tipe_dk == "K") {
			    echo "<td align=\"right\">0&nbsp;</td>";
			    echo "<td align=\"right\">".number_format($row->amount,"0",".", ",")."&nbsp;</td>";
			} else {
			   echo "<td align=\"right\">".number_format($row->amount,"0",".", ",")."&nbsp;</td>";
			   echo "<td align=\"right\">0&nbsp;</td>";
			    
			}
		?>
        <!--<td align="center"><?php echo $row->tipe_dk;?></td>
        <td align="right"><?php echo number_format($row->amount,"0",".", ",");?>&nbsp;</td>-->
        <td align="center"><?php echo $row->createdt;?></td>
        
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
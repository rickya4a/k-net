<?php
    if(empty($vaRekap)){
        setErrorMessage("Tidak ada transaksi..");
    }else{
?>
<div class="mainForm">
<form class="form-horizontal" id="vaRekapHistTrx" method="POST" action="<?php echo base_url('va/mutasiTrx/excel'); ?>">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Tipe Trx (D/K)</label>
        <div class="controls">
		   <select id="tipe_dk" name="tipe_dk" onchange="pilJenisDK()">
				<option value="">--All--</option>	
				<option value="K">Uang Masuk / Top Up Saldo</option>
				<option value="D">Uang Keluar / Transaksi</option>
		   </select>
		</div>
		<label style="display: none;" class="control-label cashout" for="typeahead">Jenis Transaksi</label>
        <div style="display: none;" class="controls cashout">
		   <select id="trxtype" name="trxtype">
				<option value="">--All--</option>	
				<?php
				foreach($listTrx as $dtax) {
				echo "<option value=\"$dtax->trxtype\">$dtax->shortdescription</option>";
				}
				?>
		   </select>
		</div>
        <label class="control-label" for="typeahead">Tgl Transaksi</label>
        <div class="controls">
        <input type="text" class="dtpicker" id="varek_trx_from" name="from" placeholder="From" required="required" />
        &nbsp;<input type="text" class="dtpicker" id="varek_trx_to" name="to" placeholder="To" required="required" />
		
		</div>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">

            <button class="btn btn-primary" type="button" name="save"  onclick="All.ajaxFormPost(this.form.id,'va/mutasiTrx')">Detail VA Trx</button>
           <!-- <button class="btn btn-primary"  type="button" id="btn_recruit" onclick="javascript:be_voucher.getListVaTrx(this)">Rekap VA Trx</button>--->
            
			<button class="btn btn-primary"  type="button" id="btn_rekap_saldo" onclick="javascript:All.ajaxFormPost(this.form.id,'va/saldo/member')">Rekap Saldo VA</button>
            
			<button class="btn btn-success" type="submit">Print Excel</button>
        </div>
         
        </div> <!-- end control-group -->
     </fieldset>
	 <div class="result">
		
	 </div>
  </form>
</div>
    
<?php
    }
?>

<script type="text/javascript">
function pilJenisDK() {
	var tipe_dk = $(All.get_active_tab() + " #tipe_dk").val();
	if(tipe_dk == "K" || tipe_dk == "") {
		$(All.get_active_tab() + " .cashout").css("display", "none");
	} else {
		$(All.get_active_tab() + " .cashout").css("display", "block");
	}
}

$(document).ready(function() 
{
	
	
	$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());
 });

</script>

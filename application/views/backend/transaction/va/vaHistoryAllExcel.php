<?php
$today= date('Ymdhis');
$filename="rekapVaHistory.xls";

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$filename);
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%" border="1">
    <thead>
	<tr>
		<th colspan="8">Rekap Mutasi Virtual Account</th>
	</tr>
    <tr>
        <th>No.</th>
        <th>No Trx</th>
		<th>Tipe Trx</th>
		<th>ID Member</th>
		<th>Member</th>
        
        <th>Debet</th>
		<th>Kredit</th>
        <th>Tgl Trx</th>
    </tr>
    </thead>
    <?php
        $no = 1;
        foreach($res as $row){
    ?>
    
    <tr>
       
        <td align="right"><?php echo $no;?></td>
		<?php
			if($row->trtype == "TRM" || $row->trtype == "TRS") {
				//$link = "onclick='javascript:be_trans.getDetailTrx(this)'";
				echo "<td align=\"center\">$row->CNno</td>";
			} else if($row->trtype == "PX2") {
				//$link = "onclick='javascript:be_trans.getDetailTrx(this)'";
				echo "<td align=\"center\">$row->trcd</td>";
			} else {
				echo "<td align=\"center\">$row->trcd</td>";
			}
		?>
        
        <td align="center"><?php echo $row->shortdescription;?></td>
		<td align="left"><?php echo $row->dfno;?></td>
		<td align="left"><?php echo $row->fullnm;?></td>
		<?php
			if($row->tipe_dk == "K") {
			    echo "<td align=\"right\">0&nbsp;</td>";
			    echo "<td align=\"right\">".$row->amount."</td>";
			} else {
			   echo "<td align=\"right\">".$row->amount."</td>";
			   echo "<td align=\"right\">0&nbsp;</td>";
			    
			}
		?>
        <!--<td align="center"><?php echo $row->tipe_dk;?></td>
        <td align="right"><?php echo $row->amount;?>&nbsp;</td>-->
        <td align="center"><?php echo $row->createdt;?></td>
        
            </tr>
    <?php
        $no++;
        }
    ?>
</table>
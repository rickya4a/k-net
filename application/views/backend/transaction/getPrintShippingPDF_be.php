<?php
	
	$pdf=new PDF_MC_Table(); // PDF_MC_Table();//
	
	
	$pdf->Open();
    $pdf->SetFillColor(255,255,255); // background = biru muda
    $pdf->SetTextColor(0,0,0);	 //	font color = black
    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
    $pdf->SetLineWidth(.1);
        
    $titleCol2 = 155;
    $lebarCell = 5;
	$left = 40;
	$border = 0;
	
	 //setting header product column width
    $no_urut = 7;
    $product_id = 25;
    $product_name = 90;
    $qty = 9;
    $dp = 23;
    $bv = 8;
    $kolom_total = $no_urut + $product_id + $product_name + $qty + $dp + $bv;
    $total_dp = 23;
    $total_bv = 15;
	
	$bcWidth = 50;
	$bcHeigth = 4;
	
    $pdf->SetFillColor(255,255,255);
    
	$xpage = 0;
	$y = 20;
	$xpos = $pdf->GetX();
	$ypos = $pdf->GetY();
	foreach($result as $row) {	
		$xpage = $xpage + 1;
			//============================RESI/CONOTE JNE DLL============================
			//print_r($result);
			$pdf->SetFont('Arial','',10);
			$pdf->AddPage();

			$pdf->SetMargins(7, 0, 1);
			$tambahBawah = 0;
			if($xpage == 6 || $xpage == 7 || $xpage == 8 || $xpage == 13 || $xpage == 14 || $xpage == 16 || $xpage == 17 || $xpage == 21 || $xpage == 23 || 
			   $xpage == 31 || $xpage == 33 || $xpage == 38 || $xpage == 40 || $xpage == 44 || $xpage == 45 || $xpage == 46 || $xpage == 48 ||
			   $xpage == 49 || $xpage == 58 || $xpage == 59 || $xpage == 60 || $xpage == 63 || $xpage == 76 || $xpage == 85 || $xpage == 87 || 
			   $xpage == 94 || $xpage == 95 || $xpage == 97 || $xpage == 102 || $xpage == 104 || $xpage == 106 || $xpage == 107 || $xpage == 111 ||
			   $xpage == 113 || $xpage == 114 || $xpage == 115){
				//$tambahBawah = 5;
				$tambahBawah = 0;
			}
			
			// ==========================Start Coba Hilal dengan menggunakan table==========================
			$currDate=date('d/M/Y');
			//$currDate= date_format($currDate,"d/M/Y");
			$pdf->SetFont('Arial','B',7.5);
			//$pdf->SetWidths(array(90));
			srand(microtime()*1000000); 
			$pdf->SetXY(7, 3);   
			
			//Start untuk Shipper
			// cuma untuk pancingan
			//$pdf->SetFont('Arial','B',1);                 
			$pdf->Code39_old(95, 0,trim(""),0, 2, 2); 
			// end pancingan
			
			$pdf->SetFont('Arial','',7.5);
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$pdf->MultiCell(15, 8, $pdf->Image("assets/klink_logo.jpeg",10.5,2.9 + $tambahBawah, 8, 8), 'LRTB', 'l', '');
			$pdf->SetXY(19, $ypos);
			$pdf->MultiCell(83, 4, "    Order No : ".trim($row['orderno'])."\n\n".
							$pdf->Code128($left - 15, $lebarCell + 1.5 + $tambahBawah,trim($row['orderno']), $bcWidth, $bcHeigth), 'RTB', 'L', '');	
			//$pdf->Ln(0);
			//$pdf->SetXY(17, $ypos+6);
			
			$pdf->MultiCell(15, 8, $pdf->Image($row['logo_url'],8, 13.5 + $tambahBawah, 13, 3.5), 'LRB', 'l', '');
			$pdf->SetXY(19, $ypos+7);
			
			if(trim($row['conoteJNE']) != null and trim($row['conoteJNE']) != ''){
				$conoteexp = $pdf->Code128($left - 15, $lebarCell + 8.8 + $tambahBawah, trim($row['conoteJNE']), $bcWidth, $bcHeigth);	
			}else{
				$conoteexp = null;
			}
			$pdf->MultiCell(83, 4.5, "    Conote : ".trim($row['conoteJNE'])."\n\n".
							$conoteexp, 'RB', 'L', '');	
			
		
			$pdf->SetFont('Arial','B',6.5);
			//$pdf->SetWidths(array(10, 65, 15,0)); //NO, PRD DESC, QTY
			//$pdf->Row(array("No", "Product Desc", "Qty"));
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			
			$pdf->MultiCell(15, 3.5, "Service", 'LRB', 'C', '');
			$pdf->SetXY(22, $ypos);
			$pdf->MultiCell(30, 3.5, strtoupper($row['service_type_name']), 'RB', 'L', '');
			$pdf->SetXY(52, $ypos);
			//$pdf->MultiCell(25, 3.5, number_format($row['payShip']), 'RB', 'L', '');
			$pdf->MultiCell(25, 3.5, '-', 'RB', 'L', '');	
			$pdf->SetXY(77, $ypos);
			$pdf->MultiCell(25, 3.5, '-', 'RB', 'L', '');	
			$pdf->Ln(0);
			
			
		 	$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95, 3.2, "Pengirim : ".strtoupper("PT K-Link Nusantara"), 'LR', 1, 'L', '', '');
			$pdf->SetFont('Arial','',7.5);
			$pdf->Cell(95, 3.2, ucwords(strtolower($row['whcd_addr1'])), 'LR', 1, 'L', '', '');
			$pdf->Cell(95, 3.2, ucwords(strtolower($row['whcd_addr2'])), 'LR', 1, 'L', '', '');
			$pdf->Cell(95, 3.2, ucwords(strtolower($row['whcd_addr3'])), 'LR', 1, 'L', '', '');
			
			$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95, 3.2, "Penerima : ".strtoupper($row['receiver_name'])." / ".strtoupper($row['contactno']), 
						'LRT', 1, 'L', '', '');
			$pdf->SetFont('Arial','',7.5);
			$addr = ucwords(strtolower(str_replace("\n", " ", $row['addr']))).
				    ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords(strtolower($row['kec']))));
			//echo "$addr</br>";
			$countAddr = 0;
			$split = 70;
			if(strlen($addr) > $split){
				$addsplit = null;
				$countAddr = round(strlen($addr)/$split);
				if($countAddr < 4) $countAddr = 4;
				//echo "countAddr = $countAddr</br>";
				for ($i=1; $i <= $countAddr; $i++) {
					if($i == 1){
						$start = 0;
					}else{
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<$countAddr) {
						$border = 'LR';
					}elseif($i == $countAddr){
						$border = 'LRB';
					}
					
					$pdf->Cell(95, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}else{
				//$pdf->Cell(95, 3.2, $addr, $border, 1, 'L', '', '');
				for ($i=1; $i <= 4; $i++) {
					if($i == 1){
						$start = 0;
					}else{
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<4) {
						$border = 'LR';
					}elseif($i == 4){
						$border = 'LRB';
					}
					
					$pdf->Cell(95, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}
			
		 	//Start untuk Shipper
			$pdf->Cell(95, 2.2, "x-------------------------------------------------------------------------------------------------------x", '', 1, 'L', '', '');
			// ==========================End Coba Hilal dengan menggunakan table==========================
         	$pdf->SetFont('Arial','B',7.5);    
			$pdf->Cell(95, 3.2, "---------SHIPPING DOCUMENT---------     Printed Date : $currDate", '', 1, 'L', '', '');
			
			
			$pdf->SetFont('Arial','',7.5);
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			$pdf->MultiCell(15, 8, $pdf->Image("assets/klink_logo.jpeg",10.5,56.5 + $tambahBawah, 8, 8), 'LRTB', 'l', '');
			$pdf->SetXY(19, $ypos);
			$pdf->MultiCell(83, 4, "    Order No : ".trim($row['orderno'])."\n\n".
							$pdf->Code128($left - 15, $lebarCell + 54.7 + $tambahBawah,trim($row['orderno']), $bcWidth, $bcHeigth), 'RTB', 'L', '');	
		
			$pdf->MultiCell(15, 8, $pdf->Image($row['logo_url'],8,67.5 + $tambahBawah, 13, 3.5), 'LRB', 'l', '');
			$pdf->SetXY(19, $ypos+7);
			
			if(trim($row['conoteJNE']) != null and trim($row['conoteJNE']) != ''){
				$conoteexp = $pdf->Code128($left - 15, $lebarCell + 62.5 + $tambahBawah, trim($row['conoteJNE']), $bcWidth, $bcHeigth);	
			}else{
				$conoteexp = null;
			}
			$pdf->MultiCell(83, 4.5, "    Conote : ".trim($row['conoteJNE'])."\n\n".
							$conoteexp, 'RB', 'L', '');	

			$pdf->SetFont('Arial','B',6.5);
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			
			$pdf->MultiCell(15, 2.8, "Service", 'LRB', 'C', '');
			$pdf->SetXY(22, $ypos);
			$pdf->MultiCell(30, 2.8, strtoupper($row['service_type_name']), 'RB', 'L', '');
			$pdf->SetXY(52, $ypos);
			//$pdf->MultiCell(25, 2.8, number_format($row['payShip']), 'RB', 'L', '');	
			$pdf->MultiCell(25, 2.8, '-', 'RB', 'L', '');
			$pdf->SetXY(77, $ypos);
			$pdf->MultiCell(25, 2.8, '-', 'RB', 'L', '');	
			$pdf->Ln(0);
			
			
			$pdf->SetFont('Arial','',7.5);
			$pdf->SetWidths(array(95,0));

		 	$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95, 3.2, "Pengirim : ".strtoupper("PT K-Link Nusantara"), 'LR', 1, 'L', '', '');
			$pdf->SetFont('Arial','',7.5);
			$pdf->Cell(95, 3.2, ucwords(strtolower($row['whcd_addr1'])), 'LR', 1, 'L', '', '');
			$pdf->Cell(95, 3.2, ucwords(strtolower($row['whcd_addr2'])), 'LR', 1, 'L', '', '');
			$pdf->Cell(95, 3.2, ucwords(strtolower($row['whcd_addr3'])), 'LR', 1, 'L', '', '');
			
			$pdf->SetFont('Arial','B',7.5);
			$pdf->Cell(95, 3.2, "Penerima : ".strtoupper($row['receiver_name'])." / ".strtoupper($row['contactno']), 
						'LRT', 1, 'L', '', '');
			$pdf->SetFont('Arial','',7.5);
			$addr = ucwords(strtolower(str_replace("\n", " ", $row['addr']))).
				    ucwords(strtolower($row['propinsi'].", Kecamatan ".ucwords(strtolower($row['kec']))));
			//echo "$addr</br>";
			$countAddr = 0;
			$split = 70;
			if(strlen($addr) > $split){
				$addsplit = null;
				$countAddr = round(strlen($addr)/$split);
				if($countAddr < 4) $countAddr = 4;
				//echo "countAddr = $countAddr</br>";
				for ($i=1; $i <= $countAddr; $i++) {
					if($i == 1){
						$start = 0;
					}else{
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<$countAddr) {
						$border = 'LR';
					}elseif($i == $countAddr){
						$border = 'LRB';
					}
					
					$pdf->Cell(95, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}else{
				//$pdf->Cell(95, 3.2, $addr, $border, 1, 'L', '', '');
				for ($i=1; $i <= 4; $i++) {
					if($i == 1){
						$start = 0;
					}else{
						$start = ($split*($i-1));
					}
					//echo "i = $i, split = $split, start = $start </br>";
					$addsplit = trim(substr($addr, $start, $split));
					if($addsplit == "" || $addsplit == null){
						$addsplit = "\n";
					}
					//echo "$addsplit</br>";
					if($i<4) {
						$border = 'LR';
					}elseif($i == 4){
						$border = 'LRB';
					}
					
					$pdf->Cell(95, 3.2, $addsplit, $border, 1, 'L', '', '');
				}
			}
			//$pdf->Row(array("Delivery By/Service", strtoupper($row['shipper_code'])."/".strtoupper($row['service_type_name'])));
			
			//==============================DETAIL PRODUCT================================================
			
			$x = 1;
		   
		    $total_qty = 0;
		    $total_dpR = 0;
		    $total_bvR = 0;
			$arrPrdcd = null;
			$no = null;
			$arrPrdDesc = null;
			$arrQty = null;
			$arrDP = null;
			$arrBV = null;
			$arrTotBV = null;
		    $arrTtotal_qty = null;
		    $arrTtotal_dpR = null;
		    $arrTtotal_bvR = null;
			
			$pdf->SetFont('Arial','B',6.5);
			$pdf->SetWidths(array(90)); //HEADER FOR PRODUCT DETAIL
			$transDate=date_create($row['datetrans2']);
			$transDate = date_format($transDate,"d/M/Y");
			//$pdf->Row(array("---------INVOICE DETAIL--------- Transaction Date : ".$transDate), FALSE);
			$pdf->Cell(95, 3.2, "---------INVOICE DETAIL--------- Transaction Date : ".$transDate, '', 1, 'L', '', '');
			
			$pdf->SetFont('Arial','B',6.5);
			//$pdf->SetWidths(array(10, 65, 15,0)); //NO, PRD DESC, QTY
			//$pdf->Row(array("No", "Product Desc", "Qty"));
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			
			$pdf->MultiCell(10, 2.5, "No", 'LRTB', 'R', '');
			$pdf->SetXY(17, $ypos);
			$pdf->MultiCell(70, 2.5, "Product Desc", 'RTB', 'L', '');
			$pdf->SetXY(84, $ypos);
			$pdf->MultiCell(18, 2.5, "Qty", 'RTB', 'R', '');	
			$pdf->Ln(0);
			
			$pdf->SetFont('Arial','',6.5);
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
				
		    foreach($row['detPrd'] as $dta2)
		    {
		        $dpR = $dta2->dpr * $dta2->qty;
		        $bvR = $dta2->bvr * $dta2->qty;

		        $total_qty += $dta2->qty;
		        $total_dpR += $dpR;
		        $total_bvR += $bvR;
				
				$no = $no.$x."\n";
			   	$arrPrdcd = $arrPrdcd.$dta2->prdcd."\n";
				$arrPrdDesc = $arrPrdDesc.$dta2->prdnm."\n";
				$arrQty = $arrQty.number_format($dta2->qty,0,".",",")."\n";
				$arrDP = $arrDP.number_format($dta2->qty,0,".",",")."\n";
				$arrBV = $arrBV.number_format($dta2->bvr,0,".",",")."\n";
				$arrTotBV = $arrTotBV.number_format($bvR,0,".",",")."\n";
				
				//MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false)
				$pdf->MultiCell(10, 2.5, $x, 'LR', 'R', '');
				$pdf->SetXY(17, $ypos);
				$pdf->MultiCell(70, 2.5, ucwords(strtolower($arrPrdDesc)), 'R', 'L', '');
				$pdf->SetXY(84, $ypos);
				$pdf->MultiCell(18, 2.5, $arrQty, 'R', 'R', '');	
				$pdf->Ln(0);
				
				if($x > 10){
					$tambahBawah = $tambahBawah + 2.3;
				}
				
		        $x++;
		    }
			
			$x = $x - 1;			
			if($x < 10){
				for($i = 1; $i <= 10 - $x; $i++){
					//echo "nilai baru $i</br>";
					$xpos = $pdf->GetX();
					$ypos = $pdf->GetY();
					$pdf->MultiCell(10, 2.5, ".", 'LR', 'R', '');
					$pdf->SetXY(17, $ypos);
					$pdf->MultiCell(70, 2.5, "\n", 'R', 'L', '');
					$pdf->SetXY(79, $ypos);
					$pdf->MultiCell(23, 2.5, "\n", 'R', 'R', '');	
					$pdf->Ln(0);
					if($i > 8){
						$tambahBawah = $tambahBawah + 2;
					}
				}				
			}
			
		    $tot = $no_urut + $product_id + $product_name;
			$tot2 = $dp + $bv;
			
			//$pdf->Row(array($no, ucwords(strtolower($arrPrdDesc)), $arrQty, "\n\n\n\n"));
			$pdf->SetWidths(array(75, 15)); //NO, PRD DESC, QTY
			$pdf->SetFont('Arial','B',6.5);
			//$pdf->Row(array("Total", $total_qty));
			
			$xpos = $pdf->GetX();
			$ypos = $pdf->GetY();
			
			$pdf->MultiCell(80, 2.5, "Total", 'LRTB', 'C', '');
			$pdf->SetXY(87, $ypos);
			$pdf->MultiCell(15, 2.5, $total_qty, 'RTB', 'R', '');	
			$pdf->Ln(0);
			//=============================END DETAIL PRODUCT=============================================
			// ==========================End Coba Hilal dengan menggunakan table==========================
			
			$pdf->Ln(0);
	}
       	
	$pdf->Output(); 
	//$pdf1->Output(); 
	

?>
<?php
 if($result == null) {
 	setErrorMessage();
 } else {
 	//echo "beda tempat";
	if($rptType == "1"){ //if rptType = 1, then export to xls
		header("Content-type: application/vnd.ms-excel");
	    header("Content-Disposition: attachment; filename=ReportReconcileSGO.xls" );
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	    header("Pragma: public");
	}
   
 	   if($rptType == 1){
 	   		$border = " border = 1";
	   }elseif($rptType == 4){
	   		$border ="";
	   }
	   
 	   echo "<table width=\"100%\" $border class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=\"9\" bgcolor=\"#lightgrey\">List SGO Transaction</th></tr>";
       echo "<tr bgcolor=\"#f4f4f4\">";
       echo "<th >No</th>";
       echo "<th >Order No</th>";
       echo "<th>Member</th>";
       echo "<th >Amount</th>";
	   echo "<th >Date</th>";
	   echo "<th>Status SGO</th>";
	   echo "<th>Status Trx</th>";
	   echo "<th>Status Memb</th>";
	   echo "<th>Bank</th>";
       echo "</tr></thead>";
       echo "<tbody>";
       $i = 1;
	   //echo $status;
       foreach($result as $list) {
                echo "<tr id=\"$i\">";
		        //echo "<td><div align=\"center\"><input type=\"checkbox\" id=\"pil$i\" name=\"orderno[]\" value=\"$list->orderno\" /></div></td>";
                echo "<td align=\"right\">$i</td>";
                echo "<td align=\"left\">$list->Order_ID
                	      <input type=\"hidden\" value=\"$list->Order_ID\" id=\"$list->Order_ID-$i\">
                	  </td>";
                echo "<td align=\"left\">$list->id_memb - $list->nmmember</td>";
				
                echo "<td align=\"right\">".number_format($list->Amount, 0, ",", ".")."</td>";
				echo "<td align=\"center\">$list->Transaction_Date</td>";
				
				//===================edit hilal 2015-12-07========================
				/*
				if($list->status_trx == "0"){
						
					/*
					if($rptType == 1){
						$act = "Suspended";
					}elseif($rptType == 0){
						$act = "<input type=\"button\" class=\"btn btn-mini btn-primary\" value=\"Reconcile\"
									   onclick=\"javascript:All.deleteData('trans/sgo/reconcile/save/$list->Order_ID','formReconcileSGO','trans/sgo/reconcile/list/0')\" 
							    />";
					}
					
					
					echo "<td align=\"left\">  </td>";
				}else{
					echo "<td align=\"center\">$list->Status_sgo</td>";
				}
				*/
				
				$status_sgo = $list->Status_sgo;
				$status_trx = $list->status_trx;
				
				if($status_sgo == "Failed"){
					$actSGO = "<font color=\"red\"> Failed </font>";
					$alignSGO = "left";
					$classSGO = "class=\"text-danger\"";
				}elseif($status_sgo == "Suspect"){ 
					$actSGO = "<font color=\"blue\"> Suspect </font>";
					$alignSGO = "left";
					$classSGO = "";
				}elseif($status_sgo == "Success"){
					$actSGO = "<font color=\"#7B823E\"> Success </font>";	
					$alignSGO = "left";
					$classSGO = "";
				}
				echo "<td align=\"$alignSGO\">$actSGO </td>";
				
				$align = "left";
				if($status_trx == 2){
					$act = "<font color=\"red\"> Failed </font>";
					$align = "left";
					$class = "class=\"text-danger\"";
				}elseif($status_sgo == "Suspect" && $status_trx == 3){ 					
					$act = "<font color=\"blue\"> Reconsile ( SP ) </font>";
					$align = "center";
					$class = "";
				}elseif($status_sgo == "Suspect" && $status_trx == 1){ 	
					$act = "<font color=\"#7B823E\"> Success </font>";	
					$align = "left";
					$class = "";
				}elseif($status_sgo == "Success" && $status_trx == 1){
					$act = "<font color=\"#7B823E\"> Success </font>";	
					$align = "left";
					$class = "";
				}elseif($status_trx == 0){ 
					$act = "<font color=\"#7B823E\"> Reconsile ( SC ) </font>";
					$align = "center";
					$class = "";
				}else{
					echo $status_sgo."<br \>".$status_trx;
				}
				echo "<td align=\"$align\"> $act </td>";
				
				
				$MEMB_STATUS = $list->MEMB_STATUS;
				
				if($MEMB_STATUS == '0' || $MEMB_STATUS == '2' || $MEMB_STATUS == '3'){
					 /*0 = PENDING INSERT ONLY TO KLINK_MLM2010.DBO.MSMEMB, 
					  '2' -- PENDING, ECOMM_MEMB_OK & MSMEMB, 
					  3' -- PENDING, ECOMM_MEMB_OK_SGO & ECOMM_MEMB_OK & MSMEMB */
					//IF $list->id_memb == NULL OR $list->id_memb = '' THEN REPLACE WITH '**XX'
					$memberid = $list->id_memb;
					if($memberid == null || $memberid == ''){
						$memberid = '**XX';
					}
					$statusMemb = "<font color =\"blue\">Reconcile</font>";
					$alignMemb = "left";
					$classMemb = "";
				}elseif($MEMB_STATUS == '1'){ //INSERT MEMBER SUCCESS
					$statusMemb = "<font color=\"#7B823E\"> Success </font>";	
					$alignMemb = "left";
					$classMemb = "";
				}elseif($MEMB_STATUS == 'S'){ ///'S' --SALES
					$statusMemb = "<font color=\"#7B823E\"> Sales </font>";	
					$alignMemb = "left";
					$classMemb = "";
				}elseif($MEMB_STATUS == 'F'){ ///'F' --Failed
					$statusMemb = "<font color=\"red\"> Failed </font>";
					$alignMemb = "left";
					$classMemb = "class=\"text-danger\"";
				}elseif($MEMB_STATUS == 'C'){ ///'C' --Payment ok, but data not inputed, confirm to distributor ask for detail
					$statusMemb = "<font color=\"red\"> Confirm </font>";
					$alignMemb = "left";
					$classMemb = "class=\"text-danger\"";
				}elseif($MEMB_STATUS == '' || $MEMB_STATUS == NULL){ ///'' OR NULL --PAYMENT FOR OTHER TRX
					$statusMemb = "<font color=\"red\"> N/A </font>";
					$alignMemb = "left";
					$classMemb = "class=\"text-danger\"";
				}  
				
				
				if($status_sgo == "Failed"){
					$actSGO = "<font color=\"red\"> Failed </font>";
					$alignSGO = "left";
					$classSGO = "class=\"text-danger\"";
				}elseif($status_sgo == "Suspect"){ 
					$actSGO = "<font color=\"blue\"> Suspect </font>";
					$alignSGO = "left";
					$classSGO = "";
				}elseif($status_sgo == "Success"){
					$actSGO = "<font color=\"#7B823E\"> Success </font>";	
					$alignSGO = "left";
					$classSGO = "";
				}
				
				
				echo "<td>$statusMemb</td>
					  <td>".$list->bankDesc."</td>";
				//echo "<td>IDEC = $IDEC <BR / >EC = $EC <BR / >RM = $RM <BR / >E = $E <BR / ></td>";
				//===================end edit hilal 2015-12-07========================
				
                
                echo "</tr>";
              $i++; 
        }
	
    echo "</tbody></tr>";
	
    echo "</table>";
 }

?>
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>

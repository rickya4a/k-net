<?php
    if(empty($listGrpDO)){
        echo setErrorMessage();
    }else{
?>
<div class="mainForm">
    <form id="listingGroupDO" method="post" action="<?php echo site_url('wh/do/txt1');?>">
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>GDO NO</th>
                    <th>Entry Date</th>
                    <th>Ship To</th>
                    <th>Create By</th>
                    <th>Ship By</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    foreach($listGrpDO as $row){
                    echo "<tr>
                            <td align=\"center\">$no</td>
                            <td><a href = \"#\" onclick=\"All.ajaxShowDetailonNextForm('wh/do/list/det/$row->trcd/$row->shipto')\">".$row->trcd."</a></td>
                            <td align=\"center\">".date("d/m/y",strtotime($row->createdt))."</td>
                            <td align=\"center\">".$row->shipto."</td>
                            <td align=\"center\">".$row->createnm."</td>
                            <td align=\"center\"> ".$row->shipby."</td>
                            
                         </tr>";
                         $no++;
                    }
                ?>
               <!-- <td align=\"center\">
                                <input type=\"hidden\" name=\"doNo\" id=\"doNo\" value=\"$row->trcd\"/>
                                <input type=\"hidden\" name=\"shipto\" id=\"shipto\" value=\"$row->shipto\"/>
                                <input type=\"submit\" value=\"Print To Text\" name=\"submit\" class=\"btn-success\"/>
                            </td>-->
            </tbody>
        </table>
        
    </form>
    <div class="resGroupDO"></div>
</div>

<?php }?> 
<script>
  $( document ).ready(function() {
   All.set_datatable();
    });
</script>
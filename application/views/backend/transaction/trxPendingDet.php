<table style="width: 100%;" class='table table-striped table-bordered bootstrap-datatable'>
    <tr>
        <td colspan="4" style="text-align: center;font-weight: bold;height: 35px;font-size: medium;">TRANSACTION DATA</td>
    </tr>
    <?php 
        foreach($header as $head){
            $orderno = $head->orderno;
            $totbv = $head->total_bv;
            $cnNo = $head->CNno;
            $RegNo = $head->REGISTERno;
            $totPay = $head->total_pay;
            $KWno = $head->KWno;
            $payShip = $head->payShip;
            $payAdm = $head->payAdm;
            $IPno = $head->IPno;
            $idmember = $head->id_memb;
            $nmmember = $head->member;
            $payConnectivity = $head->payConnectivity;
            $userlogin = $head->userlogin;
            $hplogin = $head->tel_hp;
            $trxdate = $head->datetrans;
            $idstk = $head->idstk;
            $nmstk = $head->nmstkk;
            $bonusmonth = $head->bonusmonth;
            if($KWno == null){
                $bvstat = 'unsettled';
                $cnstat = 'unsettled';
                $color = 'red';
            }else{
                $bvstat = 'settled';
                $cnstat = 'settled';
                $color = 'black';
            }
        }
    ?>
    <tr>
        <td>Orderno / Token No</td>
        <td>&nbsp;<?php echo $orderno;?></td>
        <td>Total Bv</td>
        <td style="text-align: right;">&nbsp;<?php echo $totbv;?></td>
    </tr>
    <tr>
        <td>CN No / Register No</td>
        <td>&nbsp;<?php echo $cnNo." - ".$RegNo;?></td>
        <td>Total Pay</td>
        <td style="text-align: right;">&nbsp;<?php echo number_format($totPay,0,'.','.');?></td>
    </tr>
    <tr>
        <td>KW No</td>
        <td>&nbsp;<?php echo $KWno;?></td>
        <td>Shipping Cost</td>
        <td style="text-align: right;">&nbsp;<?php echo number_format($payShip,0,'.','.');?></td>
    </tr>
    <tr>
        <td>IP No</td>
        <td>&nbsp;<?php echo $IPno;?></td>
        <td>Admin Cost</td>
        <td style="text-align: right;">&nbsp;<?php echo number_format($payAdm,0,'.','.');?></td>
    </tr>
    <tr>
        <td>ID Member</td>
        <td>&nbsp;<?php echo $idmember." / ".$nmmember;?></td>
        <td>Connectivity Cost</td>
        <td style="text-align: right;">&nbsp;<?php echo number_format($payConnectivity,0,'.','.');?></td>
    </tr>
    <tr>
        <td>Input By / Phone No</td>
        <td>&nbsp;<?php echo $idmember." / ".$hplogin;?></td>
        <td>CN Status</td>
        <td style="color: <?php echo $color;?>;">&nbsp;<?php echo $cnstat;?></td>
    </tr>
    <tr>
        <td>Trx Date</td>
        <td>&nbsp;<?php echo date("d/m/Y H:i:s",strtotime($trxdate));?></td>
        <td>BV Status</td>
        <td style="color: <?php echo $color;?>;">&nbsp;<?php echo $bvstat;?></td>
    </tr>
    <tr>
        <td>ID Stockist</td>
        <td>

            <?php
            if($idstk == ''){
                ?>
            <form action="#" target="_blank" method="post" id="frmPending" class="form-horizontal">
                <input type="hidden" id="orderno" name="orderno" value="<?php echo $orderno?>">
                <input type="text" class="form-control" id="idstk" name="idstk" onchange="javascript: All.getNmStockist(this)">
                <input type="text" class="form-control" id="nmstk" name="nmstk" readonly>
<!--                <input type="submit" class="btn btn-info" value="Update" onclick="All.ajaxFormPost('frmPending','backend/transaction/InsertTrxPending')">-->
                <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" disabled name="save" value="Update" onclick="All.ajaxFormPost('frmPending','backend/transaction/InsertTrxPending')"/>
            </form>
                <?php
            }
            else{
                ?>
                &nbsp;<?php echo $idstk." / ".$nmstk;?>
                <?php
            }
            ?>


        </td>
        <td>Bonus Period</td>
        <td>&nbsp;<?php echo $bonusmonth;?></td>
    </tr>
    <tr>
        <td colspan="4"><input class="btn btn-small btn-warning" value="<< Back" onclick="All.back_to_form(' .nextForm1',' .mainForm')" type="button"></td>
        
    </tr>
</table>
<table style="width: 100%;" class='table table-striped table-bordered bootstrap-datatable'>
    <tr>
        <td colspan="8" style="text-align: center;height: 35px;font-size: medium;font-weight: bold;">DETAIL PRODUCT</td>
    </tr>
    <tr style="text-align: center;font-weight: bold;">
        <td>No</td>
        <td>Prd Code</td>
        <td>Prd Name</td>
        <td>Qty</td>
        <td>DP</td>
        <td>BV</td>
        <td>Tot DP</td>
        <td>Tot BV</td>
    </tr>
    <?php 
        $no = 1;
        $totBVv = 0;
        $totDPp = 0;
        foreach($detail as $prod){
        $dpp = $prod->qty * $prod->dpr;
        $bvv = $prod->qty * $prod->bvr;
    ?>
    <tr>
        <td><?php echo $no;?></td>
        <td><?php echo $prod->prdcd;?></td>
        <td><?php echo $prod->prdnm;?></td>
        <td style="text-align: right;"><?php echo number_format($prod->qty,0,'.','.');?></td>
        <td style="text-align: right;"><?php echo number_format($prod->dpr,0,'.','.');?></td>
        <td style="text-align: right;"><?php echo number_format($prod->bvr,0,'.','.');?></td>
        <td style="text-align: right;"><?php echo number_format($dpp,0,'.','.');?></td>
        <td style="text-align: right;"><?php echo number_format($bvv,0,'.','.');?></td>
    </tr>
    <?php 
        $totDPp += $dpp;
        $totBVv += $bvv;
        $no++;}?>
    <tr>
        <td colspan="6">&nbsp;</td>
        <td style="text-align: right;"><?php echo number_format($totDPp,0,'.','.');?></td>
        <td style="text-align: right;"><?php echo number_format($totBVv,0,'.','.');?></td>
    </tr>
</table>
<table style="width: 100%;" class='table table-striped table-bordered bootstrap-datatable'>
    <tr>
        <td colspan="6" style="text-align: center;height: 35px;font-size: medium;font-weight: bold;">DETAIL PAYMENT INFO</td>
    </tr>
    <tr style="text-align: center;font-weight: bold;">
        <td>No</td>
        <td>Payment Type</td>
        <td>Doc No</td>
        <td>Pay Amount</td>
        <td>Charge</td>
        <td>Status</td>
    </tr>
    <?php
        $no = 1;
        $totpayments = 0;
        $totcharge = 0;
        foreach($payment as $pay)
        {
            if($pay->notes == null){
                $status = 'pending';
                $warna = 'red';
            }else{
                $status = 'trx ok';
                $warna = 'black';
            }
    ?>
    <tr>
        <td>&nbsp;<?php echo $no;?></td>
        <td>&nbsp;<?php echo $pay->paytype;?></td>
        <td>&nbsp;<?php echo $pay->docno;?></td>
        <td style="text-align: right;">&nbsp;<?php echo number_format($pay->payamt,0,'.','.');?></td>
        <td style="text-align: right;">&nbsp;<?php echo number_format($pay->charge_admin,0,'.','.');?></td>
        <td style="text-align: center;color: <?php echo $warna;?>;">&nbsp;<?php echo $status;?></td>
    </tr>    
    <?php 
            $no++;
            $totpayments += $pay->payamt;
            $totcharge = $pay->charge_admin;
        } 
    ?>
    <tr>
        <td colspan="3" style="text-align: center;font-weight: bold;"> T O T A L</td>
        <td style="text-align: right;"> <?php echo number_format($totpayments,0,'.','.');?></td>
        <td style="text-align: right;"> <?php echo number_format($totcharge,0,'.','.');?></td>
        <td>&nbsp;</td>
    </tr>
</table>
<br />

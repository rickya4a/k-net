<?php
print_r($result);
$token = $result['hdr']['token'];
echo "<form>";
echo "<table width=\"70%\" class=\"table table-striped table-bordered\">";
echo "<tr>";
echo "<td width=30%>Data produk</td>";
if($result['product'] == null) {
	echo "<td>Gagal diinput</td>";
	$prd_stt = 0;
} else {
	echo "<td>OK</td>";
	$prd_stt = 1;
}
echo "</tr>";
echo "<tr>";
echo "<td>Data Shipping</td>";
if($result['shipping'] == null) {
	echo "<td>Gagal diinput</td>";
	$ship_stt = 0;
} else if($result['shipping']) {
	echo "<td>Barang tidak dikirim</td>";
	$ship_stt = 1;
} else {
	echo "<td>OK</td>";
	$ship_stt = 1;
}
echo "</tr>";
echo "<td>Data pembayaran</td>";
if($result['payment'] == null) {
	echo "<td>Gagal diinput</td>";
	$pay_stt = 0;
} else {
	echo "<td>OK</td>";
	$pay_stt = 1;
}
echo "</tr>";
echo "<tr>";
echo "<td><input class=\"btn btn-mini btn-primary\" value=\"Reconcile Data\" type=\"button\" onclick=reconcileTrxData('$orderno','$token','$prd_stt','$ship_stt','$pay_stt') /></td><td>&nbsp;</td>";
echo "</tr>";
echo "</table>";
echo "</form>";
echo "<div id=\"recData\"></div>";
backToMainForm();
?>
<script>
function reconcileTrxData(orderno, token, prd_stt, ship_stt, pay_stt) {
	$.ajax({
			url: All.get_url("trans/reconcile/part/save"),
			type: 'POST',
			dataType: "json",
			data:{orderno: orderno, token: token, prd_stt: prd_stt, ship_stt: ship_stt, pay_stt: pay_stt},
			success:
			function(data){
				All.set_enable_button();
				//var response = data.response;
				alert(data.message);
			},
			error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
        });	
}
</script>
<?php if ($result == NULL) {
	echo setErrorMessage();
} else { ?>

<table
    class="table table-bordered bootstrap-datatable datatable"
    width="100%">
    <thead>
        <tr>
            <th colspan="7">List K-Net & Reg. Member</th>
        </tr>
        <tr>
            <th>TRCD</th>
            <th>Delete Order No.</th>
            <th>Order Number</th>
            <th>Created At</th>
            <th>Created By</th>
            <th>Full Name</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
				<?php 
				$i = 1;
				foreach ($result as $key) { 
			if ($key->delete_orderno != NULL) { ?>
        <tr style="background: lightgreen;">
						<td><?php echo $key->trcd; ?></td>
						<input type="hidden" name="va_trcd" id="va_trcd<?php echo $i; ?>" value="<?php echo $key->trcd; ?>" >
            <td><?php echo $key->delete_orderno; ?></td>
            <td style="text-align: center"><button class="btn btn-success" onclick="get('<?php echo $i;  ?>')">Reconcile</button></td>
            <td><?php echo $key->createdt; ?></td>
            <td><?php echo $key->createnm; ?></td>
            <td><?php echo $key->fullnm; ?></td>
            <td style="text-align: right;"><?php echo number_format($key->amount, 2, ".", ","); ?></td>
        </tr>
    <?php } elseif($key->delete_orderno == NULL && $key->order_number == NULL) { ?>
        <tr style="background: #ff7070;">
						<td><?php echo $key->trcd; ?></td>
						<input type="hidden" name="va_trcd" id="va_trcd<?php echo $i; ?>" value="<?php echo $key->trcd; ?>" >
            <td><?php echo $key->delete_orderno; ?></td>
            <td style="text-align: center"><button class="btn btn-danger" onclick="get('<?php echo $i;  ?>')">Reconcile</button></td>
            <td><?php echo $key->createdt; ?></td>
            <td><?php echo $key->createnm; ?></td>
            <td><?php echo $key->fullnm; ?></td>
            <td style="text-align: right;"><?php echo number_format($key->amount, 2, ".", ","); ?></td>
        </tr>
    <?php
			} else { ?>
        <tr style="background: #70a6ff;">
						<td><?php echo $key->trcd; ?></td>
						<input type="hidden" name="va_trcd" id="va_trcd<?php echo $i; ?>" value="<?php echo $key->trcd; ?>" >
						<input type="hidden" name="va_trcd" id="order_number<?php echo $i; ?>" value="<?php echo $key->order_number; ?>" >
            <td style="text-align: center"><button class="btn btn-primary" onclick="get('<?php echo $i;  ?>')">Reconcile</button></td>
            <td><?php echo $key->order_number; ?></td>
            <td><?php echo $key->createdt; ?></td>
            <td><?php echo $key->createnm; ?></td>
            <td><?php echo $key->fullnm; ?></td>
            <td style="text-align: right;"><?php echo number_format($key->amount, 2, ".", ","); ?></td>
        </tr>
				<?php
			}
			?>
				<?php
			$i++;
		}; ?>
    </tbody>
</table>

<script>
function get(param) {
	alert($('#va_trcd' + param).val());
	}

	$(document).ready(function() {
		
		$(All.get_active_tab() + " .datatable").dataTable( { 
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
			"sPaginationType": "bootstrap",
			"oLanguage": {
			},
	        "bDestroy": true
	    });
    	$(All.get_active_tab() + " .datatable").removeAttr('style');
	}); 
</script>

<?php
}
; ?>
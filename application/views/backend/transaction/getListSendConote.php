<div class="mainForm">
  <form class="form-horizontal" id="listSendConote">
    <fieldset>      
      <div class="control-group">       
        
        <label class="control-label" for="typeahead">Click To send Mail</label>                             
        <div class="controls"  id="inp_btn">
        	<input type="text" id="datepick" name="datepick" class="dtpicker" placeholder="Date Pick" value="<?php echo date("Y/m/d"); ?>"/>
        	<!-- input type="text" id="timepick" name="timepick" placeholder="Ex: 20:15" value="10:00"/ -->
        	<select id="jam" name="jam" class="input-small">
        		<?php
        			for ($i=10; $i < 21; $i++) {
        				$jam = substr("0$i", -2, 2);
						echo "<option value=$jam>$jam</option>";
					}
        		?>
			</select>
			<select id="menit" name="menit" class="input-small">
        		<?php
        			for ($i=0; $i < 60; $i++) {
        				$menit = substr("0$i", -2, 2);
						echo "<option value=$menit>$menit</option>";
					}
        		?>
			</select>
			
			<select id="cargo_id" name="cargo_id" class="input-small">
        		<?php
        			if($cargoList > 0) {
						$option = null;
						foreach($cargoList as $data) {
							$option = $option."<option value=$data->shipper_id>$data->shipper_name</option>";
						}	
						echo "$option";
					}
					else {
						 echo "<div class='alert-error'>Empty Record</div>";
					}
        		?>
			</select>
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Send Email to JNE" onclick="be_trans.sendListConote()" />
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="Preview" onclick="All.ajaxFormPost(this.form.id, 'trans/email/conote/list')" />
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="List Pick Up" onclick="All.ajaxFormPost(this.form.id, 'trans/email/pickup/list')" />
            <input type="hidden" name="CNStatus" value="2" />
         </div>

<!--
		<label class="control-label" for="typeahead">Pickup Date</label>

		<div class="controls">
			<input id="from" class="dtpicker hasDatepicker" type="text" required="required" placeholder="Date" name="from">
		</div>
-->
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker();
	});	
	
</script>

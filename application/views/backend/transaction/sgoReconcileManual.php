<?php
if($result == null) {
    setErrorMessage();
} else {
    //echo "beda tempat";
    if($rptType == "1"){ //if rptType = 1, then export to xls
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=ReportReconcileSGO.xls" );
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");
    }

    if($rptType == 1){
        $border = " border = 1";
    }elseif($rptType == 4){
        $border ="";
    }

    echo "<table width=\"100%\" $border class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
    echo "<thead><tr><th colspan=\"10\" bgcolor=\"#lightgrey\">List Mandiri Manual Transaction</th></tr>";
    echo "<tr bgcolor=\"#f4f4f4\">";
    echo "<th >No</th>";
    echo "<th >Order No</th>";
    echo "<th>Member</th>";
    echo "<th >Amount</th>";
    echo "<th >Biaya Kirim</th>";
    echo "<th >Biaya Admin</th>";
    echo "<th >Total Pay</th>";
    echo "<th >Date</th>";
    echo "<th>Status Trx</th>";
    echo "<th>Failed Pay</th>";
    echo "</tr></thead>";
    echo "<tbody>";
    $i = 1;
    //echo $status;
    foreach($result as $list) {
        $dt_trans = date("d-m-Y", strtotime($list->datetrans));

        //$dt_exp = date("Y-m-d H:i:s", strtotime($list->date_expired));
        $pay_dt = date('Y-m-d H:i:s', strtotime('+1 hour', strtotime($dt_trans))); //ditambah 1 jam
        $flag= $list->flag_payment;
        $flag2= $list->flag_production;
        $kd_unik= $list->kode_unik;
        $payadm= $list->payAdm;
        $payship= $list->payShip;

        $kd_pay= $list->kode_pay;
        $nom= $list->total_pay;
        $bank_nm= trim($list->bank_pemilik);

        $nominal= $nom;
        //$rek="1486105171000";
        $rek="1190000879898";
        $date_time= $pay_dt;

        $amount= $nominal;
        $total_pay= $nominal + $payship + $kd_unik;

        if($list->statusX == '1'){
            $desc_trx= "Success";
            $desc_trx2="";
            $color= "#43b90c";
            $href="<a id=\"$list->orderno_prod\" href=\"#\" onclick=\"be_trans.getDetailTrx(this)\">$list->orderno_sgo";
        }elseif($list -> statusX == '2'){
            $desc_trx= "<input type=\"button\" value=\"Reconcile\" id=\"$list->orderno_sgo-$i\" onclick=\"call_webhook($i)\" class='btn btn-success'>";
            $desc_trx2="";
            $color= "";
            $href="$list->orderno_sgo";
        }else{
            $desc_trx= "Menunggu Pembayaran";
            $color= "";
            $desc_trx2= "<input type=\"button\" value=\"Hit Ulang\" id=\"$list->orderno_sgo-$i\" onclick=\"call_webhook($i)\" class='btn btn-danger'>";
            $color2= "";
            $href="$list->orderno_sgo";
        }

        echo "<tr id=\"$i\">";
        //echo "<td><div align=\"center\"><input type=\"checkbox\" id=\"pil$i\" name=\"orderno[]\" value=\"$list->orderno\" /></div></td>";
        echo "<td align=\"right\">$i</td>";
        echo "<td align=\"left\">$href
                	      <input type=\"hidden\" value=\"$list->orderno_sgo\" id=\"$list->orderno_sgo-$i\">
                	      <input type=\"hidden\" value=\"$kd_pay\" id=\"codeA-$i\">
                	      <input type=\"hidden\" value=\"$total_pay\" id=\"codeB-$i\">
                	      <input type=\"hidden\" value=\"$rek\" id=\"codeC-$i\">
                	      <input type=\"hidden\" value=\"$date_time\" id=\"codeD-$i\">
                	      <input type=\"hidden\" value=\"$kd_unik\" id=\"codeE-$i\">
                	      <input type=\"hidden\" value=\"$payship\" id=\"codeF-$i\">
                	      <input type=\"hidden\" value=\"$payadm\" id=\"codeG-$i\">
                	      <input type=\"hidden\" value=\"$bank_nm\" id=\"codeH-$i\">
                	      <input type=\"hidden\" value=\"$nom\" id=\"codeI-$i\">
                	  </td>";
        echo "<td align=\"left\">$list->nmmember</td>";
        echo "<td align=\"right\">".number_format($amount, 0, ",", ".")."</td>";
        echo "<td align=\"right\">".number_format($payship, 0, ",", ".")."</td>";
        echo "<td align=\"right\">".number_format($kd_unik, 0, ",", ".")."</td>";
        echo "<td align=\"right\">".number_format($total_pay, 0, ",", ".")."</td>";
        echo "<td align=\"center\">$dt_trans</td>";

        echo "<td align=\"center\" style='color: $color'>$desc_trx</td>";
        echo "<td align=\"center\" style='color: $color'>$desc_trx2</td>";
        //echo "<td>IDEC = $IDEC <BR / >EC = $EC <BR / >RM = $RM <BR / >E = $E <BR / ></td>";
        //===================end edit hilal 2015-12-07========================


        echo "</tr>";
        $i++;
    }

    echo "</tbody></tr>";

    echo "</table>";
}

?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

    function call_webhook(param){
        var kodepay= document.getElementById("codeA-"+param).value;
        var nominal= document.getElementById("codeB-"+param).value;
        var rek= document.getElementById("codeC-"+param).value;
        var datepay= document.getElementById("codeD-"+param).value;
        var kodeunik= document.getElementById("codeE-"+param).value;
        var payship= document.getElementById("codeF-"+param).value;
        var payadm= document.getElementById("codeG-"+param).value;
        var nm_rek= document.getElementById("codeH-"+param).value;


        var nom_belanja= document.getElementById("codeI-"+param).value;

        /*alert (kodepay);
        alert (nominal);
        alert (rek);
        alert (datepay);*/

        $.ajax({
            dataType: 'json',
            type: "POST",
            url : All.get_url("trans/sgo/reconcile/postmanual"),
            data: {kd_pay: kodepay,tot_bayar: nominal,norek: rek,date_time: datepay},
            success: function(data)
            {
                alert(data.message);
            }
        });

        /*$.ajax({
            url: All.get_url('getValidate'),
            type: 'POST',
            dataType: 'json',
            data: {
                token:'caf428e83bd921a5309bb17cd43e4675bdf55d7aeeb43146b5c5ba927ddaaaee75744ac1dc3c9ad53cff4790f22f401c57793b105daa3a86a6789f96d6a43487W0.bFNrJ53h9h3~yvTZDtpjvQAg4D4SK7QarFQY-',
                //token:'f84ce6c033b9b1a7ed0e841cdfc14069524289b085ef208ba7bad3af7dc96208a9a9c4939e918de26345007f8836605e9a64075f65f24b2b7b8fede74a23a13eT0jgX~WXp9smmbQrsHUC3WoylGB.Q4hSAzeQiQ8-',
                nominal_belanja: nom_belanja,
                pay_ship: payship,
                kode_unik:kodeunik,
                kd_pay:kodepay,
                total_bayar:nominal,
                no_rekening: rek,
                nama_rekening: nm_rek,
                type:'Cr',
            },
            success:
                function(result){
                    var status = result.status;

                    console.log(result);
                    if(result.error == "false" && result.status == "true"){
                        $.ajax({
                            dataType: 'json',
                            type: "POST",
                            url : All.get_url("trans/sgo/reconcile/postmanual"),
                            data: {kd_pay: kodepay,tot_bayar: nominal,norek: rek,date_time: datepay},
                            success: function(data)
                            {
                                alert(data.message);
                            }
                        });
                    }

                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                $("input[type=button]").removeAttr('disabled');
            }
        });*/


    }
</script>

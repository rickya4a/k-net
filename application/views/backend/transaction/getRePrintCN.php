<div class="mainForm">
  <form class="form-horizontal" id="reprintCN" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" >
    <fieldset>      
      <div class="control-group">       
        <?php
          echo datepickerFromTo("Transaction Date", "cn_from", "cn_to");
        ?>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans.getListCN()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input type="hidden" name="CNStatus" value="2" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

<div class="mainForm">
    <form class="form-horizontal" id="frmUpdt" >
        <fieldset>
        	 <label class="control-label" for="typeahead">Tipe</label>
        	<div class="controls">
        	<select id="trx_type" name="trx_type" id="trx_type" class="span4">
        		<option value="rekap_kirim">List Shipping Branch Warehouse</option>
        		<option value="null_resi_fisik">Resi Fisik belum diupdate WH</option>
        		<option value="null_econote">E-conote Kosong</option>
						<option value="data_shipping_not_complete">E-Connote Kosong dan Data Shipping Tidak Lengkap</option>
        	</select>
            </div>

                <?php
                  echo datepickerFromTo("Transaction Date", "dtfroms", "dttos");
                ?>

            <label class="control-label" for="typeahead">&nbsp</label>
            <div class="controls"  id="inp_btn">
                <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'trans/jne/getListNoConot')" />
                <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
             </div>
        </fieldset>
    </form>
    <div class="result"></div>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker("setDate", new Date());;
  });
</script>
<?php
    if(empty($listnoConot)){
        echo setErrorMessage();
    }else{
        
?>

<div class="overflow-auto">
   <!-- <form id="summPromo" method="post" >-->
        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <tr >
                <th colspan="9" >List E-Conote yang kosong</th>
             </tr>
                <tr>
                    <th>No</th>
                    <th>Orderno</th>
					<th>Token</th>
                    <th>Cargo</th>
                    <th>Resi Fisik</th>
                    <th>ID Member</th>
                    <th>Trx Date</th>
                    <th>Total Pay</th>
                    <!--<th>Qty Product</th>-->
                    <th>Total Weight</th>
                    <th>&nbsp;Act<a ></a></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
					//print_r($listnoConot);
                    foreach($listnoConot as $row){
                        $cargo= $row->cargo_id;
                        if($row->servicetypeID != null){
                            $type = '1';
                        }else{
                            $type = '0';
                        }
                        
                        echo "<tr>
                                <td align='center'>&nbsp;".$no."</td>
                                <td>&nbsp;<a id=\"$row->orderno\" onclick=\"javascript:be_trans.getDetailTrx(this)\" href=\"#\">$row->orderno</a></td>";
								echo "<td>&nbsp;".$row->token."</td>";
								echo "<td><select style=\"width: 70px;\" id=\"pilcargo\" name=\"pilcargo\" onchange=\"updateCargo('$no',this.value)\">";
								foreach($listCargo as $dtx) {
									$selected = "";
									if($dtx->shipper_id == $row->cargo_id) {
										$selected = "selected=selected";
									} 
									echo "<option value=\"$dtx->shipper_id\" $selected>$dtx->shipper_code</option>";
								}
								echo "<input type=hidden id=orderno$no value=\"$row->orderno\" />";
								echo "</select></td>";
                                
                                echo "<td>&nbsp;".$row->conote_new."</td>
                                <td>&nbsp;".$row->id_memb."</td>
                                <td align='center'>&nbsp;".date('d-m-Y',strtotime($row->datetrans))."</td>
                                <td align='right'>&nbsp;".number_format($row->total_pay,0,'.','.')."</td> ";
                                //<td align='right'>&nbsp;".number_format($row->total_Items,0,'.','.')."</td>
                                echo "<td align='right'>&nbsp;".$row->total_weight."</td>
                                <td align='center'>";
                                if($row->orderno_ship == null) {
                                	echo "<a class=\"btn btn-mini btn-danger\" onclick=\"All.ajaxShowDetailonNextForm('trans/shipaddr/check/".$row->orderno."/".$row->token."')\"  title=\"Reconcile Shipping Data\">
                                    <i class=\"icon-plus icon-white\"></i></a>&nbsp;";
                                }
                                    echo "<a class=\"btn btn-mini btn-info\" onclick=\"be_trans.setUpdtEConot('$row->orderno','$type','$row->token','$cargo')\" title=\"Update Conot\">
                                    <i class=\"icon-edit icon-white\"></i></a>
                                </td>                           
                             </tr>";
                             $no++;
                    }
					/*echo "<tr>
		                  	<td colspan=\"4\" align='center'>Total</td>
                            <td align='center'>$totQty</td>   
		                  </tr>";*/
                ?>
                <!--<tr>
                <td colspan="6"></td>
                </tr>-->
            </tbody>
        </table>
        <!--<input type="button" class="btn btn-primary" onClick="be_trans.getGroupDO(); return false;" name="submit" value="Preview" id="checkss"/>-->
        
    <!--</form>-->
    <br />
</div>
<?php } ?>
<script type="text/javascript">
$(document).ready(function() 
{
	All.set_datatable();
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

 function updateCargo(param, pilihan) {
 	var order = $(All.get_active_tab() + " #orderno" +param).val();
 	//var pilcargo = $(All.get_active_tab() + " #pilcargo").val();
 	//alert("Isi orderno : " +order+ " cargo : " +pilihan);
 	
 	$.ajax({
        url: All.get_url('trans/cargo/upd/') + order + "/" +pilihan,
        type: 'GET',
		dataType: 'json',
        success:
        function(data){
        	alert(data.message);	
        },
        error: function (xhr, ajaxOptions, thrownError) {
             alert(thrownError + ':' +xhr.status);
			 $("input[type=button]").removeAttr('disabled');
        }
    });		
 }
</script>
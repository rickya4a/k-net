<?php
$today= date('Ymdhis');
$filename=$today."_xls_product_sales_summary.xls";

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$filename);
header("Pragma: no-cache");
header("Expires: 0");
?>

<table width="80%" border="1">
    <tr>
        <td height="80" colspan="4" align="center" valign="top"><strong>LIST PRODUCT SALES SUMMARY </strong><br />Tanggal Transaksi: <?php echo $dtfrom?> - <?php echo $dtto?></td>
    </tr>
    <tr align="center" bgcolor="#FFFFFF">
        <td width="5%">NO</td>
        <td width="15%">CODE PRODUCT </td>
        <td width="50%">NAME PRODUCT </td>
        <td width="10%">QTY</td>
    </tr>
    <?php
    $no = 1;
    foreach ($printSP as $r) {
        $now = gmdate("d-m-Y", time() + 60 * 60 * 5.6);
        $kode = $r->no_code;
        $nama = $r->name_code;
        $qty = $r->tot_qty;
        $nval[]=$qty;
        /* $total=  * $qty;
         $ntotal= number_format($total,0,',','.');*/

        ?>
        <tr>
            <td align="center"><?php echo $no++; ?></td>
            <td><?php echo $kode ?></td>
            <td><?php echo $nama ?></td>
            <td align="center"><?php echo $qty ?></td>
        </tr>
        <?php
    }

    $total= array_sum($nval);
    ?>
    <tr>
        <td colspan="3" align="right"><strong>*TOTAL QTY</strong> </td>
        <td align="center"><?php echo number_format($total,0,',','.')?></td>
    </tr>
</table>


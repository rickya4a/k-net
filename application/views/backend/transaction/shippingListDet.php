<?php
	if($result == null) {
		echo setErrorMessage("No result found");
	}else{
?>
<form>
<table width="80%" class="table table-bordered table-striped">
	<tr>
		<th colspan="2">Detail Shipping Info</th>
	</tr>
	<tr>
		<td width="30%">Order No</td>
		<td><?php echo $result[0]->orderno; ?></td>
	</tr>
	<tr>
		<td>Trx Date</td>
		<td><?php echo $result[0]->datetrans; ?></td>
	</tr>
	<tr>
		<td>Member</td>
		<td><?php echo $result[0]->id_memb." / ".$result[0]->nmmember; ?></td>
	</tr>
	<tr>
		<td>Stockist Info</td>
		<td><?php echo $result[0]->nmstkk; ?></td>
	</tr>
	<tr>
		<td>Free Shipping</td>
		<td><?php echo $result[0]->free_shipping; ?></td>
	</tr>
	<tr>
		<td>Shipping Cost</td>
		<td><?php echo $result[0]->payShip; ?></td>
	</tr>
	<tr>
		<td>Conote</td>
		<td><?php echo $result[0]->conote_new; ?></td>
	</tr>
	<tr>
		<td>Service Type</td>
		<td><?php echo $result[0]->service_type_name; ?></td>
	</tr>
	<tr>
		<td>Shipping Cost</td>
		<td><?php echo $result[0]->payShip; ?></td>
	</tr>
	<tr>
		<td>Receiver Name</td>
		<td><?php echo $result[0]->receiver_name; ?></td>
	</tr>
	<tr>
		<td>Address</td>
		<td><?php echo $result[0]->addr1; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" class="btn btn-warning" value="<< Back" onclick="All.back_to_form(' .nextForm1',' .mainForm')" />
		</td>
	</tr>
</table>
</form>
<?php
    }
?>
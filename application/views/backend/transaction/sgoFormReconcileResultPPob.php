<?php
 if($result == null) {
 	setErrorMessage();
 } else {

	if($rptType == "1"){ //if rptType = 1, then export to xls
		header("Content-type: application/vnd.ms-excel");
	    header("Content-Disposition: attachment; filename=ReportReconcileSGO.xls" );
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	    header("Pragma: public");
	}

 	   if($rptType == 1){
 	   		$border = " border = 1";
	   }elseif($rptType == 0){
	   		$border ="";
	   }

 	   echo "<table width=\"100%\" $border class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=\"10\" bgcolor=\"#lightgrey\">List SGO Transaction</th></tr>";
       echo "<tr bgcolor=\"#f4f4f4\">";
       echo "<th width=\"5%\">No</th>";
       echo "<th width=\"15%\">Order No</th>";
       echo "<th>Member</th>";
       echo "<th width=\"10%\">Amount</th>";
	   echo "<th width=\"15%\">Date</th>";
	   echo "<th width=\"8%\">Status SGO</th>";
		 echo "<th width=\"8%\">Status Trx</th>";
		 echo "<th width=\"8%\">No. HP</th>";
	   //echo "<th width=\"8%\">Status Memb</th>";
		 echo "<th width=\"8%\">Bank</th>";
		 echo "<th>Reconcile Trx</ht>";
       echo "</tr></thead>";
       echo "<tbody>";
       $i = 1;
	   //echo $status;
       foreach($result as $list) {
				$status_sgo = $list->Status_sgo;
				$status_trx = $list->status_trx;

                echo "<tr id=\"$i\">";
		        //echo "<td><div align=\"center\"><input type=\"checkbox\" id=\"pil$i\" name=\"orderno[]\" value=\"$list->orderno\" /></div></td>";
                echo "<td align=\"right\">$i</td>";
                echo "<td align=\"left\">
					  <a href='#' id='$list->Order_ID' onclick='javascript:be_voucher.getDetailPPOB(this)'>$list->Order_ID</a>
					  <!---<a href='#' id='$list->Order_ID' onclick=All.ajaxShowDetailonNextForm('backend/transaction/getDetTrxXL/$list->Order_ID')>$list->Order_ID</a>-->
                	  <input class=\"orderid\" type=\"hidden\" value=\"$list->Order_ID\" id=\"$list->Order_ID-$i\">
                	  </td>";
                echo "<td align=\"left\">$list->id_memb - $list->nmmember</td>";

				if($rptType == "1"){
					$amt = number_format($list->Amount, 0, ",", "");
				}else{
					$amt = number_format($list->Amount, 0, ",", ".");
				}

                echo "<td align=\"right\">".$amt."</td>";
				echo "<td align=\"center\">$list->Transaction_Datex</td>";





				if($status_sgo == "Failed"){
					$actSGO = "<font color=\"red\"> Failed </font>";
					$alignSGO = "left";
					$classSGO = "class=\"text-danger\"";
				}elseif($status_sgo == "Suspect"){
					$actSGO = "<font color=\"blue\"> Suspect </font>";
					$alignSGO = "left";
					$classSGO = "";
				}elseif($status_sgo == "Success"){
					$actSGO = "<font color=\"#7B823E\"> Success </font>";
					$alignSGO = "left";
					$classSGO = "";
				} else {
					$actSGO = "<font color=\"#7B823E\"> Trx Aneh </font>";
					$alignSGO = "left";
					$classSGO = "";
				}
				echo "<td align=\"$alignSGO\">$actSGO </td>";

				$align = "left";
				if($status_trx == 2){
					$act = "<font color=\"red\"> Failed </font>";
					$align = "left";
					$class = "class=\"text-danger\"";
				}elseif($status_sgo == "Suspect" && $status_trx == 3){
					/*$act = "<input type=\"button\" class=\"btn btn-mini btn-danger\" value=\"Reconcile(SP)\"
								   onclick=\"javascript:All.deleteData('trans/sgo/reconcile/save/$list->Order_ID','formReconcileSGO','trans/sgo/reconcile/list/0')\"
						    />"; */
					$act = "<input type=\"button\" class=\"btn btn-mini btn-primary\" value=\"Reconcile\" onclick=\"be_trans.reconcileXlPaketData('$list->Order_ID')\"

						    />";
					$align = "center";
					$class = "";
				}elseif($status_sgo == "Suspect" && $status_trx == 1){
					$act = "<font color=\"#7B823E\"> Success </font>";
					$align = "left";
					$class = "";
				}elseif($status_sgo == "Success" && $status_trx == 1){
					$act = "<font color=\"#7B823E\"> Success </font>";
					$align = "left";
					$class = "";
				}elseif($status_trx == 0 ){
				    if($list->hdr_trx_id != '0' && $list->det_trx_id != '0' && $list->pay_trx_id != '0') {
					$act = "<input type=\"button\" class=\"btn btn-mini btn-primary\" value=\"Reconcile\" onclick=\"be_trans.reconcileXlPaketData('$list->Order_ID')\"

						    />";
							//onclick=\"javascript:All.deleteData('trans/sgo/reconcile/save/$list->Order_ID','formReconcileSGO','trans/sgo/reconcile/list/0')\"
					$align = "center";
					$class = "";
					} else {
					  $act = "<input type=\"button\" class=\"btn btn-mini btn-warning\" value=\"Check\"

						    />";
					}
				}else{
					echo $status_sgo."<br \>".$status_trx;
				}
				echo "<td id=\"$list->Order_ID\" align=\"$align\">$act </td>";


				if($status_sgo == "Failed"){
					$actSGO = "<font color=\"red\"> Failed </font>";
					$alignSGO = "left";
					$classSGO = "class=\"text-danger\"";
				}elseif($status_sgo == "Suspect"){
					$actSGO = "<font color=\"blue\"> Suspect </font>";
					$alignSGO = "left";
					$classSGO = "";
				}elseif($status_sgo == "Success"){
					$actSGO = "<font color=\"#7B823E\"> Success </font>";
					$alignSGO = "left";
					$classSGO = "";
				}


				echo "<td>".$list->cust_no."</td>";
				echo "<td>".$list->bankDesc."</td>";
				if ($status_trx == 3 || $status_trx == 0) { ?>
				<td><input type="button" class="btn btn-mini btn-success" onclick="reconcile()" value="Recon Trx"></td>
				<?php
				} else { ?>
					<td>&nbsp;</td>
				<?php }
				//echo "<td>IDEC = $IDEC <BR / >EC = $EC <BR / >RM = $RM <BR / >E = $E <BR / ></td>";
				//===================end edit hilal 2015-12-07========================


                echo "</tr>";
              $i++;
        }

    echo "</tbody></tr>";

    echo "</table>";
 }

?>
<script type="text/javascript">
$(document).ready(function()
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');


 });

</script>

<script>
	function reconcile() {
		var orderid = $('.orderid').val()
		$.ajax({
			type: "POST",
			url: All.get_url('xl/recon/trx_only'),
			dataType: "json",
			data: { orderid: orderid },
			success: function (data) {
				alert(data.message);
				console.log(data)
				All.set_enable_button();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	}
</script>

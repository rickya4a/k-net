<style>
	button {
		margin-top: 2px;
	}
</style>
<?php
//echo "<h2>BELOM JADI..MSH DI KEMBANGKAN</h2>";
?>
<div class="mainForm">
  <!--<form class="form-horizontal" id="formPrintShippingLabel" method="POST" action="<?php echo $printtxt; ?>" target="_BLANK" > -->
  <form class="form-horizontal" id="formUpdResiFisik" method="POST" onsubmit="return All.checkMultipleCheckbox('orderno[]')" >
    <fieldset>      
      <div class="control-group">       
       <label class="control-label" for="typeahead">Transaction Date</label>
         <div class="controls">
           <input type="text" class="dtpicker typeahead" id="prtx_form" name="prt_form" >&nbsp;to&nbsp;
		   <input type="text"  class="dtpicker typeahead" id="prtx_to" name="prt_to" >
		 </div>
	   <label class="control-label" for="typeahead">Print Status</label>
         <div class="controls">
            <select id="print_count" name="print_count" class="span4">
            	<option value="1">Reprint</option>
				<option value="0">New Order</option>
            </select>
		 </div>
       <label class="control-label" for="typeahead">Status Resi Fisik</label>
         <div class="controls">
            <select id="stt_resi_fisik" name="stt_resi_fisik" class="span4">
            	<option value="0">Belum di update</option>
				<option value="1">Sudah di update</option>
            </select>
		 </div>		  
	   <label class="control-label" for="typeahead">&nbsp;</label>
         <div class="controls">	  
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'conote/update/list')" />
            
            
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
            
          </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());;
	});
	
	function processUpdate() {
		All.set_disable_button();
		$.ajax({
            url: All.get_url('conote/update/save') ,
            type: 'POST',
			data: $(All.get_active_tab() + ' #formUpdResiFisik').serialize(),
            success:
            function(data) {
			    All.set_enable_button();
                $(All.get_active_tab() + " .result").html(null);
				$(All.get_active_tab() + " .result").html(data);   
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	}
	
	/*function checkValidationShipping() {
		var atLeastOneIsChecked = $('input[name="orderno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			alert("Please select at least one transaction..");
			return false;
		} 
	}*/
</script>

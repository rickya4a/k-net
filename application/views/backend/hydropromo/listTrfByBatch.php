<?php
if($listTrf == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
	echo "<form>";
		
	    echo "<table width='100%' class='table table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"9\">Detail Batch Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		//echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		echo "<th width='3%'>No</th>";
		//echo "<th>Member</th>";
		echo "<th width='5%'>Bank</th>";
		echo "<th width='15%'>No Rek</th>";
		echo "<th width='20%'>Pemilik Rek</th>";
		echo "<th>ID Member</th>";
		echo "<th>Nama Member</th>";
		echo "<th width='6%'>Cashback</th>";
		echo "<th width='8%'>Status</th>";
		echo "<th width='10%'>Alasan ditolak</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($listTrf as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			$warna = "";
			if($data->status == "1") {
			  $stt = "Proses";
			} else if($data->status == "2") { 
			  $warna = "bgcolor=lightgreen";
			  $stt = "Tolak";
			} else if($data->status == "3"){
			  $stt = "Trf OK";	
			} else {
			  $stt = "Belum Diproses";
			}
			
			echo "<tr $warna>";
			//echo "<td align=center><input type=checkbox id=pil$i name=trcd[] value=\"$data->dfno\" /></td>";
			echo "<td align=right>$i</td>";
			//echo "<td align=center><a id=\"$data->CNno\" onclick=\"javascript:be_trans.getDetailShipProduct(this)\">$data->CNno</a></td>";
			//echo "<td align=center>$data->dfno / $data->fullnm</td>";
			echo "<td align=center>$data->bank</td>";
			echo "<td align=center>$data->bank_acc</td>";
			echo "<td align=center>$data->bank_accname</td>";
			echo "<td align=center>$data->dfno</td>";
			echo "<td align=center>$data->fullnm</td>"; 
			echo "<td align=right>".number_format($data->total_cashback, 0, ",", ".")."</td>";
			echo "<td align=center>$stt";
			echo "<td align=center>$data->remarkTolakan</td>"; 
			echo"</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table></form>";
		echo "<input type=\"button\" class='btn btn-warning' value=\"<< Kembali\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" />";
}
?>

<?php
if($rekap == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
	echo "<form id=\"processBatch\">
	    <input type=\"hidden\" name=\"listTrx\" value=\"$listTrx\" />
		<input type=\"button\" class='btn btn-warning' value=\"<< Kembali\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" />&nbsp;
		<input type=button name=\"proses_batch\" value=\"Generate Batch Transfer\" onclick=\"processPreviewBatch()\" class='btn btn-primary' />
	    <table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"6\">Preview Summary Batch Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		//echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		echo "<th width='5%'>No</th>";
		echo "<th>Member</th>";
		echo "<th width='10%'>Bank</th>";
		echo "<th width='15%'>No Rek</th>";
		echo "<th width='20%'>Pemilik Rek</th>";
		//echo "<th width='8%'>Cashback</th>";
		//echo "<th width='8%'>Pajak</th>";
		//echo "<th width='5%'>Biaya Bank</th>";
		echo "<th width='8%'>Transfer Cashback</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($rekap as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			
			echo "<tr>";
			//echo "<td align=center><input type=checkbox id=pil$i name=trcd[] value=\"$data->trcd\" /></td>";
			echo "<td align=right>$i</td>";
			//echo "<td align=center><a id=\"$data->CNno\" onclick=\"javascript:be_trans.getDetailShipProduct(this)\">$data->CNno</a></td>";
			echo "<td align=center>$data->dfno / $data->fullnm</td>";
			echo "<td align=center>$data->bank</td>";
			echo "<td align=center>$data->bank_acc</td>";
			echo "<td align=center>$data->bank_accname</td>";
			//echo "<td align=right>".number_format($data->total_cashback, 0, ",", ".")."</td>";
			//echo "<td align=center>$data->proc_tax %</td>";
			//echo "<td align=right>".number_format($data->biaya_bank, 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($data->cashback_setelah_pajak, 0, ",", ".")."</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table></form>";
		echo "<input type=\"button\" class='btn btn-warning' value=\"<< Kembali\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\" />&nbsp;<input type=button name=\"proses_batch\" value=\"Generate Batch Transfer\" onclick=\"processPreviewBatch()\" class='btn btn-primary' />";
}
?>

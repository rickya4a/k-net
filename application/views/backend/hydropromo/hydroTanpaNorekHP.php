<?php
if($result == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
    $url_export_excel = base_url('cashback/hydrowater/norekhp/excel');
	echo "
	    <input type=\"submit\" value=\"Export ke Excel\" class=\"btn btn-success\" formaction=\"$url_export_excel\" />
	    <table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"8\">List Belum Ditransfer / Tolakan Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		//echo "<th width='5%'>No</th>";
		echo "<th width='10%'>No CN</th>";
		echo "<th width='5%'>Bank</th>";
		echo "<th width='15%'>No Rek</th>";
		echo "<th>Pemilik Rek</th>";
		echo "<th width='10%'>No HP</th>";
		echo "<th>ID Member</th>";
		echo "<th>Nama Member</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($result as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			
			echo "<tr>";
			echo "<td align=center><input type=checkbox id=pil$i name=trcd[] value=\"$data->trcd\" /></td>";
			//echo "<td align=right>$i</td>";
			echo "<td align=center><a id=\"$data->CNno\" onclick=\"javascript:be_trans.getDetailShipProduct(this)\">$data->CNno</a></td>";
			echo "<td align=center>$data->bank</td>";
			echo "<td align=center>$data->bank_acc</td>";
			echo "<td align=left>$data->bank_accname</td>";
			echo "<td align=left>$data->no_telp</td>";
			echo "<td align=center>$data->dfno</td>";
			echo "<td align=left>$data->fullnm</td>"; 
			//echo "<td align=right>$data->nominal_cashback</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
}
?>
<!--
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[100, -1], [100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>-->
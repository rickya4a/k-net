<?php
if($res == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
    //print_r($res);
	/*echo "<input  type=submit value=\"Export To Excel\" class='btn btn-success' onclick=\"javascript: form.action='".base_url('cashback/hydrowater/batch/print')."';\" />&nbsp;";
	echo "<input  type=button value=\"Transfer Selesai\" class='btn btn-primary' onclick=\"TransferOK()\" />
	    &nbsp;Tgl Transfer&nbsp;<input type=\"text\" class=\"dtpicker\" id=\"transferdt\" name=\"transferdt\" value=\"".date('Y-m-d')."\" />";*/
	    echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"8\">List Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		//echo "<th width='5%'>Total Trx</th>";
		echo "<th>No Batch Transfer</th>";
		echo "<th>Status Transfer</th>";
		echo "<th width='10%'>Tgl</th>";
		echo "<th width='10%'>Cashback</th>";
		echo "<th width='8%'>Tax</th>";
		echo "<th width='8%'>Admin Bank</th>";
		echo "<th width='10%'>Total Transfer</th>";
		//echo "<th width='8%'>SMS</th>";
		echo "<th width='5%'>Detail</th>";
		//echo "<th width='6%'>Transfer OK</th>";
		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($res as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			
			echo "<tr id=\"$data->BatchProses\">";
			//echo "<td align=center>$data->tot_trx</td>";
			echo "<td align=center>$data->BatchProses</td>";
			echo "<td align=center>$data->status_transfer</td>";
			echo "<td align=center>$data->processdt</td>";
			echo "<td align=right>".number_format($data->nom_cashback, 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($data->potongan_tax, 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($data->biaya_bank, 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($data->nett_cashback, 0, ",", ".")."</td>";
			//echo "<td align=center>$data->sms_status</td>";
			$url = "";
			if($data->status_transfer == "Pending") {
				$url = 'cashback/hydrowater/pending/'.$data->dfno;
				$urlx = base_url($url);
			} else if($data->status_transfer == "Transfer To Account" || $data->status_transfer == "Process" || $data->status_transfer == "Transfer Rejected") {
				$url = 'cashback/hydrowater/trfprocess/'.$data->BatchProses;
			} 
			echo "<td align=center><a onclick=\"All.ajaxShowDetailonNextForm('$url')\" class='btn btn-mini btn-success'>Detail</a></td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
}
?>
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });
 

</script>

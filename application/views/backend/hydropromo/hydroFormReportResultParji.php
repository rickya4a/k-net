<?php
if($res == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
    //print_r($res);
	/*echo "<input  type=submit value=\"Export To Excel\" class='btn btn-success' onclick=\"javascript: form.action='".base_url('cashback/hydrowater/batch/print')."';\" />&nbsp;";
	echo "<input  type=button value=\"Transfer Selesai\" class='btn btn-primary' onclick=\"TransferOK()\" />
	    &nbsp;Tgl Transfer&nbsp;<input type=\"text\" class=\"dtpicker\" id=\"transferdt\" name=\"transferdt\" value=\"".date('Y-m-d')."\" />";*/
	    echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"6\">List Transaksi Hydro</th>";
		echo "</tr>";
		echo "<tr>";
		//echo "<th width='5%'>Total Trx</th>";
		echo "<th>No Batch Transfer</th>";
		echo "<th>No TTP/Trx</th>";
		echo "<th>No CN/MS/Invoice</th>";
		echo "<th>Bank</th>";
		echo "<th>No HP</th>";
		echo "<th>Status</th>";
		//echo "<th width='6%'>Transfer OK</th>";
		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($res as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			
			echo "<tr id=\"$data->BatchProses\">";
			//echo "<td align=center>$data->tot_trx</td>";
			echo "<td align=center>$data->BatchProses</td>";
			echo "<td align=center>$data->ttp_no</td>";
			echo "<td align=center>$data->CNno</td>";
			echo "<td align=center>$data->bank $data->bank_acc</td>";
			echo "<td align=center>$data->no_telp</td>";
			if($data->status != "3") {
				echo "<td align=center>$data->status_name</td>";
			} else {
				echo "<td align=center>SDH TRF $data->transferdt</td>";
			}
			//echo "<td align=center><a onclick=\"All.ajaxShowDetailonNextForm('$url')\" class='btn btn-mini btn-success'>Detail</a></td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
}
?>
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });
 

</script>

<?php
if($result == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
	echo "<input type=button name=\"proses_batch\" value=\"Proses Batch Transfer\" onclick=\"processBatch()\" class='btn btn-primary' />
	&nbsp; <font color='blue'>(Warna Biru : Status Transfer Ditolak)</font>
	    <table width='100%' class='table table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"12\">List Belum Ditransfer / Tolakan Transfer</th>";
		echo "</tr>";
		echo "<tr>";
		echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		//echo "<th width='5%'>No</th>";
		echo "<th width='10%'>No CN</th>";
		echo "<th width='5%'>Bank</th>";
		echo "<th width='15%'>No Rek</th>";
		echo "<th>Pemilik Rek</th>";
		echo "<th>ID Member</th>";
		echo "<th>Nama Member</th>";
		echo "<th width='8%'>Cashback</th>";
		echo "<th>% Pajak</th>";
		echo "<th>Pajak dlm Rupiah</th>";
		echo "<th>Biaya Bank</th>";
		echo "<th>Cashback Setelah Pajak</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($result as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			
			$sd = "";
			if($data->status == "2") {
			   $sd = "bgcolor='lightblue'";
			}
			
			echo "<tr $sd>";
			echo "<td align=center><input type=checkbox id=pil$i name=trcd[] value=\"$data->trcd\" /></td>";
			//echo "<td align=right>$i</td>";
			echo "<td align=center><a id=\"$data->CNno\" onclick=\"javascript:be_trans.getDetailShipProduct(this)\">$data->CNno</a></td>";
			echo "<td align=center>$data->bank</td>";
			echo "<td align=center>$data->bank_acc</td>";
			echo "<td align=left>$data->bank_accname</td>";
			echo "<td align=center>$data->dfno</td>";
			echo "<td align=left>$data->fullnm</td>"; 
			echo "<td align=right>".number_format($data->cashback_sblm_pajak, 0, ",", ".")."</td>";
			echo "<td align=right>".$data->proc_tax."%</td>";
			echo "<td align=right>".number_format($data->potongan_tax, 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($data->biaya_bank, 0, ",", ".")."</td>";
			echo "<td align=right>".number_format($data->nominal_cashback, 0, ",", ".")."</td>";
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
}
?>
<!--
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[100, -1], [100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>-->
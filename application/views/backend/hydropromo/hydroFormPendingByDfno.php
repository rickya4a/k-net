<?php
if($res == null) {
	echo setErrorMessage("Data tidak ditemukan..");
} else {
	/*echo "<input  type=submit value=\"Export To Excel\" class='btn btn-success' onclick=\"javascript: form.action='".base_url('cashback/hydrowater/batch/print')."';\" />&nbsp;";
	echo "<input  type=button value=\"Transfer Selesai\" class='btn btn-primary' onclick=\"TransferOK()\" />
	    &nbsp;Tgl Transfer&nbsp;<input type=\"text\" class=\"dtpicker\" id=\"transferdt\" name=\"transferdt\" value=\"".date('Y-m-d')."\" />";*/
	    echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th colspan=\"10\">List Cashback</th>";
		echo "</tr>";
		echo "<tr>";
		echo "<th width='5%'>No</th>";
		echo "<th>No Trx</th>";
		echo "<th>Cashback</th>";
		echo "<th>No Rek</th>";
		echo "<th>No HP</th>";
		
		//echo "<th width='6%'>Transfer OK</th>";
		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$i = 1;
		foreach($res as $data) {
			//echo "CONOTE = $data->conote_new<BR/>";
			
			echo "<tr>";
			echo "<td align=right>$i</td>";
			echo "<td align=center>$data->CNno</td>";
			
			echo "<td align=right>".number_format($data->nominal_cashback, 0, ",", ".")."</td>";
			if($data->bank_acc == null || $data->bank_acc == "") {
			  $rek = "BLM ADA";
			} else {
			  $rek = $data->bank_acc; 
			}
			echo "<td align=center>$rek</td>";
			
			if($data->no_telp == null || $data->no_telp == "") {
			  $rek = "BLM ADA";
			} else {
			  $rek = $data->no_telp; 
			}
			echo "<td align=center>$data->no_telp</td>";
			
			echo "</tr>";
			$i++;
		}
		echo "</tbody>";
		echo "</table>";
		echo backToMainForm();
}
?>
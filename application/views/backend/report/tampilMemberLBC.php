<?php

    //print_r($tbl_lbc);
    if(empty($tbl_lbc)){

//        setErrorMessage();
    }else {

        $no = 1;
        foreach ($tbl_lbc as $x) {

            $dfno = $x->dfno;
            $fullnm = $x->fullnm;
            $alamat1 = $x->addr1;
            $alamat2 = $x->addr2;
            $alamat3 = $x->addr3;
            $regis_date = $x->register_dt;
            $expired_date = $x->expired_dt;
            $rank = $x->rank;
            $rank_desc = $x->rank_desc;
            $status = $x->STATUS;
            $birthdt = $x->birthdt;
            $reprint = $x->reprint;
            $email = $x->email;

            $tel_hp = $x->tel_hp;
            $tel_hm = $x->tel_hm;
            $no++;}
        ?>

        <table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable"
<tr>
    <td></td>
</tr>
<tr>
    <th scope="row">ID Member LBC</th>
    <td><?php echo $dfno;?></td>
</tr>
<tr>
    <th scope="row">Nama</th>
    <td><?php echo $fullnm;?></td>
</tr>
<tr>
    <th scope="row">Alamat</th>
    <td><?php
        if($alamat1 != null){
            echo $alamat1;
        }else{
            echo "-";
        }
        ?></td>
</tr>
<tr>
    <th scope="row"></th>
    <td><?php
        if($alamat1 != null){
            echo $alamat2;
        }else{
            echo "-";
        }
        ?></td>
</tr>
<tr>
    <th scope="row"></th>
    <td><?php
        if($alamat1 != null){
            echo $alamat3;
        }else{
            echo "-";
        }
        ?></td>
</tr>
<tr>
    <th scope="row">Register Date</th>
    <td><?php echo date("d-m-Y", strtotime($regis_date));?></td>
</tr>
<tr>
    <th scope="row">Expired Date</th>
    <td><?php echo date("d-m-Y", strtotime($expired_date));?></td>
</tr>
<tr>
    <th scope="row">Ranking</th>
    <td><?php echo $rank." - ".$rank_desc;?></td>
</tr>
<tr>
    <th scope="row">Status Keanggotaan</th>
    <td><?php if($status = '0'){
            echo "tidak aktif";
        }else{
            echo "aktif";
        }
        ;?></td>
</tr>
<tr>
    <th scope="row">Tgl Lahir</th>
    <td><?php echo date("d-m-Y", strtotime($birthdt));?></td>
</tr>
<tr>
    <th scope="row">Reprint Card</th>
    <td><?php echo $reprint;?></td>
</tr>
<tr>
    <th scope="row">Email</th>
    <td><?php echo $email;?></td>
</tr>
<tr>
    <th scope="row">No. HP</th>
    <td><?php echo $tel_hp;?></td>
</tr>
<tr>
    <th scope="row">Telpon Rumah</th>
    <td><?php echo $tel_hm;?></td>
</tr>

</table>


        <?php
        }

    if(empty($detail)){

        setErrorMessage();
    }else{
?>

        <table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr>
        	<th colspan="10">Detail Data Member LBC</th>
        </tr>

        <tr>
            <th>Thn</th>
            <th>Bulan</th>
            <th>Transaction ID</th>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>BV</th>
            <th>Total Harga</th>
            <th>Total BV</th>
        </tr>

            <?php
            $no=1;
            foreach($detail as $row) {
            $thn = $row->THN;
            $bln = $row->BLN;
            $trcd = $row->trcd;
            $xprdcd = $row->xprdcd;
            $prdnm = $row->prdnm;
            $xqtyord = $row->xqtyord;
            $tdp = $row->tdp;
            $tbv = $row->tbv;
                $xdp = $row->xdp;
                $xbv = $row->xbv;
            ?>

            <tr>
                <td align="center"><?php echo $thn;?></td>
                <td align="center"><?php echo $bln;?></td>
                <td align="center"><?php echo $trcd;?></td>
                <td align="center"><?php echo $xprdcd;?></td>
                <td><?php echo $prdnm;?></td>
                <td align="center"><?php echo floor($xqtyord);?></td>
                <td align="right"><?php echo number_format($tdp,0,".",",");?></td>
                <td align="right"><?php echo floor($tbv); ?></td>
                <td align="right"><?php echo number_format($xdp,0,".",","); ?></td>
                <td align="right"><?php echo floor($xbv); ?></td>
            </tr>

                <?php
                $no++; }}
            ?>
        </table>


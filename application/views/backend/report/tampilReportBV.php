<?php
if(empty($result)){
    setErrorMessage();
}else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="14" align="center">Report Distributor Sales By BV</th>
    </tr>

    <tr>
        <th colspan="2" align="center">Period</th>
        <th colspan="3" align="center">0 To 99</th>
        <th colspan="3" align="center">100 To 199</th>
        <th colspan="3" align="center">200 To 299</th>
        <th colspan="3" align="center">300 To 399</th>
    </tr>


    <tr>

        <th>Bonus Month</th>
        <th>Bonus Year</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>
    </tr>

    <?php
    $no=1;
    foreach($result as $a){
    ?>

        <tr>
            <td align="center"><?php echo $a->bonusmonth;?></td>
            <td align="center"><?php echo $a->bonusyear;?></td>

            <td align="center"><?php echo number_format("$a->dist_0_to_99",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_0_to_99",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_0_to_99",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_100_to_199",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_100_to_199",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_100_to_199",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_200_to_299",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_200_to_299",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_200_to_299",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_300_to_399",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_300_to_399",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_300_to_399",0,",",".");?></td>
        </tr>

        <?php
        $no++; }
        ?>
</table>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">

    <tr>
        <th colspan="2" align="center">Period</th>
        <th colspan="3" align="center">400 To 499</th>
        <th colspan="3" align="center">500 To 599</th>
        <th colspan="3" align="center">600 ABOVE</th>
    </tr>

    <tr>

        <th>Bonus Month</th>
        <th>Bonus Year</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

    </tr>

    <?php
    $no=1;
    foreach($result as $a){
    ?>

    <tr>

        <td align="center"><?php echo $a->bonusmonth;?></td>
        <td align="center"><?php echo $a->bonusyear;?></td>

        <td align="center"><?php echo number_format("$a->dist_400_to_499",0,",",".");?></td>
        <td align="center"><?php echo number_format("$a->bv_400_to_499",0,",",".");?></td>
        <td align="center"><?php echo number_format("$a->dp_400_to_499",0,",",".");?></td>

        <td align="center"><?php echo number_format("$a->dist_500_to_599",0,",",".");?></td>
        <td align="center"><?php echo number_format("$a->bv_500_to_599",0,",",".");?></td>
        <td align="center"><?php echo number_format("$a->dp_500_to_599",0,",",".");?></td>

        <td align="center"><?php echo number_format("$a->dist_600_above",0,",",".");?></td>
        <td align="center"><?php echo number_format("$a->bv_600_above",0,",",".");?></td>
        <td align="center"><?php echo number_format("$a->dp_600_above",0,",",".");?></td>
    </tr>

        <?php
        $no++; }}
    ?>

</table>






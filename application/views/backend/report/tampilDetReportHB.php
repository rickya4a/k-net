<?php
if(empty($all_sales_nonsuplemen_rekrutan)){
    setErrorMessage();
}else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="9" align="center">All Sales Non-Suplemen Rekrutan</th>
    </tr>

    <tr>
        <th>ID Member</th>
        <th>Nama</th>
        <th>Tgl Join</th>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Paket</th>
        <th>QTY</th>
        <th>BV</th>
        <th>Total BV</th>
    </tr>

    <?php
    $no=1;
    foreach($all_sales_nonsuplemen_rekrutan as $det){

        $jointdt = date("d-m-Y",strtotime( $det->jointdt ));
        ?>

        <tr>
            <td align="center"><?php echo $det->dfno; ?></td>
            <td align="center"><?php echo $det->fullnm; ?></td>
            <td align="center"><?php echo $jointdt; ?></td>
            <td align="center"><?php echo $det->prdcd; ?></td>
            <td align="center"><?php echo $det->prdnm; ?></td>
            <td align="center"><?php echo $det->prdcd_paket; ?></td>
            <td align="center"><?php echo $det->qty; ?></td>
            <td align="center"><?php echo $det->sBV; ?></td>
            <td align="center"><?php echo $det->bv; ?></td>
        </tr>




        <?php
        $no++; }}
    ?>
</table>

<input class="btn btn-mini btn-warning span20" type="button" onclick="be_trans.back_to_form()" value="<< Back">
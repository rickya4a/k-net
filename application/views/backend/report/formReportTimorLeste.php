
<div class="mainForm">
    <form class="form-horizontal" id="formTL" method="POST" action="<?php echo base_url('be/report/prevSave'); ?>">
        <fieldset>
            <div class="control-group">

                <label class="control-label showDt" for="typeahead">Status </label>
                <div class="controls">
                    <select id="StatTrans" name="StatTrans">
                        <option value="all">all</option>
                        <option value="P">pending</option>
                        <option value="T">transfer</option>
                    </select>
                </div>

                <label class="control-label showDt" for="typeahead">Bonus Period</label>
                <div class="controls showDt">
                    <input type="text" class="dtpicker" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
                    <input type="text"  class="dtpicker" id="date_to" name="date_to" />
<!--                    <input type="text" id="date_from" name="date_from"  value="01/01/2018"/>&nbsp;&nbsp;to&nbsp;-->
<!--                    <input type="text" id="date_to" name="date_to" value="01/09/2018"/>-->
                </div>

                <label class="control-label showDt" for="typeahead">Amount </label>
                <div class="controls showDt">
                    <select id="amount" name="amount">
                        <option value="<="> <= </option>
                        <option value=">="> >= </option>
                        <option value=">"> > </option>
                        <option value="<"> < </option>
                        <option value="="> = </option>
                        <option value="!="> != </option>
                    </select>
                    <input tabindex="3" type="text" id="nett" name="nett" style="width: 100px;" value="0"/>
                    <br />

                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_fendi.getListTimorLeste()" />
                    <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />

                </div>
            </div>
        </fieldset>

        <div class="result">

        </div>
        <div class="nextForm1">

        </div>
        <div class="nextForm2">

        </div>
        <div class="nextForm3">

        </div>

    </form>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        $(".dtpicker").datepicker({
            changeMonth : true,
            numberOfMonths : 1,
            dateFormat : 'dd/mm/yy',
        });

    });
</script>


<?php
if(empty($rptSgo)){
    echo setErrorMessage();
}else{

    if($exportTo == 0){
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=ReportSGORes.xls" );
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        $rptHead = "<tr><th>Report From Date </th><th>$from to $to</th></tr><tr></tr>";
        $border = 'border="1" ';
    }else{
        $rptHead = '';
        $border = '';
    }

    ?>
    <div class="overflow-auto">
        <form id="groupingDO" method="post" >
            <table <?php echo $border; ?> width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <?php echo $rptHead; ?>
                <!--<tr >
                    <th colspan="31" style="font-weight: bold;text-align: center;background-color:#f4f4f4 ;">List Trx SGO</th>
                 </tr>-->
                <tr>
                    <th>No</th>
					<th>Espay Trx ID</th>
                    <th>No VA</th>
                    <th>Amount</th>
                    <th>Member ID</th>
                    <th>Member Name</th>
                    <th>Date</th>
                   
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $TotAmount = 0;
                
                foreach($rptSgo as $row){
                    if($exportTo == 0){ //jika export ke excel, maka format
                        //echo "MASUK KE 0000";
                        $Amount 	= number_format($row->total_amount,0,"","");
                        
                    }else{
                        //echo "MASUK KE 1111";
                        $Amount 	= number_format($row->total_amount,0,"",",");
                        
                    }

					
					$typess = "Trx Top Up Saldo K-Wallet";
					

                    echo "<tr>
                          <td>".$no."</td>
						  <td>".$row->Transaction_ID."</td>
                          <td>".$row->product_value."</td>";
                          
                    echo "<td align=right>".$Amount."</td>";
                    echo "<td>".$row->member_id."</td>";
                    echo "<td>".$row->member_cust_name."</td>";
					echo "<td>".$row->payment_datetime."</td>";

                    $no++;

                    $TotAmount 		= $TotAmount + $row->total_amount;
                    
                }
                if($exportTo == 0){ //jika export ke excel, maka format
                    //echo "MASUK KE 0000";
                    $TotAmount 	 = number_format($TotAmount,0,"","");
                    
                }else{
                    //echo "MASUK KE 1111";
                    $TotAmount 	 = number_format($TotAmount,0,"",",");
                    
                }
                echo "<tr>
		                  	<td colspan=\"3\" align=\"center\">Total</td>

                            <td align='right'>$TotAmount</td>";
                            echo "<td colspan=\"3\">&nbsp;</td>
							
		                  </tr>";
                ?>
                </tbody>
            </table>
        </form>
        <br />
    </div>

<?php }?>
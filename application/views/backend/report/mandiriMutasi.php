<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
	<table class="table table-bordered bootstrap-datatable datatable" width="100%">
		<thead>
			<tr>
				<th colspan='6'>Cek Mutasi Mandiri - Manual</th>
			</tr>
			<tr>
				<th>From</th>
				<th>Kode Pembayaran</th>
				<th>Tanggal Transaksi</th>
				<th>Keterangan</th>
				<th>Jumlah</th>
				<th>Total Bayar</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; foreach ($result as $list) { 
				if(empty($list->tot_bayar) || $list->tot_bayar = null){
					$tot_bayar = 0;
				}else{
					$tot_bayar = number_format($list->tot_bayar,"0",".", ",");
				}
			?>
			<tr>
				<td><?php echo $list->label; ?></td>
				<td><?php echo $list->kd_pay; ?></td>
				<td><?php echo $list->transaction_date; ?></td>
				<td><?php echo $list->description; ?></td>
				<td style="text-align: right"><?php echo number_format($list->amount,"0",".", ","); ?></td>
				<td style="text-align: right"><?php echo number_format($tot_bayar,"0",".", ","); ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<script>
		$(document).ready(function () {
			$(All.get_active_tab() + " .datatable").dataTable({
				"aLengthMenu": [
					[
						10, 25, 50, 100, -1
					],
					[
						10, 25, 50, 100, 'All'
					]
				],
				"sPaginationType": "bootstrap",
				"oLanguage": {},
				"bDestroy": true
			});
			$(All.get_active_tab() + " .datatable").removeAttr('style');
		});
	</script>
<?php
}
?>

<div class="mainForm">
    <form class="form-horizontal" id="formReport" method="post"
          action="<?php echo site_url("report/summ_bv/cetakpdf")?>" target="_blank">
        <fieldset>
            <div class="control-group">

                <label class="control-label showDt" for="typeahead">ID Member</label>
                <div class="controls showDt">
                    <input tabindex="3" type="text" id="idmember" name="idmember" style="width: 250px;"/>
                </div>

                <label class="control-label showDt" for="typeahead">Bonus Period</label>
                <div class="controls showDt">
                    <input type="text"  class="dtpicker" id="bnsperiod" name="bnsperiod" />
                </div>
                <label class="control-label showDt" for="typeahead">Sales Berdasarkan </label>
                <div class="controls showDt">
                    <select id="tipe" name="tipe" tabindex="2">
                        <option value="pribadi">pribadi</option>
                        <option value="rekrutan">rekrutan</option>
                    </select>
                    <br />
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/report/list_hb')" />
                    <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
                </div>

            </div>
        </fieldset>
        <div class="result"></div>
        <div class="mainform1"></div>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $(".dtpicker").datepicker({
            changeMonth : true,
            numberOfMonths : 1,
            dateFormat : 'yy-mm-dd',
        });

    });

</script>


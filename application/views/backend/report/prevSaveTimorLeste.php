<?php

    if(empty($result)){
        setErrorMessage();
    }else {
        ?>

        <form class="form-horizontal" id="formPrevSave" method="POST" action="<?php echo base_url('be/report/insertDetail'); ?>">
            <fieldset>
                <div class="control-group">

<!--                    <label class="control-label showDt" for="typeahead">Potongan </label>-->
<!---->
<!--                    <div class="potong">-->
<!--<!--                        <input tabindex="3" type="text" id="potong" name="potongan" style="width: 100px;" onchange="functionGanti()"/>-->
<!--                        <input type="text" id="potong">-->
<!--                        <br/>-->
<!---->
<!--                    </div>-->

                        <table style="width: 100%" id="attrTable" class="table table-striped table-bordered bootstrap-datatable datatable">
                            <tr>
                                <th colspan="14" align="center">FORM</th>
                            </tr>

                            <tr>
                                <th>No.</th>
                                <th>ID Member</th>
                                <th>Nama Member</th>
                                <th>Total Bonus</th>
                                <th>Potongan</th>
                                <th>Total Transfer</th>
                            </tr>

                            <?php

                            $no = 1;
                            $x = 0;
                            foreach ($result as $a) {

                                ?>

                                <tr>
                                    <td align="center">
                                        <?php echo $no; ?>
                                    </td>

                                    <td align="center">
                                        <?php echo $a->idnumber; ?>
                                        <input type="hidden" name="idmember[]" id="idmember" value="<?php echo $a->idnumber; ?>">
                                    </td>

                                    <td>
                                        <?php echo $a->fullnm; ?>
                                    </td>

                                    <td>
                                        <input type="number" class="bonus" value="<?php echo $a->NETT; ?>">
                                        <input type="hidden" name="bonus[]" value="<?php echo $a->NETT; ?>">
                                    </td>

                                    <td>
                                        <input type="number" class="potongan" name="potongan[]">
                                    </td>

                                    <td class="nett" align="center">
                                        <input type="number">
                                    </td>
                                </tr>
                                <?php

                                $x += $a->NETT;
                                $no++;
                            }
                                ?>

                                <tr>
                                    <td colspan="3" align="center">Jumlah</td>
                                    <td align="center">
                                        <?php
                                        echo $x;
                                        ?>
                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                </tr>

                        </table>
                    </div>
                    <br>
<!--                    <input tabindex="3" type="submit" id="btn_input_user" class="btn btn-primary .submit" name="submit" value="Submit" />-->
                    <input class="btn btn-small btn-success" type="button"  onclick="be_fendi.formPost3()" value="UPDATE">
                    <input class="btn btn-small btn-warning" type="button" onclick="be_trans.back_to_form()" value="<< Back">
                </div>
            </fieldset>

        </form>

        <?php
        }
        ?>

<script type="text/javascript">

$('.bonus, .potongan').on('blur', function(e) {

    // Get Parent ROW
    var row = $(this).closest('tr');
    var bonus = $('.bonus', row),
        potongan = $('.potongan', row),
        nett = $('.nett', row);

    bonusss = parseInt(bonus.val());
    potong = parseInt(potongan.val());

    if( ! isNaN(bonusss) && !isNaN(potong) ) {
        nett.text( ( bonusss - potong ));
    }
});

</script>

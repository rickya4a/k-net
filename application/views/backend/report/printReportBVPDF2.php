<?php

$pdf=new FPDF('L','mm', 'A4');
$pdf->Open();
$pdf->AddPage();
$pdf->Ln();
$pdf->setFont('Arial','B',9);
$pdf->SetFillColor(255,255,255);

//$panjangCellDis = 18;
$panjangCell = 20;
$lebarCell = 5;
$pjgHeader = $panjangCell * 14;

$pdf->SetFillColor(165,165,235);
$pdf->Cell($pjgHeader,$lebarCell,"Report Distributor Sales By BV",1,0,'C',true);
$pdf->Ln();

$pdf->Cell($panjangCell*2,$lebarCell,"Periode",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"0 To 100",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"101 To 200",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"201 To 300",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"301 To 400",1,0,'C',true);
$pdf->Ln();


$pdf->Cell($panjangCell,$lebarCell,"BULAN",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"TAHUN",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);
$pdf->SetFillColor(255,255,255);
$pdf->Ln();

if($result != null){

    $no = 1;
    foreach($result as $a){

        $dist_0_to_100 = number_format("$a->dist_0_to_100",0,",",".");
        $bv_0_to_100 = number_format("$a->bv_0_to_100",0,",",".");
        $dp_0_to_100 = number_format("$a->dp_0_to_100",0,",",".");

        $dist_101_to_200 = number_format("$a->dist_101_to_200",0,",",".");
        $bv_101_to_200 = number_format("$a->bv_101_to_200",0,",",".");
        $dp_101_to_200 = number_format("$a->dp_101_to_200",0,",",".");

        $dist_201_to_300 = number_format("$a->dist_201_to_300",0,",",".");
        $bv_201_to_300 = number_format("$a->bv_201_to_300",0,",",".");
        $dp_201_to_300 = number_format("$a->dp_201_to_300",0,",",".");

        $dist_301_to_400 = number_format("$a->dist_301_to_400",0,",",".");
        $bv_301_to_400 = number_format("$a->bv_301_to_400",0,",",".");
        $dp_301_to_400 = number_format("$a->dp_301_to_400",0,",",".");

        $pdf->SetFillColor(255,255,255);
        $pdf->Cell($panjangCell,$lebarCell,"$a->bonusmonth",1,0,'C',true);
        $pdf->Cell($panjangCell,$lebarCell,"$a->bonusyear",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_0_to_100",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_0_to_100",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_0_to_100",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_101_to_200",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_101_to_200",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_101_to_200",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_201_to_300",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_201_to_300",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_201_to_300",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_301_to_400",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_301_to_400",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_301_to_400",1,0,'C',true);


        $pdf->Ln();

        $no++;
    }
}

$pdf->SetFillColor(165,165,235);
$pdf->Cell($panjangCell*2,$lebarCell,"Periode",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"401 To 500",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"501 To 600",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"601 Above",1,0,'C',true);

$pdf->Ln();

$pdf->Cell($panjangCell,$lebarCell,"BULAN",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"TAHUN",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);
$pdf->SetFillColor(255,255,255);
$pdf->Ln();

if($result != null){

    $no = 1;
    foreach($result as $a){

        $bonusmonth = $a->bonusmonth;
        $bonusyear = $a->bonusyear;

        $dist_401_to_500 = number_format("$a->dist_401_to_500",0,",",".");
        $bv_401_to_500 = number_format("$a->dist_401_to_500",0,",",".");
        $dp_401_to_500 = number_format("$a->dist_401_to_500",0,",",".");

        $dist_501_to_600 = number_format("$a->dist_501_to_600",0,",",".");
        $bv_501_to_600 = number_format("$a->dist_501_to_600",0,",",".");
        $dp_501_to_600 = number_format("$a->dist_501_to_600",0,",",".");

        $dist_601_above = number_format("$a->dist_601_above",0,",",".");
        $bv_601_above = number_format("$a->bv_601_above",0,",",".");
        $dp_601_above = number_format("$a->dp_601_above",0,",",".");

        $pdf->SetFillColor(255,255,255);
        $pdf->Cell($panjangCell,$lebarCell,"$bonusmonth",1,0,'C',true);
        $pdf->Cell($panjangCell,$lebarCell,"$bonusyear",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_401_to_500",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_401_to_500",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_401_to_500",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_501_to_600",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_501_to_600",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_501_to_600",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_601_above",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_601_above",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_601_above",1,0,'C',true);

        $pdf->Ln();
        $no++;

    }
}

$pdf->Output();
?>



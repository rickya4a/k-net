<div class="mainForm">
	<form class="form-horizontal" id="reportRekapProduk" name="reportRekapProduk" method="post">
		<fieldset>
			<div class="form-group">
				<label for="input" class="col-sm-2 control-label">Status</label>
				<div class="col-sm-2">
					<select name="status" id="input" class="form-control" required="required">
						<option value="">-- Select One --</option>
						<option value="0">Belum diambil</option>
						<option value="1">Sudah diambil</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="input" class="col-sm-2 control-label">Stockist</label>
				<div class="col-lg-10">
					<input type="text" id="stockist" name="stockist" value="<?php echo $stockist; ?>" onchange="All.getFullNameByID(this.value,'trx/reconcile/helper/mssc/loccd','#stockist_name')">
					<input type="text" id="stockist_name" name="stockist_name" value="<?php echo $stockist_name; ?>" style="width: 150px;" readonly>
				</div>
				<label for="input" class="col-sm-2 control-label">Type</label>
				<div class="col-sm-2">
					<select name="type" id="input" class="form-control" required="required">
						<option value="">-- Select One --</option>
						<option value="0">Rekap Produk</option>
						<option value="1">List Transaksi</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="typeahead">Date</label>
				<div class="controls">
					<input type="text" class="dtpicker typeahead" id="date_from" name="date_from" />&nbsp;&nbsp;to&nbsp;
					<input type="text" class="dtpicker typeahead" id="date_to" name="date_to" />
				</div>
				<div class="controls" id="inp_btn">
					<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save"
						value="Submit" onclick="All.ajaxFormPost(this.form.id,'rekap/produk/stockist/check')" />
					<input type="reset" tabindex="3" class="btn btn-danger" name="reset" value="Reset" />
				</div>
			</div>
		</fieldset>
		<div class="result"></div>
	</form>
</div>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>
<?php
if(empty($result)){
    setErrorMessage();
}else {
    ?>

    <form class="form-horizontal" id="formReport" method="post">
        <table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
            <tr>
                <th colspan="14" align="center">Report Bonus Timor Leste</th>
            </tr>

            <tr>
                <th>No.</th>
                <th>ID Member</th>
                <th>Nama Member</th>
                <th>Total Bonus</th>
                <th>Status</th>
                <th>Tanggal Transfer</th>
                <th>No. Akun</th>
                <th>Bank</th>
                <th>Nama Akun</th>
                <th>Action</th>
            </tr>

            <?php
            $no = 1;
            foreach ($result as $a) {
                ?>

                <tr>
                    <td>
                        <?php echo $no; ?>
                    </td>
                    <td>
                        <?php echo $a->idnumber; ?>
                    </td>
                    <td>
                        <?php echo $a->fullnm; ?>
                    </td>
                    <td>
                        <?php echo $a->nom_NETT; ?>
                    </td>
                    <td>
                        <?php echo $a->status; ?>
                    </td>
                    <td>
                        <?php echo date("d-m-Y", strtotime($a->tgl_trf)); ?>
                    </td>
                    <td>
                        <?php echo $a->AccountNo; ?>
                    </td>
                    <td>
                        <?php echo $a->BankID; ?>
                    </td>
                    <td>
                        <?php echo $a->AccountName; ?>
                    </td>
                    <td align="center">
                        <input type="checkbox" class="idmemb" name="idmember[]" value="<?php echo $a->idnumber; ?>">
                        <!--<td align="center"><input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" value="Lihat Detail" onclick="javascript:be_trans.getDetailBonusAyu(<?php echo $no;?>)" href="#" /></td>-->
                    </td>
                </tr>

                <?php
                $no++;}
            ?>
        </table>
        <td align="center"><input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" value="Detail" onclick="javascript:be_trans.postTimorLeste()" href="#" /></td>
    </form>


    <?php
}
?>


<div class="mainForm">
    <form class="form-horizontal" id="formReport" method="post">
        <fieldset>
            <div class="control-group">

                <label class="control-label showDt" for="typeahead">ID Stockist </label>
                <div class="controls showDt">
                    <input tabindex="3" type="text" id="idstockist" name="idstockist" style="width: 250px;"/>
                </div>

                <label class="control-label" for="typeahead">Search By</label>
                <div class="controls">
                    <select id="searchBy" name="searchBy">
                        <option value="">--Select Here--</option>
                        <option value="tgl">Date</option>
                        <option value="bnsperiod">Bonus Period</option>
                    </select>
                </div>

                <?php
                echo datepickerFromTo("Parameter Value", "tgl_dari", "tgl_ke");
                //echo bonusPeriod();
                ?>

                <?php
                //echo datepickerFromTo("Transaction Date", "trx_froms", "trx_tos");
                echo bonusPeriod();
                ?>

                <div class="controls">
                    <br />
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/report/sales/bystockist/act')" />
                    <!--                    <input id="printExcel" class="btn btn-success" type="submit" value="Cetak ke Excel" name="printExcel">-->
                    <!--                    <input class="btn btn-success" type="submit" formaction="--><?php //echo site_url('')?><!--" value="Cetak ke Excel">-->
                    <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
                </div>

            </div>
        </fieldset>
        <div class="result"></div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".dtpicker").datepicker({
            changeMonth : true,
            numberOfMonths : 1,
            dateFormat : 'yy-mm-dd',
        });

    });
</script>
<?php
if(empty($result)){
    setErrorMessage();
}else {
    ?>

    <table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <tr>
            <th colspan="14" align="center">DETAIL REPORT TIMOR LESTE</th>
        </tr>

        <tr>
            <th>Bonus Period</th>
            <th>ID Member</th>
            <th>Nama Member</th>
            <th>Total Bonus</th>
            <th>Total Transfer</th>
            <th>Keterangan</th>
        </tr>

        <?php

        $no = 1;
        foreach ($result as $dt) {

            ?>

            <tr>
                <td align="center">
                    <?php echo date("d-m-Y", strtotime($dt->bnsperiod)); ?>
                </td>
                <td align="center">
                    <?php echo $dt->idnumber; ?>
                </td>
                <td>
                    <?php echo $dt->fullnm; ?>
                </td>
                <td align="center">
                    <?php echo $dt->NETT; ?>
                </td>
                <td align="center">
                    <?php echo $dt->TRF; ?>
                </td>
                <td align="center">
                    <?php echo $dt->KET; ?>
                </td>
            </tr>

            <?php

            $no++;
        }

        ?>
    </table>
    <?php
}
?>
<div>
<!--    <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>-->
    <input class="btn btn-warning" type="button" onclick="be_trans.back_to_form()" value="<< Back">
</div>
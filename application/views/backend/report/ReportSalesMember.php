<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">

    <thead>
    <tr>
        <th>Kode Transaksi</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>Total Produk</th>
        <th>Harga</th>
        <th>BV</th>
        <th>Total Harga</th>
        <th>Total BV</th>
        <th>Jenis Transaksi</th>
        <th>Jenis Produk</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if($result != null)
    {

        $i = 1;
        foreach($result as $dt) {

            ?>

            <tr>

                <td>
                    <?php echo $dt->trcd ?>
                </td>

                <td>
                    <?php echo $dt->zPRDCD ?>
                </td>

                <td>
                    <?php echo $dt->prdnm ?>
                </td>

                <td align="center">

                    <?php
                    $a = number_format($dt->zQTY, 0, '.', '');
                    echo number_format($a, 0, ".", ".");
                    ?>
                </td>

                <td align="left">
                    <?php

                    $a = number_format($dt->zdp, 0, '.', '');
                    echo number_format($a, 0, ".", ".");

                    ?>
                </td>

                <td>
                    <?php echo number_format($dt->zBV, 0, '.', ''); ?>
                </td>

                <td align="left">
                    <?php

                    $a = number_format($dt->xDP, 0, '.', '');
                    echo number_format($a, 0, '.', '.');

                    ?>
                </td>

                <td>
                    <?php echo number_format($dt->xBV, 0, '.', ''); ?>
                </td>

                <td>
                    <?php
                    if(($dt->flag2) == "1"){
                        echo "MampirKak";
                    }else{
                        echo"Reguler";
                    }
                    ?>
                </td>

                <td>
                    <?php
                    if(($dt->flag) == "0"){
                        echo "Reguler";
                    }else{
                        echo"LBC";
                    }
                    ?>
                </td>


            </tr>
            <?php
            $i++; }
    }

    ?>

    </tbody>
</table>
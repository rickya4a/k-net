<?php
if(empty($all_sales)){
    setErrorMessage();
}else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="7" align="center">Report Promo Heboh Banget</th>
    </tr>

    <tr>
        <th colspan="7" align="center">All Sales</th>
    </tr>

    <tr>
        <th>TRX Date</th>
        <th>TRX ID</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Qty</th>
        <th>BV</th>
        <th>Total BV</th>
    </tr>

    <?php

    $no = 1;
    foreach ($all_sales as $dt) {
        ?>

        <tr>
            <td align="center"><?php echo date("d-m-Y", strtotime($dt->etdt)); ?></td>
            <td align="center"><?php echo $dt->trcd; ?></td>
            <td align="center"><?php echo $dt->xprdcd; ?></td>
            <td><?php echo $dt->prdnm; ?></td>
            <td align="center"><?php echo number_format($dt->xqtyord); ?></td>
            <td align="center"><?php echo number_format($dt->tbv); ?></td>
            <td align="center"><?php echo number_format($dt->totBV); ?></td>
        </tr>
        <?php
        $no++;}
    ?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center">Total</td>
        <td align="center">
            <?php echo number_format($tot_bv1); ?>
        </td>
    </tr>
    <?php
    }
    ?>
    </table>
<?php
    if(empty($all_sales_nonsuplemen)){
        setErrorMessage();
    }else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">

    <tr>
        <th colspan="8" align="center">ALL SALES NON-SUPLEMEN</th>
    </tr>

    <tr>
        <th>TRX Date</th>
        <th>TRX ID</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Paket</th>
        <th>Qty</th>
        <th>BV</th>
        <th>Total BV</th>
    </tr>

    <?php
        $no = 1;
        foreach ($all_sales_nonsuplemen as $dt) {
    ?>

            <tr>
                <td align="center"><?php echo date("d-m-Y", strtotime($dt->etdt)); ?></td>
                <td align="center"><?php echo $dt->trcd; ?></td>
                <td align="center"><?php echo $dt->prdcd; ?></td>
                <td><?php echo $dt->prdnm; ?></td>
                <td><?php echo $dt->prdcd_paket; ?></td>
                <td align="center"><?php echo number_format($dt->qty); ?></td>
                <td align="center"><?php echo number_format($dt->sBV); ?></td>
                <td align="center"><?php echo number_format($dt->bv); ?></td>
            </tr>
            <?php
            $no++;}
    ?>

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center">Total</td>
        <td align="center">
            <?php echo number_format($tot_bv2); ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>











































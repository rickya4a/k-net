<?php
if(empty($result)){
    setErrorMessage();
}else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="14" align="center">Report Distributor Sales By BV</th>
    </tr>

    <tr>
        <th colspan="2" align="center">Period</th>
        <th colspan="3" align="center">0 To 100</th>
        <th colspan="3" align="center">101 To 200</th>
        <th colspan="3" align="center">201 To 300</th>
        <th colspan="3" align="center">301 To 400</th>
    </tr>


    <tr>

        <th>Bonus Month</th>
        <th>Bonus Year</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>
    </tr>

    <?php
    $no=1;
    foreach($result as $a){
        ?>

        <tr>
            <td align="center"><?php echo $a->bonusmonth;?></td>
            <td align="center"><?php echo $a->bonusyear;?></td>

            <td align="center"><?php echo number_format("$a->dist_0_to_100",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_0_to_100",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_0_to_100",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_101_to_200",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_101_to_200",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_101_to_200",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_201_to_300",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_201_to_300",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_201_to_300",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_301_to_400",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_301_to_400",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_301_to_400",0,",",".");?></td>
        </tr>

        <?php
        $no++; }
    ?>
</table>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">

    <tr>
        <th colspan="2" align="center">Period</th>
        <th colspan="3" align="center">401 To 500</th>
        <th colspan="3" align="center">501 To 600</th>
        <th colspan="3" align="center">601 ABOVE</th>
    </tr>

    <tr>

        <th>Bonus Month</th>
        <th>Bonus Year</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

        <th>Dis.(Pax)</th>
        <th>Total BV</th>
        <th>Total DP</th>

    </tr>

    <?php
    $no=1;
    foreach($result as $a){
        ?>

        <tr>

            <td align="center"><?php echo $a->bonusmonth;?></td>
            <td align="center"><?php echo $a->bonusyear;?></td>

            <td align="center"><?php echo number_format("$a->dist_401_to_500",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_401_to_500",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_401_to_500",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_501_to_600",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_501_to_600",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_501_to_600",0,",",".");?></td>

            <td align="center"><?php echo number_format("$a->dist_601_above",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->bv_601_above",0,",",".");?></td>
            <td align="center"><?php echo number_format("$a->dp_601_above",0,",",".");?></td>
        </tr>

        <?php
        $no++; }}
    ?>

</table>






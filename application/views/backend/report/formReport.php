
<div class="mainForm">
    <form class="form-horizontal" id="formReport" method="post"
        action="<?php echo site_url("report/summ_bv/cetakpdf")?>" target="_blank">
        <fieldset>
            <div class="control-group">
<!--                <label class="control-label showDt" for="typeahead">Bulan </label>-->
<!--                <div class="controls showDt">-->
<!--                    <input tabindex="3" type="text" id="month" name="month" style="width: 250px;"/>-->
<!--                </div>-->

                <label class="control-label showDt" for="typeahead">Tahun </label>
                <div class="controls showDt">
                    <select id="year" name="year" tabindex="2">
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                    </select>
                </div>

                <label class="control-label showDt" for="typeahead">berdasarkan </label>
                <div class="controls showDt">
                    <select id="range" name="range" tabindex="2">
                        <option value="00-99">00-99</option>
                        <option value="01-00">01-00</option>
                    </select>
                    <br />

                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'report/summ_bv/tampil')" />
                    <input id="printPdf" class="btn btn-success" type="submit" value="Cetak ke PDF" name="printPdf">
                    <input class="btn btn-success" type="submit" formaction="<?php echo site_url('report/summ_bv/cetakexcel')?>" value="Cetak ke Excel">
                    <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
                </div>
            </div>
        </fieldset>
        <div class="result"></div>
    </form>
</div>

<!--<script type="text/javascript">-->
<!---->
<!---->
<!--    $(document).ready(function() {-->
<!--        $(".dtpicker").datepicker({-->
<!--            changeMonth : true,-->
<!--            numberOfMonths : 1,-->
<!--            dateFormat : 'yy-mm-dd',-->
<!--        });-->
<!---->
<!--    });-->
<!---->
<!--</script>-->


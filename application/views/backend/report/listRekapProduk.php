<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
	<table class="table table-bordered bootstrap-datatable datatable" width="100%">
		<thead>
			<tr>
				<th colspan='3'>List Rekap Produk</th>
			</tr>
			<tr>
				<th>Kode Produk</th>
				<th>Nama Produk</th>
				<th>Jumlah</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; foreach ($result as $list) { ?>
			<tr>
				<td><?php echo $list->prdcd; ?></td>
				<td><?php echo $list->prdnm; ?></td>
				<td style="text-align: center"><?php echo $list->jumlah; ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<script>
		$(document).ready(function () {
			$(All.get_active_tab() + " .datatable").dataTable({
				"aLengthMenu": [
					[
						10, 25, 50, 100, -1
					],
					[
						10, 25, 50, 100, 'All'
					]
				],
				"sPaginationType": "bootstrap",
				"oLanguage": {},
				"bDestroy": true
			});
			$(All.get_active_tab() + " .datatable").removeAttr('style');
		});
	</script>
<?php
}
?>
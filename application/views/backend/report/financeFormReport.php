<div class="mainForm">
    <form class="form-horizontal" id="frmReportSgo" method="post"
          action="<?php echo site_url("trx/finance/report/act")?>" target="_blank">
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="typeahead">Transaction Date</label>
                <div class="controls">
                    <input type="text" class="dtpickers typeahead" id="sgoReportFrom" name="from" />&nbsp;to&nbsp;
                    <input type="text"  class="dtpickers typeahead" id="sgoReportTo" name="to" />
                </div>
                <label class="control-label" for="typeahead">Status</label>
                <div class="controls">
                    <select id="status" name="status">
                        <option value="0">All Excluded Failed</option>
                        <option value='1'>Success</option>
                        <option value='2'>Suspect</option>
                        <option value='3'>Failed</option>
                    </select>
                </div>
                <label class="control-label" for="typeahead">Bank</label>
                <div class="controls">
                    <select id="listBank" name="listBank">
					   <option value="">-- All --</option>
                        <?php
                            foreach($listBank as $dt){

                                //echo "<option value=\"$dt->bankCode\">$dt->bankDisplayNm</option>";
								echo "<option value=\"$dt->id\">$dt->bankDisplayNm</option>";
                            }
                        ?>
                    </select>
                </div>
                <label class="control-label" for="typeahead">Export To</label>
                <div class="controls">
                    <select id="exportTo" name="exportTo">
                        <option value="2">View List</option>
                        <option value="0">Excel</option>
                        <option value="1">CSV</option>
                    </select>
                </div>

				<label class="control-label" for="typeahead">Trx Type</label>
                <div class="controls">
                    <select id="tipe_trx" name="tipe_trx">
                        <option value="1">Transaksi k-net</option>
                        <option value="0">Tiket Acara</option>
						<option value="2">Top Up K-Wallet</option>
                    </select>
                </div>

                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit"
                           onclick="All.ajaxFormPost(this.form.id,'trx/finance/report/act')" />
                    <input type='submit' id='printXls' name='printXls' value='Export Data' class='btn btn-success'/>
										<input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
										<input id="getMutasi" type="button" class="btn btn-primary" onclick="getMutasiMandiri()" value="Cek Mutasi">
                </div>
            </div> <!-- end control-group -->
        </fieldset>

        <div class="result"></div>
    </form>
</div><!--/end mainForm-->
<script>
// Set datepicker format
$(document).ready(function () {
	$(All.get_active_tab() + " .dtpickers").datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		dateFormat: 'yy/mm/dd',
	}).datepicker("setDate", new Date());;
});

/**
 * Show "Cek Mutasi Mandiri - Manual" button depend on
 * onChange event
 */
$(document).ready(function () {
	$('#getMutasi').hide();
	$('#listBank').change(function () {
		if ($('#listBank').val() == '11') {
			$('#getMutasi').show();
		} else {
			$('#getMutasi').hide();
		}
	});
});

/**
 * Retrieve data mutation Mandiri from K-Net API service
 *
 * @return mixed
 */
function getMutasiMandiri() {
	let from = $('#sgoReportFrom').val(),
		to = $('#sgoReportTo').val(),
		statusx = $('#status').val();
		//statusy = "all";

	let year_from = from.substring(0, 4),
		month_from = from.substring(7, 5),
		day_from = from.substring(10, 8);

	let year_to = to.substring(0, 4),
		month_to = to.substring(7, 5),
		day_to = to.substring(10, 8);

	let date_from = year_from + "-" + month_from + "-" + day_from + " 00:00:00",
		date_to = year_to + "-" + month_to + "-" + day_to + " 23:59:59";
                        
    if(statusx == "0"){ // all status
    	statusy = "all";
    }else if(statusx == "1"){ // Success
    	statusy = "1";
    }else if(statusx == "2" || statusx == "3"){ // Suspect/Failed
    	statusy = "0";
    }
    
	All.set_disable_button();
	All.get_image_load2();
	$.ajax({
		type: "POST",
		url: All.get_url('trx/finance/report/mandiri'),
		data: {
			token: "654321",
			date_from: date_from,
			date_to: date_to,
			status: statusy,
			platform: "all",
			no_rek:"1190000879898"
			//status: "1",
			//platform: "knet"
		},
		success: function (data) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(data);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
	});
}
</script>

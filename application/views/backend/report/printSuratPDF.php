<?php

require APPPATH."libraries/Code128.php";
//$pdf=new FPDF('P','mm', 'A4');
$pdf = new Code128('P', 'mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)

//$pdf->text($kiri,75,"");

$pdf->AddPage();
$pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
$pdf->SetFont('Courier','', 8);

$pdf->Line(5,35,200,35);

$kiri = 10;

$pdf->text($kiri,45,"Kepada         :  Para STOCKIST, SUB STOCKIST, MOBILE STOCKIST dan DISTRIBUTOR ");
$pdf->text($kiri,50,"CC             :  Presdir-Bpk. Radzi Saleh,");
$pdf->text($kiri,55,"                  SFAM-Ibu Lanny,  SM-Nandang Hermansyah,");
$pdf->text($kiri,60,"                  Distribution Mgr-Maryanti, Warehouse Mgr-Ferry Kasabuana,");
$pdf->text($kiri,65,"                  CS-Parida ");
$pdf->text($kiri,70,"Tanggal        :  ".$date);
$pdf->text($kiri,75,"Perihal        :  Batas Pembelanjaan Periode BONUS Juni 2018");

$pdf->Line(5,85,200,85);
$pdf->Line(5,86,200,86);

$pdf->text($kiri,95,"Dengan hormat, ");
$pdf->text($kiri,105,"Diberitahukan kepada para seluruh Stockist, Sub Stockist dan Mobile Stockist batas akhir ");
$pdf->text($kiri,110,"pembelanjaan adalah sebagai berikut :");

$pdf->text($kiri,125,"STOCKIST ONLINE : Batas input dan pembayaran  sampai dengan tanggal");

$pdf->Line(5,135,200,135);

$pdf->text($kiri,150,"STOCKIST MANUAL : 1. Batas belanja terakhir adalah tanggal ");
$pdf->text($kiri,155,"                  2. Batas penerimaan dokumen pembelanjaan lengkap dan pembayaran, harus");
$pdf->text($kiri,160,"                     sudah diterima di kantor pusat tanggal");

$pdf->Line(5,175,200,175);

$pdf->text($kiri,190,"INVOICE         : 1. Dibuka untuk Personal, K-Mart, PGBV dan GBV melalui FAX/Whatsapp/SMS");
$pdf->text($kiri,195,"                  2. Batas waku sampai dengan tanggal ");
$pdf->text($kiri,200,"                  3. Dokumen/permintaan transaksi invoice di atas tanggal ");

$pdf->text($kiri,225,"Terima kasih atas kerjasama dan pengertiannya. ");

$pdf->text(135,235,"Jakarta, ".$date);
$pdf->text(138,240,"Hormat kami,");


//$pdf->text();


















$title = "bns_report.pdf";
//$pdfx->SetTitle($title);
$pdf->Output();

?>
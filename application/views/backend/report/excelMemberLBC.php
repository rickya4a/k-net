<?php

$filenm = "report";

header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$filenm.xls");
header("Pragma: no-cache");
header("Expires: 0");

//print_r($tbl_lbc);
if(empty($tbl_lbc)){

    setErrorMessage();
}else {

    $no = 1;
    foreach ($tbl_lbc as $x) {

        $dfno = $x->dfno;
        $fullnm = $x->fullnm;
        $alamat1 = $x->addr1;
        $alamat2 = $x->addr2;
        $alamat3 = $x->addr3;
        $regis_date = $x->register_dt;
        $expired_date = $x->expired_dt;
        $rank = $x->rank;
        $rank_desc = $x->rank_desc;
        $status = $x->STATUS;
        $birthdt = $x->birthdt;
        $reprint = $x->reprint;
        $email = $x->email;
        $no++;}
    ?>

    <p>
        <br>
        <b>ID Member LBC</b> : <?php echo $dfno;?><br>
        <b>Nama</b> : <?php echo $fullnm;?> <br>
        <b>Alamat 1</b> : <?php
        if($alamat1 != null){
            echo $alamat1;
        }else{
            echo "-";
        }
        ?> <br>
        <b>Alamat 2</b> : <?php
        if($alamat2 != null){
            echo $alamat2;
        }else{
            echo "-";
        }
        ?> <br>
        <b>Alamat 3</b> : <?php
        if($alamat2 != null){
            echo $alamat2;
        }else{
            echo "-";
        }
        ?> <br>
        <b>Register Date</b> : <?php echo $regis_date;?> <br>
        <b>Expired Date</b> : <?php echo $expired_date;?> <br>
        <b>Ranking</b> : <?php echo $rank." - ".$rank_desc;?> <br>
        <b>Status Keanggotaan</b> : <?php if($status = '0'){
            echo "tidak aktif";
        }else{
            echo "aktif";
        }
        ;?> <br>
        <b>Tgl Lahir</b> : <?php echo $birthdt;?> <br>
        <b>Reprint Card</b> : <?php echo $reprint;?> <br>
        <b>Email</b> : <?php echo $email;?> <br>


    </p>

    <?php
}

if(empty($detail)){

    setErrorMessage();
}else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="8">Detail Data Member LBC</th>
    </tr>

    <tr>
        <th>Thn</th>
        <th>Bulan</th>
        <th>Transaction ID</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Qty</th>
        <th>Total Harga</th>
        <th>BV</th>
    </tr>

    <?php
    $no=1;
    foreach($detail as $row) {
        $thn = $row->THN;
        $bln = $row->BLN;
        $trcd = $row->trcd;
        $xprdcd = $row->xprdcd;
        $prdnm = $row->prdnm;
        $xqtyord = $row->xqtyord;
        $tdp = $row->tdp;
        $tbv = $row->tbv;
        ?>

        <tr>
            <td><?php echo $thn;?></td>
            <td align="center"><?php echo $bln;?></td>
            <td align="center"><?php echo $trcd;?></td>
            <td align="center"><?php echo $xprdcd;?></td>
            <td><?php echo $prdnm;?></td>
            <td align="center"><?php echo $xqtyord;?></td>
            <td align="right"><?php echo number_format($tdp,0,".",",");?></td>
            <td align="right"><?php echo $tbv; ?></td>
        </tr>

        <?php
        $no++; }}
    ?>
</table>

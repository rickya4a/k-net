<?php

    if(empty($all_sales_rekrutan)){

        echo "tidak ada pembelanjaan oleh rekrutan pada periode tersebut!";
    }else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="10" align="center">Report Promo Heboh Banget</th>
    </tr>

    <tr>
        <th colspan="10" align="center">All Sales Rekrutan</th>
    </tr>

    <tr>
        <th>ID Member</th>
        <th>Nama</th>
        <th>Tgl Join</th>
        <th>TRX ID</th>
        <th>TRX Date</th>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Qty</th>
        <th>BV</th>
        <th>Total BV</th>
    </tr>

    <?php

    $no = 1;
    foreach ($all_sales_rekrutan as $dt) {
    ?>

    <tr>
        <td><?php echo $dt->dfno; ?></td>
        <td><?php echo $dt->fullnm; ?></td>
        <td align="center"><?php echo date("d-m-Y", strtotime($dt->jointdt)); ?></td>
        <td align="center"><?php echo $dt->trcd; ?></td>
        <td align="center"><?php echo date("d-m-Y", strtotime($dt->etdt)); ?></td>
        <td align="center"><?php echo $dt->xprdcd; ?></td>
        <td><?php echo $dt->prdnm; ?></td>
        <td align="center"><?php echo number_format($dt->xqtyord); ?></td>
        <td align="center"><?php echo number_format($dt->tbv); ?></td>
        <td align="center"><?php echo number_format($dt->totBV); ?></td>
    </tr>

    <?php
        $no++;}}
    ?>
</table>

<?php

    if(empty($rekap_bv_all_nonsuplemen)) {

        echo "pembelanjaan rekrutan pada periode tersebut tidak ada non-suplemen/hanya suplemen";
    }else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">

    <tr>
        <th colspan="10" align="center">Rekap Sales Non-Suplemen Rekrutan</th>
    </tr>

    <tr>
        <th>ID Member</th>
        <th>Nama</th>
        <th>Tgl Join</th>
        <th>Total BV</th>
        <th>Total Kupon</th>
    </tr>

        <?php

        $no = 1;
        foreach ($rekap_bv_all_nonsuplemen as $dt) {
            ?>

    <tr>
        <td align="center">
            <a id="<?php echo $dt->dfno; ?>" onclick="javascript:be_trans.getDetailHebohBanget(<?php echo $no;?>)" href="#"><?php echo $dt->dfno; ?></a>
            <input type="hidden" id="idmemb<?php echo $no;?>" value="<?php echo $dt->dfno;?>">
        </td>
        <td><?php echo $dt->fullnm; ?></td>
        <td align="center"><?php echo date("d-m-Y", strtotime($dt->jointdt)); ?></td>
        <td  align="center"><?php echo $dt->tbvs; ?></td>
<!--        <td  align="center">--><?php //echo floor($dt->tkp*100)/100; ?><!--</td> -->
        <td  align="center"><?php echo intval($dt->tkp); ?></td>
    </tr>

            <?php
            $no++;}}
        ?>
</table>
<?php

if(empty($result)){
    echo setErrorMessage();
}
else{


    $filenm = "report";

    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filenm.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

//    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//    header('Content-Disposition: attachment;filename="report.xlsx"');
//    header('Cache-Control: max-age=0');

    //echo $filenm;
    echo "<table width=\"100%\" border = \"1\">";
    echo "<thead><tr><th colspan=\"23\" bgcolor=\"#lightgrey\">Report Distributor Sales By BV</th></tr>";

    echo "<tr>
            <th colspan=\"2\" align=\"center\">Period</th>
            <th colspan=\"3\" align=\"center\">0 To 99</th>
            <th colspan=\"3\" align=\"center\">100 To 199</th>
            <th colspan=\"3\" align=\"center\">200 To 299</th>
            <th colspan=\"3\" align=\"center\">300 To 399</th>
            <th colspan=\"3\" align=\"center\">400 To 499</th>
            <th colspan=\"3\" align=\"center\">500 To 599</th>
            <th colspan=\"3\" align=\"center\">600 Above</th>
          </tr>";

    echo "<tr>";
    echo "<th>Bonus Month</th>";
    echo "<th>Bonus Year</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "</tr></thead>";
    echo "<tbody>";
    $i = 1;
    foreach($result as $a) {
        echo "<tr>";

        echo "<td>".$a->bonusmonth."</td>";
        echo "<td>".$a->bonusyear."</td>";

        echo "<td>".number_format("$a->dist_0_to_99",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_0_to_99",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_0_to_99",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_100_to_199",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_100_to_199",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_100_to_199",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_200_to_299",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_200_to_299",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_200_to_299",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_300_to_399",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_300_to_399",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_300_to_399",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_400_to_499",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_400_to_499",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_400_to_499",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_500_to_599",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_500_to_599",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_500_to_599",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_600_above",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_600_above",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_600_above",0,",",".")."</td>";

        echo "</tr>";
        $i++;
    }
}

//print_r($result);

?>
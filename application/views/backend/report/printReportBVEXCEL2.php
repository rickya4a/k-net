<?php

if(empty($result)){
    echo setErrorMessage();
}
else{
    $filenm = "report.xls";
    header("Content-type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename='.$filenm.'');
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");

    echo "<table width=\"100%\" border = \"1\">";
    echo "<thead><tr><th colspan=\"23\" bgcolor=\"#lightgrey\">Report Distributor Sales By BV</th></tr>";

    echo "<tr>
            <th colspan=\"2\" align=\"center\">Period</th>
            <th colspan=\"3\" align=\"center\">0 To 100</th>
            <th colspan=\"3\" align=\"center\">101 To 200</th>
            <th colspan=\"3\" align=\"center\">201 To 300</th>
            <th colspan=\"3\" align=\"center\">301 To 400</th>
            <th colspan=\"3\" align=\"center\">401 To 500</th>
            <th colspan=\"3\" align=\"center\">501 To 600</th>
            <th colspan=\"3\" align=\"center\">601 Above</th>
          </tr>";

    echo "<tr>";
    echo "<th>Bonus Month</th>";
    echo "<th>Bonus Year</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "<th>Dis.(Pax)</th>";
    echo "<th>Total BV</th>";
    echo "<th>Total DP</th>";

    echo "</tr></thead>";
    echo "<tbody>";
    $i = 1;
    foreach($result as $a) {
        echo "<tr>";

        echo "<td>".$a->bonusmonth."</td>";
        echo "<td>".$a->bonusyear."</td>";

        echo "<td>".number_format("$a->dist_0_to_100",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_0_to_100",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_0_to_100",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_101_to_200",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_101_to_200",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_101_to_200",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_201_to_300",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_201_to_300",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_201_to_300",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_301_to_400",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_301_to_400",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_301_to_400",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_401_to_500",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_401_to_500",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_401_to_500",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_501_to_600",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_501_to_600",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_501_to_600",0,",",".")."</td>";

        echo "<td>".number_format("$a->dist_601_above",0,",",".")."</td>";
        echo "<td>".number_format("$a->bv_601_above",0,",",".")."</td>";
        echo "<td>".number_format("$a->dp_601_above",0,",",".")."</td>";

        echo "</tr>";
        $i++;
    }
}

//print_r($result);

?>
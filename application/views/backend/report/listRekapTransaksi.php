<?php
if (empty($result)) {
	echo setErrorMessage();
} else { ?>
	<table class="table table-bordered bootstrao-datatable datatable" width="100%">
		<thead>
			<tr>
				<th colspan='5'>List Rekap Transaksi</th>
			</tr>
			<tr>
				<th>Order No</th>
				<th>Tanggal Transaksi</th>
				<th>Total Pembayaran</th>
				<th>Total BV</th>
				<th>Jenis Produk</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 1;
			foreach ($result as $list) { ?>
				<tr>
					<td style="text-align: center">
						<a href="#" id="<?php echo $list->orderno ?>" onclick="javascript:be_trans.getDetailTrx(this)"><?php echo $list->orderno; ?></a>
					</td>
					<td style="text-align: center"><?php echo date("d F Y", strtotime($list->datetrans)); ?></td>
					<td style="text-align: right;"><?php echo $list->total_pay; ?></td>
					<td style="text-align: right;"><?php echo $list->total_bv; ?></td>
					<td style="text-align: right;"><?php echo $list->jenis_produk ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<script>
		$(document).ready(function () {
			$(All.get_active_tab() + " .datatable").dataTable({
				"aLengthMenu": [
					[
						10, 25, 50, 100, -1
					],
					[
						10, 25, 50, 100, 'All'
					]
				],
				"sPaginationType": "bootstrap",
				"oLanguage": {},
				"bDestroy": true
			});
			$(All.get_active_tab() + " .datatable").removeAttr('style');
		});
	</script>
<?php
}
?>
<?php
    if(empty($result)){

        setErrorMessage();
    }else{
?>

<table class="table table-bordered" style="width: 100%" id="datatable">
                <thead>
                    <tr>
                        <th>No. Voucher</th>
                        <th>Voucher Key</th>
                        <th>Status</th>
                        <th>Transaksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    
                        foreach($result as $dt){

                            echo"<td>".$dt->formno."</td>";
                            echo"<td>".$dt->vchkey."</td>";
                            if($dt->stat == 1){
                                echo"<td>Tersedia</td>";
                            }else{
                                echo"<td>Tidak berlaku/telah digunakan</td>";
                            }
                            echo"<td>".$dt->trcd."</td>";
                        }
                    ?>
                </tbody>
            </table>
            <script>
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>
<?php
    }
?>
<?php
if(empty($result)){
    setErrorMessage();
}else {
    ?>

    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable" id="datatable">
        <thead>
        <tr>
            <th>No</th>
            <th>Periode</th>
            <th>Tgl. Join</th>
            <th>Total Pembelanjaan</th>
            <th>Total BV</th>
            <th>Pembelanjaan LBC</th>
            <th>BV LBC</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $i = 1;
        foreach ($result as $dt) {
            //$warna=($i % 2 == 0) ? "white" : "#FFE4C4";

            if ($dt->TBV >= 600 && $dt->TBV_LBC >= 200) {
                $status = "SUCCESS";
                $color = "#008000";
            } else {
                $status = "FAILED";
                $color = "#8B0000";
            }

            $bln = date("m-Y", strtotime($dt->BNSPERIOD));
            if ($bln = '10-2018') {
                $bln2 = "Oktober - 2018";
            } elseif ($bln = '11-2018') {
                $bln2 = "November - 2018";
            } elseif ($bln = '12-2018') {
                $bln2 = "Desember - 2019";
            }

            ?>
            <tr>
                <td align="center"><?php echo $i; ?></td>
                <td align="center">
                    <input type="hidden" id="bnsperiod<?php echo $i; ?>"
                           value="<?php echo date("Y-m-d", strtotime($dt->BNSPERIOD)); ?>"/>
                    <input type="hidden" id="dfno<?php echo $i; ?>" value="<?php echo $dt->DFNO; ?>"/>
                    <?php
                    echo "<a onclick=\"getDetailLBC($i)\" class='btn btn-success'>".date("m-Y", strtotime($dt->BNSPERIOD))."</a>";
                    ?>
                </td>
                <td align="center">
                    <?php
                    echo date("d-m-Y", strtotime($dt->JOINTDT))
                    ?>
                </td>
                <td align="center"><?php echo number_format($dt->TDP, 0, ".", ".") ?></td>
                <td align="center"><?php echo $dt->TBV ?></td>
                <td align="center"><?php echo number_format($dt->TDP_LBC, 0, ".", ".") ?></td>
                <td align="center"><?php echo $dt->TBV_LBC ?></td>
                <td align="center">
                    <font color="<?php echo $color ?>">
                        <b>
                            <?php echo $status ?>
                        </b>
                    </font>
                </td>
                <td>
                    <?php
                    echo "<a onclick=\"getRekrutanLBC($i)\" class='btn btn-success'>Cek Rekrutan</a>";
                    ?>
                </td>
            </tr>
            <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
    <?php
}
?>


<script>
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>

<script>

    function getDetailLBC(param){

        var dfno = $("#dfno" +param).val()
        var bnsperiod = $("#bnsperiod" +param).val()

        $.ajax({
            url: All.get_url("det_lbc"),
            type: 'POST',
            data: {dfno: dfno, bnsperiod : bnsperiod},
            success:
                function(data){
                    $(".mainForm").hide();
                    $("#detailLBC").html(null);
                    $("#detailLBC").html(data);
                }
        });
    }

    function getRekrutanLBC(param){

        var dfno = $("#dfno" +param).val()
        var bnsperiod = $("#bnsperiod" +param).val()

        $.ajax({
            url: All.get_url("det_rekrutan_lbc"),
            type: 'POST',
            data: {dfno: dfno, bnsperiod : bnsperiod},
            success:
                function(data){
                    $(".mainForm").hide();
                    $("#detailLBC").html(null);
                    $("#detailRekrutanLBC").html(null);
                    $("#detailRekrutanLBC").html(data);
                }
        });

    }

    function backToFormLBC() {

        $("#detailLBC").html(null);
        $("#detailRekrutanLBC").html(null);
        $(".mainForm").show();
    }

</script>
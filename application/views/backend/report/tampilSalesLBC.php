<?php

    if(empty($hdr)){

        setErrorMessage();
    }else{
?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="10">Member LBC yang Melakukan Pembelanjaan Paket Produk LBC Periode <?php echo $tgl_hdr; ?></th>
    </tr>

    <tr>
        <th>No</th>
        <th>Member LBC Code</th>
        <th>Member LBC Name</th>
        <th>Status Keanggotaan</th>
        <th>Product Code</th>
        <th>Qty</th>
        <th>No. HP</th>
        <th>No. Home</th>
    </tr>

    <?php
        $no = 1;
        foreach($hdr as $a){

            $dfno = $a->dfno;
            $fullnm = $a->fullnm;
            $stat = $a->REMARK;
            $prdcd = $a->prdcd;
            $qtyord = $a->qtyord;
            $no_hp = $a->notel_HP;
            $no_rmh = $a->notel_HM;
    ?>

    <tr>
                <td align="center"><?php echo $no;?></td>
                <td><?php echo $dfno;?></td>
                <td align="center"><?php echo $fullnm;?></td>
                <td align="center"><?php echo $stat;?></td>
                <td align="center"><?php echo $prdcd;?></td>
                <td align="center"><?php echo $qtyord;?></td>
                <td><?php echo $no_hp;?></td>
                <td><?php echo $no_rmh;?></td>
    </tr>

    <?php
    $no++; }}
    ?>
</table>

<?php

if(empty($det)){

    setErrorMessage();
}else{
    ?>

<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
        <th colspan="10">Rekap Sales Paket Produk LBC Periode <?php echo $tgl_hdr; ?></th>
    </tr>

    <tr>
        <th>No</th>
        <th>Product LBC</th>
        <th>Qty</th>
    </tr>


    <?php
    $no = 1;
    foreach($det as $b){

        $prdcd = $b->prdcd;
        $jml = $b->JML;
        ?>

    <tr>
        <td align="center"><?php echo $no;?></td>
        <td align="center"><?php echo $prdcd;?></td>
        <td align="center"><?php echo $jml;?></td>
    </tr>

        <?php
        $no++; }}
    ?>
</table>
<?php

$pdf=new FPDF('L','mm', 'A4');
$pdf->Open();
$pdf->AddPage();
$pdf->Ln();
$pdf->setFont('Arial','B',9);
$pdf->SetFillColor(255,255,255);

//$panjangCellDis = 18;
$panjangCell = 20;
$lebarCell = 5;
$pjgHeader = $panjangCell * 14;

$pdf->SetFillColor(165,165,235);
$pdf->Cell($pjgHeader,$lebarCell,"Report Distributor Sales By BV",1,0,'C',true);
$pdf->Ln();

$pdf->Cell($panjangCell*2,$lebarCell,"Periode",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"0 To 99",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"100 To 199",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"200 To 299",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"300 To 399",1,0,'C',true);
$pdf->Ln();


$pdf->Cell($panjangCell,$lebarCell,"BULAN",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"TAHUN",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);
$pdf->SetFillColor(255,255,255);
$pdf->Ln();

if($result != null){

    $no = 1;
    foreach($result as $a){

        $dist_0_to_99 = number_format("$a->dist_0_to_99",0,",",".");
        $bv_0_to_99 = number_format("$a->bv_0_to_99",0,",",".");
        $dp_0_to_99 = number_format("$a->dp_0_to_99",0,",",".");

        $dist_100_to_199 = number_format("$a->dist_100_to_199",0,",",".");
        $bv_100_to_199 = number_format("$a->bv_100_to_199",0,",",".");
        $dp_100_to_199 = number_format("$a->dp_100_to_199",0,",",".");

        $dist_200_to_299 = number_format("$a->dist_200_to_299",0,",",".");
        $bv_200_to_299 = number_format("$a->bv_200_to_299",0,",",".");
        $dp_200_to_299 = number_format("$a->dp_200_to_299",0,",",".");

        $dist_300_to_399 = number_format("$a->dist_300_to_399",0,",",".");
        $bv_300_to_399 = number_format("$a->bv_300_to_399",0,",",".");
        $dp_300_to_399 = number_format("$a->dp_300_to_399",0,",",".");

        $pdf->SetFillColor(255,255,255);
        $pdf->Cell($panjangCell,$lebarCell,"$a->bonusmonth",1,0,'C',true);
        $pdf->Cell($panjangCell,$lebarCell,"$a->bonusyear",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_0_to_99",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_0_to_99",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_0_to_99",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_100_to_199",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_100_to_199",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_100_to_199",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_200_to_299",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_200_to_299",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_200_to_299",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_300_to_399",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_300_to_399",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_300_to_399",1,0,'C',true);


        $pdf->Ln();

        $no++;
    }
}

$pdf->SetFillColor(165,165,235);
$pdf->Cell($panjangCell*2,$lebarCell,"Periode",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"400 To 499",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"500 To 599",1,0,'C',true);

$pdf->Cell($panjangCell*3,$lebarCell,"600 Above",1,0,'C',true);

$pdf->Ln();

$pdf->Cell($panjangCell,$lebarCell,"BULAN",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"TAHUN",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);

$pdf->Cell($panjangCell,$lebarCell,"Dis.(Pax)",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total BV",1,0,'C',true);
$pdf->Cell($panjangCell,$lebarCell,"Total DP",1,0,'C',true);
$pdf->SetFillColor(255,255,255);
$pdf->Ln();

if($result != null){

    $no = 1;
    foreach($result as $a){

        $bonusmonth = $a->bonusmonth;
        $bonusyear = $a->bonusyear;

        $dist_400_to_499 = number_format("$a->dist_400_to_499",0,",",".");
        $bv_400_to_499 = number_format("$a->dist_400_to_499",0,",",".");
        $dp_400_to_499 = number_format("$a->dist_400_to_499",0,",",".");

        $dist_500_to_599 = number_format("$a->dist_500_to_599",0,",",".");
        $bv_500_to_599 = number_format("$a->dist_500_to_599",0,",",".");
        $dp_500_to_599 = number_format("$a->dist_500_to_599",0,",",".");

        $dist_600_above = number_format("$a->dist_600_above",0,",",".");
        $bv_600_above = number_format("$a->bv_600_above",0,",",".");
        $dp_600_above = number_format("$a->dp_600_above",0,",",".");

        $pdf->SetFillColor(255,255,255);
        $pdf->Cell($panjangCell,$lebarCell,"$bonusmonth",1,0,'C',true);
        $pdf->Cell($panjangCell,$lebarCell,"$bonusyear",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_400_to_499",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_400_to_499",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_400_to_499",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_500_to_599",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_500_to_599",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_500_to_599",1,0,'C',true);

        $pdf->SetFillColor(85,255,235);
        $pdf->Cell($panjangCell,$lebarCell,"$dist_600_above",1,0,'C',true);
        $pdf->SetFillColor(255,80,80);
        $pdf->Cell($panjangCell,$lebarCell,"$bv_600_above",1,0,'C',true);
        $pdf->SetFillColor(80,255,80);
        $pdf->Cell($panjangCell,$lebarCell,"$dp_600_above",1,0,'C',true);

        $pdf->Ln();
        $no++;

    }
}

$pdf->Output();
?>



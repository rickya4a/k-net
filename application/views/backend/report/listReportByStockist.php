<?php

if($result == null){

    setErrorMessage();
}else {
    ?>

    <table width = '100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
    <thead>
    <tr>
        <td style="text-align: center" rowspan="2"><b>No.</b></td>
        <td style="text-align: center" rowspan="2"><b>Stockist</b></td>
        <td style="text-align: center" colspan="5"><b>Sales</b></td>
        <td style="text-align: center" colspan="2"><b>Distributor</b></td>
        <td style="text-align: center" colspan="2"><b>Total</b></td>
    </tr>
    <tr>
        <td align="center"><b>Date</b></td>
        <td align="center"><b>Period</b></td>
        <td align="center"><b>Sales Order</b></td>
        <td align="center"><b>SSR</b></td>
        <td align="center"><b>CN</b></td>

        <td align="center"><b>Code</b></td>
        <td align="center"><b>Name</b></td>

        <td align="center"><b>DP</b></td>
        <td align="center"><b>BV</b></td>
    </tr>
    </thead>
    <tbody>



    <?php

    $no = 1;
    foreach ($result as $a) {

        ?>

        <tr>
            <td align="center"><?php echo $no; ?></td>
            <td><?php echo $a->sc_dfno; ?></td>
            <td><?php echo date("d-m-Y", strtotime($a->trdt)) ?></td>
            <td align="center"><?php echo date("d-m-Y", strtotime($a->bnsperiod)) ?></td>
            <td align="center"><?php echo $a->trcd ?></td>
            <td align="center"> - </td>
            <td> - </td>
            <td><?php echo $a->dfno ?></td>
            <td><?php echo $a->fullnm ?></td>
            <td><?php echo $a->tdp ?></td>
            <td><?php echo $a->tbv ?></td>
        </tr>

        <?php $no++; }?>
    </tbody>
    </table>
    <script type="text/javascript">
        All.set_datatable();
    </script>
<?php }?>
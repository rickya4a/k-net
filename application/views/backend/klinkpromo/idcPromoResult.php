<?php
	if($result == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>
<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
    	<th colspan="2">Initiative Dynamic Challenge</th>
    </tr>
    <tr>
    	<td width="25%">ID Member</td>
    	<td><?php echo $result[0]->dfno; ?></td>
    </tr>
    <tr>
    	<td width="25%">Nama Member</td>
    	<td><?php echo $result[0]->fullnm; ?></td>
    </tr>
    <!--<tr>
    	<td width="25%">Total Poin</td>
    	<td><?php echo $result[0]->checkPoin; ?></td>
    </tr>
    <tr>
    	<td width="25%">Total Level</td>
    	<td><?php echo $result[0]->totlvl; ?></td>
    </tr>
    <tr>
    	<td width="25%">Jml Downline yang belanja >= 400BV</td>
    	<td><?php echo $result[0]->tot400Above; ?></td>
    </tr>-->
    <!--<tr>
    	<td width="25%">Jml Downline yang belanja &lt; 400BV</td>
    	<td><?php echo $result[0]->tot400Less; ?></td>
    </tr>-->
    <!--<tr>
    	<td width="25%">Jml Rekrutan yang belanja >= 400BV</td>
    	<td><?php echo $result[0]->totRU400; ?></td>
    </tr>-->
    <!--<tr>
    	<td width="25%">Jml Rekrutan yang belanja &lt; 400BV</td>
    	<td><?php echo $result[0]->totRL400; ?></td>
    </tr>
    <tr>
    	<td width="25%">Jml Leg yang memenuhi syarat</td>
    	<td><?php echo $result[0]->checkVal; ?></td>
    </tr>
    <tr>
    	<td width="25%">Total Semua Rekrutan yang Belanja</td>
    	<td><?php echo $result[0]->totBVR; ?></td>
    </tr>-->
    <tr>
    	<td width="25%">Detail Data per Bonus Period</td>
    	<td>
    	<select id="bnsperiod" name="bnsperiod" onchange="be_promo.showIDCDetail()">
    		<option value="">--Select here--</option>
    		<?php
    		  foreach($bnsPeriod as $dtax) {
    		  	echo "<option value=\"$dtax->bnsperiod\">$dtax->bnsperiod</option>";
    		  }
    		?>
    	</select>
    	</td>
    </tr>	
</table>  
<div class="result2"></div>      	
<?php
	}
?>
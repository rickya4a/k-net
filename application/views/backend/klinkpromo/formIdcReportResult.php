<?php
	if($result == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>
<table style="width: 90%" align="center" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
    	<th colspan="7"><div align="center">Detail Data</div></th>
    </tr>
    <tr>
    	<th>No</th>
    	<th>ID Member</th>
    	<th>Nama Member</th>	
    	<th>BV</th>
    	<th>Poin</th>
    </tr>
    <?php
       $i = 1;
       foreach($result as $data) {
       	echo "<tr>";
		echo "<td align=right>$i&nbsp;</td>";
		echo "<td align=center>$data->dfno</td>";
		echo "<td align=center>$data->fullnm</td>";
		//echo "<td align=center>$data->jointdt</td>";
		echo "<td align=center>$data->totBVR</td>";
		echo "<td align=right>$data->checkPoin&nbsp;</td>";   
		echo "</tr>";  
		$i++; 
       }
    ?>
</table>
<?php
}
?>    
<?php
	if($result == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>
<table style="width: 80%" align="center" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
    	<th colspan="2"><div align="center">Summary Data</div></th>
    </tr>
    <tr>
    	<td width="30%">Total Poin</td>
    	<td><?php echo $resultHeader[0]->checkPoin; ?></td>
    </tr>
    <tr>
    	<td>Jml Rekrutan yang belanja >= 400BV</td>
    	<td><?php echo $resultHeader[0]->totRU400; ?></td>
    </tr>
    <tr>
    	<td width="25%">Total Level</td>
    	<td><?php echo $resultHeader[0]->totlvl; ?></td>
    </tr>
</table>
	
<table style="width: 80%" align="center" class="table table-striped table-bordered bootstrap-datatable datatable"> 
	<tr>
    	<th colspan="7"><div align="center">Detail Data</div></th>
    </tr>   
    <tr>
    	<th>No</th>
    	<th>Leg</th>
    	<th>ID Member</th>
    	<th>Nama Member</th>
    	<th>Join Date</th>
    	<th>Recruiter</th>
    	<th>BV</th>
    </tr>
    <?php
       $i = 1;
       foreach($result as $data) {
       	echo "<tr>";
		echo "<td align=right>$i&nbsp;</td>";
		echo "<td align=center>$data->leg</td>";
		echo "<td align=center>$data->bDfno</td>";
		echo "<td>$data->bFullnm</td>";
		echo "<td align=center>$data->jointdt</td>";
		echo "<td align=center>$data->sfno_reg</td>";
		echo "<td align=right>$data->bv&nbsp;</td>";   
		echo "</tr>";  
		$i++; 
       }
    ?>
</table>
<?php
}
?>    
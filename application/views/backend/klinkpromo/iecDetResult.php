<?php
	if($detail == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>
<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable">
    <tr>
    	<th colspan="2">Initiative Elite Challenge</th>
    </tr>
    <tr>
    	<td width="25%">ID Member</td>
    	<td><?php echo $header[0]->dfno; ?></td>
    </tr>
    <tr>
    	<td width="25%">Nama Member</td>
    	<td><?php echo $header[0]->fullnm; ?></td>
    </tr>
    <tr>
        <td>Bonus Month</td>
        <td><?php echo $bnsmonth;?></td>
    </tr>
    <tr>
        <td colspan="2"><input type="button" onclick="All.back_to_form(' .nextForm1',' .mainForm')" class="btn btn-small btn-warning" value="&lt;&lt; Back"/></td>
    </tr>
</table>
<br />
<table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable" id="test">
    <thead>
    <tr>
    	<th colspan="4">Detail Recruit & BV</th>
    </tr>
    
    <tr>
        <th class="no-sort">No</th>
        <th class="no-sort">ID Member</th>
        <th class="no-sort">Nama Member</th>
        <th class="no-sort">BV</th>
    </tr>
    </thead>
    <tbody>
    <?php 
        $no = 1;
        foreach($detail as $list){
            
    ?>
    
    <tr>
        <td style="text-align: center;"><?php echo $no;?></td>
        <td style="text-align: center;"><?php echo $list->dfno;?></td>
        <td style="text-align: center;"><?php echo $list->fullnm;?></td>
        <td style="text-align: center;"><?php echo $list->ppv;?></td>
    </tr>
    
    <?php $no++; } ?>
    </tbody>
</table>    	
<?php
	}
?>
<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " #test").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"bDestroy": true,
        "aoColumnDefs" : [ {
            "bSortable" : false,
            "aTargets" : [ "no-sort" ]
        } ]
        
	});
    
 });

</script>
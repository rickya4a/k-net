<div class="mainForm">
	<form class="form-horizontal" id="checkIDC">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Period</label>
				<div class="controls">
					<input type="text" id="rep_bnsperiod" name="rep_bnsperiod" value="<?php echo date("Y-m")."-"."01"; ?>" />				
				</div>
				<label class="control-label" for="typeahead">Order By</label>
				<div class="controls">
					<select id="orderby" name="orderby">
			    		<option value="totBVR">BV</option>
			    		<option value="dfno">ID</option>
			    		<option value="fullnm">Name</option>
			    		<!--<option value="jointdt">Join Date</option>-->
			    		<option value="checkPoin">Poin</option>
			    	</select>					
				</div>
				<label class="control-label" for="typeahead">&nbsp</label>
				<div class="controls"  id="inp_btn">
					<input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit"
					onclick="All.ajaxFormPost(this.form.id,'be/promo/idc/report/result')" />
					<input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
				</div>
			</div>
			<!-- end control-group -->
		</fieldset>

		<div class="result"></div>
		<div class="result2"></div>
	</form>
</div><!--/end mainForm-->

<?php
  if($listPrd == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listPrd><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=8>List E-Ticket</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=7%>No</th><th width=15%>Kode E-Ticket</th><th width=15%>Nama</th><th>Lokasi</th><th width=20%>Tanggal Event</th><th width=10%>Status</th><th width=10%>Action</th><th width=10%>Set Merchandise</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listPrd as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td>".$i."</td>";
				echo "<td><div align=center>$list->etiket_code_id</div></td>";
                echo "<td><div align=center>$list->nama</div></td>";
                echo "<td><div align=center>$list->lokasi</div></td>";
				echo "<td><div align=center>$list->event_date2</div></td>";
				$style="style='width: 100px'";
                if($list->status==0) {
                	echo "<td>";
					echo "<select $style id=\"status_aktif\" name=\"status_aktif\" onchange=\"setStatusEtiket('$list->id',this.value)\">";
					echo "<option value=\"0\" selected=\"selected\">Aktif</option>";
					echo "<option value=\"1\">Non Aktif</option>";
					echo "</select>";
					echo "</td>";
                } else {
					echo "<td>";
					echo "<select $style id=\"status_aktif\" name=\"status_aktif\" onchange=\"setStatusEtiket('$list->id',this.value)\">";
					echo "<option value=\"0\">Aktif</option>";
					echo "<option value=\"1\" selected=\"selected\">Non Aktif</option>";
					echo "</select>";
					echo "</td>";
                }
                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
				/*if($username != "DION") {
					$arr = array(
						"update" => "Product.getUpdateEtiket('$list->id')",
					   // "delete" => "#"
					);
				} else {*/
					$arr = array(
						"update" => "Product.getUpdateEtiket2('$list->id')",
					   // "delete" => "#"
					);
				//}
                echo btnUpdateDelete($arr);
				//$urlx = "'etiket/merchandise/set/'".$list->id;
		?>
				<td><input type="button" class="btn btn-mini btn-primary" value="Setting Merchandise" onclick="All.ajaxShowDetailonNextForm('etiket/merchandise/set/<?php echo $list->id; ?>')" /></td>
				
		<?php	
        echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
		
	   function setMerchandise(id_acara) {
	   
	   }		
	   
	   function setStatusEtiket(idacara, status) {
	        var r = confirm("Anda yakin ingin mengubah status Etiket?");
			if (r == true) {
			    All.set_disable_button();
				//All.get_wait_message();
				$.ajax({
					url: All.get_url("etiket/status/update"),
					type: 'POST',
					dataType: 'json',
					data: {id: idacara, status: status},
					success:
					function(data) {
					    All.set_enable_button();
						alert(data.message);
					},
					error: function (xhr, ajaxOptions, thrownError) {
						 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
					}
				});
			} 
			
			
	   }
    </script>
	<?php
  }
?>

<link href="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<script src="<?php echo site_url();?>vendors/moment/min/moment.min.js"></script>
<script src="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<script>
	$(document).ready(function(){

		$('#exp_date_online').daterangepicker({
			singleDatePicker: true,
			timePicker: true,
			timePicker24Hour: true,
			singleClasses: "picker_3",
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});

		$('#exp_date_offline').daterangepicker({
			singleDatePicker: true,
			timePicker: true,
			timePicker24Hour: true,
			singleClasses: "picker_3",
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#bird_date').daterangepicker({
			singleDatePicker: true,
			timePicker: true,
			timePicker24Hour: true,
			singleClasses: "picker_3",
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#event_date').daterangepicker({
			singleDatePicker: true,
			timePicker: true,
			timePicker24Hour: true,
			singleClasses: "picker_3",
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});







	});



</script>


<div class="mainForm">
  <form class="form-horizontal"  id="formInputProduct" enctype="multipart/form-data" >
    <fieldset>

		<div class="control-group">
			<div class="controls" style="margin-left: 0px;">

      	<table width="100%" border="0">
      		<tr>
      			<td  width="15%" align="right">Event Name&nbsp;</td>
      			<td  width="35%">
      				<!--<input type="text" class=" span20" placeholder="<?php echo placeholderCheck(); ?>" id="nm_event" name="nm_event" required="required" />-->
					<input type="hidden" class=" span20" id="id" name="id"  />
					<select id="pil_event" name="pil_event" onchange="pilihjenisAcara()">
						<?php
						foreach($listTipeAcara as $dta) {
							echo "<option value=\"$dta->shortname|$dta->cat_id\">$dta->category_desc</option>";
						}
						?>
					</select>
					<input type="hidden" class=" span20" id="event_cat_id" name="event_cat_id" value="<?php echo $listTipeAcara[0]->cat_id; ?>"  />
					<input type="hidden" class=" span20" id="nm_event" name="nm_event" value="<?php echo $listTipeAcara[0]->shortname; ?>"  />
				</td>

      		</tr>

			<tr>
				<td  width="15%" align="right">Event Location&nbsp;</td>
				<td  width="35%">
					<!--<input type="text" class=" span20" placeholder="<?php echo placeholderCheck(); ?>" id="loc_event" name="loc_event" required="required"/>-->
					<select id="pil_loc" name="pil_loc" onchange="pilihLocation(this.value)">
					  	
					  <?php
					  foreach($listKota as $dtaKota) {
						echo "<option value=\"$dtaKota->kode_kota|$dtaKota->nama_kota\">$dtaKota->nama_kota</option>";
					  }
					  ?>
					</select>
					<input type="hidden" class=" span20" id="loc_id" name="loc_id" value="<?php echo $listKota[0]->kode_kota; ?>"  />
					<input type="hidden" class=" span20" id="loc_event" name="loc_event" value="<?php echo $listKota[0]->nama_kota; ?>"  />
				</td>

			</tr>
      		<tr>
      			<td  align="right">Max Online&nbsp;</td>
      			<td  >
					<input type="number" class=" span5" placeholder="Numeric/decimal" id="max_online" name="max_online" required="required"/>
				</td>
				<td align="right">Event Date &nbsp;</td>
      			<td>
					<input class="span20" id="event_date" name="event_date" type="text" required="required">
      			</td>
      		</tr>
      		<tr>
      			<td align="right">Max Offline&nbsp;</td>
				<td >
					<input type="number" class=" span5" placeholder="Numeric/decimal" id="max_offline" name="max_offline" required="required"/>
				</td>
				<td  align="right">Early Bird &nbsp;</td>
      			<td >
					<input class="span20" id="bird_date" name="bird_date" type="text" required="required">
      		</td>
      		</tr>
      		
		    <tr>
 				<td width="15%" align="right">Pembicara&nbsp;</td>
				<td width="35%">
					<input type="text" class=" span20"   id="pembicara" name="pembicara" />
				</td>
				<td width="15%" align="right">Early Bird Price&nbsp;</td>
				<td   width="25%">
					<input type="number" class="span5" placeholder="Numeric/decimal"  id="price_bird" name="price_bird" required="required"/>
				</td>
      		</tr>
      		
            <tr>
<!--				<td align="right">Online Exp &nbsp;</td>-->
<!--				<td  >-->
<!--					<input class="dtpicker " id="exp_date_online" name="exp_date_online" type="text" required="required">-->
<!---->
<!--				</td>-->

				<td align="right">Online Exp
				</td>
				<td >
					<input  id="exp_date_online" class="span20" name="exp_date_online" type="text" required="required">
				</td>


				<td align="right">Offline Exp &nbsp;</td>
				<td>
					<input class="span20" id="exp_date_offline" name="exp_date_offline" type="text" required="required">

				</td>
      			<!---->
      		</tr>
			
			<tr>
				<td width="15%" align="right">Online Price&nbsp;</td>
				<td   width="35%">
					<input type="number" class=" span5" placeholder="Numeric/decimal"  id="price_online" name="price_online" required="required"/>
				</td>
				<td width="15%" align="right">Offline Price&nbsp;</td>
				<td width="35%">
					<input type="number" class=" span5" placeholder="Numeric/decimal"  id="price_offline" name="price_offline" required="required" />
				</td>
			</tr>
			<tr>
				<td align="right">Remark&nbsp;</td>
				<td >
					<input type="text" class=" span20"   id="remark" name="remark" />
				</td>
				<td align="right">Kode E-Ticket</td>
				<td >
					<input readonly="readonly" id="etiket_code_id" name="etiket_code_id" type="text" class=" span20"   id="remark" />
				</td>	
			</tr>



      	</table>
			</div>
				<?php
//				//$input = "All.inputFormData('product/save', this.form.id)";
				$input = "Product.saveInputEtiket('etiket/save', 'formInputProduct')";
				$update = "All.updateFormDataEnctypeNoListReload('etiket/update','formInputProduct', 'etiket/list')";
				$view = "All.getListData('etiket/list')";
//				echo inputHidden("country_id", "ID");
//				echo inputHidden("hq_id", "BID06");
//				echo inputHidden("branch_id", "B001");
				echo button_set($input, $update, $view);
				?>

<!--			<button type="submit" class="btn btn-success">Submit</button>-->


		</div>
     </fieldset>
     
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->

<script>
	$(document).ready(function() {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());

		All.set_datatable();
	});
	
	function pilihjenisAcara() {
		var pil_event = $(All.get_active_tab() + " #pil_event").val();
		res = pil_event.split("|");
		//console.log(res);
		$(All.get_active_tab() + " #event_cat_id").val(res[1]);
		$(All.get_active_tab() + " #nm_event").val(res[0]);
	}
	
	function pilihLocation(nilai){
		//var pil_event = $(All.get_active_tab() + " #pil_event").val();
		res = nilai.split("|");
		//console.log(res);
		$(All.get_active_tab() + " #loc_event").val(res[1]);
		$(All.get_active_tab() + " #loc_id").val(res[0]);
	} 
</script>




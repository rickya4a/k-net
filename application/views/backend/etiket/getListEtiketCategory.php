<?php
  if($listCatEtiket == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listCatTiket>";
	 echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=5>List Category E-Ticket</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=10%>No</th>";
	   echo "<th>Nama Kategori</th>";
	   echo "<th width=20%>Singkatan</th>";
	   echo "<th width=15%>Aktif</th><th width=15%>Update</th></tr></thead>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listCatEtiket as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
                echo "<td><div align=center><input type=\"hidden\" id=\"cat_tiket_id$i\" value=\"$list->cat_id\" />$list->category_desc</div></td>";
                echo "<td><div align=center>$list->shortname</div></td>"; 
				if($list->status == "1") {	
					echo "<td><div align=center>Ya</div></td>";
				} else {
					echo "<td><div align=center>Tidak</div></td>";
				}
                /*echo "<td><div align=\"center\">";
                echo "<a class=\"btn btn-mini btn-info\" onclick=\"Gallery.getUpdateGallery($i)\"><i class=\"icon-edit icon-white\"></i></a>";
                echo "&nbsp;<a class=\"btn btn-mini btn-danger\" onclick=\"Gallery.deleteGallery($i)\"><i class=\"icon-trash icon-white\"></i></a>";
                echo "</div></td>";*/
                //$ondelete = "All.deleteFormData('user/delete/', '$list->username', 'user/list')";
                $arr = array(
				    "update" => "getUpdateEtiketCategory($i)",
				    //"delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody>";
    echo "</table>
	<input type=\"hidden\" id=\"sdsd\" value=\"3\" />
	</form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
		
	  function getUpdateEtiketCategory(param) {
		All.set_disable_button();
		//All.get_image_load();
		var cat_id = $(All.get_active_tab() + " #cat_tiket_id" +param).val();
		//console.log(param);
		console.log(cat_id);
		$.ajax({
            url: All.get_url('etiket/cat/id') +"/" +cat_id ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					All.formUpdateActivate();
					var arrayData = data.arrayData;
					$(All.get_active_tab() + " #cat_id").val(arrayData[0].cat_id);
					$(All.get_active_tab() + " #category_desc").val(arrayData[0].category_desc);
					$(All.get_active_tab() + " #shortname").val(arrayData[0].shortname);
					$(All.get_active_tab() + " #status").val(arrayData[0].status);
				} else {
				
				}
				
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	  }	
    </script>
	<?php
  }
?>

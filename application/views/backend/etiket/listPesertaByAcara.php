<?php
if($report == null) {
    echo emptyResultDiv();
} else {
    backToMainForm();
    ?>
    
    <form id=listoffice>
        <?php
        echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
        echo "<thead><tr><th colspan=9>E-tiket Report</th></tr>";
        echo "<tr bgcolor=#f4f4f4><th>No Trx</th><th>No Tiket</th>";
		//echo "<th>No Member</th><th>Nama Member</th>";
		echo "<th>No Peserta</th><th>Nama Peserta</th><th>Tanggal Transaksi</th><th>Pembayaran</th><th>Biaya</th><th>Resend SMS</th></thead></tr>";
        echo "<tbody>";
        $i = 1;
        $total=0;
        foreach ($report as $list) {
            echo "<tr id=\"$i\">";
			echo "<td><div align=center>$list->order_no</div></td>";
            echo "<td><div align=center>$list->notiket</div></td>";

            //echo "<td><div align=center>$list->dfno</div></td>";
            //echo "<td><div align=center>$list->fullnm</div></td>";
            echo "<td><div align=center>$list->valid_dfno</div></td>";
            echo "<td><div align=center>$list->valid_fullnm</div></td>";
            

            echo "<td><div align=center>".date("d-M-Y", strtotime($list->tglbeli))."</div></td>";
            if($list->flag_online!='W'){
                echo "<td><div align=center>Offline</div></td>";
            }else{
                echo "<td><div align=center>Online</div></td>";
            }
            echo "<td><div align=center>".number_format($list->price)."</div></td>";

//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);
			echo "<td><input class='btn btn-mini btn-warning' value='Send SMS' onclick=\"sendSmsAcara('notiket','$list->notiket')\" /></td>";
            echo "</tr>";
			
			$total += $list->price;
            $i++;
        }
        echo "</tbody>
        <tfoot>
        <tr><th colspan=6>Total</th><th>".number_format($total)."</th><th>&nbsp;</th></tr>
        </tfoot>
        </tr>";
        echo "</table>";



     backToMainForm();
	?>

  
         </form>


	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

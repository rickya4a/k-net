<?php
$pdf=new FPDF('P','mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)

$pdf->AddPage();
//$pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
//$pdf->Cell(50,10,$img,'L',0,0,'R');
$pdf->SetFont('Courier','', 8);
$thnCheck = '2017';

function NumberToMonth($bln)
{
    switch($bln)
    {
        case 1 :
            $jum = "january";
            break;
        case 2 :
            $jum = "february";
            break;
        case 3 :
            $jum = "march";
            break;
        case 4 :
            $jum = "april";
            break;
        case 5 :
            $jum = "may";
            break;
        case 6 :
            $jum = "june";
            break;
        case 7 :
            $jum = "july";
            break;
        case 8 :
            $jum = "august";
            break;
        case 9 :
            $jum = "september";
            break;
        case 10 :
            $jum = "october";
            break;
        case 11 :
            $jum = "november";
            break;
        case 12 :
            $jum = "december";
            break;
    }
    return $jum;
}

$lebarCell = 4;
$pdf->Ln();
$pdf->SetXY(6,6);
$titleCol1 = 30;
$titleCol2 = 100;
$titleCol3 = 35;
$titleCol4 = 35;


$pdf->SetFont('Arial','B', 8);

{
    //        $pdf->Cell(198,5,"BONUS STATEMENT REPORT $bulan $dta2->bonusyear",1,0,'C', true);
    $pdf->SetFont('Arial','B', 20);
    $pdf->SetFillColor(255,255,255); // white

    $pdf->Cell(200,9,"E - tiket Report",0,0,'C', true);
    $pdf->Ln();

    $pdf->SetFont('Arial','B', 12);

}
$pdf->SetLeftMargin(6);
$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);


$pdf->SetTextColor(0,0,0);

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','B', 8);

$pdf->Ln();
$pdf->Ln();
$pdf->Cell(18,$lebarCell,"No Tiket ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(25,$lebarCell,"No Member ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(25,$lebarCell,"Nama Member ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(25,$lebarCell,"No Peserta ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(25,$lebarCell,"Nama Peserta ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(20,$lebarCell,"Acara ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(20,$lebarCell,"Tanggal TRX ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(20,$lebarCell,"Pembayaran ",1,0,'L',true); 	// its similar with TD in HT
$pdf->Cell(20,$lebarCell,"Biaya ",1,0,'L',true); 	// its similar with TD in HT
$total=0;
$i=0;
$pdf->Ln();
$pdf->SetFont('Courier','', 8);

foreach($report as $dxd)
{
    $i++;
    if(strlen ($dxd->fullnm)>15){
        $nm=substr_replace($dxd->fullnm,"...",12);
    }else
    {$nm= $dxd->fullnm;}
    if(strlen ($dxd->valid_fullnm)>15){
        $nm2=substr_replace($dxd->valid_fullnm,"...",12);
    }else
    {$nm2= $dxd->valid_fullnm;}

    $pdf->Cell(18,$lebarCell,$dxd->notiket,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(25,$lebarCell,$dxd->dfno,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(25,$lebarCell,$nm,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(25,$lebarCell,$dxd->valid_dfno,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(25,$lebarCell,$nm2,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(20,$lebarCell,$dxd->nama."-".$dxd->lokasi,1,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell(20,$lebarCell,date("d-m-Y", strtotime($dxd->tglbeli)),1,0,'L',true); 	// its similar with TD in HT
    $total+=$dxd->price;

    if($dxd->flag_online!='W'){
        $pdf->Cell(20,$lebarCell,'Offline',1,0,'L',true); 	// its similar with TD in HT
    }
    else
    {
        $pdf->Cell(20,$lebarCell,'Online',1,0,'L',true); 	// its similar with TD in HT

    }
    $pdf->Cell(20,$lebarCell,number_format($dxd->price),1,0,'L',true); 	// its similar with TD in HT

    $pdf->Ln();
}
$pdf->SetFont('Courier','B', 8);

$pdf->Cell(178,$lebarCell,'Total',1,0,'L',true);
$pdf->Cell(20,$lebarCell,number_format($total),1,0,'L',true);


$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);
$pdf->SetTextColor(0,0,255);



$title = "ReportEtiket.pdf";
$pdf->SetTitle($title);
$pdf->Output();

?>
<?php
if($report == null) {
    echo emptyResultDiv();
} else {
    backToMainForm();
    ?>
    
    <form id="listPesertaAcara">
        <?php
        echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
        echo "<thead><tr><th colspan=9>E-tiket Report</th></tr>";
        echo "<tr bgcolor=#f4f4f4>";
		echo "<th width='3%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		echo "<th>No Trx</th><th>No Tiket</th>";
		//echo "<th>No Member</th><th>Nama Member</th>";
		echo "<th>No Peserta</th><th>Nama Peserta</th><th>Tanggal Transaksi</th><th>Pembayaran</th><th>Biaya</th><th>Resend SMS</th></thead></tr>";
        echo "<tbody>";
        $i = 1;
        $total=0;
        foreach ($report as $list) {
            echo "<tr id=\"$i\">";
			echo "<td align=center><input type=checkbox id=pil$i name=order_no[] value=\"$list->order_no\" /></td>";
			echo "<td><div align=center>$list->order_no</div></td>";
            echo "<td><div align=center>$list->notiket</div></td>";

            //echo "<td><div align=center>$list->dfno</div></td>";
            //echo "<td><div align=center>$list->fullnm</div></td>";
            echo "<td><div align=center>$list->valid_dfno</div></td>";
            echo "<td><div align=center>$list->valid_fullnm</div></td>";
            

            echo "<td><div align=center>".date("d-M-Y", strtotime($list->tglbeli))."</div></td>";
            if($list->flag_online!='W'){
                echo "<td><div align=center>Offline</div></td>";
            }else{
                echo "<td><div align=center>Online</div></td>";
            }
            echo "<td><div align=center>".number_format($list->price)."</div></td>";

//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);
			echo "<td><input class='btn btn-mini btn-warning' value='Send SMS' onclick=\"sendSmsAcara('notiket','$list->notiket')\" /></td>";
            echo "</tr>";
			
			$total += $list->price;
            $i++;
        }
        echo "</tbody>
        <tfoot>
        <tr><th colspan=7>Total</th><th>".number_format($total)."</th><th>&nbsp;</th></tr>
        </tfoot>
        </tr>";
        echo "</table>";



     backToMainForm();
	 
	?>
	<input type="hidden" name="cat_id" id="cat_id" value="<?php echo $report[0]->cat_id."**"."1"; ?>" />
	<input type="hidden" name="kode_acara" id="kode_acara" value="<?php echo $report[0]->id_etiket; ?>" />
    <input type="button" name="btn_update" value="Pindah Jadwal" class="btn btn-mini btn-primary" onclick="pindahJadwal()" />
    
  
         </form>


	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
		
	  function pindahJadwal() {
		  All.set_disable_button();
		  $.ajax({
            url: All.get_url('etiket/pindahJadwal'),
            type: 'POST',
			data: $(All.get_active_tab() + " #listPesertaAcara").serialize(),
            success:
            function(data){
            	All.set_enable_button();
            	$(All.get_active_tab() + " .nextForm1").hide();
                All.clear_div_in_boxcontent(".nextForm2");
                $(All.get_active_tab() + " .nextForm2").html(data);  
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
          });	
	  }		
    </script>
	<?php
  }
?>

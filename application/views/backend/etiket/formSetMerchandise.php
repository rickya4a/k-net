<?php
    if(empty($listTipe)){
        echo setErrorMessage("Tipe Merchandise tidak ada..");
        ?>

        <input value="<< Back " type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
        <br>

        <?php
    }else{
?>
<form class="form-horizontal" id="formMembKnet" method="POST" action="<?php echo base_url('member/knet/print') ?>">
    <fieldset>      
      <div class="control-group">       
	    <label class="control-label" for="typeahead">Acara</label>
		<div class="controls">
			<input type="text" style="width: 230px;" readonly="readonly" id="nama_event" name="nama_event" value="<?php echo $detailAcara[0]->nama." - ".$detailAcara[0]->lokasi ?>" /> Harga Tiket <input type="text" readonly="readonly" id="harga_tiket" name="harga_tiket" value="<?php echo $detailAcara[0]->price_online ?>" />
			<input type="hidden" id="etiket_id" name="etiket_id" value="<?php echo $detailAcara[0]->id  ?>" />
		</div>
		<!--
        <label class="control-label" for="typeahead">Tipe Merchandise</label>
		<div class="controls">
			<select id="pil_tipe_mer" name="pil_tipe_mer" onchange="pilihTipeMerchandise(this.value)">
			   <option value="">-- Pilih --</option>
			   <?php
			   foreach($listTipe as $dtax) {
				echo "<option value=\"$dtax->tipe_merchandise|$dtax->ukuran|$dtax->warna\">$dtax->tipe_merchandise</option>";
			   }
			   ?>
			</select>
			<input type="hidden" id="id_etiket" name="id_etiket" value="<?php echo $id_etiket; ?>" />
			<input type="hidden" id="tipe_merchandise" name="tipe_merchandise" value="" />
			<input type="hidden" id="status_warna" name="status_warna" value="" />
			<input type="hidden" id="status_ukuran" name="status_ukuran" value="" />
		</div>
		<label class="control-label" for="typeahead">Nama Merchandise</label>
		<div class="controls">
			<input type="text" id="nama" name="nama" class="span5" value="" />
		</div>
		
		<label style="display: none;" class="show_ukuran control-label" for="typeahead">Ukuran/Size</label>
		<div style="display: none;" class="show_ukuran controls">
			<input type="text" id="nama" name="nama" class="span5" value="" />
		</div>
		
		<label style="display: none;" class="show_warna control-label" for="typeahead">Warna</label>
		<div style="display: none;" class="show_warna controls">
			<input type="text" id="nama" name="nama" class="span5" value="" />
		</div>-->
		
		<label class="control-label" for="typeahead">&nbsp;</label>
		<div class="controls">
			<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Tambah Merchandise" onclick="addRowProduct()" />
			<input tabindex="4" type="button" id="btn_input_user2" class="btn btn-warning .submit" name="save" value="Tambah Uang Makan/Subsidi" onclick="addRowProduct2()" />
		</div>
		<!--<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/member/knet/list')" />
		<input type="submit" class="btn btn-success" value="Print to Excel" />
		<input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />-->
	  </div> <!-- end control-group -->
    </fieldset>
    
	<table class="table table-striped table-bordered" width="100%">
	   <thead>
		   <tr>
			 <th colspan="6">List Product</th>
		   </tr> 	
	   </thead>
	   <thead>
		   <tr>
			<th width="20%">Kode Produk</th>
			<th>Nama Produk</th>
			<th width="15%">Tipe</th>
			<th width="15%">Harga</th>
			<th width="10%">Tampil di Web</th>
			<th width="5%">Hapus</th>
		   </tr>
	   </thead>
	   <tbody id="addPrdBnd">
	<?php
	$i = 0;
	if($listPrdBundling != null) {
		foreach($listPrdBundling as $dataPrd) {
			$i++;
			echo "<tr id=\"$i\">";
			echo "<td><input onchange=\"cariHarga('$i')\" class=\"span20\" type=\"text\" id=\"kode_produk$i\" name=\"kode_produk[]\" value=\"$dataPrd->kode_produk\" />
			<input type=\"hidden\" id=\"kode_produkPrev$i\" name=\"kode_produkPrev[]\" value=\"$dataPrd->kode_produk\" />
			</td>";
			echo "<td><input class=\"span20\" readonly=\"readonly\" type=\"text\" id=\"nama$i\" name=\"nama[]\" value=\"$dataPrd->nama\" /></td>";
			
			if($dataPrd->tipe != null && $dataPrd->tipe != "") {
				echo "<td>"; 
				echo "<select id=\"tipe$i\" name=\"tipe[]\">";
				//print_r($listTipe);
				
					foreach($listTipe as $tipeMer) {
						$varSelect = "";
						if($dataPrd->tipe == $tipeMer->tipe_merchandise) {
								$varSelect = "selected=selected";
						}
						echo "<option value=\"$tipeMer->tipe_merchandise\" $varSelect>$tipeMer->tipe_merchandise</option>";
					}
				echo "</select>";
				echo "</td>";
				echo "<td><input class=\"span20\" type=\"text\" id=\"harga$i\" name=\"harga[]\" value=\"$dataPrd->harga\" /></td>";
				echo "<td><select style=\"width:80px\" id=\"status$i\" name=\"status[]\">";
				if($dataPrd->status == "0") {
				    echo "<option value=\"0\" selected=\"selected\">Tidak</option>";
					echo "<option value=\"1\">Ya</option>";
				} else {
					echo "<option value=\"0\">Tidak</option>";
					echo "<option value=\"1\" selected=\"selected\">Ya</option>";
				}
				echo "<select></td>";
				echo "<td><a onclick=\"hapusRow('$i')\" class=\"btn btn-mini btn-danger\" type=\"button\" id=\"btn$i\"><i class='icon-white icon-trash'></i></a></td>";
			} else {
				echo "<td><input class=\"span20\" readonly=\"readonly\" type=\"text\" id=\"tipe$i\" name=\"tipe[]\" value=\"\" /></td>";
				echo "<td><input class=\"span20\" type=\"text\" id=\"harga$i\" name=\"harga[]\" value=\"$dataPrd->harga\" /></td>";
				echo "<td><select style=\"width:80px\" id=\"status$i\" readonly=\"readonly\" name=\"status[]\">";
				if($dataPrd->status == "0") {
				    echo "<option value=\"0\">Tidak</option>";
					echo "<option value=\"1\" disabled=\"disabled\">Ya</option>";
				} else {
					echo "<option value=\"0\" disabled=\"disabled\">Tidak</option>";
					echo "<option value=\"1\">Ya</option>";
				}
				echo "<select></td>";
				echo "<td>&nbsp;</td>";
			}
			
			//echo "<td></td>";
			echo "</tr>";
			
		}	
	} else {
	    //print_r($uangMakan);
		if($uangMakan['response'] == "true") {
			$i = 0;
			foreach($uangMakan['arrayData'] as $dataPrd) {
				$i++;
				echo "<tr id=\"$i\">";
				echo "<td><input onchange=\"cariHarga('$i')\" class=\"span20\" type=\"text\" id=\"kode_produk$i\" name=\"kode_produk[]\" value=\"$dataPrd->kode_produk\" />
				<input type=\"hidden\" id=\"kode_produkPrev$i\" readonly=\"readonly\" name=\"kode_produkPrev[]\" value=\"$dataPrd->kode_produk\" />
				</td>";
				echo "<td><input class=\"span20\" type=\"text\" id=\"nama$i\" name=\"nama[]\" value=\"$dataPrd->prdnm\" /></td>";
				
				
					echo "<td><input class=\"span20\" readonly=\"readonly\" type=\"text\" id=\"tipe$i\" name=\"tipe[]\" value=\"\" /></td>";
					echo "<td><input class=\"span20\" type=\"text\" id=\"harga$i\" name=\"harga[]\" value=\"$dataPrd->harga\" /></td>";
					echo "<td><select style=\"width:80px\" id=\"status$i\" readonly=\"readonly\" name=\"status[]\">";
					
						echo "<option value=\"0\" >Tidak</option>";
						echo "<option value=\"1\" disabled=\"disabled\">Ya</option>";
					
					echo "<select></td>";
					echo "<td>&nbsp;</td>";
				
				
				//echo "<td></td>";
				echo "</tr>";
				
			}
		}	
	}
	?>
	  </tbody>
    </table>
	<input type="hidden" name="recMerchandise" id="recMerchandise" value="<?php echo $i;  ?>" />
  </form> 
  <input value="<< Back " type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
  <input value="Simpan" type="button" class="btn btn-small btn-primary" onclick="simpanMerchandise()" />
        <br>
<?php
}
?>
<script>
	function hapusRow(urutan) {
		$(All.get_active_tab() + " tbody#addPrdBnd tr#" +urutan).remove();
	}

	function pilihTipeMerchandise(nilai) {
		if(nilai !== "" && nilai !== " ") {
			var x = nilai.split("|");
			//console.log(x);	
			$(All.get_active_tab() + " #tipe_merchandise").val(x[0]);
			$(All.get_active_tab() + " #status_warna").val(x[1]);
			$(All.get_active_tab() + " #status_ukuran").val(x[2]);
			
			var status_warna = $(All.get_active_tab() + " #status_warna").val();
			var status_ukuran = $(All.get_active_tab() + " #status_ukuran").val();
			
			if(status_warna == "1") {
				$(All.get_active_tab() + " .show_warna").css("display", "block");
			} else {
				$(All.get_active_tab() + " .show_warna").css("display", "none");
			}
			
			if(status_ukuran == "1") {
				$(All.get_active_tab() + " .show_ukuran").css("display", "block");
			} else {
				$(All.get_active_tab() + " .show_ukuran").css("display", "none");
			}
		} else {
		
		}
	}
	
	function cariHarga(param) {
		var nilai = $(All.get_active_tab() + " #kode_produk" +param).val();
		if(nilai !== "" && nilai !== null) {
			var kodeProdukPrev = $(All.get_active_tab() + " #kode_produkPrev" +param).val();
			var etiket_id = $(All.get_active_tab() + " #etiket_id").val();
			//var etiket_id = $(All.get_active_tab() + " #etiket_id").val();
			$.ajax({
				url: All.get_url('etiket/merchandise/getPrice'),
				type: 'POST',
				dataType: 'json',
				data: {id_etiket : etiket_id,kode_produk : nilai},
				success: function (data) {              
					All.set_enable_button();
					
					if(data.response == "false") {
						alert(data.message);
						$(All.get_active_tab() + " #kode_produk" +param).val(kodeProdukPrev);
					} else {
					    
						var arrData = data.arrayData;
						$(All.get_active_tab() + " #nama" +param).val(arrData[0].prdnm);
						$(All.get_active_tab() + " #harga" +param).val(arrData[0].harga);
						if(arrData[0].prdcd == "XL001N" || arrData[0].prdcd == "XL001NS") {
							$(All.get_active_tab() + " #tipe" +param).val("SIMCARD-XL");
						}
						
						if(arrData[0].prdcd == "INZPRA") {
							$(All.get_active_tab() + " #tipe" +param).val("KAOS");
						}
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
				}
			});
		} else {
			
		}
	}
	
	function addRowProduct() {
		var recMerchandise = $(All.get_active_tab() + " #recMerchandise").val();
		recMerchandise++;
		
		var rowhtml = "";
		rowhtml += "<tr id='"+recMerchandise+"'>";
		rowhtml += "<td><input onchange=cariHarga("+recMerchandise+") class=span20 type=text id=kode_produk"+recMerchandise+" name=kode_produk[] value='' /><input type=hidden id=kode_produkPrev"+recMerchandise+" name=kode_produkPrev[] /></td>";
		rowhtml += "<td><input class=span20 type=text id=nama"+recMerchandise+" name=nama[] /></td>";	
		rowhtml += "<td><select id=tipe"+recMerchandise+" name=tipe[]>";
		
		<?php
		foreach($listTipe as $tipeMer) {
		?>			
		  rowhtml += "<option value='<?php echo $tipeMer->tipe_merchandise; ?>'><?php echo $tipeMer->tipe_merchandise; ?></option>";
		<?php
		}
		?>
		
		rowhtml += "</select></td>";
		rowhtml += "<td><input class=span20 type=text id=harga"+recMerchandise+" name=harga[] value='' /></td>";
		rowhtml += "<td><select style='width:80px' id=status"+recMerchandise+" name=status[]>";
		rowhtml += "<option value=0>Tidak</option><option value=1>Ya</option>";
		rowhtml += "</select></td>";
		rowhtml += "<td><a onclick='hapusRow('"+recMerchandise+"')' class='btn btn-mini btn-danger' type='button' id=btn"+recMerchandise+"><i class='icon-white icon-trash'></i></a></td>";
		rowhtml += "</tr>";
		$(All.get_active_tab() + " #recMerchandise").val(recMerchandise);
		console.log(rowhtml);
		$(All.get_active_tab() + " tbody#addPrdBnd").append(rowhtml);
		
	}

	function addRowProduct2() {
		var recMerchandise = $(All.get_active_tab() + " #recMerchandise").val();
		recMerchandise++;
		
		var rowhtml = "";
		rowhtml += "<tr id='"+recMerchandise+"'>";
		rowhtml += "<td><input onchange=cariHarga("+recMerchandise+") class=span20 type=text id=kode_produk"+recMerchandise+" name=kode_produk[] value='' /><input type=hidden id=kode_produkPrev"+recMerchandise+" name=kode_produkPrev[] /></td>";
		rowhtml += "<td><input class=span20 type=text id=nama"+recMerchandise+" name=nama[] /></td>";	
		rowhtml += "<td><input readonly=readonly class=span20 type=text id=tipe"+recMerchandise+" name=tipe[] /></td>";	
		
		rowhtml += "</select></td>";
		rowhtml += "<td><input class=span20 type=text id=harga"+recMerchandise+" name=harga[] value='' /></td>";
		rowhtml += "<td><select style='width:80px' id=status"+recMerchandise+" name=status[]>";
		rowhtml += "<option value=0>Tidak</option><option value=1 disabled=disabled>Ya</option>";
		rowhtml += "</select></td>";
		rowhtml += "<td><a onclick='hapusRow('"+recMerchandise+"')' class='btn btn-mini btn-danger' type='button' id=btn"+recMerchandise+"><i class='icon-white icon-trash'></i></a></td>";
		rowhtml += "</tr>";
		$(All.get_active_tab() + " #recMerchandise").val(recMerchandise);
		console.log(rowhtml);
		$(All.get_active_tab() + " tbody#addPrdBnd").append(rowhtml);
	}
	
	function simpanMerchandise() {
		All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url('etiket/merchandise/save') , $(All.get_active_tab() + " #formMembKnet").serialize(), function(data)
        {
            All.set_enable_button();
			alert(data.message);
            
        },"json").fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
	}
	
	function showListUkuran(tipeMerchandise) {
	
	}
</script>
<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="inputEtiketCat">
    <fieldset>      
      <div class="control-group">                      
         <?php
         //username
		 $onchange = "onchange=All.checkDoubleInput('user/list/','username',this.value)";
         $category_desc = array(
		 	"labelname" => "Kategori E-Ticket",
		 	"fieldname" => "category_desc",
		 	"placeholder" => placeholderCheck(),
		 	"addClass" => "setReadOnly",	
		 	"event" => $onchange
 		 );
		 echo inputText($category_desc);
		 //password
         $shortname = array(
		 	"labelname" => "Singkatan",
		 	"fieldname" => "shortname",
		 	"placeholder" => "required"
 		 );
		 echo inputText($shortname);
		 //status
		 echo selectFlagActive("Status", "status");
		 //branch
         //echo selectFlagActive("Active", "status");
		 echo inputHidden("cat_id");
         $input  = "All.inputFormData('etiket/cat/input/save', 'inputEtiketCat')";
		 $update = "All.updateFormData('etiket/cat/update/save', 'inputEtiketCat', 'etiket/cat/list')";
		 $view   = "All.getListData('etiket/cat/list')";
         echo button_set($input, $update, $view);
		 ?>    
        </div> <!-- end control-group -->
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
	
</script>
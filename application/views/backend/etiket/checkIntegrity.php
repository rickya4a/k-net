<div class="mainForm">
  <form class="form-horizontal" id="etiketCheckInt">
    <fieldset>      
      <div class="control-group">
      	
      	<label class="control-label" for="typeahead">Kategori Check</label>                             
        <div class="controls">
        	<select id="tipe_check" name="tipe_check" style="width: 350px">
        	   <option value="null_merchandise">List Tiket yang tidak ada merchandise</option>	
			   <option value="null_ip">List Transaksi yang tidak ada Incoming Payment</option>
			   <!--<option value="resend_sms_tiket">Resend SMS per Ticket</option>
			   <option value="resend_sms_trx">Resend SMS per Transaksi</option>-->
        	</select>
        </div>
       
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input onclick="All.ajaxFormPost(this.form.id,'etiket/integrity/check/list')" tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" />
            <input tabindex="5"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> 
     </fieldset>
    
    <div class="result"></div>
    
  </form> 
</div>
<script>
	function recoverIP(param) {
		//alert('No IP :' +param);All.set_disable_button();
    	$.ajax({
            url: All.get_url('dtc/createIP') +"/"+ param ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	}
</script>    
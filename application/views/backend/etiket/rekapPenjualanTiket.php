<table class="table table-bordered table-striped" width="80%">
	<thead>
		<tr>
			<th colspan="6">REKAP PENJUALAN TIKET ACARA ONLINE</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Kode Acara</th>
			<th>Acara</th>
			<th>Jumlah Transaksi</th>
			<th>Total Penjualan</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$no = 1;
			foreach($report as $dtax) {
			    $url = "etiket/listPesertaByAcara/".$dtax->id_etiket;
				echo "<tr>";
				echo "<td>$no</td>";
				echo "<td align=center>$dtax->etiket_code_id</td>";
				echo "<td>$dtax->acara</td>";
				echo "<td align=right>$dtax->jumlah_trx</td>";
				echo "<td align=right>".number_format($dtax->total_harga, 0,",",".")."</td>";
				echo "<td align=center><input onclick=\"All.ajaxShowDetailonNextForm('$url')\" type=button class='btn btn-mini btn-success' value='Rekap Peserta' /></td>";
				echo "</tr>";
				$no++;
		    }
		?>
	</tbody>
</table>
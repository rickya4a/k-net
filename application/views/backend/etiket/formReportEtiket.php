<div class="mainForm">
  <form class="form-horizontal" method="post" id="frmLounge" enctype="multipart/form-data" action="<?php echo $action; ?>" target="_blank">
    <fieldset> 
		<div class="control-group">
			<div class="controls">
				<input type="hidden" class="span5" id="id" name="id"  />
			</div>
			<label class="control-label" for="typeahead">Tgl Input</label>
			<div class="controls">
				<input type="text" class="TabOnEnter dtpicker" name="so_from" id="so_from" tabindex="5">
				<input type="text" class="TabOnEnter dtpicker" name="so_to" id="so_to" tabindex="6"  >
			</div>
			<label class="control-label" for="typeahead">Jenis Acara/Event</label>
			<div class="controls">
			<select id="cat_id" name="cat_id" onchange="pilihjenisAcara()">
			<option value="">Semua</option>
						<?php
						foreach($listTipeAcara as $dta) {
							echo "<option value=\"$dta->cat_id**$dta->is_date_req\">$dta->category_desc</option>";
						}
						?>
					</select>
			</div>
			<label class="control-label" for="typeahead">Lokasi / Tanggal</label>
			<div class="controls">
			<select class="form-control" style="width: 300px;" data-live-search="true" id="opsi" name="opsi" onchange="getInfoAcara()">
                                

                            </select>
			</div>			
			<label class="control-label" for="typeahead">Tipe Transaksi</label>
			<div class="controls">
				<select id="typeTrx" name="typeTrx" tabindex="2">
					<option value="1">Semua</option>
					<option value="2">Online</option>
					<option value="3">Offline</option>
				</select>
			</div>
			
			<label class="control-label" for="typeahead">Kategori Pencarian</label>
			<div class="controls">
				<select id="category" name="category" tabindex="3" style="width: 300px">
					<option value="1">Semua</option>
					<option value="2">ID Member yg transaksi</option>
					<option value="3">Nama Member yg transaksi</option>
					<option value="4">ID Member Peserta Acara</option>
					<option value="5">Nama Member Peserta Acara</option>
					<option value="6">Rekap Peserta yang terdaftar</option>
					<!--<option value="6">Acara</option>-->
				</select>
			</div>
			<div class="controls">
				<input type="text" class="span5"  id="search" name="search" />
            </div>
			<label class="control-label" for="typeahead">Jenis Report</label>
			<div class="controls">
				<select id="jenis" name="jenis" tabindex="3">
					<option value="1">Peserta</option>
					<option value="2">Finance</option>

				</select>
			</div>

			<!--            <label class="control-label showDt" for="typeahead">NAMA MEMBER</label>-->
<!--			<div class="controls">-->
<!--				<input type="text" class="span5" readonly="readonly" id="loc_event" name="loc_event" required="required"/>-->
<!--            </div>-->
<!--            <label class="control-label showDt" for="typeahead">PERINGKAT</label>-->
<!--			<div class="controls">-->
<!--				<input type="text" class=" span5" readonly="readonly" id="max_online" name="max_online" required="required"/>-->
<!--            </div>-->
<!--            <label class="control-label showDt" for="typeahead">PEMBAYARAN</label>-->
<!--			<div class="controls">-->
<!--				<input type="text" class=" span5" id="max_offline" name="max_offline" required="required" value="50000"/>-->
<!--            </div>-->
<!--            <label class="control-label showDt" for="typeahead">REMARK</label>-->
<!--			<div class="controls">-->
<!--				<input type="text" class=" span5"   id="remark" name="remark" />-->
<!--            </div>-->
<!--			--><?php
//			//				//$input = "All.inputFormData('product/save', this.form.id)";
//			$input = "All.searchOfficeCahyono('loungereport/getReport','frmLounge')";
//			$update = "#";
//			$view = "#";
//			//				echo inputHidden("country_id", "ID");
//			//				echo inputHidden("hq_id", "BID06");
//			//				echo inputHidden("branch_id", "B001");
//			echo button_set($input, $update, $view);
//			?>
<!---->
			<div class="controls"  id="inp_btn">
				<input tabindex="7" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.searchOfficeCahyono('etiket/getReport','frmLounge')" />
				<button type="submit" class="btn btn-success" >Cetak</button>
			</div>

<!--			<button type="submit" class="btn btn-success">Submit</button>-->





		</div>
     </fieldset>
     
  </form>
	<div id="pencarianBonus"></div>
	<div id="detail_downline"></div>
	<div id="hasilPencarian1xd"></div>
	<div id="result"class="result"></div>

</div><!--/end mainForm-->

<script>
	$(document).ready(function() {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());

		All.set_datatable();
	});
	
	function pilihjenisAcara() {
		//var pil_event = $("#pil_event").val();
		//res = pil_event.split("|");
		
		//console.log(res);
		//$("#event_cat_id").val(res[1]);
		//$("#nm_event").val(res[0]);
		
		var cat_id = $(All.get_active_tab() + " #cat_id").val();
		
		if(cat_id != '') {
			$.ajax({
				type : "GET",
				url  : "<?php echo site_url('etiket/listEventByCat/');?>" + cat_id,
				dataType : 'json',
				success: function(data){
					if(data.response == "true") {
						var arrayData = data.arrayData;
						var rowhtml = "";
						$("#opsi").html(null);
						rowhtml += "<option value=''>--Pilih Lokasi dan Waktu--</option>";
						$.each(arrayData, function(key, value) {
							rowhtml += "<option value='"+value.id+"'>"+value.nama+" - "+value.lokasi+" ("+value.event_date2+")</option>";	
						});
						$(All.get_active_tab() + " #opsi").html(rowhtml);
					} else  {
						alert("Acara tidak tersedia..");
						$(All.get_active_tab() + " #opsi").html(null);
					}
				}
			});
		} else {
			$(All.get_active_tab() + " #opsi").html(null);
		}
	}
	
	function sendSmsAcara(param, notiket) {
		//alert("param : " +param+ " notiket : " +notiket);
		$.ajax({
				type : "GET",
				url  : "<?php echo site_url('etiket/resendsms');?>" + "/" +param+ "/" +notiket,
				dataType : 'json',
				success: function(data){
					alert(data.message);
				}
			});
	}
	
	function getInfoAcara() {
	
	}
</script>
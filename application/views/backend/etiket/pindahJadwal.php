
  <form class="form-horizontal" method="post" id="frmUpdateJadwal" enctype="multipart/form-data" >
    <fieldset> 
		<div class="control-group">
			<label class="control-label" for="typeahead">Acara Sebelumnya</label>
			<div class="controls">
			   <input type="text" class="span10" id="prev_acara" name="prev_acara" readonly="readonly" value="<?php echo $prev_acara[0]->lokasi." - ".$prev_acara[0]->event_date2; ?>" />
			</div>
			<label class="control-label" for="typeahead">Lokasi / Tanggal Baru</label>
			<div class="controls">
			<select class="form-control" style="width: 300px;" data-live-search="true" id="opsi" name="opsi" onchange="getInfoAcara()">
            <?php
			foreach($res as $dta) {
				echo"<option value=\"$dta->id\">$dta->nama - $dta->lokasi ($dta->event_date2)</option>";	
			}
			?>
            </select>
			</div>			
			
			<div class="controls"  id="inp_btn">
			    <input type="hidden" id="list_orderno" name="list_orderno" value="<?php echo $listOrderno; ?>" />
				<input tabindex="7" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="ubahJadwal()"  />
				
			</div>

<!--			<button type="submit" class="btn btn-success">Submit</button>-->





		</div>
     </fieldset>
     
  </form>
  <div class="hasilUpdate"></div>	
<script>
	function ubahJadwal() {
		All.set_disable_button();
		  $.ajax({
            url: All.get_url('etiket/pindahJadwal/save'),
            type: 'POST',
			data: $(All.get_active_tab() + " #frmUpdateJadwal").serialize(),
            success:
            function(data){
            	All.set_enable_button();
            	//$(All.get_active_tab() + " .nextForm1").hide();
                All.clear_div_in_boxcontent(" .hasilUpdate");
                $(All.get_active_tab() + " .hasilUpdate").html(data);  
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
          });	
	}
</script>
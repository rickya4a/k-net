<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Report.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<table border="1" width="100%">

    <thead>

    <tr>
        <th>No </th>
        <th>No Trx</th><th>No Tiket</th>
		<!--<th>No Member</th><th>Nama Member</th>-->
		<th>No Peserta</th><th>Nama Peserta</th><th>Acara</th><th>Tanggal Transaksi</th><th>Pembayaran</th><th>Biaya</th><th>Rekening</th><th>No telepon</th><th>Email</th><th>Kaos</th>

    </tr>

    </thead>

    <tbody>

    <?php
//    print_r($report);
    $i = 1;
    $total=0;
    if($report!='Err'){
        foreach ($report as $list) {
            echo "<tr id=\"$i\">";
            echo "<td><div align=center>$i</div></td>";
			echo "<td><div align=center>$list->order_no</div></td>";
            echo "<td><div align=center>$list->notiket</div></td>";
            //echo "<td><div align=center>$list->dfno_pembeli</div></td>";
            //echo "<td><div align=center>$list->pembeli</div></td>";
            echo "<td><div align=center>$list->valid_dfno</div></td>";
            echo "<td><div align=center>$list->peserta</div></td>";
            echo "<td><div align=center>".$list->nama." - ".$list->lokasi."</div></td>";
            $total+=$list->price;
            if($list->tglbeli==null){
                $date=$list->tanggal;
            }else
                $date=$list->tglbeli;
            date("d-M-Y", strtotime($list->tglbeli));
            echo "<td><div align=center>".date("d-M-Y", strtotime($date))."</div></td>";
            if($list->flag_online!='W'){
                echo "<td><div align=center>Offline</div></td>";
            }else{
                echo "<td><div align=center>Online</div></td>";
            }
            echo "<td><div align=center>".($list->price)."</div></td>";
            echo "<td><div align=center>".$list->pembayaran." - ".$list->rekening."</div></td>";
            echo "<td><div align=center>".$list->notelp." / ".$list->tel_hp."</div></td>";
            echo "<td><div align=center>".$list->email_buyer."</div></td>";
            echo "<td><div align=center>".$list->prdnm."</div></td>";

//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);

            echo "</tr>";
            $i++;
        }
    }

    ?>

    </tbody>

</table>
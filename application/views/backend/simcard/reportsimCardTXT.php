<?php
header("Content-Type: plain/text");
header("Content-Disposition: Attachment; filename=ReportsimCard.txt");
header("Pragma: no-cache");

date_default_timezone_set("Asia/Jakarta");

$tgl = date("d F Y");
$waktu = date("d/m/y h:i:s");

function tmbh_spaceHeaderxxx($value)
{
    $kosong = '';
    for($x = 1; $x <= $value; $x++)
    {
        $kosong .= " ";
    }

    echo $kosong;
}

function titleHeader()
{
    echo "No";
    tmbh_spaceHeaderxxx(2);
    echo "ID Member";
    tmbh_spaceHeaderxxx(15);
    echo "Nama member";
    tmbh_spaceHeaderxxx(15);
    echo "No.XL";
    tmbh_spaceHeaderxxx(12);
    echo "Amount";
    echo "\n";

    //garisStripSum();
}

function titleKosong(){
    echo "-";
    tmbh_spaceHeaderxxx(4);
    echo "-";
    tmbh_spaceHeaderxxx(17);
    echo "-";
    tmbh_spaceHeaderxxx(20);
    echo "-";
    tmbh_spaceHeaderxxx(20);
    echo "-";
    tmbh_spaceHeaderxxx(10);
    echo "-";
}

/*foreach($dtVch as $dtax) {
    if($dtax->claimstatus =='1'){
        $y = 'VOUCHER SUDAH DI CLAIM';
    }else{
        $y = 'VOUCHER BELUM DI CLAIM';
    }
}*/

echo "\n\n\n";
echo "                                 PT.K-Link Indonesia\n\n";
echo "                              DAFTAR PENJUALAN KARTU XL\n\n\n";

garisStrip();
echo "\n";
titleHeader();
echo "\n";
garisStrip();
echo "\n";

$no=0;
foreach ($sql->result() as $r) {
    $no++;
    $amount=105000;
    $nk1 = 4;
    $nk2 = 28;
    $nk3 = 55;
    $nk4 = 70;

    $nama=$r->nama;
    $str1= strlen($no);
    $str2= strlen($r->idmemb);
    $str3= strlen($nama);
    $str4= strlen($r->nomor);
    $str5= strlen($amount);

    if($str3 > 24){
        $str3= strlen(substr($nama,0,24));
    }else{
        $str3= $str3;
    }

    $kolom1= ($nk1 - ($str1)); // ok
    $kolom2= ($nk2 - ($nk1 + $str2)); //ok
    $kolom3= ($nk3 - ($str1 + $kolom1 + $str2 + $kolom2 + $str3));
    $kolom4= ($nk4 - ($str1 + $kolom1 + $str2 + $kolom2 + $str3 + $kolom3 + $str4));

    echo $no;

    tmbh_spaceHeaderxxx($kolom1);
    echo $r->idmemb;

    tmbh_spaceHeaderxxx($kolom2);
    echo substr($nama,0,24);

    tmbh_spaceHeaderxxx($kolom3);
    echo $r->nomor;

    tmbh_spaceHeaderxxx($kolom4);
    //tmbh_spaceHeaderxxx(9,number_format(105000,0,".",","));
    echo number_format(105000,0,".",",");

    echo "\n";
    garisStrip();
    echo "\n";

    $nsum[]=$amount;
}
/*titleKosong();
echo "\n";
garisStrip();
echo "\n";*/
$tot1= array_sum($nsum);
tmbh_spaceHeaderxxx(47);
echo "*Total DP Amount :  ".number_format($tot1,0,".",",");
echo "\n";
tmbh_spaceHeaderxxx(67);
echo "==========";
echo "\n";

?>
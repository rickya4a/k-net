<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Report.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<table border="1" width="100%">

    <thead>

    <tr>
        <th>No </th>
        <th>No TRX</th><th>No Member</th><th>Nama Member</th><th>Tanggal Transaksi</th><th>Pembayaran</th><th>Rekening</th><th>Biaya</th><th>Remark</th>

    </tr>

    </thead>

    <tbody>

    <?php
    $i = 1;
    $total=0;
    foreach ($report as $list) {
        echo "<tr id=\"$i\">";
        echo "<td><div align=center>$i</div></td>";

        echo "<td><div align=center>$list->order_no</div></td>";
        echo "<td><div align=center>$list->dfno</div></td>";
        echo "<td><div align=center>$list->fullnm</div></td>";
        echo "<td><div align=center>".date("d-M-Y", strtotime($list->datetrans))."</div></td>";
        if($list->flag_online!='W'){
            echo "<td><div align=center>Offline</div></td>";
        }else{
            echo "<td><div align=center>Online</div></td>";
        }
        echo "<td><div align=center>".$list->pembayaran." - ".$list->rekening."</div></td>";
        echo "<td><div align=center>".($list->nominal)."</div></td>";
        echo "<td><div align=center>".$list->marker."</div></td>";

//        echo "<td><div align=center>".$list->nama." - ".$list->lokasi."</div></td>";
        $total+=$list->nominal;


//                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
//                $arr = array(
//				    "update" => "Product.getUpdateEtiket('$list->id')",
////				    "delete" => $ondelete
//				);
//                echo btnUpdate($arr);

        echo "</tr>";
        $i++;
    }
    ?>

    </tbody>
    <tr>
        <th colspan="7">Total </th>
        <th><?php echo$total;?></th>

    </tr>
</table>
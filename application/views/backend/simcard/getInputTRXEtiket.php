<div class="mainForm">
  <form class="form-horizontal"  id="formInputProduct" enctype="multipart/form-data" >
    <fieldset>

		<div class="control-group">
			<div class="controls" style="margin-left: 0px;">

      	<table width="100%" border="0">
      		<tr>
      			<td width="15%" align="right">Order No&nbsp;</td>
      			<td width="35%">
      				<input type="text" class=" span20" placeholder="<?php echo placeholderCheck(); ?>" id="nm_event" name="nm_event" required="required" />
					<input type="hidden" class=" span20" id="id" name="id"  />

				</td>

      		</tr>

			<tr>
				<td width="15%" align="right">ID Member&nbsp;</td>
				<td width="35%">
					<input type="text" class=" span20" placeholder="<?php echo placeholderCheck(); ?>" id="loc_event" name="loc_event" required="required"/>
				</td>

			</tr>
      		<tr>
      			<td  align="right">Nama Member&nbsp;</td>
      			<td>
					<input type="number" class=" span20" placeholder="Numeric/decimal" id="max_online" name="max_online" required="required"/>
				</td>
				<td  align="right">Nomor Tiket &nbsp;</td>
				<td>
					<input type="number" class=" span20" placeholder="Numeric/decimal" id="act_online" name="act_online" required="required" />
				</td>

				<!---->
      		</tr>
      		<tr>
				<td width="15%" align="right">Event&nbsp;</td>
				<td width="35%">

					<input type="number" class=" span20" placeholder="Numeric/decimal" id="max_offline" name="max_offline" required="required"/>

				</td>

				<td  align="right">Harga &nbsp;</td>
				<td>
					<input type="number" class=" span20" placeholder="Numeric/decimal" id="act_offline" name="act_offline" required="required" />
				</td>
      			<!---->
      		</tr>
			<tr>
				<td width="15%" align="right">Peserta&nbsp;</td>
				<td width="35%">

					<input type="number" class=" span20" placeholder="Numeric/decimal" id="total" name="total" required="required"/>

				</td>

				<td  align="right">Nama Peserta&nbsp;</td>
				<td>
					<input type="number" class=" span20" placeholder="Numeric/decimal" id="act_total" name="act_total" required="required"/>
				</td>
      			<!---->
      		</tr>
		    <tr>
      			<td align="right">Event Date &nbsp;</td>
      			<td>
					<input class="dtpicker " id="event_date" name="event_date" type="text" required="required">

      			</td>
				<td width="15%" align="right">Pembicara&nbsp;</td>
				<td width="35%">
					<input type="text" class=" span20"   id="pembicara" name="pembicara" />
				</td>
      		</tr>

            <tr>
				<td align="right">Online Exp &nbsp;</td>
				<td>
					<input class="dtpicker " id="exp_date_online" name="exp_date_online" type="text" required="required">

				</td>
				<td align="right">Offline Exp &nbsp;</td>
				<td>
					<input class="dtpicker " id="exp_date_offline" name="exp_date_offline" type="text" required="required">

				</td>
      			<!---->
      		</tr>

			<tr>
				<td width="15%" align="right">Online Price&nbsp;</td>
				<td width="35%">
					<input type="number" class=" span20" placeholder="Numeric/decimal"  id="price_online" name="price_online" required="required"/>
				</td>
				<td width="15%" align="right">Offline Price&nbsp;</td>
				<td width="35%">
					<input type="number" class=" span20" placeholder="Numeric/decimal"  id="price_offline" name="price_offline" required="required" />
				</td>
			</tr>
			<tr>
				<td align="right">Remark&nbsp;</td>
				<td >
					<input type="text" class=" span20"   id="remark" name="remark" />
				</td>

			</tr>



      	</table>
			</div>
				<?php
//				//$input = "All.inputFormData('product/save', this.form.id)";
				$input = "Product.saveInputEtiket('etiket/save', 'formInputProduct')";
				$update = "All.updateFormDataEnctypeNoListReload('etiket/update','formInputProduct', 'etiket/list')";
				$view = "All.getListData('etiket/list')";
//				echo inputHidden("country_id", "ID");
//				echo inputHidden("hq_id", "BID06");
//				echo inputHidden("branch_id", "B001");
				echo button_set($input, $update, $view);
				?>

<!--			<button type="submit" class="btn btn-success">Submit</button>-->


		</div>
     </fieldset>
     
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->

<script>
	$(document).ready(function() {
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());

		All.set_datatable();
	});
</script>
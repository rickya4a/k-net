<link href="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<script src="<?php echo site_url();?>vendors/moment/min/moment.min.js"></script>
<script src="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<script>
    function checkIDMemb(){
        var id = document.getElementById('idmemb').value;

        if(id !== ""){
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>backend/Simcard/checkIdMemb",
                data : {idmemb: id},
                dataType: "json",
                success : function (hasil) {
                    var nama = hasil;

                    if(nama !=null){
                        $('#nm_memb').val(nama[0].fullnm);
                    }
                    else{
                        alert('ID yang diinput tidak valid');
                        $('#nm_memb').val('');
                    }

                }

            });
        }
        else
            alert('Silahkan Input ID Member');
    }

    function checkNomor(){
        var no = document.getElementById('nomor').value;

        if(no != ""){

            //var no = document.getElementById("nomor").innerHTML;
            var check =(no.charAt(0));
            if(check == '0'){
            var tes = no.substring(0, 2);
            var res = no.replace(tes, 628);
            }else
                var res = no;
            //document.getElementById("nomor").innerHTML = res;
            //document.getElementById("nomor").value = res;
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>backend/Simcard/checkNomor",
                data : {nomor: res},
                dataType: "json",
                success : function (hasil) {

                    if(hasil === null){
                        alert('Nomor yang Anda Input Tidak Terdaftar');
                    }
                    else{
                        var nomor = hasil[0].msidn;
                        var status = hasil[0].status;
                        console.log(nomor);
                        console.log(status);

                        if(nomor != null) {
                            //alert('nomor ada');
                            if(status == 0 || status == 1){
                                alert('Nomor SIM Card Available');
                                document.getElementById("myBtn").disabled = false;
                            }
                            else{
                                alert('Nomor Sudah Diregistrasi, Silahkan Input Nomor yang Lain');
                                document.getElementById("myBtn").disabled = true;
                            }
                        }
                        else{
                            alert('Nomor Tidak Valid');
                        }
                    }

                }

            });
        }
        else
            alert('Silahkan Input ID Member');
    }
</script>

<div class="mainForm">
    <!--	<form class="form-horizontal"  id="formInputProduct" method="post" enctype="multipart/form-data" action="--><?php //echo base_url();?><!--backend/Simcard/saveData">-->
    <form class="form-horizontal"  id="formSimcard" name="formSimcard" enctype="multipart/form-data">

        <fieldset>

            <div class="control-group">
                <div class="controls" style="margin-left: 0px;">

                    <table width="80%" border="0">
                        <tr>
                            <td  width="15%" align="right">ID Member</td>
                            <td  width="40%">
                                <input type="text" tabindex="1" class=" span20" id="idmemb" name="idmemb" required="required" onchange="checkIDMemb()" />
                            </td>
                        </tr>

                        <tr>
                            <td  width="15%" align="right">Nama Member</td>
                            <td  width="35%">
                                <input class="span20" tabindex="2" id="nm_memb" name="nm_memb" readonly type="text" required="required">
                            </td>

                        </tr>
                        <tr>
                            <td  align="right">Nomor SIMCARD&nbsp;</td>
                            <td  >
                                <input type="text" tabindex="3" class=" span20" id="nomor" name="nomor" required="required" onchange="checkNomor()" />
                            </td>

                        </tr>
                        <tr>
                            <td  width="15%" align="right">Remark</td>
                            <td  width="35%">
                                <input class="span20" tabindex="2" id="remark" name="remark" type="text" required="required">
                            </td>

                        </tr>
                    </table>
                </div>
                <br>
                <button type="button" tabindex="4" style="margin-left: 225px;" id="myBtn" class="btn btn-success" disabled onclick="All.ajaxFormPost('formSimcard','backend/Simcard/saveSim')">SIMPAN</button>
                <button class="btn btn-primary" tabindex="5" type="button" name="save" value="Submit" onclick="All.sanListVch('simcard/getList','formVch')">View List</button>

            </div>

        </fieldset>
        
        <div id="listReleased"></div>
        <div id="listClaimed"></div>
        <div id="hasilPencarian1xd"></div>
        <div class="result"></div>

    </form>

</div><!--/end mainForm-->

<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>




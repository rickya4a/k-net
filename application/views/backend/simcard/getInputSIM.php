<link href="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<script src="<?php echo site_url();?>vendors/moment/min/moment.min.js"></script>
<script src="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<script type="text/javascript">
    var no=0;

    function OnSelectionChange()
    {
        var qty = $("#qty").val();
        var vch1 = $("#sampai").val();

        var Add = vch1;
        document.formInputProduct.dengan.value = Add;
        $("#urutkan").prop('checked', false);

    }

    function kelipatan() {
        var idnya = document.getElementById("nm_event").value;

        if (idnya != "" ) {

            var x = $("#sampai").val();
            var y = $("#dengan").val();
            var qty = $("#qty").val();

            var rowshtml = "";
            var masuk = 0;
            // var i =0;
            var counter = 0;
            if (x <= y) {

                $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>simcard/cariKelipatan",
                        data: {sampai: x, dengan: y},
                        dataType: "json",

                        success: function (result) {
                            var rows = $('.main tr').length;
                            console.log("rows" + rows);

                            if (result.arraydata != null) {
                                $.each(result.arraydata, function (key, value) {
                                    for (i = 0; i < rows; i++) {
                                        var nama = $("tr#" + i).find('input').val();
                                        console.log("i = " + i);
                                        console.log("namanya = " + nama);
                                        if (value.msidn == nama) {
                                            var troble = nama;
                                            counter++;
                                        }
                                    }

                                    if (counter > 0) {
                                        alert('Duplicate Entry ' + troble)
                                    }
                                    else {
                                        if ($('.main tr').length <= qty) {
                                            rowshtml += "<tr data-status=\"I\" id='" + no + "'>";
                                            rowshtml += "<td><input type=\"hidden\" name=\"msidn[]\" id=\"msidn\" value=\"" + value.msidn + "\"/>" + ($('.main tr').length) + "</td>";
                                            rowshtml += "<td>" + value.msidn + "</td>";
                                            rowshtml += "<td><input type=\"button\" class=\"btn btn-danger\" name=\"del\" id=\"del\" value=\"Delete\" onclick='deletebaris(" + no + ")' /></td>";
                                            rowshtml += "</tr>";
                                            no++;
                                            masuk++;
                                            $('#hasiltable').append(rowshtml);
                                            rowshtml = "";
                                        }
                                        else {
                                            alert('Release melebihi qty');
                                            return false;
                                        }

                                    }

                                });
                                alert(masuk + " dari " + (y - x + 1) + " nomor yang valid");
                                if (result.nomorbermasalah != null) {
                                    alert(result.nomorbermasalah[0].msidn + " sudah di relase untuk stokis " + result.nomorbermasalah[0].loccd);
                                }

                            }
                            else {
                                if (result.nomorbermasalah != null)
                                    alert(result.nomorbermasalah[0].msidn + " sudah di relase untuk stokis " + result.nomorbermasalah[0].loccd);
                                else
                                    alert("Nomor yang Anda Masukkan Salah");
                            }


                        }
                    }
                );
            }
            else
                alert("Range nomor yang anda masukkan salah")
        }
        else
            alert("Masukkan nomor KW terlebih dahulu")

    }


    function deletebaris(id){
        $("tr#"+id).remove();
    }

    function loaddata()
    {
        var rowshtml="";
        var idnya=document.getElementById( "nm_event" ).value;

        if (idnya)
        {
            //$( '#display_info' ).html("Isinya : "+idnya);

            $.ajax({
                //			alert('coba ajax');
                type : "POST",
                url  : "<?php echo base_url();?>backend/Simcard/checkKW",
                data : {idm: idnya},
                dataType: "json",

                success : function(result)
                {
                    var header = result.header;
                    var detail = result.detail;

                    if(header != null){
                        $("#event_date").val( header[0].etdt );
                        $("#loccd").val( header[0].dfno );
                        $("#qty").val( header[0].qtyord );
                        $("#idmbr2").val( idnya);
                        document.getElementById('nm_event').readOnly = true;
                        document.getElementById('event_date').readOnly = true;
                        document.getElementById('loccd').readOnly = true;
                        document.getElementById('qty').readOnly = true;

                        if(detail != null){
                            //if detail exist, show detail!
                            //alert('detail here');
                            $.each(detail, function(key, value) {
                                rowshtml += "<tr data-status=\"I\" id='tr"+no+"'>";
                                rowshtml += "<td>"+(no+1)+"</td>";
                                rowshtml += "<td>"+value.simcard+"</td>";
                                rowshtml += "<td>"+value.createdt+"</td>";
                                rowshtml += "</tr>";

                                no++;
                                //masuk++;
                            });
                            $('#hasiltable').append(rowshtml);

                        }
                    }
                    else
                    {
                        alert("KW anda tidak valid");
                        $("#event_date").val('');
                        $("#loccd").val('');
                        $("#qty").val('');
                        $("#idmbr2").val('');
                        $("#nm_event").val('');

                    }
                }
            });

        } else
        {
            $( '#display_info' ).html("Please Enter Some Words");
        }
    }

    function tipse(){
        var x =document.getElementById( "tipe" ).value;
        var y = document.getElementById("tipestokis");
        console.log(x);
        if(x==1){
            y.style.display = "none";
        }
        else{
            //y.style.display = "true";
			$(All.get_active_tab() + " #tipestokis").removeAttr("style");
        }
    }
</script>

<script>
    $(document).ready(function() {
        $("#urutkan").on('change', function() {
            if ($(this).is(':checked')) {
                var table = document.getElementById('box-table-b');
                var rowCount = table.rows.length;

                //var varJS = (parseInt( $("#voucherno1").val()) + parseInt($("#qty").val()))-1;
                var varJS = ((parseInt( $("#sampai").val()) + parseInt($("#qty").val()))-(parseInt (rowCount)));
                document.formInputProduct.dengan.value = varJS;
            } else {
                document.formInputProduct.dengan.value = $("#sampai").val();
            }
        });


    });
</script>


<div class="mainForm">
    <!--	<form class="form-horizontal"  id="formInputProduct" method="post" enctype="multipart/form-data" action="--><?php //echo base_url();?><!--backend/Simcard/saveData">-->
    <form class="form-horizontal"  id="formInputProduct" name="formInputProduct" enctype="multipart/form-data">

        <fieldset>

            <div class="control-group">
                <div class="controls" style="margin-left: 0px;">

                    <table width="100%" border="0">
                        <tr>
                            <td  width="20%" align="right">Tipe Transaksi&nbsp;</td>
                            <td width="80%">
                                <select class="form-control" data-live-search="true" id="tipe" name="tipe" onchange="tipse()">
                                    <option value="2">Member</option>
									<option value="1">Stokis</option>
                                    
                                <!--								<input type="text" class=" span20" id="id" name="id"  />-->
                                </select>

                            </td>

                        </tr><tr>
                            <td  width="15%" align="right">No TRX.&nbsp;</td>
                            <td  width="50%">
                                <input type="text" class=" span20" placeholder="<?php echo placeholderCheck(); ?>" id="nm_event" name="nm_event" required="required" onchange="loaddata()" />
                                <!--								<input type="text" class=" span20" id="id" name="id"  />-->

                            </td>

                        </tr>

                        <tr>
                            <td  width="15%" align="right">Tgl TRX.&nbsp;</td>
                            <td  width="35%">
                                <input class="span20" id="event_date" name="event_date" type="text" required="required">
                            </td>

                        </tr>
                        <tr>
                            <td  align="right">Stokis&nbsp;</td>
                            <td  >
                                <input type="text" class=" span20" placeholder="<?php echo placeholderCheck(); ?>" id="loccd" name="loccd" required="required" />
                            </td>

                        </tr>
                        <tr>
                            <td align="right">Jumlah Pembelian &nbsp;</td>
                            <td>
                                <input type="number" class=" span5" placeholder="Numeric/decimal" id="qty" name="qty" required="required"/>
                            </td>
                        </tr>
                        <tr id="tipestokis">
                            <td  align="right">Member&nbsp;</td>
                            <td  >
                                <input style="width: 200px;" type="text" onchange="All.getFullNameByID(this.value,'be/memberinfo/msmemb/dfno','#fullnm')" style="width: 200px;"  placeholder="<?php echo placeholderCheck(); ?>" id="dfno" name="dfno" required="required" />
								<input type="text"  id="fullnm" name="fullnm" readonly="readonly" style="width: 400px;"/>
                            </td>

                        </tr>
                        <tr>
                            <td align="right">Nomor SIM CARD&nbsp;</td>
                            <td >
                                <input id="sampai" name="sampai" type="text" required="required" style="width:150px" onchange="OnSelectionChange();">
                                s/d
                                <input id="dengan" name="dengan" type="text" required="required" style="width:150px">

                                <input type="checkbox" name="urutkan" id="urutkan"> Urutkan

                            </td>
                            <td>
                            </td>
                            <td >
                            </td>
                        </tr>
                    </table>
                </div>
                <br>
                <button type="button" style="margin-left: 155px;" class="btn btn-success" onclick="kelipatan()">Tambahkan Data</button>

                <!--				<a class="btn btn-success" id="addItem"> Tambahkan </a>-->

                <!--			<button type="submit" class="btn btn-success">Submit</button>-->


            </div>
            <table width="90%" class="main table table-hover" id="box-table-b">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Sim Card</th>
                    <th>Create Date</th>
                </tr>
                </thead>
                <tbody id="hasiltable">
                </tbody>

            </table>

            <!--            <button type="submit" class="btn btn-success">Simpan</button>-->
            <br>
            <input style="margin-left: 155px;" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost('formInputProduct','backend/Simcard/saveData')"/>


        </fieldset>
        <div class="result"></div>

    </form>

</div><!--/end mainForm-->

<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>




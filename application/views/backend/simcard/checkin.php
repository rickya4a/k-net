<script type="text/javascript">

    function pilihan() {
        var x = document.getElementById("pilihan");
        var y = document.getElementById("opsi").value;


        if (y==1) {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }


    function loaddatavalid()
    {
        var idnya=document.getElementById( "search" ).value;
        var opsi=document.getElementById( "opsi" ).value;
        var acara=document.getElementById( "acara" ).value;

        if (idnya)
        {
            //$( '#display_info' ).html("Isinya : "+idnya);

            $.ajax({
                //			alert('coba ajax');
                type : "POST",
                url  : "<?php echo base_url();?>etiket/validasitiket",
                data : {idm: idnya, acara:acara, opsi:opsi},
                dataType: "json",

                success : function(result)
                {
                    if(result[0].status==0){
                        $("#display_info").val("Tiket anda Valid! ");
                        $("#nm_acara").val(result[0].nama+" - "+result[0].lokasi);
                        $("#nm_member").val(result[0].valid_fullnm);
                        $("#idtiket").val(result[0].id_etiket);
                        $("#nomember").val(result[0].valid_dfno);

                        $("#status").val(result[0].status);
//                        $("#display_info2").val( result[0].fullnm + " [" +result[0].shortnm+ "]");

//                        $("#display_info2").val( result[0].fullnm + " [" +result[0].rank+ "]");
//                        $("#idmbr2").val( idnya);
                    }
//	                	$( '#display_info' ).html(result[0].fullnm + " [" +result[0].rank+ "]");


//						document.getElementById("#display_info").value = result[0].fullnm + " [" +result[0].rank+ "]";
                    else if(result[0].status==null)
                    {
                        $("#display_info").val("Data tidak ditemukan");
                        $("#nm_acara").val("Data tidak ditemukan");
                        $("#nm_member").val("Data tidak ditemukan");
                        $("#idtiket").val("Data tidak ditemukan");
                        $("#nomember").val("Data tidak ditemukan");

                        $("#status").val(result[0].status);

                    }
                    else if(result[0].status==1)
                    {
                        $("#display_info").val("Tiket anda tidak valid/belum dibayarkan");
                        $("#nm_acara").val(result[0].nama+" - "+result[0].lokasi);
                        $("#nm_member").val(result[0].valid_fullnm);
                        $("#idtiket").val(result[0].id_etiket);
                        $("#nomember").val(result[0].valid_dfno);

                        $("#status").val(result[0].status);

                    }
                    else if(result[0].status==2)
                    {
                        $("#display_info").val("Tiket anda sudah dipakai");
                        $("#nm_acara").val(result[0].nama+" - "+result[0].lokasi);
                        $("#nm_member").val(result[0].valid_fullnm);
                        $("#idtiket").val(result[0].id_etiket);
                        $("#nomember").val(result[0].valid_dfno);

                        $("#status").val(result[0].status);

                    }
                    else if(result==null)
                    {
                        $("#display_info").val("Data tidak ditemukan");;
                        $("#nm_acara").val("Data tidak ditemukan");
                        $("#nm_member").val("Data tidak ditemukan");
                        $("#idtiket").val("Data tidak ditemukan");
                        $("#nomember").val("Data tidak ditemukan");


                        $("#status").val(result[0].status);

                    }
//						$( '#display_info' ).html("ID Member tidak valid");

//
//						document.getElementById("#display_info").value ="ID Member tidak valid";

                }
            });

        } else
        {
            $( '#display_info' ).html("Please Enter Some Words");
        }
    }

    function simpandata(){
        alert('coba ajax');
        /*$.ajax({

         type : "POST",
         url  : "   <!--?php echo base_url();?--> dtc/Cdtc_mbr/cariID",
         data : {idm: idnya},
         dataType: "json",

         success : function(result)
         {
         $( '#display_info' ).html(result[0].fullnm + " [" +result[0].rank+ "]");
         }
         });*/
    }


</script>

<div class="mainForm">
    <div id="frmRegumroh">

        <form class="form-horizontal" id="frmListReg" name="frmListReg" method="post" action="<?php echo $action; ?>" >
            <fieldset>
                <div class="control-group">
                    <div class="form-group">
                        <label class="control-label col-sm-4" >Search by:</label>
                        <div class="col-sm-8">
                            <select class="form-control" data-live-search="true" id="opsi" name="opsi" onchange="pilihan()">
                                <option value="0">No tiket</option>
                                <option value="1">No member</option>
                            </select>
                        </div>
                    </div>



                    <div class="form-group" name="pilihan" id="pilihan" hidden>
                        <label class="control-label col-sm-4" >Acara:</label>
                        <div class="col-sm-8">
                            <select class="form-control" data-live-search="true" id="acara" name="acara">
                                <option></option>
                                <?php
                                foreach ($alamat as $kota) {

                                    echo "<option value=\"$kota->id\">$kota->nama - $kota->lokasi ($kota->event_date)</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="typeahead">Input:</label>
                        <div class="col-sm-8">
                            <input type="text" id="search" name="search" onchange="loaddatavalid()" style="width: 500px;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" >Nama Acara</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="nm_acara"  name="nm_acara" readonly style="width: 500px;">
                            <!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" >Nama Member</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="nm_member"  name="nm_member" readonly style="width: 500px;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" >Status</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="display_info"  name="display_info" readonly style="width: 500px;">
                            <!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
                        </div>
                    </div>

                    <input type="text" class="form-control" id="idtiket"  name="idtiket" readonly style="width: 500px;">
                    <input type="text" class="form-control" id="nomember"  name="nomember" readonly style="width: 500px;">
                    <input type="text" class="form-control" id="status"  name="status" readonly style="width: 500px;">


                    <div>
                        <!--                        <input type="input" name="qtytot" id="qtytot" readonly  value="0">-->

                        <!--                        <button type="submit" class="btn btn-success">Submit</button>-->
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost('frmListReg','be_dtc/submit')"/>
                        <!--                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be_dtc/get')"/>-->
                    </div>
                </div>
            </fieldset>
            <div class="result"></div>
        </form>
    </div>
</div>


<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>
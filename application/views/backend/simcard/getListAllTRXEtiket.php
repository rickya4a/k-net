<div class="mainForm">
    <form class="form-horizontal"  id="formInputProduct" enctype="multipart/form-data" >
        <fieldset>

            <div class="control-group">
                <div class="controls" style="margin-left: 0px;">
                    <form id=listPrd>
                        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead><tr><th colspan=9>List E-Ticket</th></tr>
                            <tr bgcolor=#f4f4f4>
<!--                                <th>No</th>-->

                                <th>Nama Member</th>
                                <th>Valid Member</th>

                                <th>Nomor Tiket</th>
                                <th>Event</th>
                                <th>Tanggal Event</th>
                                <th>Tanggal Beli</th>

                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            $i=0;
                            foreach($listPrd as $list) {
                                echo "<tr id=\"$i\">";
//                                echo "<td><div align=center>$i</div></td>";

                                echo "<td><div align=center>$list->fullnm</div></td>";
                                echo "<td><div align=center>$list->valid_fullnm</div></td>";

                                echo "<td><div align=center>$list->notiket</div></td>";
                                $edate= date("d/m/Y", strtotime($list->event_date));
                                $bdate= date("d/m/Y", strtotime($list->tanggalbeli));

                                echo "<td><div align=center>$list->nama - $list->lokasi </div></td>";
                                echo "<td><div align=center>$edate</div></td>";
                                echo "<td><div align=center>$bdate</div></td>";

                                if($list->stats==0) {
                                    echo "<td><div align=center>Aktif</div></td>";
                                } else {
                                    echo "<td><div align=center>Non-Aktif</div></td>";
                                }
                                $ondelete = "All.deleteFormData('etiket/delete/', '$list->idtiket', 'etiket/list3')";
                                $arr = array(
                                    "update" => "Product.getKlaimEtiket('$list->idtrx')",
                                    "delete" => $ondelete
                                );
                                echo btnUpdateDelete($arr);
                                echo "</tr>";
                                $i++;
                            }

                            ?>

                            </tbody></tr>
                        </table></form>
                </div>



            </div>
        </fieldset>

    </form>
    <div class="result"></div>
</div><!--/end mainForm-->
<script>
    $( document ).ready(function() {
        All.set_datatable();
    });
</script>
<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>



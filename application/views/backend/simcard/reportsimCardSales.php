<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        <th colspan="7">List simCard Sales</th>
    </tr>
    <tr>
        <th width="10%">No.</th>
        <th width="25%">Tanggal</th>
        <th width="15%">Qty</th>
        <th width="25%">Total Nominal(Rp)</th>
        <th width="25%">Aksi Report</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no = 1;

    foreach ($sql->result() as $r) {
        $now=gmdate("d-m-Y", time()+60*60*5.6);
        $kode=$r->kode;
        $qty=$r->qty;
        $total= 105000 * $qty;
        $ntotal= number_format($total,0,',','.');

        if(strlen($kode) == 12){
            $tombol="Cetak";
            $link="#";
            $href="simcard/printtxt/".$kode;
        }else{
            $tombol="Generate + Cetak";
            $link="generate(".$qty.")";
            $href="#";
        }
        //echo strlen($kode);
        ?>

        <tr>

            <td align="center"><?php echo $no;?></td>
            <td align="center"><?php echo $now;?></td>
            <td align="center"><?php echo $qty;?></td>
            <td align="right"><?php echo $ntotal; ?></td>
            <td align="center"><a href="<?php echo $href;?>" onclick="<?php echo $link;?>"><?php echo $tombol;?></a></td>
        </tr>
        <?php

        $no++;
    }
    ?>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function()
    {
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

    function generate(qty){
        $.ajax({
            type : "POST",
            url: All.get_url('simcard/generate/'+ qty ),
            dataType : 'json',
            data : {jumlah: qty},
            success: function(result){
                //alert(result.message);

              if(result.response == "true"){
                  alert(result.message);
                  All.reload_page('backend/Simcard/reportsimCard');
                  cetak(result.kode);
                  //document.frm_reg.nama.value = result.message;
                }
                else {
                   alert(result.message);

                }
            }
        });
    }

    function cetak(kode){

        window.open('http://www.k-net.co.id/backend/simcard/printtxt/'+kode, '_parent');

        /*$.ajax({
            type : "POST",
            url: All.get_url('simcard/printtxt'),
            dataType : 'json',
            success: function(result){
                alert(result.message);
            }
        });*/
    }
</script>
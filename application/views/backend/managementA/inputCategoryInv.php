<div class="mainForm">
  <form class="form-horizontal" id="frmCatInv">
    <fieldset>      
      <div class="control-group">
        <label class="control-label">ID Kategori</label>
        <input type="text" id="id_cat" name="id_cat" class="span3 typehead setReadOnly" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/catInv/check/','id_cat',this.value)"/>
    
        <label class="control-label">Nama Kategori</label>
        <input type="text" id="cat_nm" name="cat_nm" class="span5 typehead" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/catInv/check/','cat_nm',this.value)"/>
    
        <label class="control-label">Catatan</label>
        <input type="text" id="remarks" name="remarks" class="span5 typehead"/>
    
        <label class="control-label">Status</label>
        <select id="status" name="status">
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
        
        <!--<div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit" id="saveBtn" name="save" value="Submit" onclick="All.inputFormData('ma/catInv/save', this.form.id)"/>
            <input type="reset" class="btn btn-reset" name="reset"  value="Reset"/>
            <input type="button" class="btn btn-success"name="viewList"  value="View List" onclick="All.getListData('ma/catInv/list')"/>
        </div>-->
        
        <div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit" name="save" id="save" value="Submit" onclick="All.inputFormData('ma/catInv/save', this.form.id)" />
		    <button type="reset" class="btn">Cancel</button>
            <button type="button" class="btn btn-success" name="viewList" id="viewList" onclick="All.getListData('ma/catInv/list')">View List</button>
        </div>
        
        <div class="controls" id="upd_btn" style="display: none;">
            <input type="button" class="btn btn-primary" name="update" id="update" value="update" onclick="All.updateFormData('ma/catInv/update', 'frmCatInv', 'ma/catInv/list')" />
    		<button type="reset" class="btn btn-warning" onclick="All.cancelUpdateForm()">Cancel Update</button>
            <button type="button" class="btn btn-success" name="view" id="view" onclick="All.getListData('ma/catInv/list');">View List</button>
        </div>
        
      </div>
     </fieldset>
  </form>   
  <div class="result"></div>
</div>

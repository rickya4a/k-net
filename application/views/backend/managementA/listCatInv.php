<?php   if($listCat == null) {
  	 echo setErrorMessage("No result found");
  } else {
?>
<form id="listCats">
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead><tr><th colspan="5">List Kategori Asset</th></tr>
    <tr>
        <th>No</td>
        <th>ID Kategori</th>
        <th>Nama Kategori</th>
        <th>Status</th>
        <th>Act</th>
    </tr>
    <tbody>
    <?php
        $no = 1;
        foreach($listCat as $list)
        {
            
    ?>
    
    <tr id="<?php echo $no;?>">
        <td style="text-align: center;"><?php echo $no;?></td>
        <td><input type="hidden" id="id_cat<?php echo $no;?>" value="<?php echo $list->id_cat;?>"/><?php echo $list->id_cat;?></td>
        <td><?php echo $list->cat_nm;?></td>
        <td style="text-align: center;"><?php echo $list->status;?></td>
        <td style="text-align: center;">
            <a class="btn btn-mini btn-info" onclick="Asset.getUpdateInvCat(<?php echo $no;?>)"><i class="icon-edit icon-white"></i></a>
            <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/catInv/delete/', '<?php echo $list->id_cat;?>', 'ma/catInv/list')">
                <i class="icon-trash icon-white"></i>
            </a>
        </td>
    </tr>
    <?php
        $no++; }
    ?>
    </tbody>
    </table>
</form>
<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
</script>

<?php
}
?>
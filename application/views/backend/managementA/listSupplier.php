<?php   if($listSupp == null) {
  	 echo setErrorMessage("No result found");
  } else {
?>
<form id="listSupps">
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead><tr><th colspan="7">List Supplier</th></tr>
    <tr>
        <th>No</td>
        <th>ID Supplier</th>
        <th>Nama Supplier</th>
        <th>Nama Perusahaan</th>
        <th>Tel. HP</th>
        <th>Catatan</th>
        <th>Act</th>
    </tr>
    <tbody>
    <?php
        $no = 1;
        foreach($listSupp as $list)
        {
            
    ?>
    
    <tr id="<?php echo $no;?>">
        <td style="text-align: center;"><?php echo $no;?></td>
        <td><input type="hidden" id="id_supplier<?php echo $no;?>" value="<?php echo $list->id_supplier;?>"/><?php echo $list->id_supplier;?></td>
        <td><?php echo $list->nm_supplier;?></td>
        <td style="text-align: center;"><?php echo $list->company_nm;?></td>
        <td style="text-align: center;"><?php echo $list->tel_hp_supplier;?></td>
        <td style="text-align: center;"><?php echo $list->remarks;?></td>
        <td style="text-align: center;">
            <a class="btn btn-mini btn-info" onclick="Asset.getUpdateSupplier(<?php echo $no;?>)"><i class="icon-edit icon-white"></i></a>
            <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/supplier/delete/', '<?php echo $list->id_supplier;?>', 'ma/supplier/list')">
                <i class="icon-trash icon-white"></i>
            </a>
        </td>
    </tr>
    <?php
        $no++; }
    ?>
    </tbody>
    </table>
</form>
<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
</script>

<?php
}
?>
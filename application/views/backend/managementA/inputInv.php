<div class="mainForm">
  <form class="form-horizontal" id="frmInv">
    <fieldset>      
      <div class="control-group">
        <label class="control-label">Kode Asset</label>
        <input type="text" id="id_inventory" name="id_inventory" class="span3 typehead setReadOnly" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/inv/check/','id_inventory',this.value)"/>
    
        <label class="control-label">Nama Asset</label>
        <input type="text" id="nm_inventory" name="nm_inventory" class="span5 typehead setReadOnly" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/inv/check/','nm_inventory',this.value)"/>
  
    
        <label class="control-label">Kategori</label>
        <select id="cat_inv" name="cat_inv" class="span4">
            <?php 
                foreach($listCat as $list){
                    if($list->status == '1'){
                    
                    
            ?>
                <option value="<?php echo $list->id_cat;?>"><?php echo $list->id_cat." - ".$list->cat_nm;?></option>    
            <?php 
                }}
            ?>
        </select>
        <label class="control-label">Supplier</label>
        <select id="list_supp" name="list_supp" class="span4">
            <?php 
                foreach($listSupplier as $supp){
            ?>
                <option value="<?php echo $supp->id_supplier;?>"><?php echo $supp->id_supplier." - ".$supp->company_nm;?></option>    
            <?php 
                }
            ?>
        </select>
        
        <label class="control-label">Kuantitas Tersedia</label>
        <input type="text" id="qtyInv" name="qtyInv" class="span3 typehead" value="0" readonly="yes"/>
        
        <label class="control-label">Kuantitas Permintaan</label>
        <input type="text" id="qtyReq" name="qtyReq" class="span3 typehead" value="0" />
        
        <label class="control-label">Harga Beli</label>
        <input type="text" id="value" name="value" class="span3 typehead" placeholder="price of inventory"/>
        
        <label class="control-label">Status Asset</label>
        <select id="status_invv" name="status_invv" class="span4">
            <option value="0">Tersedia</option>
            <option value="1">Sedang Diproses</option>
            <option value="3">Ditolak</option>
            <option value="4">Tidak Tersedia</option>
        </select>
        
        <label class="control-label">Catatan</label>
        <input type="text" id="remarks" name="remarks" class="span5 typehead"/>
        
        <div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit setReadOnly" name="save" id="save" value="Submit" onclick="All.inputFormData('ma/inv/save', this.form.id)" />
		    <button type="reset" class="btn">Cancel</button>
            <button type="button" class="btn btn-success setReadOnly" name="viewList" id="viewList" onclick="All.getListData('ma/inv/list')">View List</button>
        </div>
        
        <div class="controls" id="upd_btn" style="display: none;">
            <input type="hidden" name="idToHistory" id="idToHistory"/>
            <input type="button" class="btn btn-primary" name="update" id="update" value="update" onclick="All.updateFormData('ma/inv/update', 'frmInv', 'ma/inv/list')" />
    		<button type="reset" class="btn btn-warning setReadOnly" onclick="All.cancelUpdateForm()">Cancel Update</button>
            <button type="button" class="btn btn-success" name="view" id="view" onclick="All.getListData('ma/inv/list');">View List</button>
        </div>
        
      </div>
     </fieldset>
  </form>   
  <div class="result"></div>
</div>

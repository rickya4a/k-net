<div class="mainForm">
  <form class="form-horizontal" id="frmMut">
    <fieldset>      
      <div class="control-group">
        <label class="control-label">ID Karyawan</label>
        <!--<input type="text" id="id_employee" name="id_employee" class="span3 typehead" placeholder="Enter tab to check data" onchange="Asset.getDetailEmployee(this.value)" tabindex="1" onfocus=""/>-->
        <select id="id_employee" name="id_employee" class="span4 setReadOnly" tabindex="2" onchange="Asset.getDetailEmployee(this.value)">
            <option value="">Select Here</option>
            <?php 
                $no = 1;
                foreach($listEmpl as $listEmp){
                    //if($list->qty_avail != '0' || $list->status_inv == '0'){  
            ?>
                <option value="<?php echo $listEmp->id_employee;?>" id="<?php echo $no;?>"><?php echo $listEmp->id_employee." - ".$listEmp->employee_nm;?></option>    
            <?php 
                $no++; }  //}     
            ?>
        </select>
        
        <label class="control-label">Nama Karyawan</label>
        <input type="text" id="nm_employee" name="nm_employee" class="span5 typehead" readonly="yes"/>
        
        <label class="control-label">Divisi</label>
        <input type="text" id="divisi" name="divisi" class="span5 typehead" readonly="yes"/>
    
        <label class="control-label">Jenis Asset</label>
        <select id="id_inventory" name="id_inventory" class="span4 setReadOnly" tabindex="2" onchange="Asset.getDetailInv(this.value)">
            <option value="">Select Here</option>
            <?php 
                $no = 1;
                foreach($listInv as $list){
                    //if($list->qty_avail != '0' || $list->status_inv == '0'){  
            ?>
                <option value="<?php echo $list->id_inventory;?>" id="<?php echo $no;?>"><?php echo $list->id_inventory." - ".$list->nm_inventory;?></option>    
            <?php 
                $no++; }  //}     
            ?>
        </select>
        
        <label class="control-label">Kuantitas Tersedia</label>
        <input type="text" id="qtyAvail" name="qtyAvail" class="span3 typehead" value="0" readonly="yes"/>
        
        <label class="control-label">Kuantitas Permintaan</label>
        <input type="text" id="qtyReq" name="qtyReq" class="span3 typehead setReadOnly" value="0" tabindex="3"/>
        
        
        
        <label class="control-label">Status Mutasi</label>
        <select id="status_invv" name="status_invv" class="span4" tabindex="4">
            <option value="0">Dipinjam</option>
            <option value="1">Dikembalikan</option>
            <option value="2">Hilang</option>
        </select>
        
        <label class="control-label">Catatan</label>
        <input type="text" id="remarks" name="remarks" class="span5 typehead" tabindex="5"/>
        
        <div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit" name="save" id="save" value="Submit" onclick="Asset.inputFormData('ma/mutasi/save', this.form.id)" />
		    <button type="reset" class="btn">Cancel</button>
            <button type="button" class="btn btn-success" name="viewList" id="viewList" onclick="All.getListData('ma/mutasi/list')">View List</button>
        </div>
        
        <div class="controls" id="upd_btn" style="display: none;">
            <input type="hidden" name="id_muts" id="id_muts"/>
            <input type="hidden" name="id_invent" id="id_invss"/>
            <input type="button" class="btn btn-primary" name="update" id="update" value="update" onclick="Asset.updateFormData('ma/mutasi/update', 'frmMut', 'ma/mutasi/list')" />
    		<button type="reset" class="btn btn-warning" onclick="All.cancelUpdateForm()">Cancel Update</button>
            <button type="button" class="btn btn-success" name="view" id="view" onclick="All.getListData('ma/mutasi/list')">View List</button>
        </div>
        
      </div>
     </fieldset>
  </form>   
  <div class="result"></div>
</div>

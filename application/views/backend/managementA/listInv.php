<?php   if($listInvv == null) {
  	 echo setErrorMessage("No result found");
  } else {
?>
<form id="listInvs">
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead><tr><th colspan="9">List Asset</th></tr>
    <tr>
        <th>No</td>
        <th>Kode Asset</th>
        <th>Nama Asset</th>
        <th>Kategori</th>
        <th>Supplier</th>
        <th>Status</th>
        <th>Qty Tersedia</th>
        <th>Qty Permintaan</th>
        <th>Act</th>
    </tr>
    <tbody>
    <?php
        $no = 1;
        foreach($listInvv as $list)
        {
            if($list->qty_avail == '0'){
                $qtyAvail = '0';
            }else{
               $qtyAvail = $list->qty_avail; 
            }
            
            if($list->qty_req == '0'){
                $qtyReq = '0';
            }else{
                $qtyReq = $list->qty_req;
            }
            
            if($list->status_inv == '0'){
                $stat = 'Tersedia';
                $color = 'green';
            }elseif($list->status_inv == '1'){
                $stat = 'Sedang Diproses';
                $color = 'blue';
            }elseif($list->status_inv == '3'){
                $stat = 'Ditolak';
                $color = 'red';
            }else{
                $stat = 'Tidak Tersedia';
                $color = 'red';
            }
            
    ?>
    
    <tr id="<?php echo $no;?>">
        <td style="text-align: center;"><?php echo $no;?></td>
        <td><input type="hidden" class="id_inventory<?php echo $no;?>" id="id_inventory<?php echo $no;?>" value="<?php echo $list->id_inventory;?>"/><?php echo $list->id_inventory;?></td>
        <td><?php echo $list->nm_inventory;?></td>
        <td><?php echo $list->id_cat;?></td>
        <td style="text-align: center;"><?php echo $list->nm_supplier;?></td>
        <td style="text-align: center;color: <?php echo $color;?>;"><?php echo $stat;?></td>
        <td style="text-align: center;">&nbsp;<?php echo $qtyAvail;?></td>
        <td style="text-align: center;"><?php echo $list->qty_req;?></td>
        <td style="text-align: center;">
            <?php
                //if($list->status_inv == '3'){
            ?>
                <!--<a class="btn btn-mini btn-info" onclick="Asset.getUpdateInv('<?php echo $no;?>')"><i class="icon-edit icon-white"></i></a>
                <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/inv/delete/', '<?php echo $list->id_inventory;?>', 'ma/inv/list')">
                    <i class="icon-trash icon-white"></i>
                </a>-->
            <?php
                //}else
                if($list->status_inv == '0' || $list->qty_redemp != '0'){
            ?>
                <a class="btn btn-mini btn-info" onclick="Asset.getUpdateInv('<?php echo $no;?>')"><i class="icon-edit icon-white"></i></a>
                
            <?php }else{
            ?>
                <a class="btn btn-mini btn-info" onclick="Asset.getUpdateInv('<?php echo $no;?>')"><i class="icon-edit icon-white"></i></a>
                <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/inv/delete/', '<?php echo $list->id_inventory;?>', 'ma/inv/list')">
                    <i class="icon-trash icon-white"></i>
                </a>
            <?php }?>
        </td>
    </tr>
    <?php
        $no++; }
    ?>
    </tbody>
    </table>
</form>
<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
</script>

<?php
}
?>
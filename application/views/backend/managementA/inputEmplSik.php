<div class="mainForm">
  <form class="form-horizontal" id="frmSik">
    <fieldset>      
      <div class="control-group">
          
        <label class="control-label">Nama Karyawan</label>
        <input type="text" id="nm_employee" name="nm_employee" class="span5 typehead" placeholder="Nama Lengkap" />
        
        <label class="control-label">Alamat Karyawan</label>
        <input type="text" id="addr_empl" name="addr_empl" class="span5 typehead" placeholder="Alamat Lengkap" />
        
        <label class="control-label">Tempat lahir</label>
        <input type="text" id="birthplace" name="birthplace" class="span5 typehead" placeholder="Tempat Lahir" />
        
        <label class="control-label">Tanggal Lahir</label>
        <div class="controls">
             <?php echo datebirth_combo(0, 80, 'birthdt'); ?>
        </div>
        
        <label class="control-label">No Telp.</label>
        <div class="controls">
             <input type="text" id="telhp" name="telhp" class="span4 typehead" placeholder="Nomor telpon yang bisa dihubungi" />
        </div> 
        
        <label class="control-label">Status Pernikahan</label>
        <div class="controls">
            <select id="status" name="status" class="span3 typehead">
                <option value="">Silahkan Pilih</option>
                <option value="0">Belum Nikah</option>
                <option value="1">Sudah Nikah</option>
            </select>
        </div> 
        
        <label class="control-label">Divisi</label>
        <select id="divisi" name="divisi" class="span3 typehead">
            <?php 
                if($listDivisi== null){
                    echo "<option value=\"no\">No Divisi</option>";
                }else{
                    foreach($listDivisi as $list){            
            ?>
                <option value="<?php echo $list->divisi;?>"><?php echo $list->divisi;?></option>    
            <?php 
                }}
            ?>
        </select>
        
        <label class="control-label">Tanggal Masuk</label>
        <div class="controls">
            <input name="tgl_masuk" id="dtpicker" class="span3 typehead"/>
        </div>
        
        <label class="control-label">Status Karyawan</label>
        <div class="controls">
            <select id="status" name="status" class="span3 typehead">
                <option value="">Silahkan Pilih</option>
                <option value="1">Aktif</option>
                <option value="0">Non Aktif</option>
            </select>
        </div>
        
        <div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit" name="save" id="save" value="Submit" onclick="All.inputFormData('sik/employee/save', this.form.id)" />
		    <button type="reset" class="btn">Cancel</button>
            <button type="button" class="btn btn-success" name="viewList" id="viewList" onclick="All.getListData('ma/employee/list')">View List</button>
        </div>
        
        <div class="controls" id="upd_btn" style="display: none;">
            <input type="button" class="btn btn-primary" name="update" id="update" value="update" onclick="All.updateFormData('sik/employee/update', 'frmSik', 'sik/employee/list')" />
    		<button type="reset" class="btn btn-warning" onclick="All.cancelUpdateForm()">Cancel Update</button>
            <button type="button" class="btn btn-success" name="view" id="view" onclick="All.getListData('sik/employee/list');">View List</button>
        </div>
        
      </div>
     </fieldset>
  </form>   
  <div class="result"></div>
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " #dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'dd/mm/yy',
		}).datepicker("setDate", new Date());;
	});	
</script>

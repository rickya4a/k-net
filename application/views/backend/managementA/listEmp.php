<?php   if($listEmpl == null) {
  	 echo setErrorMessage("No result found");
  } else {
?>
<form id="listInvs">
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead><tr><th colspan="6">List Karyawan</th></tr>
    <tr>
        <th>No</td>
        <th>ID Karyawan</th>
        <th>Nm Karyawan</th>
        <th>Divisi</th>
        <th>Status</th>
        <th>Act</th>
    </tr>
    <tbody>
    <?php
        $no = 1;
        foreach($listEmpl as $list)
        {
            if($list->status == '0'){
                $stat = 'Non Active';
                $color = 'red';
            }else{
                $stat = 'Active';
                $color = 'green';
            }
            
    ?>
    
    <tr id="<?php echo $no;?>">
        <td style="text-align: center;"><?php echo $no;?></td>
        <td><input type="hidden" id="id_employee<?php echo $no;?>" value="<?php echo $list->id_employee;?>"/><?php echo $list->id_employee;?></td>
        <td><?php echo $list->employee_nm;?></td>
        <td><?php echo $list->divisi;?></td>
        <td style="text-align: center;"><?php echo $list->status;?></td>
        <td style="text-align: center;">
            <a class="btn btn-mini btn-info" onclick="Asset.getUpdateEmpl('<?php echo $list->id_employee;?>')"><i class="icon-edit icon-white"></i></a>
            <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/employee/delete/', '<?php echo $list->id_employee;?>', 'ma/employee/list')">
                <i class="icon-trash icon-white"></i>
            </a>
        </td>
    </tr>
    <?php
        $no++; }
    ?>
    </tbody>
    </table>
</form>
<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
</script>

<?php
}
?>
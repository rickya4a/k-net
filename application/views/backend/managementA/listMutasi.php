<?php   if($listMutasi == null) {
  	 echo setErrorMessage("No result found");
  } else {
?>
<form id="listInvs">
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead><tr><th colspan="7">List Mutasi</th></tr>
    <tr>
        <th>No</td>
        <th>Nm Karyawan</th>
        <th>Divisi</th>
        <th>Asset</th>
        <th>Qty</th>
        <th>Status</th>
        <th>Act</th>
    </tr>
    <tbody>
    <?php
        $no = 1;
        foreach($listMutasi as $list)
        {            
            if($list->status == '0'){
                $stat = 'Borrowed';
                $color = '#ff9900';
            }elseif($list->status == '1'){
                $stat = 'Returned';
                $color = 'green';
            }else{
                $stat = 'Elimated';
                $color = 'red';
            }
            
    ?>
    
    <tr id="<?php echo $no;?>">
        <td style="text-align: center;"><?php echo $no;?></td>
        <td><input type="hidden" id="id_mut<?php echo $no;?>" name="id_mut" value="<?php echo $list->id;?>"/><?php echo $list->employee_nm;?></td>
        <td><?php echo $list->divisi;?></td>
        <td><?php echo $list->id_inventory." / ".$list->nm_inventory;?></td>
        <td style="text-align: center;"><?php echo $list->qty;?></td>
        <td style="text-align: center;color: <?php echo $color;?>;"><?php echo $stat;?></td>
        <td style="text-align: center;">
            <?php
                if($list->status == '2' || $list->status == '0'){
            ?>
                <a class="btn btn-mini btn-info" onclick="Asset.getUpdateMutasi('<?php echo $no;?>')"><i class="icon-edit icon-white"></i></a>
                <!--<a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/inv/delete/', '<?php echo $list->id_inventory;?>', 'ma/inv/list')">
                    <i class="icon-trash icon-white"></i>
                </a>-->
            <?php
                }elseif($list->status == '1'){
            ?>
                <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/mutasi/delete/', '<?php echo $list->id;?>', 'ma/mutasi/list')">
                    <i class="icon-trash icon-white"></i>
                </a>
                
            <?php }else{
                
            ?>
                <a class="btn btn-mini btn-info" onclick="Asset.getUpdateMutasi('<?php echo $no;?>')"><i class="icon-edit icon-white"></i></a>
                <a class="btn btn-mini btn-danger" onclick="All.deleteFormData('ma/mutasi/delete/', '<?php echo $list->id;?>', 'ma/mutasi/list')">
                    <i class="icon-trash icon-white"></i>
                </a>
        </td>
    </tr>
    <?php
        }$no++; }
    ?>
    </tbody>
    </table>
</form>
<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
</script>

<?php
}
?>
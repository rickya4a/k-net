<div class="mainForm">
  <form class="form-horizontal" id="frmSupplier">
    <fieldset>      
      <div class="control-group">
        <label class="control-label">ID Supplier</label>
        <input type="text" id="id_supplier" name="id_supplier" class="span3 typehead setReadOnly" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/supplier/check/','id_supplier',this.value)"/>
    
        <label class="control-label">Nama Supplier</label>
        <input type="text" id="nm_supplier" name="nm_supplier" class="span5 typehead" />
    
        <label class="control-label">Nama Perusahaan</label>
        <input type="text" id="company" name="company" class="span5 typehead"/>
        
        <label class="control-label">No Hp Supplier</label>
        <input type="text" id="tel_hp" name="tel_hp" class="span5 typehead"/>
        
        <label class="control-label">Catatan</label>
        <input type="text" id="remarks" name="remarks" class="span5 typehead"/>
        
        <div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit" name="save" id="save" value="Submit" onclick="All.inputFormData('ma/supplier/save', this.form.id)" />
		    <button type="reset" class="btn">Cancel</button>
            <button type="button" class="btn btn-success" name="viewList" id="viewList" onclick="All.getListData('ma/supplier/list')">View List</button>
        </div>
        
        <div class="controls" id="upd_btn" style="display: none;">
            <input type="button" class="btn btn-primary" name="update" id="update" value="update" onclick="All.updateFormData('ma/supplier/update', 'frmSupplier', 'ma/supplier/list')" />
    		<button type="reset" class="btn btn-warning" onclick="All.cancelUpdateForm()">Cancel Update</button>
            <button type="button" class="btn btn-success" name="view" id="view" onclick="All.getListData('ma/supplier/list');">View List</button>
        </div>
        
      </div>
     </fieldset>
  </form>   
  <div class="result"></div>
</div>

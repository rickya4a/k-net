<div class="mainForm">
  <form class="form-horizontal" id="formMutasiRpt" method="post" target="_blank" action="<?php echo site_url("ma/mutasi/report/act/1");?>">
    <fieldset>      
      <div class="control-group"> 
      	
      	<label class="control-label" for="typeahead">Tipe Pencarian</label>                             
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Silahkan Pilih--</option>
        		<option value="0">Dipinjam</option>
        		<option value="1">Dikembalikan</option>
        		<option value="2">Hilang</option>
        	</select>
        </div>
        		      
        <?php
          echo datepickerFromTo("Tanggal Mutasi", "mut_froms", "mut_tos");
		  
        ?>
                               
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="Asset.getListMutasi()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input tabindex="4"  type="submit" class="btn btn-success" value="To Excel" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
		}).datepicker("setDate", new Date());;
	});	
</script>

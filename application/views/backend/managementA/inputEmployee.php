<div class="mainForm">
  <form class="form-horizontal" id="frmInv">
    <fieldset>      
      <div class="control-group">
        <label class="control-label">ID Karyawan</label>
        <input type="text" id="id_employee" name="id_employee" class="span3 typehead setReadOnly" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/employee/check/','id_employee',this.value)"/>
    
        <label class="control-label">Nama Karyawan</label>
        <input type="text" id="nm_employee" name="nm_employee" class="span5 typehead" placeholder="Enter tab to check data" onchange="All.checkDoubleInput('ma/employee/check/','employee_nm',this.value)"/>
  
    
        <label class="control-label">Divisi</label>
        <select id="divisi" name="divisi" class="span4">
            <?php 
                if($listDivisi== null){
                    echo "<option value=\"no\">No Divisi</option>";
                }else{
                    foreach($listDivisi as $list){            
            ?>
                <option value="<?php echo $list->divisi;?>"><?php echo $list->divisi;?></option>    
            <?php 
                }}
            ?>
        </select>
        
        <label class="control-label">Status</label>
        <select id="status" name="status">
            <option value="1">Aktif</option>
            <option value="0">Non Aktif</option>
        </select>
        
        <div class="controls" id="inp_btn">
            <input type="button" class="btn btn-primary .submit" name="save" id="save" value="Submit" onclick="All.inputFormData('ma/employee/save', this.form.id)" />
		    <button type="reset" class="btn">Cancel</button>
            <button type="button" class="btn btn-success" name="viewList" id="viewList" onclick="All.getListData('ma/employee/list')">View List</button>
        </div>
        
        <div class="controls" id="upd_btn" style="display: none;">
            <input type="button" class="btn btn-primary" name="update" id="update" value="update" onclick="All.updateFormData('ma/employee/update', 'frmInv', 'ma/employee/list')" />
    		<button type="reset" class="btn btn-warning" onclick="All.cancelUpdateForm()">Cancel Update</button>
            <button type="button" class="btn btn-success" name="view" id="view" onclick="All.getListData('ma/employee/list');">View List</button>
        </div>
        
      </div>
     </fieldset>
  </form>   
  <div class="result"></div>
</div>

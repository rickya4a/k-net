<?php if(empty($results)){
    echo "<div class = \"alert alert-error\" style=\"text-align:center;\">No Record</div> ";
}else{
    if($tipe == '1'){
	    	header("Content-type: application/vnd.ms-excel");
		    header("Content-Disposition: attachment; filename=ReportMutasi.xls" );
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");
			
			//$rptHead = "<tr><th>Report From Date </th><th>$from to $to</th></tr><tr></tr>";
			$border = '1';
		}else{
			//$rptHead = '';
			$border = '0';
		}
?>

<table border="<?php echo $border;?>" style="width: 100%;"  class='table table-striped table-bordered bootstrap-datatable datatable'>
    <thead>
        <tr>
            <th colspan='8' style="height: 45px;vertical-align: middle;font-size: medium;">List Mutasi</th>
        </tr>
		<tr style="background-color: #f5f4f4;">
            <th>No</th>
			<th style="width: 17%;">ID / Nama Karyawan</th>
			<th style="width: 15%;">Divisi</th>
			<th style="width: 25%;">ID / Nama Asset</th>
			<th style="width: 5%;">Qty</th>
			<th style="width: 12%;">Harga</th>
            <th style="width: 13%;">Tot Harga</th>
            <th style="width: 13%;">Status</th>
		</tr>
    </thead>
	<tbody>
        <?php
            $no = 1;
            foreach($results as $list){
                if($list->status == '0'){
                    $status = "Masih Pinjam";
                    $color = "#ff9900";
                }elseif($list->status == '1'){
                    $status = "Sudah Dikembalikan";
                    $color = "green";
                }else{
                    $status = "Hilang";
                    $color = "red";
                }
                $totAmount = $list->amount * $list->qty;
        ?>
        <tr>
            <td style="text-align: center;"><?php echo $no;?></td>
            <td><?php echo $list->id_employee." / ".$list->employee_nm;?></td>
            <td><?php echo $list->divisi;?></td>
            <td><?php echo $list->id_inventory." / ".$list->nm_inventory;?></td>
            <td style="text-align: right;"><?php echo $list->qty;?></td>
            <td style="text-align: right;"><?php echo number_format($list->amount,0,",",",");?></td>
            <td style="text-align: right;"><?php echo number_format($totAmount,0,",",",");?></td>
            <td style="text-align: right;color:<?php echo $color;?>;"><?php echo $status;?></td>
        </tr>
        <?php $no++; }?>
    </tbody>
</table>
<script type="text/javascript">
    All.set_datatable();
</script>
<?php }?>
<div class="mainForm">
  <form class="form-horizontal" id="shoppingVch">
    <fieldset>      
      <div class="control-group">
      	
      	<label class="control-label" for="typeahead">Jumlah Voucher</label>                             
        <div class="controls">
        	<input type="text" id="jml_voucher" name="jml_voucher" class="span4" />
        </div>
        <label class="control-label" for="typeahead">Maksimum Karakter</label>                             
        <div class="controls">
        	<input type="text" id="max_char" name="max_char" class="span4" placeholder="Max char no voucher termasuk prefix" />
        </div>
        <label class="control-label" for="typeahead">Prefix Voucher</label>                             
        <div class="controls">
        	<input type="text" id="prefix" name="prefix" class="span4" placeholder="Prefix No Voucher. Cnth:VC" />
        </div>
        <label class="control-label" for="typeahead">Nominal Voucher</label>                             
        <div class="controls">
        	<input type="text" id="nominal" name="nominal" class="span4" placeholder="Nilai voucher" />
        </div>
        <label class="control-label" for="typeahead">Tgl Expire</label>                             
        <div class="controls">
        	<input type="text" id="expiredt" name="expiredt" class="span4 dtpicker" />
        </div>
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id, 'shop/vch/create/preview')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    
  </form> 
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		}).datepicker("setDate", new Date());
		
		All.set_datatable();
	});	
</script>
<?php
	if($ssr['trx_ssr'] == null) {
		echo setErrorMessage();
	} else {
	  $trx_ssr = $ssr['trx_ssr'];
	  if($trx_ssr != null) {
	  //print_r($trx_ssr);
	  $urlDetailTtp = "transklink/list/batchno/".$trx_ssr[0]->batchno;
?>
<form id="recoverForm">
<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
		<th colspan="4">Data SSR</th>
	</tr>	
	<tr>
		<td width="15%">SSR No / SSR Date</td>
		<td>
			<?php echo $trx_ssr[0]->batchno. " / ".$trx_ssr[0]->batchdt; ?>
		    <input type="hidden" id="batchno" name="batchno" value="<?php echo $trx_ssr[0]->batchno; ?>" />
			<input type="hidden" id="flag_recover" name="flag_recover" value="<?php echo $trx_ssr[0]->flag_recover; ?>" />
		</td>
		<td width="15%">Stockist</td>
		<td><?php echo $trx_ssr[0]->sc_dfno; ?></td>
	</tr>
	<tr>
		<td>CN No</td>
		<td><?php echo $trx_ssr[0]->csno; ?></td>
		<td>C/O Stockist</td>
		<td><?php echo $trx_ssr[0]->sc_co; ?></td>
	</tr>
	<tr>
		<td>Total DP / BV</td>
		<td><?php echo number_format($trx_ssr[0]->total_dp, 0, ",","."). " / ". number_format($trx_ssr[0]->total_bv, 0, ",","."); ?></td>
		<td>Main Stockist</td>
		<td><?php echo $trx_ssr[0]->loccd; ?></td>
	</tr>
	<tr>
		<td>Recover Status</td>
		<td><?php $recover = $trx_ssr[0]->flag_recover == "1" ? "Yes" : "No"; echo $recover; ?></td>
		<td>Create Name</td>
		<td><?php echo $trx_ssr[0]->createnm; ?></td>
	</tr>
	<tr>
		<td>Total TTP</td>
		<td><?php echo $trx_ssr[0]->jum_ttp; ?>&nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-mini btn-success" value="View List Transaction" onclick="All.ajaxShowDetailonNextForm('<?php echo $urlDetailTtp; ?>')" /></td>
		<td>Bonus Month</td>
		<td><?php echo $trx_ssr[0]->bnsperiod; ?></td>
	</tr>
</table>

<?php
//check apakah trx sudah di generate/belum
//sudah dibuat CN atau belum
$recover_able = 0;
if($trx_ssr[0]->flag_batch != "0" && $trx_ssr[0]->flag_batch != "2" && 
   $trx_ssr[0]->batchno != null && $trx_ssr[0]->batchno != "" && ($trx_ssr[0]->csno == null || $trx_ssr[0]->csno == "")) {
   $recover_able = 1;
   $url = 'transklink/recover/ssr';
   echo "<input type=\"button\" id=\"btn_recover\" name=\"btn_recover\" class=\"btn btn-mini btn-primary\" value=\"Recover Sales Report\" onclick=\"recoverSSR(this.form.id,'$url')\" />";
} 
echo "<input type=\"hidden\" id=\"recoverable\" name=\"recoverable\" value=\"$recover_able\" />";
}
	$newtrp_stt = 1;
	$newtrp = $ssr['newtrp'];
	if($newtrp != null) {
		echo "<table id=\"tbl_newtrp\" width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
		echo "<tr><th colspan=4>Data Incoming Payment</th></tr>";
		echo "<tr>";
		echo "<th>SSR No</th>";
		echo "<th>Inc Pay No</th>";
		echo "<th>Pay Type</th>";
		echo "<th>Amount</th>";
		echo "</tr>";
		
		$total_amount = 0;
		foreach($newtrp as $dtax) {
			echo "<tr>";
			echo "<td align=center>".$dtax->trcd."</td>";
			echo "<td align=center>".$dtax->trcd2."</td>";
			echo "<td align=center>".$dtax->description."</td>";
			echo "<td align=right>".number_format($dtax->amount, 0, ",",".")."</td>";
			echo "</tr>";
			$total_amount += $dtax->amount;
		}
		
		echo "<tr><td colspan=3 align=center>TOTAL AMOUNT</td><td align=right>".number_format($total_amount, 0, ",",".")."</td></tr>";
		echo "</table>";
	} else {
		$newtrp_stt = 0;
	}
	echo "<input type=\"hidden\" id=\"newtrp_stt\" name=\"newtrp_stt\" value=\"$newtrp_stt\" />";
	$deposit = $ssr['deposit'];
	$deposit_vc_stt = 1;
	if($deposit != null) {
		echo "<table id=\"tbl_deposit\" width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
		echo "<tr><th colspan=7>Deposit Voucher</th></tr>";
		echo "<tr>";
		echo "<th>Deposit No</th>";
		echo "<th>Stockist</th>";
		echo "<th>Total Deposit</th>";
		echo "<th>Total Usage</th>";
		echo "<th>Remain</th>";
		echo "<th>Create Date</th>";
		echo "<th width=5%>Status</th>";
		echo "</tr>";
		//print_r($deposit);
		
		//$total_amount = 0;
		foreach($deposit as $dtax2) {
		    $sisa = $dtax2->total_deposit - $dtax2->total_keluar;
			echo "<tr>";
			echo "<td align=center>".$dtax2->no_trx."</td>";
			echo "<td align=center>".$dtax2->loccd."</td>";
			echo "<td align=right>".number_format($dtax2->total_deposit, 0, ",",".")."</td>";
			echo "<td align=right>".number_format($dtax2->total_keluar, 0, ",",".")."</td>";
			echo "<td align=right>".number_format($sisa, 0, ",",".")."</td>";
			echo "<td align=center>".$dtax2->createdt."</td>";
			echo "<td align=center>";
			//echo "<select>";
			if($dtax2->status == "1") {
				//echo "<option value=\"0\">Inactive</option><option value=\"1\" selected=\"selected\">Aktif</option>";	
				echo "Aktif";
			} else {
				//echo "<option value=\"0\" selected=\"selected\">Inactive</option><option value=\"1\">Aktif</option>";	
				echo "Generated";
			}
			//echo "</select>";
			echo "</td>";	
			echo "</tr>";
			//$total_amount += $dtax2->amount;
		}
		echo "</table>";
	} else {
		$deposit_vc_stt = 0;
	}
	echo "<input type=\"hidden\" id=\"deposit_vc_stt\" name=\"deposit_vc_stt\" value=\"$deposit_vc_stt\" />";
?>

<?php
     echo "</form>";
	}
?>

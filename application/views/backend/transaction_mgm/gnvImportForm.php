
<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="post" id="formUploadGNV" action="<?php echo base_url('trans/sgo/import/preview'); ?>">
    <fieldset>      
      <div class="control-group">
       
         <label class="control-label" for="typeahead">File Excel to upload</label>
         <div class="controls" >
          	<input type="file" id="fileXls" name="myfile" class="span7 typeahead" />
         </div>
         
      	 <label class="control-label" for="typeahead">Warehouse</label>
         <div class="controls" >
          	<select id="whcd" name="whcd">
	        	<?php 
	        		foreach ($listWH as $data){
	        			$whcd = $data->kode;
						$whnm = $data->warehouse_name;
						$whid = $data->id;
	        			echo "<option value=\"$whcd***$whnm***$whid\">$whcd - $whnm</option>";
	        		}
				?>
        	</select>
         </div>
          
         <label class="control-label" for="typeahead">&nbsp</label>                             
         <div class="controls"  id="inp_btn">
        	<input type="button" class="btn btn-success" value="Preview Content File" onclick="be_trans_klink.readFromFile()" />
        	<input tabindex="5" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Upload Stock" onclick="be_trans_klink.saveGNVFileToDatabase()" />
            <input tabindex="6"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
         
        </div> <!-- end control-group -->
     </fieldset>
  </form>
  <div id="editor"></div>
  <div class="result"></div>
</div><!--/end mainForm-->
<script>
$(document).ready(function()
{
   
   $(All.get_active_tab() + " #fileXls").change(function () {
   		
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'xls':
            case 'xlsx':
            case 'csv':
            case 'txt':
            
                $(All.get_active_tab() + ' #uploadButton').attr('disabled', false);
                break;
            default:
                alert('This is not an allowed file type, only XLS/CSV/TXT file..!!');
                this.value = '';
        }
   });
   
   
   
});
</script>

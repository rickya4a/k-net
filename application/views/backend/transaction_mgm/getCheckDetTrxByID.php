<?php
	foreach ($result["header"] as $data) {
		$trcd = $data->trcd;
		$orderno = $data->orderno;
		$batchno = $data->batchno;
		$invoiceno = $data->invoiceno;
		$trtype = $data->trtype;
		$ttptype = $data->ttptype;
		$etdt = $data->etdt;
		$etdt = date("d-m-Y", strtotime($etdt));
		$batchdt = $data->batchdt;
		$remarks = $data->remarks;
		$updatedt = $data->updatedt;
		$updatedt = date("d-m-Y", strtotime($updatedt));
		$updatenm = $data->updatenm;
		$createdt = $data->createdt;
		$createnm = $data->createnm;
		$dfno = $data->dfno;
		$distnm = $data->distnm;
		$loccd = $data->loccd;
		$loccdnm = $data->loccdnm;
		$sc_co = $data->sc_co;
		$sc_conm = $data->sc_conm;
		$sc_dfno = $data->sc_dfno;
		$sc_dfnonm = $data->sc_dfnonm;
		$tdp = $data->tdp;
		$tbv = $data->tbv;
		$tdp = number_format($tdp, 2, ",", ".");
		$tbv = number_format($tbv, 2, ",", ".");
		$bnsperiod = $data->bnsperiod;
		$bnsperiod = date("M-Y", strtotime($bnsperiod));
		$statusTrx = $data->statusTrx;

		if($statusTrx == "OP"){ //PENDING
			$descStat = "Stockist Pending";
		}elseif($statusTrx == "OA"){ //OA APPROVED
			$descStat = "Stockist Approved";
		}elseif($statusTrx == "MA"){ //MA APPROVED
			$descStat = "Manual Approved";
		}elseif($statusTrx == "OP"){ //OP PENDING
			$descStat = "Online Pending";
		} elseif($statusTrx == "OR") {
			$descStat = "Online Need To Reconcile";
		}
			

	}

	echo "<form id=\"formDetailTrxByID\">";
	echo "<table class='table table-striped table-bordered' width='100%'>
			<tr><th >HEADER TRANSACTION</th></tr></table>";
	echo "<table class='table table-striped table-bordered' width='100%'>";
	echo "<tbody>
			<tr><td width=12% align='right'><strong>Transaction No&nbsp;&nbsp;</strong></td>
				<td width=25%><strong>$trcd</strong></td>
				<td width=8% align='right'>Order No&nbsp;&nbsp;</td>
				<td width=25%>$orderno</td>
			</tr>
			<tr><td width=12% align='right'>Distributor&nbsp;&nbsp;</td>
				<td width=25%>$dfno - $distnm</td>
				<td width=8% align='right'>Period&nbsp;&nbsp;</td>
				<td width=25%>$bnsperiod</td>
			</tr>
			<tr><td width=12% align='right'>Stockist&nbsp;&nbsp;</td>
				<td width=25%>$sc_dfno - $sc_dfnonm</td>
				<td width=8% align='right'>Batch No&nbsp;&nbsp;</td>
				<td width=25%>$batchno</td>
			</tr>
			<tr><td width=12% align='right'>C/O Stockist&nbsp;&nbsp;</td>
				<td width=25%>$sc_co - $sc_conm</td>
				<td width=8% align='right'>CN No&nbsp;&nbsp;</td>
				<td width=25%>$invoiceno</td>
			</tr>
			<tr><td width=12% align='right'>Main Stockist&nbsp;&nbsp;</td>
				<td width=25%>$loccd - $loccdnm </td>
				<td width=8% align='right'>Total DP&nbsp;&nbsp;</td>
				<td width=25%>$tdp</td>
			</tr>
			<tr><td width=12% align='right'>Created&nbsp;&nbsp;</td>
				<td width=25%>$createnm @ $etdt</td>
				<td width=8% align='right'>Total BV&nbsp;&nbsp;</td>
				<td width=25%>$tbv</td>
			</tr>
			<tr><td width=12% align='right'>Updated&nbsp;&nbsp;</td>
				<td width=25%>$updatenm @ $updatedt</td>
				<td width=8% align='right'>Remarks&nbsp;&nbsp;</td>
				<td width=25%>$remarks</td>
			</tr>
			<tr>
				<td><input type=\"button\" value=\"&lt;&lt; Back\" 
									   onclick=\"be_trans.back_to_form()\" 
									   class=\"btn btn-mini btn-warning span20\"></td>
				<td>&nbsp;</td>
				<td width=8% align='right'>Status</td>
				<td>$descStat</td>
			</tr>
		  </tbody>
	</table>";
	
	echo "<table class='table table-striped table-bordered' width='100%'>
		  	<thead>
		  		<tr><th colspan=8>DETAIL TRANSACTION PRODUCT</th></tr>
		  		<tr><th width=5%>No</th>
		  			<th width=15%>Code</th>
		  			<th>Name</th>
		  			<th width=5%>Qty</th>
		  			<th width=10%>DP</th>
		  			<th width=8%>BV</th>
		  			<th width=13%>Tot DP</th>
		  			<th width=10%>Tot BV</th>
		  		</tr>
		  	</thead>
		  <tbody>";
		  
		$i = 1;
		$tdp = 0;
		$tbv = 0;
		$tqty = 0;
		$tot_dp = 0;
		$tot_bv = 0;
		$tot_qty = 0;
		foreach($result["detail"] as $data) {
			$trcd = $data->trcd;
			$prdcd = $data->prdcd;
			$prdnm = $data->prdnm;
			$qtyord = $data->qtyord;
			$bv = $data->bv;
			$dp = $data->dp;
			$TOTBV = $data->TOTBV;
			$TOTDP = $data->TOTDP;
			
			$qtyord = number_format($qtyord, 0, "", ".");	
			$tdpnom = number_format($dp, 2, ",", ".");	
			$tbvnom = number_format($bv, 2, ",", ".");	
			$TOTDP = number_format($TOTDP, 2, ",", ".");	
			$TOTBV = number_format($TOTBV, 2, ",", ".");	
			
			echo "<tr>
					<td align=right>$i</td>
					<td align=center>$prdcd</td>
					<td align=left>$prdnm</td>
					<td align=right>$qtyord</td>
					<td align=right>$tdpnom</td>
					<td align=right>$tbvnom</td>
					<td align=right>$TOTDP</td>
					<td align=right>$TOTBV</td>
				 </tr>";
			$tqty += $data->qtyord;
			$tdp += $data->TOTDP;
			$tbv += $data->TOTBV;
			$i++;
		}	
		
	$tdp = number_format($tdp, 2, ",", ".");	
	$tbv = number_format($tbv, 2, ",", ".");	
	echo "<tr>
			<td colspan='3' align='right'><strong>Grand Total</strong></td>
			<td align=right><strong>$tqty</strong></td>
			<td colspan='2'>&nbsp;</td>
			<td align=right><strong>$tdp</strong></td>
			<td align=right><strong>$tbv</strong></td>
		 </tr>";
				 
	echo "</tbody></table><br />
		  </form>";
		  
	if($result['pay'] != null) {
		echo "<table class='table table-striped table-bordered' width='70%'>
		  	<thead>
		  		<tr><th colspan=8>DETAIL PAY</th></tr>
		  		<tr><th width=5%>No</th>
		  			<th width=10%>Pay Type</th>
		  			<th>Doc No</th>
					<th>Vch Type</th>
		  			<th width=15%>Pay amount</th>
		  			
		  		</tr>
		  	</thead>
		  <tbody>";
		  $i=1;
		  $totalPay = 0;
		  foreach($result["pay"] as $dataPay) {
			  $payAmount = number_format($dataPay->payamt, 2, ",", ".");	
			  echo "<tr>
					<td align=right>$i</td>
					<td align=center>$dataPay->paytype</td>
					<td align=center>$dataPay->docno</td>
					<td align=center>$dataPay->vchtype</td>
					<td align=right>$payAmount</td>
					
				 </tr>";
				 $totalPay += $dataPay->payamt;
				 $i++;
		  }
		  echo "<tr><td colspan=4>T O T A L</td><td align=right>".number_format($totalPay, 2, ",", ".")."</td></tr>";	
		  echo "</tbody>";
		  echo "</table>";
	}	  
?>
<?php	 
     $idform = "updateIncPay";
     echo opening_form(array("id" => $idform));    	 

	 $onchange = "onchange=be_newera.getDetailIncomingPayment(this.value)";
	 $name = array(
	 	"labelname" => "Incoming Payment No.", "fieldname" => "inc_pay", 
	 	"addClass" => "span5", "placeholder" => "Press Tab to Check Data",
	 	"event" => $onchange
	 );
     echo inputText($name);
	 
	 $status = array("H" => "Hold", "O" => "Open");
	 echo inputSelectArray("Status","status", $status);
	 
	 $cust_type = array("M" => "Member", "S" => "Stockist", "O" => "Other");
	 echo inputSelectArray("Customer Type","cust_type", $cust_type);
	 
	 $amount = array(
	 	"labelname" => "Amount", "fieldname" => "amount", "addClass" => "span4"  	
	 );
     echo inputText($amount);
	 
	 $onchange = 'onchange=be_newera.getFullNameByID()';
	 $value1 = array(
	 	"labelname" => "ID Member/Stockist", "fieldname" => "value1", 
	 	"addClass" => "span4", "event" => $onchange	
	 );
     echo inputText($value1);
	 
	 $value2 = array(
	 	"labelname" => "Information", "fieldname" => "value2", "addClass" => "span7",  
	 	"readonly" => "readonly", "placeholder" => "Member/Stockist name or Free text"	
	 );
     echo inputText($value2);
     
	 $desc = array(
	 	"labelname" => "Description", "fieldname" => "desc", "addClass" => "span8", 
	 	"placeholder" => "Description"	
	 );
     echo inputText($desc);
	 
	 $createnm = array(
	 	"labelname" => "Create By", "readonly" => "readonly", "fieldname" => "createnm", "addClass" => "span4",  		
	 );
     echo inputText($createnm);
	 
	 $updatenm = array(
	 	"labelname" => "Update By", "readonly" => "readonly", "fieldname" => "updatenm", "addClass" => "span4", 
	 );
     echo inputText($updatenm);
	 
	 $input = "All.ajaxPostResetField('$idform','be/klink/incoming/update/save')";
	 echo button_set2($input);
	 echo closing_form();
?>
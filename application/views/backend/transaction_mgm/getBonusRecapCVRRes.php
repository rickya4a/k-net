<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
		if($is_xls == '1'){
			$filenm = "rptRecapBonus.xls";
	        header("Content-type: application/vnd.ms-excel");
	        header('Content-Disposition: attachment; filename='.$filenm.'');
	        header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");	
		}
		//a.DistributorCode, a.VoucherNo, a.vchtype, a.VoucherAmtCurr, a.VoucherAmt, a.countrycode, a.bonusyear, a.bonusmonth
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
				  <thead>
					  <tr>
						  <th width='3%'>No</th>
						  <th width='10%'>Code</th>
						  <th width='20%'>Name</th>
						  <th width='10%'>Voucher#</th>
						  <th width='10%'>IDR/USD</th>
						  <th width='6%'>Period</th>
						  <th width='6%'>Country</th>
					  </tr>
				  </thead>
			  <tbody>";
		$i = 1;
		$totDP = 0;
		$totDPAll = 0;
		
		foreach($result as $data) {
			$totDPAll += $data->VoucherAmt;
			$bnsperiod = date("M-Y", strtotime('01-'.$data->bonusmonth.'-'.$data->bonusyear));
			if($is_xls == '0'){
				$VoucherAmt = number_format($data->VoucherAmt, 2, ",", ".");
			}else{
				$VoucherAmt = $data->VoucherAmt;
			}
			
			echo "<tr>
				  <td align=right>$i</td>
				  <td align=left>$data->DistributorCode</td>
				  <td align=left>$data->fullnm</td>
				  <td align=right>$data->VoucherNo</td>
				  <td align=right>$VoucherAmt</td>
				  <td align=right>$bnsperiod</td>
				  <td align=left>$data->countrycode</td>";
			echo "</tr>";
			$i++;
		}
		
		if($is_xls == '0'){
			$totDPAll = number_format($totDPAll, 2, ",", ".");
		}else{
			$totDPAll = $totDPAll;
		}
		
		echo "	 </tbody>
			  </table>";
			  
		echo "<table width='100%' class='table table-striped table-bordered'>
				  <thead>
					  <tr>
						  <th width='3%'>&nbsp;</th>
						  <th width='10%'>&nbsp;</th>
						  <th width='20%'>Summary</th>
						  <th width='10%'>&nbsp;</th>
						  <th width='10%' align=right>$totDPAll</th>
						  <th width='6%'>&nbsp;</th>
						  <th width='6%'>&nbsp;</th>
					  </tr>
				  </thead>
			  </table>";
	}

?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
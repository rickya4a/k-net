<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
		if($is_xls == '1'){
			$filenm = "rptRecapBonus.xls";
	        header("Content-type: application/vnd.ms-excel");
	        header('Content-Disposition: attachment; filename='.$filenm.'');
	        header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");	
		}
		//a.DistributorCode,  'CVoucherNo', PVoucherNo', a.countrycode, a.bonusyear, a.bonusmonth
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
				  <thead>
					  <tr>
						  <th width='3%'>No</th>
						  <th width='10%'>Code</th>
						  <th width='20%'>Name</th>
						  <th width='10%'>C_Voucher#</th>
						  <th width='10%'>P_Voucher#</th>
						  <th width='10%'>Bonus (RM)</th>
						  <th width='6%'>Period</th>";
						  
						  if($rptType == 'novac'){
						  	echo "<th width='6%'>VA#</th>";
						  }elseif($rptType == 'chq_stk'){
						  	echo "<th width='6%'>Stockist</th>";
						  }
						  
				echo "	<th width='6%'>Country</th>
					  </tr>
				  </thead>
			  <tbody>";
		$i = 1;
		$netincome = 0;
		$netincomeAll = 0;
		
		foreach($result as $data) {
			$bnsperiod = date("M-Y", strtotime('01-'.$data->bonusmonth.'-'.$data->bonusyear));
			$netincome = $data->netincome;
			$netincomeAll += $data->netincome;
			
			if($is_xls == '0'){
				$netincome = number_format($netincome, 2, ",", ".");
			}else{
				$netincome = $netincome;
			}
			echo "<tr>
				  <td align=right>$i</td>
				  <td align=left>$data->DistributorCode</td>
				  <td align=left>$data->fullnm</td>
				  <td align=right>$data->CVoucherNo</td>
				  <td align=right>$data->PVoucherNo</td>
				  <td align=right>$netincome</td>
				  <td align=right>$bnsperiod</td>";
				  if($rptType == 'novac'){
				  		echo "<td align=left>$data->novac</td>";
				  }elseif($rptType == 'chq_stk'){
				  		echo "<td align=left>$data->stockiestcode</td>";
				  }
			echo "<td align=left>$data->countrycode</td>
				  </tr>";
			$i++;
		}
		
		echo "</tbody> </table>";

		
		if($is_xls == '0'){
			$netincomeAll = number_format($netincomeAll, 2, ",", ".");
		}else{
			$netincomeAll = $netincomeAll;
		}
		
		echo "<table width='100%' class='table table-striped table-bordered'>
				  <thead>
					  <tr>
						  <th width='3%'>&nbsp;</th>
						  <th width='10%'>&nbsp;</th>
						  <th width='20%'>Summary</th>
						  <th width='10%'>&nbsp;</th>
						  <th width='10%'>&nbsp;</th>
						  <th width='10%'>$netincomeAll</th>
						  <th width='6%'>&nbsp;</th>
						  <th width='6%'>&nbsp;</th>
					  </tr>
				  </thead>
			  </table>";
	}

?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
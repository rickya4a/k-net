<div class="mainForm">
  <form class="form-horizontal" id="formSalesRecap" method="post" target="_blank" action="<?php echo site_url("transklink/recapSales/list/1");?>">
    <fieldset>      
      <div class="control-group">     
        <span id="loccd" style="display: block;">
          	<label class="control-label" for="typeahead">Stockist</label>                           
	        <div class="controls">
	        	<select id="sc_code" name="sc_code">
            		<option value="all">--All--</option>
            		<option value="all-sc">--Stockist--</option>
            		<option value="all-scandsub">--Stockist And Sub Stockist--</option>
            		<option value="all-sub">--Sub Stockist--</option>
            		<option value="all-ms">--Mobile Stockist--</option>
		        	<?php 
		        		foreach ($loccd as $data){
		        			$sc_code = $data->loccd;
							$sc_name = $data->fullnm;
		        			echo "<option value=\"$sc_code\">$sc_code-$sc_name</option>";
		        		}
					?>
            	</select>
            	<!--
	        	<input type="text" id="sc_code" name="sc_code" style="width:120px" placeholder="Stockist Code" onblur="be_trans_klink.getFullNameByID(this.value, 'transklink/getStockist', '#sc_name')"/> 
				<input type="text" id="sc_name" name="sc_name" style="width:250px" class="sc_name" disabled="disabled" />
				-->   
	        </div>
        </span>
        
        <span id="period" style="display: block;">
        	<?php echo bonusPeriodAll(); ?>
        </span>
       
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans_klink.getSalesRecap('0')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            <input type="submit" value="To Excel" class="btn btn-success" tabindex="4">
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		/*
		var sc_name = $("#sc_name").val();
		alert(sc_name);
		$("#sc_name").val('isi');
		if( sc_name != ""){
			alert('test kosong');
		}
		*/
	});	
</script>

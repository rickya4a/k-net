<input type="button" class="btn btn-mini btn-danger" value="Clear Preview" onclick="be_trans.clear()" />
<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Import Stock Status</th>
   </tr>
   </thead>
   <tbody>
   <tr>
   	 <td width="70%">Record Found</td><td align="right"><?php echo $totalRecord; ?></td>
   </tr>
   <tr>	 
   	 <td>Inserted Success</td><td align="right"><?php echo $insSuccess; ?></td>
   </tr>
   <tr>	 
   	 <td>Inserted Failed</td><td align="right"><?php echo $insFail; ?></td>
   </tr>
   <tr>	 
   	 <td>Double Record on DB</td><td align="right"><?php echo $double; ?></td>
   </tr>
   </tbody>
</table>

<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Double Record</th>
   </tr>
   <tr>
   	 <th width="15%">No</th>
   	 <th>Product Code</th>
   </tr>
   </thead>
   <tbody>
   <?php 
   $i = 0;
   foreach($arrDouble as $arr) {
   $i++;
   ?>	
   <tr>
   	 <td width="10%" align="right"><?php echo $i; ?></td><td align="center"><?php echo $arr; ?></td>
   </tr>
   <?php } ?>
   </tbody>	
</table>

<table width="40%" class="table table-striped table-bordered bootstrap-datatable datatable" cellpadding="0" cellspacing="0">
   <thead>
   <tr>
   	 <th colspan="2">Insert Record Fail</th>
   </tr>
   <tr>
   	 <th width="15%">No</th>
   	 <th>Product Code</th>
   </tr>
   </thead>
   <tbody>
   <?php 
   $i = 0;
   foreach($arrFail as $arr) {
   $i++;
   ?>	
   <tr>
   	 <td width="10%" align="right"><?php echo $i; ?></td><td align="center"><?php echo $arr; ?></td>
   </tr>
   <?php } ?>
   </tbody>	
</table>
<?php
	$deposit_header = $result['deposit_header'];
?>
<table class="table table-bordered table-striped" width="100%">
	<tr>
		<td width="15%">No Deposit</td>
		<td width="40%"><?php echo $deposit_header[0]->no_trx; ?></td>
		<td width="15%">Status</td>
		<td><?php echo $deposit_header[0]->status; ?></td>
	</tr>
	<tr>
		<td>Total Deposit</td>
		<td><?php echo number_format($deposit_header[0]->total_deposit, 0, ",","."); ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Total Penggunaan</td>
		<td><?php echo number_format($deposit_header[0]->total_keluar, 0, ",","."); ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Tgl Input</td>
		<td><?php echo $deposit_header[0]->createdt; ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Create By</td>
		<td><?php echo $deposit_header[0]->createnm; ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>

<?php
    $list_vch = $result['list_vch'];
	$list_ttp = $result['list_ttp'];
	if($list_vch != null) {
?>
<table class="table table-bordered" width="100%">
	<thead>
	  <tr>
		<th colspan="7">List Voucher Scan</th>
	  </tr>
	  <tr>
		<th>No</th>
		<th>Voucher Scan</th>
		<th>Nominal</th>
		<th>ID Member</th>
		<th>Nama Member</th>
		<th>Create By</th>
		<th>Tgl Input</th>
	  </tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	$total_vch = 0;
	foreach($list_vch as $dta_vch) {
		if ($dta_vch->createnm != $deposit_header[0]->createnm) {
			echo "<tr bgcolor=#ff3838>";
			echo "<td align=right>$no</td>";
			echo "<td align=center>$dta_vch->voucher_scan</td>";
			echo "<td align=right>".number_format($dta_vch->nominal, 0, ",",".")."</td>";
			echo "<td align=center>$dta_vch->dfno</td>";
			echo "<td align=left>$dta_vch->fullnm</td>";
			echo "<td align=center>$dta_vch->createnm</td>";
			echo "<td align=center>$dta_vch->createdt</td>";
			$total_vch += $dta_vch->nominal;
			$no++;
			echo "</tr>";
		} else {
			echo "<tr>";
			echo "<td align=right>$no</td>";
			echo "<td align=center>$dta_vch->voucher_scan</td>";
			echo "<td align=right>".number_format($dta_vch->nominal, 0, ",",".")."</td>";
			echo "<td align=center>$dta_vch->dfno</td>";
			echo "<td align=left>$dta_vch->fullnm</td>";
			echo "<td align=center>$dta_vch->createnm</td>";
			echo "<td align=center>$dta_vch->createdt</td>";
			$total_vch += $dta_vch->nominal;
			$no++;
			echo "</tr>";
		}
	}
	?>
	<tr>
		<td colspan="2" align="center">T O T A L</td>
		<td align="right"><?php echo number_format($total_vch, 0, ",","."); ?></td>
		<td colspan="3">&nbsp;</td>
	</tr>
	</tbody>
</table>
<?php
}

if($list_ttp != null) {
?>
<table class="table table-bordered table-striped" width="100%">
	<thead>
	  <tr>
		<th colspan="7">List TTP / Transaksi</th>
	  </tr>
	  <tr>
		<th>No</th>
		<th>No Trx</th>
		<th>No TTP</th>
		<th>Member</th>
		<th>Created By</th>
		<th>Totap DP</th>
		<th>Total BV</th>
	  </tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	$tdp = 0;
	$tbv = 0;
	foreach($list_ttp as $ttp) {
		echo "<tr>";
		echo "<td align=right>$no</td>";
		echo "<td align=center>$ttp->trcd</td>";
		echo "<td align=center>$ttp->orderno</td>";
		echo "<td align=center>$ttp->dfno</td>";
		echo "<td align=center>$ttp->createnm</td>";
		echo "<td align=right>".number_format($ttp->totpay, 0, ",",".")."</td>";
		echo "<td align=right>".number_format($ttp->tbv, 0, ",",".")."</td>";
		$tdp += $ttp->totpay;
		$tbv += $ttp->tbv;
		$no++;
		echo "</tr>";
	}
	?>
	<tr>
		<td colspan="4" align="center">T O T A L</td>
		<td align="right"><?php echo number_format($tdp, 0, ",","."); ?></td>
		<td align="right"><?php echo number_format($tbv, 0, ",","."); ?></td>
	</tr>
	</tbody>
</table>
<?php
}
?>
<?php
	foreach ($result["header"] as $data) {
		$trcd = $data->trcd;
		$orderno = $data->orderno;
		$batchno = $data->batchno;
		$invoiceno = $data->invoiceno;
		$trtype = $data->trtype;
		$ttptype = $data->ttptype;
		$id_deposit = $data->id_deposit;
		$flag_batch = $data->flag_batch;
		$etdt = $data->etdt;
		$etdt = date("d-m-Y", strtotime($etdt));
		$batchdt = $data->batchdt;
		$remarks = $data->remarks;
		$updatedt = $data->updatedt;
		$updatedt = date("d-m-Y", strtotime($updatedt));
		$updatenm = $data->updatenm;
		$createdt = $data->createdt;
		$createnm = $data->createnm;
		$dfno = $data->dfno;
		$distnm = $data->distnm;
		$loccd = $data->loccd;
		$loccdnm = $data->loccdnm;
		$sc_co = $data->sc_co;
		$sc_conm = $data->sc_conm;
		$sc_dfno = $data->sc_dfno;
		$sc_dfnonm = $data->sc_dfnonm;
		$tdp = $data->tdp;
		$tbv = $data->tbv;
		$tdp = number_format($tdp, 2, ",", ".");
		$tbv = number_format($tbv, 2, ",", ".");
		$bnsperiod = date("Y-m-d", strtotime($data->bnsperiod));
		// echo $batchno;
		// $bnsperiod = date("M-Y", strtotime($bnsperiod));
		$statusTrx = $data->statusTrx;

		if($statusTrx == "OP") { //PENDING
			$descStat = "Stockist Pending";
		} elseif($statusTrx == "OA") { //OA APPROVED
			$descStat = "Stockist Approved";
		} elseif($statusTrx == "MA") { //MA APPROVED
			$descStat = "Manual Approved";
		} elseif($statusTrx == "OP") { //OP PENDING
			$descStat = "Online Pending";
		} elseif($statusTrx == "OR") {
			$descStat = "Online Need to Reconcile";
		}


	}
?>
<form id="formDetailTrxByID" enctype="multipart/form-data" method="post">
	<table class='table table-striped table-bordered' width='100%'>
		<tr>
			<th>HEADER TRANSACTION</th>
		</tr>
	</table>
	<table class='table table-striped table-bordered' width='100%'>
		<tbody>
			<tr>
				<td width=12% align='right'><strong>Transaction No&nbsp;&nbsp;</strong></td>
				<td width=25%><strong><?php echo $trcd; ?></strong><input type="hidden" name="trcd" id="trcd" value="<?php echo $trcd; ?>"></td>
				<td width=8% align='right'>Order No&nbsp;&nbsp;</td>
				<td width=25%><?php echo $orderno; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>TRTYPE</td>
				<td width=25%><?php echo $trtype; ?></td>
				<td width=8% align='right'>TTPTYPE</td>
				<td width=25%><?php echo $ttptype; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>ID Deposit</td>
				<td width=25%><?php echo $id_deposit; ?></td>
				<td width=8% align='right'>Flag Batch</td>
				<td width=25%><?php echo $flag_batch; ?></td>
			</tr>
			<?php if ($batchno == NULL || $batchno == "") { ?>
			<tr>
				<td width=12% align='right'>Distributor&nbsp;&nbsp;</td>
				<td width=25%><?php echo $dfno. "-" .$distnm ?></td>
				<td width=8% align='right'>Period&nbsp;&nbsp;</td>
				<td width=25%><input type="text" name="bnsperiod" id="bnsperiod" value="<?php echo $bnsperiod; ?>"></td>
			</tr>
			<tr>
				<td width=12% align='right'>Stockist&nbsp;&nbsp;</td>
				<td width=25%><input type="text" name="sc_dfno" id="sc_dfno"
						onchange="All.getFullNameByID(this.value, 'trx/reconcile/helper/mssc/loccd', '#sc_dfnonm')"
						value="<?php echo $sc_dfno ?>"> - <input style="width: 50%;" type="text" name="sc_dfnonm" id="sc_dfnonm"
						value="<?php echo $sc_dfnonm; ?>" readonly></td>
				<td width=8% align='right'>Batch No&nbsp;&nbsp;</td>
				<td width=25%><?php echo $batchno; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>C/O Stockist&nbsp;&nbsp;</td>
				<td width=25%><input type="text" name="sc_co" id="sc_co"
						onchange="All.getFullNameByID(this.value, 'trx/reconcile/helper/mssc/loccd', '#sc_conm')"
						value="<?php echo $sc_co ?>"> - <input style="width: 50%;" type="text" name="sc_conm" id="sc_conm"
						value="<?php echo $sc_conm; ?>" readonly></td>
				<td width=8% align='right'>CN No&nbsp;&nbsp;</td>
				<td width=25%><?php echo $invoiceno; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Main Stockist&nbsp;&nbsp;</td>
				<td width=25%><input type="text" name="loccd" id="loccd"
						onchange="All.getFullNameByID(this.value, 'trx/reconcile/helper/mssc/loccd', '#loccdnm')"
						value="<?php echo $loccd ?>"> - <input style="width: 50%;" type="text" name="loccdnm" id="loccdnm"
						value="<?php echo $loccdnm; ?>" readonly></td>
				<td width=8% align='right'>Total DP&nbsp;&nbsp;</td>
				<td width=25%><?php echo $tdp; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Created&nbsp;&nbsp;</td>
				<td width=25%><?php echo $createnm. "@" .$etdt ?></td>
				<td width=8% align='right'>Total BV&nbsp;&nbsp;</td>
				<td width=25%><?php echo $tbv; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Updated&nbsp;&nbsp;</td>
				<td width=25%><?php echo $updatenm. "@" .$updatedt ?></td>
				<td width=8% align='right'>Remarks&nbsp;&nbsp;</td>
				<td width=25%><?php echo $remarks; ?></td>
			</tr>
			<tr>
				<td><input type="button" value="&lt;&lt; Back" onclick="be_trans.back_to_form()"
						class="btn btn-mini btn-warning span20"></td>

				<td>
					<input type="button" value="Update" onclick="formPost(this.form.id, 'transklink/recover/update')" class="btn btn-primary">
					<input type="button" value="Delete" onclick="formPost(this.form.id, 'transklink/recover/remove')" class="btn btn-danger">
				</td>
			<?php } else { ?>
				<tr>
				<td width=12% align='right'>Distributor&nbsp;&nbsp;</td>
				<td width=25%><?php echo $dfno. "-" .$distnm ?></td>
				<td width=8% align='right'>Period&nbsp;&nbsp;</td>
				<td width=25%><?php echo $bnsperiod ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Stockist&nbsp;&nbsp;</td>
				<td width=25%><?php echo $sc_dfno ?> - <?php echo $sc_dfnonm; ?></td>
				<td width=8% align='right'>Batch No&nbsp;&nbsp;</td>
				<td width=25%><?php echo $batchno; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>C/O Stockist&nbsp;&nbsp;</td>
				<td width=25%><?php echo $sc_co ?> - <?php echo $sc_conm; ?></td>
				<td width=8% align='right'>CN No&nbsp;&nbsp;</td>
				<td width=25%><?php echo $invoiceno; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Main Stockist&nbsp;&nbsp;</td>
				<td width=25%><?php echo $loccd ?> - <?php echo $loccdnm; ?></td>
				<td width=8% align='right'>Total DP&nbsp;&nbsp;</td>
				<td width=25%><?php echo $tdp; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Created&nbsp;&nbsp;</td>
				<td width=25%><?php echo $createnm. "@" .$etdt ?></td>
				<td width=8% align='right'>Total BV&nbsp;&nbsp;</td>
				<td width=25%><?php echo $tbv; ?></td>
			</tr>
			<tr>
				<td width=12% align='right'>Updated&nbsp;&nbsp;</td>
				<td width=25%><?php echo $updatenm. "@" .$updatedt ?></td>
				<td width=8% align='right'>Remarks&nbsp;&nbsp;</td>
				<td width=25%><?php echo $remarks; ?></td>
			</tr>
			<tr>
				<td><input type="button" value="&lt;&lt; Back" onclick="be_trans.back_to_form()"
						class="btn btn-mini btn-warning span20"></td>
				<td>&nbsp;</td>
			<?php } ?>

				<td width=8% align='right'>Status</td>
				<td><?php echo $descStat; ?></td>
			</tr>
		</tbody>
	</table>

	<table class='table table-striped table-bordered' width='100%'>
		<thead>
			<tr>
				<th colspan=8>DETAIL TRANSACTION PRODUCT</th>
			</tr>
			<tr>
				<th width=5%>No</th>
				<th width=15%>Code</th>
				<th>Name</th>
				<th width=5%>Qty</th>
				<th width=10%>DP</th>
				<th width=8%>BV</th>
				<th width=13%>Tot DP</th>
				<th width=10%>Tot BV</th>
			</tr>
		</thead>
		<tbody>
			<?php
		$i = 1;
		$tdp = 0;
		$tbv = 0;
		$tqty = 0;
		$tot_dp = 0;
		$tot_bv = 0;
		$tot_qty = 0;
		foreach($result["detail"] as $data) {
			$trcd = $data->trcd;
			$prdcd = $data->prdcd;
			$prdnm = $data->prdnm;
			$qtyord = $data->qtyord;
			$bv = $data->bv;
			$dp = $data->dp;
			$TOTBV = $data->TOTBV;
			$TOTDP = $data->TOTDP;

			$qtyord = number_format($qtyord, 0, "", ".");
			$tdpnom = number_format($dp, 2, ",", ".");
			$tbvnom = number_format($bv, 2, ",", ".");
			$TOTDP = number_format($TOTDP, 2, ",", ".");
			$TOTBV = number_format($TOTBV, 2, ",", ".");

			echo "<tr>
					<td align=right>$i</td>
					<td align=center>$prdcd</td>
					<td align=left>$prdnm</td>
					<td align=right>$qtyord</td>
					<td align=right>$tdpnom</td>
					<td align=right>$tbvnom</td>
					<td align=right>$TOTDP</td>
					<td align=right>$TOTBV</td>
				 </tr>";
			$tqty += $data->qtyord;
			$tdp += $data->TOTDP;
			$tbv += $data->TOTBV;
			$i++;
		}

	$tdp = number_format($tdp, 2, ",", ".");
	$tbv = number_format($tbv, 2, ",", ".");
	?>
			<tr>
				<td colspan='3' align='right'><strong>Grand Total</strong></td>
				<td align=right><strong><?php echo $tqty ?></strong></td>
				<td colspan='2'>&nbsp;</td>
				<td align=right><strong><?php echo $tdp; ?></strong></td>
				<td align=right><strong><?php echo $tbv; ?></strong></td>
			</tr>

		</tbody>
	</table><br />
</form>
<?php if($result['pay'] != null) { ?>
<table class='table table-striped table-bordered' width='70%'>
	<thead>
		<tr>
			<th colspan=8>DETAIL PAY</th>
		</tr>
		<tr>
			<th width=5%>No</th>
			<th width=10%>Pay Type</th>
			<th>Doc No</th>
			<th>Vch Type</th>
			<th width=15%>Pay amount</th>

		</tr>
	</thead>
	<tbody>
		<?php
		  $i=1;
		  $totalPay = 0;
		  foreach($result["pay"] as $dataPay) {
			  $payAmount = number_format($dataPay->payamt, 2, ",", ".");	?>
		<tr>
			<td align=right><?php echo $i ?></td>
			<td align=center><?php echo $dataPay->paytype ?></td>
			<td align=center><?php echo $dataPay->docno ?></td>
			<td align=center><?php echo $dataPay->vchtype ?></td>
			<td align=right><?php echo $payAmount ?></td>
		</tr>
		<?php	$totalPay += $dataPay->payamt;
				 $i++;
		  } ?>
		<tr>
			<td colspan=4>T O T A L</td>
			<td align=right><?php echo number_format($totalPay, 2, ",", "."); ?></td>
		</tr>
	</tbody>
</table>
<?php	}	  ?>
<script>
	function formPost(formID, url) {
		All.set_disable_button();
		$.post(All.get_url(url), $(All.get_active_tab() + " #" + formID).serialize(), function (data) {
			if (data.response == 'false') {
				All.set_error_message(".mainForm .result", data.message);
			} else {
				All.set_error_message(".mainForm .result", data.message);
				All.set_enable_button();
			}
		}, "json").fail(function () {
			alert("Error requesting page");
			All.set_enable_button();
		});
	}
</script>
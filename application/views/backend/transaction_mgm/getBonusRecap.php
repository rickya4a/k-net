<div class="mainForm">
  <form class="form-horizontal" id="formBonusRecap" method="post" target="_blank" action="<?php echo site_url("transklink/recapBonus/list/1");?>">
    <fieldset>      
      <div class="control-group">     
        <span id="loccd" style="display: block;">
          	<label class="control-label" for="typeahead">Country Code</label>                           
	        <div class="controls">
	        	<select id="country_cd" name="country_cd">
            		<option value="all">--All--</option>
            		<option value="idr">Indonesia</option>
            		<option value="sar">Saudi Arabia</option>
            		<option value="tl">Timor Leste</option>
            		<option value="xid">Exclude Indonesia</option>
            	</select>  
	        </div>
        </span>
        
        <span id="period" style="display: block;">
        	<?php echo bonusPeriodAll(); ?>
        </span>
       
       <span id="loccd" style="display: block;">
          	<label class="control-label" for="typeahead">Type</label>                           
	        <div class="controls">
	        	<select id="rpt_type" name="rpt_type">
            		<option value="all">--All Report--</option>
            		<option value="allvr">All Voucher</option>
            		<option value="chq">Cheque List</option>
            		<option value="chq_stk">Cheque Stockist List</option>
            		<option value="novac">VA List</option>
            		<option value="cvr">Cash Voucher</option>
            		<option value="pvr">Product Voucher</option>
            </select>  
	        </div>
        </span>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans_klink.getBonusRecap('0')" />
            <input type="submit" value="To Excel" class="btn btn-success" tabindex="4">
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		/*
		var sc_name = $("#sc_name").val();
		alert(sc_name);
		$("#sc_name").val('isi');
		if( sc_name != ""){
			alert('test kosong');
		}
		*/
	});	
</script>

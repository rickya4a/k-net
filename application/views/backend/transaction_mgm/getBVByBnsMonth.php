<div class="mainForm">
  <form class="form-horizontal" id="frmBV">
    <fieldset>      
      <div class="control-group">
      	
      	<label class="control-label" for="typeahead">Month</label>                             
        <div class="controls">
        	<select id="month" name="month">
        		<?php
                    /*foreach($bnsmonth as $list){
                        echo "<option value = \"$list->bnsmonth\">$list->bnsmonth</option>";
                    } */
                    
                    $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                      for($y=1;$y<=12;$y++)
                      {
                          if($y==date("mm"))
                          { 
                              $pilih="selected";
                          }
                          else
                          {
                              $pilih="";
                          }
                          
                          echo("<option value=\"".sprintf("%02s",$y)."\" $pilih>$bulan[$y]</option>"."\n");
                      }
                ?>
        	</select>
            <?php $thn = date("Y");?>
            <input type="text" name="year" size="14" placeholder="Tahun Bonus" id="year" class="only-number" value="<?php echo $thn;?>"/>
        </div>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="be_trans_klink.listBVByBnsmonth()" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
		if($is_xls == '1'){
			$filenm = "rprRecapSls.xls";
	        header("Content-type: application/vnd.ms-excel");
	        header('Content-Disposition: attachment; filename='.$filenm.'');
	        header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		    header("Pragma: public");	
		}
		echo "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>
				  <thead>
					  <tr>
						  <th width='3%'>No</th>
						  <th width='10%'>Code</th>
						  <th width='20%'>Name</th>
						  <th width='10%'>Total DP</th>
						  <th width='10%'>Total BV</th>
						  <th width='6%'>Period</th>
						  <th width='6%'>Type</th>
					  </tr>
				  </thead>
			  <tbody>";
		$i = 1;
		
		$totBV = 0;
		$totDP = 0;
		$totBVAll = 0;
		$totDPAll = 0;
		
		foreach($result as $data) {
			/* A.sctype, A.sc_dfno, A.fullnm_sc, 
			    A.sc_co, A.fullnm_co, 
			    A.loccd, A.fullnm_loccd, 
			    A.bonusmth, A.bonusyr, 
			    A.tdp, A.ndp, A.tbv, A.nbv
			 */
			$totBVAll += $data->tbv;
			$totDPAll += $data->tdp;
			
			$bnsperiod = date("M-Y", strtotime('01-'.$data->bonusmth.'-'.$data->bonusyr));
			if($is_xls == '0'){
				$tdp = number_format($data->tdp, 2, ",", ".");
				$tbv = number_format($data->tbv, 2, ",", ".");
			}else{
				$tdp = $data->tdp;
				$tbv = $data->tbv;
			}
			
			if($data->sctype == '1'){
				$type = "Stockist";
			}elseif($data->sctype == '2'){
				$type = "Sub";
			}elseif($data->sctype == '3'){
				$type = "Mobile";
			}
			
			echo "<tr>
				  <td align=right>$i</td>
				  <td align=left>$data->sc_dfno</td>
				  <td align=left>$data->fullnm_sc</td>
				  <td align=right>$tdp</td>
				  <td align=right>$tbv</td>
				  <td align=right>$bnsperiod</td>
				  <td align=left>$type</td>";
			echo "</tr>";
			$i++;
		}
		
		if($is_xls == '0'){
			$totDPAll = number_format($totDPAll, 2, ",", ".");
			$totBVAll = number_format($totBVAll, 2, ",", ".");
		}else{
			$totDPAll = $totDPAll;
			$totBVAll = $totBVAll;
		}
		
		echo "	 </tbody>
			  </table>";
			  
		echo "<table width='100%' class='table table-striped table-bordered'>
				  <thead>
					  <tr>
						  <th width='3%'>&nbsp;</th>
						  <th width='10%'>&nbsp;</th>
						  <th width='20%'>Summary</th>
						  <th width='10%' align=right>$totDPAll</th>
						  <th width='10%' align=right>$totBVAll</th>
						  <th width='6%'>&nbsp;</th>
						  <th width='6%'>&nbsp;</th>
					  </tr>
				  </thead>
			  </table>";
	}

?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
<?php 
if(empty($result['bbhdr'])) {
	echo setErrorMessage();
} else {
	$bbhdr = $result['bbhdr'];
	if(!empty($bbhdr)) {
		?>
<table style="width: 100%;" class='table table-striped table-bordered bootstrap-datatable datatable'>
    <thead>
        <tr>
            <th colspan='10' style="height: 45px;vertical-align: middle;font-size: medium;">Voucher Cash Incoming Payment</th>
        </tr>
		<tr style="background-color: #f5f4f4;">
            <th>Incoming Payment</th>
			<th>Ref. No.</th>
			<th>Description</th>
			<th>Amount</th>
			<th>ETDT</th>
            <th>Df. No.</th>
		</tr>
    </thead>
	<tbody>
        <tr>
            <td style="text-align: center"><?php echo $bbhdr[0]->trcd; ?></td>
            <td style="text-align: center"><?php echo substr($bbhdr[0]->refno, 1, -3); ?></td>
            <td style="text-align: center"><?php echo $bbhdr[0]->description; ?></td>
            <td style="text-align: right"><?php echo number_format($bbhdr[0]->amount, "0", ".", ",") ;?></td>
            <td style=" text-align: center;"><?php echo date("d F Y", strtotime($bbhdr[0]->etdt));?></td>
            <td style="text-align: center"><?php echo $bbhdr[0]->dfno ?></td>
        </tr>
    </tbody>
</table>
<?php 
	  }
?>
<?php if (empty($result['recover'])) { ?>
	<input type="button" class="btn btn-success" onclick="get()" value="Recover">
<?php } ?>
<?php
      if (empty($result['sc_newtrp_vc_det'])) {
          echo setErrorMessage();
      } else {
          $newtr = $result['sc_newtrp_vc_det'];
          if (!empty($newtr)) {
?>
     <table style="width: 50%;" class='table table-striped table-bordered bootstrap-datatable datatable'>
    <thead>
        <tr>
            <th colspan='10' style="height: 45px;vertical-align: middle;font-size: medium;">Detail</th>
        </tr>
		<tr style="background-color: #f5f4f4;">
            <th>No</th>
			<th>SSR/MSR</th>
			<th>Payment Type</th>
			<th>No. Voucher</th>
			<th>Amount</th>
		</tr>
    </thead>
	<input type="hidden" id="ssr" name="ssr" value="<?php echo $result['sc_newtrp_vc_det'][0]->trcd; ?>">
	<input type="hidden" id="ipsvc" name="ipsvc" value="<?php echo substr($result['bbhdr'][0]->refno, 1, -3); ?>">
	<tbody>
        <?php
            $no = 1;
            foreach($newtr as $list){
				?>
        <tr>
            <td style="text-align: center"><?php echo $no;?></td>
            <td style="text-align: center"><?php echo $list->trcd; ?></td>
            <td style="text-align: center"><?php echo $list->paytype;?></td>
            <td style="text-align: center"><?php echo $list->vhcno;?></td>
            <td style=" text-align: right;"><?php echo number_format($list->payamt, "0", ".", ",");?></td>
        </tr>
        <?php $no++; }?>
    </tbody>
</table>

<?php
          }
      }
?>
<?php 
if (empty($result['custpaydet'])) {
          echo setErrorMessage();
      } else {
          $custpaydet = $result['custpaydet'];
          if (!empty($custpaydet)) {
?>
<table style="width: 100%;" class='table table-striped table-bordered bootstrap-datatable datatable'>
    <thead>
        <tr>
            <th colspan='10' style="height: 45px;vertical-align: middle;font-size: medium;">Detail</th>
        </tr>
		<tr style="background-color: #f5f4f4;">
            <th>No</th>
			<th>TRCD</th>
            <th>Effect</th>
            <th>Df. No.</th>
            <th>Create Name</th>
            <th>Date Created</th>
			<th>TrType</th>
			<th>AppplyTo</th>
			<th>ID No.</th>
            <th>Amount</th>
		</tr>
    </thead>
	<tbody>
    <?php
        $no = 1;
        foreach($custpaydet as $list){
        ?>
        <tr>
            <td style="text-align: center"><?php echo $no;?></td>
            <td style="text-align: center"><?php echo $list->trcd; ?></td>
            <td style="text-align: center"><?php echo $list->effect; ?></td>
            <td style="text-align: center"><?php echo $list->dfno; ?></td>
            <td style="text-align: center"><?php echo $list->createnm; ?></td>
            <td style="text-align: center"><?php echo date("d F Y", strtotime($list->createdt)); ?></td>
            <td><?php echo $list->trtype;?></td>
            <td><?php echo $list->applyto;?></td>
            <td style=" text-align: center;"><?php echo $list->idno;?></td>
            <td style="text-align: right"><?php echo number_format($list->amount,"0",".", ","); ?></td>
        </tr>
        <?php $no++; }?>
    </tbody>
</table>
<?php
		}
	}
} 
?>
<script>
function get() {
		var ssr = $('#ssr').val(),
			ipsvc = $('#ipsvc').val();
			trxno = $('#trxno').val();
		$.ajax({
        type: "POST",
        url: All.get_url('transklink/recover/checkssr'),
        dataType:"json",
        data: {
			ssr: ssr,
			ipsvc: ipsvc,
			trxno: trxno
		},
		success: function (data) {
			alert(data.message);
			All.set_enable_button();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(thrownError + ':' + xhr.status);
			All.set_enable_button();
		}
      });
	}
</script>
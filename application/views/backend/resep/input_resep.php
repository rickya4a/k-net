<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input Resep</title>

    <!-- include CDN lib jQuery and select2 here -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <script>
    $(document).ready(function(){
        $(".js-example-basic-single").select2();
        $('#addItem').click(function() {
            var prd = $('#prd option:selected').text();
            var qty = document.getElementById("qty");
            var aturan = document.getElementById("aturan");

            var $x = document.getElementById("prd");

            var rowCount = $('.main tr').length-1;
            var no = rowCount + 1;

            //validate empty column
            if($('#prd').val() != '' && $('#qty').val() != '' && $('#aturan').val() != ''){
                console.log('2');
                $("<tr data-status=\"I\" style=\"text-align: center\">"
                    + "<td><input type=\"hidden\" class=\"span4\" name=\"prd2[]\" id=\"prd2\" readonly value=\""+$('#prd').val()+"\"/>"+$('#prdnm').val()+"</td>"
                    + "<td><input type=\"text\" name=\"qty2[]\" id=\"qty2\" style=\"text-align:center\" value=\""+$('#qty').val()+"\" /></td>"
                    + "<td><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"text\" class=\"span12\" name=\"aturan2[]\" id=\"aturan2\" value=\""+$('#aturan').val()+"\"/></td>"
                    + "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"DELETE\" class=\"btn btn-danger\" /></td>"
                    + "</tr>").appendTo(".main tbody");

                    $('#qty').val('');
                    $('#aturan').val('');
                    
                    
            }
            else{
                alert('Data belum lengkap!');
            }

        });
    });

    $("table.main").on("click", "#ibtnDel", function (event) {
      
        var tr = $(this).closest('tr');
        var row = tr.find("input[id=row]").val();
        var rowCount = $('.main tr').length-1;

        $(this).closest("tr").remove();
    });

    function select(ddl) {
        document.getElementById('prdnm').value = ddl.options[ddl.selectedIndex].text;
    }

    </script>

</head>
<body>
    <div class="mainForm">
        <form class="form-horizontal" id="formResep" name="formResep" enctype="multipart/form-data" method="post">
            <div class="control-group">
                <label for="typehead" class="control-label">Nama Karyawan</label>
                <div class="controls">
                    <input type="text" name="nama" id="nama" class="span4">
                </div>

                <label for="typehead" class="control-label">Produk</label>
                <div class="controls">
                    <select name="prd" id="prd" class="span8 js-example-basic-single" onchange="select(this)">
                        <option value="">Pilih</option>
                        <?php
                            foreach($products as $product ){
                                ?>
                                <option value="<?php echo $product['prdcd']?>"><?php echo $product['prdcd'] ." - ". $product['prdnm']?></option>
                                <?php 
                           } 
                        ?>
                    </select>

                    <!-- get label when selected option -->
                    <input type="hidden" name="prdnm" id="prdnm">
                </div>
                
                <label for="typehead" class="control-label">Qty</label>
                <div class="controls">
                    <input type="text" name="qty" id="qty">
                </div>
           
                <label for="typehead" class="control-label">Aturan Pakai</label>
                <div class="controls">
                    <input type="text" name="aturan" id="aturan" style="width: 250px">
                </div>

                <label for="typehead" class="control-label">&nbsp</label>
                <div class="controls">
                    <input type="button" name="addItem" id="addItem" class="btn btn-danger" value="Tambahkan">
                </div>

                <!-- detail resep -->
                <div class="form-group">
                    <div class="col-lg-11">
                        <!-- table untuk menampilkan detail inventaris yg dimiliki karyawan -->
                        <table width="80%" class="main table table-bordered" id="tabel-detail">
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Qty</th>
                                    <th>Aturan Pakai</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- detail here -->
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <hr>

            <div class="control-group">
                <input type="button" value="BATAL" class="btn btn-warning">
                <input type="button" value="SIMPAN" class="btn btn-primary" onclick="All.ajaxFormPost('formResep', 'resep/save')">
            </div>
        </form>
    </div>
</body>
</html>
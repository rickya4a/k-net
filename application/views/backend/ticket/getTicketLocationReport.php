<div class="mainForm">
    <div id="frmApproval">
        <form class="form-horizontal" id="locationReport" name="locationReport" method="post">
            <fieldset>
                <div class="control-group"> 
                    
                    <label class="control-label" for="typeahead">Event Location</label>
                    <div class="controls">
                        <select id="event_id" name="event_id">
                        	<?php
                        	  foreach($location as $data) {
                        	  	echo "<option value=\"$data->event_id\">$data->event_desc</option>";
                        	  }
                        	?>
                        </select>
                    
                        <!-- input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/ticket/location/list')"/ -->
                        <!-- input tabindex="3" type="button" id="btn_reconcile" class="btn btn-primary .submit" name="save" value="Refresh List" onclick="be_umroh.getReconcile()"/ -->
                   
                </div>
                
                <label class="control-label" for="typeahead">Status</label>
                <div class="controls">
                    <select id="pay_status" name="pay_status">
                    	<option value="all">All</option>
                    	<option value="0">Pending</option>
                    	<option value="1">Paid</option>
                    </select>
                 	<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/ticket/location/list')"/>
                 </div>
                        
            </fieldset>
        </form>
        <div class="result"></div>
    </div>
    
</div>

<div class="mainForm">
    <div id="frmApproval">
        <form class="form-horizontal" id="ticketPayReport" name="ticketPayReport" method="post">
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Register Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker typeahead" id="tktPay_from" name="pay_ticket_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker typeahead" id="tktPay_to" name="pay_ticket_to" />
                    </div>
                    <label class="control-label" for="typeahead">Pay Status</label>
                    <div class="controls">
                        <select id="flag_paid" name="flag_paid">
                        	<option value="0">Pending</option>
                        	<option value="1">Approved</option>
                        </select>
                    </div>
                    <label class="control-label" for="typeahead">&nbsp;</label> 
                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/ticket/payment/report/list')"/>
                        <!-- input tabindex="3" type="button" id="btn_reconcile" class="btn btn-primary .submit" name="save" value="Refresh List" onclick="be_umroh.getReconcile()"/ -->
                    </div>
                </div>
            </fieldset>
            <div class="result"></div>
        </form>
    </div>
    
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>
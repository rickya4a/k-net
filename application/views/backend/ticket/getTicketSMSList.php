<?php
	if($result == null) {
		 setErrorMessage();
	} else {
?>
<form>
	
	<table width="80%" align="center" class="table table-striped table-bordered bootstrap-datatable datatable">
		<thead>
			<tr>
			  <th colspan="5">List Participants</th>	
			</tr>
			<tr>
			  <th width="8%">No</th>	
			  <th>Name</th>
			  
			  <th width="25%">No. HP</th>
			  <th width="25%">Ticket No</th>
			  <th width="10%">SMS</th>
			</tr>
		</thead>
		<tbody>
			<?php
		        $no = 1;
		        foreach($result as $row){
		    ?>
		    
		    <tr>
		        <td align="right"><?php echo $no; ?></td>
		        <td align="center"><?php echo $row->trans_det_name; ?></a></td>
		        
		        <td align="center"><?php echo $row->trans_det_hp; ?></td>
		        <td align="center"><?php echo $row->trans_tiket_no; ?></td>
		        <td align="center"><input type="button" class="btn btn-mini btn-primary" value="Resend SMS" onclick="be_ticket.resendTicketSMS('<?php echo $row->trans_tiket_no; ?>')" /></td>
		    </tr>
		    <?php
		        $no++;
		        }
		    ?>
		</tbody>
	</table>
</form>

<?php
}
?>
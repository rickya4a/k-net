<?php if ($data == NULL) {
	echo setErrorMessage('Data not found');
} else { ?>
<style>
td[case="uppercase"] {
	text-transform: uppercase;
}
</style>
<table class="table table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
		<tr>
			<th colspan="9">List Trx Integrity Check</th>
		</tr>
		<tr>
			<th>No. Order</th>
			<th>Valid Dfno</th>
			<th>Valid Fullname</th>
			<th>Fullname</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$i = 1;
	foreach ($data as $key) { ?>
		<tr>
			<td>
				<?php echo $key->order_no; ?>
				<input type="hidden" name="id" value="<?php echo $key->id; ?>">
			</td>
			<td><?php echo $key->valid_dfno; ?></td>
			<td case="uppercase"><?php echo $key->valid_fullnm; ?></td>
			<td><?php echo $key->fullnm; ?></td>
		</tr>
		<?php
	$i++;
	}; ?>
	</tbody>
</table>
<script>
	$(document).ready(function () {
		$(All.get_active_tab() + " .datatable").dataTable({
			"aLengthMenu": [
				[
					10, 25, 50, 100, -1
				],
				[
					10, 25, 50, 100, 'All'
				]
			],
			"sPaginationType": "bootstrap",
			"oLanguage": {},
			"bDestroy": true
		});
		$(All.get_active_tab() + " .datatable").removeAttr('style');
	});
</script>
<?php } ?>
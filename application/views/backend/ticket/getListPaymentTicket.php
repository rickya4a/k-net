<div class="mainForm">
    <div id="frmApproval">
        <form class="form-horizontal" id="listPaymentTicket" name="listPaymentTicket" method="post">
            <fieldset>
                <div class="control-group"> 
                    <label class="control-label" for="typeahead">Register Date</label>
                    <div class="controls">
                        <input type="text" class="dtpicker " id="pay_ticket_from" name="pay_ticket_from" />&nbsp;&nbsp;to&nbsp;
                        <input type="text"  class="dtpicker " id="pay_ticket_to" name="pay_ticket_to" />
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be/ticket/payment/list')" />
                    </div>
                       <!-- input tabindex="3" type="button" id="btn_reconcile" class="btn btn-primary .submit" name="save" value="Refresh List" onclick="be_umroh.getReconcile()"/ -->                   
                </div>
            </fieldset>
            <div class="result"></div>
        </form>
    </div>
    
</div>
<script>
	$(document).ready(function() { 
		$(All.get_active_tab() + " .dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>
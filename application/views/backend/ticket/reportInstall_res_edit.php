<?php
    if(empty($detCicilan)){
        echo "no Data";
    }else{
?>
<form method="post" action="<?php echo site_url('be_umroh/report/printExcell');?>" target="_blank">
    <input type="hidden" name="date_from" id="date_from" value="<?php echo $date_from;?>"/>
    <input type="hidden" name="date_to" id="date_to" value="<?php echo $date_to;?>"/>
    <input type="hidden" name="banklist" id="banklist" value="<?php echo $banklist;?>"/>
   <input type="submit" id="printTxt" name="printTxt" value="Print Report" class="btn btn-primary"/> 
</form>
<div class="overflow-auto">
 <table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable datatable ">
 	
 	<?php
 		$totIns = 5;
		$totInsHead = $totIns + ($totIns * 2) + 6 + 1;
    	echo "<tr style='height: 50px;text-align: center;font-size: 16px;text-shadow: none;'>
		        <td colspan='$totInsHead'>Report Setoran Cicilan K-Smart Umroh melalui Bank ".$headCicilan[0]->bankDesc."</td>
		        <td colspan='6'>&nbsp;</td>
		     </tr>
    
		     <tr style='height: 30px;text-align: center;text-shadow: none;' >
		        <th>No</th>
		        <th>Reg. Date</th>
		        <th>Reg. No</th>
		        <th>Member ID</th>
		        <th>Member Name</th>
		        <th>VA No.</th>
		        <th>DP</th>
		        <th>Voucher Amt</th>
		        <th>Starterkit</th>
		        <th>DP - (Voucher + SK)</th>
		        <th>IP No</th>
		    ";
        
	        for ($i=0; $i<$totIns; $i++) {
	        	$ii = $i+1; 
				echo "<th>Inst. Date $ii</th>
					  <th>Installment $ii</th>";
			}
			
		echo "	<th>Total Saldo</th>
			  </tr>";
	    
		//detail of installment
		
        $no 			= 	1;
        $totFund 		= 	0;
		$saldoTempTot	=	0;
		$saldoTotX		=	0;
		$DPUmrTot		=	0;
		$vchUmrTot		=	0;
		$vchSKTot		=	0;
		$saldoTempTot	=	0;
        foreach($detCicilan as $list){

			echo "<tr style='height: 30px;text-align: center;text-shadow: none;'>
		        	<td>".$no."</td>
			        <td>".$list->createdt2."</td>
			        <td>".$list->registerno."</td>
			        <td>".$list->dfno."</td>
			        <td>".$list->fullnm."</td>
			        <td>".$list->novac."</td>";
			        			
			$vcrAmt = explode('#', $list->installment_d);
			if(count($vcrAmt) > 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= $vcrAmt[1];
			}elseif(count($vcrAmt) == 1){
				$vchUmr = $vcrAmt[0];
				$vchSK 	= 0;
			}else{
				$vchUmr = 0;
				$vchSK 	= 0;
			}
			//$instTD;
			
			$instAmt 	= explode('#', $list->installment_c); // data DP & installment
			$instDt 	= explode('#', $list->installment_createdt); // data DP & installment
			$DPUmr 		= $instAmt[0];
			$DPUmrTot 	= $DPUmrTot + $DPUmr;
			$saldoTemp	= $DPUmr - ($vchUmr + $vchSK);
			$saldoTot 	= $saldoTemp;
			$vchUmrTot	= $vchUmrTot + $vchUmr;
			$vchSKTot	= $vchSKTot + $vchSK;
			$saldoTempTot	= $saldoTempTot + $saldoTemp;
			
			echo   "<td>".number_format($DPUmr,0,".",".")."</td>
					<td>".number_format($vchUmr,0,".",".")."</td>
					<td>".number_format($vchSK,0,".",".")."</td>
					<td>".number_format($saldoTemp,0,".",".")."</td>
					<td>".$list->IPno."</td>";

			if(count($instAmt) > 1){
				$instTD = "";
				for ($i=0; $i < count($instAmt)  ; $i++) {  //iteration for installment data
					if($i > 0){
						$installment 	= $instAmt[$i];
						$installmentDt 	= $instDt[$i];
						echo "<td>$installmentDt</td>
							  <td>".number_format($installment,0,".",".")."</td>";
						$saldoTot = $saldoTemp + $installment;
					}
				}
				
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
					echo "<td>&nbsp;</td>
						  <td>".number_format(0,0,".",".")."</td>";
				}
				
			}elseif(count($instAmt) == 1){
				$installment 	= 0;
				for ($i = count($instAmt); $i < $totIns+1 ; $i++) {  //iteration for installment data
					$installment 	= 0;
					echo "<td>&nbsp;</td>
						  <td>".number_format(0,0,".",".")."</td>";
				}
				
			}else{
				$installment 	= 0;
			}
			
			echo   "<td>".number_format($saldoTot,0,".",".")."</td>
				    </tr>";
				  
	        //$totFund += $list->tot_fund;
	        $no++;
        }
    ?>
    <tr style="height: 30px;text-align: center;font-size: 14px;text-shadow: none;"> 
        <td colspan="6">Total</td>
        <td style="text-align: right;font-weight: "><?php echo number_format($DPUmrTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: "><?php echo number_format($vchUmrTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: "><?php echo number_format($vchSKTot,0,".",".");?></td>
        <td style="text-align: right;font-weight: "><?php echo number_format($saldoTempTot,0,".",".");?></td>
    </tr>
</table>
</div>
<?php
    }
?>
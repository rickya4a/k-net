<?php
	if($result == null) {
		setErrorMessage();
	} else {
?>
<form method="post" action="<?php echo site_url('be/ticket/location/listXls');?>" target="_blank">
    <input type="hidden" name="pay_status" id="pay_status" value="<?php echo $paystatus;?>"/>
    <input type="hidden" name="event_id" id="event_id" value="<?php echo $eventid;?>"/>
   	<input type="submit" id="printTxt" name="printTxt" value="Print Report" class="btn btn-primary"/> 
</form>

	<table width="100%" align="center" class="table table-striped table-bordered bootstrap-datatable datatable">
		<thead>
			<tr>
			  <th colspan="6">List Participants</th>	
			</tr>
			<tr>
			  <th width="8%">No</th>	
			  <th>Name</th>
			  
			  <th width="20%">No. HP</th>
			  <th width="15%">Ticket No</th>
			  <th width="15%">Booking No</th>
			  <th width="7%">Status</th>
			</tr>
		</thead>
		<tbody>
			<?php
		        $no = 1;
		        foreach($result as $row){
		    ?>
		    
		    <tr>
		        <td align="right"><?php echo $no; ?></td>
		        <td align="center"><?php echo $row->trans_det_name; ?></a></td>
		        
		        <td align="center"><?php echo $row->trans_det_hp; ?></td>
		        <td align="center"><?php echo $row->trans_tiket_no; ?></td>
		        <td align="center"><?php echo $row->bookingNo; ?></td>
		        <td align="center"><?php if($row->status_pay == 'A'){
		        								echo 'Approved';
		        							}else{
		        								echo 'Pending';
		        							}; ?></td>
		    </tr>
		    <?php
		        $no++;
		        }
		    ?>
		</tbody>
	</table>

<?php
}
?>


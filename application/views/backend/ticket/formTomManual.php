<style>
	.fullname {
		width: 500px;
	}

	.idx {
		width: 200px
	}

	.small_select,
	.birthdt {
		width: 80px
	}
</style>
<div class="mainForm">
	<form class="form-horizontal" id="tomManual" method="post">
		<fieldset>
			<div class="control-group">
				<label class="control-label" for="typeahead">Member ID</label>
				<div class="controls">
					<input type="text" class="idx" id="memberid" name="memberid"
						onchange="be_member.getDataMemberInfo(this.value)" />
					<input type="text" class="fullname" id="membername" name="membername" tabindex="3"
						placeholder="Member Name" />
					<input type="hidden" id="status_expire" name="status_expire" value="" />
				</div>
				<label class="control-label" for="typeahead">ID Number</label>
				<div class="controls">
					<input type="text" class="span5" id="ktpno" name="ktpno" readonly="readonly">

				</div>
				<label class="control-label" for="typeahead">Address</label>
				<div class="controls">
					<input type="text" class="span6" id="addr1" name="addr1">
				</div>

				<label class="control-label" for="typeahead">&nbsp;</label>
				<div class="controls">
					<input type="text" class="span6" id="addr2" name="addr2">
				</div>

				<label class="control-label" for="typeahead">&nbsp;</label>
				<div class="controls">
					<input type="text" class="span6" id="addr3" name="addr3">
				</div>
				<label class="control-label" for="typeahead">Email</label>
				<div class="controls">
					<input type="text" class="span6" id="email" name="email">
				</div>
				<label class="control-label" for="typeahead">Cellular Phone No</label>
				<div class="controls">
					<input type="text" class="idx" id="tel_hp" name="tel_hp"
						onchange="All.checkDoubleInput('db2/get/tel_hp/from/msmemb/','tel_hp',this.value)" />

				</div>
				<label class="control-label" for="typeahead">&nbsp;</label>
				<div class="controls" id="inp_btn">
					<input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="submit"
						value="Submit" onclick="save()" />
					<input tabindex="5" type="reset" class="btn btn-reset" value="Reset" />
					<input tabindex="3" type="button" id="btn_input_user" class="btn btn-success" name="submit"
						value="List" onclick="All.ajaxFormPost(this.form.id, 'etiket/tom/manual/list')" />
				</div>
			</div> <!-- end control-group -->
		</fieldset>
	</form>
	<div class="result"></div>
</div>
<!--/end mainForm-->

<script>
function save() {
    var formData = new FormData($(All.get_active_tab() + " #tomManual")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('etiket/tom/manual/save'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
}
</script>
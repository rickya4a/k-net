<div class="mainForm">
  <form class="form-horizontal" id="ticketSMSResend">
    <fieldset>      
      <div class="control-group"> 
      	<label class="control-label" for="typeahead">Search By</label>                             
        <div class="controls">
        	<select id="searchBy" name="searchBy">
        		<option value="">--Select Here--</option>
        		<option value="trans_det_name">Participant Name</option>
        		<option value="trans_tiket_no">Ticket No</option>
        	</select>
        </div>
        <label class="control-label" for="typeahead">Parameter Value</label>                             
        <div class="controls">
        	<input type="text" id="paramValue" name="paramValue" class="span4" />
        </div>		      
        <?php
          echo datepickerFromTo("Transaction Date", "ticket_sms_from", "ticket_sms_to");
        ?>
        
        <label class="control-label" for="typeahead">&nbsp</label>                             
        <div class="controls"  id="inp_btn">
            <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'be/ticket/sms/list')" />
            <input tabindex="4"  type="reset" class="btn btn-reset" value="Reset" />
            
         </div>
        </div> <!-- end control-group -->
     </fieldset>
    
    <div class="result"></div>
  </form> 
</div><!--/end mainForm-->
<script>
	$(document).ready(function() { 
		$(".dtpicker").datepicker({
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
		});
	});	
</script>

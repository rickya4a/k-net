<?php
    if(empty($result)){
        echo "no Data";
    }else{
?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
    <tr>
        
        <th>Reg. No</th>
        <th>Name</th>
        <th>Qty</th>
        <th>Price</th>
        <th>Date</th>
        <th>Pay Type</th>
        
    </tr>
    </thead>
    <?php
        //$no = 1;
        foreach($result as $row){
    ?>
    
    <tr>
       
        <td align="center"><a href='#' id="<?php echo $row->trans_hdr_id;?>" onclick="All.ajaxShowDetailonNextForm('be/ticket/event/detail/<?php echo $row->trans_hdr_id;?>')"><?php echo $row->bookingNo;?></a></td>
        <td><?php echo $row->trans_hdr_name;?></td>
        <td style="text-align: right;"><?php echo number_format($row->trans_qty_ticket,0,".",".");?></td>
        <td style="text-align: right;"><?php echo number_format($row->trans_hdr_dp,0,".",".");?></td>
        <td align="center"><?php echo $row->trans_hdr_date;?></td>
        <td align="center">
        	 <?php if($row->pay_type == "0") {echo "Transfer"; } else {echo "SGO";}?>
         </td>
    </tr>
    <?php
        //$no++;
        }
    ?>
</table>
<?php
    }
?>

<script type="text/javascript">
$(document).ready(function() 
{
	$(All.get_active_tab() + " .datatable").dataTable( {
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
		"sPaginationType": "bootstrap",
		"oLanguage": {
		},
        "bDestroy": true
	});
    $(All.get_active_tab() + " .datatable").removeAttr('style');
 });

</script>
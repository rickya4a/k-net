<?php
	if($result == null) {
		echo "<script>alert('No result found..')</script>";
	} else {
?>
<form>
	<table  width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
		<thead>
		  <th colspan="2">Detail Event</th>	
		</thead>
		<tbody>
			<tr>
				<td width="20%">Event Goals</td>
				<td><?php echo $result[0]->event_goals; ?></td>
			</tr>
			<tr>
				<td>Event Desc</td>
				<td><?php echo $result[0]->event_desc; ?></td>
			</tr>
			<tr>
				<td>Event Address</td>
				<td><?php echo $result[0]->event_address; ?></td>
			</tr>
			<tr>
				<td>Event Date</td>
				<td><?php echo $result[0]->event_date; ?></td>
			</tr>
			<tr>
				<td>Participant Name</td>
				<td><?php echo $result[0]->trans_hdr_name; ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><?php echo $result[0]->trans_hdr_email; ?></td>
			</tr>
			<tr>
				<td>HP</td>
				<td><?php echo $result[0]->trans_hdr_hp; ?></td>
			</tr>
			<tr>
				<td>Ticket Qty</td>
				<td><?php echo $result[0]->trans_qty_ticket; ?></td>
			</tr>
			<tr>
				<td>Total Price</td>
				<td><?php echo number_format($result[0]->trans_hdr_dp,0,".","."); ?></td>
			</tr>
			<tr>
				<td>Payment Status</td>
				<td><?php if($result[0]->flag_paid == "1") {echo "Approved";} else {echo "Pending";} ?></td>
			</tr>
			<tr>
				<td>Payment</td>
				<td><?php if($result[0]->flag_paid == "0") {echo "Transfer";} else {echo "SGO";} ?></td>
			</tr>
			<tr>
				<td colspan="2"><input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/></td>
			</tr>
		</tbody>
	</table>
	<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
		<thead>
			<tr>
			  <th colspan="5">List Participants</th>	
			</tr>
			<tr>
			  <th width="8%">No</th>	
			  <th>Name</th>
			  <th width="15%">Pay</th>
			  <th width="20%">No. HP</th>
			  <th width="20%">Ticket No</th>
			</tr>
		</thead>
		<tbody>
			<?php
		        $no = 1;
		        foreach($result as $row){
		    ?>
		    
		    <tr>
		        <td align="right"><?php echo $no; ?></td>
		        <td align="center"><?php echo $row->trans_det_name; ?></a></td>
		        <td align="right"><?php echo number_format($row->trans_det_dp,0,".","."); ?></td>
		        <td align="center"><?php echo $row->trans_det_hp; ?></td>
		        <td align="center"><?php echo $row->trans_tiket_no; ?></td>
		        
		    </tr>
		    <?php
		        $no++;
		        }
		    ?>
		</tbody>
	</table>
</form>

<?php
}
?>
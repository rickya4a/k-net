

<?php
    /*header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=ReportInstallmentPerBank.xls");
    header("Pragma: no-cache");
    header("Expires: 0");*/
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=ReportListingYoga.xls" );
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");

	if($result == null) {
		setErrorMessage();
	} else {
?>
	<table border="1" width="100%" align="center" class="table table-striped table-bordered bootstrap-datatable datatable">
		<thead>
			<tr>
			  <th colspan="6">List Participants</th>	
			</tr>
			<tr>
			  <th width="8%">No</th>	
			  <th>Name</th>
			  
			  <th width="20%">No. HP</th>
			  <th width="15%">Ticket No</th>
			  <th width="15%">Booking No</th>
			  <th width="7%">Status</th>
			</tr>
		</thead>
		<tbody>
			<?php
		        $no = 1;
		        foreach($result as $row){
		    ?>
		    
		    <tr>
		        <td align="right"><?php echo $no; ?></td>
		        <td align="center"><?php echo $row->trans_det_name; ?></a></td>
		        
		        <td align="center"><?php echo $row->trans_det_hp; ?></td>
		        <td align="center"><?php echo $row->trans_tiket_no; ?></td>
		        <td align="center"><?php echo $row->bookingNo; ?></td>
		        <td align="center"><?php if($row->status_pay == 'A'){
		        								echo 'Approved';
		        							}else{
		        								echo 'Pending';
		        							}; ?></td>
		    </tr>
		    <?php
		        $no++;
		        }
		    ?>
		</tbody>
	</table>

<?php
}
?>



<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="formInputProductCat">
    <fieldset>      
      <div class="control-group">       
         <?php
         //Product Category ID
         $onchange = "onchange=All.checkDoubleInput('product/cat/list/','cat_id',this.value)";
         $cat_id = array(
		 	"labelname" => "Product Category ID",
		 	"fieldname" => "cat_id",
		 	"addClass" => "setReadOnly",
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($cat_id);
		  //Product Category Name
         $onchange = "onchange=All.checkDoubleInput('product/cat/list/','cat_desc',this.value)";
         $cat_desc = array(
		 	"labelname" => "Description",
		 	"fieldname" => "cat_desc",
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($cat_desc);
		 //remarks
		 $remarks = array(
		 	"labelname" => "Remarks",
		 	"fieldname" => "remarks"
 		 );
         echo inputText($remarks);
		 //status
		 echo selectFlagActive("Status", "status");
         $input = "All.inputFormData('product/cat/save', this.form.id)";
		 $update = "All.updateFormData('product/cat/update', 'formInputProductCat', 'product/cat/list')";
		 $view = "All.getListData('product/cat/list')";
         echo button_set($input, $update, $view);
		 ?>    
        </div> <!-- end control-group -->
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->

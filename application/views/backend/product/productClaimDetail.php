<?php
  if($result == null){
        echo setErrorMessage("No result found");
    }else{
?>
<form>
<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th colspan="6">Detail Employee Product Purchase</th>
		</tr>
		<tr>
			<th>No</th>			
			<th>Code</th>
			<th>Product Name</th>
			<th>Qty</th>
			<th>DP</th>
			<th>Total DP</th>
			
		</tr>
	</thead>
	<tbody>
		<?php 
		    $i=1;
			foreach($result['arrayData'] as $dta) {
				echo "<tr>";
				echo "<td align=right>$i</td>";
				echo "<td align=center>$dta->prdcd</td>";
				echo "<td align=center>$dta->prdnm</td>";
				
				echo "<td align=right>".number_format($dta->qtyord, 0, ".", ",")."</td>";
				echo "<td align=right>".number_format($dta->tdp, 0, ".", ",")."</td>";
				echo "<td align=right>".number_format($dta->totaldp, 0, ".", ",")."</td>";
				
				echo "</tr>";
				$i++;
			}
		?>
	</tbody>
</table>
<p></p>
	<div>
		 <input value="<< Back" type="button" class="btn btn-small btn-warning" onclick="All.back_to_form(' .nextForm1',' .mainForm')"/>
		 <!--<input type="button" value="Save Changes" class="btn btn-small btn-primary" onclick="be_member.updateDataMember()" />-->
    </div>
	<p></p></form>
<?php
    	
	}	
?>
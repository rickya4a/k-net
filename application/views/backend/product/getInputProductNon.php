<div class="mainForm">
    <form class="form-horizontal" enctype="multipart/form-data" id="formProductSearch">
        <fieldset>
            <div class="control-group">
                <!--<label class="control-label" for="typeahead">Pencarian</label>
                <div class="controls">
                    <select id="param" name="param" class="span4">
                        <option value="prdcd">Kode Produk</option>
                        <option value="prdnm">Nama Produk</option>
                        <option value="F">Daftar Kode Produk Free</option>
                        <option value="knet">Daftar Produk K-Net</option>
                        <option value="non_knet">Daftar Produk Non K-Net</option>
                        <option value="dis">Daftar Produk Inden / Discontinue</option>

                    </select>
                </div>-->
                <label class="control-label" for="typeahead">Parameter/Nilai</label>
                <div class="controls">
                    <input type="text" class="TabOnEnter span6" id="paramValue" name="paramValue" placeholder="Masukkan ID Stokist" onchange="cek_stokist(this.value)"/>
                </div>

                <label class="control-label" for="typeahead">Nama Stokist</label>
                <div class="controls">
                    <input type="text" class="TabOnEnter span6" id="paramName" name="paramName" placeholder="Otomatis" readonly/>
                </div>

                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input type="button" id="btn_input_user" class="btn btn-success .submit" name="save" value="View List" onclick="All.ajaxFormPost(this.form.id,'product/list_non_available')" />
                    <input type="reset" class="btn btn-reset" value="Reset" />

                </div>
            </div> <!-- end control-group -->
</div><!-- end control-group -->
</fieldset>
</form>
<div class="result"></div>
</div><!--/end mainForm-->

<script>
    function cek_stokist(val){
        $.ajax({
            type: "POST",
            url: All.get_url('product/cek_stokist'),
            dataType: 'json',
            data: {kd_stk:val},
            success: function (result) {

                if (result.response == "true") {

                    document.getElementById("paramName").value= result.message;
                }else{
                    document.getElementById("paramName").value="";
                    alert("Stokist Tidak Ditemukan!");
                }
            }

        });
    }
</script>
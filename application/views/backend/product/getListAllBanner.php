<?php
  if($listPrd == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listPrd><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=6>List Banner</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=15%>ID</th><th>Description</th><th width=20%>Tanggal</th><th width=10%>Status</th><th width=10%>Action</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listPrd as $list) {
                echo "<tr id=\"$i\">";
                echo "<td>$list->id</td>";
                echo "<td><input type=\"hidden\" id=\"cat_inv_id$i\" value=\"$list->id\" /><div align=center>$list->hdr_desc</div></td>";
				echo "<td><div align=center>$list->uploadDate</div></td>";
                if($list->hdr_status) {
                	echo "<td><div align=center>Aktif</div></td>";
                } else {
					echo "<td><div align=center>Non Aktif</div></td>";
                }
                $ondelete = "All.deleteFormData('product/delete/', '$list->id', 'product/list')";
                $arr = array(
				    "update" => "Product.getUpdateBanner($i)",
				    "delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

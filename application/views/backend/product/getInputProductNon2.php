<div class="mainForm">
    <form class="form-horizontal" enctype="multipart/form-data" id="formProductSearch">
        <fieldset>
            <div class="control-group">

        <label class="control-label" for="typeahead">Kode Stokist</label>
        <div class="controls">
            <input type="text" class="TabOnEnter span6" id="paramValue" name="paramValue" placeholder="Masukkan Kode Stokist" onchange="cek_stokist(this.value)"/>
        </div>

                <label class="control-label" for="typeahead">Nama Stokist</label>
                <div class="controls">
                    <input type="text" class="TabOnEnter span6" id="paramName" name="paramName" placeholder="Otomatis" readonly/>
                </div>
               <!-- <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <input type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'product/search/list')" />
                    <input type="reset" class="btn btn-reset" value="Reset" />
                </div>-->

                <br><br>
                <?php
                echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
                echo "<thead><tr><th colspan=6>List Product</th></tr>";
                echo "<tr bgcolor=#f4f4f4><th width=6%>No</th><th width=15%>ID</th><th>Description</th><th width=20%>Category</th><th width=10%>Status</th><th width=10%>Non Available</th></thead></tr>";
                echo "<tbody>";
                $i = 1;


                foreach($listPrd as $list) {
                    echo "<tr id=\"$i\">";
                    echo "<td><div align=right>$i<div></td>";
                    echo "<td><div align=center><input type=\"hidden\" id=\"cek_inv_id$i\"  name=\"cek_inv_id[]\" value=\"$list->cat_inv_id\"/>$list->cat_inv_id</div></td>";
                    echo "<td><div align=center>$list->cat_inv_desc</div></td>";
                    echo "<td><div align=center>$list->cat_desc</div></td>";
                    if($list->is_discontinue) {
                        echo "<td><div align=center>Discontinue</div></td>";
                    } else {
                        echo "<td><div align=center>Ready</div></td>";
                    }

                    $status_non= trim($list->flag_non_available);

                    if($status_non != "OFF"){
                        $status="ON";
                    }else{
                        $status="OFF";
                    }
                    /*if($status_non == "1"){
                        $status_non="OFF";
                    }else{
                        $status_non="ON";
                    }*/
                    /*$ondelete = "All.deleteFormData('product/delete/', '$list->cat_inv_id', 'product/list')";
                    $arr = array(
                        "update" => "Product.getUpdateProduct($i)",
                        "delete" => $ondelete
                    );
                    echo btnUpdateDelete($arr);*/
                    echo"<td><div align=center>
                        <input type=\"hidden\" id=\"butt_val$i\"  name=\"butt_val[]\" value=\"$status\"/>
                        <input type=\"button\" id=\"but_inv_id\"  name=\"but_inv_id\" value=\"$status\" onclick=\"save_stokist($i)\"/>
                        </div></td>";

                    echo "</tr>";
                    $i++;
                }
                echo "</tbody></tr>";
                echo "</table>";
                    ?>

                <label class="control-label" for="typeahead">&nbsp</label>
                <div class="controls"  id="inp_btn">
                    <!--<input type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost(this.form.id,'product/search/list')"-->
                    <!--<input type="button" id="btn_input_user" class="btn btn-primary" name="save" value="Submit" onclick="save_stokist()" />-->
                    <!--<input type="button" value="Submit" onclick="save_stokist()">
                    <input type="reset" class="btn btn-reset" value="Reset" />-->

                </div>
            </div><!-- end control-group -->
        </fieldset>
    </form>
    <div class="result"></div>
</div><!--/end mainForm-->
<script>
    $( document ).ready(function() {
        All.set_datatable();
    });
</script>

<script>
    function cek_stokist(val){
        $.ajax({
            type: "POST",
            url: All.get_url('product/cek_stokist'),
            dataType: 'json',
            data: {kd_stk:val},
            success: function (result) {

                if (result.response == "true") {

                    document.getElementById("paramName").value= result.message;
                }else{
                    document.getElementById("paramName").value="";
                    alert("Stokist Tidak Ditemukan!");
                }
            }

        });
    }

    function save_stokist(param) {
        var loccd = document.getElementById("paramValue").value;
        var prdcd = document.getElementById("cek_inv_id" + param).value;
        var button = document.getElementById("butt_val"+param).value;
        //alert(button+" "+prdcd+" "+loccd);

        if(button == "ON" && loccd !=""){
            //alert(button);
            $.ajax({
                type: "POST",
                url: All.get_url('product/save_nonavailable'),
                dataType: 'json',
                data: {kd_stk:loccd,kd_brg:prdcd},
                success: function (result) {

                    if (result.response == "true") {
                        document.getElementById("butt_val"+param).value="OFF";
                        alert("Data Berhasil Diubah Menjadi Non Available!");

                    }else{
                        document.getElementById("butt_val"+param).value="ON";
                        alert("Data Gagal Diubah Menjadi Non Available!");
                    }
                }

            });
        }else if(button == "OFF" && loccd !=""){

            //alert(button);
           $.ajax({
                type: "POST",
                url: All.get_url('product/del_nonavailable'),
                dataType: 'json',
                data: {kd_stk:loccd,kd_brg:prdcd},
                success: function (result) {

                    if (result.response == "true") {
                        document.getElementById("butt_val"+param).value="ON";
                        alert("Data Berhasil Diubah Menjadi Available!");

                    }else{
                        document.getElementById("butt_val"+param).value="OFF";
                        alert("Data Gagal Diubah Menjadi Available!");
                    }
                }

            });
            //alert("Data Akan Dihapus!");
        }else{
         alert("Silahkan Lengkapi Data ... ")
         }
     }
</script>
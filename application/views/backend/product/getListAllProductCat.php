<?php
  if($listPrdCat == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listPrdCat><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=5>List Product Category</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=6%>No</th><th width=10%>ID</th><th>Description</th><th width=10%>Create By</th><th width=15%>Action</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listPrdCat as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
                echo "<td><div align=center><input type=\"hidden\" id=\"cat_id$i\" value=\"$list->cat_id\" />$list->cat_id</div></td>";
                echo "<td><div align=center>$list->cat_desc</div></td>";  
				echo "<td><div align=center>$list->createnm</div></td>";
               
                $ondelete = "All.deleteFormData('product/cat/delete/', '$list->cat_id', 'product/cat/list')";
                $arr = array(
				    "update" => "Product.getUpdateProductCat($i)",
				    "delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

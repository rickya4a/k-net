<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="formInputPriceCode">
    <fieldset>      
      <div class="control-group">       
         <?php
         echo inputCountryHQBranch();
         //pricecode ID
         $onchange = "onchange=All.checkDoubleInput('pricecode/list/','pricecode',this.value)";
         $pricecode = array(
		 	"labelname" => "Price Code",
		 	"fieldname" => "pricecode",
		 	"addClass" => "setReadOnly",
		 	"placeholder" => placeholderCheck(),
		 	"maxlength" => "5",
		 	"event" => $onchange
 		 );
         echo inputText($pricecode);
		  //Description
         $onchange = "onchange=All.postCheckDoubleInput('pricecode/postlist/','pricecode_desc',this.value)";
         $pricecode_desc = array(
		 	"labelname" => "Description",
		 	"fieldname" => "pricecode_desc",
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($pricecode_desc);
		 
         
          //remarks
		 $remarks = array(
		 	"labelname" => "Remarks",
		 	"fieldname" => "remarks"
 		 );
         echo inputText($remarks);
		 
		 //status
		 echo selectFlagActive("Status", "status");
         $input = "All.inputFormData('pricecode/save', this.form.id)";
		 $update = "All.updateFormData('pricecode/update', this.form.id, 'pricecode/list')";
		 $view = "All.getListData('pricecode/list')";
         echo button_set($input, $update, $view);
		 ?>    
        </div> <!-- end control-group -->
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->

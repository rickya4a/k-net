<?php
  if(isset($err_msg)) {
  	  echo setErrorMessage($err_msg);
  } else {
?>
<table align="center" class="table table-striped table-bordered bootstrap-datatable datatable" width="100%">
	<thead>
	<tr>
		<th colspan="6">Employee Product Purchase List</th>
	</tr>	
	<tr>
		<th>No</th>
		<th width="20%">Invoice No</th>
		<th>Trx Date</th>
		<th>Input By</th>
		<th width="20%">Total Pay</th>
		<th width="20%">Purchase By</th>
	</tr>
	</thead>
	<tbody>
	<?php
	  $i = 1;
	  $tot = 0;
	  foreach($result['arrayData'] as $data) {
	  	echo "<tr>";
		echo "<td align=right>$i</td>";
		echo "<td align=center><a href = \"#\" id=\"$data->invoiceno\" onclick=\"All.ajaxShowDetailonNextForm('product/claim/detail/$data->invoiceno')\">$data->invoiceno</a></td>";
		echo "<td align=center>$data->invoicedt</td>";
		echo "<td align=center>$data->usernya</td>";
		echo "<td align=right>".number_format($data->tdp, 0)."</td>";
		echo "<td align=center>$data->remark</td>";
		echo "</tr>";  
		$i++;
		$tot += $data->tdp;
	  }
	?>
	<tr>
		<td colspan="4" align="center">T O T A L</td>
		<td align="right"><?php echo number_format($tot, 0) ?></td>
		<td>&nbsp;</td>
	</tr>
	</tbody>
</table>
<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
</script>
<?php
}
?>
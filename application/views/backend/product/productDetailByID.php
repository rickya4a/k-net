<?php
	if($result == null) {
		echo setErrorMessage();
	} else {
	$title = "width: 20%";	
?>
<table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
   <thead>
   	<tr>
   		<th colspan="10">
   			<?php
   			 if($search == "prdcd") {
   			 	echo "DAFTAR PENCARIAN PRODUK BERDASARKAN KODE";
   			 } else if($search == "prdnm") {
   			 	echo "DAFTAR PENCARIAN PRODUK BERDASARKAN NAMA";
   			 } else if($search == "3") {
   			 	echo "DAFTAR PRODUK FREE";
   			 } else if($search == "knet") {
   			 	echo "DAFTAR PRODUK DISPLAY DI K-NET";
   			 } else if($search == "non_knet") {
   			 	echo "DAFTAR PRODUK YANG TIDAK DI DISPLAY DI K-NET";
   			 } else if($search == "dis") {
   			 	echo "DAFTAR PRODUK YANG INDEN / DISCONTINUE";
   			 }
   			?>
   		</th>
   	</tr>
   	 <tr>
		<th rowspan="2" width="3%">No</th>
		<th rowspan="2" width="10%">Kode</th>
		<th rowspan="2" width="33%">Nama Produk</th>
		<th colspan="2">Harga Distributor</th>
		
		<th colspan="2">Harga Customer</th>
		<th rowspan="2" width="5%">BV</th>
		<th rowspan="2" width="8%">K-net</th>
		<th rowspan="2" width="8%">Inden</th>
	 </tr>
	 <tr>
		<th width="7%">12W</th>
        <th width="7%">12E</th>
        <th width="7%">12W</th>
        <th width="7%">12E</th>
	</tr>    
	
   </thead>
   <tbody> 
    <?php
     $no = 1;
      foreach($result as $data) {
      	echo "<tr>";
		echo "<td align=right>$no</td>";
		echo "<td align=center>$data->prdcd</td>";
		echo "<td align=left>&nbsp;$data->prdnm</td>";
		echo "<td align=right>".number_format($data->price_w, 0, "", ".")."</td>";
		echo "<td align=right>".number_format($data->price_e, 0, "", ".")."</td>";  
		
		echo "<td align=right>".number_format($data->price_cw, 0, "", ".")."</td>";
		echo "<td align=right>".number_format($data->price_ce, 0, "", ".")."</td>";
		echo "<td align=right>".number_format($data->bv, 0, "", ".")."</td>";  
		if($data->ecomm_status == "1") {
			echo "<td align=center>";
		    echo "<select class=span20 onchange=\"Product.updateStatusPrd('".$data->prdcd."','ecomm_status',this.value)\">";
		    echo "<option value=\"1\" selected=selected>Y</option>";
		    echo "<option valu\"0\">N</option>";
		    echo "</select>";
		    echo "</td>";
		} else {
			echo "<td align=center>";
		    echo "<select class=span20 onchange=\"Product.updateStatusPrd('".$data->prdcd."','ecomm_status',this.value)\">";
		    echo "<option value=\"1\" >Y</option>";
		    echo "<option valu\"0\" selected=selected>N</option>";
		    echo "</select>";
		    echo "</td>";
		}
		
		
		if($data->is_discontinue == "1") {
			echo "<td align=center>";
		    echo "<select class=span20 onchange=\"Product.updateStatusPrd('".$data->prdcd."','is_discontinue',this.value)\">";
		    echo "<option value=\"1\" selected=selected>Y</option>";
		    echo "<option valu\"0\">N</option>";
		    echo "</select>";
		    echo "</td>";
		} else {
			echo "<td align=center>";
		    echo "<select class=span20 onchange=\"Product.updateStatusPrd('".$data->prdcd."','is_discontinue',this.value)\">";
		    echo "<option value=\"1\" >Y</option>";
		    echo "<option valu\"0\" selected=selected>N</option>";
		    echo "</select>";
		    echo "</td>";
		}
		//echo "<td align=center>$ind</td>";
		echo "</tr>"; 
		$no++; 
      }
    ?>
   </tbody> 
</table>	

<?php		
setDatatable();
	}
?>
<?php
  if($listPriceCode == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listPriceCode><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=5>List Price Code</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=6%>No</th><th width=10%>ID</th><th>Description</th><th width=10%>Create By</th><th width=15%>Action</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listPriceCode as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
                echo "<td><div align=center><input type=\"hidden\" id=\"pricecode$i\" value=\"$list->pricecode\" />$list->pricecode</div></td>";
                echo "<td><div align=center>$list->pricecode_desc</div></td>";  
				echo "<td><div align=center>$list->createnm</div></td>";
               
                $ondelete = "All.deleteFormData('pricecode/delete/', '$list->pricecode', 'pricecode/list')";
                $arr = array(
				    "update" => "Product.getUpdatePriceCode($i)",
				    "delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

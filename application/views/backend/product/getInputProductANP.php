<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="formInputProduct">
    <fieldset>      
      <div class="control-group">
      	<table width="100%" border="0">
      		<tr>
      			<td width="15%" align="right">Product ID&nbsp;</td>
      			<td width="35%">
      				<input type="text" class="setReadOnly span20" placeholder="<?php echo placeholderCheck(); ?>" id="cat_inv_id" name="cat_inv_id" onchange="All.checkDoubleInput('product/list/','cat_inv_id',this.value)" />
      			</td>
      			<td width="15%" align="right">Description&nbsp;</td>
      			<td width="35%">
      				<input type="text" class="span20" placeholder="<?php echo placeholderCheck(); ?>" id="cat_inv_desc" name="cat_inv_desc" onchange="All.checkDoubleInput('product/list/','cat_inv_id',this.value)" />
      			</td>
      		</tr>
      		<tr>
      			<td align="right">Product Category</td>
      			<td>
      				<select id="cat_id" name="cat_id">
      				 <option value="">--Select here--</option>
      				<?php
      				   foreach($listPrdCat as $list) {
		 		          echo "<option value=\"$list->cat_id\">$list->cat_desc</option>";
					   }
      				?>	
      				</select>
      				&nbsp;<input class="btn btn-mini btn-primary" type="button" onclick="Product.refreshListProductCat(' #cat_id')" value="Refresh">
      			</td>
      			<td align="right">Parent product&nbsp;</td>
      			<td>
      				 
      				<select id="parent_cat_inv_id" name="parent_cat_inv_id">
      					 <option value="">--Select here--</option>
      				<?php
      				   foreach($listPrd as $list) {
		 		          echo "<option value=\"$list->cat_inv_id\">$list->cat_inv_desc</option>";
					   }
      				?>	
      				</select>
      				&nbsp;<input class="btn btn-mini btn-primary" type="button" onclick="Product.refreshListProduct(' #parent_cat_inv_id')" value="Refresh">
      			</td>
      		</tr>
      		<tr style="display: none;">
      			<td align="right">Weight&nbsp;</td>
      			<td>
      				<input type="text" class="span20"  id="weight" name="weight" value="1"  />
      			</td>
      			<td align="right">Single/Bundle&nbsp;</td>
      			<td>
      				<select id="inv_type" name="inv_type">
      				<option value="S" selected>Single</option>
      				<option value="B">Bundle</option>	
      				
      				</select>
      			</td>
      			<!---->
      		</tr>
      		<tr >
      			<td align="right">Status&nbsp;</td>
      			<td>
      				<select id="status" name="status">
      				<option value="1" selected>Active</option>
      				<option value="0">Inactive</option>	
      				
      				</select>

      			</td>
      			<td align="right" style="display: none;">MS Online&nbsp;</td>
      			<td style="display: none;">
      				<select id="ms_status" name="ms_status">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>
      			</td>
      			<!---->
      		</tr>
      		<tr style="display: none;">
      			<td align="right">Invoice&nbsp;</td>
      			<td>
      				<select id="bo_inv_status" name="bo_inv_status">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>

      			</td>
      			<td align="right">BO Status&nbsp;</td>
      			<td>
      				<select id="bo_status" name="bo_status">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>
      			</td>
      			<!---->
      		</tr>
			<tr style="display: none;">
				<td align="right">Starterkit Include&nbsp;</td>
				<td>
					<select style="display: none;" id="is_starterkit" name="is_starterkit">
						<option value="0" selected="selected">No</option>
						<option value="1">Yes</option>
					</select>


				</td>
      			<td align="right">Web Status&nbsp;</td>
      			<td>
      				<select id="web_status" name="web_status">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>
      			</td>
      			<!---->
      		</tr>
		    <tr style="display: none;">
      			<td align="right">SC Online &nbsp;</td>
      			<td>
      				<select id="sc_status" name="sc_status">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>

      			</td>
      			<td align="right">Sub SC Online&nbsp;</td>
      			<td>
      				<select id="sub_status" name="sub_status">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>
      			</td>
      			<!---->
      		</tr>
			<tr >
				<td align="right">E-Commerce &nbsp;</td>
				<td>
					<select id="ecomm_status" name="ecomm_status">
						<option value="1" selected>Yes</option>
						<option value="0">No</option>

					</select>

				</td>

      			<td align="right">Discontinue Status&nbsp;</td>
      			<td>
      				<select id="is_discontinue" name="is_discontinue">
      				<option value="1">Yes</option>
      				<option value="0" selected>No</option>
      				
      				</select>
      			</td>
      			<!---->
      		</tr>
            <tr style="display: none;">
      			<td align="right">Shipping Charge&nbsp;</td>
      			<td>
      				<select id="is_charge_ship" name="is_charge_ship">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>

      			</td>
      			<td align="right">Physical Goods&nbsp;</td>
      			<td>
      				<select id="flag_is_do" name="flag_is_do">
      				<option value="1" selected>Yes</option>
      				<option value="0">No</option>	
      				
      				</select>
      			</td>
      			<!---->
      		</tr>

			<tr style="display: none;">
				<td align="right">Qty/box&nbsp;</td>
				<td>
					<input type="text" class="span20" value="1" id="box" name="box"  />
				</td>

				<!---->
			</tr>
      		<tr>
      			<td align="right">Image&nbsp;</td>
      			<td colspan="3">
      				<input id="myfile" type="file" name="myfile" class="cfile span7 typeahead" />
                    <span id="spanPic" class="fileExistingInfo"></span>
                    <input type="hidden" class="fileHiddenExistingInfo" id="filename" name="filename" />
        
      			</td>
      			
      		</tr>
      	</table>
     	 <?php
         //$arr = array("id" => "formInputProduct");
         //echo opening_form($arr);
      	 //echo inputCountryHQBranch();
		 //Product ID
		 
         /*
		 
		 $arr2 = array(
		    "labelname" => "Include Starterkit", 
		 	"fieldname" => "is_starterkit",
		 	"options" => status_arr()
		 ); 
		 echo inputDoubleSelect($arr1,$arr2);
		 
		 $arr1 = array(
		    "labelname" => "SC Online", 
		 	"fieldname" => "sc_status",
		 	"options" => status_arr()
		 );
		 $arr2 = array(
		    "labelname" => "Discontinue Status", 
		 	"fieldname" => "is_discontinue",
		 	"options" => status_arr()
		 ); 
		 echo inputDoubleSelect($arr1,$arr2);
		 
		 $arr1 = array(
		    "labelname" => "Sub SC Online", 
		 	"fieldname" => "sub_status",
		 	"options" => status_arr()
		 );
		 $arr2 = array(
		    "labelname" => "Shipping Charge", 
		 	"fieldname" => "is_charge_ship",
		 	"options" => status_arr()
		 ); 
		 echo inputDoubleSelect($arr1,$arr2);
		
		 $opt = "";
		 $opt .= "<option value=\"S\">Single</option>";
		 $opt .= "<option value=\"B\">Bundle</option>";
		 echo selectTwoOption("Type", "inv_type", $opt);
		 
		 
		 //status
		 echo selectFlagActive("Status", "status");
		 //status
		 echo selectFlagActive("Invoice", "bo_inv_status");
		 //status
		 echo selectFlagActive("Back Office", "bo_status");
		 //status
		 echo selectFlagActive("SC Online", "sc_status");
		 //status
		 echo selectFlagActive("Sub Online", "sub_status");
		 //status
		 echo selectFlagActive("MS Online", "ms_status");
		 //status
		 echo selectFlagActive("E-Commerce", "ecomm_status");
		 //status
		 echo selectFlagActive("Web Status", "web_status");
		 
		 //starterkit
		 echo selectFlagActive("Include Starterkit", "is_starterkit");
		 //status
		 echo selectFlagActive("Discontinue Status", "is_discontinue");
		 //status
		 echo selectFlagActive("Shipping Charge", "is_charge_ship");
		 //status
		 echo selectFlagActive("Physical Goods", "flag_is_do");
		 
		 //status
		 echo setSingleUploadFile("Image");
		  * 
		  */
		  
		 ?> 
		 <?php
		 //$input = "All.inputFormData('product/save', this.form.id)";
		 $input = "Product.saveInputProduct('product/save', this.form.id)";
		 $update = "All.updateFormDataEnctypeNoListReload('product/update', this.form.id, 'product/list')";
		 $view = "All.getListData('product/list')";
		 echo inputHidden("country_id", "ID");
		 echo inputHidden("hq_id", "BID06");
		 echo inputHidden("branch_id", "B001");
         echo button_set($input, $update, $view);
		 ?>
		 
      </div> 
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->	  
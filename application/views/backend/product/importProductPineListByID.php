<style>
    
	#formImportPrd select {
		height: 20px;
		padding:0px;
		margin-bottom: 0px;
	}
	
	#formImportPrd tr {
		line-height: 21px;
	}
</style>
<?php
	if(isset($err_msg)) {
		setErrorMessage($err_msg);
	} else {
		//print_r($result['header']);
		$yes_selected = "<select id=status name=status><option value=1 selected=selected>Yes</option><option value=0>No</option></select>";
		$no_selected = "<select id=status name=status><option value=1>Yes</option><option value=0 selected=selected>No</option></select>";
		
		$status = $no_selected;
		$web_status = $no_selected;
		$sc_status = $no_selected;
		$single = "";
		
		echo "<form id=formImportPrd><table width=100% class='table table-bordered'>";
			echo "<thead>";
			echo "<tr><th colspan=4>Data Product</th></tr>";
			echo "<tr><td width=15%>Product Code</td>";
			echo "<td width=35%>".$result['header'][0]->prdcd."</td>";
			echo "<td>Active Status</td>";
			if($result['header'][0]->status == 1) {
				$status = "<select id=web_status name=status><option value=0>Inactive</option><option value=1 selected=selected>Active</option></select>";
			} else {
				$status = "<select id=web_status name=status><option value=0 selected=selected>Inactive</option><option value=1>Active</option></select>";
			}
			echo "<td>$status</td>";
			
			echo "</tr>";
			
			echo "<tr>";
			echo "<td>Product Name</td>";
			echo "<td>".$result['header'][0]->prdnm."</td>";
			echo "<td>Web Status</td>";
			if($result['header'][0]->webstatus == 1) {
				$web_status = "<select id=web_status name=web_status><option value=0>Inactive</option><option value=1 selected=selected>Active</option></select>";
			} else {
				$web_status = "<select id=web_status name=web_status><option value=0 selected=selected>Inactive</option><option value=1>Active</option></select>";
			}
			echo "<td>$web_status</td>";
			
			echo "</tr>";
			
			echo "<tr>";
			echo "<td>Total BV</td>";
			echo "<td>".number_format($result['header'][0]->bv, 0, ".", ".")."</td>";
			echo "<td>SC status</td>";
			if($result['header'][0]->scstatus == 1) {
				$sc_status = "<select id=sc_status name=sc_status><option value=0>Inactive</option><option value=1 selected=selected>Active</option></select>";
			} else {
				$sc_status = "<select id=sc_status name=sc_status><option value=0 selected=selected>Inactive</option><option value=1>Active</option></select>";
			}
			echo "<td>$sc_status</td>";
			
			echo "</tr>";
			
			
			echo "<tr>";
			echo "<td width=15%>12W3 Dist Price</td>";
			echo "<td>".number_format($result['header'][0]->price_w, 0, ".", ".")."</td>";
			echo "<td>Single/Bundling</td>";
			if($result['detail'] != null) {
				$single = "<select id=inv_type name=inv_type><option value=S>Single</option><option value=B selected=selected>Bundling</option></select>";
			} else {
				$single = "<select id=inv_type name=inv_type><option value=S selected=selected>Single</option><option value=B>Bundling</option></select>";
			}
			echo "<td>$single</td>";
			
			echo "</tr>";
			
		    //echo "<tr><td colspan=2>&nbsp;</td><td>&nbsp;</td></tr>";
		    
			echo "<tr>";
			echo "<td>12E3 Dist Price</td>";
			echo "<td>".number_format($result['header'][0]->price_e, 0, ".", ".")."</td>";
			echo "<td>Category Product</td>";
			echo "<td>";
			echo "<select id=\"cat_id\" name=\"cat_id\">";
			foreach($result['category'] as $cat) {
			 echo "<option value=\"$cat->cat_id\">$cat->cat_desc</option>";
			}
			echo "</select>";
			echo "</td>";
			echo "</tr>";
			
			
			echo "<tr>";
			echo "<td>12W3 Cust Price</td>";
			echo "<td>".number_format($result['header'][0]->price_cw, 0, ".", ".")."</td>";
			
			echo "<td>Weight</td><td><input type=\"text\" id=\"weight\" name=\"weight\" /></td>";
			echo "</tr>";
			
			
			echo "<tr>";
			echo "<td>12E3 Cust Price</td>";
			echo "<td>".number_format($result['header'][0]->price_ce, 0, ".", ".")."</td>";
			echo "<td>&nbsp;</td>";
			echo "<td><input type=\"button\" class=\"btn btn-mini btn-primary\" value=\"Import to DB E-commerce\" onclick=\"Product.importPrdToEcommerce()\" /></td>";
			echo "</tr>";
			echo "<input type=hidden name=head_prdcd value=\"".$result['header'][0]->prdcd."\" />";
			echo "<input type=hidden name=head_prdnm value=\"".$result['header'][0]->prdnm."\" />";
			echo "<input type=hidden name=total_bv value=\"".$result['header'][0]->bv."\" />";
			echo "<input type=hidden name=total_dpw value=\"".$result['header'][0]->price_w."\" />";
			echo "<input type=hidden name=total_dpe value=\"".$result['header'][0]->price_e."\" />";
			echo "<input type=hidden name=total_cpw value=\"".$result['header'][0]->price_cw."\" />";
			echo "<input type=hidden name=total_cpe value=\"".$result['header'][0]->price_ce."\" />";
			echo "<input type=hidden name=category value=\"".$result['header'][0]->category."\" />";
		echo "</table>";	
			
		if($result['detail'] != null) {
			//echo "<tr></tr>";
			//echo "<tr></tr>";
			//echo "<tr></tr>";
			echo "<table width=100% class='table table-bordered'>";
			echo "<tr>";
			echo "<th width=5%>No</th>";
			echo "<th width=10%>Code</th>";
			echo "<th>Name</th>";
			echo "<th width=8%>Tot Qty</th>";
			echo "<th width=8%>Tot BV</th>";
			echo "<th width=15%>Total 12W3</th>";
			echo "<th width=15%>Total 12E3</th>";
			echo "</tr></thead>";
			echo "<tbody>";
			$i=1;
			$tot_qty = 0; $tot_bv = 0; $tot_price_w = 0;
			$tot_price_e = 0; $tot_price_cw = 0; $tot_price_ce = 0;
			
			foreach($result['detail'] as $dta) {
				echo "<tr>";
				echo "<td align=right>$i</td>";
				echo "<td align=center>$dta->prdcd<input type=hidden name=prdcd[] value=\"$dta->prdcd\" /></td>";
				echo "<td>$dta->prdnm<input type=hidden name=prdnm[] value=\"$dta->prdnm\" /></td>";
				echo "<td align=right>".number_format($dta->qty, 0, ".", ".")."</td>";
				echo "<td align=right>".number_format($dta->tot_bv, 0, ".", ".")."</td>";
				//echo "<td align=right>".number_format($dta->price_w, 0, ".", ".")."</td>";
				
				echo "<td align=right>".number_format($dta->tot_price_w, 0, ".", ".")."";
				echo "<td align=right>".number_format($dta->tot_price_e, 0, ".", ".")."";
				echo "<input type=hidden name=qty[] value=\"$dta->qty\" />";
				/*echo "<input type=hidden name=bv[] value=\"$dta->bv\" />";
				echo "<input type=hidden name=tot_bv[] value=\"$dta->tot_bv\" />";
				echo "<input type=hidden name=price_e[] value=\"$dta->price_e\" />";
				echo "<input type=hidden name=price_ce[] value=\"$dta->price_ce\" />";
				echo "<input type=hidden name=price_$dta->tot_bv[] value=\"$dta->price_e\" />";
				echo "<input type=hidden name=price_cw[] value=\"$dta->price_cw\" />";
				echo "<input type=hidden name=tot_price_w[] value=\"$dta->tot_price_w\" />";
				echo "<input type=hidden name=tot_price_cw[] value=\"$dta->tot_price_cw\" />";
				echo "<input type=hidden name=tot_price_e[] value=\"$dta->tot_price_e\" />";
				echo "<input type=hidden name=tot_price_ce[] value=\"$dta->tot_price_ce\" />";*/
				echo "</td></tr>";
				$i++;
				$tot_bv += $dta->tot_bv;
				$tot_price_w += $dta->tot_price_w;
				$tot_price_e += $dta->tot_price_e;
			}
	        echo "<tr><td colspan=4>T O T A L</td>";
			echo "<td align=right>".number_format($tot_bv, 0, ".", ".")."</td>";
			echo "<td align=right>".number_format($tot_price_w, 0, ".", ".")."</td>";
			echo "<td align=right>".number_format($tot_price_e, 0, ".", ".")."</td>";
			echo "</tr></tbody></table></form>";
		} else {
			echo "</thead></table></form>";
		}	
	}
?>


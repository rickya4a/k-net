<?php
if($listPrd == null) {
    echo emptyResultDiv();
} else {
    echo "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
    echo "<thead>
          <tr>
          <th colspan=6>List Product Stokist $search - $searchName</th>
          <input type=\"hidden\" class=\"TabOnEnter span6\" id=\"paramValue\" name=\"paramValue\" value=\"$search\"/>
          <input type=\"hidden\" class=\"TabOnEnter span6\" id=\"paramName\" name=\"paramName\" value=\"$searchName\"/>
          </tr>";
    echo "<tr bgcolor=#f4f4f4><th width=6%>No</th><th width=15%>ID</th><th>Description</th><th width=20%>Category</th><th width=10%>Status</th><th width=10%>Non Available</th></thead></tr>";
    echo "<tbody>";
    $i = 1;


    foreach($listPrd as $list) {
        $status_non= trim($list->flag_non_available);

        if($status_non != "KOSONG"){
            $status="ON";
            $tipe="success";
        }else{
            $status="OFF";
            $tipe="danger";
        }

        echo "<tr id=\"$i\">";
        echo "<td><div align=right>$i<div></td>";
        echo "<td><div align=center><input type=\"hidden\" id=\"cek_inv_id$i\"  name=\"cek_inv_id[]\" value=\"$list->cat_inv_id\"/>$list->cat_inv_id</div></td>";
        echo "<td><div align=center>$list->cat_inv_desc</div>
                    <input type=\"hidden\" id=\"butt_val$i\"  name=\"butt_val[]\" value=\"$status\"/></td>";
        echo "<td><div align=center>$list->cat_desc</div></td>";
        /*if($list->is_discontinue) {
            echo "<td><div align=center>Discontinue</div></td>";
        } else {
            echo "<td><div align=center>Ready</div></td>";
        }*/

        //$status_non= trim($list->flag_non_available);

        if($status_non != "KOSONG"){
            echo "<td id=status$i ><div align=center>Available</div></td>";
        }else{
            echo "<td id=status$i>
                    <div align=center style='color: #ff0a18'>Non Available</div>
                  </td>";
        }
        /*if($status_non == "1"){
            $status_non="OFF";
        }else{
            $status_non="ON";
        }*/
        /*$ondelete = "All.deleteFormData('product/delete/', '$list->cat_inv_id', 'product/list')";
        $arr = array(
            "update" => "Product.getUpdateProduct($i)",
            "delete" => $ondelete
        );
        echo btnUpdateDelete($arr);*/
        echo"<td id=\"but_inv_id$i\">
            <div align=center>
            <input type=\"button\" class=\"btn btn-$tipe\" id=\"but_inv_id\"  name=\"but_inv_id\" value=\"$status\" onclick=\"save_stokist($i)\"/>
            </div>
            </td>";

        echo "</tr>";
        $i++;
    }
    echo "</tbody></tr>";
    echo "</table>";
    ?>
    <script>
        $( document ).ready(function() {
            All.set_datatable();
        });
    </script>
    <script>
        function save_stokist(param) {
            var loccd = document.getElementById("paramValue").value;
            var prdcd = document.getElementById("cek_inv_id" + param).value;
            var button = document.getElementById("butt_val"+param).value;

            /*var div1= document.getElementById("statusA"+param).innerHTML;
            var div2= document.getElementById("statusB"+param).innerHTML;*/

            if(button == "ON" && loccd !=""){
                //alert(button);
                $.ajax({
                    type: "POST",
                    url: All.get_url('product/save_nonavailable'),
                    dataType: 'json',
                    data: {kd_stk:loccd,kd_brg:prdcd},
                    success: function (result) {

                        if (result.response == "true") {
                            alert("Data Berhasil Diubah Menjadi Non Available!");
                            $(All.get_active_tab() + " #status"+param).html("<div align=center style='color: #ff0a18'>Non Available</div>");
                            $(All.get_active_tab() + " #but_inv_id"+param).html("<div align=center><input type=\"button\" class=\"btn btn-danger\" id=\"but_inv_id\"  name=\"but_inv_id\" value=\"OFF\" onclick=\"save_stokist($i)\"/></div>");


                        }else{
                            alert("Data Gagal Diubah Menjadi Non Available!");
                        }
                    }

                });
            }else if(button == "OFF" && loccd !=""){

                //alert(button);
                $.ajax({
                    type: "POST",
                    url: All.get_url('product/del_nonavailable'),
                    dataType: 'json',
                    data: {kd_stk:loccd,kd_brg:prdcd},
                    success: function (result) {

                        if (result.response == "true") {
                            alert("Data Berhasil Diubah Menjadi Available!");
                            $(All.get_active_tab() + " #status"+param).html("<div align=center style='color: #000000'>Available</div>");
                            $(All.get_active_tab() + " #but_inv_id"+param).html("<div align=center><input type=\"button\" class=\"btn btn-success\" id=\"but_inv_id\"  name=\"but_inv_id\" value=\"ON\" onclick=\"save_stokist($i)\"/></div>");

                        }else{
                            alert("Data Gagal Diubah Menjadi Available!");
                        }
                    }

                });
                //alert("Data Akan Dihapus!");
            }else{
                alert("Silahkan Lengkapi Data ... ")
            }
        }

    </script>
    <?php
}
?>

<div class="mainForm">
  <form class="form-horizontal" enctype="multipart/form-data" id="formInputProductPrice">
    <fieldset>      
      <div class="control-group">       
         <?php
         //echo inputCountryHQBranch();
         echo monthYearPeriod();
         //pricecode ID
         $onchange = "onchange=Product.getPriceCodeByProdID(this.form)";
         $prdcd = array(
		 	"labelname" => "Product Code",
		 	"fieldname" => "cat_inv_id",
		 	"addClass" => "setReadOnly",
		 	"placeholder" => placeholderCheck(),
		 	"event" => $onchange
 		 );
         echo inputText($prdcd);
		 $prdnm = array(
		 	"labelname" => "Product Description",
		 	"fieldname" => "cat_inv_desc",
		 	"readonly" => "readonly"
 		 );
         echo inputText($prdnm);
		 echo "<div id=listPricePrd></div>"; 
		 
         $input = "Product.saveProductPrice('product/price/save', this.form.id)";
		 $update = "Product.saveProductPrice('product/price/update', this.form.id, 'product/price/list')";
		 $view = "All.getListData('product/price/list')";
		 echo inputHidden("country_id", "ID");
		 echo inputHidden("hq_id", "BID06");
		 echo inputHidden("branch_id", "B001");
         echo button_set_prdprice($input, $update, $view);
		 ?>    
        </div> <!-- end control-group -->
     </fieldset>
  </form>   
  <div class="result"></div>
</div><!--/end mainForm-->

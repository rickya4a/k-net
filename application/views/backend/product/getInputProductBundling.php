<style>
    .kanan 
    {
        text-align: right;
    }
    .form_title
    { 
      width: 20%;
      text-align: right;   
    }
    
    .form_title_left
    { 
      width: 20%;
      text-align: left;   
    }
</style>

<div class="mainForm"> 
  <form class="form-horizontal" id="formInputPrdBundling" name="formInputPrdBundling">
	<fieldset> 
       <div class="control-group">
        <table width="100%" border="0">
             <tr>
             	<td class="form_title">Product Bundling&nbsp;</td>
             	<td>
             		<select id="prdid" name="prdid" onchange="Product.getDetailPrdBundling(this.value)">
             			<option value="">--Select here--</option>
             		  <?php
             		    foreach($list as $dta) {
             		    	echo "<option value=\"$dta->prdcd\">$dta->prdnm</option>";
             		    }
             		  ?>
             		</select>
             	</td>
             	<td class="form_title">West Dist Price&nbsp;</td>
                <td>
                	<input type="text" class="span20 typeahead kanan" id="west_price"  name="west_price" onchange="Product.convertMoney(this, 'west_price_real')" />
                    <input type="hidden" class="span20 typeahead" id="west_price_real"  name="west_price_real" />
                </td>
             </tr>
	         <tr>
                <td class="form_title">Product Bundling Code&nbsp;</td>
                <td><input tabindex="1" type="text" class="span20 typeahead" id="cat_inv_id_parent"  name="cat_inv_id_parent" placeholder="Required" onchange="Product.getDetailPrdBundlingPost(this.value)" /></td>
                <td class="form_title">East Dist Price&nbsp;</td>
                <td>
                	<input type="text" class="span20 typeahead kanan" id="east_price"  name="east_price" onchange="Product.convertMoney(this, 'east_price_real')" />
                    <input type="hidden" class="span20 typeahead" id="east_price_real"  name="east_price_real" />
                </td>
             </tr>
             <tr>
                <td class="form_title">Description&nbsp;</td>
                <td><input tabindex="2" readonly="yes" type="text" class="span20 typeahead" id="cat_desc"  name="cat_desc" placeholder="Required"  /></td>
                <td class="form_title">West Cust Price&nbsp;</td>
                <td>
                    <input type="text" class="span20 typeahead kanan" id="cust_west_price"  name="cust_west_price" onchange="Product.convertMoney(this, 'cust_west_price_real')" />
                    <input type="hidden" class="span20 typeahead" id="cust_west_price_real"  name="cust_west_price_real" />
                </td>
             </tr>
             <tr>
                <td class="form_title">BV&nbsp;</td>
                <td>
                    <input type="text" class="span20 typeahead" id="bv" name="bv" onchange="Product.convertMoney(this, 'bv_real')" />
                    <input type="hidden" class="span20 typeahead" id="bv_real" name="bv_real"  />
                </td>
                <td class="form_title">East Cust Price&nbsp;</td>
                <td>
                    <input type="text" class="span20 typeahead kanan" id="cust_east_price"  name="cust_east_price" onchange="Product.convertMoney(this, 'cust_east_price_real')" />
                    <input type="hidden" class="span20 typeahead" id="cust_east_price_real"  name="cust_east_price_real" />  
                </td>	
             </tr>

            <tr>
                <td class="form_title">Weight&nbsp;</td>
                <td>
                    <input type="text" class="span20 typeahead" id="tot_all_weight" name="tot_all_weight" onchange="(this, 'tot_weight')" />
                    <input type="hidden" class="span20 typeahead" id="tot_all_weight_real" name="tot_all_weight_real"  />
                </td>
            </tr>
             
         </table>        
     
             <br />
             <!--<span id="show_form"></span> -->
             <table align="center" class="table table-striped table-bordered" width="100%">
               <thead>
                 <tr>
                   <th width="10%">Prod Code</th>
                   <th>Prod Name</th>
                   <th width="5%">Qty</th>
                   <th width="8%">BV</th>
                   <th width="8%">Total BV</th>
                   <th width="10%">12W Price</th>
                   <th width="10%">Weight</th>
                     <th width="10%">Total Weight</th>
                   <th width="10%">Total Price</th>
                   <!--<th width="5%">Act</th>-->
                 </tr>
               </thead>
               <tbody id="addData">
                
               </tbody>
               <tbody id="SS">
                  <tr><td colspan="2" align="right">T O T A L</td>
                    
                      <td>
                        <input readonly="yes" style="text-align:right;" type="text" class="span12 typeahead" id="tot_qty"  name="tot_qty" />
                        <input style="text-align:right;"  type="hidden" class="span12 typeahead" id="tot_qty_real" name="tot_qty_real" />
                        <input style="text-align:right;"  type="hidden" class="span12 typeahead" id="total_item" name="total_item" value="$amount" />
                      </td>

                      <td>&nbsp;</td>

                      <td>
                      	<input class="span14" style="text-align: right;" type="text" readonly="yes" id="total_bv"/>
                      	<input type="hidden" readonly="yes" id="total_bv_real"/>
                      </td>

                      <td>&nbsp;</td>

                       <!-- DISINI TOTALNYA -->
                      <td>&nbsp;</td>

                      <td>
                          <input class="span14" style="text-align: right;" type="text" readonly="yes" id="tot_weight"/>
                          <input type="hidden" readonly="yes" id="tot_weight_real"/>
                      </td>

                      <td>
                        <input class="span14" style="text-align: right;" type="text" readonly="yes" id="total_all_west"/>
                        <input class="span14" type="hidden" readonly="yes" id="total_all_west_real" />
                        <input class="span14" type="hidden" readonly="yes" id="total_all_east_real" />
                        <input class="span14" type="hidden" readonly="yes" id="total_all_cust_west_real" />
                        <input class="span14" type="hidden" readonly="yes" id="total_all_cust_east_real" />
                      </td>
                      <!--<td>&nbsp;</td>-->
                    </tr>
               
               <tr>
                <td colspan="7">
                  <input type="button" class="btn btn-warning" name="new_record" id="new_record" value="Add 1 row" onclick="Product.helper_add_new_row()" /> 
                  <input type="button" class="btn btn-primary" name="save" id="save" value="Save"  onclick="Product.post_input_product_grouping()" /> 
                  <!--<input type="button" disabled="disabled" class="btn btn-primary" name="save2" value="save to KlinkMLM"  id="save2" onclick="Product.post_input_product_groupingKlinkMlm()" />-->
                  <span id="ket"></span>
                  
                  <input type="hidden" name="amt_record" id="amt_record" value="0" />
                  <input type="hidden" name="amount" id="amount" value="1" />
                  <input type="hidden" name="tabidx" id="tabidx" value="6" />
                  <input type="hidden" name="db_comm" id="db_comm" value="" />
                  <input type="hidden" name="db_klink" id="db_klink" value="" />
                  
                  <input type="hidden" name="country_id" id="country_id" value="ID" />
                  <input type="hidden" name="hq_id" id="hq_id" value="BID06" />
                  <input type="hidden" name="branch_id" id="branch_id" value="B001" />
                </td>
               </tr>
              </tbody> 
             </table>
             <div id="forbutton" class="form-actions" style="display: none;">
            </div>

       </div> <!-- end control-group -->
    </fieldset>
   </form>
   <div class="result"></div> 
</div> <!--End mainForm -->
<script>
$(document).ready(function(){
     $(All.get_active_tab() + " #prdcdGroupCode").focus();  
});   
</script>  
            

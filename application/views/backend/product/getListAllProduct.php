<?php
  if($listPrd == null) {
  	 echo emptyResultDiv();
  } else {
  	 echo "<form id=listPrd><table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">";
       echo "<thead><tr><th colspan=6>List Product</th></tr>";
       echo "<tr bgcolor=#f4f4f4><th width=6%>No</th><th width=15%>ID</th><th>Description</th><th width=20%>Category</th><th width=10%>Status</th><th width=10%>Action</th></thead></tr>";
       echo "<tbody>";
       $i = 1;
	   
       foreach($listPrd as $list) {
                echo "<tr id=\"$i\">";
		        echo "<td><div align=right>$i<div></td>";
                echo "<td><div align=center><input type=\"hidden\" id=\"cat_inv_id$i\" value=\"$list->cat_inv_id\" />$list->cat_inv_id</div></td>";
                echo "<td><div align=center>$list->cat_inv_desc</div></td>";  
				echo "<td><div align=center>$list->cat_desc</div></td>";
                if($list->is_discontinue) {
                	echo "<td><div align=center>Discontinue</div></td>";
                } else {
					echo "<td><div align=center>Ready</div></td>";
                }
                $ondelete = "All.deleteFormData('product/delete/', '$list->cat_inv_id', 'product/list')";
                $arr = array(
				    "update" => "Product.getUpdateProduct($i)",
				    "delete" => $ondelete
				);
                echo btnUpdateDelete($arr);
                echo "</tr>";
              $i++; 
        }
    echo "</tbody></tr>";
    echo "</table></form>";
	?>
	<script>
      $( document ).ready(function() {
       All.set_datatable();
        });
    </script>
	<?php
  }
?>

<?php

    if($result == null){

        setErrorMessage();
    }else {
        ?>

        <form>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Distributor Code</th>
                    <th>Bonus Month</th>
                    <th>BonusYear</th>
                    <th>ExpireDate</th>
                    <th>voucherno</th>
                    <th>voucherkey</th>
                    <th>VoucherAmt</th>
                    <th>voucheramt_ori</th>
                    <th>CurrencyNote</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $i = 1;
                    foreach ($result as $dt) {
                        ?>
                        <td align="center">
                            <?php echo $dt->DistributorCode ?>
                        </td>
                        <td align="center">
                            <?php echo $dt->BonusMonth ?>
                        </td>
                        <td align="center">
                            <?php echo $dt->BonusYear ?>
                        </td>
                        <td>
                            <?php echo date("d-m-Y", strtotime($dt->ExpireDate))  ?>
                        </td>
                        <td align="center">
                            <?php echo $dt->voucherno ?>
                        </td>
                        <td align="center">
                            <?php echo $dt->voucherkey ?>
                            <input type="hidden" id="voucherkey<?php echo $i; ?>" value="<?php echo $dt->voucherkey; ?>"/>
                        </td>
                        <td align="center">
                            <?php echo $dt->VoucherAmt ?>
                            <input type="hidden" id="voucheramt<?php echo $i; ?>" value="<?php echo $dt->VoucherAmt; ?>"/>
                        </td>
                        <td align="center">
                            <?php echo $dt->voucheramt_ori ?>
                        </td>
                        <td align="center">
                            <?php echo $dt->CurrencyNote ?>
                        </td>
                        <td>
                            <?php
                            echo "<a onclick=\"updateVoucher($i)\" class='btn btn-success btn-sm'>update</a>";
                            ?>

                        </td>
                        <?php
                        $i++;
                    }
                    ?>
                </tr>
                </tbody>
            </table>
        </form>

        <script>
            function updateVoucher(param){

                var voucherkey = $("#voucherkey" +param).val()
                var voucheramt = $("#voucheramt" +param).val()

                $.ajax({
                    url: All.get_url("be/update/voucher"),
                    type: 'POST',
                    data: {voucherkey: voucherkey, voucheramt: voucheramt},
//                    success:
//                        function(data){
//
//                            $("#notif").html(data);
//                        }
                });
            }
        </script>
        <?php
    }
        ?>
<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <!-- Custom Theme files -->
  <link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" />
  <!-- Custom Theme files -->
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!--webfont-->

  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>

  <script src="<?php echo base_url() ?>asset/js_module/global.js"></script>
  <script src="<?php echo base_url() ?>assets/module_js/shopping.js"></script>
  <script src="<?php echo base_url() ?>assets/module_js/shopping_cust.js"></script>
  <script src="<?php echo base_url() ?>assets/module_js/sales.js"></script>
  <script src="<?php echo base_url() ?>assets/module_js/umroh.js"></script>
  <script src="<?php echo base_url() ?>assets/module_js/promo.js"></script>

  <!--data tables (cahyono)-->
  <!-- Datatables -->
  <link href="<?php echo site_url();?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo site_url();?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo site_url();?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo site_url();?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo site_url();?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <script src="<?php echo site_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="<?php echo site_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>


  <!-- payment-->
  <script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery-1.4.3.min.js"></script>
  <!--<script type="text/javascript" src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.js"></script>-->
  <script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery.fancybox.pack.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/' ?>jquery.fancybox-1.3.4.css"/>
  <!--Google Analytic-->


  <!--<script type="text/javascript" src="https://secure.sgo.co.id/public/signature/js"></script>-->

  <!--slider-->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.flexisel.js"></script>

  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/customs.js"></script>
<style>
  @import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300);

  * {
  box-sizing: border-box;
}

html, body {
  background: radial-gradient(circle at top center, #333333 0%, #111111 100%);
  overflow: hidden;
  font: 300 30px/1 'Open Sans Condensed', sans-serif;
  text-align: center;
  text-transform: uppercase;
  color: #fff;
}


#myInput {
  /*background-image: url('/css/searchicon.png');*/
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  /*font-size: 16px;*/
  padding: 12px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  text-transform: uppercase;
  color: #fff;

}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #000000;
}
</style>
</head>
<body>

<h2 class="w3-center">PENGUMUMAN PEMENANG UNDIAN ANNIVERSARY</h2>

<!--<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Temukan Nomor Anda!" title="Temukan Nomor Anda">-->

<div class="row">
  <div class="col-sm-1"></div>
  <div class="col-sm-10">

    <table width="90%" class="main table table-hover" id="box-table-b">
   <thead>
      <tr >
        <th width="50%" style="    left: 50%;
    margin-right: -50%;">Nomor</th>
        <th width="50%" style="    left: 50%;
    margin-right: -50%;">Hadiah</th>
      </tr>
   </thead>

      <tbody>

      <?php
      foreach ($pemenang as $dta) {
        echo " <tr>
        <td>$dta->NOUNDIAN</td>
        <td>$dta->namahadiah</td>
        </tr>";

      }

      ?>

      </tbody>

    </table>

  </div>
  <div class="col-sm-1"></div>
</div>

<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>

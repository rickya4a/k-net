<?php
if(empty($result)) {
    setErrorMessage("No result found, try another date..");
} else {
    ?>
    <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
        <thead>
        <tr>
            <th colspan="8">Submit DTC</th>
        </tr>
        <tr>
            <th width="5%">No.</th>
            <th width="10%">Period</th>
            <th width="15%">Paket Ladies</th>
            <th width="15%">Paket Umum</th>
            <th width="10%">TBV</th>
            <th width="10%">Voucher</th>
            <th width="10%">Status</th>
            <th width="10%">Action</th>

        </tr>
        </thead>

    </table>

    <?php
}
?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $(All.get_active_tab() + " .datatable").dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
            "sPaginationType": "bootstrap",
            "oLanguage": {
            },
            "bDestroy": true
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    });

</script>
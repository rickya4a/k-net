<!--<head>-->
<!--  <meta charset="utf-8"/>-->
<!--  <title>K-LINK Back End Apps</title>-->
<!--  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
<!--  <meta name="description" content="k-link, responsive, HTML5, K-System Online"/>-->
<!--  <meta name="author" content="k-link"/>-->
<!---->
<!--  <!-- The styles -->
<!--  -->
<!-- -->
<!--  <link href="--><?php //echo base_url('asset/css/bootstrap.min.css')?><!--" rel="stylesheet"/>-->
<!--  <script type="text/javascript" src="--><?php //echo base_url('assets/js/jquery.min.js')?><!--"></script>-->
<!--  <script src="--><?php //echo base_url('assets/js/bootstrap.min.js')?><!--"></script>-->

<!--<script type="text/javascript" src="http://www.k-net.co.id/assets/js/jquery-1.11.1.min.js"></script>-->
<!--<script type="text/javascript" src="http://www.k-net.co.id/assets/js/jquery-1.4.3.min.js"></script>-->
<!--<script type="text/javascript" src="https://sandbox-kit.espay.id/public/signature/js"></script>-->
<!---->

<script type="text/javascript">

	//	function setSelectPay() {
	//		var x = $("#bank").val();
	//		var bankDetail = $("#bank option:selected").text();
	//		//alert("isi : " +x);
	//		//var bankDesc = $("#bank option:selected").text();
	//		var str = x.split("|");
	//		$("#bankid").val(str[0]);
	//		$("#bankCode").val(str[1]);
	//		//$("#bankDesc").val(bankDesc);
	//		$("#bankDesc").val(str[4]);
	//		$("#charge_connectivity").val(str[2]);
	//		$("#charge_admin").val(str[3]);
	//		$("#bankDescDetail").val(bankDetail);
	//
	//
	//	}
	function setSelectPay() {
		var x = $("#bank").val();
		var bankDetail = $("#bank option:selected").text();
		//alert("isi : " +x);
		//var bankDesc = $("#bank option:selected").text();
		var str = x.split("|");
		var bankid = str[0];
		$("#bankid").val(bankid);
		$("#bankCode").val(str[1]);
		//$("#bankDesc").val(bankDesc);
		$("#bankDesc").val(str[4]);
		$("#charge_connectivity").val(str[2]);
		$("#charge_admin").val(str[3]);
		$("#bankDescDetail").val(bankDetail);

		if(bankid === "25") {
			$(".sal_va").css('display', 'block');
			var idmember = "<?php echo getUserID(); ?>";
			var password = "<?php echo getUserPassword(); ?>";

			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('va/balance/'.getUserID());?>",
				dataType : 'json',
				success: function(data){
					$("#saldo_va").val(data.arrayData[0].amount);

				}
			});
		} else {
			$("#saldo_va").val(null);
			$(".sal_va").css('display', 'none');
		}


	}

	function getDataEvent(){
		if($('#opsi').val() != '')
		{
			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('be_dtc/getDetailTiket');?>",
				dataType : 'json',
				data : {opsi:$('#opsi').val()},
				success: function(data){
					$("#price").val(data.price_online);
					$("#stok").val(data.max_online);
					$("#note").val(data.early_note);
					$("#tglearly").val(data.earlybird_date);
					$("#tglnow").val(data.tglnow);
					document.getElementById('note').innerHTML = data.early_note;
				}
			});
		}

	}



	function DataKeTable(){
		var acara = $('#opsi option:selected').text();
		var x = document.getElementById("idmbr");
		var qty =parseInt( $('#qty').val());
		var stok=parseInt ($('#stok').val());
		var qtytot =parseInt( $('#qtytot').val());
		var harga =parseInt( $('#price').val());
		var duit=parseInt( $('#duit').val());
		var i =0;
		var counter=0;
		for(i=0; i< $('.main tr').length;i++ ){
			var nama =$("tr#"+i).find('input').val();

			if($('#idmbr').val() == nama ){
				counter++;
			}
		}
		if(counter>0){
			alert('Peserta hanya boleh mendaftar satu kali')
		}
		else
		{
			if(stok<qty){
				alert('Pemesanan tiket melebihi stok')
			}
			else
			{
				if(($('#idmbr').val()!= '')&&($('#qty').val()!= '')&&($('#email').val()!= '')&&($('#display_info').val()!= 'ID Member tidak valid/Sudah Terdaftar') ){
					var rowCount = $('.main tr').length-1;
					var no = rowCount + 1;

					var num2 = parseFloat(Math.round(($('#price').val()*$('#qty').val()) * 100) / 100);

					$("<tr data-status=\"I\" id='"+no+"' >"
						+ "<td><input type=\"hidden\" name=\"idpendaftar[]\" id=\"idpendaftar\" value=\""+$('#idmbr').val()+"\" /><input type=\"hidden\" name=\"jumlah[]\" id=\"jumlah\" readonly value=\""+$('#qty').val()+"\"  />"+$('#idmbr').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"hidden\" name=\"nmpendaftar[]\" id=\"nmpendaftar\" value=\""+$('#display_info').val()+"\" />"+$('#display_info').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"emailrow[]\" id=\"emailrow\" value=\""+$('#email').val()+"\" />"+$('#email').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"notelprow[]\" id=\"notelprow\" value=\""+$('#notelp').val()+"\" />"+$('#notelp').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"event_db[]\" id=\"event_db\" value=\""+$('#opsi').val()+"\" /><input type=\"hidden\" name=\"event_nm[]\" id=\"event_nm\" value=\""+acara+"\" />"+acara+"</td>"
						+ "<td>"+$('#qty').val()+"</td>"
						+ "<td><input type=\"hidden\" name=\"harga[]\" id=\"harga\" value=\""+$('#price').val()+"\"  />"+$('#price').val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
						+ "<td>"+(num2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
						+ "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\"  onclick=\"DelTable('"+no+"')\"   /></td>"
						+" </tr>").appendTo(".main tbody");
					//       x.remove(x.selectedIndex);

					$('#opsi').attr("disabled", true);
					$("#stok").val( ($('#stok').val()) -qty);
					$("#qtytot").val( qtytot +qty);
					$("#duit").val( duit +(qty*harga));


				}else{
					alert('Data harus diisi');
				}
			}

		}







	}

	function loaddata()
	{
		var idnya=document.getElementById( "idmbr" ).value;
		var acara=document.getElementById( "opsi" ).value;

		if (idnya)
		{
			//$( '#display_info' ).html("Isinya : "+idnya);

			$.ajax({
				//			alert('coba ajax');
				type : "POST",
				url  : "<?php echo base_url();?>dtc/cariID2",
				data : {idm: idnya, acara: acara},
				dataType: "json",

				success : function(result)
				{
					if(result[0].fullnm!=null)
//	                	$( '#display_info' ).html(result[0].fullnm + " [" +result[0].rank+ "]");
						$("#display_info").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
//						document.getElementById("#display_info").value = result[0].fullnm + " [" +result[0].rank+ "]";
					else
//						$( '#display_info' ).html("ID Member tidak valid");
						$("#display_info").val( "ID Member tidak valid/Sudah Terdaftar");
//
//						document.getElementById("#display_info").value ="ID Member tidak valid";

				}
			});

		} else
		{
			$( '#display_info' ).html("Please Enter Some Words");
		}
	}

	function DelTable(no){

//		alert(no);
//		var tr = $("tr#"+no).val;
		var harga =parseInt( $('#price').val());
		var duit=parseInt( $('#duit').val());

		var stok=parseInt ($('#stok').val());
		var qty =parseInt($("tr#"+no).find('input').val() );
		var row = $("tr#"+no).find("input[id=row]").val();
		$("#stok").val( stok+1 );
		$("#duit").val( duit -(1*harga));

		var rowCount = $('.main tr').length;

		$("tr#"+no).remove();
	}








</script>


<!--<script>-->
<!--	$(document).ready(function(){-->
<!---->
<!---->
<!--			$("table.main").on("click", "#ibtnDel", function (event) {-->
<!---->
<!--					var tr = $(this).closest('tr');-->
<!---->
<!--					var stok=parseInt ($('#stok').val());-->
<!--					var qty =parseInt(tr.find('input').val() );-->
<!---->
<!--					var row = tr.find("input[id=row]").val();-->
<!--					$("#stok").val( stok+qty );-->
<!---->
<!--					var rowCount = $('.main tr').length-1;-->
<!---->
<!--					$(this).closest("tr").remove();-->
<!--				}-->
<!---->
<!--			);-->
<!---->
<!---->
<!---->
<!---->
<!--		}-->
<!--	);-->
<!--</script>-->
<!--</head>-->
<!--<script type="text/javascript" src="https://sandbox-kit.espay.id/public/signature/js"></script>-->

<div class="mainForm">

	<div class="row vpullset4" style="min-height: 410px;">

		<div class="container-fluid">
			<div class="row-fluid">
				<form class="form-horizontal" action="<?php echo $action; ?>" method="post" id="frmListReg" name="frmListReg">
					<!--			  <div class="form-group">-->
					<!--			    <label class="control-label col-sm-2" >&nbsp;</label>-->
					<!--			    <div class="col-sm-6">-->
					<!--			      <h2 align="center">Entry Data Peserta DTC</h2>-->
					<!--			    </div>-->
					<!--			  </div>-->

					<div class="col-lg-12 col-xs-12 cart_header">
						<div class="col-lg-12">
							<!--<h1>Menu Ini Sedang Dalam Tahap Pengembangan</h1>-->
							<h2>Entry Data Peserta DTC (DEV)</h2>
						</div>
					</div>
					<div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">


						<div class="form-group">
							<label class="control-label col-sm-4" >Pilihan:</label>
							<div class="col-sm-4">
								<select class="form-control" data-live-search="true" id="opsi" name="opsi" onchange="getDataEvent()">
									<option></option>
									<?php
									foreach ($alamat as $kota) {

										echo "<option value=\"$kota->id\">$kota->nama - $kota->lokasi ($kota->event_date)</option>";
									}
									?>
									<!--				  <option value="">Hot Dog, Fries and a Soda</option>
                                                      <option>Burger, Shake and a Smile</option>
                                                      <option>Sugar, Spice and all things nice</option>
                                    -->
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >ID Member</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="idmbr" placeholder="Enter id" onchange="loaddata()">
								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
							<!--			    <div class="col-sm-4" id="display_info"></div>-->
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Nama Member</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="display_info"  readonly >
								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Email</label>
							<div class="col-sm-3">
								<input type="email" class="form-control" id="email" name="email"  >
								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >No. HP</label>
							<div class="col-sm-3">
								<input type="email" class="form-control" id="notelp" name="notelp"  >
								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Harga</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="price" name="price" readonly style="width: 300px;">
								<!--		      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
							<div class="col-sm-3">
								<label class="control-label" id="note"></label>
								<!--
								<input type="text" class="form-control" id="note" name="note" readonly style="width: 100px;">
								<input type="text" class="form-control" id="tglearly" name="tglearly" readonly style="width: 100px;">
								<input type="text" class="form-control" id="tglnow" name="tglnow" readonly style="width: 100px;">
								-->
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" >Tiket Tersedia</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="stok" name="stok" readonly style="width: 500px;">
								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
						</div>


						<input type="hidden" class="form-control" id="qty" value="1"  style="width: 500px;">
						<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->

						<!--
                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                              <label><input type="checkbox"> Remember me</label>
                            </div>
                          </div>
                        </div>
                        -->
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a class="btn btn-default" id="addItemxd" onclick="DataKeTable()"> Tambahkan </a>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-10">

								<table width="90%" class="main table table-hover" id="box-table-b">
									<thead>
									<tr>
										<th >ID Member</th>
										<th >Nama Member</th>
										<th >Email</th>
										<th>No Telp</th>
										<th >Event</th>

										<th >Quantity </th>
										<th >Harga</th>
										<th >Total amount</th>

										<th >Action</th>
									</tr>
									</thead>
									<tbody>
									</tbody>

								</table>

							</div>
							<div class="col-sm-1"></div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" >Total Bayar</label>
							<div class="col-sm-3">
								<!--								<input type="hidden" class="form-control" id="duit" value="0" readonly name="duit" style="width: 500px;">-->
								<input type="text" class="form-control" id="duit" value="0" readonly name="duit" style="width: 500px;">

								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4" >Bank:</label>
							<div class="col-sm-4">
								<select class="form-control"  id="bank" name="bank" required onchange="setSelectPay()">
									<?php
									echo "<option value=\"\">--Pilih disini--</option>";

									foreach($listBank as $dta) {
										echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
									}
									?>
								</select>
							</div>

						</div>
						<div class="form-group">
							<label style="display: none;" class="sal_va control-label col-sm-4">Saldo Virtual Account</label>
							<div style="display: none;" class="sal_va col-sm-3" >
								<input type="text" class="form-control" readonly="readonly" style="width: 400px" id="saldo_va" name="saldo_va"/>
							</div>
						</div>
						<input type="hidden" name="totalx" value="" />
						<input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
						<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
						<input type="hidden" name="bankid" id="bankid" value=""  />
						<input type="hidden" name="bankCode" id="bankCode" value=""  />
						<input type="hidden" name="bankDesc" id="bankDesc" value=""  />
						<input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
						<input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
						<input type="hidden" name="charge_admin" id="charge_admin" value=""  />



						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="button" class="btn btn-default" name="Input"  id="save" onclick="simpandata()">Pembayaran</button>
								<!--								<!--		      <button type="submit" class="btn btn-default" name="Edit" value="Edit" id="edit">Edit</button> -->
								<!--								<!--		      <button type="submit" class="btn btn-default" name="Delete" value="Delete" id="delete">Delete</button> -->
								<!--								<button type="reset" class="btn btn-default" name="Clear" value="Clear" id="clear">Reset</button>-->

								<!--								<input tabindex="3" type="submit" id="btn_input_user" class="btn btn-primary " name="save" value="Submit" />-->

							</div>
						</div>

					</div>
				</form>
				<!--
                            <label for="submit" class="labelfrm">&nbsp;</label>
                            <input type="submit" name="Input" value="Input" id="input"/>
                            <input type="submit" name="Edit" value="Edit" id="edit"/>
                            <input type="submit" name="Delete" value="Delete" id="delete"/>
                            <input type="reset" name="Clear" value="Clear" id="clear"/>
                -->
				<!--		<div class="col-sm-offset-4 col-sm-8">-->
				<!--			<table class="table table-striped table-condensed">-->
				<!--		    <thead>-->
				<!--		      <tr>-->
				<!--		      	<th>No</th>-->
				<!--		      	<th>NoPeserta</th>-->
				<!--		        <th>ID</th>-->
				<!--		        <th>Nama</th>-->
				<!--		        <th>Rank</th>-->
				<!--		        <th>Acara</th>-->
				<!--		      </tr>-->
				<!--		    </thead>-->
				<!--		    <tbody>-->
				<!--		      --><?php
				//		          $i=1;
				//				  foreach ($isiTabel as $vIsi) {
				//					  echo "<tr> ";
				//					  echo "<td>$i</td>";
				//					  echo "<td> $vIsi->noPeserta </td>";
				//					  echo "<td> $vIsi->dfno </td>";
				//					  echo "<td> $vIsi->fullnm </td>";
				//					  echo "<td> $vIsi->peringkat </td>";
				//					  echo "<td> $vIsi->lokasi </td>";
				//					  echo "</tr>";
				//					  $i++;
				//				  }
				//			  ?><!--	-->
				<!--		    </tbody>-->
				<!--		  </table>-->
				<!--		</div>	-->
			</div><!--/fluid-row-->
		</div>
	</div>

</div>

<div>
	<iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
</div>


<script>

	function simpandata(){
		var bank= ($('#bankid').val());
		if(bank==''){
			alert("Mohon lengkapi data");
		}
		else if (bank=='25'){
			document.getElementById("save").disabled = true;

			$.post(All.get_url('dtcdev/submit') , $("#frmListReg").serialize(), function(data)
				{
					if(data.response == 'true')
					{
//			alert('masuk');
						var payment=data.arrayData;
						var orderno=payment[0].orderno;
						var total_pay=payment[0].total_pay;

						$.ajax({
							//url: All.get_url('sgo_ppob/saveTemp'),
							url: All.get_url('dtc/submit/va'),
							type: 'POST',
							data: {
								memberid : "<?php echo getUserID();?>",
								paymentId : orderno,
								paymentAmount : total_pay,
							},
							dataType: 'json',
							success:
								function(data){
									$("input[type=button]").removeAttr('disabled');
									if(data.response == 'true') {
//										alert("Transaksi Berhasil..");
//										resetField();
//										var arrayData = data.arrayData;
//										var xhtml = "<table border=0 width=50%>";
//										xhtml += "<tr><td width=25%>No Trx</td><td>:</td><td>"+arrayData[0].trx_id+"</td></tr>";
//										xhtml += "<tr><td>Nominal</td><td>:</td><td>"+arrayData[0].nominal+"</td></tr>";
//										xhtml += "<tr><td>Reff No</td><td>:</td><td>"+arrayData[0].param_data.reff_id+"</td></tr>";
//										xhtml += "<table>";
//										//$(".result").html(null);
//										$(".result").html(xhtml);
										window.location = "http://www.k-net.co.id/dtc/done/"+orderno;

									} else {
										alert(data.message);
									}
								},
							error: function (xhr, ajaxOptions, thrownError) {
								alert(thrownError + ':' +xhr.status);
								$("input[type=button]").removeAttr('disabled');
							}
						});



					}
					else
					{
//			alert('keluar');
					}


				},"json"

			).fail(function() {
					alert("Error requesting page");
					All.set_enable_button();
				});
		}
		else
		{
			document.getElementById("save").disabled = true;

			$.post(All.get_url('dtcdev/submit') , $("#frmListReg").serialize(), function(data)
				{
					if(data.response == 'true')
					{
//			alert('masuk');

						var payment=data.arrayData;
						var orderno=payment[0].orderno;
						var total_pay=payment[0].total_pay;

						var data = {
								key : "0df5835ee198d49944c372ead860c241",
								paymentId : orderno,
								paymentAmount : total_pay,
								backUrl : "http://www.k-net.co.id/dtc/done/"+orderno,
								bankCode : $('#bankCode').val(),
								bankProduct: $('#bankDesc').val()
							},
							sgoPlusIframe = document.getElementById("sgoplus-iframe");

						if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
						SGOSignature.receiveForm();



					}
					else
					{
//			alert('keluar');
					}


				},"json"

			).fail(function() {
					alert("Error requesting page");
					All.set_enable_button();
				});
		}


	}


	//key development
	//$this->sgokey = "0df5835ee198d49944c372ead860c241";
	//key production
	//							$this->sgokey = "51edf5e8117da341a8be702d9bc18de5";
</script>
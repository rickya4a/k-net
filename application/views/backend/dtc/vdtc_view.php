<!--<head>-->
<!--  <meta charset="utf-8"/>-->
<!--  <title>K-LINK Back End Apps</title>-->
<!--  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
<!--  <meta name="description" content="k-link, responsive, HTML5, K-System Online"/>-->
<!--  <meta name="author" content="k-link"/>-->
<!---->
<!--  <!-- The styles -->
<!--  -->
<!-- -->
<!--  <link href="--><?php //echo base_url('asset/css/bootstrap.min.css')?><!--" rel="stylesheet"/>-->
<!--  <script type="text/javascript" src="--><?php //echo base_url('assets/js/jquery.min.js')?><!--"></script>-->
<!--  <script src="--><?php //echo base_url('assets/js/bootstrap.min.js')?><!--"></script>-->

<script type="text/javascript" src="http://www.k-net.co.id/assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://www.k-net.co.id/assets/js/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="https://sandbox-kit.espay.id/public/signature/js"></script>



<div class="mainForm">

	<div class="row vpullset4" style="min-height: 410px;">

		<div class="container-fluid">
			<div class="row-fluid">
				<form class="form-horizontal"  method="post" id="frmListReg" name="frmListReg">
					<!--			  <div class="form-group">-->
					<!--			    <label class="control-label col-sm-2" >&nbsp;</label>-->
					<!--			    <div class="col-sm-6">-->
					<!--			      <h2 align="center">Entry Data Peserta DTC</h2>-->
					<!--			    </div>-->
					<!--			  </div>-->

					<div class="col-lg-12 col-xs-12 cart_header">
						<div class="col-lg-12">
							<!--<h1>Menu Ini Sedang Dalam Tahap Pengembangan</h1>-->
							<h2>Daftar Tiket </h2>
						</div>
					</div>
					<div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">

						<div class="col-lg-12 text-center">
							<!--<h1>Menu Ini Sedang Dalam Tahap Pengembangan</h1>-->
							<h2 style="centered">Harap mencatat nomor tiket anda untuk masuk ke dalam acara</h2>
						</div>

						<br>
						<br>
						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-10">
								<?php
								if($tabelValid!=null)
								{
								?>
								<table width="90%" class="main table table-hover" id="box-table-b">
									<thead>
									<tr >
										<th colspan="5">Tiket Milik Anda</th>
									</tr>
									<tr>
										<th >ID Member</th>
										<th >Nama Member</th>
										<th >Email Member</th>
										<th >Event</th>
										<th >Nomor tiket</th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach($tabelValid as $detail){
										echo
											"<tr><td>".$detail->valid_dfno."</td>"
											. "<td>".$detail->valid_fullnm."</td>"
											. "<td>".$detail->email."</td>"

											. "<td>".$detail->nama." - ".$detail->lokasi." ($detail->event_date)</td>"
											. '<td><a href="'.site_url('vtiket2/'.$detail->notiket).'" target="_blank">'.$detail->notiket.'</a></td>
											</tr>';
									}

									?>

									</tbody>

								</table>
								<?php
								}
								else
								echo "Data tidak ditemukan";
								?>
							</div>
							<div class="col-sm-1"></div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-10">
								<?php
								if($tabelBuyer!=null)
								{
								?>
								<table width="90%" class="main table table-hover" id="box-table-b">
									<thead>
									<tr >
										<th colspan="5">Tiket Lainnya</th>
									</tr>
									<tr>
										<th >ID Member</th>
										<th >Nama Member</th>
										<th >Email Member</th>
										<th >Event</th>
										<th >Nomor tiket</th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach($tabelBuyer as $detail){
										echo
											"<tr>
											<td>".$detail->valid_dfno."</td>"
											. "<td>".$detail->valid_fullnm."</td>"
											. "<td>".$detail->email."</td>"

											. "<td>".$detail->nama." - ".$detail->lokasi." ($detail->event_date)</td>"
											. '<td><a href="'.site_url('vtiket2/'.$detail->notiket).'" target="_blank">'.$detail->notiket.'</a></td>
											</tr>';
									}

									?>

									</tbody>

								</table>
									<?php
								}
								else
									echo "Data tidak ditemukan";
								?>
							</div>
							<div class="col-sm-1"></div>
						</div>


						<!--						<div class="form-group">-->
						<!--							<label class="control-label col-sm-4" >Total Bayar</label>-->
						<!--							<div class="col-sm-3">-->
						<!--								<!--								<input type="hidden" class="form-control" id="duit" value="0" readonly name="duit" style="width: 500px;">-->
						<!--								<input type="text" class="form-control" id="duit" value="0" readonly name="duit" style="width: 500px;">-->
						<!---->
						<!--								<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
						<!--							</div>-->
						<!--						</div>-->
						<!--						<div class="form-group">-->
						<!--							<label class="control-label col-sm-4" >Bank:</label>-->
						<!--							<div class="col-sm-4">-->
						<!--								<select id="bank" name="bank" required onchange="setSelectPay()">-->
						<!--									--><?php
						//									echo "<option value=\"\">--Pilih disini--</option>";
						//
						//									foreach($listBank as $dta) {
						//										echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
						//									}
						//									?>
						<!--								</select>-->
						<!--							</div>-->
						<!--						</div>-->
						<!---->
						<!--						<input type="hidden" name="totalx" value="" />-->
						<!--						<input type="hidden" name="temp_orderidx" value="--><?php //echo $key;?><!--" />-->
						<!--						<input type="hidden" name="temp_paymentIdx" value="--><?php //echo $payID;?><!--" />-->
						<!--						<input type="hidden" name="bankid" id="bankid" value=""  />-->
						<!--						<input type="hidden" name="bankCode" id="bankCode" value=""  />-->
						<!--						<input type="hidden" name="bankDesc" id="bankDesc" value=""  />-->
						<!--						<input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />-->
						<!--						<input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />-->
						<!--						<input type="hidden" name="charge_admin" id="charge_admin" value=""  />-->
						<!---->
						<!---->
						<!---->
						<!--						<div class="form-group">-->
						<!--							<div class="col-sm-12 text-center">-->
						<!--								<a href="http://www.k-net.co.id/dtc/done/--><?php //echo $orderno;?><!--" class="btn btn-default" >Batalkan</a>-->
						<!---->
						<!--								<button type="button" class="btn btn-default" name="lanjutkan"  id="lanjutkan" onclick="submitdataXX()">Lanjutkan</button>-->
						<!---->
						<!--							</div>-->
						<!--						</div>-->

					</div>
				</form>
				<!--
                            <label for="submit" class="labelfrm">&nbsp;</label>
                            <input type="submit" name="Input" value="Input" id="input"/>
                            <input type="submit" name="Edit" value="Edit" id="edit"/>
                            <input type="submit" name="Delete" value="Delete" id="delete"/>
                            <input type="reset" name="Clear" value="Clear" id="clear"/>
                -->
				<!--		<div class="col-sm-offset-4 col-sm-8">-->
				<!--			<table class="table table-striped table-condensed">-->
				<!--		    <thead>-->
				<!--		      <tr>-->
				<!--		      	<th>No</th>-->
				<!--		      	<th>NoPeserta</th>-->
				<!--		        <th>ID</th>-->
				<!--		        <th>Nama</th>-->
				<!--		        <th>Rank</th>-->
				<!--		        <th>Acara</th>-->
				<!--		      </tr>-->
				<!--		    </thead>-->
				<!--		    <tbody>-->
				<!--		      --><?php
				//		          $i=1;
				//				  foreach ($isiTabel as $vIsi) {
				//					  echo "<tr> ";
				//					  echo "<td>$i</td>";
				//					  echo "<td> $vIsi->noPeserta </td>";
				//					  echo "<td> $vIsi->dfno </td>";
				//					  echo "<td> $vIsi->fullnm </td>";
				//					  echo "<td> $vIsi->peringkat </td>";
				//					  echo "<td> $vIsi->lokasi </td>";
				//					  echo "</tr>";
				//					  $i++;
				//				  }
				//			  ?><!--	-->
				<!--		    </tbody>-->
				<!--		  </table>-->
				<!--		</div>	-->
			</div><!--/fluid-row-->
		</div>
	</div>

</div>

<div>
	<iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
</div>


<script>

	function submitdataXX() {
		document.getElementById("batalkan").disabled = true;
		document.getElementById("lanjutkan").disabled = true;

		var total_all = $("#total_all").val();

		var data = {
				key : "0df5835ee198d49944c372ead860c241",
				paymentId : "<?php echo $orderno;?>",
				paymentAmount : <?php echo $total_pay; ?>,
				backUrl : "http://www.k-net.co.id/dtc/done/"+"<?php echo $orderno;?>",
				bankCode : "<?php echo $bankCode; ?>",
				bankProduct: "<?php echo $bankDesc; ?>"
			},
			sgoPlusIframe = document.getElementById("sgoplus-iframe");

		if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
		SGOSignature.receiveForm();


	}



</script>
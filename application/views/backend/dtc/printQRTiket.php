<?php
$pdf=new FPDF('P','mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)
$lebarCell = 4;

$pdf->AddPage();
//$pdf->Cell(25,25);
$pdf->Image($qr,150,5, 50, 50);
{
    //        $pdf->Cell(198,5,"BONUS STATEMENT REPORT $bulan $dta2->bonusyear",1,0,'C', true);
    $pdf->SetFont('Arial', 'B', 20);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetXY(6, 10);
    foreach ($data as $dxd){
        $pdf->Cell(30, 9, "TIKET DTC ".$dxd->lokasi." ($dxd->event_date )", 0, 0, 'L', true);

    }
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B', 8);
    foreach ($data as $dxd){
        $pdf->Cell(25,$lebarCell,$dxd->valid_dfno,0,0,'L',true); 	// its similar with TD in HT

        $pdf->Ln();
        $pdf->Cell(25,$lebarCell,$dxd->valid_fullnm,0,0,'L',true); 	// its similar with TD in HT

        $pdf->Ln();
        $pdf->Cell(25,$lebarCell,$dxd->notiket,0,0,'L',true);
        $pdf->Ln();

        }

    $pdf->SetFont('Arial','B', 12);
//    $pdf->Cell(192,9,"Initiative Anniversary Challenge",0,0,'C', true);

}
//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
//$pdf->Cell(50,10,$img,'L',0,0,'R');
$pdf->SetFont('Courier','', 8);
$thnCheck = '2017';

function NumberToMonth($bln)
{
    switch($bln)
    {
        case 1 :
            $jum = "january";
            break;
        case 2 :
            $jum = "february";
            break;
        case 3 :
            $jum = "march";
            break;
        case 4 :
            $jum = "april";
            break;
        case 5 :
            $jum = "may";
            break;
        case 6 :
            $jum = "june";
            break;
        case 7 :
            $jum = "july";
            break;
        case 8 :
            $jum = "august";
            break;
        case 9 :
            $jum = "september";
            break;
        case 10 :
            $jum = "october";
            break;
        case 11 :
            $jum = "november";
            break;
        case 12 :
            $jum = "december";
            break;
    }
    return $jum;
}

$lebarCell = 4;
$pdf->Ln();
$pdf->SetXY(6,50);
$titleCol1 = 30;
$titleCol2 = 100;
$titleCol3 = 35;
$titleCol4 = 35;


$pdf->SetFont('Arial','B', 8);


$pdf->SetLeftMargin(6);
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);

$pdf->SetFillColor(204, 228, 180);
$pdf->SetFont('Courier','B', 8);


$title = "Form_Claim_IAC.pdf";
$pdf->SetTitle($title);
$pdf->Output();

?>
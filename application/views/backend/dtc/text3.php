<!DOCTYPE html>
<html>
<title>K-Link Anniversary 2017</title>

<head>
<style>
  @import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300);

  * {
  box-sizing: border-box;
}

html {
  background: radial-gradient(circle at top center, #333333 0%, #111111 100%);
  font: 300 30px/1 'Open Sans Condensed', sans-serif;
  text-align: center;
  text-transform: uppercase;
  color: #fff;
  overflow: scroll;
}

  body {
    background: radial-gradient(circle at top center, #333333 0%, #111111 100%);
    font: 300 30px/1 'Open Sans Condensed', sans-serif;
    text-align: center;
    text-transform: uppercase;
    color: #fff;
    overflow: hidden;

  }

#myInput {
  /*background-image: url('/css/searchicon.png');*/
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  /*font-size: 16px;*/
  padding: 12px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  text-transform: uppercase;
  color: #fff;
  overflow: hidden;

}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #000000;
}
</style>
</head>
<body>
<h1 class="w3-center">Pemenang Doorprize K-Link Anniversary 2017</h1>
<h2 class="w3-center">Pajak Ditanggung Pemenang</h2>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Temukan Nomor Anda!" title="Temukan Nomor Anda">

<table id="myTable">
  <tr class="header">
    <th style="width:60%;">Nomor</th>
    <th style="width:40%;">Hadiah</th>
  </tr>

  <?php
  foreach ($pemenang as $dta) {
    echo " <tr>
        <td>$dta->NOUNDIAN</td>
        <td>$dta->namahadiah</td>
        </tr>";

  }

  ?>

</table>
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>

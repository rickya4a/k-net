<html lang="en">
<head>
  <meta charset="utf-8"/>
  <title>K-LINK Back End Apps</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="k-link, responsive, HTML5, K-System Online"/>
  <meta name="author" content="k-link"/>

  <!-- The styles -->
  
 
  <link href="<?php echo base_url('asset/css/bootstrap.min.css')?>" rel="stylesheet"/>
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <script type="text/javascript">
	    function loaddata()
	    {
	    	var idnya=document.getElementById( "idmbr" ).value;
	 		if (idnya)
	        {
	    	//$( '#display_info' ).html("Isinya : "+idnya);
	    		
	    		$.ajax({
	    //			alert('coba ajax');
	            type : "POST",
	            url  : "<?php echo base_url();?>dtc/Cdtc_mbr/cariID",
	            data : {idm: idnya},
	            dataType: "json",
	          
	            success : function(result)
	                {
	                	$( '#display_info' ).html(result[0].fullnm);
	                }
	              });
	     	
	         } else
	        	 {
			    $( '#display_info' ).html("Please Enter Some Words");
			    }
	     }
	     
	     function simpanData(){
	     	<?php
	     	echo 'Nandang'; ?>
	     }
  </script>
  
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<form class="form-horizontal" action="" method="post" id="frm" onreset="resetForm()">	
			  <div class="form-group">
			    <label class="control-label col-sm-2" >&nbsp;</label>
			    <div class="col-sm-6">
			      <h2 align="center">Entry Data Peserta DTC</h2>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-4" >ID Member</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" id="idmbr" placeholder="Enter id" onchange="loaddata()">
<!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
			    </div>
			    <div class="col-sm-4" id="display_info"></div>
			  </div>
			  
			  
			  <div class="form-group">
			    <label class="control-label col-sm-4" >Pilihan:</label>
			    <div class="col-sm-4">
				  <select class="form-control" data-live-search="true">
					  <option value="">Hot Dog, Fries and a Soda</option>
					  <option>Burger, Shake and a Smile</option>
					  <option>Sugar, Spice and all things nice</option>
				  </select>
				  </div>
			  </div>
			  <!--
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <div class="checkbox">
			        <label><input type="checkbox"> Remember me</label>
			      </div>
			    </div>
			  </div>
			  -->
			  <div class="form-group">
			    <div class="col-sm-offset-4 col-sm-10">
			      <button type="submit" class="btn btn-default" name="Input" value="Input" id="input" onclick="simpanData">Input</button>
			      <button type="submit" class="btn btn-default" name="Edit" value="Edit" id="edit">Edit</button>
			      <button type="submit" class="btn btn-default" name="Delete" value="Delete" id="delete">Delete</button>
			      <button type="reset" class="btn btn-default" name="Clear" value="Clear" id="clear">Reset</button>
			    </div>
			  </div>
		</form> 
<!-- 
			<label for="submit" class="labelfrm">&nbsp;</label>
			<input type="submit" name="Input" value="Input" id="input"/>
			<input type="submit" name="Edit" value="Edit" id="edit"/>
			<input type="submit" name="Delete" value="Delete" id="delete"/>
			<input type="reset" name="Clear" value="Clear" id="clear"/>
-->
	</div><!--/fluid-row-->
</div>

</body>
</html>
<script type="text/javascript">
    function setSelectPay() {
        var x = $("#bank").val();
        var bankDetail = $("#bank option:selected").text();
        //alert("isi : " +x);
        //var bankDesc = $("#bank option:selected").text();
        var str = x.split("|");
        $("#bankid").val(str[0]);
        $("#bankCode").val(str[1]);
        //$("#bankDesc").val(bankDesc);
        $("#bankDesc").val(str[4]);
        $("#charge_connectivity").val(str[2]);
        $("#charge_admin").val(str[3]);
        $("#bankDescDetail").val(bankDetail);


    }
	
	function getDataEvent(){
		if($('#opsi').val() != '')
		{
			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('be_dtc/getDetailTiket');?>",
				dataType : 'json',
				data : {opsi:$('#opsi').val()},
				success: function(data){
					$(All.get_active_tab() + " #price").val(data.price_online);
					$(All.get_active_tab() + " #stok").val(data.max_online);
					$(All.get_active_tab() + " #note").val(data.early_note);
					$(All.get_active_tab() + " #early_status").val(data.earlystatus);
					$(All.get_active_tab() + " #tglearly").val(data.earlybird_date);
					$(All.get_active_tab() + " #tglnow").val(data.tglnow);
					//document.getElementById('note').innerHTML = data.early_note;
					
					var rowhtml = "";
					if(data.merchandise !== null) {
						//console.log(Object.getOwnPropertyNames(data.merchandise.prdPilihan));
						var prdPilihan = data.merchandise.prdPilihan;
						var prdPasti = data.merchandise.prdPasti;
						//console.log(prdPilihan);
						
						var ix = 1;
						$("#pilihProduk").html(null);
						if(prdPilihan != null) {
							Object.keys(prdPilihan).forEach((item) => {
							 // create select dengan nama = 'item'
							  //console.log('select name = ', item);
							  rowhtml += "<div class='form-group'>";
							  rowhtml += "<label class='control-label col-sm-4' >"+item+"</label>";
							  rowhtml += "<div class='col-xl-8'>";
							  rowhtml += "<select style='width: 500px;' class='form-control inp_array' name=free[]>";
							  prdPilihan[item].forEach((opt) => {
								// create option dengan value = opt.id; textnya = opt.kode_produk
								//console.log('value = ', opt.kode_produk);
								//console.log('text = ', opt.kode_produk);
								var setWarna = "";
								if(opt.warna !== null && opt.warna !== "" && opt.warna !== " ") {
								   setWarna = " - "+opt.warna;
								}
								
								var setUkuran = "";
								if(opt.ukuran !== null && opt.ukuran !== "" && opt.ukuran !== " ") {
								   setUkuran = " - "+opt.ukuran;
								}
								
								 rowhtml += "<option value='"+opt.kode_produk+"'>"+opt.nama+setWarna+setUkuran+"</option>";	
								// jangan lupa option yang sudah dibuat di append ke select
							  });
							  rowhtml += "</select>";
							  rowhtml += "</div>";
							  rowhtml += "</div>";
							});
						}
						
						if(prdPasti != null) {
							Object.keys(prdPasti).forEach((item) => {
							 // create select dengan nama = 'item'
							  //console.log('select name = ', item);
							  rowhtml += "<div class='form-group'>";
							  rowhtml += "<label class='control-label col-sm-4' >"+item+"</label>";
							  rowhtml += "<div class='col-xl-8'>";
							  rowhtml += "<select style='width: 500px;' class='form-control inp_array' name=free[]>";
							  prdPasti[item].forEach((opt) => {
								// create option dengan value = opt.id; textnya = opt.kode_produk
								//console.log('value = ', opt.kode_produk);
								//console.log('text = ', opt.kode_produk);
								 rowhtml += "<option value='"+opt.kode_produk+"'>"+opt.nama+"</option>";	
								// jangan lupa option yang sudah dibuat di append ke select
							  });
							  rowhtml += "</select>";
							  rowhtml += "</div>";
							  rowhtml += "</div>";
							});
							
							/*$.each(prdPilihan, function(key, value) {
							  rowhtml += "<div class='form-group'>";
								 //sdf = Object.getOwnPropertyNames(prdPilihan);
								 //console.log(sdf[key]);
								rowhtml += "<label class='control-label col-sm-4' >"+prdPilihan.properties[key].name+"</label>";
								rowhtml += "<div class='col-sm-3'>";
																
								rowhtml += "</div>";
							  rowhtml += "</div>";
							});*/
						}
					    $(All.get_active_tab() + " #pilihProduk").append(rowhtml);
					} else {
						$(All.get_active_tab() + " #pilihProduk").append(rowhtml);
					}
				}
			});
		}

	}
	
	function pilihjenisAcara() {
		var pil_event = $(All.get_active_tab() + " #pil_event").val();
		res = pil_event.split("|");
		
		//console.log(res);
		$(All.get_active_tab() + " #event_cat_id").val(res[1]);
		$(All.get_active_tab() + " #nm_event").val(res[0]);
		
		var cat_id = res[1];
		
		if(pil_event != '') {
			$.ajax({
				type : "GET",
				url  : "<?php echo site_url('dtc/listEvent/');?>" + cat_id,
				dataType : 'json',
				success: function(data){
					if(data.response == "true") {
						var arrayData = data.arrayData;
						var rowhtml = "";
						$(All.get_active_tab() + " #opsi").html(null);
						rowhtml += "<option value=''>--Pilih Lokasi dan Waktu--</option>";
						$.each(arrayData, function(key, value) {
							rowhtml += "<option value='"+value.id+"'>"+value.nama+" - "+value.lokasi+" ("+value.event_date2+")</option>";	
						});
						$(All.get_active_tab() + " #opsi").html(rowhtml);
					} else  {
						alert("Acara tidak tersedia..");
						$(All.get_active_tab() + " #opsi").html(null);
					}
				}
			});
		} else {
			$(All.get_active_tab() + " #opsi").html(null);
		}
	}

    function loaddata()
    {
        var idnya=document.getElementById( "idmbr" ).value;

        if (idnya)
        {
            //$( '#display_info' ).html("Isinya : "+idnya);

            $.ajax({
                //			alert('coba ajax');
                type : "POST",
                url  : "<?php echo base_url();?>dtc/Cdtc_mbr/cariID",
                data : {idm: idnya},
                dataType: "json",

                success : function(result)
                {
                    if(result[0].fullnm!=null){
                        $("#display_info").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
                        //$("#display_info2").val( result[0].fullnm + " [" +result[0].shortnm+ "]");

//                        $("#display_info2").val( result[0].fullnm + " [" +result[0].rank+ "]");
                        $("#idmbr2").val( idnya);
						$("#display_info2").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
						$(All.get_active_tab() + " #notelp").val( result[0].tel_hp);
						$(All.get_active_tab() + " #email").val( result[0].email);
                    }
//]	                	$( '#display_info' ).html(result[0].fullnm + " [" +result[0].rank+ "]");


//						document.getElementById("#display_info").value = result[0].fullnm + " [" +result[0].rank+ "]";
                    else
                    {
                        $("#display_info").val( "ID Member tidak valid");
                        $("#display_info2").val( "ID Member tidak valid");
                        $("#idmbr2").val( idnya);

                    }
//						$( '#display_info' ).html("ID Member tidak valid");

//
//						document.getElementById("#display_info").value ="ID Member tidak valid";

                }
            });

        } else
        {
            $( '#display_info' ).html("Please Enter Some Words");
        }
    }
	
	function getInfoTicket() {

		var opsi = $(All.get_active_tab() + " #opsi").val();
		console.log("opsi : " +opsi);
        if(opsi != ""){
			$.ajax({
					//			alert('coba ajax');
					type : "POST",
					url  : "<?php echo base_url();?>be_dtc/getDetailTiket",
					data : {opsi: opsi},
					dataType: "json",

					success : function(result)
					{
						if(result != null) {
							$(All.get_active_tab() + " #price").val(result.price_offline);
							$(All.get_active_tab() + " #stok").val(result.max_offline);
							
							var rowhtml = "";
							if(result.merchandise !== null) {
								//console.log(Object.getOwnPropertyNames(data.merchandise.prdPilihan));
								var prdPilihan = result.merchandise.prdPilihan;
								var prdPasti = result.merchandise.prdPasti;
								//console.log(prdPilihan);
								
								var ix = 1;
								$(All.get_active_tab() + " #pilihProduk").html(null);
								Object.keys(prdPilihan).forEach((item) => {
								 // create select dengan nama = 'item'
								  //console.log('select name = ', item);
								  rowhtml += "<div class='form-group'>";
								  rowhtml += "<label class='control-label col-sm-4' >"+item+"</label>";
								  rowhtml += "<div class='col-xl-8'>";
								  rowhtml += "<select class='form-control inp_array' name=free[]>";
								  prdPilihan[item].forEach((opt) => {
									// create option dengan value = opt.id; textnya = opt.kode_produk
									//console.log('value = ', opt.kode_produk);
									//console.log('text = ', opt.kode_produk);
									var setWarna = "";
									if(opt.warna !== null && opt.warna !== "" && opt.warna !== " ") {
									   setWarna = " - "+opt.warna;
									}
									
									var setUkuran = "";
									if(opt.ukuran !== null && opt.ukuran !== "" && opt.ukuran !== " ") {
									   setUkuran = " - "+opt.ukuran;
									}
									
									 rowhtml += "<option value='"+opt.kode_produk+"'>"+opt.nama+setWarna+setUkuran+"</option>";	
									// jangan lupa option yang sudah dibuat di append ke select
								  });
								  rowhtml += "</select>";
								  rowhtml += "</div>";
								  rowhtml += "</div>";
								});
								
								Object.keys(prdPasti).forEach((item) => {
								 // create select dengan nama = 'item'
								  //console.log('select name = ', item);
								  rowhtml += "<div class='form-group'>";
								  rowhtml += "<label class='control-label col-sm-4' >"+item+"</label>";
								  rowhtml += "<div class='col-xl-8'>";
								  rowhtml += "<select class='form-control inp_array' name=free[]>";
								  prdPasti[item].forEach((opt) => {
									// create option dengan value = opt.id; textnya = opt.kode_produk
									//console.log('value = ', opt.kode_produk);
									//console.log('text = ', opt.kode_produk);
									 rowhtml += "<option value='"+opt.kode_produk+"'>"+opt.nama+"</option>";	
									// jangan lupa option yang sudah dibuat di append ke select
								  });
								  rowhtml += "</select>";
								  rowhtml += "</div>";
								  rowhtml += "</div>";
								});
								
								/*$.each(prdPilihan, function(key, value) {
								  rowhtml += "<div class='form-group'>";
									 //sdf = Object.getOwnPropertyNames(prdPilihan);
									 //console.log(sdf[key]);
									rowhtml += "<label class='control-label col-sm-4' >"+prdPilihan.properties[key].name+"</label>";
									rowhtml += "<div class='col-sm-3'>";
																	
									rowhtml += "</div>";
								  rowhtml += "</div>";
								});*/
								
								$(All.get_active_tab() + " #pilihProduk").append(rowhtml);
							} else {
								$(All.get_active_tab() + " #pilihProduk").append(null);
							}
							
						} else {
							alert("Data untuk pilihan acara tersebut tidak ada...");
							$(All.get_active_tab() + " #price").val(null);
							$(All.get_active_tab() + " #stok").html(null);
						}
					}
			});
		}	else {
			$(All.get_active_tab() + " #pilihProduk").html(null);
		}
	}

    function loaddata2()
    {
        var idnya=document.getElementById( "idmbr2" ).value;
        var acara=document.getElementById( "opsi" ).value;
        if (idnya)
        {
            //$( '#display_info' ).html("Isinya : "+idnya);

            $.ajax({
                //			alert('coba ajax');
                type : "POST",
                url  : "<?php echo base_url();?>dtc/Cdtc_mbr/cariID2",
                data : {idm: idnya,acara: acara},
                dataType: "json",

                success : function(result)
                {
                    if(result[0].fullnm!=null) {
//	                	$( '#display_info' ).html(result[0].fullnm + " [" +result[0].rank+ "]");
                        $("#display_info2").val( result[0].fullnm + " [" +result[0].shortnm+ "]");
//						document.getElementById("#display_info").value = result[0].fullnm + " [" +result[0].rank+ "]";
						$(All.get_active_tab() + " #notelp").val( result[0].tel_hp);
						$(All.get_active_tab() + " #email").val( result[0].email);
					}	
                    else {
//						$( '#display_info' ).html("ID Member tidak valid");
                        $("#display_info2").val( "ID Member tidak valid/Sudah Terdaftar");
						
					}
						//
//						document.getElementById("#display_info").value ="ID Member tidak valid";

                }
            });

        } else
        {
            $( '#display_info2' ).html("Please Enter Some Words");
        }
    }
    function simpandata(){
        alert('coba ajax');
        /*$.ajax({

         type : "POST",
         url  : "   <!--?php echo base_url();?--> dtc/Cdtc_mbr/cariID",
         data : {idm: idnya},
         dataType: "json",

         success : function(result)
         {
         $( '#display_info' ).html(result[0].fullnm + " [" +result[0].rank+ "]");
         }
         });*/
    }


</script>

<script>
    $(document).ready(function(){


            /*
            $('#opsi').change(function(){
                    if($('#opsi').val() != '')
                    {
                        $.ajax({
                            type : "POST",
                            url  : "<?php echo site_url('be_dtc/getDetailTiket');?>",
                            dataType : 'json',
                            data : {opsi:$('#opsi').val()},
                            success: function(data){
                                $("#price").val(data.price_offline);
                                $("#stok").val(data.max_offline);
                            }
                        });
                    }
                }
            );
            */  

            $('#addItem').click(function() {


                var acara = $('#opsi option:selected').text();
                var x = document.getElementById("idmbr2");

                var qty =parseInt( $('#qty').val());
                var stok=parseInt ($('#stok').val());
                var qtytot =parseInt( $('#qtytot').val());
                var harga =parseInt( $('#price').val());
                var duit=parseInt( $('#duit').val());
				
				var free_prd = "";
				$(All.get_active_tab() + ' select.inp_array').each(function() {
					//console.log('ISI : ' +$(this).val()); 
					free_prd +=  $(this).val() + ",";
				});
				
                var i =0;
                var counter=0;
                for(i=0; i< $('.main tr').length;i++ ){
                    var nama =$("tr#"+i).find('input').val();

                    if($('#idmbr2').val() == nama ){
                        counter++;
                    }
                }
                if(harga==''){
                    alert('Mohon refresh halaman dan tunggu sampai harga ter generate')
                }else
                {
                    if(counter>0){
                        alert('Peserta hanya boleh mendaftar satu kali')
                    }
                    else{
                        if(stok<qty){
                            alert('Pemesanan tiket melebihi stok')
                        }
                        else
                        {
                            if(($('#idmbr2').val()!= '')&&($('#qty').val()!= '')&&($('#opsi').val()!= '')&&($('#display_info2').val()!= 'ID Member tidak valid/Sudah Terdaftar') ){
                                var rowCount = $('.main tr').length-1;
                                var no = rowCount + 1;

                                var num2 = parseFloat(Math.round(($('#price').val()*$('#qty').val()) * 100) / 100);

                                $("<tr data-status=\"I\" id='"+no+"'>"
                                    + "<td><input type=\"hidden\" name=\"idpendaftar[]\" id=\"idpendaftar\" value=\""+$('#idmbr2').val()+"\" /><input type=\"hidden\" name=\"jumlah[]\" id=\"jumlah\" readonly value=\""+$('#qty').val()+"\"  />"+$('#idmbr2').val()+"</td>"
                                    + "<td><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"hidden\" name=\"nmpendaftar[]\" id=\"nmpendaftar\" value=\""+$('#display_info2').val()+"\" />"+$('#display_info2').val()+"</td>"

                                    + "<td><input type=\"hidden\" name=\"emailrow[]\" id=\"emailrow\" value=\""+$('#email').val()+"\" />"+$('#email').val()+"</td>"
                                    + "<td><input type=\"hidden\" name=\"notelprow[]\" id=\"notelprow\" value=\""+$('#notelp').val()+"\" />"+$('#notelp').val()+"</td>"

                                    + "<td><input type=\"hidden\" name=\"event_db[]\" id=\"event_db\" value=\""+$('#opsi').val()+"\" /><input type=\"hidden\" name=\"event_nm[]\" id=\"event_nm\" value=\""+acara+"\" />"+acara+"</td>"
                                    + "<td>"+1+"</td>"
                                    + "<td><input type=\"hidden\" name=\"harga[]\" id=\"harga\" value=\""+$('#price').val()+"\"  />"+$('#price').val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
                                    + "<input type=\"hidden\" name=\"prd_bundling[]\" value="+free_prd+" /><td>"+(num2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
                                    + "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\" /></td>"
                                    +" </tr>").appendTo(".main tbody");
                                //       x.remove(x.selectedIndex);

                                $('#opsi').attr("disabled", true);
                                $("#stok").val( ($('#stok').val()) -qty);
                                $("#qtytot").val( qtytot +qty);
                                $("#duit").val( duit +(qty*harga));
                                $("#duit2").val( $("#duit").val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                            }else{
                                alert('Data harus diisi');
                            }
                        }


                    }
                }

            });

            $("table.main").on("click", "#ibtnDel", function (event) {
//                    {
//                        $.ajax({
//                            type : "POST",
//                            url  : "<?php //echo site_url('PO/getProduk');?>//",
//                            dataType : 'json',
//                            data : {idproduk:$('#idproduk').val()},
//                            success: function(data){
//                                $("#ID_PRODUCT").html(data.option);
//
//                            }
//                        });
//                    }
                    var tr = $(this).closest('tr');

                    var stok=parseInt ($('#stok').val());
                    var qty =parseInt(tr.find('input').val() );
                    var harga =parseInt( $('#price').val());
                    var duit=parseInt( $('#duit').val());

                    var row = tr.find("input[id=row]").val();
                    $("#stok").val( stok+1 );
                    $("#duit").val( duit -(1*harga));
                    $("#duit2").val( $("#duit").val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                    var rowCount = $('.main tr').length-1;

                    $(this).closest("tr").remove();
                }

            );


        }
    );
</script>
<div class="mainForm">
    <div id="frmRegumroh">
        <!--        <table width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">-->
        <!--            <thead>-->
        <!--            <tr>-->
        <!--                <th colspan="8">Listing DTC</th>-->
        <!--            </tr>-->
        <!--            <tr>-->
        <!--                <th>No</th>-->
        <!--                <th>NoPeserta</th>-->
        <!--                <th>ID</th>-->
        <!--                <th>Nama</th>-->
        <!--                <th>Rank</th>-->
        <!--                <th>Acara</th>-->
        <!--            </tr>-->
        <!--            </thead>-->
        <!--            <tbody>-->
        <!--            --><?php
        //            $i=1;
        //            foreach ($isiTabel as $vIsi) {
        //                echo "<tr> ";
        //                echo "<td>$i</td>";
        //                echo "<td> $vIsi->noPeserta </td>";
        //                echo "<td> $vIsi->dfno </td>";
        //                echo "<td> $vIsi->fullnm </td>";
        //                echo "<td> $vIsi->peringkat </td>";
        //                echo "<td> $vIsi->lokasi </td>";
        //                echo "</tr>";
        //                $i++;
        //            }
        //            ?>
        <!--            </tbody>-->
        <!--        </table>-->
        <form class="form-horizontal" id="frmListReg" name="frmListReg" method="post" action="<?php echo $action; ?>" >
            <fieldset>
                <div class="control-group">
                    <div class="form-group">
                        <label class="control-label" for="typeahead">Input Member</label>
                        <div class="col-sm-8">
                            <input type="text" id="idmbr" name="idmbr" onchange="loaddata()" style="width: 500px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" >Nama Member</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="display_info"  name="display_info" readonly style="width: 500px;">
                            <!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" for="typeahead">Tanggal Pembayaran</label>
                        <div class="col-sm-8">
                            <input type="text" class="TabOnEnter dtpicker" name="tgltrf" id="tgltrf" tabindex="5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="typeahead">Cara Pembayaran</label>
                        <div class="col-sm-8">
                            <input type="text" id="pembayaran" name="pembayaran"  style="width: 500px;" value="TRF BCA">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="typeahead">No Rekening</label>
                        <div class="col-sm-8">
                            <input type="text" id="rekening" name="rekening"  style="width: 500px;" value="4502209098">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="typeahead">Remark</label>
                        <div class="col-sm-8">
                            <textarea id="REMARK" name="REMARK" cols="100" rows="3" data-bvalidator="required" style="width: 500px; "></textarea>
                        </div>
                    </div>
                    <hr>

                    <div class="form-group">
					    <label class="control-label col-sm-4" >Jenis Acara/Event:</label>
						<div class="col-xl-8">
							<select class="form-control" data-live-search="true" id="pil_event" name="pil_event" onchange="pilihjenisAcara()">
									<?php
									echo "<option value=\"\">--Pilih Jenis Acara--</option>";
									foreach($listTipeAcara as $dta) {
										echo "<option value=\"$dta->shortname|$dta->cat_id\">$dta->category_desc</option>";
									}
									?>
								</select>
								<input type="hidden" class=" span20" id="event_cat_id" name="event_cat_id" value=""  />
								<input type="hidden" class=" span20" id="nm_event" name="nm_event" value=""  />
						</div>
                        <label class="control-label col-sm-4" >Lokasi</label>
                        <div class="col-xl-8">
                            <select class="form-control" style="width: 500px;" data-live-search="true" id="opsi" name="opsi" onchange="getDataEvent()">
                                <option></option>
                                <?php
                                foreach ($alamat as $kota) {

                                    echo "<option value=\"$kota->id\">$kota->nama - $kota->lokasi ($kota->event_date2)</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" >Harga</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="price" name="price" readonly style="width: 500px;">
							<input type="hidden" class="form-control" id="early_status" name="early_status" value="" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" >Tiket Tersedia</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="stok" name="stok" readonly style="width: 500px;">

                        </div>
                    </div>
					<span id="pilihProduk">

					</span>	
                    <div class="form-group">
                        <label class="control-label" for="typeahead">Input Peserta</label>
                        <div class="col-sm-8">
                            <input type="text" id="idmbr2" name="idmbr2" onchange="loaddata2()" style="width: 500px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" >Nama Peserta</label>
                        <div class="col-xl-8">
                            <input type="text" class="form-control" id="display_info2"  readonly style="width: 500px;">
                            <!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" for="typeahead">Nomor HP</label>
                        <div class="col-sm-8">
                            <input type="text" id="notelp" name="notelp"  style="width: 500px;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="typeahead">Alamat Email</label>
                        <div class="col-sm-8">
                            <input type="text" id="email" name="email"  style="width: 500px;">
                        </div>
                    </div>



                    <input type="hidden" class="form-control" id="qty"   value="1">



                    <input type="hidden" name="totalx" value="" />
                    <input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
                    <input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
                    <input type="hidden" name="bankid" id="bankid" value=""  />
                    <input type="hidden" name="bankCode" id="bankCode" value=""  />
                    <input type="hidden" name="bankDesc" id="bankDesc" value=""  />
                    <input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
                    <input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
                    <input type="hidden" name="charge_admin" id="charge_admin" value=""  />

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a class="btn btn-success" id="addItem"> Tambahkan </a>
                            <input tabindex="3" style="margin-left: 550px;" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="All.ajaxFormPost('frmListReg','be_dtc/submit')"/>

                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        </div>
                    </div>


                    <table width="90%" class="main table table-hover" id="box-table-b">
                        <thead>
                        <tr>
                            <th >ID Member</th>
                            <th >Nama Member</th>
                            <th >Email</th>
                            <th >No telp</th>
                            <th >Event</th>

                            <th >Quantity </th>
                            <th >Harga</th>
                            <th >Total amount</th>

                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                    <div class="form-group">
                        <label class="control-label col-sm-4" >Total Bayar</label>
                        <div class="col-sm-3">
                            <input type="hidden" class="form-control" id="duit" value="0" readonly name="duit" style="width: 500px;">
                            <input type="text" class="form-control" id="duit2" value="0" readonly name="duit2" style="width: 500px;">

                            <!--			      <input type="text" name="username" id="idmbr" onchange="loaddata()">  -->
                        </div>
                    </div>
                    <div>
                        <!--                        <input type="input" name="qtytot" id="qtytot" readonly  value="0">-->

                        <!--                        <button type="submit" class="btn btn-success">Submit</button>-->
                        <!--                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Search" onclick="All.ajaxFormPost(this.form.id,'be_dtc/get')"/>-->
                    </div>
                </div>
            </fieldset>
            <div class="result"></div>
        </form>
    </div>
</div>


<script>
    $(document).ready(function() {
        $(All.get_active_tab() + " .dtpicker").datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        }).datepicker("setDate", new Date());

        All.set_datatable();
    });
</script>
<!DOCTYPE html>
<html>
<title>K-Link Anniversary 2017</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300);
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }



    html, body {
        height: 100vh;
        overflow: hidden;
        background: radial-gradient(circle at top center, #333333 0%, #111111 100%);
        font: 300 30px/1 'Open Sans Condensed', sans-serif;
        text-align: center;
        text-transform: uppercase;
        color: #fff;
    }


    .mySlides {display:none;}
    .w3-left, .w3-right, .w3-badge {cursor:pointer}
    .w3-badge {height:13px;width:13px;padding:0}





</style>
<body>
<!--<h2 class="w3-center">PENGUMUMAN PEMENANG UNDIAN ANNIVERSARY</h2>-->
<h1>Pemenang Undian Perayaan Ulang Tahun K-Link Indonesia 2017</h1>
<h4>Pajak Ditanggung Oleh Pemenang</h4>

<div class="w3-content">
    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>SEPEDA MOTOR </b></h1>
        <?php
        foreach ($motor as $dta) {
            echo "<h1>$dta->NOUNDIAN</h1>";
        }
        ?>
    </div>

    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>NOTE BOOK </b></h1>
        <?php
        foreach ($leptop as $dta) {
            echo "<h1>$dta->NOUNDIAN</h1>";
        }
        ?>
    </div>

    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>LED TV 32 INCH </b></h1>
        <?php
        foreach ($tv as $dta) {
            echo "<h1>$dta->NOUNDIAN</h1>";
        }
        ?>
    </div>


    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>7STAR</b></h1>
        <?php
        for($i=0;$i<5;$i++){
            echo "<h1>$star1[$i]<span style='display:inline-block; width: 50px;'></span> $star2[$i]<span style='display:inline-block; width: 50px;'></span> $star3[$i]<span style='display:inline-block; width: 50px;'></span> $star4[$i]<span style='display:inline-block; width: 50px;'></span> $star5[$i]</h1>
            ";
        }
        ?>
    </div>
    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>2 BOX K-LIQUID KOLAGEN </b></h1>
        <?php
        foreach ($kolagen as $dta) {
            echo "<h1>$dta->NOUNDIAN</h1>";
        }
        ?>
    </div>


    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>YOROPEN</b></h1>
        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$yoropen1[$i]<span style='display:inline-block; width: 50px;'></span> $yoropen2[$i]<span style='display:inline-block; width: 50px;'></span> $yoropen3[$i]<span style='display:inline-block; width: 50px;'></span> $yoropen4[$i]<span style='display:inline-block; width: 50px;'></span> $yoropen5[$i]</h1>";
        }
        ?>
    </div>



    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>3 STARTERKIT PROMO & CHLOROPHYLL & K VIT C</b></h1>
        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$STARTERKIT1[$i]<span style='display:inline-block; width: 50px;'></span> $STARTERKIT2[$i]</h1>";
        }
        ?>
    </div>



    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>6 VOUCHER STARTERKIT PROMO</b></h1>
        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$STARTERKITz1[$i]<span style='display:inline-block; width: 50px;'></span> $STARTERKITz2[$i]</h1>";
        }
        ?>
    </div>



    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>K-ENGINE POWER</b></h1>
        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$engine1[$i]<span style='display:inline-block; width: 50px;'></span> $engine2[$i]<span style='display:inline-block; width: 50px;'></span> $engine3[$i]<span style='display:inline-block; width: 50px;'></span> $engine4[$i]</h1>";
        }
        ?>
    </div>


    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>K-VIT C TEAVIGO</b></h1>
        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$teh1[$i]<span style='display:inline-block; width: 50px;'></span> $teh2[$i]<span style='display:inline-block; width: 50px;'></span> $teh3[$i]<span style='display:inline-block; width: 50px;'></span> $teh4[$i]<span style='display:inline-block; width: 50px;'></span> $teh5[$i]</h1>";
        }
        ?>
    </div>


    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>SHAKER BOTTLE K-LINK</b></h1>
        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$shaker1[$i]<span style='display:inline-block; width: 50px;'></span> $shaker2[$i]<span style='display:inline-block; width: 50px;'></span> $shaker3[$i]<span style='display:inline-block; width: 50px;'></span> $shaker4[$i]<span style='display:inline-block; width: 50px;'></span> $shaker5[$i]</h1>";
        }

        ?>
    </div>
    <div class="mySlides w3-container w3-xlarge w3-animate-right w3-card-4">
        <h1><b>SHAKER BOTTLE K-LINK</b></h1>

        <?php
        for($i=0;$i<10;$i++){
            echo "<h1>$shaker6[$i]<span style='display:inline-block; width: 50px;'></span> $shaker7[$i]<span style='display:inline-block; width: 50px;'></span> $shaker8[$i]<span style='display:inline-block; width: 50px;'></span> $shaker9[$i]<span style='display:inline-block; width: 50px;'></span> $shaker10[$i]</h1>";
        }

        ?>

    </div>


    <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
        <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
        <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(4)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(5)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(6)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(7)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(8)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(9)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(10)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(11)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(12)"></span>



    </div>

</div>



<script>
    var slideIndex = 0;
    carousel();

    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");

        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > x.length) {slideIndex = 1}
        x[slideIndex-1].style.display = "block";
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-white", "");
        }
        dots[slideIndex-1].className += " w3-white";

        setTimeout(carousel, 5000);

    }



    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function currentDiv(n) {
        showDivs(slideIndex = n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length}
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-white", "");
        }
        x[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " w3-white";
    }

</script>

</body>
</html>

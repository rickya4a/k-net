<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
<!-- Update Password -->
<div class="col-lg-12 col-xs-12" id="pwd">
	<form name="upd_bnsstt_view" id="upd_bnsstt_view" method="post"> 
    	<div class="track-order-grid">
        	<div>
            	<span>Bonus Statement</span>
            	<input type="hidden" name="idmember" id="idmember" value="<?php echo $member[0]->dfno; ?>"/>                            
       		</div>
       		<div>
       			<select name="bnsstatus_view" id="bnsstatus_view">
       				<?php
       				 if($member[0]->bnsstt_view_type == "1" || $member[0]->bnsstt_view_type == "" || $member[0]->bnsstt_view_type == null) {
       				 	echo "<option value=\"1\" selected=selected>Cetak dan dikirim ke Stockist</option>";
						 echo "<option value=\"2\">Digital (dapat dilihat melalui aplikasi atau website www.k-net.co.id)</option>";
       				 } else {
       				 	echo "<option value=\"1\" >Cetak dan dikirim ke Stockist</option>";
						 echo "<option value=\"2\" selected=selected>Digital (dapat dilihat melalui aplikasi atau website www.k-net.co.id)</option>";
       				 }
       				?>
       				
       			</select>
       			
       		</div>
       		<div>
       			<font color=red>Mohon perhatian terhadap pilihan di atas:</font>
       		</div>
       		<div>
       			Jika Anda memilih <font color=red>Cetak dan dikirim ke Stockist</font>, maka akan dikenakan biaya Rp.10.000 dipotong dari bonus yang Anda dapat.
       		</div>
       		<div>
       			Jika Anda memilih <font color=red>Digital</font>, maka akan bebas biaya, tapi bonus statement bisa dilihat melalui www.k-net.co.id atau aplikasi mobile.
       		</div>
            <div>
            	<input class="btn1 btn2 btn-primary1"  tabindex="2" type="button" value="Update" onclick="Shopping.updBnsStt()"/>
            </div>
		</div>
   	</form>
</div>
<div id="result"></div>
<!--<script type="text/javascript">
function mouseoverPass(obj) {
  var obj = document.getElementById('new_pass');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('new_pass');
  obj.type = "password";
}
</script>-->
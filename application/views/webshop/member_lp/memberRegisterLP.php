<style>
	.form-list {
		font-size: 0.75em;
	}	
	
	.sponsorid{
		width: 80%;
	}	
	
	
	.register-top-grid > .col-md-6.col-xs-12 {
		min-height: 60px;
	}
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Member&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Pendaftaran Baru&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
      <div id="voucherDiv">
      	
      	   
      	<div class="col-md-12 col-xs-12 vpullset3">   
	        <form id="formMember" action="<?php echo base_url('member/reg/starterkit'); ?>" method="POST" onsubmit="return memberlp.validateInputMember()">        
	    	  <div class="register-top-grid">
	    	    
	            <!--<div class="col-md-6 col-xs-12">
	                <label class="label-list">ID Sponsor (Tekan tab untuk mengecek ID)<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="hidden" id="sponsorid" name="sponsorid" value="<?php echo getUserID(); ?>" onchange="Shopping.getMemberInfo(this.value, 'sponsorname')" />
	                <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo $idmember ?>" />
	                <input type="hidden" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo getUserName(); ?>" />
	                
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Sponsor<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="hidden" readonly="readonly" id="sponsorname" name="sponsorname" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">ID Recruiter<font color="red">&nbsp;*</font></label><br/>
	                <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo getUserID(); ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Recruiter<font color="red">&nbsp;*</font></label><br/>
	                <input type="hidden" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo getUserName(); ?>" />
	            </div>-->
	            
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Member<font color="red">&nbsp;*</font></label><br/>
	                <input readonly="readonly" class="required uppercase" type="text" id="membername" name="membername" value="<?php echo $nama; ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo $idmember ?>" />
	                <input type="hidden" id="id_lp" name="id_lp" value="NONLP" />
	                <input type="hidden" id="recruitername" name="recruitername" value="" />
	                <input type="hidden" id="sponsorid" name="sponsorid" value="0000999" />
	                <input  type="hidden" readonly="readonly" id="sponsorname" name="sponsorname" value="KIV CODE" />
	                <input type="hidden" id="email_pendaftar" name="email_pendaftar" value="<?php echo $email_pendaftar ?>" />
	                <label class="label-list">No KTP<font color="red">&nbsp;*</font></label><br/>
	                <input class="required uppercase" type="text" id="idno" name="idno" value="" onchange="memberlp.checkDoubleKTP(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label  class="label-list">Jenis Kelamin<font color="red">&nbsp;*</font></label><br/>
	                <select  class="form-list required" id="sex" name="sex">
	                	<option value="M">Pria</option>
	                	<option value="F">Wanita</option>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Tgl Lahir<font color="red">&nbsp;*</font></label><br/>
	                <!--<input class="required" type="text" id="birthdt" name="birthdt" placeholder="DD/MM/YYYY" value="" onchange="memberlp.checkAgeMember(this.value)" />-->
	                <?php 
	                  echo datebirth_combo();
	                ?>
	            </div>
	            <!--<div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="addr1" name="addr1" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat (Opsional)</label><br/>
	                <input type="text" id="addr2" name="addr2" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat (Opsional)</label><br/>
	                <input type="text" id="addr3" name="addr3" value="" />
	            </div>
	            
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="tel_hp" name="tel_hp" value="" onchange="memberlp.checkDoubleHP(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Rekening Bank</label><br/>
	                <select class="form-list" id="pilBank" name="pilBank">
	                	<option value="">--Pilih disini--</option>
	                 <?php
	                    //print_r($listBank['arrayData']);
	                    foreach($listBank['arrayData'] as $dta) {
	                    	echo "<option value=\"$dta->bankid\">$dta->description</option>";
	                    }
	                 ?>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No Rekening</label><br/>
	                <input type="text" id="no_rek" name="no_rek" value="" />
	            </div> -->
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat<font color="red">&nbsp;*</font></label><br/>
	                <input tabindex="6" class="required uppercase" type="text" id="addr1" name="addr1" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
	                <input tabindex="10" class="required numeric-input" type="text" id="tel_hp" name="tel_hp" value="" onchange="memberlp.checkDoubleHP(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Kota</label><br/>
	                <input tabindex="7" class="uppercase" type="text" id="addr2" name="addr2" value="" />
	            </div>
	            
	            <!--<div class="col-md-6 col-xs-12">
	                <label class="label-list">Rekening Bank</label><br/>
	                <select tabindex="11" class="form-list" id="pilBank" name="pilBank">
	                	<option value="">--Pilih disini--</option>
	                 <?php
	                    //print_r($listBank['arrayData']);
	                    foreach($listBank['arrayData'] as $dta) {
	                    	echo "<option value=\"$dta->bankid\">$dta->description</option>";
	                    }
	                 ?>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">             
	                <label class="label-list">Kode Pos</label><br/>
	                <input tabindex="8" type="text" id="addr3" name="addr3" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No Rekening</label><br/>
	                <input tabindex="12" type="text" id="no_rek" name="no_rek" value="" />
	            </div>-->
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat Email</label><br/>
	                <input readonly="readonly" tabindex="9" type="text" id="memb_email" name="memb_email" value="<?php echo $email_memb; ?>" />
	            </div>
	            <div class="clearfix"> </div>
	          </div>
	          <div class="col-md-12">
       
            
            	<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjut<i class="fa fa-arrow-right"></i></button><br />
              </div>
        <!-- next button-->
        </div>  
	        </form>
        </div>
       </div>
        <!-- Delivery Options--> 
      </div> <!-- End formCart1-->
      <div id="divCheckOut"></div> 
      <div id="afterPayment"></div>
    </div>
</div>
<!--Checkout Wrapper-->

<style>
	.form-list {
		font-size: 0.75em;
	}	
	
	.sponsorid{
		width: 80%;
	}	
</style>


<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
      <div id="voucherDiv">
      	
      	   
      	<div class="col-md-12 col-xs-12 vpullset3">   
	        <form id="formMember" action="<?php echo base_url('member/starterkit'); ?>" method="POST" onsubmit="return Shopping.validateInputMember()">        
	    	  <div class="register-top-grid">
	    	    
            <input type="hidden" id="id_lp" name="id_lp" value="NONLP" />
	            
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No KTP<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="idno" name="idno" value="" onchange="Shopping.checkDoubleKTP(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Member<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="membername" name="membername" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label  class="label-list">Jenis Kelamin<font color="red">&nbsp;*</font></label><br/>
	                <select  class="form-list required" id="sex" name="sex">
	                	<option value="M">Pria</option>
	                	<option value="F">Wanita</option>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Tgl Lahir<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="birthdt" name="birthdt" placeholder="DD/MM/YYYY" value="" onchange="Shopping.checkAgeMember(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="addr1" name="addr1" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat (Opsional)</label><br/>
	                <input type="text" id="addr2" name="addr2" value="" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat (Opsional)</label><br/>
	                <input type="text" id="addr3" name="addr3" value="" />
	            </div>
	            
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
	                <input class="required" type="text" id="tel_hp" name="tel_hp" value="" onchange="Shopping.checkDoubleHP(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Rekening Bank</label><br/>
	                <select class="form-list" id="pilBank" name="pilBank">
	                 <?php
	                    //print_r($listBank['arrayData']);
	                    foreach($listBank['arrayData'] as $dta) {
	                    	echo "<option value=\"$dta->bankid\">$dta->description</option>";
	                    }
	                 ?>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No Rekening</label><br/>
	                <input type="text" id="no_rek" name="no_rek" value="" />
	            </div>
	            <div class="clearfix"> </div>
	          </div>
	          <div class="col-md-12">
       
            
            	<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjut<i class="fa fa-arrow-right"></i></button><br />
              </div>
        <!-- next button-->
        </div>  
	        </form>
        </div>
       </div>
        <!-- Delivery Options--> 
      </div> <!-- End formCart1-->
      <div id="divCheckOut"></div> 
      <div id="afterPayment"></div>
    </div>
</div>
<!--Checkout Wrapper-->

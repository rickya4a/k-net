
<div class="col-lg-12 col-xs-12 nopadding">
    <h1>Transaksi e-Pay BRI</h1>
    <ol>
      <li>Pastikan anda telah memiliki User ID Internet Banking BRI dan sudah mendaftarkan mTOKEN sebelum melakukan pembayaran menggunakan e-Pay BRI langsung dari website kami </li>
      <li>Untuk mendapatkan User ID Internet Banking BRI, silahkan melakukan registrasi melalui ATM BRI terdekat</li>
      <li>Untuk mendaftarkan mTOKEN, silahkan melakukan registrasi finansial di cabang BRI terdekat</li>
      <li>Pembayaran menggunakan e-Pay BRI akan di proses secara online, saldo rekening BRI anda akan didebet secara otomatis sesuai jumlah pembelanjaan anda</li>
      <li>Transaksi anda akan dibatalkan jika pembayaran tidak diselesaikan dalam 2 jam</li>
    </ol>
    <form action="<?php echo site_url('reg/pay/bri/epay'); ?>" method="POST" id="payment-form">
      <!--<button id="submit-button" type="submit">Pay</button>-->
      <button id="submit-button" class="btn1 btn2 btn-primary1" style="margin-top:15px;" type="submit">Proses Pembayaran</button>
    </form>
</div>
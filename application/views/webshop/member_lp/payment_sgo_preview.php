<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Payment Preview&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Preview Pembayaran&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->
<?php
  $biaya = getTotalSKandShipForLP();
  //print_r($biaya);
  $totPayAndShip = $biaya['total_pay'];
  $freeship = $biaya['freeship'];
  $tot_discount = $biaya['tot_discount'];
  $shipping_jne_info = $this->session->userdata('shipping_jne_info');
?>
<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">Preview Pembayaran <?php echo $bankDesc; ?></div>
            <div class="col-lg-12 col-xs-12 nomargin">
            	
                <p class="p2 nopadding" style="font-size:16px;">
                   <?php
                      //print_r($res);
                      //$shipping_jne_info = $this->session->userdata('shipping_jne_info');
                      echo "<table width=70% align=center border=0>";
                      echo "<tr><td align=right>Tipe Pembayaran       :</td><td align=right>".$bankDesc."</td></tr>";
					  echo "<tr><td align=right>Total Pembelanjaan    :</td><td align=right>".$res[0]->total_pay."</td></tr>";
					  //echo "<tr><td align=right>Biaya Kirim           :</td><td align=right>".$shipping_jne_info['price']."</td></tr>";
					  if($freeship == "1") {
	                  	if($tot_discount > 0) {
	                  		echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
	                  		echo "<tr><td align=right>Disc Biaya Kirim        :</td><td align=right>Rp. -(".number_format($tot_discount, 2, ",", ".").")</td></tr>";
	                  	} else {
	                  		echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. 0</td></tr>";
	                  	}
	                  	 
	                  } else {
	                  	 echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
	                  }
                      echo "<tr><td align=right>Biaya Connectivity    :</td><td align=right>".$charge_connectivity."</td></tr>";
                      echo "<tr><td align=right>Biaya Administrasi    :</td><td align=right>".$charge_admin."</td></tr>";
					  $total_semua = $totPayAndShip  + $charge_connectivity + $charge_admin;
					  echo "<tr><td align=right>Total                 :</td><td align=right>".$total_semua."</td></tr>";
                      echo "</table>";
					  $total_all = $res[0]->total_pay + $charge_connectivity;
                   ?>
                </p>
                <p>
                	<?php
                		//print_r($jne);
					?>
                </p>
                <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">
                	
                	<input type="hidden" id="total_all" value="<?php echo $total_all; ?>" />
                    <button class="btn2 btn2 btn-primary1" onclick="submitdataXX()">
                        <i class="fa fa-arrow-right"></i>
                        <span>Lanjut ke Pembayaran</span>
                    </button>
                    <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
                </div>	
            </div>
		</div>	
	</div>
</div>
 
<script type="text/javascript">
    
  function submitdataXX() {
     var total_all = $("#total_all").val();
		
 	 var data = {
					key : "<?php echo $temp_orderidx;?>",
					paymentId : "<?php echo $temp_paymentIdx;?>",
					paymentAmount : total_all,
					//backUrl : "http://www.k-net.co.id/pay/sgo/finish/dev/<?php echo $temp_paymentIdx;?>",
					//backUrl : encodeURIComponent("http://www.k-net.co.id/memberlp/sgo/finish/dev/<?php echo $temp_paymentIdx;?>"),
					backUrl : "<?php echo $backURL;?>",
					bankCode : "<?php echo $bankCode; ?>",
					bankProduct: "<?php echo $bankDesc; ?>"
			    },
		sgoPlusIframe = document.getElementById("sgoplus-iframe");
				
		if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
		SGOSignature.receiveForm();
					
		
}
</script>       
<!-- Login Form --> 
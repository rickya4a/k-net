<!-- detail for bank transfer-->     
    
    <!-- detail for bank transfer-->
    
    <!-- detail for credit card
    <div class="col-lg-12 col-xs-12 nopadding">-->
    <form action="<?php echo site_url('reg/pay/cc'); ?>" method="POST" id="payment-form">	
        <div class="register-top-grid">                            
            <div>
            	<span>Nomor Kartu<label>*</label></span>
             	<input type="text" id="card-number" name="card-number" /> 
            </div>
           <div>
                <span>Security Code<label>*</label></span>
                 <input type="text" id="card-cvv" name="card-cvv"/> 
            </div>
            <div>
            	<span>Expiration Month<label>*</label></span>
                <select class="form-list" id="card-expiry-month" name="card-expiry-month">
                <?php
                    $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                    for($y=1;$y<=12;$y++)
                    {
                        if($y==date("mm"))
                        { 
                            $pilih="selected";
                        }
                        else
                        {
                            $pilih="";
                        }
                        
                        echo("<option value=\"".sprintf("%02s",$y)."\" $pilih>$bulan[$y]</option>"."\n");
                    }
                ?>
                  <!--<option value="Januari" selected>Januari</option>
                  <option value="Februari">Februari</option>
                  <option value="Maret">Maret</option>-->
                </select>
            </div>
            <div>
            	<span>Expiration Year<label>*</label></span>
                <select class="form-list" id="card-expiry-year" name="card-expiry-year">
                <?php
                    $now=date("Y");
                      for ($a=$now;$a<=$now+9;$a++)
                        {
                            echo "<option value='".$a."'>".$a."</option>";
                        }
                    ?>
                  <!--<option value="2015" selected>2015</option>
                  <option value="2016">2016</option>
                  <option value="2017">2017</option>-->
                </select>
            </div>
            <?php
	          $total_payment = getTotalPaymentSK("cc");
	        ?>
            <div>
            	<input id="token_id" type="hidden" name="token_id">
            	<!--<input class="submit-button" id="pay_cc" type="submit" value="Proses Pembayaran">-->
            	<button class="btn1 btn2 btn-primary1 submit-button" id="pay_cc" type="submit">Proses Pembayaran</button>
            </div>	
            
        </div>
     </form>   
    <!-- </div>
    detail for credit card-->
    <script type="text/javascript">
      $(function() {
      	  // Sandbox URL
        //Veritrans.url = "https://api.sandbox.veritrans.co.id/v2/token";
        Veritrans.url = "https://api.veritrans.co.id/v2/token";
        // TODO: Change with your client key.
        //Veritrans.client_key = "VT-client-01vcCDtMihq4F3Ab";
        //Veritrans.client_key = "VT-client-01vcCDtMihq4F3Ab";
        Veritrans.client_key = "VT-client-ePYw4sw8JNlHbB6w";
        var card = function() {
            return {
                "card_number": $("#card-number").val(),
                "card_exp_month": $("#card-expiry-month").val(),
                "card_exp_year": $("#card-expiry-year").val(),
                "card_cvv": $("#card-cvv").val(),
                "secure": true,
                "gross_amount": <?php echo $total_payment; ?>
            }
        };
        
        function callback(response) {
                    console.log(response);
                    if (response.redirect_url) {
                        console.log("3D SECURE");
                        // 3D Secure transaction, please open this popup
                        openDialog(response.redirect_url);
                        
                        //$("#paymentDiv").hide();
                        //$("#checkoutDiv").html(null);
                        //$("#checkoutDiv").load(response.redirect_url);
                        var xx = response.token_id;
                        //alert("masuk di 3D Secure "+xx);
						//window.location.href = response.redirect_url;
                    }
                    else if (response.status_code == "200") {
                        console.log("NOT 3-D SECURE");
                        // Success 3-D Secure or success normal
                        closeDialog();
                        // Submit form
                        $("#token_id").val(response.token_id);
                        var xx = response.token_id;
                        //alert("masuk disini "+xx);
                        //window.location.href = response.redirect_url;
                        $("#payment-form").submit();
                    }
                    else {
                        // Failed request token
                        console.log(response.status_code);
                        alert(response.status_message);
                        $(".submit-button").removeAttr("disabled", "disabled");
                    }
                }

                function openDialog(url) {
                    $.fancybox.open({
                        href: url,
                        type: "iframe",
                        autoScale: true,
                        width: "100%",
                        height: "100%",
                        closeBtn: false,
                        modal: true,
                        autoDimensions: true
                    });
                }
                
             
                function closeDialog() {	
                    $.fancybox.close();
                }
                
                
                
                var btnPayCC = $('#choosePaymentDiv').find('#pay_cc');
	             btnPayCC.click(function(e){
	               console.log("SUBMIT");
	               e.preventDefault();
	               $(this).attr("disabled", "disabled");
	               Veritrans.token(card, callback);
	               return false; 
	              //alert("ok");
	             });
      });		
    </script>
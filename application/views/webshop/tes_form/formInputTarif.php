<div class="container">
    <div align="left">
        <form class="form-horizontal" id="formNoResi">
            <fieldset>
                <div>
                    <div class="controls">
                        <label>Kota Asal</label>
                        <select id="origin" name="origin" style="width: 200px">

                        </select>
                    </div>

                    <label>&nbsp</label>

                    <div class="controls">
                        <label>Kota Tujuan</label>
                        <select id="destination" name="destination" style="width: 200px">

                        </select>
                    </div>

                    <label>&nbsp</label>

                    <div class="controls">
                        <label>Berat</label>
                        <input type="text" id="weight" name="weight">
                    </div>

                    <label>&nbsp</label>
                    <label>&nbsp</label>

                    <div class="controls"  id="inp_btn">
                        <input tabindex="3" type="button" id="btn_input_user" class="btn btn-primary .submit" name="save" value="Submit" onclick="tekan()" />
                    </div>
                </div> <!-- end control-group -->

            </fieldset>

            <div class="result">

            </div>
            <!--        <div class="result2"></div>-->

        </form>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>


<script>

    function tekan(){

        //var isi = $(All.get_active_tab() + " #formNoResi").serialize();
        var origin = $('#origin').val();
        var destination = $('#destination').val();
        var weight = $('#weight').val();

//        console.log("dari : "+origin);
//        console.log("ke : "+destination);
//        console.log("berat : "+weight);

        All.set_disable_button();
        //$("#formNoResi").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');

        $.ajax({
            url : All.get_url('api/sicepat/tarif'),
            type : 'POST',
            data : {origin : origin, destination : destination, weight : weight},

            success : function(data) {

                All.set_enable_button();

                //$("#result").show();
                $(".result").html(data);
            },

            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }

        });
    }

</script>

<script>
    $(document).ready(function(){

        $.ajax({
            url : All.get_url('api/sicepat/origin/list'),
            type : 'GET',
            dataType : 'json',
            //data : dt,

            success : function(data) {

                //console.log(data.sicepat.results);
                var arr_result = data.sicepat.results;

                $.each(arr_result, function(k, v) {

                    //console.log(v.origin_name);
                    var origin_name = v.origin_name;
                    var origin_code = v.origin_code;

                    var x = document.getElementById("origin");

                    var option = document.createElement("option");

                    option.text = origin_name;
                    option.value = origin_code;

                    x.add(option);
                });
            }
//
//            error: function (xhr, ajaxOptions, thrownError) {
//                alert(thrownError + ':' +xhr.status);
//                All.set_enable_button();
//            }

        });

        $.ajax({
            url : All.get_url('api/sicepat/destination/list'),
            type : 'GET',
            dataType : 'json',
            //data : dt,

            success : function(data) {

                //console.log(data.sicepat.results);
                var arr_result = data.sicepat.results;

                $.each(arr_result, function(k, v) {

                    //console.log(v);
                    var province = v.province;
                    var city = v.city;
                    var subdistrict = v.subdistrict;
                    var destination_code = v.destination_code;

                    var x = document.getElementById("destination");

                    var option = document.createElement("option");

                    option.text = subdistrict+", "+city+", "+province;
                    option.value = destination_code;

                    x.add(option);

                });
            }
//
//            error: function (xhr, ajaxOptions, thrownError) {
//                alert(thrownError + ':' +xhr.status);
//                All.set_enable_button();
//            }

        });
    });
</script>
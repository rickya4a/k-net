
<style>   
.soldout{
   content: " ";
    z-index: 10;
    display: block;
    position: absolute;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.5);
    }
    
p.soldtext {
    text-align: center;
    position: relative;
    color: white;
    background: #00BCD4;
    top: 77%;
    margin: 0 10px;
    padding: 10px 0;}

    input.greysold{
    border-color:#dcdcdc;
    background: #dcdcdc;
}
</style>

 <style>
.halaman
 {
 margin:10px;
 font-size:11px;
 }

.halaman a
 {

padding:7px 12px;
 background:#DDE6DF;
 -moz-border-radius:5px;
 -webkit-border-radius:5px;
 font-size:13px;
}
</style>

<div class="mens-toolbar">
	<div class="sort">
   		<div class="sort-by">
            <span class="title_header">
                <i class="fa fa-list-ol"></i> Product Categories
            </span>
     	</div>
	</div>

    <div class="pages">   
        <!--<div class="limiter visible-desktop">
            <label>Showing 1-12 of 347 results</label>
        </div>
        <ul class="dc_pagination dc_paginationA dc_paginationA06">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
        </ul>-->
      
        <div class="halaman">
            <?php echo $halaman;?>
        </div>
        
        <div class="clearfix"></div>
    </div>
	<div class="clearfix"></div>		
</div>
                
<div class="product_box1">
<?php 
if(isset($prodCat))
{
    $n = 1;
    foreach($prodCat['arrayData'] as $row)
    {
       $prdcd = $row->prdcd;
        $imgName = $row->img_url;
        $prdcdnm = $row->prdnm;
        $prdcdcat = $row->prdcdcat;
        $prdcdcatnm = $row->prdnmcatnm;
        $westPrice = $row->price_w;
        $eastPrice = $row->price_e;
        $bv = $row->bv;     
        $weight = $row->weight;
		$ecomm_status = $row->ecomm_status;
	    $is_discontinue = $row->is_discontinue; 
        //echo "dimari ".$dt->prdcd;

?>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 voffset4">
    	<?php   
             if($ecomm_status == '2' && $is_discontinue == '0')
                {
                    echo "<div class=\"soldout\"> <p class=\"soldtext\"> Stok Tidak Tersedia </div>";
                }
           ?>
        <div class="view view-first">
        	<h5 class="title-desc">
            	<a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)"><?php echo $prdcdnm;?></a>
            	<span class="label label-success">BV <?php echo "".number_format($bv,0,".",".");?></span>
            </h5>
            <a id="<?php echo $prdcd;?>" onclick="Shop.get_detailProds(<?php echo $n;?>)">
                <!--<img src="<?php echo "".base_url()."assets/images/".$imgName."";?>" class="img-responsive" alt=""/>-->
                <img src="<?php echo $folderPrd.$imgName; ?>" class="lazy img-responsive" alt=""/>
            </a>                    
            <div class="tab_desc1">                
                <div class="col-xs-12 col-lg-12 nopadding">
                	<div class="col-xs-6 col-lg-6"> <h5 class="green">Rp. <?php echo "".number_format($westPrice,0,".",".");?></h5>
                    	<p class="p3">Harga Wilayah A</p>
                	</div>
               		<div class="col-xs-6 col-lg-6"> <h5 class="green">Rp. <?php echo "".number_format($eastPrice,0,".",".");?></h5>
                    	<p class="p3">Harga Wilayah B</p>
                	</div>	
                </div>
                <input type="hidden" name="prdcd" id="<?php echo "prdcd".$n; ?>" value="<?php echo $prdcd;?>"/>
                <input type="hidden" name="prdnm" id="<?php echo "prdnm".$n; ?>" value="<?php echo $prdcdnm;?>"/>
                <input type="hidden" name="bv" id="<?php echo "bv".$n; ?>" value="<?php echo $bv;?>"/>
                <input type="hidden" name="westPrice" id="<?php echo "westPrice".$n; ?>" value="<?php echo $westPrice;?>"/>
                <input type="hidden" name="eastPrice" id="<?php echo "eastPrice".$n; ?>" value="<?php echo $eastPrice;?>"/>
                <input type="hidden" name="weight" id="<?php echo "weight".$n; ?>" value="<?php echo $weight;?>"/>
                                <?php
                  $usr = getUsername(); 
					//echo getUsername(); 
				  /*
				   
				   if($usr != NULL) {
				  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onClick=\"Shopping.addToCart($n)\" value=\"Masukan Ke Keranjang\"/>";
				  } else {
				  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan Ke Keranjang\"/>";
				  }
				   * 
				   */
				   
				   
				 //edit hilal @2018-12-21 show soldout
				 if($ecomm_status == '2' && $is_discontinue == '0') {
                    echo "<input type=\"text\" class=\"greysold\"  value=\"\"/>";
                }elseif($ecomm_status == '1' && $is_discontinue == '0') {
					if($usr != NULL ) {
    				  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukan ke keranjang\"/>";
					  } else {
					  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
					  }	
				}	
                ?>
            </div>
        </div>                    
    </div>
    <?php
        $n++;
        }
        }else{
            echo "no data";
        }
    ?>
</div>
              
<div class="pages">   
 <!--<div class="limiter visible-desktop">
   <label>Showing 1-12 of 347 results</label>
 </div>
 <ul class="dc_pagination dc_paginationA dc_paginationA06">
	<li><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
  </ul>-->
   <!--<div class="halaman">
    <?php //echo $halaman;?>
   </div>-->
  <div class="clearfix"></div>
</div>

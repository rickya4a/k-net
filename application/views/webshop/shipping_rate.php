<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;Shipping Rates&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Shopping rate-->
<div class="row vpullset4 voffset4">
	<div class="container">
    
    	<!--Left sidebar-->
    	<div class="col-md-3 col-sm-12">          	
            <div class="list_grid">
		<ul style="margin-top:0px;">
                	<li><a href="<?php echo site_url('static/about');?>">About K-Link</a></li>
                	<li><a href="<?php echo site_url('static/why');?>">Why K-Link</a></li>
                	<li><a href="<?php echo site_url('static/how-to-order');?>">How To Order</a></li>
                    	<li><a href="<?php echo site_url('static/replacement');?>">Replacment Policy</a></li>
                	<li><a href="<?php echo site_url('static/delivery');?>">Delivery</a></li>
                </ul>				
	   </div>            
    	</div>
        <!--Left sidebar--> 
        
        <!--right-->
    	<div class="col-md-9 col-sm-12"> 
        
        	<div class="mens-toolbar">
                <span class="title_header">
                    <i class="fa fa-truck"></i> Shipping Rate
                </span>
            </div>
            
            <p class="desc">
            	Jika memilih pengiriman ke alamat pribadi maka biaya pengiriman akan dibebankan kepada pembeli, sesuai dengan berat / volume barang serta jarak dari stockist ke alamat pribadi Anda.
            </p>
            <br>                
           	<ol class="desc-li" type="1">
            	<li>Tentukan Area Stockis, contoh; Jawa Tengah, Jawa Barat,</li>
                <li>Pilih stockist yang Anda inginkan</li>
                <li>Penentuan Volume / Berat secara otomatis dihitung oleh system</li>
                <li>Masukan alamat pengiriman kota serta Wilayah</li>
                <li>Secara otomatis biaya pengiriman akan dikalkulasikan</li>
            </ol>
                     
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- Shopping rate-->

<?php
	if($listing == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>


<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Promo Olive Oil & K-Honey&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
        
         <span class="red"> &nbsp;List Promo Olive Oil & K-honey&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<div class="row vpullset4" style="min-height: 450px;">
    <span id="formCekTransaksi">
        <div class="container">
            <div class="col-md-12">
            <?php
            
                $currentMonth = date('F');
                $lastMonth = date('F', strtotime($currentMonth . " last month"));
                $year = date('Y');
                $bnsperiod = $year." / ".$lastMonth." / 01";
            ?>
                <table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
                    <tr>
                    	<th colspan="6" style="text-align: center;">LISTING PROMO K-OLIVE OIL & K-HONEY Maret 2017 s/d <?php echo $lastMonth."  ".$year;?></th>
                    </tr>
                    <tr>
                    	<th style="text-align: center;">No</th>
                    	<th style="text-align: center;">ID Member</th>
                    	<th style="text-align: center;">Nama Member</th>
                    	<th style="text-align: center;">Total Olive Oil</th>
                    	<th style="text-align: center;">Total K-Honey</th>
                    	
                    </tr>
                    <?php
                       $i = 1;
                       foreach($listing as $data) {
                        
                        if($i <= 50){
                            $colors = "green";
                        }else{
                            $colors = "black";
                        }
                    ?>
                       	<tr style="font: bold;">
                		  <td style="text-align: right;color:  <?php echo $colors;?>;"><?php echo $i;?>&nbsp;</td>
                		  <td style="text-align: center;color: <?php echo $colors;?>;"><?php echo$data->dfno;?></td>
                		  <td style="text-align: center;color: <?php echo $colors;?>;"><?php echo$data->fullnm;?></td>
                		  <td style="text-align: center;color: <?php echo $colors;?>;"><?php echo$data->jml_o;?></td>
                		  <td style="text-align: center;color: <?php echo $colors;?>;"><?php echo$data->jml_h;?></td>
                		<tr>  
                	<?php	
                        $i++; 
                       }
                    ?>
                </table>
            </div>
        </div>
    </span>
</div>

<?php
}
?>    
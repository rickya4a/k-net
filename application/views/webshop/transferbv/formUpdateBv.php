<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="row vpullset4">
	<div class="container">
		<?php
			if(empty($hasil)) {
				echo "Tidak Ada Data";
			} else {
		?>
		<form id="formUpdateBV">
			<div class="col-md-12">
      <p>
      <strong>Catatan:</strong> Proses BV dilakukan setiap 3(tiga) jam dan dimulai dari jam 12.00
      </p>
				<div class="col-lg-12 col-xs-12 cart_header">
					<div class="col-lg-12">
						Form Update BV
					</div>
				</div>
				<div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
					<div class="col-md-3 col-xs-12">
						No Trx
					</div>
					<div class="col-md-9 col-xs-12">
						<?php echo $hasil[0]->trcd; ?>
					</div>
					<div class="col-md-3 col-xs-12">
						Total Pay / BV
					</div>
					<div class="col-md-9 col-xs-12">
						<?php echo $hasil[0]->ndp. " / " .$hasil[0]->nbv; ?>
					</div>
					<div class="col-md-3 col-xs-12">
						ID Member
					</div>
					<div class="col-md-9 col-xs-12">
						<?php echo $hasil[0]->dfno." / ".$hasil[0]->fullnm; ?>
						<input type="hidden" id="idmember" name="idmember" value="<?php echo $hasil[0]->dfno; ?>" />
						<input type="hidden" id="nmmember" name="nmmember" value="<?php echo $hasil[0]->fullnm; ?>" />
						<input type="hidden" id="notrx" name="notrx" value="<?php echo $hasil[0]->trcd; ?>" />
						<input type="hidden" id="tipetrx" name="tipetrx" value="<?php echo $tipe; ?>" />
					</div>

					<div class="col-md-3 col-xs-12">
						ID Penerima BV
					</div>
					<div class="col-md-9 col-xs-12">
						<input class="form-list" type="text" id="new_idmember" onchange="cariNama(this.form.new_idmember)" name="new_idmember"
						 value="<?php echo $hasil[0]->dfno; ?>" />
						<input type="button" value="Check ID Member" onclick="cariNama(this.form.new_idmember)" />
					</div>

					<div class="col-md-3 col-xs-12">
						Nama Penerima BV
					</div>
					<div class="col-md-9 col-xs-12">
						<input readonly="readonly" class="form-list" type="text" id="new_nmmember" name="new_nmmember" value="<?php echo $hasil[0]->fullnm; ?>" />
						<input type="hidden" id="success_upd" name="success_upd" value="0" />
					<div class="g-recaptcha" data-sitekey="6LdNPJoUAAAAADZanK9Yl2GwUsSvWG75erJbe9MK"></div>
						<input type="button" value="Update BV" onclick="updateBV()" />
					</div>
					<!-- <div class="col-md-3 col-xs-12">
						<p>Masukkan kode berikut</p>
					</div>
					<div class="col-md-3 col-xs-12">
						<span><?php echo $captcha_img['image']; ?></span>
						<input type="text" name="captcha" class="uppercase" autocomplete="off" /> 
					</div> -->
				</div>
		</form>
		<?php }?>
		<div>
			<input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="backAfterUpdate()" name="submit"
			 id="submit" />
		</div>
	</div>
</div>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
	<div class="container">
		<li class="home">
			<a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt="" /></a>&nbsp;
			&nbsp; <span>&gt;</span>
		</li>
		<li class="home">&nbsp;
			Sales&nbsp;&nbsp;
			<span>&gt;</span>
		</li>
		<li class="home">
			<span class="red"> &nbsp;Cek Transaksi&nbsp;&nbsp;</span>
		</li>

	</div>
</ul>
<!-- Breadcrumb -->

<!--form -->
<div class="row vpullset4" style="min-height: 415px;">
	<span id="formCekTransaksi">
		<div class="container">
			<div id="salesMember">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3 text-center">
						<form id="cektransaksi" method="POST" onkeypress="return event.keyCode != 13;">
							<div class="track-order-grid">
								<div>
									<span>Pencarian Transaksi Member</span>
									<!--<span>
				  <?php 
					//echo $period[0]->update_bv."<br />";
					$tgl = date("F Y", strtotime($period[0]->update_bv));
				   ?>-->
	</span>
	<span>Bonus Month</span>
	<select class="required form-list" style="margin-bottom:10px; width:100%" id="bnsmonth" name="bnsmonth">
		<option value="<?php echo $period[0]->update_bv; ?>">
			<?php echo $tgl; ?>
		</option>
	</select>

</div>
<div>
	<button class="btn1 btn2 btn-primary1" type="button" onClick="searchTransaksi()" id="submit">Cek Transaksi <i class="fa fa-arrow-right"></i></button>
	<!--<input class="btn1 btn2 btn-primary1" type="button" value="Cek Transaksi" onClick="Sales.searchTransaksi();" name="submit" id="submit"/>-->
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<div id="hasilPencarian"></div>
</span>
<div id="detail_trans"></div>
</div><!-- form -->
<!--Checkout Wrapper-->
<script>
	function searchTransaksi() {
		var tahun = $('#tahun').val();
		/*var objRegthn  =  /^[a-z0-9]+$/i.exec(tahun);*/
		if (tahun == "") {
			alert('Field Tahun Bonus Tidak Boleh Kosong');
			//All.set_disable_button();
		}
		/*else if(!objRegthn){
		 alert('Format Tahun Harus berupa Angka');
		 }*/
		else {
			All.set_disable_button();
			$("#hasilPencarian").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
			$("input[type=button]").attr('disabled', 'disabled');
			$.post(All.get_url("sales/pindahbv/search"), $("#cektransaksi").serialize(), function (hasil) {
				All.set_enable_button();

				$("#hasilPencarian").html(null);
				$("#hasilPencarian").html(hasil);
			});
		}

	}

	function showFormUpdateBv(param) {
		All.set_disable_button();
		$("#formCekTransaksi").hide();
		$("#detail_trans").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');

		$.ajax({
			url: All.get_url('sales/pindahbv/form'),
			type: 'POST',
			data: {
				trx: param
			},
			success: function (data) {
				All.set_enable_button();
				$("#detail_trans").html(null);
				$("#detail_trans").html(data);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				All.set_enable_button();
			}
		});
	}

	function cariNama(param) {
		var idx = param.value;
		$.ajax({
			url: All.get_url('sales/pindahbv/showfullnm'),
			type: 'POST',
			data: {
				new_idmember: idx
			},
			dataType: "json",
			success: function (data) {
				All.set_enable_button();
				if (data.response == "false") {
					alert(data.message);
					$("#new_nmmember").val(null);
				} else {
					var arrayData = data.arrayData;
					$("#new_nmmember").val(arrayData[0].fullnm);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				All.set_enable_button();
			}
		});
	}

	function updateBV() {
		All.set_disable_button();
		//$("#formCekTransaksi").hide();
		//$("#detail_trans").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');

		$.ajax({
			url: All.get_url('sales/pindahbv/update'),
			type: 'POST',
			dataType: 'json',
			data: $("#formUpdateBV").serialize(),
			success: function (data) {
				All.set_enable_button();
				alert(data.message);
				if (data.response == "true") {
					$("#formUpdateBV #success_upd").val("1");
				} else {
					$("#formUpdateBV #success_upd").val("0");
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				All.set_enable_button();
			}
		});
	}

	function backAfterUpdate() {
		var success_upd = $("#formUpdateBV #success_upd").val();
		$("#detail_trans").html(null);
		if (success_upd == "1") {
			$("#formCekTransaksi").show();
			searchTransaksi();
		} else {
			Sales.backToSearch();
		}
	}
</script>
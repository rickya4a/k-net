<style>
#start_date, #end_date, #thn {
	cursor: pointer;
}
</style>

<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
    .tg th{font-family:Arial, sans-serif;text-align:center;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
    .tg .tg-0pky{border-color:inherit;text-align:center;vertical-align:top}
</style>
<div class="row vpullset4">
    <div class="container">
        <!--right-->
        <div class="col-md-12" id="mainDiv">
            <!-- Track Order -->
            <!-- tracking content header-->
            <div class="col-lg-12 col-xs-12 cart_header">
                <div class="col-lg-12">
                    Daftar Transaksi Pembayaran
                </div>
            </div>
            <form id="frm_input" name="frm_input" method="post" action="<?php echo base_url('sales/cek_trans_payment'); ?>">
            <div class="col-lg-12 col-xs-12 cart_content_noborder" style="font-size:14px;">

                <!--line 1-->
                <div class="col-lg-3 col-xs-3">
                    ID Member
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID(); ?>
                </div>
                <!--line 1-->
                <!--line 2-->
                <div class="col-lg-3 col-xs-3">
                    Nama Member
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserName(); ?>
                </div>
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <div class="col-lg-3 col-xs-3">
                   <strong>Cari Berdasarkan</strong>
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <div class="col-lg-3 col-xs-3">
                    Periode Tgl
                </div>
                <div class="col-lg-9 col-xs-9">
                    <input type="text" name="start_date" id="start_date" value="" placeholder="contoh: 31-12-2018" autocomplete="off" />
                    &nbsp;S/D&nbsp;
                    <input type="text" name="end_date" id="end_date" value="" placeholder="contoh: 31-12-2018" autocomplete="off" />
                </div>
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <div class="col-lg-3 col-xs-3">
                    Bonus Month
                </div>
                <div class="col-lg-9 col-xs-9">
                    <!--<input type="text" id="from" name="from" placeholder="DD" value="<?php echo date("d/m/Y"); ?>" />
                   &nbsp;sampai&nbsp;
                   <input type="text" id="to" name="to" placeholder="to" value="<?php echo date("d/m/Y"); ?>" />-->
                    <select id="bln" name="bln">
                        <option value="">-Pilih Bulan-</option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>

                    </select>
                    <input type="text" name="thn" id="thn" value="" placeholder="contoh: 2019" autocomplete="off" />
                    <input type="hidden" id="recruitid" name="recruitid" value="<?php echo getUserID(); ?>" />
					<!--<input type="button" id="btn_recruit" onclick="Shopping.getListTransPayment()" value="Cari Daftar Transaksi" />-->
                    <input type="submit" id="btn_recruit" value="Cari Daftar Transaksi" />

                    <!-- <button class="btn1 btn-primary1" type="button" onclick="Shopping.getListTransPayment()">Cari Daftar Transaksi</button> -->
                </div>
                <!--line 2-->
            </div>
</form>
            <!-- table result-->
            <!-- result header-->
            <!--<div class="col-lg-12 col-xs-12 cart_header">
                <div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-2 col-xs-2">No. Token</div>
                <div class="col-lg-3 col-xs-2">Tgl Trans</div>
                <div class="col-lg-1 col-xs-2">Bonus Month</div>
                <div class="col-lg-2 col-xs-2">Total Bayar</div>
                <div class="col-lg-3 col-xs-3">Status</div>
            </div>-->

            <div id="listRecruit" class="col-lg-12 col-xs-12 cart_content_noborder" style="height:300px; overflow-y: auto;">
                <table class="tg" width="100%">
                    <tr >
                        <th>No</th>
                        <th>No. Token</th>
                        <th>Tgl Trans</th>
                        <th>Bonus Month</th>
                        <th>Total Bayar (Rp)</th>
                        <th>Status</th>
                    </tr>
                <?php
                $no=0;
                if($arr != null) {
                    foreach ($arr as $row) {
                        $no++;
                        $orderno= $row->orderno;
                        $trans= $row->datetrans;
                        $date_trans=date("d-m-Y H:i:s",strtotime($trans));
                        $bns_month= $row ->bonusmonth;
                        $total_pay= $row->nom_pay;

                        $flag_pay= $row->flag_payment;
                        $flag_prod= $row->flag_production;
                        $exp= $row->date_expired;
                        $date_exp=date("Y-m-d H:i:s",strtotime($exp));
                        $today=date("Y-m-d H:i:s");

                        /*echo $date_exp;
                        echo "<br>";
                        echo $today;*/
                        if($flag_pay == '1' AND $flag_prod == '1'){
                            $status= "SUKSES";
                            $color="#208612";
                        }else{
                            if($today > $date_exp){
                                $status = "KADALUWARSA";
                                $color="#ff0a18";
                            }elseif($flag_pay == '1' AND $flag_prod == 'null'){
                                $status = "MENUNGGU RECONCILE";
                                $color="#FD6A18";
                            }
                            else{
                                $status= "MENUNGGU PEMBAYARAN";
                                $color="#100e66";
                            }
                        }

                        ?>
                        <tr>
                            <td align="center"><?php echo $no?></td>
                            <td class="tg-0pky"><?php echo $orderno ?></td>
                            <td class="tg-0pky"><?php echo $date_trans?></td>
                            <td class="tg-0pky"><?php echo $bns_month?></td>
                            <td class="tg-0pky"><?php echo "Rp ".number_format($total_pay,0,',','.');?></td>
                            <td class="tg-0pky"><span  style="color: <?php echo $color?>"><?php echo $status?></span></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </div>
                </table>

        </div>

    </div>
    <div class="col-md-12" id="divUpd">

    </div>
</div>

<script>
$("#thn").datepicker({
	format: "yyyy",
	viewMode: "years",
	minViewMode: "years"
})
$("#start_date, #end_date").datepicker({
	format: "dd-mm-yyyy"
})
</script>
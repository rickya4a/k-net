<?php
$pdf = new FPDF('P', 'mm', 'A4');
$pdf -> Open();
$pdf -> SetFillColor(255, 255, 255);
// background = biru muda
$pdf -> SetTextColor(0, 0, 0);
//	font color = black
$pdf -> SetDrawColor(0, 0, 0);
// border 	   = brown
$pdf -> SetLineWidth(.1);
// border thickness = 0.3 (maybe in e.m)

$pdf -> AddPage();
$pdf -> Image("assets/images/logo.jpg", 6, 5, 85, 25);
//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
//$pdf->Cell(50,10,$img,'L',0,0,'R');
$pdf -> SetFont('Courier', '', 8);
$thnCheck = '2017';

$lebarCell = 4;
$pdf -> Ln();
$garis = 0;
$titleCol1 = 50;
$titleCol2 = 90;
$titleCol3 = 25;
$titleCol4 = 25;

$prdCodeCol = 40;
$prdDescCol = 80;
$qtyCol = 30;
$amtCol = 40;

$total = $titleCol1 + $titleCol2 + $titleCol3 + $titleCol4;

$jumrec = count($result);
$jum = 1;
foreach ($arr as $dta) {

//  jika data blm fix dari pak toto
//	if ('bnsperiod' != '01/02/2018') {
//	}

	$pdf->Image("assets/images/logo.jpg", 6, 5, 85, 25);
	//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
	//$pdf->Cell(50,10,$img,'L',0,0,'R');
	//$pdf -> SetFont('Courier', '', 8);
	//$pdf->SetFillColor(204, 228, 180);
	$pdf->SetFont('Courier', 'B', 10);
	$pdf->SetXY(4, 40);

	$title_in = "PROMO VOUCHER";
	$title_term = "promo voucher";
	$title_mal = "BAUCER PROMO";
	$sd1 = "*Less: the product voucher amount   :";

	$pdf->SetLeftMargin(4);
	$pdf->Ln();

	$pdf->Cell($total, $lebarCell, $title_in, $garis, 0, 'C', true);
	$pdf->Ln();

	//$pdf -> Cell($total, $lebarCell, $title_mal, $garis, 0, 'C', true);
	//$pdf -> Ln();

	$pdf->SetFont('Courier', '', 10);

	$pdf->Cell($total, $lebarCell, "No Voucher      : $dta[VoucherNo]", $garis, 0, 'L', true);
	$pdf->Ln();

	$pdf->Cell($total, $lebarCell, "Tgl Cetak       : $dta[createdt]", $garis, 0, 'L', true);
	$pdf->Ln();

	$pdf->Cell($total, $lebarCell, "Member Code     : $dta[dfno]", $garis, 0, 'L', true);
	$pdf->Ln();

	$pdf->Cell($total, $lebarCell, "Name            : $dta[Fullnm]", $garis, 0, 'L', true);
	$pdf->Ln();

	$pdf->Cell($total, $lebarCell, "Bonus Period    : $dta[bnsperiod]", $garis, 0, 'L', true);
	$pdf->Ln();
	$pdf->Ln();

	$pdf->Cell($prdCodeCol, 5, "Product Code", 1, 0, 'C', true);
	$pdf->Cell($prdDescCol, 5, "Product Description", 1, 0, 'C', true);
	$pdf->Cell($qtyCol, 5, "Quantity", 1, 0, 'C', true);
	//$pdf->Cell($amtCol,5,"Amount",1,0,'C',true);
	$pdf->Ln();

	foreach ($dta['product'] as $tes) {
		$pdf->Cell($prdCodeCol, 5, $tes['prdcd'], 1, 0, 'C', true);
		$pdf->Cell($prdDescCol, 5, $tes['prdnm'], 1, 0, 'L', true);
		$pdf->Cell($qtyCol, 5, $tes['qtyord'], 1, 0, 'C', true);
		//$pdf->Cell($amtCol,5,"",1,0,'C',true);
		$pdf->Ln();
	}

	$pdf->Ln();


	$garis = "";
	for ($x = 1; $x <= 90; $x++) {
		$garis .= "-";
	}
	$pdf->Cell(120, 5, $garis, 0, 0, 'L', true);
	$pdf->Ln();
	$pdf->Cell(120, 5, "I certify that the products redeemed is correct and in good condition", 0, 0, 'L', true);
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	if ($dta['claimstatus'] == "1") { //jika sudah diklaim
		$pdf->Cell(120, 5, "Redeemed At " . $dta['loccd'] . ' -- ' . $dta['sc_fullnm'] . " (" . $dta['claim_date'] . ")", 0, 0, 'L', true);

		//$this->RotatedText(35,190,'W a t e r m a r k   d e m o',45);
	}
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();

	$pdf->Cell(85, 5, "====================================== ", 0, 0, 'L', true);
	$pdf->Cell(55, 5, "                   ", 0, 0, 'L', true);
	$pdf->Cell(55, 5, "======================= ", 0, 0, 'L', true);

	$pdf->Ln();
	$pdf->Cell(85, 5, "Stockist's/Officer's Stamp & Signature", 0, 0, 'L', true);
	$pdf->Cell(55, 5, "", 0, 0, 'L', true);
	$pdf->Cell(55, 5, "Distributor's signature ", 0, 0, 'L', true);
	$pdf->Ln();
	$pdf->Cell(195, 5, "" . $dta['dfno'] . " - " . $dta['Fullnm'], 0, 0, 'R', true);
	$jum++;

	$pdf->SetFont('Courier', 'B', 8);
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Cell(100, 5, "TERMS AND CONDITIONS", 0, 0, 'L', true);
	$pdf->Ln();
	$pdf->Cell(100, 5, "   1.  Voucher hanya digunakan oleh Distributor yang bersangkutan.", 0, 0, 'L', true);
	$pdf->Ln();
	//$pdf->Cell(100,5,"       However it cannot be exchanged for cash.",0,0,'L',true);
	//$pdf -> Ln();
	$pdf->Cell(100, 5, "   2.  Voucher hanya bisa ditukarkan dengan produk non bv yang telah ditentukan oleh PT K-Link Indonesia.", 0, 0, 'L', true);
	$pdf->Ln();
	//$pdf->Cell(100,5,"    However it cannot be exchanged for cash",0,0,'L',true);
	$pdf->Cell(100, 5, "   3.  Voucher ini diterbitkan hanya pada masa tertentu", 0, 0, 'L', true);
	$pdf->Ln();
	$pdf->Cell(100, 5, "   4.  Batas akhir penukaran voucher adalah " . $dta['expiredt'], 0, 0, 'L', true);
	$pdf->Ln();

	if ($jum <= $jumrec) {
		$pdf->AddPage();
	}
}






	


/*
//start hilal

function RotatedText($x, $y, $txt, $angle)
{
	//Text rotated around its origin
	$this->Rotate($angle,$x,$y);
	$this->Text($x,$y,$txt);
	$this->Rotate(0);
}

//var $angle=0;
function Rotate($angle,$x=-1,$y=-1){
    if($x==-1)
        $x=$this->x;
    if($y==-1)
        $y=$this->y;
    if($this->angle!=0)
        $this->_out('Q');
    $this->angle=$angle;
    if($angle!=0)
    {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
    }
}

function _endpage(){
    if($this->angle!=0)
    {
        $this->angle=0;
        $this->_out('Q');
    }
    parent::_endpage();
}
//end hilal
*/

$title = "ada.pdf";
$pdf -> SetTitle($title);
$pdf -> Output();
?>  
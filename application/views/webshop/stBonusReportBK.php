
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="#" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Sales&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Statement Bonus&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--form -->
<div class="row vpullset4" style="min-height: 410px;">
<span id="formCekBonus">
	<div class="container">
    <div id="bonusMember">
      <div class="col-md-12"> 
        <div class="col-md-6 col-md-offset-3 text-center">  
	        <form id="cekBonus" method="POST" method="POST" target="_blank" action="<?php echo site_url('bonus/action');?>">
	            <div class="track-order-grid">
                <div>
                    <span>Statement Bonus</span>
                  <span>Bonus Month</span>	                
	                <select  class="required form-list" style="margin-bottom:10px; width:100%" id="month" name="month">
  	                <?php
                      /*foreach($bnsmonth['bonusmonth'] as $row){
                        echo "<option value = \"$row->bnsmonth\">$row->bnsmonth</option>";
                      }*/
                      $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                      for($y=1;$y<=12;$y++)
                      {
                          if($y==date("mm"))
                          { 
                              $pilih="selected";
                          }
                          else
                          {
                              $pilih="";
                          }
                          
                          echo("<option value=\"".sprintf("%02s",$y)."\" $pilih>$bulan[$y]</option>"."\n");
                      }
                    ?>
	                </select>
                   <?php
                        $thn = date("Y");
                    ?>
                    <input type="text" name="year" size="14" placeholder="Tahun Bonus" id="year" class="only-number" value="<?php echo $thn;?>"/>
	              </div>
                  
                <div>
                    <input type="hidden" id="idmember" name="idmember" value="kosong"/>
                  <!--<input class="btn1 btn2 btn-primary1" type="button" value="Cek Downline" onClick="Sales.searchDonwline();" name="submit" id="submit"/>-->
                  <input id="submit" type="submit" name="submit" value="Find" class="btn1 btn2 btn-primary1"/> 
                </div>
              </div>	           
	        </form>          
        </div>        
      </div>
    </div>
  </div>
  <div id="pencarianBonus"></div>
  </span>
  <div id="detail_downline"></div>
</div><!-- form -->    
<!--Checkout Wrapper-->
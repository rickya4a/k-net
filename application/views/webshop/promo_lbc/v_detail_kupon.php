<!-- jQuery -->
<script src="<?php echo site_url();?>vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo site_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Datatables -->
<script src="<?php echo site_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
    WebFont.load({
        google: {
            families: ["Lato:100,300,400,700,900","Karla:regular","Cookie:regular"]
        }
    });
</script>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Promo LBC Trip to Bangkok&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Cek Info&nbsp;&nbsp;</span>
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Detail Cek Info&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->
<div class="row vpullset4" style="min-height: 410px;">

    <div class="container">

        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-12">
                <h2 style="color: #d6156c;">Promo LBC Trip to Bangkok</h2>
            </div>
        </div>

        <div class="col-lg-12 col-xs-12 cart_content" style="font-size:18px;">

            <!--line 1-->
            <div class="col-lg-3 col-xs-3">
                ID Member
            </div>
            <div class="col-lg-9 col-xs-9">
                : <?php echo $dfno;?>
            </div>
            <!--line 1-->
            <!--line 3-->
            <div class="col-lg-3 col-xs-3">
                Nama Member
            </div>
            <div class="col-lg-9 col-xs-9">
                : <?php echo $fullnm; ?>
            </div>
            <!--line 2-->
            <div class="col-lg-3 col-xs-3">
                Bonus Periode
            </div>
            <div class="col-lg-9 col-xs-9">
                : <?php echo $bnsperiod?>
            </div>
            <!--line 2-->
        </div>

        <div id="formUtama">
            <table width="70%" class="main table table-hover table-bordered">
                <thead>
                <tr style="background-color:#FFB6C1">
                    <th>No</th>
                    <th>BV</th>
                    <th>BV LBC</th>
                    <th>BV Group</th>
                    <th>Total NA</th>
                    <th>BV NA</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
                <tr>
                    <td colspan="6"><input type="button" name="submit" value="<< Kembali" onclick="history.back()" /></td>
                </tr>
            </table>

        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>
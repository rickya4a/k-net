<!-- jQuery -->
<script src="<?php echo site_url();?>vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo site_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Datatables -->
<script src="<?php echo site_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
    WebFont.load({
        google: {
            families: ["Lato:100,300,400,700,900","Karla:regular","Cookie:regular"]
        }
    });
</script>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Promo LBC Trip to Bangkok&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">

            <span class="red"> &nbsp;Cek Info&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->
<div class="row vpullset4" style="min-height: 410px;">

    <div class="container">

        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-12">
                <h2 style="color: #d6156c;">Promo LBC Trip to Bangkok</h2>
            </div>
        </div>

        <!--line 1-->
        <div class="col-lg-3 col-xs-3">
            ID Member
        </div>
        <div class="col-lg-9 col-xs-9">
            : <?php echo $dfno;?>
        </div>
        <!--line 1-->
        <!--line 3-->
        <div class="col-lg-3 col-xs-3">
            Nama Member
        </div>
        <div class="col-lg-9 col-xs-9">
            : <?php echo $fullnm; ?>
        </div>
        <!--line 2-->
        <div class="col-lg-3 col-xs-3">
            Peringkat
        </div>
        <div class="col-lg-9 col-xs-9">
            : &nbsp;
        </div>
        <!--line 2-->

        <!--line 2-->
        <div class="col-lg-3 col-xs-3">
            &nbsp;
        </div>
        <div class="col-lg-9 col-xs-9">
            &nbsp;
        </div>

        <div id="formUtama">
            <form>
                <table width="100%" class="table table-bordered" style="width: 100%" id="datatable">
                    <thead>
                    <tr  style="background-color:#FFB6C1">
                        <th>No</th>
                        <th>Periode</th>
                        <th>Tgl. Join</th>
                        <th>Total Pembelanjaan</th>
                        <th>Total BV</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $i = 1;
                    foreach($cekLBC2 as $dt) {
                        //$warna=($i % 2 == 0) ? "white" : "#FFE4C4";

//                        if($dt->TBV >= 600 && $dt->TBV_LBC >= 200){
//
//                            $status= "SUCCESS";
//                            $color= "#008000";
//                        }else{
//                            $status= "FAILED";
//                            $color= "#8B0000";
//                        }

                        //print_r($bln);

//                        if($dodol = '10-2018'){
//                            $dodol = "Oktober - 2018";
//                        }
//                        elseif($dodol = '11-2018'){
//                            $dodol = "November - 2018";
//                        }
//                        elseif($dodol = '12-2018'){
//                            $dodol = "Desember - 2019";
//                        }

                        ?>
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td align="center">
                                <input type="hidden" id="bnsperiod<?php echo $i; ?>" value="<?php echo date("Y-m-d", strtotime($dt->bnsperiod));?>"/>
                                <input type="hidden" id="dfno<?php echo $i; ?>" value="<?php echo $dfno; ?>"/>
                                <?php

                                $dodol = date("m-Y", strtotime($dt->bnsperiod));

                                //echo $qwe;
                                echo "<a onclick=\"getDetailLBC($i)\" class='btn btn-info'>".$dodol."</a>";
                                ?>
                            </td>
                            <td align="center">
                                <?php
                                echo date("d-m-Y", strtotime($dt->jointdt))
                                ?>
                            </td>
                            <td align="center"><?php echo number_format($dt->qDP, 0, ".", ".") ?></td>
                            <td align="center"><?php echo number_format($dt->qBV, 0, ".", ".") ?></td>
                            <td align="center">
                                <font>
                                    <b>
                                        <?php //echo $status ?>
                                    </b>
                                </font>
                            </td>
                            <td>
                                <?php
                                echo "<a onclick=\"getRekrutanLBC($i)\" class='btn btn-info'>Cek Rekrutan</a>";
                                ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </form>
        </div>

        <div id="detailLBC">

        </div>

        <div id="detailRekrutanLBC">

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>

<script>

    function getDetailLBC(param){

        var dfno = $("#dfno" +param).val()
        var bnsperiod = $("#bnsperiod" +param).val()

        $.ajax({
            url: All.get_url("det_lbc"),
            type: 'POST',
            data: {dfno: dfno, bnsperiod : bnsperiod},
            success:
                function(data){
                    $("#formUtama").hide();
                    $("#detailLBC").html(null);
                    $("#detailLBC").html(data);
                }
        });
    }

    function getRekrutanLBC(param){

        var dfno = $("#dfno" +param).val()
        var bnsperiod = $("#bnsperiod" +param).val()

        $.ajax({
            url: All.get_url("det_rekrutan_lbc"),
            type: 'POST',
            data: {dfno: dfno, bnsperiod : bnsperiod},
            success:
                function(data){
                    $("#formUtama").hide();
                    $("#detailLBC").html(null);
                    $("#detailRekrutanLBC").html(null);
                    $("#detailRekrutanLBC").html(data);
                }
        });

    }

    function backToFormLBC() {

        $("#detailLBC").html(null);
        $("#detailRekrutanLBC").html(null);
        $("#formUtama").show();
    }

</script>
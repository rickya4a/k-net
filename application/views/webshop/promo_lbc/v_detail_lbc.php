<?php
if($result != null)
{
?>

<br>
<button id="bold" onclick="backToFormLBC()"><i class='fa fa-arrow-left'> Kembali</i></button>
<br>
<table width="100%" class="table table-striped" id="datatable">

    <thead>
    <tr>
        <th>Kode Transaksi</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>Total Produk</th>
        <th>Harga</th>
        <th>BV</th>
        <th>Total Harga</th>
        <th>Total BV</th>
        <th>Jenis Transaksi</th>
        <th>Jenis Produk</th>
    </tr>
    </thead>
    <tbody>

        <?php

        $i = 1;
        foreach($result as $dt) {

//            if($dt->flag = "1") {

                ?>

                <tr>
                    <td>
                        <?php echo $dt->trcd ?>
                    </td>

                    <td>
                        <?php echo $dt->zPRDCD ?>
                    </td>

                    <td>
                        <?php echo $dt->prdnm ?>
                    </td>

                    <td align="center">

                        <?php
                        $a = number_format($dt->zQTY, 0, '.', '');
                        echo number_format($a, 0, ".", ".");
                        ?>
                    </td>

                    <td>
                        <?php

                        $a = number_format($dt->zdp, 0, '.', '');
                        echo number_format($a, 0, ".", ".");

                        ?>
                    </td>

                    <td>
                        <?php echo number_format($dt->zBV, 0, '.', ''); ?>
                    </td>

                    <td>
                        <?php

                        $a = number_format($dt->xDP, 0, '.', '');
                        echo number_format($a, 0, '.', '.');

                        ?>
                    </td>

                    <td>
                        <?php echo number_format($dt->xBV, 0, '.', ''); ?>
                    </td>

                    <td>
                        <?php
                        if (($dt->flag2) == "1") {
                            echo "MampirKak";
                        } else {
                            echo "Reguler";
                        }
                        ?>
                    </td>

                    <td>
                        <?php
                        if (($dt->flag) == "0") {
                            echo "Prod. Reg";
                        } else {
                            echo "Prod. LBC";
                        }
                        ?>
                    </td>

<!--                    --><?php
//                    if(($dt->flag) == "0"){
//                    ?>
<!--                        <td>-->
<!--                            Prod. Reg-->
<!--                        </td>-->
<!--                        --><?php
//                    }else {
//                        ?>
<!--                        <td bgcolor="#ff69b4">-->
<!--                            Prod. LBC-->
<!--                        </td>-->
<!--                        --><?php
//                    }
//                        ?>
                </tr>

                <?php
            //}
            $i++;
        }
    }

    ?>
<!--    <tr>-->
<!--        <td colspan="10"><input type="button" name="submit" value="<< Kembali" onclick="backToFormLBC()" /></td>-->
<!--    </tr>-->
    </tbody>
</table>
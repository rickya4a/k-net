<?php
if($result != null)
{
    ?>
<br>
<input type="button" name="submit" value="<< Kembali" onclick="backToFormLBC()" />
<br>
<table width="100%" class="table table-striped" id="datatable">

    <thead>
    <tr>
        <th>No.</th>
        <th>ID Rekrutan</th>
        <th>Nama Rekrutan</th>
        <th>Tgl Join</th>
        <th>Jenis Kelamin</th>
        <th>Status Rekrutan</th>
        <th>Total BV</th>
    </tr>
    </thead>
    <tbody>

    <?php
        $no = 1;
        foreach($result as $dt) {

            ?>

            <tr>

                <td>
                    <?php echo $no; ?>
                </td>
                <td>
                    <?php echo $dt->DFNO ?>
                </td>

                <td>
                    <?php echo $dt->FULLNMR ?>
                </td>

                <td align="center">
                    <?php echo date("d-m-Y", strtotime($dt->JOINTDT)) ?>
                </td>

                <td align="center">
                    <?php
                        if(($dt->JSEX ) == "F"){
                            echo "Perempuan";
                        }else{
                            echo "Laki-laki";
                        }
                    ?>

                </td>

                <td align="center">
                    <?php
                    if(($dt->STATUS_REKRUTAN) == "0"){
                        echo "Reguler";
                    }else{
                        echo"LBC";
                    }
                    ?>
                </td>

                <td align="center">

                    <?php
                    $a = number_format($dt->TBV_R, 0, '.', '');
                    echo number_format($a, 0, ".", ".");
                    ?>
                </td>

            </tr>


            <?php
            $no++; }
            ?>

            <tr>
                <td colspan="6" align="center">
                    total BV seluruh
                </td>

                <td align="center">
                    <?php
                        foreach($tot_bv as $data){

                            echo $data->total_bv;
                        }
                    ?>
                </td>
            </tr>
    </tbody>
</table>
    <?php
    }else{
    ?>
    <br>
        <button id="bold" onclick="backToFormLBC()"><i class='fa fa-arrow-left'> Kembali</i></button>
    <br>

    <div class="col-lg-12 col-xs-12 cart_header">
        <div class="col-lg-12">
            <h1 style="color: red;">Dalam Proses Perhitungan Ulang</h1>
        </div>
    </div>

        <?php
    }
    ?>


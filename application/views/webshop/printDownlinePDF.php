<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
        
        /*$pdf->SetFont('Helvetica','B',8);
        $pdf->Image("assets/images/logo.jpg",10,5, 85, 30);
        $pdf->Ln();*/

        //$pdf->SetXY(10,50);
        $titleCol1 = 28;
        $titleCol2 = 92;
        $titleCol3 = 35;
        $titleCol4 = 35;
        $lebarCell = 5;
    
        $pdf->SetFont('Helvetica','B',14);
        $pdf->Cell(190,5,"LAPORAN JARINGAN MEMBER",0,0,'C',true); 
        $pdf->Ln();
        $pdf->Ln();
    
        $pdf->SetFont('Helvetica','B',8);
        $pdf->SetFillColor(219,215,217);
        foreach($cekDownline['pbv'] as $dta2)
        {
            
           $pdf->Cell($titleCol1,$lebarCell,"ID MEMBER",1,0,'L',true); 	// its similar with TD in HT
           $pdf->Cell($titleCol2,$lebarCell,$dta2->dfno,1,0,'L',true);
           $pdf->Cell($titleCol3,$lebarCell,"TAHUN PERIOD",1,0,'L',true);
           $pdf->Cell($titleCol4,$lebarCell,$dta2->year,1,0,'L',true);
           $pdf->Ln();
           $pdf->Cell($titleCol1,$lebarCell,"NAMA MEMBER",1,0,'L',true); 	// its similar with TD in HT
           $pdf->Cell($titleCol2,$lebarCell,$dta2->fullnm,1,0,'L',true);
           $pdf->Cell($titleCol3,$lebarCell,"BULAN PERIOD",1,0,'L',true);
           $pdf->Cell($titleCol4,$lebarCell,$dta2->month,1,0,'L',true);
           $pdf->Ln();
        }
        
        $pdf->Ln();
        $pdf->SetFillColor(219,215,217);
        $pdf->Cell($titleCol1,$lebarCell,"LEVEL",1,0,'C',true);
        $pdf->Cell($titleCol2,$lebarCell,"PPV",1,0,'C',true);
        $pdf->Cell($titleCol3,$lebarCell,"GBV",1,0,'C',true);
        $pdf->Cell($titleCol4,$lebarCell,"PGBV",1,0,'C',true);
        $pdf->Ln();
        
        
        foreach($cekDownline['pbv'] as $dta)
        {
            $pdf->SetFont('Helvetica','',7);
            $pdf->SetFillColor(255,255,255);
            
			/*
			if($dta->level == "1" or $dta->level == "2"){
				$rank = "m";
			}elseif($dta->level == "3"){
				$rank = "sm";
			}elseif($dta->level == "4"){
				$rank = "sp";
			}elseif($dta->level == "5"){
				$rank = "am";
			}elseif($dta->level == "6"){
				$rank = "M";
			}elseif($dta->level == "7"){
				$rank = "SM";
			}elseif($dta->level == "8"){
				$rank = "RM";
			}elseif($dta->level == "9"){
				$rank = "EM";
			}elseif($dta->level == "10"){
				$rank = "DM";
			}elseif($dta->level == "11"){
				$rank = "CM";
			}elseif($dta->level == "12"){
				$rank = "CA";
			}elseif($dta->level == "13"){
				$rank = "SCA";
			}elseif($dta->level == "14"){
				$rank = "RCA";
			}
			*/
			
            $pdf->Cell($titleCol1,$lebarCell,$dta->level,1,0,'C',true);
            $pdf->Cell($titleCol2,$lebarCell,number_format($dta->ppv,0,".",","),1,0,'C',true);
            $pdf->Cell($titleCol3,$lebarCell,number_format($dta->popv,0,".",","),1,0,'C',true);
            $pdf->Cell($titleCol4,$lebarCell,number_format($dta->pgpv,0,".",","),1,0,'C',true);
        }
    
       $pdf->Ln();
       $pdf->Ln();
       $pdf->SetFont('Helvetica','B',8);
       $pdf->SetFillColor(219,215,217);
       $pdf->Cell(190,5,"INFO JARINGAN",1,0,'C',true);
       $pdf->Ln();
    
       $pdf->Cell(55,$lebarCell,"ID DISTRIBUTOR",1,0,'C',true);
       $pdf->Cell(65,$lebarCell,"NAMA",1,0,'C',true);
       $pdf->Cell(10,$lebarCell,"LEVEL",1,0,'C',true);
       $pdf->Cell(15,$lebarCell,"PPV",1,0,'C',true);
       $pdf->Cell(30,$lebarCell,"GBV",1,0,'C',true);
       $pdf->Cell(15,$lebarCell,"PGBV",1,0,'C',true);
       $pdf->Ln();
       
        foreach($cekDownline['gbv'] as $dta1)
        {
            $pdf->SetFont('Helvetica','',7);
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(55,$lebarCell,$dta1->dfno,1,0,'L',true);
            $pdf->Cell(65,$lebarCell,$dta1->fullnm,1,0,'L',true);
            $pdf->Cell(10,$lebarCell,$dta1->level,1,0,'R',true);
            $pdf->Cell(15,$lebarCell,number_format($dta1->ppv,0,".",","),1,0,'R',true);
            $pdf->Cell(30,$lebarCell,number_format($dta1->popv,0,".",","),1,0,'R',true);
            $pdf->Cell(15,$lebarCell,number_format($dta1->pgpv,0,".",","),1,0,'R',true);
            $pdf->Ln();
        }
        
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(250,10,62);
        $pdf->SetFont('Helvetica','',7);
        
        $pdf->Cell(190,5," PLEASE TAKE NOTE :-",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"1. The Personal (PPV), Personal Group PV (PGPV) and Group Performance (GPV) status for current bonus month,",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"    as displayed on your screen is based on the actual sales submission received by our Sales Counter and the EDP department.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"2. The PPV, PGPV and GPV status is subjected to the sales submitted by the Mobile Stockist, stockist and Super Stockist.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"3. Do not use the information as displayed to gauge your expected Personal Ranking.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"4. All information displayed is in Malaysian BV.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"5. Please allow 48 hours (working hours only) for the information to be updated.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFillColor(219,215,217);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','B',7);
        //$pdf->Cell(190,5,"BV MEMBER REPORT",1,0,'C',true); 
        $pdf->Ln();
        
        $pdf->Output();
?>
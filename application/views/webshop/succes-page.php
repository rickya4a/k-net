<!DOCTYPE HTML>
<html>
<head>
<title>K-Link WebStore</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.min.css" rel="stylesheet" >

<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/responsiveslides.min.js"></script>
</head>
<body>

<!-- Header -->
<?php include('header.php') ?>
<!-- Header -->

<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red">Order Successfull </span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      		<div class="account_grid">			   
	        	<div class="col-md-12 login-right">
                    <span class="title_header">
                        <i class="fa fa-check"></i> Pesanan Berhasil
                    </span>
                    <p><br>Pesanan Anda telah berhasil di proses, Silahkan cek email Anda untuk melihat detail dan invoice mengenai pesanan Anda</p>                    
			   	</div>	
			   	<div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>        
<!-- Login Form -->
   


<!-- Footer --> 
<?php include('footer.php') ?>
<!-- Footer --> 

</body>
</html>		
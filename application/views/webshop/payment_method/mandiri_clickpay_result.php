<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Shipping&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Konfirmasi Pembayaran&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">Status Transaksi Mandiri ClickPay</div>
            <div class="col-lg-12 col-xs-12 nomargin">
                <p class="p2 nopadding" style="font-size:16px;">
                   <?php
                   $status = "Gagal";
                    if($result->transaction_status == "settlement")
                    {
                    	$status = "Berhasil";
					}	
                      echo "Status Transaksi anda : $status"."<br />";
                      echo "Tipe Pembayaran       : Mandiri ClickPay"."<br />";
                      echo "ID Transaksi          : ".$trans['orderno'].""."<br />";
                      echo "ID Member             : ".$trans['idmember'].""."<br />";
                      echo "No kartu              : $result->masked_card"."<br />";
                      echo "No Order              : $result->order_id"."<br />";
                      echo "Jumlah transaksi      : Rp."."".number_format($result->gross_amount, 2, ".", ",").""."<br />";
                      echo "Waktu transaksi       : $result->transaction_time"."<br />";
					  echo "No Resi               : $jne";
                   ?>
                </p>
                <p>
                	<?php
                		//print_r($jne);
					?>
                </p>
                <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">
                	<!--<a href="<?php //echo base_url('shop/product'); ?>">Klik disini untuk kembali ke halaman utama</a>-->
                    <a class="btn1 btn2 btn-primary1" href="<?php echo base_url('shop/product'); ?>">
                        <i class="fa fa-arrow-left"></i>
                        <span>Klik disini untuk kembali ke halaman utama</span>
                    </a>
                </div>	
            </div>
		</div>	
	</div>
</div>        
<!-- Login Form --> 
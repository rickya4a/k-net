<?php
$pdf=new FPDF('P','mm', 'A4');
$pdf->Open();
$pdf->SetFillColor(255,255,255); // background = biru muda
$pdf->SetTextColor(0,0,0);	 //	font color = black
$pdf->SetDrawColor(0,0,0); // border 	   = brown
$pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)

$pdf->AddPage();
$pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
//$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
//$pdf->Cell(50,10,$img,'L',0,0,'R');
$pdf->SetFont('Courier','', 8);
$thnCheck = '2017';

function NumberToMonth($bln)
{
    switch($bln)
    {
        case 1 :
            $jum = "january";
            break;
        case 2 :
            $jum = "february";
            break;
        case 3 :
            $jum = "march";
            break;
        case 4 :
            $jum = "april";
            break;
        case 5 :
            $jum = "may";
            break;
        case 6 :
            $jum = "june";
            break;
        case 7 :
            $jum = "july";
            break;
        case 8 :
            $jum = "august";
            break;
        case 9 :
            $jum = "september";
            break;
        case 10 :
            $jum = "october";
            break;
        case 11 :
            $jum = "november";
            break;
        case 12 :
            $jum = "december";
            break;
    }
    return $jum;
}

$lebarCell = 4;
$pdf->Ln();
$pdf->SetXY(6,50);
$titleCol1 = 30;
$titleCol2 = 100;
$titleCol3 = 35;
$titleCol4 = 35;


$pdf->SetFont('Arial','B', 8);

{
    //        $pdf->Cell(198,5,"BONUS STATEMENT REPORT $bulan $dta2->bonusyear",1,0,'C', true);
    $pdf->SetFont('Arial','B', 20);
    $pdf->SetTextColor(0,0,255);

    $pdf->Cell(200,9,"Formulir Claim Pemenang",0,0,'C', true);
    $pdf->Ln();

    $pdf->SetFont('Arial','B', 12);
    $pdf->Cell(192,9,"Initiative Anniversary Challenge",0,0,'C', true);

}
$pdf->SetLeftMargin(6);
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);

$pdf->Cell(192,5,"DATA PRIBADI",0,0,'C', true);

$pdf->SetTextColor(0,0,0);

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','', 8);

$pdf->Ln();
$pdf->Ln();
foreach($res1 as $dxd)
{
    $pdf->Cell($titleCol1,$lebarCell,"ID Distributor ",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->dfno)){
        $dfno=$dxd->dfno;
    }else
        $dfno="";

    $arr1=str_split(str_pad($dfno, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();

    $pdf->Cell($titleCol1,$lebarCell,"Nama ",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->fullnm)){
        $fullnm=$dxd->fullnm;
    }else
        $fullnm="";

    $arr1=str_split(str_pad($fullnm, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();

    $pdf->Cell($titleCol1,$lebarCell,"Nomor KTP ",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->idno)){
        $idno=$dxd->idno;
    }else
        $idno="";

    $arr1=str_split(str_pad($idno, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();


    $pdf->Cell($titleCol1,$lebarCell,"Handphone ",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->tel_hp)){
        $tel_hp=$dxd->tel_hp;
    }else
        $tel_hp="";

    $arr1=str_split(str_pad($tel_hp, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();

    $pdf->Cell($titleCol1,$lebarCell,"Email ",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->email)){
        $email=$dxd->email;
    }else
        $email="";

    $arr1=str_split(str_pad($email, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();


    $pdf->Cell($titleCol1,$lebarCell,"Alamat ",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->add1)){
        $add1=$dxd->add1;
    }else
        $add1="";
    $arr1=str_split(str_pad($add1, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();

    $pdf->Cell($titleCol1,$lebarCell,"",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->addr2)){
        $addr2=$dxd->addr2;
    }else
        $addr2="";
    $arr1=str_split(str_pad($addr2, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();

    $pdf->Cell($titleCol1,$lebarCell,"",0,0,'L',true); 	// its similar with TD in HT
    if(isset($dxd->addr3)){
        $addr3=$dxd->addr3;
    }else
        $addr3="";
    $arr1=str_split(str_pad($addr3, 30));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();

    $pdf->Cell($titleCol1,$lebarCell,"",0,0,'L',true); 	// its similar with TD in HT
    for($i=0;$i<14;$i++){
        $pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
    }
    $pdf->Cell(30, $lebarCell, "Kode Pos", 0, 0, 'L', true);
    if(isset($dxd->postcd)){
        $postcd=$dxd->postcd;
    }else
        $postcd="";
    $arr1=str_split(str_pad($postcd, 6));
    foreach ($arr1 as $x) {
        $pdf->Cell(5, $lebarCell, $x, 1, 0, 'L', true);
    }
    $pdf->Ln();    $pdf->Ln();
}

$pdf->SetFont('Arial','B', 8);
$pdf->SetFillColor(255,255,0);
$pdf->SetTextColor(0,0,255);

$pdf->Cell(192,5,"REKENING BANK",0,0,'C', true);
$pdf->Ln();    $pdf->Ln();

$pdf->SetTextColor(0,0,0);

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','', 8);

$pdf->Cell($titleCol1,$lebarCell,"Rekening Bank ",0,0,'L',true); 	// its similar with TD in HT

    $pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
$pdf->Cell(10, $lebarCell, "BCA", 0, 0, 'L', true);
$pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
$pdf->Cell(15, $lebarCell, "Mandiri", 0, 0, 'L', true);
$pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
$pdf->Cell(11, $lebarCell, "BNI", 0, 0, 'L', true);
$pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
$pdf->Cell(11, $lebarCell, "BRI", 0, 0, 'L', true);
$pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
$pdf->Cell(13, $lebarCell, "Lain2", 0, 0, 'L', true);
for($i=0;$i<13;$i++){
    $pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
}
$pdf->Ln();    $pdf->Ln();
$pdf->Cell($titleCol1,$lebarCell,"Nomor Rekening ",0,0,'L',true); 	// its similar with TD in HT
for($i=0;$i<30;$i++){
    $pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
}
$pdf->Ln();    $pdf->Ln();

$pdf->Cell($titleCol1,$lebarCell,"Nama ",0,0,'L',true); 	// its similar with TD in HT
for($i=0;$i<30;$i++){
    $pdf->Cell(5, $lebarCell, "", 1, 0, 'L', true);
}
$pdf->Ln();    $pdf->Ln();
$pdf->Cell(200, $lebarCell, "", 'T', 0, 'L', true);
$pdf->Ln();
$pdf->Cell(25,$lebarCell,"- Kami tunduk dengan semua persyaratan dan keputusan PT K-Link Indonesia.",0,0,'L',true); 	// its similar with TD in HT

$pdf->Ln();
$pdf->Cell(25,$lebarCell,"- Kami bersedia untuk dilakukan verifikasi atas semua kebenaran data yang kami ajukan.",0,0,'L',true);

$pdf->Ln();
$pdf->Cell(25,$lebarCell,"- Semua data hasil downline hasil rekruter adalah benar dan tidak fiktif.",0,0,'L',true);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(25,$lebarCell,"............................. tgl:..../..../20...",0,0,'L',true);
$pdf->Ln();

$pdf->Cell(25,$lebarCell,"Yang Menyatakan, ",0,0,'L',true);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Courier','', 6);

$pdf->Cell(25,$lebarCell,"Materai 6000",0,0,'R',true);
$pdf->Ln();
$pdf->Ln();

$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Courier','', 8);

$pdf->Cell(31,$lebarCell,"Nama dan Peringkat",'T',0,'L',true);

$pdf->SetFillColor(204, 228, 180);
$pdf->SetFont('Courier','B', 8);


$title = "Form_Claim_IAC.pdf";
$pdf->SetTitle($title);
$pdf->Output();

?>
<?php
    $pdf=new FPDF('P','mm', 'A4');
    $pdf->Open();
    $pdf->SetFillColor(255,255,255); // background = biru muda
    $pdf->SetTextColor(0,0,0);	 //	font color = black
    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
    $pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)
    
    $pdf->AddPage();
    $pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
    //$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
    //$pdf->Cell(50,10,$img,'L',0,0,'R');
    $pdf->SetFont('Courier','', 8);
    $thnCheck = '2017';
    
    function NumberToMonth($bln)
    {
        switch($bln)
        {
            case 1 :
                $jum = "january";
                break;
            case 2 :
                $jum = "february";
                break;
            case 3 :
                $jum = "march";
                break;
            case 4 :
                $jum = "april";
                break;
            case 5 :
                $jum = "may";
                break;
            case 6 :
                $jum = "june";
                break;
            case 7 :
                $jum = "july";
                break;
            case 8 :
                $jum = "august";
                break;
            case 9 :
                $jum = "september";
                break;
            case 10 :
                $jum = "october";
                break;
            case 11 :
                $jum = "november";
                break;
            case 12 :
                $jum = "december";
                break;
        }	   
        return $jum;
    }
    
    $lebarCell = 4; 
    $pdf->Ln();
    $pdf->SetXY(6,50);
    $titleCol1 = 50;
    $titleCol2 = 100;
    $titleCol3 = 35;
    $titleCol4 = 35;
    
	
	$pdf->SetFillColor(204, 228, 180); 
	$pdf->SetFont('Courier','B', 8);
	

	//echo "nilai nya adalah == $tableBaru";

$pdf->SetFillColor(204, 228, 180);
$pdf->SetFont('Courier','B', 8); 

$pdf->Cell(198,5,"Print Bonus Statement",1,0,'C', true);

$titleCol1 = 28;
$titleCol2 = 65;
$titleCol3 = 35;
$titleCol4 = 70;
$pdf->Ln();

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','', 8);


$pdf->SetFont('Courier','BU', 10); //set font to Bold

foreach($res2 as $dta2)
{
    $bulan = ucwords(NumberToMonth($dta2->bonusmonth));
    $pdf->Cell(198,5,"Bonus Statement For $bulan $dta2->bonusyear",0,0,'C', true);
}

	$pdf->SetFont('Courier','BU', 8); //set font to Regular
	
	$pdf->Ln();
	$pdf->Ln();

      $pdf->AddPage('L');
      
      $colDownlineCode = 40;
      $colDownlineName = 99;
      $colprank = 10;
      $colarank = 10;
      $colerank = 10;
      $colACCPGPV = 15;
      $colPPV = 10;
      $colPGPV = 10;
      $colGPV = 16;
      $colPBV = 10;
      $colPGBV = 10;
      $colGBV = 16;
      $colPBVB = 10;
      $colGBVB = 16;

$titleCol1 = 282;

      //$pdf->SetFont('Times','', 9);
      $pdf->Cell($titleCol1,$lebarCell,"DIRECT DOWNLINE SALES INFORMATION",1,0,'C',true);
      $pdf->Ln();
      $pdf->Cell($colDownlineCode,$lebarCell,"Downline Code",1,0,'C',true);
      $pdf->Cell($colDownlineName,$lebarCell,"Downline Name",1,0,'C',true);
      $pdf->Cell($colprank,$lebarCell,"P.Rank",1,0,'C',true);
      $pdf->Cell($colarank,$lebarCell,"A.Rank",1,0,'C',true);
      $pdf->Cell($colerank,$lebarCell,"E.Rank",1,0,'C',true);
      $pdf->Cell($colACCPGPV,$lebarCell,"ACCPGPV",1,0,'C',true);						 
      $pdf->Cell($colPPV,$lebarCell,"PPV",1,0,'C',true);
      $pdf->Cell($colPGPV,$lebarCell,"PGPV",1,0,'C',true);
      $pdf->Cell($colGPV,$lebarCell,"GPV",1,0,'C',true);
      $pdf->Cell($colPBV,$lebarCell,"PBV",1,0,'C',true);
      $pdf->Cell($colPGBV,$lebarCell,"PGBV",1,0,'C',true);
      $pdf->Cell($colGBV,$lebarCell,"GBV",1,0,'C',true);
      $pdf->Cell($colPBVB,$lebarCell,"PBVB",1,0,'C',true);
      $pdf->Cell($colGBVB,$lebarCell,"GBVB",1,0,'C',true); 
      $pdf->Ln(); 
	  if($hasil2 != null) {
	      foreach ($hasil2 as $data2)
	      {
	            $ppv  = number_format($data2->ppv);
	    	 	$pgpv = number_format($data2->pgpv,0,".",",");
	    		$gpv = number_format($data2->gpv,0,".",",");
	    		$pbv = number_format($data2->pbv);
	    		$pgbv = number_format($data2->pgbv,0,".",",");
	    		$gbv = number_format($data2->gbv,0,".",",");
	    		$pbvb = number_format($data2->pbvb,0,".",",");
	    		$gbvb = number_format($data2->gbvb,0,".",",");
	    		$AccPGPV = number_format($data2->AccPGPV,0,".",",");
	            
	          $pdf->Cell($colDownlineCode,$lebarCell,"$data2->distributorcode",1,0,'L',true);
	          $pdf->Cell($colDownlineName,$lebarCell,"$data2->fullnm",1,0,'L',true);
	          $pdf->Cell($colprank,$lebarCell,"$data2->currentrank",1,0,'R',true);
	          $pdf->Cell($colarank,$lebarCell,"$data2->adjustedrank",1,0,'R',true);
	          $pdf->Cell($colerank,$lebarCell,"$data2->effectiverank",1,0,'R',true);
	          $adjust1 = (int)$data2->adjustedrank;
				//echo $adjust1;
				if ($adjust1 < 6)
				{
				  		
	              $pdf->Cell($colACCPGPV,$lebarCell,"$AccPGPV",1,0,'R',true);						  
				}
				else
				{
				  $pdf->Cell($colACCPGPV,$lebarCell,"0",1,0,'R',true);
				}
	          $pdf->Cell($colPPV,$lebarCell,"$ppv",1,0,'R',true);
	          $pdf->Cell($colPGPV,$lebarCell,"$pgpv",1,0,'R',true);
	          $pdf->Cell($colGPV,$lebarCell,"$gpv",1,0,'R',true);
	          $pdf->Cell($colPBV,$lebarCell,"$pbv",1,0,'R',true);
	          $pdf->Cell($colPGBV,$lebarCell,"$pgbv",1,0,'R',true);
	          $pdf->Cell($colGBV,$lebarCell,"$gbv",1,0,'R',true);
	          $pdf->Cell($colPBVB,$lebarCell,"$pbvb",1,0,'R',true);
	          $pdf->Cell($colGBVB,$lebarCell,"$gbvb",1,0,'R',true);
	          
	          $pdf->Ln();  
	      }
	  }
      foreach($hasil3 as $data3)
    	{
        	 $spgpv = number_format("$data3->pgpv",0,".",",");
        	 $sgpv = number_format("$data3->gpv",0,".",",");
        	 $spbv = number_format($data3->pbv);
        	 $spgpv = number_format("$data3->pgbv",0,".",",");
        	 $sgbv = number_format("$data3->gbv",0,".",",");
        	 $spbvb =number_format("$data3->pbvb",0,".",",");
        	 $sgbvb = number_format("$data3->gbvb",0,".",",");
             $AccPGPV = number_format("$data3->AccPGPV",0,".",",");
             $ppv = number_format("$data3->ppv",0,".",",");
             
             $pdf->Cell($colDownlineCode,$lebarCell,"$data3->distributorcode",1,0,'L',true);
              $pdf->Cell($colDownlineName,$lebarCell,"$data3->fullnm",1,0,'L',true);
              $pdf->Cell($colprank,$lebarCell,"$data3->currentrank",1,0,'R',true);
              $pdf->Cell($colarank,$lebarCell,"$data3->adjustedrank",1,0,'R',true);
              $pdf->Cell($colerank,$lebarCell,"$data3->effectiverank",1,0,'R',true);
              $adjust1 = (int)$data3->adjustedrank;
    			//echo $adjust1;
    			if ($adjust1 < 6)
    			{
    			  		
                  $pdf->Cell($colACCPGPV,$lebarCell,"$AccPGPV",1,0,'R',true);						  
    			}
    			else
    			{
    			  $pdf->Cell($colACCPGPV,$lebarCell,"0",1,0,'R',true);
    			}
              $pdf->Cell($colPPV,$lebarCell,"$ppv",1,0,'R',true);
              $pdf->Cell($colPGPV,$lebarCell,"$spgpv",1,0,'R',true);
              $pdf->Cell($colGPV,$lebarCell,"$sgpv",1,0,'R',true);
              $pdf->Cell($colPBV,$lebarCell,"$spbv",1,0,'R',true);
              $pdf->Cell($colPGBV,$lebarCell,"$spgpv",1,0,'R',true);
              $pdf->Cell($colGBV,$lebarCell,"$sgbv",1,0,'R',true);
              $pdf->Cell($colPBVB,$lebarCell,"$spbvb",1,0,'R',true);
              $pdf->Cell($colGBVB,$lebarCell,"$sgbvb",1,0,'R',true);	    
        }      
        $pdf->Ln();
        $pdf->Ln();
      
      if(isset($devresult))
      {
          $titleCol1 = 100;
          $headCol1 = 15;
          $headCol2 = 25;
          $headCol3 = 30;
          $headCol4 = 30;
          $totalCol = $headCol1 + $headCol2 + $headCol3;
          $allCol = $totalCol + $headCol4;
          
          //$pdf->SetFont('Times','', 9);
          $pdf->Cell($allCol,$lebarCell,"DEVELOPMENT BONUS",1,0,'C',true);
          $pdf->Ln();  
          $pdf->Cell($headCol1,$lebarCell,"Override %",1,0,'C',true);
          $pdf->Cell($headCol2,$lebarCell,"Override BV",1,0,'C',true);
          $pdf->Cell($headCol3,$lebarCell,"Effective Override",1,0,'C',true);
          $pdf->Cell($headCol4,$lebarCell,"Override Amount",1,0,'C',true);
          $pdf->Ln();
          $totalOverrideAmount = 0;
				  foreach($devresult as $dev)
				  {
					  	if($dev->overridep == "S")	{
					  		$ss = "Self";
					  	}
						else {
							$overridep = number_format("$dev->overridep",2,".",",");
							$ss = $overridep." "."%";
						}
						
						if($dev->overridep != "0")
						{
							$overridebv = number_format("$dev->overridebv",2,".",",");
						
							$effectiveoverrate = number_format("$dev->effectiveoverrate",2,".",","); 
							$overrideamount = number_format("$dev->overrideamount",2,".",","); 
						  	
                              $pdf->Cell($headCol1,$lebarCell,"$ss",1,0,'R',true);
                              $pdf->Cell($headCol2,$lebarCell,"$overridebv",1,0,'R',true);
                              $pdf->Cell($headCol3,$lebarCell,"$effectiveoverrate %",1,0,'R',true);
                              $pdf->Cell($headCol4,$lebarCell,"$overrideamount",1,0,'R',true);      
							$totalOverrideAmount +=	$dev->overrideamount;
							$passup = 0;
							$passup2 = 0;
                            $pdf->Ln();
						}
						else {
							$passup	= $dev->overrideamount;
							$passup2 = number_format($passup,2,".",","); 
					   }	
					  
				  }
                  $net = number_format($totalOverrideAmount + $passup,2,".",",");
				  $totalOverride = number_format($totalOverrideAmount,2,".",",");
                  
                  $pdf->Cell($totalCol,$lebarCell,"Gross Total",1,0,'R',true);
                  $pdf->Cell($headCol4,$lebarCell,"$totalOverride",1,0,'R',true);
                  $pdf->Ln();
                  $pdf->Cell($totalCol,$lebarCell,"Add Passup",1,0,'R',true);
                  $pdf->Cell($headCol4,$lebarCell,"$passup2",1,0,'R',true);
                  $pdf->Ln();
                  $pdf->Cell($totalCol,$lebarCell,"Net Total",1,0,'R',true);
                  $pdf->Cell($headCol4,$lebarCell,"$net",1,0,'R',true);
                  $pdf->Ln(); 
				  
      }
      //voucher disini
      if(isset($voucher))
      {
        $pdf->AddPage('L'); 
        foreach ($voucher as $voucher1)
        {
         	$amtCurr = number_format($voucher1->VoucherAmtCurr,2,".",",");
         	$amt = number_format($voucher1->VoucherAmt,2,".",",");	
            $col1 = 30;
            $col2 = 70;
            $sep = 5;
            $headCol1 = 30;
            $headCol2 = 90;
            $headCol3 = 25;
            $headCol4 = 50;
            $totCol = $headCol1 + $headCol2 + $headCol3 + $headCol4;
            $pdf->Cell($totCol,$lebarCell,"PRODUCT VOUCHER",0,0,'C',true);
            $pdf->Ln();	
            $pdf->Cell($col1,$lebarCell,"Voucher No.",0,0,'L',true);
            $pdf->Cell($sep,$lebarCell,":",0,0,'L',true); 
            $pdf->Cell($col2,$lebarCell,"$voucher1->VoucherNo",0,0,'L',true);
            $pdf->Ln();	
            $pdf->Cell($col1,$lebarCell,"DATE",0,0,'L',true);
            $pdf->Cell($sep,$lebarCell,":",0,0,'L',true);  
            $pdf->Cell($col2,$lebarCell,"$voucher1->tglterbit",0,0,'L',true);
            $pdf->Ln();		
            $pdf->Cell($col1,$lebarCell,"MEMBER CODE",0,0,'L',true); 
            $pdf->Cell($sep,$lebarCell,":",0,0,'L',true); 
            $pdf->Cell($col2,$lebarCell,"$voucher1->DistributorCode",0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($col1,$lebarCell,"NAME",0,0,'L',true); 
            $pdf->Cell($sep,$lebarCell,":",0,0,'L',true); 
            $pdf->Cell($col2,$lebarCell,"$voucher1->fullnm",0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($col1,$lebarCell,"AMOUNT",0,0,'L',true); 
            $pdf->Cell($sep,$lebarCell,":",0,0,'L',true); 
            $pdf->Cell($col2,$lebarCell,"$amtCurr ($amt)",0,0,'L',true);
            $pdf->Ln();
            
            $pdf->Cell($headCol1,$lebarCell,"Prod Code",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"Prod Name",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"Qty",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"Amount",1,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($headCol1,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"",1,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($headCol1,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"",1,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($headCol1,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"",1,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($headCol1,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"",1,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($headCol1,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"",1,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($headCol1,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol2,$lebarCell,"",1,0,'C',true); 
            $pdf->Cell($headCol3,$lebarCell,"",1,0,'C',true);
            $pdf->Cell($headCol4,$lebarCell,"",1,0,'C',true);
            $pdf->Ln();
            $pdf->Ln();
         	$penutupCol1 = 50;
            $sep1 = 5;
            $penutupCol2 = 70;
            $pdf->Cell($penutupCol1,$lebarCell,"Total DP",0,0,'L',true);
            $pdf->Cell($penutupCol2,$lebarCell,"",0,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"For HQ/branch/stockist use only",0,0,'L',true);
            $pdf->Cell($penutupCol2,$lebarCell,"",0,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"Payment Mode",0,0,'L',true);
            $pdf->Cell($sep1,$lebarCell,":",0,0,'C',true);
            $pdf->Cell($penutupCol2,$lebarCell,"",0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"Total DP Reddem",0,0,'L',true);
            $pdf->Cell($sep1,$lebarCell,":",0,0,'C',true);
            $pdf->Cell($penutupCol2,$lebarCell,"",0,0,'C',true);
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"Less Voucher Reddem",0,0,'L',true);
            $pdf->Cell($sep1,$lebarCell,":",0,0,'C',true);
            $pdf->Cell($penutupCol2,$lebarCell,"$amtCurr\t\t\t\t\t\t\t\t\t\t\t\t($amt)",0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"Different Pay By Cash",0,0,'L',true);
            $pdf->Cell($sep1,$lebarCell,":",0,0,'C',true);
            $pdf->Cell($penutupCol2,$lebarCell,"______________________",0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"Balance Not Refundable",0,0,'L',true);
            $pdf->Cell($sep1,$lebarCell,":",0,0,'C',true);
            $pdf->Cell($penutupCol2,$lebarCell,"______________________",0,0,'L',true); 
            $pdf->Ln();
            $pdf->Cell($penutupCol1,$lebarCell,"Certify Good Condition",0,0,'L',true);
            $pdf->Cell($sep1,$lebarCell,"",0,0,'C',true);
            $pdf->Cell($penutupCol2,$lebarCell,"",0,0,'L',true); 
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($totCol,$lebarCell,"________________________________________\t\t\t\t\t\t\t ________________________________________\t\t\t\t\t\t\t _________________________________________",0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell(70,$lebarCell,"Distributor signature",0,0,'C',true); 
            $pdf->Cell(70,$lebarCell,"Distributor Telp No",0,0,'C',true); 
            $pdf->Cell(70,$lebarCell,"Stockist Officer Stamp",0,0,'C',true);  
         }    
      }  
      
      //jangan lupa
      if(isset($ldbresult))
      {
              
              
              $colDownlineCode = 40;
              $colDownlineName = 75;    
              $colerank = 10;
              $colPGPV = 10;
              $first = 14;
              $second = 14;
              $third = 14;
              $fourth = 14;
              $fifth = 14;
              $sixth = 14;
              $seventh = 14;
              $eighth = 14;
              $ninth = 14;
              $tenth = 19;
              
              $titleCol1 = 282;
              $pdf->Ln();
              //$pdf->SetFont('Times','', 9);
              $pdf->Cell($titleCol1,$lebarCell,"LEADERSHIP BONUS",1,0,'C',true);
              $pdf->Ln();
              $pdf->Cell($colDownlineCode,$lebarCell,"Downline Code",1,0,'C',true);
              $pdf->Cell($colDownlineName,$lebarCell,"Downline Name",1,0,'C',true);
              $pdf->Cell($colerank,$lebarCell,"E.Rank",1,0,'C',true);
              $pdf->Cell($colPGPV,$lebarCell,"PGBV",1,0,'C',true);
              $pdf->Cell($first,$lebarCell,"1st",1,0,'C',true);
              $pdf->Cell($second,$lebarCell,"2nd",1,0,'C',true);						 
              $pdf->Cell($third,$lebarCell,"3rd",1,0,'C',true);
              $pdf->Cell($fourth,$lebarCell,"4th",1,0,'C',true);
              $pdf->Cell($fifth,$lebarCell,"5th",1,0,'C',true);
              $pdf->Cell($sixth,$lebarCell,"6th",1,0,'C',true);
              $pdf->Cell($seventh,$lebarCell,"7th",1,0,'C',true);
              $pdf->Cell($colGBV,$lebarCell,"8th",1,0,'C',true);
              $pdf->Cell($ninth,$lebarCell,"9th",1,0,'C',true);
              $pdf->Cell($tenth,$lebarCell,"Tot.PGBV",1,0,'C',true); 
              $pdf->Ln();
              
              foreach ($ldbresult as $ldbonus)
                {
                  
            	  $pgbv = number_format("$ldbonus->pgbv",0,".",",");
            	  $incomepgbv1 = number_format("$ldbonus->incomepgbv1",0,".",",");
            	  $incomepgbv2 = number_format("$ldbonus->incomepgbv2",0,".",",");
            	  $incomepgbv3 = number_format("$ldbonus->incomepgbv3",0,".",",");
            	  $incomepgbv4 = number_format("$ldbonus->incomepgbv4",0,".",",");
            	  $incomepgbv5 = number_format("$ldbonus->incomepgbv5",0,".",",");
            	  $incomepgbv6 = number_format("$ldbonus->incomepgbv6",0,".",",");
            	  $incomepgbv7 = number_format("$ldbonus->incomepgbv7",0,".",",");
            	  $incomepgbv8 = number_format("$ldbonus->incomepgbv8",0,".",",");
            	  $incomepgbv9 = number_format("$ldbonus->incomepgbv9",0,".",",");
            	  $totalpgbv = number_format("$ldbonus->totalpgbv",0,".",",");
                  
                  $pdf->Cell($colDownlineCode,$lebarCell,"$ldbonus->odistributorcode",1,0,'L',true);
                  $pdf->Cell($colDownlineName,$lebarCell,"$ldbonus->fullnm",1,0,'L',true);
                  $pdf->Cell($colerank,$lebarCell,"$ldbonus->effectiverank",1,0,'R',true);
                  $pdf->Cell($colPGPV,$lebarCell,"$pgbv",1,0,'R',true);
                  $pdf->Cell($first,$lebarCell,"$incomepgbv1",1,0,'R',true);
                  $pdf->Cell($second,$lebarCell,"$incomepgbv2",1,0,'R',true);						 
                  $pdf->Cell($third,$lebarCell,"$incomepgbv3",1,0,'R',true);
                  $pdf->Cell($fourth,$lebarCell,"$incomepgbv4",1,0,'R',true);
                  $pdf->Cell($fifth,$lebarCell,"$incomepgbv5",1,0,'R',true);
                  $pdf->Cell($sixth,$lebarCell,"$incomepgbv6",1,0,'R',true);
                  $pdf->Cell($seventh,$lebarCell,"$incomepgbv7",1,0,'R',true);
                  $pdf->Cell($colGBV,$lebarCell,"$incomepgbv8",1,0,'R',true);
                  $pdf->Cell($ninth,$lebarCell,"$incomepgbv9",1,0,'R',true);
                  $pdf->Cell($tenth,$lebarCell,"$totalpgbv",1,0,'R',true); 
                  $pdf->Ln();   
                }
       }
       if(isset($hasil5))
       {
              $pdf->AddPage('L');
              $colDownlineCode = 40;
              $colDownlineName = 100;    
              $colPGPV = 10;
              $first = 12;
              $second = 12;
              $third = 12;
              $fourth = 12;
              $fifth = 12;
              $sixth = 12;
              $seventh = 12;
              $eighth = 12;
              $ninth = 12;
              $tenth = 20;
              
              $titleCol1 = 282;
              
              //$pdf->SetFont('Times','', 9);
              $pdf->Ln();
              $pdf->Cell($titleCol1,$lebarCell,"PLAN B (UNILEVEL)",1,0,'C',true);
              $pdf->Ln();
              $pdf->Cell($colDownlineCode,$lebarCell,"Downline Code",1,0,'C',true);
              $pdf->Cell($colDownlineName,$lebarCell,"Downline Name",1,0,'C',true);
              $pdf->Cell($colPGPV,$lebarCell,"PGBV",1,0,'C',true);
              $pdf->Cell($first,$lebarCell,"1st",1,0,'C',true);
              $pdf->Cell($second,$lebarCell,"2nd",1,0,'C',true);						 
              $pdf->Cell($third,$lebarCell,"3rd",1,0,'C',true);
              $pdf->Cell($fourth,$lebarCell,"4th",1,0,'C',true);
              $pdf->Cell($fifth,$lebarCell,"5th",1,0,'C',true);
              $pdf->Cell($sixth,$lebarCell,"6th",1,0,'C',true);
              $pdf->Cell($seventh,$lebarCell,"7th",1,0,'C',true);
              $pdf->Cell($colGBV,$lebarCell,"8th",1,0,'C',true);
              $pdf->Cell($ninth,$lebarCell,"9th",1,0,'C',true);
              $pdf->Cell($tenth,$lebarCell,"Total",1,0,'C',true); 
              $pdf->Ln();
              
        foreach ($hasil5 as $data4)
		{
    		$pbv = number_format("$data4->pbv",0,".",",");
    		$satu = number_format("$data4->incomepbv1",0,".",",");
    		$dua = number_format("$data4->incomepbv2",0,".",",");
    		$tiga = number_format("$data4->incomepbv3",0,".",",");
    		$empat = number_format("$data4->incomepbv4",0,".",",");
    		$lima = number_format("$data4->incomepbv5",0,".",",");
    		$enam = number_format("$data4->incomepbv6",0,".",",");
    		$tujuh = number_format("$data4->incomepbv7",0,".",",");
    		$delapan = number_format("$data4->incomepbv8",0,".",",");
    		$sembilan = number_format("$data4->incomepbv9",0,".",",");
    		$total = number_format("$data4->totalpbv",0,".",",");
		
          $pdf->Cell($colDownlineCode,$lebarCell,"$data4->odistributorcode",1,0,'L',true);
          $pdf->Cell($colDownlineName,$lebarCell,"$data4->fullnm",1,0,'L',true);
          $pdf->Cell($colPGPV,$lebarCell,"$pbv",1,0,'R',true);
          $pdf->Cell($first,$lebarCell,"$satu",1,0,'R',true);
          $pdf->Cell($second,$lebarCell,"$dua",1,0,'R',true);						 
          $pdf->Cell($third,$lebarCell,"$tiga",1,0,'R',true);
          $pdf->Cell($fourth,$lebarCell,"$empat",1,0,'R',true);
          $pdf->Cell($fifth,$lebarCell,"$lima",1,0,'R',true);
          $pdf->Cell($sixth,$lebarCell,"$enam",1,0,'R',true);
          $pdf->Cell($seventh,$lebarCell,"$tujuh",1,0,'R',true);
          $pdf->Cell($colGBV,$lebarCell,"$delapan",1,0,'R',true);
          $pdf->Cell($ninth,$lebarCell,"$sembilan",1,0,'R',true);
          $pdf->Cell($tenth,$lebarCell,"$total",1,0,'R',true); 
          $pdf->Ln();
		
		} 
		
       } 
     if(isset($hasil6))
	 {
	       
           $totLevel=0;
           $titleCol = 100;
           $headCol1 = 13;
           $headCol2 = 13;
           $headCol3 = 14;
           $headCol4 = 18;
           $totalCol = $headCol1 + $headCol2 + $headCol3;
           
           //$pdf->SetFont('Times','', 9); 	
        	foreach ($hasil6 as $data5)
        	{
        		  $levelpbv = number_format("$data5->levelpbv",0,".",",");
        		  $levelincome = number_format("$data5->levelincome",0,".",",");
        	
                  $pdf->Cell($headCol1,$lebarCell,"$data5->distributorlevel Level",1,0,'C',true); 
                  $pdf->Cell($headCol2,$lebarCell,"$levelpbv",1,0,'R',true);
                  $pdf->Cell($headCol3,$lebarCell,"$data5->levelrate %",1,0,'R',true);
                  $pdf->Cell($headCol4,$lebarCell,"$levelincome",1,0,'R',true);
                  $pdf->Ln();
            	  $addinfinityincome = number_format("$data5->addinfinityincome",0,".",",");
            	  $totLevel += $data5->levelincome;
            	  $totLevel1 = number_format("$totLevel",0,".",",");
            	  $totbonus = $data5->addinfinityincome + $totLevel;
        	  }
        	  
             $pdf->Cell($totalCol,$lebarCell,"Unilevel",1,0,'R',true); 
        	 $pdf->Cell($headCol4,$lebarCell,"$totLevel1",1,0,'R',true);
             $pdf->Ln();
             $pdf->Cell($totalCol,$lebarCell,"Add Infinity",1,0,'R',true); 
        	 $pdf->Cell($headCol4,$lebarCell,"$addinfinityincome",1,0,'R',true);
             $pdf->Ln();
             $pdf->Cell($totalCol,$lebarCell,"Plan B Bonus",1,0,'R',true); 
        	 $pdf->Cell($headCol4,$lebarCell,number_format("$totbonus",0,".",","),1,0,'R',true);
        	 $pdf->Ln();
     }
     
     if(isset($hasil4))
	 {
	   if($year < $thnCheck){
	       $labelPlanB = "PLAN B (Infinity Bonus)";
           $bvPlanB = "Infinity BV";
           $labelFooter = "INFINITY BONUS";
	   }else{
	       $labelPlanB = "PLAN B (Initiative Bonus)";
           $bvPlanB = "Initiative BV";
           $labelFooter = "INITIATIVE BONUS";
	   }
	   $totinfinity = 0;
       $titleCol = 270;
       $headCol1 = 40;
       $headCol2 = 100;
       $headCol3 = 25;
       $headCol4 = 10;
       $headCol5 = 18;
       $totalCol = $headCol1 + $headCol2 + $headCol3 + $headCol4 ;
       $allCol = $totalCol + $headCol5;
       $pdf->Ln();
       $pdf->Cell($allCol,$lebarCell,$labelPlanB,1,0,'C',true);
       $pdf->Ln();
       $pdf->Cell($headCol1,$lebarCell,"Downline Code ",1,0,'C',true);
       $pdf->Cell($headCol2,$lebarCell,"Downline Name",1,0,'C',true);
       $pdf->Cell($headCol3,$lebarCell,$bvPlanB,1,0,'C',true);
       $pdf->Cell($headCol4,$lebarCell,"Total",1,0,'C',true);
       $pdf->Cell($headCol5,$lebarCell,"Total",1,0,'C',true);
       $pdf->Ln();
       $totinfinity = 0;
        	foreach ($hasil4 as $data6)
        	{
        	   if($year < $thnCheck){
        	      $infinitybv = number_format("$data6->infinitybv",0,".",",");
                  $bonusrate = number_format("$data6->BonusRate",0,".",",");
            	  $TotalBonus = number_format("$data6->TotalBonus",0,".",",");
        	   }else{
    	          $infinitybv = number_format("$data6->BonusBV",0,".",",");
        		  $bonusrate = number_format("$data6->BonusRate",0,".",",");
        		  $TotalBonus = number_format("$data6->TotalBonus",0,".",",");
        	   }
        	   
        		/*$infinitybv = number_format("$data6->infinitybv",0,".",",");
        		$bonusrate = number_format("$data6->BonusRate",0,".",",");
        		$TotalBonus = number_format("$data6->TotalBonus",0,".",",");*/
        	
        	
              $pdf->Cell($headCol1,$lebarCell,"$data6->ODistributorCode",1,0,'L',true);
              $pdf->Cell($headCol2,$lebarCell,"$data6->fullnm",1,0,'L',true);
              $pdf->Cell($headCol3,$lebarCell,"$infinitybv",1,0,'R',true);
              $pdf->Cell($headCol4,$lebarCell,"$bonusrate %",1,0,'R',true);
              $pdf->Cell($headCol5,$lebarCell,"$TotalBonus",1,0,'R',true); 
              $pdf->Ln();   
        	  $totinfinity += $data6->TotalBonus;
        	  
        	 $totinfinitybon = number_format("$totinfinity",0,".",",");
        	} 
        	
            $pdf->Cell($totalCol,$lebarCell,$labelFooter,1,0,'R',true);
            $pdf->Cell($headCol5,$lebarCell,"$totinfinitybon",1,0,'R',true);
     }  

    $pdf->Ln();
	$pdf->Ln();    
	//$url = "http://www.k-net.co.id/api/voucher/".$idmember."/".$month."/".$year;
	//$url2 = "<a href=\"$url\">Tess</a>";
    //$pdf->Link(100,10,10,10,$url);
	//$pdf->Cell(200,$lebarCell,$url2,0,0,'R',true);
    $title = "bns_report"."-".$month."-".$year.".pdf";
	$pdf->SetTitle($title);
    $pdf->Output();
	
?>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="#" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;&nbsp; <span>&gt;</span>
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;How To Order&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- How To Order -->
<div class="row vpullset4 voffset4">
	<div class="container">
    
        <div class="mens-toolbar">
            <span class="title_header">
                <i class="fa fa-book"></i> Cara Melakukan Pembayaran Dengan Internet Banking SGO
            </span>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	1
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pastikan Anda Mempunyai Internet Banking</h4>
                <p class="p2 nopadding">Untuk melakukan pembayaran, Distributor diharuskan mempunyai user id internet banking masing - masing ( contoh Bank Mandiri,BCA ).</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	2
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Masuk Dengan User ID dan Password Anda pada Internet Banking</h4>
                <p class="p2 nopadding">Setelah member memilih metode pembayaran melalui internet banking,member login pada form login internet banking yang telah disediakan ( User Id dan Password sama dengan yang ada pada internet banking masing - masing).</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	3
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Aktifkan Token Internet Banking</h4>
                <p class="p2 nopadding">Setelah sukses login,aktifkan token internet banking,dengan masukkan pin anda</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	4
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Input Challange Token</h4>
                <p class="p2 nopadding">pilih </p>
                <ol class="desc-li" type="a" >
                	<li>Pengambilan melalui stockist</li>
                    <li>Dikirim ke alamat pribadi menggunakan jasa kurir</li>
               	</ol>
                <p class="p2 nopadding">Jika Anda memilih dikirim ke alamat pribadi maka biaya pengiriman akan dibebankan kepada pembeli, sebaliknya jika pengambilan melalui stockist maka Anda tidak dikenakan biaya.
                    </p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	5
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Harga Produk dibagi sesuai Wilayah</h4>
                <p class="p2 nopadding">Untuk menentukan harga produk, kami tetap membagi berdasarkan wilayah barat atau timur area stockist berada. Kami tidak dapat barang yang akan dikirim berada diluar area stockist.</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	6
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pilih Metode Pembayaran</h4>
                <p class="p2 nopadding">Metode pembayaran dapat dilakukan melalui kartu kredit, Internet Banking, Transfer Bank dll. (gambar halaman payment)</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	7
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pembayaran Menggunakan Kartu Kredit</h4>
                <p class="p2 nopadding">Khusus pembayaran dengan menggunakan kartu kredit, diwajibkan menggunakan data dengan benar pada billing information dan pastikan jika saldo yang Anda miliki cukup. Jika kartu Kredit Anda telah menggunakan memasukan OTP dari Bank Penerbit.</p>
        	</div>
        </div>
        
         <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	8
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Konfirmasi Pesanan Anda</h4>
                <p class="p2 nopadding">Setelah selesai melakukan transaksi, cetak dan simpan bukti pembelanjaan Anda, copy transaksi akan kami kirim melalui email.</p>
        	</div>
        </div>
        
         <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	9
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Terima Pesanan Anda</h4>
                <p class="p2 nopadding">Token pengambilan barang (OTP) akan dikirimkan melalui SMS ke nomor handphone Anda, jika Anda memilih mengambil barang pesanan di stockist terdekat.</p>
        	</div>
        </div>
        
    </div>
</div>
<!-- How To Order -->
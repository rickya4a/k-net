<style>
    .form-list {
        font-size: 0.75em;
    }

    .sponsorid{
        width: 80%;
    }

    .register-top-grid > .col-md-6.col-xs-12 {
        min-height: 60px;
    }

    .controls {
        background-color: #fff;
        border-radius: 2px;
        border: 1px solid transparent;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        font-family: Roboto;
        font-size: xx-large;
        height: 30px;
        margin-left: 0px;
        margin-top: 5px;
        outline: none;
        text-overflow: ellipsis;
        width: 200px;
        color: #000;
    }

    #pac-input {
        background-color: #f8efc0;
        padding: 0 11px 0 13px;
        width: 400px;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        text-overflow: ellipsis;
    }

    #pac-input:focus {
        border-color: #F7D358;
        margin-left: -1px;
        padding-left: 14px;  /* Regular padding-left + 1. */
        width: 401px;
    }
    .pac-container {
        border-color: #F7D358;
        background-color: #F3E2A9;
    }

    .controls:focus {
        border-color: #4d90fe;
    }
    .title {
        font-weight: bold;
    }
    #infowindow-content {
        display: none;
    }
    #map #infowindow-content {
        display: inline;
    }

</style>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyBvGYeXPsDtArbUZMvgDx49JUZ8U3CuAw4" type="text/javascript"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU&libraries=places&callback=initMap" async defer></script>

<script>
    function alamat() {
        var addr1= document.getElementById("addr1").value;
        document.getElementById("pac-input").value = addr1;
    }

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -6.2400257, lng: 106.8320919},
            zoom: 13
        });

        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(
            input, {placeIdOnly: true});
        autocomplete.bindTo('bounds', map);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var geocoder = new google.maps.Geocoder;
        var marker = new google.maps.Marker({
            map: map,
            draggable: true
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });


        marker.addListener('dragend', function() {
            infowindow.open(map, marker);
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            var place = autocomplete.getPlace();

            if (!place.place_id) {
                return;
            }
            geocoder.geocode({'placeId': place.place_id}, function(results, status) {

                if (status !== 'OK') {
                    window.alert('Geocoder failed due to: ' + status);
                    return;
                }
                map.setZoom(17);
                map.setCenter(results[0].geometry.location);
                // Set the position of the marker using the place ID and location.
                marker.setPlace({
                    placeId: place.place_id,
                    location: results[0].geometry.location
                });
                marker.setVisible(true);
                //infowindowContent.children['place-name'].textContent = place.name;
                //infowindowContent.children['place-id'].textContent = place.place_id;
                //infowindowContent.children['place-address'].textContent = results[0].formatted_address;
                infowindow.open(map,marker);

                var cX=results[0].geometry.location.lat();
                var cY=results[0].geometry.location.lng();

                console.log(cX);
                console.log(cY);
                cek_jarak(cX,cY);
            });
        });
    }

    var map = null;
    var geocoder = null;
    /*function initialize() {
     if (GBrowserIsCompatible()) {
     map = new GMap2(document.getElementById("map_canvas"));
     map.setCenter(new GLatLng(-6.2400257,106.8320919), 1);
     map.setUIToDefault();
     geocoder = new GClientGeocoder();
     }
     }*/

    /*function showAddress() {
     var address= $('#addr1').val();
     //alert('GO!'+ address);
     if( address == ""){
     alert('Anda harus mengisi data dengan lengkap!');
     frm_reg.addr1.focus();
     }

     else {

     if (geocoder) {
     geocoder.getLatLng(
     address,
     function(point) {
     if (!point) {
     alert(address + " tidak ditemukan! Gunakan Kata Kunci Lain Di Sekitar Lokasi Anda.");
     frm_reg.addr1.focus();

     } else {
     map.setCenter(point, 16);

     var marker = new GMarker(point, {draggable: true});

     map.addOverlay(marker);
     GEvent.addListener(marker, "dragend", function() {

     var xy= marker.getLatLng().toUrlValue(6);
     var latlngStr = xy.split(',', 2);
     var X= latlngStr[0];
     var Y = latlngStr[1];

     //marker.openInfoWindowHtml("Lokasi Anda!");
     //var latlong=marker.getLatLng().toUrlValue(6);
     //document.frm_reg.coord.value = marker.getLatLng().toUrlValue(6);
     //document.frm_reg.coordX.value =X;
     //document.frm_reg.coordY.value =Y;

     //geocodeLatLng(X,Y);

     cek_jarak(X,Y);

     //console.log(latlngStr);
     console.log(X);
     console.log(Y);

     });
     GEvent.addListener(marker, "click", function() {
     var xy= marker.getLatLng().toUrlValue(6);
     var latlngStr = xy.split(',', 2);
     var X= latlngStr[0];
     var Y = latlngStr[1];

     marker.openInfoWindowHtml('Lokasi Anda!', {maxWidth: '5'});
     //document.frm_reg.coord.value = marker.getLatLng().toUrlValue(6);
     //document.frm_reg.coordX.value =X;
     //document.frm_reg.coordY.value =Y;

     cek_jarak(X,Y);
     //console.log(latlngStr);
     //console.log(X);
     //console.log(Y);
     //geocodeLatLng($('#coord').val());
     });
     GEvent.trigger(marker, "click");

     //addRow('dataTable')

     }
     }
     );
     }
     }

     } //END Show Address*/

    function cek_jarak(cX,cY){
        //var cX= $('#coordX').val();
        //var cY= $('#coordY').val();
        var Vcoord= cX+","+cY;
        //console.log("Vera1");
        //console.log(Vcoord);

        $.ajax({
            type: "POST",
            url: All.get_url('member/cek_jarak/'+cX+'/'+cY),
            dataType: 'json',
            data: {coord: Vcoord},
            success: function (result) {
                if (result.response == "true") {
                    //alert(result.message);
                    $("#dataTable").empty();
                    var R= result.jarak;
                    // points.sort(function(a, b){return a - b}); //sort array;

                    //console.log("Vera2");
                    //console.log(R);
                    var nomor=1;
                    $.each(R, function(k, v) {
                        var s= v.split("+");

                        tr = $("<tr></tr>");
                        tr.append("<td><input type='radio' name='stk' id='stk' value='"+s[2]+"|"+s[3]+"'></td>");
                        tr.append("<td>"+ s[2]+" - "+s[1]+"<br>"+s[4]+"</td>");
                        tr.append("<td>"+ parseFloat(s[0])+"</td>");
                        nomor++
                        $("#dataTable").append(tr);
                    });

                    sortTable();
                }
                else {
                    alert(result.error);

                }
            }
        });
    }

    function myFunction() {
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;

        var c = document.getElementById("myDiv3");
        var d = document.getElementById("myDiv4");

        var e = document.getElementById("myDiv5");
        var f = document.getElementById("myDiv6");
        var g = document.getElementById("myDiv7");
        var h = document.getElementById("myDiv8");


        if (selectedValue == 1) {
            c.style.display = "none";
            d.style.display = "none";

            e.style.display = "block";
            f.style.display = "block";
            g.style.display = "block";
            h.style.display = "block";

        } else if(selectedValue == 2) {
            c.style.display = "block";
            d.style.display = "block";

            e.style.display = "none";
            f.style.display = "none";
            g.style.display = "none";
            h.style.display = "none";
        } else{

            c.style.display = "none";
            d.style.display = "none";
            e.style.display = "none";
            f.style.display = "none";
            g.style.display = "none";
            h.style.display = "none";

        }
    }
</script>
<!-- Breadcrumb -->
<body onload="initialize()" onunload="GUnload()">
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Member&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Pendaftaran Baru VERA&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
    <div class="container">
        <div id="voucherDiv">


            <div class="col-md-12 col-xs-12 vpullset3">
                <form autocomplete="off" id="formMember" action="<?php echo base_url('member/voucher/save'); ?>" method="POST" onsubmit="return Shopping.validateInputMember()">
                    <div class="register-top-grid">

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">ID Sponsor<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="1" class="required" type="text" id="sponsorid" name="sponsorid" value="<?php echo getUserID(); ?>" onchange="Shopping.getMemberInfo(this.value, 'sponsorname')" />
                            <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo getUserID(); ?>" />
                            <input type="hidden" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo getUserName(); ?>" />

                        </div>
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Nama Sponsor<font color="red">&nbsp;*</font></label><br/>
                            <input class="required uppercase" type="text" readonly="readonly" id="sponsorname" name="sponsorname" value="<?php echo getUserName(); ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Nama Member<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="2" class="required uppercase" type="text" id="membername" name="membername" value="" />
                        </div>

                        <!--<div class="col-md-6 col-xs-12">
	                <label class="label-list">ID Recruiter<font color="red">&nbsp;*</font></label><br/>
	                <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo getUserID(); ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Recruiter<font color="red">&nbsp;*</font></label><br/>
	                <input type="hidden" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo getUserName(); ?>" />
	            </div>-->
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">No KTP<font color="red">&nbsp;*</font></label><br/>
                            <input  tabindex="3" class="required uppercase" type="text" id="idno" name="idno" value="" onchange="Shopping.checkDoubleKTP(this.value)" />


                        </div>
                        <div class="col-md-6 col-xs-12">
                            <label  class="label-list">Jenis Kelamin<font color="red">&nbsp;*</font></label><br/>
                            <select tabindex="4"  class="form-list required" id="sex" name="sex">
                                <option value="M">Pria</option>
                                <option value="F">Wanita</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Tgl Lahir<font color="red">&nbsp;*</font></label><br/>
                            <!--<input tabindex="5" class="required" type="text" id="birthdt" name="birthdt" placeholder="DD/MM/YYYY" value="" onchange="Shopping.checkAgeMember(this.value)" />-->
                            <?php
                            echo datebirth_combo();
                            ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="5" class="required uppercase" type="text" id="tel_hp" name="tel_hp" value="" onchange="Shopping.checkDoubleHP(this.value)" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Alamat Email</label><br/>
                            <input tabindex="6" type="text" id="memb_email" name="memb_email" value="" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Rekening Bank</label><br/>
                            <select tabindex="7" class="form-list" id="pilBank" name="pilBank">
                                <option value="">--Pilih disini--</option>
                                <?php
                                //print_r($listBank['arrayData']);
                                foreach($listBank['arrayData'] as $dta) {
                                    echo "<option value=\"$dta->bankid\">$dta->description</option>";
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">No Rekening</label><br/>
                            <input tabindex="8" type="text" id="no_rek" name="no_rek" value="" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Alamat<font color="red">&nbsp;*</font></label><br/>
                            <input  tabindex="9" class="required uppercase" type="text" id="addr1" name="addr1" value="" onkeyup="alamat()"/>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Kota<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="10" type="text" id="addr2" name="addr2" value="" class="required uppercase" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Kode Pos</label><br/>
                            <input tabindex="11" type="text" id="addr3" name="addr3" value="" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Pencarian Stockist<font color="red">&nbsp;*</label><br/>
                            <select tabindex="12" id="selectBox" onchange="myFunction();">
                                <option value="0">- Pilih Disini -</option>
                                <option value="1">Manual</option>
                                <option value="2">Google Maps</option>
                            </select>
                            <!-- <label class="label-list">Pilih Stockist Pada Tabel</label>

					<select tabindex="12" class="form-list required" id="stk" name="stk" >
						  <option value="">--Pilih disini--</option>
						  <?php
                            //print_r($listStk);
                            //foreach($listStk as $dta) {
                            //echo "<option value=\"$dta->loccd|$dta->state\">$dta->loccd - $dta->fullnm</option>";
                            //}
                            ?>
					  </select>-->
                        </div>


                        <div class="col-md-6 col-xs-12" id="myDiv5" style="display: none">
                            <label class="label-list">Provinsi <font color="red">&nbsp;*</font></label>
                            <select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
                                <option value="">--Select Here--</option>
                                <?php
                                //print_r($show_provinsi);
                                foreach ($show_provinsi['arrayData'] as $row) {
                                    echo "<option value=\"" . $row->kode_provinsi . "\">" . strtoupper($row->provinsi) . "</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="" />
                        </div>

                        <div class="col-md-6 col-xs-12" id="myDiv7" style="display: none">
                            <label class="label-list">Kota / Kabupaten <font color="red">&nbsp;*</font></label>
                            <select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')"></select>
                            <input type="hidden" name="nama_kota1" id="nama_kota1" value=""/>
                        </div>

                        <div class="col-md-6 col-xs-12" id="myDiv6" style="display: none">
                            <label class="label-list">Kecamatan <font color="red">&nbsp;*</font></label>
                            <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                            <select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >

                            </select>
                            <input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="" />
                            <input type="hidden" id="state1" name="state1" value="" />
                        </div>

                        <!--  Options 1 lokasi-->
                        <div class="col-md-6 col-xs-12" id="myDiv8" style="display: none">
                            <label class="label-list">Pilih Lokasi Stockist <font color="red">&nbsp;*</font></label>
                            <select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestk2(this,'#pricecode')"></select>
                            <input type="hidden" id="stk" name="stk" value=""/>
                        </div>
                        <!--  Options 1 lokasi-->

                        <!-- <div class="col-md-6 col-xs-12">
                            <div  align="left" id="map_canvas" style="width: 420px; height: 300px "></div>
                        </div> -->

                        <div class="col-md-6 col-xs-12" id="myDiv3" style="display: none">
                            <label class="label-list">Cari Stockist Terdekat</label>
                            <input id="pac-input" name="input_maps" class="controls" type="text" placeholder="Masukkan Alamat/Landmark">
                            <div id="map" style="width: 480px; height: 350px "></div>
                            <!--  <div id="infowindow-content">
                               <span id="place-name"  class="title">Lokasi Anda Disini!</span><br>
                               Place ID <span id="place-id"></span><br>
                               <span id="place-address"></span>
                            </div>-->
                        </div>

                        <div class="col-md-6 col-xs-12" id="myDiv4" style="display: none">
                            <label class="label-list">Daftar Stockist Terdekat</label>
                            <div style="overflow:auto;width:480px;height:350px;border:1px solid grey;" >
                                <table class="table table-bordered" style="font-size:10px;color:#000007;">
                                    <thead>
                                    <tr>
                                        <th width="5%"> Pilih </th>
                                        <th width="35%"> Nama Stockist</th>
                                        <th width="10%"> Jarak (km)</th>
                                    </tr>

                                    </thead>
                                    <tbody id="dataTable">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-12">


                        <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjut<i class="fa fa-arrow-right"></i></button><br />
                    </div>
                    <!-- next button-->
            </div>
            </form>
        </div>
    </div>
    <!-- Delivery Options-->
</div> <!-- End formCart1-->
<div id="divCheckOut"></div>
<div id="afterPayment"></div>
<script>
    function sortTable() {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("dataTable");
        switching = true;
        /*Make a loop that will continue until
         no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.getElementsByTagName("TR");
            /*Loop through all table rows (except the
             first, which contains table headers):*/
            for (i = 0; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                 one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[2];
                y = rows[i + 1].getElementsByTagName("TD")[2];
                //check if the two rows should switch place:
                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                 and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }
</script>


<!--Checkout Wrapper-->

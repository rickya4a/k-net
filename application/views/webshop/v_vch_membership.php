<!-- jQuery -->
<script src="<?php echo site_url();?>vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo site_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Datatables -->
<script src="<?php echo site_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo site_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
    WebFont.load({
        google: {
            families: ["Lato:100,300,400,700,900","Karla:regular","Cookie:regular"]
        }
    });
</script>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="#" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Voucher Free Membership&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">

            <span class="red"> &nbsp;Cek Voucher&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->
<div class="row vpullset4" style="min-height: 410px;">
    <div class="container">
        <?php
            if($result != null){
        ?>
        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-12">
                <h1 style="color: #ff0000;"><span class="fa fa-list"></span> List Voucher Free Membership</h1>
            </div>
        </div>

        <div id="formUtama">
            <table class="table table-striped" style="width: 100%" id="datatable">
                <thead>
                    <tr>
                        <th>No. Voucher</th>
                        <th>Voucher Key</th>
                        <th>Status</th>
                        <th>Transaksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    
                        foreach($result as $dt){

                            echo "<tr>";
                            echo"<td>".$dt->formno."</td>";
                            echo"<td>".$dt->vchkey."</td>";
                            if($dt->stat == 1){
                                echo"<td>Tersedia</td>";
                            }else{
                                echo"<td>Tidak berlaku/telah digunakan</td>";
                            }
                            echo"<td>".$dt->trcd."</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
            }else{
        ?>
        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-12">
                <h1 style="color: #ff0000;">Anda Tidak Memiliki Voucher Free Membership</h1>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;How To Order&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- How To Order -->
<div class="row vpullset4 voffset4">
	<div class="container">
    
        <div class="mens-toolbar">
            <span class="title_header">
                <i class="fa fa-book"></i> Cara Melakukan Pemesanan
            </span>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	1
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pastikan Anda Telah Terdaftar</h4>
                <p class="p2 nopadding">Untuk melakukan pembelanjaan, Distributor diharuskan menggunakan Kode Distributor / Member ID serta password yang valid. Jika Distributor tidak memiliki  password dapat menghubungi Customer Service Officer di 021-2902900.</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	2
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Masuk Dengan ID dan Password Anda</h4>
                <p class="p2 nopadding">Distributor harus melakukan login terlebih dahulu agar dapat berbelanja di K-Linkn Store (gambar halaman login)</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	3
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pilih Produk</h4>
                <p class="p2 nopadding">Pilih produk yang akan Anda beli dan klik tombol "add to cart" secara otomatis, produk yang dipilih akan masuk ke dalam keranjang belanja virtual yang terletak di atas layar Anda.</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	4
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Tentukan Lokasi Anda</h4>
                <p class="p2 nopadding">Setelah menentukan produk yang akan Anda beli, tahapan selanjutnya adalah metode pengiriman, untuk metode pengiriman dibagi menjadi dua bagian</p>
                <ol class="desc-li" type="a" >
                	<li>Pengambilan melalui stockist</li>
                    <li>Dikirim ke alamat pribadi menggunakan jasa kurir</li>
               	</ol>
                <p class="p2 nopadding">Jika Anda memilih dikirim ke alamat pribadi maka biaya pengiriman akan dibebankan kepada pembeli, sebaliknya jika pengambilan melalui stockist maka Anda tidak dikenakan biaya.
                    </p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	5
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Harga Produk dibagi sesuai Wilayah</h4>
                <p class="p2 nopadding">Untuk menentukan harga produk, kami tetap membagi berdasarkan wilayah barat atau timur area stockist berada. Kami tidak dapat barang yang akan dikirim berada diluar area stockist.</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	6
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pilih Metode Pembayaran</h4>
                <p class="p2 nopadding">Metode pembayaran dapat dilakukan melalui kartu kredit, Internet Banking, Transfer Bank dll. (gambar halaman payment)</p>
        	</div>
        </div>
        
        <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	7
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Pembayaran Menggunakan Kartu Kredit</h4>
                <p class="p2 nopadding">Khusus pembayaran dengan menggunakan kartu kredit, diwajibkan menggunakan data dengan benar pada billing information dan pastikan jika saldo yang Anda miliki cukup. Jika kartu Kredit Anda telah menggunakan memasukan OTP dari Bank Penerbit.</p>
        	</div>
        </div>
        
         <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	8
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Konfirmasi Pesanan Anda</h4>
                <p class="p2 nopadding">Setelah selesai melakukan transaksi, cetak dan simpan bukti pembelanjaan Anda, copy transaksi akan kami kirim melalui email.</p>
        	</div>
        </div>
        
         <div class="col-md-12 col-sm-12 how-wrapper">
        	<div class="col-md-1">
            	<div class="how-number">
                	9
                </div>
            </div>
        	<div class="col-md-11">
                <h4 class="green">Terima Pesanan Anda</h4>
                <p class="p2 nopadding">Token pengambilan barang (OTP) akan dikirimkan melalui SMS ke nomor handphone Anda, jika Anda memilih mengambil barang pesanan di stockist terdekat.</p>
        	</div>
        </div>
        
    </div>
</div>
<!-- How To Order -->
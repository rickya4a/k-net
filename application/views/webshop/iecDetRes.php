<?php
	if($detail == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>
<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable">
    <tr>
    	<th colspan="2" style="text-align: center;">Initiative Elite Challenge</th>
    </tr>
    <tr>
    	<td width="25%">ID Member</td>
    	<td><?php echo $header[0]->dfno; ?></td>
    </tr>
    <tr>
    	<td width="25%">Nama Member</td>
    	<td><?php echo $header[0]->fullnm; ?></td>
    </tr>
    <tr>
        <td>Bonus Month</td>
        <td><?php echo $bnsmonth;?></td>
    </tr>
    <tr>
        <td colspan="2"><input type="button" onclick="Promo.back_to_form()" class="btn btn-small btn-warning" value="&lt;&lt; Back"/></td>
    </tr>
</table>

<table style="width: 100%;" class="table table-striped table-bordered bootstrap-datatable" id="test">
    <thead>
    <tr>
    	<th colspan="4" style="text-align: center;">Detail Recruit & BV</th>
    </tr>
    
    <tr style="text-align: center;">
        <th class="no-sort" style="text-align: center;">No</th>
        <th class="no-sort" style="text-align: center;">ID Member</th>
        <th class="no-sort" style="text-align: center;">Nama Member</th>
        <th class="no-sort" style="text-align: center;">BV</th>
    </tr>
    </thead>
    <tbody>
    <?php 
        $no = 1;
        foreach($detail as $list){
            
        
    ?>
    
    <tr>
        <td style="text-align: center;"><?php echo $no;?></td>
        <td style="text-align: center;"><?php echo $list->dfno;?></td>
        <td style="text-align: center;"><?php echo $list->fullnm;?></td>
        <td style="text-align: center;"><?php echo $list->ppv;?></td>
    </tr>
    
    <?php $no++; } ?>
    </tbody>
</table>    	
<?php
	}
?>
<div class="col-md-6 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Order Details
                </span>
                <?php 
                    $cart_check = $this->cart->contents();

                    // If cart is empty, this will show below message.
                    if(empty($cart_check)) {
                    echo '<br/>To add products to your shopping cart click on "Add to Cart" Button';
                    } 
                ?> 
                <!-- header-->
                <div class="col-md-12 col-xs-12 cart_header">          	
                  	<div class="col-md-3 col-xs-3">Product </div>
                   	<div class="col-md-2 col-xs-2" align="right">Qty</div> 
                    <div class="col-md-1 col-xs-1" align="right">BV</div> 
                   	<div class="col-md-2 col-xs-2" align="right">Price</div>
                    <div class="col-md-3 col-xs-3" align="right">Amount</div> 
                    <div class="col-md-1 col-xs-1"><i class="fa fa-trash-o"></i></div>          	 
                </div>  
                <!-- header-->
                 
                    <!-- product item-->
                    <?php
                        /*$totalWestPrice = 0;
                        $totalEastPrice = 0;
                        $totalAmountWest = 0;
                        $totalAmountEast = 0;
                        $totQty = 0;
                        $totBv = 0;*/
                        $n = 1;
                        foreach($cart as $dt)
                        {
                            $rowid = $dt['rowid'];
                            $prdcd = $dt['id'];
                            $prdnm = $dt['name'];
                            $westPrice = $dt['west_price'];
                            $qty = $dt['qty'];
                            $bv = $dt['bv'];
                            $eastPrice = $dt['east_price'];
                            /*$amountEast = $eastPrice * $qty;
                            $amountWest = $westPrice * $qty;
                            
                            $totQty += $qty;
                            $totBv += $bv;
                            $totalWestPrice += $westPrice;
                            $totalEastPrice += $eastPrice;
                            $totalAmountWest += $amountWest;
                            $totalAmountEast += $amountEast;*/
                        
                    ?>
                    <div id="<?php echo $n;?>">
                    <div class="col-md-12 col-xs-12 order_wrap">   
                    	<div class="col-md-3 col-xs-3 order_desc">
                        	<a href="#">
                            <input type="hidden" value="<?php echo $prdnm;?>" name="prdnm[]" id="<?php echo "prdnm".$n;?>" readonly="yes"/>
                            <h3><?php echo $prdnm;?> </h3></a>
                        	<!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
                    	</div>
                        <div class="col-md-2 col-xs-2">
                            <input class="orderdetails-box" onkeyup="Shopping.setNewPrice(<?php echo $n;?>)" type="text" value="<?php echo $qty;?>" name="qty[]" id="<?php echo "qty".$n;?>"/>
                        </div> 
                        <div class="col-md-1 col-xs-1" align="right"><?php echo $bv;?>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $bv;?>" name="bv[]" id="<?php echo "bv".$n;?>" readonly="yes"/>
                        </div>
                        <div class="col-md-2 col-xs-2 westP" align="right"><?php echo "".number_format($westPrice,0,",",".")."";?>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $westPrice;?>" name="westPrice[]" id="<?php echo "westPrice".$n;?>" readonly="yes"/>
                        </div>
                        <div class="col-md-3 col-xs-3 westP" align="right" id="<?php echo "divSubTotWestPrice".$n ?>"><?php echo "".number_format($dt['subtotal_west_price'],0,",",".").""; ?>
                            
                        </div>
                        <div class="col-md-2 col-xs-2 eastP" align="right" style="display: none;"><?php echo "".number_format($eastPrice,0,",",".").""; ?>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $eastPrice;?>" name="eastPrice[]" id="<?php echo "eastPrice".$n;?>" readonly="yes"/>
                        </div>
                        <div class="col-md-3 col-xs-3 eastP" align="right" id="<?php echo "divSubTotEastPrice".$n ?>" style="display: none;" ><?php echo "".number_format($dt['subtotal_east_price'],0,",",",").""; ?>
                            
                        </div> 
                        <div class="col-md-1 col-xs-1 text-center">
                            <input type="hidden" id="<?php echo "rowid".$n; ?>" value="<?php echo $rowid;?>" name="rowid[]"/>
                            <a href="#" id="<?php echo $rowid;?>" onclick="Shopping.delete_rowCart(<?php echo $n; ?>)"><i class="fa fa-minus-circle"></i></a>
                        </div>
                        <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_price']; ?>" name="amountWest[]" id="<?php echo "amountWest".$n;?>" readonly="yes"/>
                        <input class="amtE orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_price'];?>" name="amountEast[]" id="<?php echo "amountEast".$n;?>" readonly="yes"/>
                    </div>
                        
                    </div>
                <!-- product item-->
                <?php
                    $n++;
                    }
                ?>
                 <!-- shipping cost-->
                <div class="col-md-12 col-xs-12 order_wrap">   
                	<div class="col-md-3 col-xs-3 order_desc">
                    	<h3>Shipping Weight</h3>
                        <p>Stookist Jakarta - Kebayoran Baru</p>
                    </div>
                    <div class="col-md-2 col-xs-2">3 Kg</div>
                    <div class="col-md-3 col-xs-3"></div> 
                    <div class="col-md-3 col-xs-3" align="right">10.000.000</div> 
                </div>  
                <!-- shipping cost-->
                
                <!-- subtotal -->
                <div class="col-md-12 col-xs-12 order_total_wrap westP">   
                	<div class="col-md-3 col-xs-3">SUBTOTAL</div>
                    <div class="col-md-2 col-xs-2" id="divTotQtyWest"><?php echo $this->cart->total_items(); ?>
                        
                    </div>
                    <div class="col-md-3 col-xs-3">&nbsp;</div> 
                    <div class="col-md-1 col-xs-1">
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>
                    </div>
                    <div class="col-md-2 col-xs-2"></div> 
                    <div class="col-md-3 col-xs-3">
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->contents('subtotal_west_price');?>" name="totalWestPrice" id="totalWestPrice" readonly="yes"/>
                    </div>
                    <div class="col-md-3 col-xs-3" id="totAllWestPrice" align="right"><?php echo "".number_format($this->cart->total_west_price(),0,",",",");?>
                        
                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items(); ?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_west_price();?>" name="totalAmountWest" id="totalAmountWest" readonly="yes"/>
                </div>
                <div class="col-md-12 col-xs-12 order_total_wrap eastP " style="display: none;">   
                	<div class="col-md-3 col-xs-3">SUBTOTAL</div>
                    <div class="col-md-2 col-xs-2" id="divTotQtyEast"><?php echo $this->cart->total_items();?>
                        
                    </div> 					
                    <div class="col-md-3 col-xs-3">&nbsp;</div> 
                    <div class="col-md-1 col-xs-1 ">
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>
                    </div>
					 <div class="col-md-2 col-xs-2"></div> 
                    <div class="col-md-3 col-xs-3">
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->contents('subtotal_east_price');?>" name="totalEastPrice" id="totalEastPrice" readonly="yes"/>
                    </div>
                    <div class="col-md-3 col-xs-3" id="totAllEastPrice" align="right"><?php echo "".number_format($this->cart->total_east_price(),0,",",",");?>
                        
                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items();?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_east_price();?>" name="totalAmountEast" id="totalAmountEast" readonly="yes"/>
                </div>
                <!-- subtotal 
                
                <!-- subtotal -->
                <div class="col-md-12 col-xs-12 order_add">  
                	<a href="<?php echo "".site_url('shop/product/cat')."";?>">
                    	<i class="fa fa-shopping-cart"></i> Tambahkan Lagi Keranjang Belanja Anda
                    </a>
                </div>
                <!-- subtotal -->
            </div>
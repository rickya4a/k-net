<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;Peplacement Policy&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Privacy Policy -->
<div class="row vpullset4 voffset4">
	<div class="container">
    
    	<!--Left sidebar-->
    	<div class="col-md-3 col-sm-12">          	
            <div class="list_grid">
				<ul style="margin-top:0px;">
                	<li><a href="#">About K-Link</a></li>
                	<li><a href="#">Why K-Link</a></li>
                	<li><a href="#">How To Order</a></li>
                    <li><a href="#">Replacment Policy</a></li>
                	<li><a href="#">Shipping Rate</a></li>
                	<li><a href="#">Delivery</a></li>
                </ul>				
			</div>            
    	</div>
        <!--Left sidebar-->
        
        <!--right-->
    	<div class="col-md-9 col-sm-12"> 
        
        	<div class="mens-toolbar">
                <span class="title_header">
                    <i class="fa fa-undo"></i> Replacement Policy
                </span>
            </div>
            
            <p class="desc">
            	Ada beberapa hal yang bisa mengakibatkan penukaran barang terjadi.apabila terjadi proses penukaran barang, Anda harus memenuhi kriteria berikut ini :
            </p>
            <br>                
           	<ol class="desc-li" type="1">
            	<li>Jika mendapatkan barang yang telah Anda order melalui website ini berbeda dengan barang yang Anda terima, maka Anda diwajibkan untuk mengisi form secara online 
atau menghubungi stockist dimana Anda melakukan transaksi.</li> 
				<li>Jika barang yang Anda order rusak pada saat proses pengiriman, Anda dapat melakukan klaim ke stockist dengan syarat transaksi tersebut dilindungi dengan asuransi.</li> 	
                <li>Pengisian form kerusakan barang wajib dilengkapi jika Anda melakukan proses retur selama memenuhi poin 1 & 2.</li>
            </ol>
                     
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- Privacy Policy -->
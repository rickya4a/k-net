<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src=" <?php echo base_url() ?>assets/images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Product Categories&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->


<!-- Product Slider -->
<div class="product_banner">
	<div class="container">
    	<div class="wmuSlider example1">        
			<div class="wmuSliderWrapper">
            	
                <!-- Slide Full-->
				<article style="position: absolute; width: 100%; opacity: 0;"> 
					<div class="banner-wrap">
				   		<div class="banner_full">
				   	    	<img src="<?php echo base_url() ?>assets/images/product-full.jpg" class="img-responsive" alt=""/>
				   	    </div>				   		 	
				   		<div class='clearfix'></div>
					</div>
				</article>
				<!-- Slide Full-->
                
                <!-- Slide teks-->
                <article style="position: relative; width: 100%; opacity: 1;"> 
					<div class="banner-wrap">
				   		<div class="banner_left">
				   	    	<img src="<?php echo base_url() ?>assets/images/product-2.jpg" class="img-responsive" alt=""/>
				   	    </div>
				   		<div class="banner_right">
				   			<h1>Kayurveda Ayuasmo</h1>
				   			<p class="p1">Rp. 100.000</p>
				   			<p class="p2">sed diam nonummy nibh euismod tincidunt Lorem ipsum dolor sit amet, consectetuer adipiscing elit,  dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim</p>
				   		</div>
				   		<div class='clearfix'></div>
					</div>
				</article>
                <!-- Slide teks-->
                
                <!-- Slide teks-->
                <article style="position: relative; width: 100%; opacity: 1;"> 
					<div class="banner-wrap">
				   		<div class="banner_left">
				   	    	<img src="<?php echo base_url() ?>assets/images/product-2.jpg" class="img-responsive" alt=""/>
				   	    </div>
				   		<div class="banner_right">
				   			<h1>Kayurveda Ayuasmo</h1>
				   			<p class="p1">Rp. 120.000</p>
				   			<p class="p2">sed diam nonummy nibh euismod tincidunt Lorem ipsum dolor sit amet, consectetuer adipiscing elit,  dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim</p>
				   		</div>
				   		<div class='clearfix'></div>
					</div>
				</article>
                <!-- Slide teks-->
                
			</div>
            
			<a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
            <ul class="wmuSliderPagination">
            	<li><a href="#" class="">0</a></li>
                <li><a href="#" class="">1</a></li>
                <li><a href="#" class="wmuActive">2</a></li>
            </ul>
            
		</div>
		<script src="<?php echo base_url() ?>assets/js/jquery.wmuSlider.js"></script> 
		<script>
       		$('.example1').wmuSlider();         
   		</script> 	           	      
	</div>
</div>
<!-- Product Slider -->

<!-- Main Content -->
<div class="products_top">
	<div class="container">
    	<!-- left content -->
    	<div class="col-md-9 col-xs-12" id="listprd">
        <h4>Silahkan pilih Kategori Produk</h4>        
		</div>
		<div class='clearfix'></div>
        <!-- left content -->
        <!-- sidebar -->
		<div class="col-md-3 product_right">        
        	<!--<div class="mens-toolbar">
               	<div class="sort-by">
                	<span class="title_header">
                    	<i class="fa fa-list-ol"></i> Product Categories
                    </span>
                </div> 
			</div>-->
               	<h3 class="m_1">Categories</h3>
				<div class="sale_grid">
                  <?php 
                        $n=1;
                        foreach($prodCat as $row)
                        {
                            echo "<ul class=\"grid_1\">
                				  <li class=\"grid_1-desc\">";
                            echo "<h4>
                                    <input type=\"hidden\" id=\"prdcat$n\" value=\"".$row->cat_id."\">                            
                                    <a id=\"".$row->cat_id."\" onclick=\"Shopping.getProdByCat(this)\" >".$row->cat_desc."</a>
                            </h4>";
                            echo "<br>
            					</li>
            					<div class=\"clearfix\"> </div>
            				</ul>";
                            $n++;
                        }
                  ?>   
				</div>
               
			<!--<h3 class="m_1 vpullset3">Other Customer Bought</h3>
				<div class="sale_grid">
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>assets/images/pic1.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>					  
                      <p>BV 10</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>assets/images/pic9.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>BV 10</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>assets/images/pic3.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>BV 10</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				</div>
                
			<h3 class="m_1">Featured Products</h3>
			    <ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>assets/images/pic2.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>BV 10</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>assets/images/pic6.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>BV 10</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>assets/images/pic5.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>BV 10</p>
					</li>
					<div class="clearfix"> </div>
				</ul>-->
		</div>
        <!-- sidebar -->        
    </div>
</div>
<!-- Main Content -->
<div class="row vpullset4">
    <div class="container">
        <!--right-->
        <div class="col-md-12">
            <!-- Track Order -->
            <!-- tracking content header-->
            <form method="post" id="printJaringan" target="_blank" action="<?php echo site_url('sales/print');?>">

                <div>
                    <?php
                    if($cashback['detail'] == 'no data' or $cashback['detail'] == null){
                        echo "Data Tidak Ditemukan";
                        echo "<br>";
//                        echo "<input class=\"btn1 btn2 btn-primary1\" type=\"button\" value=\"Kembali\" onclick=\"Sales.cashback_hydro('".getUserID()."','".$cashback['month']."','".$cashback['year']."');\" name=\"submit\" id=\"submit\">";
                    }
                    else{

                        foreach($cashback['detail'] as $r){
                            $idmember = $r->dfno;
                            $fullnm = $r->fullnm;
                        }

                        ?>

                    <div class="col-lg-12 col-xs-12 cart_header">
                        <div class="col-lg-12">
                            Hasil Pencarian
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">

                        <!--line 1-->
                        <div class="col-lg-3 col-xs-3">
                            ID Member
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo $idmember; ?>
                        </div>
                        <!--line 1-->
                        <!--line 2-->
                        <div class="col-lg-3 col-xs-3">
                            Nama Member
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo $fullnm; ?>
                        </div>
                        <!--line 2-->
                        <!--line 3-->
                        <!--                    <div class="col-lg-3 col-xs-3">-->
                        <!--                        Periode Bonus-->
                        <!--                    </div>-->
                        <!--                    <div class="col-lg-9 col-xs-9">-->
                        <!--<!--                        --><?php ////echo $bns; ?>
                        <!--                    </div>-->
                        <!--line 2-->
                    </div>

            </form>

            <div class="col-lg-12 col-xs-12 cart_header">
                <div class="col-lg-12">
                    Cashback K-Hydrogenwater
                </div>
            </div>

            <!-- table result-->
            <table class="table table-bordered cart_header" width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt" >
                <thead>
                <tr style="text-align: center;">
                    <td rowspan="2"><b>NO</b></td>
                    <td rowspan="2"><b>NOMINAL</b></td>
                    <td colspan="2"><b>PAJAK</b></td>
                    <td rowspan="2"><b>ADMIN BANK</b></td>
                    <td rowspan="2"><b>NETT</b></td>
                    <td rowspan="2"><b>TGL TRANSFER</b></td>
                </tr>
                <tr style="text-align: center;">
                    <td><b>%</b></td>
                    <td><b>NOMINAL</b></td>
                </tr>

                </thead>

                <tbody>

                <?php
                $no = 1;
                foreach($cashback['detail'] as $d){
                    $date = date_create($d->processdt);
                    $tgl = date_format($date,"d-m-Y");
                    ?>
                    <tr>
                        <td><?php echo $no?></td>
                        <td>Rp <?php echo number_format($d->nom_cashback,"0",",",".")?></td>
                        <td></td>
                        <td>Rp <?php echo number_format($d->potongan_tax,"0",",",".")?></td>
                        <td>Rp <?php echo number_format($d->biaya_bank,"0",",",".")?></td>
                        <td>Rp <?php echo number_format($d->nett_cashback,"0",",",".")?></td>
                        <td style="text-align: center"><?php echo $tgl?></td>
                    </tr>

                    <?php
                    $no++;
                }
                ?>

                </tbody>

            </table>

            <br>


            <?php
                    }

                ?>

            </div>
        </div>
    </div>
</div>

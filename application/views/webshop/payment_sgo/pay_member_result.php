<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Shipping&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Konfirmasi Pembayaran&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->
<!-- Login Form -->

<div class="contact">

    <div class="container">

        <div class="col-md-12  col-sm-12  text-center">
            <div class="brands"style="border-bottom:0;">
                <div class="m_3 voffset3"><span class="left_line"> </span><h3>Member Registration</h3><span class="right_line"> </span></div>
            </div>
            <div class="col-lg-12 col-xs-12">
                <?php
                if($trxInsertStatus == "ok") {

                    if($trans[0]->free_shipping == "1") {
                        $tot = $trans[0]->total_pay + $trans[0]->payAdm + $trans[0]->payConnectivity;
                        $payShip = 0;
                    } else {
                        $tot = $trans[0]->total_pay + $trans[0]->payShip + $trans[0]->payAdm + $trans[0]->payConnectivity;
                        $payShip = $trans[0]->payShip;
                    }

                    //$tot = $trans[0]->total_pay + $trans[0]->payShip + $trans[0]->payAdm + $trans[0]->payConnectivity;
                    //$tot = $trans[0]->total_pay + $trans[0]->payShip;
                    ?>


                <div class="row" >
                    <div class="col-md-6  col-sm-6  text-center" style="border-left:12px solid #25a25b;border-width: medium;">
                        <h1 align="left">Selamat <span style="color:#25a25b;">bergabung</span> dengan <span style="color:#25a25b;">K-link Indonesia !</span></h1>
                        <p align="left">Kami <span style="color:#25a25b;">senang</span> anda menjadi bagian dari <span style="color:#25a25b;">keluaga besar K-link</span></p>
                        <p> <br></p>
                        <p> <br></p>
                        <p> <br></p>
                        <p> <br></p>
                        <p> <br></p>
                        <p> <br></p>
                        <p> <br></p>

                    </div>
                    <div class="col-md-6  col-sm-6  text-center" style="border-left:12px solid #25a25b;border-right:12px solid #25a25b;border-width: medium;">
                        <p style="font-size:16px;">

                            <?php
                            //print_r($trans);
                            echo "ID Transaksi          : ".$trans[0]->trx_no.""."<br />";
                            echo "ID Member             : ".$trans[0]->memberid.""."<br />";
                            echo "Nama Member           : ".$trans[0]->membername.""."<br />";
                            echo "Password              : ".$trans[0]->password.""."<br />";
                            echo "ID Sponsor            : ".$trans[0]->sponsorid.""."<br />";
                            echo "Nama Sponsor          : ".$trans[0]->sponsorname.""."<br />";
                            echo "ID Stockist           : ".$trans[0]->loccd.""."<br />";
                            echo "Harga Starterkit      : Rp."."".number_format($trans[0]->total_pay, 0, ".", ",").""."<br />";
                            echo "Biaya Admin           : Rp."."".number_format($trans[0]->payAdm, 0, ".", ",").""."<br />";
                            echo "Biaya Kirim           : Rp."."".number_format($payShip, 0, ".", ",").""."<br />";
                            echo "Total Pembayaran      : Rp."."".number_format($tot, 0, ".", ",").""."<br />";
                            echo "Waktu transaksi       : ".$trans[0]->datetrans."<br />";

                            if($jne != "") {
                                echo "No Resi Pengiriman    : $jne"."<br />";
                            }
                            //echo "No Trx                : $trx_no"."<br />";
                            /* echo "No Resi Pengiriman    : $jne"."<br />"; */

                            //print_r($trx_no);
                            ?>
                        </p>

                    </div>

                </div>



            </div>

            <div class="col-md-12 col-sm-12 text-center">
                <br>
            </div>
            <div class="col-md-12 col-sm-12 text-center">

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <div class="panel-group" id="accordion">
<!--                    <div class="panel panel-default">-->
<!--                        <div class="panel-heading">-->
<!--                            <h4 class="panel-title">-->
<!--                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Voucher Starterkit Online <span style="color:#25a25b;">"Paket Combo"</span> ini anda mendapatkan:</a>-->
<!--                            </h4>-->
<!--                        </div>-->
<!--                        <div id="collapse1" class="panel-collapse collapse in">-->
<!--                            <div class="panel-body">-->
<!---->
<!--                                    <p style="text-align: center;">-->
<!--                                        1. Kartu anggota K-link<br>-->
<!--                                        2. 1 botol produk ayuartis oil (testimoni mengenai produk terkait dapat anda lihat di www.k-net.co.id )</p>-->
<!---->
<!---->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Informasi mengenai <span style="color:#25a25b;">K-link</span></a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                Informasi mengenai K-link, produk dan bagaimana memulai bisnis ini silahkan klik di sini</label>  <label href="www.k-net.co.id">(www.k-net.co.id ) 
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php
            } else if($trxInsertStatus == "pending") {
                echo "Transaksi pendaftaran member menunggu pembayaran selesai diproses..";

            } else {
                echo "Proses pendaftaran member baru gagal...";

            }
            ?>
        </div>

        <div class="col-md-12 col-sm-12 text-center">
            <br>
        </div>
        <div class="col-lg-12 col-xs-12 text-center" style="font-size:12px;">
            <!--<a href="<?php echo base_url('shop/product'); ?>">Klik disini untuk kembali ke halaman utama</a>-->
            <a class="btn1 btn2 btn-primary1" href="<?php echo base_url('shop/product'); ?>">
                <i class="fa fa-arrow-left"></i>
                <span>Klik disini untuk kembali ke halaman utama</span>
            </a>
            <?php
            //link_sk();
            ?>
        </div>
    </div>
</div>
<!-- Login Form -->

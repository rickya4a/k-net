<!-- Breadcrumb -->
<?php
$biaya = getTotalPayNetAndShipCostTerbaru(0);

$totPayAndShip = $biaya['total_pay'];
  $dt['freeship'] = $biaya['freeship'];
  $tot_discount = $biaya['tot_discount'];
  $shipping_jne_info = $this->session->userdata('shipping_jne_info');
  $hrsdibayar = $price + $shipping_jne_info['price'];
  //echo "Price : ".$price;
  //echo "<br />harga kirim : ".$shipping_jne_info['price'];
  
  $potongan = $totPayAndShip - 25000 - $hrsdibayar;
?>
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Shopping&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Payment&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->
<!--Payment Wrapper-->
<div class="row vpullset4">
    <div class="container">
        <div id="paymentDiv">
            <!--<form action="<?php echo site_url('checkout_process'); ?>" method="POST" id="payment-form">-->
    	 <!--Left Content-->
            <div class="col-lg-7 col-xs-12 nopadding voffset4">
                <!-- Billing Info-->
                <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-usd"></i> Informasi Penerima
                </span>
                    <div class="col-lg-12 col-xs-12 vpullset3">
                        <div class="col-lg-12 col-xs-12 delivery_header">

                            <!--form pengiriman-->

                            <div class="register-top-grid">
                                    <div>
                                        <span>Nama<label><font color="red">*</font></label></span>
                                        <input type="text" id="nama" name="nama" value="<?php echo $nama; ?>">
                                    </div>

                                    <div>
                                        <span>No Telp/HP<label><font color="red">*</font></label></span>
                                        <input type="text" id="telp" name="telp" value="<?php echo $telp; ?>">
                                    </div>
                                    <div>
                                        <span>Alamat Email<label><font color="red">*</font></label></span>
                                        <input type="text" id="email" name="email" value="<?php echo $email; ?>">
                                    </div>

                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <!--form pengiriman-->
                    </div>
                </div>
                <!-- Billing Info-->

                <!-- Paymen Method-->
                <div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-money"></i> Metode Pembayaran
                </span>

                    <div class="col-lg-12 col-xs-12 nopadding voffset4">
                        <div class="col-lg-12 col-xs-12">
                            <div class="col-lg-12 col-xs-12" style="font-size:inherit;">
                                <div>
                                    <div class="nomargin">
                                        <form id="selPayment" action="<?php echo base_url('vch/winner/sgo/pay/preview'); ?>" method="post">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-bordered">
                                                <tr>
                                                    <th bgcolor="#f4f4f4" colspan="3" align="center">Payment Gateway Bank</th>
                                                </tr>
                                                <tr>
                                                    <td>Pilih Tipe pembayaran</td>
                                                    <td>
			                                			<select id="bank" name="bank" onchange="setSelectPay()">
						                                	<?php
						                                	 echo "<option value=\"\">--Pilih disini--</option>";
						                                	
						                                	  foreach($listBank as $dta) {
						                                	  	echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
						                                	  }
						                                	?>
						                                </select>
						                                <input type="hidden" name="totalx" value="" />
						                                <input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
						                        		<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
						                                <input type="hidden" name="bankid" id="bankid" value=""  />
						                                <input type="hidden" name="bankCode" id="bankCode" value=""  />
						                                <input type="hidden" name="bankDesc" id="bankDesc" value=""  />
						                                <input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
						                                <input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
						                                <input type="hidden" name="charge_admin" id="charge_admin" value=""  />
			                                		</td>
                                                </tr>

                                                <tr>
                                                    <th bgcolor="#f4f4f4" colspan="3" align="center">Voucher Belanja</th>
                                                </tr>

                                                <tr>

                                                    <td>
                                                        <table width="100%" class="table table-bordered" id="tbl-voucher">
                                                            <tr>
                                                                <th align=center>No Voucher</th>
                                                                <th align=center>Nilai</th>
                                                            </tr>
                                                            <?php

                                                                echo "<tr>";
                                                                echo "<td align=center>$voucher</td>";
                                                                echo "<td align=center>25.000</td>";
                                                                echo "</tr>";
                                                            ?>


                                                        </table>
                                                    </td>

                                                    <td>
                                                        <table width="100%" class="table table-bordered">
                                                            <tr>
                                                                <td>Total Belanja</td>
                                                                <td>:</td>

                                                                <td align="right">
                                                                    <span id="tot_blanja">
                                                                        <?php echo number_format($totPayAndShip); ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Voucher</td>
                                                                <td>:</td>
                                                                <td align="right">
                                						            <span id="resl">
                                                                        25.000
                                						            </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Potongan lain-lain</td>
                                                                <td>:</td>
                                                                <td align="right">
                                						            <span id="resl">
                                                                        <?php
                                                                            
                                                                            echo number_format($potongan);
                                                                        ?>
                                						            </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sisa harus di bayar</td>
                                                                <td>:</td>
                                                                <td align="right">
                                						            <span id="sisa">
                                                                        <?php
                                                                            //$potongan = $westPrice - (25000 + $price);
                                                                            //$hb = ($westPrice - $potongan)+$shipping_jne_info['price'];
                                                                            
                                                                            echo number_format($hrsdibayar);
                                                                        echo "<input type=\"hidden\" name=\"shop_vch[]\" value=\"$voucher\" />";
                                                                        echo "<input type=\"hidden\" name=\"shop_vch_nominal[]\" value=\"25000\" />";
                                                                        echo "<input type=\"hidden\" name=\"shop_vch[]\" value=\"DISC\" />";
                                                                        echo "<input type=\"hidden\" name=\"shop_vch_nominal[]\" value=\"$potongan\" />";
                                                                        ?>
                                						            </span>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input type="submit" value="Proses Pembayaran" />
                                                        <input type="hidden" name="sisa_bayar" id="sisa_bayar" value="<?php echo $hrsdibayar; ?>" />
                                                        <input type="hidden" name="jml_total_vch" id="jml_total_vch" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <p>
                                                * Pembayaran dapat melalui bank atau voucher atau kombinasi kedua nya
                                                <br />
                                                * Bila total nilai voucher melebihi jumlah belanja, sisa nya tidak dapat diuangkan
                                            </p>


                                        </form>

                                    </div>
                                    <div class="nomargin">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Paymen Method-->


        </div>
        <!-- Left Content-->

        <!-- right Content-->
        <div class="col-lg-5 col-xs-12 nopadding voffset4">
            <!-- Adress Delivery-->
            <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-truck"></i> Alamat Tujuan Pengiriman
                </span>
                <div class="col-lg-12 col-xs-12 cart_header">
                 <?php
                  $inp = $this->session->userdata('personal_info');
				  if($inp['delivery'] == "2")
				   {
				   	 //echo $inp['firstname']." ".$inp['lastname']."</br>";
				   	 echo $inp['nama_penerima']."</br>";
					 echo $inp['alamat']."</br>";
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 //echo $inp['nama_kelurahan']."</br>";
					 echo $inp['notlp']."</br>";
				   }
				  else {
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 echo $inp['nama_stockist']."</br>";
				  }	
                  ?>	
                       	 
                </div>

            </div>


            <!-- Adress Delivery-->

            <!-- Order Details-->
            <div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Detail Pembelanjaan
                </span>
                <br />
                <!-- header-->
                <div class="col-lg-12 col-xs-12 cart_header">
<!--                    <div class="col-lg-4 col-xs-4" >No. Voucher </div>-->
                    <div class="col-lg-4 col-xs-4" >Produk</div>
                    <div class="col-lg-4 col-xs-4" >Harga</div>
                    <div class="col-lg-4 col-xs-4" >Biaya Kirim</div>
                </div>

                <!-- header-->

                <!-- product item-->

                <!-- product item-->

                <!-- shipping cost-->
                <div class="col-lg-12 col-xs-12 order_wrap">
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <?php echo $prdcdnm; ?>
<!--                        <h3>Biaya Kirim</h3>-->
                    </div>

                    <div class="col-lg-4 col-xs-4">
                        <?php echo number_format($westPrice); ?>
                    </div>

                    <div class="col-lg-3 col-xs-3" align="right">
                        <?php echo number_format($shipping_jne_info['price']); ?>
                    </div>

                </div>
                </div>

                <!-- shipping cost-->

                <!-- subtotal-->
<!--                <div class="col-lg-12 col-xs-12 order_total_wrap">-->
<!--                    <div class="col-lg-6 col-xs-6">-->
<!--                        Subtotal-->
<!--                    </div>-->
<!--                    <div class="col-lg-6 col-xs-6" id="tdx">-->
<!--                        <p>-->
<!---->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
                <!-- subtotal-->

                <!-- order number

                <!-- order number-->
            </div>
            <!-- Order Details-->
        </div>
        <!-- right Content-->

        <!-- next button-->
        <div class="col-lg-12">
            <!--<button onclick="Shopping.back_to_cart('#divCheckOut','#formCart1')" type="button" class="btn1 btn2 btn-primary1 pull-left" style="margin-right:15px;" id="back"><i class="fa fa-arrow-left" ></i> Kembali ke Cart</button>&nbsp;&nbsp;
            <button onclick="Shopping.proceedPayment()" type="button" class="btn1 btn2 btn-primary1 pull-left" id="checkout"><span>Checkout</span><i class="fa fa-check-circle"></i></button>&nbsp;&nbsp;-->

        </div>
        <!-- next button-->

    </div>
    <!--</form>-->
</div>
<div id="nextPaymentProcessDIV"></div>
</div>
<script type="text/javascript">
    $("#tbl-voucher input[name^=shop_vch]").bind('change', function() {
        var idx;
        var nom;
        var total = 0;
        $("input:checkbox[name^=shop_vch]:checked").each(function(){
            idx = $(this).attr("rel");
            nom = parseInt($("#nom" +idx).val());
            total += nom;
        });

        var belanja = parseInt($("#tot_blanja").text());
        var sisa = belanja - total;
        $("#resl").text(total);
        $("#sisa").text(sisa);
        $("#sisa_bayar").val(sisa);
        $("#jml_total_vch").val(total);
    });

    $("#selPayment").submit(function(event) {

        var sisa_bayar = parseInt($("#sisa_bayar").val());
        console.log("sisa bayar : " +sisa_bayar);
        if(sisa_bayar > 0) {
            var bankCode = $("#bankCode").val();
            var productCode = $("#bankDesc").val();
            if (bankCode != "" && productCode != "") {
                return;
            }
            alert("Jumlah yang harus di bayar Rp."+sisa_bayar+ " , silahkan pilih tipe pembayaran..")
            event.preventDefault();
        } else if(sisa_bayar <= 0) {
            return;
        } else {
            alert("Minimum pembayaran melalui bank adalah Rp.10000..")
        }



    });

    function setSelectPay() {
        var x = $("#bank").val();
        var bankDetail = $("#bank option:selected").text();
        //alert("isi : " +x);
        //var bankDesc = $("#bank option:selected").text();
        var str = x.split("|");
        $("#bankid").val(str[0]);
        $("#bankCode").val(str[1]);
        //$("#bankDesc").val(bankDesc);
        $("#bankDesc").val(str[4]);
        $("#charge_connectivity").val(str[2]);
        $("#charge_admin").val(str[3]);
        $("#bankDescDetail").val(bankDetail);


    }

</script>
<!--Payment Wrapper-->
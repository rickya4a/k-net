<!-- Breadcrumb -->
<ul class="breadcrumbs" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <li class="home">
            <a href="http://www.k-net.co.id" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Claim&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Voucher&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
    <div class="container">
        <div id="formCart1">

            <form id="formCart" action="<?php echo base_url('tes_voucher/proses'); ?>" method="POST">
                <!-- Delivery Options--><br />
                <div class="col-md-6 col-xs-12">
        	<span class="title_header">
            	<i class="fa fa-truck"></i> Tipe Pengiriman
            </span>
                    <?php
                    //echo "dsds";
                    $personal_info = $this->session->userdata("personal_info");
                    ?>


                    <!--  Options 2-->
                    <div class="col-md-12 col-xs-12 nopadding voffset4">
                        <!--  Options 2 header-->
                        <div class="col-md-12 col-xs-12 delivery_header">
                            Dikirim ke Alamat
                        </div>
                        <!--  Options 2 header-->

                        <!--  Options 2 delivery address-->
                        <div class="col-md-12 col-xs-12 vpullset3 dikirim">

                            <div class="register-top-grid">
                                <div>
                                    <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                                    <input class="uppercase" type="text" name="nama" id="nama" value="<?php echo $nama; ?>" />

                                    <input type="hidden" name="prdcd" id="prdcd" value="<?php echo $prdcd; ?>" />
                                    <input type="hidden" name="prdcdnm" id="prdcdnm" value="<?php echo $prdcdnm; ?>" />
                                    <input type="hidden" name="weight" id="weight" value="<?php echo $weight; ?>" />
                                    <input type="hidden" name="price" id="price" value="<?php echo $price; ?>" />
                                    <input type="hidden" name="westPrice" id="westPrice" value="<?php echo $westPrice; ?>" />
                                    <input type="hidden" name="eastPrice" id="eastPrice" value="<?php echo $eastPrice; ?>" />

                                    <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" />
                                    <input type="hidden" name="idmemb" id="idmemb" value="<?php echo $idmemb; ?>" />
                                    <input type="hidden" name="nama_penerima" id="nama_penerima" value="<?php echo $nama; ?>" />
                                    <input type="hidden" name="status" id="status" value="<?php echo $status; ?>" />
                                    <input type="hidden" name="notlp" id="notlp" value="<?php echo $telp; ?>" />
                                    <input type="hidden" name="voucher" id="voucher" value="<?php echo $voucher; ?>" />
                                    <input type="hidden" name="membername" id="membername" value="<?php echo $nama; ?>" />
                                    <input type="hidden" name="id_lp" id="id_lp" value="vch_streaming" />



                                    <input type="hidden" name="delivery" id="delivery" value="2" />

                                    <input type="hidden" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo $idmemb; ?>"/>
                                    <input type="hidden" name="bnsperiod" id="bnsperiod" class="form-list" value="<?php
                                    $xx = $bns['arrayData'];
                                    for($i=0; $i <= $xx[0]->rangeperiod; $i++) {
                                        $x = addRangeOneMonth($xx[0]->lastperiod, $i);
                                        echo $x;
                                        //echo "<option value=".$x.">".$x."</option>";
                                    }
                                    ?>"/>

                                </div>

                                <div>
                                    <span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
                                    <input class="numeric-input" type="text" name="telp" id="telp" value="<?php echo $telp; ?>" />
                                </div>
                                <div style="width:98%">
                                    <span>Email Address</span>
                                    <input type="text" name="email" id="email" value="<?php echo $email; ?>" readonly />
                                </div>
                                <div style="width:98%">
                                    <span>Alamat<label><font color="red">&nbsp;*</font></label></span>
                                 <textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" id="alamat">
                                 </textarea>
                                </div>

                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <!--  Options 2 delivery address-->
                        <div>
                            <div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
                                Pilih Stockist Terdekat Dengan Alamat Tujuan
                            </div>

                            <div>
                                <span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
                                <select class="form-list" name="shipper" id="shipper" onchange="Shopping.listProvinceByCargo(this)">
                                    <option value="">--Select Here--</option>
                                    <option value="1">JNE</option>                            </select>

                            </div>

                            <div id="provDiv">
                                <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')"><option value="">--Select Here--</option><option value="1">BALI</option><option value="2">BANGKA BELITUNG</option><option value="3">BANTEN</option><option value="4">BENGKULU</option><option value="5">D.I. YOGYAKARTA</option><option value="6">DKI JAKARTA</option><option value="7">GORONTALO</option><option value="8">JAMBI</option><option value="9">JAWA BARAT</option><option value="10">JAWA TENGAH</option><option value="11">JAWA TIMUR</option><option value="12">KALIMANTAN BARAT</option><option value="13">KALIMANTAN SELATAN</option><option value="14">KALIMANTAN TENGAH</option><option value="15">KALIMANTAN TIMUR</option><option value="16">KEPULAUAN RIAU</option><option value="17">LAMPUNG</option><option value="18">MALUKU</option><option value="19">MALUKU UTARA</option><option value="20">NANGRO ACEH DARUSALAM</option><option value="21">NUSA TENGGARA BARAT</option><option value="22">NUSA TENGGARA TIMUR</option><option value="23">PAPUA</option><option value="24">PAPUA BARAT</option><option value="25">RIAU</option><option value="26">SULAWESI BARAT</option><option value="27">SULAWESI SELATAN</option><option value="28">SULAWESI TENGAH</option><option value="29">SULAWESI TENGGARA</option><option value="30">SULAWESI UTARA</option><option value="31">SUMATERA BARAT</option><option value="32">SUMATERA SELATAN</option><option value="33">SUMATERA UTARA</option></select>
                                <input id="nama_provinsi" name="nama_provinsi" value="DKI JAKARTA" type="hidden">
                            </div>

                            <div id="KabDiv">
                                <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                <select class="form-list" name="kota" id="kota" onchange="Shopping.tampilkanKecamatan()"><option value="">--Select here--</option><option value="39">JAKARTA BARAT</option><option value="40">JAKARTA PUSAT</option><option value="41">JAKARTA SELATAN</option><option value="42">JAKARTA TIMUR</option><option value="43">JAKARTA UTARA</option></select>
                                <input id="nama_kota" name="nama_kota" value="JAKARTA SELATAN" type="hidden">
                            </div>
                            <div id="KecDiv" style="display: block;">
                                <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')"><option value="">--Select here--</option><option value="405">CILANDAK</option><option value="406">JAGAKARSA</option><option value="408">KEBAYORAN BARU</option><option value="409">KEBAYORAN LAMA</option><option value="410">MAMPANG PRAPATAN</option><option value="411">PANCORAN</option><option value="412">PASAR MINGGU</option><option value="415">TEBET</option></select>
                                <input id="nama_kecamatan" name="nama_kecamatan" value="PANCORAN" type="hidden">
                            </div>
                            <div>
                                <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                <!--<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">-->
                                <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.tampilkanPriceCode()"><option value="">--Select here--</option><option value="BID06">BID06 - PT. K-LINK NUSANTARA - JK</option></select>
                                <input id="nama_stockistr1ref" name="nama_stockistr1ref" value="BID06 - PT. K-LINK NUSANTARA - JK" type="hidden">
                                <input id="sender_address" name="sender_address" value="BOO10000" type="hidden">
                                <input id="jne_branch" name="jne_branch" value="BOO000" type="hidden">
                                <input id="destination_address" name="destination_address" value="CGK10205" type="hidden">
                                <input id="pricecode" name="pricecode" value="12W3" type="hidden">
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <!--  Options 2-->
                </div>

                <div class="col-md-6 col-xs-12">
                    <span class="title_header">
                	    <i class="fa fa-check-square-o"></i> Detail Voucher
                    </span>

                    <!-- header-->
                    <div class="col-md-12 col-xs-12 cart_header">
                        <div class="col-md-3 col-xs-3">No. Voucher</div>
                        <div class="col-md-4 col-xs-4" align="right">Product</div>
<!--                        <div class="col-md-4 col-xs-4" align="right">Price</div>-->
                    </div>
                    <!-- header-->

                    <div class="col-md-12 col-xs-12 order_wrap">
                        <div class="col-md-3 col-xs-3"><?php echo $voucher; ?></div>
                        <div class="col-md-4 col-xs-4" align="right"><?php echo $prdcdnm; ?></div>
<!--                        <div class="col-md-4 col-xs-4" align="right">--><?php //echo number_format($price); ?><!--</div>-->
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    </br>
                    </br>
                    </br>
                    </br>
                    </br>
                </div>

                <!-- next button-->
                <div class="col-md-12">
                    <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />
                </div>
                <!-- next button-->



    </form>
</div>
</div>
<!--Checkout Wrapper-->

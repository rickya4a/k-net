
                <!--<p class="p2 nopadding" style="font-size:16px;">-->
                   <?php
                  
					  if($trxInsertStatus == "ok") {
					  	if($trans[0]->free_shipping == "1") {
					  		$tot = $trans[0]->total_pay + $trans[0]->payAdm + $trans[0]->payConnectivity;
					  		$payShip = 0;
					  	} else {
					  		$tot = $trans[0]->total_pay + $trans[0]->payShip + $trans[0]->payAdm + $trans[0]->payConnectivity;
							$payShip = $trans[0]->payShip;
					  	}
					  
                   ?>
	                <table border="0" cellspacing="1" cellpadding="1" width="100%">
	                	<tr>
	                	<th colspan="2">
	                	  <strong>Transaksi Pembelanjaan Produk Anda berhasil</strong>	
	                	</th>
	                	<tr>
	                		<th colspan="2">&nbsp;</th>
	                	</tr>
	                
	                	<tr>
	                	  <td width="30%" align="right">ID Transaksi&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;<?php echo $trans[0]->orderno; ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">ID Member&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;<?php echo $trans[0]->id_memb; ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">Total Pembelanjaan&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;Rp. <?php echo number_format($trans[0]->total_pay, 0, ",", "."); ?></td>
	                	</tr>
						<tr>
	                	  <td align="right">Total BV&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;<?php echo number_format($trans[0]->total_bv, 0, ",", "."); ?></td>
	                	</tr>
	                	

	                	<tr>
	                	  <td align="right">Biaya Kirim&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;Rp. <?php echo number_format($payShip, 0, ",", "."); ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">Biaya Connectivity&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;Rp. <?php echo number_format($trans[0]->payConnectivity, 0, ",", "."); ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">Biaya Administrasi&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;Rp. <?php echo number_format($trans[0]->payAdm, 0, ",", "."); ?></td>
	                	</tr>
	                	
	                	<tr>
	                	  <td align="right">Total&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp;Rp. <?php echo number_format($tot, 0, ".", ","); ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">ID Stockist&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp; <?php echo $trans[0]->nmstkk; ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">Waktu Transaksi&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp; <?php echo $trans[0]->datetrans; ?></td>
	                	</tr>
	                	<tr>
	                	  <td align="right">No Resi&nbsp;&nbsp;:&nbsp;</td>	
	                	  <td align="left">&nbsp; <?php echo $jne; ?></td>
	                	</tr>
	                </table>  
	               
                 <?php
                    } else if($trxInsertStatus == "pending") {
                    	echo "Transaksi menunggu pembayaran";
				 ?>	
				 
				 <?php	
                    } else if($trxInsertStatus == "invalid") {
                    	echo "Transaksi Gagal, No order invalid..";
				 
                    } 
                    else {
                    	echo "Transaksi Gagal, Transaksi Anda tidak dapat diproses";
				 
                    }
                 ?>
               
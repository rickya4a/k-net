<div class="row vpullset4">
    <div class="container">
        <?php if(empty($listdetail)){
            echo "<h4>Data Tidak Ada</h4>";
        }else{
            ?>
            <div class="col-md-12">

                <div class="col-lg-12 col-xs-12 cart_header">
                    <div class="col-lg-12">
                        Detail Klaim Produk
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                    <?php
                    $no = 1;
                    foreach($formdetail as $row){
                        ?>
                        <!--line 1-->
                        <div class="col-lg-3 col-xs-3">
                            ID Member
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo $row->dfno; ?>
                        </div>
                        <!--line 1-->
                        <!--line 2-->
                        <div class="col-lg-3 col-xs-3">
                            Nama Member
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo $row->Fullnm; ?>
                        </div>
                        <!--line 2-->
                        <!--line 3-->
                        <div class="col-lg-3 col-xs-3">
                            Kode Voucher
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo $row->VoucherNo; ?>
                        </div>
                        <div class="col-lg-3 col-xs-3">
                            Di Klaim stockist
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo $row->loccd." - ".$row->Claim_STK; ?>
                        </div>
                        <div class="col-lg-3 col-xs-3">
                            Pada tanggal
                        </div>
                        <div class="col-lg-9 col-xs-9">
                            <?php echo date("d-M-Y",strtotime($row->Claim_dt));?>
                        </div>
                        <?php
                        $no++;
                    }?>
                    <!--line 2-->
                </div>

                <div class="col-lg-12 col-xs-12 cart_header">
                    <div class="col-lg-12">
                        Detail Produk
                    </div>
                </div>


                <!-- table result-->
                <div class="col-lg-12 col-xs-12 cart_header">
                    <div class="col-lg-3 col-xs-1"></div>

                    <div class="col-lg-1 col-xs-1">No</div>
                    <div class="col-lg-2 col-xs-2">Kode Produk</div>
                    <div class="col-lg-2 col-xs-3">Nama Produk</div>
                    <div class="col-lg-1 col-xs-1">Qty</div>

                </div>
                <?php
                $no = 1;
                foreach($listdetail as $row){
                    ?>
                    <div class="col-lg-12 col-xs-12 cart_content text-center" id="<?php echo $no;?>">
                        <div class="col-lg-3 col-xs-1"></div>

                        <div class="col-lg-1 col-xs-1"><?php echo $no;?></div>
                        <div class="col-lg-2 col-xs-3"><?php echo $row->prdcd;?></div>
                        <div class="col-lg-2 col-xs-3"><?php echo $row->prdnm;?></div>
                        <div class="col-lg-1 col-xs-3"><?php echo $row->qtyord;?></div>


                    </div>

                    <?php
                    $no++;
                }?>
            </div>
        <?php }?>
        <div>
            <input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="Sales.backToKlaim();" name="submit" id="submit"/>
        </div>
    </div>
</div>
 
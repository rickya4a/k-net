
<!-- Main Content -->
<div class="products_top">
	<div class="container">
    	<!-- left content -->
    	<div class="col-md-9 col-xs-12">
        	<!-- Image -->
            <div class="col-lg-6 col-xs-12 nomargin nopadding">
                <img src="<?php echo base_url() ?>images/pic6.jpg" class="img-responsive" />
            </div>
            <!-- Image -->
            
            <!-- product detail -->
            <div class="col-lg-6 col-xs-12 nomargin">
                <h3 class="product-title">K-Link Kinotakara <span class="label label-success">50 BV</span></h3>
                <div class="col-lg-6 col-xs-6 nomargin nopadding">
                	<h3 class="product-price">IDR 180.000</h3>
                    <p class="p4">Harga Wilayah A</p>
                </div>
                <div class="col-lg-6 col-xs-6 nomargin nopadding">
                	<h3 class="product-price">IDR 180.000</h3>
                    <p class="p4">Harga Wilayah B</p>
                </div>
                <p class="product-share">Share This Product</p>
                <div class="col-lg-12 voffset2 nopadding">
                    <ul class="footer_social">
                        <li><a href="#"> <i class="tw1"> </i> </a></li>
                        <li><a href="#"> <i class="db1"> </i> </a></li>
                        <li><a href="#"> <i class="fb1"> </i></a></li>
                        <li><a href="#"> <i class="g1"> </i></a></li>
                        <li><a href="#"> <i class="thumb"> </i></a></li>
                        <li><a href="#"> <i class="vimeo"> </i></a></li>
                    </ul>
                </div>
                <a href="#" class="btn1 btn2 btn-primary1 col-lg-offset2"><span>ADD TO CART</span><i class="fa fa-plus"></i></a>
            </div>
            <!-- product detail -->            
            <!-- product desc -->
            <div class="col-lg-12">
        		<p class="product-desc">K-Link Kinotakara adalah koyo yang berfungsi untuk menyerap racun dalam tubuh yang beredar bersama aliran darah. K-Link Kinotakara merupakan suatu inovasi yang mengagumkan dalam metode penyembuhan Fisioterapi praktis, yang menggabungkan aneka ramuan berkhasiat dengan penerapan 60 titik-titik akupuntur dibagian telapak kaki. K-Link Kinotakara dapat mengatasi berbagai gangguan kesehatan secara tuntas dan efektif tanpa efek samping.
                </p>
       		</div>
            <!-- product desc -->
            
            <!-- product tab -->
			<div class="col-lg-12">
            	<ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab">Description</a></li>
                    <li><a href="#ios" data-toggle="tab">Testimonial</a></li>
                    <li><a href="#java" data-toggle="tab">Gallery</a></li>
				</ul>
                
				<div id="myTabContent" class="tab-content">
				  <div class="tab-pane fade in active" id="home">
        				<p>Tutorials Point is a place for beginners in all technical areas. This website covers most of the latest technoligies and explains each of the technology with simple examples. You also have a <b>tryit</b> editor, wherein you can edit your code and try out different possibilities of the examples. This website covers most of the latest technoligies and explains each of the technology with simple examples. You also have a editor, wherein you can edit your code and try out different possibilities of the examples.</p>
                      	<p>K-AyuArtis Oil juga meningkatkan sirkulasi darah ke area sekitar jaringan sendi, memaksimalkan nutrisi yang dibutuhkan di daerah sambungan sendi sehingga sel-selnya dapat diperbaiki dan diregenerasi untuk membantu meningkatkan massa jaringan sendi di area nyeri sendi.</p>
                        <ul>
                            <li> K-Ayurveda </li>
                            <li> K-AyuArtis Oil </li>
                            <li> K-Ayurveda AyuArtis </li>
                            <li> K-Ayurveda </li>
                            <li> K-AyuArtis Oil </li>
                            <li> K-Ayurveda AyuArtis </li>
                        </ul>
                        <p class="desc">Tutorials Point is a place for beginners in all technical areas. This website covers most of the latest technoligies and explains each of the technology with simple examples. You also have a <b>tryit</b> editor, wherein you can edit your code and try out different possibilities of the examples. This website covers most of the latest technoligies and explains each of the technology with simple examples. You also have a editor, wherein you can edit your code and try out different possibilities of the examples.</p>
                      	<p class="desc">K-AyuArtis Oil juga meningkatkan sirkulasi darah ke area sekitar jaringan sendi, memaksimalkan nutrisi yang dibutuhkan di daerah sambungan sendi sehingga sel-selnya dapat diperbaiki dan diregenerasi untuk membantu meningkatkan massa jaringan sendi di area nyeri sendi.</p>
                    </div>
                    
                    <div class="tab-pane fade" id="ios">
                        <p>iOS is a mobile operating system developed and distributed by Apple Inc. Originally released in 2007 for the iPhone, iPod Touch, and Apple TV. iOS is derived from OS X, with which it shares the Darwin foundation. iOS is Apple's mobile version of the OS X operating system used on Apple computers.
                        </p>
                    </div>
                    
                    <div class="tab-pane fade" id="java">
                        <p>jMeter is an Open Source testing software. It is 100% pure Java application for load and performance testing.</p>
                    </div>                    
                    
				</div>                 
					
        	</div>
        	<!-- product tab -->
		</div>
        <!-- left content -->
        <!-- sidebar -->
		<div class="col-md-3 product_right">        
        	<div class="clearfix"></div>
     		<h3 class="m_1">Categories</h3>
     			<select class="dropdown" tabindex="10" data-settings='{"wrapperClass":"metro"}'>
            		<option value="0">K-Ayurveda</option>	
					<option value="1">K-AyuArtis Oil</option>
					<option value="2">K-Ayurveda AyuArtis</option>
					<option value="3">K-Ayurveda AyuAsmo</option>
					<option value="4">K-Ayurveda AyuVigo</option>
					<option value="5">K-Ayurveda AyuVita </option>
					<option value="6">K-Ayurveda AyuLite</option>
					<option value="7">K-Ayurveda AyuRhoids</option>
		       </select>
				  	<select class="dropdown" tabindex="50" data-settings='{"wrapperClass":"metro"}'>
					<option value="1">Beauty Care</option>
					<option value="2">Body Wear</option>
					<option value="3">Sub Category2</option>
					<option value="4">Sub Category3</option>
			   </select>
				  	<select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>
					<option value="1">Body Wear</option>
					<option value="2">Sub Category1</option>
					<option value="3">Sub Category2</option>
					<option value="4">Sub Category3</option>
			       </select>
			   <select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>
					<option value="1">Health Drinks</option>
					<option value="2">Sub Category1</option>
					<option value="3">Sub Category2</option>
					<option value="4">Sub Category3</option>
			   </select>
			   <select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>
					<option value="1">Health Care</option>
					<option value="2">Sub Category1</option>
					<option value="3">Sub Category2</option>
					<option value="4">Sub Category3</option>
			   </select>
               <select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>
					<option value="1">Personal Care</option>
					<option value="2">Sub Category1</option>
					<option value="3">Sub Category2</option>
					<option value="4">Sub Category3</option>
			   </select>
               <select class="dropdown" tabindex="8" data-settings='{"wrapperClass":"metro"}'>
					<option value="1">Other</option>
					<option value="2">Sub Category1</option>
					<option value="3">Sub Category2</option>
					<option value="4">Sub Category3</option>
			   </select>
			    
			<h3 class="m_1 vpullset3">Other Customer Bought</h3>
				<div class="sale_grid">
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>images/pic1.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>$45.55</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>images/pic9.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>$45.55</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>images/pic3.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>$45.55</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				</div>
                
			<h3 class="m_1">Featured Products</h3>
			    <ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>images/pic2.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>$45.55</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>images/pic6.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>$45.55</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
				<ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo base_url() ?>images/pic5.jpg" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="#">Pintuck Tank</a></h4>
					  <p>$45.55</p>
					</li>
					<div class="clearfix"> </div>
				</ul>
		</div>
        <!-- sidebar -->        
    </div>
</div>
<!-- Main Content -->
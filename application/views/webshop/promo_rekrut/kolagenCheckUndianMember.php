<?php
if($promo == null) {
	echo setErrorMessage("Anda tidak memiliki nomor undian..");
} else {
?>
<table class="table table-striped table-bordered" width="50%">
	<tr>
		<th colspan="2">Daftar Nomor Undian</th>
	</tr>
	<tr>
		<th>No</th>
		<th>No Undian</th>
	</tr>
	<?php
	        	    $i = 1;
	        		foreach($promo as $list) {
	        			echo "<tr>";
						echo "<td align=right>$i</td>";
						echo "<td align=center>$list->vno</td>";
						echo "</tr>";
						$i++;
	        		}
	        	?>
</table>
<?php
}
?>
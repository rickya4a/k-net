<?php
	if($result == null) {
		echo setErrorMessage("Data tidak ditemukan");
	} else {
		//print_r($result[0]);
?>
<table style="width: 100%" class="table table-striped table-bordered bootstrap-datatable datatable">
    <tr>
    	<th colspan="6">Detail Data</th>
    </tr>
    <tr>
    	<th>No</th>
    	<th>Leg</th>
    	<th>ID Member</th>
    	<th>Nama Member</th>
    	<th>Join Date</th>
    	<th>Recruiter</th>
    	<th>BV</th>
    </tr>
    <?php
       $i = 1;
       foreach($result as $data) {
       	echo "<tr>";
		echo "<td align=right>$i&nbsp;</td>";
		echo "<td align=center>$data->leg</td>";
		echo "<td align=center>$data->bDfno</td>";
		echo "<td>$data->bFullnm</td>";
		echo "<td align=center>$data->jointdt</td>";
		echo "<td align=center>$data->sfno_reg</td>";
		echo "<td align=right>$data->bv&nbsp;</td>";   
		echo "<tr>";  
		$i++; 
       }
    ?>
</table>
<?php
}
?>    
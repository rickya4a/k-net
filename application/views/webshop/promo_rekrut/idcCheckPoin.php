
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Promo Info&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
        
         <span class="red"> &nbsp;Cek Poin IDC ( Initiative Dynamic Challenge )&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--form -->
<div class="row vpullset4" style="min-height: 410px;">
    <span id="formCekTransaksi">
        <div class="container">
            <div class="col-md-12"> 
                <!--<div class="col-md-6 col-md-offset-3 text-center">  -->
                    <form id="cekPoinIDc" method="POST" onkeypress="return event.keyCode != 13;">
                        <!--<div class="track-order-grid">-->
                            <div class="col-lg-12 col-xs-12 cart_header">  
                                <div class="col-lg-12">
                                    <!--<h1>Sedang Dalam Tahap Pengembangan</h1>-->
                                    <h3>Cek Poin IDC ( Initiative Dynamic Challenge )</h3>
                                </div>            
                            </div>
                            
                            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                                <div class="col-lg-3 col-xs-3">
                                    ID Member  
                                </div>
                                <div class="col-lg-9 col-xs-9">
                                    <?php echo getUserID(); ?> 
                                </div> 
                                <div class="col-lg-3 col-xs-3">
                                    Nama Member  
                                </div>
                                <div class="col-lg-9 col-xs-9">
                                   <?php echo getUsername(); ?>
                                </div> 
                                
                                <div class="col-lg-3 col-xs-3">
                                    Detail Data Per Bonus Period
                                </div>
                                <div class="col-lg-9 col-xs-9">
                                    <select class="required form-list" style="margin-bottom:10px;width: 15%;" id="bnsperiod" name="bnsperiod">
                  	                     <option value = "">Silahkan Pilih</option>
                                      <?php
                                        if(empty($bnsperiod)){
                                            echo "Data Tidak Ada";
                                        }else{
                                                
                                            foreach($bnsperiod as $row){
                                                echo "<option value = \"$row->bnsperiod\">$row->bnsperiod</option>";
                                            }
                                        }
                                    ?>
                	                </select><input type="hidden" id="memberid" name="memberid" value="<?php echo getUserID();?>"/><button class="btn1 btn2 btn-primary1" type="button" onClick="Promo.searchIDC();" id="submit" style="margin: 0 0 0 10px;">Cek Detail <i class="fa fa-arrow-right"></i></button>
                                </div>         
                            </div>
                            <?php //}?>
                        <!--</div>-->          
                    </form>          
                <!--</div>-->      
            </div>
        </div>
        <div id="detail-idc"></div>
    </span>
    
</div><!-- form -->    
<!--Checkout Wrapper-->
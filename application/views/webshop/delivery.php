<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;Shipping Rates&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- delivery -->
<div class="row vpullset4 voffset4">
	<div class="container">
    
    	<!--Left sidebar-->
    	<div class="col-md-3 col-sm-12">          	
            <div class="list_grid">
				<ul style="margin-top:0px;">
                	<li><a href="#">About K-Link</a></li>
                	<li><a href="#">Why K-Link</a></li>
                	<li><a href="#">How To Order</a></li>
                    <li><a href="#">Replacment Policy</a></li>
                	<li><a href="#">Shipping Rate</a></li>
                	<li><a href="#">Delivery</a></li>
                </ul>				
			</div>            
    	</div>
        <!--Left sidebar-->
        
        <!--right-->
    	<div class="col-md-9 col-sm-12"> 
        
        	<div class="mens-toolbar">
                <span class="title_header">
                    <i class="fa fa-truck"></i> Delivery
                </span>
            </div>
            
            <p class="desc">Pengiriman barang yang sudah di pesan akan dikirim melalui kurir yang telah bekerja sama dengan Pihak K-Link Indonesia. Untuk mengetahui biaya pengiriman selengkapnya, silahkan mengklik tautan (di samping, di bawah tergantung kondisi di body teks) ini (link ke biaya pengiriman). </p><br>

            <p class="desc">Proses pengiriman barang dibutuhkan waktu berkisar 3 sd 4 hari kerja tergantung kondisi lokasi tujuan pengiriman barang. Wilayah cakupan pengiriman arang adalah sekitar area service dari stockist K-LINK yang Anda pilih dan tidak dapat keluar dari kota dimana stockist tersebut berada.
            </p>            
                     
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- delivery -->
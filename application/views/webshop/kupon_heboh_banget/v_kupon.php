<?php
    if($det == null) {
        ?>
        <!-- Breadcrumb -->
        <ul class="breadcrumbs">
            <div class="container">
                <li class="home">
                    <a href="index.html" title="Go to Home Page"><img
                            src="<?php echo base_url('assets/images/home.png'); ?>" alt=""/></a>&nbsp;
                    &nbsp; <span>&gt;</span>
                </li>
                <li class="home">&nbsp;
                    Promo Info Heboh Banget&nbsp;&nbsp;
                    <span>&gt;</span>
                </li>
                <li class="home">

                    <span class="red"> &nbsp;Cek Kupon&nbsp;&nbsp;</span>
                </li>

            </div>
        </ul>
        <!-- Breadcrumb -->

        <div class="container">
            <div class="col-lg-12 col-xs-12 cart_header">
                <div class="col-lg-12">
                    <h2>ANDA TIDAK MEMILIKI KUPON HEBOH BANGET</h2>
                </div>
            </div>
        </div>


        <?php
    }else {
        ?>
        <!-- jQuery -->
        <script src="<?php echo site_url(); ?>vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo site_url(); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Datatables -->
        <script src="<?php echo site_url(); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script
            src="<?php echo site_url(); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script
            src="<?php echo site_url(); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="<?php echo site_url(); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

        <!-- Breadcrumb -->
        <ul class="breadcrumbs">
            <div class="container">
                <li class="home">
                    <a href="index.html" title="Go to Home Page"><img
                            src="<?php echo base_url('assets/images/home.png'); ?>" alt=""/></a>&nbsp;
                    &nbsp; <span>&gt;</span>
                </li>
                <li class="home">&nbsp;
                    Promo Info Heboh Banget&nbsp;&nbsp;
                    <span>&gt;</span>
                </li>
                <li class="home">

                    <span class="red"> &nbsp;Cek Kupon&nbsp;&nbsp;</span>
                </li>

            </div>
        </ul>
        <!-- Breadcrumb -->
        <div class="row vpullset4" style="min-height: 410px;">

            <div class="container">

                <div class="col-lg-12 col-xs-12 cart_header">
                    <div class="col-lg-12">
                        <h2>Listing Kupon Heboh Banget</h2>
                    </div>
                </div>

                <div id="formUtama">
                    <form>
                        <table width="100%" class="table table-striped table-bordered" style="width: 100%"
                               id="datatable">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Member</th>
                                <th>Nama</th>
                                <th>Bonus Period</th>
                                <th>Total Kupon</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($det as $dt) {
                                ?>
                                <tr>
                                    <td align="center"><?php echo $i; ?></td>
                                    <?php echo "<td>$dt->dfno<input type=\"hidden\" id=\"dfno$i\" value=\"$dt->dfno\"/></td>"; ?>
                                    <td><?php echo $dt->fullnm; ?></td>
                                    <td align="center"><?php echo $dt->bnsperiod; ?></td>
                                    <td align="center"><?php echo $dt->total; ?></td>
                                    <?php
                                    echo "<td><a onclick=\"Promo.getDetailKupon($i)\" class='btn btn-primary'>Lihat Kupon</a></td>";
                                    ?>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </form>
                </div>

                <div id="detailkupon"></div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('#datatable').DataTable();
            });
        </script>

        <?php
    }
?>
<!-- Shopping Cart -->
<!-- Title -->
<div class="row">
	<div class="container">
    	<div class="col-lg-12 col-xs-12 vpullset4">
        	<span class="title_header"><i class="fa fa-shopping-cart"></i> Shopping Cart</span>
        </div>
    </div>
</div>
<!-- Title -->

<div class="row">
	<div class="container">
    	<!-- Shopping Cart Header-->
		<div class="col-lg-12 col-xs-12 cart_header">  
            <div class="col-lg-2 col-xs-2">Preview</div> 
            <div class="col-lg-4 col-xs-4">Product </div>
            <div class="col-lg-1 col-xs-1">BV</div> 
            <div class="col-lg-1 col-xs-1">Qty</div> 
            <div class="col-lg-2 col-xs-2">Price</div>
            <div class="col-lg-2 col-xs-2">Subtotal</div>           	 
		</div>
        <!-- Shopping Cart Header-->
        
        <!-- Shopping Cart content-->
        <div class="col-lg-12 col-xs-12 cart_content">  
            <div class="col-lg-2 col-xs-2">
            	<img src="<?php echo base_url() ?>images/pic1.jpg" class="img-responsive" />
            </div> 
            <div class="col-lg-4 col-xs-4">
                <h3>Kinokatara</h3>
                <p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>
                <p><a href="#">Product Detail</a> | <a href="#">Hapus Item</a> </p>
            </div>
            <div class="col-lg-1 col-xs-1 text-center">100</div> 
            <div class="col-lg-1 col-xs-1 text-center">
            	<select name="qty">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
          		Qty</select>
            </div> 
            <div class="col-lg-2 col-xs-2 text-center">Rp. 155.000 </div>
            <div class="col-lg-2 col-xs-2 text-center">Rp. 0</div>           	 
		</div>
        <div class="col-lg-12 col-xs-12 cart_content">  
            <div class="col-lg-2 col-xs-2">
            	<img src="<?php echo base_url() ?>images/pic2.jpg" class="img-responsive" />
            </div> 
            <div class="col-lg-4 col-xs-4">
                <h3>K-Epicor</h3>
                <p>This tool has been built to help with testing your responsive websites </p>
                <p><a href="#">Product Detail</a> | <a href="#">Hapus Item</a> </p>
            </div>
            <div class="col-lg-1 col-xs-1 text-center">500</div> 
            <div class="col-lg-1 col-xs-1 text-center">
            	<select name="qty">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
          		Qty</select>
            </div> 
            <div class="col-lg-2 col-xs-2 text-center">Rp. 255.000 </div>
            <div class="col-lg-2 col-xs-2 text-center">Rp. 0</div>           	 
		</div>
        <!-- Shopping Cart content-->
        
        <!-- Shopping Cart total-->
        <div class="col-lg-5 col-xs-12 col-lg-offset-7 col-xs-offset-0 cart_total_container">
        	<div class="col-lg-12 col-xs-12 nopadding cart_total_container">
                <div class="col-lg-6 col-xs-6 nomargin nopadding">
                    <p class="cart_total_text">Total BV :</p> 
                </div>
                <div class="col-lg-6 col-xs-6">
                    <p class="cart_total">1200</p> 
                </div>
            </div>
        	<div class="col-lg-12 col-xs-12 nopadding cart_total_container">                
                <div class="col-lg-6 col-xs-6 nomargin nopadding">
                    <p class="cart_total_text">Subtotal :</p> 
                </div>
                <div class="col-lg-6 col-xs-6">
                    <p class="cart_total">Rp. 1.110.500</p> 
                </div>
                <div class="col-lg-6 col-xs-6 nomargin nopadding">
                    <p class="cart_total_text">Biaya Pengiriman :</p> 
                </div>
                <div class="col-lg-6 col-xs-6">
                    <p class="cart_total">Rp. 90.500</p> 
                </div>
            </div>
            <div class="col-lg-6 col-xs-6 nomargin nopadding">
            	<p class="cart_total_text">TOTAL yang harus dibayar :</p> 
            </div>
            <div class="col-lg-6 col-xs-6">
            	<p class="cart_total">Rp. 1.200.500</p> 
            </div>
        </div>
        <!-- Shopping Cart total-->
        
        <!-- Shopping Cart button-->
        <div class="col-lg-5 col-xs-12 col-lg-offset-7 col-xs-offset-0 voffset4">
        	<div class="col-lg-6 col-xs-6">
            	<a href="index.php" class="btn1 btn2 btn-primary1 pull-left"><span>Kembali Belanja</span><i class="fa fa-shopping-cart"></i></a>
            </div>
            <div class="col-lg-6 col-xs-6">
            	<a href="shipping.php" class="btn1 btn2 btn-primary1 pull-right"><span>Lanjutkan</span><i class="fa fa-check-circle"></i></a>
            </div>
        </div>
        <!-- Shopping Cart button-->
        
    </div>
</div>
 <!-- Shopping Cart -->
   
<!--promo -->        
<div class="brands">
	<div class="m_3 voffset3"><span class="left_line2"> </span><h2>On Sale Product</h2><span class="right_line2"> </span></div>
    	<div class="container">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 voffset4">
                <div class="view view-first">
                    <a href="#">
                        <img src="<?php echo base_url() ?>images/pic4.jpg" class="img-responsive" alt=""/>
                    </a>                    
                    <div class="tab_desc">
                        <h3><a href="#">Kinotakara</a></h3>
                        <h5 class="green">IDR 150.000</h5>
                        <a href="#" class="btn1 btn-primary1"><span>Add To Cart</span><i class="fa fa-plus"></i></a>
                    </div>
                </div>  
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 voffset4">
                <div class="view view-first">
                    <a href="#">
                        <img src="<?php echo base_url() ?>images/pic5.jpg" class="img-responsive" alt=""/>
                    </a>                    
                    <div class="tab_desc">
                        <h3><a href="#">Kinotakara</a></h3>
                        <h5 class="green">IDR 150.000</h5>
                        <a href="#" class="btn1 btn-primary1"><span>Add To Cart</span><i class="fa fa-plus"></i></a>
                    </div>
                </div>  
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 voffset4">
                <div class="view view-first">
                    <a href="#">
                        <img src="<?php echo base_url() ?>images/pic6.jpg" class="img-responsive" alt=""/>
                    </a>                    
                    <div class="tab_desc">
                        <h3><a href="#">Kinotakara</a></h3>
                        <h5 class="green">IDR 150.000</h5>
                        <a href="#" class="btn1 btn-primary1"><span>Add To Cart</span><i class="fa fa-plus"></i></a>
                    </div>
                </div>  
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 voffset4">
                <div class="view view-first">
                    <a href="#">
                        <img src="<?php echo base_url() ?>images/pic7.jpg" class="img-responsive" alt=""/>
                    </a>                    
                    <div class="tab_desc">
                        <h3><a href="#">Kinotakara</a></h3>
                        <h5 class="green">IDR 150.000</h5>
                        <a href="#" class="btn1 btn-primary1"><span>Add To Cart</span><i class="fa fa-plus"></i></a>
                    </div>
                </div>  
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 voffset4">
                <div class="view view-first">
                    <a href="#">
                        <img src="<?php echo base_url() ?>images/pic8.jpg" class="img-responsive" alt=""/>
                    </a>                    
                    <div class="tab_desc">
                        <h3><a href="#">Kinotakara</a></h3>
                        <h5 class="green">IDR 150.000</h5>
                        <a href="#" class="btn1 btn-primary1"><span>Add To Cart</span><i class="fa fa-plus"></i></a>
                    </div>
                </div>  
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 voffset4">
                <div class="view view-first">
                    <a href="#">
                        <img src="<?php echo base_url() ?>images/pic9.jpg" class="img-responsive" alt=""/>
                    </a>
					<div class="tab_desc">
                    	<h3><a href="#">Kinotakara</a></h3>
                   		<h5 class="green">IDR 150.000</h5>
                    	<a href="#" class="btn1 btn-primary1"><span>Add To Cart</span><i class="fa fa-plus"></i></a>
                	</div>
            	</div>  
    		</div>     
		</div>
</div>
<!--promo --> 

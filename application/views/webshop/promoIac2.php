
<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Promo Info&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">

            <span class="red"> &nbsp;Cek IAC ( Initiative Anniversary Challenge )&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->


<div class="row vpullset4" style="min-height: 410px;">
    <div class="container">
        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-12">
                <!--<h1>Menu Ini Sedang Dalam Tahap Pengembangan</h1>-->
                <h2>Listing Initiative Anniversary Challenge ( IAC ) </h2>
            </div>
        </div>
        <?php if(empty($result)){
            echo "<h4>Data Tidak Ada</h4>";
        }else{
        ?>
        <div id="SummIEC">
            <?php
            if ($pemenang == true) {
            ?>
            <form id="cekBonus" method="POST" method="POST" target="_blank" action="http://www.k-net.co.id/iac/cetak">

                <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                    <!--line 1-->
                    <div class="col-lg-3 col-xs-3">
                        ID Member
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <?php echo getUserID(); ?>
                    </div>
                    <!--line 1-->
                    <!--line 2-->
                    <div class="col-lg-3 col-xs-3">
                        Nama Member
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <?php echo getUserName(); ?>
                    </div>
                    <br>
                    <div class="col-lg-3 col-xs-3">

                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <input id="submit" type="submit" name="submit" value="Cetak Form" class="btn1 btn2 btn-primary1"/>
                    </div>
                </div>
            </form>

            <!--line 2-->
        </div>
    <?php
    }
    else
    { echo '';}
    ?>
        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-1 col-xs-1">No</div>
            <div class="col-lg-2 col-xs-2">ID</div>
            <div class="col-lg-3 col-xs-3">Nama</div>
            <div class="col-lg-3 col-xs-3">Total Downline Inisiatif</div>
            <div class="col-lg-2 col-xs-2">Bonus Inisiatif</div>
            <div class="col-lg-1 col-xs-1">Grade IAC</div>


        </div>

        <?php
        $no = 1;
        foreach($result as $row){
            ?>
            <div class="col-lg-12 col-xs-12 cart_content text-center" id="<?php echo $no;?>">
                <div class="col-lg-1 col-xs-1"><?php echo $no;?></div>
                <div class="col-lg-2 col-xs-2"><?php echo $row->dfno;?></div>
                <div class="col-lg-3 col-xs-3"><?php echo $row->fullnm;?></div>

                <div class="col-lg-3 col-xs-3"><?php echo number_format($row->total_rec_IAC,0,".",",");?></div>
                <div class="col-lg-2 col-xs-2"><?php echo number_format($row->totalbonus_rec_IAC_Summ_RP,0,".",",");?></div>
                <div class="col-lg-1 col-xs-1"><?php echo $row->flag_IAC_type;?></div>

            </div>
            <?php $no++; } ?>

    </div>
    <?php }?>
    <div id="detIEC"></div>
</div>
</div>

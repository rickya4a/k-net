
<style>   
.soldout{
   content: " ";
    z-index: 10;
    display: block;
    position: absolute;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.5);
    }
    
p.soldtext {
    text-align: center;
    position: relative;
    color: white;
    background: #00BCD4;
    top: 77%;
    margin: 0 10px;
    padding: 10px 0;}

    input.greysold{
    border-color:#dcdcdc;
    background: #dcdcdc;
}
</style>


<script type="text/javascript">
// To conform clear all data in cart.
function clear_cart() {
var result = confirm('Are you sure want to clear all bookings?');

if (result) {
window.location = "<?php echo base_url(); ?>index.php/shopping/remove/all";
} else {
return false; // cancel button
}
}
</script>
 
<div class="slider">
	<div class="callbacks_container">
		<ul class="rslides" id="slider">
            <!--<li><img src="<?php //echo base_url() ?>assets/images/promo_ieduladha.jpg" class="img-responsive" alt=""/></li>
	    	<li><img src="<?php echo base_url() ?>assets/images/banner.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="<?php echo base_url() ?>assets/images/banner1.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="<?php echo base_url() ?>assets/images/banner4.jpg" class="img-responsive" alt=""/></li>-->
	         <?php 
                    $n = 1;
                    foreach($banner as $bann){ 
                        $id = $bann->id; 
                        $hdr_desc = $bann->hdr_desc; 
                        $goup_hdr = $bann->goup_hdr; 
                        $img_url = $bann->img_url; 
                        $hdr_status = $bann->hdr_status;
						
						echo "<li><img src=\"".base_url($img_url)."\" class=\"img-responsive\" alt=\"\"/></li>";
					}
            ?> 
	    </ul>
	 </div>
</div>
<script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	nav: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
    
    
</script>	
<div class="column_center">
	<div class="container">
		<div class="search">
        <div class="stay">Search Product</div>          
            <div class="stay_right">
                <form action="<?php echo base_url('shop/product/name'); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" novalidate>
                    <input type="text" name="name" value="" placeholder="Nama Produk">                    
                    <input type="submit" value="GO" style="color:#fff;">
                </form>
            </div>            
            <div class="clearfix"></div>
		</div>
             
        <ul class="social">
        	<li class="find">Find us here</li>
          	<li><a href="https://www.facebook.com/pages/AP-K-Link-Indonesia/154582434691756" target="_blank"> <i class="fb"> </i> </a></li>
          	<li><a href="https://twitter.com/official_klink" target="_blank"> <i class="tw"> </i> </a></li>
          	<li><a href="https://plus.google.com/u/0/+KlinkCoIdisgone/posts" target="_blank"> <i class="gl"> </i></a></li>
            <li><a href="http://instagram.com/klink_indonesia_official?ref=badge" target="_blank"> <i class="ig"> </i> </a></li>
          	<li><a href="http://www.youtube.com/channel/UC1YLBuEkJLzocmc_yp5Pgvw" target="_blank"> <i class="yt"> </i> </a></li>
         	<div class="clearfix"> </div>
        </ul>
		<div class="clearfix"></div>        
  	</div>
</div>
<!--Stripe -->

<!--promo -->
<div class="row">
	<div class="container">
    	<div class="col-md-6">
        	<a href="<?php echo "".site_url('shop/productHeader/cat/id/13').""; ?>"><img src="<?php echo base_url() ?>assets/images/promo.jpg" class="img-responsive margin0" alt=""/></a>
        </div>
        <div class="col-md-6">
        	<a href="<?php echo "".site_url('tracking')."";?>"><img src="<?php echo base_url() ?>assets/images/tracking.jpg" class="img-responsive margin0" alt=""/></a>
        </div>		
 	</div>
</div>
<!--promo -->

<!--product -->
		
<div class="clearence">
	<div class="content_bottom">
        <div class="container" id="listProductDiv">
        	
            <div class="m_3" style="margin-bottom:25px;">
            	
            	<span class="left_line"> </span><h3>Best Seller</h3>
            	<span class="right_line"> </span></div>
            <?php
             if(isset($prod) || $prod != null)
             {
			?>	
            <form id="formAddCart" class="clearfix">
            <?php 
                    $n = 1;
                    foreach($prod as $row)
                    { 
                        $prdcd = $row->prdcd;
                        $imgName = $row->img_url;
                        $prdcdnm = $row->prdnm;
                        $prdcdcat = $row->prdcdcat;
                        $prdcdcatnm = $row->prdnmcatnm;
                        $westPrice = $row->price_cw;
                        $eastPrice = $row->price_ce;
						$weight = $row->weight;
                        $bv = $row->bv;   
						$ecomm_status = $row->ecomm_status;
                        $is_discontinue = $row->is_discontinue;                       
            ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 voffset4 thumb-wrap">
						<?php   
                             if($ecomm_status == '2' && $is_discontinue == '0')
                                {
                                    echo "<div class=\"soldout\"> <p class=\"soldtext\"> Stok Tidak Tersedia </div>";
                                }
						?>
                    	
                        <div class="view view-first">
                        	<h5 class="title-desc">
                            	<a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)"><?php echo $prdcdnm;?></a>
                                <span class="label label-success">BV <?php echo "".number_format($bv,0,".",".");?></span>
                            </h5>
                            <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)">
                                <img src="<?php echo "".base_url()."assets/images/".$imgName."";?>" class="lazy img-responsive" alt=""/>
                            </a>                   
                            <div class="tab_desc">
                                <div class="col-xs-12 col-md-12 nopadding">
                        			<div class="col-xs-6 col-md-6 nopadding price-wrap">
                                		<h5 class="green">Rp. <?php echo "".number_format($westPrice,0,".",".");?></h5>
                                    	<h5 class="p3">Harga Wilayah A</h5>
                                    </div>
                                    <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                		<h5 class="green">Rp. <?php echo "".number_format($eastPrice,0,".",".");?></h5>
                                    	<h5 class="p3">Harga Wilayah B</h5>
                                    </div>                                
                                <!--<form method="post" action="<?php echo "".site_url('cart/addtocart')."";?>">-->
                                
                                    <input type="hidden" name="prdcd" id="<?php echo "prdcd".$n; ?>" value="<?php echo $prdcd;?>"/>
                                    <input type="hidden" name="prdnm" id="<?php echo "prdnm".$n; ?>" value="<?php echo $prdcdnm;?>"/>
                                    <input type="hidden" name="bv" id="<?php echo "bv".$n; ?>" value="<?php echo $bv;?>"/>
                                    <input type="hidden" name="westPrice" id="<?php echo "westPrice".$n; ?>" value="<?php echo $westPrice;?>"/>
                                    <input type="hidden" name="eastPrice" id="<?php echo "eastPrice".$n; ?>" value="<?php echo $eastPrice;?>"/>
                                    <input type="hidden" name="weight" id="<?php echo "weight".$n; ?>" value="<?php echo $weight;?>"/>
                                    
                                    <?php
                                      /*
                                      $usr = getUsername(); 
                    					//echo getUsername(); 
                    				  if($usr != NULL) {
                    				  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukan ke keranjang\"/>";
									  } else {
									  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
									  }	*/
                                    ?>
                                    					                                
                                </div>
                                <div class="clearfix"></div>
                                <!--<a href="cart.php" class="btn1 btn-primary1">
                                        <span>Add To Cart</span><i class="fa fa-plus"></i>
                                </a>-->
                            </div>
                        </div>                     
                    </div>
                       
                <?php
                    $n++;
                    }
                ?>
                </form> 
                <?php
	             } else {
	             	echo "Pencarian produk yang mengandung kata '$prdnm' tidak ditemukan..";
	             }
				?>               
                <div class="col-md-12 voffset4 text-center">
                    <!--<a href="<?php echo "".site_url('shop/product/cat')."";?>" class="btn1 btn2 btn-primary1"><span>View More Product</span><i class="fa fa-list"></i></a> -->
                </div> 
        </div>
        <!--container -->
        <div class="container" id="detailProductDiv" style="display:none">
        	
        </div>
    </div>
    <!--content_bottom -->
</div>
<!--clearence -->
<!--product --> 

   
<!--brand -->        
<div class="brands">
	<div class="m_3 voffset3"><span class="left_line2"> </span><h3>Product Category</h3><span class="right_line2"> </span></div>
    <div class="container">
        <ul id="logoslider"> 
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-1.jpg" class="category" alt=""/> </a></li>   
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-2.jpg" class="category" alt=""/> </a></li> 
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-3.jpg" class="category" alt=""/> </a></li> 
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-4.jpg" class="category" alt=""/> </a></li> 
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-5.jpg" class="category" alt=""/> </a></li
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-6.jpg" class="category" alt=""/> </a></li>                                                                          
        </ul>       	
	</div>
</div>
<script type="text/javascript">

$(window).load(function() {
    $("#logoslider").flexisel({
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3
            }
        }
    });    
    
});
</script>
<!--brand --> 

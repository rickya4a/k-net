
<div class="row vpullset4 voffset4" style="min-height: 405px;">
	<div class="container">
    
    	<!--Left sidebar-->
    	<div class="col-md-3 col-sm-12">   
        	<span class="title_header">
            	<i class="fa fa-user"></i> My Account
        	</span>
            <div class="list_grid voffset3">
				<ul>
                	<!--<li><a href="#" onclick="Javascript:Shopping.getAccountPage('history/trx')">Transaction History</a></li>-->
                	<li><a href="#" onclick="Javascript:Shopping.getAccountPage('myaccount')">Personal Particulars</a></li>
                	<li><a href="#" onclick="Javascript:Shopping.getAccountPage('tracking2')">Tracking Your Package</a></li>
                    <li><a href="#" onclick="Javascript:Shopping.getAccountPage('myEmail')">Update Email</a></li>
                    <li><a href="#" onclick="Javascript:Shopping.getAccountPage('myPassword')">Change Password</a></li>
					<li><a href="#" onclick="Javascript:Shopping.getAccountPage('bnsstt_view')">Bonus Statement Status View</a></li>
					<li><a href="#" onclick="Javascript:Shopping.getAccountPage('saldoVA')">Saldo Virtual Account</a></li>

                </ul>				
			</div>            
    	</div>
        <!--Left sidebar-->
        
        <!--right-->
    	<div class="col-md-9 col-sm-12" id="acc_div"> 
            
        	<!-- Track Order -->
        	<div class="col-md-6 col-md-offset-3 text-center">
				 
                	<div class="track-order-grid">
                    	<div>
                        	<span>HISTORI TRANSAKSI PEMBELANJAAN YANG DIKIRIM KE ALAMAT</span>
                        	                       
                   		</div>
                       
            		</div>
               	
			</div>
           
            <!-- Track Order -->
            <!-- tracking content header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-3 col-xs-3">NO.ORDER</div>
                <div class="col-lg-3 col-xs-3">ID MEMBER</div>
                <div class="col-lg-3 col-xs-3">TANGGAL TRX</div> 
                <div class="col-lg-3 col-xs-3">CNOTE</div>          	 
            </div>
            <!-- tracking content-->
            <!-- tracking content 1-->
            <div id="resultAwb" class="col-lg-12 col-xs-12 cart_content">
            	<?php
            	if($listTrx != null) {
            	foreach($listTrx as $dta) {
            		echo "<div class=\"col-lg-3 col-xs-3\">$dta->orderno</div>";
                    echo "<div class=\"col-lg-3 col-xs-3\">$dta->id_memb</div>";
                   echo "<div class=\"col-lg-3 col-xs-3\">$dta->datetrans</div>";
				   /*if($dta->cargo_id == "2") {
				   	 echo "<div class=\"col-lg-3 col-xs-3\"><a id=\"$dta->conoteJNE\" onclick=\"getDetailCnote(this)\">$dta->conoteJNE</a></div>";
				   } else {
				   	 echo "<div class=\"col-lg-3 col-xs-3\">$dta->conoteJNE</div>";
				   }*/
				   
				   if($dta->cargo_id == "2") {
				   	 echo "<div class=\"col-lg-3 col-xs-3\"><a id=\"$dta->conoteJNE\" onclick=\"getDetailCnote(this)\">$dta->conoteJNE</a></div>";
				   } else {
				   	 //echo "<div class=\"col-lg-3 col-xs-3\">$dta->conoteJNE</div>";
					 echo "<div class=\"col-lg-3 col-xs-3\"><a id=\"$dta->conoteJNE\" onclick=\"getDetailCnoteJne(this)\">$dta->conoteJNE</a></div>";
				   }
                   
            	}
				}
				?>
            </div>
            <div id="detailCnote" class="col-lg-12 col-xs-12">
            	
            </div>
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- Shopping Cart -->
<script>
	function getDetailCnoteJne(idx) {
		$("#resultAwb").hide();
		//$("#detailCnote").show();
        $("#detailCnote").html("<h3>Mohon menunggu..</h3>");
		
		var id = idx.id;
		$.ajax({
            url: All.get_url("trans/conote/track/") +"/"+id,
            type: 'GET',
			//dataType: 'json',
            success:
            function(data){
            	
                
                	$("#detailCnote").html(null);
                    $("#detailCnote").html(data);
               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	}

	function getDetailCnote(idx) {
		$("#resultAwb").hide();
		//$("#detailCnote").show();
        $("#detailCnote").html("<h3>Mohon menunggu..</h3>");
		
		var id = idx.id;
		$.ajax({
            url: All.get_url("kgp/track") +"/"+id,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                
                	$("#detailCnote").html(null);
                    if(data.Status === "000") {
	                	var htmlx = "<table border='1' width='60%'><tr><td colspan=2>Status Pengiriman</td></tr>";
	                	
	                	if(data.DeliveryStatus == "Sukses") {
	                		htmlx += "<tr><td width=30%>Diterima oleh</td><td>"+data.ReceivedBy+"</td></tr>";
	                		htmlx += "<tr><td width=30%>Hub. dgn Penerima</td><td>"+data.RelationshipWithConsignee+"</td></tr>";
	                	    htmlx += "<tr><td>Tgl Terima</td><td>"+data.ReceivedDate+"</td></tr>";
	                	} else if(data.DeliveryStatus == "On Progress"){
	                		htmlx += "<tr><td>Status Kirim</td><td>Barang dalam proses pengiriman (KGP Cargo)</td></tr>";
	                	}
	                	htmlx += "<tr><td colspan=2><input type='button' value='<< Kembali' onclick='backForm()' /></td></tr>";
	                	htmlx += "</table>";
	                	$("#detailCnote").html(htmlx);
                    } else {
                    	if(data.RejectDescription == null) {
                    		var htmlx = "<table border='1' width='60%'><tr><td>Status Pengiriman</td></tr>";
	                	htmlx += "<tr><td>Transaksi Cnote "+id+" tidak ada.. </td></tr>";
	                	htmlx += "<tr><td><input type='button' value='<< Kembali' onclick='backForm()' /></td></tr>";
	                	htmlx += "</table>";
                    	} else {
                    		var htmlx = "<table border='1' width='60%'><tr><td>Status Pengiriman</td></tr>";
	                	htmlx += "<tr><td>"+data.RejectDescription+"</tr>";
	                	htmlx += "<tr><td><input type='button' value='<< Kembali' onclick='backForm()' /></td></tr>";
	                	htmlx += "</table>";
                    	}	
                    	
	                	$("#detailCnote").html(htmlx);
                    }
               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	}
	
	function backForm() {
		$("#detailCnote").html(null);
		$("#resultAwb").show();
	}
</script>

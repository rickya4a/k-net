
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Shopping&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Payment&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->
<!--Payment Wrapper-->
<?php
  //if(getUserID() == "IDSPAAA96407") {
	$biaya = getTotalPayNetAndShipCostTerbaru();
 /* }	else {
  	$biaya = getTotalPayNetAndShipCost2();
  } */
  
  $totPayAndShip = $biaya['total_pay'];
  $dt['freeship'] = $biaya['freeship'];
  $tot_discount = $biaya['tot_discount'];
  $shipping_jne_info = $this->session->userdata('shipping_jne_info');
  //echo getUserID();
?>
<div class="row vpullset4">
	<div class="container">
       <div id="paymentDiv">
       	<!--<form action="<?php echo site_url('checkout_process'); ?>" method="POST" id="payment-form">
    	 Left Content-->
        <div class="col-lg-7 col-xs-12 nopadding voffset4">
        	<!-- Billing Info-->
        	<div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-usd"></i> Informasi Penerima
                </span>
                <div class="col-lg-12 col-xs-12 vpullset3"> 
            		<div class="col-lg-12 col-xs-12 delivery_header">
                    	
                   
                    
                    <!--form pengiriman-->
					
                        <div class="register-top-grid">
                            <?php
                              $inp = $this->session->userdata('personal_info');
							  if($inp['delivery'] == "2")
							  {
                            ?>
                            <div>
                            <span>Nama Depan<label><font color="red">*</font></label></span>
                                <input type="text" id="nama_penerima"  readonly="readonly" value="<?php echo $inp['nama_penerima']; ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo $inp['notlp']; ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo $inp['email']; ?>"> 
                            </div>
                            
                            
                            <?php
                             } else {
                            ?>
                            <div>
                            	<span>Nama <label><font color="red">*</font></label></span>
                                <input type="text" id="firstname"  readonly="readonly" value="<?php echo getUsername(); ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo getUserPhone(); ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo getUserEmail(); ?>"> 
                            </div>
                            <?php
							 }
                            ?>
                            <div class="clearfix"> </div>					   
                        </div>				     
					  </div>
                	<!--form pengiriman--> 
				</div>   
			</div>
            <!-- Billing Info-->
        	
            <!-- Paymen Method-->
			<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-money"></i> Metode Pembayaran
                </span>
                <!--<div class="col-lg-12 col-xs-12 nopadding voffset4">   
                    <div class="col-lg-12 col-xs-12">       	
                        <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                            <div class="register-top-grid">                            
                                <div class="nomargin">
                                    <select class="form-list small" id="chooseCardType" name="chooseCardType">
                                      <option value="" selected>Pilih Kartu</option>	
                                      <option value="cc">Kartu Kredit</option>
                                      <option value="dc">Debit Card</option>
                                      
                                    </select>
                                </div>
                                <div class="nomargin">
                                	<select class="form-list small" id="payType" name="payType">
                                		<option value="" >Pilih jenis pembayaran</option>	
                                	</select>	
                                </div>	
                            </div>
                        </div>
                -->
                <div class="col-lg-12 col-xs-12 nopadding voffset4"> 
                   <div class="col-lg-12 col-xs-12"> 	      	
                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                        <div class="register-top-grid">                            
                            <div class="nomargin"> 
                                <form id="selPayment" action="<?php echo base_url('cart/checkout/sgo/pay/preview'); ?>" method="post">
                                    <table id="form_bayar" width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-bordered">

                                        <tr>
                                            <td style="width: 50%;">  Silahkan Pilih :</td>
                                            <td style="width: 50%;">

                                <select id="bank" name="bank" onchange="setSelectPay()">
                                	<?php
                                	 echo "<option value=\"\">--Pilih disini--</option>";
                                	
                                	  foreach($listBank as $dta) {
                                	  	//echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
                                          echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc|$dta->bankacc|$dta->bank_pemilik\">$dta->bankDisplayNm</option>";
                                      }
                                	?>
                                </select>
                                <input type="hidden" name="totalx" value="" />
                                <input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
                        		<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
                                <input type="hidden" name="bankid" id="bankid" value=""  />
                                <input type="hidden" name="bankCode" id="bankCode" value=""  />
                                <input type="hidden" name="bankDesc" id="bankDesc" value=""  />
                                <input type="hidden" name="bankDescDetail" id="bankDescDetail" value=""  />
                                <input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
                                <input type="hidden" name="charge_admin" id="charge_admin" value=""  />
                                    <input type="hidden" name="delivery_type" id="delivery_type" value="<?php echo $inp['delivery']; ?>"  />
                                    <input type="hidden" name="kode_unik" id="kode_unik" />
                                    <input type="hidden" name="kode_ref" id="kode_ref" />
                                    <input type="hidden" name="kode_pay" id="kode_pay" />
                                    <input type="hidden" name="total_byr" id="total_byr" />
                                    <input type="hidden" name="bank_acc" id="bank_acc" />
                                    <input type="hidden" name="bank_pemilik" id="bank_pemilik" />
                                    </td>

                                    </tr>
                                    <tr>
                                        <td id="metode_cc" style="width: 50%;"></td>
                                        <td style="width: 50%;">
                                            <select id="option_cc" name="option_cc" onchange="setSelectCC()" hidden>
                                                <?php
                                                echo "<option value=\"\">--Pilih disini--</option>";

                                                foreach($listCC as $dta) {
                                                    echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc|$dta->bankacc|$dta->bank_pemilik\">$dta->bankDisplayNm</option>";
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" name="ccid" id="ccid" value=""  />
                                            <input type="hidden" name="ccCode" id="ccCode" value=""  />
                                            <input type="hidden" name="ccDesc" id="ccDesc" value=""  />
                                            <input type="hidden" name="ccDescDetail" id="ccDescDetail" value=""  />

                                        </td>

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input id="proses_bayar" type="submit" value="Proses Pembayaran" /></td>
                                        <input type="hidden" name="sisa_bayar" id="sisa_bayar" value="<?php echo $totPayAndShip; ?>" />

                                    </tr>
                                        </table>
                                </form>
                                
                            </div>
                             <div class="nomargin">
                             	&nbsp;
                             </div>	
                        </div>
                    </div>
                   </div> 
                           
                        <!-- detail for credit card-->
                        <!-- 02/11/2015 DION-->
    					<!--<div class="col-lg-12 col-xs-12 nopadding" id="choosePaymentDiv">
                        	<form method="post" action="<?php echo base_url('pay/inp/test'); ?>">
                        		<input type="hidden" name="temp_orderid" value="<?php echo $key;?>" />
                        		<input type="hidden" name="temp_paymentId" value="<?php echo $payID;?>" />
                        		<input type="submit" value="Test Input ke DB" />
                        	</form>
                     </div>-->
                         
                    </div>                    
				</div>
        	</div>
        	<!-- Paymen Method-->
            
			
		</div>
        <!-- Left Content-->
        
        <!-- right Content-->
		<div class="col-lg-5 col-xs-12 nopadding voffset4">
        	<!-- Adress Delivery-->
            <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-check-square-o"></i> Alamat Tujuan Pengiriman
                </span>
                <div class="col-lg-12 col-xs-12 cart_header">
                 <?php
                  $inp = $this->session->userdata('personal_info');
				  if($inp['delivery'] == "2")
				   {
				   	 //echo $inp['firstname']." ".$inp['lastname']."</br>";
				   	 echo $inp['nama_penerima']."</br>";
					 echo $inp['alamat']."</br>";
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 //echo $inp['nama_kelurahan']."</br>";
					 echo $inp['notlp']."</br>";
				   }
				  else {
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 echo $inp['nama_stockist']."</br>";
				  }	
                  ?>	
                       	 
                </div>
            </div>   
        	<!-- Adress Delivery-->
            
            <!-- Order Details-->
        	<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Detail Pembelanjaan
                </span>
				<br />
				
                <div class="col-lg-12 col-xs-12 cart_header">
				ID Member     : <?php echo $inp['idmemberx']."<br />"; ?>
				Nama Member   : <?php echo $inp['membername']."<br />"; ?>
				Periode Bonus : <?php echo $inp['bnsperiod']."<br />"; ?>
				</div>
                
                <!-- header-->
                <div class="col-lg-12 col-xs-12 cart_header">          	
                    <div class="col-lg-4 col-xs-4" align="right">Product </div>
                    <div class="col-lg-2 col-xs-2" align="right">Qty</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Price</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Amount</div>          	 
                </div>  
                <!-- header-->   
                
                <!-- product item-->
                
                
                <?php
                   $cart = $this->cart->contents();
				   //print_r($cart);
                   $pricecode = $this->session->userdata('pricecode');
                   $sub_pricecode=substr($pricecode,0,3);

                   foreach($cart as $items) {
                   	 echo "<div class=\"col-lg-12 col-xs-12 order_wrap\">";
						 echo "<div class=\"col-lg-4 col-xs-4 order_desc\">";
	                     echo "<h3>".$items['name']."</h3>";
	                     $price = 0; $sub = 0;  $totalPrice = 0; $totalPrice2 = 0; $totalBV = 0; 
	                     echo "</div>";
						 if($sub_pricecode == "12W") {
						 	$price = number_format($items['west_Cprice'], 0, ",", ".");
							$sub = number_format($items['subtotal_west_Cprice'], 0, ",", ".");
							$totalPrice = $this->cart->total_west_price();
							$totalPrice2 = number_format($this->cart->total_west_Cprice(), 0, ",", ".");
							$totalBV = number_format($items['bv'], 0, ",", ".");
						 } else {
						 	$price = number_format($items['east_Cprice'], 0, ",", ".");
							$sub = number_format($items['subtotal_east_Cprice'], 0, ",", ".");
							$totalPrice = $this->cart->total_east_price();
							$totalPrice2 = number_format($this->cart->total_east_Cprice(), 0, ",", ".");
							$totalBV = number_format($items['bv'], 0, ",", ".");
						 }
	                     echo "<div class=\"col-lg-2 col-xs-2\" align=right>".$items['qty']."</div>"; 
	                     //echo "<div class=\"col-lg-3 col-xs-3\" align=right>".$totalBV."</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".$price."</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".$sub."</div>
                     </div>";  
                   }  
                ?>	 
                    <!--<div class="col-lg-6 col-xs-6 order_desc">
                        <h3>Kinokatara</h3>
                        <p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>
                    </div>
                    <div class="col-lg-3 col-xs-3">5</div> 
                    <div class="col-lg-3 col-xs-3">Rp. 130.000</div> -->
                
                <!-- product item-->
                
                <!-- shipping cost-->
                <div class="col-lg-12 col-xs-12 order_wrap">   
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <h3>Biaya Kirim</h3>
                    </div>
                    <div class="col-lg-2 col-xs-2" align="right">
                    	<?php echo $this->cart->total_weight(); ?>
                    </div> 
                    <div class="col-lg-3 col-xs-3" align="right">&nbsp</div> 
                    <div class="col-lg-3 col-xs-3" align="right">
                    	<?php
                    	  //print_r($pricelist->price);
                    	  //$shipping_jne_info = $this->session->userdata('shipping_jne_info');
						  echo number_format($shipping_jne_info['price'], 0, ",", "."); 
                    	?>
                    </div> 
                </div>  
                <!-- shipping cost-->
                <?php
                 if($tot_discount <= $shipping_jne_info['price']) {
               	 ?>
               	 <div class="col-lg-12 col-xs-12 order_wrap">   
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <h3>Diskon Biaya Kirim</h3>
                    </div>
                    <div class="col-lg-2 col-xs-2" align="right">
                    	
                    </div>
                   
                    <div class="col-lg-3 col-xs-3" align="right">&nbsp</div> 
                    <div class="col-lg-3 col-xs-3" align="right">
                    	<font color="red"> - <?php echo number_format($tot_discount, 0, ",", ".");  ?> </font>
                    </div> 
                </div>  
               	 <?php
                 }
                 ?> 
                <!-- subtotal-->
                <div class="col-lg-12 col-xs-12 order_total_wrap">   
                    <div class="col-lg-6 col-xs-6">                	
                        Subtotal
                    </div>
                    <div class="col-lg-6 col-xs-6">                	
                        <p>
                        <?php 
                        
                        $total = $totPayAndShip;
                        echo number_format($total, 0, ",", "."); 
                        ?>
                        </p>
                    </div>
                </div>
                <!-- subtotal-->
                
                <!-- order number
                <div class="col-lg-12 col-xs-12 order_number">                	
                    <p>No. Order Anda Adalah :</p>
                    <p><span>#ha7a63hd8</span></p>
                </div> --> 
                <!-- order number-->                  
			</div>
            <!-- Order Details-->            
		</div>
        <!-- right Content-->
        
        <!-- next button-->
        <div class="col-lg-12">
            <!--<button onclick="Shopping.back_to_cart('#divCheckOut','#formCart1')" type="button" class="btn1 btn2 btn-primary1 pull-left" style="margin-right:15px;" id="back"><i class="fa fa-arrow-left" ></i> Kembali ke Cart</button>&nbsp;&nbsp;
            <button onclick="Shopping.proceedPayment()" type="button" class="btn1 btn2 btn-primary1 pull-left" id="checkout"><span>Checkout</span><i class="fa fa-check-circle"></i></button>&nbsp;&nbsp;-->
                                
        </div>
        <!-- next button-->       
        
    </div>
    <!--</form>-->
    </div>
    <div id="nextPaymentProcessDIV"></div>
</div>
<script type="text/javascript">

  $("#selPayment").submit(function(event) {
	  var bankCode = $("#bankCode").val();
      var productCode = $("#bankDesc").val();
      if (bankCode != "" && productCode != "") {
	    return;
	  }
	 
	  alert("Please select payment..")
	  event.preventDefault();
	});

  function setSelectCC() {
      var x = $("#option_cc").val();
      var str = x.split("|");

      $("#ccid").val(str[0]);
      $("#bankid").val(str[0]);

      $("#ccCode").val(str[1]);
      $("#ccDesc").val(str[4]);
      $("#ccDescDetail").val(str[7]);

      var id_pay = str[0];
      var desc_cc = str[6];
      //alert(id_pay);
  }

  function setSelectPay() {
  	 var x = $("#bank").val();
  	 var bankDetail = $("#bank option:selected").text();
  	 //alert("isi : " +x);
  	 //var bankDesc = $("#bank option:selected").text();
  	 var str = x.split("|");
  	 $("#bankid").val(str[0]);
  	 $("#bankCode").val(str[1]);
  	 //$("#bankDesc").val(bankDesc);
  	 $("#bankDesc").val(str[4]);
  	 $("#charge_connectivity").val(str[2]);
  	 $("#charge_admin").val(str[3]);
  	 $("#bankDescDetail").val(bankDetail);

     var tipe_pay = str[0];

      if(tipe_pay == '30' || tipe_pay == '31'){
          var tot_belanja = $("#sisa_bayar").val();
          var payShip= $("#total_ship_price").val();
          var str = x.split("|");
          var bank_id = str[0];
          var bank_acc = str[5];
          var nm_acc = str[6];

          //alert(payShip);

          $("#bank_acc").val(bank_acc);
          $("#bank_pemilik").val(nm_acc);

          $.ajax({
              //url: All.get_url('sgo_ppob/saveTemp'),
              //url: All.get_url('getUnique'),
              url: All.get_url('getKodePay'),
              type: 'POST',
              dataType: 'json',
              data: {
                  token:'7383d8fa96681b037e4b1c5f7b7e4a17ff4e0d84a860ec4b27bea1158850245f6a827b22ae16f2441e655a97e8b1bf92034d3652a5ee18e9697b5c0b3f95d0de8OVJPF6WI30QG21Ed5n0jGD9rK.XjD0quEBA8a8-',
                  nominal: tot_belanja,
                  pay_ship: payShip,
                  no_rek_tujuan: bank_acc,
                  nama_rek_tujuan: nm_acc,
                  type:'Cr'
              },
              success:
                  function(result){

                      if(result.koneksi != "false"){
                          var kodeunik = result.kode_unik;
                          var kodepay = result.kd_pay;
                          var total_bayar = result.total_bayar;
                          var total_belanja = result.total_belanja;
                          var kode_ref = result.kd_ref;

                          console.log(result);
                          //alert("total bayar: " + total_bayar);

                          $("#kode_unik").val(kodeunik);
                          $("#kode_ref").val(kode_ref);
                          $("#kode_pay").val(kodepay);
                          $("#total_byr").val(total_bayar);
                      }else{
                          alert("Gagal Call API Mandiri");
                          window.location.reload();
                      }

                  },
              error: function (xhr, ajaxOptions, thrownError) {
                  alert(thrownError + ':' +xhr.status);
                  $("input[type=button]").removeAttr('disabled');
              }
          });
      }

      //alert(tipe_pay);

      if(tipe_pay == '8'){
          //alert("masuk sini");
          //alert(document.getElementById("form_bayar").rows[1].cells[0].innerHTML);
          var x = document.getElementById("form_bayar").rows[1].cells;
           x[0].innerHTML = "Metode Credit Card";
           $("#option_cc").show();

      }else{
          var x = document.getElementById("form_bayar").rows[1].cells;
          x[0].innerHTML = "";
          $("#option_cc").hide();
      }

  	
  }
  
  
  
  function submitdata() {
    var bankCode = $("#bankCode").val();
    var productCode = $("#bankDesc").val();
    var charge_connectivity = parseInt($("#charge_connectivity").val());
    var tot_dp = parseInt(<?php echo $total; ?>);
    var total = tot_dp + charge_connectivity;
    if (bankCode === "" || productCode === "") {
		alert("Please Select Payment Method");
		
	}else{
		
	 	 
	 	 var data = {
						key : "<?php echo $key;?>",
						paymentId : "<?php echo $payID;?>",
						paymentAmount : total,
						backUrl : encodeURIComponent("http://www.k-net.co.id/pay/sgo/finish/dev/<?php echo $payID;?>"),
						bankCode : bankCode,
						bankProduct: productCode
				    },
			sgoPlusIframe = document.getElementById("sgoplus-iframe");
					
			if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
			SGOSignature.receiveForm();
					
	}
}
</script>
<!--Payment Wrapper-->
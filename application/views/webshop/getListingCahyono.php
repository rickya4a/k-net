    <div class="container">
        <!--right-->
        <div class="col-md-12">
            <!-- Track Order -->
            <!-- tracking content header-->

                <?php

                ?>

                <div class="col-lg-12 col-xs-12 cart_header">
                    <div class="col-lg-12">
                        Hasil Bonus Report
                    </div>
                </div>

                <div class="col-lg-12 col-xs-12 cart_header">
                    <div class="col-lg-2 col-md-2 col-xs-12">Bulan</div>
                    <div class="col-lg-1 col-md-1 col-xs-12">Total Bonus</div>
                    <div class="col-lg-1 col-md-1 col-xs-12">Total Potongan</div>
                    <div class="col-lg-2 col-md-2 col-xs-12">Net Bonus</div>
                    <div class="col-lg-6 col-md-6 col-xs-12">Action</div>

                </div>


                <?php
                $xs = "col-xs-12";
				$md1 = "col-md-2";
				$md2 = "col-md-1";
				$md3 = "col-md-1";
				$md4 = "col-md-2";
				$md5 = "col-md-2";
				$md6 = "col-md-2";
				$md7 = "col-md-2";
                if(empty($report)){
                    echo "Data Tidak Ada";
                }else {
                    foreach ($report as $row) {
                        $idmember = $row->distributorcode;
                        $netincome = number_format($row->netincome, 0, ".", ",");
                        $totBonus = number_format($row->totBonus, 0, ".", ",");
                        $tax = number_format($row->tax, 0, ".", ",");
                        $ar_total = number_format($row->ar_total, 0, ".", ",");
                        $totBonus_Nett = number_format($row->totBonus_Nett, 0, ".", ",");
                        $nama = $row->nama;
                        $voucherno = $row->VoucherNo;

                        $year = $row->bonusyear;
                        $month = $row->bonusmonth;
                        //echo $month;
                        $aksi = site_url('bonus/action');
                        //$route['api/voucher/(:any)/(:any)/(:any)'] = 'mobile_api/listVoucherMember/$1/$2/$3';
                        $aksi2 = site_url('api/voucher/' . $idmember . '/' . $month . '/' . $year);
                        $aksi3 = site_url('bonus/action/pajak');

                        if ($nama != null) {

                            if (($month < 8) && ($year >= 2017)) {

                                if ($voucherno == null) {

                                    echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $md1 $xs\">$month</div>
                                    <div class=\"col-lg-1 $md2 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $md3 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $md4 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $md5 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>

                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $md7 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi3\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Tax Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                }
                                else {

                                    if ($year == 2018) {

                                        echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Voucher\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi3\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Tax Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                    } else {

                                        echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Voucher\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi3\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Tax Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                    }
                                }
                            }
                            else {
                                if ($voucherno == null) {

                                    echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>

                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>

                                        </form>
                                    </div>

                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi3\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Tax Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                }
                                else {

                                    echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>

                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Voucher\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>

                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi3\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Tax Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                }
                            }
                        } else {

                            if (($month < 8) && ($year >= 2017)) {

                                if ($voucherno == null) {

                                    echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>

                                        </form>
                                    </div>
                                    </div>";

                                }
                                else {

                                    if ($year == 2018) {

                                        echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Voucher\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                    }
                                    else {

                                        echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Voucher\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                    }

                                }
                            } else {

                                //pak toto minta jgn ditampilin dulu
//                                if($month == 3){
//
//                                }

                                if ($voucherno == null) {

                                    echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>

                                        </form>
                                    </div>
                                    </div>";
                                }
                                else {

                                    echo "<div class=\"col-lg-12 $xs cart_content text-center\" id=\"\">
                                    <div class=\"col-lg-2 $xs\">$month</div>
                                    <div class=\"col-lg-1 $xs\">$totBonus</div>
                                    <div class=\"col-lg-1 $xs\">$tax</div>
                                    <div class=\"col-lg-2 $xs\">$totBonus_Nett</div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Bonus Report\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    <div class=\"col-lg-2 $xs\">
                                        <form id=\"cekBonus\" method=\"POST\" method=\"POST\" target=\"_blank\" action=\"$aksi2\">
                                            <input type=\"hidden\" id=\"month\" name=\"month\" value=$month>
                                            <input type=\"hidden\" id=\"year\" name=\"year\" value=$row->bonusyear>
                                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Voucher\" class=\"btn btn-primary1\" />
                                        </form>
                                    </div>
                                    </div>";
                                }

                            }
                        }
                    }
                }
                ?>
        </div>
    </div>
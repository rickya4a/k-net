<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;&nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;Perjalanan Rohani&nbsp;&nbsp;
             <span>&gt;</span>
        </li>
        <li class="home">
             <span class="red">&nbsp;Pembayaran Cicilan Umroh&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->
<div class="row vpullset4" style="min-height: 415px;">
	<div class="container">
        <div class="col-md-12 col-xs-12 vpullset3" style="margin-top: -12px;"> 
        <span class="title_header">
            <i class="fa fa-usd"></i> SETORAN TABUNGAN K-SMART Umroh
        </span>
            <form id="formCicilan" method="POST" action="<?php echo site_url('umroh/installments/pay');?>" onkeypress="return event.keyCode != 13;">    
              <div class="register-top-grid clearfix">                 
                    <div>
                        <label class="label-list">RegisterNo</label><br/>
                        <input tabindex="1" type="text" id="regnos" name="regnos" onchange="Umroh.getRegnoInfo(this.value)" placeholder="contoh : U00000001"/>
                    </div>
            	   
                    <div>
                        <label class="label-list">ID Member</label><br/>
                        <input tabindex="2" class="required uppercase" type="text" id="idmember" name="idmember" readonly="true"/>
                    </div>
                    
                    <div>
                        <label class="label-list">Nama Member</label><br/>
                        <input tabindex="3" class="uppercase" type="text" id="nmmember" name="nmmember" readonly="true"/>
                    </div>
                    
                    <div>
                        <label class="label-list">Paket Perjalanan</label><br/>
                        <input tabindex="4" class="uppercase" type="text" id="pktUmroh" name="pktUmroh" readonly="true"/>
                        <input type="hidden" id="tipePerjalanan" name="tipePerjalanan" readonly="true"/>
                    </div>
            
                    <div>
                         <label class="label-list">Saldo</label><br/>
                        <input  tabindex="5" type="text" id="saldo" name="saldo" readonly="true"/>
                    </div>
                   
                    <div>
                        <label  class="label-list">Cicilan ( Rp )<font color="red">&nbsp;*</font> tanpa titik (Contoh 1500000)</label><br/>
                        <input  tabindex="6" class="required uppercase" type="text" id="amtUmroh" name="amtUmroh"/>
                    </div>
                    
                    <div>
                        <label  class="label-list">Tipe Pembayaran</label><br/>
                        <select id="tipepaymentt" name="tipepaymentt" tabindex="7" style="width: 96%;height: 35px;">
                                
                              <?php
                                  foreach($bank as $row){
                                        echo "<option value=\"$row->bankCode\">$row->bankDesc</option>";
                                  }
                              ?>
                        </select>
                    </div>
                    <br />
                    
                </div>
                
                    <!--<button type="button" name="netbankings" class="btn1 btn2 btn-primary1 pull-left" id="netbankings" onclick="Umroh.sendAmtUmroh();">Lanjut Pembayaran&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>-->
                    <button type="submit" name="netbanking" class="btn1 btn2 btn-primary1" id="netbanking" >Lanjut Pembayaran&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
            </form> 
        </div>
    </div>
</div>

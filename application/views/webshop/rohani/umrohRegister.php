<style>
.register-top-grid > .col-md-6.col-xs-12 {
min-height: 60px;
}	
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="#" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;&nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;Perjalanan Rohani&nbsp;&nbsp;
             <span>&gt;</span>
        </li>
        <li class="home">
             <span class="red">&nbsp;Pendaftaran Umroh&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div id="formJamaah">
<div class="row vpullset4" style="min-height: 415px;">
	<div class="container">
        <div class="col-md-12 col-xs-12 vpullset3" style="margin-top: -12px;">
	           <form id="formUmroh" method="POST">
               <!--<form id="formUmroh" method="POST" action="<?php //echo "".site_url('umroh/preview')."";?>">-->
                    <div class="col-md-12" style="background:#bfedaa; padding-bottom:4px; padding-top:4px; margin-bottom:6px;">
                        <div class="col-md-12 text-center" style="padding:4px 0; margin-bottom:4px; background:#5CB85C; color: #fff;  ">
                            <label>Tipe Calon Jamaah<font color="#bfedaa">&nbsp;*Silahkan Pilih Tipe Calon Jamaah</font></label>
                        </div>
                        <div class="col-md-4" style="padding-bottom:4px; padding-top:4px; background: #d6fac5;">
                            <input name="tipeJamaah" id="tipeJamaah1" type="radio" value="1" checked="checked"/> Member
                        </div>
                        <div class="col-md-4" style="padding-bottom:4px; padding-top:4px; background:#e7fedc;">
                            <input name="tipeJamaah" id="tipeJamaah2" type="radio" value="2" /> Bukan Member kurang dari 17 tahun
                                
                        </div>
                        <div class="col-md-4" style="padding-bottom:4px; padding-top:4px; background: #d6fac5;">
                            <input name="tipeJamaah" id="tipeJamaah3" type="radio" value="3" /> Bukan Member diatas 17 tahun
                        </div>
                    </div>
                    
                    <div class="col-md-12" style="background:#bfedaa; padding-bottom:4px; padding-top:4px; margin-bottom:6px;">
                        <div class="col-md-12 text-center" style="padding:4px 0; margin-bottom:4px; background:#5CB85C; color: #fff;  ">
                            <label>Tipe Perjalanan Rohani<font color="#bfedaa">&nbsp;</font></label>
                        </div>
                        <div class="col-md-4" style="padding-bottom:4px; padding-top:4px; background: #d6fac5;">
                            <input name="tipePrjlanan" id="tipePerjalanan1" type="radio" value="umr" checked="checked"/> Umroh
                        </div>
                    </div>      
                   
	    	      <div class="register-top-grid">                 
                        
                    <div>
                        <label class="label-list">ID Member K-link<font color="red">&nbsp;*Kosongkan jika bukan member k-link </font></label><br/>
    	                <input tabindex="1" class="uppercase" type="text" id="idmember" name="idmember" value="<?php echo getUserID();?>" onblur="Umroh.get_distributor_info(this.value)" />
                    </div>
    	    	   
    	            <div>
    	                <label class="label-list">Nama Lengkap<font color="red">&nbsp;*</font></label><br/>
    	                <input tabindex="2" class="required uppercase" type="text" id="fullnm" name="fullnm" value="<?php echo getUsername();?>" readonly="true"/>
    	            </div>
                    
                    <div class="hide">
                        <label class="label-list">ID Sponsor</label><br/>
    	                <input tabindex="3" class="uppercase" type="text" id="idsponsor" name="idsponsor" onchange="Umroh.get_sponsor_info(this.value)" />
                    </div>
                    
                    <div class="hide">
                        <label class="label-list">Nama Sponsor</label><br/>
    	                <input tabindex="4" class="uppercase" type="text" id="nmsponsor" name="nmsponsor" />
                    </div>
            
    	            <div>
    	                 <label class="label-list">No KTP<font color="red">&nbsp;*</font></label><br/>
    	                <input  tabindex="5" type="text" id="idno" name="idno" value="<?php echo getUserIdno();?>" readonly="true" onchange="Umroh.cekNoKtp(this.value)"/> <!--onblur="Umroh.cekNoKtp(this.value)"/> -->
    	            </div>
                    
                    <div>
    	                <label  class="label-list">Jenis Kelamin<font color="red">&nbsp;*</font></label><br/>
    	                <select tabindex="6"  class="form-list required" id="sex" name="sex">
    	                	<option value="M">Pria</option>
    	                	<option value="F">Wanita</option>
    	                </select>
    	            </div>
                    
    	            <div>
    	                <label  class="label-list">Tempat Lahir<font color="red">&nbsp;*</font></label><br/>
    	                <input  tabindex="7" class="required uppercase" type="text" id="birthplace" name="birthplace"/>
    	            </div>
                    
    	            <div style="height: 60px;">
    	                <label class="label-list">Tgl Lahir<font color="red">&nbsp;*</font></label><br/>
                            <?php echo datebirth_combo(0,80, 'birthdt'); ?>
                       
    	            </div>
                    
                    <div>
    	                <label  class="label-list">Nama Ayah Kandung<font color="red">&nbsp;*</font></label><br/>
    	                <input  tabindex="8" class="required uppercase" type="text" id="fathersnm" name="fathersnm"/>
    	            </div>
                    
                    <div>
    	                <label class="label-list">Email<font color="red">&nbsp;*</font></label><br/>
    	                <input tabindex="9" type="text" id="email" name="email" value="<?php echo getUserEmail();?>" />
    	            </div>
                    
    	            <div>
    	                <label class="label-list">Alamat<font color="red">&nbsp;* Sertakan Kecamatan dan Kelurahan</font></label><br/>
    	                <!--<input tabindex="9" class="required uppercase" type="text" id="addr1" name="addr1" value="<?php //echo getUserAddress();?>" readonly="true"/>-->
                        <textarea tabindex="10" rows="3" draggable="" name="addr1" id="addr1" style="resize: none;" class="required uppercase" ><?php echo getUserAddress();?></textarea>
    	            </div>
                    
                    <div>
    	                <label class="label-list">Alamat Statement Bonus<font color="red">&nbsp;*</font></label><br/>
                        <textarea tabindex="11" rows="3" draggable="" name="addr2" style="resize: none;" class="required uppercase"></textarea>
    	            </div>
                    
                    <!--<div>
    	                <label class="label-list">Provinsi</label><br/>
    	                <input tabindex="10" type="text" id="prov" name="prov" value="" class="uppercase"/>
    	            </div>
                    
                    <div>
    	                <label class="label-list">Kota</label><br/>
    	                <input tabindex="11" type="text" id="kota" name="kota" value="" class="uppercase"/>
    	            </div>
                    
                    <div>
    	                <label class="label-list">Kecamatan</label><br/>
    	                <input tabindex="12" type="text" id="kecamatan" name="kecamatan" value="" class="uppercase"/>
    	            </div>
                    
                    <div>
    	                <label class="label-list">Kelurahan/Desa</label><br/>
    	                <input tabindex="13" type="text" id="kelurahan" name="kelurahan" value="" class="uppercase"/>
    	            </div>-->
                    
                    <div>             
    	                <label class="label-list">Kode Pos</label><br/>
    	                <input tabindex="12" type="text" id="zipcode" name="zipcode" value="" class="only-number"/>
    	            </div>
                    
                    <div>             
    	                <label class="label-list">Kartu & Statement Bonus</label><br/>
    	                <select tabindex="13"  class="form-list required" id="idstk" name="idstk">
    	                	<option value="0">--Pilih Stockist--</option>
                            <?php
                                foreach($idstk as $row){
                                    echo "<option value = \"$row->loccd\">".$row->loccd." - ".$row->fullnm." </option>";    
                                }
                                
                            ?>
    	                </select>
                        
    	            </div>
                    
    	            <div>
    	                <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
    	     
    	                <input tabindex="14" class="required only-number" type="text" id="tel_hp" name="tel_hp" value="<?php echo getUserPhone();?>" readonly="true" onchange="Umroh.cekNoHp(this.value)"/> <!-- onblur="Umroh.cekNoHp(this.value)"/> -->
    	                
    	            </div>
                    
                    <div>
    	                <label class="label-list">No. Telp Rmh</label><br/>
    	                <input tabindex="15" class="required only-number" type="text" id="tel_hm" name="tel_hm" value="<?php echo getUserPhonehome();?>" />
    	            </div>
                    
                    <div>
    	                <label class="label-list">Hubungan Keluarga<font color="red">&nbsp;*</font></label><br/>
    	                <select tabindex="16"  class="form-list required" id="statusJamaah" name="statusJamaah">
    	                	<option value="1">Anak</option>
                            <option value="2">Istri</option>
                            <option value="3">Saudara</option>
    	                	<option value="4">Suami</option>
                            <option value="5">Lainnya</option>
    	                </select>
    	            </div>
                   
                    <div>
    	                <label class="label-list">Jadwal Keberangkatan<font color="red">&nbsp;*</font></label><br/>
    	                <select tabindex="18"  class="form-list required" id="jdwlbrkt" name="jdwlbrkt">
    	                	<?php 
                                foreach($jadwal as $row){
                                    if($row->type_ks !="2"){
                                        $tglbrkt = date("d-m-Y",strtotime($row->departuredt));
                                    echo "<option value=\"".$row->id."-".$row->departuredesc."\">".$tglbrkt." ".$row->departuredesc."</option>";
                                    }
                                    
                                }
                            ?>
                            
    	                </select>
    	            </div>
                    
                    <div>             
    	                <label class="label-list">Nama Sesuai Passport<font color="red">&nbsp;</font></label><br/>
    	                <input tabindex="19" type="text" id="passportnm" name="passportnm" value="" class="uppercase" />
    	            </div>
                    
                     <div>             
    	                <label class="label-list">Nomor Passport<font color="red">&nbsp;</font></label><br/>
    	                <input tabindex="20" type="text" id="passportno" name="passportno" value="" class="uppercase"/>
    	            </div>
                    
    	               <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn1 btn2 btn-primary1 pull-left" id="submits" onclick="Umroh.getPreview()" style="margin-right: 750px;" >Lanjut Preview&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
                        <!--<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits" style="margin-right: 750px;" >Lanjut Preview&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>-->
                        <!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" id="submits" onclick="Umroh.addJamaah()">Tambah Calon Umroh&nbsp;&nbsp;<i class="fa fa-plus"></i></button><br />-->
                    </div> 
	           </form> 
        </div>
    </div>
</div>
</div>
<div id="jamaahPreview"></div>

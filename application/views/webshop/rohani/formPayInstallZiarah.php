<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;&nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;Perjalanan Rohani&nbsp;&nbsp;
             <span>&gt;</span>
        </li>
        <li class="home">
             <span class="red">&nbsp;Setoran Cicilan K-Smart Ziarah&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>

<!-- Breadcrumb -->
<!--form -->
<div class="row vpullset4" style="min-height: 415px;">
	<div class="container">
        <!-- LEFT CONTENT-->
    	<div class="col-lg-6 col-xs-12 nopadding voffset4">
            <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-usd"></i> SETORAN CICILAN K-SMART ZIARAH
                </span>
                <div class="col-lg-12 col-xs-12"> 
                    <div class="col-lg-12 col-xs-12">
                        <!--<form id="pembayaranSGo">-->
                            <div class="register-top-grid clearfix">
                                <div style="width:100%">
                                    <span><label>JumLah Setoran Cicilan</label></span>
                                    <input type="text" id="tot_setoran" name="tot_setoran" readonly="readonly" value="<?php echo "Rp. ".number_format($cicilann,0,".",".")."";?>"/> 
                                </div>
                                <!--<div style="width:100%">
                                    <span><label>Charge Connectivity Bank</label></span>
                                    <input type="text" id="connectivity" name="connectivity" readonly="readonly" value="<?php echo "Rp. ".number_format($charge_connectivity,0,".",".")."";?>"/> 
                                </div>-->
                                <div style="width:100%">
                                    <span><label>Charge Admin</label></span>
                                    <input type="text" id="biaya_admin" name="biaya_admin" readonly="readonly" value="<?php echo "Rp. ".number_format($charge_admin,0,".",".")."";?>"/> 
                                </div>
                                <div style="width:100%">
                                    <span><label>Total Pembayaran</label></span>
                                    <input type="text" id="total_biaya" name="total_biaya" readonly="readonly" value="<?php echo "Rp. ".number_format($total,0,".",".")."";?>"/> 
                                </div>
                                
                                <div style="width:100%">
                                    
                                    <button type="submit" name="submit" class="btn1 btn2 btn-primary1 pull-left" id="submit" onclick="submitdata()">Submit Pembayaran&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
                                </div>
                            </div>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END LEFT CONTENT-->
        
        <!-- RIGHT CONTENT-->
        <div class="col-lg-6 col-xs-12 nopadding voffset4">
            <div class="col-lg-12 col-xs-12">
                
                <div class="col-lg-12 col-xs-12">       	
                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                        <div class="register-top-grid">                            
                            <div class="nomargin">
                                <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
        <!-- END RIGHT CONTENT-->
     </div>
</div><!-- form -->
<script type="text/javascript">
function submitdata(){
    var bankCode = "<?php echo $bankCode;?>";
	var productCode = "<?php echo $bankDesc;?>"
    if ((bankCode || productCode) === 'undefined'){
		alert("Please Select Payment Method");
		
	}else{
		
	 	 var data = {
						key : "<?php echo $key;?>",
						paymentId : "<?php echo $regno;?>",
						paymentAmount : "<?php echo $cicilann;?>",
						backUrl : encodeURIComponent('http://www.k-net.co.id/ziarah/finish/paymentCicilan/<?php echo $regno;?>'),
						bankCode : bankCode,
						bankProduct: productCode
				    },
			sgoPlusIframe = document.getElementById("sgoplus-iframe");
					
			if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
			SGOSignature.receiveForm();		
		}
}  
</script>
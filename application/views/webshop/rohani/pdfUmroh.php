<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
        
        $prefix = $dtUmroh[0]->tipe_perjalanan;
        if($prefix == '1'){
            $pref = 'UMROH';
            $jx = 'JAMAAH';
            $img = 'headerUmroh.jpg';
        }else{
            $pref = 'ZIARAH YERUSALEM';
            $jx = 'JEMAAT';
            $img = 'headerZiarah.jpg';
        }
        
        $pdf->Image("assets/images/".$img."",5,15,198,35);
        
        $pdf->SetXY(10,50);
        
        
        $titleCol2 = 95;
        $lebarCell = 5;
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Helvetica','B',14);
        $pdf->Cell(190,$lebarCell,"BUKTI PENDAFTARAN K-SMART ".$pref."",0,0,'C',true); 
        $pdf->Ln();
        $pdf->Ln();
        
        $pdf->SetFont('Helvetica','B',8);
        $pdf->SetFillColor(255,255,255);
        foreach($dtUmroh as $row)
        {
            $prefixID = substr($row->idrekruiter,0,2);
            if($row->sex == 'F'){
                $sex = "Perempuan";
            }else{
                $sex = "Pria";
            }
            if($row->idrekruiter == 'WIRRO' || $row->idrekruiter == 'TIA' ||  $row->idrekruiter == 'TANTO' || $row->idrekruiter == 'ADMIN' || $row->idrekruiter == 'DION'){
                $rekruiterID = '';
                $rekruiterName = '';
            }else{
                $rekruiterID = $row->idrekruiter;
                $rekruiterName = $row->rekruiternm;
            }
            $pdf->Cell(190,$lebarCell,"DATA CALON JAMAAH K-SMART $pref",1,0,'C',true);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NO REGISTER $pref",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->registerno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"ID MEMBER $jx",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->dfno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NAMA $jx",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->fullnm,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"TEMPAT / TANGGAL LAHIR",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->birthplace ."  /  ". date("d F Y",strtotime($row->birthdate)),0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"JENIS KELAMIN",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$sex,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"ALAMAT",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->addr1 ,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NO HP",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->tel_hp ,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NAMA BAPAK KANDUNG",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->father_name ,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"JADWAL KEBERANGKATAN",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,date("d-m-Y",strtotime($row->departuredt)) ."  /  ". $row->departuredesc,0,0,'L',true);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell(190,$lebarCell,"DATA REKRUITER",1,0,'C',true);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"ID REKRUITER",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$rekruiterID,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NAMA REKRUITER",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$rekruiterName,0,0,'L',true);
            $pdf->Ln();
        }
        
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(250,10,62);
        $pdf->SetFont('Helvetica','',7);
        
        $pdf->Cell(190,$lebarCell,"SYARAT DAN KETENTUAN :",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"1. Pembayaran melalui transfer untuk pendaftaran, mohon bukti pembayaran dan bukti pendaftaran di fax ke nomor (021) 290-27010, Up. Ibu Lena.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"2. Untuk setoran cicilan K-SMART $pref,dapat dilakukan melalui transfer ke rekening mandiri KCP Mangga Dua, no rek 119-0028718888,",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"    mohon bukti pembayaran dan RegisterNo $pref di konfirm ke kantor pusat PT. K-Link, Up. Ibu Mariani.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"3. Pendaftaran program K-SMART $pref adalah distributor K-LINK (memiliki no. ID K-LINK).",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"4. Pendaftaran program K-SMART $pref untuk member baru,starterkit dan kartu keanggotan K-LINK dapat diambil",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"    pada stockist yang telah dipilih pada saat pendaftaran.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"5. Harga tidak mengikat dan dapat berubah sewaktu-waktu.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"6. Biaya paket K-SMART $pref harus telah lunas minimal 45 hari sebelum tanggal keberangkatan yang dipilih.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"7. Pembatalan keikutsertaan paket $pref dan pengembalian dana diatur sesuai dengan ketentuan dari perusahaan.",0,0,'L',true);
        /*,jika distributor meninggal dunia,",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"    keikutsertaan dapat dialihkan kepada ahli waris.",0,0,'L',true);*/
        $pdf->Ln();
        $pdf->Ln();
        //$pdf->Cell(190,5,"BV MEMBER REPORT",1,0,'C',true); 
        $pdf->Output();
?>
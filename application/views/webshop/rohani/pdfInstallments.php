<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
        
        $prefix = $dtCicilan[0]->tipe_perjalanan;
        if($prefix == '1'){
            $pref = 'UMROH';
            $jx = 'JAMAAH';
            $img = 'headerUmroh.jpg';
        }else{
            $pref = 'ZIARAH YERUSALEM';
            $jx = 'JEMAAT';
            $img = 'headerZiarah.jpg';
        }
        
        $pdf->Image("assets/images/".$img."",5,15,198,35);
        
        $pdf->SetXY(10,50);
        
        $titleCol2 = 95;
        $lebarCell = 5;
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Helvetica','B',14);
        $pdf->Cell(190,$lebarCell,"BUKTI SETORAN CICILAN K-SMART ".$pref."",0,0,'C',true); 
        $pdf->Ln();
        $pdf->Ln();
        
        $pdf->SetFont('Helvetica','B',8);
        $pdf->SetFillColor(255,255,255);
        foreach($dtCicilan as $row)
        {
            $pdf->Cell(190,$lebarCell,"DATA CALON JAMAAH K-SMART ".$pref."",1,0,'C',true);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NO REGISTER ".$pref."",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->registerno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"ID MEMBER ".$jx."",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->dfno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NAMA ".$jx."",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->fullnm,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"NO HP",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->tel_hp ,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"JADWAL KEBERANGKATAN",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,date("d-m-Y",strtotime($row->departuredt)) ."  /  ". $row->departuredesc,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"JUMLAH SETORAN",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,"Rp. ".number_format($row->tot_fund,0,".","."),0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"TOTAL TABUNGAN",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,"Rp. ".number_format($row->tot_tab,0,".","."),0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol2,$lebarCell,"BANK",0,0,'L',true);
            $pdf->Cell($titleCol2,$lebarCell,$row->bankDesc,0,0,'L',true);
            $pdf->Ln();
        }
        
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(250,10,62);
        $pdf->SetFont('Helvetica','',7);
        
        $pdf->Cell(190,$lebarCell,"SYARAT DAN KETENTUAN :",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"1. Pembayaran melalui transfer, mohon bukti pembayaran dan bukti pendaftaran di fax ke nomor (021) 290-27010, Up. Ibu Lena.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"2. Pembayaran melalui transfer untuk setoran cicilan, mohon bukti pembayaran dan RegisterNo ".$pref." di fax ke nomor (021) 290-27010, Up. Ibu Lena.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"3. Pendaftaran program K-SMART ".$pref." adalah distributor K-LINK (memiliki no. ID K-LINK).",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"4. Harga tidak mengikat dan dapat berubah sewaktu-waktu.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"5. Biaya paket K-SMART ".$pref." harus telah lunas minimal 45 hari sebelum tanggal keberangkatan yang dipilih.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"6. Pembatalan keikutsertaan paket ".$pref." dan pengembalian dana diatur sesuai dengan ketentuan dari perusahaan,jika distributor meninggal dunia,",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"    keikutsertaan dapat dialihkan kepada ahli waris.",0,0,'L',true);
        $pdf->Ln();
        $pdf->Ln();
        //$pdf->Cell(190,5,"BV MEMBER REPORT",1,0,'C',true); 
        $pdf->Output();
?>
<div class="row vpullset4">
    <div class="container">
    	<div class="col-md-12">
        <?php
                    foreach($dtJamaah as $row){
                        $registerNo = $row->registerno;
                        $idmemberjamaah = $row->dfno;
                        $nmjamaah = $row->fullnm;
                        $tmptLahir = $row->birthplace;
                        $tglLahir = date("d-m-Y",strtotime($row->birthdate));
                        $jdwl = date("d-m-Y",strtotime($row->departuredt));
                        $jdwldesc = $row->departuredesc;
                        $tipePrjlanan = $row->tipe_perjalanan;
                    }
                    //echo "sukses";
                    if($tipePrjlanan == '2'){
                        $x = 'Ziarah';
                        $y = 'Jemaat';
                    }else{
                        $x = 'Umroh';
                        $y = 'Jamaah';
                    }
                ?>
            <div class="col-lg-12 col-xs-12 cart_header" style="margin-top:50px;">  
                <div class="col-lg-12" style="font-size:16px;">
                   Hasil Pendaftaran <?php echo $x;?>
                </div>            
            </div>
            <form method="post" action="<?php echo "".site_url('umroh/print/notaUmroh')."";?>" id="printPDF" target="_blank">
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                
                <div class="col-lg-12 col-xs-12" style="font-size:14px; margin-bottom:50px; margin-top:20px;">
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Register No <?php echo $x;?>  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $registerNo;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">ID Member <?php echo $y;?>  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $idmemberjamaah;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Nama <?php echo $y;?>  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $nmjamaah;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Tempat Lahir  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $tmptLahir;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Tanggal Lahir  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $tglLahir;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Tanggal Keberangkatan  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo "".$jdwl." / ".$jdwldesc."";?>&nbsp;</div>    
                </div> 
            </div>
            <div class="col-md-12">
                <input type="hidden" name="regnos" value="<?php echo $registerNo;?>" id="regnos"/>
                <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits" style="margin-right: 750px;" >Print PDF&nbsp;&nbsp;<i class="fa fa-arrow-left"></i></button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Perjalanan Rohani&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Cek Mutasi Perjalanan Rohani&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--form -->
<div class="row vpullset4" style="min-height: 415px;">
<span id="formCekMutasi">
	<div class="container">
      <div class="col-md-12"> 
        <div class="col-md-6 col-md-offset-3 text-center">  
	        <form id="cekmutasi" method="POST" onkeypress="return event.keyCode != 13;">
	            <div class="track-order-grid">
                <div>
                  <span>Pencarian Mutasi Calon Peserta Perjalanan Rohani</span>
                  <span>Berdasarkan : </span>	                
	                <select class="required form-list" style="margin-bottom:10px; width:100%" id="searchMutation" name="searchMutation">
                         <option value="idmember" >ID Member</option>
                         <option value="regno" >Register No</option>
	                </select>
                    <span id="noreg" style="display: none;">
                        <input type="text" name="noregisters" size="14" placeholder="Register No" id="noregisters" />
                    </span>
                    <span id="memberid" style="display: none;">
                        <input type="text" name="idmembers" value="<?php echo getUserID();?>" size="14" placeholder="ID Member" id="idmembers"/>
                    </span>
	              </div>
                <div>
                    <button class="btn1 btn2 btn-primary1" type="button"  onClick="Umroh.searchMutasi();" id="submit">Cek Mutasi <i class="fa fa-arrow-right"></i></button>
                  <!--<input class="btn1 btn2 btn-primary1" type="button" value="Cek Transaksi" onClick="Sales.searchTransaksi();" name="submit" id="submit"/>-->
                </div>
              </div>	           
	        </form>          
        </div>        
      </div>
  </div>
  <div id="resPencarianUmroh"></div> 
  </span>
</div><!-- form --> 
  
<!--Checkout Wrapper-->
<script type="text/javascript">
$(document).ready(function() {
	$("#memberid").css('display', 'block');
    $("#noreg").css('display', 'none');
$("#searchMutation").change(function() {
     if( $("#searchMutation").val() == "idmember")
     {
        $("#memberid").css('display', 'block');
        $("#noreg").css('display', 'none');
     }
     else if( $("#searchMutation").val() == "regno")  
     {
        $("#memberid").css('display', 'none');
        $("#noreg").css('display', 'block');
     }
 });
});
</script>
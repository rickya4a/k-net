<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
        
        $prefix = $dtVch[0]->tipe_perjalanan;
        if($prefix == '1'){
            $pref = 'UMROH';
            $jx = 'JAMAAH';
            $img = 'headerUmroh.jpg';
        }else{
            $pref = 'ZIARAH YERUSALEM';
            $jx = 'JEMAAT';
            $img = 'headerZiarah.jpg';
        }
        
        $pdf->Image("assets/images/".$img."",5,15,198,35);
        
        $pdf->SetXY(10,50);
        
        $titleCol1 = 45;
        $titleCol2 = 92;
        $titleCol3 = 95;
        $titleCol4 = 95;
        $lebarCell = 5;
        
        
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Helvetica','B',14);
        $pdf->Cell(190,$lebarCell,"K-SMART $pref CASH VOUCHER",0,0,'C',true); 
        $pdf->Ln();
        $pdf->Ln();
        
        $pdf->SetFont('Helvetica','B',8);
        $pdf->SetFillColor(255,255,255);
        foreach($dtVch as $row)
        {
            $pdf->Cell($titleCol1,$lebarCell,"VOUCHER NO",0,0,'L',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol4,$lebarCell,$row->voucherno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"DATE",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,date('d/m/Y',strtotime($row->createdt)),0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"MEMBER CODE",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,$row->dfno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"MEMBER NAME",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,$row->fullnm,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"AMOUNT ( RP )",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,"Rp. ".number_format($row->tot_fund,0,".","."),0,0,'L',true);
        }
        
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        //$pdf->SetTextColor(250,10,62);
        $pdf->SetFont('Helvetica','',7);
        
        $pdf->Cell(55,$lebarCell,"- PRODUCT CODE",1,0,'C',true);
        $pdf->Cell(55,$lebarCell,"- PRODUCT DESCRIPTION",1,0,'C',true);
        $pdf->Cell(40,$lebarCell,"- QUANTITY",1,0,'C',true);
        $pdf->Cell(40,$lebarCell,"- AMOUNT -",1,0,'C',true);
        $pdf->Ln();
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Ln();
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Ln();
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Ln();
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Ln();
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(55,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Cell(40,$lebarCell," ",1,0,'C',true);
        $pdf->Ln();
        
        $pdf->Cell(150,$lebarCell,"* Total DP Amount",1,0,'R',true);
        $pdf->Cell(40,$lebarCell,"",1,0,'R',true);
        
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Helvetica','',9);
        
        $pdf->Cell(190,5,"For HQ/Branch/Stockist use only",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"PAYMENT MODE :",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"*Total DP of K-link products redeemed   :",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"Less : The K-Link cash voucher amount : Rp. 2,000,000",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"The difference paid by cash                    :",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"__________________________________________________________________________________________________________",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,5,"I certify that the products redeemed is correct and in a good condition",0,0,'L',true);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(50,5,"================",0,0,'L',true);
        $pdf->Cell(50,5,"================",0,0,'L',true);
        $pdf->Cell(90,5,"====================================",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(50,5,"Distributor's",0,0,'L',true);
        $pdf->Cell(50,5,"Distributor's TelNo",0,0,'L',true);
        $pdf->Cell(90,5,"Stockist's/Officer's Stamp & Signature",0,0,'L',true);
        //$pdf->Cell(190,5,"BV MEMBER REPORT",1,0,'C',true); 
        $pdf->Output();
?>
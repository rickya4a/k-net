<div class="row vpullset4">
    <div class="container">
    	<div class="col-md-12">
        <?php
                if(empty($headMutasi) || empty($detMutasi)){
                    echo "Data Tidak Ada";
                }else{
                    foreach($headMutasi as $row){
                        $dfno = $row->dfno;
                        $fullnm = $row->fullnm;
                        $jdwl = date("d-m-Y",strtotime($row->departuredt));
                        $jdwldesc = $row->departuredesc;
						$novac	=	$row->novac;
                        $perjalanan = $row->tipe_perjalanan;
                        
                        if($row->depart_status == '0'){
                            $status = "Belum Berangkat";
                        }else if($row->depart_status = '1'){
                            $status = "Sudah Berangkat";
                        }else{
                            $status = "Batal";
                        }
                    }
                    if($perjalanan== '1'){
                        $x = 'Umroh';
                        $y = 'Jamaah';
                    }else{
                        $x = 'Ziarah';
                        $y = 'Jemaat';
                    }
            ?>
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                    Hasil Pencarian Mutasi K-Smart <?php echo $x;?>
                </div>            
            </div>
            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo $dfno; ?> 
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Nama Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $fullnm; ?>
                </div>
                <div class="col-lg-3 col-xs-3">
                    Status Keberangkatan  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $status; ?>
                </div>
                <div class="col-lg-3 col-xs-3">
                    Jadwal Keberangkatan  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $jdwl ." / ". $jdwldesc; ?>
                </div>    
                <div class="col-lg-3 col-xs-3">
                    Virtual Account  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo "88146$novac" ?>
                </div>      
            </div>

            <!-- table result-->
            <!-- result header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-2 col-xs-2">Tanggal</div> 
                <div class="col-lg-2 col-xs-2">Deskripsi</div>
                <!--<div class="col-lg-2 col-xs-2">Kredit</div>
                <div class="col-lg-2 col-xs-2">Debit</div>-->
                <div class="col-lg-2 col-xs-2">Tipe</div>
                <div class="col-lg-2 col-xs-2">Jumlah</div>
                <div class="col-lg-2 col-xs-2">Saldo</div>
            </div>
            <!-- result header-->

            <?php
                $no = 1;
                $jml = 0;
                   foreach($detMutasi as $dt)
                   {
                        if($dt->description == 'Payment Manual' && $dt->tipe_perjalanan == '1'){
                            $desc = 'Dp K-Smart Umroh';
                        }elseif($dt->description == 'Payment Manual' && $dt->tipe_perjalanan == '2'){
                            $desc = 'Dp K-Smart Ziarah';
                        }else{
                            $desc = $dt->description;
                        }
                       $jml +=  $dt->tot_fund;
                        echo "
                            <form id=\"sendtrcd\" name=\"sendtrcd\" method=\"POST\">
                            <div class=\"col-lg-12 col-xs-12 cart_content text-center\" id=\"$no\">
                                <div class=\"col-lg-1 col-xs-1\">".$no."</div>
                                <div class=\"col-lg-2 col-xs-2\">".date("d/m/Y",strtotime($dt->createdt))."</div>
                                <div class=\"col-lg-2 col-xs-2\">".$desc."</div>
                                <div class=\"col-lg-2 col-xs-2\">Kredit</div>
                                <div class=\"col-lg-2 col-xs-2\">".number_format($dt->tot_fund,0,",",",")."</div>
                                <div class=\"col-lg-2 col-xs-2\">".number_format($jml,0,",",",")."</div>
                                
                            </div>
                            </form>
                        ";
                        $no++;
                   }
               }
            ?>
        </div>
    </div>
 </div>
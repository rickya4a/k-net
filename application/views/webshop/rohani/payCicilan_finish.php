<div class="row vpullset4">
    <div class="container">
    	<div class="col-md-12">
         <?php
                    foreach($dtCicilan as $row){
                        $regno = $row->regno;
                        $idmember = $row->idmember;
                        $nmmember = $row->nmmember;
                        $hp = $row->tel_hp;
                        $tot_fund = $row->tot_fund;
                        $pktumrh = $row->departuredesc;
                        $perjalanan = $row->tipe_perjalanan;
                    }
                    if($perjalanan == '1'){
                        $x = 'Umroh';
                        $y = 'Jamaah';
                    }else{
                        $x = 'Ziarah';
                        $y = 'Jemaat';
                    }
                    //echo "sukses";
         ?>
            <div class="col-lg-12 col-xs-12 cart_header" style="margin-top:50px;">  
                <div class="col-lg-12" style="font-size:16px;">
                   Hasil Pembayaran Setoran Cicilan K-Smart <?php echo $x;?> 
                </div>            
            </div>
            <form method="post" action="<?php echo "".site_url('umroh/print/notaCicilanUmroh')."";?>" id="printPDF" target="_blank">
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                <div class="col-lg-12 col-xs-12" style="font-size:14px; margin-bottom:50px; margin-top:20px;">
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Register No <?php echo $x;?></div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $regno;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">ID Member </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $idmember;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Nama <?php echo $y;?></div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo $nmmember;?>&nbsp;</div>
                    <div class="col-md-5 col-xs-6" style="text-align: left;">Total Setoran  </div>
                    <div class="col-md-7 col-xs-6" style="text-align: left;">: <?php echo "Rp. ".number_format($tot_fund,0,".",".")."";?>&nbsp;</div> 
                    <div class="col-md-7 col-xs-6" style="text-align: left;">Sukses Transaksi&nbsp;</div>   
                </div> 
            </div>
            <div class="col-md-12">
                <input type="hidden" name="regnos" value="<?php echo $regno;?>" id="regnos"/>
                <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits" style="margin-right: 750px;" >Print PDF&nbsp;&nbsp;<i class="fa fa-arrow-left"></i></button>
            </div>
            </form>
        </div>
    </div>
</div>
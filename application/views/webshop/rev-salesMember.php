<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Sales&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Cek Transaksi&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--form -->
<div class="row vpullset4">
	<div class="container">
    <div id="salesMember">
      <div class="col-md-12"> 
        <div class="col-md-6 col-md-offset-3 text-center">  
	        <form id="cektransaksi" method="POST">
	            <div class="track-order-grid">
                <div>
                  <span>Search Member Purchase</span>	                
	                <select  class="required form-list" style="margin-bottom:10px; width:100%" id="bulan" name="bulan">
  	                <?php
                      $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                      for($y=1;$y<=12;$y++)
                      {
                          if($y==date("mm"))
                          { 
                              $pilih="selected";
                          }
                          else
                          {
                              $pilih="";
                          }
                          
                          echo("<option value=\"".sprintf("%02s",$y)."\" $pilih>$bulan[$y]</option>"."\n");
                      }
                    ?>
	                </select>
                  <input type="text" name="tahun" size="14" placeholder="Tahun Bonus"/>
	              </div>
                <div>
                  <input class="btn1 btn2 btn-primary1" type="button" value="Cek Transaksi" onClick="Sales.searchTransaksi();" name="submit" id="submit">
                </div>
              </div>	           
	        </form>          
        </div>        
      </div>
    </div>
  </div>
</div><!-- form -->

<div id="hasilPencarian"></div> 
      
<!--Checkout Wrapper-->
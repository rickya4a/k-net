<style>
	.delivery_header {
    border-bottom: 1px solid #d0d0d0;
    border-top: 1px solid #d0d0d0;
    float: left;
    font-size: 12px;
    margin: 8px 0 1em;
    padding: 1em 0;
    text-transform: none;
}
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Payment Preview&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Preview Pembayaran&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">Preview Pembayaran <?php echo $bankDescDetail; ?></div>
            <div class="col-lg-12 col-xs-12 nomargin">
            	
                <p class="p2 nopadding" style="font-size:16px;">
                   <?php
                      //print_r($res);
                      $biaya = getTotalPayNetAndShipCost2();
					  $tot_hrs_dibayar = $biaya['total_pay'];
		    		  $dt['freeship'] = $biaya['freeship'];
					  $dt['discount_shipping'] = $biaya['tot_discount'];
                      $shipping_jne_info = $this->session->userdata('shipping_jne_info');
                      echo "<table width=70% align=center border=0>";
                      echo "<tr><td align=right>Tipe Pembayaran       :</td><td align=right>".$bankDescDetail."</td></tr>";
					  echo "<tr><td align=right>Total Pembelanjaan    :</td><td align=right>Rp. ".number_format($res[0]->total_pay, 2, ",", ".")."</td></tr>";
					  
					  echo "<tr><td align=right>Total BV              :</td><td align=right>".number_format(getTotalBV(), 2, ",", ".")."</td></tr>";
					  //echo "<tr><td align=right>Biaya Kirim           :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                      echo "<tr><td align=right>Biaya Connectivity    :</td><td align=right>Rp. ".number_format($charge_connectivity, 2, ",", ".")."</td></tr>";
                      echo "<tr><td align=right>Biaya Administrasi    :</td><td align=right>Rp. ".number_format($charge_admin, 2, ",", ".")."</td></tr>";
					  if($dt['freeship'] == "1") {
	                  	if($dt['discount_shipping'] > 0) {
	                  		echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
	                  		echo "<tr><td align=right>Disc Biaya Kirim        :</td><td align=right>Rp. -(".number_format($dt['discount_shipping'], 2, ",", ".").")</td></tr>";
	                  	} else {
	                  		echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. 0</td></tr>";
	                  	}
	                  	 
	                  } else {
	                  	 echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
	                  }
					  
					  //$total_semua = $res[0]->total_pay + $shipping_jne_info['price'] + $charge_connectivity + $charge_admin;
					  $total_semua = $tot_hrs_dibayar + $charge_connectivity + $charge_admin;
					  echo "<tr><td align=right>Total                 :</td><td align=right>".number_format($total_semua, 2, ",", ".")."</td></tr>";
                      echo "</table>";
					  $total_all = $res[0]->total_pay + $charge_connectivity;
                   ?>
                
                
                <!--<div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">-->
                	
                	<input type="hidden" id="total_all" value="<?php echo $total_all; ?>" />
                    <button class="btn2 btn2 btn-primary1" onclick="submitdataXX()">
                        <i class="fa fa-arrow-right"></i>
                        <span>Lanjut ke Pembayaran</span>
                    </button>
                    
                <!--</div>	-->
                </p>	
            </div>
            <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
		</div>	
	</div>
</div>
 
<script type="text/javascript">
    
  function submitdataXX() {
     var total_all = $("#total_all").val();
		
 	 var data = {
					key : "<?php echo $temp_orderidx;?>",
					paymentId : "<?php echo $temp_paymentIdx;?>",
					paymentAmount : <?php echo $total_all; ?>,
					//backUrl : "http://www.k-net.co.id/pay/sgo/finish/dev/$temp_paymentIdx;?>",
					backUrl : "<?php echo $backURL;?>",
					bankCode : "<?php echo $bankCode; ?>",
					bankProduct: "<?php echo $bankDesc; ?>"
			    },
		sgoPlusIframe = document.getElementById("sgoplus-iframe");
				
		if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
		SGOSignature.receiveForm();
					
		
}
</script>       
<!-- Login Form --> 
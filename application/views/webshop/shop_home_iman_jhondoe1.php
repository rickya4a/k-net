
<style>
    .soldout {
        content: " ";
        z-index: 10;
        display: block;
        position: absolute;
        height: 100%;
        top: 0;
        left: 6px;
        right: 0;
        background: rgba(0, 0, 0, 0.3);
        width: 96%;
    }

    p.soldtext {
        
        text-align: center;
        position: relative;
        color: white;
        background: #00BCD4;
        top: 86%;
        height: 32px;
        width: 95%;
        border-radius: 3px;
        left: 5px;
        font-size: 13px;
        padding-top: 7px;
    }

    input.greysold {
        border: none;
        padding-top: 14.6px;
    }
</style>

<script type="text/javascript">

    // To conform clear all data in cart.
    function clear_cart() {
        var result = confirm('Are you sure want to clear all bookings?');

        if (result) {
            window.location = "<?php echo base_url(); ?>index.php/shopping/remove/all";
        } else {
            return false; // cancel button
        }
    }

    function cek_lipstik(){
        var str1 = document.getElementById("prod1_color").value;
        var str2= document.getElementById("prod2_color").value;

        var res1 = str1.split("|");
        var prd1= res1[1];

        var res2 = str2.split("|");
        var prd2= res2[1];


        if(prd1.trim() == prd2.trim()){
            alert("Silahkan pilih warna lain untuk produk lipstik kedua .. ");
        }
    }

    function validasi_lipstik(){
        var str1 = document.getElementById("prod1_color").value;
        var str2= document.getElementById("prod2_color").value;

        var res1 = str1.split("|");
        var prd1= res1[1];

        var res2 = str2.split("|");
        var prd2= res2[1];


        if(prd1.trim() == prd2.trim()){
            alert("Silahkan pilih warna lain untuk produk lipstik kedua .. ");
            return false;
        }else{
            submitData();
        }
    }


    function submitData(){

        //K-Ion Nano + D'Flora
        var prd1 = document.getElementById("prod1_color").value;

        var res = prd1.substring(0, 2);

        if( res == "HC"){
            submitData1();
        }else{
            submitData2()
        }


    }

    function submitData1(){


        var size = document.getElementById("kwirst_size").value;
        var color = document.getElementById("kwirst_color").value;
        var prd1 = document.getElementById("knano_color").value;

        if(size.trim() != "#" && color.trim()  != "#" && prd1.trim()  != "#"){
            addToCartVera();

        }else{
            alert("Silahkan lengkapi detail produk ..");
        }

        console.log(size);
        console.log(color);
        console.log(prd1);

        //console.log("nomor1");
        //console.log(param.substring(0, 1));
        //var ptk=param.substring(0, 1);


        /*if((param >= "44" && param <= "60") || (param >= "68" && param <= "71")){
         addToCartVera2(param);

         }else{
         if(size.trim() != "#" && color.trim()  != "#" && prd1.trim()  != "#"){
         addToCartVera();

         }else{
         alert("Silahkan lengkapi detail produk ..");
         }
         }*/
    }

    function addToCartVera(){

        var size = document.getElementById("kwirst_size").value;
        var color = document.getElementById("kwirst_color").value;
        var prd1 = document.getElementById("knano_color").value;
        var prd2= size+'-'+color;

        var prod=[];
        prod.push(prd1);
        prod.push(prd2);

        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 5;
        var randomstring = '';
        for (var i=0; i<string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
        }

        //var unik= randomstring;
        //console.log("Kode: "+randomstring);
        //console.log(prod);
        console.log("nomor2");
        console.log(prd1);
        console.log(prd2);

        var unik="";

        $.ajax({
            dataType: 'json',
            url: All.get_url('dummyvera1/getProd'),
            data: {prdcd1: prd1, prdcd2: prd2},
            type: 'POST',
            success: function (result) {

                console.log("nomor3");
                console.log(result);

                if( result.response == "false"){
                    alert(result.message);

                }else{

                    //produk 1
                    var prdcd1 = result.prdcd1;
                    var prdnm1 = unik+result.prdnm1;
                    var bv1 = result.bv1;
                    var qty1 = 1;
                    var westPrice1 = result.price_w1;
                    var eastPrice1 = result.price_e1;
                    var eastCPrice1 = result.price_ce1;
                    var westCPrice1 = result.price_cw1;
                    var weight1 = result.weight1;
                    var desc=1;

                    prdcd1= "00"+prdcd1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd1, prdnm: prdnm1, bv: bv1, qty: qty1, westPrice: westPrice1, eastPrice: eastPrice1,westCPrice: westCPrice1, eastCPrice: eastCPrice1, weight : weight1, desc : desc, css_class : "prmbundle"},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log("SKIP1");
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + ':' +xhr.status);
                            All.set_enable_button();
                        }
                    });

                    //produk 2
                    var prdcd2 = result.prdcd2;
                    prdcd2= "00"+prdcd2;
                    //var prdnm2 = unik+"+"+result.prdnm2;
                    var prdnm2 = result.prdnm2;

                    var bv2 = result.bv2;
                    var qty2 = 1;
                    var westPrice2 = result.price_w2;
                    var eastPrice2 = result.price_e2;
                    var eastCPrice2 = result.price_ce2;
                    var westCPrice2 = result.price_cw2;
                    var weight2 = result.weight2;
                    var desc=1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd2, prdnm: prdnm2, bv: bv2, qty: qty2, westPrice: westPrice2, eastPrice: eastPrice2,westCPrice: westCPrice2, eastCPrice: eastCPrice2, weight : weight2, desc : desc, css_class : "prmbundle"},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log("SKIP2");
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + ':' +xhr.status);
                            All.set_enable_button();
                        }
                    });
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' + xhr.status);
            }
        });

        /*$.each(prod, function(index, value) {
         console.log(value);
         $.ajax({
         dataType: 'json',
         url: All.get_url('dummyvera/getProd'),
         data: {prdcd: value},
         type: 'POST',
         success: function (result) {
         console.log(result);

         var prdcd = result.prdcd;
         var prdnm = result.prdnm;
         var bv = result.bv;
         var qty = 1;
         var westPrice = result.price_w;
         var eastPrice = result.price_e;
         var eastCPrice = result.price_ce;
         var westCPrice = result.price_cw;
         var weight = result.weight;
         var desc=1;

         $.ajax ({
         url: All.get_url("cart/add"),
         type: 'POST',
         dataType: 'json',
         data: {prdcd: prdcd, prdnm: prdnm, bv: bv, qty: qty, westPrice: westPrice, eastPrice: eastPrice,westCPrice: westCPrice, eastCPrice: eastCPrice, weight : weight, desc : desc},
         success:
         function(data){
         All.set_enable_button();
         console.log(data);
         alert(data.message);
         if(data.response == "true") {
         var maxID = parseInt($("#maxID").val());
         var nextMaxID = maxID + 1;
         $("#maxID").val(nextMaxID);
         $("#showInfoSumCart").html(null);
         var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
         $("#showInfoSumCart").append(row);
         }

         },
         error: function (xhr, ajaxOptions, thrownError) {
         alert(thrownError + ':' +xhr.status);
         All.set_enable_button();
         }
         });

         },
         error: function (xhr, ajaxOptions, thrownError) {
         alert(thrownError + ':' + xhr.status);
         }
         });
         });*/
        /*var prdcd = $("#prdcd" +param).val();
         var prdnm = $("#prdnm" +param).val();
         var bv = $("#bv" +param).val();
         var qty = $("#qty" +param).val();
         var westPrice = $("#westPrice" +param).val();
         var eastPrice = $("#eastPrice" +param).val();
         var eastCPrice = $("#eastCPrice" +param).val();
         var westCPrice = $("#westCPrice" +param).val();

         var weight = $("#weight" +param).val();
         $.ajax ({
         url: All.get_url("cart/add"),
         type: 'POST',
         dataType: 'json',
         data: {prdcd: prdcd, prdnm: prdnm, bv: bv, qty: qty, westPrice: westPrice, eastPrice: eastPrice,westCPrice: westCPrice, eastCPrice: eastCPrice, weight : weight},
         success:
         function(data){
         All.set_enable_button();
         alert(data.message);
         if(data.response == "true") {
         var maxID = parseInt($("#maxID").val());
         var nextMaxID = maxID + 1;
         $("#maxID").val(nextMaxID);
         $("#showInfoSumCart").html(null);
         var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
         $("#showInfoSumCart").append(row);
         }

         },
         error: function (xhr, ajaxOptions, thrownError) {
         alert(thrownError + ':' +xhr.status);
         All.set_enable_button();
         }
         });
         */
    }

    function addToCartVera2(param){

        //produk 1
        var prdcd1 = $("#prdcd" +param).val();
        var prdnm1 = $("#prdnm" +param).val();
        var bv1 = $("#bv" +param).val();
        var qty1 = 1;
        var westPrice1 = $("#westPrice" +param).val();
        var eastPrice1 = $("#eastPrice" +param).val();
        var eastCPrice1 = $("#eastPrice" +param).val();
        var westCPrice1 = $("#westPrice" +param).val();
        var weight1 = $("#weight" +param).val();
        var desc=2;

        //alert(prdcd1);

        $.ajax ({
            url: All.get_url("cart/add"),
            type: 'POST',
            dataType: 'json',
            data: {prdcd: prdcd1, prdnm: prdnm1, bv: bv1, qty: qty1, westPrice: westPrice1, eastPrice: eastPrice1,westCPrice: westCPrice1, eastCPrice: eastCPrice1, weight : weight1, desc : desc, css_class : ""},
            success:
                function(data){
                    All.set_enable_button();
                    console.log("SKIP1");
                    console.log(data);
                    alert(data.message);
                    if(data.response == "true") {
                        var maxID = parseInt($("#maxID").val());
                        var nextMaxID = maxID + 1;
                        $("#maxID").val(nextMaxID);
                        $("#showInfoSumCart").html(null);
                        var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                        $("#showInfoSumCart").append(row);
                    }

                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' + xhr.status);
                All.set_enable_button();
            }

        });
    }

    function submitData2(){

        //K-Ion Nano + D'Flora
        var str1 = document.getElementById("prod1_color").value;
        var str2= document.getElementById("prod2_color").value;

        var res1 = str1.split("|");
        var prd1= res1[0].trim();

        var res2 = str2.split("|");
        var prd2= res2[0].trim();

        //termasuk D'Flora
        var prd3 = document.getElementById("prod3_color").value;
        var prd4= document.getElementById("prod4_color").value;
        var prd5 = document.getElementById("prod5_color").value;
        var prd6 = document.getElementById("prod6_color").value;
        var prd7 = document.getElementById("prod7_color").value;

        console.log(prd1);
        console.log(prd2);
        console.log(prd3);
        console.log(prd4);
        console.log(prd5);
        console.log(prd6);
        console.log(prd7);

        if(prd1.trim() != "#" && prd2.trim()  != "#" && prd3.trim()  != "#" && prd4.trim()  != "#" && prd5.trim()  != "#" && prd6.trim()  != "#" && prd7.trim()  != "#"){
            addToCartVe();

        }else{
            alert("Silahkan lengkapi detail produk ..");
        }

    }

    function addToCartVe(){

        //K-Ion Nano + D'Flora
        var str1 = document.getElementById("prod1_color").value;
        var str2= document.getElementById("prod2_color").value;

        var res1 = str1.split("|");
        var prd1= res1[0].trim();

        var res2 = str2.split("|");
        var prd2= res2[0].trim();

        //termasuk D'Flora
        var prd3 = document.getElementById("prod3_color").value;
        var prd4= document.getElementById("prod4_color").value;
        var prd5 = document.getElementById("prod5_color").value;
        var prd6 = document.getElementById("prod6_color").value;
        var prd7 = document.getElementById("prod7_color").value;



        var prod=[];
        prod.push(prd1);
        prod.push(prd2);
        prod.push(prd3);
        prod.push(prd4);
        prod.push(prd5);
        prod.push(prd6);
        prod.push(prd7);

        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 5;
        var randomstring = '';
        for (var i=0; i<string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
        }

        //var unik= randomstring;
        //console.log("Kode: "+randomstring);
        //console.log(prod);
        console.log("nomor2");
        console.log(prd1);
        console.log(prd2);
        console.log(prd3);
        console.log(prd4);
        console.log(prd5);
        console.log(prd6);
        console.log(prd7);

        var unik="";

        $.ajax({

            dataType: 'json',
            url: All.get_url('dummyvera1/getProd3'),
            data: {prdcd1: prd1, prdcd2: prd2, prdcd3: prd3, prdcd4: prd4, prdcd5: prd5, prdcd6: prd6, prdcd7: prd7},
            type: 'POST',
            success: function (result) {

                console.log("nomor3");
                console.log(result);

                if (result.response == "false") {
                    alert(result.message);

                    /*alert("Promo Produk Tidak Ditemukan ..");*/

                } else {

                    /* var i;
                     for (i=1;i<=7;i=i+1){}*/

                    //produk 1
                    var prdcd1 = result.prdcd1;
                    var prdnm1 = result.prdnm1;
                    var bv1 = result.bv1;
                    var qty1 = 1;
                    var westPrice1 = result.price_w1;
                    var eastPrice1 = result.price_e1;
                    var eastCPrice1 = result.price_ce1;
                    var westCPrice1 = result.price_cw1;
                    var weight1 = result.weight1;
                    var desc=1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd1, prdnm: prdnm1, bv: bv1, qty: qty1, westPrice: westPrice1, eastPrice: eastPrice1,westCPrice: westCPrice1, eastCPrice: eastCPrice1, weight : weight1, desc : desc, css_class : "prmbundle"},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log("SKIP2");
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            /*alert(thrownError + ':' +xhr.status);*/
                            alert("Promo Produk Tidak Ditemukan ..");
                            All.set_enable_button();
                        }
                    });
                }
            }
        });

    }

    function submitData3(){

        //Paket Ticket Anniv 2019
        var prd1 = document.getElementById("ticket").value;
        var prd2= document.getElementById("shirt_size").value;

        console.log(prd1);
        console.log(prd2);

        if(prd1.trim() != "#" && prd2.trim()){
            addToCartVe1();

        }else{
            alert("Silahkan lengkapi detail produk ..");
        }

    }

    function addToCartVe1(){

        //Paket Ticket Anniv 2019
        var prd1 = document.getElementById("ticket").value;
        var prd2= document.getElementById("shirt_size").value;



        var prod=[];
        prod.push(prd1);
        prod.push(prd2);

        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 5;
        var randomstring = '';
        for (var i=0; i<string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
        }

        //var unik= randomstring;
        //console.log("Kode: "+randomstring);
        //console.log(prod);
        console.log("nomor2");
        console.log(prd1);
        console.log(prd2);

        var unik="";

        $.ajax({

            dataType: 'json',
            url: All.get_url('dummyvera1/getProd4'),
            data: {prdcd1: prd1, prdcd2: prd2},
            type: 'POST',
            success: function (result) {

                console.log("nomor3");
                console.log(result);

                if (result.response == "false") {
                    alert(result.message);

                    /*alert("Promo Produk Tidak Ditemukan ..");*/

                } else {

                    /* var i;
                     for (i=1;i<=7;i=i+1){}*/

                    //produk 1
                    var prdcd1 = result.prdcd1;
                    var prdnm1 = result.prdnm1;
                    var bv1 = result.bv1;
                    var qty1 = 1;
                    var westPrice1 = result.price_w1;
                    var eastPrice1 = result.price_e1;
                    var eastCPrice1 = result.price_ce1;
                    var westCPrice1 = result.price_cw1;
                    var weight1 = result.weight1;
                    var desc=1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd1, prdnm: prdnm1, bv: bv1, qty: qty1, westPrice: westPrice1, eastPrice: eastPrice1,westCPrice: westCPrice1, eastCPrice: eastCPrice1, weight : weight1, desc : desc, css_class : "prmbundle"},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log("SKIP2");
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            /*alert(thrownError + ':' +xhr.status);*/
                            alert("Promo Produk Tidak Ditemukan ..");
                            All.set_enable_button();
                        }
                    });
                }
            }
        });

    }

    function submitData4(){

        //K-Ion Nano + D'Flora
        var str1 = document.getElementById("product1_color").value;
        var str2= document.getElementById("product2_color").value;

        var res1 = str1.split("|");
        var prd1= res1[0].trim();

        var res2 = str2.split("|");
        var prd2= res2[0].trim();

        //termasuk D'Flora

        var prd5 = document.getElementById("product5_color").value;
        var prd7 = document.getElementById("product7_color").value;


        console.log(str1);
        console.log(str2);
        console.log(prd5);
        console.log(prd7);

        if(prd1.trim() != "#" && prd2.trim()  != "#" && prd5.trim()  != "#" && prd7.trim()  != "#"){
            addToCartVe2();

        }else{
            alert("Silahkan lengkapi detail produk ..");
        }

    }

    function addToCartVe2(){

        //K-Ion Nano + D'Flora
        var str1 = document.getElementById("product1_color").value;
        var prd2= document.getElementById("product2_color").value;

        var res1 = str1.split("|");
        var prd1= res1[0].trim();

        //termasuk D'Flora
        var prd5 = document.getElementById("product5_color").value;
        var prd7 = document.getElementById("product7_color").value;



        var prod=[];
        prod.push(prd1);
        prod.push(prd2);
        prod.push(prd5);
        prod.push(prd7);

        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 5;
        var randomstring = '';
        for (var i=0; i<string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
        }

        //var unik= randomstring;
        //console.log("Kode: "+randomstring);
        //console.log(prod);
        console.log("nomor2");
        console.log(prd1);
        console.log(prd2);
        console.log(prd5);
        console.log(prd7);

        var unik="";

        $.ajax({

            dataType: 'json',
            url: All.get_url('dummyvera1/getProdSP'),
            data: {prdcd1: prd1, prdcd2: prd2, prdcd3: '-', prdcd4: '-', prdcd5: prd5, prdcd6: '-', prdcd7: prd7},
            type: 'POST',
            success: function (result) {

                console.log("nomor3");
                console.log(result);

                if (result.response == "false") {
                    alert(result.message);

                    /*alert("Promo Produk Tidak Ditemukan ..");*/

                } else {

                    /* var i;
                     for (i=1;i<=7;i=i+1){}*/

                    //produk 1
                    var prdcd1 = result.prdcd1;
                    var prdnm1 = result.prdnm1;
                    var bv1 = result.bv1;
                    var qty1 = 1;
                    var westPrice1 = result.price_w1;
                    var eastPrice1 = result.price_e1;
                    var eastCPrice1 = result.price_ce1;
                    var westCPrice1 = result.price_cw1;
                    var weight1 = result.weight1;
                    var desc=1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd1, prdnm: prdnm1, bv: bv1, qty: qty1, westPrice: westPrice1, eastPrice: eastPrice1,westCPrice: westCPrice1, eastCPrice: eastCPrice1, weight : weight1, desc : desc, css_class : "prmbundle"},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log("SKIP2");
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            /*alert(thrownError + ':' +xhr.status);*/
                            alert("Promo Produk Tidak Ditemukan ..");
                            All.set_enable_button();
                        }
                    });
                }
            }
        });

    }

</script>
<div class="column_center">
    <div class="container">
        <div class="search">
            <div class="stay_right">
                <form action="<?php echo base_url('shop/product/name'); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" novalidate>
                    <input type="text" name="name" value="" placeholder="Cari Produk...">
                    <input type="submit" value="GO" style="color:#fff;">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>

        <ul class="social">
            <li class="find">Find us here</li>
            <li><a href="https://www.facebook.com/pages/AP-K-Link-Indonesia/154582434691756" target="_blank"> <i class="fb"> </i> </a></li>
            <li><a href="https://twitter.com/official_klink" target="_blank"> <i class="tw"> </i> </a></li>
            <li><a href="https://plus.google.com/u/0/+KlinkCoIdisgone/posts" target="_blank"> <i class="gl"> </i></a></li>
            <li><a href="http://instagram.com/klink_indonesia_official?ref=badge" target="_blank"> <i class="ig"> </i> </a></li>
            <li><a href="http://www.youtube.com/channel/UC1YLBuEkJLzocmc_yp5Pgvw" target="_blank"> <i class="yt"> </i> </a></li>
            <div class="clearfix"> </div>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>

<div class="container yp-mobile">
    <div class="slider">
        <div class="col-md-8 yp-nopadding col-md-push-4 callbacks_container">
            <ul class="rslides" id="slider">
                <!--<li><img src="<?php //echo base_url() ?>assets/images/promo_ieduladha.jpg" class="img-responsive" alt=""/></li>
            <li><img src="<?php echo base_url() ?>assets/images/banner.jpg" class="img-responsive" alt=""/></li>
            <li><img src="<?php echo base_url() ?>assets/images/banner1.jpg" class="img-responsive" alt=""/></li>
            <li><img src="<?php echo base_url() ?>assets/images/banner4.jpg" class="img-responsive" alt=""/></li>-->
                <?php
                $n = 1;
                foreach($bannerCache as $bann){
                    $id = $bann->id;
                    $hdr_desc = $bann->hdr_desc;
                    $goup_hdr = $bann->goup_hdr;
                    $img_url = $bann->img_url;
                    $hdr_status = $bann->hdr_status;

                    echo "<li><img src=\"".base_url($img_url)."\" class=\"img-responsive\" alt=\"\"/></li>";
                }
                ?>
            </ul>
        </div>
        <div class="col-md-4  yp-mt-25 col-md-pull-8">

            <div class="yp-shortcut-container">
                <a href="https://www.k-net.co.id/sales/online?utm_source=internal&utm_medium=shortcut" id="59" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Kategori" src="https://img.icons8.com/color/96/000000/paid.png" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">Cek Transaksi</p></a>
                <a href="https://www.k-net.co.id/sales/direct/downline?utm_source=internal&utm_medium=shortcut" id="144" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="TV Kabel" src="https://img.icons8.com/color/96/000000/reseller.png" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">Info Jaringan</p></a>
                <a href="https://www.k-net.co.id/bonus?utm_source=internal&utm_medium=shortcut" id="203" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Telkom" src="https://img.icons8.com/color/96/000000/loyalty-card.png" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">Bonus </p></a>

                <a href="https://www.k-net.co.id/va/history/trx/fe?utm_source=internal&utm_medium=shortcut" id="138" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Emas" src="https://img.icons8.com/color/96/000000/wallet.png" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">K-Wallet</p></a>
                <a href="https://www.k-net.co.id/cart/list?utm_source=internal&utm_medium=shortcut" id="63" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Jual" src="https://img.icons8.com/color/96/000000/shopping-cart.png" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">Keranjang</p></a>
                <a href="https://www.k-net.co.id/xl/paketdata/fe?utm_source=internal&utm_medium=shortcut" id="6" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Official Store" src="https://is4-ssl.mzstatic.com/image/thumb/Purple128/v4/b4/7a/cc/b47acc3c-fa9a-3a62-7cfe-cfccdfe893d0/source/256x256bb.jpg" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">Pulsa XL</p></a>
                <a href="https://www.k-net.co.id/dtc?utm_source=internal&utm_medium=shortcut" id="94" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Buku" src="https://img.icons8.com/color/48/000000/starred-ticket.png" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">Tiket Acara</p></a>
                <a href="https://www.k-net.co.id/tracking?utm_source=internal&utm_medium=shortcut" id="142" class="yp-shortcut-grid"><div class="yp-shortcut-icon"><img alt="Air PDAM" src="https://image.flaticon.com/icons/svg/149/149067.svg" height="36px" class="yp-shortcut-img loading" data-was-processed="true"></div><p class="yp-shortcut-text">My Profile</p></a>

            </div>
        </div>
    </div>
</div>


<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: false,
            speed: 500,
            namespace: "callbacks",
            pager: true,
        });
    });


</script>

<!--Stripe -->

<!--divider -->
<div class="brands">
    <div class="m_3 voffset3"><span class="left_line"> </span><h3>Product Category</h3><span class="right_line"> </span></div>
</div>

<!--kategori -->



<!--kategori -->

<div class="owl-carousel">
    <?php
    foreach($prodCat as $data) {
        $prdcat = $data->cat_id;
        $jpg = $data->cat_id.'.jpg';
        $url = base_url('assets/images/kat/'.$jpg.'');
        if ($prdcat !=14):
            echo "
                    <div class=\"owlitem\">
                <a href=\"".site_url()."/shop/productHeader/cat/id/$prdcat\">
                    <img src=\"$url\" class=\"img-responsive\" alt=\"\"/>
                </a>                    
             </div>
                    ";
        endif;
    }
    ?>

    <?php

    ?>
</div>

<!--kategori -->


<!--product -->
<div class="clearence">
    <div class="content_bottom">
        <div class="container yp-prodcontainer" id="listProductDiv">

            <div class="m_3" style="margin-bottom:25px;">

                <span class="left_line"> </span><h3>Best Seller</h3>
                <span class="right_line"> </span></div>

            <!--Tambahan Vera-->
            <?php
            $usr = getUsername();
            $n="";
            $prdcd = "";
            $imgName = "";
            $prdcdnm = "";
            $prdcdcat = "";
            $prdcdcatnm = "";
            $westPrice = 0;
            $eastPrice = 0;
            $westCPrice =0;
            $eastCPrice = 0;
            $weight = "";
            $bv = 0;

            foreach($prodjhon as $row)
            {
                $prdcd = $row->prdcd;
                $imgName = $row->img_url;
                $prdcdnm = $row->prdnm;
                $prdcdcat = $row->prdcdcat;
                $prdcdcatnm = $row->prdnmcatnm;
                $westPrice = $row->price_w;
                $eastPrice = $row->price_e;
                $westCPrice = $row->price_cw;
                $eastCPrice = $row->price_ce;
                $weight = $row->weight;
                $bv = $row->bv;
                $isheader= $row->isheader;
                $isdetail= $row->isdetail;

                if($usr != NULL) {
                    $hargaA= $westPrice;
                    $hargaB= $eastPrice;
                } else {
                    $hargaA= $westCPrice;
                    $hargaB= $eastCPrice;
                }

                if($prdcd == "VRA01") {
                    $produk1 = "K-ION NANO :";
                    $produk2 = "K-E WRIST BAND :";
                    $hidden2 = "";
                    $hidden2_1= "";

                    $produk3 = "";
                    $hidden3 = "hidden";

                    $produk4 = "";
                    $produk5 = "";
                    $produk6 = "";
                    $produk7 = "";

                }elseif($prdcd == "VRA02"){
                    $produk1= "TICKET :";
                    $produk2= "POLO SHIRT :";
                    $hidden2= "hidden";
                    $hidden2_1= "hidden";

                    $produk3= "";
                    $hidden3= "hidden";

                    $produk4= "";
                    $produk5= "";
                    $produk6= "";
                    $produk7= "";

                }elseif($prdcd == "VRA03"){
                    $produk1= "LIPSTIK D'FLORA 1 :";
                    $produk2= "LIPSTIK D'FLORA 2 :";
                    $hidden2= "hidden";
                    $hidden2_1= "";

                    $produk3= "CC CREAM :";
                    $hidden3= "";

                    $produk4= "LOOSE POWDER :";
                    $produk5= "COMPACT POWDER :";
                    $produk6= "BRUSH POWDER :";
                    $produk7= "POUCH POWDER :";

                }else{
                    $produk1= "LIPSTIK D'FLORA :";
                    $produk2= "CC CREAM :";
                    $hidden2= "hidden";
                    $hidden2_1= "";

                    $produk3= "";
                    $hidden3= "";

                    $produk4= "";
                    $produk5= "LOOSE POWDER :";
                    $produk6= "";
                    $produk7= "POUCH POWDER :";

                }
                ?>
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 voffset4 thumb-wrap">
                    <div class="view view-first">
                        <h5 class="title-desc">
                            <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)"><?php echo strtolower($prdcdnm);?></a>
                            <span class="label bvbar label-success">BV <?php echo "".number_format($bv,0,".",".");?></span>
                        </h5>
                        <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)">

                            <img src="<?php echo $folderPrd.$imgName; ?>" class="lazy img-responsive" alt=""/>
                            <!--<img src="<?php echo $imgName; ?>" class="lazy img-responsive" alt=""/>-->
                        </a>

                        <div class="tab_desc">
                            <div class="col-xs-12 col-md-12 nopadding">
                                <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                    <h5 class="yp-pricetxt">Rp. <?php echo "".number_format($hargaA,0,".",".");?></h5>
                                    <h5 class="p3">Harga Wilayah A</h5>
                                </div>
                                <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                    <h5 class="yp-pricetxt">Rp. <?php echo "".number_format($hargaB,0,".",".");?></h5>
                                    <h5 class="p3">Harga Wilayah B</h5>
                                </div>
                                <!--<form method="post" action="<?php echo "".site_url('cart/addtocart')."";?>">-->
                                <div class="clearfix"></div>
                                <?php
                                $usr = getUsername();
                                $usr_id = getUserID();
                                //echo getUsername();

                                if($isheader == 1 AND $isdetail == 0){
                                    $view1= "button";
                                    $view2="";
                                    $view3="button";

                                    echo "<br>";
                                    //tutup sementara
                                    echo  "<div id=\"prod1\" style=\"font-size: 12px\" align=\"left\" $view2> &nbsp; $produk1";

                                    if($prdcd == "VRA01"){
                                        echo "<select id=\"knano_color\" name=\"knano_color\">
                                                <option value=\"#\">- Warna -</option>";
                                        foreach($prod1 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }

                                        echo  "</select></div>";

                                    }elseif($prdcd == "VRA02"){
                                        echo "<select id=\"ticket\" name=\"ticket\">";
                                        foreach($T1 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->prdnm;
                                            $val= $prdcd1.'|'.$name1;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                    }elseif($prdcd == "VRA03"){
                                        echo "<select id=\"prod1_color\" name=\"prod1_color\">
                                                <option value=\"#\">- Warna -</option>";
                                        foreach($p1 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;
                                            $val= $prdcd1.'|'.$name1;

                                            echo "<option value=\"$val\">$name1</option>";
                                        }

                                        echo  "</select></div>";
                                    }else{
                                        echo "<select id=\"product1_color\" name=\"product1_color\">
                                                <option value=\"#\">- Warna -</option>";
                                        foreach($p1 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;
                                            $val= $prdcd1.'|'.$name1;

                                            echo "<option value=\"$val\">$name1</option>";
                                        }

                                        echo  "</select></div>";
                                    }

                                    echo "<br>";
                                    echo  "<div id=\"prod2\" style=\"font-size: 12px\" class=\"title-desc\" align=\"left\" $view2>&nbsp; $produk2";

                                    if($prdcd == "VRA01"){
                                        echo "<select id=\"kwirst_color\" name=\"kwirst_color\" style=\"visibility: $hidden2_1\">
                                                <option value=\"#\">- Warna -</option>";
                                        foreach($prod3 as $row3) {
                                            $end_pref= $row2 -> end_prefix;
                                            $nwarna = $row3->warna;
                                            $code3= $row3 ->id_warna;
                                            $val2= $code3.$end_pref;

                                            echo "<option value=\"$val2\">$nwarna</option>";
                                        }

                                        echo "</select>";
                                    }elseif($prdcd == "VRA02"){
                                        echo "<select id=\"shirt_size\" name=\"shirt_size\">";
                                        foreach($T2 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->ukuran_desc;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select>";

                                    }elseif($prdcd == "VRA03"){
                                        echo "<select id=\"prod2_color\" name=\"prod2_color\" style=\"visibility: $hidden2_1\" onchange=\"cek_lipstik()\">
                                                <option value=\"#\">- Warna -</option>";
                                        foreach($p2 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            $val= $prdcd1.'|'.$name1;

                                            echo "<option value=\"$val\">$name1</option>";
                                        }

                                        echo  "</select>";

                                    }else{
                                        echo "<select id=\"product2_color\" name=\"product2_color\" style=\"visibility: $hidden2_1\">
                                                <option value=\"#\">- Warna -</option>";
                                        foreach($p5 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            $val= $prdcd1.'|'.$name1;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }

                                        echo  "</select>";
                                    }

                                    echo "<select id=\"kwirst_size\" name=\"kwirst_size\"  style=\"visibility: $hidden2\">
                                                <option value=\"#\">- Uk. -</option>";
                                    foreach($prod2 as $row2) {
                                        $prefix= $row2 -> prefix;
                                        $ukuran_desc = $row2->ukuran_desc;
                                        $code2 = $row2->ukuran;
                                        $val1= $prefix.$code2;
                                        //echo "<option value=\"HC050".$code2."-\">$code2</option>";
                                        echo "<option value=\"$val1\">$ukuran_desc</option>";
                                    }

                                    echo    "</select></div>";


                                    if($prdcd == "VRA03"){
                                        echo  "<div id=\"prod3\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk3
                                                <select id=\"prod3_color\" name=\"prod3_color\" style=\"visibility: $hidden3\" onchange=\"cek_lipstik()\">
                                                <option value=\"#\">- Warna -</option>";

                                        foreach($p5 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                        echo "<br>";
                                        echo  "<div id=\"prod4\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk4
                                                <select id=\"prod4_color\" name=\"prod4_color\" style=\"visibility: $hidden3\" onchange=\"cek_lipstik()\">
                                                <option value=\"#\">- Warna -</option>";

                                        foreach($p4 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                        echo "<br>";
                                        echo  "<div id=\"prod5\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk5
                                                <select id=\"prod5_color\" name=\"prod5_color\" style=\"visibility: $hidden3\" onchange=\"cek_lipstik()\">
                                                <option value=\"#\">- Warna -</option>";

                                        foreach($p3 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                        echo "<br>";
                                        echo  "<div id=\"prod6\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk6
                                                <select id=\"prod6_color\" name=\"prod6_color\" style=\"visibility: $hidden3\">
                                                <!--<option value=\"#\">- Uk. -</option>-->";

                                        foreach($p6 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                        echo "<br>";
                                        echo  "<div id=\"prod7\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk7
                                                <select id=\"prod7_color\" name=\"prod7_color\" style=\"visibility: $hidden3\">
                                                <!--<option value=\"#\">- Uk. -</option>-->";

                                        foreach($p7 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                    }elseif($prdcd == "VRA04"){

                                        echo  "<div id=\"prod5\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk5
                                                <select id=\"product5_color\" name=\"product5_color\" style=\"visibility: $hidden3\" onchange=\"cek_lipstik()\">
                                                <option value=\"#\">- Warna -</option>";

                                        foreach($p4 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";

                                        echo "<br>";
                                        echo  "<div id=\"prod7\" style=\"font-size: 12px\" align=\"left\" $view2>&nbsp; $produk7
                                                <select id=\"product7_color\" name=\"product7_color\" style=\"visibility: $hidden3\">
                                                <!--<option value=\"#\">- Uk. -</option>-->";

                                        foreach($p7 as $row1) {
                                            $prdcd1 = $row1->prdcd;
                                            $name1 = $row1->warna;

                                            echo "<option value=\"$prdcd1\">$name1</option>";
                                        }
                                        echo  "</select></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";

                                    }else{
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                        echo "<div class=\"clearfix\"></div>";
                                        echo "<br>";
                                    }

                                    // echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukan ke keranjang\"/>";
                                    //echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 btn-primary1\" onclick=\"submitData()\" value=\"Masukkan Keranjang\"/>";
                                    
                                    /*if($usr != NULL ) {
                                         echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 btn-primary1\" onclick=\"submitData()\" value=\"Masukkan Keranjang\"/>";
                                     } else {
                                         echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
                                     }*/

                                    //jika mau ditutup
                                    if($usr_id == 'IDSPAAA66834' ) {
                                        echo "<div class=\"clearfix\"></div>";
                                        if($prdcd == "VRA03"){
                                            echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 cartbtn btn-primary1\" onclick=\"validasi_lipstik(this.value)\" value=\"!Masukkan Keranjang\"/>";
                                        }elseif($prdcd == "VRA02"){
                                            echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 cartbtn btn-primary1\" onclick=\"submitData3(this.value)\" value=\"!Masukkan Keranjang\"/>";
                                        }else if($prdcd == "VRA04"){
                                            echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 cartbtn btn-primary1\" onclick=\"submitData4(this.value)\" value=\"!Masukkan Keranjang\"/>";
                                        }else{
                                            echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 cartbtn btn-primary1\" onclick=\"submitData1(this.value)\" value=\"!Masukkan Keranjang\"/>";
                                        }

                                    } else {
                                        //echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
                                        echo "";
                                    }

                                }
                                /*else{
                                    $view1="hidden";
                                    $view2="hidden";
                                    $view3="button";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "<br>";
                                    echo "<br>";
                                    if($usr != NULL) {
                                        echo "<input type=\"$view3\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukkan Keranjang\"/>";
                                    } else {
                                        echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
                                    }
                                }*/


                                ?>

                            </div>
                            <div class="clearfix"></div>
                            <!--<a href="cart.php" class="btn1 btn-primary1">
                                    <span>Add To Cart</span><i class="fa fa-plus"></i>
                            </a>-->
                        </div>
                    </div>
                </div>
                <?php
                $n++;
            }
            ?>
            <div class="clearfix"></div>

            <?php
            if(isset($prodCache) || $prodCache != null)
            {
                ?>
                <form id="formAddCart" class="clearfix">
                    <?php
                    $n = 1;
                    foreach($prodCache as $row)
                    {
                        $prdcd = $row->prdcd;
                        $imgName = $row->img_url;
                        $prdcdnm = $row->prdnm;
                        $prdcdcat = $row->prdcdcat;
                        $prdcdcatnm = $row->prdnmcatnm;
                        $westPrice = $row->price_w;
                        $eastPrice = $row->price_e;
                        $westCPrice = $row->price_cw;
                        $eastCPrice = $row->price_ce;
                        $weight = $row->weight;
                        $bv = $row->bv;
                        $ecomm_status = $row->ecomm_status;
                        $is_discontinue = $row->is_discontinue;
                        $max_order = $row->max_order;
                        ?>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 voffset4 thumb-wrap">
                            <?php
                            if($ecomm_status == '2' && $is_discontinue == '0')
                            {
                                echo "<div class=\"soldout\"> <p class=\"soldtext\"> Stok Tidak Tersedia </div>";
                            }
                            ?>
                            <div class="view view-first">
                                <h5 class="title-desc">
                                    <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)"><?php echo strtolower($prdcdnm);?></a>
                                    <span class="label bvbar label-success">BV <?php echo "".number_format($bv,0,".",".");?></span>
                                </h5>
                                <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)">

                                    <img data-original="<?php echo $folderPrd.$imgName; ?>" class="lazy img-responsive" alt=""/>
                                    <!--<img src="<?php echo $imgName; ?>" class="lazy img-responsive" alt=""/>-->
                                </a>

                                <div class="tab_desc">
                                    <div class="col-xs-12 col-md-12 nopadding">
                                        <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                            <h5 class="yp-pricetxt">Rp. <?php echo "".number_format($westPrice,0,".",".");?></h5>
                                            <h5 class="p3">Harga Wilayah A</h5>
                                        </div>
                                        <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                            <h5 class="yp-pricetxt">Rp. <?php echo "".number_format($eastPrice,0,".",".");?></h5>
                                            <h5 class="p3">Harga Wilayah B</h5>
                                        </div>
                                        <!--<form method="post" action="<?php echo "".site_url('cart/addtocart')."";?>">-->

                                        <input type="hidden" name="prdcd" id="<?php echo "prdcd".$n; ?>" value="<?php echo $prdcd;?>"/>
                                        <input type="hidden" name="prdnm" id="<?php echo "prdnm".$n; ?>" value="<?php echo $prdcdnm;?>"/>
                                        <input type="hidden" name="bv" id="<?php echo "bv".$n; ?>" value="<?php echo $bv;?>"/>
                                        <input type="hidden" name="westPrice" id="<?php echo "westPrice".$n; ?>" value="<?php echo $westPrice;?>"/>
                                        <input type="hidden" name="eastPrice" id="<?php echo "eastPrice".$n; ?>" value="<?php echo $eastPrice;?>"/>
                                        <input type="hidden" name="westCPrice" id="<?php echo "westCPrice".$n; ?>" value="<?php echo $westCPrice;?>"/>
                                        <input type="hidden" name="eastCPrice" id="<?php echo "eastCPrice".$n; ?>" value="<?php echo $eastCPrice;?>"/>
                                        <input type="hidden" name="weight" id="<?php echo "weight".$n; ?>" value="<?php echo $weight;?>"/>
                                        <input type="hidden" name="max_order" id="<?php echo "max_order".$n; ?>" value="<?php echo $max_order;?>"/>

                                        <?php
                                        $usr = getUsername();
                                        //echo getUsername();

                                        /*
                                         *
                                         if($usr != NULL) {
                                               echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukan ke keranjang\"/>";
                                         } else {
                                               echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
                                         }
                                         *
                                         */
                                        //edit hilal @2018-12-21 show soldout
                                        if($ecomm_status == '2' && $is_discontinue == '0') {
                                            echo "<input type=\"text\" class=\"greysold\"  value=\"\"/>";
                                        }elseif($ecomm_status == '1' && $is_discontinue == '0') {
                                            //cek kode prdcd ion nano & harga
                                            $a1= substr($prdcd,0,7);
                                            $nwestPrice1="474000";
                                            $neastCPrice1="494000";

                                            //cek kode prdcd kwirst band &harga
                                            $a2= substr($prdcd,0,5) == 'HC050';
                                            $nwestPrice2="232000";
                                            $neastCPrice2="246000";

                                            $non= "non";

                                            /*if($usr != NULL ) {
                                                if($a1 == 'HC075NA' && ($westPrice == $nwestPrice1 || $eastPrice == $neastCPrice1) ){
                                                    echo "<input type=\"button\"  class=\"btn1 btn-primary1\" onclick=\"submitData($n)\" value=\"Masukan ke keranjang\"/>";

                                                }elseif($a2 == 'HC050' && ($westPrice == $nwestPrice2 || $eastPrice == $neastCPrice2) ){
                                                    echo "<input type=\"button\"  class=\"btn1 btn-primary1\" onclick=\"submitData($n)\" value=\"Masukan ke keranjang\"/>";

                                                }else {
                                                    echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukan ke keranjang\"/>";
                                                }

                                            } else {
                                                echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
                                            }*/

                                            if($usr != NULL ) {
                                                echo "<input type=\"button\" class=\"btn1 cartbtn btn-primary1\" onclick=\"addToCart($n)\" value=\"Masukan ke keranjang\"/>";
                                            } else {
                                                echo "<input type=\"button\" class=\"btn1 cartbtn btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
                                            }
                                        }

                                        ?>

                                    </div>
                                    <div class="clearfix"></div>
                                    <!--<a href="cart.php" class="btn1 btn-primary1">
                                            <span>Add To Cart</span><i class="fa fa-plus"></i>
                                    </a>-->
                                </div>
                            </div>
                        </div>

                        <?php
                        $n++;
                    }
                    ?>
                </form>
                <?php
            } else {
                echo "Pencarian produk yang mengandung kata '$prdnm' tidak ditemukan..";
            }
            ?>
            <div class="col-md-12 voffset4 text-center">
                <!--<a href="<?php echo "".site_url('shop/product/cat')."";?>" class="btn1 btn2 btn-primary1"><span>View More Product</span><i class="fa fa-list"></i></a> -->
            </div>
        </div>
        <!--container -->
        <div class="container" id="detailProductDiv" style="display:none">

        </div>
    </div>
    <!--content_bottom -->
</div>
<!--clearence -->
<!--product -->

<!--promo -->
<div class="brands">
    <div class="m_3 voffset3"><span class="left_line"> </span><h3>Promotion</h3><span class="right_line"> </span></div>
</div>

<!--promo -->
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <a href="<?php echo "".site_url('shop/productHeader/cat/id/13').""; ?>"><img src="<?php echo base_url() ?>assets/images/promo.jpg" class="img-responsive margin0" alt=""/></a>
        </div>
        <div class="col-md-6">
            <a href="<?php echo "".site_url('tracking')."";?>"><img src="<?php echo base_url() ?>assets/images/tracking.jpg" class="img-responsive margin0" alt=""/></a>
        </div>
    </div>
</div>
<!--promo -->



<!--brand -->
<div class="brands">
    <div class="m_3 voffset3"><span class="left_line2"> </span><h3>Product Category</h3><span class="right_line2"> </span></div>
    <div class="container">
        <ul id="logoslider">
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-1.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-2.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-3.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-4.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-5.jpg" class="category" alt=""/> </a></li
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-6.jpg" class="category" alt=""/> </a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">

    $(window).load(function() {
        $("#logoslider").flexisel({
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
        });

    });
</script>
<!--brand -->
<script>$('.owl-carousel').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        margin: 10,
        stagePadding: 150,
        autoplay: true,

        responsive : {
            0 : {
                items: 1,
                stagePadding:80,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 2,
            },
            1200 : {
                items: 3,

            }
        }
    });
	
	function addToCart(param) {
		All.set_disable_button();
		var prdcd = $("#prdcd" + param).val();
		var prdnm = $("#prdnm" + param).val();
		var bv = $("#bv" + param).val();
		var qty = $("#qty" + param).val();
		var westPrice = $("#westPrice" + param).val();
		var eastPrice = $("#eastPrice" + param).val();
		var eastCPrice = $("#eastCPrice" + param).val();
		var westCPrice = $("#westCPrice" + param).val();

		var weight = $("#weight" + param).val();
		var max_order = $("#max_order" + param).val(); //vera 27.03.2019
		console.log(max_order);
		var desc = 0; //tambahan vera
		/* $.ajax({
			url: All.get_url("cart/add"),
			type: 'POST',
			dataType: 'json',
			data: {
				prdcd: prdcd,
				prdnm: prdnm,
				bv: bv,
				qty: qty,
				westPrice: westPrice,
				eastPrice: eastPrice,
				westCPrice: westCPrice,
				eastCPrice: eastCPrice,
				weight: weight,
				desc: desc,
				max_order: max_order
			},
			success: function (data) {
				All.set_enable_button();
				alert(data.message);
				if (data.response == "true") {
					var maxID = parseInt($("#maxID").val());
					var nextMaxID = maxID + 1;
					$("#maxID").val(nextMaxID);
					$("#showInfoSumCart").html(null);
					var row = "&nbsp;&nbsp;&nbsp;&nbsp;" + data.total_item + " items | Rp. " + All.num(data.total_west_price);
					$("#showInfoSumCart").append(row);
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		}); */
		NetFunnel_Action({
			service_id: 'service_1',
			action_id: 'a_cart'
		}, function(ev, ret) {
			console.log(ret);
			$.ajax({
				url: All.get_url("cart/add"),
				type: 'POST',
				dataType: 'json',
				data: {
					prdcd: prdcd,
					prdnm: prdnm,
					bv: bv,
					qty: qty,
					westPrice: westPrice,
					eastPrice: eastPrice,
					westCPrice: westCPrice,
					eastCPrice: eastCPrice,
					weight: weight,
					desc: desc,
					max_order: max_order
				},
				success: function (data) {
					All.set_enable_button();
					alert(data.message);
					if (data.response == "true") {
						var maxID = parseInt($("#maxID").val());
						var nextMaxID = maxID + 1;
						$("#maxID").val(nextMaxID);
						$("#showInfoSumCart").html(null);
						var row = "&nbsp;&nbsp;&nbsp;&nbsp;" + data.total_item + " items | Rp. " + All.num(data.total_west_price);
						$("#showInfoSumCart").append(row);
					}
					NetFunnel_Complete();

				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' + xhr.status);
					All.set_enable_button();
					NetFunnel_Complete();
				}
			});
		});
	}

</script>

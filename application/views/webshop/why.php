<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="<?php echo site_url('shop/product') ?>" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;Why Klink?&nbsp;&nbsp;</span>
  </div>
</ul>
test
<!-- Breadcrumb -->

<!-- Shopping rate-->
<div class="row vpullset4 voffset4">
	<div class="container">
    
    	<!--Left sidebar-->
        <div class="col-md-3 col-sm-12">            
            <div class="list_grid">
        <ul style="margin-top:0px;">
                    <li><a href="<?php echo site_url('static/about');?>">About K-Link</a></li>
                    <li><a href="<?php echo site_url('static/why');?>">Why K-Link</a></li>
                    <li><a href="<?php echo site_url('static/how-to-order');?>">How To Order</a></li>
                    <li><a href="<?php echo site_url('static/replacement');?>">Replacment Policy</a></li>
                    <li><a href="<?php echo site_url('static/delivery');?>">Delivery</a></li>
                </ul>               
       </div>            
        </div>
        <!--Left sidebar--> 
        
        <!--right-->
    	<div class="col-md-9 col-sm-12"> 
        
        	<div class="mens-toolbar">
                <span class="title_header">
                    <i class="fa fa-question"></i> Why K-Link
                </span>
            </div>
            
            <p class="desc" style="margin-bottom:20px;">
            	K-Link saat ini telah menjadi salah satu penjual langsung terbesar di Indonesia, 
                sehingga dengan menjadi distributor K-Link berarti anda menjual merek nasional 
                yang mapan dan terpercaya di seluruh negeri ini. Pengalaman kami dalam industri
                penjualan langsung memberikan jaminan bahwa kami adalah pilihan yang tepat
            </p>
            <p class="desc" style="margin-bottom:20px;">
                Setiap anggota memiliki upline yang akan memberikan pendampingan, pelatihan dan 
                mendukung setiap langkah anda menuju sukses dengan kami. Modul pelatihan standar
                kami dijelaskan secara sederhana, mudah dipelajari dan dimengerti, serta mudah
                dilakukan dan diajarkan            
            </p>
            <p class="desc" style="margin-bottom:20px;">
                Rencana pemasaran (Marketing Plan) kami sangat bermanfaat dalam menguraikan potensi
                yang ditawarkan kepada seluruh Distributor K-link. Tidak hanya distributor mendapatkan
                uang dari penjualan produk, mereka juga menerima pembayaran bonus dari jumlah point 
                BV ( Business Value/ Nilai Bisnis) yang diperoleh melalui penjualan itu. Selain PBV 
                yang merupakan penjualan pribadi dari distributor itu sendiri, ada juga PGBV (Personal 
                Group Business Value/ Nilai Bisnis Grup Personal), yang merupakan pembayaran bonus 
                berdasarkan persentasi penjualan jaringan distributor tersebut. Maka, semakin besar 
                jaringan distributornya, semakin besar pula pembayaran bonusnya, hal ini membuka 
                kesempatan mendapatkan penghasilan yang benar-benar tak terbatas
            </p>           
            <br>              
           	
                     
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- Shopping rate-->
</body>
</html>		

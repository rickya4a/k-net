
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Sales&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Cek Transaksi&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--form -->
<div class="row vpullset4" style="min-height: 415px;">
<span id="formCekTransaksi">
	<div class="container">
    <div id="salesMember">
      <div class="col-md-12"> 
        <div class="col-md-6 col-md-offset-3 text-center">  
	        <form id="cektransaksi" method="POST" onkeypress="return event.keyCode != 13;">
	            <div class="track-order-grid">
                <div>
                  <span>Pencarian Transaksi Member</span>
                  <span>Bonus Month</span>	                
	                <select  class="required form-list" style="margin-bottom:10px; width:100%" id="bulan" name="bulan">
  	                <?php
                      $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                      for($y=1;$y<=12;$y++)
                      {
                          if($y==date("mm"))
                          { 
                              $pilih="selected";
                          }
                          else
                          {
                              $pilih="";
                          }
                          
                          echo("<option value=\"".sprintf("%02s",$y)."\" $pilih>$bulan[$y]</option>"."\n");
                      }
                    ?>
	                </select>
                    <?php
                        $thn = date("Y");
                    ?>
                  <input type="text" name="tahun" size="14" placeholder="Tahun Bonus" id="tahun" class="only-number" autocomplete="off"/>
                  
	              </div>
                <div>
                    <button class="btn1 btn2 btn-primary1" type="button"  onClick="Sales.searchTransaksi();" id="submit">Cek Transaksi <i class="fa fa-arrow-right"></i></button>
                  <!--<input class="btn1 btn2 btn-primary1" type="button" value="Cek Transaksi" onClick="Sales.searchTransaksi();" name="submit" id="submit"/>-->
                </div>
              </div>	           
	        </form>          
        </div>        
      </div>
    </div>
  </div>
  <div id="hasilPencarian"></div>
  </span>
  <div id="detail_trans"></div>
</div><!-- form -->    
<!--Checkout Wrapper-->

<script>

$("#tahun").datepicker({
	format: "yyyy",
	viewMode: "years",
	minViewMode: "years"
})

</script>
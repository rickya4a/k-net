<?php
    require APPPATH."libraries/Code128.php";	
    //$pdf=new FPDF('P','mm', 'A4');
	$pdf = new Code128('P', 'mm', 'A4');
    $pdf->Open();
    $pdf->SetFillColor(255,255,255); // background = biru muda
    $pdf->SetTextColor(0,0,0);	 //	font color = black
    $pdf->SetDrawColor(0,0,0); // border 	   = brown	
    $pdf->SetLineWidth(.1); // border thickness = 0.3 (maybe in e.m)
    
    $pdf->AddPage();
    $pdf->Image("assets/images/logo.jpg",6,5, 85, 25);
    //$pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln(); $pdf->Ln();
    //$pdf->Cell(50,10,$img,'L',0,0,'R');
    $pdf->SetFont('Courier','', 8);
    $thnCheck = '2017';
    
    function NumberToMonth($bln)
    {
        switch($bln)
        {
            case 1 :
                $jum = "january";
                break;
            case 2 :
                $jum = "february";
                break;
            case 3 :
                $jum = "march";
                break;
            case 4 :
                $jum = "april";
                break;
            case 5 :
                $jum = "may";
                break;
            case 6 :
                $jum = "june";
                break;
            case 7 :
                $jum = "july";
                break;
            case 8 :
                $jum = "august";
                break;
            case 9 :
                $jum = "september";
                break;
            case 10 :
                $jum = "october";
                break;
            case 11 :
                $jum = "november";
                break;
            case 12 :
                $jum = "december";
                break;
        }	   
        return $jum;
    }
    
    $lebarCell = 4; 
    $pdf->Ln();
    $pdf->SetXY(6,50);
    $titleCol1 = 50;
    $titleCol2 = 100;
    $titleCol3 = 35;
    $titleCol4 = 35;
    
	
	$pdf->SetFillColor(204, 228, 180); 
	$pdf->SetFont('Courier','B', 8);
	

	//echo "nilai nya adalah == $tableBaru";
	//print_r($tableBaru);
	if($tableBaru != null)

        foreach($tableBaru as $dxd)
        {
            //if($dxd->bonustype != 'kosong'){
            foreach($res2 as $dta2)
            {
                $bulan = ucwords(NumberToMonth($dta2->bonusmonth));
//        $pdf->Cell(198,5,"BONUS STATEMENT REPORT $bulan $dta2->bonusyear",1,0,'C', true);
                $pdf->Cell(198,5,"Perhitungan Bonus Distributor $bulan $dta2->bonusyear",1,0,'C', true);

            }
            $pdf->SetLeftMargin(6);
            $pdf->Ln();


            $pdf->SetFillColor(255,255,255); // white
            $pdf->SetFont('Courier','', 8);


            $pdf->Cell($titleCol1,$lebarCell,"Bulan Bonus :",0,0,'R',true); 	// its similar with TD in HT
            $bulan = strtoupper(NumberToMonth($dxd->bonusmonth));
            $pdf->Cell($titleCol2,$lebarCell,$bulan."/".$dxd->bonusyear,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"No. ID/ Stockist :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol2,$lebarCell,$dxd->dfno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Nama :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol2,$lebarCell,$dxd->fullnm,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Jumlah Bonus :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,"Rp. ".number_format($dxd->totBonus,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"PPh Pasal 21/23 :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->tax,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Biaya Administrasi :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->adm_bank,2,".",","),0,0,'R',true);
            //      $pdf->Cell($titleCol4,$lebarCell,"Rp. ".number_format($dxd->totBonus,2,".",","),0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Biaya Administrasi - Giro :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->adm_giro,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"K-News :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->adm_knews,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"A/R - K-Link :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->ar_klink,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"A/R - K-System :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->ar_ksystem,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Lain - lain :",0,0,'R',true); 	// its similar with TD in HT
            $dll=$dxd->ar_shamil+$dxd->donation+$dxd->donation_sum+$dxd->ar_sms+$dxd->ar_others;
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dll,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Leaders Club :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"Rp. ".number_format($dxd->lc,2,".",","),0,0,'R',true);
            $pdf->Ln();
            $pdf->SetTextColor(255,0,0);
            $pdf->Cell($titleCol1,$lebarCell,"Jumlah Potongan :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"",0,0,'R',true);
            $pdf->Cell($titleCol4,$lebarCell,"-Rp. ".number_format($dxd->ar_total,2,".",","),0,0,'R',true);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Starter Kit :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,"Rp. ".number_format(0,2,".",","),'B',0,'R',true);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"Netto :",0,0,'R',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol1,$lebarCell,"",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell,"Rp. ".number_format($dxd->totBonus_Nett,2,".",","),0,0,'R',true);

            $pdf->Ln();

            $pdf->AddPage('');
            //}
        }

        $pdf->SetFillColor(204, 228, 180);
$pdf->SetFont('Courier','B', 8);

$pdf->Cell(198,5,"Print Bonus Statement",1,0,'C', true);

$titleCol1 = 28;
$titleCol2 = 65;
$titleCol3 = 35;
$titleCol4 = 70;
$pdf->Ln();

$pdf->SetFillColor(255,255,255); // white
$pdf->SetFont('Courier','', 8);

foreach($res1 as $dd)
{
    $pdf->Cell($titleCol1,$lebarCell,"Dist. Code :",0,0,'R',true); 	// its similar with TD in HT
    $pdf->Cell($titleCol2,$lebarCell,$dd->dfno,0,0,'L',true);
    $pdf->Cell($titleCol3,$lebarCell,"Sponsor Code",0,0,'L',true);
    $pdf->Cell($titleCol4,$lebarCell,": ".$dd->sponsorcode,0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell($titleCol1,$lebarCell,"Dist. Name :",0,0,'R',true); 	// its similar with TD in HT
    $pdf->Cell($titleCol2,$lebarCell,$dd->fullnm,0,0,'L',true);
    $pdf->Cell($titleCol3,$lebarCell,"Sponsor Name",0,0,'L',true);
    $pdf->Cell($titleCol4,$lebarCell,": ".$dd->sp_nm,0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell($titleCol1,$lebarCell,"Address :",0,0,'R',true); 	// its similar with TD in HT
    $pdf->Cell($titleCol2,$lebarCell,$dd->addr1,0,0,'L',true);
    $pdf->Cell($titleCol3,$lebarCell,"Previous Rank",0,0,'L',true);
    $pdf->Cell($titleCol4,$lebarCell,": ".$dd->perc_prev."% (".$dd->prev.")",0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell($titleCol1,$lebarCell,"",0,0,'R',true); 	// its similar with TD in HT
    $pdf->Cell($titleCol2,$lebarCell,$dd->addr2,0,0,'L',true);
    $pdf->Cell($titleCol3,$lebarCell,"Adjusted Rank",0,0,'L',true);
    $pdf->Cell($titleCol4,$lebarCell,": ".$dd->perc_adj."% (".$dd->adjust.")",0,0,'L',true);
    $pdf->Ln();
    $pdf->Cell($titleCol1,$lebarCell,"",0,0,'R',true); 	// its similar with TD in HT
    $pdf->Cell($titleCol2,$lebarCell,$dd->addr3,0,0,'L',true);
    $pdf->Cell($titleCol3,$lebarCell,"Effective Rank",0,0,'L',true);
    $pdf->Cell($titleCol4,$lebarCell,": ".$dd->perc_eff."% (".$dd->effective.")",0,0,'L',true);
    $pdf->Ln();
    $pdf->Ln();
}

$pdf->SetFont('Courier','BU', 10); //set font to Bold

foreach($res2 as $dta2)
{
    $bulan = ucwords(NumberToMonth($dta2->bonusmonth));
    $pdf->Cell(198,5,"Bonus Statement For $bulan $dta2->bonusyear",0,0,'C', true);
}

$pdf->SetFont('Courier','BU', 8); //set font to Regular

$pdf->Ln();
$pdf->Ln();

//$pdf->SetFont('Times','', 12);
$pdf->Cell(83,$lebarCell,"Bonus Summary",0,0,'C',true); 	// its similar with TD in HT
$pdf->Cell(30,$lebarCell,"Amt Payable",0,0,'C',true);
$pdf->Cell(50,$lebarCell,"LB Pts",0,0,'C',true);
$pdf->Cell(20,$lebarCell,"Pt Value",0,0,'C',true);
$pdf->Cell(15,$lebarCell,"Share",0,0,'C',true);
$pdf->Ln();

$pdf->SetFont('Courier','', 8); //set font to Regular
foreach($res2 as $dta2)
{
    //$pdf->SetFont('Times','', 9);
    $planbincome = number_format($dta2->planbincome,2,".",",");
    $gpincome = number_format($dta2->gpincome,2,".",",");
    $ldbincome = number_format($dta2->ldbincome,2,".",",");
    $ldbtotalpoint = number_format($dta2->ldbtotalpoint,2,".",",");
    $sredincome = number_format($dta2->sredincome,2,".",",");
    $crownincome = number_format($dta2->crownincome,2,".",",");
    $x = $dta2->planbincome + $dta2->addinfinityincome;
    $unilevel = number_format($x,2,".",",");
    $caincome = number_format($dta2->caincome,2,".",",");
    $scaincome = number_format($dta2->scaincome,2,".",",");
    $rcaincome = number_format($dta2->rcaincome,2,".",",");
    $capointvalue = number_format($dta2->capointvalue,4,".",",");
    $scapointvalue = number_format($dta2->scapointvalue,4,".",",");
    $rcapointvalue = number_format($dta2->rcapointvalue,4,".",",");
    $chpointvalue = number_format($dta2->chpointvalue,4,".",",");
    $pspointvalue = number_format($dta2->pspointvalue,4,".",",");
    $chincome = number_format($dta2->chincome,2,".",",");
    $psharingincome = number_format($dta2->psharingincome,2,".",",");
    $psharingpoints = number_format($dta2->psharingpoints,2,".",",");
    $gpsharing1income = number_format($dta2->gpsharing1income,2,".",",");
    $gpsharing1points = number_format($dta2->gpsharing1points,2,".",",");
    $gpsharing2income = number_format($dta2->gpsharing2income,2,".",",");
    $gpsharing2points = number_format($dta2->gpsharing2points,2,".",",");
    $gpsharing3income = number_format($dta2->gpsharing3income,2,".",",");
    $gpsharing3points = number_format($dta2->gpsharing3points,2,".",",");
    $infinityincome = number_format($dta2->infinityincome,2,".",",");
    $gps2pointvalue = number_format($dta2->gps2pointvalue,4,".",",");
    $gps1pointvalue = number_format($dta2->gps1pointvalue,4,".",",");
    $gps3pointvalue = number_format($dta2->gps3pointvalue,4,".",",");
    $gps3pointvalue = number_format($dta2->gps3pointvalue,4,".",",");
    $netincomeAfterAdjust = $dta2->netincome + $dta2->totaladjustedamt;
    $totaladjustedamt = number_format($dta2->totaladjustedamt,2,".",",");
    $netincomeAfterAdjust2 = number_format($netincomeAfterAdjust,2,".",",");
    $netincome = number_format($dta2->netincome,2,".",",");

    $x = $netincomeAfterAdjust * $currency;
    //echo "dfdf ".$currency;
    $totaladjustedamtIND = number_format($x,2,".",",");
    //END UPDATE
    $lastmonthyend = number_format($dta2->lastmonthyend ,2,".",",");
    $currentyend = number_format($dta2->currentyend ,2,".",",");
    $accyend = number_format($dta2->accyend ,2,".",",");
    $lastmonthoversea = number_format($dta2->lastmonthoversea ,2,".",",");
    $currentoversea = number_format($dta2->currentoversea ,2,".",",");
    $accoversea = number_format($dta2->accoversea ,2,".",",");
    $CAShare = number_format($dta2->CAShare ,2,".",",");
    $SCAShare = number_format($dta2->scashare ,2,".",",");
    $CHShare = number_format($dta2->CHShare ,2,".",",");
    $scashare = number_format($dta2->scashare ,2,".",",");
    $crownshare = number_format($dta2->crownshare ,2,".",",");
    $rcashare = number_format($dta2->rcashare ,2,".",",");
    $col1 = 70;
    $col2 = 13;
    $col3 = 30;
    $col4 = 50;
    $col5 = 20;
    $col6 = 15;

    if($year < $thnCheck)
    {
        $unilevelpercen = "28%=";
    }else{
        $unilevelpercen = "18%=";
    }

    $pdf->Cell($col1,$lebarCell,"Unilevel",0,0,'L',true);	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,$unilevelpercen,0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,$unilevel,0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    $pdf->Cell($col1,$lebarCell,"Development Bonus",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"28%=",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,$gpincome,0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    if($dta2->ldbincome > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Leadership Bonus",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"30%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,$ldbincome,0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$ldbtotalpoint",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Leadership Bonus",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"30%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,$ldbincome,0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    $pdf->Cell($col1,$lebarCell,"S.R.E.D Fund",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"3%=",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"$sredincome",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    $pdf->Cell($col1,$lebarCell,"Crown Fund",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"1%=",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"$crownincome",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    if($dta2->caincome > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Crown Ambassador Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"2%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$caincome",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$ldbtotalpoint",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$capointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"$CAShare",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Crown Ambassador Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"2%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($dta2->scaincome > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Senior Crown Ambassador Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"1%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$scaincome",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$scapointvalue",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$scapointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"$SCAShare",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {

        $pdf->Cell($col1,$lebarCell,"Senior Crown Ambassador Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"1%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($dta2->rcaincome > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Royal Crown Ambassador Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"1%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$rcaincome",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$rcapointvalue",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$rcapointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"$rcashare",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {

        $pdf->Cell($col1,$lebarCell,"Royal Crown Ambassador Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"1%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($dta2->chincome > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Car/House Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"3%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$chincome",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$chpointvalue",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$chpointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"$CHShare",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Car/House Fund",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"3%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($dta2->psharingincome > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Profit Sharing",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"9%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$psharingincome",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$pspointvalue",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Profit Sharing",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"9%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($year < $thnCheck)
    {
        $labelBNS = "Infinity Bonus";
        $percentageBNS = "20%=";
        $nilaiBNS = "".$infinityincome."";
    }
    else
    {
        $labelBNS = "Initiative Bonus";
        $percentageBNS = "30%=";
        if($initiateSUm!=null){
            $x=$initiateSUm[0]->initiateBns;
        }else
            $x=0;
        $nilaiBNS = number_format($x,2,".",",");
    }
    $pdf->Cell($col1,$lebarCell,$labelBNS,0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,$percentageBNS,0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,$nilaiBNS,0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    if($dta2->gpsharing1income > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Global Profit Sharing < 100,000 GPV",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"5%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$gpsharing1income",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$gpsharing1points",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$gps1pointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Global Profit Sharing < 100,000 GPV",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"5%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$gpsharing1income",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$gpsharing1points",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($dta2->gpsharing2income > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Global Profit Sharing < 400,000 GPV",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"5%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$gpsharing2income",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$gpsharing2points",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$gps2pointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Global Profit Sharing < 400,000 GPV",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"5%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$gpsharing2income",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$gpsharing2points",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    if($dta2->gpsharing3income > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Global Profit Sharing >= 400,000 GPV",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"5%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$gpsharing3income",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$gpsharing3points",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"$gps3pointvalue",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Global Profit Sharing >= 400,000 GPV",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"5%=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$gpsharing3income",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$gpsharing3points",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    $pdf->Cell($col1,$lebarCell,"Year-End Bonus",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"3%=",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
    //$pdf->Cell($col2,$lebarCell,"_____________________",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    $pdf->Cell($col1,$lebarCell,"",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col2,$lebarCell,"_____________________",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    $pdf->Cell($col1,$lebarCell,"Gross Total",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"=",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"$netincome",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();
    /*
    $pdf->Cell($col1,$lebarCell,"",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col2,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();
    */
    $pdf->SetTextColor(255,0,0);

    if($dta2->totaladjustedamt > 0)
    {
        $pdf->Cell($col1,$lebarCell,"Adjustment",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$totaladjustedamt",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"$dta2->remark",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }
    else
    {
        $pdf->Cell($col1,$lebarCell,"Adjustment",0,0,'L',true); 	// its similar with TD in HT
        $pdf->Cell($col2,$lebarCell,"=",0,0,'R',true);
        $pdf->Cell($col3,$lebarCell,"$totaladjustedamt",0,0,'R',true);
        $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
        $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
        $pdf->Ln();
    }

    $pdf->SetTextColor(0,0,0);

    $pdf->Cell($col1,$lebarCell,"",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col2,$lebarCell,"___________________ -",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();

    $pdf->SetFont('Courier','B', 8); //set font to Bold

    $pdf->SetFillColor(205, 214, 173); // Grey
    //$pdf->SetTextColor(220,50,50);

    $pdf->Cell($col1,$lebarCell,"Net Total (RM)",0,0,'L',true); 	// its similar with TD in HT
    $pdf->Cell($col2,$lebarCell,"=",0,0,'R',true);
    $pdf->Cell($col3,$lebarCell,"$netincomeAfterAdjust2",0,0,'R',true);
    $pdf->Cell($col4,$lebarCell,"(IND) $totaladjustedamtIND",0,0,'R',true);
    $pdf->Cell($col5,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($col6,$lebarCell,"",0,0,'R',true);
    $pdf->Ln();
    $pdf->Ln();
    $titelCol = 99;
    $headCol = 33;

    $pdf->SetFont('Courier','B', 7.5); //set font to Regular
    $pdf->SetFillColor(255,255,255); // white

    $titelCol = $titelCol - 11;
    $headCol = $headCol - 9;
    //$pdf->SetFont('Courier','B', 8); //set font to Bold

    //$pdf->Line(10,80,50,80);

    $pdf->Cell($titelCol,$lebarCell,"Year-End Bonus",0,0,'C',true);
    $pdf->Cell($titelCol,$lebarCell,"OverSea Bonus",0,0,'C',true);
    $pdf->Ln();

    /*
    $pdf->Cell($titelCol,$lebarCell,"________________________________________________",0,0,'C',true);
    $pdf->Cell($titelCol,$lebarCell,"________________________________________________",0,0,'C',true);
    $pdf->Ln();
    */

    $pdf->SetFont('Courier','', 7.5); //set font to Regular

    $pdf->Cell($headCol,$lebarCell,"B/F",1,0,'C',true);
    $pdf->Cell($headCol,$lebarCell,"Current",1,0,'C',true);
    $pdf->Cell($headCol,$lebarCell,"C/F",1,0,'C',true);
    $pdf->Cell($headCol-10,$lebarCell,"",0,0,'C',true);
    $pdf->Cell($headCol,$lebarCell,"B/F",1,0,'C',true);
    $pdf->Cell($headCol,$lebarCell,"Current",1,0,'C',true);
    $pdf->Cell($headCol,$lebarCell,"C/F",1,0,'C',true);
    $pdf->Ln();

    $pdf->Cell($headCol,$lebarCell,"$lastmonthyend",1,0,'R',true);
    $pdf->Cell($headCol,$lebarCell,"$currentyend",1,0,'R',true);
    $pdf->Cell($headCol,$lebarCell,"$accyend",1,0,'R',true);
    $pdf->Cell($headCol-10,$lebarCell,"",0,0,'R',true);
    $pdf->Cell($headCol,$lebarCell,"$lastmonthoversea",1,0,'R',true);
    $pdf->Cell($headCol,$lebarCell,"$currentoversea",1,0,'R',true);
    $pdf->Cell($headCol,$lebarCell,"$accoversea",1,0,'R',true);
    $pdf->Ln();
    $pdf->Ln();

    $headCol = $headCol + 10;
}

if($hasil1)
{
    $pdf->SetFont('Courier','', 7);
    $titelCol = 99;
    $headCol1 = 20;
    $headCol2 = 44;
    $headCol3 = 35;

    $pdf->Cell($titelCol,$lebarCell,"Montly Personal BV",0,0,'C',true);
    $pdf->Ln();

    $pdf->Cell($headCol1,$lebarCell,"Date",0,0,'C',true);
    $pdf->Cell($headCol2,$lebarCell,"POR No",0,0,'C',true);
    $pdf->Cell($headCol3,$lebarCell,"BV",0,0,'C',true);
    $pdf->Ln();
    $tot1=0;
    foreach ($hasil1 as $data1)
    {
        $nbv = number_format($data1->nbv);

        $pdf->Cell($headCol1,$lebarCell,"$data1->tglc",0,0,'C',true);
        $pdf->Cell($headCol2,$lebarCell,"$data1->trcd",0,0,'C',true);
        $pdf->Cell($headCol3,$lebarCell,"$nbv",0,0,'R',true);
        $pdf->Ln();
        $tot1 += $data1->nbv;
    }
    $ntot = number_format($tot1);
    $pdf->Cell($headCol1,$lebarCell,"Total",0,0,'C',true);
    $pdf->Cell($headCol2,$lebarCell,"",0,0,'C',true);
    $pdf->Cell($headCol3,$lebarCell,"$ntot",0,0,'R',true);

}

$pdf->AddPage('L');

$colDownlineCode = 40;
$colDownlineName = 99;
$colprank = 10;
$colarank = 10;
$colerank = 10;
$colACCPGPV = 15;
$colPPV = 10;
$colPGPV = 10;
$colGPV = 16;
$colPBV = 10;
$colPGBV = 10;
$colGBV = 16;
$colPBVB = 10;
$colGBVB = 16;

$titleCol1 = 282;

//$pdf->SetFont('Times','', 9);
$pdf->Cell($titleCol1,$lebarCell,"DIRECT DOWNLINE SALES INFORMATION",1,0,'C',true);
$pdf->Ln();
$pdf->Cell($colDownlineCode,$lebarCell,"Downline Code",1,0,'C',true);
$pdf->Cell($colDownlineName,$lebarCell,"Downline Name",1,0,'C',true);
$pdf->Cell($colprank,$lebarCell,"P.Rank",1,0,'C',true);
$pdf->Cell($colarank,$lebarCell,"A.Rank",1,0,'C',true);
$pdf->Cell($colerank,$lebarCell,"E.Rank",1,0,'C',true);
$pdf->Cell($colACCPGPV,$lebarCell,"ACCPGPV",1,0,'C',true);
$pdf->Cell($colPPV,$lebarCell,"PPV",1,0,'C',true);
$pdf->Cell($colPGPV,$lebarCell,"PGPV",1,0,'C',true);
$pdf->Cell($colGPV,$lebarCell,"GPV",1,0,'C',true);
$pdf->Cell($colPBV,$lebarCell,"PBV",1,0,'C',true);
$pdf->Cell($colPGBV,$lebarCell,"PGBV",1,0,'C',true);
$pdf->Cell($colGBV,$lebarCell,"GBV",1,0,'C',true);
$pdf->Cell($colPBVB,$lebarCell,"PBVB",1,0,'C',true);
$pdf->Cell($colGBVB,$lebarCell,"GBVB",1,0,'C',true);
$pdf->Ln();
if($hasil2 != null) {
    foreach ($hasil2 as $data2)
    {
        $ppv  = number_format($data2->ppv);
        $pgpv = number_format($data2->pgpv,0,".",",");
        $gpv = number_format($data2->gpv,0,".",",");
        $pbv = number_format($data2->pbv);
        $pgbv = number_format($data2->pgbv,0,".",",");
        $gbv = number_format($data2->gbv,0,".",",");
        $pbvb = number_format($data2->pbvb,0,".",",");
        $gbvb = number_format($data2->gbvb,0,".",",");
        $AccPGPV = number_format($data2->AccPGPV,0,".",",");

        $pdf->Cell($colDownlineCode,$lebarCell,"$data2->distributorcode",1,0,'L',true);
        $pdf->Cell($colDownlineName,$lebarCell,"$data2->fullnm",1,0,'L',true);
        $pdf->Cell($colprank,$lebarCell,"$data2->currentrank",1,0,'R',true);
        $pdf->Cell($colarank,$lebarCell,"$data2->adjustedrank",1,0,'R',true);
        $pdf->Cell($colerank,$lebarCell,"$data2->effectiverank",1,0,'R',true);
        $adjust1 = (int)$data2->adjustedrank;
        //echo $adjust1;
        if ($adjust1 < 6)
        {

            $pdf->Cell($colACCPGPV,$lebarCell,"$AccPGPV",1,0,'R',true);
        }
        else
        {
            $pdf->Cell($colACCPGPV,$lebarCell,"0",1,0,'R',true);
        }
        $pdf->Cell($colPPV,$lebarCell,"$ppv",1,0,'R',true);
        $pdf->Cell($colPGPV,$lebarCell,"$pgpv",1,0,'R',true);
        $pdf->Cell($colGPV,$lebarCell,"$gpv",1,0,'R',true);
        $pdf->Cell($colPBV,$lebarCell,"$pbv",1,0,'R',true);
        $pdf->Cell($colPGBV,$lebarCell,"$pgbv",1,0,'R',true);
        $pdf->Cell($colGBV,$lebarCell,"$gbv",1,0,'R',true);
        $pdf->Cell($colPBVB,$lebarCell,"$pbvb",1,0,'R',true);
        $pdf->Cell($colGBVB,$lebarCell,"$gbvb",1,0,'R',true);

        $pdf->Ln();
    }
}
foreach($hasil3 as $data3)
{
    $spgpv = number_format("$data3->pgpv",0,".",",");
    $sgpv = number_format("$data3->gpv",0,".",",");
    $spbv = number_format($data3->pbv);
    $spgpv = number_format("$data3->pgbv",0,".",",");
    $sgbv = number_format("$data3->gbv",0,".",",");
    $spbvb =number_format("$data3->pbvb",0,".",",");
    $sgbvb = number_format("$data3->gbvb",0,".",",");
    $AccPGPV = number_format("$data3->AccPGPV",0,".",",");
    $ppv = number_format("$data3->ppv",0,".",",");

    $pdf->Cell($colDownlineCode,$lebarCell,"$data3->distributorcode",1,0,'L',true);
    $pdf->Cell($colDownlineName,$lebarCell,"$data3->fullnm",1,0,'L',true);
    $pdf->Cell($colprank,$lebarCell,"$data3->currentrank",1,0,'R',true);
    $pdf->Cell($colarank,$lebarCell,"$data3->adjustedrank",1,0,'R',true);
    $pdf->Cell($colerank,$lebarCell,"$data3->effectiverank",1,0,'R',true);
    $adjust1 = (int)$data3->adjustedrank;
    //echo $adjust1;
    if ($adjust1 < 6)
    {

        $pdf->Cell($colACCPGPV,$lebarCell,"$AccPGPV",1,0,'R',true);
    }
    else
    {
        $pdf->Cell($colACCPGPV,$lebarCell,"0",1,0,'R',true);
    }
    $pdf->Cell($colPPV,$lebarCell,"$ppv",1,0,'R',true);
    $pdf->Cell($colPGPV,$lebarCell,"$spgpv",1,0,'R',true);
    $pdf->Cell($colGPV,$lebarCell,"$sgpv",1,0,'R',true);
    $pdf->Cell($colPBV,$lebarCell,"$spbv",1,0,'R',true);
    $pdf->Cell($colPGBV,$lebarCell,"$spgpv",1,0,'R',true);
    $pdf->Cell($colGBV,$lebarCell,"$sgbv",1,0,'R',true);
    $pdf->Cell($colPBVB,$lebarCell,"$spbvb",1,0,'R',true);
    $pdf->Cell($colGBVB,$lebarCell,"$sgbvb",1,0,'R',true);
}
$pdf->Ln();
$pdf->Ln();

if(isset($devresult))
{
    $titleCol1 = 100;
    $headCol1 = 15;
    $headCol2 = 25;
    $headCol3 = 30;
    $headCol4 = 30;
    $totalCol = $headCol1 + $headCol2 + $headCol3;
    $allCol = $totalCol + $headCol4;

    //$pdf->SetFont('Times','', 9);
    $pdf->Cell($allCol,$lebarCell,"DEVELOPMENT BONUS",1,0,'C',true);
    $pdf->Ln();
    $pdf->Cell($headCol1,$lebarCell,"Override %",1,0,'C',true);
    $pdf->Cell($headCol2,$lebarCell,"Override BV",1,0,'C',true);
    $pdf->Cell($headCol3,$lebarCell,"Effective Override",1,0,'C',true);
    $pdf->Cell($headCol4,$lebarCell,"Override Amount",1,0,'C',true);
    $pdf->Ln();
    $totalOverrideAmount = 0;
    foreach($devresult as $dev)
    {
        if($dev->overridep == "S")	{
            $ss = "Self";
        }
        else {
            $overridep = number_format("$dev->overridep",2,".",",");
            $ss = $overridep." "."%";
        }

        if($dev->overridep != "0")
        {
            $overridebv = number_format("$dev->overridebv",2,".",",");

            $effectiveoverrate = number_format("$dev->effectiveoverrate",2,".",",");
            $overrideamount = number_format("$dev->overrideamount",2,".",",");

            $pdf->Cell($headCol1,$lebarCell,"$ss",1,0,'R',true);
            $pdf->Cell($headCol2,$lebarCell,"$overridebv",1,0,'R',true);
            $pdf->Cell($headCol3,$lebarCell,"$effectiveoverrate %",1,0,'R',true);
            $pdf->Cell($headCol4,$lebarCell,"$overrideamount",1,0,'R',true);
            $totalOverrideAmount +=	$dev->overrideamount;
            $passup = 0;
            $passup2 = 0;
            $pdf->Ln();
        }
        else {
            $passup	= $dev->overrideamount;
            $passup2 = number_format($passup,2,".",",");
        }

    }
    $net = number_format($totalOverrideAmount + $passup,2,".",",");
    $totalOverride = number_format($totalOverrideAmount,2,".",",");

    $pdf->Cell($totalCol,$lebarCell,"Gross Total",1,0,'R',true);
    $pdf->Cell($headCol4,$lebarCell,"$totalOverride",1,0,'R',true);
    $pdf->Ln();
    $pdf->Cell($totalCol,$lebarCell,"Add Passup",1,0,'R',true);
    $pdf->Cell($headCol4,$lebarCell,"$passup2",1,0,'R',true);
    $pdf->Ln();
    $pdf->Cell($totalCol,$lebarCell,"Net Total",1,0,'R',true);
    $pdf->Cell($headCol4,$lebarCell,"$net",1,0,'R',true);
    $pdf->Ln();

}
//voucher disini


//jangan lupa
if(isset($ldbresult))
{


    $colDownlineCode = 40;
    $colDownlineName = 75;
    $colerank = 10;
    $colPGPV = 10;
    $first = 14;
    $second = 14;
    $third = 14;
    $fourth = 14;
    $fifth = 14;
    $sixth = 14;
    $seventh = 14;
    $eighth = 14;
    $ninth = 14;
    $tenth = 19;

    $titleCol1 = 282;
    $pdf->Ln();
    //$pdf->SetFont('Times','', 9);
    $pdf->Cell($titleCol1,$lebarCell,"LEADERSHIP BONUS",1,0,'C',true);
    $pdf->Ln();
    $pdf->Cell($colDownlineCode,$lebarCell,"Downline Code",1,0,'C',true);
    $pdf->Cell($colDownlineName,$lebarCell,"Downline Name",1,0,'C',true);
    $pdf->Cell($colerank,$lebarCell,"E.Rank",1,0,'C',true);
    $pdf->Cell($colPGPV,$lebarCell,"PGBV",1,0,'C',true);
    $pdf->Cell($first,$lebarCell,"1st",1,0,'C',true);
    $pdf->Cell($second,$lebarCell,"2nd",1,0,'C',true);
    $pdf->Cell($third,$lebarCell,"3rd",1,0,'C',true);
    $pdf->Cell($fourth,$lebarCell,"4th",1,0,'C',true);
    $pdf->Cell($fifth,$lebarCell,"5th",1,0,'C',true);
    $pdf->Cell($sixth,$lebarCell,"6th",1,0,'C',true);
    $pdf->Cell($seventh,$lebarCell,"7th",1,0,'C',true);
    $pdf->Cell($colGBV,$lebarCell,"8th",1,0,'C',true);
    $pdf->Cell($ninth,$lebarCell,"9th",1,0,'C',true);
    $pdf->Cell($tenth,$lebarCell,"Tot.PGBV",1,0,'C',true);
    $pdf->Ln();

    foreach ($ldbresult as $ldbonus)
    {

        $pgbv = number_format("$ldbonus->pgbv",0,".",",");
        $incomepgbv1 = number_format("$ldbonus->incomepgbv1",0,".",",");
        $incomepgbv2 = number_format("$ldbonus->incomepgbv2",0,".",",");
        $incomepgbv3 = number_format("$ldbonus->incomepgbv3",0,".",",");
        $incomepgbv4 = number_format("$ldbonus->incomepgbv4",0,".",",");
        $incomepgbv5 = number_format("$ldbonus->incomepgbv5",0,".",",");
        $incomepgbv6 = number_format("$ldbonus->incomepgbv6",0,".",",");
        $incomepgbv7 = number_format("$ldbonus->incomepgbv7",0,".",",");
        $incomepgbv8 = number_format("$ldbonus->incomepgbv8",0,".",",");
        $incomepgbv9 = number_format("$ldbonus->incomepgbv9",0,".",",");
        $totalpgbv = number_format("$ldbonus->totalpgbv",0,".",",");

        $pdf->Cell($colDownlineCode,$lebarCell,"$ldbonus->odistributorcode",1,0,'L',true);
        $pdf->Cell($colDownlineName,$lebarCell,"$ldbonus->fullnm",1,0,'L',true);
        $pdf->Cell($colerank,$lebarCell,"$ldbonus->effectiverank",1,0,'R',true);
        $pdf->Cell($colPGPV,$lebarCell,"$pgbv",1,0,'R',true);
        $pdf->Cell($first,$lebarCell,"$incomepgbv1",1,0,'R',true);
        $pdf->Cell($second,$lebarCell,"$incomepgbv2",1,0,'R',true);
        $pdf->Cell($third,$lebarCell,"$incomepgbv3",1,0,'R',true);
        $pdf->Cell($fourth,$lebarCell,"$incomepgbv4",1,0,'R',true);
        $pdf->Cell($fifth,$lebarCell,"$incomepgbv5",1,0,'R',true);
        $pdf->Cell($sixth,$lebarCell,"$incomepgbv6",1,0,'R',true);
        $pdf->Cell($seventh,$lebarCell,"$incomepgbv7",1,0,'R',true);
        $pdf->Cell($colGBV,$lebarCell,"$incomepgbv8",1,0,'R',true);
        $pdf->Cell($ninth,$lebarCell,"$incomepgbv9",1,0,'R',true);
        $pdf->Cell($tenth,$lebarCell,"$totalpgbv",1,0,'R',true);
        $pdf->Ln();
    }
}
if(isset($hasil5))
{
    $pdf->AddPage('L');
    $colDownlineCode = 40;
    $colDownlineName = 100;
    $colPGPV = 10;
    $first = 12;
    $second = 12;
    $third = 12;
    $fourth = 12;
    $fifth = 12;
    $sixth = 12;
    $seventh = 12;
    $eighth = 12;
    $ninth = 12;
    $tenth = 20;

    $titleCol1 = 282;

    //$pdf->SetFont('Times','', 9);
    $pdf->Ln();
    $pdf->Cell($titleCol1,$lebarCell,"PLAN B (UNILEVEL)",1,0,'C',true);
    $pdf->Ln();
    $pdf->Cell($colDownlineCode,$lebarCell,"Downline Code",1,0,'C',true);
    $pdf->Cell($colDownlineName,$lebarCell,"Downline Name",1,0,'C',true);
    $pdf->Cell($colPGPV,$lebarCell,"PGBV",1,0,'C',true);
    $pdf->Cell($first,$lebarCell,"1st",1,0,'C',true);
    $pdf->Cell($second,$lebarCell,"2nd",1,0,'C',true);
    $pdf->Cell($third,$lebarCell,"3rd",1,0,'C',true);
    $pdf->Cell($fourth,$lebarCell,"4th",1,0,'C',true);
    $pdf->Cell($fifth,$lebarCell,"5th",1,0,'C',true);
    $pdf->Cell($sixth,$lebarCell,"6th",1,0,'C',true);
    $pdf->Cell($seventh,$lebarCell,"7th",1,0,'C',true);
    $pdf->Cell($colGBV,$lebarCell,"8th",1,0,'C',true);
    $pdf->Cell($ninth,$lebarCell,"9th",1,0,'C',true);
    $pdf->Cell($tenth,$lebarCell,"Total",1,0,'C',true);
    $pdf->Ln();

    foreach ($hasil5 as $data4)
    {
        $pbv = number_format("$data4->pbv",0,".",",");
        $satu = number_format("$data4->incomepbv1",0,".",",");
        $dua = number_format("$data4->incomepbv2",0,".",",");
        $tiga = number_format("$data4->incomepbv3",0,".",",");
        $empat = number_format("$data4->incomepbv4",0,".",",");
        $lima = number_format("$data4->incomepbv5",0,".",",");
        $enam = number_format("$data4->incomepbv6",0,".",",");
        $tujuh = number_format("$data4->incomepbv7",0,".",",");
        $delapan = number_format("$data4->incomepbv8",0,".",",");
        $sembilan = number_format("$data4->incomepbv9",0,".",",");
        $total = number_format("$data4->totalpbv",0,".",",");

        $pdf->Cell($colDownlineCode,$lebarCell,"$data4->odistributorcode",1,0,'L',true);
        $pdf->Cell($colDownlineName,$lebarCell,"$data4->fullnm",1,0,'L',true);
        $pdf->Cell($colPGPV,$lebarCell,"$pbv",1,0,'R',true);
        $pdf->Cell($first,$lebarCell,"$satu",1,0,'R',true);
        $pdf->Cell($second,$lebarCell,"$dua",1,0,'R',true);
        $pdf->Cell($third,$lebarCell,"$tiga",1,0,'R',true);
        $pdf->Cell($fourth,$lebarCell,"$empat",1,0,'R',true);
        $pdf->Cell($fifth,$lebarCell,"$lima",1,0,'R',true);
        $pdf->Cell($sixth,$lebarCell,"$enam",1,0,'R',true);
        $pdf->Cell($seventh,$lebarCell,"$tujuh",1,0,'R',true);
        $pdf->Cell($colGBV,$lebarCell,"$delapan",1,0,'R',true);
        $pdf->Cell($ninth,$lebarCell,"$sembilan",1,0,'R',true);
        $pdf->Cell($tenth,$lebarCell,"$total",1,0,'R',true);
        $pdf->Ln();

    }

}
if(isset($hasil6))
{

    $totLevel=0;
    $titleCol = 100;
    $headCol1 = 13;
    $headCol2 = 13;
    $headCol3 = 14;
    $headCol4 = 18;
    $totalCol = $headCol1 + $headCol2 + $headCol3;

    //$pdf->SetFont('Times','', 9);
    foreach ($hasil6 as $data5)
    {
        $levelpbv = number_format("$data5->levelpbv",0,".",",");
        $levelincome = number_format("$data5->levelincome",0,".",",");

        $pdf->Cell($headCol1,$lebarCell,"$data5->distributorlevel Level",1,0,'C',true);
        $pdf->Cell($headCol2,$lebarCell,"$levelpbv",1,0,'R',true);
        $pdf->Cell($headCol3,$lebarCell,"$data5->levelrate %",1,0,'R',true);
        $pdf->Cell($headCol4,$lebarCell,"$levelincome",1,0,'R',true);
        $pdf->Ln();
        $addinfinityincome = number_format("$data5->addinfinityincome",0,".",",");
        $totLevel += $data5->levelincome;
        $totLevel1 = number_format("$totLevel",0,".",",");
        $totbonus = $data5->addinfinityincome + $totLevel;
    }

    $pdf->Cell($totalCol,$lebarCell,"Unilevel",1,0,'R',true);
    $pdf->Cell($headCol4,$lebarCell,"$totLevel1",1,0,'R',true);
    $pdf->Ln();
    $pdf->Cell($totalCol,$lebarCell,"Add Infinity",1,0,'R',true);
    $pdf->Cell($headCol4,$lebarCell,"$addinfinityincome",1,0,'R',true);
    $pdf->Ln();
    $pdf->Cell($totalCol,$lebarCell,"Plan B Bonus",1,0,'R',true);
    $pdf->Cell($headCol4,$lebarCell,number_format("$totbonus",0,".",","),1,0,'R',true);
    $pdf->Ln();
}

if(isset($hasil4))
{
    if($year < $thnCheck){
        $labelPlanB = "PLAN B (Infinity Bonus)";
        $bvPlanB = "Infinity BV";
        $labelFooter = "INFINITY BONUS";
    }else{
        $labelPlanB = "PLAN B (Initiative Bonus)";
        $bvPlanB = "Initiative BV";
        $labelFooter = "INITIATIVE BONUS";
    }
    $totinfinity = 0;
    $titleCol = 270;
    $headCol1 = 40;
    $headCol2 = 100;
    $headCol3 = 25;
    $headCol4 = 10;
    $headCol5 = 18;
    $totalCol = $headCol1 + $headCol2 + $headCol3 + $headCol4 ;
    $allCol = $totalCol + $headCol5;
    $pdf->Ln();
    $pdf->Cell($allCol,$lebarCell,$labelPlanB,1,0,'C',true);
    $pdf->Ln();
    $pdf->Cell($headCol1,$lebarCell,"Downline Code ",1,0,'C',true);
    $pdf->Cell($headCol2,$lebarCell,"Downline Name",1,0,'C',true);
    $pdf->Cell($headCol3,$lebarCell,$bvPlanB,1,0,'C',true);
    $pdf->Cell($headCol4,$lebarCell,"Total",1,0,'C',true);
    $pdf->Cell($headCol5,$lebarCell,"Total",1,0,'C',true);
    $pdf->Ln();
    $totinfinity = 0;
    foreach ($hasil4 as $data6)
    {
        if($year < $thnCheck){
            $infinitybv = number_format("$data6->infinitybv",0,".",",");
            $bonusrate = number_format("$data6->BonusRate",0,".",",");
            $TotalBonus = number_format("$data6->TotalBonus",0,".",",");
        }else{
            $infinitybv = number_format("$data6->BonusBV",0,".",",");
            $bonusrate = number_format("$data6->BonusRate",0,".",",");
            $TotalBonus = number_format("$data6->TotalBonus",0,".",",");
        }

        /*$infinitybv = number_format("$data6->infinitybv",0,".",",");
        $bonusrate = number_format("$data6->BonusRate",0,".",",");
        $TotalBonus = number_format("$data6->TotalBonus",0,".",",");*/


        $pdf->Cell($headCol1,$lebarCell,"$data6->ODistributorCode",1,0,'L',true);
        $pdf->Cell($headCol2,$lebarCell,"$data6->fullnm",1,0,'L',true);
        $pdf->Cell($headCol3,$lebarCell,"$infinitybv",1,0,'R',true);
        $pdf->Cell($headCol4,$lebarCell,"$bonusrate %",1,0,'R',true);
        $pdf->Cell($headCol5,$lebarCell,"$TotalBonus",1,0,'R',true);
        $pdf->Ln();
        $totinfinity += $data6->TotalBonus;

        $totinfinitybon = number_format("$totinfinity",0,".",",");
    }

    $pdf->Cell($totalCol,$lebarCell,$labelFooter,1,0,'R',true);
    $pdf->Cell($headCol5,$lebarCell,"$totinfinitybon",1,0,'R',true);
}

if($voucher != null) {


$pdf -> AddPage();
$pdf -> SetFont('Courier', '', 8);
$thnCheck = '2017';
$lebarCell = 4;
$pdf -> Ln();
$garis = 0;
$titleCol1 = 50;
$titleCol2 = 90;
$titleCol3 = 25;
$titleCol4 = 25;

$prdCodeCol = 40;
$prdDescCol = 110;
$qtyCol = 30;
$amtCol = 40;

$total = $titleCol1 + $titleCol2 + $titleCol3 + $titleCol4;


$jumrec = count($voucher);
$jum = 1;
	foreach ($voucher as $dta) {

		$pdf->Image("assets/images/logo.jpg", 6, 5, 85, 25);

		if ($dta->vchtype == "P") {
			$pdf -> SetFillColor(0, 0, 0);
			$pdf->Code128(150, 20, $dta->VoucherNo, 50,10);
			$pdf -> SetFillColor(255, 255, 255);
			$pdf->SetXY(160, 15);
			$pdf->SetFont('Courier', 'B', 13);
			$pdf->Write(5,$dta->VoucherNo);
			//$pdf->Write(3,'P');
		}
		elseif ($dta->vchtype == "C") {
			$pdf -> SetFillColor(0, 0, 0);
			$pdf->Code128(150, 20, $dta->voucherkey, 50,10);
			$pdf -> SetFillColor(255, 255, 255);
			$pdf->SetXY(160,15);
			$pdf->SetFont('Courier', 'B', 13);
			$pdf->Write(5, $dta->voucherkey);
			//$pdf->Write(3,'C');
		}

		$pdf -> SetFillColor(255, 255, 255);

		$pdf->SetFont('Courier', 'B', 10);
		$pdf->SetXY(4, 40);

		if ($dta->vchtype == "P") {
			$awal = substr($dta->VoucherNo, 0, 3);
			if ($awal == "XPV" || $awal == "ZVO" || $awal == "XPP") {

				$title_in = "PROMO VOUCHER";
				$title_term = "promo voucher";
				$title_mal = "BAUCER PROMO";
				$sd1 = "*Less: the product voucher amount   :";
				
				
			} else {
				$pdf -> SetFillColor(0, 0, 0);
				$pdf->Code128(150, 20, $dta->VoucherNo, 50,10);
				$pdf -> SetFillColor(255, 255, 255);
				$title_in = "PRODUCT VOUCHER";
				$title_term = "product voucher";
				$title_mal = "BAUCER PRODUK";
				$sd1 = "*Less: the product voucher amount   :";
			}
			$pdf->SetLeftMargin(4);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, $title_in, $garis, 0, 'C', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, $title_mal, $garis, 0, 'C', true);
			$pdf->Ln();

			
			$pdf->SetFont('Courier', '', 10);

			$pdf->Cell($total, $lebarCell, "Voucher No       : $dta->VoucherNo", $garis, 0, 'L', true);
			$pdf->Ln(); 


			$pdf->Cell($total, $lebarCell, "Distributor Code : $dta->DistributorCode", $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, "Name             : $dta->fullnm", $garis, 0, 'L', true);
			$pdf->Ln();
			
			$pdf->SetFont('Courier', 'B', 10);
			$pdf->Cell($total, $lebarCell, "Bonus Period     : $dta->periode_bns", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Ln();	

			$pdf->SetFont('Courier', '', 10);
			if ($awal == "XPV" || $awal == "ZVO" || $awal == "XPP") {
				$pdf->Cell($prdCodeCol, 5, "Product Code", 1, 0, 'C', true);
				$pdf->Cell($prdDescCol, 5, "Product Description", 1, 0, 'C', true);
				$pdf->Cell($qtyCol, 5, "Quantity", 1, 0, 'C', true);
				$pdf->Ln();
				foreach ($dta->product as $prd) {
					$pdf->Cell($prdCodeCol, 5, $prd->prdcd, 1, 0, 'C', true);
					$pdf->Cell($prdDescCol, 5, $prd->prdnm, 1, 0, 'C', true);
					$pdf->Cell($qtyCol, 5, $prd->qtyord, 1, 0, 'C', true);
					//$pdf->Cell($amtCol,5,"",1,0,'C',true);
					$pdf->Ln();
				}
			} else {
				$pdf->Cell($total, $lebarCell, "Amount (RM)      : $dta->VoucherAmtCurr                                          (IND " . number_format($dta->VoucherAmt, 2, ".", ",") . ")", $garis, 0, 'L', true);
				$pdf->Ln();
				$pdf->Ln();
				
				$pdf->Cell($prdCodeCol, 5, "Product Code", 1, 0, 'C', true);
				$pdf->Cell($prdDescCol, 5, "Product Description", 1, 0, 'C', true);
				$pdf->Cell($qtyCol, 5, "Quantity", 1, 0, 'C', true);
				$pdf->Cell($amtCol, 5, "Amount", 1, 0, 'C', true);
				$pdf->Ln();
				for ($x = 1; $x <= 5; $x++) {
					$pdf->Cell($prdCodeCol, 5, "", 1, 0, 'C', true);
					$pdf->Cell($prdDescCol, 5, "", 1, 0, 'C', true);
					$pdf->Cell($qtyCol, 5, "", 1, 0, 'C', true);
					$pdf->Cell($amtCol, 5, "", 1, 0, 'C', true);
					$pdf->Ln();
				}
			}
			$pdf->Ln();

			$totDpAmt = $prdCodeCol + $prdDescCol + $qtyCol;
			$pdf->Cell($totDpAmt, 5, "*Total DP Amount", 0, 0, 'R', true);
			$pdf->Cell($amtCol, 5, "=================", 0, 0, 'C', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "For HQ/Branch/Stockist use only", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "PAYMENT MODE :", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "*Total DP of products redeemed      :", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, $sd1, 0, 0, 'L', true);
			$pdf->Cell(80, 5, "   (IND        ) " . $dta->VoucherAmtCurr, 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "       the difference paid by cash  :", 0, 0, 'L', true);
			$pdf->Cell(80, 5, "    =================", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(80, 5, "       Balance not refundable       :", 0, 0, 'L', true);
			$pdf->Cell(80, 5, "    =================", 0, 0, 'L', true);
			$pdf->Ln();
			$garis = "";
			for ($x = 1; $x <= 90; $x++) {
				$garis .= "-";
			}
			$pdf->Cell(120, 5, $garis, 0, 0, 'L', true);
			$pdf->Ln();


			$pdf->Cell(120, 5, "       I certify that the products redeemed is correct and in good condition", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
//			$pdf->Ln();

			$pdf->Cell(55, 5, "======================= ", 0, 0, 'L', true);
			$pdf->Cell(55, 5, "======================= ", 0, 0, 'L', true);
			$pdf->Cell(85, 5, "====================================== ", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(55, 5, "Distributor's signature ", 0, 0, 'L', true);
			$pdf->Cell(55, 5, "Distributor's Tel. No. ", 0, 0, 'L', true);
			$pdf->Cell(85, 5, "Stockist's/Officer's Stamp & Signature", 0, 0, 'L', true);
			$jum++;
			$pdf->Ln();

			$pdf->SetFont('', 'B', 10);
			//$pdf->Text(5, 98, 'EXPIRED DATE : '.$dta->ExpireDate);

			$pdf->SetFont('Courier', 'B', 10);
			if ($dta->claimstatus == "0") {
				$pdf->SetTextColor(0,0,0);
				$statusclaim = "Voucher ini BELUM diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}elseif ($dta->claimstatus == "1") {
				$pdf->SetTextColor(255,0,0);
				$statusclaim = "Voucher ini SUDAH diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}

			$pdf->Cell($total, $lebarCell, "Claim Status    : $statusclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Location  : $stockistclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Date    	 : ". formatTglIndo($tglclaim), $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->SetTextColor(0,0,0);

			$expireddt = $dta->ExpireDate;
			$pdf->Cell($total, $lebarCell, "Expired Date    : " . formatTglIndo($expireddt), $garis, 0, 'L', true);
			$pdf->Ln();

			$today = date("d/m/Y H:i:s");
			$pdf->Cell($total, $lebarCell, "Printed Date    : " . formatTglIndo($today), $garis, 0, 'L', true);
			$pdf->Ln();



			$pdf->SetFont('Courier', 'B', 7);


			//$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(100, 5, "TERMS AND CONDITIONS", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   1.  This " . $title_term . " allows the bearer to redeem products at any on the K-Link HQ. Branches or Stockist Center.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       However it cannot be exchanged for cash.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   2.  This " . $title_term . " must be redeemed at K-Link Sdn. Bhd. HQ. Branches and Stockist Centres within 90 days dated hereof.", 0, 0, 'L', true);
			$pdf->Ln();
			//$pdf->Cell(100,5,"    However it cannot be exchanged for cash",0,0,'L',true);
			$pdf->Cell(100, 5, "   3.  The redemption of the product(s) wth this voucher will be without BV.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   4.  If the value pf the products redeemed is less than the amount stated in this voucher, then the difference thereof shall", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       not be refunded to the bearer.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   5.  However. if the bearer uses the balance amount of this voucher to further redeem other products, the bearer thereof", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       should pay the difference and other products redeemed will also be without BV.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   6.  K-Link Sdn. Bhd. shall not be held responsible for the loss or damage of this voucher and any losses or damages", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       suffered by the bearer of this voucher that arise from or in connection with the loss or damage of the voucher.", 0, 0, 'L', true);

		}
		else {

			$title_in = "CASH VOUCHER";
			$title_mal = "BAUCER TUNAI";
			$title_term = "cash voucher";
			$sd1 = "*Less: the cash voucher amount      :";

			$pdf->SetLeftMargin(4);
			$pdf->Ln();
			$pdf->Ln();

			$pdf->SetFont('Courier', 'B', 14);
/*
			$pdf->Cell($total, $lebarCell, $dta->VoucherNo, $garis, 0, 'R', true);
			$pdf->Ln();
			$pdf->Ln();
*/
			$pdf->Cell($total, $lebarCell, $title_in, $garis, 0, 'C', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, $title_mal, $garis, 0, 'C', true);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();


			/*
			if ($dta->claimstatus == "1") {
				$pdf->SetFont('Courier', 'B', 10);
				$pdf->Cell(200, 5, "* Claimed at $dta->claim_date at $dta->loccd", 0, 0, 'R', true);
				$pdf->Ln();

			}
			*/

			$pdf->SetFont('Courier', '', 10);

			$pdf->Cell($total, $lebarCell, "Issued Date     : " . formatTglIndo($dta->IssueDate), $garis, 0, 'L', true);
			$pdf->Ln();
			
			$pdf->SetFont('Courier', 'B', 10);
			$bnsPer = $dta->BonusMonth . "/" . $dta->BonusYear;
			$pdf->Cell($total, $lebarCell, "Bonus Payment for " . formatTglIndo($bnsPer), $garis, 0, 'L', true);
			$pdf->Ln();
			
			$pdf->SetFont('Courier', '', 10);
			$pdf->Cell($total, $lebarCell, "Member Code     : " . $dta->DistributorCode, $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, "Name            : $dta->fullnm", $garis, 0, 'L', true);
			$pdf->Ln();

			$amtx = "Amount (IND)    : " . number_format($dta->VoucherAmt, 2, ".", ",") . "              (               " . $dta->VoucherAmtCurr . ")";
			$pdf->Cell($total, $lebarCell, $amtx, $garis, 0, 'L', true);
			$pdf->Ln();

			//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
			//$spellNum = convertNumberToWord($dta->VoucherAmt);
			$cs = number_format($dta->VoucherAmt, 0, "", "");
			$spellNum = $this->terbilang->toTerbilang($cs);
			$pdf->Cell($total, $lebarCell, "                  " . ucwords($spellNum), $garis, 0, 'L', true);
			$pdf->Ln();

			$expireddt = $dta->ExpireDate;
			$pdf->Cell($total, $lebarCell, "Expired Date    : " . formatTglIndo($expireddt), $garis, 0, 'L', true);
			$pdf->Ln();

			$today = date("d/m/Y H:i:s");
			$pdf->Cell($total, $lebarCell, "Printed Date    : " . formatTglIndo($today), $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->SetFont('Courier', 'B', 10);
			if ($dta->claimstatus == "0") {
				$pdf->SetTextColor(0,0,0);
				$statusclaim = "Voucher ini BELUM diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}elseif ($dta->claimstatus == "1") {
				$pdf->SetTextColor(255,0,0);
				$statusclaim = "Voucher ini SUDAH diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}

			$pdf->Cell($total, $lebarCell, "Claim Status    : $statusclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Location  : $stockistclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Date      : ". formatTglIndo($tglclaim), $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->SetTextColor(0,0,0);


			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();

			$pdf->Cell(60, 5, "==========================", $garis, 0, 'L', true);
			$pdf->Cell(60, 5, "==========================", $garis, 0, 'L', true);
			$pdf->Cell(60, 5, "===========================", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->SetFont('Courier', 'B', 10);
			$pdf->Cell(60, 5, "DISTRIBUTOR'S SIGNATORY ", $garis, 0, 'C', true);
			$pdf->Cell(60, 5, "STOCKIST SIGNATORY & CHOP ", $garis, 0, 'C', true);
			$pdf->Cell(60, 5, "AUTHORIZED SIGNATORY & CHOP", $garis, 0, 'C', true);
			$pdf->Ln();


			$pdf->SetFont('Courier', 'B', 7);
			$pdf->Ln();
			$pdf->Ln();


			$pdf->Cell(100, 5, "N.B. - This Cash Voucher must be redeemed at our stockist or HQ within 90 days dated hereof,", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       for exchange of cash afterwhich would be void.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       Baucer ini mesti ditunaikan di Pusat Stockist atau Ibu Pejabat dalam tempoh 90 hari dari tarikh baucher ini,", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       dan akan di batal selepas tempoh ini.", 0, 0, 'L', true);
			$jum++;
		}

		if ($jum <= $jumrec) {
			$pdf->AddPage();

		}

	}
}

$pdf->Ln();
//$url = "http://www.k-net.co.id/api/voucher/".$idmember."/".$month."/".$year;
//$url2 = "<a href=\"$url\">Tess</a>";
//$pdf->Link(100,10,10,10,$url);
//$pdf->Cell(200,$lebarCell,$url2,0,0,'R',true);
$title = "bns_report"."-".$month."-".$year.".pdf";
//$pdfx->SetTitle($title);
$pdf->Output();






	
?>
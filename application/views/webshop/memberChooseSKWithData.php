<!-- Breadcrumb -->
<ul class="breadcrumbs">
	<div class="container">
		<li class="home">
			<a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png'); ?>" alt=""/></a>&nbsp;
			&nbsp; <span>&gt;</span>
		</li>
		<li class="home">
			&nbsp;
			Shopping&nbsp;&nbsp;
			<span>&gt;</span>
		</li>
		<li class="home">
			<span class="red"> &nbsp;Pengiriman&nbsp;&nbsp;</span>
		</li>
	</div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
		<div id="formCart1">
			<?php
	      	  $usr = getUserID();
			  if($usr == "IDSPAAA66834") {
	      	?> 
	        <form id="formMember" action="<?php echo base_url('member/checkout/sgo'); ?>" method="POST" onsubmit="return Shopping.validateStarterKit()">        
	    	<?php
			  } else {
	    	?>
	    	 <form id="formMember" action="<?php echo base_url('member/checkout'); ?>" method="POST" onsubmit="return Shopping.validateStarterKit()">
	    	<?php
			  }
	    	?>
			<!--<form id="formMember" action="<?php echo base_url('member/checkout'); ?>" method="POST" onsubmit="return Shopping.validateStarterKit()">-->

				<!-- Delivery Options-->
				<br />
				<div class="col-md-6 col-xs-12">
					<span class="title_header"> <i class="fa fa-truck"></i> Tipe Pengiriman Starterkit </span>

					<!--  Options 1-->
					<div class="col-md-12 col-xs-12 nopadding voffset4">
                     <?php
						$personal_info = $this->session->userdata('personal_info');
						if($personal_info['delivery'] == "1")
						{
						?>
						<!--  Options 1 header-->
						<div class="col-md-12 col-xs-12 delivery_header">
							<input name="delivery" id="delivery" type="radio" class="delivery_choice" checked="checked" onclick="Shopping.getDelChoice(this.value)" value="1"/>
							Diambil di Stockist
						</div>
						<!--  Options 1 header-->

						<!--  Options 1 detail-->
						<div class="col-md-12 col-xs-12 diambil" style="display: block;">
							<p class="p2 nopadding">
								Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
								<br/>
								<br/>
							</p>
							<div class="register-top-grid">

								<div>
									<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
										<option value="">--Select Here--</option>
										<?php
										//print_r($show_provinsi);
										/*foreach ($show_provinsi['arrayData'] as $row) {
											echo "<option value=\"" . $row->kode_provinsi . "\">" . strtoupper($row->provinsi) . "</option>";
										}*/
										
	                                    //print_r($show_provinsi);
	                                    foreach($show_provinsi['arrayData'] as $row){
	                                        if($row->kode_provinsi == $personal_info['provinsi']) {
	                                          echo "<option value=\"".$row->kode_provinsi."\" selected=\"selected\">".strtoupper($row->provinsi)."</option>";	
	                                        } else {	
	                                          echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";
											}
	                                    }
	                                 
										?>
									</select>
									<input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="<?php echo $personal_info['nama_provinsi']; ?>" />
								</div>
								<div>
									<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">
										<?php 
	                                   foreach($shipping['kota'] as $row){
	                                        if($row->kode_kabupaten == $personal_info['kota']) {
	                                          echo "<option value=\"".$row->kode_kabupaten."\" selected=\"selected\">".strtoupper($row->kabupaten)."</option>";	
	                                        } else {	
	                                          echo "<option value=\"".$row->kode_kabupaten."\">".strtoupper($row->kabupaten)."</option>";
											}
	                                    }
	                                 ?>
									</select>
									<input type="hidden" id="nama_kota1" name="nama_kota1" value="<?php echo $personal_info['nama_kota']; ?>" />
									
									<!--<input type="text" name="kota" id="kota"/>-->
								</div>
								<div>
									<span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
									<!--<input type="text" name="kecamatan" id="kecamatan"/>-->
									<select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >
										<?php 
	                                   foreach($shipping['kecamatan'] as $row){
	                                        if($row->kode_kecamatan == $personal_info['kecamatan']) {
	                                          echo "<option value=\"".$row->kode_kecamatan."\" selected=\"selected\">".strtoupper($row->kecamatan)."</option>";	
	                                        } else {	
	                                          echo "<option value=\"".$row->kode_kecamatan."\">".strtoupper($row->kecamatan)."</option>";
											}
	                                    }
	                                   ?>
									</select>
									<input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="<?php echo $personal_info['nama_kecamatan']; ?>" />
									<input type="hidden" id="state1" name="state1" value="<?php echo $personal_info['state']; ?>" />
								</div>

								<!--  Options 1 lokasi-->
								<div>
									<span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestk2(this,'#pricecode')">
										<?php 
	                                   foreach($shipping['listStk'] as $row){
	                                        if($row->loccd == $personal_info['stockist']) {
	                                          echo "<option value=\"".$row->loccd."\" selected=\"selected\">".strtoupper($row->fullnm)."</option>";	
	                                        } else {	
	                                          echo "<option value=\"".$row->loccd."\">".strtoupper($row->fullnm)."</option>";
											}
	                                    }
	                                   ?>
									</select>
									<input type="hidden" id="nama_stockist" name="nama_stockist" value="<?php echo $personal_info['nama_stockist']; ?>" />
								</div>
								<div class="clearfix"></div>
								<!--  Options 1 lokasi-->
							</div>
						</div>
						<!--  Options 1 detail-->
					</div>
					<!--  Options 1-->

					<!--  Options 2-->
					<div class="col-md-12 col-xs-12 nopadding voffset4">
						<!--  Options 2 header-->
						<div class="col-md-12 col-xs-12 delivery_header">
							<input name="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/>
							Dikirim ke Alamat
						</div>
						<!--  Options 2 header-->

						<!--  Options 2 delivery address-->
						<div class="col-md-12 col-xs-12 vpullset3 dikirim" style="display: none;">
							<!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

							<div class="register-top-grid">
								<div>
									<span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
									<input type="text" class="uppercase" name="nama_penerima" id="nama_penerima"/>
								</div>

								<div>
									<span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
									<input class="numeric-input" type="text" name="notlp" id="notlp"/>
								</div>
								<div style="width:98%">
									<span>Email Address</span>
									<input type="text" name="email" id="email"/>
								</div>

								<div style="width:98%">
									<span>Alamat<label><font color="red">&nbsp;*</font></label></span>
									<textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"></textarea>
									<input type="hidden" id="destination_address" name="destination_address" value="" />
								</div>
								
								
								<div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
									Pilih Stockist Terdekat Dengan Alamat Tujuan
								</div>
								
								<div>
	                                <span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
	                                 <select class="form-list" name="shipper" id="shipper" onchange="Shopping.listProvinceByCargo(this)" >
	                                 <option value="">--Select Here--</option>
	                                 <?php
	                                    //print_r($listCargo);
	                                    foreach($listCargo as $row){
	                                        echo "<option value=\"".$row->shipper_id."\">".strtoupper($row->shipper_name)."</option>";
	                                    }
	                                 
	                                 ?>
	                                </select>
                                
                                </div>
								<div id="provDiv">
									<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')" >
										<option value="select">--Select Here--</option>
										<?php
										//print_r($show_provinsi);
										foreach ($show_provinsi['arrayData'] as $row) {
											echo "<option value=\"" . $row->kode_provinsi . "\">" . strtoupper($row->provinsi) . "</option>";
										}
										?>
									</select>
									<input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
								</div>
								<div id="KabDiv">
									<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="kota" id="kota" onchange="Shopping.tampilkanKecamatan()"></select>
									<input type="hidden" id="nama_kota" name="nama_kota" value="" />
									<!--<input type="text" name="kota" id="kota"/>-->
								</div>
								<div id="KecDiv">
									<span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
									<!--<input type="text" name="kecamatan" id="kecamatan"/>-->
									<select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >

									</select>
									<input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
								</div>

								<div>
									<span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
									<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.tampilkanPriceCode()"></select>
									<input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="" />
									<input type="hidden" id="sender_address" name="sender_address" value="" />
									<input type="hidden" id="jne_branch" name="jne_branch" value="" />
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<!--  Options 2 delivery address-->
					</div>
					<!--  Options 2-->
					<?php
					} else {
					?>	
						<!--  Options 1 header-->
					<div class="col-md-12 col-xs-12 delivery_header">
						<input name="delivery"  id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/>
						Diambil di Stockist
					</div>
					<!--  Options 1 header-->

					<!--  Options 1 detail-->
					<div class="col-md-12 col-xs-12 diambil" style="display: none;">
						<p class="p2 nopadding">
							Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
							<br/>
							<br/>
						</p>
						<div class="register-top-grid">

								<!--  Options 1 area-->
								
								<div>
	                                <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
	                                 <select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
	                                 <option value="">--Select Here--</option>
	                                 <?php
	                                    //print_r($show_provinsi);
	                                    foreach($show_provinsi['arrayData'] as $row){
	                                        
	                                          echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";
											
	                                    }
	                                 
	                                 ?>
	                                </select>
	                                <input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="<?php echo $personal_info['nama_provinsi'] ?>" />
	                            </div>
								<!--  Options 1 area-->

								<!--  Options 1 lokasi-->
								<div>
	                                <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
	                                <select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">
	                                 
	                                </select>
	                                <input type="hidden" id="nama_kota1" name="nama_kota1" value="<?php echo $personal_info['nama_kota'] ?>" />
	                                 <!--<input type="text" name="kota" id="kota"/>-->
	                            </div>
	                            <div>
	                                <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
	                                 <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
	                                 <select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >
	                                   
	                                 </select> 
	                                <input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
	                                <input type="hidden" id="state1" name="state1" value="<?php echo $personal_info['state']; ?>" />
	                            </div>
	                            <div>
		                            <span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
		                            <select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestk2(this,'#pricecode')">
		                            
		                            </select>
		                            <input type="hidden" id="nama_stockist" name="nama_stockist" value="<?php echo $personal_info['nama_stockist'] ?>" />
		                        </div>
								<div class="clearfix"> </div>
								<!--  Options 1 lokasi-->
							</div>
					</div>
					<!--  Options 1 detail-->
				</div>
				<!--  Options 1-->

				<!--  Options 2-->
				<div class="col-md-12 col-xs-12 nopadding voffset4">
					<!--  Options 2 header-->
					<div class="col-md-12 col-xs-12 delivery_header">
						<input name="delivery" checked="checked" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/>
						Dikirim ke Alamat
					</div>
					<!--  Options 2 header-->

					<!--  Options 2 delivery address-->
					<div class="col-md-12 col-xs-12 vpullset3 dikirim" >
						<!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

						<div class="register-top-grid">
							<div>
								<span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
								<input type="text" class="uppercase" name="nama_penerima" id="nama_penerima" value="<?php echo $personal_info['nama_penerima']; ?>" />
							</div>

							<div>
								<span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
								<input class="numeric-input" type="text" name="notlp" id="notlp" value="<?php echo $personal_info['notlp']; ?>" />
							</div>
							<div style="width:98%">
								<span>Email Address</span>
								<input type="text" name="email" id="email" value="<?php echo $personal_info['email']; ?>" />
							</div>
							
							<div style="width:98%">
								<span>Alamat<label><font color="red">&nbsp;*</font></label></span>
								<textarea  onkeyup="textAreaUppercase(this)" class="uppercase" name="alamat" rows="4" name="alamat" id="alamat"><?php echo $personal_info['alamat']; ?></textarea>
								<input type="hidden" class="uppercase" id="destination_address" name="destination_address" value="<?php echo $this -> session -> userdata('destination_address'); ?>" />
							</div>
							
							<div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
									Pilih Stockist Terdekat Dengan Alamat Tujuan
								</div>
								
							<div>
	                                <span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
	                                 <select class="form-list" name="shipper" id="shipper" onchange="Shopping.listProvinceByCargo(this)" >
	                                 <option value="">--Select Here--</option>
	                                 <?php
	                                    //print_r($listCargo);
	                                    foreach($listCargo as $row){
	                                        echo "<option value=\"".$row->shipper_id."\">".strtoupper($row->shipper_name)."</option>";
	                                    }
	                                 
	                                 ?>
	                                </select>
                                
                                </div>
                                	
							<div id="provDiv">
								<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
								<select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')" >
									<option value="select">--Select Here--</option>
									<?php
									foreach ($show_provinsi['arrayData'] as $row) {
										if ($row -> kode_provinsi == $personal_info['provinsi']) {
											echo "<option value=\"" . $row -> kode_provinsi . "\" selected=\"selected\">" . strtoupper($row -> provinsi) . "</option>";
										} else {
											echo "<option value=\"" . $row -> kode_provinsi . "\">" . strtoupper($row -> provinsi) . "</option>";
										}

									}
									?>
								</select>
								<input type="hidden" id="nama_provinsi" name="nama_provinsi" value="<?php echo $personal_info['nama_provinsi']; ?>" />
							</div>
							<div id="KabDiv">
								<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
								<select class="form-list" name="kota" id="kota" onchange="Shopping.tampilkanKecamatan()">
									<option value="">--Select Here--</option>
									<?php
									foreach ($shipping['kota'] as $row) {
										if ($row -> kode_kabupaten == $personal_info['kota']) {
											echo "<option value=\"" . $row -> kode_kabupaten . "\" selected=\"selected\">" . strtoupper($row -> kabupaten) . "</option>";
										} else {
											echo "<option value=\"" . $row -> kode_kabupaten . "\">" . strtoupper($row -> kabupaten) . "</option>";
										}

									}
									?>
								</select>
								<input type="hidden" id="nama_kota" name="nama_kota" value="<?php echo $personal_info['nama_kota']; ?>" />
								<!--<input type="text" name="kota" id="kota"/>-->
							</div>
							<div id="KecDiv">
								<span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
								<!--<input type="text" name="kecamatan" id="kecamatan"/>-->
								<select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >
									<option value="">--Select Here--</option>
									<?php
									foreach ($shipping['kecamatan'] as $row) {
										if ($row -> kode_kecamatan == $personal_info['kecamatan']) {
											echo "<option value=\"" . $row -> kode_kecamatan . "\" selected=\"selected\">" . strtoupper($row -> kecamatan) . "</option>";
										} else {
											echo "<option value=\"" . $row -> kode_kecamatan . "\">" . strtoupper($row -> kecamatan) . "</option>";
										}

									}
									?>
								</select>
								<input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="<?php echo $personal_info['nama_kecamatan']; ?>" />
							</div>
							
							<!-- <div>
							<span>Pilih Area Stockist Referensi<font color="red">&nbsp;*</font></span>
							<select class="form-list" name="stkarea1" onchange="Shopping.show_stk(this,'#stockistref')" id="stkarea1">
							<option value="">--Select here--</option>
							<?php
							foreach($show_state as $dta)
							{
							if($dta->st_id == $personal_info['stkarea']) {
							echo "<option value=\"$dta->st_id\" selected=\"selected\">$dta->description</option>";
							} else {
							echo "<option value=\"$dta->st_id\">$dta->description</option>";
							}
							}
							?>
							</select>
							<input type="hidden" id="nama_stockistr1area" name="nama_stockistr1area" value="<?php echo $personal_info['nama_stkarea']; ?>" />
							</div> -->
							
							<div>
								<span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
								<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.tampilkanPriceCode()">
									<option value="">--Select here--</option>
									<?php
							
									//foreach ($show_stk_by_area['arrayData'] as $dtax) {
									foreach($shipping['listStk'] as $dtax) {	
										if ($dtax -> loccd == $personal_info['stockist']) {
											echo "<option value=\"$dtax->loccd\" selected=\"selected\">$dtax->loccd - $dtax->fullnm</option>";
										} else {
											echo "<option value=\"$dtax->loccd\">$dtax->loccd - $dtax->fullnm</option>";
										}
									}
									?>
								</select>
								<input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="<?php echo $personal_info['nama_stockist']; ?>" />
								<input type="hidden" id="sender_address" name="sender_address" value="<?php echo $this -> session -> userdata('sender_address'); ?>" />
								<input type="hidden" id="jne_branch" name="jne_branch" value="<?php echo $this -> session -> userdata('jne_branch'); ?>" />
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<!--  Options 2 delivery address-->
				</div>
				<?php
				}
				
					?>
				</div>

				<div id="listCartPrd">
					<!-- Order Details-->
					<div class="col-md-6 col-xs-12">
						<span class="title_header"> <i class="fa fa-check-square-o"></i> Detail Data Member </span>
						
						<?php
						$member_info = $this->session->userdata("member_info");
						//echo "ID Member"
						//print_r($member_info);
						?>
						<div class="col-lg-12 col-xs-12 cart_header">
						ID Sponsor     : <?php echo $member_info['sponsorid']."<br />"; ?>
						Nama Sponsor   : <?php echo $member_info['sponsorname']."<br />"; ?>
						Nama Member    : <?php echo $member_info['membername']."<br />"; ?>
						No KTP         : <?php echo $member_info['idno']."<br />"; ?>
						Jenis Kelamin  : 
						<?php 
						  if($member_info['sex'] == "M") {
						  	echo "Pria<br />";
						  } else {
						  	echo "Wanita<br />";
						  }
						?>  
						Tgl Lahir      : <?php echo $member_info['tgllhr']."/".$member_info['blnlhr']."/".$member_info['thnlhr']."<br />"; ?>
						Alamat         : <?php echo $member_info['addr1']." ".$member_info['addr2']." ".$member_info['addr3']."<br />"; ?>
						No HP          : <?php echo $member_info['tel_hp']."<br />"; ?>
						Rekening Bank  : <?php echo $member_info['pilBank']."<br />"; ?>
						No Rekening    : <?php echo $member_info['no_rek']."<br />"; ?>
						</div>

					</div>
					<!-- Order Details-->
				</div>
				<br />
				<div class="col-md-6 col-xs-12">
					<span class="title_header"> <i class="fa fa-check-square-o"></i> Pilih Jenis Starterkit </span>
					<div class="register-top-grid">
						<br />
						<select id="pilStarterkit" name="pilStarterkit" onchange="Shopping.chooseSK(this.value)">
							<option value="">--Pilih disini--</option>
							<?php
							//$starterkit_prd = $this->session->user_data('starterkit_prd');
							//$pilih = $starterkit_prd['prdcd']."|".$starterkit_prd['prdnm']."|".$starterkit_prd['weight'];
							//print_r($listStarterkit);
							foreach ($listStarterkit['arrayData'] as $dta) {
								echo "<option value=\"$dta->prdcd|$dta->prdnm|$dta->weight|$dta->price_w|$dta->price_e|$dta->is_charge_ship\">";
								echo $dta->prdnm;
								echo "</option>";
							}
							?>
						</select>
						<input type="hidden" id="is_charge" name="is_charge" value="" />
						<span id="infoSK" style="display: none">
							
						</span>	
					</div>
				</div>

				<!-- next button-->
				<div class="col-md-12">

					<input type="hidden" name="pricecode" id="pricecode" value="<?php echo $this->session->userdata('pricecode'); ?>" />
					<!--<a href="payment.php" class="btn1 btn2 btn-primary1 pull-left"><span>Lanjutkan</span><i class="fa fa-arrow-right"></i></a>-->

					<!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" onclick="Shopping.checkout()" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />-->
					<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">
						Lanjutkan <i class="fa fa-arrow-right"></i>
					</button>
					<br />
				</div>
				<!-- next button-->
		</div>
		</form>
		<form method="get" action="<?php echo base_url('member/back') ?>">
			<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">
						<i class="fa fa-arrow-left">Ubah Data Member </i>
			</button>
		</form>
		<!-- Delivery Options-->
	</div>
	<!-- End formCart1-->
	<div id="divCheckOut"></div>
	<div id="afterPayment"></div>
</div>
</div>
<!--Checkout Wrapper-->

<style>
	.delivery_header {
    border-bottom: 1px solid #d0d0d0;
    border-top: 1px solid #d0d0d0;
    float: left;
    font-size: 12px;
    margin: 8px 0 1em;
    padding: 1em 0;
    text-transform: none;
}

.xcx {
	line-height:10px;
	padding:4px;
}
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Payment Preview&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Preview Pembayaran&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
         <?php
          $shipping_jne_info = $this->session->userdata('shipping_jne_info');
          if($pay_gateway == true) {
          	
			 //pembayaran dengan voucher, jumlah total nilai voucher lebih kecil daripada total nilai pembelanjaan
             //sehingga sisa pembayaran di bayar menggunakan payment gateway 
          	 if($vch_stt == true) {
          	    $total_semua = $tot_pay + $shipping_jne_info['price'] + $res[0]->charge_connectivity  + $res[0]->charge_admin;
          	    $sisa = $total_semua - $tot_nilai_voucher;	
          	  } else {
          	    //Pembayaran hanya dengan menggunakan payment gateway, tidak menggunakan voucher sama sekali 
          	    $total_semua = $tot_pay + $shipping_jne_info['price'] + $res[0]->charge_connectivity  + $res[0]->charge_admin;
          	    $sisa = $total_semua;
          	 	
          	 }
			 
			 echo "<div class=\"col-lg-12 col-xs-12 delivery_header\" style=\"font-size:18px;\">Preview Pembayaran via ".$bankDescDetail."</div>";
	          echo "<div class=\"col-lg-12 col-xs-12 nomargin\">";
	             echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
	              echo "<table width=80% align=center border=0>";
                  echo "<tr><td align=right>Tipe Pembayaran         :</td><td align=right>".$bankDescDetail."</td></tr>";
				  echo "<tr><td align=right>Total Pembelanjaan      :</td><td align=right>Rp. ".number_format($tot_pay, 2, ",", ".")."</td></tr>";
				  echo "<tr><td align=right>Total BV                :</td><td align=right>".number_format(getTotalBV(), 2, ",", ".")."</td></tr>";
                  echo "<tr><td align=right>Biaya Connectivity      :</td><td align=right>Rp. ".number_format($res[0]->charge_connectivity, 2, ",", ".")."</td></tr>";
                  echo "<tr><td align=right>Biaya Administrasi      :</td><td align=right>Rp. ".number_format($res[0]->charge_admin, 2, ",", ".")."</td></tr>";
				  echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                  
				  echo "<tr><td align=right>Total                   :</td><td align=right>Rp. ".number_format($total_semua, 2, ",", ".")."</td></tr>";
				  echo "<tr><td align=right>Total Nilai Voucher     :</td><td align=right>(-) Rp. ".number_format($tot_nilai_voucher, 2, ",", ".")."</td></tr>";
				  
				  echo "<tr><td align=right>Sisa yang harus dibayar :</td><td align=right>".number_format($sisa, 2, ",", ".")."</td></tr>";
                  echo "</table>";
				  
				  echo "<button class=\"btn2 btn2 btn-primary1\" onclick=\"submitdataXX()\">";
                  echo "<i class=\"fa fa-arrow-right\"></i>";
                  echo "<span>Proses Pembayaran</span>";
                  echo "</button>";
				  echo "<input type=hidden name=temp_orderid value=\"$temp_paymentIdx\" />";
				  echo "<input type=\"hidden\" id=\"total_all\" value=\"$sisa\" />";
	             echo"</p>";	
	          /*echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
              echo $listVch;
			   echo"</p>";*/
			  echo "</div>";
			  echo "<iframe id=\"sgoplus-iframe\" src=\"\" scrolling=\"no\" frameborder=\"0\"></iframe>";	
          	  
          }  else {
          	 //Jika pembayaran hanya dengan voucher belanja
	         //Jumlah nilai total voucher lebih besar atau sama dengan total nilai pembelanjaan 
	          echo "<div class=\"col-lg-12 col-xs-12 delivery_header\" style=\"font-size:18px;\">Preview Pembayaran via Voucher Belanja</div>";
	          echo "<div class=\"col-lg-12 col-xs-12 nomargin\">";
	             echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
	              echo "<table width=70% align=center border=0>";
                  echo "<tr><td align=right>Tipe Pembayaran       :</td><td align=right>Voucher Belanja</td></tr>";
				  echo "<tr><td align=right>Total Pembelanjaan    :</td><td align=right>Rp. ".number_format($tot_pay, 2, ",", ".")."</td></tr>";
				  
				  echo "<tr><td align=right>Total BV              :</td><td align=right>".number_format(getTotalBV(), 2, ",", ".")."</td></tr>";
				  echo "<tr><td align=right>Biaya Kirim           :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                  $total_semua = $tot_pay + $shipping_jne_info['price'];
				  echo "<tr><td align=right>Total                 :</td><td align=right>".number_format($total_semua, 2, ",", ".")."</td></tr>";
                  echo "</table>";
				  $url = base_url('pay/vch');
				  echo "<form method=POST action=\"$url\">";
				  echo "<button class=\"btn2 btn2 btn-primary1\">";
                  echo "<i class=\"fa fa-arrow-right\"></i>";
                  echo "<span>Proses Pembayaran dengan Voucher</span>";
                  echo "</button>";
				  echo "<input type=hidden name=temp_orderid value=\"$temp_paymentIdx\" />";
				  echo "</form>";
	             echo"</p>";
	          /*echo "</div>";
			  echo "<br />";
			  echo "<br />";
			  echo "<div class=\"col-lg-12 col-xs-12 nomargin\">"; */
			  /*echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
              echo $listVch;
			   echo"</p>";*/
			  echo "</div>";
          }
         ?>      
		</div>	
	</div>
</div>
 
<script type="text/javascript">
    
  function submitdataXX() {
     var total_all = $("#total_all").val();
		
 	 var data = {
					key : "<?php echo $temp_orderidx;?>",
					paymentId : "<?php echo $temp_paymentIdx;?>",
					paymentAmount : <?php echo $sisa; ?>,
					backUrl : "<?php echo $backURL;?>",
					bankCode : "<?php echo $bankCode; ?>",
					bankProduct: "<?php echo $bankDesc; ?>"
			    },
		sgoPlusIframe = document.getElementById("sgoplus-iframe");
				
		if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
		SGOSignature.receiveForm();
					
		
}
</script>       
<!-- Login Form --> 
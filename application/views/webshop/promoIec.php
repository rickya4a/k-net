
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Promo Info&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
        
         <span class="red"> &nbsp;Cek IEC ( Initiative Elite Challenge )&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->
	

<div class="row vpullset4" style="min-height: 410px;">
    <div class="container">
        <div class="col-lg-12 col-xs-12 cart_header">  
            <div class="col-lg-12">
                <!--<h1>Menu Ini Sedang Dalam Tahap Pengembangan</h1>-->
                <h2>Listing Initiative Elite Challenge ( IEC ) </h2>
            </div>            
        </div>
        <?php if(empty($result)){
            echo "<h4>Data Tidak Ada</h4>";
        }else{
        ?>
        <div id="SummIEC">
            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">  
                <!--line 1-->
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID(); ?> 
                </div> 
                <!--line 1-->
                <!--line 2-->
                <div class="col-lg-3 col-xs-3">
                    Nama Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo getUserName(); ?>
                </div> 
                <!--line 2-->          
            </div>
        
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-3 col-xs-3">Bonus Month</div> 
                <div class="col-lg-2 col-xs-2">Total Recruiter</div>
                <div class="col-lg-4 col-xs-4">Recruiter dg 400BV ( Above )</div>
            </div>
        
            <?php
                $no = 1;
                foreach($result as $row){
                    $bnsperiod = date("m/Y",strtotime($row->bnsperiod));
            ?>
            <div class="col-lg-12 col-xs-12 cart_content text-center" id="<?php echo $no;?>">
                <div class="col-lg-1 col-xs-1"><?php echo $no;?></div>
                <div class="col-lg-3 col-xs-3"><?php echo $bnsperiod;?></div> 
                <div class="col-lg-2 col-xs-2"><a onclick="Promo.getDetIEC('<?php echo $bnsperiod;?>','<?php echo $idmember;?>','<?php echo $flag;?>')"><?php echo $row->jml;?></a></div>
                <div class="col-lg-4 col-xs-4"><?php echo $row->jml400;?></div>
            </div>
            <?php $no++; } ?>
            
        </div>
        <?php }?>
        <div id="detIEC"></div>
    </div>
</div>

<div class="row vpullset4">
    <div class="container">
        <?php
            if(empty($detTrans['detail']))
            {
                echo "Tidak Ada Data";
            }else{
        ?>
        <div class="col-md-12">
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                   Hasil Pencarian Transaksi Member
                </div>            
            </div>
            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">                
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID(); ?> 
                </div> 
               
                <div class="col-lg-3 col-xs-3">
                    Nama Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo getUsername(); ?>
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Bonus Period  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $period; ?>
                </div> 
            </div>             

            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                    Detail Produk
                </div>            
            </div>
            <!-- table result-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-2 col-xs-2">Kode Produk</div> 
                <div class="col-lg-3 col-xs-3">Nama Produk</div> 
                <div class="col-lg-1 col-xs-1">Qty</div>
                <div class="col-lg-1 col-xs-1">DP</div>
                <div class="col-lg-1 col-xs-1">BV</div> 
                <div class="col-lg-1 col-xs-1">Tot DP</div> 
                <div class="col-lg-1 col-xs-1">Tot BV</div>
            </div>
            <?php
                $no = 1;
                $totalDP = 0;
                $totalBV = 0;
                
               foreach($detTrans['detail'] as $dt)
               {
                   // $dp = number_format(substr($dt->dp, 0, -3),0,",",",");
                   // $bv = number_format(substr($dt->bv, 0, -3),0,",",",");
                    $qty = number_format($dt->qtyord,0,".",",");
                   // $tdp = number_format($dt->TDP,0,".",",");
                   // $tbv = number_format($dt->TBV,0,".",",");
                    echo "
                        <div class=\"col-lg-12 col-xs-12 cart_content text-center\">
                            <div class=\"col-lg-1 col-xs-1\">".$no."</div>
                            <div class=\"col-lg-2 col-xs-2\">".$dt->prdcd."</div>
                            <div class=\"col-lg-3 col-xs-3\">".$dt->prdnm."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$qty."</div>
                            <div class=\"col-lg-1 col-xs-1\">".(int) $dt->dp."</div>
                            <div class=\"col-lg-1 col-xs-1\">".(int) $dt->bv."</div>
                            <div class=\"col-lg-1 col-xs-1\">".(int) $dt->TDP."</div>
                            <div class=\"col-lg-1 col-xs-1\">".(int) $dt->TBV."</div>
                        </div>
                    ";
                    $totalDP += $dt->TDP;
  		            $totalBV += $dt->TBV;
                    $no++;
               }
               //$totDP = number_format($totalDP,0,",",",");
               //$totBV = number_format($totalBV,0,",",",");
               echo "
               <div class=\"col-lg-12 col-xs-12 cart_content text-center\" style=\"margin-bottom:20px;\">
               <div class=\"col-lg-7 col-xs-7\" >&nbsp;</div>
               <div class=\"col-lg-2 col-xs-2\" >TOTAL</div>
               <div class=\"col-lg-1 col-xs-1\" >".$totalDP."</div>
               <div class=\"col-lg-1 col-xs-1\" >".$totalBV."</div>
               <div class=\"col-lg-1 col-xs-1\" >&nbsp;</div>
               </div>";
               
            ?>
        </div>
        <?php }?>
        <div>
            <input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="Sales.backToSearch();" name="submit" id="submit"/>
        </div>
    </div>
 </div>
 

        
        <!--right-->
    	<div class="col-md-12 col-sm-12" id="acc_div"> 
            
        	<!-- Track Order -->
        	<div class="col-md-6 col-md-offset-3 text-center">
				 
                	<div class="track-order-grid">
                    	<div>
                        	<span>HISTORI TRANSAKSI PEMBELANJAAN YANG DIKIRIM KE ALAMAT</span>
                        	                       
                   		</div>
                       
            		</div>
               	
			</div>
           
            <!-- Track Order -->
            <!-- tracking content header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-3 col-xs-3">NO.ORDER</div>
                <div class="col-lg-3 col-xs-3">ID MEMBER</div>
                <div class="col-lg-3 col-xs-3">TANGGAL TRX</div> 
                <div class="col-lg-3 col-xs-3">CNOTE</div>          	 
            </div>
            <!-- tracking content-->
            <!-- tracking content 1-->
            <div id="resultAwb" class="col-lg-12 col-xs-12 cart_content">
            	<?php
            	if($listTrx != null) {
            	foreach($listTrx as $dta) {
            		echo "<div class=\"col-lg-3 col-xs-3\">$dta->orderno</div>";
                    echo "<div class=\"col-lg-3 col-xs-3\">$dta->id_memb</div>";
                   echo "<div class=\"col-lg-3 col-xs-3\">$dta->datetrans</div>";
				   if($dta->cargo_id == "2") {
				   	 echo "<div class=\"col-lg-3 col-xs-3\"><a id=\"$dta->conoteJNE\" onclick=\"getDetailCnote(this)\">$dta->conoteJNE</a></div>";
				   } else {
				   	 //echo "<div class=\"col-lg-3 col-xs-3\">$dta->conoteJNE</div>";
					 echo "<div class=\"col-lg-3 col-xs-3\"><a id=\"$dta->conoteJNE\" onclick=\"getDetailCnoteJne(this)\">$dta->conoteJNE</a></div>";
				   }
                   
            	}
            	}
				?>
            </div>
            <div id="detailCnote" class="col-lg-12 col-xs-12">
            	
            </div>
    	</div>
    <!--right-->
    
<!-- Shopping Cart -->
<script>
	function getDetailCnoteJne(idx) {
		$("#resultAwb").hide();
		//$("#detailCnote").show();
        $("#detailCnote").html("<h3>Mohon menunggu..</h3>");
		
		var id = idx.id;
		$.ajax({
            url: All.get_url("trans/conote/track/") +"/"+id,
            type: 'GET',
			//dataType: 'json',
            success:
            function(data){
            	
                
                	$("#detailCnote").html(null);
                    $("#detailCnote").html(data);
               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	}
	function getDetailCnote(idx) {
		$("#resultAwb").hide();
		//$("#detailCnote").show();
        $("#detailCnote").html("<h3>Mohon menunggu..</h3>");
		
		var id = idx.id;
		$.ajax({
            url: All.get_url("kgp/track") +"/"+id,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                
                	$("#detailCnote").html(null);
                    if(data.Status === "000") {
	                	var htmlx = "<table border='1' width='60%'><tr><td colspan=2>Status Pengiriman</td></tr>";
	                	
	                	if(data.DeliveryStatus == "Sukses") {
	                		htmlx += "<tr><td width=30%>Diterima oleh</td><td>"+data.ReceivedBy+"</td></tr>";
	                	    htmlx += "<tr><td>Tgl Terima</td><td>"+data.ReceivedDate+"</td></tr>";
	                	} else if(data.DeliveryStatus == "On Progress"){
	                		htmlx += "<tr><td>Status Kirim</td><td>Barang dalam proses pengiriman (KGP Cargo)</td></tr>";
	                	}
	                	htmlx += "<tr><td colspan=2><input type='button' value='<< Kembali' onclick='backForm()' /></td></tr>";
	                	htmlx += "</table>";
	                	$("#detailCnote").html(htmlx);
                    } else {
                    	var htmlx = "<table border='1' width='60%'><tr><td>Status Pengiriman</td></tr>";
	                	htmlx += "<tr><td>Transaksi Cnote "+id+" tidak ada.. </td></tr>";
	                	htmlx += "<tr><td><input type='button' value='<< Kembali' onclick='backForm()' /></td></tr>";
	                	htmlx += "</table>";
	                	$("#detailCnote").html(htmlx);
                    }
               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	}
	
	function backForm() {
		$("#detailCnote").html(null);
		$("#resultAwb").show();
	}
</script>

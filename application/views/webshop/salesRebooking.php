<style xmlns="http://www.w3.org/1999/html">
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        top: 5px;
        width: 150px;
        height: 300px;
    }

</style>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU&libraries=places&callback=initMap">
</script>
<script>

    function goBack() {
        window.history.back(-2);
    }

    function convertToRupiah(angka){
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }

    function initMap() {

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        var options = {
            types: ['geocode'],
            componentRestrictions: {country: "id"}
        };

        var input = document.getElementById('autocomplete');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        //option alamat autocomplete

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: {lat: -6.2400257, lng: 106.8320919}
        });
        directionsDisplay.setMap(map);

        var onChangeHandler = function () {

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };

        document.getElementById('stockistref2').addEventListener('change', onChangeHandler);
        document.getElementById('autocomplete').addEventListener('change', onChangeHandler);

    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
            origin: document.getElementById('stockistref2').value,
            destination: document.getElementById('autocomplete').value,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);

                var a= document.getElementById('stockistref2').value;
                var b= document.getElementById('autocomplete').value;

                cek_jarak(a,b);

            }

        });
    }

    function cek_metode(){

        var res = document.getElementById('metode').value;
        var metode = res.substring(0, 1);
        var harga= res.substring(1);

        var alamat= document.getElementById('n_alamat').value;
        document.getElementById('alamat').value= alamat;

        //console.log(harga);
        if(metode != '#'){
            post_book();
        }else{
            return false;
        }

    }

    function cek_jarak(A,B){
        var origin = A,
            destination = B,
            service = new google.maps.DistanceMatrixService();

        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                avoidHighways: false,
                avoidTolls: false
            },
            callback
        );

        function callback(response, status) {
            if(status=="OK") {
                var str=response.rows[0].elements[0].distance.text;
                var res = str.replace("km", "");
                geocodeAddress1(res); //call fungsi mendapatkan koordinat

                //console.log(res);
            } else {
                alert("Error: " + status);
                //location.reload();
            }
        }

    }

    function geocodeAddress1(dist) {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('autocomplete').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                var Xa = results[0].geometry.location.lat();
                var Ya = results[0].geometry.location.lng();

                document.getElementById('latlong2').value=Xa+","+Ya;
                document.getElementById('lat_dest').value=Xa;
                document.getElementById('long_dest').value=Ya;

                geocodeAddress2(dist,Xa,Ya); //call fungsi mendapatkan koordinat lokasi akhir
                //console.log(dist,Xa,Ya);
            } else {
                alert('Lokasi Awal was not successful for the following reason: ' + status);
            }
        });
    }

    function geocodeAddress2(dist,X1,Y1) {
        $("#metode").empty();
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('stockistref2').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {

                var X2 = results[0].geometry.location.lat();
                var Y2 = results[0].geometry.location.lng();

                document.getElementById("km").innerHTML = "Jarak Pengiriman "+dist+"km";

                if(dist < 40){
                    price_send(X1,Y1,X2,Y2); //call fungsi data gojek
                    //tes_api();
                }else{
                    var x = document.getElementById("metode");
                    var option = document.createElement("option");
                    option.text = "- Pilih Metode -";
                    x.add(option, x[0]);

                    var option1 = document.createElement("option");
                    option1.text = "Instant - Jarak > 40km";
                    x.add(option1, x[1]);

                    var option2 = document.createElement("option");
                    option2.text = "Same Day - Jarak > 40km";
                    x.add(option2, x[2]);
                }

                //price_send(X1,Y1,X2,Y2);

            } else {
                alert('Lokasi Akhir was not successful for the following reason: ' + status);
            }
        });
    }

    function price_send(x1,y1,x2,y2){
        var awal= x1+","+y1;
        var akhir= x2+","+y2;

        $.ajax({
            url:"https://www.k-net.co.id/cek_price",
            type: "POST",
            dataType:"json",
            data:{
                lat:awal,
                long:akhir
            },
            success: function (result) {

                console.log(result);


                var hours = new Date().getHours();
                var minute = new Date().getMinutes();
                var time= hours+":"+minute;

                var x = document.getElementById("metode");
                var option = document.createElement("option");
                option.text = "- Pilih Metode -";
                x.add(option, x[0]);

                var service_instant= result.Instant.serviceable;
                var metode_instant=result.Instant.shipment_method_description;
                var instant_hours=metode_instant.substring(0,7);

                if(service_instant == true) {
                    var dist_instant= result.Instant.distance;
                    var price_instant= result.Instant.price.total_price;
                    var rupiah1= convertToRupiah(price_instant);

                    var x = document.getElementById("metode");
                    var option = document.createElement("option");
                    option.value= "1-"+rupiah1;
                    option.text = "Instant "+instant_hours+" Rp"+ rupiah1;
                    x.add(option, x[1]);

                    /* if (hours >= 8 && hours <= 17) {
                     option.value= "1-"+rupiah1;
                     option.text = "Instant - Rp"+ rupiah1;
                     x.add(option, x[1]);
                     }else{
                     option.value= "#";
                     option.text = "Instant - TUTUP (Jam Pesanan: 10 Pagi - 4 Sore)";
                     x.add(option, x[1]);
                     }*/

                }else{
                    var x = document.getElementById("metode");
                    var option = document.createElement("option");
                    option.value= "#";
                    option.text = "Instant - "+metode_instant;
                    x.add(option, x[1]);
                }

                var service_sameday= result.SameDay.serviceable;
                var metode_sameday=result.SameDay.shipment_method_description;
                var sameday_hours=metode_sameday.substring(0,7);

                if(service_sameday == true) {
                    var dist_sameday = result.SameDay.distance;
                    var price_sameday = result.SameDay.price.total_price;

                    var rupiah1 = convertToRupiah(price_sameday);

                    var x = document.getElementById("metode");
                    var option = document.createElement("option");

                    option.value= "2-"+rupiah1;
                    option.text = "SameDay "+sameday_hours+" Rp"+ rupiah1;
                    x.add(option, x[2]);
                    /*if (hours >= 8 && hours <= 14) {
                     option.value= "2-"+rupiah1;
                     option.text = "Same Day - Rp"+ rupiah1;
                     x.add(option, x[2]);
                     }else{
                     option.value= "#";
                     option.text = "Same Day - TUTUP (Jam Pesanan: 10 Pagi - 3 Sore)";
                     x.add(option, x[2]);
                     }*/

                }else{
                    var x = document.getElementById("metode");
                    var option = document.createElement("option");
                    option.value= "#";
                    option.text = "SameDay "+ metode_sameday;
                    x.add(option, x[2]);
                }

            }
        });
    }

    function post_book(){
        var metode=document.getElementById('metode').value;
        var cmetode=metode.substring(0,1);
        var harga= metode.substring(2);
        var price = harga.replace(".", "");

        var cnama_penerima=document.getElementById('nama_penerima').value;
        var cnotlp=document.getElementById('notlp').value;
        var calamat=document.getElementById('alamat').value;
        var coord=document.getElementById('latlong2').value;
        var x = coord.split(",", 1);
        var y = coord.split(",", 2);
        var calamat= calamat.replace(","," ");

        if(cmetode == 1){
            cmetode="Instant";
        }else{
            cmetode="SameDay";
        }
        /*penerima:cnama_penerima,tlp:cnotlp,alamat:calamat,coordX:x,coordY:y*/
        var nproduk="";

        console.log("metode:"+cmetode+",penerima:"+cnama_penerima+",tlp:"+cnotlp+",alamat:"+calamat+",coord:"+coord+",harga:"+price+",item:"+nproduk);

        $.ajax({
            //url:"https://www.k-net.co.id/make_booking",
            url:"#",
            type: "POST",
            dataType:"json",
            data:{
                metode:cmetode,penerima:cnama_penerima,tlp:cnotlp,alamat:calamat,coord:coord,harga:price,item:nproduk
            },
            success: function (result) {
                console.log(result);

                //var orderNo=result.orderNo;
                //console.log(orderNo);
                //document.getElementById("no_order").value=orderNo;
                // status_book();
            }
        });
    }

</script>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Sales&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Cek Transaksi K-net&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->
<div class="row vpullset4">
    <div class="container">
        <div class="col-md-12">
            <div class="col-lg-12 col-xs-12 cart_header">
                <div class="col-lg-12">
                    Order Ulang GO-SEND
                </div>
            </div>

            <?php
            /* if($cekTrans['response'] != "true"){
                 echo "Data Tidak Ada";
             }else {*/
            ?>

            <form id="myForm" name="myForm" method="post" action="<?php echo base_url()."sales/save_rebooking"?>">
            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                <div class="col-lg-3 col-xs-3">
                    ID Member
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID(); ?>
                </div>
                <div class="col-lg-3 col-xs-3">
                    Nama Member
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUsername(); ?>
                </div>
                <div class="col-lg-3 col-xs-3">
                    Order No
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo $orderno?>
                </div>
                <div class="col-lg-3 col-xs-3">
                    Order Diterima
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo $orderdate?>
                </div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                </div>
                <div class="col-lg-9 col-xs-9">
                    <input class="form-list" class="uppercase" type="text" name="nama_penerima" id="nama_penerima" maxlength="25" style="width:40%"/>
                </div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    <span>No. Telp / Hp<label><font color="red">&nbsp;*</font></label></span>
                </div>
                <div class="col-lg-9 col-xs-9">
                    <input class="form-list" class="numeric-input" type="text" name="notlp" id="notlp" maxlength="14" style="width:40%"/>
                </div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    <span>Alamat Penerima<label><font color="red">&nbsp;*</font></label></span>
                </div>
                <div class="col-lg-9 col-xs-9">
                    <textarea class="form-list" class="numeric-input" name="n_alamat" id="n_alamat"  style="width:40%"/></textarea></div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    <span>Masukkan Tujuan Pengiriman<label><font color="red">&nbsp;*</font></label></span>
                </div>
                <div class="col-lg-9 col-xs-9" >
                    <input class="form-list" type="text" placeholder="Enter your address" id="autocomplete" name="autocomplete" value="" style="width:67%"/>
                </div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    <span>Metode Pengiriman <label><font color="red">&nbsp;*</font></label></span>
                </div>
                <div class="col-lg-9 col-xs-9">
                    <select class="form-list" name="metode" id="metode" onchange="cek_metode()" style="width:30%">
                    </select>
                </div>

                <br>

                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    <span id="km"></span><span><label><font color="red">&nbsp;*</font></label></span>
                </div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9" style="width:50%" id="map"></div>

                <!--break-->
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    &nbsp;
                </div>
                <!--break-->

                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    <input type="hidden" id="stockistref2" name="stockistref2" value="K-link Tower" readonly>
                    <input type="hidden" id="latlong2" name="latlong2" value="" readonly>
                    <input type="hidden" id="lat_dest" name="lat_dest" value="" readonly>
                    <input type="hidden" id="long_dest" name="long_dest" value="" readonly>
                    <input type="hidden" id="alamat" name="alamat" value="" readonly>
                    <input type="hidden" id="no_order"  name="no_order"  value="<?php echo $orderno?>" readonly>
                </div>


                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-3 col-xs-3">
                    &nbsp;
                </div>
                <div class="col-lg-9 col-xs-9">
                    <input type="submit" value="Kirim Order" name="send_order" id="send_order"/>
                </div>
            </form>
            </div>

        </div>
        <div>

            <input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="goBack();" name="submit" id="submit"/>
        </div><br />
    </div>
</div>


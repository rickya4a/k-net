<div class="row vpullset4 voffset4">
	<div class="container">
     <!--right-->
    	<div class="col-md-9 col-sm-12">
            <!-- Personal Particulars--> 
            <div class="user-info" id="personalParticular">
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nama Lengkap </p>
                    <span><?php echo getUsername(); ?></span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nomor ID Member </p>
                    <span><?php echo getUserID	(); ?></span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Tanggal Lahir</p>
                    <span><?php echo getBirthDate(); ?></span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Alamat Lengkap</p>
                    <span>
                    	<?php
                    	  echo getUserAddress();
                    	?>
                    </span>
                </div>
                
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Email</p>
                    <span><?php echo getUserEmail(); ?></span>
                </div> 
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nomor ID Sponsor</p>
                    <span><?php echo getSponsorID(); ?></span>
                </div>          
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nama Sponsor</p>
                    <span><?php echo getSponsorName(); ?></span>
                </div>
                        
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nomor Virtual Account</p>
                    <!--<span>88146 -</span>-->
                    <span><?php echo getUserNovac(); ?></span>
                </div>
                
                <div class="col-md-6 col-xs-12 user-info-wrapper">
                	<p>No Rekening</p>
                	<?php
                	$sr = $this->session->userdata("store_info");
					?>
                    <span>
                    	<?php
                    	  echo $sr[0]->bankaccno;
                    	?>
                    </span>
                </div>  
                <div class="col-md-6 col-xs-12 user-info-wrapper">
                	<p>Rekening A/N</p>
                	<?php
                	   
                	?>
                    <span>
                    	<?php
                    	  echo $sr[0]->bankaccnm;
                    	?>
                    </span>
                </div>      
				<div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nomor KTP</p>
                    <!--<span>88146 -</span>-->
                    <span><?php echo getUserIdno();; ?></span>
                </div>
				
            </div>
            <!-- Personal Particulars-->
        </div>
    </div>
 </div>
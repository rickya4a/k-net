<!-- Breadcrumb -->

<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png'); ?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">
            &nbsp;
            Shopping&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Pengiriman&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
    <div class="container">
        <?php
        $cart_check = $this -> cart -> contents();
        if(empty($cart_check)) {
            echo "<h3>Keranjang Belanja anda masih kosong,silahkan klik menu Beranda untuk melihat produk</h3>";
        }
        else {
        ?>
        <div id="formCart1">
            <?php
            $usr = getUserID();
            if($usr == "IDSPAAA66834") {
            ?>
            <form id="formCart" action="<?php echo base_url('cart/checkout/sgo'); ?>" method="POST" onsubmit="return Shopping.validateCheckOut()">
                <?php
                } else {
                ?>
                <form id="formCart" action="<?php echo base_url('cart/checkout'); ?>" method="POST" onsubmit="return Shopping.validateCheckOut()">
                    <?php
                    }
                    ?>
                    <!-- Delivery Options-->
                    <br />
                    <div class="col-md-6 col-xs-12">
                        <span class="title_header"> <i class="fa fa-truck"></i> Tipe Pengiriman Vera </span>

                        <!--  Options 1-->
                        <div class="col-md-12 col-xs-12 nopadding voffset4">
                            <?php
                            $personal_info = $this->session->userdata('personal_info');
                            if($personal_info['delivery'] == "1")
                            {
                            ?>
                            <!--  Options 1 header-->
                            <div class="col-md-12 col-xs-12 delivery_header">
                                <input name="delivery" checked="checked" id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/>
                                Diambil di Stockist
                            </div>
                            <!--  Options 1 header-->

                            <!--  Options 1 detail-->
                            <div class="col-md-12 col-xs-12 diambil">
                                <p class="p2 nopadding">
                                    Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
                                    <br/>
                                    <br/>
                                </p>
                                <div class="register-top-grid">

                                    <!--  Options 1 area-->

                                    <div>
                                        <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
                                            <option value="">--Select Here--</option>
                                            <?php
                                            //print_r($show_provinsi);
                                            foreach($show_provinsi['arrayData'] as $row){
                                                if($row->kode_provinsi == $personal_info['provinsi']) {
                                                    echo "<option value=\"".$row->kode_provinsi."\" selected=\"selected\">".strtoupper($row->provinsi)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";
                                                }
                                            }

                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="<?php echo $personal_info['nama_provinsi'] ?>" />
                                    </div>
                                    <!--  Options 1 area-->

                                    <!--  Options 1 lokasi-->
                                    <div>
                                        <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">
                                            <option value="">--Select Here--</option>
                                            <?php
                                            foreach($shipping['kota'] as $row){
                                                if($row->kode_kabupaten == $personal_info['kota']) {
                                                    echo "<option value=\"".$row->kode_kabupaten."\" selected=\"selected\">".strtoupper($row->kabupaten)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->kode_kabupaten."\">".strtoupper($row->kabupaten)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_kota1" name="nama_kota1" value="<?php echo $personal_info['nama_kota'] ?>" />
                                        <!--<input type="text" name="kota" id="kota"/>-->
                                    </div>
                                    <div>
                                        <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                        <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                        <select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >
                                            <option value="">--Select Here--</option>
                                            <?php
                                            foreach($shipping['kecamatan'] as $row){
                                                if($row->kode_kecamatan == $personal_info['kecamatan']) {
                                                    echo "<option value=\"".$row->kode_kecamatan."\" selected=\"selected\">".strtoupper($row->kecamatan)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->kode_kecamatan."\">".strtoupper($row->kecamatan)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
                                    </div>
                                    <div>
                                        <span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestkX(this,'#pricecode')">
                                            <option value="">--Select Here--</option>
                                            <?php
                                            foreach($shipping['listStk'] as $row){
                                                if($row->loccd == $personal_info['stockist']) {
                                                    echo "<option value=\"".$row->loccd."\" selected=\"selected\">".strtoupper($row->fullnm)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->loccd."\">".strtoupper($row->fullnm)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_stockist" name="nama_stockist" value="<?php echo $personal_info['nama_stockist'] ?>" />
                                    </div>
                                    <div class="clearfix"> </div>
                                    <!--  Options 1 lokasi-->
                                </div>
                            </div>
                            <!--  Options 1 detail-->
                        </div>
                        <!--  Options 1-->
                        <!--  Options 2-->
                        <div class="col-md-12 col-xs-12 nopadding voffset4">
                            <!--  Options 2 header-->
                            <div class="col-md-12 col-xs-12 delivery_header">
                                <input name="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/>
                                Dikirim ke Alamat (Pilihan ini belum direkomendasikan/masih dalam pengembangan)
                            </div>
                            <!--  Options 2 header-->

                            <!--  Options 2 delivery address-->
                            <div class="col-md-12 col-xs-12 vpullset3 dikirim" style="display: none;">
                                <!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->
                                <div class="register-top-grid">
                                    <div>
                                        <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                                        <input class="uppercase" type="text" name="nama_penerima" id="nama_penerima" value="" />
                                    </div>

                                    <div>
                                        <span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
                                        <input class="numeric-input" type="text" name="notlp" id="notlp" value="" maxlength="14" />
                                    </div>

                                    <div style="width:98%">
                                        <span>Email Address</span>
                                        <input type="text" name="email" id="email" value="" />
                                    </div>

                                    <div style="width:98%">
                                        <span>Alamat<label><font color="red">&nbsp;*</font></label></span>
                                        <textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"></textarea>
                                        <input type="hidden" id="destination_address" name="destination_address" value="" />
                                    </div>

                                    <div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
                                        Pilih Stockist Terdekat Dengan Alamat Tujuan
                                    </div>

                                    <div>
                                        <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.show_kota(this,'#kota')" >
                                            <option value="select">--Select Here--</option>
                                            <?php
                                            foreach ($show_provinsi['arrayData'] as $row) {
                                                echo "<option value=\"" . $row -> kode_provinsi . "\">" . strtoupper($row -> provinsi) . "</option>";
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
                                    </div>
                                    <div>
                                        <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                        <!-- <select class="form-list" name="kota" id="kota" onchange="Shopping.show_kecamatan(this,'#kecamatan')"> -->
                                        <select class="form-list" name="kota" id="kota" onchange="Shopping.show_kecamatan(this,'#kecamatan')">
                                            <option value="">--Select Here--</option>

                                        </select>
                                        <input type="hidden" id="nama_kota" name="nama_kota" value="" />
                                        <!--<input type="text" name="kota" id="kota"/>-->
                                    </div>
                                    <div>
                                        <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                        <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                        <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >
                                            <option value="">--Select Here--</option>

                                        </select>
                                        <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
                                    </div>

                                    <!--<div>
								<span>Pilih Area Stockist Referensi<font color="red">&nbsp;*</font></span>
								<select class="form-list" name="stkarea1" onchange="Shopping.show_stk(this,'#stockistref')" id="stkarea1">
								<option value="">--Select here--</option>
								<?php
                                    foreach($show_state as $dta)
                                    {

                                        echo "<option value=\"$dta->st_id\">$dta->description</option>";

                                    }
                                    ?>
								</select>
								<input type="hidden" id="nama_stockistr1area" name="nama_stockistr1area" value="" />
								</div>-->
                                    <div>
                                        <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                        <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">
                                            <option value="">--Select here--</option>
                                        </select>
                                        <input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="" />
                                        <input type="hidden" id="sender_address" name="sender_address" value="" />
                                        <input type="hidden" id="jne_branch" name="jne_branch" value="" />
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <!--  Options 2 delivery address-->
                        </div>
                        <?php
                        } else {
                        ?>
                        <!--  Options 1 header-->
                        <div class="col-md-12 col-xs-12 delivery_header">
                            <input name="delivery"  id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/>
                            Diambil di Stockist
                        </div>
                        <!--  Options 1 header-->

                        <!--  Options 1 detail-->
                        <div class="col-md-12 col-xs-12 diambil" style="display: none;">
                            <p class="p2 nopadding">
                                Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
                                <br/>
                                <br/>
                            </p>
                            <div class="register-top-grid">

                                <!--  Options 1 area-->

                                <div>
                                    <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
                                        <option value="">--Select Here--</option>
                                        <?php
                                        //print_r($show_provinsi);
                                        foreach($show_provinsi['arrayData'] as $row){

                                            echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";

                                        }

                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="<?php echo $personal_info['nama_provinsi'] ?>" />
                                </div>
                                <!--  Options 1 area-->

                                <!--  Options 1 lokasi-->
                                <div>
                                    <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">

                                    </select>
                                    <input type="hidden" id="nama_kota1" name="nama_kota1" value="<?php echo $personal_info['nama_kota'] ?>" />
                                    <!--<input type="text" name="kota" id="kota"/>-->
                                </div>
                                <div>
                                    <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                    <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                    <select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >

                                    </select>
                                    <input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
                                </div>
                                <div>
                                    <span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestkX(this,'#pricecode')">

                                    </select>
                                    <input type="hidden" id="nama_stockist" name="nama_stockist" value="<?php echo $personal_info['nama_stockist'] ?>" />
                                </div>
                                <div class="clearfix"> </div>
                                <!--  Options 1 lokasi-->
                            </div>
                        </div>
                        <!--  Options 1 detail-->
                    </div>
                <!--  Options 1-->

                <!--  Options 2-->
                    <div class="col-md-12 col-xs-12 nopadding voffset4">
                        <!--  Options 2 header-->
                        <div class="col-md-12 col-xs-12 delivery_header">
                            <input name="delivery" checked="checked" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/>
                            Dikirim ke Alamat
                        </div>
                        <!--  Options 2 header-->

                        <!--  Options 2 delivery address-->
                        <div class="col-md-12 col-xs-12 vpullset3 dikirim" >
                            <!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

                            <div class="register-top-grid">
                                <div>
                                    <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                                    <input class="uppercase" type="text" name="nama_penerima" id="nama_penerima" value="<?php echo $personal_info['nama_penerima']; ?>" />
                                </div>

                                <div>
                                    <span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
                                    <input class="numeric-input" type="text" name="notlp" id="notlp" maxlength="14" value="<?php echo $personal_info['notlp']; ?>" />
                                </div>
                                <div style="width:98%">
                                    <span>Email Address</span>
                                    <input type="text" name="email" id="email" value="<?php echo $personal_info['email']; ?>" />
                                </div>

                                <div style="width:98%">
                                    <span>Alamat<label><font color="red">&nbsp;*</font></label></span>
                                    <textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"><?php echo $personal_info['alamat']; ?></textarea>
                                    <input type="hidden" id="destination_address" name="destination_address" value="<?php echo $this -> session -> userdata('destination_address'); ?>" />
                                </div>

                                <div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
                                    Pilih Stockist Terdekat Dengan Alamat Tujuan
                                </div>
                                <div>
                                    <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.show_kota(this,'#kota')" >
                                        <option value="select">--Select Here--</option>
                                        <?php
                                        foreach ($show_provinsi['arrayData'] as $row) {
                                            if ($row -> kode_provinsi == $personal_info['provinsi']) {
                                                echo "<option value=\"" . $row -> kode_provinsi . "\" selected=\"selected\">" . strtoupper($row -> provinsi) . "</option>";
                                            } else {
                                                echo "<option value=\"" . $row -> kode_provinsi . "\">" . strtoupper($row -> provinsi) . "</option>";
                                            }

                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="<?php echo $personal_info['nama_provinsi']; ?>" />
                                </div>
                                <div>
                                    <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="kota" id="kota" onchange="Shopping.show_kecamatan(this,'#kecamatan')">
                                        <option value="">--Select Here--</option>
                                        <?php
                                        foreach ($shipping['kota'] as $row) {
                                            if ($row -> kode_kabupaten == $personal_info['kota']) {
                                                echo "<option value=\"" . $row -> kode_kabupaten . "\" selected=\"selected\">" . strtoupper($row -> kabupaten) . "</option>";
                                            } else {
                                                echo "<option value=\"" . $row -> kode_kabupaten . "\">" . strtoupper($row -> kabupaten) . "</option>";
                                            }

                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_kota" name="nama_kota" value="<?php echo $personal_info['nama_kota']; ?>" />
                                    <!--<input type="text" name="kota" id="kota"/>-->
                                </div>
                                <div>
                                    <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                    <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                    <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >
                                        <option value="">--Select Here--</option>
                                        <?php
                                        foreach ($shipping['kecamatan'] as $row) {
                                            if ($row -> kode_kecamatan == $personal_info['kecamatan']) {
                                                echo "<option value=\"" . $row -> kode_kecamatan . "\" selected=\"selected\">" . strtoupper($row -> kecamatan) . "</option>";
                                            } else {
                                                echo "<option value=\"" . $row -> kode_kecamatan . "\">" . strtoupper($row -> kecamatan) . "</option>";
                                            }

                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="<?php echo $personal_info['nama_kecamatan']; ?>" />
                                </div>

                                <!-- <div>
							<span>Pilih Area Stockist Referensi<font color="red">&nbsp;*</font></span>
							<select class="form-list" name="stkarea1" onchange="Shopping.show_stk(this,'#stockistref')" id="stkarea1">
							<option value="">--Select here--</option>
							<?php
                                foreach($show_state as $dta)
                                {
                                    if($dta->st_id == $personal_info['stkarea']) {
                                        echo "<option value=\"$dta->st_id\" selected=\"selected\">$dta->description</option>";
                                    } else {
                                        echo "<option value=\"$dta->st_id\">$dta->description</option>";
                                    }
                                }
                                ?>
							</select>
							<input type="hidden" id="nama_stockistr1area" name="nama_stockistr1area" value="<?php echo $personal_info['nama_stkarea']; ?>" />
							</div> -->

                                <div>
                                    <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                    <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">
                                        <option value="">--Select here--</option>
                                        <?php

                                        //foreach ($show_stk_by_area['arrayData'] as $dtax) {
                                        foreach($shipping['listStk'] as $dtax) {
                                            if ($dtax -> loccd == $personal_info['stockist']) {
                                                echo "<option value=\"$dtax->loccd\" selected=\"selected\">$dtax->loccd - $dtax->fullnm</option>";
                                            } else {
                                                echo "<option value=\"$dtax->loccd\">$dtax->loccd - $dtax->fullnm</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="<?php echo $personal_info['nama_stockist']; ?>" />
                                    <input type="hidden" id="sender_address" name="sender_address" value="<?php echo $this -> session -> userdata('sender_address'); ?>" />
                                    <input type="hidden" id="jne_branch" name="jne_branch" value="<?php echo $this -> session -> userdata('jne_branch'); ?>" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!--  Options 2 delivery address-->
                    </div>
                <?php
                }
                ?>
                    <!--  Options 2-->
        </div>

        <div id="listCartPrd">
            <!-- Order Details-->
            <div class="col-md-6 col-xs-12">
                <span class="title_header"> <i class="fa fa-check-square-o"></i> Detail Pembelanjaan </span>
                <div class="register-top-grid">
                    <?php
                    $personal_info = $this->session->userdata('personal_info');
                    ?>
                    <div style="width:100%;">
                        <span>ID Member<label>*</label></span>
                        <input type="text" class="required uppercase" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo getUserID(); ?>"/>
                        <input type="button" id="checkIDMember" value="Check ID" class="btn1 btn2 btn-primary1" onclick="Shopping.getMemberNameByID(this.form.idmemberx, '#membername')" />
                        <input type="hidden" id="id_lp" name="id_lp" value="" />
                    </div>
                    <div>
                        <span>Nama Member<label>*</label></span>
                        <input readonly="readonly" class="required uppercase" type="text" name="membername" id="membername" value="<?php echo getUsername(); ?>"/>
                    </div>
                    <div>
                        <span>Periode Bonus<label>*</label></span>
                        <select name="bnsperiod" id="bnsperiod" class="form-list">
                            <?php

                            /*for($i=0; $i < $dta['rangeperiod']; $i++) {

                             }*/
                            
                            /* //===========START OFF BY HILAL @2019-03-23===================
                            $xx = $bns['arrayData'];
                            for ($i = 0; $i <= $xx[0] -> rangeperiod; $i++) {
                                $x = addRangeOneMonth($xx[0] -> lastperiod, $i);
                                //echo $x . "<br />";
                                $date = str_replace('/', '-', $x);
                                $period = date('F-Y', strtotime($date));
                                echo "<option value=\"$x\">$period</option>";
                            }
                            //print_r($bns['arrayData']);
                            */ //===========END OFF BY HILAL @2019-03-23===================
                            
                            //===========START ON BY HILAL @2019-03-23===================
							$c_bns = count($bns_hilal);
  
							for ($i=0; $i < $c_bns; $i++) { 
								$date = str_replace('/', '-', $bns_hilal[$i]);
                            	$period = date('F-Y', strtotime($date));
                            	echo "<option value=\"$date\">$period</option>";
							  }
							//===========END ON BY HILAL @2019-03-23===================
									  
                            ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <br />
                <?php


                // If cart is empty, this will show below message.
                if (empty($cart_check)) {
                    echo '<br/>To add products to your shopping cart click on "Add to Cart" Button';
                }
                ?>
                <!-- header-->
                <div class="col-md-12 col-xs-12 cart_header">
                    <div class="col-md-3 col-xs-3">
                        Product
                    </div>
                    <div class="col-md-1 col-xs-1" align="right">
                        Qty
                    </div>

                    <div class="col-md-1 col-xs-1" align="right">
                        BV
                    </div>
                    <div class="col-md-2 col-xs-2" align="right">
                        Price
                    </div>
                    <div class="col-md-2 col-xs-2" align="right">
                        Amount
                    </div>
                    <div class="col-md-1 col-xs-1" align="right">
                        Berat
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <i class="fa fa-trash-o"></i>
                    </div>
                </div>
                <!-- header-->

                <!-- product item-->
                <?php
                $pricecode = $this->session->userdata('pricecode');
                if($pricecode == "12W3")
                {
                    $n = 1;
                    foreach($cart as $dt)
                    {
                        $rowid = $dt['rowid'];
                        $prdcd = $dt['id'];
                        $prdnm = $dt['name'];
                        $westPrice = $dt['west_price'];
                        $qty = $dt['qty'];
                        $bv = $dt['bv'];
                        $eastPrice = $dt['east_price'];

                        ?>
                        <div id="<?php echo $n; ?>">

                            <div class="col-md-12 col-xs-12 order_wrap">
                                <div class="col-md-3 col-xs-3 order_desc">
                                    <a href="#">
                                        <input type="hidden" value="<?php echo $prdnm; ?>" name="prdnm[]" id="<?php echo "prdnm" . $n; ?>" readonly="yes"/>
                                        <h3><?php echo $prdnm; ?></h3></a>
                                    <!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <input class="orderdetails-box" onkeyup="Shopping.setNewPrice(<?php echo $n; ?>)" type="text" value="<?php echo $qty; ?>" name="qty[]" id="<?php echo "qty" . $n; ?>"/>
                                </div>
                                <div class="col-md-1 col-xs-1" align="right">
                                    <?php echo $bv; ?>
                                    <input class="orderdetails-box" type="hidden" value="<?php echo $bv; ?>" name="bv[]" id="<?php echo "bv" . $n; ?>" readonly="yes"/>
                                </div>
                                <div class="col-md-2 col-xs-2 westP" align="right">
                                    <?php echo "" . number_format($westPrice, 0, ",", ".") . ""; ?>
                                    <input class="orderdetails-box" type="hidden" value="<?php echo $westPrice; ?>" name="westPrice[]" id="<?php echo "westPrice" . $n; ?>" readonly="yes"/>
                                </div>
                                <div class="col-md-3 col-xs-3 westP" align="right" id="<?php echo "divSubTotWestPrice".$n ?>">
                                    <?php echo "" . number_format($dt['subtotal_west_price'], 0, ",", ".") . ""; ?>
                                </div>
                                <div class="col-md-2 col-xs-2 eastP" align="right" style="display: none;">
                                    <?php echo "" . number_format($eastPrice, 0, ",", ".") . ""; ?>
                                    <input class="orderdetails-box" type="hidden" value="<?php echo $eastPrice; ?>" name="eastPrice[]" id="<?php echo "eastPrice" . $n; ?>" readonly="yes"/>
                                </div>
                                <div class="col-md-3 col-xs-3 eastP" align="right" id="<?php echo "divSubTotEastPrice".$n ?>" style="display: none;" >
                                    <?php echo "" . number_format($dt['subtotal_east_price'], 0, ",", ",") . ""; ?>
                                </div>
                                <div class="col-md-1 col-xs-1 text-center">
                                    <input type="hidden" id="<?php echo "rowid" . $n; ?>" value="<?php echo $rowid; ?>" name="rowid[]"/>
                                    <a href="#" id="<?php echo $rowid; ?>" onclick="Shopping.delete_rowCart(<?php echo $n; ?>)"><i class="fa fa-minus-circle"></i></a>
                                </div>
                                <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_price']; ?>" name="amountWest[]" id="<?php echo "amountWest" . $n; ?>" readonly="yes"/>
                                <input class="amtE orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_price']; ?>" name="amountEast[]" id="<?php echo "amountEast" . $n; ?>" readonly="yes"/>

                                <?php $sub_totalbv = $qty * $bv; ?>
                                <input class="amtE orderdetails-box" type="hidden" value="<?php echo $sub_totalbv; ?>" name="amountBV[]" id="<?php echo "amountBV".$n;?>" readonly="yes"/>
                            </div>

                        </div>
                        <!-- product item-->
                        <?php
                        $n++;
                    }
                    ?>
                    <!-- shipping cost
                    <div class="col-md-12 col-xs-12 order_wrap">
                    <div class="col-md-3 col-xs-3 order_desc">
                    <h3>Shipping Weight</h3>
                    <p>Stookist Jakarta - Kebayoran Baru</p>
                    </div>
                    <div class="col-md-2 col-xs-2">3 Kg</div>
                    <div class="col-md-3 col-xs-3"></div>
                    <div class="col-md-3 col-xs-3" align="right">10.000.000</div>
                    </div>  -->
                    <!-- shipping cost-->

                    <!-- subtotal -->
                    <div class="col-md-12 col-xs-12 order_total_wrap westP">
                        <div class="col-md-3 col-xs-3">
                            SUBTOTAL
                        </div>
                        <div class="col-md-2 col-xs-2" id="divTotQtyWest">
                            <?php echo $this -> cart -> total_items(); ?>
                        </div>
                        <!--<div class="col-md-3 col-xs-3">
                            &nbsp;
                        </div>-->
                        <div class="col-md-3 col-xs-3" id="divTotBVWest">
                            <?php echo $this->cart->total_bv();?>
                        </div>
                        <div class="col-md-1 col-xs-1">
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_bv(); ?>" name="totBv" id="totBv" readonly="yes"/>
                        </div>
                        <div class="col-md-2 col-xs-2"></div>
                        <div class="col-md-3 col-xs-3">
                            <input class="orderdetails-box" type="hidden" value="<?php //echo $this -> cart -> contents('subtotal_west_price'); ?>" name="totalWestPrice" id="totalWestPrice" readonly="yes"/>
                        </div>
                        <div class="col-md-3 col-xs-3" id="totAllWestPrice" align="right">
                            <?php echo "" . number_format($this -> cart -> total_west_price(), 0, ",", ","); ?>
                        </div>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_items(); ?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_west_price(); ?>" name="totalAmountWest" id="totalAmountWest" readonly="yes"/>
                    </div>
                    <div class="col-md-12 col-xs-12 order_total_wrap eastP " style="display: none;">
                        <div class="col-md-3 col-xs-3">
                            SUBTOTAL
                        </div>
                        <div class="col-md-2 col-xs-2" id="divTotQtyEast">
                            <?php echo $this -> cart -> total_items(); ?>
                        </div>
                        <!--<div class="col-md-3 col-xs-3">
                            &nbsp;
                        </div>-->
                        <div class="col-md-3 col-xs-3" id="divTotBVEast">
                            <?php echo $this->cart->total_bv();?>
                        </div>
                        <div class="col-md-1 col-xs-1 ">
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_bv(); ?>" name="totBv" id="totBv" readonly="yes"/>
                        </div>
                        <div class="col-md-2 col-xs-2"></div>
                        <div class="col-md-3 col-xs-3">
                            <input class="orderdetails-box" type="hidden" value="<?php //echo $this -> cart -> contents('subtotal_east_price'); ?>" name="totalEastPrice" id="totalEastPrice" readonly="yes"/>
                        </div>
                        <div class="col-md-3 col-xs-3" id="totAllEastPrice" align="right">
                            <?php echo "" . number_format($this -> cart -> total_east_price(), 0, ",", ","); ?>
                        </div>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_items(); ?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_east_price(); ?>" name="totalAmountEast" id="totalAmountEast" readonly="yes"/>
                    </div>
                    <!-- subtotal -->
                    <?php
                } else {

                    $n = 1;
                    foreach($cart as $dt)
                    {
                        $rowid = $dt['rowid'];
                        $prdcd = $dt['id'];
                        $prdnm = $dt['name'];
                        $westPrice = $dt['west_price'];
                        $qty = $dt['qty'];
                        $bv = $dt['bv'];
                        $eastPrice = $dt['east_price'];
                        ?>
                        <div id="<?php echo $n; ?>">

                            <div class="col-md-12 col-xs-12 order_wrap">
                                <div class="col-md-3 col-xs-3 order_desc">
                                    <a href="#">
                                        <input type="hidden" value="<?php echo $prdnm; ?>" name="prdnm[]" id="<?php echo "prdnm" . $n; ?>" readonly="yes"/>
                                        <h3><?php echo $prdnm; ?></h3></a>
                                    <!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <input class="orderdetails-box" onkeyup="Shopping.setNewPrice(<?php echo $n; ?>)" type="text" value="<?php echo $qty; ?>" name="qty[]" id="<?php echo "qty" . $n; ?>"/>
                                </div>
                                <div class="col-md-1 col-xs-1" align="right">
                                    <?php echo $bv; ?>
                                    <input class="orderdetails-box" type="hidden" value="<?php echo $bv; ?>" name="bv[]" id="<?php echo "bv" . $n; ?>" readonly="yes"/>
                                </div>
                                <div class="col-md-2 col-xs-2 westP" align="right" style="display: none;">
                                    <?php echo "" . number_format($westPrice, 0, ",", ".") . ""; ?>
                                    <input class="orderdetails-box" type="hidden" value="<?php echo $westPrice; ?>" name="westPrice[]" id="<?php echo "westPrice" . $n; ?>" readonly="yes"/>
                                </div>
                                <div class="col-md-3 col-xs-3 westP" align="right" id="<?php echo "divSubTotWestPrice".$n ?>" style="display: none;">
                                    <?php echo "" . number_format($dt['subtotal_west_price'], 0, ",", ".") . ""; ?>
                                </div>
                                <div class="col-md-2 col-xs-2 eastP" align="right" >
                                    <?php echo "" . number_format($eastPrice, 0, ",", ".") . ""; ?>
                                    <input class="orderdetails-box" type="hidden" value="<?php echo $eastPrice; ?>" name="eastPrice[]" id="<?php echo "eastPrice" . $n; ?>" readonly="yes"/>
                                </div>
                                <div class="col-md-3 col-xs-3 eastP" align="right" id="<?php echo "divSubTotEastPrice".$n ?>"  >
                                    <?php echo "" . number_format($dt['subtotal_east_price'], 0, ",", ",") . ""; ?>
                                </div>
                                <div class="col-md-1 col-xs-1 text-center">
                                    <input type="hidden" id="<?php echo "rowid" . $n; ?>" value="<?php echo $rowid; ?>" name="rowid[]"/>
                                    <a href="#" id="<?php echo $rowid; ?>" onclick="Shopping.delete_rowCart(<?php echo $n; ?>)"><i class="fa fa-minus-circle"></i></a>
                                </div>
                                <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_price']; ?>" name="amountWest[]" id="<?php echo "amountWest" . $n; ?>" readonly="yes"/>
                                <input class="amtE orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_price']; ?>" name="amountEast[]" id="<?php echo "amountEast" . $n; ?>" readonly="yes"/>
                                <?php $sub_totalbv = $qty * $bv; ?>
                                <input class="amtE orderdetails-box" type="hidden" value="<?php echo $sub_totalbv; ?>" name="amountBV[]" id="<?php echo "amountBV".$n;?>" readonly="yes"/>
                            </div>

                        </div>
                        <!-- product item-->
                        <?php
                        $n++;
                    }
                    ?>
                    <!-- shipping cost
                    <div class="col-md-12 col-xs-12 order_wrap">
                    <div class="col-md-3 col-xs-3 order_desc">
                    <h3>Shipping Weight</h3>
                    <p>Stookist Jakarta - Kebayoran Baru</p>
                    </div>
                    <div class="col-md-2 col-xs-2">3 Kg</div>
                    <div class="col-md-3 col-xs-3"></div>
                    <div class="col-md-3 col-xs-3" align="right">10.000.000</div>
                    </div>  -->
                    <!-- shipping cost-->

                    <!-- subtotal -->
                    <div class="col-md-12 col-xs-12 order_total_wrap westP" style="display: none;">
                        <div class="col-md-3 col-xs-3">
                            SUBTOTAL
                        </div>
                        <div class="col-md-2 col-xs-2" id="divTotQtyWest">
                            <?php echo $this -> cart -> total_items(); ?>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            &nbsp;
                        </div>
                        <div class="col-md-1 col-xs-1">
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_bv(); ?>" name="totBv" id="totBv" readonly="yes"/>
                        </div>
                        <div class="col-md-2 col-xs-2"></div>
                        <div class="col-md-3 col-xs-3">
                            <input class="orderdetails-box" type="hidden" value="<?php //echo $this -> cart -> contents('subtotal_west_price'); ?>" name="totalWestPrice" id="totalWestPrice" readonly="yes"/>
                        </div>
                        <div class="col-md-3 col-xs-3" id="totAllWestPrice" align="right">
                            <?php echo "" . number_format($this -> cart -> total_west_price(), 0, ",", ","); ?>
                        </div>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_items(); ?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_west_price(); ?>" name="totalAmountWest" id="totalAmountWest" readonly="yes"/>
                    </div>
                    <div class="col-md-12 col-xs-12 order_total_wrap eastP " >
                        <div class="col-md-3 col-xs-3">
                            SUBTOTAL
                        </div>
                        <div class="col-md-2 col-xs-2" id="divTotQtyEast">
                            <?php echo $this -> cart -> total_items(); ?>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            &nbsp;
                        </div>
                        <div class="col-md-1 col-xs-1 ">
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_bv(); ?>" name="totBv" id="totBv" readonly="yes"/>
                        </div>
                        <div class="col-md-2 col-xs-2"></div>
                        <div class="col-md-3 col-xs-3">
                            <input class="orderdetails-box" type="hidden" value="<?php //echo $this -> cart -> contents('subtotal_east_price'); ?>" name="totalEastPrice" id="totalEastPrice" readonly="yes"/>
                        </div>
                        <div class="col-md-3 col-xs-3" id="totAllEastPrice" align="right">
                            <?php echo "" . number_format($this -> cart -> total_east_price(), 0, ",", ","); ?>
                        </div>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_items(); ?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
                        <input class="orderdetails-box" type="hidden" value="<?php echo $this -> cart -> total_east_price(); ?>" name="totalAmountEast" id="totalAmountEast" readonly="yes"/>
                    </div>

                    <?php
                }
                ?>
                <!-- subtotal -->
                <div class="col-md-12 col-xs-12 order_add">
                    <a href="<?php echo "" . site_url('/shop/product') . ""; ?>"> <i class="fa fa-shopping-cart"></i> Tambahkan Lagi Keranjang Belanja Anda. </a>
                </div>
                <?php
                if (getUserID()=='IDSPAAA66834'){
                    ?>
                    <div class="col-md-12 col-xs-12 order_add">
                        <a href="<?php echo "".site_url('shop/product/')."";?>">
                            Estimasi Harga Kirim....
                        </a>
                    </div>
                    <?php
                }
                ?>
                <!-- subtotal -->
            </div>
            <!-- Order Details-->
        </div>

        <!-- next button-->
        <div class="col-md-12">

            <input type="hidden" name="pricecode" id="pricecode" value="<?php echo $this -> session -> userdata('pricecode'); ?>" />
            <!--<a href="payment.php" class="btn1 btn2 btn-primary1 pull-left"><span>Lanjutkan</span><i class="fa fa-arrow-right"></i></a>-->
            <!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" onclick="Shopping.checkout()" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />-->
            <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">
                Lanjutkan <i class="fa fa-arrow-right"></i>
            </button>
            <br />
        </div>
        <!-- next button-->
    </div>
    </form>
    <!-- Delivery Options-->
</div>
<!-- End formCart1-->
<div id="divCheckOut"></div>
<div id="afterPayment"></div>
<?php
}
?>
</div>
</div>
<!--Checkout Wrapper-->

<div class="row vpullset4">
    <div class="container">
        <?php
		    //print_r($detTrans);
            if(empty($detTrans['detail']) && empty($detTrans['header']))
            {
                echo "Tidak Ada Data";
            }else{
        ?>
        <div class="col-md-12">
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                   Hasil Pencarian Transaksi Member
                </div>            
            </div>
            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
            <?php 
                foreach($detTrans['header'] as $row){
                    if($row->status_vt_pay == '0'){
                        $status = "Pending";
                        $dateTrans = $row->status_vt_reject_dt;
                    }else{
                        $status = "Sukses";
                        $dateTrans = $row->status_vt_app_dt;
                    }
                    $tipe = $row->paytype;
                    $orderno = $row->orderno;
                    $idmember = $row->idmember;
                    $orderid = $row->token;
                    $payamt = $row->payamt;
                    $sccode = $row->sccode; 
                    $sentTo = $row->sentTo;
					
					if($sentTo == '1'){ //pengambilan ke stockist, munculkan security no
						$sentToDet = "(Security No. : $sccode )";
					}elseif($sentTo == '2'){ //pengambilan ke alamat, munculkan conote no
						$sentToDet = "(Conote No. : $sccode )";
					}  
                }
            ?>  
                <div class="col-lg-3 col-xs-3">
                    Status Transaksi  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo $status; ?> 
                </div>
                
                <div class="col-lg-3 col-xs-3">
                    Tipe Pembayaran  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo $tipe; ?> 
                </div>
                
                <div class="col-lg-3 col-xs-3">
                    ID Transaksi  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo "$orderno -- $sentToDet"; ?> 
                </div>  
                         
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo $idmember; ?> 
                </div> 
               
                <div class="col-lg-3 col-xs-3">
                    Nama Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo getUsername(); ?>
                </div>
                
                <div class="col-lg-3 col-xs-3">
                    Bonus Period  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $period; ?>
                </div>
                
                <div class="col-lg-3 col-xs-3">
                    No Order  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $orderid; ?>
                </div> 
                
                <div class="col-lg-3 col-xs-3">
                    Jumlah Transaksi  
                </div>
                <div class="col-lg-9 col-xs-9">
                   Rp. <?php echo "".number_format($payamt,0,",",",").""; ?> *(termasuk biaya administrasi)
                </div> 
                
                <div class="col-lg-3 col-xs-3">
                    Waktu Transaksi  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo "".date("d/m/Y H:i:s",strtotime($dateTrans)).""; ?>
                </div> 
            </div>             

            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                    Detail Produk
                </div>            
            </div>
            <!-- table result-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-2 col-xs-2">Kode Produk</div> 
                <div class="col-lg-3 col-xs-3">Nama Produk</div> 
                <div class="col-lg-1 col-xs-1">Qty</div>
                <div class="col-lg-1 col-xs-1">DP</div>
                <div class="col-lg-1 col-xs-1">BV</div> 
                <div class="col-lg-1 col-xs-1">Tot DP</div>
                <div class="col-lg-1 col-xs-1">Tot BV</div>
                <div class="col-lg-1 col-xs-1">Download</div>
            </div>
            <?php
                $no = 1;
                $totalDP = 0;
                $totalBV = 0;
                
               foreach($detTrans['detail'] as $dt)
               {
                    $dp = number_format(substr($dt->dpr, 0, -3),0,",",",");
                    $bv = number_format(substr($dt->bvr, 0, -3),0,",",",");
                    $qty = number_format($dt->qty,0,".",",");
                    $tdp = number_format($dt->TDP,0,".",",");
                    $tbv = number_format($dt->TBV,0,".",",");
                    $row= "
                        <div class=\"col-lg-12 col-xs-12 cart_content text-center\">
                            <div class=\"col-lg-1 col-xs-1\">".$no."</div>
                            <div class=\"col-lg-2 col-xs-2\">".$dt->prdcd."</div>
                            <div class=\"col-lg-3 col-xs-3\">".$dt->prdnm."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$qty."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$dp."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$bv."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$tdp."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$tbv."</div>

                    ";
                   if($dt->download_filename!=null){
                       $row.="<div class=\"col-lg-1 col-xs-1\">
                                            <a href='http://www.k-net.co.id/catalogue/download/$orderid/$dt->prdcd'>
											
											<span>Download</span>
										</a> </div>";
                   }
                   else
                       $row.="<div class=\"col-lg-1 col-xs-1\">-</div>
                       </div>";
                   echo $row;
                    $totalDP += $dt->TDP;
  		            $totalBV += $dt->TBV;
                    $no++;
               }
               $totDP = number_format($totalDP,0,",",",");
               $totBV = number_format($totalBV,0,",",",");
               echo "
               <div class=\"col-lg-12 col-xs-12 cart_content text-center\" style=\"margin-bottom:20px;\">
               <div class=\"col-lg-7 col-xs-7\" >&nbsp;</div>
               <div class=\"col-lg-2 col-xs-2\" >TOTAL</div>
               <div class=\"col-lg-1 col-xs-1\" >".$totDP."</div>
               <div class=\"col-lg-1 col-xs-1\" >".$totBV."</div>
               <div class=\"col-lg-1 col-xs-1\" >&nbsp;</div>
               </div>";
               
            ?>
        </div>
        <?php }?>
        <div>
            <input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="Sales.backToSearch();" name="submit" id="submit"/>
        </div><br />
    </div>
 </div>
 
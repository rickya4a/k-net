<!-- Breadcrumb -->
<?php
  $biaya = getTotalSKandShipCost2();
  $totPayAndShip = $biaya['total_pay'];
  $dt['freeship'] = $biaya['freeship'];
  $tot_discount = $biaya['tot_discount'];
  $shipping_jne_info = $this->session->userdata('shipping_jne_info');
?>
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Register Member&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Payment&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->
<!--Payment Wrapper-->
<div class="row vpullset4">
	<div class="container">
       <div id="paymentDiv">
       	<!--<form action="<?php echo site_url('checkout_process'); ?>" method="POST" id="payment-form">
    	 Left Content-->
        <div class="col-lg-7 col-xs-12 nopadding voffset4">
        	<!-- Billing Info-->
        	<div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-usd"></i> Informasi Penerima
                </span>
                <div class="col-lg-12 col-xs-12 vpullset3"> 
            		<div class="col-lg-12 col-xs-12 delivery_header">
                    	
                   
                    
                    <!--form pengiriman-->
					
                        <div class="register-top-grid">
                            <?php
                              $inp = $this->session->userdata('personal_info');
							  if($inp['delivery'] == "2")
							  {
                            ?>
                            <div>
                            <span>Nama Depan<label><font color="red">*</font></label></span>
                                <input type="text" id="nama_penerima"  readonly="readonly" value="<?php echo $inp['nama_penerima']; ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo $inp['notlp']; ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo $inp['email']; ?>"> 
                            </div>
                            
                            
                            <?php
                             } else {
                            ?>
                            <div>
                            	<span>Nama <label><font color="red">*</font></label></span>
                                <input type="text" id="firstname"  readonly="readonly" value="<?php echo getUsername(); ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo getUserPhone(); ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo getUserEmail(); ?>"> 
                            </div>
                            <?php
							 }
                            ?>
                            <div class="clearfix"> </div>					   
                        </div>				     
					  </div>
                	<!--form pengiriman--> 
				</div>   
			</div>
            <!-- Billing Info-->
        	
            <!-- Paymen Method-->
			<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-money"></i> Metode Pembayaran
                </span>
                <!--<div class="col-lg-12 col-xs-12 nopadding voffset4">   
                    <div class="col-lg-12 col-xs-12">       	
                        <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                            <div class="register-top-grid">                            
                                <div class="nomargin">
                                    <select class="form-list small" id="chooseCardType" name="chooseCardType">
                                      <option value="" selected>--Pilih Kartu--</option>	
                                      <option value="cc">Kartu Kredit</option>
                                      <option value="dc">Debit Card</option>
                                      
                                    </select>
                                </div>
                                <div class="nomargin">
                                	<select class="form-list small" id="payType" name="payType">
                                		<option value="" selected>--Pilih jenis pembayaran--</option>	
                                	</select>	
                                </div>	
                            </div>
                        </div> -->
                        
                        <div class="col-lg-12 col-xs-12 nopadding voffset4"> 
		                   <div class="col-lg-12 col-xs-12"> 	      	
		                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
		                        <div class="register-top-grid">                            
		                            <div class="nomargin"> 
		                                <!--<iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>-->
		                                 <form id="selPayment" action="<?php echo base_url('member/checkout/sgo/pay/preview'); ?>" method="post">
		                                Silahkan Pilih :
		                                <select id="bank" name="bank" onchange="setSelectPay()">
		                                	<?php
		                                	//var_dump($listBank);
		                                	 /*echo "<option value=\"\">--Pilih disini--</option>";
		                                	  foreach($listBank as $dta) {
		                                	  	echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
		                                	  }*/
                                            echo "<option value=\"\">--Pilih disini--</option>";

                                            foreach($listBank as $dta) {
                                                echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc|$dta->bankacc|$dta->bank_pemilik\">$dta->bankDisplayNm</option>";
                                            }
		                                	?>
		                                </select>
		                                <input type="hidden" name="totalx" value="" />
		                                <input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
		                        		<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
		                                <input type="hidden" name="bankid" id="bankid" value=""  />
		                                <input type="hidden" name="bankCode" id="bankCode" value=""  />
		                                <input type="hidden" name="bankDesc" id="bankDesc" value=""  />
		                                <input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
		                                <input type="hidden" name="charge_admin" id="charge_admin" value=""  />

                                             <!-- tambahan Vera -->
                                             <input type="hidden" name="sisa_bayar" id="sisa_bayar" value="<?php echo $totPayAndShip?>"/>
                                             <input type="hidden" name="total_ship_price" id="total_ship_price" value="<?php echo $shipping_jne_info['price']?>" />
                                             <input type="hidden" name="kode_unik" id="kode_unik" />
                                             <input type="hidden" name="kode_ref" id="kode_ref" />
                                             <input type="hidden" name="kode_pay" id="kode_pay" />
                                             <input type="hidden" name="total_byr" id="total_byr" />
                                             <input type="hidden" name="bank_acc" id="bank_acc" />
                                             <input type="hidden" name="bank_pemilik" id="bank_pemilik" />
                                             <input type="checkbox" name="oke" id="oke" checked hidden>
		                                
		                                <input type="submit" value="Proses Pembayaran" />
		                                </form>
		                            </div>
		                             <div class="nomargin">
		                             	<div>Saldo VA</div>
									    <div id="text_saldo_va"><?php echo $saldo_va; ?></div>
		                             </div>	
		                        </div>
		                    </div>
		                   </div> 
                         
                        <!-- detail for credit card-
    					<div class="col-lg-12 col-xs-12 nopadding" id="choosePaymentDiv">
                        	
                        </div>->
                        
                        <!-- detail for credit card-->
                        <!--
    					<div class="col-lg-12 col-xs-12 nopadding" id="choosePaymentDiv">
                        	<form method="post" action="<?php echo base_url('member/inp/test'); ?>">
                        		<input type="hidden" name="temp_orderid" value="<?php echo $key;?>" />
                        		<input type="hidden" name="temp_paymentId" value="<?php echo $payID;?>" />
                        		<input type="submit" value="Test Input ke DB" />
                        	</form>
                        </div>
                        -->
                    </div>                    
				</div>
				
        	</div>
        	<!-- next button-->
        
        	<!-- Paymen Method-->
            
			
		</div>
        <!-- Left Content-->
        
        <!-- right Content-->
		<div class="col-lg-5 col-xs-12 nopadding voffset4">
        	<!-- Adress Delivery-->
            <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-check-square-o"></i> Alamat Tujuan Pengiriman
                </span>
                <div class="col-lg-12 col-xs-12 cart_header">
                 <?php
                  $inp = $this->session->userdata('personal_info');
				  if($inp['delivery'] == "2")
				   {
				   	 //echo $inp['firstname']." ".$inp['lastname']."</br>";
				   	 echo $inp['nama_penerima']."</br>";
					 echo $inp['alamat']."</br>";
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 //echo $inp['nama_kelurahan']."</br>";
					 echo $inp['notlp']."</br>";
				   }
				  else {
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 echo $inp['nama_stockist']."</br>";
				  }	
                  ?>	
                       	 
                </div>
            </div>   
        	<!-- Adress Delivery-->
            
            <!-- Order Details-->
        	<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Pembelanjaan Starterkit
                </span>
				<br />
				
                
						
						<?php
						$member_info = $this->session->userdata("member_info");
						//echo "ID Member"
						//print_r($member_info);
						?>
						<div class="col-lg-12 col-xs-12 cart_header">
						ID Sponsor     : <?php echo $member_info['sponsorid']."<br />"; ?>
						Nama Sponsor   : <?php echo $member_info['sponsorname']."<br />"; ?>
						Nama Member    : <?php echo $member_info['membername']."<br />"; ?>
						No KTP         : <?php echo $member_info['idno']."<br />"; ?>
						Jenis Kelamin  : 
						<?php 
						  if($member_info['sex'] == "M") {
						  	echo "Pria<br />";
						  } else {
						  	echo "Wanita<br />";
						  }
						?>  
						Tgl Lahir      : <?php echo $member_info['tgllhr']."/".$member_info['blnlhr']."/".$member_info['thnlhr']."<br />"; ?>
						Alamat         : <?php echo $member_info['addr1']." ".$member_info['addr2']." ".$member_info['addr3']."<br />"; ?>
						No HP          : <?php echo $member_info['tel_hp']."<br />"; ?>
						Rekening Bank  : <?php echo $member_info['pilBank']."<br />"; ?>
						No Rekening    : <?php echo $member_info['no_rek']."<br />"; ?>
						Stokis Reff    : <?php echo $inp['nama_stockist']."<br />"; ?>
						</div>
                
                <!-- header-->
                <div class="col-lg-12 col-xs-12 cart_header">          	
                    <div class="col-lg-4 col-xs-4" align="right">Product </div>
                    <div class="col-lg-2 col-xs-2" align="right">Qty</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Price</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Amount</div>          	 
                </div>  
                <!-- header-->   
                
                <!-- product item-->
                
                
                <?php
                   $starterkit_prd = $this->session->userdata("starterkit_prd");
				   //print_r($cart);
                   $pricecode = $this->session->userdata('pricecode');
                   
                   	 echo "<div class=\"col-lg-12 col-xs-12 order_wrap\">";
						 echo "<div class=\"col-lg-4 col-xs-4 order_desc\">";
	                     echo "<h3>".$starterkit_prd['prdnm']."</h3>";
	                     $price = 0; 
	                     echo "</div>";
						
	                     echo "<div class=\"col-lg-2 col-xs-2\" align=right>1</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".number_format($starterkit_prd['price'], 0, ",", ".")."</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".number_format($starterkit_prd['price'], 0, ",", ".")."</div>
                     </div>";  
                     
                ?>	 
                    <!--<div class="col-lg-6 col-xs-6 order_desc">
                        <h3>Kinokatara</h3>
                        <p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>
                    </div>
                    <div class="col-lg-3 col-xs-3">5</div> 
                    <div class="col-lg-3 col-xs-3">Rp. 130.000</div> -->
                
                <!-- product item-->
                
                <!-- shipping cost-->
                <div class="col-lg-12 col-xs-12 order_wrap">   
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <h3>Biaya Kirim</h3>
                    </div>
                    <div class="col-lg-2 col-xs-2" align="right">
                    	<?php echo $starterkit_prd['weight']; ?>
                    </div> 
                    <div class="col-lg-3 col-xs-3" align="right">&nbsp</div> 
                    <div class="col-lg-3 col-xs-3" align="right">
                    	<?php
                    	  
                    	  //$shipping_jne_info = $this->session->userdata('shipping_jne_info');
						  /*if($shipping_jne_info == null) {
						  	echo number_format(0, 0, ",", ".");
						  } else rmat($shipping_jne_info['price'], 0, ",", ".");
						  }*/
						   echo number_format($shipping_jne_info['price'], 0, ",", ".");
                    	?>
                    </div> 
                </div>  
                <!-- shipping cost-->
               
               	 <div class="col-lg-12 col-xs-12 order_wrap">   
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <h3>Diskon Biaya Kirim</h3>
                    </div>
                    <div class="col-lg-2 col-xs-2" align="right">
                    	
                    </div>
                   
                    <div class="col-lg-3 col-xs-3" align="right">&nbsp</div> 
                    <div class="col-lg-3 col-xs-3" align="right">
                    	- <?php echo number_format($tot_discount, 0, ",", ".");  ?>
                    </div> 
                </div>  
               	
                <!-- subtotal-->
                <div class="col-lg-12 col-xs-12 order_total_wrap">   
                    <div class="col-lg-6 col-xs-6">                	
                        Subtotal
                    </div>
                    <div class="col-lg-6 col-xs-6">                	
                        <p>
                        <?php 
						/*$ship_price = 0;
						if($shipping_jne_info != null) {
							$ship_price = $shipping_jne_info['price'];
						}
						$totPayAndShip
                        $total = $starterkit_prd['price'] + $ship_price;*/
						$total = $totPayAndShip;
                        echo number_format($total, 0, ",", "."); 
                        ?>
                        </p>
                    </div>
                </div>
                <!-- subtotal-->
                
                <!-- order number
                <div class="col-lg-12 col-xs-12 order_number">                	
                    <p>No. Order Anda Adalah :</p>
                    <p><span>#ha7a63hd8</span></p>
                </div> --> 
                <!-- order number-->                  
			</div>
            <!-- Order Details-->            
		</div>
        <!-- right Content-->
        
        <!-- next button-->
        <div class="col-lg-12 col-xs-12">
        	<form method="get" action="<?php echo base_url('member/starterkit/back'); ?>">
        		<button class="btn1 btn2 btn-primary1 submit-button" type="submit">Ubah Data Pengiriman</button>
        	</form>
            <!--<button onclick="Shopping.back_to_cart('#divCheckOut','#formCart1')" type="button" class="btn1 btn2 btn-primary1 pull-left" style="margin-right:15px;" id="back"><i class="fa fa-arrow-left" ></i> Kembali ke Cart</button>&nbsp;&nbsp;
            <button onclick="Shopping.proceedPayment()" type="button" class="btn1 btn2 btn-primary1 pull-left" id="checkout"><span>Checkout</span><i class="fa fa-check-circle"></i></button>&nbsp;&nbsp;-->
                                
        </div>
        <!-- next button-->       
        
    </div>
    <!--</form>-->
    </div>
    <div id="nextPaymentProcessDIV"></div>
</div>
<!--
<script type="text/javascript">
    window.onload = function() {
        
        
        var data = {
                key : "<?php echo $key;?>",
                paymentId : "<?php echo $payID ;?>",
                paymentAmount : "<?php echo getTotalSKandShipCost() ?>",
                backUrl : encodeURIComponent ('http://www.k-net.co.id/member/sgo/finish/<?php echo $payID;?>')
            },
            sgoPlusIframe = document.getElementById("sgoplus-iframe");
        
        if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
        SGOSignature.receiveForm();
        
        console.log(data);
   };    
</script>
-->

<script type="text/javascript">
  $("#selPayment").submit(function(event) {
	  var bankCode = $("#bankCode").val();
      var productCode = $("#bankDesc").val();
      if (bankCode != "" && productCode != "") {
	    return;
	  }
	 
	  alert("Please select payment..")
	  event.preventDefault();
	});

  function setSelectPay() {
  	 var x = $("#bank").val();
  	 //alert("isi : " +x);
  	 var bankDesc = $("#bank option:selected").text();
  	 var str = x.split("|");
  	 $("#bankid").val(str[0]);
  	 $("#bankCode").val(str[1]);
  	 //$("#bankDesc").val(bankDesc);
  	 $("#bankDesc").val(str[4]);
  	 $("#charge_connectivity").val(str[2]);
  	 $("#charge_admin").val(str[3]);

     var tipe_pay = str[0];
      if(tipe_pay == '30' || tipe_pay == '31'){
          var tot_belanja = $("#sisa_bayar").val();
          var payShip= $("#total_ship_price").val();
          var str = x.split("|");
          var bank_id = str[0];
          var bank_acc = str[5];
          var nm_acc = str[6];

          //alert(payShip);

          $("#bank_acc").val(bank_acc);
          $("#bank_pemilik").val(nm_acc);

          $.ajax({
              //url: All.get_url('sgo_ppob/saveTemp'),
              //url: All.get_url('getUnique'),
              url: All.get_url('getKodePay'),
              type: 'POST',
              dataType: 'json',
              data: {
                  //token:'7383d8fa96681b037e4b1c5f7b7e4a17ff4e0d84a860ec4b27bea1158850245f6a827b22ae16f2441e655a97e8b1bf92034d3652a5ee18e9697b5c0b3f95d0de8OVJPF6WI30QG21Ed5n0jGD9rK.XjD0quEBA8a8-',
                  token:'caf428e83bd921a5309bb17cd43e4675bdf55d7aeeb43146b5c5ba927ddaaaee75744ac1dc3c9ad53cff4790f22f401c57793b105daa3a86a6789f96d6a43487W0.bFNrJ53h9h3~yvTZDtpjvQAg4D4SK7QarFQY-',
                  nominal: tot_belanja,
                  pay_ship: payShip,
                  no_rek_tujuan: bank_acc,
                  nama_rek_tujuan: nm_acc,
                  type:'Cr',
              },
              success:
                  function(result){
                      var kodeunik = result.kode_unik;
                      var kodepay = result.kd_pay;
                      var total_bayar = result.total_bayar;
                      var total_belanja = result.total_belanja;
                      var kode_ref = result.kd_ref;

                      //alert("total pembayaran" + total_bayar);

                      $("#kode_unik").val(kodeunik);
                      $("#kode_ref").val(kode_ref);
                      $("#kode_pay").val(kodepay);
                      $("#total_byr").val(total_bayar);
                      $("#oke").show();
                  },
              error: function (xhr, ajaxOptions, thrownError) {
                  alert(thrownError + ':' +xhr.status);
                  $("input[type=button]").removeAttr('disabled');
              }
          });
      }

  	 
  	
  }
</script>
<!--Payment Wrapper-->
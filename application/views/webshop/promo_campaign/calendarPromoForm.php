<style>
	select {
		border: 1px solid #f90;
    font-size: 0.8em;
    outline-color: #ff5b36;
    padding: 0.5em;
    width: 96%;
	}
</style>
<div class="row vpullset4 voffset4" style="min-height: 405px;">
	<div class="container">
    
    	<!--Left sidebar
    	<div class="col-md-3 col-sm-12">   
        	<span class="title_header">
            	<i class="fa fa-user"></i> My Account
        	</span>
            <div class="list_grid voffset3">
							
			</div>            
    	</div>
        Left sidebar
        
        <!--right-->
    	<div class="col-md-12 col-sm-12" id="acc_div"> 
            
        	<!-- Track Order -->
        	<div class="col-md-6 col-md-offset-3 text-center">
				<form name="promoCalendar" id="promoCalendar" method="post" action="<?php echo base_url('promo/calendar/stk') ?>"> 
                	<div class="track-order-grid">
                		<div>
                        	<span>Promo</span>
                        	<select id="promotype" name="promotype">
                        		<option value="">--Select Here--</option>
                        		<?php
                        		 foreach($listPromo as $dta) {
                        		 	echo "<option value=\"$dta->promocode\">$dta->promodesc</option>";
                        		 }
                        		?>
                        	</select>
                        	
                   		</div>
                    	<div>
                        	<span>ID Member</span>
                        	<input type="text" name="idmember" id="idmember"/>                            
                   		</div>
                   		<div>
                        	<span>Member Name</span>
                        	<input type="text" name="membername" id="membername" readonly="readonly"/>
                        	<input type="hidden" name="promocode" id="promocode"/>                            
                        	<input type="hidden" name="prdcd" id="prdcd"/>
                        	<input type="hidden" name="qtyPrd" id="qtyPrd"/>
                        	<input type="hidden" name="totalPay" id="totalPay"/>
                        	<input type="hidden" name="totalBV" id="totalBV"/>
                        	
                   		</div>
                   		<div>
                        	<span>Stockist</span>
                        	<select id="idstk" name="idstk" onchange="Promo.setStockist(this.value)">
                        		<option value="">--Select Here--</option>
                        		<?php
                        		 foreach($listStockist as $dta) {
                        		 	echo "<option value=\"$dta->loccd|$dta->fullnm|$dta->pricecode\">$dta->loccd - $dta->fullnm</option>";
                        		 }
                        		?>
                        	</select>
                        	<input type="hidden" name="idstkx" id="idstkx"/>
                        	<input type="hidden" name="stkname" id="stkname"/>
                        	<input type="hidden" name="pricecode" id="pricecode"/>
                   		</div>
                        <div>
                        	<input onclick="Promo.getMemberInfo(this.form.idmember.value, 'membername')" id="button_track" class="btn1 btn2 btn-primary1" type="button" value="Search" />
                        	<input type="button" id="btnSavePromo" onclick="Promo.saveDataPromo()" value="Save Data" class="btn1 btn2 btn-primary1" disabled="disabled" />
                        </div>
            		</div>
               	</form>
			</div>
           
            <!-- Track Order -->
            <!-- tracking content header-->
            
            <!-- tracking content-->
            <!-- tracking content 1-->
            <div id="resultCheck">  
                
            </div>
            <!-- tracking content 1-->
            <!-- tracking content 2-->
            <!--<div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-3 col-xs-3">a6aha7a</div>
                <div class="col-lg-3 col-xs-3">2015-02-06 - 09:25:55</div> 
                <div class="col-lg-3 col-xs-3">Kirim YES</div>
                <div class="col-lg-3 col-xs-3">On Progress</div> 
            </div>-->
            <!-- tracking content 2-->
            <!-- tracking content 3-->
            <!--<div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-3 col-xs-3">a836ha8</div>
                <div class="col-lg-3 col-xs-3">2015-02-06 - 09:25:55</div> 
                <div class="col-lg-3 col-xs-3">Pick Up Stockist</div>
                <div class="col-lg-3 col-xs-3">Ready to Pick Up</div> 
            </div>-->
            <!-- tracking 3-->
                     
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- Shopping Cart -->


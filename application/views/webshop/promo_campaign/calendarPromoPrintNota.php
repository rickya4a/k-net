<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
        
        $pdf->SetXY(10,30);
        
        $titleCol1 = 45;
        $titleCol2 = 92;
        $titleCol3 = 95;
        $titleCol4 = 95;
        $lebarCell = 5;
        
        
        $pdf->Ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Helvetica','B',14);
        $pdf->Cell(190,$lebarCell,"$header",0,0,'C',true); 
        $pdf->Ln();
        $pdf->Ln();
        
        $pdf->SetFont('Helvetica','B',8);
        $pdf->SetFillColor(255,255,255);
        foreach($result as $row)
        {
            $pdf->Cell($titleCol1,$lebarCell,"KODE PROMO",0,0,'L',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol4,$lebarCell," : ".$row->promotype,0,0,'L',true);
            $pdf->Ln();	
            $pdf->Cell($titleCol1,$lebarCell,"IDMEMBER",0,0,'L',true); 	// its similar with TD in HT
            $pdf->Cell($titleCol4,$lebarCell," : ".$row->id_memb,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"NAMA MEMBER",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell," : ".$row->nmmember,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"CLAIM ORDER NO",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell," : ".$row->orderno,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"TOTAL SALES",0,0,'L',true);
			$totdp = "Rp. ".number_format($row->total_pay,0,".",".");
			$totbv = number_format($row->total_bv,0,".",".")." BV";
			$res = $totdp. " / ".$totbv;
            $pdf->Cell($titleCol4,$lebarCell," : ".$res,0,0,'L',true);
            $pdf->Ln();
            $pdf->Cell($titleCol1,$lebarCell,"STOCKIST",0,0,'L',true);
            $pdf->Cell($titleCol4,$lebarCell," : ".$row->idstk." - ".$row->nmstkk,0,0,'L',true);
        }
        
        $pdf->Ln();
		
		$pdf->Output();
?>
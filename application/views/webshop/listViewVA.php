<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
<!-- Update Password -->
<div class="col-lg-12 col-xs-12" id="va">
	<form name="upd_bnsstt_view">
		<div>
			<span>
				<font color=red>
					<h2>Mutasi Virtual Account</h2>
				</font>
			</span>
			<div class="col-sm-3">Nama Distributor :</div>
			<div class="col-sm-3">
				<?php echo $fullnm;?>
			</div>
			<br>
			<div class="col-sm-3">Virtual Acount :</div>
			<div class="col-sm-3">
				<?php echo $novac;?>
			</div>
		</div>
		<?php
			if($saldo != null) {
			?>
		<div class="table-responsive">
			<table class="table table-bordered table-sm table-striped">
				<thead class="thead-light">
					<tr>
						<th>No</th>
						<th>Tgl.Trx</th>
						<th>Order No</th>
						<th>Debet</th>
						<th>Kredit</th>
						<!-- th>Nominal</th -->
						<th>Keterangan</th>
					</tr>
				</thead>

				<tbody>
					<?php
                         $mUrut = 1;
						 foreach($saldo as $mutasi)
						 {
						 	echo "<tr>
				            <td>".$mUrut."</td>
				  			<td>".$mutasi->trdt."</td>
				  			<td>".$mutasi->trcd."</td>";
				  			//<td>".$mutasi->tipe_dk."</td>";
							//<td align='right'>".number_format($mutasi->amount)."</td>
							$kredit = null;
			  				if($mutasi->tipe_dk == 'K'){
			  					$kredit = number_format($mutasi->amount);
			  				}
							echo "<td align='right'>$kredit</td>";
							
							$debet = null;
				  			if($mutasi->tipe_dk == 'D'){
			  					$debet = number_format($mutasi->amount);
			  				}
							echo "<td align='right'>$debet</td>";
							
				  			echo "<td>".$mutasi->bankDesc."</td>
								  </tr>";
				 		$mUrut++;						 	
						 }
						echo "<tr>
						<td>&nbsp</td>
						<td>&nbsp</td>
						<td>SALDO AKHIR :</td>
						<td align='right'>".number_format($mutasi->saldoAkhir)."</td>";
				  		echo "</tr>";	
					?>
				</tbody>
			</table>
		</div>
		<?php
			} else {
				echo "Belum ada transaksi TOP UP saldo / Transaksi menggunakan VA";
			}
			?>

	</form>
</div>
<div id="result"></div>
<!--<script type="text/javascript">
function mouseoverPass(obj) {
  var obj = document.getElementById('new_pass');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('new_pass');
  obj.type = "password";
}
</script>-->
<style>
	.form-list {
		font-size: 0.75em;
	}	
	
	.sponsorid{
		width: 80%;
	}
	
	

.register-top-grid > .col-md-6.col-xs-12 {
min-height: 60px;
}	
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Member&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Pendaftaran Baru&nbsp;&nbsp;</span>
     </li>
     
  </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
      <div id="voucherDiv">
      	
      	 <?php
      	   $member_info = $this->session->userdata('member_info');
      	 ?>  
      	<div class="col-md-12 col-xs-12 vpullset3">   
	        <form id="formMember" action="<?php echo base_url('member/starterkit'); ?>" method="POST" onsubmit="return Shopping.validateInputMember()">        
	    	  <div class="register-top-grid">
	    	   
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">ID Sponsor<font color="red">&nbsp;*</font></label><br/>
	                <input tabindex="1" class="required uppercase" type="text" id="sponsorid" name="sponsorid" value="<?php echo $member_info['sponsorid']; ?>" onchange="Shopping.getMemberInfo(this.value, 'sponsorname')" />
	                <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo $member_info['recruiterid']; ?>" />
	                <input type="hidden" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo $member_info['recruitername']; ?>" />
	                <input type="hidden" id="id_lp" name="id_lp" value="NONLP" />
	                
	            </div>
	            <div class="col-md-6 col-xs-12">
	               <label class="label-list">Nama Sponsor<font color="red">&nbsp;*</font></label><br/>
	                <input class="required uppercase" type="text" readonly="readonly" id="sponsorname" name="sponsorname" value="<?php echo $member_info['sponsorname']; ?>" />
	            </div>
	            
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Member<font color="red">&nbsp;*</font></label><br/>
	                <input tabindex="2" class="required uppercase" type="text" id="membername" name="membername" value="<?php echo $member_info['membername']; ?>" />
	            </div>
	          
	            <!--<div class="col-md-6 col-xs-12">
	                <label class="label-list">ID Recruiter<font color="red">&nbsp;*</font></label><br/>
	                <input type="hidden" id="recruiterid" name="recruiterid" value="<?php echo getUserID(); ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Nama Recruiter<font color="red">&nbsp;*</font></label><br/>
	                <input type="hidden" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo getUserName(); ?>" />
	            </div>-->
	            
	            <div class="col-md-6 col-xs-12">
	                 <label class="label-list">No KTP<font color="red">&nbsp;*</font></label><br/>
	                <input  tabindex="3" class="required numeric-input" type="text" id="idno" name="idno" value="<?php echo $member_info['idno']; ?>" onchange="Shopping.checkDoubleKTP(this.value)" />
	                
	                
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label  class="label-list">Jenis Kelamin<font color="red">&nbsp;*</font></label><br/>
	                <select tabindex="4"  class="form-list required" id="sex" name="sex">
	                	<option value="M">Pria</option>
	                	<option value="F">Wanita</option>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Tgl Lahir<font color="red">&nbsp;*</font></label><br/>
	                <!--<input tabindex="5" class="required" type="text" id="birthdt" name="birthdt" placeholder="DD/MM/YYYY" value="" onchange="Shopping.checkAgeMember(this.value)" />-->
	                <?php 
	                  echo datebirth_combo();
	                ?>
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat<font color="red">&nbsp;*</font></label><br/>
	                <input tabindex="6" class="required uppercase" type="text" id="addr1" name="addr1" value="<?php echo $member_info['addr1']; ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
	                <input tabindex="10" class="required numeric-input" type="text" id="tel_hp" name="tel_hp" value="<?php echo $member_info['tel_hp']; ?>" onchange="Shopping.checkDoubleHP(this.value)" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Kota</label><br/>
	                <input class="required uppercase" tabindex="7" type="text" id="addr2" name="addr2" value="<?php echo $member_info['addr2']; ?>" />
	            </div>
	            
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Rekening Bank</label><br/>
	                <select tabindex="11" class="form-list" id="pilBank" name="pilBank">
	                	<option value="">--Pilih disini--</option>
	                 <?php
	                    //print_r($listBank['arrayData']);
	                    foreach($listBank['arrayData'] as $dta) {
	                    	if($member_info['pilBank'] == $dta->bankid) {
	                    		echo "<option value=\"$dta->bankid\" selected=\"selected\">$dta->description</option>";
	                    	} else {
	                    		echo "<option value=\"$dta->bankid\">$dta->description</option>";
							}
	                    }
	                 ?>
	                </select>
	            </div>
	            <div class="col-md-6 col-xs-12">             
	                <label class="label-list">Kode Pos</label><br/>
	                <input tabindex="8" type="text" id="addr3" name="addr3" value="<?php echo $member_info['addr3']; ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">No Rekening</label><br/>
	                <input tabindex="12" class="numeric-input" type="text" id="no_rek" name="no_rek" value="<?php echo $member_info['no_rek']; ?>" />
	            </div>
	            <div class="col-md-6 col-xs-12">
	                <label class="label-list">Alamat Email</label><br/>
	                <input tabindex="9" type="text" id="memb_email" name="memb_email" value="<?php echo $member_info['memb_email']; ?>" />
	            </div>
	            <div class="clearfix"> </div>
	          </div>
	          <div class="col-md-12">
       
            
            	<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjut<i class="fa fa-arrow-right"></i></button><br />
              </div>
        <!-- next button-->
        </div>  
	        </form>
        </div>
       </div>
        <!-- Delivery Options--> 
      </div> <!-- End formCart1-->
      <div id="divCheckOut"></div> 
      <div id="afterPayment"></div>

<script type="text/javascript">
	$(function() {
		$("#sex").val("<?php echo $member_info['sex'] ?>");
		$("#tgllhr").val("<?php echo $member_info['tgllhr'] ?>");
		$("#blnlhr").val("<?php echo $member_info['blnlhr'] ?>");
		$("#thnlhr").val("<?php echo $member_info['thnlhr'] ?>");
	});
</script>
<!--Checkout Wrapper-->

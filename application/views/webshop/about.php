<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;About&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Shopping rate-->
<div class="row vpullset4 voffset4">
    <div class="container">     
<!--Left sidebar-->
    	<div class="col-md-3 col-sm-12">          	
            <div class="list_grid">
		<ul style="margin-top:0px;">
                	<li><a href="<?php echo site_url('static/about');?>">About K-Link</a></li>
                	<li><a href="<?php echo site_url('static/why');?>">Why K-Link</a></li>
                	<li><a href="<?php echo site_url('static/how-to-order');?>">How To Order</a></li>
                    	<li><a href="<?php echo site_url('static/replacement');?>">Replacment Policy</a></li>
                	<li><a href="<?php echo site_url('static/delivery');?>">Delivery</a></li>
                </ul>				
	   </div>            
    	</div>
        <!--Left sidebar--> 
       <!--right-->
        <div class="col-md-9 col-sm-12"> 
        
	

            <div class="mens-toolbar">
                <span class="title_header">
                    <i class="fa fa-info"></i> About K-Link
                </span>
            </div>
            
            <p class="desc" style="margin-bottom:20px;">
                Di zaman yang semakin maju, dibutuhkan cara berbelanja yang cepat, nyaman dan aman
                (E-Commerce) atau Berbelanja online melalui Internet menjadi jawaban cara berbelanja saat ini,
                PT K-Link Indonesia menyadari pentingnya E-Commerce, untuk lebih menunjang kemudahan Anda untuk
                mendapatkan produk-produk K-LINK, PT K-Link Indonesia akhirnya meluncurkan K-LINK Web Store.
            </p>
            <p class="desc" style="margin-bottom:20px;">
                K-LINK Web Store merupakan sistem berbelanja online yang menjangkau seluruh Indonesia. 
                Distributor K-LINK kini dapat dengan mudah memesan produk-produk K-LINK secara online dan
                destinasi barang dapat dikirim sesuai dengan keinginan Distributor.
            </p>
            <p class="desc" style="margin-bottom:20px;">
                Selain memudahkan Anda saat berbelanja, K-LINK Web Store juga telah dilengkapi dengan 3D
                Secure. 3D Secure adalah sistem keamanan data kartu kredit pada saat digunakan dalam melakukan 
                transaksi online. Sehingga para pemegang kartu kredit akan lebih aman saat berbelanja di K-LINK Web Store.
                DenganBukan hanya pembayaran melalui kartu kredit saja, namun, K-LINK Web Store menawarkan 
                metode pembayaran lainnya seperti  transfer antar Bank, Internet Banking dan Mobile Payment yang
                mudah dan aman
            </p>
            <p class="desc" style="margin-bottom:20px;">
                Dengan memiliki lebih dari 2,4 juta anggota, kehadiran K-LINK Web Store diharapkan menjadi solusi
                yang tepat, guna memudahkan para anggotanya untuk berbelanja produk K-LINK. Kehadiran K-LINK Web Store
                juga sejalan dengan misi PT K-Link Indonesia  yaitu Untuk memasarkan produk makanan dan minuman kesehatan, 
                produk penunjang dan perawatan kesehatan yang berkualitas dan teruji khasiatnya, dengan harga yang dapat
                dijangkau bagi masyarakat luas. K-Link menawarkan konsep bisnis direct selling yang dapat dijalankan 
                oleh setiap orang dari beragam latar belakang sosial dan pendidikan.
            </p>
            <br>              
            
                     
        </div>
    <!--right-->
        
    </div>
</div>
<!-- Shopping rate-->

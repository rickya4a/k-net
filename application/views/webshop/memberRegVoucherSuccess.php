<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Shipping&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Konfirmasi Pembayaran&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">REGISTRASI MEMBER DENGAN VOUCHER</div>
            <div class="col-lg-12 col-xs-12 nomargin">
                <p class="p2 nopadding" style="font-size:16px;">
                   <?php
                     if($res['response'] == "true") {                 
	                      echo "ID Member             : ".$res['arrayData'][0]->memberid.""."<br />";
						  echo "Nama Member           : ".$res['arrayData'][0]->membername.""."<br />";
						  echo "Password              : ".$res['arrayData'][0]->password.""."<br />";
	                      echo "ID Sponsor            : ".$res['arrayData'][0]->sponsorid.""."<br />";
	                      echo "Nama Sponsor          : ".$res['arrayData'][0]->sponsorname.""."<br />";
						  echo "ID Recruiter          : ".$res['arrayData'][0]->recruiterid.""."<br />";
	                      echo "Nama Recruiter        : ".$res['arrayData'][0]->recruitername.""."<br />";
						  echo "ID Stockist           : ".$res['arrayData'][0]->stk_code.""."<br />";
						  echo "ID Trx                : ".$res['arrayData'][0]->trx_no.""."<br />";
                     } else {
                     	  echo "Pendaftaran Member gagal..";
                     } 
                   ?>
                </p>
                <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">
                	<!--<a href="<?php //echo base_url('shop/product'); ?>">Klik disini untuk kembali ke halaman utama</a>-->
                    <a class="btn1 btn2 btn-primary1" href="<?php echo base_url('shop/product'); ?>">
                        <i class="fa fa-arrow-left"></i>
                        <span>Klik disini untuk kembali ke halaman utama</span>
                    </a>
                </div>	
            </div>
		</div>	
	</div>
</div>        
<!-- Login Form --> 
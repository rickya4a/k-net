<script>
    $(document ).ready(function() {
        var cno_order=$(no_order).val();
        var tipe="";
        var kurir_nama='-';
        var kurir_hp='';
        var track='#';

        $.ajax({
            url:"https://www.k-net.co.id/status_booking",
            type: "POST",
            dataType:"json",
            data:{
                orderno:cno_order
            },
            success: function (result) {
                console.log(result);

                tipe=result.bookingType;
                track=result.liveTrackingUrl;
                //track=track.replace('-integration','');

                if(result.driverName != null && result.driverPhone != null){
                    kurir_nama=result.driverName;
                    kurir_hp=result.driverPhone;
                }

                $("#jenis_text").text('GO-SEND '+tipe);
                $("#kurir_text").text(kurir_nama+' (phone: '+kurir_hp+')');

                document.getElementById("live_text").href = track;
                document.getElementById("live_text").target = "_blank";
            }
        });
    });

    function goBack() {
        window.history.back();
    }

    function getRebooking(orderno){
        window.location.href = "/sales/rebooking/"+orderno;
    }
</script>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Sales&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Cek Transaksi K-net&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>
<!-- Breadcrumb -->
<div class="row vpullset4">
    <div class="container">
        <div class="col-md-12">
            <div class="col-lg-12 col-xs-12 cart_header">
                <div class="col-lg-12">
                    Lacak Pesanan
                </div>
            </div>

            <?php
           /* if($cekTrans['response'] != "true"){
                echo "Data Tidak Ada";
            }else {*/
                ?>

                <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                    <div class="col-lg-3 col-xs-3">
                        ID Member
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <?php echo getUserID(); ?>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        Nama Member
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <?php echo getUsername(); ?>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        Order No
                    </div>
                    <div class="col-lg-9 col-xs-9">
                       <?php echo $orderno?>
                        <input type="hidden" id="no_order"  name="no_order"  value="<?php echo $no_gosend?>" readonly>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        Order Diterima
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <?php echo $orderdate?>
                    </div>

                    <!--break-->
                    <div class="col-lg-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        &nbsp;
                    </div>

                    <div class="col-lg-3 col-xs-3" id="jenis_text">
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        &nbsp;
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        No. Resi
                    </div>
                    <div class="col-lg-9 col-xs-9" id="order_text">
                        <?php echo $no_gosend?>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        Kurir
                    </div>
                    <div class="col-lg-9 col-xs-9" id="kurir_text">
                    </div>

                    <!--break-->
                    <div class="col-lg-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        &nbsp;
                    </div>
                    <!--break-->

                    <div class="col-lg-3 col-xs-3">
                        Live Tracking
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        <a id="live_text"><button>Live Tracking</button></a>
                    </div>

                    <div class="col-lg-3 col-xs-3">
                        Status Pengiriman
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        &nbsp;
                    </div>
                    <?php
                    if($tracking != null){
                        foreach($tracking as $row) {
                            $status = $row->status;
                            $pickup = $row->pickup_eta;
                            $delivery = $row->delivery_eta;
                            $cancelled = $row->cancelled_by;
                            $cancel_reason="-";
                            $receiver = "";
                            $live_track = $row->live_tracking;
                            $created = $row->created;
                            $hide="hidden";

                            if($status == "confirmed"){
                                $status= "Kami sedang melakukan pencarian Kurir";
                                $color="#000007";
                                $created= $orderdate;

                            }elseif ($status == "allocated"){
                                $status = "Kurir telah didapatkan";
                                $color="#000007";
                                $created=$pickup;

                            }elseif ($status == "out_for_pickup") {
                                $status = "Kurir dalam perjalanan untuk pick-up";
                                $color="#000007";
                                $created=$pickup;

                            }elseif ($status == "picked") {
                                $status = "Pesanan telah diambil Kurir";
                                $color="#000007";
                                $created=$pickup;

                            }elseif ($status == "out_for_delivery") {
                                $status = "Kurir mengantarkan pesanan";
                                $color="#000007";
                                $created= $delivery;

                            }elseif ($status == "delivered") {
                                $receiver = $row->receiver_name;
                                $status = "Pesanan telah terkirim. Penerima: ".$receiver;
                                $color= "#0000e6";
                                $created= $delivery;


                            }elseif ($status == "cancelled") {
                                $cancel_reason = $row->cancellation_reason;
                                $status = "Pesanan dibatalkan oleh ".$cancelled.". catatan: ".$cancel_reason;
                                $color ="#FF1810";
                                $created= $created;
                                $hide="";


                            }elseif ($status == "rejected") {
                                $cancel_reason = $row->cancellation_reason;
                                $status = "Pesanan telah dikembalikan ke stockist. catatan: ".$cancel_reason;
                                $color ="#FF1810";
                                $created= $created;
                                $hide="";

                            }elseif ($status == "no_driver") {
                                $status = "Kurir tidak ditemukan";
                                $color ="#FF1810";
                                $created= $created;

                            }elseif ($status == "on_hold") {
                                $status = "Penundaan pengiriman pesanan ( Pending Delivery )";
                                $color ="#FF1810";
                                $created= $created;

                            }else{
                                $status = "Kami telah menerima rincian pesanan Anda";
                                $color="#000007";
                                $created= $created;
                            }
                            ?>
                    <div class="col-lg-3 col-xs-3">
                     &nbsp;
                    </div>
                    <div class="col-lg-9 col-xs-9" id="status1_text" style="color: <?php echo $color?>">
                        <?php echo $status ?>

                    </div>
                    <div class="col-lg-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-lg-9 col-xs-9" id="date1_text" style="color: <?php echo $color?>">
                        <?php echo $created?>
                    </div>
                            <?php
                        }
                    }
                    ?>
                    <!--break-->
                    <div class="col-lg-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-lg-9 col-xs-9">
                        &nbsp;
                    </div>
                    <div class="col-lg-3 col-xs-3" id="reorder1" <?php echo $hide?>>
                        Order Ulang GO-SEND
                    </div>
                    <div class="col-lg-9 col-xs-9" id="reorder2"<?php echo $hide?>>
                        <a href="<?php echo base_url()."sales/rebooking/".base64_encode($orderno)?>"><button>Pesan Disini!</button></a>
                    </div>
                    <!--break-->
                </div>
        </div>
        <div>
            <input class="btn1 btn2 btn-primary1" type="button" value="Kembali ke Form" onClick="goBack();" name="submit" id="submit"/>
        </div><br />
        </div>
    </div>


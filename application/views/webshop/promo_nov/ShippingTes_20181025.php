<script>
    function setNewPriceAldi (param) {
        console.log("e")
        var qty = parseInt($("#qty" +param).val());
        var nm = /^[0-9]+$/;
        if($("#qty" +param).val().match(nm) && qty > 0)
        {

            var westPrice = $("#westPrice" +param).val();
            var westCPrice = $("#westCPrice" +param).val();

            var amountKIRIM = parseInt($("#amountKIRIM").val());
            var weightPrice = $("#weightPrice" +param).val();

            var eastPrice = $("#eastPrice" +param).val();
            var eastCPrice = $("#eastCPrice" +param).val();
            var bvr = parseInt($("#bv" +param).val());
            $("#amountWest" +param).val(qty * westPrice);
            $("#amountEast" +param).val(qty * eastPrice);
            $("#amountCWest" +param).val(qty * westCPrice);
            $("#amountCEast" +param).val(qty * eastCPrice);
            $("#amountWeight" +param).val(qty * weightPrice);
            console.log("bvr : "+bvr+ " qty : "+qty+ " weight : "+(weightPrice) );
            $("#amountBV" +param).val(qty * bvr);
            $("#divSubTotWestPrice" +param).html(null);
            $("#divSubTotEastPrice" +param).html(null);
            $("#divSubTotWeightPrice" +param).html(null);
            $("#divSubTotWestPrice" +param).text(All.num(qty * westPrice));
            $("#divSubTotEastPrice" +param).text(All.num(qty * eastPrice));
            $("#divSubTotWestCPrice" +param).text(All.num(qty * westCPrice));
            $("#divSubTotEastCPrice" +param).text(All.num(qty * eastCPrice));

            var totWeightCnt = qty * weightPrice;
            $("#divSubTotWeightPrice" +param).text(totWeightCnt.toFixed(2) +" Kg");
//            $("#divSubTotWeightPrice" +param).text(weightPrice);

            /*$("#divTotBVWest").html(null);
             $("#divTotBVEast").html(null);
             $("#divTotBVWest" +param).text(All.num(qty * bvr));
             $("#divTotBVEast" +param).text(All.num(qty * bvr));*/

            var totQty = 0;
            var totBV = 0;
            var totWest = 0;
            var totEast = 0;
            var totWeight = 0;
            var totCWest = 0;
            var totCEast = 0;


            $('input[name="amountCWest[]"]').each(function(){
                totCWest += parseInt($(this).val());
            });



            $('input[name="amountCEast[]"]').each(function(){
                totCEast += parseInt($(this).val());
            });

            $('input[name="qty[]"]').each(function(){
                totQty += parseInt($(this).val());
            });
            $('input[name="amountWest[]"]').each(function(){
                totWest += parseInt($(this).val());
            });

            $('input[name="amountWeight[]"]').each(function(){
                console.log("aa ");

                console.log(parseFloat($(this).val()));
                totWeight += parseFloat($(this).val());
            });

            $('input[name="amountEast[]"]').each(function(){
                totEast += parseInt($(this).val());
            });

            $('input[name="amountBV[]"]').each(function(){
                totBV += parseInt($(this).val());
            });

            totWest=totWest+(amountKIRIM);
            totEast=totWest+(amountKIRIM);
            totCWest=totCWest+(amountKIRIM);
            totCEast=totCEast+(amountKIRIM);
            //alert("totQty :" +totQty+ " west :" +totWest+ " east :" +totEast);

            $("#totalAmountWest").val(totWest);
            $("#totalAmountEast").val(totEast);
            $("#totalAmountWeight").val(totWeight);
            $("#totQtyWest").val(totQty);
            $("#totQtyEast").val(totQty);
            $("#divTotQtyWest").html(null);
            $("#divTotQtyEast").html(null);
            $(".divTotWeight").html(null);



            $("#divTotQtyWest").html(totQty);
            $("#divTotQtyEast").html(totQty);
            $(".divTotWeight").html(totWeight.toFixed(2)+" Kg");

            $("#totAllWestPrice").html(null);
            $("#totAllEastPrice").html(null);
            $("#totAllWestCPrice").html(null);
            $("#totAllEastCPrice").html(null);
            $("#totAllItemWest").html(null);
            $("#totAllItemEast").html(null);
            $("#totAllWestPrice").text(All.num(totWest));
            $("#totAllEastPrice").text(All.num(totEast));
            $("#totAllItemWest").text(All.num(totQty));
            $("#totAllItemEast").text(All.num(totQty));
            $("#divTotBVWest").html(All.num(totBV));
            $("#divTotBVEast").html(All.num(totBV));
            $("#totAllWestCPrice").text(All.num(totCWest));
            $("#totAllEastCPrice").text(All.num(totCEast));


            var pricecode = $("#pricecode").val();
            Shopping.updatePriceAtHeader(pricecode);

            getBiayaKirim();

        } else {
            alert('Tidak boleh huruf atau angka nol');
            $("#qty" +param).val(1);
            $("#qty" +param).focus();
        }
    };

    function delete_rowCart (param){
        All.set_disable_button();
        var rowid = $("#rowid" +param).val();
        var qty = parseInt($("#qty" +param).val());
        var totQtyWest = parseInt($("#totQtyWest").val());
        var pricecode = $("#pricecode").val();
        $.ajax({
            dataType: 'json',
            type: "POST",
            url : All.get_url("cart/remove/"),
            data: {rowid: rowid},
            success: function(data)
            {
                All.set_enable_button();
                alert(data.message);
                if(data.response == "true")
                {
                    /*var newQty = totQtyWest - qty;
                     $("#divTotQtyWest").html(null);
                     $("#divTotQtyEast").html(null);
                     $("#divTotQtyWest").html(All.num(newQty));
                     $("#divTotQtyEast").html(All.num(newQty));
                     $("#totQtyWest").val(newQty);
                     $("#totQtyEast").val(newQty); */

                    recalculateItems2(param);
                    $("div#" +param).remove();
                    //Shopping.setNewPrice(param);

                    Shopping.updatePriceAtHeader(pricecode);
                }

            }
        });
    };

    function recalculateItems2 (param) {
        var qty = parseInt($("#qty" +param).val());
        var westPrice = parseInt($("#amountWest" +param).val());
        var weightPrice = parseFloat($("#amountWeight" +param).val());
        var eastPrice = parseInt($("#amountEast" +param).val());
        var totBv = parseInt($("#totBv").val());
        var totQtyWest = parseInt($("#totQtyWest").val());
        var totalAmountWest = parseInt($("#totalAmountWest").val());
        var totQtyEast = parseInt($("#totQtyEast").val());
        var totalAmountEast = parseInt($("#totalAmountEast").val());
        var totalAmountWeight = parseFloat($("#totalAmountWeight").val());
        var amountKIRIM = parseInt($("#amountKIRIM").val());

        console.log(totalAmountWeight);
        console.log(weightPrice);
        var selisih_barat = totalAmountWest - westPrice;
        var selisih_timur = totalAmountEast - eastPrice;
        var selisih_qty = totQtyWest - qty;
        var selisih_weight = totalAmountWeight - weightPrice;
        //alert("Selisih barat : " +selisih_barat+ ", selisih timur : " +selisih_timur);
        selisih_weight=selisih_weight.toPrecision(2);

        $("#totalAmountWest").val(selisih_barat);
        $("#totalAmountWeight").val(selisih_weight);
        $("#totalAmountEast").val(selisih_timur);
        $("#totQtyWest").val(selisih_qty);
        $("#totQtyEast").val(selisih_qty);
        $("#divTotQtyWest").html(null);
        $("#divTotQtyEast").html(null);
        $(".divTotWeight").html(null);
        $("#divTotQtyWest").html(selisih_qty);
        $("#divTotQtyEast").html(selisih_qty);
        $(".divTotWeight").html(selisih_weight +" Kg");

        $("#totAllWestPrice").html(null);
        $("#totAllEastPrice").html(null);
        $("#totAllItemWest").html(null);
        $("#totAllItemEast").html(null);
        $("#totAllWestPrice").text(All.num(selisih_barat));
        $("#totAllEastPrice").text(All.num(selisih_timur));
        $("#totAllItemWest").text(All.num(selisih_qty));
        $("#totAllItemEast").text(All.num(selisih_qty));
        getBiayaKirim();

    };


    function tampilkanPriceCode2() {
        All.set_disable_button();
        console.log("heyyy");
        var shipper = $("#shipper").val();
        var stockistref = $("#stockistref").val();
        var kota = $("#kota").val();
        var drop = $("#id_lp").val();

        if(shipper == "") {
            alert("Silahkan pilih Stockist dahulu");
        } else {
            //var niss = $("#kota :stockistref").text();
            if(shipper == "1") {
                var kec = $("#kecamatan").val();
                All.set_disable_button();
                $("#pricecode").attr("disabled", "disabled");
                $.ajax({
                    url: All.get_url("shipping/getPricecodeX/") +stockistref +"/" +kec,
                    type: 'GET',
                    dataType: 'json',
                    success:
                        function(data){
                            var stkname = $("#stockistref :selected").text();
                            console.log("drop "+drop);
                            console.log("kode "+data.arrayData[0].pricecode);

                            $("#pricecode").removeAttr("disabled", "disabled");
                            All.set_enable_button();
                            if(data.response == "true") {
                                $("#nama_stockist").val(stkname);
                                $("#nama_stockistr1ref").val(stkname);
                                $("#state").val(data.arrayData[0].state);
                                $("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
                                $("#jne_branch").val(data.arrayData[0].jne_branchcd);
                                //console.log(data.arrayData[0].kode_kec_JNE_Origin);
                                $("#pricecode").val(data.arrayData[0].pricecode);
                                if(data.arrayData[0].pricecode == "12E3" && drop =='') {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "block");
                                    $(".westCP").css("display", "none");
                                    $(".eastCP").css("display", "none");

                                } else if(data.arrayData[0].pricecode != "12E3" && drop =='')  {
                                    $(".westP").css("display", "block");
                                    $(".eastP").css("display", "none");
                                    $(".westCP").css("display", "none");
                                    $(".eastCP").css("display", "none");

                                }
                                else if(data.arrayData[0].pricecode != "12E3" && drop !='')  {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "none");
                                    $(".westCP").css("display", "block");
                                    $(".eastCP").css("display", "none");

                                }
                                else if(data.arrayData[0].pricecode == "12E3" && drop !='')  {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "none");
                                    $(".westCP").css("display", "none");
                                    $(".eastCP").css("display", "block");

                                }
                                //$("#state").val(data.arrayData[0].pricecode);
                                Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);


                                getBiayaKirim();


                            } else {
                                alert("Price code tidak ada");
                            }
                        },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + ':' +xhr.status);
                        All.set_enable_button();
                    }
                });




            } else {
                $.ajax({
                    url: All.get_url("cargo/pricecode/") +kota,
                    type: 'GET',
                    dataType: 'json',
                    success:
                        function(data){

                            var stkname = $("#stockistref :selected").text();
                            $("#pricecode").removeAttr("disabled", "disabled");
                            All.set_enable_button();
                            if(data.response == "true") {
                                //var arrayData = data.arrayData;
                                $("#nama_stockist").val(stkname);
                                $("#nama_stockistr1ref").val(stkname);
                                $("#pricecode").val(data.arrayData[0].pricecode);
                                $("#sender_address").val(null);
                                $("#jne_branch").val(null);
                                if(data.arrayData[0].pricecode == "12E3" && drop =='') {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "block");
                                    $(".westCP").css("display", "none");
                                    $(".eastCP").css("display", "none");

                                } else if(data.arrayData[0].pricecode != "12E3" && drop =='')  {
                                    $(".westP").css("display", "block");
                                    $(".eastP").css("display", "none");
                                    $(".westCP").css("display", "none");
                                    $(".eastCP").css("display", "none");

                                }
                                else if(data.arrayData[0].pricecode == "12E3" && drop !='')  {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "none");
                                    $(".westCP").css("display", "none");
                                    $(".eastCP").css("display", "block");

                                }
                                else if(data.arrayData[0].pricecode != "12E3" && drop !='')  {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "none");
                                    $(".westCP").css("display", "block");
                                    $(".eastCP").css("display", "none");

                                }
                                Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
                            } else {
                                alert("Price code tidak ada..")
                                //$(setToDiv).append(null);
                            }
                        },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + ':' +xhr.status);
                        All.set_enable_button();
                    }
                });
            }
            //$("#nama_kota").val(niss);
        }
    };

    function getBiayaKirim(){
        var sender =$("#sender_address").val();
        var jbranch = $("#jne_branch").val();
        if(sender != '' && jbranch != ''){
            console.log("berhasiwl");

            $.ajax({
                url: All.get_url("cargo/price/"),
                type: 'POST',
                data : {
                    cargo_id : 1,
                    dest_code : $("#kecamatan").val(),
                    stockist_code : $("#stockistref").val(),
                    weight : $("#totalAmountWeight").val()

                },
                dataType: 'json',
                success:
                    function(data){


                        //$("#provDiv").html(null);
                        //$("#kabDiv").html(null);
                        //$("#kecDiv").html(null);
                        All.set_enable_button();
                        if(data.response == "true") {
                            var arrayData = data.arrayData;
                            //alert("hore");
                            $("#amountKIRIM").val(arrayData.price);
                            $("#divAmountKirim").html(null);
                            $("#divAmountKirim").text(All.num(arrayData.price));



                            var amountKIRIM = parseInt($("#amountKIRIM").val());
                            var totQty=0;
                            var totWest = 0;
                            var totEast = 0;
                            var totWeight = 0;
                            var totCWest = 0;
                            var totCEast = 0;



                            $('input[name="amountWest[]"]').each(function(){
                                totWest += parseInt($(this).val());
                            });



                            $('input[name="amountEast[]"]').each(function(){
                                totEast += parseInt($(this).val());
                            });

                            $('input[name="amountCWest[]"]').each(function(){
                                totCWest += parseInt($(this).val());
                            });



                            $('input[name="amountCEast[]"]').each(function(){
                                totCEast += parseInt($(this).val());
                            });

                            totWest=totWest+(amountKIRIM);
                            totEast=totEast+(amountKIRIM);
                            totCWest=totCWest+(amountKIRIM);
                            totCEast=totCEast+(amountKIRIM);

                            $("#totAllWestPrice").html(null);
                            $("#totAllEastPrice").html(null);
                            $("#totAllWestPrice").text(All.num(totWest));
                            $("#totAllEastPrice").text(All.num(totEast));
                            $("#totAllWestCPrice").text(All.num(totCWest));
                            $("#totAllEastCPrice").text(All.num(totCEast));

                            //console.log(arrayData);
                        } else {
                            //alert(data.message)
                        }
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' +xhr.status);
                    All.set_enable_button();
                }
            });




        }else
        {
            console.log("tidaaak");
            console.log("sender "+sender);
            console.log("jbranch "+jbranch);
        }

    };

    function show_pricestkX(idx,setToDiv){
        if(idx.value !== "" && idx.value != undefined ) {
            console.log("idx =="+idx.value);
            var kec = $("#kecamatan1").val();
            var drop = $("#id_lp").val();

            All.set_disable_button();
            $(setToDiv).attr("disabled", "disabled");
            $.ajax({
                url: All.get_url("shipping/getPricecodeX/") +idx.value +"/" +kec,
                type: 'GET',
                dataType: 'json',
                success:
                    function(data){
                        var stkname = $("#"+idx.id+" :selected").text();
                        $(setToDiv).removeAttr("disabled", "disabled");
                        All.set_enable_button();
                        if(data.response == "true") {
                            $("#nama_stockist").val(stkname);
                            $("#nama_stockistr1ref").val(stkname);
                            $("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
                            $("#jne_branch").val(data.arrayData[0].jne_branchcd);
                            //console.log(data.arrayData[0].kode_kec_JNE_Origin);
                            console.log(data.arrayData[0].pricecode );
                            $(setToDiv).val(data.arrayData[0].pricecode);
                            if(data.arrayData[0].pricecode == "12E3" && drop =='') {
                                $(".westP").css("display", "none");
                                $(".eastP").css("display", "block");
                                $(".westCP").css("display", "none");
                                $(".eastCP").css("display", "none");

                            } else if(data.arrayData[0].pricecode != "12E3" && drop =='')  {
                                $(".westP").css("display", "block");
                                $(".eastP").css("display", "none");
                                $(".westCP").css("display", "none");
                                $(".eastCP").css("display", "none");

                            }
                            else if(data.arrayData[0].pricecode == "12E3" && drop !='')  {
                                $(".westP").css("display", "none");
                                $(".eastP").css("display", "none");
                                $(".westCP").css("display", "block");
                                $(".eastCP").css("display", "none");

                            }
                            else if(data.arrayData[0].pricecode != "12E3" && drop !='')  {
                                $(".westP").css("display", "none");
                                $(".eastP").css("display", "none");
                                $(".westCP").css("display", "block");
                                $(".eastCP").css("display", "none");

                            }
                            Shopping.updatePriceAtHeader(data.arrayData[0].pricecode);
                        } else {
                            alert("Price code tidak ada");
                        }
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' +xhr.status);
                    All.set_enable_button();
                }
            });

            {
//                var arrayData = data.arrayData;
                //alert("hore");
                $("#amountKIRIM").val(0);
                $("#divAmountKirim").html(null);
                $("#divAmountKirim").text(All.num(0));



                var amountKIRIM = parseInt($("#amountKIRIM").val());
                var totQty=0;
                var totWest = 0;
                var totEast = 0;
                var totCWest = 0;
                var totCEast = 0;
                var totWeight = 0;



                $('input[name="amountWest[]"]').each(function(){
                    totWest += parseInt($(this).val());
                });



                $('input[name="amountEast[]"]').each(function(){
                    totEast += parseInt($(this).val());
                });
                $('input[name="amountCWest[]"]').each(function(){
                    totCWest += parseInt($(this).val());
                });



                $('input[name="amountCEast[]"]').each(function(){
                    totCEast += parseInt($(this).val());
                });


                totWest=totWest+(amountKIRIM);
                totEast=totEast+(amountKIRIM);
                totCWest=totCWest+(amountKIRIM);
                totCEast=totCEast+(amountKIRIM);

                $("#totAllWestPrice").html(null);
                $("#totAllEastPrice").html(null);
                $("#totAllWestPrice").text(All.num(totWest));
                $("#totAllEastPrice").text(All.num(totEast));
                $("#totAllWestCPrice").text(All.num(totWest));
                $("#totAllEastCPrice").text(All.num(totEast));

                //console.log(arrayData);
            }
        }


    };

    function getDP(idx,setToDiv){
        var dp=$("#id_lp").val();
        var radios = document.getElementsByName('delivery');

        for (var i = 0, length = radios.length; i < length; i++){
            if (radios[i].checked)
            {
                // do whatever you want with the checked radio
                console.log(radios[i].value);

                if(radios[i].value==2){
                    console.log(radios[i].value);
                    tampilkanPriceCode2();
                }
                else
                {
                    console.log(radios[i].value);
                    show_pricestkX(idx,setToDiv);


                }

                // only one radio can be logically checked, don't check the rest
                break;
            }
        }

    }



    $(document).ready(function(){
        // we call the function
        getDP('#stockist','#pricecode');
    });


</script>


<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Shopping&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Pengiriman&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
    <div class="container">

        <div id="formCart1">
            <?php
            $cart_check = $this -> cart -> contents();
            $personal_info = $this->session->userdata('personal_info');
            $shipping_jne_info = $this->session->userdata('shipping_jne_info');

            $usr = getUserID();
            if($usr == "IDSPAAA66834" || getUserID() == "IDSPAAB04607") {
            ?>
            <form id="formCart" action="<?php echo base_url('cart/checkout/sgo'); ?>" method="POST" onsubmit="return Shopping.validateCheckOutBaru()">
                <?php
                } else {
                ?>
                <form id="formCart" action="<?php echo base_url('cart/checkout'); ?>" method="POST" onsubmit="return Shopping.validateCheckOutBaru()">
                    <?php
                    }
                    ?>

                    <!-- Delivery Options--><br />
                    <div class="col-md-6 col-xs-12">
        	<span class="title_header">
            	<i class="fa fa-truck"></i> Tipe Pengiriman
            </span>
                        <!-- ul><li>Pengumuman : berkenaan dengan libur hari raya idul fitri 2017, maka pengiriman akan dihentikan sementara mulai tanggal 22 Juni 2017 s/d 04 Juli 2017.</li></ul -->
                        <!--  Options 1-->
                        <div class="col-md-12 col-xs-12 nopadding voffset4">
                            <div class="col-md-12">
                                Dikirim Sebagai
                                <select class="form-list" name="id_lp" id="id_lp" onchange="getDP('#stockist','#pricecode')">
                                    <!--                            <option value="">--Select Here--</option>-->
                                    <option value="" <?php echo set_select('id_lp',"",($personal_info['id_lp'] == "")? true : false); ?> > Member</option>
                                    <!--
                                    <option value="DROPSHIP"
									 <?php echo set_select('id_lp',"DROPSHIP",($personal_info['id_lp'] == "DROPSHIP")? true : false); ?> > Dropshipper
									 </option>
									 -->
                                </select>

                            </div>


                            <?php
                            //$personal_info = $this->session->userdata('personal_info');
                            if($personal_info['delivery'] == "1")
                            {
                            ?>
                            <!--  Options 1 header-->
                            <div class="col-md-12 col-xs-12 delivery_header">
                                <input name="delivery" id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/> Diambil di Stockist
                            </div>
                            <!--  Options 1 header-->

                            <!--  Options 1 detail-->
                            <div class="col-md-12 col-xs-12 diambil">
                                <p class="p2 nopadding">
                                    Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
                                    <br/>
                                    <br/>
                                </p>
                                <div class="register-top-grid">

                                    <!--  Options 1 area-->

                                    <div>
                                        <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
                                            <option value="">--Select Here--</option>
                                            <?php
                                            //print_r($show_provinsi);
                                            foreach($show_provinsi['arrayData'] as $row){
                                                if($row->kode_provinsi == $personal_info['provinsi']) {
                                                    echo "<option value=\"".$row->kode_provinsi."\" selected=\"selected\">".strtoupper($row->provinsi)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";
                                                }
                                            }

                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="<?php echo $personal_info['nama_provinsi'] ?>" />
                                    </div>
                                    <!--  Options 1 area-->

                                    <!--  Options 1 lokasi-->
                                    <div>
                                        <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">
                                            <option value="">--Select Here--</option>
                                            <?php
                                            foreach($shipping['kota'] as $row){
                                                if($row->kode_kabupaten == $personal_info['kota']) {
                                                    echo "<option value=\"".$row->kode_kabupaten."\" selected=\"selected\">".strtoupper($row->kabupaten)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->kode_kabupaten."\">".strtoupper($row->kabupaten)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_kota1" name="nama_kota1" value="<?php echo $personal_info['nama_kota'] ?>" />
                                        <!--<input type="text" name="kota" id="kota"/>-->
                                    </div>
                                    <div>
                                        <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                        <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                        <select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >
                                            <option value="">--Select Here--</option>
                                            <?php
                                            foreach($shipping['kecamatan'] as $row){
                                                if($row->kode_kecamatan == $personal_info['kecamatan']) {
                                                    echo "<option value=\"".$row->kode_kecamatan."\" selected=\"selected\">".strtoupper($row->kecamatan)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->kode_kecamatan."\">".strtoupper($row->kecamatan)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
                                    </div>
                                    <div>
                                        <span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestkX(this,'#pricecode')">
                                            <option value="">--Select Here--</option>
                                            <?php
                                            foreach($shipping['listStk'] as $row){
                                                if($row->loccd == $personal_info['stockist']) {
                                                    echo "<option value=\"".$row->loccd."\" selected=\"selected\">".strtoupper($row->fullnm)."</option>";
                                                } else {
                                                    echo "<option value=\"".$row->loccd."\">".strtoupper($row->fullnm)."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_stockist" name="nama_stockist" value="<?php echo $personal_info['nama_stockist'] ?>" />
                                    </div>
                                    <div class="clearfix"> </div>
                                    <!--  Options 1 lokasi-->
                                </div>
                            </div>
                            <!--  Options 1 detail-->
                        </div>
                        <!--  Options 1-->

                        <!--  Options 2-->
                        <div class="col-md-12 col-xs-12 nopadding voffset4">
                            <!--  Options 2 header-->
                            <div class="col-md-12 col-xs-12 delivery_header">
                                <input name="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/> Dikirim ke Alamat <!-- (Pilihan ini belum direkomendasikan/masih dalam pengembangan) -->
                            </div>
                            <!--  Options 2 header-->

                            <!--  Options 2 delivery address-->
                            <div class="col-md-12 col-xs-12 vpullset3 dikirim" style="display: none;">
                                <!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->
                                <div class="register-top-grid">
                                    <div>
                                        <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                                        <input type="text" class="required uppercase" name="nama_penerima" id="nama_penerima" value="" />
                                    </div>

                                    <div>
                                        <span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
                                        <input type="text" class="required numeric-input" name="notlp" id="notlp" value="" />
                                    </div>

                                    <div style="width:98%">
                                        <span>Email Address</span>
                                        <input type="text" name="email" id="email" value="" />
                                    </div>

                                    <div style="width:98%">
                                        <span>Alamat<label><font color="red">&nbsp;*</font></label></span>
                                        <textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"></textarea>
                                        <input type="hidden" id="destination_address" name="destination_address" value="" />
                                    </div>

                                    <div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
                                        Pilih Stockist Terdekat Dengan Alamat Tujuan
                                    </div>

                                    <div>
                                        <span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="shipper" id="shipper" onchange="Shopping.listProvinceByCargo(this)" >
                                            <option value="">--Select Here--</option>
                                            <?php
                                            //print_r($listCargo);
                                            /*foreach($listCargo as $row){
                                                echo "<option value=\"".$row->shipper_id."\">".strtoupper($row->shipper_name)."</option>";
                                            }*/
                                            //print_r($listCargo);
                                            foreach($listCargo as $row){
                                                $jam=date("H");
                                                $value= $row->shipper_id;
                                                $name=strtoupper($row->shipper_name);

                                                $now = new Datetime("now");
                                                if($value == 4 ){
                                                    if($now >= $begintime && $now <= $endtime){
                                                        $value='';
                                                        $name=$name;
                                                    } else {
                                                        $value='';
                                                        $name='GOSEND TUTUP (Jam Operasional 09:30 pagi - 17:00 sore)';
                                                    }
                                                }

                                                //echo "<option value=''>".$jam."</option>";
                                                echo "<option value=\"".$value."\">".$name."</option>";
                                            }
                                            ?>
                                        </select>

                                    </div>

                                    <div id="provDiv">
                                        <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')" >

                                        </select>
                                        <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="<?php echo $personal_info['nama_provinsi'] ?>" />
                                    </div>

                                    <div id="KabDiv">
                                        <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                        <!-- <select class="form-list" name="kota" id="kota" onchange="Shopping.show_kecamatan(this,'#kecamatan')"> -->
                                        <!--<select class="form-list" name="kota" id="kota" onchange="Shopping.show_stockistKGB(this,'#kecamatan')">
                                        </select>-->
                                        <select class="form-list" name="kota" id="kota" onchange="Shopping.show_kecamatan(this,'#kecamatan')">
                                        </select>

                                        <input type="hidden" id="nama_kota" name="nama_kota" value="<?php echo $personal_info['nama_kota'] ?>" />
                                    </div>
                                    <div id="KecDiv">
                                        <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                        <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                        <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >

                                        </select>
                                        <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
                                    </div>
                                    <!--<div id="provKGBDiv" style="display: none;">
                                        <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                         <select class="form-list" name="provKGB" id="provKGB" onchange="Shopping.show_kota_kgb(this,'#kotaKgb')" >
                                         <option value="">--Select Here--</option>

                                        </select>
                                        <input type="hidden" id="namaProvKGB" name="namaProvKGB" value="" />
                                    </div>
                                   <div id="kotaKGBDiv" style="display: none;">
                                        <span>Kota<label><font color="red">&nbsp;*</font></label></span>

                                         <select class="form-list" name="kotaKgb" id="kotaKgb" onchange="Shopping.show_kota_kgb(this,'#stockistref')" >

                                        </select>
                                        <input type="hidden" id="namaKotaKgb" name="namaKotaKgb" value="" />
                                   </div>-->
                                    <div>
                                        <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                        <!--<select class="form-list" name="stockistref" id="stockistref" onchange="tampilkanPriceCode(this,'#pricecode')">
                                        </select>-->
                                        <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">
                                        </select>

                                        <input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="" />
                                        <input type="hidden" id="sender_address" name="sender_address" value="" />
                                        <input type="hidden" id="jne_branch" name="jne_branch" value="" />
                                    </div>
                                    <div class="clearfix"> </div>

                                </div>
                            </div>
                            <!--  Options 2 delivery address-->
                        </div>
                        <?php
                        } else {
                        ?>
                        <!--  Options 1 header-->
                        <div class="col-md-12 col-xs-12 delivery_header">
                            <input name="delivery"  id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/>
                            Diambil di Stockist
                        </div>
                        <!--  Options 1 header-->

                        <!--  Options 1 detail-->
                        <div class="col-md-12 col-xs-12 diambil" style="display: none;">
                            <p class="p2 nopadding">
                                Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
                                <br/>
                                <br/>
                            </p>
                            <div class="register-top-grid">

                                <!--  Options 1 area-->

                                <div>
                                    <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
                                        <option value="">--Select Here--</option>
                                        <?php
                                        //print_r($show_provinsi);
                                        foreach($show_provinsi['arrayData'] as $row){

                                            echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";

                                        }

                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="<?php echo $personal_info['nama_provinsi'] ?>" />
                                </div>
                                <!--  Options 1 area-->

                                <!--  Options 1 lokasi-->
                                <div>
                                    <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">

                                    </select>
                                    <input type="hidden" id="nama_kota1" name="nama_kota1" value="<?php echo $personal_info['nama_kota'] ?>" />
                                    <!--<input type="text" name="kota" id="kota"/>-->
                                </div>
                                <div>
                                    <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                    <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                    <select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >

                                    </select>
                                    <input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
                                </div>
                                <div>
                                    <span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestkX(this,'#pricecode')">

                                    </select>
                                    <input type="hidden" id="nama_stockist" name="nama_stockist" value="<?php echo $personal_info['nama_stockist'] ?>" />
                                </div>
                                <div class="clearfix"> </div>
                                <!--  Options 1 lokasi-->
                            </div>
                        </div>
                        <!--  Options 1 detail-->
                    </div>
                <!--  Options 1-->

                <!--  Options 2-->
                    <div class="col-md-12 col-xs-12 nopadding voffset4">
                        <!--  Options 2 header-->
                        <div class="col-md-12 col-xs-12 delivery_header">
                            <input name="delivery" checked="checked" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/>
                            Dikirim ke Alamat
                        </div>
                        <!--  Options 2 header-->

                        <!--  Options 2 delivery address-->
                        <div class="col-md-12 col-xs-12 vpullset3 dikirim" >
                            <!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

                            <div class="register-top-grid">
                                <div>
                                    <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                                    <input type="text" class="required uppercase" name="nama_penerima" id="nama_penerima" value="<?php echo $personal_info['nama_penerima']; ?>" />
                                </div>

                                <div>
                                    <span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
                                    <input type="text" class="required numeric-input" name="notlp" id="notlp" value="<?php echo $personal_info['notlp']; ?>" />
                                </div>
                                <div style="width:98%">
                                    <span>Email Address</span>
                                    <input type="text" name="email" id="email" value="<?php echo $personal_info['email']; ?>" />
                                </div>

                                <div style="width:98%">
                                    <span>Alamat<label><font color="red">&nbsp;*</font></label></span>
                                    <textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"><?php echo $personal_info['alamat']; ?></textarea>
                                    <input type="hidden" id="destination_address" name="destination_address" value="<?php echo $this -> session -> userdata('destination_address'); ?>" />
                                </div>

                                <div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
                                    Pilih Stockist Terdekat Dengan Alamat Tujuan
                                </div>

                                <div>
                                    <span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="shipper" id="shipper" onchange="Shopping.listProvinceByCargo(this)" >
                                        <option value="">--Select Here--</option>
                                        <?php

                                        foreach ($listCargo as $row) {
                                            if ($row -> shipper_id == $personal_info['shipper']) {
                                                echo "<option value=\"" . $row -> shipper_id . "\" selected=\"selected\">" . strtoupper($row -> shipper_name) . "</option>";
                                            } else {
                                                echo "<option value=\"" . $row -> shipper_id . "\">" . strtoupper($row -> shipper_name) . "</option>";
                                            }

                                        }

                                        ?>
                                    </select>

                                </div>

                                <div id="provDiv">
                                    <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                    <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')" >
                                        <?php
                                        if($personal_info['shipper'] == "1") {
                                            foreach ($show_provinsi['arrayData'] as $row) {
                                                if ($row -> kode_provinsi == $personal_info['provinsi']) {
                                                    echo "<option value=\"" . $row -> kode_provinsi . "\" selected=\"selected\">" . strtoupper($row -> provinsi) . "</option>";
                                                } else {
                                                    echo "<option value=\"" . $row -> kode_provinsi . "\">" . strtoupper($row -> provinsi) . "</option>";
                                                }

                                            }
                                        } else {
                                            foreach ($show_provinsi['arrayData'] as $row) {
                                                if ($row -> provinsi == $personal_info['provinsi']) {
                                                    echo "<option value=\"" . $row -> provinsi . "\" selected=\"selected\">" . strtoupper($row -> provinsi) . "</option>";
                                                } else {
                                                    echo "<option value=\"" . $row -> provinsi . "\">" . strtoupper($row -> provinsi) . "</option>";
                                                }

                                            }
                                        }

                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="<?php echo $personal_info['nama_provinsi'] ?>" />
                                </div>

                                <div id="KabDiv">
                                    <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                    <!--<select class="form-list" name="kota" id="kota" onchange="Shopping.show_kecamatan(this,'#kecamatan')">--></select>
                                    <select class="form-list" name="kota" id="kota" onchange="Shopping.tampilkanKecamatan()">
                                        <?php
                                        if($personal_info['shipper'] == "1") {
                                            foreach ($shipping['kota'] as $row) {
                                                if ($row -> kode_kabupaten == $personal_info['kota']) {
                                                    echo "<option value=\"" . $row -> kode_kabupaten . "\" selected=\"selected\">" . strtoupper($row -> kabupaten) . "</option>";
                                                } else {
                                                    echo "<option value=\"" . $row -> kode_kabupaten . "\">" . strtoupper($row -> kabupaten) . "</option>";
                                                }

                                            }
                                        } else {
                                            foreach ($shipping['kota'] as $row) {
                                                if ($row -> KotaKode == $personal_info['kota']) {
                                                    echo "<option value=\"" . $row -> KotaKode . "\" selected=\"selected\">" . strtoupper($row -> KotaNama) . "</option>";
                                                } else {
                                                    echo "<option value=\"" . $row -> KotaKode . "\">" . strtoupper($row -> KotaNama) . "</option>";
                                                }

                                            }
                                        }

                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_kota" name="nama_kota" value="<?php echo $personal_info['nama_kota'] ?>" />
                                </div>
                                <?php
                                if($personal_info['shipper'] == "1") {
                                    ?>
                                    <div id="KecDiv">
                                        <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                        <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                        <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >
                                            <?php
                                            if($personal_info['shipper'] == "1") {
                                                foreach ($shipping['kecamatan'] as $row) {
                                                    if ($row -> kode_kecamatan == $personal_info['kecamatan']) {
                                                        echo "<option value=\"" . $row -> kode_kecamatan . "\" selected=\"selected\">" . strtoupper($row -> kecamatan) . "</option>";
                                                    } else {
                                                        echo "<option value=\"" . $row -> kode_kecamatan . "\">" . strtoupper($row -> kecamatan) . "</option>";
                                                    }

                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="<?php echo $personal_info['nama_kecamatan'] ?>" />
                                    </div>
                                    <?php
                                }
                                ?>
                                <!--<div id="provKGBDiv" style="display: none;">
                                    <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                     <select class="form-list" name="provKGB" id="provKGB" onchange="Shopping.show_kota_kgb(this,'#kotaKgb')" >
                                     <option value="">--Select Here--</option>

                                    </select>
                                    <input type="hidden" id="namaProvKGB" name="namaProvKGB" value="" />
                                </div>
                               <div id="kotaKGBDiv" style="display: none;">
                                    <span>Kota<label><font color="red">&nbsp;*</font></label></span>

                                     <select class="form-list" name="kotaKgb" id="kotaKgb" onchange="Shopping.show_kota_kgb(this,'#stockistref')" >

                                    </select>
                                    <input type="hidden" id="namaKotaKgb" name="namaKotaKgb" value="" />
                               </div>-->
                                <div>
                                    <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                    <!--<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">--></select>
                                    <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">
                                        <?php
                                        //if($personal_info['shipper'] == "1") {
                                        //foreach ($show_stk_by_area['arrayData'] as $dtax) {
                                        foreach($shipping['listStk'] as $dtax) {
                                            if ($dtax -> loccd == $personal_info['stockist']) {
                                                echo "<option value=\"$dtax->loccd\" selected=\"selected\">$dtax->loccd - $dtax->fullnm</option>";
                                            } else {
                                                echo "<option value=\"$dtax->loccd\">$dtax->loccd - $dtax->fullnm</option>";
                                            }
                                        }
                                        //}
                                        ?>
                                    </select>
                                    <input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="<?php echo $personal_info['nama_stockist']; ?>" />
                                    <input type="hidden" id="sender_address" name="sender_address" value="<?php echo $this -> session -> userdata('sender_address'); ?>" />
                                    <input type="hidden" id="jne_branch" name="jne_branch" value="<?php echo $this -> session -> userdata('jne_branch'); ?>" />
                                </div>
                                <div class="clearfix"> </div>


                            </div>
                        </div>
                        <!--  Options 2 delivery address-->
                    </div>
                <?php
                }
                ?>
                    <!--  Options 2-->
        </div>

        <div id="listCartPrd">
            <!-- Order Details-->
            <div class="col-md-6 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Detail Pembelanjaan
                </span>
                <div class="register-top-grid">
                    <div style="width:100%;">
                        <span>ID Member<label>*</label></span>
                        <input type="text" class="required uppercase" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo getUserID(); ?>"/>
                        <input type="button" id="checkIDMember" value="Check ID" class="btn1 btn2 btn-primary1" onclick="Shopping.getMemberNameByID(this.form.idmemberx, '#membername')" />
                        <!--                                    <input type="hidden" id="id_lp" name="id_lp" value="" />-->
                    </div>
                    <div>
                        <span>Nama Member<label>*</label></span>
                        <input readonly="readonly" class="required uppercase" type="text" name="membername" id="membername" value="<?php echo getUsername(); ?>"/>
                    </div>
                    <div>
                        <span>Periode Bonus<label>*</label></span>
                        <select name="bnsperiod" id="bnsperiod" class="form-list">
                            <?php

                            $xx = $bns['arrayData'];
                            for($i=0; $i <= $xx[0]->rangeperiod; $i++) {
                                $x = addRangeOneMonth($xx[0]->lastperiod, $i);
                                echo $x."<br />";
                                echo "<option value=\"$x\">$x</option>";
                            }
                            //echo "<option value=\"01/05/2010\">01/05/2010</option>";
                            ?>
                        </select>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <br />
                <?php
                $cart_check = $this->cart->contents();

                // If cart is empty, this will show below message.
                if(empty($cart_check)) {
                    echo '<br/>To add products to your shopping cart click on "Add to Cart" Button';
                }
                ?>
                <!-- header-->
                <div class="col-md-12 col-xs-12 cart_header">
                    <div class="col-md-2 col-xs-2">
                        Product
                    </div>
                    <div class="col-md-2 col-xs-2" align="center">
                        Qty
                    </div>

                    <div class="col-md-1 col-xs-1" align="right">
                        BV
                    </div>
                    <div class="col-md-2 col-xs-2" align="right">
                        Berat
                    </div>
                    <div class="col-md-2 col-xs-2" align="right">
                        Price
                    </div>
                    <div class="col-md-2 col-xs-2" align="right">
                        Amount
                    </div>

                    <div class="col-md-1 col-xs-1">
                        <i class="fa fa-trash-o"></i>
                    </div>
                </div>
                <!-- header-->

                <!-- product item-->
                <?php
                $n = 1;
                foreach($cart as $dt)
                {
                    $rowid = $dt['rowid'];
                    $prdcd = $dt['id'];
                    $prdnm = $dt['name'];
                    $westPrice = $dt['west_price'];
                    $westCPrice = $dt['west_Cprice'];
                    $qty = $dt['qty'];
                    $bv = $dt['bv'];
                    $eastPrice = $dt['east_price'];
                    $eastCPrice = $dt['east_Cprice'];
                    $weight = $dt['weight'];

                    ?>
                    <div id="<?php echo $n;?>">
                        <div class="col-md-12 col-xs-12 order_wrap">
                            <div class="col-md-2 col-xs-2 order_desc">
                                <a href="#">
                                    <input type="hidden" value="<?php echo $prdnm;?>" name="prdnm[]" id="<?php echo "prdnm".$n;?>" readonly="yes"/>
                                    <h3><?php echo $prdnm;?> </h3></a>
                                <!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
                            </div>
                            <div class="col-md-2 col-xs-2">
                                <input class="orderdetails-box" onclick="this.select()" onkeyup="setNewPriceAldi(<?php echo $n;?>)" type="text" value="<?php echo $qty;?>" name="qty[]" id="<?php echo "qty".$n;?>"/>
                            </div>
                            <div class="col-md-1 col-xs-1" align="right">
                                <?php

                                echo $bv;
                                ?>
                                <input class="orderdetails-box" type="hidden" value="<?php echo $bv;?>" name="bv[]" id="<?php echo "bv".$n;?>" readonly="yes"/>

                            </div>
                            <div class="col-md-2 col-xs-2 weightP" align="right" id="<?php echo "divSubTotWeightPrice".$n ?>">
                                <?php echo "".number_format(($dt['subtotal_weight']),2,",",",");?> Kg


                            </div>
                            <div class="col-md-2 col-xs-2 westP" align="right"><?php echo "".number_format($westPrice,0,",",".")."";?>
                                <input class="orderdetails-box" type="hidden" value="<?php echo $weight;?>" name="weightPrice[]" id="<?php echo "weightPrice".$n;?>" readonly="yes"/>
                                <input class="orderdetails-box" type="hidden" value="<?php echo $westPrice;?>" name="westPrice[]" id="<?php echo "westPrice".$n;?>" readonly="yes"/>
                            </div>
                            <div class="col-md-2 col-xs-2 westP" align="right" id="<?php echo "divSubTotWestPrice".$n ?>">
                                <?php echo "".number_format($dt['subtotal_west_price'],0,",",".").""; ?>

                            </div>

                            <div class="col-md-2 col-xs-2 eastP" align="right" style="display: none;"><?php echo "".number_format($eastPrice,0,",",".").""; ?>
                                <input class="orderdetails-box" type="hidden" value="<?php echo $eastPrice;?>" name="eastPrice[]" id="<?php echo "eastPrice".$n;?>" readonly="yes"/>
                            </div>
                            <div class="col-md-2 col-xs-2 eastP" align="right" id="<?php echo "divSubTotEastPrice".$n ?>" style="display: none;" ><?php echo "".number_format($dt['subtotal_east_price'],0,",",",").""; ?>

                            </div >

                            <!--cp-->
                            <div class="col-md-2 col-xs-2 westCP" align="right" style="display: none;"><?php echo "".number_format($westCPrice,0,",",".").""; ?>
                                <input class="orderdetails-box" type="hidden" value="<?php echo $westCPrice;?>" name="westCPrice[]" id="<?php echo "westCPrice".$n;?>" readonly="yes"/>
                            </div>
                            <div class="col-md-2 col-xs-2 westCP" align="right" id="<?php echo "divSubTotWestCPrice".$n ?>" style="display: none;" ><?php echo "".number_format($dt['subtotal_west_Cprice'],0,",",",").""; ?>

                            </div >

                            <div class="col-md-2 col-xs-2 eastCP" align="right" style="display: none;"><?php echo "".number_format($eastCPrice,0,",",".").""; ?>
                                <input class="orderdetails-box" type="hidden" value="<?php echo $eastCPrice;?>" name="eastCPrice[]" id="<?php echo "eastCPrice".$n;?>" readonly="yes"/>
                            </div>
                            <div class="col-md-2 col-xs-2 eastCP" align="right" id="<?php echo "divSubTotEastCPrice".$n ?>" style="display: none;" ><?php echo "".number_format($dt['subtotal_east_Cprice'],0,",",",").""; ?>

                            </div >
                            <!--
                            <div class="col-md-3 col-xs-3 divTotBV">

                            </div>
                            -->
                            <div class="col-md-1 col-xs-1 text-center">
                                <input type="hidden" id="<?php echo "rowid".$n; ?>" value="<?php echo $rowid;?>" name="rowid[]"/>
                                <a href="#" id="<?php echo $rowid;?>" onclick="delete_rowCart(<?php echo $n; ?>)"><i class="fa fa-ban"></i></a>
                            </div>
                            <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_price']; ?>" name="amountWest[]" id="<?php echo "amountWest".$n;?>" readonly="yes"/>
                            <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_Cprice']; ?>" name="amountCWest[]" id="<?php echo "amountCWest".$n;?>" readonly="yes"/>
                            <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_Cprice']; ?>" name="amountCEast[]" id="<?php echo "amountCEast".$n;?>" readonly="yes"/>
                            <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_weight']; ?>" name="amountWeight[]" id="<?php echo "amountWeight".$n;?>" readonly="yes"/>
                            <input class="amtE orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_price'];?>" name="amountEast[]" id="<?php echo "amountEast".$n;?>" readonly="yes"/>
                            <?php $sub_totalbv = $qty * $bv; ?>
                            <input class="amtE orderdetails-box" type="hidden" value="<?php echo $sub_totalbv; ?>" name="amountBV[]" id="<?php echo "amountBV".$n;?>" readonly="yes"/>
                        </div>

                    </div>
                    <!-- product item-->
                    <?php
                    $n++;
                }
                $n++;
                ?>
                <div id="<?php echo $n;?>">
                    <div class="col-md-12 col-xs-12 order_wrap">
                        <div class="col-md-2 col-xs-2 order_desc">
                            <a href="#">

                                <h3>Biaya Kirim</h3></a>
                            <!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
                        </div>
                        <div class="col-md-2 col-xs-2">

                        </div>
                        <div class="col-md-1 col-xs-1" align="right">

                        </div>
                        <div class="col-md-2 col-xs-2 weightP" align="right" >

                        </div>
                        <div class="col-md-2 col-xs-2 " align="right">

                        </div>
                        <div class="col-md-2 col-xs-2 " align="right" id="<?php echo "divAmountKirim" ?>" >
                            0
                        </div>
                        <input class="amtW orderdetails-box" type="hidden" value="<?php echo 0; ?>" name="amountKIRIM[]" id="<?php echo "amountKIRIM";?>" readonly="yes"/>

                        <!--
                        <div class="col-md-3 col-xs-3 divTotBV">

                        </div>
                        -->
                        <div class="col-md-1 col-xs-1 text-center">

                        </div>

                    </div>

                </div>
                <!-- subtotal -->
                <div class="col-md-12 col-xs-12 order_total_wrap westP">
                    <div class="col-md-2 col-xs-2">SUBTOTAL</div>
                    <div class="col-md-2 col-xs-2" id="divTotQtyWest"><?php echo $this->cart->total_items(); ?>

                    </div>
                    <div class="col-md-1 col-xs-1" align="right" id="divTotBVWest">
                        <?php echo $this->cart->total_bv();?>

                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>

                    <div class="col-md-2 col-xs-2 divTotWeight" id="divTotWeight">

                        <?php echo "".number_format($this->cart->total_weight(),2,",",",");?> Kg
                    </div>
                    <div class="col-md-2 col-xs-2">
                        -
                        <input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_west_price');?>" name="totalWestPrice" id="totalWestPrice" readonly="yes"/>
                    </div>
                    <div class="col-md-2 col-xs-2" id="totAllWestPrice" align="right"><?php echo "".number_format($this->cart->total_west_price(),0,",",",");?>

                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items(); ?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_west_price();?>" name="totalAmountWest" id="totalAmountWest" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_weight();?>" name="totalAmountWeight" id="totalAmountWeight" readonly="yes"/>
                </div>
                <div class="col-md-12 col-xs-12 order_total_wrap eastP " style="display: none;">
                    <div class="col-md-2 col-xs-2">SUBTOTAL</div>
                    <div class="col-md-2 col-xs-2" id="divTotQtyEast"><?php echo $this->cart->total_items();?>

                    </div>
                    <div class="col-md-1 col-xs-1" align="right" id="divTotBVEast">
                        <?php echo $this->cart->total_bv();?>
                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>

                    <div class="col-md-2 col-xs-2 divTotWeight" id="divTotWeight">
                        <?php echo "".number_format($this->cart->total_weight(),2,",",",");?> Kg
                    </div>
                    <div class="col-md-2 col-xs-2">
                        -
                        <input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_east_price');?>" name="totalEastPrice" id="totalEastPrice" readonly="yes"/>
                    </div>
                    <div class="col-md-2 col-xs-2" id="totAllEastPrice" align="right"><?php echo "".number_format($this->cart->total_east_price(),0,",",",");?>

                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items();?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_east_price();?>" name="totalAmountEast" id="totalAmountEast" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_weight();?>" name="totalAmountWeight" id="totalAmountWeight" readonly="yes"/>

                </div>
                <div class="col-md-12 col-xs-12 order_total_wrap eastCP " style="display: none;">
                    <div class="col-md-2 col-xs-2">SUBTOTAL</div>
                    <div class="col-md-2 col-xs-2" id="divTotQtyEast"><?php echo $this->cart->total_items();?>

                    </div>
                    <div class="col-md-1 col-xs-1" align="right" id="divTotBVEast">
                        <?php echo $this->cart->total_bv();?>
                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>

                    <div class="col-md-2 col-xs-2 divTotWeight" id="divTotWeight">
                        <?php echo "".number_format($this->cart->total_weight(),2,",",",");?> Kg
                    </div>
                    <div class="col-md-2 col-xs-2">
                        -
                        <input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_east_price');?>" name="totalEastCPrice" id="totalEastCPrice" readonly="yes"/>
                    </div>
                    <div class="col-md-2 col-xs-2" id="totAllEastCPrice" align="right"><?php echo "".number_format($this->cart->total_east_Cprice(),0,",",",");?>

                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items();?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_east_Cprice();?>" name="totalAmountCEast" id="totalAmountCEast" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_weight();?>" name="totalAmountWeight" id="totalAmountWeight" readonly="yes"/>

                </div>
                <div class="col-md-12 col-xs-12 order_total_wrap westCP " style="display: none;">
                    <div class="col-md-2 col-xs-2">SUBTOTAL</div>
                    <div class="col-md-2 col-xs-2" id="divTotQtyWest"><?php echo $this->cart->total_items();?>

                    </div>
                    <div class="col-md-1 col-xs-1" align="right" id="divTotBVWest">
                        <?php echo $this->cart->total_bv();?>
                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>

                    <div class="col-md-2 col-xs-2 divTotWeight" id="divTotWeight">
                        <?php echo "".number_format($this->cart->total_weight(),2,",",",");?> Kg
                    </div>
                    <div class="col-md-2 col-xs-2">
                        -
                        <input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_east_price');?>" name="totalWestCPrice" id="totalWestCPrice" readonly="yes"/>
                    </div>
                    <div class="col-md-2 col-xs-2" id="totAllWestCPrice" align="right"><?php echo "".number_format($this->cart->total_west_Cprice(),0,",",",");?>

                    </div>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items();?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_west_Cprice();?>" name="totalAmountCWest" id="totalAmountCWest" readonly="yes"/>
                    <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_weight();?>" name="totalAmountWeight" id="totalAmountWeight" readonly="yes"/>

                </div>
                <!-- subtotal

                <!-- subtotal -->
                <div class="col-md-12 col-xs-12 order_add">
                    <a href="<?php echo "".site_url('shop/product/')."";?>">
                        <i class="fa fa-shopping-cart"></i> Tambahkan Lagi Keranjang Belanja Anda..
                    </a>
                </div>
                <?php
                if (getUserID()=='IDSPAAA66834'){
                    ?>

                    <?php
                }
                ?>
                <!-- subtotal -->
            </div>
            <!-- Order Details-->
        </div>

        <!-- next button-->
        <div class="col-md-12">

            <input type="hidden" name="pricecode" id="pricecode" value="<?php echo $this->session->userdata('pricecode'); ?>" />
            <!--<a href="payment.php" class="btn1 btn2 btn-primary1 pull-left"><span>Lanjutkan</span><i class="fa fa-arrow-right"></i></a>-->

            <!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" onclick="Shopping.checkout()" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />-->
            <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjutkeun  <i class="fa fa-arrow-right"></i></button><br />
        </div>
        <!-- next button-->
    </div>
    </form>
    <!-- Delivery Options-->
</div> <!-- End formCart1-->
<div id="divCheckOut"></div>
<div id="afterPayment"></div>
</div>
</div>
<!--Checkout Wrapper-->

<style>
    .delivery_header {
        border-bottom: 1px solid #d0d0d0;
        border-top: 1px solid #d0d0d0;
        float: left;
        font-size: 12px;
        margin: 8px 0 1em;
        padding: 1em 0;
        text-transform: none;
    }

    .xcx {
        line-height:10px;
        padding:4px;
    }
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Payment Preview&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Preview Pembayaran&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
    <div class="container">
        <div id="div_info1" class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <?php
            //$shipping_jne_info = $this->session->userdata('shipping_jne_info');
            //$biaya = getTotalPayNetBaru();
            //$dt['freeship'] = $biaya['freeship'];
            //$dt['shipping'] = $shipping_jne_info['price'];
            //$dt['discount_shipping'] = $shipping_jne_info['ship_discount'];
            $tot_hrs_dibayar = $summary['total_pay'] + $summary['payShip'] - $summary['discount_shipping'];
            //print_r($dt);
            //echo "<br />";
            //print_r($dt);
            if($pay_gateway == true) {

                //pembayaran dengan voucher, jumlah total nilai voucher lebih kecil daripada total nilai pembelanjaan
                //sehingga sisa pembayaran di bayar menggunakan payment gateway
                if($vch_stt == true) {
                    //$total_semua = $tot_hrs_dibayar  + $res[0]->charge_connectivity  + $res[0]->charge_admin;
                    $total_semua = $tot_hrs_dibayar  + $summary['payConnectivity']  + $summary['payAdm'];
                    $sisa = $total_semua - $tot_nilai_voucher;
                    $sisaKotor = $tot_hrs_dibayar - $tot_nilai_voucher;
                } else {
                    //Pembayaran hanya dengan menggunakan payment gateway, tidak menggunakan voucher sama sekali
                    //$total_semua = $tot_hrs_dibayar +  $res[0]->charge_connectivity  + $res[0]->charge_admin;
                    $total_semua = $tot_hrs_dibayar  + $summary['payConnectivity']  + $summary['payAdm'];
                    $sisa = $total_semua;

                }

                echo "<div class=\"col-lg-12 col-xs-12 delivery_header\" style=\"font-size:18px;\">Preview Pembayaran via ".$bankDescDetail."</div>";
                echo "<div class=\"col-lg-12 col-xs-12 nomargin\">";
                echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
                echo "<table width=80% align=center border=0>";
                echo "<tr><td align=right>Tipe Pembayaran         :</td><td align=right>".$bankDescDetail."</td></tr>";
                echo "<tr><td align=right>Total Pembelanjaan      :</td><td align=right>Rp. ".number_format($tot_pay, 2, ",", ".")."</td></tr>";
                echo "<tr><td align=right>Total BV                :</td><td align=right>".number_format(getTotalBV(), 2, ",", ".")."</td></tr>";
                //echo "<tr><td align=right>Biaya Connectivity      :</td><td align=right>Rp. ".number_format($res[0]->charge_connectivity, 2, ",", ".")."</td></tr>";
                echo "<tr><td align=right>Biaya Connectivity      :</td><td align=right>Rp. ".number_format($summary['payConnectivity'], 2, ",", ".")."</td></tr>";
                //echo "<tr><td align=right>Biaya Administrasi      :</td><td align=right>Rp. ".number_format($res[0]->charge_admin, 2, ",", ".")."</td></tr>";
                echo "<tr><td align=right>Biaya Administrasi      :</td><td align=right>Rp. ".number_format($summary['payAdm'], 2, ",", ".")."</td></tr>";
                //if($freeship == "1") {
                if($summary['discount_shipping'] > 0) {
                    echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($summary['payShip'], 2, ",", ".")."</td></tr>";
                    echo "<tr><td align=right>Disc Biaya Kirim        :</td><td align=right>Rp. -(".number_format($summary['discount_shipping'], 2, ",", ".").")</td></tr>";
                } else {
                    echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($summary['payShip'], 2, ",", ".")."</td></tr>";
                }

                /*} else {
                     echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                }*/
                echo "<tr><td align=right>Total                   :</td><td align=right>Rp. ".number_format($total_semua, 2, ",", ".")."</td></tr>";
                echo "<tr><td align=right>Total Nilai Voucher     :</td><td align=right>(-) Rp. ".number_format($tot_nilai_voucher, 2, ",", ".")."</td></tr>";

                echo "<tr><td align=right>Sisa yang harus dibayar :</td><td align=right>".number_format($sisa, 2, ",", ".")."</td></tr>";
                echo "</table>";

                echo "<button class=\"btn2 btn2 btn-primary1\" onclick=\"submitdataXX()\">";
                echo "<i class=\"fa fa-arrow-right\"></i>";
                echo "<span>Proses Pembayaran</span>";
                echo "</button>";
                echo "<input type=hidden name=temp_orderid value=\"$temp_paymentIdx\" />";
                echo "<input type=\"hidden\" id=\"total_all\" value=\"$prd_value_minus_voucher\" />";
                echo "<input type=\"hidden\" id=\"sisa_hrs_dibayar\" value=\"$sisa\" />";
                echo"</p>";
                /*echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
                echo $listVch;
                 echo"</p>";*/
                echo "</div>";
                echo "<iframe id=\"sgoplus-iframe\" src=\"\" scrolling=\"no\" frameborder=\"0\"></iframe>";

            }  else {
                //Jika pembayaran hanya dengan voucher belanja
                //Jumlah nilai total voucher lebih besar atau sama dengan total nilai pembelanjaan
                echo "<div class=\"col-lg-12 col-xs-12 delivery_header\" style=\"font-size:18px;\">Preview Pembayaran via Voucher Belanja</div>";
                echo "<div class=\"col-lg-12 col-xs-12 nomargin\">";
                echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
                echo "<table width=70% align=center border=0>";
                echo "<tr><td align=right>Tipe Pembayaran       :</td><td align=right>Voucher Belanja</td></tr>";
                echo "<tr><td align=right>Total Pembelanjaan    :</td><td align=right>Rp. ".number_format($tot_pay, 2, ",", ".")."</td></tr>";

                echo "<tr><td align=right>Total BV              :</td><td align=right>".number_format(getTotalBV(), 2, ",", ".")."</td></tr>";
                //echo "<tr><td align=right>Biaya Kirim           :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                /*if($freeship == "1") {
                     echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. 0</td></tr>";
                } else {
                     echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                }*/

                if($summary['discount_shipping'] > 0) {
                    echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($summary['payShip'], 2, ",", ".")."</td></tr>";
                    echo "<tr><td align=right>Disc Biaya Kirim        :</td><td align=right>Rp. -(".number_format($summary['discount_shipping'], 2, ",", ".").")</td></tr>";
                } else {
                    echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($summary['payShip'], 2, ",", ".")."</td></tr>";
                }
                $total_semua = $tot_pay;
                echo "<tr><td align=right>Total                 :</td><td align=right>".number_format($total_semua, 2, ",", ".")."</td></tr>";
                echo "</table>";
                $url = base_url('pay/vch');
                echo "<form method=POST action=\"$url\">";
                echo "<button id=\"btn_submit\" class=\"btn2 btn2 btn-primary1\">";
                echo "<i class=\"fa fa-arrow-right\"></i>";
                echo "<span>Proses Pembayaran dengan Voucher</span>";
                echo "</button>";
                echo "<input type=hidden name=temp_orderid value=\"$temp_paymentIdx\" />";
                echo "</form>";
                echo"</p>";
                /*echo "</div>";
                echo "<br />";
                echo "<br />";
                echo "<div class=\"col-lg-12 col-xs-12 nomargin\">"; */
                /*echo "<p class=\"p2 nopadding\" style=\"font-size:16px;\">";
                echo $listVch;
                 echo"</p>";*/
                echo "</div>";
            }
            ?>
        </div>
        <div id="wait_message"></div>
    </div>
</div>

<script type="text/javascript">

    function submitdataXX() {
        var total_all = $("#total_all").val();
        var tipe_bank = "<?php echo $bankCode; ?>";
        var pay_amount = $("#total_all").val();
        var backUrlRedirect = "<?php echo $backURL;?>";
        //var pay_amount = 2300000;
        var temp_orderno = "<?php echo $temp_paymentIdx;?>";

        if(tipe_bank == "VAC") {
            $("#btn_submit").attr("disabled", "disabled");
//		 $("#div_info1").css("display", "none");
            var x = document.getElementById("div_info1");
            x.style.display = "none";
            $("#wait_message").html(null);
            $("#wait_message").html("<p>Mohon tunggu sebentar, transaksi sedang diproses, jangan refresh halaman ini..</p>");
            var hrs_dibayar = $("#sisa_hrs_dibayar").val();
            $.ajax({
                //url: All.get_url('sgo_ppob/saveTemp'),
                url: All.get_url('cart/pay/va2'),
                type: 'POST',
                data: {pay_amount: hrs_dibayar, temp_orderno: temp_orderno, tipe_bank: tipe_bank},
                dataType: 'json',
                success:
                    function(data){

                        var response = data.response;
                        if(response == "true") {
                            //window.location.replace(backUrlRedirect);
                            window.location.href = backUrlRedirect
                        } else {
                            alert(data.message);
                            $("#btn_submit").removeAttr("disabled");
                            $("#div_info1").css("display", "block");
                            $("#wait_message").html(null);
                        }
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' +xhr.status);
                    $("input[type=button]").removeAttr('disabled');
                }
            });

            /*
             var form = document.createElement("form");
             form.setAttribute("method", "POST");
             form.setAttribute("action", All.get_url('cart/pay/va'));

             var hiddenField = document.createElement("input");
             hiddenField.setAttribute("type", "hidden");
             hiddenField.setAttribute("name", "pay_amount");
             hiddenField.setAttribute("value", pay_amount);
             var hiddenField2 = document.createElement("input");
             hiddenField2.setAttribute("type", "hidden");
             hiddenField2.setAttribute("name", "temp_orderno");
             hiddenField2.setAttribute("value", temp_orderno);
             var hiddenField3 = document.createElement("input");
             hiddenField3.setAttribute("type", "hidden");
             hiddenField3.setAttribute("name", "tipe_bank");
             hiddenField3.setAttribute("value", tipe_bank);
             form.appendChild(hiddenField);
             form.appendChild(hiddenField2);
             form.appendChild(hiddenField3);
             document.body.appendChild(form);
             form.submit();	*/
        } else {

            var data = {
                    key : "<?php echo $keySgo;?>",
                    paymentId : "<?php echo $temp_paymentIdx;?>",
                    paymentAmount : pay_amount,
                    backUrl : backUrlRedirect,
                    bankCode : tipe_bank,
                    bankProduct: "<?php echo $bankDesc; ?>"
                },
                sgoPlusIframe = document.getElementById("sgoplus-iframe");

            if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
            SGOSignature.receiveForm();
        }

    }
</script>
<!-- Login Form -->
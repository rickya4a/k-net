<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Shipping&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Konfirmasi Pembayaran&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
    <div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">Transaksi Pembelanjaan Produk</div>
            <div class="col-lg-12 col-xs-12 nomargin">
                <!--<p class="p2 nopadding" style="font-size:16px;">-->
                <?php

                //echo "<div class='pay_label'>ID Transaksi          : </div><div class='pay_result'>".$trans[0]->orderno.""."</div>";
                //echo "<div class='pay_label'>ID Member             : </div><div class='pay_result'>".$trans[0]->id_memb.""."</div>";
                //echo "<div class='pay_label'>Nilai transaksi       : </div><div class='pay_result'>Rp."."".number_format($trans[0]->total_pay, 2, ".", ",").""."</div>";
                //echo "<div class='pay_label'>Biaya Kirim           : </div><div class='pay_result'>Rp."."".number_format($trans[0]->payShip, 2, ".", ",").""."</div>";
                //$tot = $trans[0]->total_pay + $trans[0]->payShip;
                //echo "<div class='pay_label'>Total                 : </div><div class='pay_result'>Rp.".number_format($tot, 2, ".", ",")."</div>";
                //echo "<div class='pay_label'>ID Stockist           : </div><div class='pay_result'>".$trans[0]->nmstkk."</div>";
                //echo "<div class='pay_label'>Waktu transaksi       : </div><div class='pay_result'>".$trans[0]->status_vt_reject_dt."</div>";

                //echo "No Trx                : $trx_no"."<br />";
                //echo "No Resi Pengiriman    : $jne"."<br />";

                //print_r($trx_no);
                if($trxInsertStatus == "ok") {
                    if ($trans[0]->free_shipping == "1") {
                        $tot = $trans[0]->total_pay + $trans[0]->payAdm + $trans[0]->payConnectivity;
                        $payShip = 0;
                    } else {
                        $tot = $trans[0]->total_pay + $trans[0]->payShip + $trans[0]->payAdm + $trans[0]->payConnectivity;
                        $payShip = $trans[0]->payShip;
                    }

                    ?>
                    <table border="0" cellspacing="1" cellpadding="1">
                        <tr>
                            <td colspan="2" align="center"><strong>Transaksi anda berhasil</strong></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="200" align="right">ID Transaksi&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;<?php echo $trans[0]->orderno; ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">ID Member&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;<?php echo $trans[0]->id_memb; ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">Total Pembelanjaan&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;Rp. <?php echo number_format($trans[0]->total_pay, 2, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">Total BV&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;<?php echo number_format($trans[0]->total_bv, 2, ",", "."); ?></td>
                        </tr>


                        <tr>
                            <td width="200" align="right">Biaya Kirim&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;Rp. <?php echo number_format($payShip, 2, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">Biaya Connectivity:&nbsp;</td>
                            <td align="left">
                                &nbsp;Rp. <?php echo number_format($trans[0]->payConnectivity, 2, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">Biaya Administrasi&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;Rp. <?php echo number_format($trans[0]->payAdm, 2, ",", "."); ?></td>
                        </tr>

                        <tr>
                            <td width="200" align="right">Total&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp;Rp. <?php echo number_format($tot, 2, ".", ","); ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">ID Stockist&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp; <?php echo $trans[0]->nmstkk; ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">Waktu Transaksi&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp; <?php echo $trans[0]->datetrans; ?></td>
                        </tr>
                        <tr>
                            <td width="200" align="right">No Resi&nbsp;:&nbsp;</td>
                            <td align="left">&nbsp; <?php echo $jne; ?></td>
                        </tr>
                        <?php if (isset($catalog)) {
                            echo '
							<tr>
								<td colspan="2"><b>Link Download Catalogue</b></td>

							</tr>
							';
                            foreach ($catalog as $row){
                                ?>
                                <tr>
                                    <td width="200" align="right"><?php echo $row->cat_inv_desc ?>&nbsp;:&nbsp;</td>
                                    <td align="center">

                                        <a class="btn btn-success" href="<?php echo base_url('catalogue/download/'.$token.'/'.$row->cat_inv_id); ?>">
                                            <i class="fa fa-arrow-left"></i>
                                            <span>Download</span>
                                        </a>

                                    </td>
                                </tr>
                                <?php
                            }

                        }?>
                    </table>
                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">
                        <!--<a href="<?php //echo base_url('shop/product'); ?>">Klik disini untuk kembali ke halaman utama</a>-->
                        <a class="btn1 btn2 btn-primary1" href="<?php echo base_url('shop/product'); ?>">
                            <i class="fa fa-arrow-left"></i>
                            <span>Klik disini untuk kembali ke halaman utama</span>
                        </a>
                    </div>
                    <?php
                } else if($trxInsertStatus == "pending") {
                    echo "Transaksi menunggu pembayaran selesai diproses";
                    ?>
                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">
                        <!--<a href="<?php //echo base_url('shop/product'); ?>">Klik disini untuk kembali ke halaman utama</a>-->
                        <a class="btn1 btn2 btn-primary1" href="<?php echo base_url('cart/list'); ?>">
                            <i class="fa fa-arrow-left"></i>
                            <span>Kembali ke keranjang belanja</span>
                        </a>
                    </div>
                    <?php
                } else {
                    echo "Transaksi Gagal, Transaksi Anda tidak dapat diproses";
                    ?>
                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">
                        <!--<a href="<?php //echo base_url('shop/product'); ?>">Klik disini untuk kembali ke halaman utama</a>-->
                        <a class="btn1 btn2 btn-primary1" href="<?php echo base_url('cart/list'); ?>">
                            <i class="fa fa-arrow-left"></i>
                            <span>Kembali ke keranjang belanja</span>
                        </a>
                    </div>
                    <?php
                }
                ?>
                <!--</p>-->

            </div>
        </div>
    </div>
</div>
<!-- Login Form --> 
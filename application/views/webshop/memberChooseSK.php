<!-- Breadcrumb -->
<ul class="breadcrumbs">
	<div class="container">
		<li class="home">
			<a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png'); ?>" alt=""/></a>&nbsp;
			&nbsp; <span>&gt;</span>
		</li>
		<li class="home">
			&nbsp;
			Shopping&nbsp;&nbsp;
			<span>&gt;</span>
		</li>
		<li class="home">
			<span class="red"> &nbsp;Pengiriman&nbsp;&nbsp;</span>
		</li>
	</div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
		<div id="formCart1">
			<?php
	      	  $usr = getUserID();
			  if($usr == "IDSPAAA66834") {
	      	?> 
	        <form id="formMember" action="<?php echo base_url('member/checkout/sgo'); ?>" method="POST" onsubmit="return Shopping.validateStarterKit_baru()">        
	    	<?php
			  } else {
	    	?>
	    	 <form id="formMember" action="<?php echo base_url('member/checkout'); ?>" method="POST" onsubmit="return Shopping.validateStarterKit_baru()">
	    	<?php
			  }
	    	?>
			<!--<form id="formMember" action="<?php echo base_url('member/checkout'); ?>" method="POST" onsubmit="return Shopping.validateStarterKit()">-->

				<!-- Delivery Options-->
				<br />
				<div class="col-md-6 col-xs-12">
					<span class="title_header"> <i class="fa fa-truck"></i> Tipe Pengiriman Starterkit </span>
<!-- ul><li class="red">Pengumuman : berkenaan dengan libur hari raya idul fitri 2017, maka pengiriman akan dihentikan sementara mulai tanggal 22 Juni 2017 s/d 04 Juli 2017.</li></ul -->
					<!--  Options 1-->
					<div class="col-md-12 col-xs-12 nopadding voffset4">

						<!--  Options 1 header-->
						<div class="col-md-12 col-xs-12 delivery_header">
							<input name="delivery" id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/>
							Diambil di Stockist
						</div>
						<!--  Options 1 header-->

						<!--  Options 1 detail-->
						<div class="col-md-12 col-xs-12 diambil" style="display: none;">
							<p class="p2 nopadding">
								Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih
								<br/>
								<br/>
							</p>
							<div class="register-top-grid">

								<div>
									<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
										<option value="">--Select Here--</option>
										<?php
										//print_r($show_provinsi);
										foreach ($show_provinsi['arrayData'] as $row) {
											echo "<option value=\"" . $row->kode_provinsi . "\">" . strtoupper($row->provinsi) . "</option>";
										}
										?>
									</select>
									<input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="" />
								</div>
								<div>
									<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')"></select>
									<input type="hidden" id="nama_kota1" name="nama_kota1" value="" />
									
									<!--<input type="text" name="kota" id="kota"/>-->
								</div>
								<div>
									<span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
									<!--<input type="text" name="kecamatan" id="kecamatan"/>-->
									<select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >

									</select>
									<input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="" />
									<input type="hidden" id="state1" name="state1" value="" />
								</div>

								<!--  Options 1 lokasi-->
								<div>
									<span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
									<select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestk2(this,'#pricecode')"></select>
									<input type="hidden" id="nama_stockist" name="nama_stockist" value="" />
								</div>
								<div class="clearfix"></div>
								<!--  Options 1 lokasi-->
							</div>
						</div>
						<!--  Options 1 detail-->
					</div>
					<!--  Options 1-->

					<!--  Options 2-->
					<div class="col-md-12 col-xs-12 nopadding voffset4">
						<!--  Options 2 header-->
						<div class="col-md-12 col-xs-12 delivery_header">
							<input name="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/>
							Dikirim ke Alamat
						</div>
						<!--  Options 2 header-->

						<!--  Options 2 delivery address-->
						<div class="col-md-12 col-xs-12 vpullset3 dikirim" style="display: none;">
							<!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

							<div class="register-top-grid">
								<div>
									<span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
									<input type="text" class="uppercase" name="nama_penerima" id="nama_penerima" maxlength="25"/>
								</div>

								<div>
									<span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
									<input class="numeric-input" type="text" class="uppercase" name="notlp" id="notlp"/>
								</div>
								<div style="width:98%">
									<span>Email Address</span>
									<input type="text" name="email" id="email"/>
								</div>

								<div style="width:98%">
									<span>Alamat<label><font color="red">&nbsp;*</font></label></span>
									<textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"></textarea>
									<input type="hidden" class="uppercase" id="destination_address" name="destination_address" value="" />
								</div>
								
								
								<div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
									Pilih Stockist Terdekat Dengan Alamat Tujuan
								</div>
								
                           <div>
                                <span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
                                 <select class="form-list" name="shipper" id="shipper" onchange="Shopping.listProvinceByCargo(this)" >
                                 <option value="">--Select Here--</option>
                                 <?php
                                    //print_r($listCargo);
                                    foreach($listCargo as $row){
                                        echo "<option value=\"".$row->shipper_id."\">".strtoupper($row->shipper_name)."</option>";
                                    }
                                 
                                 ?>
                                </select>
                                
                            </div>
                            
                            <div id="provDiv">
                            	<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                 <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')" >
                                 
                                </select>
                                <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
                            </div>
                          
                            <div id="KabDiv">
                            	<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                <select class="form-list" name="kota" id="kota" onchange="Shopping.tampilkanKecamatan()">
                                </select>
                                <input type="hidden" id="nama_kota" name="nama_kota" value="" />
                            </div>
                             <div id="KecDiv">
                            	  <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                 <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                 <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan(this,'#stockistref')" >
                                 
                                </select> 
                                <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
                            </div>
                            <!--<div id="provKGBDiv" style="display: none;">
                            	<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                 <select class="form-list" name="provKGB" id="provKGB" onchange="Shopping.show_kota_kgb(this,'#kotaKgb')" >
                                 <option value="">--Select Here--</option>
                                 
                                </select>
                                <input type="hidden" id="namaProvKGB" name="namaProvKGB" value="" />
                            </div>
                           <div id="kotaKGBDiv" style="display: none;">
                                <span>Kota<label><font color="red">&nbsp;*</font></label></span>
                                 
                                 <select class="form-list" name="kotaKgb" id="kotaKgb" onchange="Shopping.show_kota_kgb(this,'#stockistref')" >
                                 
                                </select> 
                                <input type="hidden" id="namaKotaKgb" name="namaKotaKgb" value="" />
                           </div>-->
                            <div>
                                <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                 <!--<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">-->
                                <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.tampilkanPriceCode()">
                                </select>
                                <input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="" />
                                <input type="hidden" id="sender_address" name="sender_address" value="" />
                                <input type="hidden" id="jne_branch" name="jne_branch" value="" />
                            </div>
                           <div class="clearfix"> </div>
                           
							</div>
						</div>
						<!--  Options 2 delivery address-->
					</div>
					<!--  Options 2-->
				</div>

				<div id="listCartPrd">
					<!-- Order Details-->
					<div class="col-md-6 col-xs-12">
						<span class="title_header"> <i class="fa fa-check-square-o"></i> Detail Data Member </span>
						
						<?php
						$member_info = $this->session->userdata("member_info");
						//echo "ID Member";
						//echo "<br />";
						//print_r($member_info);
						?>
						<div class="col-lg-12 col-xs-12 cart_header">
						ID Sponsor     : <?php echo $member_info['sponsorid']."<br />"; ?>
						Nama Sponsor   : <?php echo $member_info['sponsorname']."<br />"; ?>
						Nama Member    : <?php echo $member_info['membername']."<br />"; ?>
						No KTP         : <?php echo $member_info['idno']."<br />"; ?>
						Jenis Kelamin  : 
						<?php 
						  if($member_info['sex'] == "M") {
						  	echo "Pria<br />";
						  } else {
						  	echo "Wanita<br />";
						  }
						?>  
						Tgl Lahir      : <?php echo $member_info['tgllhr']."/".$member_info['blnlhr']."/".$member_info['thnlhr']."<br />"; ?>
						Alamat         : <?php echo $member_info['addr1']." ".$member_info['addr2']." ".$member_info['addr3']."<br />"; ?>
						No HP          : <?php echo $member_info['tel_hp']."<br />"; ?>
						Rekening Bank  : <?php echo $member_info['pilBank']."<br />"; ?>
						No Rekening    : <?php echo $member_info['no_rek']."<br />"; ?>
						</div>

					</div>
					<!-- Order Details-->
				</div>
				<br />
				<div class="col-md-6 col-xs-12">
					<span class="title_header"> <i class="fa fa-check-square-o"></i> Pilih Jenis Starterkit </span>
					<div class="register-top-grid">
						<br />
						<select id="pilStarterkit" name="pilStarterkit" onchange="Shopping.chooseSK(this.value)">
							<option value="">--Pilih disini--</option>
							<?php
							//print_r($listStarterkit);
							foreach ($listStarterkit['arrayData'] as $dta) {
								echo "<option value=\"$dta->prdcd|$dta->prdnm|$dta->weight|$dta->price_w|$dta->price_e|$dta->is_charge_ship\">";
								echo $dta->prdnm;
								echo "</option>";
							}
							?>
						</select>
						<input type="hidden" id="is_charge" name="is_charge" value="" />
						<span id="infoSK" style="display: none">
							
						</span>	
					</div>
				</div>

				<!-- next button-->
				<div class="col-md-12">

					<input type="hidden" name="pricecode" id="pricecode" value="<?php echo $this->session->userdata('pricecode'); ?>" />
					<!--<a href="payment.php" class="btn1 btn2 btn-primary1 pull-left"><span>Lanjutkan</span><i class="fa fa-arrow-right"></i></a>-->

					<!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" onclick="Shopping.checkout()" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />-->
					<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">
						Lanjutkan <i class="fa fa-arrow-right"></i>
					</button>
					<br />
				</div>
				<!-- next button-->
		</div>
		</form>
		<form method="get" action="<?php echo base_url('member/back') ?>">
			<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">
						<i class="fa fa-arrow-left">Ubah Data Member </i>
			</button>
		</form>
		<!-- Delivery Options-->
	</div>
	<!-- End formCart1-->
	<div id="divCheckOut"></div>
	<div id="afterPayment"></div>
</div>
</div>
<!--Checkout Wrapper-->

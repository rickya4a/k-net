<?php
$pdf -> AddPage();
$pdf -> SetFont('Courier', '', 8);
$thnCheck = '2017';	
$lebarCell = 4;
$pdf -> Ln();
$garis = 0;
$titleCol1 = 50;
$titleCol2 = 90;
$titleCol3 = 25;
$titleCol4 = 25;

$prdCodeCol = 40;
$prdDescCol = 80;
$qtyCol = 30;
$amtCol = 40;

$total = $titleCol1 + $titleCol2 + $titleCol3 + $titleCol4;

$jumrec = count($voucher);
$jum = 1;
	foreach ($voucher as $dta) {

		$pdf->Image("assets/images/logo.jpg", 6, 5, 85, 25);

		if ($dta->vchtype == "P") {
			$awal = substr($dta->VoucherNo, 0, 3);
			
			$pdf -> SetFillColor(0, 0, 0);
			$pdf->Code128(150, 20, $dta->VoucherNo, 50,10);
			$pdf -> SetFillColor(255, 255, 255);
			$pdf->SetXY(160, 15);
			$pdf->SetFont('Courier', 'B', 13);
			$pdf->Write(5,$dta->VoucherNo);
			//$pdf->Write(3,'P');
			/*
			if ($awal == "XPV" || $awal == "ZVO") {

				$title_in = "PROMO VOUCHER";
				$title_term = "promo voucher";
				$title_mal = "BAUCER PROMO";
				$sd1 = "*Less: the product voucher amount   :";
			} else {
				$pdf -> SetFillColor(0, 0, 0);
				$pdf->Code128(150, 20, $dta->VoucherNo, 50,10);
				$pdf -> SetFillColor(255, 255, 255);
				$title_in = "PRODUCT VOUCHER";
				$title_term = "product voucher";
				$title_mal = "BAUCER PRODUK";
				$sd1 = "*Less: the product voucher amount   :";
			}
			
			$pdf->SetLeftMargin(4);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, $title_in, $garis, 0, 'C', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, $title_mal, $garis, 0, 'C', true);
			$pdf->Ln();

			
			$pdf->SetFont('Courier', '', 10);

			$pdf->Cell($total, $lebarCell, "Voucher No       : $dta->VoucherNo", $garis, 0, 'L', true);
			$pdf->Ln(); 


			$pdf->Cell($total, $lebarCell, "Distributor Code : $dta->DistributorCode", $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, "Name             : $dta->fullnm", $garis, 0, 'L', true);
			$pdf->Ln();
			
			$pdf->SetFont('Courier', 'B', 10);
			$pdf->Cell($total, $lebarCell, "Bonus Period     : $dta->PERIODEX", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Ln();	
			
			$pdf->SetFont('Courier', '', 10);
			if ($awal == "XPV" || $awal == "ZVO") {
				$pdf->Cell($prdCodeCol, 5, "Product Code", 1, 0, 'C', true);
				$pdf->Cell($prdDescCol, 5, "Product Description", 1, 0, 'C', true);
				$pdf->Cell($qtyCol, 5, "Quantity", 1, 0, 'C', true);
				$pdf->Ln();
				foreach ($dta->product as $prd) {
					$pdf->Cell($prdCodeCol, 5, $prd->prdcd, 1, 0, 'C', true);
					$pdf->Cell($prdDescCol, 5, $prd->prdnm, 1, 0, 'C', true);
					$pdf->Cell($qtyCol, 5, $prd->qtyord, 1, 0, 'C', true);
					//$pdf->Cell($amtCol,5,"",1,0,'C',true);
					$pdf->Ln();
				}
			} else {
				$pdf->Cell($total, $lebarCell, "Amount (RM)      : $dta->VoucherAmtCurr                                          (IND " . number_format($dta->VoucherAmt, 2, ".", ",") . ")", $garis, 0, 'L', true);
				$pdf->Ln();
				$pdf->Ln();
				
				$pdf->Cell($prdCodeCol, 5, "Product Code", 1, 0, 'C', true);
				$pdf->Cell($prdDescCol, 5, "Product Description", 1, 0, 'C', true);
				$pdf->Cell($qtyCol, 5, "Quantity", 1, 0, 'C', true);
				$pdf->Cell($amtCol, 5, "Amount", 1, 0, 'C', true);
				$pdf->Ln();
				for ($x = 1; $x <= 5; $x++) {
					$pdf->Cell($prdCodeCol, 5, "", 1, 0, 'C', true);
					$pdf->Cell($prdDescCol, 5, "", 1, 0, 'C', true);
					$pdf->Cell($qtyCol, 5, "", 1, 0, 'C', true);
					$pdf->Cell($amtCol, 5, "", 1, 0, 'C', true);
					$pdf->Ln();
				}
			}
			 
			$pdf->Ln();

			$totDpAmt = $prdCodeCol + $prdDescCol + $qtyCol;
			$pdf->Cell($totDpAmt, 5, "*Total DP Amount", 0, 0, 'R', true);
			$pdf->Cell($amtCol, 5, "=================", 0, 0, 'C', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "For HQ/Branch/Stockist use only", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "PAYMENT MODE :", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "*Total DP of products redeemed      :", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, $sd1, 0, 0, 'L', true);
			$pdf->Cell(80, 5, "   (IND        ) " . $dta->VoucherAmtCurr, 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(80, 5, "       the difference paid by cash  :", 0, 0, 'L', true);
			$pdf->Cell(80, 5, "    =================", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(80, 5, "       Balance not refundable       :", 0, 0, 'L', true);
			$pdf->Cell(80, 5, "    =================", 0, 0, 'L', true);
			$pdf->Ln();
			$garis = "";
			for ($x = 1; $x <= 90; $x++) {
				$garis .= "-";
			}
			$pdf->Cell(120, 5, $garis, 0, 0, 'L', true);
			$pdf->Ln();

           
			$pdf->Cell(120, 5, "       I certify that the products redeemed is correct and in good condition", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
//			$pdf->Ln();

			$pdf->Cell(55, 5, "======================= ", 0, 0, 'L', true);
			$pdf->Cell(55, 5, "======================= ", 0, 0, 'L', true);
			$pdf->Cell(85, 5, "====================================== ", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(55, 5, "Distributor's signature ", 0, 0, 'L', true);
			$pdf->Cell(55, 5, "Distributor's Tel. No. ", 0, 0, 'L', true);
			$pdf->Cell(85, 5, "Stockist's/Officer's Stamp & Signature", 0, 0, 'L', true);
			$jum++;
			$pdf->Ln();

			$pdf->SetFont('', 'B', 10);
			//$pdf->Text(5, 98, 'EXPIRED DATE : '.$dta->ExpireDate);

			$pdf->SetFont('Courier', 'B', 10);
			if ($dta->claimstatus == "0") {
				$pdf->SetTextColor(0,0,0);
				$statusclaim = "Voucher ini BELUM diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}elseif ($dta->claimstatus == "1") {
				$pdf->SetTextColor(255,0,0);
				$statusclaim = "Voucher ini SUDAH diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}

			$pdf->Cell($total, $lebarCell, "Claim Status    : $statusclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Location  : $stockistclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Date    	 : ". formatTglIndo($tglclaim), $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->SetTextColor(0,0,0);

			$expireddt = $dta->ExpireDate;
			$pdf->Cell($total, $lebarCell, "Expired Date    : " . formatTglIndo($expireddt), $garis, 0, 'L', true);
			$pdf->Ln();

			$today = date("d/m/Y H:i:s");
			$pdf->Cell($total, $lebarCell, "Printed Date    : " . formatTglIndo($today), $garis, 0, 'L', true);
			$pdf->Ln();



			$pdf->SetFont('Courier', 'B', 7);


			//$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(100, 5, "TERMS AND CONDITIONS", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   1.  This " . $title_term . " allows the bearer to redeem products at any on the K-Link HQ. Branches or Stockist Center.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       However it cannot be exchanged for cash.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   2.  This " . $title_term . " must be redeemed at K-Link Sdn. Bhd. HQ. Branches and Stockist Centres within 90 days dated hereof.", 0, 0, 'L', true);
			$pdf->Ln();
			//$pdf->Cell(100,5,"    However it cannot be exchanged for cash",0,0,'L',true);
			$pdf->Cell(100, 5, "   3.  The redemption of the product(s) wth this voucher will be without BV.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   4.  If the value pf the products redeemed is less than the amount stated in this voucher, then the difference thereof shall", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       not be refunded to the bearer.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   5.  However. if the bearer uses the balance amount of this voucher to further redeem other products, the bearer thereof", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       should pay the difference and other products redeemed will also be without BV.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "   6.  K-Link Sdn. Bhd. shall not be held responsible for the loss or damage of this voucher and any losses or damages", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       suffered by the bearer of this voucher that arise from or in connection with the loss or damage of the voucher.", 0, 0, 'L', true);
			$jum++; */
		}
		elseif ($dta->vchtype == "C") {
			$pdf -> SetFillColor(0, 0, 0);
			$pdf->Code128(150, 20, $dta->voucherkey, 50,10);
			$pdf -> SetFillColor(255, 255, 255);
			$pdf->SetXY(160,15);
			$pdf->SetFont('Courier', 'B', 13);
			$pdf->Write(5, $dta->voucherkey);
			//$pdf->Write(3,'C');
			
			$title_in = "CASH VOUCHER";
			$title_mal = "BAUCER TUNAI";
			$title_term = "cash voucher";
			$sd1 = "*Less: the cash voucher amount      :";

			$pdf->SetLeftMargin(4);
			$pdf->Ln();
			$pdf->Ln();

			$pdf->SetFont('Courier', 'B', 14);

			$pdf->Cell($total, $lebarCell, $title_in, $garis, 0, 'C', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, $title_mal, $garis, 0, 'C', true);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();


			$pdf->SetFont('Courier', '', 10);

			$pdf->Cell($total, $lebarCell, "Issued Date     : " . formatTglIndo($dta->IssueDate), $garis, 0, 'L', true);
			$pdf->Ln();
			
			$pdf->SetFont('Courier', 'B', 10);
			$bnsPer = $dta->BonusMonth . "/" . $dta->BonusYear;
			$pdf->Cell($total, $lebarCell, "Bonus Payment for " . formatTglIndo($bnsPer), $garis, 0, 'L', true);
			$pdf->Ln();
			
			$pdf->SetFont('Courier', '', 10);
			$pdf->Cell($total, $lebarCell, "Member Code     : " . $dta->DistributorCode, $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->Cell($total, $lebarCell, "Name            : $dta->fullnm", $garis, 0, 'L', true);
			$pdf->Ln();

			$amtx = "Amount (IND)    : " . number_format($dta->VoucherAmt, 2, ".", ",") . "              (               " . $dta->VoucherAmtCurr . ")";
			$pdf->Cell($total, $lebarCell, $amtx, $garis, 0, 'L', true);
			$pdf->Ln();

			//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
			//$spellNum = convertNumberToWord($dta->VoucherAmt);
			$cs = number_format($dta->VoucherAmt, 0, "", "");
			$spellNum = $this->terbilang->toTerbilang($cs);
			$pdf->Cell($total, $lebarCell, "                  " . ucwords($spellNum), $garis, 0, 'L', true);
			$pdf->Ln();

			$expireddt = $dta->ExpireDate;
			$pdf->Cell($total, $lebarCell, "Expired Date    : " . formatTglIndo($expireddt), $garis, 0, 'L', true);
			$pdf->Ln();

			$today = date("d/m/Y H:i:s");
			$pdf->Cell($total, $lebarCell, "Printed Date    : " . formatTglIndo($today), $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->SetFont('Courier', 'B', 10);
			if ($dta->claimstatus == "0") {
				$pdf->SetTextColor(0,0,0);
				$statusclaim = "Voucher ini BELUM diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}elseif ($dta->claimstatus == "1") {
				$pdf->SetTextColor(255,0,0);
				$statusclaim = "Voucher ini SUDAH diklaim.";
				$stockistclaim =  $dta->loccd ;
				$tglclaim = $dta->claim_date;
			}

			$pdf->Cell($total, $lebarCell, "Claim Status    : $statusclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Location  : $stockistclaim", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell($total, $lebarCell, "Claim Date      : ". formatTglIndo($tglclaim), $garis, 0, 'L', true);
			$pdf->Ln();

			$pdf->SetTextColor(0,0,0);


			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();

			$pdf->Cell(60, 5, "==========================", $garis, 0, 'L', true);
			$pdf->Cell(60, 5, "==========================", $garis, 0, 'L', true);
			$pdf->Cell(60, 5, "===========================", $garis, 0, 'L', true);
			$pdf->Ln();
			$pdf->SetFont('Courier', 'B', 10);
			$pdf->Cell(60, 5, "DISTRIBUTOR'S SIGNATORY ", $garis, 0, 'C', true);
			$pdf->Cell(60, 5, "STOCKIST SIGNATORY & CHOP ", $garis, 0, 'C', true);
			$pdf->Cell(60, 5, "AUTHORIZED SIGNATORY & CHOP", $garis, 0, 'C', true);
			$pdf->Ln();


			$pdf->SetFont('Courier', 'B', 7);
			$pdf->Ln();
			$pdf->Ln();


			$pdf->Cell(100, 5, "N.B. - This Cash Voucher must be redeemed at our stockist or HQ within 90 days dated hereof,", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       for exchange of cash afterwhich would be void.", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       Baucer ini mesti ditunaikan di Pusat Stockist atau Ibu Pejabat dalam tempoh 90 hari dari tarikh baucher ini,", 0, 0, 'L', true);
			$pdf->Ln();
			$pdf->Cell(100, 5, "       dan akan di batal selepas tempoh ini.", 0, 0, 'L', true);
			$jum++;
		}
 
        /*
		$pdf -> SetFillColor(255, 255, 255);

		$pdf->SetFont('Courier', 'B', 10);
		$pdf->SetXY(4, 40);

		if ($dta->vchtype == "P") {
			$awal = substr($dta->VoucherNo, 0, 3);
			
			
            
			
			
			
		}
		else {

			
		} */

		if ($jum <= $jumrec) {
			$pdf->AddPage();

		}
        
	}	
	
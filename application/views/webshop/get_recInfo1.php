<script>
    function getDetailBonus(param) {

        var dfno = $("#dfno").val();
        var date1 = $("#from" +param).val();
        var date2 = $("#to" +param).val();
        //All.set_disable_button();
        $.ajax({
            url : All.get_url('sales/recruiter/detailinfo'),
            type : 'POST',
            data : {dfno : dfno, date1 : date1, date2 : date2},

            success : function(data) {

                All.set_enable_button();
                //$(All.get_active_tab() + " #hasilPencarian1").hide();
                //$(All.get_active_tab() + " #nextForm1").html(null);
                //$(All.get_active_tab() + " #nextForm1").html(data);

                $("#formCekDownline").hide();
                //$("#formCekDownline").html(null);
                $("#nextForm1").show();
                $("#nextForm1").html(null);
                $("#nextForm1").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    }
</script>

<div class="row vpullset4">
    <div class="container">
        <?php
        if (($dfno==null )) {
            echo "Data Tidak Ada";
        } else{
        ?>
        <!--right-->
        <div class="col-md-12">
            <!-- Track Order -->
            <!-- tracking content header-->
            <form method="post" id="printJaringan" target="_blank" action="<?php echo site_url('sales/print');?>">


        </div>


        <div class="col-lg-12 col-xs-12 cart_header">
            <div class="col-lg-12">
                Hasil Pencarian Data Recruitment
            </div>
        </div>
        <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">

            <!--line 1-->
            <div class="col-lg-3 col-xs-3">
                ID Member
            </div>
            <div class="col-lg-9 col-xs-9">
                <?php if($dfno == null){
                    echo "-";
                }else{
                    echo $dfno;
                }?>
            </div>
            <!--line 1-->
            <!--line 3-->
            <div class="col-lg-3 col-xs-3">
                Atas Nama
            </div>
            <div class="col-lg-9 col-xs-9">
                <?php if($acc_nm == null){
                    echo "-";
                }else{
                    echo $acc_nm;
                }?>
            </div>
            <!--line 2-->
            <div class="col-lg-3 col-xs-3">
                Bank
            </div>
            <div class="col-lg-9 col-xs-9">
                <?php if($acc_bank == null){
                    echo "-";
                }else{
                    echo $acc_bank;
                }?>
            </div>
            <!--line 2-->
            <div class="col-lg-3 col-xs-3">
                No. Rek
            </div>
            <div class="col-lg-9 col-xs-9">
                 <?php if($acc_no == null){
                    echo "-";
                }else{
                    echo $acc_no;
                }?>
            </div>


            <!--line 2-->
        </div>

        </form>

        <div class="row">
            <?php
            if (($dt_table==null )) {
                echo "Data Tidak Ada";
            } else{
            ?>
            <div class="col-sm-12">

                <table width="90%" class="main table table-hover" id="box-table-b">
                    <thead>
                    <tr>
                        <th >No.</th>
                        <th >Tanggal Join</th>
                        <th >Jumlah Rekrutan</th>
                        <th>Nama Rekening</th>
                        <th >No Rekening</th>
                        <th >Bank</th>
                        <th>Tanggal Transfer</th>
                        <th >Bonus</th>
                        <th>Total Transfer</th>
                        <th >Status</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    $totss=0;
                    foreach ($dt_table as $dt) {
//                        $join=date("d-m-Y",strtotime( $dt->joindt ));
//                        $trx= isset($dt->transfer_dt) ?  date("d-m-Y",strtotime( $dt->transfer_dt )) : '-';
//                        if($dt->status_transfer==0){
//                            $stat='Pending';
//                        }else if ($dt->status_transfer==1){
//                            $stat='Sukses';
//                        }else if ($dt->status_transfer==2){
//                            $stat='Gagal';
//                        }
//                        $bonusrec = number_format($dt->bonusrec, 0, ".", ",");
//                        $tax_decrease = number_format($dt->tax_decrease, 0, ".", ",");
//                        $tot=$dt->bonusrec-$dt->tax_decrease;
//                        $totss=$totss+$tot;
//                        $tots = number_format($tot, 0, ".", ",");

                        $tgl1 = date("d-m-Y",strtotime( $dt->tgl1 ));
                        $tgl2 = date("d-m-Y",strtotime( $dt->tgl2 ));
                        $datel = $dt->tgl1;
                        $date2 = $dt->tgl2;

                        if($dt->tglFINANCE == null){
                            $tglFINANCE = "";
                        }else{
                            $tglFINANCE = date("d-m-Y",strtotime( $dt->tglFINANCE ));
                        }

                        $jml_rekrutan = $dt->jml_rekrutan;
                        $NAMAREK = $dt->NAMAREK;
                        $REKENING = $dt->REKENING;
                        $BANK = $dt->BANK;
                        $NOMINAL = number_format($dt->NOMINAL, 0, ".", ",");
                        $NOMINAL_TRF = number_format($dt->NOMINAL_TRF, 0, ".", ",");
                        $status = $dt->status;

                        $aksi=site_url('sales/recruiter/detailinfo');

                        echo "
                        <tr data-status=\"I\" id=$no>
                        <td>$no</td>
                        <td>
                        <input type=\"hidden\" id=\"dfno\" value=\"$dfno\">
                        <input type=\"hidden\" id=\"from$no\" value=\"$datel\">
                        <input type=\"hidden\" id=\"to$no\" value=\"$date2\">
                            $tgl1 hingga $tgl2
                        </td>
                        <td>$jml_rekrutan</td>
                        <td>$NAMAREK</td>
                        <td>$REKENING</td>
                        <td>$BANK</td>
                        <td>$tglFINANCE</td>
                        <td>$NOMINAL</td>
                        <td>$NOMINAL_TRF</td>
                        <td>$status</td>
                        <td>
                            <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Lihat\" class=\"btn btn-primary1\" onclick=\"getDetailBonus($no)\" method=\"POST\"/>
                        </td>
                        </tr>
                    ";
//                     echo  isset($acc_bank) ? $acc_bank : '-';

                    $no++;

                    }
//                    echo "
//                    <td colspan='8'>Total</td>
//                    <td >".number_format($totss, 0, ".", ",")."</td>
//                    ";
                    }
                    ?>


                    </tbody>

                </table>

            </div>
        </div>
        <?php }
        ?>

    </div>
</div>

 
<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         <span class="red"> &nbsp;My Account&nbsp;&nbsp;</span>
  </div>
</ul>
<!-- Breadcrumb -->

<div class="row vpullset4 voffset4">
	<div class="container">
    
    	<!--Left sidebar-->
    	<div class="col-md-3 col-sm-12">   
        	<span class="title_header">
            	<i class="fa fa-user"></i> My Account
        	</span>
            <div class="list_grid voffset3">
				<ul>
                	<li><a href="#">Transaction History</a></li>
                	<li><a href="#">Personal Particulars</a></li>
                	<li><a href="#">Tracking Your Package</a></li>
                </ul>				
			</div>            
    	</div>
        <!--Left sidebar-->
        
        <!--right-->
    	<div class="col-md-9 col-sm-12"> 
        
        	<!-- transaction content-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-2 col-xs-2">Tanggal</div> 
                <div class="col-lg-2 col-xs-4">No. Order</div>
                <div class="col-lg-3 col-xs-3">Detail Order</div> 
                <div class="col-lg-3 col-xs-3">Total</div> 
                <div class="col-lg-2 col-xs-2">Status</div>          	 
            </div>
            <!-- transaction content-->
            <!-- transaction content 1-->
            <div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-2 col-xs-2">
                    2015-02-09 <br>
                    09:25:55
                </div>
                <div class="col-lg-2 col-xs-2">as7asha8</div> 
                <div class="col-lg-3 col-xs-3">
                    2x  Kinokatara<br>
                    4x Bio Greesn<br>
                    1x K-Enginee<br>
                    5x K-Sophie 
                </div>
                <div class="col-lg-3 col-xs-3">Rp. 200.871.000</div>             
                <div class="col-lg-2 col-xs-2">Deliver</div>           	 
            </div>
            <!-- transaction content 1-->
            <!-- transaction content 2-->
            <div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-2 col-xs-2">
                    2015-03-19 <br>
                    09:25:55
                </div>
                <div class="col-lg-2 col-xs-2">5gas5ga</div> 
                <div class="col-lg-3 col-xs-3">               	
                    4x Bio Greesn<br>
                    1x K-Enginee<br>
                    2x  Kinokatara<br>
                    5x K-Sophie 
                </div>
                <div class="col-lg-3 col-xs-3">Rp. 213.223</div>             
                <div class="col-lg-2 col-xs-2">Waiting For Payment</div>           	 
            </div>
            <!-- transaction content 2-->
            
            
            
            <!-- Personal Particulars--> 
            <div class="user-info">
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nama Lengkap </p>
                    <span>Abient Novant Rusmana</span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nomor Id User </p>
                    <span>164664</span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Tanggal Lahir</p>
                    <span>15 Juni 1990</span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Kota Asal</p>
                    <span>DKI Jakarta</span>
                </div>            
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nama Sponsor</p>
                    <span>Kristi Damayanti Kusuma</span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Nomor Id Sponsor</p>
                    <span>358562</span>
                </div>
                <div class="col-md-6 col-xs-12 user-info-wrapper">  	          
                    <p>Alamat Lengkap</p>
                    <span>Ruko Grand Vico No. 20, Jl. Soekarno Hatta, Magelang Selatan, (sebelah Utara Terminal Soekarno Hatta Magelang), Magelang, Indonesia</span>
                </div>
            </div>
            <!-- Personal Particulars-->
            
            
            
        	<!-- Track Order -->
        	<div class="col-md-6 col-md-offset-3 text-center">
				<form> 
                	<div class="track-order-grid">
                    	<div>
                        	<span>Masukkan Nomor Order Anda</span>
                        	<input type="text">                            
                   		</div>
                        <div>
                        	<input class="btn1 btn2 btn-primary1" name="" type="button" value="Search">
                        </div>
            		</div>
               	</form>
			</div>    
            <!-- Track Order -->
            <!-- tracking content header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-3 col-xs-3">No. Order</div>
                <div class="col-lg-3 col-xs-3">Tanggal Kirim</div> 
                <div class="col-lg-3 col-xs-3">Jenis Kirim</div> 
                <div class="col-lg-3 col-xs-3">Status</div>          	 
            </div>
            <!-- tracking content-->
            <!-- tracking content 1-->
            <div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-3 col-xs-3">5asas45</div>
                <div class="col-lg-3 col-xs-3">2015-03-19 - 09:25:55</div> 
                <div class="col-lg-3 col-xs-3">Kirim Regular</div>
                <div class="col-lg-3 col-xs-3">On Progress</div> 
            </div>
            <!-- tracking content 1-->
            <!-- tracking content 2-->
            <div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-3 col-xs-3">a6aha7a</div>
                <div class="col-lg-3 col-xs-3">2015-02-06 - 09:25:55</div> 
                <div class="col-lg-3 col-xs-3">Kirim YES</div>
                <div class="col-lg-3 col-xs-3">On Progress</div> 
            </div>
            <!-- tracking content 2-->
            <!-- tracking content 3-->
            <div class="col-lg-12 col-xs-12 cart_content text-center">  
                <div class="col-lg-3 col-xs-3">a836ha8</div>
                <div class="col-lg-3 col-xs-3">2015-02-06 - 09:25:55</div> 
                <div class="col-lg-3 col-xs-3">Pick Up Stockist</div>
                <div class="col-lg-3 col-xs-3">Ready to Pick Up</div> 
            </div>
            <!-- tracking 3-->
            
                     
    	</div>
    <!--right-->
        
    </div>
</div>
<!-- Shopping Cart -->
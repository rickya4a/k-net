<style>
	.form-list {
		font-size: 0.75em;
	}
	.sponsorid{
		width: 80%;
	}
	.register-top-grid > .col-md-6.col-xs-12 {
		min-height: 60px;
	}
</style>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
	<div class="container">
		<li class="home">
			<a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt="" /></a>&nbsp;
			&nbsp; <span>&gt;</span>
		</li>
		<li class="home">&nbsp;
			Member&nbsp;&nbsp;
			<span>&gt;</span>
		</li>
		<li class="home">
			<span class="red"> &nbsp;Pendaftaran Baru&nbsp;&nbsp;</span>
		</li>

	</div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
		<div id="voucherDiv">

			<?php
					 $member_info = $this->session->userdata('member_info');
      	 ?>
			<div class="col-md-12 col-xs-12 vpullset3">
				<form id="formMember" action="<?php echo base_url('member/starterkit'); ?>" method="POST" onsubmit="return Shopping.validateInputMemberDev()">
					<div class="register-top-grid">
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">ID Recruiter&nbsp; (* HARUS DIISI )</font>
							</label><br />
							<input class="required uppercase" tabindex="1" type="text" id="recruiterid" chars="alphanumeric" name="recruiterid" value="<?php echo getUserID(); ?>"
							 onchange="Shopping.getMemberInfoDev(this.value, 'recruitername')" />
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Nama Recruiter&nbsp;*</font>
							</label><br />
							<input type="text" readonly="readonly" id="recruitername" name="recruitername" value="<?php echo getUserName(); ?>" />
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">ID Sponsor&nbsp; (* HARUS DIISI )</font>
							</label><br />
							<input tabindex="2" class="required uppercase" type="text" id="sponsorid" chars="alphanumeric" name="sponsorid" value="<?php echo $member_info['sponsorid']; ?>"
							 onchange="Shopping.getMemberInfoDev(this.value, 'sponsorname')" />


							<input type="hidden" id="id_lp" name="id_lp" value="NONLP" />

						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Nama Sponsor&nbsp;*</font>
							</label><br />
							<input class="required uppercase" type="text" readonly="readonly" id="sponsorname" name="sponsorname" value="<?php echo $member_info['sponsorname']; ?>" />
						</div>

						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Nama Member&nbsp;*</font>
							</label><br />
							<input tabindex="3" class="required uppercase" type="text" id="membername" chars="alpha-numeric" name="membername" value="<?php echo $member_info['membername']; ?>" />
						</div>

						<!-- < !--<div class="col-md-6 col-xs-12"><label class="label-list">ID Recruiter<font color="red">&nbsp;
						</font></label><br/><input type="hidden"id="recruiterid"name="recruiterid"value="<?php echo getUserID(); ?>"/></div><div class="col-md-6 col-xs-12"><label class="label-list">Nama Recruiter<font color="red">&nbsp;
						</font></label><br/><input type="hidden"readonly="readonly"id="recruitername"name="recruitername"value="<?php echo getUserName(); ?>"/></div>-->

						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">No KTP&nbsp;*</font>
							</label><br />
							<input tabindex="4" class="required numeric-input" type="text" id="idno" name="idno" value="<?php echo $member_info['idno']; ?>"
							 onchange="Shopping.checkDoubleKTP(this.value)"  />


						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Jenis Kelamin&nbsp;*</font>
							</label><br />
							<select tabindex="5" class="form-list required" id="sex" name="sex">
								<option value="M">Pria</option>
								<option value="F">Wanita</option>
							</select>
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Tgl Lahir&nbsp;*</font>
							</label><br />
							<!--<input tabindex="5" class="required" type="text" id="birthdt" name="birthdt" placeholder="DD/MM/YYYY" value="" onchange="Shopping.checkAgeMember(this.value)" />-->
							<?php
	                  echo datebirth_comboTabIndex("6", "7", "8");
	                ?>
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Alamat&nbsp;*</font>
							</label><br />
							<input tabindex="9" class="required uppercase" type="text" id="addr1" name="addr1" value="<?php echo $member_info['addr1']; ?>" />
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">No. HP&nbsp;*</font>
							</label><br />
							<input tabindex="10" class="required numeric-input" type="text" id="tel_hp" name="tel_hp" value="<?php echo $member_info['tel_hp']; ?>"
							 onchange="Shopping.checkDoubleHP(this.value)" />
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">
								<font color="red">Kota&nbsp;*</font>
							</label><br />
							<input class="required uppercase" tabindex="11" type="text" id="addr2" name="addr2" value="<?php echo $member_info['addr2']; ?>" />
						</div>

						<div class="col-md-6 col-xs-12">
							<label class="label-list">Rekening Bank</label><br />
							<select tabindex="12" class="form-list" id="pilBank" name="pilBank">
								<option value="">--Pilih disini--</option>
								<?php //print_r($listBank['arrayData']);
								foreach($listBank['arrayData'] as $dta) {
									if($member_info['pilBank']==$dta->bankid) {
										echo "<option value=\"$dta->bankid\" selected=\"selected\">$dta->description</option>";
									}
									else {
										echo "<option value=\"$dta->bankid\">$dta->description</option>";
									}
								}
								?>
							</select>
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">Kode Pos</label><br />
							<input tabindex="13" type="text" id="addr3" name="addr3" value="<?php echo $member_info['addr3']; ?>" />
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">No Rekening</label><br />
							<input tabindex="14" class="numeric-input" type="text" id="no_rek" name="no_rek" value="<?php echo $member_info['no_rek']; ?>" />
						</div>
						<div class="col-md-6 col-xs-12">
							<label class="label-list">Alamat Email</label><br />
							<input tabindex="15" type="text" id="memb_email" name="memb_email" chars='\wÑñ@._\-' value="<?php echo $member_info['memb_email']; ?>" />
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-12">
						<input type="hidden" id="errformRecruiter" name="errformRecruiter" value="0" />
						<input type="hidden" id="errformSponsor" name="errformSponsor" value="0" />
						<input type="hidden" id="errformHP" name="errformHP" value="0" />
						<input type="hidden" id="errformKTP" name="errformKTP" value="0" />
						<button tabindex="17" type="submit" class="btn1 btn2 btn-primary1 pull-left" onclick="formCheck()" id="submits">Lanjut<i class="fa fa-arrow-right"></i></button><br />
					</div>
					<!-- next button-->
			</div>
			</form>
		</div>
	</div>
	<!-- Delivery Options-->
</div> <!-- End formCart1-->
<div id="divCheckOut"></div>
<div id="afterPayment"></div>

<script type="text/javascript">
	/**
	 * @author Ricky
	 * @desc Create RegEx function to prevent user to input invalid character
	 */
	$(document).on('keyup change input', '[chars]', function(event) {
		var $elem = $(this),
		value = $elem.val(),
		regReplace,
		preset = {
			'only-numbers': '0-9',
			'numbers': '0-9\\s',
			'only-letters': 'A-Za-z',
			'letters': 'A-Za-z\\s',
			'email': '\\wÑñ@._\\-',
			'alpha-numeric': '\\w\\s',
			'latin-alpha-numeric': '\\w\\sÑñáéíóúüÁÉÍÓÚÜ',
			'alphanumeric': 'A-Z0-9'
		},
		filter = preset[$elem.attr('chars')] || $elem.attr('chars');
		regReplace = new RegExp('[^' + filter + ']', 'ig');
		$elem.val(value.replace(regReplace, ''));
	});

	/**
	 * @author Ricky
	 * @desc If users enter invalid character, the window sends an alert
	 */
	/* $("#sponsorid").keypress(function(e) {
		var k = e.keyCode,
		$return = ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32  || (k >= 48 && k <= 57));
		if(!$return) {
			alert('Hanya boleh menggunakan angka dan huruf')
			return true;
		}
	})
	$("#recruiterid").keypress(function(e) {
		var k = e.keyCode,
		$return = ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32  || (k >= 48 && k <= 57));
		if(!$return) {
			alert('Hanya boleh menggunakan angka dan huruf')
			return true;
		}
	}) */

	/**
	 * @author Ricky
	 * @desc Check form data before submitting
	 * @return  mixed
	 */
	/* -- Temporary disabled -- */
	function formCheck() {
		let errFormKTP = $('#errformKTP').val(),
		errFormHP = $('#errformHP').val(),
		errFormSponsor = $('#errformSponsor').val(),
		errFormRecruiter = $('#errformRecruiter').val(),
		idno = $('#idno').val(),
		membername = $('#membername').val(),
		addr1 = $('#addr1').val(),
		sponsorid = $('#sponsorid').val(),
		recruiterid = $('#recruiterid').val();

		event.preventDefault();
		if (errFormKTP === '1' || errFormHP === '1' || errFormSponsor === '1' || errFormRecruiter === '1' || idno == "" || membername == "") {
			alert('Silakan periksa kembali data Anda dan pastikan semua data diisi dengan benar');
			return false;
		} else if (idno === '' || sponsorid === '' || recruiterid === '') {
			alert('Harap lengkapi data yang akan didaftarkan');
		} else {
			$('#formMember').submit();
			return true;
		}
	}
</script>
<!--Checkout Wrapper-->

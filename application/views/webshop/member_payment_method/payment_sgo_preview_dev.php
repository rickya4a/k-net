<!-- Breadcrumb -->
<?php
  $biaya = getTotalSKandShipCost2();
  $totPayAndShip = $biaya['total_pay'];
  $freeship = $biaya['freeship'];
  $tot_discount = $biaya['tot_discount'];
?>
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Payment Preview&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Preview Pembayaran&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">Preview Pembayaran <?php echo $bankDesc; ?></div>
            <div id="div_info1" class="col-lg-12 col-xs-12 nomargin">
            	
                <p class="p2 nopadding" style="font-size:16px;">
                   <?php
                      //print_r($res);
                      $shipping_jne_info = $this->session->userdata('shipping_jne_info');
                      echo "<table width=70% align=center border=0>";
                      echo "<tr><td align=right>Tipe Pembayaran       :</td><td align=right>".$bankDesc."</td></tr>";
					  echo "<tr><td align=right>Total Pembelanjaan    :</td><td align=right>".$res[0]->total_pay."</td></tr>";
					  
					  //echo "<tr><td align=right>Biaya Kirim           :</td><td align=right>".$ongkir."</td></tr>";
                      if($freeship == "1") {
	                  	if($tot_discount > 0) {
	                  		echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
	                  		echo "<tr><td align=right>Disc Biaya Kirim        :</td><td align=right>Rp. -(".number_format($tot_discount, 2, ",", ".").")</td></tr>";
	                  	} else {
	                  		echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. 0</td></tr>";
	                  	}
	                  	 
	                  } else {
	                  	 echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
	                  }
                      echo "<tr><td align=right>Biaya Connectivity    :</td><td align=right>".$charge_connectivity."</td></tr>";
                      echo "<tr><td align=right>Biaya Administrasi    :</td><td align=right>".$charge_admin."</td></tr>";
					  /*if($freeship == "0") {
					  	$total_semua = $tot_hrs_dibayar + $charge_connectivity + $charge_admin;
					  } else {
					  	$total_semua = $res[0]->total_pay + $shipping_jne_info['price'] + $charge_connectivity + $charge_admin;
					  }*/
					  $total_semua = $totPayAndShip + $charge_connectivity + $charge_admin;
					  //$total_semua = $res[0]->total_pay + $shipping_jne_info['price'] + $charge_connectivity + $charge_admin;
					  echo "<tr><td align=right>Total                 :</td><td align=right>".$total_semua."</td></tr>";
                      echo "</table>";
					  $total_all = $res[0]->total_pay + $charge_connectivity;
                   ?>
                </p>
               
                <!--<div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">-->
                	
                	<input type="hidden" id="total_all" value="<?php echo $total_all; ?>" />
					<input type="hidden" id="total_semua" value="<?php echo $total_semua; ?>" />
					
                    <button class="btn2 btn2 btn-primary1" onclick="submitdataXX()">
                        <i class="fa fa-arrow-right"></i>
                        <span>Lanjut ke Pembayaran</span>
                    </button>
                    
                <!--</div>	-->
            </div>
			<!--<iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>-->
	        <?php sgo_iframe(); ?>
			<div id="wait_message"></div>
		</div>	
	</div>
</div>
 
<script type="text/javascript">
    
  function submitdataXX() {
     var total_all = $("#total_all").val();
	  var tipe_bank = "<?php echo $bankCode; ?>";
	 var pay_amount = $("#total_all").val();
	 var backUrlRedirect = "<?php echo $backURL;?>";
	 //var pay_amount = 2300000;
	 var temp_orderno = "<?php echo $temp_paymentIdx;?>";
	 
	 if(tipe_bank == "VAC") {
		var x = document.getElementById("div_info1");
		 x.style.display = "none";
		 $("#wait_message").html(null);
		 $("#wait_message").html("<p>Mohon tunggu sebentar, transaksi sedang diproses..</p>");
	     var total_semua = $("#total_semua").val();
		$.ajax({
			//url: All.get_url('sgo_ppob/saveTemp'),
			url: All.get_url('member/reg/pay/va'),
			type: 'POST',
			data: {pay_amount: total_semua, temp_orderno: temp_orderno, tipe_bank: tipe_bank},
			dataType: 'json',
			success:
			function(data){
			   
				var response = data.response;
				if(response == "true") {
					//window.location.replace(backUrlRedirect);
					window.location.href = backUrlRedirect
				} else {
					alert(data.message);
					$("#btn_submit").removeAttr("disabled");
					$("#div_info1").css("display", "block");
					$("#wait_message").html(null);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				 alert(thrownError + ':' +xhr.status);
				 $("input[type=button]").removeAttr('disabled');
			}
		});	
	 } else {
		
		 var data = {
						key : "<?php echo $temp_orderidx;?>",
						paymentId : temp_orderno,
						paymentAmount : pay_amount,
						backUrl : backUrlRedirect,
						bankCode : tipe_bank,
						bankProduct: "<?php echo $bankDesc; ?>"
					},
			sgoPlusIframe = document.getElementById("sgoplus-iframe");
					
			if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
			SGOSignature.receiveForm();
	 }				
		
  }
</script>       
<!-- Login Form --> 
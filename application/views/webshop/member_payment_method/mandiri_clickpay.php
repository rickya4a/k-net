<div class="col-lg-12 col-xs-12 vpullset3"> 
	<div class="col-lg-12 col-xs-12 delivery_header">Mandiri Clickpay Transaction</div>
    <div class="col-lg-12 col-xs-12 nomargin">
    	<p class="p2 nopadding">
          1. Silahkan aktifkan Mandiri Token anda</br>
          2. Masukkan password Mandiri Token anda</br>
          3. Pilih "3" ketika token menampilkan APPLY.</br>
          4. Isian "Input 1" : 10 Digit terakhir kartu Mandiri Debit andat</br>
          5. Isian "Input 2" : Nilai transaksi / Total Pembayaran</br>
          6. Isian "Input 3" : Request Number</br>
          7. Isi respon dari token mandiri anda ke field "Challenge Token"</br>
        </p>
   	</div>
    
    <form action="<?php echo site_url('member/pay/mandiri/clickpay'); ?>" method="POST" id="payment-form"> 
        <div class="register-top-grid">
            <div>
				<span>Card Number<label>*</label></span>
				<!--<input id="card-number" name="card-number" type="text" value="4111111111111111"/>-->
				<input id="card-number" name="card-number" type="text" value=""/>  
            </div>
            <div>
				<span>Input 1<label>*</label></span>
				<input id="input1" name="input1" type="text" readonly="readonly"/>
            </div>
            <div>
				<span>Input 2<label>*</label></span>
				<input id="input2" name="input2" type="text" readonly="readonly"/>
            </div>
            <div>
				<span>Input 3<label>*</label></span>
				<input id="input3" name="input3" size="5" type="text" readonly="readonly"/>
            </div>   
            <div>
				<span>Challenge Token<label>*</label></span>
				<!--<input id="token" name="token" size="10" type="text" value="000000"/>-->
				<input id="token" name="token" size="10" type="text" value=""/>
            </div>     
            <div>
            	<button id="submit-button" class="btn1 btn2 btn-primary1" style="margin-top:15px;" type="submit">Proses Pembayaran</button>
            </div>
            
            <div class="clearfix"> </div>
      	</div>
      	<div class="register-top-grid">
      		<div>
            	<font color="red">*Harga di atas sudah termasuk charge Rp 2.500,-</font>
            </div>
      	</div>	
   	</form>
    <?php
       $total_payment = getTotalPaymentSK("dc");  
       $priceSK = $this->session->userdata('starterkit_prd');
	   //echo "Hrg : ".$priceSK['price'];
    ?>
    <script type="text/javascript">
	$(function(){
	  $("#input2").val("<?php echo $total_payment;  ?>");
	  updateInput1();
	
	  $('#card-number').keyup(function(){
	    updateInput1();
	  });
	
	  $('#input3').val(random5digit());
	
	  function updateInput1(){
	    var ccNumber = $('#card-number').val();
	    if(ccNumber.length > 9) {
	      $('#input1').val(ccNumber.substring(ccNumber.length, ccNumber.length-10));
	    }
	  }
	
	  function paddy(n, p, c) {
	    var pad_char = typeof c !== 'undefined' ? c : '0';
	    var pad = new Array(1 + p).join(pad_char);
	    return (pad + n).slice(-pad.length);
	  }
	
	  function random5digit(){
	    return paddy(Math.floor(Math.random() * 99999), 5); 
	  }
	
	});
   </script>
    
</div>   
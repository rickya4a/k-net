<!-- Breadcrumb -->
<?php
$shipping_jne_info = $this->session->userdata('shipping_jne_info');
$biaya = getTotalSKandShipCost2();
$totPayAndShip = $biaya['total_pay'];
$freeship = $biaya['freeship'];
$tot_discount = $biaya['tot_discount'];
$tot_belanja= $totPayAndShip - $shipping_jne_info['price'];
?>
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Payment Preview&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Preview Pembayaran&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
    <div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
            <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:18px;">Transaksi Berhasil Pembayaran via <br><?php echo $bankDesc; ?></div>
            <div id="div_info1" class="col-lg-12 col-xs-12 nomargin">

                <p class="p2 nopadding" style="font-size:16px;">
                    <?php
                    //print_r($res);
                    $shipping_jne_info = $this->session->userdata('shipping_jne_info');
                    echo "<table width=70% align=center border=0>";
                    echo "<tr><td align=right>Tipe Pembayaran       :</td><td align=right>".$bankDesc."</td></tr>";
                    echo "<tr><td align=right><strong>Nomor Rekening</strong>     :</td><td align=right>".$bank_acc."</td></tr>";
                    echo "<tr><td align=right><strong>Atas Nama</strong>     :</td><td align=right>".$bank_pemilik."</td></tr>";
                    echo "<tr><td align=right><strong>Nomor Referensi</strong>     :</td><td align=right>".$kode_ref."</td></tr>";
                    echo "<tr><td align=right>&nbsp;</td></tr>";
                    echo "<tr><td align=right>Total Pembelanjaan    :</td><td align=right>Rp. ".number_format($tot_belanja, 2, ",", ".")."</td></tr>";
                    echo "<tr><td align=right>Kode Unik    :</td><td align=right>".number_format($kode_unik, 2, ",", ".")."</td></tr>";

                    //echo "<tr><td align=right>Biaya Kirim           :</td><td align=right>".$ongkir."</td></tr>";
                    if($freeship == "1") {
                        if($tot_discount > 0) {
                            echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                            echo "<tr><td align=right>Disc Biaya Kirim        :</td><td align=right>Rp. -(".number_format($tot_discount, 2, ",", ".").")</td></tr>";
                        } else {
                            echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. 0</td></tr>";
                        }

                    } else {
                        echo "<tr><td align=right>Biaya Kirim             :</td><td align=right>Rp. ".number_format($shipping_jne_info['price'], 2, ",", ".")."</td></tr>";
                    }
                    echo "<tr><td align=right>Biaya Connectivity    :</td><td align=right>".$charge_connectivity."</td></tr>";
                    echo "<tr><td align=right>Biaya Administrasi    :</td><td align=right>".$charge_admin."</td></tr>";
                    /*if($freeship == "0") {
                        $total_semua = $tot_hrs_dibayar + $charge_connectivity + $charge_admin;
                    } else {
                        $total_semua = $res[0]->total_pay + $shipping_jne_info['price'] + $charge_connectivity + $charge_admin;
                    }*/
                    $total_semua = $totPayAndShip + $charge_connectivity + $charge_admin + $kode_unik;
                    //$total_semua = $res[0]->total_pay + $shipping_jne_info['price'] + $charge_connectivity + $charge_admin;
                    echo "<tr><td align=right><strong>Total Yang Harus Ditransfer </strong> :</td><td align=right>Rp. ".number_format($total_semua, 2, ",", ".")."</td></tr>";
                    echo "</table>";
                    $total_all = $res[0]->total_pay + $charge_connectivity;
                    //$total_all = $totPayAndShip + $charge_connectivity;
                    ?>
                </p>

                <!--<div class="col-lg-12 col-xs-12 delivery_header" style="font-size:12px;">-->

                <input type="hidden" id="total_all" value="<?php echo $total_all; ?>" />
                <input type="hidden" id="total_semua" value="<?php echo $total_semua; ?>" />

                <a href="<?php echo base_url('shop/product'); ?>"><button class="btn2 btn2 btn-primary1">
                    <i class="fa fa-arrow-right"></i>
                    <span>Kembali Ke Halaman Utama</span>
                </button></a>

                <!--</div>	-->
            </div>
            <!--<iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>-->
            <?php sgo_iframe(); ?>
            <div id="wait_message"></div>
        </div>
    </div>
</div>

<!-- Login Form -->
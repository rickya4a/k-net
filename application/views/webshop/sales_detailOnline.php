<?php
    //print_r($cekTrans);
	//if($cekTrans['response'] == "true") {
?>
<script>
    //tambahan Vera
    function getTracking(orderno){
        window.location.href = "/sales/tracking/"+orderno;
    }
</script>

<div class="row vpullset4">
    <div class="container">
    	<div class="col-md-12">
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-12">
                    Hasil Pencarian Transaksi Member
                </div>            
            </div>
            <?php
                if($cekTrans['response'] != "true"){
                    echo "Data Tidak Ada";
                }else{
            ?>
            <div class="col-lg-12 col-xs-12 cart_content" style="font-size:14px;">
                <div class="col-lg-3 col-xs-3">
                    ID Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                    <?php echo getUserID(); ?> 
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Nama Member  
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo getUsername(); ?>
                </div> 
                <div class="col-lg-3 col-xs-3">
                    Bonus Period 
                </div>
                <div class="col-lg-9 col-xs-9">
                   <?php echo $bnsperiod; ?>
                </div>             
            </div>

            <!-- table result-->
            <!-- result header-->
            <div class="col-lg-12 col-xs-12 cart_header">  
                <div class="col-lg-1 col-xs-1">No</div>
                <div class="col-lg-1 col-xs-1">Order#</div> 
                <div class="col-lg-3 col-xs-3">ID/Name</div> 
                <div class="col-lg-2 col-xs-2">Stockist</div>
                <div class="col-lg-2 col-xs-2">Tgl. Input</div>
                <div class="col-lg-1 col-xs-1">DP</div> 
                <div class="col-lg-1 col-xs-1">BV</div> 
                <div class="col-lg-1 col-xs-1">&nbsp;</div>
            <!-- result header-->

            <?php
                $no = 1;
                $totalDP = 0;
                $totalBV = 0;
                
               foreach($cekTrans['arrayData'] as $dt)
               {
                    $orderno= base64_encode($dt->orderno);
                    echo "
                        <form id=\"sendtrcd\" name=\"sendtrcd\" method=\"POST\">
                        <div class=\"col-lg-12 col-xs-12 cart_content text-center\" id=\"$no\">
                            <div class=\"col-lg-1 col-xs-1\">".$no."</div>
                            <div class=\"col-lg-1 col-xs-1\">".$dt->orderno."</div>
                            <div class=\"col-lg-3 col-xs-3\">".$dt->id_memb." - ".$dt->fullnm."</div>
                            <div class=\"col-lg-2 col-xs-2\">".$dt->idstk."</div>
                            <div class=\"col-lg-2 col-xs-2\">".date("d/m/Y",strtotime($dt->datetrans))."</div>
                            <div class=\"col-lg-1 col-xs-1\">".number_format($dt->total_pay,0,",",",")."</div>
                            <div class=\"col-lg-1 col-xs-1\">".number_format($dt->total_bv,0,",",",")."</div>
                            <div class=\"col-lg-1 col-xs-1\">
                                <a onclick=\"Sales.get_listing_ttpOnline('$dt->orderno','$bln','$thn')\">View</a>
                                |
                                <a onclick=\"getTracking('$orderno')\">Tracking</a>
                            </div>

                        </div>
                        </form>
                    ";
                    $totalDP += $dt->total_pay;
  		            $totalBV += $dt->total_bv;
                    $no++;
               }
               $totDP = number_format($totalDP,0,",",",");
               $totBV = number_format($totalBV,0,",",",");
               echo "
               <div class=\"col-lg-12 col-xs-12 cart_content text-center\" style=\"margin-bottom:20px;\">
               <div class=\"col-lg-7 col-xs-7\" >&nbsp;</div>
               <div class=\"col-lg-2 col-xs-2\" >TOTAL</div>
               <div class=\"col-lg-1 col-xs-1\" >".$totDP."</div>
               <div class=\"col-lg-1 col-xs-1\" >".$totBV."</div>
               <div class=\"col-lg-1 col-xs-1\" >&nbsp;</div>
               </div>";
               }
            ?>
        
        </div>
    </div>
 </div>

 
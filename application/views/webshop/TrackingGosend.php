<script>
   function getStatus(){
       var notrans=document.getElementById("orderno").value;

       $.ajax({
           url:"https://www.k-net.co.id/getNo/"+notrans,
           type: "GET",
           dataType:"json",
           success: function (data) {
               console.log(data.tglorder);

               var cno_order= data.message;

               $("#no_trans").text(": "+notrans);
               $("#tgl_order").text(": "+data.tglorder);
               $("#no_resi").text(": "+data.message);
               $("#jenis_kirim").text(": "+data.jenis);

               $.ajax({
                   url:"https://www.k-net.co.id/status_booking",
                   type: "POST",
                   dataType:"json",
                   data:{
                       orderno:notrans
                   },
                   success: function (result) {
                       //console.log(result);

                       $("#kurir").text(": "+result.driverName+" / "+result.driverPhone);
                       document.getElementById("image").src=result.driverPhoto;

                       if(result.receiverName != null){
                           $("#penerima").text(": "+result.receiverName);
                       }else{
                           $("#penerima").text(": -");
                       }

                       if(result.status == 'Cancelled'){
                           $("#Cancel").text(result.cancelDescription);
                       }else{
                           $("#Cancel").text("");
                       }

                       document.getElementById("live_track").value=result.liveTrackingUrl;
                       $('#live_track').click(function() {
                           window.open(result.liveTrackingUrl, '_blank');
                       });

                   }
               });

           }

       });

       $.ajax({
           url:"https://www.k-net.co.id/getWebhook/"+notrans,
           type: "GET",
           dataType:"json",
           success: function (dt) {
               //console.log("masuk sini");
               console.log(dt);

               var arr=dt.message;

               var uniqueNames = [];
               $.each(arr, function(i, el){
                   if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
               });

               console.log(uniqueNames);
               $.each(arr, function (index, value) {
                   var nket1;
                   var nket2;
                   var ket;
                   var waktu;
                   /*ket=value.replace("confirmed", "Kami sedang melakukan pencarian Kurir");
                   ket=value.replace("allocated", "Kurir telah didapatkan");
                   ket=value.replace("out_for_pickup", "Kurir dalam perjalanan untuk pick-up");
                   ket=value.replace("picked", "Pesanan telah diambil Kurir");
                   ket=value.replace("out_for_delivery", "Kurir mengantarkan pesanan");
                   ket=value.replace("delivered", "Pesanan telah terkirim");*/

                   if(value.match(/confirmed/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("confirmed", "Kami sedang melakukan pencarian Kurir");

                       waktu=nket2.replace("Kami sedang melakukan pencarian Kurir", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/allocated/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("allocated", "Kurir telah didapatkan");

                       waktu=nket2.replace("Kurir telah didapatkan", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/out_for_pickup/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("out_for_pickup", "Kurir dalam perjalanan untuk pick-up");

                       waktu=nket2.replace("Kurir dalam perjalanan untuk pick-up", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/picked/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("picked", "Pesanan telah diambil Kurir");

                       waktu=nket2.replace("Pesanan telah diambil Kurir", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/out_for_delivery/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("out_for_delivery", "Kurir mengantarkan pesanan");

                       waktu=nket2.replace("Kurir mengantarkan pesanan", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/delivered/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("delivered", "Pesanan telah terkirim");

                       waktu=nket2.replace("Pesanan telah terkirim", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/cancelled/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("cancelled", "Pesanan telah dibatalkan");

                       waktu=nket2.replace("Pesanan telah dibatalkan", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/no_driver/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("no_driver", "Driver Tidak Dapat Ditemukan");

                       waktu=nket2.replace("Driver Tidak Dapat Ditemukan", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/on_hold/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("on_hold", "Driver Pending Pengiriman");

                       waktu=nket2.replace("Driver Pending Pengiriman", "");
                       ket=nket2.replace(waktu,"");

                   }else if(value.match(/rejected/g)){
                       nket1=value.replace("+", "");
                       nket2=nket1.replace("rejected", "Driver Mengembalikan Ke Penjual");

                       waktu=nket2.replace("Driver Mengembalikan Ke Penjual", "");
                       ket=nket2.replace(waktu,"");

                   }else{
                       ket="Kami telah menerima rincian pesanan Anda";
                       waktu= data.tglorder;
                   }



                   tr = $("<tr></tr>");
                   tr.append("<td width='20%'>"+ waktu +"</td>");
                   tr.append("<td width='60%'>" + ket + "</td>");
                   $("#hasilTable").append(tr);
                   tr = $("<tr></tr>");
                   tr.append("<td width='20%'>&nbsp;</td>");
                   tr.append("<td width='60%'>&nbsp;</td>");
                   $("#hasilTable").append(tr);
               });

           }
       });


   }
</script>
<!DOCTYPE HTML>
<html>
<head>
    <title>K-Link by GOSEND</title>
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Custom Theme files -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" >
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>
    <script src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
    <style>
        .content{
            padding-left: 10px;
        }

    </style>
</head>
<body>

<!--Header-->
<div class="header">
    <!-- top-header -->
    <div class="header_top">
        <!-- top-container -->
        <div class="container">
            <div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : customer_service@k-link.co.id</p>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- top-container -->
    </div>
    <!-- top-header -->

    <!-- bottom-header -->
    <div class="header_bottom">
        <div class="container">

            <!-- Right Menu-->
            <div class="header_bottom_right">

            </div>
            <!-- Right Menu-->
        </div>
    </div>
    <!-- bottom-header -->
</div>
<!--Header-->

<div class="content">
    <input type="text" id="orderno" name="orderno" placeholder="Masukkan No. Order"> <input type="button" id="submit" name="submit" value="Submit" onclick="getStatus()">
</div>
<br>
<!-- Logo -->
<div class="logo">
    <a href="index.php"><img id="image" src="" alt="" width="30%" height="30%"/></a>
</div>
<!-- Logo -->
<br><br>
<div class="content">
    <table width="100%" border="0">
        <!--START STATUS API-->
        <tr>
            <td width="40%">Nomor Trans </td>
            <td width="60%" id="no_trans"></td>
        </tr>
        <tr>
            <td width="40%">Tgl Trans </td>
            <td width="60%" id="tgl_order"></td>
        </tr>
        <tr>
            <td width="40%">No. Resi </td>
            <td width="60%" id="no_resi"></td>
        </tr>
        <tr>
            <td width="40%">Metode </td>
            <td width="60%" id="jenis_kirim"></td>
        </tr>
        <tr>
            <td width="40%">Kurir</td>
            <td width="60%" id="kurir"></td>
        </tr>
        <tr>
            <td width="40%">Penerima</td>
            <td width="60%" id="penerima"></td>
        </tr>
        <tr>
            <td width="40%">&nbsp;</td>
            <td width="60%">&nbsp;</td>
        </tr>
        <tr>
            <td width="40%">Live Tracking</td>
            <td width="60%"><input type="button" value="" id="live_track"></td>
        </tr>
        <!--END STATUS API-->
        <!--START STATUS WEBHOOK-->
        <tr>
            <td width="40%">&nbsp;</td>
            <td width="60%">&nbsp;</td>
        </tr>
        <tr>
            <td width="40%">&nbsp;</td>
            <td width="60%">&nbsp;</td>
        </tr>
        <tr>
            <td style="color:#208612"><strong>Status Pengiriman</strong></td>
            <td id="Cancel"></td>
        </tr>
        <tr>
            <td width="40%">&nbsp;</td>
            <td width="60%" ></td>
        </tr>
        <tbody id="hasilTable" align="left">
        </tbody>
       <!-- <tr>
            <td colspan="2">
                <table width="100%" border="0">
                    <tbody id="hasilTable" align="left">
                    </tbody>
            </td>
        </tr>-->

        <!--END STATUS WEBHOOK-->
    </table>
</div>
<div class="clearfix"><br><br></div>

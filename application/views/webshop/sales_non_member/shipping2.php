<script>
    function validateCheckMaxOrder(){
        //alert("masuk sini");
        var x = document.getElementsByName("qty[]").length;
        var y=0;
        for (var i= 1; i <= x; i++) {
            var element1=document.getElementById('qty'+i).value;
            var element2=document.getElementById('max_order'+i).value;

            console.log(element1 + " | " + element2);

            if(element1 > 10){
                alert("Maksimum Order " + element2+ " pcs");
                $("#qty" +i).val(1);
                $("#qty" +i).focus();
                var y = 1;
                break;
            }
            //nprd.push(prd+" "+qty+" pcs");
        }

        console.log("Y: "+ y);
        if(y <= 0){
            Shopping_cust.validateCheckOut();
        }

    }
</script>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Shopping&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Pengiriman&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
    <div class="container">
        <div id="formCart1">

            <!--<form id="formCart" action="<?php /*echo base_url('shop/cart/checkout'); */?>" method="POST" onsubmit="return Shopping_cust.validateCheckOut()">-->
            <form id="formCart" action="<?php echo base_url('shop/cart/checkout'); ?>" method="POST" onsubmit="return validateCheckMaxOrder()">
                <!-- Delivery Options--><br />
                <div class="col-md-6 col-xs-12">
        	<span class="title_header">
            	<i class="fa fa-truck"></i> Tipe Pengiriman ..
            </span>
                    <ul><!-- li>Pengumuman : berkenaan dengan libur hari raya idul fitri 2017, maka pengiriman akan dihentikan sementara mulai tanggal 22 Juni 2017 s/d 04 Juli 2017.</li></ul -->
                        <?php
                        $sales_lp = $this->session->userdata("sales_lp");
                        if($sales_lp != null) {
                            $nama = $sales_lp['nama'];
                            $email = $sales_lp['email'];
                            $id_memb = $sales_lp['idmemb'];
                            $id_lp = $sales_lp['id_lp'];
                            $nmmemb = $sales_lp['nmmemb'];
                        } else {
                            $nama = "";
                            $email = "";
                            $id_memb = "IDJKID004717";
                            $id_lp = "";
                            $nmmemb = "ISMIYANI";
                        }
                        ?>


                        <!--  Options 2-->
                        <div class="col-md-12 col-xs-12 nopadding voffset4">
                            <!--  Options 2 header-->
                            <div class="col-md-12 col-xs-12 delivery_header">
                                <input name="delivery" checked="checked" type="radio" class="delivery_choice" onclick="Shopping_cust.getDelChoice(this.value)" value="2"/> Dikirim ke Alamat <!-- (Pilihan ini belum direkomendasikan/masih dalam pengembangan) -->
                            </div>
                            <!--  Options 2 header-->

                            <!--  Options 2 delivery address-->
                            <div class="col-md-12 col-xs-12 vpullset3 dikirim">
                                <!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

                                <div class="register-top-grid">
                                    <div>
                                        <span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
                                        <input class="uppercase" type="text" name="nama_penerima" id="nama_penerima" value="<?php echo $nama; ?>" />
                                    </div>

                                    <div>
                                        <span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
                                        <input class="numeric-input" type="text" name="notlp" id="notlp"/>
                                    </div>
                                    <div style="width:98%">
                                        <span>Email Address</span>
                                        <input type="text" name="email" id="email" value="<?php echo $email; ?>" />
                                    </div>
                                    <div style="width:98%">
                                        <span>Alamat<label><font color="red">&nbsp;*</font></label></span>
                                        <textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"></textarea>
                                        <input type="hidden" id="destination_address" name="destination_address" value="" />
                                    </div>

                                    <div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
                                        Pilih Stockist Terdekat Dengan Alamat Tujuan
                                    </div>

                                    <div>
                                        <span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="provinsi" id="provinsi" onchange="Shopping_cust.show_kota(this,'#kota')" >
                                            <option value="">--Select Here--</option>
                                            <?php
                                            //print_r($show_provinsi);
                                            foreach($show_provinsi['arrayData'] as $row){
                                                echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";
                                            }

                                            ?>
                                        </select>
                                        <input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
                                    </div>
                                    <div>
                                        <span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
                                        <select class="form-list" name="kota" id="kota" onchange="Shopping_cust.show_kecamatan(this,'#kecamatan')">
                                        </select>
                                        <input type="hidden" id="nama_kota" name="nama_kota" value="" />
                                        <!--<input type="text" name="kota" id="kota"/>-->
                                    </div>
                                    <div>
                                        <span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
                                        <!--<input type="text" name="kecamatan" id="kecamatan"/>-->
                                        <select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping_cust.show_kelurahan(this,'#stockistref')" >

                                        </select>
                                        <input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
                                    </div>

                                    <div>
                                        <span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
                                        <select class="form-list" name="stockistref" id="stockistref" onchange="Shopping_cust.show_pricestkY(this,'#pricecode')">
                                        </select>
                                        <input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="" />
                                        <input type="hidden" id="sender_address" name="sender_address" value="" />
                                        <input type="hidden" id="jne_branch" name="jne_branch" value="" />
                                        <input type="hidden" id="shipper" name="shipper" value="1" />
                                    </div>
                                    <div class="clearfix"> </div>


                                </div>
                            </div>
                            <!--  Options 2 delivery address-->
                        </div>
                        <!--  Options 2-->
                </div>

                <div id="listCartPrd">
                    <!-- Order Details-->
                    <div class="col-md-6 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Detail Pembelanjaan
                </span>
                        <div class="register-top-grid">
                            <?php
                            if($sales_lp != null) {

                                ?>
                                <input type="hidden" class="required uppercase" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo $id_memb;?>"/>
                                <input type="hidden" name="membername" id="membername" value="<?php echo $nmmemb;?>"/>
                                <input type="hidden" name="bnsperiod" id="bnsperiod" value="<?php echo date("d/m/Y"); ?>">
                                <input type="hidden" name="id_lp" id="id_lp" value="<?php echo $id_lp; ?>">
                                <div class="clearfix"> </div>
                                <?php
                            } else {
                                ?>
                                <input type="hidden" class="required uppercase" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo $id_memb; ?>"/>
                                <input type="hidden" name="membername" id="membername" value="<?php echo $nmmemb;?>"/>
                                <input type="hidden" name="bnsperiod" id="bnsperiod" value="<?php echo date("d/m/Y"); ?>">
                                <input type="hidden" name="id_lp" id="id_lp" value="">
                                <div class="clearfix"> </div>
                                <?php
                            }
                            ?>
                        </div>
                        <br />
                        <?php
                        $cart_check = $this->cart->contents();

                        // If cart is empty, this will show below message.
                        if(empty($cart_check)) {
                            echo '<br/>To add products to your shopping cart click on "Add to Cart" Button';
                        }
                        ?>
                        <!-- header-->
                        <div class="col-md-12 col-xs-12 cart_header">
                            <div class="col-md-3 col-xs-3">Product </div>
                            <div class="col-md-2 col-xs-2" align="right">Qty</div>
                            <div class="col-md-1 col-xs-1" align="right">BV</div>
                            <div class="col-md-2 col-xs-2" align="right">Price</div>
                            <div class="col-md-3 col-xs-3" align="right">Amount</div>
                            <!--<div class="col-md-3 col-xs-3" align="right">Tot BV</div> -->
                            <div class="col-md-1 col-xs-1"><i class="fa fa-trash-o"></i></div>
                        </div>
                        <!-- header-->

                        <!-- product item-->
                        <?php
                        $n = 1;
                        foreach($cart as $dt)
                        {
                            $rowid = $dt['rowid'];
                            $prdcd = $dt['id'];
                            $prdnm = $dt['name'];
                            $westPrice = $dt['west_Cprice'];
                            $qty = $dt['qty'];
                            $bv = $dt['bv'];
                            $eastPrice = $dt['east_Cprice'];
                            $max_order = $dt['max_order'];

                            //echo "MaX: ".$max_order;

                            ?>
                            <div id="<?php echo $n;?>">
                                <div class="col-md-12 col-xs-12 order_wrap">
                                    <div class="col-md-3 col-xs-3 order_desc">
                                        <a href="#">
                                            <input type="hidden" value="<?php echo $prdnm;?>" name="prdnm[]" id="<?php echo "prdnm".$n;?>" readonly="yes"/>
                                            <h3><?php echo $prdnm;?> </h3></a>
                                        <!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        <input class="orderdetails-box" onkeyup="Shopping_cust.setNewPrice(<?php echo $n;?>)" type="text" value="<?php echo $qty;?>" name="qty[]" id="<?php echo "qty".$n;?>"/>
                                    </div>
                                    <div class="col-md-1 col-xs-1" align="right"><?php echo $bv;?>
                                        <input class="orderdetails-box" type="hidden" value="<?php echo $bv;?>" name="bv[]" id="<?php echo "bv".$n;?>" readonly="yes"/>
                                        <input class="orderdetails-box" type="hidden" value="<?php echo $max_order;?>" name="max_order[]" id="<?php echo "max_order".$n;?>" readonly="yes"/>
                                    </div>
                                    <div class="col-md-2 col-xs-2 westP" align="right"><?php echo "".number_format($westPrice,0,",",".")."";?>
                                        <input class="orderdetails-box" type="hidden" value="<?php echo $westPrice;?>" name="westPrice[]" id="<?php echo "westPrice".$n;?>" readonly="yes"/>
                                    </div>
                                    <div class="col-md-3 col-xs-3 westP" align="right" id="<?php echo "divSubTotWestPrice".$n ?>"><?php echo "".number_format($dt['subtotal_west_Cprice'],0,",",".").""; ?>

                                    </div>
                                    <div class="col-md-2 col-xs-2 eastP" align="right" style="display: none;"><?php echo "".number_format($eastPrice,0,",",".").""; ?>
                                        <input class="orderdetails-box" type="hidden" value="<?php echo $eastPrice;?>" name="eastPrice[]" id="<?php echo "eastPrice".$n;?>" readonly="yes"/>
                                    </div>
                                    <div class="col-md-3 col-xs-3 eastP" align="right" id="<?php echo "divSubTotEastPrice".$n ?>" style="display: none;" ><?php echo "".number_format($dt['subtotal_east_Cprice'],0,",",",").""; ?>

                                    </div >
                                    <!--
                                    <div class="col-md-3 col-xs-3 divTotBV">

                                    </div>
                                    -->
                                    <div class="col-md-1 col-xs-1 text-center">
                                        <input type="hidden" id="<?php echo "rowid".$n; ?>" value="<?php echo $rowid;?>" name="rowid[]"/>
                                        <a href="#" id="<?php echo $rowid;?>" onclick="Shopping_cust.delete_rowCart(<?php echo $n; ?>)"><i class="fa fa-minus-circle"></i></a>
                                    </div>
                                    <input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_Cprice']; ?>" name="amountWest[]" id="<?php echo "amountWest".$n;?>" readonly="yes"/>
                                    <input class="amtE orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_Cprice'];?>" name="amountEast[]" id="<?php echo "amountEast".$n;?>" readonly="yes"/>
                                </div>

                            </div>
                            <!-- product item-->
                            <?php
                            $n++;
                        }
                        ?>

                        <!-- subtotal -->
                        <div class="col-md-12 col-xs-12 order_total_wrap westP">
                            <div class="col-md-3 col-xs-3">SUBTOTAL</div>
                            <div class="col-md-2 col-xs-2" id="divTotQtyWest"><?php echo $this->cart->total_items(); ?>

                            </div>
                            <div class="col-md-3 col-xs-3">&nbsp;</div>
                            <div class="col-md-1 col-xs-1">
                                <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>
                            </div>
                            <div class="col-md-2 col-xs-2"></div>
                            <div class="col-md-3 col-xs-3">
                                <input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_west_price');?>" name="totalWestPrice" id="totalWestPrice" readonly="yes"/>
                            </div>
                            <div class="col-md-3 col-xs-3" id="totAllWestPrice" align="right"><?php echo "".number_format($this->cart->total_west_Cprice(),0,",",",");?>

                            </div>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items(); ?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_west_Cprice();?>" name="totalAmountWest" id="totalAmountWest" readonly="yes"/>
                        </div>
                        <div class="col-md-12 col-xs-12 order_total_wrap eastP " style="display: none;">
                            <div class="col-md-3 col-xs-3">SUBTOTAL</div>
                            <div class="col-md-2 col-xs-2" id="divTotQtyEast"><?php echo $this->cart->total_items();?>

                            </div>
                            <div class="col-md-3 col-xs-3">&nbsp;</div>
                            <div class="col-md-1 col-xs-1 ">
                                <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>
                            </div>
                            <div class="col-md-2 col-xs-2"></div>
                            <div class="col-md-3 col-xs-3">
                                <input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_east_price');?>" name="totalEastPrice" id="totalEastPrice" readonly="yes"/>
                            </div>
                            <div class="col-md-3 col-xs-3" id="totAllEastPrice" align="right"><?php echo "".number_format($this->cart->total_east_Cprice(),0,",",",");?>

                            </div>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items();?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
                            <input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_east_Cprice();?>" name="totalAmountEast" id="totalAmountEast" readonly="yes"/>
                        </div>
                        <!-- subtotal

                        <!-- subtotal -->
                        <div class="col-md-12 col-xs-12 order_add">
                            <a href="<?php echo "".site_url('shop/product/')."";?>">
                                <i class="fa fa-shopping-cart"></i> Tambahkan Lagi Keranjang Belanja Anda
                            </a>
                        </div>
                        <!-- subtotal -->
                    </div>
                    <!-- Order Details-->
                </div>

                <!-- next button-->
                <div class="col-md-12">

                    <input type="hidden" name="pricecode" id="pricecode" value="<?php echo $this->session->userdata('pricecode'); ?>" />
                    <!--<a href="payment.php" class="btn1 btn2 btn-primary1 pull-left"><span>Lanjutkan</span><i class="fa fa-arrow-right"></i></a>-->

                    <!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" onclick="Shopping_cust.checkout()" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />-->
                    <button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />
                </div>
                <!-- next button-->
        </div>
        </form>
        <!-- Delivery Options-->
    </div> <!-- End formCart1-->
    <div id="divCheckOut"></div>
    <div id="afterPayment"></div>
</div>
</div>
<!--Checkout Wrapper-->

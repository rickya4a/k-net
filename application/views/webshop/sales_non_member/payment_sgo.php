<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Shopping&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Payment&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->
<!--Payment Wrapper-->
<div class="row vpullset4">
	<div class="container">
       <div id="paymentDiv">
       	<!--<form action="<?php echo site_url('checkout_process'); ?>" method="POST" id="payment-form">
    	 Left Content-->
        <div class="col-lg-7 col-xs-12 nopadding voffset4">
        	<!-- Billing Info-->
        	<div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-usd"></i> Informasi Penerima
                </span>
                <div class="col-lg-12 col-xs-12 vpullset3"> 
            		<div class="col-lg-12 col-xs-12 delivery_header">
                    	
                   
                    
                    <!--form pengiriman-->
					
                        <div class="register-top-grid">
                            <?php
                              $inp = $this->session->userdata('personal_info');
							  if($inp['delivery'] == "2")
							  {
                            ?>
                            <div>
                            <span>Nama Depan<label><font color="red">*</font></label></span>
                                <input type="text" id="nama_penerima"  readonly="readonly" value="<?php echo $inp['nama_penerima']; ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo $inp['notlp']; ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo $inp['email']; ?>"> 
                            </div>
                            
                            
                            <?php
                             } else {
                            ?>
                            <div>
                            	<span>Nama <label><font color="red">*</font></label></span>
                                <input type="text" id="firstname"  readonly="readonly" value="<?php echo getUsername(); ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo getUserPhone(); ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo getUserEmail(); ?>"> 
                            </div>
                            <?php
							 }
                            ?>
                            <div class="clearfix"> </div>					   
                        </div>				     
					  </div>
                	<!--form pengiriman--> 
				</div>   
			</div>
            <!-- Billing Info-->
        	
            <!-- Paymen Method-->
			<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-money"></i> Metode Pembayaran
                </span>
                <!--<div class="col-lg-12 col-xs-12 nopadding voffset4">   
                    <div class="col-lg-12 col-xs-12">       	
                        <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                            <div class="register-top-grid">                            
                                <div class="nomargin">
                                    <select class="form-list small" id="chooseCardType" name="chooseCardType">
                                      <option value="" selected>--Pilih Kartu--</option>	
                                      <option value="cc">Kartu Kredit</option>
                                      <option value="dc">Debit Card</option>
                                      
                                    </select>
                                </div>
                                <div class="nomargin">
                                	<select class="form-list small" id="payType" name="payType">
                                		<option value="" selected>--Pilih jenis pembayaran--</option>	
                                	</select>	
                                </div>	
                            </div>
                        </div>
                -->
                <div class="col-lg-12 col-xs-12 nopadding voffset4"> 
                   <div class="col-lg-12 col-xs-12"> 	      	
                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                        <div class="register-top-grid">                            
                            <div class="nomargin"> 
                                <form id="selPayment" action="<?php echo base_url('shop/pay/preview'); ?>" method="post">
                                Silahkan Pilih :
                                <select id="bank" name="bank" onchange="setSelectPay()">
                                	<?php
                                	 echo "<option value=\"\">--Pilih disini--</option>";
                                	
                                	  foreach($listBank as $dta) {
                                	  	echo "<option value=\"$dta->id|$dta->bankCode|$dta->charge_connectivity|$dta->charge_admin|$dta->bankDesc\">$dta->bankDisplayNm</option>";
                                	  }
                                	?>
                                </select>
                                <input type="hidden" name="totalx" value="" />
                                <input type="hidden" name="temp_orderidx" value="<?php echo $key;?>" />
                        		<input type="hidden" name="temp_paymentIdx" value="<?php echo $payID;?>" />
                                <input type="hidden" name="bankid" id="bankid" value=""  />
                                <input type="hidden" name="bankCode" id="bankCode" value=""  />
                                <input type="hidden" name="bankDesc" id="bankDesc" value=""  />
                                <input type="hidden" name="charge_connectivity" id="charge_connectivity" value=""  />
                                <input type="hidden" name="charge_admin" id="charge_admin" value=""  />
                                
                                <input type="submit" value="Proses Pembayaran" />
                                </form>
                                
                            </div>
                             <div class="nomargin">
                             	&nbsp;
                             </div>	
                        </div>
                    </div>
                   </div> 
                           
                        <!-- detail for credit card-->
                        <!-- 02/11/2015 DION-->
    					<!--<div class="col-lg-12 col-xs-12 nopadding" id="choosePaymentDiv">
                        	<form method="post" action="<?php echo base_url('pay/inp/test'); ?>">
                        		<input type="hidden" name="temp_orderid" value="<?php echo $key;?>" />
                        		<input type="hidden" name="temp_paymentId" value="<?php echo $payID;?>" />
                        		<input type="submit" value="Test Input ke DB" />
                        	</form>
                     </div>-->
                         
                    </div>                    
				</div>
        	</div>
        	<!-- Paymen Method-->
            
			
		</div>
        <!-- Left Content-->
        
        <!-- right Content-->
		<div class="col-lg-5 col-xs-12 nopadding voffset4">
        	<!-- Adress Delivery-->
            <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-check-square-o"></i> Alamat Tujuan Pengiriman
                </span>
                <div class="col-lg-12 col-xs-12 cart_header">
                 <?php
                  $inp = $this->session->userdata('personal_info');
				  if($inp['delivery'] == "2")
				   {
				   	 //echo $inp['firstname']." ".$inp['lastname']."</br>";
				   	 echo $inp['nama_penerima']."</br>";
					 echo $inp['alamat']."</br>";
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 //echo $inp['nama_kelurahan']."</br>";
					 echo $inp['notlp']."</br>";
				   }
				  else {
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 echo $inp['nama_stockist']."</br>";
				  }	
                  ?>	
                       	 
                </div>
            </div>   
        	<!-- Adress Delivery-->
            
            <!-- Order Details-->
        	<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Detail Pembelanjaan
                </span>
				<br />
				
                <div class="col-lg-12 col-xs-12 cart_header">
				ID Member     : <?php echo $inp['idmemberx']."<br />"; ?>
				Nama Member   : <?php echo $inp['membername']."<br />"; ?>
				Periode Bonus : <?php echo $inp['bnsperiod']."<br />"; ?>
				</div>
                
                <!-- header-->
                <div class="col-lg-12 col-xs-12 cart_header">          	
                    <div class="col-lg-4 col-xs-4" align="right">Product </div>
                    <div class="col-lg-2 col-xs-2" align="right">Qty</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Price</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Amount</div>          	 
                </div>  
                <!-- header-->   
                
                <!-- product item-->
                
                
                <?php
                   $cart = $this->cart->contents();
				   //print_r($cart);
                   $pricecode = $this->session->userdata('pricecode');
                   foreach($cart as $items) {
                   	 echo "<div class=\"col-lg-12 col-xs-12 order_wrap\">";
						 echo "<div class=\"col-lg-4 col-xs-4 order_desc\">";
	                     echo "<h3>".$items['name']."</h3>";
	                     $price = 0; $sub = 0;  $totalPrice = 0; $totalPrice2 = 0; $totalBV = 0; 
	                     echo "</div>";
						 if($pricecode == "12W3") {
						 	$price = number_format($items['west_price'], 0, ",", ".");
							$sub = number_format($items['subtotal_west_price'], 0, ",", ".");
							$totalPrice = $this->cart->total_west_price();
							$totalPrice2 = number_format($this->cart->total_west_price(), 0, ",", ".");
							$totalBV = number_format($items['bv'], 0, ",", ".");
						 } else {
						 	$price = number_format($items['east_price'], 0, ",", ".");
							$sub = number_format($items['subtotal_east_price'], 0, ",", ".");
							$totalPrice = $this->cart->total_east_price();
							$totalPrice2 = number_format($this->cart->total_east_price(), 0, ",", ".");
							$totalBV = number_format($items['bv'], 0, ",", ".");
						 }
	                     echo "<div class=\"col-lg-2 col-xs-2\" align=right>".$items['qty']."</div>"; 
	                     //echo "<div class=\"col-lg-3 col-xs-3\" align=right>".$totalBV."</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".$price."</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".$sub."</div>
                     </div>";  
                   }  
                ?>	 
                    <!--<div class="col-lg-6 col-xs-6 order_desc">
                        <h3>Kinokatara</h3>
                        <p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>
                    </div>
                    <div class="col-lg-3 col-xs-3">5</div> 
                    <div class="col-lg-3 col-xs-3">Rp. 130.000</div> -->
                
                <!-- product item-->
                
                <!-- shipping cost-->
                <div class="col-lg-12 col-xs-12 order_wrap">   
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <h3>Biaya Kirim</h3>
                    </div>
                    <div class="col-lg-2 col-xs-2" align="right">
                    	<?php echo $this->cart->total_weight(); ?>
                    </div> 
                    <div class="col-lg-3 col-xs-3" align="right">&nbsp</div> 
                    <div class="col-lg-3 col-xs-3" align="right">
                    	<?php
                    	  //print_r($pricelist->price);
                    	  $shipping_jne_info = $this->session->userdata('shipping_jne_info');
						  echo number_format($shipping_jne_info['price'], 0, ",", "."); 
                    	?>
                    </div> 
                </div>  
                <!-- shipping cost-->
                
                <!-- subtotal-->
                <div class="col-lg-12 col-xs-12 order_total_wrap">   
                    <div class="col-lg-6 col-xs-6">                	
                        Subtotal
                    </div>
                    <div class="col-lg-6 col-xs-6">                	
                        <p>
                        <?php 
                        $total = $totalPrice + $shipping_jne_info['price'];
                        echo number_format($total, 0, ",", "."); 
                        ?>
                        </p>
                    </div>
                </div>
                <!-- subtotal-->
                
                <!-- order number
                <div class="col-lg-12 col-xs-12 order_number">                	
                    <p>No. Order Anda Adalah :</p>
                    <p><span>#ha7a63hd8</span></p>
                </div> --> 
                <!-- order number-->                  
			</div>
            <!-- Order Details-->            
		</div>
        <!-- right Content-->
        
        <!-- next button-->
        <div class="col-lg-12">
            <!--<button onclick="Shopping.back_to_cart('#divCheckOut','#formCart1')" type="button" class="btn1 btn2 btn-primary1 pull-left" style="margin-right:15px;" id="back"><i class="fa fa-arrow-left" ></i> Kembali ke Cart</button>&nbsp;&nbsp;
            <button onclick="Shopping.proceedPayment()" type="button" class="btn1 btn2 btn-primary1 pull-left" id="checkout"><span>Checkout</span><i class="fa fa-check-circle"></i></button>&nbsp;&nbsp;-->
                                
        </div>
        <!-- next button-->       
        
    </div>
    <!--</form>-->
    </div>
    <div id="nextPaymentProcessDIV"></div>
</div>
<script type="text/javascript">

  $("#selPayment").submit(function(event) {
	  var bankCode = $("#bankCode").val();
      var productCode = $("#bankDesc").val();
      if (bankCode != "" && productCode != "") {
	    return;
	  }
	 
	  alert("Please select payment..")
	  event.preventDefault();
	});
	
  function setSelectPay() {
  	 var x = $("#bank").val();
  	 //alert("isi : " +x);
  	 //var bankDesc = $("#bank option:selected").text();
  	 var str = x.split("|");
  	 $("#bankid").val(str[0]);
  	 $("#bankCode").val(str[1]);
  	 //$("#bankDesc").val(bankDesc);
  	 $("#bankDesc").val(str[4]);
  	 $("#charge_connectivity").val(str[2]);
  	 $("#charge_admin").val(str[3]);
  	 
  	 
  	
  }
  
  
  
  function submitdata() {
    var bankCode = $("#bankCode").val();
    var productCode = $("#bankDesc").val();
    var charge_connectivity = parseInt($("#charge_connectivity").val());
    var tot_dp = parseInt(<?php echo $total; ?>);
    var total = tot_dp + charge_connectivity;
    if (bankCode === "" || productCode === "") {
		alert("Please Select Payment Method");
		
	}else{
		
	 	 
	 	 var data = {
						key : "<?php echo $key;?>",
						paymentId : "<?php echo $payID;?>",
						paymentAmount : total,
						backUrl : encodeURIComponent("http://www.k-net.co.id/pay/sgo/finish/dev/<?php echo $payID;?>"),
						bankCode : bankCode,
						bankProduct: productCode
				    },
			sgoPlusIframe = document.getElementById("sgoplus-iframe");
					
			if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
			SGOSignature.receiveForm();
					
	}
}
</script>
<!--Payment Wrapper-->
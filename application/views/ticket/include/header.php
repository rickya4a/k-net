<!doctype html>
<html>
<head>
	<!-- ## SITE META ## -->
	<meta charset="utf-8">
	<title>Yoga Ayurveda Tour 2015 </title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<link rel="shortcut icon" href="<?php echo base_url().'ticket/images/favicon.ico';?>"/>
    
    <!-- ## LOAD STYLSHEETS ## -->
	<link rel="stylesheet" media="all" href="<?php echo base_url().'ticket/css/style.css';?>"/>

	<!-- ## GOOGLE FONTS ## -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'/>
    
	<!-- SGO DEV-->
	 <script type="text/javascript" src="http://secure-dev.sgo.co.id/public/signature/js"></script> 

	<!-- SGO PROD-->
	<!-- <script type="text/javascript" src="https://secure.sgo.co.id/public/signature/js"></script> -->

	<script src="<?php echo base_url() ?>asset/js_module/global.js"></script>

</head>

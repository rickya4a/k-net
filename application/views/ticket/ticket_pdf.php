<?php    
        $pdf=new FPDF('P','mm', 'A4');
        $pdf->Open();
        $pdf->SetFillColor(255,255,255); // background = biru muda
        $pdf->SetTextColor(0,0,0);	 //	font color = black
        $pdf->SetDrawColor(0,0,0); // border 	   = brown	
        $pdf->SetLineWidth(.1);
        $pdf->AddPage();
        
        $pdf->Image("assets/images/logo_klink.jpg",10,10,180,25);
        
        $pdf->SetXY(10,45);
        $lebarCell = 7;
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Helvetica','B',14);
        
        foreach($head as $dt){
            $eventPlace = $dt->event_address;
            $eventDesc = $dt->event_desc;
            $eventDate = $dt->event_date;
            $pay_type = $dt->pay_type;
            $TOT_dP = $dt->trans_hdr_dp;
            $flagpaid = $dt->flag_paid;
        }
        if($pay_type == '0'){
            $txtHeader = 'BUKTI PENDAFTARAN K-LINK TIKET YOGA';
            $txtStatus = 'Transfer';
            //$pdf->Cell(170,$lebarCell,"BUKTI PENDAFTARAN K-LINK TIKET YOGA",0,0,'C',true);
        }else{
            $txtHeader = 'BUKTI PEMBELIAN K-LINK TIKET YOGA';
            $txtStatus = 'Internet Banking';
            //$pdf->Cell(170,$lebarCell,"BUKTI PEMBELIAN K-LINK TIKET YOGA",0,0,'C',true);
        }
        $pdf->Cell(170,$lebarCell,$txtHeader,0,0,'C',true);
         
        $pdf->Ln();
        $pdf->Ln();
        
        
        
        $pdf->SetFont('Helvetica','B',8);
        $pdf->SetFillColor(219,215,217);
        $pdf->Cell(180,$lebarCell,"LISTING",1,0,'C',true);
        $pdf->Ln();
        
        $pdf->Cell(35,$lebarCell,"TICKET NUMBER",1,0,'C',true);
        $pdf->Cell(65,$lebarCell,"NAME",1,0,'C',true);
        $pdf->Cell(30,$lebarCell,"EVENT DATE",1,0,'C',true);
        $pdf->Cell(50,$lebarCell,"EVENT PLACE",1,0,'C',true);
        $pdf->Ln();
        
        foreach($det as $row)
        {
            $pdf->SetFont('Helvetica','',8);
            $pdf->SetFillColor(255,255,255);
        
            $pdf->Cell(35,$lebarCell,$row->trans_tiket_no,1,0,'L',true);
            $pdf->Cell(65,$lebarCell,$row->trans_det_name,1,0,'L',true);
            $pdf->Cell(30,$lebarCell,date('d/m/Y',strtotime($eventDate)),1,0,'R',true);
            $pdf->Cell(50,$lebarCell,$eventDesc ." / ".$eventPlace,1,0,'R',true);
            $pdf->Ln();
        }
        
        $pdf->Ln();
        $pdf->SetFont('Helvetica','',9);
        $pdf->Cell(50,$lebarCell,"Total Pembayaran",0,0,'L',true);
        $pdf->Cell(50,$lebarCell,"Rp. ".number_format($TOT_dP,0,".","."),0,0,'R',true);
        $pdf->Ln();
        $pdf->Cell(50,$lebarCell,"Metode Pembayaran",0,0,'L',true);
        $pdf->Cell(50,$lebarCell,$txtStatus,0,0,'R',true);
        $pdf->Ln();
        $pdf->Cell(50,$lebarCell,"Status Pembayaran",0,0,'L',true);
        if($flagpaid == '1'){
            $status = 'Approved';
        }else{
            $status = 'Pending';
        }
        $pdf->Cell(50,$lebarCell,$status,0,0,'R',true);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Helvetica','',8);
        $pdf->Cell(190,$lebarCell,"SYARAT DAN KETENTUAN :",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"1. Pembayaran melalui transfer, mohon transfer ke nomor rekening BCA KCP RICCI 6440628395 a/n K-LINK NUSANTARA",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"2. Lakukan Pembayaran sesuai nominal diatas agar pembayaran Anda terkonfirmasi secara otomatis ",0,0,'L',true);
        $pdf->Ln();
        $pdf->Cell(190,$lebarCell,"3. Status tiket yoga masih pending, jika pembayaran dilakukan melalui transfer",0,0,'L',true);
        $pdf->Ln();
        $pdf->Ln();
        
        $pdf->Output();
?>
<body>
<!-- ## HEADER ## -->
<header id="qcHeader">
	<div class="row">

		<!-- ## LOGO ## -->
		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo site_url('yoga');?>"><img src="<?php echo base_url().'ticket/images/logo-header.png';?>" alt="" /></a>
		</div>

		<!-- ## SITE NAVIGATION ## -->
		<nav id="qcPriNav" class="col-6 col">
			<ul class="clearfix">
				<li><a href="<?php echo site_url('event');?>"><i class="icon-calendar-2 icon"></i> <span>Event Details</span></a></li>
				<li><a href="<?php echo site_url('ticketing');?>"><i class="icon-ticket icon"></i> <span>Buy Tickets</span></a></li>
			</ul>
		</nav>

	</div>
</header>
<!-- ## HEADER END ## -->


<!-- ## CONTENT WRAPPER ## -->
<div id="qcContentWrapper">

	<!-- ## PAGE TITLE ## -->
	<section id="qcSecbar">
		<div class="qcContainer">
			<h1>Ticket. <span>K-Ayurveda Tour Yoga 2015</span></h1>
		</div>
	</section>

	<!-- ## PAGE CONTENT ## -->
	<section id="qcContent">
		<div class="qcContainer">

			<!-- ## MOB NAV ## -->
			<div id="qcMbTrigger"></div>

			<!-- ## TABS ## -->
			<div id="qcTabs" class="tabs">

				<!-- ## TAB NAV ## -->
				<ul id="qcTabNav" class="clearfix">
					<li><a href="#tab-1"><i class="icon-ticket icon"></i> <span>Book Ticket</span></a></li>
					<li><a href="#tab-2"><i class="icon-lkdto icon"></i> <span>Price</span></a></li>
					<li><a href="#tab-3"><i class="icon-book-open icon"></i> <span>FAQ's</span></a></li>
					<li><a href="#tab-4"><i class="icon-mail-1 icon"></i> <span>Contact</span></a></li>
				</ul>


				<!-- ===============================================

					PAGE 1 - TICKET

				=============================================== -->
				<div id="tab-1" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="ticket row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>Buy ticket</h4>
								<p class="qcPageDesc full">Pendaftaran K-Ayurveda Tour Yoga 2015 hanya bisa dilakukan melalui online dan tempatnya sangat terbatas, pastikan data yang anda masukan benar</p>
								<ol>
									<li style="margin-bottom:20px;"><strong>Pengambilan Hampers
										Dilakukan 1 minggu sebelum acara di masing-masing kota di lakanakan : </strong>
										<ul style="padding-left:15px;">
											<li>Bali 17 September 2015 s/d 24 September 2015</li>
											<li>Jakarta 18 September 2015 s/d 25 September 2015</li>
											<li>Surabaya 19 September 2015 s/d 26 September 2015</li>
										</ul>
									</li>
									<li><strong>Persyaratan pengambilan Hampers : </strong>
										<ul style="padding-left:15px;">
											<li>Harus menyertai Bukti/ konfirmasi telah melakukan registrasi online saat pengambilan hampers.</li>
											<li>Pengambil hampers adalah orang yang datanya terdaftar dalam registrasi online, apabila orang yang bersagkutan berhalangan, bisa digantikan dengan orang lain, selain membawa bukti registrasi online juga harus dilengkapi surat kuasa dan KTP asli peserta yang terdaftar di registrasi online.</li>
										</ul>
									</li>
								
							</div>
						</div>

						<!-- ## TICKET ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<div class="qcTcktRegForm">
								
									
									
									<!-- ## TICKET FORM ## -->
									<!-- <form id="qcTcktForm" class="qcForm" name="qcTcktForm" method="post" action="<?php echo site_url('ticketing/payment');?>" > -->
										<!-- ## MODULE TITLE ## -->
										<div class="qcModTitle">
											<h1>Payment Method</h1>
											<p><?php
												//foreach ($variable as $key => $value) {
													
												//}
												?></p>
										</div>
										  
                                            <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
                                                <select name="bank" id="bank" name="bank">
                                                    <?php
                                                        foreach($bank as $xx){
                                                            echo "<option value=\"$xx->bankCode\">$xx->bankDesc</option>";      
                                                        }
                                                    
                                                    ?>
                                                </select>
                                                <input type="submit" name="submit" onclick="submitdata()"/>
                                       
									<!-- </form> -->
									<div id="resTEs"></div>
								</div>
								<!-- ## TICKET FORM END ## -->
							</div>
						</div>
						<!-- ## TICKET END ## -->

					</div>
					<!-- ## ROW END ## -->
					

				</div>
				<!-- ## PAGE 1 END ## -->



				<!-- ===============================================

					PAGE 2 - FAQ's

				=============================================== -->
				<div id="tab-3" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>FAQ's<span> Frequently asked questions</span></h4>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<p class="qcPageDesc full">Secara bersama-sama yoga dan Ayurveda akan
								menciptakan keseimbangan anatar tubuh dan pikiran sehingga tercapai kesehatan tubuh. 
								Ayurveda akan membatu keseimbangan jasmani dan yoga akan menciptakan keseimbangan rohani/spiritual.</p>
							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">

								<!-- ## FAQ's LIST ## -->
								<div class="qcEventlayout">
									<!-- ## FAQ 3 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>1.</span> Kapan voucher ditukar? Dimana voucher dapat ditukar?</a>
										</h4>
										<div class="toggle-content">Voucher dapat ditukar pada H-2 atau H-1 atau langsung di venue acara. Voucher tiket dapat ditukarkan di K-Mart, K-LINK Tower Jl Gatot Subroto No. 59 A Jakarta Selatan 12950 tlp. 021 29027000
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									<!-- ## FAQ 4 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>2.</span> Apa Yang Harus Dibawa Saat Penukaran?</a>
										</h4>
										<div class="toggle-content">Yang wajib di bawa saat penukaran voucher dengan tiket asli adalah:
										<ul style="padding-left:20px;">
											<li>Voucher pemesanan</li>
											<li>Kartu identitas asli yang datanya sesuai dengan data di voucher</li>
											<li>Bila pembayaran menggunakan kartu kredit, maka kartu kredit asli wajib dibawa dan ditunjukkan saat penukaran (untuk menghindari penyalahgunaan kartu/fraud). Bila tidak dapat menunjukkan kartu kredit asli, maaf voucher tidak dapat ditukarkan.</li>
										</ul>
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									<!-- ## FAQ 5 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>3.</span> Tiket (asli) Saya Hilang! Masih Tetep Bisa Mengikuti Tidak?</a>
										</h4>
										<div class="toggle-content">Saat tiket asli telah berada di tangan pembeli, maka tiket tersebut ada dalam tanggung jawab penuh sang pembeli. Bila hilang Anda terpaksa harus membeli tiket lagi untuk dapat mengikuti  event ini. Jadi, jaga tiket tersebut baik-baik.
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									<!-- ## FAQ 4 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>4.</span> Mengapa saya belum mendapat email konfirmasi balik saat membeli tiket online?</a>
										</h4>
										<div class="toggle-content">Proses validasi pembayaran dan pengiriman konfirmasi email terkadang memakan waktu. Terlebih untuk event yang sangat menarik seperti ini. Jadi silahkan ditunggu. Dan jangan lupa untuk memeriksa kotak Spam/Junk di akun email anda, siapa tahu email dari kami ternyata ada di situ.
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									<!-- ## FAQ 5 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>5.</span> Pembayaran Belum Dikonfirmasi?</a>
										</h4>
										<div class="toggle-content">Setiap pembayaran yang dilakukan melalui transfer antar bank akan memakan waktu minimal 2 hari kerja. Bila ada transfer yang dilakukan pada hari libur nasional yang cukup panjang (misalnya Lebaran, Natal, Tahun Baru, dll.) atau pada akhir minggu, maka proses konfirmasi akan memakan waktu lebih lama lagi. 
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									<!-- ## FAQ 4 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>6.</span> Saya Sudah Transfer, Tapi Kenapa Ditolak?</a>
										</h4>
										<div class="toggle-content">Ada satu hal penting yang membuat pembayaran Anda kami tolak: Anda belum melakukan pemesanan! Anda tidak dapat melakukan transfer sejumlah uang untuk pembelian tiket tanpa memesannya terlebih dahulu.
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
								</div>
								<!-- ## FAQ's END ## -->

							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 2 END ## -->



				
				<!-- ===============================================

					PAGE 3 - PRICE

				=============================================== -->
				<div id="tab-2" class="qcTabPage clearfix">

				
				<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>Pricelist<span> Ticket Pricelist</span></h4>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<p class="qcPageDesc full">Ayurveda merupakan bagian dari Yoga, Yoga merupakan bagian dari Ayurveda. Dilansir dari <a href="http://www.artofliving.org" target="_blank">www.artofliving.org</a> , menurut guru besar Ayurveda, Ayurveda merupakan ilmu kehidupan, dan Yoga merupakan salah satu penerapan ilmunya.</p>
							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">
								<!-- ## MODULE TITLE ## -->
								<div class="qcModTitle">
									<h1>Pricing Table</h1>
									<p>Ticket packages & Features</p>
								</div>
								<!-- ## PRICING ## -->
								<div class="qcPricingWrapper clearfix">
									<!-- ## PRICING 1 ## -->
									<div class="qcPricing col-4 col">
										<div class="box">
											<header>Jakarta</header>
											<div class="price"><span>Rp.200.000,-</span> Per Entry</div>
											<ul>
												<li>Kaos</li>
												<li>Yoga Bag</li>
												<li>Potongan Diskon untuk Produk Ayurveda</li>
											</ul>
											<footer><a href="#">Buy Now</a></footer>
										</div>
									</div>
									<!-- ## PRICING 2 ## -->
									<div class="qcPricing col-4 col">
										<div class="box">
											<header>Surabaya</header>
											<div class="price"><span>Rp.200.000,-</span> Per Entry</div>
											<ul>
												<li>Kaos</li>
												<li>Yoga Bag</li>
												<li>Potongan Diskon untuk Produk Ayurveda</li>
											</ul>
											<footer><a href="#">Buy Now</a></footer>
										</div>
									</div>
									<!-- ## PRICING 3 ## -->
									<div class="qcPricing col-4 col">
										<div class="box">
											<header>Bali</header>
											<div class="price"><span>Rp.200.000,-</span> Per Entry</div>
											<ul>
												<li>Kaos</li>
												<li>Yoga Bag</li>
												<li>Potongan Diskon untuk Produk Ayurveda</li>
											</ul>
											<footer><a href="#">Buy Now</a></footer>
										</div>
									</div>
								</div>
								<!-- ## PRICING ## -->
							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->


				</div>
				<!-- ## PAGE 3 END ## -->



				<!-- ===============================================

					PAGE 4 - CONTACT

				=============================================== -->
				<div id="tab-4" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-6 col" >
							<div class="qcTabTitle no-border">
								<h4>Contact<span> K-LINK TOWER</span></h4>
								<p>Monday - Friday : 10:00AM until 18:00PM</p>
								<p>Saturday : 10:00AM until 14:00PM</p>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-6 col">
							<ul class="qcAddress">
								<li><i class="icon-map"></i><p><strong>ADDRESS</strong>: JL.Gatot Subroto Kav. 59 A, Jakarta Selatan 12950 - Indonesia</p></li>
								<li><i class="icon-user-1"></i><p><strong>PHONE</strong>:  021.290.27.000</p></li>
								<li><i class="icon-print"></i><p><strong>FAX</strong>: 021.290.27.001 - 290.27.004</p></li>
								<li><i class="icon-mail-1"></i><p><strong>EMAIL</strong>: customer_service@k-link.co.id</p></li>
								<li><i class="icon-globe-1"></i><p><strong>WEBSITE</strong>: <a title="website" href="http://www.k-link.co.id/" target="_blank">www.k-link.co.id</a></p></li>
							</ul>
						</div>

					</div>
					<!-- ## ROW END ## -->					


				</div>
				<!-- ## PAGE 4 END ## -->



			</div>
			<!-- ## TABS END ## -->

		</div>
	</section>
	<!-- ## PAGE CONTENT END ## -->


</div>
<!-- ## CONTENT WRAPPER END ## -->


<!-- ## FOOTER ## -->
<footer id="qcFooter" class="clearfix">
	<div class="qcContainer">

		<!-- ## FOOTER NAV ## -->
		<div class="col-5 col">
			<nav id="qcFooterNav">
				<ul class="clearfix">
					<li><a href="#">Home</a></li>
					<li><a href="#">About Event</a></li>
					<li><a href="#">Buy Ticket</a></li>
				</ul>
			</nav>
		</div>

		
		<!-- ## FOOTER LOGO ## -->
		<div id="qcFooterLogo" class="col-2 col">
			<a href="#">
				<img src="<?php echo base_url().'ticket/images/logo-footer.png';?>" alt="LOGO" />
			</a>
		</div>

		<!-- ## FOOTER COPYRIGHTS ## -->
		<div id="qcFooterPara" class="col-5 col">
			<p>K-Ayurveda Tour Yoga 2015 <br /> <span>&copy;Copyrights 2015</span></p>
		</div>

	</div>
</footer>
<!-- ## FOOTER END ## -->
</body>
</html>

<script type="text/javascript">
   /* window.onload = function() {
        var data = {
                key : "<?php echo $key;?>",
                paymentId : "<?php echo $bookingNo;?>",
                paymentAmount : "<?php echo $tot_dp;?>",
                backUrl : "http://www.k-net.co.id/ticketing/finish/payment/<?php echo $bookingNo;?>"
            },
            sgoPlusIframe = document.getElementById("sgoplus-iframe");
        
        if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
        SGOSignature.receiveForm();
    };*/
    
function submitdata(){
    var bankCode = "008";
	var productCode = "MANDIRIIB";
    if ((bankCode || productCode) === 'undefined'){
		alert("Please Select Payment Method");
		
	}else{
		
	 	 var data = {
						key : "<?php echo $key;?>",
						paymentId : "<?php echo $bookingNo;?>",
						paymentAmount : "<?php echo $tot_dp;?>",
						backUrl : "http://www.k-net.co.id/ticketing/finish/payment/<?php echo $bookingNo;?>",
						bankCode : bankCode,
						bankProduct: productCode
				    },
			sgoPlusIframe = document.getElementById("sgoplus-iframe");
					
			if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
			SGOSignature.receiveForm();
					
		}
}

</script>


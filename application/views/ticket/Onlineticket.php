<!doctype html>
<html>
<head>
	<!-- ## SITE META ## -->
	<meta charset="utf-8">
	<title>Yoga Ayurveda Tour 2015 </title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url().'ticket/images/favicon.ico';?>">

	<!-- ## LOAD STYLSHEETS ## -->
	<link rel="stylesheet" media="all" href="<?php echo base_url().'ticket/css/style.css';?>"/>

	<!-- ## GOOGLE FONTS ## -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>

	<!-- ## FULLSCREEN SLIDESHOW ## -->
	<script type="text/javascript">
	function slide_fullscreen() {
	jQuery(function($){
		$.supersized({
			// Functionality
			slide_interval          :   8000,
			transition              :   1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
			transition_speed	:   1000,
			// Components
			slide_links		:   'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
			progress_bar	:	1,
			slides 			:   [
								{	image : '<?php echo base_url().'ticket/images/slide-1.jpg';?>'
								},
								{	image : '<?php echo base_url().'ticket/images/slide-2.jpg';?>'
								}
							]
		});
	});
	}
	window.onload = slide_fullscreen;
	</script>

</head>

<body>

<!-- ## HEADER ## -->
<header id="qcHomeHeader">
	<div class="row">

		<!-- ## LOGO ## -->
		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo site_url('yoga');?>"><img src="<?php echo base_url().'ticket/images/logo-header.png';?>" alt="" /></a>
		</div>

	<!-- ## SITE NAVIGATION ## -->
		<nav id="qcPriNav" class="col-6 col">
			<ul class="clearfix">
				<li><a href="<?php echo site_url('event');?>"><i class="icon-calendar-2 icon"></i> <span>Event Details</span></a></li>
				<li><a href="<?php echo site_url('ticketing');?>"><i class="icon-ticket icon"></i> <span>Buy Tickets</span></a></li>
			</ul>
		</nav>

	</div>
	<div class="row">

		<!-- ## COUNTDOWN TIMER ## -->
		<div id="qcEventCountDown" class="col-6 col">
			<!-- ## DAYS ## -->
			<div class="dash days_dash">
				<div class="dash_title">days</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
					<div class="digit digit-3">0</div>
				</div>
			</div>
			<!-- ## HOURS ## -->
			<div class="dash hours_dash">
				<div class="dash_title">hours</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
				</div>
			</div>
			<!-- ## MINUTES ## -->
			<div class="dash minutes_dash">
				<div class="dash_title">minutes</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
				</div>
			</div>
			<!-- ## SECONDS ## -->
			<div class="dash seconds_dash">
				<div class="dash_title">seconds</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
				</div>
			</div>
		</div>

		<!-- ## EVENT BANNER ## -->
		<div id="qcEventBanner" class="col-6 col">
			<ul>
				<li><b>#BALI.</b> 25 September 2015 - Bhumiku Convention Center</li>
				<li><b>#JAKARTA.</b> 26 September 2015 - Ballroom K-Link Tower</li>				
				<li><b>#SURABAYA.</b> 27 September 2015 - Graha Barunawati </li>
			</ul>
		</div>

	</div>
</header>
<!-- ## HEADER END ## -->

<!-- ## FULLSCREEN SLIDES ## -->
<section id="slideContent">

	<!-- ## SLIDE CONTROLS ## -->
	<div id="qcHomeSlideControls">

		<!-- ## SLIDE DOT NAV ## -->
		<ul id="slide-list"></ul>

		<!-- ## PROGRESS BAR ## -->
		<div id="progress-back" class="load-item">
			<div id="progress-bar"></div>
		</div>

	</div>


</section>
<!-- ## FULLSCREEN SLIDES ## -->

<!-- ## BACKGROUND OVERLAY ## -->
<section id="qcOverlay">
	<div id="qcEventLogo" class="loading">
		<a href="#">
			<img src="<?php echo base_url().'ticket/images/center-logo.png';?>" alt="" />
		</a>
	</div>
</section>

<!-- ## LOAD JAVASCRIPTS ## -->
<script src="<?php echo base_url().'ticket/js/2.1.1.jquery.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/supersized.3.2.7.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/countdown.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/jquery.marquee.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/library.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/init.js';?>" type="text/javascript"></script>

</body>

</html>
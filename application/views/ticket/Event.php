

<!-- ## HEADER ## -->
<header id="qcHeader">
	<div class="row">

		<!-- ## LOGO ## -->
		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo site_url('yoga');?>"><img src="<?php echo base_url().'ticket/images/logo-header.png';?>" alt="" /></a>
		</div>

		<!-- ## SITE NAVIGATION ## -->
		<nav id="qcPriNav" class="col-6 col">
			<ul class="clearfix">
				<li><a href="<?php echo site_url('event');?>"><i class="icon-calendar-2 icon"></i> <span>Event Details</span></a></li>
				<li><a href="<?php echo site_url('ticketing');?>"><i class="icon-ticket icon"></i> <span>Buy Tickets</span></a></li>
			</ul>
		</nav>

	</div>
</header>
<!-- ## HEADER END ## -->


<!-- ## CONTENT WRAPPER ## -->
<div id="qcContentWrapper">

	<!-- ## PAGE TITLE ## -->
	<section id="qcSecbar">
		<div class="qcContainer">
			<h1>Event. <span>K-Ayurveda Tour Yoga 2015</span></h1>
		</div>
	</section>

	<!-- ## PAGE CONTENT ## -->
	<section id="qcContent">
		<div class="qcContainer">

			<!-- ## MOB NAV ## -->
			<div id="qcMbTrigger"></div>

			<!-- ## TABS ## -->
			<div id="qcTabs" class="tabs">

				<!-- ## TAB NAV ## -->
				<ul id="qcTabNav" class="clearfix">
					<li><a href="#tab-1"><i class="icon-book-open icon"></i> <span>About</span></a></li>
					<li><a href="#tab-2"><i class="icon-clock-1 icon"></i> <span>Schedule</span></a></li>
					<li><a href="#tab-3"><i class="icon-user-add icon"></i> <span>Speakers</span></a></li>
					<li><a href="#tab-4"><i class="icon-map icon"></i> <span>Venue</span></a></li>
					<li><a href="#tab-5"><i class="icon-picture icon"></i> <span>Gallery</span></a></li>
				</ul>


				<!-- ===============================================

					PAGE 1 - ABOUT

				=============================================== -->
				<div id="tab-1" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="stretch row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-4 col">
							<div class="qcTabTitle">
								<h4>About<span> Tentang K-Ayurveda Tour Yoga 2015</span></h4>
							</div>
							<p class="qcPageDesc">K-Ayurveda Tour Yoga 2015 merupakan sebuah acara yoga yang akan menghadirkan langsung instruktur yoga dari negara India yaitu Anupa Patel yang merupakan master yoga dan pengobatan alternatif di India yang disebut ayurveda. Berbeda dengan acara yoga lainnya, selain melakukan yoga bersama, acara Yoga Ayurveda Tour juga akan menjelaskan kepada peserta mengenai sejarah yoga, penjelasan hubungan ayurveda dengan yoga yang dijelaskan dr Roopam dari Welex Laboratories.</p>
						</div>

						<!-- ## SLIDER ## -->
						<div class="col-8 col">
							<div class="video box no-border">
								<div class="qcSliderWrapper">
									<ul class="single-carousel owl-carousel">
										<li>
											<!-- ## IMAGE SLIDE ## -->
											<img src="<?php echo base_url().'ticket/images/about-1.jpg';?>" alt="" />
										</li>
										<li>
											<!-- ## IMAGE SLIDE ## -->
											<img src="<?php echo base_url().'ticket/images/about-2.jpg';?>" alt="" />
										</li>
										<li>
											<!-- ## IMAGE SLIDE ## -->
											<img src="<?php echo base_url().'ticket/images/about-3.jpg';?>" alt="" />
										</li>
										<li>
											<!-- ## IMAGE SLIDE ## -->
											<img src="<?php echo base_url().'ticket/images/about-4.jpg';?>" alt="" />
										</li>
									</ul>

									<!-- ## SLIDER NEXT PREV ## -->
									<div class="qcPrevNext">
										<div class="qcPrev"><i class="icon-left-open-big"></i></div>
										<div class="qcNext"><i class="icon-right-open-big"></i></div>
									</div>
								</div>
								<!-- ## SLIDER END ## -->

							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->					

				</div>
				<!-- ## PAGE 1 END ## -->



				<!-- ===============================================

					PAGE 2 - SCHEDULE

				=============================================== -->
				<div id="tab-2" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-4 col">
							<div class="qcTabTitle no-border">
								<h4>Schedule<span> Rundown K-Ayurveda Tour Yoga 2015</span></h4>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<p class="qcPageDesc full">Menurut guru besar Ayurveda, Ayurveda merupakan ilmu kehidupan, dan Yoga merupakan salah satu penerapan ilmunya. Secara bersama-sama yoga dan Ayurveda akan menciptakan keseimbangan anatar tubuh dan pikiran sehingga tercapai kesehatan tubuh. Ayurveda akan membatu keseimbangan jasmani dan yoga akan menciptakan keseimbangan rohani/spiritual.</p>
							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->


					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">

								<!-- ## SCHEDULE LIST ## -->
								<div id="qcScheduleWrapper">

									<!-- ## DAY 1 ## -->
									<div class="qcScheduleDay">
										<header class="qcSchDay">
											<p>Rundown</p>
										</header>
										<ul class="qcScheduleList clearfix">
											<li>
												<a href="#" class="tips" title="Yoga Expert">
													<img src="<?php echo base_url().'ticket/images/gallery/ultraba14a_129.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchSpeaker">Yoga Expert</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Yoga Exercise">
													<img src="<?php echo base_url().'ticket/images/gallery//ultrasa14b_072';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchSpeaker">Yoga Exercise</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Acrobatic Shows">
													<img src="<?php echo base_url().'ticket/images/gallery/ultrasa14b_155.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchSpeaker">Acrobatic Shows</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Live Music">
													<img src="<?php echo base_url().'ticket/images/gallery/ultraba14a_023.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchSpeaker">Live Music</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Games">
													<img src="<?php echo base_url().'ticket/images/gallery/ultraba14a_150.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchSpeaker">Games</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Lucky Draw">
													<img src="<?php echo base_url().'ticket/images/gallery/ultrasa14b_138.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchSpeaker">Lucky Draw</p>
													</div>
												</a>
											</li>
												
										</ul>
									</div>
									<!-- ## DAY 1 END ## -->
									

								
								</div>
								<!-- ## SCHEDULE LIST END ## -->

							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 2 END ## -->



				<!-- ===============================================

					PAGE 3 - SPEAKERS

				=============================================== -->
				<div id="tab-3" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="speakers row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-4 col">
							<div class="qcTabTitle">
								<h4>Speakers<span> Pembicara Yoga Aryuveda Tour 2015</span></h4>
							</div>
							<p class="qcPageDesc">K-Ayurveda Tour Yoga 2015 merupakan sebuah acara yoga yang akan menghadirkan langsung instruktur yoga dari negara India yaitu Anupa Patel yang merupakan master yoga dan pengobatan alternatif di India yang disebut ayurveda. Serta dihadiri praktisi yoga dari indonesia yang juga merupakan seorang publik figure yaitu Anjasmara. Berbeda dengan acara yoga lainnya, selain melakukan yoga bersama, acara Yoga Ayurveda Tour juga akan menjelaskan kepada peserta mengenai sejarah yoga, penjelasan hubungan ayurveda dengan yoga yang dijelaskan dr Roopam dari Welex Laboratories.</p>
						</div>
							<!-- ## SPEAKERS ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">

								<!-- ## SPEAKERS LIST ## -->
								<ul class="3-col-carousel owl-carousel qcTeamCol row">									
									<!-- ## SPEAKER 1 ## -->									
									<li>
										<div class="small-box">
											<img src="<?php echo base_url().'ticket/images/t3.png';?>" alt="Team-2" title="Event Speaker"/>
											<h4>Anupa Patel</h4>
											<p>Yoga Trainer</p>
											
										</div>
									</li>
									<!-- ## SPEAKER 2 ## -->
									<li>
										<div class="small-box">
											<img src="<?php echo base_url().'ticket/images/t1.png';?>" alt="Team-1" title="Event Speaker"/>
											<h4>Anjasamara</h4>
											<p> Public Figure &amp; Praktisi Yoga</p>
										</div>
									</li>
									<!-- ## SPEAKER 3 ## -->
									<li>
										<div class="small-box">
											<img src="<?php echo base_url().'ticket/images/t2.png';?>" alt="Team-1" title="Event Speaker"/>
											<h4>dr. Roopam</h4>
											<p>Speaker From Welex Laboratories</p>
										</div>
									</li>									
								</ul>
								<!-- ## SPEAKERS LIST END ## -->

							</div>
						</div>
						<!-- ## SPEAKERS END ## -->

					</div>
					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 3 END ## -->



				<!-- ===============================================

					PAGE 4 - VENUE

				=============================================== -->
				<div id="tab-4" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-4 col">
							<div class="qcTabTitle no-border">
								<h4>Venue <span> Venue of the event</span></h4>
							</div>
						</div>

						<!-- ## ADDRESS LIST ## -->
						<div class="col-8 col">

								<!-- ## SCHEDULE LIST ## -->
								<div id="qcScheduleWrapper">

									<!-- ## DAY 1 ## -->
									<div class="qcScheduleDay">
										<header class="qcSchDay">
											<p>Venue</p>
										</header>
										<ul class="qcScheduleList clearfix">
											<li>
												<a href="#" class="tips" title="Bali">
													<img src="<?php echo base_url().'ticket/images/bali.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchTime">Bali</sup></p>
														<p class="qcSchSpeaker">Bhumiku Convention Center</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Jakarta">
													<img src="<?php echo base_url().'ticket/images/jakarta.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchTime">Jakarta</sup></p>
														<p class="qcSchSpeaker">Ballroom K-Link Tower</p>
													</div>
												</a>
											</li>
											<li>
												<a href="#" class="tips" title="Surabaya">
													<img src="<?php echo base_url().'ticket/images/surabaya.jpg';?>" alt="" />
													<div class="qcSchDesc">
														<p class="qcSchTime">Surabaya</sup></p>
														<p class="qcSchSpeaker">Graha Barunawati</p>
													</div>
												</a>
											</li>
																						
										</ul>
									</div>
									<!-- ## DAY 1 END ## -->
									

								</div>
								<!-- ## SCHEDULE LIST END ## -->

							</div>

					</div>
					<!-- ## ROW END ## -->
					

				</div>
				<!-- ## PAGE 4 END ## -->



				<!-- ===============================================

					PAGE 5 - GALLERY

				=============================================== -->
				<div id="tab-5" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="stretch row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-4 col">
							<div class="qcTabTitle">
								<h4>Gallery<span> Yoga Exercise</span></h4>
							</div>
							<p class="qcPageDesc">Latihan yoga sangat membantu dalam mengatasi permasalahan kesehatan. gerakan yang tidak biasa dilakukan akan melibatkan lebih banyak sistem saraf dan meningkatkan organisasi musculo-skeletal, sehingga meningkatkan fleksibilitas, kekuatan dan koordinasi. </p>
						</div>

						<!-- ## GALLERY LIST ## -->
						<div class="col-8 col">
							<div class="video box no-border">
								<ul id="imgGallery" class="clearfix">
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/image-1.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/image-1.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/image-6.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/image-6.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/ultraba14a_008.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/ultraba14a_008.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/image-3.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/image-3.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/image-4.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/image-4.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/ultraba14a_150.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/ultraba14a_150.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/image-5.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/image-5.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/image-2.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/image-2.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>									
									<li>
										<a href="<?php echo base_url().'ticket/images/gallery/ultraba14a_023.jpg';?>" class="tips" title="K-Ayurveda Tour Yoga 2015" data-rel="prettyPhoto[pp_gal]">
											<img src="<?php echo base_url().'ticket/images/gallery/ultraba14a_023.jpg';?>" alt="K-Ayurveda Tour Yoga 2015"/>
										</a>
									</li>
									<
								</ul>
							</div>
						</div>
						<!-- ## GALLERY LIST END ## -->

					</div>
					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 5 END ## -->


			</div>
			<!-- ## TABS END ## -->

		</div>
	</section>
	<!-- ## PAGE CONTENT END ## -->


</div>
<!-- ## CONTENT WRAPPER END ## -->


<!-- ## FOOTER ## -->
<footer id="qcFooter" class="clearfix">
	<div class="qcContainer">

		<!-- ## FOOTER NAV ## -->
		<div class="col-5 col">
			<nav id="qcFooterNav">
				<ul class="clearfix">
					<li><a href="#">Home</a></li>
					<li><a href="#">About Event</a></li>
					<li><a href="#">Buy Ticket</a></li>
				</ul>
			</nav>
		</div>

		
		<!-- ## FOOTER LOGO ## -->
		<div id="qcFooterLogo" class="col-2 col">
			<a href="#">
				<img src="<?php echo base_url().'ticket/images/logo-footer.png';?>" alt="LOGO" />
			</a>
		</div>

		<!-- ## FOOTER COPYRIGHTS ## -->
		<div id="qcFooterPara" class="col-5 col">
			<p>K-Ayurveda Tour Yoga 2015 <br /> <span>&copy;Copyrights 2015</span></p>
		</div>

	</div>
</footer>
<!-- ## FOOTER END ## -->

<!-- ## LOAD JAVASCRIPTS ## -->
<script src="<?php echo base_url().'ticket/js/2.1.1.jquery.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/owl.carousel.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/library.js';?>" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;ver=3.5" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/init.js';?>" type="text/javascript"></script>


</body>

</html>
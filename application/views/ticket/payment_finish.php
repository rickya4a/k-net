<style>
.qcTcktRegForm li .wrapper {
  padding:20px 20px 10px 0;
}
.qcTcktRegForm li {
    min-height: 10px;
}

.table-bordered {
  border:1px solid #DDDDDD;
}
.table {
    margin-top: 10px;
  margin-bottom:20px;
  max-width:100%;
  width:100%;
  border-spacing:0;
}

.table > thead > tr > th {
  border-bottom-color:#DDDDDD;
  border-bottom-style:solid;
  border-bottom-width:2px;
  vertical-align:bottom;
}

th {
  text-align:left;
}

.table-bordered > thead {
  background:#DDDDDD;
  border:2px solid #B8B8B8;
  text-align:center;
  font-weight: bold;
}

.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
  border:1px solid #DDDDDD;
}
</style>
<body>
<!-- ## HEADER ## -->
<header id="qcHeader">
	<div class="row">

		<!-- ## LOGO ## -->
		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo site_url('yoga');?>"><img src="<?php echo base_url().'ticket/images/logo-header.png';?>" alt="" /></a>
		</div>

		<!-- ## SITE NAVIGATION ## -->
		<nav id="qcPriNav" class="col-6 col">
			<ul class="clearfix">
				<li><a href="<?php echo site_url('event');?>"><i class="icon-calendar-2 icon"></i> <span>Event Details</span></a></li>
				<li><a href="<?php echo site_url('ticketing');?>"><i class="icon-ticket icon"></i> <span>Buy Tickets</span></a></li>
			</ul>
		</nav>

	</div>
</header>
<!-- ## HEADER END ## -->


<!-- ## CONTENT WRAPPER ## -->
<div id="qcContentWrapper">

	<!-- ## PAGE TITLE ## -->
	<section id="qcSecbar">
		<div class="qcContainer">
			<h1>Ticket. <span>K-Ayurveda Tour Yoga 2015</span></h1>
		</div>
	</section>

	<!-- ## PAGE CONTENT ## -->
	<section id="qcContent">
		<div class="qcContainer">

			<!-- ## MOB NAV ## -->
			<div id="qcMbTrigger"></div>

			<!-- ## TABS ## -->
			<div id="qcTabs" class="tabs">

				<!-- ## TAB NAV ## -->
				<ul id="qcTabNav" class="clearfix">
					<li><a href="<?php echo site_url('ticketing');?>"><i class="icon-ticket icon"></i> <span>Success</span></a></li>
				</ul>


				<!-- ===============================================

					PAGE 1 - TICKET

				=============================================== -->
				<div id="tab-1" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="ticket row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>Perhatian</h4>								
								<ol>
									<li style="margin-bottom:20px;"><strong>Pengambilan Hampers
										Dilakukan 1 minggu sebelum acara di masing-masing kota di laksanakan : </strong>
										<ul style="padding-left:15px;">
											<li>Bali 17 September 2015 s/d 24 September 2015</li>
											<li>Jakarta 18 September 2015 s/d 25 September 2015</li>
											<li>Surabaya 19 September 2015 s/d 26 September 2015</li>
										</ul>
									</li>
									<li><strong>Persyaratan pengambilan Hampers : </strong>
										<ul style="padding-left:15px;">
											<li>Harus menyertai Bukti pembayaran tiket saat pengambilan hampers.</li>
											<li>Pengambil hampers adalah orang yang datanya terdaftar dalam registrasi online, apabila orang yang bersagkutan berhalangan, bisa digantikan dengan orang lain, selain membawa bukti registrasi online juga harus dilengkapi surat kuasa dan KTP asli peserta yang terdaftar di registrasi online.</li>
										</ul>
									</li>
								
							</div>
						</div>

												<!-- ## TICKET ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<div class="qcTcktRegForm">
								
									<!-- ## TICKET FORM ## -->
									<form id="finishForm" class="qcForm" name="finishForm" method="post" action="<?php echo site_url('ticketing/print');?>" target="_blank">
										<!-- ## MODULE TITLE ## -->
										<div class="qcModTitle">
											<h1>RESULT </h1>
										</div>
										<?php
                                            foreach($dtReg as $row){
                                                $bookingNo = $row->bookingNo;
                                                $telhp = $row->trans_hdr_hp;
                                                $nmmember = $row->trans_hdr_name;
                                                $email = $row->trans_hdr_email;
                                                $eventPlace = $row->event_address;
                                                $eventDesc = $row->event_desc;
                                                $eventDate = $row->event_date;
                                                $qtyTiket = $row->trans_qty_ticket;
                                                $totDp = $row->trans_hdr_dp;
                                                $paytipe = $row->pay_type;
                                            }
                                        ?>
									   <div class="table-two-cols txt-uppercase">
                                            <div class="table-row">
												<div class="table-col">
													<label for="name">Booking Number</label>
												</div>
												<div class="table-col">
													<label for="name"><?php echo $bookingNo; ?></label>
												</div>
											</div>
                                            
                                            <div class="table-row">
												<div class="table-col">
													<label for="name">Nama</label>
												</div>
												<div class="table-col">
													<label for="name"><?php echo $nmmember; ?></label>
												</div>
											</div>
                                            
                                            <div class="table-row">
												<div class="table-col">
													<label for="jmltkt">Jumlah Tiket</label>
												</div>
												<div class="table-col">
													<label for="jmltkt"><?php echo $qtyTiket; ?></label>
												</div>
											</div>
											
											<div class="table-row">
												<div class="table-col">
													<label for="hape">No Telepon</label>
												</div>
                                                <div class="table-col">
													<label for="hape"><?php echo $telhp;?> </label>
												</div>
											</div>
                                            
											<div class="table-row">
												<div class="table-col">
													<label for="email">Email</label>
												</div>
												<div class="table-col">
													<label for="email"><?php echo $email;?></label>
												</div>
											</div>
                                            <div class="table-row">
												<div class="table-col">
													<label for="place">Tempat Acara</label>
												</div>
												<div class="table-col">
													<label for="place"><?php echo $eventDesc ." / ".$eventPlace;?></label>
												</div>
											</div>
                                            
                                            <div class="table-row">
												<div class="table-col">
													<label for="place">Tanggal Acara</label>
												</div>
												<div class="table-col">
													<label for="place"><?php echo date('d/m/Y',strtotime($eventDate));?></label>
												</div>
											</div>
                                            <div class="table-row">
												<div class="table-col">
													<label for="place">Total Pembayaran</label>
												</div>
												<div class="table-col">
													<label for="place"><?php echo "Rp. ".number_format($totDp,0,".",".");?></label>
												</div>
											</div>				
										</div>
									
                                        <table border="1" class="table table-bordered table-striped" width="100%">
                                        	<thead>
                                                <tr>
                                                    <td colspan="4"> Detail Ticket</td>
                                                </tr>
                                            </thead>
                                        		<tr>
                                                    <td>No</td>
                                        			<td>Tiket No</td>
                                        			<td>Name</td>
                                        			<td>Contact No</td>
                                        			
                                        		</tr>
                                        	
                                        	<tbody>
                                            <?php
                                                $no = 1;
                                                foreach($det as $list){
                                            ?>
                                        		<tr>
                                                    <td><?php echo $no;?></td>
                                        			<td><?php echo $list->trans_tiket_no; ?></td>
                                        			<td><?php echo $list->trans_det_name; ?></td>
                                                    <td><?php echo $list->trans_det_hp; ?></td>
                                        		</tr>
                                          <?php $no++;}?>
                                        	</tbody>
                                        </table>
                                        <p>Catatan : Untuk Pembayaran melalui transfer bank, silahkan melakukan transfer ke  nomor rekening BCA KCP RICCI 6440628395 a/n K-LINK NUSANTARA sesuai dengan nominal diatas agar pembayaran Anda terkonfirmasi secara otomatis by system</p>
                                        <input type="hidden" name="bookno" id="bookno" value="<?php echo $bookingNo;?>"/>
                                        <input type="hidden" name="paytipe" id="paytipe" value="<?php echo $paytipe;?>"/>
                                        <div class="qcTcktSubmit">
                                        	<button type="submit" class="submit" >Print To PDF</button>
                                        </div>
									</form>
									<div id="resTEs"></div>
								</div>
								<!-- ## TICKET FORM END ## -->
							</div>
						</div>
						<!-- ## TICKET END ## -->

					</div>
					<!-- ## ROW END ## -->
					

				</div>
				<!-- ## PAGE 1 END ## -->			



			</div>
			<!-- ## TABS END ## -->

		</div>
	</section>
	<!-- ## PAGE CONTENT END ## -->


</div>
<!-- ## CONTENT WRAPPER END ## -->


<!-- ## FOOTER ## -->
<footer id="qcFooter" class="clearfix">
	<div class="qcContainer">

		<!-- ## FOOTER NAV ## -->
		<div class="col-5 col">
			<nav id="qcFooterNav">
				<ul class="clearfix">
					<li><a href="#">Home</a></li>
					<li><a href="#">About Event</a></li>
					<li><a href="#">Buy Ticket</a></li>
				</ul>
			</nav>
		</div>

		
		<!-- ## FOOTER LOGO ## -->
		<div id="qcFooterLogo" class="col-2 col">
			<a href="#">
				<img src="<?php echo base_url().'ticket/images/logo-footer.png';?>" alt="LOGO" />
			</a>
		</div>

		<!-- ## FOOTER COPYRIGHTS ## -->
		<div id="qcFooterPara" class="col-5 col">
			<p>K-Ayurveda Tour Yoga 2015 <br /> <span>&copy;Copyrights 2015</span></p>
		</div>

	</div>
</footer>
<!-- ## FOOTER END ## -->
</body>
</html>
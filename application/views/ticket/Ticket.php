
<body >

<!-- ## HEADER ## -->
<header id="qcHeader">
	<div class="row">

		<!-- ## LOGO ## -->
		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo site_url('yoga');?>"><img src="<?php echo base_url().'ticket/images/logo-header.png';?>" alt="" /></a>
		</div>

		<!-- ## SITE NAVIGATION ## -->
		<nav id="qcPriNav" class="col-6 col">
			<ul class="clearfix">
				<li><a href="<?php echo site_url('event');?>"><i class="icon-calendar-2 icon"></i> <span>Event Details</span></a></li>
				<li><a href="<?php echo site_url('ticketing');?>"><i class="icon-ticket icon"></i> <span>Buy Tickets</span></a></li>
			</ul>
		</nav>

	</div>
</header>
<!-- ## HEADER END ## -->


<!-- ## CONTENT WRAPPER ## -->
<div id="qcContentWrapper">

	<!-- ## PAGE TITLE ## -->
	<section id="qcSecbar">
		<div class="qcContainer">
			<h1>Ticket. <span>K-Ayurveda Yoga Tour 2015</span></h1>
		</div>
	</section>

	<!-- ## PAGE CONTENT ## -->
	<section id="qcContent">
		<div class="qcContainer">

			<!-- ## MOB NAV ## -->
			<div id="qcMbTrigger"></div>

			<!-- ## TABS ## -->
			<div id="qcTabs" class="tabs">

				<!-- ## TAB NAV ## -->
				<ul id="qcTabNav" class="clearfix">
					<li><a href="#tab-1"><i class="icon-ticket icon"></i> <span>Book Ticket</span></a></li>
					<li><a href="#tab-2"><i class="icon-lkdto icon"></i> <span>Price</span></a></li>
					<li><a href="#tab-3"><i class="icon-book-open icon"></i> <span>FAQ's</span></a></li>
					<li><a href="#tab-4"><i class="icon-mail-1 icon"></i> <span>Contact</span></a></li>
				</ul>


				<!-- ===============================================

					PAGE 1 - TICKET

				=============================================== -->
				<div id="tab-1" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="ticket row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>Buy ticket</h4>
								<p class="qcPageDesc full">Pendaftaran K-Ayurveda Yoga Tour 2015 bisa dilakukan melalui online atau offline dengan cara transfer ke rekening BCA KCP RICCI 6440628395 a/n K-LINK NUSANTARA
                                                         dan tempatnya sangat terbatas, pastikan data yang anda masukan benar</p>
								<ol>
									<li style="margin-bottom:20px;"><strong>Pengambilan Hampers
										Dilakukan 2 jam sebelum acara di masing-masing kota di laksanakan : </strong>
										<ul style="padding-left:15px;">
											<li>Bali 25 September 2015</li>
											<li>Jakarta 26 September 2015</li>
											<li>Surabaya 27 September 2015</li>
										</ul>
									</li>
									<li><strong>Persyaratan pengambilan Hampers : </strong>
										<ul style="padding-left:15px;">
											<li>SMS bukti transaksi</li>
											<li>Harus menyertai Bukti/ konfirmasi telah melakukan registrasi online saat pengambilan hampers.</li>
											<li>Pengambil hampers adalah orang yang datanya terdaftar dalam registrasi online, apabila orang yang bersagkutan berhalangan, bisa digantikan dengan orang lain, selain membawa bukti registrasi online juga harus dilengkapi surat kuasa dan KTP asli peserta yang terdaftar di registrasi online.</li>
										</ul>
									</li>
								</0l>
							</div>
						</div>

						<!-- ## TICKET ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<div class="qcTcktRegForm">
								
									<!-- ## TICKET FORM ## -->
									<form id="qcTcktForm" class="qcForm" name="qcTcktForm" method="post" action="<?php echo site_url('ticketing/preview');?>" >
										<!-- ## MODULE TITLE ## -->
										<div class="qcModTitle">
											<h1>R S V P</h1>
											<p>Fill out the following form.</p>
										</div>
										
										<ul class="clearfix">
											<li>
												<div class="wrapper">
													<label for="name">Nama (sesuai KTP)</label>
													<input type="text" id="nameHdr" name="nameHdr" class="requiredField" value="" placeholder="Nama Lengkap" />
												</div>
											</li>
											<li>
												<div class="wrapper">
													<label for="ktp">No. Identitas (KTP atau SIM)</label>
													<input type="text" id="ktpHdr" name="ktpHdr" class="requiredField" value="" placeholder="Nomor Identitas" />
												</div>
											</li>
											<li>
												<div class="wrapper">
													<label for="name">Alamat</label>
													<input type="text" id="alamatHdr" name="alamatHdr" class="requiredField" value="" placeholder="Alamat" />
												</div>
											</li>
											<li>
												<div class="wrapper">
													<label for="phone">Nomor Handphone</label>
													<input type="text" id="phoneHdr" name="phoneHdr" class="requiredField" value="" placeholder="Harus valid untuk pengiriman nomor tiket" />
												</div>
											</li>
											<li>
												<div class="wrapper">
													<label for="email">Email</label>
													<input type="text" id="emailHdr" name="emailHdr" class="email requiredField" value="" placeholder="Alamat Email" />
												</div>
											</li>											
											<li>
												<div class="wrapper herr">
													<label for="ticket">Ticket</label>
													<select name="ticket" id="ticket" class="requiredField">
														<option value="">Select Package</option>
														<?php
							                                foreach($event as $row){
							                                    echo "<option value = \"$row->event_id\">".$row->event_desc."</option>";    
							                                }
							                                
							                            ?>
														<!--
															<option value="early">Jakarta</option>
															<option value="general">Bali</option>
															<option value="vip">Surabaya</option>
														-->
													</select>
													<select name="quantity" id="quantity" class="requiredField" onchange="showFormData(this.value)">
														<option value="0">Quantity</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>
											</li>
										</ul>
										<div id="formDta">
											
										</div>
										<div class="qcTcktSubmit">
											<!--<input type="hidden" name="formtype" value="ticket"/>-->
											<button type="submit" class="submit" >Pesan Tiket</button>
										      
                                        </div>
										
									</form>
									<div id="resTEs"></div>
									<div class="qcTcktCheck" style="margin-top:50px;">
											<div class="qcModTitle">
												<h1>Beli Tiket Di Rajakarcis.com</h1>
											</div>
											<div class="qcTcktSubmit">											
												<a href="https://rajakarcis.com/2015/09/16/k-ayurveda-yoga-tour-2015" target="_blank"><img src="<?php echo base_url().'ticket/images/rajakarcis.jpg';?>" alt="raja karcis.com"></a>
											</div>
											<div class="qcTcktSubmit">											
												<a href="https://rajakarcis.com/2015/09/16/k-ayurveda-yoga-tour-2015" target="_blank" style=" background:#FF4D00; border:0; border-radius:2px; color:#FFFFFF; cursor:pointer; display:inline-block; font-size:13px; letter-spacing:1px; padding:14px; position:relative; text-transform:uppercase;">Go to Rajakarcis.com</a>
											</div>
										</div>
								</div>
								<!-- ## TICKET FORM END ## -->
							</div>
						</div>
						<!-- ## TICKET END ## -->

					</div>
					<!-- ## ROW END ## -->
					

				</div>
				<!-- ## PAGE 1 END ## -->



				<!-- ===============================================

					PAGE 2 - FAQ's

				=============================================== -->
				<div id="tab-3" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>FAQ's<span> Frequently asked questions</span></h4>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<p class="qcPageDesc full">Secara bersama-sama yoga dan Ayurveda akan
								menciptakan keseimbangan anatar tubuh dan pikiran sehingga tercapai kesehatan tubuh. 
								Ayurveda akan membatu keseimbangan jasmani dan yoga akan menciptakan keseimbangan rohani/spiritual.</p>
							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">

								<!-- ## FAQ's LIST ## -->
								<div class="qcEventlayout">
									<!-- ## FAQ 3 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>1.</span> Kapan Pengambilan Hampers Dilakukan?</a>
										</h4>
										<div class="toggle-content">Pengambilan Hampers Dilakukan 2 hari sebelum acara di masing-masing kota di lakanakan
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									<!-- ## FAQ 4 ## -->
									<div class="shortcode-toggle toggle-open-me closed default border">
										<h4 class="toggle-trigger">
											<a href="#"><span>2.</span> Apa Yang Harus Dibawa Saat Penukaran?</a>
										</h4>
										<div class="toggle-content">Yang wajib di bawa saat penukaran voucher dengan tiket asli adalah:
										<ul style="padding-left:20px;">
											<li>SMS Bukti Transaksi</li>
											<li>Kartu identitas asli yang datanya sesuai dengan data di voucher</li>
											<li>Bila pembayaran menggunakan kartu kredit, maka kartu kredit asli wajib dibawa dan ditunjukkan saat penukaran (untuk menghindari penyalahgunaan kartu/fraud). Bila tidak dapat menunjukkan kartu kredit asli, maaf voucher tidak dapat ditukarkan.</li>
										</ul>
										</div>
										<input type="hidden" name="title_open" value="Close Me" />
										<input type="hidden" name="title_closed" value="Open Me" />
									</div>
									
								</div>
								<!-- ## FAQ's END ## -->

							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 2 END ## -->



				
				<!-- ===============================================

					PAGE 3 - PRICE

				=============================================== -->
				<div id="tab-2" class="qcTabPage clearfix">

				
				<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-4 col" >
							<div class="qcTabTitle no-border">
								<h4>Pricelist<span> Ticket Pricelist</span></h4>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-8 col">
							<div class="box no-border nopad">
								<p class="qcPageDesc full">Ayurveda merupakan bagian dari Yoga, Yoga merupakan bagian dari Ayurveda. Dilansir dari <a href="http://www.artofliving.org" target="_blank">www.artofliving.org</a> , menurut guru besar Ayurveda, Ayurveda merupakan ilmu kehidupan, dan Yoga merupakan salah satu penerapan ilmunya.</p>
							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">
								<!-- ## MODULE TITLE ## -->
								<div class="qcModTitle">
									<h1>Pricing Table</h1>
									<p>Ticket packages & Features</p>
								</div>
								<!-- ## PRICING ## -->
								<div class="qcPricingWrapper clearfix">
									<!-- ## PRICING 1 ## -->
									<div class="qcPricing col-4 col">
										<div class="box">
											<header>Jakarta</header>
											<div class="price"><span>Rp.150.000,-</span> Per Entry</div>
											<ul>
												<li>Tumbler</li>
												<li>Tas Yoga</li>
												<li>Hand Towel</li>
												<li>Voucher diskon produk 50%</li>
												<li>Voucher free membership</li>
												<li>1 box produk Ayuderme</li>
											</ul>
											<footer><a href="#tab-1">Buy Now</a></footer>
										</div>
									</div>
									<!-- ## PRICING 2 ## -->
									<div class="qcPricing col-4 col">
										<div class="box">
											<header>Surabaya</header>
											<div class="price"><span>Rp.150.000,-</span> Per Entry</div>
											<ul>
												<li>Tumbler</li>
												<li>Tas Yoga</li>
												<li>Hand Towel</li>
												<li>Voucher diskon produk 50%</li>
												<li>Voucher free membership</li>
												<li>1 box produk Ayuderme</li>
											</ul>
											<footer><a href="#">Buy Now</a></footer>
										</div>
									</div>
									<!-- ## PRICING 3 ## -->
									<div class="qcPricing col-4 col">
										<div class="box">
											<header>Bali</header>
											<div class="price"><span>Rp.150.000,-</span> Per Entry</div>
											<ul>
												<li>Tumbler</li>
												<li>Tas Yoga</li>
												<li>Hand Towel</li>
												<li>Voucher diskon produk 50%</li>
												<li>Voucher free membership</li>
												<li>1 box produk Ayuderme</li>
											</ul>
											<footer><a href="#">Buy Now</a></footer>
										</div>
									</div>
								</div>
								<!-- ## PRICING ## -->
							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->


				</div>
				<!-- ## PAGE 3 END ## -->



				<!-- ===============================================

					PAGE 4 - CONTACT

				=============================================== -->
				<div id="tab-4" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-6 col" >
							<div class="qcTabTitle no-border">
								<h4>Contact<span> K-LINK TOWER</span></h4>
								<p>Monday - Friday : 10:00AM until 18:00PM</p>
								<p>Saturday : 10:00AM until 14:00PM</p>
							</div>
						</div>

						<!-- ## TAB DESC ## -->
						<div class="col-6 col">
							<ul class="qcAddress">
								<li><i class="icon-map"></i><p><strong>ADDRESS</strong>: JL.Gatot Subroto Kav. 59 A, Jakarta Selatan 12950 - Indonesia</p></li>
								<li><i class="icon-user-1"></i><p><strong>PHONE</strong>:  021.290.27.000</p></li>
								<li><i class="icon-print"></i><p><strong>FAX</strong>: 021.290.27.001 - 290.27.004</p></li>
								<li><i class="icon-mail-1"></i><p><strong>EMAIL</strong>: customer_service@k-link.co.id</p></li>
								<li><i class="icon-globe-1"></i><p><strong>WEBSITE</strong>: <a title="website" href="http://www.k-link.co.id/" target="_blank">www.k-link.co.id</a></p></li>
							</ul>
						</div>

					</div>
					<!-- ## ROW END ## -->					


				</div>
				<!-- ## PAGE 4 END ## -->



			</div>
			<!-- ## TABS END ## -->

		</div>
	</section>
	<!-- ## PAGE CONTENT END ## -->


</div>
<!-- ## CONTENT WRAPPER END ## -->


<!-- ## FOOTER ## -->
<footer id="qcFooter" class="clearfix">
	<div class="qcContainer">

		<!-- ## FOOTER NAV ## -->
		<div class="col-5 col">
			<nav id="qcFooterNav">
				<ul class="clearfix">
					<li><a href="#">Home</a></li>
					<li><a href="#">About Event</a></li>
					<li><a href="#">Buy Ticket</a></li>
				</ul>
			</nav>
		</div>

		
		<!-- ## FOOTER LOGO ## -->
		<div id="qcFooterLogo" class="col-2 col">
			<a href="#">
				<img src="<?php echo base_url().'ticket/images/logo-footer.png';?>" alt="LOGO" />
			</a>
		</div>

		<!-- ## FOOTER COPYRIGHTS ## -->
		<div id="qcFooterPara" class="col-5 col">
			<p>K-Ayurveda Yoga Tour 2015 <br /> <span>&copy;Copyrights 2015</span></p>
		</div>

	</div>
</footer>
<!-- ## FOOTER END ## -->
</body>

</html>

<!-- ## LOAD JAVASCRIPTS ## -->
<script src="<?php echo base_url().'ticket/js/2.1.1.jquery.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/owl.carousel.min.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/library.js';?>" type="text/javascript"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;ver=3.5" type="text/javascript"></script>-->
<script src="<?php echo base_url().'ticket/js/init.js';?>" type="text/javascript"></script>
<script src="<?php echo base_url().'ticket/js/formValidate.js';?>" type="text/javascript"></script>
<script>
  function showFormData(nilai) {
  	 var x = parseInt(nilai);
  	 
  	 var nameHdr 	= $("#nameHdr").val();
  	 var ktpHdr 	= $("#ktpHdr").val();
  	 var alamatHdr 	= $("#alamatHdr").val();
  	 var phoneHdr 	= $("#phoneHdr").val();
  	 var emailHdr 	= $("#emailHdr").val();
  	 
  	 //alert(nameHdr);
  	 
  	 if(x === 0) {
  	 	$("#formDta").html(null);
  	 } else {
  	 	$("#formDta").html(null);
  	 	var rowhtml = ""
  	 	if(nameHdr != "" && ktpHdr != "" && alamatHdr != "" && phoneHdr != ""  && emailHdr != "" ){
	  	 	for(i=1; i<=x; i++) {
	  	 		rowhtml += "<div class='qcTcktCheck'>";
				rowhtml += "<h1>DATA TICKET "+i+"</h1>";
				rowhtml += "<ul class='clearfix'>";
			    rowhtml += "<li><div class='wrapper'>";
			    rowhtml += "<label for='name"+i+"'>Nama (sesuai KTP)</label>";
			    rowhtml += "<input type='text' id=name"+i+" name='name[]' class='requiredField' placeholder='Nama Lengkap' />";
				rowhtml += "</div></li><li><div class='wrapper'>";
				rowhtml += "<label for='email"+i+"'>No. Identitas (KTP atau SIM)</label>";
				rowhtml += "<input type='text' id='ktp"+i+"' name='ktp[]' class='requiredField' placeholder='Nomor Identitas' />";
				rowhtml += "</div></li><li>";
				rowhtml += "<div class=wrapper><label for=name>Alamat</label>";	
				rowhtml += "<input type='text' id='alamat"+i+"' name='alamat[]' class='requiredField'  placeholder='Alamat' />";
				rowhtml += "</div></li><li><div class='wrapper'>";
				rowhtml += "<label for='phone'>Nomor Handphone</label>";	
				rowhtml += "<input type='text' id='phone"+i+"' name='phone[]' class='requiredField' placeholder='Harus valid untuk pengiriman nomor tiket' />";	
				rowhtml += "</div></li></ul></div>";		
			}	
  	 	}else{
  	 		alert('Select insert all data first!');
  	 	}
  	 	$("#formDta").append(rowhtml);
  	 	
  	 	$("#name1").val(nameHdr);
  	 	$("#ktp1").val(ktpHdr);
  	 	$("#alamat1").val(alamatHdr);
  	 	$("#phone1").val(phoneHdr);
  	 }
  }
  
  /*function submitForm() {
  	 var qty 	= $("#quantity").val();
  	 var ticket = $("#ticket").val();
     var name = $("#name").val();
     var ktp = $("#ktp").val();
     var alamat = $("#alamat").val();
     var phone = $("#phone").val();
     var email = $("#email").val();
  	 var err 	= 0;

  	 
  	 if(qty == '0') {
  	 	//alert('Please Select Quantity');
  	 	err++;
  	 }
  	 if(ticket == '') {
  	 	//alert('Please Select Ticket');
  	 	err++;
  	 }
     
  	 if(err == 0){
  	 	 $.post(All.get_url("ticketing/payment") , $("#qcTcktForm").serialize(), function(data)
        {
        	$("#resTEs").html(data);
        });.fail(function() { 
            alert("Error requesting page"); 
            
        });
  	 }else{
  	 	alert('Please check Place/Quantity');
  	 }
  	  
  }*/
</script>

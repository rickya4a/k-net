<script type="text/javascript">
    // To conform clear all data in cart.
    function clear_cart() {
        var result = confirm('Are you sure want to clear all bookings?');

        if (result) {
            window.location = "<?php echo base_url(); ?>index.php/shopping/remove/all";
        } else {
            return false; // cancel button
        }
    }

    function submitData(){
        var size = document.getElementById("kwrist_size").value;
        var color = document.getElementById("kwrist_color").value;
        var prd1 = document.getElementById("kion_color").value;

        console.log(size);
        console.log(color);
        console.log(prd1);

        if(size.trim() != "#" && color.trim()  != "#" && prd1.trim()  != "#"){
            addToCartVera();

        }else{
            alert("Silahkan lengkapi detail produk ..");
        }
    }

    function addToCartVera(){
        var size = document.getElementById("kwrist_size").value;
        var color = document.getElementById("kwrist_color").value;

        var prd1 = document.getElementById("kion_color").value;
        var prd2= size+'-'+color;


        var prod=[];
        prod.push(prd1);
        prod.push(prd2);

        //console.log(prod);

        $.ajax({
            dataType: 'json',
            url: All.get_url('dummyvera1/getProd'),
            data: {prdcd1: prd1, prdcd2: prd2},
            type: 'POST',
            success: function (result) {
                console.log(result);

                if( result.response == "false"){
                    alert(result.message);
                }else{

                    //produk 1
                     var prdcd1 = result.prdcd1;
                     var prdnm1 = result.prdnm1;
                     var bv1 = result.bv1;
                     var qty1 = 1;
                     var westPrice1 = result.price_w1;
                     var eastPrice1 = result.price_e1;
                     var eastCPrice1 = result.price_ce1;
                     var westCPrice1 = result.price_cw1;
                     var weight1 = result.weight1;
                     var desc=1;

                     $.ajax ({
                     url: All.get_url("cart/add"),
                     type: 'POST',
                     dataType: 'json',
                     data: {prdcd: prdcd1, prdnm: prdnm1, bv: bv1, qty: qty1, westPrice: westPrice1, eastPrice: eastPrice1,westCPrice: westCPrice1, eastCPrice: eastCPrice1, weight : weight1, desc : desc},
                     success:
                     function(data){
                     All.set_enable_button();
                     console.log("SKIP1");
                     console.log(data);
                     alert(data.message);
                     if(data.response == "true") {
                     var maxID = parseInt($("#maxID").val());
                     var nextMaxID = maxID + 1;
                     $("#maxID").val(nextMaxID);
                     $("#showInfoSumCart").html(null);
                     var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                     $("#showInfoSumCart").append(row);
                     }

                     },
                     error: function (xhr, ajaxOptions, thrownError) {
                     alert(thrownError + ':' +xhr.status);
                     All.set_enable_button();
                     }
                     });

                    //produk 2
                    var prdcd2 = result.prdcd2;
                    var prdnm2 = result.prdnm2;
                    var bv2 = result.bv2;
                    var qty2 = 1;
                    var westPrice2 = result.price_w2;
                    var eastPrice2 = result.price_e2;
                    var eastCPrice2 = result.price_ce2;
                    var westCPrice2 = result.price_cw2;
                    var weight2 = result.weight2;
                    var desc=1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd2, prdnm: prdnm2, bv: bv2, qty: qty2, westPrice: westPrice2, eastPrice: eastPrice2,westCPrice: westCPrice2, eastCPrice: eastCPrice2, weight : weight2, desc : desc},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log("SKIP2");
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + ':' +xhr.status);
                            All.set_enable_button();
                        }
                    });
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' + xhr.status);
            }
        });

        /*$.each(prod, function(index, value) {
           console.log(value);
            $.ajax({
                dataType: 'json',
                url: All.get_url('dummyvera/getProd'),
                data: {prdcd: value},
                type: 'POST',
                success: function (result) {
                    console.log(result);

                    var prdcd = result.prdcd;
                    var prdnm = result.prdnm;
                    var bv = result.bv;
                    var qty = 1;
                    var westPrice = result.price_w;
                    var eastPrice = result.price_e;
                    var eastCPrice = result.price_ce;
                    var westCPrice = result.price_cw;
                    var weight = result.weight;
                    var desc=1;

                    $.ajax ({
                        url: All.get_url("cart/add"),
                        type: 'POST',
                        dataType: 'json',
                        data: {prdcd: prdcd, prdnm: prdnm, bv: bv, qty: qty, westPrice: westPrice, eastPrice: eastPrice,westCPrice: westCPrice, eastCPrice: eastCPrice, weight : weight, desc : desc},
                        success:
                            function(data){
                                All.set_enable_button();
                                console.log(data);
                                alert(data.message);
                                if(data.response == "true") {
                                    var maxID = parseInt($("#maxID").val());
                                    var nextMaxID = maxID + 1;
                                    $("#maxID").val(nextMaxID);
                                    $("#showInfoSumCart").html(null);
                                    var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                                    $("#showInfoSumCart").append(row);
                                }

                            },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + ':' +xhr.status);
                            All.set_enable_button();
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' + xhr.status);
                }
            });
        });*/
        /*var prdcd = $("#prdcd" +param).val();
        var prdnm = $("#prdnm" +param).val();
        var bv = $("#bv" +param).val();
        var qty = $("#qty" +param).val();
        var westPrice = $("#westPrice" +param).val();
        var eastPrice = $("#eastPrice" +param).val();
        var eastCPrice = $("#eastCPrice" +param).val();
        var westCPrice = $("#westCPrice" +param).val();

        var weight = $("#weight" +param).val();
        $.ajax ({
            url: All.get_url("cart/add"),
            type: 'POST',
            dataType: 'json',
            data: {prdcd: prdcd, prdnm: prdnm, bv: bv, qty: qty, westPrice: westPrice, eastPrice: eastPrice,westCPrice: westCPrice, eastCPrice: eastCPrice, weight : weight},
            success:
                function(data){
                    All.set_enable_button();
                    alert(data.message);
                    if(data.response == "true") {
                        var maxID = parseInt($("#maxID").val());
                        var nextMaxID = maxID + 1;
                        $("#maxID").val(nextMaxID);
                        $("#showInfoSumCart").html(null);
                        var row = "&nbsp;&nbsp;&nbsp;&nbsp;"+data.total_item + " items | Rp. " + All.num(data.total_west_price);
                        $("#showInfoSumCart").append(row);
                    }

                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
*/
    }
</script>

<div class="slider">
    <div class="callbacks_container">
        <ul class="rslides" id="slider">
            <!--<li><img src="<?php //echo base_url() ?>assets/images/promo_ieduladha.jpg" class="img-responsive" alt=""/></li>
	    	<li><img src="<?php echo base_url() ?>assets/images/banner.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="<?php echo base_url() ?>assets/images/banner1.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="<?php echo base_url() ?>assets/images/banner4.jpg" class="img-responsive" alt=""/></li>-->
            <?php
            $n = 1;
            foreach($banner as $bann){
                $id = $bann->id;
                $hdr_desc = $bann->hdr_desc;
                $goup_hdr = $bann->goup_hdr;
                $img_url = $bann->img_url;
                $hdr_status = $bann->hdr_status;

                echo "<li><img src=\"".base_url($img_url)."\" class=\"img-responsive\" alt=\"\"/></li>";
            }
            ?>
        </ul>
    </div>
</div>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            pager: true,
        });
    });


</script>
<div class="column_center">
    <div class="container">
        <div class="search">
            <div class="stay">Search Products</div>
            <div class="stay_right">
                <form action="<?php echo base_url('mampirkak/product/name'); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" novalidate>
                    <input type="text" name="name" value="" placeholder="Nama Produk">
                    <input type="submit" value="GO" style="color:#fff;">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>

        <ul class="social">
            <li class="find">Find us here</li>
            <li><a href="https://www.facebook.com/pages/AP-K-Link-Indonesia/154582434691756" target="_blank"> <i class="fb"> </i> </a></li>
            <li><a href="https://twitter.com/official_klink" target="_blank"> <i class="tw"> </i> </a></li>
            <li><a href="https://plus.google.com/u/0/+KlinkCoIdisgone/posts" target="_blank"> <i class="gl"> </i></a></li>
            <li><a href="http://instagram.com/klink_indonesia_official?ref=badge" target="_blank"> <i class="ig"> </i> </a></li>
            <li><a href="http://www.youtube.com/channel/UC1YLBuEkJLzocmc_yp5Pgvw" target="_blank"> <i class="yt"> </i> </a></li>
            <div class="clearfix"> </div>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!--Stripe -->

<!--divider -->
<div class="brands">
    <div class="m_3 voffset3"><span class="left_line"> </span><h3>Product Category</h3><span class="right_line"> </span></div>
</div>

<!--kategori -->

<div class="clearence">
    <div class="content_bottom">
        <div class="container">
            <?php
            foreach($prodCat as $data) {
                $prdcat = $data->cat_id;
                $jpg = $data->cat_id.'.jpg';
                $url = base_url('assets/images/kat/'.$jpg.'');
                echo "
                 	<div style=\"padding:0px;\" class=\"grid-item col-md-4\">
                <a href=\"".site_url()."/mampirkak/productHeader/cat/id/$prdcat\">
                    <img src=\"$url\" class=\"img-responsive\" alt=\"\"/>
                </a>
             </div>
                 	";

            }
            ?>

            <?php

            ?>
        </div>
    </div>
</div>

<!--kategori -->


<!--product -->
<div class="clearence">
    <div class="content_bottom">
        <div class="container" id="listProductDiv">

            <div class="m_3" style="margin-bottom:25px;">

                <span class="left_line"> </span><h3>Best Seller</h3>
                <span class="right_line"> </span></div>
            <?php
            if(isset($prod) || $prod != null)
            {
                ?>
                <form id="formAddCart" class="clearfix">
                    <?php
                    $n = 1;
                    foreach($prod as $row)
                    {
                        $prdcd = $row->prdcd;
                        $imgName = $row->img_url;
                        $prdcdnm = $row->prdnm;
                        $prdcdcat = $row->prdcdcat;
                        $prdcdcatnm = $row->prdnmcatnm;
                        $westPrice = $row->price_w;
                        $eastPrice = $row->price_e;
                        $weight = $row->weight;
                        $bv = $row->bv;
                        $isheader= $row->isheader;
                        $isdetail= $row->isdetail;
                        ?>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 voffset4 thumb-wrap">

                            <div class="view view-first">
                                <h5 class="title-desc">
                                    <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)"><?php echo $prdcdnm;?></a>
                                    <span class="label label-success">BV <?php echo "".number_format($bv,0,".",".");?></span>
                                </h5>
                                <a id="<?php echo $prdcd;?>" onclick="Shopping.get_detailProds(<?php echo $n;?>)">

                                    <img src="<?php echo $folderPrd.$imgName; ?>" class="lazy img-responsive" alt=""/>
                                    <!--<img src="<?php echo $imgName; ?>" class="lazy img-responsive" alt=""/>-->
                                </a>
                                <div class="tab_desc">
                                    <div class="col-xs-12 col-md-12 nopadding">
                                        <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                            <h5 class="green">Rp. <?php echo "".number_format($westPrice,0,".",".");?></h5>
                                            <h5 class="p3">Harga Wilayah A</h5>
                                        </div>
                                        <div class="col-xs-6 col-md-6 nopadding price-wrap">
                                            <h5 class="green">Rp. <?php echo "".number_format($eastPrice,0,".",".");?></h5>
                                            <h5 class="p3">Harga Wilayah B</h5>
                                        </div>
                                        <!--<form method="post" action="<?php echo "".site_url('cart/addtocart')."";?>">-->

                                        <input type="hidden" name="prdcd" id="<?php echo "prdcd".$n; ?>" value="<?php echo $prdcd;?>"/>
                                        <input type="hidden" name="prdnm" id="<?php echo "prdnm".$n; ?>" value="<?php echo $prdcdnm;?>"/>
                                        <input type="hidden" name="bv" id="<?php echo "bv".$n; ?>" value="<?php echo $bv;?>"/>
                                        <input type="hidden" name="westPrice" id="<?php echo "westPrice".$n; ?>" value="<?php echo $westPrice;?>"/>
                                        <input type="hidden" name="eastPrice" id="<?php echo "eastPrice".$n; ?>" value="<?php echo $eastPrice;?>"/>
                                        <input type="hidden" name="westCPrice" id="<?php echo "westCPrice".$n; ?>" value="<?php echo $westPrice;?>"/>
                                        <input type="hidden" name="eastCPrice" id="<?php echo "eastCPrice".$n; ?>" value="<?php echo $eastPrice;?>"/>
                                        <input type="hidden" name="weight" id="<?php echo "weight".$n; ?>" value="<?php echo $weight;?>"/>



                                        <?php
                                        $usr = getUsername();

                                        if($isheader == 1 AND $isdetail == 0){
                                            $view1= "button";
                                            $view2="";
                                            $view3="button";

                                            echo "<br>";
                                            echo  "<div id=\"knano\" style=\"font-size: 12px\" align=\"left\" $view2> K-ION NANO :
                                                <select id=\"kion_color\" name=\"kion_color\">
                                                <option value=\"#\">- Warna -</option>";

                                            foreach($prod1 as $row1) {
                                                $prdcd1 = $row1->prdcd;
                                                $name1 = $row1->warna;
                                                echo "<option value=\"$prdcd1\">$name1</option>";
                                            }

                                             echo  "</select></div>";
                                            echo "<br>";
                                            echo  "<div id=\"kwrist\" style=\"font-size: 12px\" class=\"title-desc\" align=\"left\" $view2>K-E WRIST BAND :
                                                <select id=\"kwrist_size\" name=\"kwrist_size\">
                                                <option value=\"#\">- Uk. -</option>";

                                            foreach($prod2 as $row2) {
                                                $prefix= $row2 -> prefix;
                                                $ukuran_desc = $row2->ukuran_desc;
                                                $code2 = $row2->ukuran;
                                                $val1= $prefix.$code2;
                                                //echo "<option value=\"HC050".$code2."-\">$code2</option>";
                                                echo "<option value=\"$val1\">$ukuran_desc</option>";
                                            }

                                                echo "</select>";

                                            echo "<select id=\"kwrist_color\" name=\"kwrist_color\">
                                                <option value=\"#\">- Warna -</option>";
                                            foreach($prod3 as $row3) {
                                                $end_pref= $row2 -> end_prefix;
                                                $nwarna = $row3->warna;
                                                $code3= $row3 ->id_warna;
                                                $val2= $code3.$end_pref;

                                                echo "<option value=\"$val2\">$nwarna</option>";
                                            }
                                            echo    "</select></div>";

                                            echo "<input id=\"enter\" type=\"$view3\" class=\"btn1 btn-primary1\" onclick=\"submitData()\" value=\"Masukkan Keranjang\"/>";

                                        }else{
                                           $view1="hidden";
                                           $view2="hidden";
                                           $view3="button";
                                            echo "<br>";
                                            echo "<br>";
                                            echo "<br>";
                                            echo "<br>";
                                            echo "<br>";
                                            echo "<br>";
                                            echo "<input type=\"$view3\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukkan Keranjang\"/>";

                                        }
                                        //echo getUsername();
                                        //print_r("user:".$usr);
                                        /*
                    				  if($usr != NULL) {
                    				  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"Shopping.addToCart($n)\" value=\"Masukan ke keranjang\"/>";
									  } else {
									  	  echo "<input type=\"button\" class=\"btn1 btn-primary1\" onclick=\"return alert('Silahkan login terlebih dahulu..!!')\" value=\"Masukan ke keranjang\"/>";
									  }*/
                                         ?>

                                    </div>
                                    <div class="clearfix"></div>
                                    <!--<a href="cart.php" class="btn1 btn-primary1">
                                            <span>Add To Cart</span><i class="fa fa-plus"></i>
                                    </a>-->
                                </div>
                            </div>
                        </div>

                        <?php
                        $n++;
                    }
                    ?>
                </form>
                <?php
            } else {
                echo "Pencarian produk yang mengandung kata '$prdnm' tidak ditemukan..";
            }
            ?>
            <div class="col-md-12 voffset4 text-center">
                <!--<a href="<?php echo "".site_url('shop/product/cat')."";?>" class="btn1 btn2 btn-primary1"><span>View More Product</span><i class="fa fa-list"></i></a> -->
            </div>
        </div>
        <!--container -->
        <div class="container" id="detailProductDiv" style="display:none">

        </div>
    </div>
    <!--content_bottom -->
</div>
<!--clearence -->
<!--product -->

<!--promo -->
<div class="brands">
    <div class="m_3 voffset3"><span class="left_line"> </span><h3>Promotion</h3><span class="right_line"> </span></div>
</div>

<!--promo -->
<div class="row">
    <div class="container">
        <div class="col-md-12">
            <a href="<?php echo "".site_url('mampirkak/productHeader/cat/id/13').""; ?>"><img src="<?php echo base_url() ?>assets/images/promo.jpg" class="img-responsive margin0" alt=""/></a>
        </div>
        <!--        <div class="col-md-6">-->
        <!--            <a href="--><?php //echo "".site_url('tracking')."";?><!--"><img src="--><?php //echo base_url() ?><!--assets/images/tracking.jpg" class="img-responsive margin0" alt=""/></a>-->
        <!--        </div>      -->
    </div>
</div>
<!--promo -->



<!--brand -->
<div class="brands">
    <div class="m_3 voffset3"><span class="left_line2"> </span><h3>Product Category</h3><span class="right_line2"> </span></div>
    <div class="container">
        <ul id="logoslider">
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-1.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-2.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-3.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-4.jpg" class="category" alt=""/> </a></li>
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-5.jpg" class="category" alt=""/> </a></li
            <li><a href="#"><img src="<?php echo base_url() ?>assets/images/category-6.jpg" class="category" alt=""/> </a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">

    $(window).load(function() {
        $("#logoslider").flexisel({
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
        });

    });
</script>
<!--brand -->

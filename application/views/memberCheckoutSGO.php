<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         Register Member&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Payment&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->
<!--Payment Wrapper-->
<div class="row vpullset4">
	<div class="container">
       <div id="paymentDiv">
       	<!--<form action="<?php echo site_url('checkout_process'); ?>" method="POST" id="payment-form">
    	 Left Content-->
        <div class="col-lg-7 col-xs-12 nopadding voffset4">
        	<!-- Billing Info-->
        	<div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-usd"></i> Informasi Penerima
                </span>
                <div class="col-lg-12 col-xs-12 vpullset3"> 
            		<div class="col-lg-12 col-xs-12 delivery_header">
                    	
                   
                    
                    <!--form pengiriman-->
					
                        <div class="register-top-grid">
                            <?php
                              $inp = $this->session->userdata('personal_info');
							  if($inp['delivery'] == "2")
							  {
                            ?>
                            <div>
                            <span>Nama Depan<label><font color="red">*</font></label></span>
                                <input type="text" id="nama_penerima"  readonly="readonly" value="<?php echo $inp['nama_penerima']; ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo $inp['notlp']; ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo $inp['email']; ?>"> 
                            </div>
                            
                            
                            <?php
                             } else {
                            ?>
                            <div>
                            	<span>Nama <label><font color="red">*</font></label></span>
                                <input type="text" id="firstname"  readonly="readonly" value="<?php echo getUsername(); ?>"> 
                            </div>
                            
                            <div>
                                <span>No Telp/HP<label><font color="red">*</font></label></span>
                                 <input type="text" id="notlp"  readonly="readonly" value="<?php echo getUserPhone(); ?>">
                            </div>
                            <div>
                                <span>Alamat Email<label><font color="red">*</font></label></span>
                                 <input type="text" id="email"  readonly="readonly" value="<?php echo getUserEmail(); ?>"> 
                            </div>
                            <?php
							 }
                            ?>
                            <div class="clearfix"> </div>					   
                        </div>				     
					  </div>
                	<!--form pengiriman--> 
				</div>   
			</div>
            <!-- Billing Info-->
        	
            <!-- Paymen Method-->
			<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-money"></i> Metode Pembayaran
                </span>
                <!--<div class="col-lg-12 col-xs-12 nopadding voffset4">   
                    <div class="col-lg-12 col-xs-12">       	
                        <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
                            <div class="register-top-grid">                            
                                <div class="nomargin">
                                    <select class="form-list small" id="chooseCardType" name="chooseCardType">
                                      <option value="" selected>--Pilih Kartu--</option>	
                                      <option value="cc">Kartu Kredit</option>
                                      <option value="dc">Debit Card</option>
                                      
                                    </select>
                                </div>
                                <div class="nomargin">
                                	<select class="form-list small" id="payType" name="payType">
                                		<option value="" selected>--Pilih jenis pembayaran--</option>	
                                	</select>	
                                </div>	
                            </div>
                        </div> -->
                        
                        <div class="col-lg-12 col-xs-12 nopadding voffset4"> 
		                   <div class="col-lg-12 col-xs-12"> 	      	
		                    <div class="col-lg-12 col-xs-12 delivery_header" style="font-size:inherit;">
		                        <div class="register-top-grid">                            
		                            <div class="nomargin"> 
		                                <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
		                            </div>
		                             <div class="nomargin">
		                             	&nbsp;
		                             </div>	
		                        </div>
		                    </div>
		                   </div> 
                        
                        <!-- detail for credit card-->
    					<div class="col-lg-12 col-xs-12 nopadding" id="choosePaymentDiv">
                        	
                        </div>
                         
                    </div>                    
				</div>
        	</div>
        	<!-- Paymen Method-->
            
			
		</div>
        <!-- Left Content-->
        
        <!-- right Content-->
		<div class="col-lg-5 col-xs-12 nopadding voffset4">
        	<!-- Adress Delivery-->
            <div class="col-lg-12 col-xs-12">
                <span class="title_header">
                    <i class="fa fa-check-square-o"></i> Alamat Tujuan Pengiriman
                </span>
                <div class="col-lg-12 col-xs-12 cart_header">
                 <?php
                  $inp = $this->session->userdata('personal_info');
				  if($inp['delivery'] == "2")
				   {
				   	 //echo $inp['firstname']." ".$inp['lastname']."</br>";
				   	 echo $inp['nama_penerima']."</br>";
					 echo $inp['alamat']."</br>";
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 //echo $inp['nama_kelurahan']."</br>";
					 echo $inp['notlp']."</br>";
				   }
				  else {
					 echo $inp['nama_provinsi']."</br>";
					 echo $inp['nama_kota']."</br>";
					 echo $inp['nama_kecamatan']."</br>";
					 echo $inp['nama_stockist']."</br>";
				  }	
                  ?>	
                       	 
                </div>
            </div>   
        	<!-- Adress Delivery-->
            
            <!-- Order Details-->
        	<div class="col-lg-12 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Pembelanjaan Starterkit
                </span>
				<br />
				
                
						
						<?php
						$member_info = $this->session->userdata("member_info");
						//echo "ID Member"
						//print_r($member_info);
						?>
						<div class="col-lg-12 col-xs-12 cart_header">
						ID Sponsor     : <?php echo $member_info['sponsorid']."<br />"; ?>
						Nama Sponsor   : <?php echo $member_info['sponsorname']."<br />"; ?>
						Nama Member    : <?php echo $member_info['membername']."<br />"; ?>
						No KTP         : <?php echo $member_info['idno']."<br />"; ?>
						Jenis Kelamin  : 
						<?php 
						  if($member_info['sex'] == "M") {
						  	echo "Pria<br />";
						  } else {
						  	echo "Wanita<br />";
						  }
						?>  
						Tgl Lahir      : <?php echo $member_info['tgllhr']."/".$member_info['blnlhr']."/".$member_info['thnlhr']."<br />"; ?>
						Alamat         : <?php echo $member_info['addr1']." ".$member_info['addr2']." ".$member_info['addr3']."<br />"; ?>
						No HP          : <?php echo $member_info['tel_hp']."<br />"; ?>
						Rekening Bank  : <?php echo $member_info['pilBank']."<br />"; ?>
						No Rekening    : <?php echo $member_info['no_rek']."<br />"; ?>
						</div>
                
                <!-- header-->
                <div class="col-lg-12 col-xs-12 cart_header">          	
                    <div class="col-lg-4 col-xs-4" align="right">Product </div>
                    <div class="col-lg-2 col-xs-2" align="right">Qty</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Price</div> 
                    <div class="col-lg-3 col-xs-3" align="right">Amount</div>          	 
                </div>  
                <!-- header-->   
                
                <!-- product item-->
                
                
                <?php
                   $starterkit_prd = $this->session->userdata("starterkit_prd");
				   //print_r($cart);
                   $pricecode = $this->session->userdata('pricecode');
                   
                   	 echo "<div class=\"col-lg-12 col-xs-12 order_wrap\">";
						 echo "<div class=\"col-lg-4 col-xs-4 order_desc\">";
	                     echo "<h3>".$starterkit_prd['prdnm']."</h3>";
	                     $price = 0; 
	                     echo "</div>";
						
	                     echo "<div class=\"col-lg-2 col-xs-2\" align=right>1</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".number_format($starterkit_prd['price'], 0, ",", ".")."</div>"; 
	                     echo "<div class=\"col-lg-3 col-xs-3\" align=right>".number_format($starterkit_prd['price'], 0, ",", ".")."</div>
                     </div>";  
                     
                ?>	 
                    <!--<div class="col-lg-6 col-xs-6 order_desc">
                        <h3>Kinokatara</h3>
                        <p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>
                    </div>
                    <div class="col-lg-3 col-xs-3">5</div> 
                    <div class="col-lg-3 col-xs-3">Rp. 130.000</div> -->
                
                <!-- product item-->
                
                <!-- shipping cost-->
                <div class="col-lg-12 col-xs-12 order_wrap">   
                    <div class="col-lg-4 col-xs-4 order_desc">
                        <h3>Biaya Kirim</h3>
                    </div>
                    <div class="col-lg-2 col-xs-2" align="right">
                    	<?php echo $starterkit_prd['weight']; ?>
                    </div> 
                    <div class="col-lg-3 col-xs-3" align="right">&nbsp</div> 
                    <div class="col-lg-3 col-xs-3" align="right">
                    	<?php
                    	  //print_r($pricelist->price);
                    	  $shipping_jne_info = $this->session->userdata('shipping_jne_info');
						  if($shipping_jne_info == null) {
						  	echo number_format(0, 0, ",", ".");
						  } else {
						  	echo number_format($shipping_jne_info['price'], 0, ",", ".");
						  }
						   
                    	?>
                    </div> 
                </div>  
                <!-- shipping cost-->
                
                <!-- subtotal-->
                <div class="col-lg-12 col-xs-12 order_total_wrap">   
                    <div class="col-lg-6 col-xs-6">                	
                        Subtotal
                    </div>
                    <div class="col-lg-6 col-xs-6">                	
                        <p>
                        <?php 
						$ship_price = 0;
						if($shipping_jne_info != null) {
							$ship_price = $shipping_jne_info['price'];
						}
                        $total = $starterkit_prd['price'] + $ship_price;
                        echo number_format($total, 0, ",", "."); 
                        ?>
                        </p>
                    </div>
                </div>
                <!-- subtotal-->
                
                <!-- order number
                <div class="col-lg-12 col-xs-12 order_number">                	
                    <p>No. Order Anda Adalah :</p>
                    <p><span>#ha7a63hd8</span></p>
                </div> --> 
                <!-- order number-->                  
			</div>
            <!-- Order Details-->            
		</div>
        <!-- right Content-->
        
        <!-- next button-->
        <div class="col-lg-12">
        	<form method="get" action="<?php echo base_url('member/starterkit/back'); ?>">
        		<button class="btn1 btn2 btn-primary1 submit-button" type="submit">Ubah Data Pengiriman</button>
        	</form>
            <!--<button onclick="Shopping.back_to_cart('#divCheckOut','#formCart1')" type="button" class="btn1 btn2 btn-primary1 pull-left" style="margin-right:15px;" id="back"><i class="fa fa-arrow-left" ></i> Kembali ke Cart</button>&nbsp;&nbsp;
            <button onclick="Shopping.proceedPayment()" type="button" class="btn1 btn2 btn-primary1 pull-left" id="checkout"><span>Checkout</span><i class="fa fa-check-circle"></i></button>&nbsp;&nbsp;-->
                                
        </div>
        <!-- next button-->       
        
    </div>
    <!--</form>-->
    </div>
    <div id="nextPaymentProcessDIV"></div>
</div>

<!--Payment Wrapper-->
<?php
   $usr = getUsername();
   $usrid = getUserID();
?>
<!DOCTYPE HTML>
<html>
<head>
<title>K-Link Digital Store</title>
<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files comment -->
<link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->

	<!-- Google Tag Manager NANDANG 2018-09-12 -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K899XNL');</script>
	<!-- End Google Tag Manager -->


<link href='https://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>

<script src="<?php echo base_url() ?>asset/js_module/global.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/shopping.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/shopping_cust.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/sales.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/umroh.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/promo.js"></script>
<!-- payment-->
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery-1.4.3.min.js"></script>
<!--<script type="text/javascript" src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.js"></script>-->
<script src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/' ?>jquery.fancybox-1.3.4.css"/>
<style>
.btn-link{
  border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#FFFFFF;
  padding:0;
  text-decoration:underline;
  font-family:inherit;
  font-size:inherit;
}
</style>
<!--Google Analytic-->
<!--script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77205058-1', 'auto');
  ga('send', 'pageview');

</script> -->

<!--<script type="text/javascript" src="https://secure.sgo.co.id/public/signature/js"></script>-->

<!--slider-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.flexisel.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/customs.js"></script>

		<!-- Facebook Pixel Code -->
	<!--	<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		 	fbq('init', '279471709537754');
			fbq('track', 'PageView');
		</script>
	-->
		<noscript>
		 <img height="1" width="1"
			src="https://www.facebook.com/tr?id=279471709537754&ev=PageView
			&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '423327018470580');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=423327018470580&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<!-- Google Tag Manager (noscript) NANDANG 2018-09-12 -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K899XNL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--Header-->




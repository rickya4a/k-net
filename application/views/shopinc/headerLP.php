<!DOCTYPE HTML>
<html>
<head>
<title>K-Link WebStore</title>
<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='https://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>

<script src="<?php echo base_url() ?>asset/js_module/global.js"></script>
<!--<script src="<?php echo base_url() ?>assets/module_js/shopping.js"></script>-->
<script src="<?php echo base_url() ?>assets/module_js/memberlp.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/shopping.js"></script>

<!-- payment-->
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery-1.4.3.min.js"></script>
<!--<script type="text/javascript" src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.js"></script>-->            
<script src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/' ?>jquery.fancybox-1.3.4.css"/>

<!--Google Analytic-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77205058-1', 'auto');
  ga('send', 'pageview');

</script>

<!--slider-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.flexisel.js"></script>
	
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		 	fbq('init', '279471709537754'); 
			fbq('track', 'PageView');
		</script>
		<noscript>
		 <img height="1" width="1"
			src="https://www.facebook.com/tr?id=279471709537754&ev=PageView
			&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '423327018470580');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=423327018470580&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>

<!--Header-->
<div class="header">
	<!-- top-header -->
	<div class="header_top">
    	<!-- top-container -->
    	<div class="container">
            <div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : cs@k-link.co.id</p>
            </div>
            <!-- top-left -->
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 pull-right">
                <!-- Checkout -->
                <ul class="header_check">
                    <a class="login" href="<?php echo site_url('cart/list');?>">
                    	<i class="cart"></i> 
                    	<li class="user_desc" id="showInfoSumCart">
                    		
						</li>
                  	</a>
                </ul>
                <!-- Checkout -->
                <!-- My Account-->
                <ul class="header_user_info">
            	
                  <div class="clearfix"> </div>
                </ul>
                <!-- My Account-->        
                <!-- start search-->
                <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your keyword" type="search" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value=""/>
                            <span class="sb-icon-search"> </span>
                        </form>
                     </div>
                </div>
                <!----search-scripts---->
                <script src="<?php echo base_url() ?>assets/js/classie.js"></script>
                <script src="<?php echo base_url() ?>assets/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>
                <!----//search-scripts---->             
                <div class="clearfix"></div>
            </div>
            <!-- top-left --> 
		<div class="clearfix"> </div>
		</div>
    	<!-- top-container -->
	</div>
    <!-- top-header -->
    
  	<!-- bottom-header -->
  	<div class="header_bottom">
		<div class="container">
            <!-- Logo -->	 			
            <div class="logo">
                <a href="index.php"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""/></a>
            </div>	
            <!-- Logo -->
            
            <!-- Right Menu--> 
            <div class="header_bottom_right">			
                <!-- start h_menu4 -->
                <div class="h_menu4">            
                    <a class="toggleMenu" href="#">Menu</a>
                    <ul class="nav">
                        <li class="active"><a href="<?php echo "".site_url('shop/product');?>">Home</a></li>
                        
                    </ul>
                    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/nav.js"></script>
                </div>
                <!-- end h_menu4 -->
              
                <!-- cart info 
                <div class="shopping_cart pull-right">
                    <ul class="shopping_cart_top">
                        <a href="#">
                           <li class="shop_left">Your cart</li>
                           <li class="shop_right"><img src="<?php echo base_url() ?>assets/images/arrow1.png" alt=""/></li>
                           <div class="clearfix"></div>	
                        </a>
                    </ul>
                    <ul class="shopping_bag">
                        <a href="<?php echo site_url('cart/list');?>">
                           <li class="bag_left"><img src="<?php echo base_url() ?>assets/images/bag.png" alt=""/></li>
                           <li class="bag_right" id="showInfoSumCart">
						   <?php 
						   if($this->cart->total_items() != 0) { 
						     //echo $this->cart->total_west_price();
                             echo $this->cart->total_items();
						   } else { 
						     echo "0";
						   }
						   ?> Items | 
						   <?php 
						   if($this->cart->total_west_price() != 0) { 
						     echo "Rp. ".number_format($this->cart->total_west_price(),0,".",".");
						   } else { 
						     echo "Rp. 0";
						   }
						   ?></li>
                           <div class="clearfix"></div>	
                        </a>
                    </ul>		
                </div>
                <div class="clearfix"></div>
                 cart info -->
            </div>
            <!-- Right Menu--> 
        </div>
	</div>
    <!-- bottom-header -->
</div>
<!--Header-->

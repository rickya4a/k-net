<!--footer-->
<div class="footer_top">
    <div class="container">
        <div class="col-sm-2 grid-3">
            <h3>Tentang K-Link</h3>
            <ul class="footer_list">
            <li><a href="<?php echo site_url('static/about'); ?>">Tentang Kami</a></li>
                <li><a href="<?php echo site_url('static/why');?>">Kenapa Harus K-Link</a></li>
            <li><a href="<?php echo "".site_url('static/shipping_rate')."";?>">Biaya Kirim</a></li>
            <li><a href="<?php echo "".site_url('static/replacement')."";?>">Penggantian</a></li>
                <li><a href="<?php echo site_url('static/delivery');?>">Pengiriman</a></li>
            </ul>
        </div>
        <div class="col-sm-2 grid-3">
            <h3>Bantuan Untuk Anda</h3>
            <ul class="footer_list">
            <li><a href="<?php echo site_url('static/how-to-order'); ?>"">Cara Berbelanja</a></li>
                <li><a href="<?php echo site_url('cart/list'); ?>"">Keranjang Belanja</a></li>
                <li><a href="<?php echo site_url('myaccount'); ?>">Akun Saya</a></li>
            </ul>
        </div>
        
        <div class="col-sm-2 grid-3">
            <h3>Link Bantuan</h3>
            <ul class="footer_list">
                <li><a href="http://www.k-link.co.id/news/det/73/Harga-K-Link-terbaru-Per-Juli-2014">Price List</a></li>                    
                <li><a href="http://www.k-link.co.id/store">Audio Store</a></li>
            <li><a href="#">Contact Us</a></li>
            </ul>
        </div>      
        <div class="col-sm-3 grid-3">
                  <h3>Subscribe</h3>
                  <p style="font-size:13px; margin-bottom:10px;">Masukkan email Anda untuk update info seputar produk K-Link</p>
                  <div class="register-top-grid">
                    <form action="<?php echo base_url() ?>daftaremail" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline"  >
                        <div style="width:100%">
                            <input type="text" value="" name="name" id="name" placeholder="Name" style="width:100%;" required >
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        </div>
                        <div style="width:100%">
                        <input type="email" value="" name="email" id="email" placeholder="E-mail Address" style="width:100%;" required >
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                         </div>
                        <div style="width:100%">
                            <input type="text" value="" name="nope" id="nope" placeholder="Phone Number" style="width:100%;">
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        </div>

                    <div style="width:100%; margin:0px;">                        
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn1 btn2 btn-primary1">
                        <div style="position: absolute; left: -5000px;"><input type="text" name="b_47314fe9f13048a3b7ae50701_98e8200075" class="form-control" tabindex="-1" value=""></div>
                    </div>
                </form>
                        
                  </div>
            </div>
            <div class="col-sm-3 grid-3">
            <h3>Pembayaran</h3>
            <img class="img-responsive" src="<?php echo base_url() ?>assets/images/secure.png" alt=""/>
        </div>
    </div>
</div>

<div class="footer_bottom">
    <div class="container">
        <div class="cssmenu">
            <ul>
                <li class="active"><a href="<?php echo "".site_url('shop/product');?>">Home</a></li> |
                <li><a href="<?php echo site_url('static/about'); ?>">About K-Link</a></li> |
                <li><a href="<?php echo "".site_url('static/how-to-order')."";?>">How To Order</a></li> 
            </ul>
        </div>
        <div class="copy">
            <p>Copyright &copy; 2014 ~ 2017 - PT K-Link Indonesia - Digital Network Marketing Bersistem Syariah</p>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.lazyload.js"></script>

<!--Live Chat Crisp-->

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="25553668-2398-4d20-8486-ddfbdccb8f47";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<script>
$crisp.push(["set", "user:nickname", ["<?php echo  $usrcrisp = getUsername(); ?>"]]);
$crisp.push(["set", "session:data", ["IDMember", "<?php echo $idcrisp = getUserID();?>"]]);
$crisp.push(["set", "user:email", "<?php echo $emailcrisp = getUserEmail()?>"]);
var visitor_email = $crisp.get("user:email");
</script>



 <script type="text/javascript" charset="utf-8">
  $(function() {
     $("img.lazy").lazyload({
         effect : "fadeIn",
         threshold : 200
     });
     
     $('.uppercase').bind('change', function() {
         $(this).val($(this).val().toUpperCase().replace(/'/g,"`"));
     });

    

    $('.numeric-input').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
        }
    });

  });
  
  function textAreaUppercase(idx) {
    var str=idx.value;
    idx.value=str.toUpperCase().replace(/'/g,"`");
  }
  </script>
<script type="text/javascript">
    $('.button').click(function(){
          $('#modal').css('display','block');
          $('.modal-bg').fadeIn();
    });

        $('#close').click(function(){
              $('.modal-bg').fadeOut();     
              $('#modal').fadeOut();
          return false;
        });
</script>

<!-- untuk datatable 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<!--<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>-->
<!--<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>-->
<script>/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-80px";
  }
  prevScrollpos = currentScrollPos;
}</script>

  
</body>
</html>     

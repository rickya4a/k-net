<?php
   $usr = getUsername(); 
   $usrid = getUserID();
?>
<!DOCTYPE HTML>
<html>
<head>
<title>K-Link Digital Store</title>
<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='https://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>

<script src="<?php echo base_url() ?>asset/js_module/global.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/shopping.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/shopping_cust.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/sales.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/umroh.js"></script>
<script src="<?php echo base_url() ?>assets/module_js/promo.js"></script>
<!-- payment
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.js"></script>
<script src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>-->            
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/' ?>jquery.fancybox-1.3.4.css"/>
<!--Google Analytic-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77205058-1', 'auto');
  ga('send', 'pageview');

</script>

<!--<script type="text/javascript" src="https://secure.sgo.co.id/public/signature/js"></script>-->

<!--slider-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.flexisel.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/customs.js"></script>




<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
 	fbq('init', '279471709537754'); 
	fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1"
	src="https://www.facebook.com/tr?id=279471709537754&ev=PageView
	&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '423327018470580');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=423327018470580&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



</head>
<body>

<!--Header-->
<div class="header">
	<!-- top-header -->
	<div class="header_top">
    	<!-- top-container -->
    	<div class="container">
            <div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : customer_service@k-link.co.id</p>
            </div>
            <!-- top-left -->
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 pull-right">
                <!-- Checkout -->
                <ul class="header_check">
                	<?php
                	if($usr != NULL) {
                	?>
                    <a class="login" href="<?php echo site_url('cart/list');?>">
                    <?php
					} else {
						
                    ?>	
                    <a class="login" href="<?php echo site_url('shop/cart/list');?>">
                    <?php
					} 
                    ?>	
                    	<i class="cart"></i> 
                    	<li class="user_desc" id="showInfoSumCart">
                    		<?php 
	                    		if($this->cart->total_items() != 0) {
	                    		  echo $this->cart->total_items();
	                    			
								} else {
								    echo "0";
								}
							?> 
							Items | 
							<?php 
    							$pricecode = $this->session->userdata('pricecode');
    							if($pricecode == "12W3" ) {
    							    if($this->cart->total_west_price() != 0) {
    							        echo "Rp. ".number_format($this->cart->total_west_price(),0,".",".");
    								} else {
    									echo "Rp. 0";
    								}
    							} else {
    								if($this->cart->total_east_price() != 0) {
    							        echo "Rp. ".number_format($this->cart->total_east_price(),0,".",".");
    								} else {
    									echo "Rp. 0";
    								}
    							}
                                
                                
							?>
						</li>
                  	</a>
                </ul>
                <!-- Checkout -->
                <!-- My Account-->
                <ul class="header_user_info">
            	<?php 
                	
                	//echo getUsername(); 
                	if($usr != NULL) {
                		echo "<i class=\"user\"></i>"; 
                		echo "<li class=\"user_desc\">";		
                		echo "<a href=\"".site_url('tracking')."\"> Hi, ".$usr." | </a>";
            	?><a href="<?php echo "".site_url('logout')."";?>">Logout</a></li>
            	<?php 
				}else{ 
                    echo "<i class=\"user\"></i>"; 
                    echo "<li class=\"user_desc\"> <a href=\"".site_url('loginmember')."\" class=\"user\">Hi User, Please Login</a></li>";}
            	?>
                  <div class="clearfix"> </div>
                </ul>
                <!-- My Account-->        
                <!-- start search-->
                <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your keyword" type="search" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value=""/>
                            <span class="sb-icon-search"> </span>
                        </form>
                     </div>
                </div>
                <!----search-scripts---->
                <script src="<?php echo base_url() ?>assets/js/classie.js"></script>
                <script src="<?php echo base_url() ?>assets/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>
                <!----//search-scripts---->             
                <div class="clearfix"></div>
            </div>
            <!-- top-left --> 
		<div class="clearfix"> </div>
		</div>
    	<!-- top-container -->
	</div>
    <!-- top-header -->
    
  	<!-- bottom-header -->
  	<div class="header_bottom">
		<div class="container">
            <!-- Logo -->	 			
            <div class="logo">
                <a href="<?php echo "".site_url('shop/product');?>"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""/></a>
            </div>	
            <!-- Logo -->
            
            <!-- Right Menu--> 
            <div class="header_bottom_right">			
                <!-- start h_menu4 -->
                <div class="h_menu4">            
                    <a class="toggleMenu" href="#">Menu</a>
                    <form>
                    <ul class="nav">
                        <li class="active"><a href="<?php echo "".site_url('shop/product');?>">Beranda</a></li>
                        <!--<li><a href="products.html">About us</a></li>
                        <li><a href="<?php echo "".site_url('static/how-to-order')."";?>">How To Order</a>-->
                        <li><a href="#">Produk </a>
                            <ul>
                                 <?php 
                                        $n=1;
                                        foreach($prodCat as $row)
                                        {   
                                            $prdcat = $row->cat_id;
                                            echo "<li>";
                                            echo "
                                                <input type=\"hidden\" id=\"prdcat$n\" value=\"".$row->cat_id."\">
                                                <a href = \"".site_url()."/shop/productHeader/cat/id/$prdcat\" >".$row->cat_desc."</a>                           
                                                
                                            ";
                                            echo "</li>";
                                            $n++;
                                        }
                                  ?>   
                            </ul>
                        </li>
                        <!--<li><a href="<?php echo "".site_url('sales').""; ?>">Cek Transaksi</a></li>-->
                        
                        <!--<li><a href="products.html">Cek Bonus</a></li>-->
                        <li><a href="#">Sales & Bonus</a>
                            <ul>
                                    <li><a href="<?php echo "".site_url('sales/direct/downline').""; ?>">Info Jaringan</a></li>
                                    <li><a href="<?php echo "".site_url('bonus').""; ?>">Bonus Report</a></li>
                                    <li><a href="#">Cek Transaksi</a>
                                      <ul>
                                      	 <li><a href="<?php echo "".site_url('sales/online').""; ?>">Transaksi K-net</a></li>	
                                    	 <li><a href="<?php echo "".site_url('sales').""; ?>">Transaksi Global</a></li>
                                      </ul>	 
                                    </li>
                            </ul>
                        </li>
                        
                        <li>
                        	<a href="#">Pendaftaran Distributor</a>
                        	<ul>
                        		<li><a href="<?php echo "".site_url('member/choosePay').""; ?>">Pendaftaran Distributor</a></li>
                        		<li><a href="<?php echo "".site_url('member/recruit').""; ?>">Daftar Rekrutan Member</a></li>
                        	</ul>
                        	
                        </li>
                        <li><a href="#">Landing Page Recruit</a>
                          <ul>
                          	 <!--<li><a href="<?php echo "".site_url('member/reg/list/recruit').""; ?>">List Recruit Member</a></li>-->	
                        	 <li><a href="<?php echo "".site_url('member/reg/downline').""; ?>">Update Sponsor</a></li>
                          </ul>	 
                        </li>
                        <li><a href="#">Perjalanan Rohani</a>
                         
                          <ul>
                            <li><a href="#">Pendaftaran</a>
                              <ul>
                              	 <li><a href="<?php echo "".site_url('umroh').""; ?>">Umroh</a></li>	
                            	 <li><a href="<?php echo "".site_url('ziarah').""; ?>">Ziarah</a></li>
                              </ul>	 
                            </li>
                          	 
                        	 <li><a href="<?php echo "".site_url('umroh/cek/mutasi/').""; ?>">Cek Mutasi</a></li>
                            <li><a href="#">Setor Cicilan</a>
                                <ul>
                                    <li><a href="<?php echo "".site_url('umroh/installments/').""; ?>">Cicilan Umroh</a></li>
                                    <!--<li><a href="<?php echo "".site_url('ziarah/installments/').""; ?>">Cicilan Ziarah</a></li>-->
                                </ul>
                            </li>
                            <li><a href="<?php echo "".site_url('umroh/printVchNo/').""; ?>">Cetak Voucher</a></li>
                            <li><a href="<?php echo "".site_url('umroh/reprint/').""; ?>">Reprint Bukti Pendaftaran</a></li>
                          </ul>	 
                        </li>
                        <li><a href="#">Promo Info</a>
	                        <ul>
	                        	<!--	nandang 20161004
	                        	<li><a href="#">Promo Ayuverda</a>
			                        <ul>
			                        	<li><a href="<?php echo "".site_url('promoayu/6').""; ?>">Juni 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promoayu/7').""; ?>">Juli 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promoayu/8').""; ?>">Agustus 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promoayu/9').""; ?>">September 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promoayu/10').""; ?>">Oktober 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promoayu/11').""; ?>">Nopember 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promoayu/12').""; ?>">Desember 2015</a></li>
			                        </ul>
		                        </li>
		                        -->
		                        
		                        <li><a href="<?php echo "".site_url('promo_rekrut').""; ?>">Cek Poin Recruit Rewards</a></li>
		                        <!--<li><a href="<?php echo "".site_url('promo_kolagen').""; ?>">Cek Undian Kolagen Anniversary</a></li>-->
		                        <!--
		                        <li><a href="#">Promo K-Liquid Collagen</a>
		                        	<ul>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1512').""; ?>">Desember 2015</a></li>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1601').""; ?>">Januari 2016</a></li>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1602').""; ?>">Pebruari 2016</a></li>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1603').""; ?>">Maret 2016</a></li>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1604').""; ?>">April 2016</a></li>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1605').""; ?>">Mei 2016</a></li>
			                        	<li><a href="<?php echo "".site_url('promo/collagen/1606').""; ?>">Juni 2016</a></li>
			                        </ul>
		                        </li>
		                        -->
		                        
		                        <!--<li><a href="#">Promo Calendar</a>
			                          <ul><li><a href="<?php //echo "".site_url('promo/calendar').""; ?>">Claim Calendar</a></li></ul>
		                        </li>-->
		                    </ul>
                        </li>
    <!--                    
                        <li><a href="#">Backpack</a>
                          <ul>
                            <li>
                                <a href="#" onclick="Shopping.download_file('FORM_KLAIMBACKPAK.doc')" >Form Backpack</a> 
                            </li>
                            <li><a href="#">List Backpack</a></li>
                          </ul>
                        </li>
   -->                   
                        <li><a href="#">Download</a>
                        	<ul>
   <!--                        	<li>
	                        		<a href="#" onclick="Shopping.download_file('FORM_KLAIMBACKPAK.doc')" >Form Backpack</a>
                        		</li>
   -->
                        		<li>
	                        		<a href="#" onclick="Shopping.download_file('SADAForm.jpg')" >Form SADA</a>
                        		</li>
                        	</ul>
                        </li>
                     <!--
                        <li><a href="#">Promo Calendar</a>
                          <ul>
                            
                            <li><a href="<?php echo "".site_url('promo/calendar').""; ?>">Claim Calendar</a></li>
                          </ul>
                        </li>
                     -->
                    </ul>
                    </form>
                    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/nav.js"></script>
                </div>
                <!-- end h_menu4 -->
              
                <!-- cart info 
                <div class="shopping_cart pull-right">
                    <ul class="shopping_cart_top">
                        <a href="#">
                           <li class="shop_left">Your cart</li>
                           <li class="shop_right"><img src="<?php echo base_url() ?>assets/images/arrow1.png" alt=""/></li>
                           <div class="clearfix"></div>	
                        </a>
                    </ul>
                    <ul class="shopping_bag">
                        <a href="<?php echo site_url('cart/list');?>">
                           <li class="bag_left"><img src="<?php echo base_url() ?>assets/images/bag.png" alt=""/></li>
                           <li class="bag_right" id="showInfoSumCart">
						   <?php 
						   if($this->cart->total_items() != 0) { 
						     echo $this->cart->total_west_price();
						   } else { 
						     echo "0";
						   }
						   ?> Items | 
						   <?php 
						   if($this->cart->total_west_price() != 0) { 
						     echo "Rp. ".number_format($this->cart->total_west_price(),0,".",".");
						   } else { 
						     echo "Rp. 0";
						   }
						   ?></li>
                           <div class="clearfix"></div>	
                        </a>
                    </ul>		
                </div>
                <div class="clearfix"></div>
                 cart info -->
            </div>
            <!-- Right Menu--> 
        </div>
	</div>
    <!-- bottom-header -->
</div>
<!--Header-->

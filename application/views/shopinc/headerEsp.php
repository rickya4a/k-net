<?php
$usr = getUsername();
$usrid = getUserID();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>K-Link Digital Store</title>
    <meta name="theme-color" content="#dd7b33">
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo.png"  />
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Custom Theme files -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url() ?>assets/css/yp-ui.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'>
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/normalize.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <!--<link href='https://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'/>-->
<!--    <!-- Select2 -->
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>-->


		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>
	
	<script src="<?php echo base_url() ?>assets/js/netfunnel/netfunnel.js?20190405"></script>
	<script src="<?php echo base_url() ?>assets/js/netfunnel/skin/klink_skin_square.js?20190405"></script>
	<script src="<?php echo base_url() ?>assets/js/netfunnel/skin/klink_skin_circle.js?20190405"></script>

    <script src="<?php echo base_url() ?>asset/js_module/global.js"></script>
    <script src="<?php echo base_url() ?>assets/module_js/shopping.js"></script>
    <script src="<?php echo base_url() ?>assets/module_js/shopping_cust.js"></script>
    <script src="<?php echo base_url() ?>assets/module_js/sales.js"></script>
    <script src="<?php echo base_url() ?>assets/module_js/umroh.js"></script>
    <script src="<?php echo base_url() ?>assets/module_js/promo.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>

    <!--data tables (cahyono)-->
    <!-- Datatables -->
    <link href="<?php echo site_url();?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <script src="<?php echo site_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <link href="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <script src="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- payment-->
    <!--<script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery-1.4.3.min.js"></script> -->
    <!--<script type="text/javascript" src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.js"></script>-->
    <script src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/js/' ?>jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/' ?>jquery.fancybox-1.3.4.css"/>
    <!--Google Analytic-->
    <?php echo link_js_sgo("prod"); ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77205058-1', 'auto');
        ga('send', 'pageview');

    </script>

    <script type="text/javascript" src="https://secure.sgo.co.id/public/signature/js"></script>

    <!--slider-->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.flexisel.js"></script>

    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/customs.js"></script>
    <style type="text/css">
        .modal-bg{
            position:fixed;
            left:0;right:0;
            top:0;bottom:0;
            z-index:99999999;
            background-color:rgba(50,50,50,0.7);
        }

        .tombol{
            padding: 8px 12px;
            margin-top: 15px;
            background: #5DC3A7;
            border: none;
            color: #fff;
            border-bottom: 3px solid #23AA84;
            border-radius: 3px;
            cursor: pointer;
        }
        .tombol:active{
            margin: 18px 10px;
        }
        .tombol:hover{
            background-color: #0FC491;
        }
        #modal{
            position:absolute;
            background-color:#F5F5F6;
            top:50%;
            left:50%;
            z-index:101;
            border-radius:3px;
            width:340px;
            height:260px;
            margin-top:-130px;
            margin-left:-170px;
            border-bottom: 3px solid #5DC3A7;
            box-shadow:0 0 10px 0 rgba(0,0,0,0.3);
        }

        #modal span{
            display: block;
            background:#5DC3A7;
            padding: 10px;
            color:#fff;
            border-radius:3px;
        }
        #close{
            float: right;
            color: #fff;
            font-family: serif;
            font-size: 30px;
        }
        #close:hover{
            color: #000;
        }
    </style>
    
    
    		
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		 	fbq('init', '279471709537754'); 
			fbq('track', 'PageView');
		</script>
		<noscript>
		 <img height="1" width="1"
			src="https://www.facebook.com/tr?id=279471709537754&ev=PageView
			&noscript=1"/>
		</noscript>
		
		<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '423327018470580');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=423327018470580&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
		
		
		<!-- End Facebook Pixel Code -->
<!-- Google Tag Manager NANDANG 2018-09-12 -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K899XNL');</script>
<!-- End Google Tag Manager -->

<!-- For NetFUNNEL API kimkm in AimToG co.ltd 2019-04-03 -->
<script type="text/javascript">
	function onClickCart() {
		var uriPath = "<?php echo site_url('cart/list');?>";
		NetFunnel_Action({
			service_id: 'service_1',
			action_id: 'a_cart'
		}, uriPath);
	}
</script>
<!-- End NetFUNNEL API -->
    
</head>
<body>
<!-- Google Tag Manager (noscript) NANDANG 2018-09-12 -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K899XNL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--
<div class="modal-bg">
    <div id="modal">
        <span>Halo Mitra K-Link<a href="#close" id="close">&#215;</a></span>
        <div style="display:block;    margin:15px;    text-align: center;">
            <p style="margin-bottom:30px; font-size: 13px;">
                Dalam rangka upaya meningkatkan pelayanan kepada para Mitra K-Link,
                kami mengadakan survey mengenai Aniversary K-Link ke 15, tanggal 12 Nov 2017 di Sentul Intl Convention Center
                silahkan mengisi survey dengan klik tombol dibawah
            </p>

            <a href="https://goo.gl/forms/zGxQuNc94qXIScXW2" target="_blank" class="tombol">Isi Survey 2017 Klik Disini</a>
        </div>
    </div>
</div >
-->
<!--Header-->
<div class="header">
    <!-- top-header -->
    <div class="header_top">
        <!-- top-container -->
        <div class="container">
            <div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : customer_service@k-link.co.id</p>
            </div>
            <!-- top-left -->
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-5 pull-right">
                <!-- Checkout -->
                <ul class="header_check">
                    <?php
                    if($usr != NULL) {
                    ?>
                    <!--
						<a class="login" href="<?php echo site_url('cart/list');?>">
					-->
					
					<a class="login" onclick="onClickCart();">
                        <?php
                        } else {

                        ?>
                        <a class="login" href="<?php echo site_url('shop/cart/list');?>">
                            <?php
                            }
                            ?>
                            <i class="cart"></i>
                            <li class="user_desc" id="showInfoSumCart">
                                <?php
                                if($this->cart->total_items() != 0) {
                                    echo $this->cart->total_items();

                                } else {
                                    echo "0";
                                }
                                ?>
                                Items |
                                <?php
                                $pricecode = $this->session->userdata('pricecode');
                                if($pricecode == "12W3" || $pricecode == "12W4" ) {
                                    if($this->cart->total_west_price() != 0) {
                                        echo "Rp. ".number_format($this->cart->total_west_price(),0,".",".");
                                    } else {
                                        echo "Rp. 0";
                                    }
                                } else {
                                    if($this->cart->total_east_price() != 0) {
                                        echo "Rp. ".number_format($this->cart->total_east_price(),0,".",".");
                                    } else {
                                        echo "Rp. 0";
                                    }
                                }


                                ?>
                            </li>
                        </a>
                </ul>
                <!-- Checkout -->
                <!-- My Account-->
                <ul class="header_user_info">
                    <?php

                    //echo getUsername();
                    if($usr != NULL) {
                        echo "<i class=\"user\"></i>";
                        echo "<li class=\"user_desc\">";
                        echo "<a href=\"".site_url('tracking')."\"> Hi, ".$usr." | </a>";
                ?><a href="<?php echo "".site_url('logout')."";?>">Logout</a></li>
                <?php
                }else{
                    echo "<i class=\"user\"></i>"; 
                    echo "<li class=\"user_desc\"> <a href=\"".site_url('loginmember')."\" class=\"user\">Hi User, Please Login</a></li>";}
                    ?>
                    <div class="clearfix"> </div>
                </ul>
                <!-- My Account-->
                <!-- start search
                <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your keyword" type="search" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value=""/>
                            <span class="sb-icon-search"> </span>
                        </form>
                    </div>
                </div>
                <!-- search-scripts 
                <script src="<?php echo base_url() ?>assets/js/classie.js"></script>
                <script src="<?php echo base_url() ?>assets/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>
                <!----//search-scripts---->
                <div class="clearfix"></div>
            </div>
            <!-- top-left -->
            <div class="clearfix"> </div>
        </div>
        <!-- top-container -->
    </div>
    <!-- top-header -->

    <!-- bottom-header -->
    <div class="header_bottom">
        <div class="container">
            <!-- Logo -->
            <div class="logo">
                <a href="<?php echo "".site_url('shop/product');?>"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""/></a>
            </div>
            <!-- Logo -->

            <!-- Right Menu-->
            <div class="header_bottom_right">
                <!-- start h_menu4 -->
                <div class="h_menu4">
                    <a class="toggleMenu" href="#">Menu</a>
                    <form>
                        <ul class="nav">
                            <li class="active"><a href="<?php echo "".site_url('shop/product');?>">Beranda</a></li>
                            <!--<li><a href="products.html">About us</a></li>
                        <li><a href="<?php echo "".site_url('static/how-to-order')."";?>">How To Order</a>-->
                            <li><a href="#">Produk </a>
                                <ul>
                                    <?php
                                    $n=1;
                                    foreach($prodCat as $row)
                                    {
                                        $prdcat = $row->cat_id;
                                        echo "<li>";
                                        echo "
                                                <input type=\"hidden\" id=\"prdcat$n\" value=\"".$row->cat_id."\">
                                                <a href = \"".site_url()."/shop/productHeader/cat/id/$prdcat\" >".$row->cat_desc."</a>                           
                                                
                                            ";
                                        echo "</li>";
                                        $n++;
                                    }
                                    ?>
                                </ul>
                            </li>
                            
                           <?php echo menuWebSalesdanBonus(); ?>
                           <?php echo menuWebPendaftaranDist(); ?>
                           <?php echo menuWeblandingPageRecruit(); ?>
                           <?php echo menuWebPerjalananRohani(); ?>
                           <?php echo menuWebPromoInfo(); ?>
                           <?php echo menuWebDownload(); ?>
                           <?php echo menuWebPpobXL(); ?>
                           <?php //echo menuWebPromoCalendar(); ?> 
                        </ul>
                    </form>
                    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/nav.js"></script>
                </div>
                
            </div>
            <!-- Right Menu-->
        </div>
    </div>
    <!-- bottom-header -->
</div>
<!--Header-->

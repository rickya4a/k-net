<!--footer-->
<div class="footer_top">
	<div class="container">
    	<div class="col-sm-2 grid-3">
       		<h3>Tentang K-Link</h3>
       		<ul class="footer_list">
       		<li><a href="<?php echo site_url('static/about'); ?>">Tentang Kami</a></li>
                <li><a href="<?php echo site_url('static/why');?>">Kenapa Harus K-Link</a></li>
       		<li><a href="<?php echo "".site_url('static/shipping_rate')."";?>">Biaya Kirim</a></li>
       		<li><a href="<?php echo "".site_url('static/replacement')."";?>">Penggantian</a></li>
               	<li><a href="<?php echo site_url('static/delivery');?>">Pengiriman</a></li>
       		</ul>
       	</div>
       	<div class="col-sm-2 grid-3">
       		<h3>Bantuan Untuk Anda</h3>
       		<ul class="footer_list">
			<li><a href="<?php echo site_url('static/how-to-order'); ?>"">Cara Berbelanja</a></li>
       			<li><a href="<?php echo site_url('cart/list'); ?>"">Keranjang Belanja</a></li>
       			<li><a href="<?php echo site_url('myaccount'); ?>">Akun Saya</a></li>
       		</ul>
       	</div>
        
       	<div class="col-sm-2 grid-3">
       		<h3>Link Bantuan</h3>
       		<ul class="footer_list">
       			<li><a href="http://www.k-link.co.id/news/det/73/Harga-K-Link-terbaru-Per-Juli-2014">Price List</a></li>                	
       			<li><a href="http://www.k-link.co.id/store">Audio Store</a></li>
			<li><a href="#">Contact Us</a></li>
       		</ul>
       	</div>		
       	<div class="col-sm-3 grid-3">
                  <h3>Subscribe</h3>
                  <p style="font-size:13px; margin-bottom:10px;">Masukkan email Anda untuk update info seputar produk K-Link</p>
                  <div class="register-top-grid">
                    <form action="//k-link.us9.list-manage.com/subscribe/post?u=47314fe9f13048a3b7ae50701&amp;id=98e8200075" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" target="_blank" novalidate>                        
                    <div style="width:100%">
                        <input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="email address" style="width:100%;" required >
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    </div>
                    <div style="width:100%; margin:0px;">                        
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn1 btn2 btn-primary1">
                        <div style="position: absolute; left: -5000px;"><input type="text" name="b_47314fe9f13048a3b7ae50701_98e8200075" class="form-control" tabindex="-1" value=""></div>
                    </div>
                </form>
                        
                  </div>
            </div>
            <div class="col-sm-3 grid-3">
       		<h3>Pembayaran</h3>
       		<img class="img-responsive" src="<?php echo base_url() ?>assets/images/secure.png" alt=""/>
       	</div>
	</div>
</div>

<div class="footer_bottom">
	<div class="container">
		<div class="cssmenu">
			<ul>
				<li class="active"><a href="<?php echo "".site_url('shop/product');?>">Home</a></li> |
				<li><a href="<?php echo site_url('static/about'); ?>">About K-Link</a></li> |
				<li><a href="<?php echo "".site_url('static/how-to-order')."";?>">How To Order</a></li> 
			</ul>
		</div>
		<div class="copy">
			<p>Copyright &copy; 2014 ~ 2015 - PT K-Link Indonesia - MLM Bersistem Syariah</p>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery.lazyload.js"></script>
 <script type="text/javascript" charset="utf-8">
  $(function() {
     $("img.lazy").lazyload({
         effect : "fadeIn",
	threshold : 200
     });
    $('.uppercase').bind('change', function() {
	  $(this).val($(this).val().toUpperCase().replace("'","`"));
	});
  });
  </script>
</body>
</html>		

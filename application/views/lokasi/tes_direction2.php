<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Directions Service</title>
    <script type="text/javascript" language="Javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            /*height: 100%;*/
            width: 30%;
            height: 40%;
            left: 18%;
            top: 20px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #locationField {
            top: 10px;
            position: relative;
            padding-left: 18%;

        }
        #autocomplete {
            position: relative;
            top: 0px;
            left: 0px;
            width: 500px;
        }

        #address td {
            font-size: 10pt;
        }

        #priceField {
            top: 10px;
            position: relative;
            padding-left: 18%;
        }
    </style>
</head>
<body>
<!--<div id="floating-panel"></div>-->
<div id="locationField">
    <b>Pickup: </b>
    <input type="text" id="start" value="K-link Tower" readonly>
    <!-- <input id="end" placeholder="Enter your address" type="text"></input>-->

    <b>Destination: </b>
    <input id="autocomplete" placeholder="Enter your address" type="text"></>
</div>

<div id="map"></div>

<div id="priceField">
    <p style="color: red">*The distance limitation 40 km</p>
    <p id="instant">The distance 0 km, CASH GO-SEND Instant:</p>
    <p id="sameday">The distance 0 km, CASH GO-SEND Sameday:</p>
</div>

<div id="priceField">
    <b>Referensi Stokist:</b>
    <select id="stokist">
        <option value="#">- Pilih Stokist - </option>
    </select>
</div>
<script>
    function initMap() {

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var auto_complete;

        //option alamat autocomplete
        auto_complete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */
            (document.getElementById('autocomplete')),
            {types: ['geocode']});

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: {lat: -6.2400257, lng: 106.8320919}
        });
        directionsDisplay.setMap(map);

        var onChangeHandler = function () {

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };

        document.getElementById('start').addEventListener('change', onChangeHandler);
        document.getElementById('autocomplete').addEventListener('change', onChangeHandler);

    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
            origin: document.getElementById('start').value,
            destination: document.getElementById('autocomplete').value,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);

                var a= document.getElementById('start').value;
                var b= document.getElementById('autocomplete').value;
                cek_jarak(a,b);

            } else {
                window.alert('Lokasi Tidak Ditemukan ' + status);

            }
        });
    }


    function cek_jarak(A,B){
        var origin = A,
            destination = B,
            service = new google.maps.DistanceMatrixService();

        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                avoidHighways: false,
                avoidTolls: false
            },
            callback
        );

        function callback(response, status) {
            if(status=="OK") {
                var str=response.rows[0].elements[0].distance.text;
                var res = str.replace("km", "");

                var dist= res * 1;

                var rp_instant= 2500;
                var rp_sameday;


                //perhitungan sameday
                if(0 < dist && dist < 15.1){
                    rp_sameday= 15000;
                }

                if(15.1 < dist && dist < 30.1){
                    rp_sameday= 20000;
                }

                if(30.1 < dist && dist < 40.1){
                    rp_sameday= 25000;
                }

                var instant= rp_instant * dist;
                var sameday= rp_sameday * 1;

                //Start format Rupiah
                var	number_string1 = instant.toString(),
                    sisa1	= number_string1.length % 3,
                    rupiah1 = number_string1.substr(0, sisa1),
                    ribuan1 = number_string1.substr(sisa1).match(/\d{3}/g);

                if (ribuan1) {
                    separator1 = sisa1 ? '.' : '';
                    rupiah1 += separator1 + ribuan1.join('.');
                }

                var	number_string2 = sameday.toString(),
                    sisa2 	= number_string2.length % 3,
                    rupiah2	= number_string2.substr(0, sisa2),
                    ribuan2	= number_string2.substr(sisa2).match(/\d{3}/g);

                if (ribuan2) {
                    separator2 = sisa2 ? '.' : '';
                    rupiah2 += separator2 + ribuan2.join('.');
                }
                //End format rupiah


                if(dist > 40 ){
                    //alert("The distance "+ dist+" km. Oopps, couldn't load the price!");
                    document.getElementById("instant").innerHTML = "The distance "+dist+" km, CASH GO-SEND Instant: Oopps, couldn't load the price!";
                    document.getElementById("sameday").innerHTML = "The distance "+dist+" km, CASH GO-SEND Sameday: Oopps, couldn't load the price!";
                }

                else{

                    //cek jam order
                    var today = new Date().getHours();

                    console.log(today);

                    if (today < 6  && today > 18) {
                        //console.log("Instant");
                        //alert("Jarak pengiriman "+ dist+" km. oops, couldn't load the price!");
                        document.getElementById("instant").innerHTML = "The distance "+dist+" km, CASH GO-SEND Instant: Closed (Order time: 6AM - 6PM)";
                        document.getElementById("sameday").innerHTML = "The distance "+dist+" km, CASH GO-SEND Sameday: Closed (Order time: 8AM - 3PM)";

                    }

                    if (today >= 8 && today <= 15) {
                        //console.log("Sameday");
                        //alert("Jarak pengiriman "+ dist+" km, NEXT!");
                        document.getElementById("instant").innerHTML = "The distance "+dist+" km, CASH GO-SEND Instant: Rp."+rupiah1;
                        document.getElementById("sameday").innerHTML = "The distance "+dist+" km, CASH GO-SEND Sameday: Rp."+rupiah2;

                    }

                    if ((today >= 6 && today < 8) || (today > 15 && today <= 18)) {
                        //console.log("Instant");
                        //alert("Jarak pengiriman "+ dist+" km, NEXT!");
                        document.getElementById("instant").innerHTML = "The distance "+dist+" km, CASH GO-SEND Instant: Rp."+rupiah1;
                        document.getElementById("sameday").innerHTML = "The distance "+dist+" km, CASH GO-SEND Sameday: Closed (Order time: 8AM - 3PM)";

                    }



                }

            } else {
                alert("Error: " + status);

                //location.reload();
            }
        }

        geocodeAddress1(); //call fungsi mendapatkan koordinat lokasi awal
    }

    function geocodeAddress1() {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('autocomplete').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                var Xa = results[0].geometry.location.lat();
                var Ya = results[0].geometry.location.lng();


                geocodeAddress2(Xa,Ya); //call fungsi mendapatkan koordinat lokasi akhir

            } else {
                alert('Lokasi Awal was not successful for the following reason: ' + status);
            }
        });
    }

    function geocodeAddress2(X1,Y1) {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('autocomplete').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {

                var X2 = results[0].geometry.location.lat();
                var Y2 = results[0].geometry.location.lng();

                console.log("koordinat Start:" + X1 +","+Y1);
                console.log("koordinat End:" + X2 +","+Y2);

                //gojek(X1,Y1,X2,Y2) //call fungsi data gojek

            } else {
                alert('Lokasi Akhir was not successful for the following reason: ' + status);
            }
        });
    }

    function gojek(x1,y1,x2,y2){
        var awal= x1+","+y1;
        var akhir= x2+","+y2;

        //console.log(akhir);
        $.ajax({
            url:"https://integration-kilat-api.gojekapi.com/gojek/v3/calculate/price?origin="+awal+"8&destination="+akhir+"&paymentType=3",
            type: "GET",
            crossDomain:true,
            dataType:"json",
            success: function (data) {
                console.log(data);
            }
        });
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU&libraries=places&callback=initMap">
</script>
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU&libraries=places&callback=initService"
        async defer></script>-->
</body>
</html>
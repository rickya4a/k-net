<!DOCTYPE HTML>
<html>
<head>
    <title>Stokist Location</title>
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Custom Theme files -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" >
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>
    <script src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>

    <style>
        .loader {
            border: 8px solid #f3f3f3;
            border-radius: 50%;
            border-top: 8px solid #3498db;
            width: 40px;
            height:40px;
            -webkit-animation: spin 2s linear infinite;
            /* Safari */
            animation: spin 2s linear infinite;
            margin-left: 220px;
        }
        .judul {padding: 50px;}
        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% { -webkit-transform: rotate(360deg);
            }
        }@keyframes spin {
             0% {
                 transform: rotate(0deg);
             }
             100% {
                 transform: rotate(360deg);
             }
         }
    </style>

    <script type="text/javascript">
        function showLocation(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            var lokasi = {"lat" : latitude,"long" : longitude};
            var hasil= JSON.stringify(lokasi);
            //console.log("Vera1: "+hasil);
            var vars = {};
            /*var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
             function(m,key,value) {vars[key] = value;
             console.log(vars["id"]);
             console.log(vars["page"]);
             });*/

       
            var page='<?php echo $page?>';
            document.post_coord.lat.value = latitude;
            document.post_coord.long.value = longitude;
            document.post_coord.loccd.value = page;

            //window.location='https://www.k-net.co.id/otp_location/';
            myFunction();
            /*if(id == 3 ){
                //alert("Success 3!");
                //window.location='http://www.k-linkmember.co.id/transaction/lokasi/addms/'+latitude+'/'+longitude;
                window.location='https://www.k-net.co.id/otp_location/'+latitude+'/'+longitude+'/'+page;
            }else {

                //alert("Success < 3!");
                //window.location='http://www.k-linkmember.co.id/transaction/lokasi/db/'+page+'/'+latitude+'/'+longitude;
                window.location='https://www.k-net.co.id/otp_location/'+latitude+'/'+longitude+'/'+page;
            }*/
            return vars;
        }

        function errorHandler(err) {
            if(err.code == 1) {
//alert("Error: Access is denied!");
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                    function(m,key,value) {
                        vars[key] = value;
                        console.log(vars["id"]);
                        console.log(vars["page"]);
                    });
                var id=vars["id"];
                var page=vars["page"];
                if(id == 3 ){
                    alert("Error: Access is denied!");
                    window.location='http://www.k-linkmember.co.id/transaction/lokasi/addms//0/0';
                }else {
//window.location='http://www.k-linkmember.co.id/transaction/lokasi/add/0/0';
                    alert("Error: Access is denied. Kembali Ke Menu Sebelumnya!");
                    window.history.go(-2);
                }
                return vars;
            }

            else if( err.code == 2) {
//alert("Error: Position is unavailable!");
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                    function(m,key,value) {vars[key] = value;console.log(vars["id"]);
                        console.log(vars["page"]);
                    });
                var id=vars["id"];var page=vars["page"];
                if(id == 3 ){
                    alert("Error: Position is unavailable!");
                    window.location='http://www.k-linkmember.co.id/transaction/lokasi/addms/0/0';
                }else {
//window.location='http://www.k-linkmember.co.id/transaction/lokasi/add/0/0';
                    alert("Error: Position is unavailable. Kembali Ke Menu Sebelumnya!");
                    window.history.go(-2);
                }
                return vars;
            }
        }

        function getLocation(){
            if(navigator.geolocation){
                var options = {timeout:60000};
                navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);
            }else{
//alert("Sorry, browser does not support geolocation!");
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                    function(m,key,value) {
                        vars[key] = value;
                        console.log(vars["id"]);
                        console.log(vars["page"]);
                    });
                var id=vars["id"];
                var page=vars["page"];
                if(id == 3 ){
                    alert("Sorry, browser does not support geolocation!");
                    window.location='http://www.k-linkmember.co.id/transaction/lokasi/addms/0/0';
                }
                else {
                    alert("Sorry, browser does not support geolocation!");
                    window.history.go(-2);
//window.location='http://www.k-linkmember.co.id/transaction/lokasi/add/0/0';
                }return vars;
            }
        }

        function myFunction() {
            document.getElementById("post_coord").submit();
        }
    </script>
</head>

<body onLoad="getLocation()">

<!--Header-->
<div class="header">
    <!-- top-header -->
    <div class="header_top">
        <!-- top-container -->
        <div class="container">
            <!--<div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : customer_service[at]k-link.co.id</p>
            </div>-->
            <!-- top-left -->
            <div class="col-lg-4 col-md-6 pull-right">
                <!-- Checkout -->

                <!-- My Account-->
                <!-- start search-->
                <!-- <div class="search-box">
                     <div id="sb-search" class="sb-search">
                         <form>
                             <input class="sb-search-input" placeholder="Enter your keyword" type="search" name="search" id="search">
                             <input class="sb-search-submit" type="submit" value=""/>
                             <span class="sb-icon-search"> </span>
                         </form>
                     </div>
                 </div>-->
                <!----search-scripts---->
                <!--<script src="<?php /*echo base_url() */?>assets/js/classie.js"></script>
                <script src="<?php /*echo base_url() */?>assets/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>-->
                <!----//search-scripts---->
                <div class="clearfix"></div>
            </div>
            <!-- top-left -->
            <div class="clearfix"> </div>
        </div>
        <!-- top-container -->
    </div>
    <!-- top-header -->

    <!-- bottom-header -->
    <div class="header_bottom">
        <div class="container">
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""/></a>
            </div>
            <!-- Logo -->

            <!-- Right Menu-->
            <div class="header_bottom_right">


                <!-- end h_menu4 -->

                <!-- cart info
                <div class="shopping_cart pull-right">
                    <ul class="shopping_cart_top">
                        <a href="#">
                           <li class="shop_left">Your cart</li>
                           <li class="shop_right"><img src="<?php echo base_url() ?>assets/images/arrow1.png" alt=""/></li>
                           <div class="clearfix"></div>
                        </a>
                    </ul>
                    <ul class="shopping_bag">
                        <a href="#">
                           <li class="bag_left"><img src="<?php echo base_url() ?>assets/images/bag.png" alt=""/></li>
                           <li class="bag_right"> 0 Items | $ 0</li>
                           <div class="clearfix"></div>
                        </a>
                    </ul>
                </div>
                <div class="clearfix"></div>-->
                <!-- cart info -->
            </div>
            <!-- Right Menu-->
        </div>
    </div>
    <!-- bottom-header -->
</div>
<!--Header-->



<!-- Breadcrumb -->
<!--<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php /*echo "".base_url('assets/images/home.png')."";*/?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            My Account&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Login&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>-->
<!-- Breadcrumb -->
<!-- Login Form -->
<div class="contact">
    <div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="account_grid">
                <div class="loader"></div>
                <br>
                <h2 align="center">Mencari Titik Lokasi</h2>
                <!--<div class="col-md-12 login-right"></div>-->
                <form id="post_coord" name="post_coord" method="post" action="<?php echo base_url();?>otp_location">
                    <div>
                        <input type="hidden" name="lat" />
                        <input type="hidden" name="long" />
                        <input type="hidden" name="loccd" />
                    </div>
                </form>
            </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
</body>
<!-- Login Form -->

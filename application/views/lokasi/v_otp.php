<!DOCTYPE HTML>
<html>
<head>
    <title>Stokist Location</title>
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Custom Theme files -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" >
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>
    <script src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
</head>
<script>
    $(document).ready(function() {
        var detik = 3;
        var menit = 5;
        document.getElementById("submit").value = "Verifikasi";
        document.getElementById("myForm").action = "<?php echo base_url();?>C_location/otp_proses/<?php echo $lat?>/<?php echo $long?>/<?php echo $page?>";

        function hitung() {
            setTimeout(hitung,1000);
            $('#tampilkan').html( ' habis waktu ' + menit + ' menit ' + detik + ' detik ');
            detik --;
            if(detik < 0) {
                detik = 59;
                menit --;
                if(menit < 0) {
                    menit = 0;
                    detik = 0;
                    document.getElementById("submit").value = "Kirim Ulang OTP";
                    document.getElementById("myForm").action = "<?php echo base_url();?>find_location/<?php echo $page?>";
                }
            }
        }
        hitung();
    });
</script>
<body>

<!--Header-->
<div class="header">
    <!-- top-header -->
    <div class="header_top">
        <!-- top-container -->
        <div class="container">
            <!--<div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : customer_service[at]k-link.co.id</p>
            </div>-->
            <!-- top-left -->
            <div class="col-lg-4 col-md-6 pull-right">
                <!-- Checkout -->

                <!-- My Account-->
                <!-- start search-->
               <!-- <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your keyword" type="search" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value=""/>
                            <span class="sb-icon-search"> </span>
                        </form>
                    </div>
                </div>-->
                <!----search-scripts---->
                <!--<script src="<?php /*echo base_url() */?>assets/js/classie.js"></script>
                <script src="<?php /*echo base_url() */?>assets/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>-->
                <!----//search-scripts---->
                <div class="clearfix"></div>
            </div>
            <!-- top-left -->
            <div class="clearfix"> </div>
        </div>
        <!-- top-container -->
    </div>
    <!-- top-header -->

    <!-- bottom-header -->
    <div class="header_bottom">
        <div class="container">
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""/></a>
            </div>
            <!-- Logo -->

            <!-- Right Menu-->
            <div class="header_bottom_right">


                <!-- end h_menu4 -->

                <!-- cart info
                <div class="shopping_cart pull-right">
                    <ul class="shopping_cart_top">
                        <a href="#">
                           <li class="shop_left">Your cart</li>
                           <li class="shop_right"><img src="<?php echo base_url() ?>assets/images/arrow1.png" alt=""/></li>
                           <div class="clearfix"></div>
                        </a>
                    </ul>
                    <ul class="shopping_bag">
                        <a href="#">
                           <li class="bag_left"><img src="<?php echo base_url() ?>assets/images/bag.png" alt=""/></li>
                           <li class="bag_right"> 0 Items | $ 0</li>
                           <div class="clearfix"></div>
                        </a>
                    </ul>
                </div>
                <div class="clearfix"></div>-->
                <!-- cart info -->
            </div>
            <!-- Right Menu-->
        </div>
    </div>
    <!-- bottom-header -->
</div>
<!--Header-->



<!-- Breadcrumb -->
<!--<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php /*echo "".base_url('assets/images/home.png')."";*/?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            My Account&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Login&nbsp;&nbsp;</span>
        </li>
    </div>
</ul>-->
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
    <div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="account_grid">
                <div class="col-md-12 login-right">
                    <span class="title_header">
                        <i class="fa fa-sign-in"></i> Verifikasi OTP<br />
                    </span>
                    <p style="font-size: 14px"><br/>Silahkan anda masukkan kode OTP yang diterima lewat SMS sebelumnya.</p>
                    <div id='tampilkan' style="font-size: 14px;color: red;"></div>
                    <form id="myForm" method="post" action="">
                        <div>
                            <span>Masukkan kode OTP:<label style="color: red">*</label></span>
                            <input type="text" name="otp" class="uppercase" />
                        </div>
                        <input type="submit" id="submit" value=""/>
                        <!--<a class="forgot" href="#" >LUPA Password?</a><br /><br />-->
                        <font color="red">
                            <?php if(isset($error))
                            {echo $error ;}?></font>
                    </form>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
<!-- Login Form -->

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Directions Service</title>
    <script type="text/javascript" language="Javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

    <title>Rute Ke Stockist</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            /*height: 100%;*/
            width: 30%;
            height: 50%;
            left: 10%;
            top: 20px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #locationField {
            top: 20px;
            position: relative;
            padding-left: 10%;

        }
        #autocomplete {
            position: relative;
            top: 0px;
            left: 0px;
            width: 700px;
        }

        #start{
            width: 700px;
        }

        #address td {
            font-size: 10pt;
        }

        #priceField {
            top: 10px;
            position: relative;
            padding-left: 10%;
        }
    </style>

    <script>

        function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
        }

        function cek_stokist(){
            $("#start").empty();
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('autocomplete').value;

            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    var cX = results[0].geometry.location.lat();
                    var cY = results[0].geometry.location.lng();

                    /*console.log("koordinat Pengiriman:" + cX +","+cY);
                     price_send(-6.190262199999999,106.7912918,-6.2400341,106.83439120000003);
                     tes_api2();*/

                    option_stokist(cX,cY);
                } else {
                    alert('Lokasi Awal was not successful for the following reason: ' + status);
                }
            });
        }

        function option_stokist(cX,cY){
            $.ajax({
                url:"https://www.k-net.co.id/cek_stockist",
                type: "POST",
                dataType:"json",
                data:{
                    lat:cX,
                    long:cY
                },
                success: function (result) {
                    if (result.response == "true") {

                        var R= result.jarak;

                        var x = document.getElementById("start");
                        var option = document.createElement("option");
                        option.text = "- Pilih Stokist";
                        x.add(option, x[0]);

                        $.each(R, function(k, v) {
                            var s= v.split("+");
                            var x = document.getElementById("start");
                            var option = document.createElement("option");

                            option.text = s[1]+ " (" +s[4].toLowerCase()+")" ;
                            x.add(option);
                        });
                    }
                    else {
                        alert(result.error);

                    }
                }
            });
        }

        function initMap() {

            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var auto_complete;

            //option alamat autocomplete
            auto_complete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */
                (document.getElementById('autocomplete')),
                {types: ['geocode']});

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: {lat: -6.2400257, lng: 106.8320919}
            });
            directionsDisplay.setMap(map);

            var onChangeHandler = function () {

                calculateAndDisplayRoute(directionsService, directionsDisplay);
            };

            document.getElementById('start').addEventListener('change', onChangeHandler);
            document.getElementById('autocomplete').addEventListener('change', onChangeHandler);

        }

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            directionsService.route({
                origin: document.getElementById('start').value,
                destination: document.getElementById('autocomplete').value,
                travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);

                    var a= document.getElementById('start').value;
                    var b= document.getElementById('autocomplete').value;

                    cek_jarak(a,b);

                }

            });
        }

        function cek_jarak(A,B){
            var origin = A,
                destination = B,
                service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix(
                {
                    origins: [origin],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING,
                    avoidHighways: false,
                    avoidTolls: false
                },
                callback
            );

            function callback(response, status) {
                if(status=="OK") {
                    var str=response.rows[0].elements[0].distance.text;
                    var res = str.replace("km", "");
                    geocodeAddress1(res); //call fungsi mendapatkan koordinat
                } else {
                    alert("Error: " + status);
                    //location.reload();
                }
            }

        }

        function geocodeAddress1(dist) {
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('autocomplete').value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    var Xa = results[0].geometry.location.lat();
                    var Ya = results[0].geometry.location.lng();

                    geocodeAddress2(dist,Xa,Ya); //call fungsi mendapatkan koordinat lokasi akhir

                } else {
                    alert('Lokasi Awal was not successful for the following reason: ' + status);
                }
            });
        }

        function geocodeAddress2(dist,X1,Y1) {
            $("#metode").empty();
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('start').value;
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {

                    var X2 = results[0].geometry.location.lat();
                    var Y2 = results[0].geometry.location.lng();

                    if(dist < 40){
                        price_send(X1,Y1,X2,Y2) //call fungsi data gojek
                        //tes_api();
                    }else{
                        var x = document.getElementById("metode");
                        var option = document.createElement("option");
                        option.text = "- Pilih Metode -";
                        x.add(option, x[0]);

                        var option1 = document.createElement("option");
                        option1.text = "Instant - Jarak > 40km";
                        x.add(option1, x[1]);

                        var option2 = document.createElement("option");
                        option2.text = "Same Day - Jarak > 40km";
                        x.add(option2, x[2]);
                    }

                } else {
                    alert('Lokasi Akhir was not successful for the following reason: ' + status);
                }
            });
        }

        function tes_api(){
            $.ajax({
                url:"https://www.k-net.co.id/api/login",
                type: "POST",
                dataType:"json",
                data:{
                    api_usr:"k-net.apps",
                    api_pwd:"apps@2017",
                    idmember:"IDJHID000065",
                    password:"CALVIN***123"
                },
                success: function (res) {

                    console.log(res);

                    /*var arr_result=res.sicepat.results;
                     $.each(arr_result, function(k, v) {
                     console.log(v.origin_name);
                     var origin= v.origin_name;

                     var x = document.getElementById("metode");
                     var option = document.createElement("option");
                     option.text = origin;
                     x.add(option);
                     });*/
                }
            });
        }

        function tes_api2(){
            $.ajax({
                url:"https://www.k-net.co.id/make_booking",
                type: "POST",
                dataType:"json",
                data:{
                    metode:"Instant"
                },
                success: function (res) {

                    console.log(res);

                    /*var arr_result=res.sicepat.results;
                     $.each(arr_result, function(k, v) {
                     console.log(v.origin_name);
                     var origin= v.origin_name;

                     var x = document.getElementById("metode");
                     var option = document.createElement("option");
                     option.text = origin;
                     x.add(option);
                     });*/
                }
            });
        }

        function price_send(x1,y1,x2,y2){
            var awal= x1+","+y1;
            var akhir= x2+","+y2;

            $.ajax({
                url:"https://www.k-net.co.id/cek_price",
                type: "POST",
                dataType:"json",
                data:{
                    lat:awal,
                    long:akhir
                },
                success: function (result) {

                    //console.log(result);

                    var hours = new Date().getHours();
                    var minute = new Date().getMinutes();
                    var time= hours+":"+minute;


                    var x = document.getElementById("metode");
                    var option = document.createElement("option");
                    option.text = "- Pilih Metode -";
                    x.add(option, x[0]);

                    var service_instant= result.Instant.serviceable;
                    var metode_instant=result.Instant.shipment_method_description;

                    if(service_instant == true) {
                        var dist_instant= result.Instant.distance;
                        var price_instant= result.Instant.price.total_price;

                        var rupiah1= convertToRupiah(price_instant);

                        //cek jam order
                        var x = document.getElementById("metode");
                        var option = document.createElement("option");

                        if (hours >= 8 && hours <= 17) {
                            option.value= "Instant";
                            option.text = "Instant - Rp"+ rupiah1;
                            x.add(option, x[1]);
                        }else{
                            option.value= "#";
                            option.text = "Instant - TUTUP (Jam Pesanan: 6 Pagi - 6 Sore)";
                            x.add(option, x[1]);
                        }

                    } else{
                        option.value= "#";
                        option.text = "Instant - "+metode_instant;
                        x.add(option, x[1]);
                    }


                    var service_sameday= result.SameDay.serviceable;
                    var metode_sameday=result.SameDay.shipment_method_description;

                    if(service_sameday == true) {
                        var dist_sameday= result.SameDay.distance;
                        var price_sameday= result.SameDay.price.total_price;

                        var rupiah1= convertToRupiah(price_sameday);

                        //cek jam order
                        var x = document.getElementById("metode");
                        var option = document.createElement("option");

                        if (hours >= 8 && hours <= 14) {
                            option.value= "SameDay";
                            option.text = "Same Day - Rp"+ rupiah1;
                            x.add(option, x[2]);
                        }else{
                            option.value= "#";
                            option.text = "Same Day - TUTUP (Jam Pesanan: 8 Pagi - 3 Sore)";
                            x.add(option, x[2]);
                        }
                    } else{
                        option.value= "#";
                        option.text = "Same Day "+ metode_sameday;
                        x.add(option, x[2]);
                    }
                }

            });
        }

        function post_book(){
            var cmetode=$(metode).val();
            $.ajax({
                url:"https://www.k-net.co.id/make_booking",
                type: "POST",
                dataType:"json",
                data:{
                    metode:cmetode
                },
                success: function (result) {
                    console.log(result);

                    var orderNo=result.orderNo;
                    document.getElementById("no_order").value=orderNo;

                    status_book();
                }
            });
        }

        function status_book(){
            var cno_order=$(no_order).val();
            $.ajax({
                url:"https://www.k-net.co.id/status_booking",
                type: "POST",
                dataType:"json",
                data:{
                    orderno:cno_order
                },
                success: function (result) {
                    console.log(result);

                    var status=result.status;
                    document.getElementById("status").value=status;
                }
            });
        }
    </script>
</head>
<!--<body onLoad="document.form.value.focus()"> -->
<body>
<div id="locationField">
    <b>Tujuan Pengiriman: </b>
    <input id="autocomplete" placeholder="Enter your address" type="text" onchange="cek_stokist()"></>
</div>
<br>
<div id="locationField">
    <b>Referensi - Stokist : </b>
    <!--<input type="text" id="start" value="K-link Tower" readonly>-->
    <select id="start">
        <option value="#">- Pilih Stokist - </option>
    </select>
</div>
<br>

<div id="map"></div>
<div id="priceField">
    <p style="color: red">*Jarak Maks. 40 km</p>

    <b>Metode Pengiriman: </b>
    <select id="metode" onchange="post_book()">
        <option value="#">- Pilih Metode - </option>
    </select>
</div>
<div id="locationField">
    <b>No. Order: </b>
    <input id="no_order" placeholder="Automaticly" type="text" readonly>

    <b>Status: </b>
    <input id="status" placeholder="Automaticly" type="text" readonly>
</div>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU&libraries=places&callback=initMap">
</script>
</body>
</html>
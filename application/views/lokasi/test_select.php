<html>
<head>
    <meta charset="UTF-8">
    <title>Test Select 2</title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url();?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!--Select2 here-->
    <link href="<?php echo base_url('assets/css/select2.css')?>" rel="stylesheet" type="text/css" />

</head>
<body>

    <div class="col-lg-4">

        <h4>Test Select2</h4>

        <!--    <div class="col-lg-4">-->
        <select name="test" id="test" class="form-control js-example-basic-single" onkeyup="test()">
            <option value="">--Pilih--</option>
        </select>
        <!--    </div>-->
    </div>


</body>
</html>

<!-- jQuery -->
<script src="<?php echo site_url();?>vendors/jquery/dist/jquery.min.js"></script>
<!--Select2-->
<script src="<?php echo base_url('assets/js/select2.min.js')?>"></script>

<script>
    $(document).ready(function(){
        $(".js-example-basic-single").select2();

    });

    function test(){
        console.log('test');
    }
</script>
<!DOCTYPE HTML>
<html>
<head>
<title>K-Link WebStore</title>
<link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo.png"  />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="<?php echo base_url() ?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" >
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.easydropdown.js"></script>
<script src="<?php echo base_url() ?>assets/js/responsiveslides.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/netfunnel/netfunnel.js?20190405"></script>
<script src="<?php echo base_url() ?>assets/js/netfunnel/skin/klink_skin_square.js?20190405"></script>
<script src="<?php echo base_url() ?>assets/js/netfunnel/skin/klink_skin_circle.js?20190405"></script>
<script type="text/javascript">
	// if redirecting at '/loginmember',
	// then NetFunnel_Complete() should be executed.
	NetFunnel_Complete();

	function loginAction() {
		var loginForm = document.querySelector('#_nf_login_form');

		NetFunnel_Action({
			service_id: 'service_1',
			action_id: 'a_login'
		}, function(ev, ret) {
			loginForm.submit();
		});
	}
</script>
</head>
<body>

<!--Header-->
<div class="header">
	<!-- top-header -->
	<div class="header_top">
    	<!-- top-container -->
    	<div class="container">
            <div class="header_top_left col-md-4 pull-left">
                <p class="top-info"><i class="glyphicon glyphicon-earphone"></i> Customer Care : customer_service[at]k-link.co.id</p>
            </div>
            <!-- top-left -->
            <div class="col-lg-4 col-md-6 pull-right">
                <!-- Checkout -->

                <!-- My Account-->
                <!-- start search-->
                <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your keyword" type="search" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value=""/>
                            <span class="sb-icon-search"> </span>
                        </form>
                     </div>
                </div>
                <!----search-scripts---->
                <script src="<?php echo base_url() ?>assets/js/classie.js"></script>
                <script src="<?php echo base_url() ?>assets/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>
                <!----//search-scripts---->
                <div class="clearfix"></div>
            </div>
            <!-- top-left -->
		<div class="clearfix"> </div>
		</div>
    	<!-- top-container -->
	</div>
    <!-- top-header -->

  	<!-- bottom-header -->
  	<div class="header_bottom">
		<div class="container">
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="<?php echo base_url() ?>assets/images/logo.png" alt=""/></a>
            </div>
            <!-- Logo -->

            <!-- Right Menu-->
            <div class="header_bottom_right">


                <!-- end h_menu4 -->

                <!-- cart info
                <div class="shopping_cart pull-right">
                    <ul class="shopping_cart_top">
                        <a href="#">
                           <li class="shop_left">Your cart</li>
                           <li class="shop_right"><img src="<?php echo base_url() ?>assets/images/arrow1.png" alt=""/></li>
                           <div class="clearfix"></div>
                        </a>
                    </ul>
                    <ul class="shopping_bag">
                        <a href="#">
                           <li class="bag_left"><img src="<?php echo base_url() ?>assets/images/bag.png" alt=""/></li>
                           <li class="bag_right"> 0 Items | $ 0</li>
                           <div class="clearfix"></div>
                        </a>
                    </ul>
                </div>
                <div class="clearfix"></div>-->
                <!-- cart info -->
            </div>
            <!-- Right Menu-->
        </div>
	</div>
    <!-- bottom-header -->
</div>
<!--Header-->



<!-- Breadcrumb -->
<ul class="breadcrumbs">
 <div class="container">
     <li class="home">
        <a href="index.html" title="Go to Home Page"><img src="<?php echo "".base_url('assets/images/home.png')."";?>" alt=""/></a>&nbsp;
       &nbsp; <span>&gt;</span>
     </li>
     <li class="home">&nbsp;
         My Account&nbsp;&nbsp;
         <span>&gt;</span>
     </li>
     <li class="home">
         <span class="red"> &nbsp;Login&nbsp;&nbsp;</span>
     </li>
  </div>
</ul>
<!-- Breadcrumb -->

<!-- Login Form -->
<div class="contact">
	<div class="container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      		<div class="account_grid">
	        	<div class="col-md-12 login-right">
                    <span class="title_header">
                        <i class="fa fa-sign-in"></i> Please Login<br />

                    </span>
                    <p><br/>Silahkan anda masuk menggunakan Member ID dan password yang anda miliki.
                        Username dan Password sama dengan website www.k-linkmember.co.id.
                        Jika anda mengalami kesulitan untuk login, silahkan hub cs 021-29027000</p>
                    <form method="post" action="<?php echo "".$formAction."";?>" id="_nf_login_form" onsubmit="loginAction(); return false;">
                        <div>
                            <span>ID Member<label>*</label></span>
                            <input type="text" name="idmember" class="uppercase"/>
                        </div>
                        <div>
                            <span>Password<label>*</label></span>
                            <input type="password" name="password"  />
                        </div>
                        <div>
                        	<span>Masukan kode berikut :<label>*</label></span>
                        	<span><?php echo $captcha_img['image']; ?></span>
                        	 <input type="text" name="captcha" class="uppercase" autocomplete="off" />
                        </div>
                        <input type="submit" value="Login"/>
                        <!--<a class="forgot" href="#" >LUPA Password?</a><br /><br />-->
                        <font color="red">
                          <?php if(isset($error))
                        {echo $error ;}?></font>
                    </form>
			   	</div>
			   	<div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!-- Login Form -->

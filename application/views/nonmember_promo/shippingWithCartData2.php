<style>
	/* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
	#map {
		top: 5px;
		width: 150px;
		height: 300px;
	}

</style>
<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXOu_ML5h0T_U6q5qisVxa6k6AzL9fdRU&libraries=places&callback=initMap">
</script>
<script>
	//Vera
	$( document ).ready(function() {
	    console.log("cartWithData");
		$("#InputAddress").css("display", "none");
		$("#PilihMetode").css("display", "none");
		$("#InputAddress").css("display", "none");
		$("#map").css("display", "none");
		$("#Stockist2").css("display", "none");

		$("#provDiv").css("display", "none");
		$("#KabDiv").css("display", "none");
		$("#KecDiv").css("display", "none");
		$("#StockistDiv").css("display", "none");
	});

	function cekHp(){
		var res = document.getElementById('notlp').value;
		var index= "6";

		var no2 = res.substring(0, 2);
		var no3 = res.substring(0, 3);

		if(no2 == '02' || no2 == '62' || no2 == '08' || no3 == '628'){
			/*console.log(no2);
			 console.log(no3);*/
			return true;
		}else{
			alert('Cek kembali nomor telpon yang dimasukkan!');
			return false;
		}

	}

	function clear_addr(){
		document.getElementById('autocomplete').value="";
		return;
	}

	function listProvinceByCargo(idx) {
		All.set_disable_button();
		var txt = $("#"+idx.id+" :selected").text();
		//$(setToDiv).attr("disabled", "disabled");
		if(idx.value == "") {
			alert("Silahkan pilih Ekspedisi/Cargo dahulu");
		} else {
			$.ajax({
				url: All.get_url("cargo/province/list/") +idx.value,
				type: 'GET',
				dataType: 'json',
				success:
					function(data){
						//$("#provDiv").html(null);
						//$("#kabDiv").html(null);
						//$("#kecDiv").html(null);
						All.set_enable_button();
						if(data.response == "true") {
							var arrayData = data.arrayData;

							if(idx.value == "1") {

								$("#PilihMetode").css("display", "none");
								$("#InputAddress").css("display", "none");
								$("#map").css("display", "none");
								$("#Stockist2").css("display", "none");

								$("#provDiv").css("display", "block");
								$("#KabDiv").css("display", "block");
								$("#KecDiv").css("display", "block");
								$("#StockistDiv").css("display", "block");


								//$("#provKGBDiv").css("display", "none");
								//$("#kotaKGBDiv").css("display", "none");
								//$("#provDiv").css("display", "block");
								//$("#KabDiv").css("display", "block");
								//$("#KecDiv").css("display", "block");
								$("#provinsi").html(null);
								$("#kota").html(null);
								$("#kecamatan").html(null);
								$("#stockistref").html(null);
								var rowhtml = "<option value=''>--Select Here--</option>";
								//rowhtml += "<span>Provinsi<label><font color=red>&nbsp;*</font></label></span>";
								//rowhtml += "<select class='form-list' name='provinsi' id='provinsi' onchange=Shopping.show_kota(this,'#kota') >";
								//rowhtml += "<option value=''>--Select Here--</option>";
								$.each(arrayData, function(key, value) {
									rowhtml += "<option value="+value.kode_provinsi+">"+value.provinsi+"</option>";

								});
								$("#provinsi").append(rowhtml);
								// $("#provinsi").removeAttr("onchange");
								//$("#provinsi").attr("onchange", "Shopping.show_kota(this,'#kota')");

							} else if(idx.value == "2") {
								var rowhtml = "<option value=''>--Select Here--</option>";


								$("#provinsi").html(null);
								$("#kota").html(null);
								$("#kecamatan").html(null);
								$("#stockistref").html(null);
								$("#KecDiv").css("display", "none");
								//$("#provDiv").css("display", "none");
								//$("#KabDiv").css("display", "none");
								//$("#kotaKGBDiv").css("display", "block");
								//$("#provKGBDiv").css("display", "block");
								$.each(arrayData, function(key, value) {
									rowhtml += "<option value='"+value.provinsi+"'>"+value.provinsi+"</option>";

								});
								$("#provinsi").append(rowhtml);
								//$("#provinsi").removeAttr("onchange");
								//$("#provinsi").attr("onchange", "Shopping.show_kota_kgb(this,'#kota')");

							}else if(idx.value == "4") {
								$("#PilihMetode").css("display", "block");
								$("#InputAddress").css("display", "block");
								$("#map").css("display", "block");
								$("#Stockist2").css("display", "block");

								$("#provDiv").css("display", "none");
								$("#KabDiv").css("display", "none");
								$("#KecDiv").css("display", "none");
								$("#StockistDiv").css("display", "none");

							}
							/*else if(idx.value =="0"){
							 $("#AlamatDiv").css("display", "none");
							 $("#PilihMetode").css("display", "none");
							 $("#InputAddress").css("display", "none");
							 $("#map").css("display", "none");
							 $("#Stockist2").css("display", "none");

							 $("#provDiv").css("display", "none");
							 $("#KabDiv").css("display", "none");
							 $("#KecDiv").css("display", "none");
							 $("#StockistDiv").css("display", "none");
							 }*/
						} else {
							//alert(data.message)
						}
					},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + ':' +xhr.status);
					All.set_enable_button();
				}
			});
		}
	}

	function validateCheckOutBaru() {
		var del = $('input[name=delivery]').is(':checked');
		var update = $("#updateCart").val();
		var val = $('input:radio[name=delivery]:checked').val();
		var err = 0;
		if(!del) {
			alert("Silahkan Pilih Tipe Pengiriman");
			//$("#submits").attr("disabled", "disabled");
			$('input[name=delivery]').focus();
			err++;
			//alert("isi err :" +err);
			return false;
		} else {
			//alert("Pilihan :" +val);
			//return false;

			var idmemberx = $("#idmemberx").val();
			var membername = $("#membername").val();

			if(idmemberx === "" || idmemberx === null || membername === "" || membername === null) {
				err++;
			}

			if(val == "1") {
				var provinsi1 = $("#provinsi1").val();
				var kota1 = $("#kota1").val();
				var kecamatan1 = $("#kecamatan1").val();
				var stockist = $("#stockist").val();

				//alert("prov :" +provinsi1+ " kota1" +kota1+ " kecamatan1" +kecamatan1+ " stockist" +stockist);
				/*if(provinsi1 === 0 || kota1 === 0 || kecamatan1 === 0 || stockist === 0) {
				 alert("Silahkan lengkapi data anda sebelum melanjutkan, pilihan 1");
				 err++;
				 return false;
				 } */
				if(provinsi1 === "" || provinsi1 === null || kota1 === "" ||
					kota1 === null || kecamatan1 === "" || kecamatan1 === null ||
					stockist === "" || stockist === null) {
					err++;
					//alert("prov :" +provinsi1+ " kota1 : " +kota1+ " kecamatan1 : " +kecamatan1+ " stockist : " +stockist);
				}

			}
			else {
				var nama_penerima = $("#nama_penerima").val();
				var shipper = $("#shipper").val();
				//var lastname = $("#lastname").val();
				var notlp = $("#notlp").val();
				//var stkarea1 = $("#stkarea1").val();


				//var kelurahan = $("#kelurahan").val();
				var alamat = $("#alamat").val();

				if(shipper == 1) {
					var stockistref = $("#stockistref").val();
					var provinsi = $("#provinsi").val();
					var kota = $("#kota").val();
					var kecamatan = $("#kecamatan").val();
					if(nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null || kecamatan === "" || kecamatan === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				}else if(shipper == "4") {
					var metode = $("#metode").val();
					var alamat = $("#alamat").val();
					if(metode === "" || metode === null || metode == "- Pilih Metode -") {
						alert("harga GOSEND tidak ada / di luar jangkauan / di luar jam service.. ");
						err++;
					}
					
					if(alamat === "" || alamat === null ) {
						alert("alamat harus diisi.. ");
                        err++;
                    }

				}
				else {
					if(nama_penerima === "" || nama_penerima === null || notlp === "" || notlp === null ||
						stockistref === "" || stockistref === null || provinsi === "" || provinsi === null ||
						kota === "" || kota === null ||
						alamat === "" || alamat === null) {
						err++;
					}
				}


			}
		}


		if(err > 0) {
			alert("Silahkan lengkapi data anda sebelum melanjutkan");
			return false;
		} else {
			/*
			var resHitung = Shopping.hitungTotalCart();
          	if(resHitung == false) {
          		alert("Minimum pembelanjaan senilai Rp. 350000,-")
          		return false;
          	} else {
          		return true;
          	}*/
			
			var min_dp = $("#min_dp").val();
            var resHitung = Shopping.checkTotalBelanjaMampirkak(min_dp);
          	if(resHitung == false) {
          		alert("Minimum pembelanjaan senilai Rp. "+ min_dp +",-")
          		return false;
          	} else {
          		return true;
          	}
		}
	}

	function convertToRupiah(angka){
		var rupiah = '';
		var angkarev = angka.toString().split('').reverse().join('');
		for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
		return rupiah.split('',rupiah.length-1).reverse().join('');
	}

	function cek_metode(){

		var res = document.getElementById('metode').value;
		var metode = res.substring(0, 1);
		var harga= res.substring(2);
		var harga= harga.replace(".", "");

		console.log(res);
		document.getElementById("price_harga").value=harga;
		if(metode != '#'){
			post_book();
		}else{
			return false;
		}

	}

	function initMap() {

		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer;

		var options = {
			types: ['geocode'],
			componentRestrictions: {country: "id"}
		};

		var input = document.getElementById('autocomplete');
		var autocomplete = new google.maps.places.Autocomplete(input, options);
		//option alamat autocomplete
		/*  var auto_complete = new google.maps.places.Autocomplete(
		 /!** @type {!HTMLInputElement} *!/
		 (document.getElementById('autocomplete')),
		 {types: ['geocode'],
		 componentRestrictions: {country: "Indonesia"}
		 });*/

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 11,
			center: {lat: -6.2400257, lng: 106.8320919}
		});
		directionsDisplay.setMap(map);

		var onChangeHandler = function () {

			calculateAndDisplayRoute(directionsService, directionsDisplay);
		};

		document.getElementById('stockistref2').addEventListener('change', onChangeHandler);
		document.getElementById('autocomplete').addEventListener('change', onChangeHandler);

	}

	function cek_stokist(){
		$("#stockistref2").empty();
		var geocoder = new google.maps.Geocoder();
		var address = document.getElementById('autocomplete').value;

		geocoder.geocode({'address': address}, function (results, status) {
			if (status === 'OK') {
				var cX = results[0].geometry.location.lat();
				var cY = results[0].geometry.location.lng();

				option_stokist(cX,cY);
				//console.log(cX,cY);

			} else {
				alert('Lokasi Awal was not successful for the following reason: ' + status);
			}
		});
	}

	function option_stokist(cX,cY){
		$.ajax({
			url:"https://www.k-net.co.id/cek_stockist",
			type: "POST",
			dataType:"json",
			data:{
				lat:cX,
				long:cY
			},
			success: function (result) {
				if (result.response == "true") {

					var R= result.jarak;

					var x = document.getElementById("stockistref2");
					var option = document.createElement("option");
					option.text = "--Select Here--";
					x.add(option, x[0]);

					var x = document.getElementById("stockistref2");
					var option1 = document.createElement("option");
					option1.text = "K-Link Tower";
					x.add(option1, x[1]);


					$.each(R, function(k, v) {
						var s= v.split("+");
						/* var x = document.getElementById("stockistref2");
						 var option = document.createElement("option");

						 option.text = s[1]+ " (" +s[4].toLowerCase()+")" ;
						 x.add(option);*/
					});
				}
				else {
					alert(result.error);

				}
			}
		});
	}

	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		directionsService.route({
			origin: document.getElementById('stockistref2').value,
			destination: document.getElementById('autocomplete').value,
			travelMode: 'DRIVING'
		}, function(response, status) {
			if (status === 'OK') {
				directionsDisplay.setDirections(response);

				var a= document.getElementById('stockistref2').value;
				var b= document.getElementById('autocomplete').value;

				//cek_jarak(a,b);
				geocodeAddress1();

			}

		});
	}

	//get jarak menggunakan API google maps
	function cek_jarak(A,B){
		var origin = A,
			destination = B,
			service = new google.maps.DistanceMatrixService();

		service.getDistanceMatrix(
			{
				origins: [origin],
				destinations: [destination],
				travelMode: google.maps.TravelMode.DRIVING,
				avoidHighways: false,
				avoidTolls: false
			},
			callback
		);

		function callback(response, status) {
			if(status=="OK") {
				var str=response.rows[0].elements[0].distance.text;
				var res = str.replace("km", "");
				geocodeAddress1(res); //call fungsi mendapatkan koordinat

				//console.log(res);
			} else {
				alert("Error: " + status);
				//location.reload();
			}
		}

	}

	function geocodeAddress1(/*dist*/) {
		var geocoder = new google.maps.Geocoder();
		var address = document.getElementById('autocomplete').value;
		geocoder.geocode({'address': address}, function (results, status) {
			if (status === 'OK') {
				var Xa = results[0].geometry.location.lat();
				var Ya = results[0].geometry.location.lng();

				document.getElementById('latlong2').value=Xa+","+Ya;
				document.getElementById('lat_dest').value=Xa;
				document.getElementById('long_dest').value=Ya;

				geocodeAddress2(Xa,Ya); //call fungsi mendapatkan koordinat lokasi akhir
				//console.log(dist,Xa,Ya);
			} else {
				alert('Lokasi Awal was not successful for the following reason: ' + status);
			}
		});
	}

	function geocodeAddress2(X1,Y1) {
		$("#metode").empty();
		var geocoder = new google.maps.Geocoder();
		var address = document.getElementById('stockistref2').value;
		geocoder.geocode({'address': address}, function (results, status) {
			if (status === 'OK') {

				var X2 = results[0].geometry.location.lat();
				var Y2 = results[0].geometry.location.lng();

				//document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+"km";

				/*dist = dist * 1;

				if( dist < 40){
					price_send(X1,Y1,X2,Y2);
					document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+" km";
					document.getElementById("metode").disabled = false;
				}else{
					document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+" km (Diluar Jangkauan)";
					document.getElementById("metode").disabled = true;
				}*/

				/*if(dist < 40){
					price_send(X1,Y1,X2,Y2); //call fungsi data gojek
					//tes_api();
				}
				else{
					alert ("Error, ulangi pencarian alamat!");
					$("#autocomplete").empty();
					document.getElementById("autocomplete").focus()
						var x = document.getElementById("metode");
					var option = document.createElement("option");
					option.text = "- Pilih Metode -";
					x.add(option, x[0]);

					var option1 = document.createElement("option");
					option1.text = "Instant - Jarak > 40km";
					x.add(option1, x[1]);

					var option2 = document.createElement("option");
					option2.text = "Same Day - Jarak > 40km";
					x.add(option2, x[2]);
				}*/

				//price_send(X1,Y1,X2,Y2);
				hitung_jarak(X1,Y1,X2,Y2);

			} else {
				alert('Lokasi Akhir was not successful for the following reason: ' + status);
			}
		});
	}

	//get jarak menggunakan rumus lat long
	function hitung_jarak(x1,y1,x2,y2){
		$.ajax({
			url:"https://www.k-net.co.id/get_jarak",
			type: "POST",
			dataType:"json",
			data:{
				lat1:x1,
				long1:y1,
				lat2:x2,
				long2:y2
			},
			success: function (result) {
				if (result.response == "true") {

					var R= result.jarak;
					var dist = R * 1;

					/*if( dist < 40){
					 price_send(X1,Y1,X2,Y2);
					 document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+" km";
					 document.getElementById("metode").disabled = false;
					 }else{
					 document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+" km (Diluar Jangkauan)";
					 document.getElementById("metode").disabled = true;
					 }*/

					if( dist > 40){
						document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+" km (Diluar Jangkauan)";
						document.getElementById("metode").disabled = true;

					}else{
						document.getElementById("km").innerHTML = "Estimasi Jarak Pengiriman "+dist+" km";
						document.getElementById("metode").disabled = false;
						price_send(x1,y1,x2,y2);
					}

				}
				else {
					alert(result.error);

				}
			}
		});
	}

	function price_send(x1,y1,x2,y2){
		var awal= x1+","+y1;
		var akhir= x2+","+y2;

		$.ajax({
			url:"https://www.k-net.co.id/cek_price",
			type: "POST",
			dataType:"json",
			data:{
				lat:awal,
				long:akhir
			},
			success: function (result) {

				console.log(result);

				var weightVal = document.getElementById('totalAmountWeight').value;
				var weightV= weightVal * 1;

				console.log("total berat:"+weightV);

				var hours = new Date().getHours();
				var minute = new Date().getMinutes();
				var time= hours+":"+minute;

				var x = document.getElementById("metode");
				var option = document.createElement("option");
				option.text = "- Pilih Metode -";
				x.add(option, x[0]);

				var service_instant= result.Instant.serviceable;
				var metode_instant=result.Instant.shipment_method_description;
				var instant_hours=metode_instant.substring(0,7);

				if(service_instant == true) {
					var dist_instant= result.Instant.distance;
					var price_instant= result.Instant.price.total_price;
					var rupiah1= convertToRupiah(price_instant);

					var x = document.getElementById("metode");
					var option = document.createElement("option");
					option.value= <?php echo $value1?>;
					option.text = <?php echo $option1?>;
					x.add(option, x[1]);

					/*if (hours >= 8 && hours <= 17) {
					 option.value= "1-"+rupiah1;
					 option.text = "Instant "+instant_hours+" Rp"+ rupiah1;
					 x.add(option, x[1]);
					 }else{
					 option.value= "#";
					 option.text = "Instant - TUTUP (Jam Pesanan: 10 Pagi - 4 Sore)";
					 x.add(option, x[1]);
					 }*/

					if(weightV > 20) {
						option.value= "#";
						option.text = "Instant Tidak Tersedia (Berat > 20 Kg)";
						x.add(option, x[2]);
						console.log("Overweight > 20");
					}

				}else{
					var x = document.getElementById("metode");
					var option = document.createElement("option");
					option.value= "#";
					option.text = "Instant - "+metode_instant;
					x.add(option, x[1]);
				}

				var service_sameday= result.SameDay.serviceable;
				var metode_sameday=result.SameDay.shipment_method_description;
				var sameday_hours=metode_sameday.substring(0,7);

				if(service_sameday == true) {
					var dist_sameday = result.SameDay.distance;
					var price_sameday = result.SameDay.price.total_price;

					var rupiah1 = convertToRupiah(price_sameday);

					var x = document.getElementById("metode");
					var option = document.createElement("option");

					option.value= <?php echo $value2?>;
					option.text = <?php echo $option2?>;
					x.add(option, x[2]);

					/* if (hours >= 8 && hours <= 14) {
					 option.value= "2-"+rupiah1;
					 option.text = "Same Day "+sameday_hours+" Rp"+ rupiah1;
					 x.add(option, x[2]);
					 }else{
					 option.value= "#";
					 option.text = "Same Day - TUTUP (Jam Pesanan: 10 Pagi - 3 Sore)";
					 x.add(option, x[2]);
					 }*/

					if(weightV > 7) {
						option.value= "#";
						option.text = "SameDay Tidak Tersedia (Berat > 7 Kg)";
						x.add(option, x[2]);
						console.log("Overweight > 7");
					}

				}else{
					var x = document.getElementById("metode");
					var option = document.createElement("option");
					option.value= "#";
					option.text = "SameDay "+ metode_sameday;
					x.add(option, x[2]);
				}

			}
		});
	}

	function post_book(){

		var west=document.getElementById('totalAmountWest').value;
		var east=document.getElementById('totalAmountEast').value;
		var amountK=document.getElementById('amountKIRIM').value;

		//pengurangan

		var back_west= west - amountK;
		var back_east= east - amountK;

		console.log("Back West"+back_west);
		console.log("Back East"+back_east);

		$("#amountKIRIM").val(0);
		$("#divAmountKirim").text(0);

		var metode=document.getElementById('metode').value;
		var cmetode=metode.substring(0,1);
		var harga= metode.substring(2);
		var price = harga.replace(".", "");



		var cnama_penerima=document.getElementById('nama_penerima').value;
		var cnotlp=document.getElementById('notlp').value;
		var calamat=document.getElementById('alamat').value;
		var coord=document.getElementById('latlong2').value;
		var x = coord.split(",", 1);
		var y = coord.split(",", 2);
		var calamat= calamat.replace(","," ");

		console.log(cmetode);
		console.log("hrg gosend:"+price);


		$("#amountKIRIM").val(price);
		$("#divAmountKirim").text(harga);

		var tot_west= (price * 1) + (back_west * 1);
		var tot_east= (price * 1) + (back_east * 1);

		console.log("total west:"+tot_west);
		console.log("total east:"+tot_east);

		$("#totalAmountWest").val(tot_west);
		$("#totalAmountEast").val(tot_east);

		$("#totAllWestPrice").text(tot_west);
		$("#totAllEastPrice").text(tot_east);

		document.getElementById('tot_west').value= tot_west;
		document.getElementById('tot_east').value= tot_east;

		//var amountKIRIM = parseInt($("#amountKIRIM").val());

		if(cmetode == 1){
			cmetode="Instant";
		}else{
			cmetode="SameDay";
		}
		/*penerima:cnama_penerima,tlp:cnotlp,alamat:calamat,coordX:x,coordY:y*/

		var x = document.getElementsByName("prdnm[]").length;
		var nprd = [];
		for (var i= 1; i <= x; i++) {
			var prd=document.getElementById('prdnm'+i).value;
			var qty=document.getElementById('qty'+i).value;

			nprd.push(prd+" "+qty+" pcs");
		}

		//console.log(nprd.toString());
		document.getElementById('produk').value=nprd.toString();
		var nproduk = document.getElementById("produk").value;
		var n= nproduk;

		console.log("metode:"+cmetode+",penerima:"+cnama_penerima+",tlp:"+cnotlp+",alamat:"+calamat+",coord:"+coord+",harga:"+price+",item:"+nproduk);

		$.ajax({
			//url:"https://www.k-net.co.id/make_booking",
			url:"#",
			type: "POST",
			dataType:"json",
			data:{
				metode:cmetode,penerima:cnama_penerima,tlp:cnotlp,alamat:calamat,coord:coord,harga:price,item:nproduk
			},
			success: function (result) {
				console.log(result);

				var orderNo=result.orderNo;
				console.log(orderNo);
				//document.getElementById("no_order").value=orderNo;
				// status_book();
			}
		});
	}


</script>
<!-- Breadcrumb -->
<ul class="breadcrumbs">
	<div class="container">
		<li class="home">
			<a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
			&nbsp; <span>&gt;</span>
		</li>
		<li class="home">&nbsp;
			Shopping&nbsp;&nbsp;
			<span>&gt;</span>
		</li>
		<li class="home">
			<span class="red"> &nbsp;Pengiriman&nbsp;&nbsp;</span>
		</li>
	</div>
</ul>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
	<div class="container">
		<div id="formCart1">
			<?php
			$usr = getUserID();
			$id_memb = getUserID();
			$nmmemb = getUserName();
			$id_lp = "n_promo";
			if($usr == "IDSPAAA66834") {
			?>
			<form id="formCart" action="<?php echo base_url('mampirkak/register_membership'); ?>" method="POST" onsubmit="return validateCheckOutBaru()">
				<?php
				} else {
				?>
				<form id="formCart" action="<?php echo base_url('mampirkak/register_membership'); ?>" method="POST" onsubmit="return validateCheckOutBaru()">
					<?php
					}
					?>

					<!-- Delivery Options--><br />
					<div class="col-md-6 col-xs-12">
        	<span class="title_header">
            	<i class="fa fa-truck"></i> Tipe Pengiriman MAMPIR KAK2
            </span>
						<!-- ul><li>Pengumuman : berkenaan dengan libur hari raya idul fitri 2017, maka pengiriman akan dihentikan sementara mulai tanggal 22 Juni 2017 s/d 04 Juli 2017.</li></ul -->
						<!--  Options 1-->

						<div class="col-md-12 col-xs-12 nopadding voffset4">

							<div class="col-md-12 col-xs-12 delivery_header">
								<input name="delivery" id="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="1"/> Diambil di Stockist
							</div>

							<div class="col-md-12 col-xs-12 diambil" style="display: none;">
								<p class="p2 nopadding">Barang Yang Dipesan Dapat Diambil Pada Stockist yang dipilih<br/><br/></p>
								<div class="register-top-grid">

									<div>
										<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="provinsi1" id="provinsi1" onchange="Shopping.show_kota2(this,'#kota1')" >
											<option value="">--Select Here--</option>
											<?php
											//print_r($show_provinsi);
											foreach($show_provinsi['arrayData'] as $row){
												echo "<option value=\"".$row->kode_provinsi."\">".strtoupper($row->provinsi)."</option>";
											}

											?>
										</select>
										<input type="hidden" id="nama_provinsi1" name="nama_provinsi1" value="" />
									</div>
									<div>
										<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="kota1" id="kota1" onchange="Shopping.show_kecamatan2(this,'#kecamatan1')">

										</select>
										<input type="hidden" id="nama_kota1" name="nama_kota1" value="" />

									</div>
									<div>
										<span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>

										<select class="form-list" name="kecamatan1" id="kecamatan1" onchange="Shopping.show_kelurahan2(this,'#stockist')" >

										</select>
										<input type="hidden" id="nama_kecamatan1" name="nama_kecamatan1" value="" />
									</div>


									<div>
										<span>Pilih Lokasi Stockist<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="stockist" id="stockist" onchange="Shopping.show_pricestkX(this,'#pricecode')">
										</select>
										<input type="hidden" id="nama_stockist" name="nama_stockist" value="" />
									</div>
									<div class="clearfix"> </div>

								</div>
							</div>

						</div>


						<!--  Options 2-->
						<div class="col-md-12 col-xs-12 nopadding voffset4">
							<!--  Options 2 header-->
							<div class="col-md-12 col-xs-12 delivery_header">
								<input name="delivery" type="radio" class="delivery_choice" onclick="Shopping.getDelChoice(this.value)" value="2"/> Dikirim ke Alamat <!-- (Pilihan ini belum direkomendasikan/masih dalam pengembangan) -->
							</div>
							<!--  Options 2 header-->

							<!--  Options 2 delivery address-->
							<div class="col-md-12 col-xs-12 vpullset3 dikirim" style="display: none;">
								<!--<div class="col-md-12 col-xs-12 delivery_header"><i class="fa fa-map-marker"></i> Delivery Address</div>-->

								<div class="register-top-grid">
									<div>
										<span>Nama Penerima<label><font color="red">&nbsp;*</font></label></span>
										<input class="uppercase" type="text" name="nama_penerima" id="nama_penerima" maxlength="25"/>
									</div>

									<div>
										<span>No Telp / HP<label><font color="red">&nbsp;*</font></label></span>
										<input class="numeric-input" type="text" name="notlp" id="notlp" onchange="cekHp()"/>
									</div>
									<div style="width:98%">
										<span>Email Address</span>
										<input type="text" name="email" id="email" />
									</div>
									<div style="width:98%">
										<span>Alamat<label><font color="red">&nbsp;*</font></label></span>
										<textarea onkeyup="textAreaUppercase(this)" name="alamat" rows="4" name="alamat" id="alamat"></textarea>
										<input type="hidden" id="destination_address" name="destination_address" value="" />
									</div>

									<div class="col-md-12 col-xs-12 delivery_header" style="width:98%">
										Pilih Stockist Terdekat Dengan Alamat Tujuan
									</div>

									<div>
										<span>Ekspedisi / Cargo<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="shipper" id="shipper" onchange="listProvinceByCargo(this)" >
											<option value="">--Select Here--</option>
											<?php
											//print_r($listCargo);
											foreach($listCargo as $row){
												$jam=date("H");
												$value= $row->shipper_id;
												$name=strtoupper($row->shipper_name);

												$now = new Datetime("now");
												$open= date('H:i',strtotime($open_gsend));
												$close= date('H:i',strtotime($closed_gsend));
												if($value == 4 ){
													if(($now >= $begintime && $now <= $endtime) && $val_holiday == 0){
														$value=$value;
														$name=$name;
													} else {
														$value='';
														//$name='GOSEND TUTUP (Maaf sedang dalam perbaikan)';
														$name="GOSEND TUTUP (Jam Order ".$open." WIB - ".$close." WIB)";
													}
												}
												//echo "<option value=''>".$jam."</option>";
												echo "<option value=\"".$value."\">".$name."</option>";
											}
											?>
										</select>
									</div>


									<div id="provDiv">
										<span>Provinsi<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="provinsi" id="provinsi" onchange="Shopping.tampilkanKota('#kota')" >

										</select>
										<input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />
									</div>

									<div id="KabDiv">
										<span>Kota / Kabupaten<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="kota" id="kota" onchange="Shopping.tampilkanKecamatan()">
										</select>
										<input type="hidden" id="nama_kota" name="nama_kota" value="" />
									</div>
									<div id="KecDiv">
										<span>Kecamatan<label><font color="red">&nbsp;*</font></label></span>
										<!--<input type="text" name="kecamatan" id="kecamatan"/>-->
										<select class="form-list" name="kecamatan" id="kecamatan" onchange="Shopping.show_kelurahan_nonm_promo(this,'#stockistref')" >

										</select>
										<input type="hidden" id="nama_kecamatan" name="nama_kecamatan" value="" />
									</div>

									<div id="StockistDiv" style="width:98%">
										<span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>
										<!--<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.show_pricestkY(this,'#pricecode')">-->
										<select class="form-list" name="stockistref" id="stockistref" onchange="Shopping.tampilkanPriceCode()">
										</select>
										<input type="hidden" id="nama_stockistr1ref" name="nama_stockistr1ref" value="" />
										<input type="hidden" id="state" name="state" value="" />
										<input type="hidden" id="sender_address" name="sender_address" value="" />
										<input type="hidden" id="jne_branch" name="jne_branch" value="" />
									</div>

									<div id="InputAddress" style="width:98%">
										<span>Masukkan Alamat Tujuan<label><font color="red">&nbsp;*</font></label></span>
										<input type="text" placeholder="Enter your address" id="autocomplete" name="autocomplete" value="" />
										<input  type="button" value="clear" onclick="clear_addr()">
										<br><br>
										<span id="km"></span>
									</div>

									<div id="PilihMetode">
										<span>Metode Pengiriman<label><font color="red">&nbsp;*</font></label></span>
										<select class="form-list" name="metode" id="metode" onchange="cek_metode()">
										</select>
										<br>
										<span style="color:#ff0a18;">Jam Order GOSEND <?php echo $open?> WIB - <?php echo $close?> WIB</span>
										<br>
										<span>Syarat & Ketentuan Go-Send <a href="<?php echo $info_html?>" target="_blank"><u>klik disini</u></a> </span>
										<!--<input type="hidden" id="nama_provinsi" name="nama_provinsi" value="" />-->
									</div>


									<br>

									<div style="width:98%" id="map"></div>

									<div id="Stockist2">
										<!--<span>Pilih Lokasi Stockist Referensi<font color="red">&nbsp;*</font></span>-->
										<input type="hidden" id="stockistref2" name="stockistref2" value="K-link Tower" readonly>
										<input type="hidden" id="latlong2" name="latlong2" value="" readonly>
										<input type="hidden" id="lat_dest" name="lat_dest" value="" readonly>
										<input type="hidden" id="long_dest" name="long_dest" value="" readonly>
										<input type="hidden" id="tot_berat" name="tot_berat" value="" readonly>
										<input type="hidden" id="tot_east" name="tot_east" value="" readonly>
										<input type="hidden" id="tot_west" name="tot_west" value="" readonly>
										<input type="hidden" id="no_order"  name="no_order"  value="" readonly>
										<input type="hidden" id="price_harga"  name="price_harga"  value="" readonly>
										<input type="hidden" id="produk"  name="produk"  value="" readonly>
									</div>
									<div class="clearfix"> </div>

								</div>
							</div>
							<!--  Options 2 delivery address-->
						</div>
						<!--  Options 2-->
					</div>

					<div id="listCartPrd">
						<!-- Order Details-->
						<div class="col-md-6 col-xs-12">
            	<span class="title_header">
                	<i class="fa fa-check-square-o"></i> Detail Pembelanjaan
                </span>
							<div>
								<font color="red">No HP untuk Konfirmasi :</font>
								<input type="text" name="no_hp_konfirmasi" class="numeric-input" id="no_hp_konfirmasi" />
							</div>
							<div class="register-top-grid">
								<!--<div style="width:100%;">
                	<span>ID Member<label>*</label></span>
                    <input type="text" class="required uppercase" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo getUserID(); ?>"/>  
                    <input type="button" id="checkIDMember" value="Check ID" class="btn1 btn2 btn-primary1" onclick="Shopping.getMemberNameByID(this.form.idmemberx, '#membername')" />
                    <input type="hidden" id="id_lp" name="id_lp" value="" />                  
                </div>
                <div>
                	<span>Nama Member<label>*</label></span>
                    <input readonly="readonly" class="required uppercase" type="text" name="membername" id="membername" value="<?php echo getUsername(); ?>"/> 
                </div>
                <div>
                	<span>Periode Bonus<label>*</label></span>                    
                     <select name="bnsperiod" id="bnsperiod" class="form-list">
                      <?php

								$xx = $bns['arrayData'];
								for($i=0; $i <= $xx[0]->rangeperiod; $i++) {
									$x = addRangeOneMonth($xx[0]->lastperiod, $i);
									echo $x."<br />";
									echo "<option value=\"$x\">$x</option>";
								}
								//echo "<option value=\"01/05/2010\">01/05/2010</option>";
								?>
                     </select>
                </div>-->
								<input type="hidden" class="required uppercase" name="idmemberx" id="idmemberx" style="width:70%;" value="<?php echo $id_memb;?>"/>
								<input type="hidden" name="membername" id="membername" value="<?php echo $nmmemb;?>"/>
								<input type="hidden" name="bnsperiod" id="bnsperiod" value="<?php echo date("d/m/Y"); ?>">
								<input type="hidden" name="id_lp" id="id_lp" value="<?php echo $id_lp; ?>">
								<div class="clearfix"> </div>
								<div class="clearfix"> </div>
							</div>
							<br />
							<?php
							$cart_check = $this->cart->contents();

							// If cart is empty, this will show below message.
							if(empty($cart_check)) {
								echo '<br/>To add products to your shopping cart click on "Add to Cart" Button';
							}
							?>
							<!-- header-->
							<div class="col-md-12 col-xs-12 cart_header">
								<div class="col-md-3 col-xs-3">Product </div>
								<div class="col-md-2 col-xs-2" align="right">Qty</div>
								<div class="col-md-1 col-xs-1" align="right">BV</div>
								<div class="col-md-2 col-xs-2" align="right">Price</div>
								<div class="col-md-3 col-xs-3" align="right">Amount</div>
								<!--<div class="col-md-3 col-xs-3" align="right">Tot BV</div> -->
								<div class="col-md-1 col-xs-1"><i class="fa fa-trash-o"></i></div>
							</div>
							<!-- header-->

							<!-- product item-->
							<?php
							$n = 1;
							foreach($cart as $dt)
							{
								$rowid = $dt['rowid'];
								$prdcd = $dt['id'];
								$prdnme = $dt['name'];
								$westPrice = $dt['west_price'];
								$qty = $dt['qty'];
								$bv = $dt['bv'];
								$eastPrice = $dt['east_price'];

								if(substr($prdcd,0,5) == 'HC050' || substr($prdcd,0,7) == 'HC075NA') { //promo paket k-ion
									$prdnm = substr($prdnme, 8);
								}else{
									$prdnm= $prdnme;
								}

								?>
								<div id="<?php echo $n;?>">
									<div class="col-md-12 col-xs-12 order_wrap">
										<div class="col-md-3 col-xs-3 order_desc">
											<a href="#">
												<input type="hidden" value="<?php echo $prdnm;?>" name="prdnm[]" id="<?php echo "prdnm".$n;?>" readonly="yes"/>
												<h3><?php echo $prdnm;?> </h3></a>
											<!--<p>Unfortunately, with the way browser security works, you are unable to navigate your site</p>-->
										</div>
										<div class="col-md-2 col-xs-2">
											<input class="orderdetails-box" onkeyup="Shopping.setNewPrice(<?php echo $n;?>)" type="text" value="<?php echo $qty;?>" name="qty[]" id="<?php echo "qty".$n;?>"/>
										</div>
										<div class="col-md-1 col-xs-1" align="right">
											<?php

											echo $bv;
											?>
											<input class="orderdetails-box" type="hidden" value="<?php echo $bv;?>" name="bv[]" id="<?php echo "bv".$n;?>" readonly="yes"/>

										</div>
										<div class="col-md-2 col-xs-2 westP" align="right"><?php echo "".number_format($westPrice,0,",",".")."";?>
											<input class="orderdetails-box" type="hidden" value="<?php echo $westPrice;?>" name="westPrice[]" id="<?php echo "westPrice".$n;?>" readonly="yes"/>
										</div>
										<div class="col-md-3 col-xs-3 westP" align="right" id="<?php echo "divSubTotWestPrice".$n ?>"><?php echo "".number_format($dt['subtotal_west_price'],0,",",".").""; ?>

										</div>
										<div class="col-md-2 col-xs-2 eastP" align="right" style="display: none;"><?php echo "".number_format($eastPrice,0,",",".").""; ?>
											<input class="orderdetails-box" type="hidden" value="<?php echo $eastPrice;?>" name="eastPrice[]" id="<?php echo "eastPrice".$n;?>" readonly="yes"/>
										</div>
										<div class="col-md-3 col-xs-3 eastP" align="right" id="<?php echo "divSubTotEastPrice".$n ?>" style="display: none;" ><?php echo "".number_format($dt['subtotal_east_price'],0,",",",").""; ?>

										</div >
										<!--
                                        <div class="col-md-3 col-xs-3 divTotBV">

                                        </div>
                                        -->
										<div class="col-md-1 col-xs-1 text-center">
											<input type="hidden" id="<?php echo "rowid".$n; ?>" value="<?php echo $rowid;?>" name="rowid[]"/>
											<a href="#" id="<?php echo $rowid;?>" onclick="Shopping.delete_rowCart(<?php echo $n; ?>)"><i class="fa fa-minus-circle"></i></a>
										</div>
										<input class="amtW orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_west_price']; ?>" name="amountWest[]" id="<?php echo "amountWest".$n;?>" readonly="yes"/>
										<input class="amtE orderdetails-box" type="hidden" value="<?php echo $dt['subtotal_east_price'];?>" name="amountEast[]" id="<?php echo "amountEast".$n;?>" readonly="yes"/>
										<?php $sub_totalbv = $qty * $bv; ?>
										<input class="amtE orderdetails-box" type="hidden" value="<?php echo $sub_totalbv; ?>" name="amountBV[]" id="<?php echo "amountBV".$n;?>" readonly="yes"/>
									</div>

								</div>
								<!-- product item-->
								<?php
								$n++;
							}
							?>

							<!-- subtotal -->
							<div class="col-md-12 col-xs-12 order_total_wrap westP">
								<div class="col-md-3 col-xs-3">SUBTOTAL</div>
								<div class="col-md-2 col-xs-2" id="divTotQtyWest"><?php echo $this->cart->total_items(); ?>

								</div>
								<div class="col-md-1 col-xs-1" align="right" id="divTotBVWest">
									<?php echo $this->cart->total_bv();?>
								</div>
								<div class="col-md-1 col-xs-1">
									<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>
								</div>
								<div class="col-md-2 col-xs-2"></div>
								<div class="col-md-3 col-xs-3">
									<input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_west_price');?>" name="totalWestPrice" id="totalWestPrice" readonly="yes"/>
								</div>
								<div class="col-md-3 col-xs-3" id="totAllWestPrice" align="right"><?php echo "".number_format($this->cart->total_west_price(),0,",",",");?>

								</div>
								<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items(); ?>" name="totQtyWest" id="totQtyWest" readonly="yes"/>
								<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_west_price();?>" name="totalAmountWest" id="totalAmountWest" readonly="yes"/>
								<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_weight();?>" name="totalAmountWeight" id="totalAmountWeight" readonly="yes"/>
							</div>
							<div class="col-md-12 col-xs-12 order_total_wrap eastP " style="display: none;">
								<div class="col-md-3 col-xs-3">SUBTOTAL</div>
								<div class="col-md-2 col-xs-2" id="divTotQtyEast"><?php echo $this->cart->total_items();?>

								</div>
								<div class="col-md-1 col-xs-1" align="right" id="divTotBVEast">
									<?php echo $this->cart->total_bv();?>
								</div>
								<div class="col-md-1 col-xs-1 ">
									<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_bv();?>" name="totBv" id="totBv" readonly="yes"/>
								</div>
								<div class="col-md-2 col-xs-2"></div>
								<div class="col-md-3 col-xs-3">
									<input class="orderdetails-box" type="hidden" value="<?php //echo $this->cart->contents('subtotal_east_price');?>" name="totalEastPrice" id="totalEastPrice" readonly="yes"/>
								</div>
								<div class="col-md-3 col-xs-3" id="totAllEastPrice" align="right"><?php echo "".number_format($this->cart->total_east_price(),0,",",",");?>

								</div>
								<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_items();?>" name="totQtyEast" id="totQtyEast" readonly="yes"/>
								<input class="orderdetails-box" type="hidden" value="<?php echo $this->cart->total_east_price();?>" name="totalAmountEast" id="totalAmountEast" readonly="yes"/>
							</div>
							<!-- subtotal

                            <!-- subtotal -->
							<div class="col-md-12 col-xs-12 order_add">
								<a href="<?php echo "".site_url('shop/product/')."";?>">
									<i class="fa fa-shopping-cart"></i> Tambahkan Lagi Keranjang Belanja Anda
								</a>
							</div>

							<!-- subtotal -->
						</div>

						<?php showListFreePrdNM_Promo($listFreeProduct); ?>
						<!-- Order Details-->
					</div>

					<!-- next button-->
					<div class="col-md-12">

						<input type="hidden" name="pricecode" id="pricecode" value="<?php echo $this->session->userdata('pricecode'); ?>" />
						<input type="hidden" id="min_dp"  name="min_dp"  value="<?php echo $resM['min_dp'];?>" readonly>
						<!--<a href="payment.php" class="btn1 btn2 btn-primary1 pull-left"><span>Lanjutkan</span><i class="fa fa-arrow-right"></i></a>-->

						<!--<button type="button" class="btn1 btn2 btn-primary1 pull-left" onclick="Shopping.checkout()" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />-->
						<button type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjutkan  <i class="fa fa-arrow-right"></i></button><br />
					</div>
					<!-- next button-->
		</div>
		</form>
		<!-- Delivery Options-->
	</div> <!-- End formCart1-->
	<div id="divCheckOut"></div>
	<div id="afterPayment"></div>
</div>
</div>
<!--Checkout Wrapper-->

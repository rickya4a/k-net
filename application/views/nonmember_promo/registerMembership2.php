<style>
    .form-list {
        font-size: 0.75em;
    }

    .sponsorid{
        width: 80%;
    }



    .register-top-grid > .col-md-6.col-xs-12 {
        min-height: 60px;
    }
</style>

<!-- Breadcrumb -->
<ul class="breadcrumbs">
    <div class="container">
        <li class="home">
            <a href="index.html" title="Go to Home Page"><img src="<?php echo base_url('assets/images/home.png');?>" alt=""/></a>&nbsp;
            &nbsp; <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Member&nbsp;&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">
            <span class="red"> &nbsp;Pendaftaran Baru&nbsp;&nbsp;</span>
        </li>

    </div>
</ul>

<script>
    $(document).ready(function(){
        $('#sponsoridinput').change(function(){
                if($('#sponsoridinput').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url: All.get_url("nonmember/getSponsorName"),
                        dataType : 'json',
                        data : {sponsoridinput:$('#sponsoridinput').val()},//what is this? ^-^
                        success: function(data){
                            if(data.response == "true") {
                                var result = data.arrayData;
                                $("#sponsorname").val(result[0].fullnm);
                                $("#submits").removeAttr("disabled");
                            } else {
                                alert(data.message);
                                $("#submits").attr("disabled", "disabled");
                            }


                        }
                    });
                }
            }
        );
		
		$('#recruiteridinput').change(function(){
                if($('#recruiteridinput').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url: All.get_url("nonmember/getSponsorName"),
                        dataType : 'json',
                        data : {sponsoridinput:$('#recruiteridinput').val()},//what is this? ^-^
                        success: function(data){
                            if(data.response == "true") {
                                var result = data.arrayData;
                                $("#recruitername").val(result[0].fullnm);
                                $("#submits").removeAttr("disabled");
                            } else {
                                alert(data.message);
                                $("#submits").attr("disabled", "disabled");
                            }


                        }
                    });
                }
            }
        );
		
		

    });
</script>
<!-- Breadcrumb -->

<!--Checkout Wrapper-->
<div class="row vpullset4">
    <div class="container">
        <div id="voucherDiv">

            <div class="col-md-12 col-xs-12 vpullset3">
                <h3><font color="red"><?php echo $errMessage; ?></font></h3>
                <form id="formMember" action="<?php echo base_url('mampirkak/checkout2'); ?>" method="POST" onsubmit="return validateNextCheckout()">

                    <input  tabindex="4" class="required uppercase" type="hidden" id="stk" name="stk" value="<?php echo $stockist;?>"/>
                    <div class="col-md-12">

                        <select tabindex="9"  class="form-list required" id="isMember" name="isMember" onchange="Membership()">
                            <option value="1" <?php if($memberlama){echo 'selected';} ?>>Member</option>
                            <option value="0">Non-member (Untuk mendaftarkan member baru)</option>

                        </select>
                    </div>

                    <div id="memberlama" class="col-md-12"  style="display: block;"  >
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">No Member<font color="red">&nbsp;*</font></label><br/>
                            <input  tabindex="1" style="text-transform: uppercase" class="required" type="text" id="dfnoo" name="dfnoo" value="<?php echo $idno?>" onchange="loaddata()" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Nama Lengkap<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="2" class="required uppercase" type="text" id="fullnmo" name="fullnmo" value="<?php echo $nama?>" />
                        </div>

                    </div>

                    <div id="Nonmember" class="col-md-12" style="display: none;" >
                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">No KTP<font color="red">&nbsp;*</font></label><br/>
                            <input  tabindex="1" class="required numeric-input" type="text" id="idno" name="idno" value="<?php echo $idno?>" onchange="Shopping.checkDoubleKTP(this.value)" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Nama Lengkap<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="2" class="required uppercase" type="text" id="membername" name="membername" value="<?php echo $nama?>" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Tgl Lahir<font color="red">&nbsp;*</font></label><br/>
                            <!--<input tabindex="5" class="required" type="text" id="birthdt" name="birthdt" placeholder="DD/MM/YYYY" value="" onchange="Shopping.checkAgeMember(this.value)" />-->
                            <?php
                            echo datebirth_comboTabIndex("6", "7", "8");
                            ?>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label  class="label-list">Jenis Kelamin<font color="red">&nbsp;*</font></label><br/>
                            <select tabindex="9"  class="form-list required" id="sex" name="sex">
                                <option value="M">Pria</option>
                                <option value="F">Wanita</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Alamat<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="10" class="required uppercase" type="text" id="addr1" name="addr1" value="<?php echo $alamat?>" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">No. HP<font color="red">&nbsp;*</font></label><br/>
                            <input tabindex="11" class="required uppercase" type="text" id="tel_hp" name="tel_hp" value="<?php echo $notlp?>" onchange="Shopping.checkDoubleHP(this.value)" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label class="label-list">Email</label><br/>
                            <input tabindex="12" type="text" id="memb_email" name="memb_email" value="<?php echo $email?>" />
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <label  class="label-list">Sponsor<font color="red">&nbsp;*</font></label><br/>
                            <select tabindex="13"  class="form-list required" id="sponsor" name="sponsor" onchange="Shopping.chooseSponsorId(this.value)">
                                <option value="">---Pilih---</option>
                                <option value="input">Input ID Sponsor</option>
                                <!--<option value="pilih">Pilih ID Sponsor</option>-->
                            </select>
                        </div>
                        <div class="row" style="margin-left: 0px;margin-right: 0px;">

                            <div class="col-md-6 col-xs-12 input" style="display: none;">
                                <label class="label-list">ID Sponsor<font color="red">&nbsp;*</font></label><br/>
                                <!--                            <input class="required uppercase" type="text"  id="sponsorid" name="sponsorid" onchange="Shopping.getMemberInfoDev(this.value, 'sponsorname')" />-->
                                <input class="required uppercase" type="text"  id="sponsoridinput" name="sponsoridinput" />
                            </div>
                            <div class="col-md-6 col-xs-12 pilih" style="display: none;">
                                <label class="label-list">Pilih Sponsor<font color="red">&nbsp;*</font></label><br/>
                                <select name="sponsoridpilih" id="sponsoridpilih" class="form-list required" onchange="select(this)">
                                    <option value=""> -- Select One -- </option>
                                    <?php
                                    foreach($showsponsor as $r){
                                        ?>
                                        <option value="<?php echo $r->dfno;?>"><?php echo $r->dfno." - ".$r->fullnm;?></option>

                                    <?php } ?>

                                </select>
                            </div>

                            <div class="col-md-6 col-xs-12 nama" style="display: none;">
                                <label class="label-list">Nama Sponsor<font color="red">&nbsp;*</font></label><br/>
                                <input class="required uppercase" type="text" readonly="readonly" id="sponsorname" name="sponsorname" readonly="readonly" />
                            </div>
                        </div>
                        <!--                            recruiter-->
                        <div class="row" style="margin-left: 0px;margin-right: 0px;">

                            <div class="col-md-6 col-xs-12">
                                <label  class="label-list">Recruiter<font color="red">&nbsp;*</font></label><br/>
                                <select tabindex="13"  class="form-list required" id="recruiter" name="recruiter" onchange="Shopping.chooseSponsorId2(this.value)">
                                    <option value="">---Pilih---</option>
                                    <option value="input">Input ID Recruiter</option>
                                    <!--<option value="pilih">Pilih ID Recruiter</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 input2" style="display: none;">
                            <label class="label-list">ID Recruiter<font color="red">&nbsp;*</font></label><br/>
                            <!--                            <input class="required uppercase" type="text"  id="sponsorid" name="sponsorid" onchange="Shopping.getMemberInfoDev(this.value, 'sponsorname')" />-->
                            <input class="required uppercase" type="text"  id="recruiteridinput" name="recruiteridinput" />
                        </div>

                        <div class="col-md-6 col-xs-12 pilih2" style="display: none;">
                            <label class="label-list">Pilih Recruiter<font color="red">&nbsp;*</font></label><br/>
                            <select name="recruiteridpilih" id="recruiteridpilih" class="form-list required" onchange="select2(this)">
                                <option value=""> -- Select One -- </option>
                                <?php
                                foreach($showsponsor as $r){
                                    ?>
                                    <option value="<?php echo $r->dfno;?>"><?php echo $r->dfno." - ".$r->fullnm;?></option>

                                <?php } ?>

                            </select>
                        </div>

                        <div class="col-md-6 col-xs-12 nama2" style="display: none;">
                            <label class="label-list">Nama Recruiter<font color="red">&nbsp;*</font></label><br/>
                            <input class="required uppercase" type="text" readonly="readonly" id="recruitername" name="recruitername" readonly="readonly" />
                        </div>


                    <!-- EDIT ningsih 01/11/2018-->
                        <div class="row" style="margin-left: 0px;margin-right: 0px;">
                            <div class="col-md-6 col-xs-12">
                                <label  class="label-list">Bank</label><br/>
                                <select tabindex="13"  class="form-list" id="bank" name="bank">
                                    <option value="">---Pilih---</option>
                                    <?php
                                    foreach($bank as $b){
                                        ?>
                                        <option value="<?php echo $b->bankid;?>"><?php echo $b->description;?></option>

                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-6 col-xs-12 nama2">
                                <label class="label-list">No. Rekening</label><br/>
                                <input class="required uppercase" type="text" id="norek" name="norek"/>
                            </div>
                        </div>

                    <!-- END Edit-->

                    </div>
                    <div class="clearfix"> </div>
                    <hr>




                    <div class="col-md-12">

                        <input type="hidden" id="errformRecruiter" name="errformRecruiter" value="0" />
                        <input type="hidden" id="errformSponsor" name="errformSponsor" value="0" />
                        <button tabindex="17" type="submit" class="btn1 btn2 btn-primary1 pull-left" id="submits">Lanjut<i class="fa fa-arrow-right"></i></button><br />
                    </div>
                    <!-- next button-->
            </div>
            </form>
        </div>
    </div>
    <!-- Delivery Options-->
</div> <!-- End formCart1-->
<div id="divCheckOut"></div>
<div id="afterPayment"></div>

<script>
    function select(ddl) {
        var test = ddl.options[ddl.selectedIndex].text;
        //console.log(test);
        var res= test.substr(test.indexOf('-'));
        var res1 = res.replace("-", "");
        //console.log(res1);
        document.getElementById('sponsorname').value = res1;
    }
    function select2(ddl) {
        var test = ddl.options[ddl.selectedIndex].text;
        //console.log(test);
        var res= test.substr(test.indexOf('-'));
        var res1 = res.replace("-", "");
        //console.log(res1);
        document.getElementById('recruitername').value = res1;
    }
</script>

<script>
    function Membership() {
        var a = document.getElementById("isMember");
        var x= a.options[a.selectedIndex].value;

        var y = document.getElementById("memberlama");
        var z = document.getElementById("Nonmember");

        console.log(x);
        if(x==0){
            y.style.display = "none";
            z.style.display = "block";
        }
        else{
            z.style.display = "none";
            y.style.display = "block";
        }
//        var res= test.substr(test.indexOf('-'));
//        var res1 = res.replace("-", "");
//        //console.log(res1);
//        document.getElementById('sponsorname').value = res1;
    }



    function loaddata()
    {
        var idnya=document.getElementById( "dfnoo" ).value;

        if (idnya !== null)
        {
        	$("#submits").attr("disabled", "disabled") 
            //$( '#display_info' ).html("Isinya : "+idnya);

            $.ajax({
                //			alert('coba ajax');
                type : "POST",
                url  : "<?php echo base_url();?>dtc/Cdtc_mbr/cariID",
                data : {idm: idnya},
                dataType: "json",

                success : function(result)
                {
                    if(result[0].fullnm!=null) {
                        $("#fullnmo").val( result[0].fullnm );
						$("#submits").removeAttr("disabled");
                    }
                    else
                    {
                        alert("ID Member tidak valid");
						$("#submits").attr("disabled", "disabled");

                    }

                }
            });

        } else
        {
            $( '#fullnmo' ).html("Please Enter Some Words");
        }
    }
</script>
<script>
 $( document ).ready(function() {  
   $("input[type=text]").keyup(function(){
     $(this).val( $(this).val().toUpperCase() );
   });
});

function validateNextCheckout() {
	var isMember = $("#isMember").val();
	var err = 0;
	if(isMember === "1") {
		var memberid = $("#dfnoo").val();
		if(memberid === "" || memberid === null) {
			err++;
		}
	}
	
	
	if(err > 0) {
		alert("Silahkan lengkapi data anda sebelum melanjutkan");
		return false;
	} else {
		return true;
	}
}

</script>

<!--Checkout Wrapper-->

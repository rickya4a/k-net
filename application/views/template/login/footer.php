<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    </div>
    <script src="<?php echo base_url('asset/js/jquery-1.11.1.min.js'); ?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('asset/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/js/angular-1.3.13.js'); ?>"></script>
    <script src="<?php echo base_url('asset/js_module/phone.js'); ?>"></script>
    <script language="javascript">
  
	  function openTree(id)
	  {
	  	// ambil semua tag <ul> yang mengandung attribut parent = id dari link yang dipilih
	  	var elm = $('ul[@parent='+id+']'); 
	  	if(elm != undefined)
		{ // jika element ditemukan
	  	  if(elm.css('display') == 'none')
		  { // jika element dalam keadaan tidak ditampilkan
	  	    elm.show(); // tampilkan element 	  	
	  	    $('#img'+id).attr('src','asset/image/folderopen.jpg'); // ubah gambar menjadi gambar folder sedang terbuka
	  	  }
		  else
		  {
	  	  	elm.hide(); // sembunyikan element
	  	    $('#img'+id).attr('src','asset/image/folderclose2.jpg'); // ubah gambar menjadi gambar folder sedang tertutup
	  	  }
		}
	  }
	 </script>
	  
  </body>
</html>
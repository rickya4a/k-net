<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/bootstrap/css/theme.css'); ?>" rel="stylesheet" >
    <link href="<?php echo base_url('asset/css/bootstrap/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
    <style>
		  ul{
		  	list-style-type:none; /* setiap list dihilangkan penanda setiap list-nya */
		  	padding-left: 3px;
		  	margin-left: 3px;
		  }
		  a:link, a:visited{
		  	text-decoration: none;
		    font-face: Verdana;	
			font-size: 15px;
		  	color: #006;
		  }
		  a:hover, a:active{
		    font-face: Verdana;
			font-size: 15px;
			color: green;
		  	/*text-decoration: underline;*/
		  }
		  
		  table.specNoTopBottom
			{
			 border-bottom-width:2;
			 border-left-width:2;
			 border-right-width:2;
			 
			 border-left-style:outset;
			 border-right-style:inset;
			 border-top-style:none;
			 border-bottom-style:none;
			}
		 
    </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid">
  	
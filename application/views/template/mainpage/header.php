<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/dashboard.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/datatables.css'); ?>" rel="stylesheet">
    <!--<link href="<?php echo base_url('asset/css/bootstrap/css/theme.css'); ?>" rel="stylesheet" >
    <link href="<?php echo base_url('asset/css/bootstrap/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
    -->
   
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Proyek Percontohan</a>
          
        </div>
        <div id="navbar" class="nav navbar-nav navbar-right">
          
          <li class="dropdown">
          		
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                	<i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;
                	<?php 
                	  echo $this->session->userdata('name');
                	?> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="<?php echo base_url('admin/logout'); ?>">Logout</a></li>
                </ul>
              </li>
                
        </div>
      </div>
    </nav>	
  
  	
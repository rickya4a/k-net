<div class="container-fluid">
  	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
        
            <?php
             $usermenu = $this->session->userdata('usermenu');
             foreach($usermenu as $menu) {
             	echo "<li><a class='ss' id='$menu->url'>$menu->menuname</a></li>";
             }
            ?>
          </ul>
          
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<div id="content"></div>
        	<div class="result"></div>
		    <div class="nextForm1"></div>
		    <div class="nextForm2"></div>
		    <div class="nextForm3"></div>
        </div>
		        
    </div>    	  
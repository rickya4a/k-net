<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Database Error</title>
<style type="text/css">

body {
  overflow: hidden;
}
html,
body {
  position: relative;
  background: #D4DFE6;
  min-height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #274c5e;
}
.Container {
  text-align: center;
  position: relative;
  padding:0 50px;
}
.MainTitle {
  display: block;
  font-size: 32px;
  font-weight: bold;
  text-align: center;
  font-family:"arial";
  padding-top: 50px;
}
.MainDescription {
 
  font-size: 19px;
  font-weight: normal;
  font-family:"arial";
}
.MainGraphic {
  position: relative;
}
.Cog {
  width: 10rem;
  height: 10rem;
  fill: #FF5722;
  transition: easeInOutQuint();
  -webkit-animation: CogAnimation 5s infinite;
          animation: CogAnimation 5s infinite;
}

@-webkit-keyframes CogAnimation {
  0% {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@keyframes CogAnimation {
  0% {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}


</style>

<body>
 

  <div class="Container">
    <img src="https://k-mart.co.id/assets/img/klink/logo.png" alt="Smiley face" height="82" width="auto">
    <div class="MainGraphic">
       
  
    
    <svg class="Cog" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M29.18 19.07c-1.678-2.908-.668-6.634 2.256-8.328L28.29 5.295c-.897.527-1.942.83-3.057.83-3.36 0-6.085-2.743-6.085-6.126h-6.29c.01 1.043-.25 2.102-.81 3.07-1.68 2.907-5.41 3.896-8.34 2.21L.566 10.727c.905.515 1.69 1.268 2.246 2.234 1.677 2.904.673 6.624-2.24 8.32l3.145 5.447c.895-.522 1.935-.82 3.044-.82 3.35 0 6.066 2.725 6.083 6.092h6.29c-.004-1.035.258-2.08.81-3.04 1.676-2.902 5.4-3.893 8.325-2.218l3.145-5.447c-.9-.515-1.678-1.266-2.232-2.226zM16 22.48c-3.578 0-6.48-2.902-6.48-6.48S12.423 9.52 16 9.52c3.578 0 6.48 2.902 6.48 6.48s-2.902 6.48-6.48 6.48z"/></svg>
    
    </div>

  
   <h1 class="MainTitle">
         Saat ini sedang terjadi dilakukan upgrade pada jaringan server
      <!--  Server Under Maintenance -->
      </h1>
    <p class="MainDescription">
    <!-- Maaf, saat ini K-Net sedang mengalami kendala, silahkan coba beberapa saat lagi. Terimakasih atas pengertian dan kesabarannya. -->
    Mohon maaf, saat ini K-Net sedang dilakukan perbaikan dalam perangkat jaringan, silahkan coba beberapa saat lagi. Terimakasih atas pengertian dan kesabarannya.
     <!--  Maaf, saat ini K-Net sedang dalam proses maintenance hingga <b>Senin, 25 Februari 2019 pukul 06:00 WIB</b>. Terimakasih atas pengertian dan kesabarannya. -->
    </p>   
    
  </div>
 <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="25553668-2398-4d20-8486-ddfbdccb8f47";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();$crisp.push(["set", "session:segments", [["errorDB"]]])</script> 


</body>
</html>
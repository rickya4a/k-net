<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Title of the document</title>
		<link rel="stylesheet" href="<?php echo base_url('/asset/css/main.css'); ?>">
	</head>

	<body>
		<header>
			<h2>Styles Conference</h2>
			<h4>August 24&ndash;26th &mdash; Chicago, IL</h4>
		</header>
		<nav>
			<ul>
				<li>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">About</a>
				</li>
				<li>
					<a href="#">Career</a>
				</li>
				<li>
					<a href="#">Contact Us</a>
				</li>
			</ul>
		</nav>
		<section>
			<a href="speakers.html"> <h5>Speakers</h5> <h3>World-Class Speakers</h3> </a>
			<p>
				Joining us from all around the world are over twenty fantastic speakers, here to share their stories.
				<a class="btn btn-danger">Click here..</a>
			</p>
			
		</section>
		<footer>
			<p>
				Copyright@2015 by EDP BangBroz
			</p>
		</footer>
	</body>

</html>
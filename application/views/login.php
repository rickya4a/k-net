<div class="container-fluid">
  <div class="row">	
  	<h2>Login <?php echo $usr; ?> </h2> 
	 <form class="form-signin" method="POST" action="<?php echo $formAction; ?>">
	    <label for="username" class="sr-only">Username</label>
	    <div class="input-group col-sm-10">
	      <div class="input-group-addon">
	      	<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
	      </div>	
	      <input type="text" class="form-control" id="username" name="username" placeholder="Username">
	    </div>
	  
	    <label for="password" class="sr-only">Password</label>
	    <div class="input-group col-sm-10">
	      <div class="input-group-addon ">
	      	<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
	      </div>
	      <input type="password" class="form-control" id="password" name="password" placeholder="Password">
	    </div>
	    <div class="btn-group" role="group" aria-label="...">
            <button type="submit" class="btn btn-primary">Sign in</button>
            <button type="reset" class="btn btn-default">Clear Form</button>           
        </div>
     </form>
     <?php
       if(isset($err)) {
       	  echo "<div align=center class=\"alert alert-danger\" role=\"alert\"><strong>$err</strong></div>";
       }
     ?>
   
  </div>     
</div>		
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>WMS K-LINK</title>

    <!-- Bootstrap -->


    <!-- Bootstrap -->
    <link href="<?php echo site_url();?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url();?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url();?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo site_url();?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo site_url();?>vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo site_url();?>vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo site_url();?>vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="<?php echo site_url();?>vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!--buat scroll sidebar-->
    <link href="<?php echo site_url();?>vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>


    <!-- Datatables -->
    <link href="<?php echo site_url();?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url();?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo site_url();?>build/css/custom.min.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?php echo site_url();?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo site_url();?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo site_url();?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo site_url();?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo site_url();?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo site_url();?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo site_url();?>vendors/pdfmake/build/vfs_fonts.js"></script>



</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title " style="border: 0;">
                    <a  class="site_title" style="height: 200px;" ><span><img src="<?php echo base_url();?>images/k-link-logo.gif" class="mCS_img_loaded" style="margin-left: 60px;" height="88" width="81"></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->

                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">

                        <ul class="nav side-menu">
                            <?php
                            $master=0;
                            $yuu=$this->session->userdata('menu');
                            foreach ($yuu as $row) {
                                if($row->grup_menu==1)
                                    $master++;
                            }
                            ?>
                            <?php
                            if ($master>0)
                            echo '<li><a><i class="fa fa-home"></i> Master <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">';
                                ?>

                                    <?php
                                    $yuu=$this->session->userdata('menu');
                                    foreach ($yuu as $row) {
                                        if($row->grup_menu==1)
                                            echo '<li><a href="'.site_url().$row->link.'">'.$row->nama.'</a></li>';
                                    }
                                    ?>
                            <?php
                            if ($master>0)
                                echo '                               </ul>
                            </li>';
                            ?>
                            <?php
                            $po=0;
                            $yuu=$this->session->userdata('menu');
                            foreach ($yuu as $row) {
                                if($row->grup_menu==2)
                                    $po++;
                            }
                            ?>
                            <?php
                            if ($po>0)
                                echo '<li><a><i class="fa fa-shopping-cart"></i> Purchase Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">';
                            ?>

                                    <?php
                                    $yuu=$this->session->userdata('menu');
                                    foreach ($yuu as $row) {
                                        if($row->grup_menu==2)
                                            echo '<li><a href="'.site_url().$row->link.'">'.$row->nama.'</a></li>';
                                    }
                                    ?>



                            <?php
                            if ($po>0)
                                echo '                               </ul>
                            </li>';
                            ?>


                            <?php
                            $do=0;
                            $yuu=$this->session->userdata('menu');
                            foreach ($yuu as $row) {
                                if($row->grup_menu==3)
                                    $do++;
                            }
                            ?>
                            <?php
                            if ($do>0)
                                echo '<li><a><i class="fa fa-truck"></i> Delivery Order <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">';
                            ?>

                                    <?php
                                    $yuu=$this->session->userdata('menu');
                                    foreach ($yuu as $row) {
                                        if($row->grup_menu==3)
                                            echo '<li><a href="'.site_url().$row->link.'">'.$row->nama.'</a></li>';
                                    }
                                    ?>
                            <?php
                            if ($do>0)
                                echo '                               </ul>
                            </li>';
                            ?>                            <!--li><a><i class="fa fa-table"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="tables.html">Inventory Monitoring</a></li>
                                    <li><a href="tables_dynamic.html">Inventory Detail Monitoring</a></li>
                                    <li><a href="tables_dynamic.html">Product Bundling</a></li>
                                    <li><a href="tables_dynamic.html">Packing List</a></li>
                                </ul>
                            </li-->
                            <?php
                            $rpt=0;
                            $yuu=$this->session->userdata('menu');
                            foreach ($yuu as $row) {
                                if($row->grup_menu==4)
                                    $rpt++;
                            }
                            ?>
                            <?php
                            if ($rpt>0)
                                echo '<li><a><i class="fa fa-bar-chart"></i>Report <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">';
                            ?>

                                    <?php
                                    $yuu=$this->session->userdata('menu');
                                    foreach ($yuu as $row) {
                                        if($row->grup_menu==4)
                                            echo '<li><a href="'.site_url().$row->link.'">'.$row->nama.'</a></li>';
                                    }
                                    ?>

                            <?php
                            if ($rpt>0)
                                echo '                               </ul>
                            </li>';
                            ?>

                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" >
                        <span class="glyphicon " aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" >
                        <span class="glyphicon " aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" >
                        <span class="glyphicon " aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url();?>login/logout_user">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Welcome, <?php echo $this->session->userdata('NAME')?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>

                                <li><a href="<?php echo site_url();?>login/logout_user"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->

        <div class="right_col" role="main">

        <?php $this->load->view($main_view); ?>
        </div>



        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                WMS / K-LINK by GNVSolutions
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<!-- morris.js -->
<script src="<?php echo site_url();?>vendors/raphael/raphael.min.js"></script>
<script src="<?php echo site_url();?>vendors/morris.js/morris.min.js"></script>
<!-- ECharts -->
<script src="<?php echo site_url();?>vendors/echarts/dist/echarts.min.js"></script>
<script src="<?php echo site_url();?>vendors/echarts/map/js/world.js"></script>
<!-- Chart.js -->
<script src="<?php echo site_url();?>vendors/Chart.js/dist/Chart.min.js"></script>
<!-- FastClick -->
<script src="<?php echo site_url();?>vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo site_url();?>vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo site_url();?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo site_url();?>vendors/iCheck/icheck.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo site_url();?>vendors/moment/min/moment.min.js"></script>
<script src="<?php echo site_url();?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-wysiwyg -->
<script src="<?php echo site_url();?>vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo site_url();?>vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="<?php echo site_url();?>vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="<?php echo site_url();?>vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="<?php echo site_url();?>vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="<?php echo site_url();?>vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="<?php echo site_url();?>vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="<?php echo site_url();?>vendors/autosize/dist/autosize.min.js"></script>
<!-- jQuery autocomplete -->
<script src="<?php echo site_url();?>vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="<?php echo site_url();?>vendors/starrr/dist/starrr.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo site_url();?>build/js/custom.min.js"></script>

<script src="<?php echo site_url();?>/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<!-- validator -->
<script src="<?php echo site_url();?>vendors/validator/validator.js"></script>


<script src="<?php echo site_url();?>vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>


</body>
</html>

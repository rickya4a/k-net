<script>
    $(document).ready(function (){
        var table = $('#example').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('initiateDO2/getInitial');?>",
                dataType : 'json',
                data : {id:$('#ids').val()},

            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            }],
            'order': [[1, 'asc']]
        });

        // Handle click on "Select all" control
        $('#example-select-all').on('click', function(){
            // Get all rows with search applied
            var rows = table.rows({ 'search': 'applied' }).nodes();
            // Check/uncheck checkboxes for all rows in the table
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
        });

        // Handle click on checkbox to set state of "Select all" control
        $('#example tbody').on('change', 'input[type="checkbox"]', function(){
            // If checkbox is not checked
            if(!this.checked){
                var el = $('#example-select-all').get(0);
                // If "Select all" control is checked and has 'indeterminate' property
                if(el && el.checked && ('indeterminate' in el)){
                    // Set visual state of "Select all" control
                    // as 'indeterminate'
                    el.indeterminate = true;
                }
            }
        });

        // Handle form submission event
        $('#frm-example').on('submit', function(e){
            var form = this;

            // Iterate over all checkboxes in the table
            table.$('input[type="checkbox"]').each(function(){
                // If checkbox doesn't exist in DOM
                if(!$.contains(document, this)){
                    // If checkbox is checked
                    if(this.checked){
                        // Create a hidden element
                        $(form).append(
                            $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', this.name)
                                .val(this.value)
                        );
                    }
                }
            });
        });

        $('#TANGGAL_INITIATE').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#checkBtn').click(function() {
            checked = $("input[type=checkbox]:checked").length;

            if(!checked) {
                alert("You must check at least one checkbox.");
                return false;
            }

        });



    });


    function f1(objButton){

        {
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('initiateDO2/getDetKw');?>",
                dataType : 'json',
                data : {ID_KW:objButton.value},
                success: function(data){

                    $("#box-table-b").html(data.table);

                }
            });
        }
    }

</script>



<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    Initiate DO</li>
                <li class="active">
                    Form DO</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->
        <font color="red"><?php echo isset($msg)?$msg:''; ?></font>

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>Initiate DO</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">

                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <h4 class="modal-title" id="myModalLabel">Detail Kwitansi</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table width="90%" class="main table table-hover" id="box-table-b" name="box-table-b">
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="ids" value="<?php echo set_value('ids', isset($ids) ? $ids : ''); ?>" name="ids" required="required" class="form-control col-md-7 col-xs-12">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Initiate DO <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NO_INITIATE_DO" name="NO_INITIATE_DO" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NO_INITIATE_DO', isset($NO_INITIATE_DO) ? $NO_INITIATE_DO : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Initiate DO <span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control has-feedback-left" id="TANGGAL_INITIATE" name="TANGGAL_INITIATE" placeholder="First Name" aria-describedby="inputSuccess2Status3" type="text">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Warehouse</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_WAREHOUSE" name="ID_WAREHOUSE"  class="js-example-basic-single form-control" required="required">
                                        <option></option>
                                        <?php
                                        foreach($LIST_WAREHOUSE as $k):
                                            ?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"<?php echo set_select('ID_WAREHOUSE',$k->ID_WAREHOUSE,isset($ID_WAREHOUSE)&&($k->ID_WAREHOUSE == $ID_WAREHOUSE)? true : false); ?>><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Stockies <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ID_STOCKIES" name="ID_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ID_STOCKIES', isset($ID_STOCKIES) ? $ID_STOCKIES.' - '.$NAMA_STOCKIES : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Stockies <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NAMA_STOCKIES" name="NAMA_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NAMA_STOCKIES', isset($NAMA_STOCKIES) ? $NAMA_STOCKIES : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ALAMAT_STOCKIES" name="ALAMAT_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES', isset($ALAMAT_STOCKIES) ? $ALAMAT_STOCKIES : ''); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat 2<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ALAMAT_STOCKIES2" name="ALAMAT_STOCKIES2" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES2', isset($ALAMAT_STOCKIES2) ? $ALAMAT_STOCKIES2 : ''); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat 3<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ALAMAT_STOCKIES3" name="ALAMAT_STOCKIES3" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES3', isset($ALAMAT_STOCKIES3) ? $ALAMAT_STOCKIES3 : ''); ?>">
                                </div>
                            </div>
                            <table id="example" class="display select" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th><input name="select_all" value="1" id="example-select-all" type="checkbox"></th>
                                    <th>No Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>No DO</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="ln_solid"></div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url();?>initiateDO2" class="btn btn-primary">Cancel</a>
                                    <button type="submit" name="checkBtn" id="checkBtn" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


<!-- /page content -->
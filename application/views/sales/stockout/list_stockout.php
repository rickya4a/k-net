<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#nanas').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('stockout/getTable');?>",
                dataType : 'json',
                //  data : {id:$('#ids').val()},

            },
//            'columnDefs': [{
//                'targets': 0,
//                'searchable': false,
//                'orderable': false,
//                'className': 'dt-body-center',
//                'render': function (data, type, full, meta){
//                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
//                }
//            }],
            'order': [[0, 'asc']]
        });
//    $(document).ready(function(){
//
//		$('#nanas').DataTable( {
//            "processing": true, //Feature control the processing indicator.
//            "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
//
//            // Load data for the table's content from an Ajax source
//            "ajax": {
//                "url": "<?php //echo base_url(); ?>//stockout/getTable",
//                "type": "POST"
//            },
//
//            //Set column definition initialisation properties.
//            "columnDefs": [
////			{ "name": "no",   "targets": 0 },
//                { "name": "warehouse",  "targets": 1 },
//                { "name": "category_name", "targets": 2 },
//                { "name": "date",  "targets": 3 },
//                { "name": "keluar",  "targets": 4 },
//                { "name": "module",  "targets": 5 },
//
//
//                {
//                    "targets": [ -1 ], //last column
//                    "orderable": false, //set not orderable
//                    "destroy": true
//                }
//            ]
//        } );
		
        $('#tgl_awal').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
      			format: 'DD-MM-YYYY'
    		}
        },
        
     	function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        
        $('#tgl_akhir').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
      			format: 'DD-MM-YYYY'
    		}
        }, 
        
        function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        
        $('#submit').click(function(){


		 $.ajax({
        	   	type : "POST",
        	   	url  : "<?php echo site_url('stockout/closing');?>",
        	   	dataType : 'json',

        	   	beforeSend : function(){
        	   		$('#loading').show();
        	   		$( "#submit" ).prop( "disabled", true );
        	   	},
        	   	success: function(data){ 
					alert("Berhasil closing stock");
                },
               complete: function(){
                   $('#loading').hide();
                   $( "#submit" ).prop( "disabled", true );
                   alert("Berhasil closing stock");

                   //document.location.href = "<?php echo base_url().'stockout'?>";
               }
        });
	 });
     })
</script>

<!DOCTYPE html>
<!-- page content -->
    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>#">
                        Home</a>
                </li>
                <li class="active">
                    Master</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1> <i class="fa fa-map-marker fa-lg"></i> Closing Stock</h1>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            	<form id="pinaple" data-parsley-validate class="form-horizontal form-label-left">
            		<span id="loading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" style="display: none"></span>
					<button type="button" id="submit" class="btn btn-success">Closing Stock</button>

				</form>

            </div>
        </div>
    </div>
    </div>




    </div>

<!-- /page content -->
<script>

    $(document).ready(function(){
        //alert('Qty pcs melebihi pesanan');

       // table.destroy();


        var table = $('#box-table-b').DataTable({
            'ajax': {
                type : "POST",
                url  : "<?php echo site_url('Summary/getTable');?>",
                dataType : 'json',
               // data : {ID_WAREHOUSE:$('#ID_WAREHOUSE').val(),CATEGORY:$('#CATEGORY').val() },

            },
            'order': [[0, 'asc']]
        });
        //table.destroy();
    });




</script>
<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>

                <li class="active">
                    Summary</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-map-marker fa-lg"></i> <b>Report Summary</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>" data-parsley-validate class="form-horizontal form-label-left">

                            <table width="100%" class="main table table-hover" id="box-table-b">
                                <thead>
                                <tr>
                                    <th><b>Kode Produk</b></th>
                                    <th><b>Produk</b></th>
                                    <th><b>Quantity</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>



                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Cetak</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
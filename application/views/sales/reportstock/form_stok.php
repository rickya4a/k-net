<script>
    var table = $('#box-table-b').DataTable( {
        paging: false,
        order: false
    } );

    function fun() {
        //alert('Qty pcs melebihi pesanan');

        table.destroy();


        table = $('#box-table-b').DataTable({
            'ajax': {
                type : "POST",
                url  : "<?php echo site_url('Reportstock/getTable');?>",
                dataType : 'json',
                data : {ID_WAREHOUSE:$('#ID_WAREHOUSE').val(),CATEGORY:$('#CATEGORY').val(),ORDER:$('#ORDER').val(),ASC:$('#ASC').val() },

            },
            order: false

        });
        //table.destroy();
    }




</script>
<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>

                <li class="active">
                    Report Inventory</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-map-marker fa-lg"></i> <b>Report Inventory</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>" data-parsley-validate class="form-horizontal form-label-left" target="_blank">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_WAREHOUSE"  name="ID_WAREHOUSE" class="form-control" data-bvalidator="required">
                                        <?php foreach($LIST_WAREHOUSE as $row): ?>
                                            <option value="<?php echo $row->ID_WAREHOUSE;?>"><?php echo $row->WAREHOUSE_NAME;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Search by</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="CATEGORY"  name="CATEGORY" class="form-control" data-bvalidator="required">
                                        <option value="0">Rack</option>
                                        <option value="1">Location</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Order By</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ORDER"  name="ORDER" class="form-control" data-bvalidator="required">
                                        <option value="B.RACK_NAME">Rack/Location</option>
                                        <option value="D.PRODUCT_NAME">Produk</option>
                                        <option value="B.MAX">MAX </option>
                                        <option value="B.MIN">MIN</option>
                                        <option value="A.QTY">Quantity</option>
                                        <option value="A.EXP_PABRIK">EXP Pabrik</option>
                                        <option value="A.EXP_GUDANG">EXP Gudang</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ASC"  name="ASC" class="form-control" data-bvalidator="required">
                                        <option value="ASC">ASC</option>
                                        <option value="DESC">DESC</option>


                                    </select>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-success" id="test" name="test" onclick="fun()">Submit</button>
                                </div>
                            </div>

                            <table width="100%" class="main table table-hover" id="box-table-b">
                                <thead>
                                <tr>
                                    <th><b>Rack/Lokasi</b></th>
                                    <th><b>Produk</b></th>
                                    <th><b>MAX</b></th>
                                    <th><b>MIN</b></th>
                                    <th><b>Quantity</b></th>
                                    <th><b>EXP Pabrik</b></th>
                                    <th><b>EXP Gudang</b></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>



                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Cetak</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
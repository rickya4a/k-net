<script>
    $(document).ready(function() {

        var table = $('#produk').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('Goodreturn/getTable');?>",
                dataType : 'json',
                //  data : {id:$('#ids').val()},

            },
//            'columnDefs': [{
//                'targets': 0,
//                'searchable': false,
//                'orderable': false,
//                'className': 'dt-body-center',
//                'render': function (data, type, full, meta){
//                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
//                }
//            }],
            'order': [[0, 'asc']]
        });

//        $('#produk').DataTable( {
//            "processing": true, //Feature control the processing indicator.
//            "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
//
//            // Load data for the table's content from an Ajax source
//            "ajax": {
//                "url": "<?php //echo base_url(); ?>//goodreturn/getTable",
//                "type": "POST"
//            },
//
//            //Set column definition initialisation properties.
//            "columnDefs": [
////			{ "name": "no",   "targets": 0 },
//                { "name": "NO_GOODRT", "targets": 1 },
//                { "name": "WAREHOUSE_NAME",  "targets": 2 },
//                { "name": "PRODUCT_NAME",  "targets": 3 },
//                { "name": "CREATED_DATE",  "targets": 3 },
//
//
//                {
//                    "targets": [ -1 ], //last column
//                    "orderable": false, //set not orderable
//                    "destroy": true
//                }
//            ]
//        } );
    } );
</script>
<!DOCTYPE html>
<!-- page content -->
    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="active">
                    Good Return</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1> <i class="fa fa-cubes fa-lg"></i> Daftar <b>Good Return</b></h1>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-group">
                    <?php echo $add; ?>
                </div>
                <table id="produk" class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <th>Nomor</th>
                        <th>No Good Return</th>
                        <th>Warehouse</th>
                        <th>Nama Produk</th>

                        <th>Tanggal Entry</th>
                        <th>Action</th>

                    </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>




    </div>

<!-- /page content -->
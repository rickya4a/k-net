<script>
    $(document).ready(function(){


        $('#ID_WAREHOUSE').change(function(){
                if($('#ID_WAREHOUSE').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('goodreturn/getDataProduk');?>",
                        dataType : 'json',
                        data : {ID_WAREHOUSE:$('#ID_WAREHOUSE').val()},
                        success: function(data){
                            $("#ID_PRODUCT").html(data.option);
                        }
                    });
                }
            }
        );

        $('#ID_PRODUCT').change(function(){
                if($('#ID_PRODUCT').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('goodreturn/getDataLocation');?>",
                        dataType : 'json',
                        data : {DAMAGED:0,ID_WAREHOUSE:$('#ID_WAREHOUSE').val(),ID_PRODUCT:$('#ID_PRODUCT').val()},
                        success: function(data){
                            $("#ID_RACK").html(data.option);
                        }
                    });
                }
            }
        );

        $('#EXP_BATCH').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#EXP_PABRIK').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#EXP_GUDANG').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#DAMAGED').change(function(){
                if(document.getElementById("DAMAGED").checked){
                    var x=1;
                }
                else var x=0;
                ;
                if($('#DAMAGED').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('goodreturn/getDataLocation');?>",
                        dataType : 'json',
                        data : {DAMAGED:x,ID_WAREHOUSE:$('#ID_WAREHOUSE').val(),ID_PRODUCT:$('#ID_PRODUCT').val()},
                        success: function(data){
                            $("#ID_RACK").html(data.option);
                        }
                    });
                }
            }
        );


    });

</script>
<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    Good Return</li>
                <li class="active">
                    Form Good Return</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>Good Return</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Good Return <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NO_GOODRT" value="<?php echo set_value('NO_GOODRT', isset($NO_GOODRT) ? $NO_GOODRT : ''); ?>" name="NO_GOODRT" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_WAREHOUSE" name="ID_WAREHOUSE" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_WAREHOUSE as $k):?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"<?php echo set_select('ID_WAREHOUSE',$k->ID_WAREHOUSE,isset($ID_WAREHOUSE)&&($k->ID_WAREHOUSE == $ID_WAREHOUSE)? true : false); ?>><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Produk</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_PRODUCT" name="ID_PRODUCT" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_PRODUK as $k):?>
                                            <option value="<?php echo $k->ID_PRODUCT; ?>"<?php echo set_select('ID_PRODUCT',$k->ID_PRODUCT,isset($ID_PRODUCT)&&($k->ID_PRODUCT == $ID_PRODUCT)? true : false); ?>><?php echo $k->PRODUCT_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Qty <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="QTY" value="<?php echo set_value('QTY', isset($QTY) ? $QTY : ''); ?>" name="QTY" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox">
                                        <label>
                                            <input name="DAMAGED" id="DAMAGED" type="checkbox" value="1"   <?php
                                            if (isset($DAMAGED) ? $DAMAGED : ''  !=0)
                                            {
                                                echo "checked";
                                            }
                                            else
                                            {
                                                echo " ";
                                            }
                                            ?>> <B>Damaged</B>
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_RACK" name="ID_RACK" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_RACK as $k):?>
                                            <option value="<?php echo $k->ID_RACK; ?>"<?php echo set_select('ID_RACK',$k->ID_RACK,isset($ID_RACK)&&($k->ID_RACK == $ID_RACK)? true : false); ?>><?php echo $k->RACK_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Batch<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NO_BATCH" name="NO_BATCH"  value="<?php echo set_value('NO_BATCH', isset($NO_BATCH) ? $NO_BATCH : ''); ?>"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Batch <span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control has-feedback-left" id="EXP_BATCH" name="EXP_BATCH" placeholder="First Name" aria-describedby="inputSuccess2Status3" type="text" value="<?php echo set_value('EXP_BATCH', isset($EXP_BATCH) ? $EXP_BATCH : ''); ?>"  >
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">EXP. Pabrik <span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control has-feedback-left" id="EXP_PABRIK" name="EXP_PABRIK" placeholder="First Name" aria-describedby="inputSuccess2Status3" type="text" value="<?php echo set_value('EXP_PABRIK', isset($EXP_PABRIK) ? $EXP_PABRIK : ''); ?>">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">EXP. Gudang <span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control has-feedback-left" id="EXP_GUDANG" name="EXP_GUDANG" placeholder="First Name" aria-describedby="inputSuccess2Status3" type="text" value="<?php echo set_value('EXP_GUDANG', isset($EXP_GUDANG) ? $EXP_GUDANG : ''); ?>">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url();?>goodreturn" class="btn btn-primary">Cancel</a>
                                    <?php echo ( isset($buton) ? $buton : ''); ?>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
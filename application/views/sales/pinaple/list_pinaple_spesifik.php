<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#nanas').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('Pinaple/getTable');?>",
                dataType : 'json',
                //  data : {id:$('#ids').val()},

            },
//            'columnDefs': [{
//                'targets': 0,
//                'searchable': false,
//                'orderable': false,
//                'className': 'dt-body-center',
//                'render': function (data, type, full, meta){
//                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
//                }
//            }],
            'order': [[0, 'asc']]
        });
//		$('#nanas').DataTable( {
//            "processing": true, //Feature control the processing indicator.
//            "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
//
//            // Load data for the table's content from an Ajax source
//            "ajax": {
//                "url": "<?php //echo base_url(); ?>//pinaple/getTable",
//                "type": "POST"
//            },
//
//            //Set column definition initialisation properties.
//            "columnDefs": [
////			{ "name": "no",   "targets": 0 },
//                { "name": "ID_STOCKIES",  "targets": 1 },
//                { "name": "KWITANSI_NO", "targets": 2 },
//                { "name": "PRODUK_ALIAS_ID",  "targets": 3 },
//                { "name": "QTY",  "targets": 4 },
//                { "name": "TRANSAKSI_DATE",  "targets": 5 },
//
//
//                {
//                    "targets": [ -1 ], //last column
//                    "orderable": false, //set not orderable
//                    "destroy": true
//                }
//            ]
//        } );
//
        $('#tgl_awal').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_3",
                locale: {
                    format: 'DD-MM-YYYY'
                }
            },

            function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

        $('#tgl_akhir').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_3",
                locale: {
                    format: 'DD-MM-YYYY'
                }
            },

            function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

        $('#submit').click(function(){
            var isi = {NOKW:$('#NOKW').val()};
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('pinaple/getDataSpesifik');?>",
                dataType : 'json',
                data : isi,
                beforeSend : function(){
                    $('#loading').show();
                    $( "#submit" ).prop( "disabled", true );
                },
                success: function(data){
                    if(data.pesan == 'Data berhasil di simpan sebanyak')
                    {
                        //alert(data.pesan);

                        //document.location.href = "<?php echo base_url().'pinaple/view2'?>";
                    }else{
                       // alert(data.pesan);
                    }
                },
                complete: function(){
                    $('#loading').hide();
                    $( "#submit" ).prop( "disabled", false );
                    //document.location.href = "<?php echo base_url().'pinaple/view2'?>";
                }
            });
        });



    })
</script>

<!DOCTYPE html>
<!-- page content -->
<div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>#">
                        Home</a>
                </li>
                <li class="active">
                    Master</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h1> <i class="fa fa-map-marker fa-lg"></i> Import Data</h1>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br id="pinaple" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NO KW<span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <input type="text" id="NOKW"  name="NOKW" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <span id="loading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" style="display: none"></span>
                            <button type="button" id="submit" class="btn btn-success">Get From Pinaple</button>
                        </div>
                    </div>
                    &nbsp;

                    <br>
                    </form>
                    <br>

                    <table id="nanas" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Stockiest</th>
                            <th>Kwitansi No</th>
                            <th>Inventory Name</th>
                            <th>Total</th>
                            <th>Transaction Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>




</div>

<!-- /page content -->
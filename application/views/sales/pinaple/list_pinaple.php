<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#nanas').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('Pinaple/getTable');?>",
                dataType : 'json',
                //  data : {id:$('#ids').val()},

            },
//            'columnDefs': [{
//                'targets': 0,
//                'searchable': false,
//                'orderable': false,
//                'className': 'dt-body-center',
//                'render': function (data, type, full, meta){
//                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
//                }
//            }],
            'order': [[0, 'asc']]
        });
//		$('#nanas').DataTable( {
//            "processing": true, //Feature control the processing indicator.
//            "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
//
//            // Load data for the table's content from an Ajax source
//            "ajax": {
//                "url": "<?php //echo base_url(); ?>//pinaple/getTable",
//                "type": "POST"
//            },
//
//            //Set column definition initialisation properties.
//            "columnDefs": [
////			{ "name": "no",   "targets": 0 },
//                { "name": "ID_STOCKIES",  "targets": 1 },
//                { "name": "KWITANSI_NO", "targets": 2 },
//                { "name": "PRODUK_ALIAS_ID",  "targets": 3 },
//                { "name": "QTY",  "targets": 4 },
//                { "name": "TRANSAKSI_DATE",  "targets": 5 },
//
//
//                {
//                    "targets": [ -1 ], //last column
//                    "orderable": false, //set not orderable
//                    "destroy": true
//                }
//            ]
//        } );
//
        $('#tgl_awal').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_3",
                locale: {
                    format: 'DD-MM-YYYY'
                }
            },

            function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

        $('#tgl_akhir').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_3",
                locale: {
                    format: 'DD-MM-YYYY'
                }
            },

            function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

        $('#submit').click(function(){
            var isi = {tgl_awal:$('#tgl_awal').val(),tgl_akhir:$('#tgl_akhir').val(),IS_ACTIVE:$('#IS_ACTIVE').val()};
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('pinaple/getData');?>",
                dataType : 'json',
                data : isi,
                beforeSend : function(){
                    $('#loading').show();
                    $( "#submit" ).prop( "disabled", true );
                },
                success: function(data){
                    if(data.pesan == 'Data berhasil di simpan sebanyak')
                    {
                        alert(data.pesan);

                        //document.location.href = "<?php echo base_url().'pinaple'?>";
                    }else{
                        alert(data.pesan);
                    }
                },
                complete: function(){
                    $('#loading').hide();
                    $( "#submit" ).prop( "disabled", false );
                    document.location.href = "<?php echo base_url().'pinaple'?>";
                }
            });
        });



    })
</script>

<!DOCTYPE html>
<!-- page content -->
<div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>#">
                        Home</a>
                </li>
                <li class="active">
                    Master</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h1> <i class="fa fa-map-marker fa-lg"></i> Import Data</h1>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br id="pinaple" data-parsley-validate class="form-horizontal form-label-left">
                    <span id="loading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" style="display: none"></span>
                    <button type="button" id="submit" class="btn btn-success">Get From Pinaple</button>
                    Tanggal : <input type="text" name="tgl_awal" id="tgl_awal" size="10" style="text-align: center"/> s/d <input type="text" name="tgl_akhir" id="tgl_akhir" size="10" style="text-align: center" />
                    &nbsp;
                    &nbsp;
                    <?php echo $add; ?>
                    <br>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="IS_ACTIVE" required="required" name="IS_ACTIVE" class="form-control" data-bvalidator="required">
                                <option value="0" <?php echo set_select('IS_ACTIVE',0,isset($IS_ACTIVE)&&(0 == $IS_ACTIVE)? true : false); ?>>IMPORT</option>
                                <option value="1" <?php echo set_select('IS_ACTIVE',1,isset($IS_ACTIVE)&&(1 == $IS_ACTIVE)? true : false); ?>>RE-IMPORT</option>


                            </select>
                        </div>
                    &nbsp;

                    <br>

                    </form>
                    <br>
                    <?php echo $spek; ?>


                    <br>

                    <table id="nanas" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Stockiest</th>
                            <th>Kwitansi No</th>
                            <th>Inventory Name</th>
                            <th>Total</th>
                            <th>Transaction Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>




</div>

<!-- /page content -->
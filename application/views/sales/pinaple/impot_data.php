<script>
 $(document).ready(function (){
	  $('#submit').click(function() {
		  var count = $("#count").val();
		  var k = 1;
		  var ceknama = '';
		  var cekberat = '';
		  var cekuom = '';
		  var cekbox = '';
		  var cekbundling = '';
		  var msg = '';
		  var allArray = new Array();
		  for(var i=0; i < count; i++)
		  {
			  if($("#produk_"+k).val() == ''){
			  	if($("#nama_"+k).val() == '')
			  	{
			  		ceknama = ' Nama Produk Harus di Isi,';
			  	}
			  	
			  	if($("#berat_"+k).val() == '')
			  	{
			  		cekberat = ' Berat Harus di Isi,';
			  	}
			  	
			  	if($("#uom_"+k).val() == '')
			  	{
			  		cekuom = ' Satuan Harus di Isi,';
			  	}
			  	
			  	if($("#box_"+k).val() == '')
			  	{
			  		cekbox = ' Units/Box Harus di Isi,';
			  	}
			  	
			  	if($("#bundling_"+k).val() == '')
			  	{
			  		cekbundling = ' Bundling Produk Harus di Isi,';
			  	}
			  	
			  	if(($("#nama_"+k).val() == '')||( $("#berat_"+k).val() == '')||($("#uom_"+k).val() == '')||($("#box_"+k).val() == '')||($("#bundling_"+k).val() == ''))
			  	{
			  		msg = "Pada baris ke "+k+ceknama+cekberat+cekuom+cekbox+cekbundling;
			  	}
			  	
			  }else{
			  	if($("#nama_"+k).val() == '')
			  	{
			  		msg = "Pada baris ke "+k+" Nama Produk Harus di isi ";
			  	}
			  } 
			  
			  if(msg != '')
			  {
			  	alert(msg);
			  }else{
			  	 if($("#produk_"+k).val() == ''){
					var valueToPush = {"parent":$("#produk_"+k).val(),"kode":$("#kode_"+k).val(),"nama":$("#nama_"+k).val(),"berat":$("#berat_"+k).val(),"uom":$("#uom_"+k).val(),"box":$("#box_"+k).val(),"bundling":$("#bundling_"+k).val()}
				}else{
					var valueToPush = {"parent":$("#produk_"+k).val(),"kode":$("#kode_"+k).val(),"nama":$("#nama_"+k).val(),"berat":"","uom":"","box":"","bundling":$("#bundling_"+k).val()};
				}
				
				allArray.push(valueToPush);
			  }
			  
			  k++;
		  }
		  
		  if(msg =='')
		  {
		  	 $.ajax({
		  	 	type : "POST",
        	   	url  : "<?php echo site_url('pinaple/saveData');?>",
        	   	dataType : 'json',
        	   	data : {data:allArray},
        	   	success: function(data){
        	   		document.location.href = "<?php echo base_url().'pinaple'?>";
        	   	}
		  	 });
		  }
		  
	  });
 });
 
 function cekProduk(i){
	 var produk = $("#produk_"+i).val();
	 if(produk == '')
	 {
		 $("#berat_"+i).removeAttr('readonly');
		 $("#box_"+i).removeAttr('readonly');
		 $("#uom_"+i).removeAttr("disabled") 
	 }else{
		 $("#berat_"+i).attr('readonly','readonly');
		 $("#box_"+i).attr('readonly','readonly');
		 $("#uom_"+i).attr("disabled","disabled")
	 }
 }
 
</script>
<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>Pinaple">
                    Pinaple</li>
                <li class="active">
                    Import Produk</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-cubes fa-lg"></i>Import Produk</h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action=""  data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label> <b>Produk </b>
                                </label>
                            </div>
                            <table id="table" class="display select" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                  <th width="40">No</th>
                                  <th width="250">Parent</th>
                                  <th width="150">Kode</th>
                                  <th width="200">Berat</th>
                                  <th width="150">Unist/Box</th>
                                  <th width="150">Bundling Produk</th>
                                </tr>
                                </thead>
                                <tbody>
                                	<?php 
										$i = 1;
										$o = count($list);
									?>
                                    <input type="hidden" name="count" id="count" value="<?php echo $o; ?>">
                                    <?php for($w = 0;$w < $o;$w++){ ?>
                                       <tr>
                                          <td rowspan="2"><?php echo $i; ?></td>
                                          <td>
                                            <select id="produk_<?php echo $i; ?>" name="produk[]"  class="js-example-basic-single form-control" onChange="cekProduk(<?php echo $i; ?>)">
                                                <option value="">SELF</option>
                                                <?php
                                                foreach($list_produk as $k):
                                                    ?>
                                                    <option value="<?php echo $k->ID_PRODUCT; ?>"<?php echo set_select('ID_PRODUCT',$k->ID_PRODUCT,isset($ID_PRODUCT)&&($k->ID_PRODUCT == $ID_PRODUCT)? true : false); ?>><?php echo $k->PRODUCT_CODE.' - '.$k->PRODUCT_NAME; ?></option>
                                                <?php endforeach;  ?>
                                            </select>
                                          </td>
                                          <td><input type="text" name="kode[]" id="kode_<?php echo $i; ?>" value="<?php echo $list[$w]->KODE; ?>" size="10" readonly></td>
                                          <td>
                                            <input type="text" name="berat[]" id="berat_<?php echo $i; ?>" value="" maxlength="5" size="5">
                                            <select id="uom_<?php echo $i; ?>" name="uom[]" class="form">
                                                <option value=""></option>
                                                <?php foreach($list_uom as $k):?>
                                                    <option value="<?php echo $k->ID_UOM; ?>"<?php echo set_select('ID_UOM',$k->ID_UOM,isset($ID_UOM)&&($k->ID_UOM == $ID_UOM)? true : false); ?>><?php echo $k->NAMA_UOM; ?></option>
                                                <?php endforeach;  ?>
                                            </select>
                                          </td>
                                          <td><input type="text" name="box[]" id="box_<?php echo $i; ?>" value="" maxlength="7" size="5"></td>
                                          <td>
                                          	<select id="bundling_<?php echo $i; ?>" name="bundling[]" class="form">
                                          		<option value=""></option>
                                          		<option value="1">Ya</option>
                                          		<option value="0">Tidak</option>
                                          	</select>	
                                          </td>
                                       </tr>
                                       <tr>
                                         <td><b><center>Nama Produk</center></b></td>
                                         <td colspan="3"><input type="text" name="nama[]" id="nama_<?php echo $i; ?>" value="" size="80" maxlength="80"></td>
                                       </tr>
                                      <?php 
										$i++;
									  } ?>
                                </tbody>
                            </table>
                       
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url();?>Pinaple" class="btn btn-primary">Cancel</a>
									<?php if($o > 0){ ?>
                                    <button type="button" id="submit"  class="btn btn-success">Submit</button>
                                    <?php } ?>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
<script>
    var table = $('#box-table-b').DataTable( {
        paging: false
    } );

    function fun() {
        //alert('Qty pcs melebihi pesanan');

        table.destroy();


        table = $('#box-table-b').DataTable({
            'ajax': {
                type : "POST",
                url  : "<?php echo site_url('Livestock/getTable');?>",
                dataType : 'json',
                data : {ID_WAREHOUSE:$('#ID_WAREHOUSE').val() },

            },
            'order': [[0, 'asc']]
        });
        //table.destroy();
    }




</script>
<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>

                <li class="active">
                    Report Live Stock</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-map-marker fa-lg"></i> <b>Report Live Stock</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_WAREHOUSE"  name="ID_WAREHOUSE" class="form-control" data-bvalidator="required">
                                        <option value="all">Semua Warehouse</option>
                                        <?php foreach($LIST_WAREHOUSE as $row): ?>
                                            <option value="<?php echo $row->ID_WAREHOUSE;?>"><?php echo $row->WAREHOUSE_NAME;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>




                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-success" id="test" name="test" onclick="fun()">Submit</button>
                                </div>
                            </div>

                            <table width="100%" class="main table table-hover" id="box-table-b">
                                <thead>
                                <tr>
                                    <th><b>Rack/Lokasi</b></th>
                                    <th><b>Produk</b></th>
                                    <th><b>Masuk</b></th>
                                    <th><b>Keluar</b></th>
                                    <th><b>Quantity</b></th>

                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>



                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <!--button type="submit" class="btn btn-success">Cetak</button-->
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
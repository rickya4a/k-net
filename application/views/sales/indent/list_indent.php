<script>
    $(document).ready(function() {
        var table = $('#stockies').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('indent/getTable');?>",
                dataType : 'json',
              //  data : {id:$('#ids').val()},

            },
//            'columnDefs': [{
//                'targets': 0,
//                'searchable': false,
//                'orderable': false,
//                'className': 'dt-body-center',
//                'render': function (data, type, full, meta){
//                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
//                }
//            }],
            'order': [[0, 'asc']]
        });


//        $('#stockies').DataTable( {
//            "processing": true, //Feature control the processing indicator.
//            "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
//
//            // Load data for the table's content from an Ajax source
//            "ajax": {
//
//                "url": "<?php //echo base_url(); ?>//stockies/getTable",
//                "dataType" : 'json',
//
//                "type": "POST"
//            },
//
//            //Set column definition initialisation properties.
//
//        } );
    } );
</script>
<!DOCTYPE html>
<!-- page content -->
    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="active">
                    Indent</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1> <i class="fa fa-users fa-lg"></i> List <b>Indent</b></h1>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="stockies" class="table table-striped table-bordered">
                    <thead>
                    <tr>


                        <th>Nomor</th>
                        <th>Nama Stockies</th>
                        <th>Kode Stockies</th>
                        <th>Alamat</th>


                        <th>Action</th>

                    </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>




    </div>

<!-- /page content -->
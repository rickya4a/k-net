<script>
    $(document).ready(function (){
        var table = $('#example').DataTable({
            'ajax': {
                type : "POST",

                url  : "<?php echo site_url('indent/getInitialnew');?>",
                dataType : 'json',
                data : {id:$('#ids').val()},

            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            }],
            'order': [[1, 'asc']]
        });

        // Handle click on "Select all" control
        $('#example-select-all').on('click', function(){
            // Get all rows with search applied
            var rows = table.rows({ 'search': 'applied' }).nodes();
            // Check/uncheck checkboxes for all rows in the table
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
        });

        // Handle click on checkbox to set state of "Select all" control
        $('#example tbody').on('change', 'input[type="checkbox"]', function(){
            // If checkbox is not checked
            if(!this.checked){
                var el = $('#example-select-all').get(0);
                // If "Select all" control is checked and has 'indeterminate' property
                if(el && el.checked && ('indeterminate' in el)){
                    // Set visual state of "Select all" control
                    // as 'indeterminate'
                    el.indeterminate = true;
                }
            }
        });

        // Handle form submission event
        $('#frm-example').on('submit', function(e){
            var form = this;

            // Iterate over all checkboxes in the table
            table.$('input[type="checkbox"]').each(function(){
                // If checkbox doesn't exist in DOM
                if(!$.contains(document, this)){
                    // If checkbox is checked
                    if(this.checked){
                        // Create a hidden element
                        $(form).append(
                            $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', this.name)
                                .val(this.value)
                        );
                    }
                }
            });
        });

        $('#TANGGAL_INITIATE').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#checkBtn').click(function() {
            checked = $("input[type=checkbox]:checked").length;

            if(!checked) {
                alert("You must check at least one checkbox.");
                return false;
            }

        });



    });


    function f1(objButton){

        {
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('indent/getDetKwNew');?>",
                dataType : 'json',
                data : {ID_KW:objButton.value,ids:$('#ids').val()},
                success: function(data){

                    $("#box-table-b").html(data.table);

                }
            });
        }
    }

</script>



<!DOCTYPE html>
<!-- page content -->



<div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    Indent</li>
                <li class="active">
                    Form Indent</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>Indent</b></h1>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">

                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">

                                        <h4 class="modal-title" id="myModalLabel">Detail Kwitansi</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table width="90%" class="main table table-hover" id="box-table-b" name="box-table-b">
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="ids" value="<?php echo set_value('ids', isset($ids) ? $ids : ''); ?>" name="ids" required="required" class="form-control col-md-7 col-xs-12">


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Stockies <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="NAMA_STOCKIES" name="NAMA_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NAMA_STOCKIES', isset($NAMA_STOCKIES) ? $NAMA_STOCKIES : ''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Stockies <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="ID_STOCKIES" name="ID_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ID_STOCKIES', isset($ID_STOCKIES) ? $ID_STOCKIES : ''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="ALAMAT_STOCKIES" name="ALAMAT_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES', isset($ALAMAT_STOCKIES) ? $ALAMAT_STOCKIES : ''); ?>">
                            </div>
                        </div>
                        <table id="example" class="display select" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><input name="select_all" value="1" id="example-select-all" type="checkbox"></th>
                                <th>Product Alias Code</th>
                                <th>Product Alias Name</th>
                                <th>Qty sisa</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="<?php echo site_url();?>indent" class="btn btn-primary">Back</a>
                              <!--  <button type="submit" name="checkBtn" id="checkBtn" class="btn btn-success">Submit</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- /page content -->
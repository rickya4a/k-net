<script>
    $(document).ready(function() {


        $('#tgl_awal').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#tgl_akhir').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });



    });

    //    $(document).ready(function() {
//
//        var table = $('#kota').DataTable({
//            'ajax': {
//                type : "POST",
//
//                url  : "<?php //echo site_url('viewDO/getTable');?>//",
//                dataType : 'json',
//            },
//            'order': [[0, 'asc']]
//        });
//
//    } );

var table = $('#box-table-b').DataTable( {
    paging: false,
} );

function fun() {
    //alert('Qty pcs melebihi pesanan');

    table.destroy();


    table = $('#box-table-b').DataTable({
        'ajax': {
            type : "POST",
            url  : "<?php echo site_url('viewDO/getTable');?>",
            dataType : 'json',
            data : {tgl_awal:$('#tgl_awal').val(),tgl_akhir:$('#tgl_akhir').val() },

        },

    });
    //table.destroy();
}

</script>
<!DOCTYPE html>
<!-- page content -->
    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="active">
                    View DO</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1> <i class="fa fa-compass fa-lg"></i> Daftar <b>DO</b></h1>

                <div class="clearfix"></div>
            </div>
<!--            <div class="x_content">-->
<!---->
<!--                <table id="kota" class="table table-striped table-bordered">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!---->
<!--                        <th>Nomor</th>-->
<!--                        <th>No DO</th>-->
<!--                        <th>Nama Stockies</th>-->
<!--                        <th>Warehouse</th>-->
<!---->
<!--                        <th>Tanggal</th>-->
<!--                        <th>No KW</th>-->
<!---->
<!--                        <th>Action</th>-->
<!---->
<!--                    </tr>-->
<!--                    </thead>-->
<!---->
<!---->
<!--                    <tbody>-->
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->

            <div class="x_content">
                <br />
                <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>" data-parsley-validate class="form-horizontal form-label-left" target="_blank">

                    <div class="form-group">
                        Tanggal : <input type="text" name="tgl_awal" id="tgl_awal" size="10" style="text-align: center"/> s/d <input type="text" name="tgl_akhir" id="tgl_akhir" size="10" style="text-align: center" />
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-success" id="test" name="test" onclick="fun()">Submit</button>
                        </div>
                    </div>

                    <table width="100%" class="main table table-hover" id="box-table-b">
                        <thead>
                        <tr>
                            <th><b>Nomor</b></th>
                            <th><b>No DO</b></th>
                            <th><b>Nama Stockies</b></th>
                            <th><b>Warehouse</b></th>
                            <th><b>Tanggal</b></th>
                            <th><b>No KW</b></th>
                            <th><b>Action</b></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>



                </form>
            </div>


        </div>
    </div>
    </div>




    </div>

<!-- /page content -->
<script>
    $('#TANGGAL_DO').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_3",
        locale: {
            format: 'DD-MM-YYYY'
        }
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

</script>
<!DOCTYPE html>
<!-- page content -->



<div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    View DO</li>
                <li class="active">
                    Detail DO</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>DO</b></h1>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">



                        <input type="hidden" id="ids" value="<?php echo set_value('ids', isset($ids) ? $ids : ''); ?>" name="ids" required="required" class="form-control col-md-7 col-xs-12">






                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No DO <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="NO_DO" name="NO_DO" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NO_DO', isset($NO_DO) ? $NO_DO : ''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NAMA COURIER <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="NAMA_COURIER" name="NAMA_COURIER" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NAMA_COURIER', isset($NAMA_COURIER) ? $NAMA_COURIER : '-'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="CODE_STOCKIES" name="CODE_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('CODE_STOCKIES', isset($CODE_STOCKIES) ? $CODE_STOCKIES : '-'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="NAMA_STOCKIES" name="NAMA_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NAMA_STOCKIES', isset($NAMA_STOCKIES) ? $NAMA_STOCKIES : '-'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="ALAMAT_STOCKIES" name="ALAMAT_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES', isset($ALAMAT_STOCKIES) ? $ALAMAT_STOCKIES : '-'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">WAREHOUSE NAME <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="WAREHOUSE_NAME" name="WAREHOUSE_NAME" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('WAREHOUSE_NAME', isset($WAREHOUSE_NAME) ? $WAREHOUSE_NAME : '-'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal DO <span class="required">*</span>
                            </label>
                            <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                <input class="form-control has-feedback-left" id="TANGGAL_DO" name="TANGGAL_DO" placeholder="First Name"  value="<?php echo set_value('TANGGAL_DO', isset($TANGGAL_DO) ? $TANGGAL_DO : ''); ?>" aria-describedby="inputSuccess2Status3" type="text" required="required" readonly>
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                            </div>
                        </div>

                        <!--                            <label> Contoh 1 </label>-->
                        <!---->
                        <!--                            <table id="example" class="display select" width="100%" cellspacing="0">-->
                        <!--                                <thead>-->
                        <!--                                <tr>-->
                        <!--                                    <th>No Kwitansi</th>-->
                        <!--                                    <th>Produk</th>-->
                        <!--                                    <th>Qty</th>-->
                        <!--                                    <th>Rack</th>-->
                        <!--                                </tr>-->
                        <!--                                </thead>-->
                        <!--                                <tbody>-->
                        <!--                                --><?php
                        //                                if(count($LIST_DETAIL) > 0):
                        //                                    foreach($LIST_DETAIL as $row): ?>
                        <!--                                        <tr>-->
                        <!--                                            <td>--><?php //echo $row->NO_KWITANSI;?><!--</td>-->
                        <!---->
                        <!---->
                        <!--                                            <td>--><?php //echo $row->PRODUCT_NAME;?><!--</td>-->
                        <!--                                            <td>--><?php //echo $row->QTY;?><!-- --><?php //if($row->CATEGORY==0)echo "Box";else echo "Pcs"  ?><!--</td>-->
                        <!--                                            <td>--><?php //echo $row->RACK_NAME;?><!--</td>-->
                        <!---->
                        <!---->
                        <!--                                        </tr>-->
                        <!--                                    --><?php //endforeach;
                        //                                endif;?>
                        <!--                                --><?php
                        //                                if(count($LIST_INDENT) > 0):
                        //                                    foreach($LIST_INDENT as $row): ?>
                        <!--                                        <tr>-->
                        <!--                                            <td>--><?php //echo $row->ID_KWITANSI;?><!--</td>-->
                        <!---->
                        <!--                                            <td>--><?php //echo $row->PRODUCT_NAME;?><!--</td>-->
                        <!--                                            <td>--><?php //echo $row->QTY;?><!-- Pcs</td>-->
                        <!--                                            <td>Barang indent</td>-->
                        <!---->
                        <!---->
                        <!--                                        </tr>-->
                        <!--                                    --><?php //endforeach;
                        //                                endif;?>
                        <!---->
                        <!---->
                        <!---->
                        <!---->
                        <!--                                </tbody>-->
                        <!--                            </table>-->
                        <!---->
                        <!--                            <label>Contoh 2</label>-->
                        <!---->
                        <!--                            <table id="example" class="display select" width="100%" cellspacing="0">-->
                        <!--                                <thead>-->
                        <!--                                <tr>-->
                        <!---->
                        <!--                                    <th>Produk</th>-->
                        <!--                                    <th>Qty</th>-->
                        <!---->
                        <!--                                </tr>-->
                        <!--                                </thead>-->
                        <!--                                <tbody>-->
                        <!---->
                        <!---->
                        <!--                                --><?php
                        //                                if(count($LIST_DETAIL2) > 0):
                        //                                    foreach($LIST_DETAIL2 as $row): ?>
                        <!--                                        <tr>-->
                        <!---->
                        <!---->
                        <!--                                            <td>--><?php //echo $row->PRODUCT_NAME;?><!--</td>-->
                        <!--                                            <td>--><?php //echo $row->TOTAL;?><!--</td>-->
                        <!---->
                        <!---->
                        <!--                                        </tr>-->
                        <!--                                    --><?php //endforeach;
                        //                                endif;?>
                        <!---->
                        <!---->
                        <!--                                </tbody>-->
                        <!--                            </table>-->
                        <!---->
                        <!---->
                        <!---->
                        <!--                            <label>Contoh 3</label>-->
                        <!---->
                        <!--                            <table id="example" class="display select" width="100%" cellspacing="0">-->
                        <!--                                <thead>-->
                        <!--                                <tr>-->
                        <!---->
                        <!--                                    <th>Produk</th>-->
                        <!--                                    <th>Qty</th>-->
                        <!---->
                        <!--                                </tr>-->
                        <!--                                </thead>-->
                        <!--                                <tbody>-->
                        <!---->
                        <!---->
                        <!--                                --><?php
                        //                                if(count($LIST_DETAIL3) > 0):
                        //                                    foreach($LIST_DETAIL3 as $row): ?>
                        <!--                                        <tr>-->
                        <!---->
                        <!---->
                        <!--                                            <td>--><?php //echo $row->PRODUCT_NAME;?><!--</td>-->
                        <!--                                            <td>--><?php //echo $row->total;?><!--</td>-->
                        <!---->
                        <!---->
                        <!--                                        </tr>-->
                        <!--                                    --><?php //endforeach;
                        //                                endif;?>
                        <!---->
                        <!---->
                        <!--                                </tbody>-->
                        <!--                            </table>-->
                        <!--                            <label>Indent </label>-->
                        <!--                            <table id="example" class="display select" width="100%" cellspacing="0">-->
                        <!--                                <thead>-->
                        <!--                                <tr>-->
                        <!---->
                        <!--                                    <th>Produk</th>-->
                        <!--                                    <th>Qty</th>-->
                        <!---->
                        <!--                                </tr>-->
                        <!--                                </thead>-->
                        <!--                                <tbody>-->
                        <!---->
                        <!---->
                        <!--                                --><?php
                        //                                if(count($LIST_DETAIL4) > 0):
                        //                                    foreach($LIST_DETAIL4 as $row): ?>
                        <!--                                        <tr>-->
                        <!---->
                        <!---->
                        <!--                                            <td>--><?php //echo $row->PRODUCT_NAME;?><!--</td>-->
                        <!--                                            <td>--><?php //echo $row->total;?><!--</td>-->
                        <!---->
                        <!---->
                        <!--                                        </tr>-->
                        <!--                                    --><?php //endforeach;
                        //                                endif;?>
                        <!---->
                        <!---->
                        <!--                                </tbody>-->
                        <!--                            </table>-->
                        <!--<HR>-->
                        <!--                            <label>Contoh best</label>-->

                        <table id="example" class="display select" width="100%" cellspacing="0">
                            <thead>
                            <tr>

                                <th>Produk</th>
                                <th>Qty pesanan</th>
                                <th>Qty terkirim</th>
                                <th>Qty sisa</th>

                            </tr>
                            </thead>
                            <tbody>


                            <?php
                            if(count($LIST_DETAILterbaik) > 0):
                                foreach($LIST_DETAILterbaik as $row): ?>
                                    <tr>


                                        <td><?php echo $row->PRODUCT_NAME;?></td>
                                        <td><?php echo $row->total;?></td>
                                        <td><?php echo $row->TERKIRIM;?></td>
                                        <td><?php echo $row->SISA;?></td>


                                    </tr>
                                <?php endforeach;
                            endif;?>


                            </tbody>
                        </table>

                        <hr>
                        <?php
                        if(count($LIST_DETAIL4) > 0){
                            echo('
                                 <label>Indent </label>
                                <table id="example" class="display select" width="100%" cellspacing="0">
                                <thead>
                                <tr>

                                    <th>Produk</th>
                                    <th>Qty</th>

                                </tr>
                                </thead>
                                <tbody>
                                ');
                            foreach($LIST_DETAIL4 as $row){
                                echo('  <tr>
                                            <td>'.$row->PRODUCT_NAME.'</td>
                                            <td>'.$row->total.'</td>
                                            </tr>');
                            }
                            echo ('    </tbody>
                            </table>');
                        }
                        ?>





                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="<?php echo site_url();?>viewDO" class="btn btn-primary">Cancel</a>
                                <a href="<?php echo site_url();?>viewDO/pdf/<?php echo $ids?>" class="btn btn-primary" target="_blank">Cetak</a>
                                <?php
                                $isadmin=$this->session->userdata('ID_USERROLE');
                                if($isadmin==1)
                                {
                                    ?>
                                    <a href="<?php echo site_url();?>viewDO/reroll/<?php echo $ids?>" class="btn btn-primary">RE-ROLL</a>
                                <?php } ?>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


</div>


<!-- /page content -->
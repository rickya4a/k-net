<script>
    $(document).ready(function(){
        $('#TANGGAL_GUDANG').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#ID_PO').change(function(){
                if($('#ID_PO').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('PL/getProduk');?>",
                        dataType : 'json',
                        data : {ID_PO:$('#ID_PO').val()},
                        success: function(data){
                            $("#ID_PRODUCT").html(data.option);

                        }
                    });
                }
            }
        );



        $('#ID_PRODUCT').change(function(){
                if($('#ID_PRODUCT').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('PL/getQTY');?>",
                        dataType : 'json',
                        data : {ID_PRODUCT:$('#ID_PRODUCT').val()},
                        success: function(data){
                            $("#PRO").val(data.PRO);
                            $("#qtyPO").val(data.qtyPO);

                        }
                    });
                }
            }
        );




        $('#addItem').click(function() {


            var produk = $('#ID_PRODUCT option:selected').text();

            var nopo = $('#ID_PO option:selected').text();
            var sisa = $('#qtyPO').val() - $('#qtyPCS').val();



            if(($('#ID_PRODUCT').val()!= '')){
                var rowCount = $('.main tr').length-1;
                var no = rowCount + 1;

                if(no >= 2)
                {
                    $('#submit').show();
                }

                $("<tr data-status=\"I\">"
                    + "<td><input type=\"hidden\" name=\"idpo[]\" id=\"idpo\" value=\""+$('#ID_PRODUCT').val()+"\" /><input type=\"text\" name=\"nopo[]\" id=\"nopo\" value=\""+nopo+"\" readonly=\"readonly\"  /></td>"
                    + "<td><input type=\"hidden\" name=\"qtyCRT2[]\" id=\"qtyCRT2\" value=\""+$('#qtyCRT').val()+"\" /><input type=\"hidden\" name=\"idproduk[]\" id=\"idproduk\" value=\""+$('#PRO').val()+"\" /><input type=\"text\" name=\"nmproduk[]\" id=\"nmproduk\" value=\""+produk+"\" readonly=\"readonly\"  /></td>"
                    + "<td><input type=\"hidden\" name=\"sizeCRT2[]\" id=\"sizeCRT2\" value=\""+$('#sizeCRT').val()+"\" /><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"text\" name=\"qty3[]\" id=\"qty3\" value=\""+$('#qtyPO').val()+"\" readonly=\"readonly\" /></td>"
                    + "<td><input type=\"hidden\" name=\"DETAIL_GROSS2[]\" id=\"DETAIL_GROSS2\" value=\""+$('#DETAIL_GROSS').val()+"\" /><input type=\"text\" name=\"qtyPIECE[]\" id=\"qtyPIECE\" value=\""+$('#qtyPCS').val()+"\" readonly=\"readonly\" /></td>"
                    + "<td><input type=\"hidden\" name=\"DETAIL_NET2[]\" id=\"DETAIL_NET2\" value=\""+$('#DETAIL_NET').val()+"\" /><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /><input type=\"text\" name=\"qtysisa[]\" id=\"qtysisa\" value=\""+sisa+"\" readonly=\"readonly\" /></td>"

                    + "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\" /></td>"
                    +" </tr>").appendTo(".main tbody");
                $('#ID_PRODUCT').val('');
                $('#ID_PO').val('');

                $('#qty1').val('');
                $('#qty2').val('');
                $('#qtyorder').val('');

            }else{
                alert('Produk dan Qty harus diisi');
            }

        });

        $("table.main").on("click", "#ibtnDel", function (event) {
            var tr = $(this).closest('tr');
            var row = tr.find("input[id=row]").val();
            var rowCount = $('.main tr').length-1;
            if(rowCount == 2)
            {
                $('#submit').hide();
            }
            $(this).closest("tr").remove();
        });


    });
</script>


<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li ><a href="<?php echo site_url();?>pl">
                    DO antar gudang</a>
                </li>
                <li class="active">
                    View DO antar gudang</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-map-marker fa-lg"></i> Form <b>DO antar Gudang</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post"  data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NO DO Gudang <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" readonly id="NO_DO_GUDANG" name="NO_DO_GUDANG" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NO_DO_GUDANG', isset($NO_DO_GUDANG) ? $NO_DO_GUDANG : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asal <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" readonly id="ASAL" name="ASAL" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ASAL', isset($ASAL) ? $ASAL : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tujuan <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" readonly id="TUJUAN" name="TUJUAN" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo set_value('TUJUAN', isset($TUJUAN) ? $TUJUAN : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal <span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control has-feedback-left" readonly id="TANGGAL_GUDANG" name="TANGGAL_GUDANG" placeholder="First Name" aria-describedby="inputSuccess2Status3" type="text" value="<?php echo set_value('TANGGAL_GUDANG', isset($TANGGAL_GUDANG) ? $TANGGAL_GUDANG : ''); ?>">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                </div>
                            </div>

                            <table width="90%" class="main table table-hover" id="box-table-b">
                                <thead>
                                <tr>

                                    <th >Produk</th>

                                    <th >Quantity</th>




                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(count($LIST_DETAIL) > 0):
                                    foreach($LIST_DETAIL as $row): ?>
                                        <tr>

                                            <td><?php echo $row->PRODUCT_NAME;?></td>
                                            <td><?php echo $row->SALES;?></td>


                                        </tr>
                                    <?php endforeach;
                                endif;?>
                                </tbody>

                            </table>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Remark <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea readonly id="REMARK"  name="REMARK" cols="100" rows="3" data-bvalidator="required" style="width: 500px; "><?php echo set_value('REMARK', isset($REMARK) ? $REMARK : ''); ?></textarea>
                                </div>
                            </div>



                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url();?>DOgudang" class="btn btn-primary">Cancel</a>
                                    <a href="<?php echo site_url();?>DOgudang/pdf2/<?php echo $ID;?>" class="btn btn-primary" target="_blank">Cetak DO</a>

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
<script>


    $(document).ready(function (){

        $('#ID_WAREHOUSE1').change(function(){
                if($('#ID_WAREHOUSE1').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('DOgudang/getProduk');?>",
                        dataType : 'json',
                        data : {ID_WAREHOUSE:$('#ID_WAREHOUSE1').val()},
                        success: function(data){

                            $("#ID_PRODUCT").html(data.option);

                        }
                    });
                }
            }
        );
        $('#ID_PRODUCT').change(function(){
                if($('#ID_PRODUCT').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('DOgudang/getDataQty');?>",
                        dataType : 'json',
                        data : {ID_WAREHOUSE:$('#ID_WAREHOUSE1').val(), ID_PRODUCT:$('#ID_PRODUCT').val()},
                        success: function(data){
                            $("#stok").val(data.stok);

                        }
                    });
                }
            }
        );

        $(".js-example-basic-single").select2();

        $('#TANGGAL_GUDANG').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#addItem').click(function() {
            var produk = $('#ID_PRODUCT option:selected').text();

            var nopo = $('#ID_PO option:selected').text();
            var sisa = $('#qtyPO').val() - $('#qtyPCS').val();


            if(($('#ID_PRODUCT').val()!= '')&&($('#qtyPCS').val()!= '')){
                var rowCount = $('.main tr').length-1;
                var no = rowCount + 1;
                if(no >= 2)
                {
                    $('#submit').show();
                }
                $("<tr data-status=\"I\">"
                    + "<td><input type=\"hidden\" name=\"idproduk[]\" id=\"idproduk\" value=\""+$('#ID_PRODUCT').val()+"\" />"+produk+"</td>"
                    + "<td>"+$('#qtyPCS').val()+"<input type=\"hidden\" name=\"QTYPCS[]\" id=\"QTYPCS\" value=\""+$('#qtyPCS').val()+"\" /><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /></td>"
                    + "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\" /></td>"
                    +" </tr>").appendTo(".main tbody");
                $('#ID_PRODUCT').val('');
                $('#qBOX').val('');
                $('#qPCS').val('');
                $('#BATCH').val('');


                $('#T_BATCH').val('');
                $('#T_PABRIK').val('');
                $('#T_GUDANG').val('');

                $('#qPL').val('');


            }else{
                alert('Produk dan Qty harus diisi');
            }

        });

        $("table.main").on("click", "#ibtnDel", function (event) {
            var tr = $(this).closest('tr');
            var row = tr.find("input[id=row]").val();
            var rowCount = $('.main tr').length-1;
            if(rowCount == 2)
            {
                $('#submit').hide();
            }
            $(this).closest("tr").remove();
        });



    });




</script>



<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    DO Antar Gudang</li>
                <li class="active">
                    Form Antar Gudang</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>DO Antar Gudang</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No DO Antar Gudang<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NO_DO_GUDANG" name="NO_DO_GUDANG" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NO_DO_GUDANG', isset($NO_DO_GUDANG) ? $NO_DO_GUDANG : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse Asal</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_WAREHOUSE1" name="ID_WAREHOUSE1" class="form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_WAREHOUSE as $k):?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"<?php echo set_select('ID_WAREHOUSE1',$k->ID_WAREHOUSE,isset($ID_WAREHOUSE)&&($k->ID_WAREHOUSE == $ID_WAREHOUSE)? true : false); ?>><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse Tujuan</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_WAREHOUSE2" name="ID_WAREHOUSE2" class="form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_WAREHOUSE as $k):?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"<?php echo set_select('ID_WAREHOUSE2',$k->ID_WAREHOUSE,isset($ID_WAREHOUSE)&&($k->ID_WAREHOUSE == $ID_WAREHOUSE)? true : false); ?>><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal DO Gudang<span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control has-feedback-left" id="TANGGAL_GUDANG" name="TANGGAL_GUDANG" placeholder="First Name" aria-describedby="inputSuccess2Status3" type="text">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                </div>
                            </div>


                            <hr>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Produk</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_PRODUCT"  name="ID_PRODUCT" class="js-example-basic-single form-control"  data-bvalidator="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_PRODUK as $k):?>
                                            <option value="<?php echo $k->ID_PRODUCT; ?>"><?php echo $k->ID_PRODUCT." - ".$k->PRODUCT_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Stok (pcs)<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="stok" name="stok"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Qty (pcs)<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="qtyPCS" name="qtyPCS"   class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a class="btn btn-success" id="addItem" > Tambahkan </a>
                                </div>
                            </div>

                            <table width="90%" class="main table table-hover" id="box-table-b">
                                <thead>
                                <tr>
                                    <th >Produk</th>

                                    <th >Quantity (pcs)</th>

                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>

                            </table>



                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url();?>DOgudang" class="btn btn-primary">Cancel</a>

                                    <button type="submit" name="checkBtn" id="checkBtn" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
<script>


    $(document).ready(function (){
		
		 $('#next').click(function(){
		 	   if(($('#ID_WAREHOUSE1').val() != '')&&($('#ID_WAREHOUSE2').val() != '')){
               		$('#master').hide();
               		$("#addItem").removeAttr("disabled");
               		$("#do_group").removeAttr("disabled");
               		$("#ID_WAREHOUSE1").attr("disabled", "disabled");
               		$("#ID_WAREHOUSE2").attr("disabled", "disabled");
               		$("#warehouse1").val($('#ID_WAREHOUSE1').val());
               		$("#warehouse2").val($('#ID_WAREHOUSE2').val());
               }else{
              		alert('Warehouse Asal dan Tujuan tidak boleh kosong');
               }
            }
        );
		
        $('#ID_WAREHOUSE1').change(function(){
                if($('#ID_WAREHOUSE1').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('DOgudang/getProduk');?>",
                        dataType : 'json',
                        data : {ID_WAREHOUSE:$('#ID_WAREHOUSE1').val()},
                        success: function(data){

                            $("#ID_PRODUCT").html(data.option);

                        }
                    });
                }
            }
        );
        
        $('#ID_PRODUCT').change(function(){
                if($('#ID_PRODUCT').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('DOgudang/getDataQty');?>",
                        dataType : 'json',
                        data : {ID_WAREHOUSE:$('#ID_WAREHOUSE1').val(), ID_PRODUCT:$('#ID_PRODUCT').val()},
                        success: function(data){
                            $("#stok").val(data.stok);

                        }
                    });
                }
            }
        );

        $(".js-example-basic-single").select2();

        $('#TANGGAL_GUDANG').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

		$('#do_group').click(function(){
			$.ajax({
                     type : "POST",
                        url  : "<?php echo site_url('DOgudang/getDataDO');?>",
                        dataType : 'json',
                        data : {warehouse1:$('#ID_WAREHOUSE1').val(), warehouse2:$('#ID_WAREHOUSE2').val()},
                        success: function(data){
                        	var isi = '';
                        	var rowCount = $('.main tr').length-1;
              				var no = rowCount + 1;
                            for(i=0;i<data.jum;i++)
                            {	
                            	 if(data.hasil[i]['JUM'] > data.hasil[i]['STOCK'])
                            	 {
                            	 	var qtypcs = data.hasil[i]['STOCK'];
                            	 }else{
                            	 	var qtypcs = data.hasil[i]['JUM'];
                            	 }
                            	 
                            	 isi += "<tr data-status=\"I\">"
				                    + "<td><input type=\"hidden\" name=\"idproduk[]\" id=\"idproduk\" value=\""+data.hasil[i]['ID_PRODUCT']+"\" />"+data.hasil[i]['PRODUCT_NAME']+"</td>"
				                    + "<td><input type=\"hidden\" name=\"stock[]\" id=\"stock\" value=\""+data.hasil[i]['STOCK']+"\" />"+data.hasil[i]['STOCK']+"</td>"
				                    + "<td><input type=\"hidden\" name=\"qtydo[]\" id=\"qtydo\" value=\""+data.hasil[i]['JUM']+"\" />"+data.hasil[i]['JUM']+"</td>"
				                    + "<td><input type=\"hidden\" name=\"cekqty[]\" id=\"cekqty\" value=\""+qtypcs+"\" size=\"10\" /><input type=\"text\" name=\"qtypcs[]\" id=\"qtypcs\" value=\""+qtypcs+"\" size=\"10\" /><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" /></td>"
				                    + "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\" /></td>"
				                    + " </tr>";
				                 
				                 no++;
                            }
                            
                            //alert(isi);
                            $(isi).appendTo(".main tbody");
                            $("#checkBtn").removeAttr("disabled");

                 }
            });
		});
		
        $('#addItem').click(function() {
            var produk = $('#ID_PRODUCT option:selected').text();

            var nopo = $('#ID_PO option:selected').text();
            var sisa = $('#qtyPO').val() - $('#qtyPCS').val();

			if(parseInt($('#qtyPCS').val()) <=  parseInt($('#stok').val())){
	            if((parseInt($('#qtyPCS').val()) != '') && (parseInt($('#qtyPCS').val()) != '0')){
	                var rowCount = $('.main tr').length-1;
	                var no = rowCount + 1;
	                if(no >= 2)
	                {
	                    $('#submit').show();
	                }
	                $("<tr data-status=\"I\">"
					     + "<td><input type=\"hidden\" name=\"idproduk[]\" id=\"idproduk\" value=\""+$('#ID_PRODUCT').val()+"\" />"+produk+"</td>"
					     + "<td><input type=\"hidden\" name=\"stock[]\" id=\"stock\" value=\""+$('#stok').val()+"\" />"+$('#stok').val()+"</td>"
					     + "<td><input type=\"hidden\" name=\"qtydo[]\" id=\"qtydo\" value=\"0\" /> 0 </td>"
					     + "<td><input type=\"hidden\" name=\"cekqty[]\" id=\"cekqty\" value=\"0\" size=\"10\" /><input type=\"hidden\" name=\"qtypcs[]\" id=\"qtypcs\" value=\""+$('#qtyPCS').val()+"\" /><input type=\"hidden\" name=\"row[]\" id=\"row\" value=\""+no+"\" />"+$('#qtyPCS').val()+"</td>"
					     + "<td><input type=\"button\" name=\"ibtnDel\" id=\"ibtnDel\" value=\"Delete\" /></td>"
					     +" </tr>").appendTo(".main tbody");
					     
	                $('#stok').val('');
	                $('#qtyPCS').val('');
	                $("#checkBtn").removeAttr("disabled");
	            }else{
	                alert('Produk dan Qty harus diisi');
	            }
            }else{
            	alert('Qty tidak boleh melebihi stock');
            }
        });

        $("table.main").on("click", "#ibtnDel", function (event) {
        	var rowCount = $('.main tr').length-1;
            var tr = $(this).closest('tr');
            var row = tr.find("input[id=row]").val();
           
            if(rowCount == 1)
            {
                $('#checkBtn').attr("disabled", "disabled");
            }
            $(this).closest("tr").remove();
        });

		 $("table.main").on("change", "#qtypcs", function (event) {
		 	var rowCount = $('.main tr').length-1;
            var tr = $(this).closest('tr');
            var qty = tr.find("input[id=qtypcs]").val();
            var qtydo = tr.find("input[id=qtydo]").val();
            var stock = tr.find("input[id=stock]").val();
            var cekqty = tr.find("input[id=cekqty]").val();
           	
           	if(qty > cekqty){
           		if(cekqty == stock)
	            {
	            	alert('Qty tidak boleh melebihi Stock yang tersedia');
	            	tr.find("input[id=qtypcs]").val(stock);
	            }else{
	            	alert('Qty tidak boleh melebihi Qty DO');
	            	tr.find("input[id=qtypcs]").val(qtydo);
	            }
           	}
            
            if(qty <= 0)
            {
            	alert('Qty tidak boleh 0');
            	tr.find("input[id=qtypcs]").val(qtydo);
            }
		 });

    });




</script>



<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    DO Antar Gudang</li>
                <li class="active">
                    Form Antar Gudang</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>DO Antar Gudang</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No DO Antar Gudang<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NO_DO_GUDANG" name="NO_DO_GUDANG" required="required" class="form-control col-md-7 col-xs-12" readonly="readonly" value="<?php echo set_value('NO_DO_GUDANG', isset($NO_DO_GUDANG) ? $NO_DO_GUDANG : ''); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse Asal</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                	<input type="hidden" id="warehouse1" name="warehouse1" value="">
                                    <select id="ID_WAREHOUSE1" name="ID_WAREHOUSE1" class="form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_WAREHOUSE as $k):?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"<?php echo set_select('ID_WAREHOUSE1',$k->ID_WAREHOUSE,isset($ID_WAREHOUSE)&&($k->ID_WAREHOUSE == $ID_WAREHOUSE)? true : false); ?>><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Warehouse Tujuan</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" id="warehouse2" name="warehouse2" value="">
                                    <select id="ID_WAREHOUSE2" name="ID_WAREHOUSE2" class="form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_WAREHOUSE as $k):?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"<?php echo set_select('ID_WAREHOUSE2',$k->ID_WAREHOUSE,isset($ID_WAREHOUSE)&&($k->ID_WAREHOUSE == $ID_WAREHOUSE)? true : false); ?>><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal DO Gudang<span class="required">*</span>
                                </label>
                                <div class="col-md-6 xdisplay_inputx form-group has-feedback">
                                    <input class="form-control" id="tanggal" name="tanggal" type="text" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                                </div>
                            </div>
							<div id="master">
								<hr>
								 <div class="form-group">
	                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                                    <button type="button" name="next" id="next" class="btn btn-success">Next</button>
	                                </div>
	                            </div>	
							</div>
	                            <hr>
	                            <div class="form-group">
	                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Produk</label>
	                                <div class="col-md-6 col-sm-6 col-xs-15">
	                                    <select id="ID_PRODUCT"  name="ID_PRODUCT" class="js-example-basic-single form-control"  data-bvalidator="required">
	                                        <option value=""></option>
	                                        <?php foreach($LIST_PRODUK as $k):?>
	                                            <option value="<?php echo $k->ID_PRODUCT; ?>"><?php echo $k->ID_PRODUCT." - ".$k->PRODUCT_NAME; ?></option>
	                                        <?php endforeach;  ?>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Stok (pcs)<span class="required">*</span>
	                                </label>
	                                <div class="col-md-6 col-sm-6 col-xs-12">
	                                    <input type="text" id="stok" name="stok"   class="form-control col-md-7 col-xs-12">
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Qty (pcs)<span class="required">*</span>
	                                </label>
	                                <div class="col-md-6 col-sm-6 col-xs-12">
	                                    <input type="text" id="qtyPCS" name="qtyPCS"   class="form-control col-md-7 col-xs-12">
	                                </div>
	                            </div>
	
	                            <div class="form-group">
	                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                                	<button type="button" name="next" id="addItem" class="btn btn-success" disabled="disabled">Tambahkan</button>
	                                	<button type="button" name="next" id="do_group" class="btn btn-success" disabled="disabled">DO Group</button>
	                                </div>
	                            </div>
	
	                            <table width="90%" class="main table table-hover" id="box-table-b">
	                                <thead>
		                                <tr>
		                                    <th >Produk</th>
		                                    <th >Stock</th>
											<th >Qty DO</th>
		                                    <th >Qty</th>
		                                    <th >Action</th>
		                                </tr>
	                                </thead>
	                                <tbody>
	                                </tbody>
	
	                            </table>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Remark
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="REMARK"  name="REMARK" cols="100" rows="3"  style="width: 500px; "><?php echo set_value('REMARK', isset($REMARK) ? $REMARK : ''); ?></textarea>
                                    </div>
                                </div>
	
	                            <div class="ln_solid"></div>
	                            <div class="form-group">
	                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                                    <a href="<?php echo site_url();?>DOgudang" class="btn btn-primary">Cancel</a>
	
	                                    <button type="submit" name="checkBtn" id="checkBtn" class="btn btn-success" disabled="disabled">Submit</button>
	                                </div>
	                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function() {

        $('#warehouse').change(function(){
                if($('#warehouse').val() != '')
                {
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo site_url('closingreport/getProduct');?>",
                        dataType : 'json',
                        data : {warehouse:$('#warehouse').val()},
                        success: function(data){
                            $("#ID_PRODUCT").html(data.option);
                            //$('#GR_ID').attr("disabled", true);

                        }
                    });
                }
            }
        );



        $('#tgl_awal').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#tgl_akhir').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: {
                format: 'DD-MM-YYYY'
            }
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });



    });





</script>
<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li ><a href="<?php echo site_url();?>provinsi">
                    Master</a>
                </li>
                <li class="active">
                    Kota</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-map-marker fa-lg"></i> <b>Closing Stock Report</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left" target="_blank">

                            <div class="form-group">
                                Tanggal : <input type="text" name="tgl_awal" id="tgl_awal" size="10" style="text-align: center"/> s/d <input type="text" name="tgl_akhir" id="tgl_akhir" size="10" style="text-align: center" />
                            </div>

                            <div class="form-group">
                                Warehouse
                                    <select id="warehouse" name="warehouse" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                        <option value="all">Semua Warehouse</option>
                                        <?php foreach($LIST_warehouse as $k):?>
                                            <option value="<?php echo $k->ID_WAREHOUSE; ?>"><?php echo $k->WAREHOUSE_NAME; ?></option>
                                        <?php endforeach;  ?>
                                    </select>
                            </div>

                            <div class="form-group">
                                Kategory
                                <select id="category" name="category" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                    <option value="all">Semua Kategory</option>
                                    <option value="0">Rack</option>
                                    <option value="1">Location</option>
                                </select>
                            </div>

                            <div class="form-group">
                                Produk
                                <select id="ID_PRODUCT" name="ID_PRODUCT" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                    <option value="all">Semua Produk</option>

                                </select>
                            </div>


                            <div class="form-group">
                                Sort by
                                <select id="sort" name="sort" class="js-example-basic-single form-control" data-bvalidator="required" required="required">
                                    <option value="0">Kode Rak</option>
                                    <option value="1">Nama Barang</option>
                                    <option value="2">Kode Barang</option>
                                </select>
                            </div>
                            <div class="form-group">

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox">
                                        <label>
                                            <input name="status" type="checkbox" value="1"> <B>View Detail</B>
                                        </label>
                                    </div>
                                </div>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <div class="col-md-6 col-sm-6 col-xs-12">-->
<!--                                    <select id="status" name="status" data-bvalidator="required" required="required"  style="text-align: center" >-->
<!--                                        <option value="1">Per box</option>-->
<!--                                        <option value="2">Per pcs</option>-->
<!--                                    </select>-->
<!--                                </div>-->
<!--                            </div>-->

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">


                                    <button type="submit" class="btn btn-success" >Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->

<!DOCTYPE html>
<!-- page content -->



    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="<?php echo site_url();?>produk">
                    Initiate DO</li>
                <li class="active">
                    Form DO</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1> <i class="fa fa-cubes fa-lg"></i> Form <b>DO</b></h1>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form role="form" id="demo-form2" method="post" action="<?php echo $action; ?>"  data-parsley-validate class="form-horizontal form-label-left">


                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <h4 class="modal-title" id="myModalLabel">Detail Kwitansi</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table width="90%" class="main table table-hover" id="box-table-b" name="box-table-b">

                                            </table>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="ids" value="<?php echo set_value('ids', isset($ids) ? $ids : ''); ?>" name="ids" required="required" class="form-control col-md-7 col-xs-12">




                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No DO <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NO_DO" name="NO_DO" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NO_DO', isset($NO_DO) ? $NO_DO : ''); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kurir</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="ID_COURIER" name="ID_COURIER" class="form-control" data-bvalidator="required" required="required">
                                        <option value=""></option>
                                        <?php foreach($LIST_KURIR as $k):?>
                                            <option value="<?php echo $k->ID; ?>"<?php echo set_select('ID_COURIER',$k->ID,isset($ID_COURIER)&&($k->ID == $ID_COURIER)? true : false); ?>><?php echo $k->NAMA; ?></option>
                                        <?php endforeach;  ?>


                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Warehouse <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="WAREHOUSE_NAME" name="WAREHOUSE_NAME" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('WAREHOUSE_NAME', isset($WAREHOUSE_NAME) ? $WAREHOUSE_NAME : ''); ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode  <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ID_STOCKIES" name="ID_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ID_STOCKIES', isset($ID_STOCKIES) ? $ID_STOCKIES : ''); ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama  <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="NAMA_STOCKIES" name="NAMA_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NAMA_STOCKIES', isset($NAMA_STOCKIES) ? $NAMA_STOCKIES : $NAMA); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ALAMAT_STOCKIES" name="ALAMAT_STOCKIES" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES', isset($ALAMAT_STOCKIES) ? $ALAMAT_STOCKIES : $ALAMAT1); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ALAMAT_STOCKIES2" name="ALAMAT_STOCKIES2" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES2', isset($ALAMAT2) ? $ALAMAT2 : '-'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="ALAMAT_STOCKIES3" name="ALAMAT_STOCKIES3" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT_STOCKIES3', isset($ALAMAT3) ? $ALAMAT3 : '-'); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label> <b>DO </b>
                                </label>
                            </div>
                            <table id="example" class="display select" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Kode Produk</th>

                                    <th>Produk</th>
                                    <th>Qty (unit)</th>


                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(count($LIST_DETAIL) > 0 && $LIST_DETAIL!=null):
                                    foreach($LIST_DETAIL as $row): ?>
                                        <tr>
                                            <td><input name="PRODUCT_CODE[]" id="PRODUCT_CODE" type="hidden" value="<?php echo $row->PRODUCT_CODE;?>" ><?php echo $row->PRODUCT_CODE;?></td>

                                            <td><?php echo $row->PRODUCT_NAME;?></td>
                                            <td><input name="QTYDELIVERY[]" id="QTYDELIVERY" type="hidden" value="<?php echo $row->QTYDELIVERY;?>" > <?php echo $row->QTYDELIVERY;?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;?>
                                </tbody>
                            </table>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <label> <b>Indent </b>
                                </label>
                            </div>
                            <table id="example2" class="display select" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Kode Produk</th>
                                    <th>Produk</th>
                                    <th>Qty Indent</th>
                                    <th>Qty (unit)</th>


                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(count($LIST_INDENT) > 0 && $LIST_INDENT!=null):
                                    foreach($LIST_INDENT as $row): ?>
                                        <tr>
                                            <td><input name="PRODUCT_CODE[]" id="PRODUCT_CODE" type="hidden" value="<?php echo set_value('PRODUCT_CODE', isset($row->PRODUCT_CODE) ? $row->PRODUCT_CODE : 0); ?>" ><?php echo $row->PRODUCT_CODE;?></td>
                                            <td><?php echo $row->PRODUCT_NAME;?></td>
                                            <td><input name="QTYDELIVERY1[]" id="QTYDELIVERY1" type="text" value="<?php echo set_value('QTYDELIVERY1', isset($row->QTYDELIVERY) ? $row->QTYDELIVERY : 0); ?>" readonly></td>
                                            <td><input name="QTYDELIVERY[]" id="QTYDELIVERY" type="text" value="<?php echo set_value('QTYDELIVERY', isset($row->QTYDELIVERY) ? $row->QTYDELIVERY : 0); ?>" > </td>
                                        </tr>
                                    <?php endforeach;
                                endif;?>
                                </tbody>
                            </table>


                            <div class="ln_solid"></div>


                            <!--div class="form-group">
                                <label> <b>Pembayaran Indent </b>
                                </label>
                            </div>
                            <table id="example2" class="display select" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Kode Produk</th>

                                    <th>Produk</th>
                                    <th>Qty (unit)</th>


                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(count($LIST_DETAIL) > 0):
                                    foreach($LIST_DETAIL as $row): ?>
                                        <tr>
                                            <td><?php echo $row->PRODUCT_CODE;?></td>

                                            <td><?php echo $row->PRODUCT_NAME;?></td>
                                            <td><?php echo $row->QTYDELIVERY;?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;?>
                                </tbody>
                            </table>


                            <div class="ln_solid"></div -->




                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo site_url();?>cekDO" class="btn btn-primary">Cancel</a>

                                    <button type="submit"  class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>


<!-- /page content -->
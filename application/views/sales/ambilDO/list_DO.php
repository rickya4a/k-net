<script>
    $(document).ready(function() {
        $('#kota').DataTable( {
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url(); ?>initiateDO2/getTable",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
//			{ "name": "no",   "targets": 0 },
                { "name": "NAMA_STOCKIES", "targets": 1 },
                { "name": "CODE_STOCKIES",  "targets": 2 },
                { "name": "ALAMAT_STOCKIES",  "targets": 3 },


                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                    "destroy": true
                }
            ]
        } );
    } );
</script>
<!DOCTYPE html>
<!-- page content -->
    <div class="">
    <!-- start: BREADCRUMB -->
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url();?>awal">
                        Home</a>
                </li>
                <li class="active">
                    Initiate Delivery Order</li>
            </ol>
        </div>
    </div>
    <!-- end: BREADCRUMB -->

    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1> <i class="fa fa-compass fa-lg"></i> Daftar <b>Initate DO</b></h1>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="kota" class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <th>Nomor</th>
                        <th>Nama Stockies</th>
                        <th>Kode Stockies</th>
                        <th>Alamat</th>
                        <th>Action</th>

                    </tr>
                    </thead>


                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>




    </div>

<!-- /page content -->
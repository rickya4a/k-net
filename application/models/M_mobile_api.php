<?php
class M_mobile_api extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
		$date = date("Y-m-d");
		if($date >= "2019-03-01") {
			$this->tbl_pricelist = "V_Ecomm_PriceList_Baru";
			$this->tbl_pricelistDion = "V_Ecomm_PriceList_Dion_Baru";
		} else {
			$this->tbl_pricelist = "V_Ecomm_PriceList";
			$this->tbl_pricelistDion = "V_Ecomm_PriceList_Dion";
		}
	}
	/*------------------
	 * JATIS
	 * ----------------*/
	function ShowMemberInfoJatis($tel_hp, $tgl_lahir) {
		$tgl = substr($tgl_lahir, 0, 2);
		$bln = substr($tgl_lahir, 2, 2);
		$thn = substr($tgl_lahir, 4, 4);

		$tgl_lahirx = $tgl."/".$bln."/".$thn;
		//$idmember = strtoupper($idmember);
		$slc = "select a.*
		       from V_HILAL_API_LOGIN a 
		       where a.tel_hp = '".$tel_hp."' AND a.birthdt = '".$tgl_lahirx."'";

		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}
	/*--------------
	 * HOME
	 * -------------*/
	function getListBanner() {
		$qry = "SELECT a.id, 'https://www.k-net.co.id/' + a.img_url as img_url
				FROM ecomm_master_header_pic a
				WHERE a.hdr_status = '1'
				ORDER BY CAST(a.id AS INT) DESC";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListPromotionActivityBanner() {
		$qry = "SELECT a.id, hdr_desc as title, banner_content,
		             'https://www.k-net.co.id/' + a.img_url as img_url, CONVERT(VARCHAR(30),a.uploadDate,103) as  uploadDate
				FROM ecomm_master_header_pic a
				WHERE tipe_banner = '2'
				ORDER BY uploadDate, CAST(a.id AS INT) DESC";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getLatestEvent() {
		$conx = $this->load->database("klaim_acara", true);
		$qry = "Select top 10 * from V_API_LIST_EVENT a
		        order by a.tanggalacara desc";
		$res = $conx->query($qry);
		$hasil = $res->result();
		return $hasil;
	}

	function getListDownloadMaterial($category = null, $limit = null, $offset = null) {
		$str = "";
		if($category != null) {
			$str .= " AND category = '$category' ";
		}

		if($limit == null && $offset == null) {
			$qry = "SELECT id, title, fileUrl, fileType,
		   			  CONVERT(VARCHAR(30),a.uploadDate,103) as uploadDate,	
		              category
					FROM ecomm_material_download a
					WHERE status = '1' $str
					ORDER BY uploadDate DESC";
		} else {
			$first = 0;
			$last = 0;
			if($offset == 0 || $offset == 1){
				$first = 1;
				$last  = $limit;
				//echo "test ".$no_page."-".$first."-".$last."<br>";
			} else {
				$xx = $offset - 1;
				if($xx == 0){
					$xx = 1;
				}
				//echo $xx;
				$first = $xx * $limit + 1;
				$last  = $first + ($limit - 1);
			}

			$qry = "SELECT 
			           XX.RowNumber,
			           XX.id,
                	   XX.title,
                	   XX.fileUrl,
                	   XX.fileType,
                	   XX.uploadDate,
                       XX.category
        
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY id desc) as RowNumber,
							        id, 
							        title, 
							        fileUrl, 
							        fileType,
							        CONVERT(VARCHAR(30),a.uploadDate,103) as uploadDate, 
							        category
						FROM ecomm_material_download a
						WHERE status = '1' $str
						
				) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."
                ORDER BY uploadDate DESC";

		}

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getHomeProductDisplay($stt) {
		/*$qry = "SELECT b.prdcd, 
					    b.prdnm, 
					    b.prdcdcat, 
					    b.prdnmcatnm,
					    b.img_mb as img_url,
					    b.price_w, 
					    b.price_e, 
					    b.bv,
					    b.price_cw, 
					    b.price_ce, 
					    b.weight,
					    b.is_starterkit,
					    b.is_charge_ship,
					    b.is_discontinue
				FROM master_prd_promoted_and_top_selling a
				INNER JOIN V_Ecomm_PriceList b ON (a.prdcd = b.prdcd)
				WHERE a.type_status = '$stt'";*/
		$tblx = $this->tbl_pricelist;		
		$qry = "SELECT  c.id_head as varian_id,
				        b.prdcd, 
				        b.prdnm, 
				        b.prdcdcat, 
				        b.prdnmcatnm,
				        b.img_mb as img_url,
				        b.price_w, 
				        b.price_e, 
				        b.bv,
				        b.price_cw, 
				        b.price_ce, 
				        b.weight,
				        b.is_starterkit,
				        b.is_charge_ship,
				        b.is_discontinue
				FROM master_prd_promoted_and_top_selling a
				INNER JOIN $tblx b ON (a.prdcd = b.prdcd)
				INNER JOIN master_prd_cat_inv_mobile c ON (a.prdcd = c.cat_inv_id)
				WHERE a.type_status = '$stt' and b.price_w is not null and b.price_e is not null";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getHomeProductDisplayWithPaging($stt, $limit, $offset) {
		$first = 0;
		$last = 0;
		if($offset == 0 || $offset == 1){
			$first = 1;
			$last  = $limit;
			//echo "test ".$no_page."-".$first."-".$last."<br>";
		} else {
			$xx = $offset - 1;
			if($xx == 0){
				$xx = 1;
			}
			//echo $xx;
			$first = $xx * $limit + 1;
			$last  = $first + ($limit - 1);
		}
		$tblx = $this->tbl_pricelist;	
		$qry = "SELECT XX.RowNumber,
		               XX.varian_id,
                	   XX.prdcd,
                	   XX.prdnm,
                	   XX.prdcdcat,
                       XX.prdnmcatnm,
                	   XX.img_url,
                	   XX.price_w,
                	   XX.price_e,
                	   XX.price_cw,
                	   XX.price_ce,
                	   XX.bv,
                	   XX.weight,
                	   XX.is_starterkit,
				       XX.is_charge_ship,
				       XX.is_discontinue
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY b.prdcd desc) as RowNumber,
                    	            c.id_head as varian_id,
							        b.prdcd, 
								    b.prdnm, 
								    b.prdcdcat, 
								    b.prdnmcatnm,
								    b.img_mb as img_url,
								    b.price_w, 
								    b.price_e, 
								    b.bv,
								    b.price_cw, 
								    b.price_ce, 
								    b.weight,
								    b.is_starterkit,
								    b.is_charge_ship,
								    b.is_discontinue
							FROM master_prd_promoted_and_top_selling a
							INNER JOIN $tblx b ON (a.prdcd = b.prdcd)
							INNER JOIN master_prd_cat_inv_mobile c ON (a.prdcd = c.cat_inv_id)
							WHERE a.type_status = '$stt' and
							b.price_w is not null and b.price_e is not null
				) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
	/*--------
	 * END HOME
	 * -------*/

	function checkAvailableOtp($idmember) {
		$qry = "SELECT * FROM otp_login a
				WHERE a.idmember = '$idmember' AND a.is_expire != 1 
 				AND GETDATE() <= DATEADD(day, 1, createdt)";
		$res = $this->getRecordset($qry, null, $this->db1);

		return $res;
	}

	function getInfoStockist($idstk) {
		$qry = "select loccd, fullnm, pricecode 
		        from mssc where loccd = '$idstk'
		        AND sctype != '3' AND fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL') and
				scstatus='1'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
	
	function getLatLongStk($idstk) {
		$qry = "select loccd, latitude, longitude from mssc where loccd = '$idstk' 
		        AND longitude is not null and latitude is not null and longitude <> '' and latitude <> ''";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	} 

	function getLoginStockist($idstk, $pwd) {
		//$id = strtoupper($id);
		$qry = "SELECT a.loccd as idstockist, b.[password], a.fullnm as stockistname, 
			         a.tel_hm, a.tel_hp, a.tel_of, 
			         a.addr1 + ' ' + a.addr2 + ' ' + a.addr3 as address, a.email,a.latitude, a.longitude 
		        FROM mssc a 
				LEFT OUTER JOIN sc_users b ON (a.loccd = b.username)
				WHERE a.loccd = '$idstk' and b.[password] = '$pwd' 
					AND fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL') and
					scstatus='1' and take_order_allowed = '1'";
		//echo $qry;
		//$result = $this->getRecordset($qry,null,$this->db2);
		//return $result;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function getStokistProfile($idstk) {
		$qry = "SELECT a.loccd as idstockist, a.fullnm as stockistname, 
			         a.tel_hm, a.tel_hp, a.tel_of, 
			         a.addr1 + ' ' + a.addr2 + ' ' + a.addr3 as address, a.email,
			         a.latitude, a.longitude 
		        FROM mssc a 
				WHERE a.loccd = '$idstk'
					AND fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL')";
		//echo $qry;
		//$result = $this->getRecordset($qry,null,$this->db2);
		//return $result;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}



	function getInfoStockist2($param, $value) {
		$value = strtoupper($value);
		if($param == "loccd") {
			$str = " AND loccd = '$value' ";
		} else if($param == "fullnm") {
			$str = " AND fullnm LIKE '%$value%' ";
		} else if($param == "city") {
			$str = " AND city LIKE '$value%' ";
		}
		$qry = "select loccd, fullnm, pricecode, addr1+' '+addr2+' '+addr3
					as address, city, tel_of as phone1, tel_hm as phone2 
		        from mssc where 
		          sctype != '3' AND 
		         fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL') and
				 scstatus='1' $str";
		//echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function OtpAuth($idmember, $otp) {
		$qry = "SELECT * FROM otp_login a
				WHERE a.idmember = '$idmember' AND a.otp = '$otp' AND a.is_expire != 1 
 				AND GETDATE() <= DATEADD(day, 1, createdt)";
		$res = $this->getRecordset($qry, null, $this->db1);
		if($res != null) {
			$upd = "UPDATE otp_login SET is_expire =  1 WHERE idmember = '$idmember' AND otp = '$otp'";
			$res = $this->executeQuery($upd);
			if($res > 0) {
				$res = jsonTrueResponse("Login OTP sukses");
			} else {
				$res = jsonFalseResponse("Login OTP sukses");
			}
		} else {
			$res = jsonFalseResponse("Data OTP tidak valid, silahkan resend OTP");
		}
		return $res;
	}

	function insertOTP($idmember, $otp) {
		$qry = "INSERT INTO otp_login (idmember, otp, createdt) VALUES ('$idmember', '$otp', '".$this->dateTime."')";
		$res = $this->executeQuery($qry);
		return $res;
	}

	function sendOtpViaSMS($otp, $tel_hp) {
		$alternate2 = $this->load->database('alternate2', true);
		$text = 'OTP :'.$otp;
		$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, senderID) 
		                    VALUES ('$tel_hp','".$text."','OTP Sent Status', 'MyPhone6')";
		//echo "Ins SMS : $insSms";
		$query = $alternate2->query($insSms);
	}

	function sendTextMessage($message, $no_hp, $keterangan) {
		$alternate2 = $this->load->database('alternate2', true);
		//$text = 'OTP :'.$otp;
		$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, senderID) 
		                    VALUES ('$no_hp','".$message."', '$keterangan', 'MyPhone6')";
		//echo "Ins SMS : $insSms";
		$query = $alternate2->query($insSms);
		return $query;
	}

	function getConfigEmail() {
		$qry = "select * FROM ecomm_mail_listing a where a.id = 1";
		//echo $qry;		
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function sendEmailMessage($message, $email, $keterangan) {
		$res = $this->getConfigEmail();
		if($res > 0) {
			$senderEmail 	= $res[0]->email;
			$senderPassword = $res[0]->password;
			$senderAlias 	= $res[0]->alias_name;
			$senderSmtp 	= $res[0]->smtp;
			$senderSmtpPort = $res[0]->smtp_port;

			$config = Array(
				'protocol' 	=> 'smtp',
				'smtp_host' => $senderSmtp,
				'smtp_port' => $senderSmtpPort,
				'smtp_user' => $senderEmail,
				'smtp_pass' => $senderPassword,
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'wordwrap' 	=> TRUE,
				'newline' 	=> '\r\n',
			);
			$this->load->library('email', $config);
			//var_dump($config);

			//kirim email
			$this->email->from($senderEmail, $senderAlias);
			$this->email->to($email);
			//$this->email->cc($sendCc);
			//$this->email->bcc($sendBcc);
			$this->email->set_mailtype("html");
			$this->email->subject("Resend Password Member");
			$this->email->message($message);
			if ($this->email->send()) {
				return 1;
			} else {

				//echo $this->email->print_debugger();
				return 0;
			}
		} else {
			return 2;
		}

	}

	function checkDoubleKtpHpEmail($noktp, $hp, $email) {
		$qry = "select dfno, fullnm, idno, tel_hp, email
		       FROM msmemb 
		       where idno = '$noktp' OR tel_hp = '$hp' OR email = '$email'";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function checkDoubleKtpHpEmailTest($noktp, $hp, $email) {
		$dtaktp = "";
		$dtanohp = "";
		$dtaemail = "";
		if($noktp != null || $noktp != "") {
			$dtaktp .= " idno = '$noktp'";
		}

		if($hp != null || $hp != "") {
			$dtanohp .= " OR tel_hp = '$hp'";
		}

		/*
		if($email != null || $email != "") {
			$dtaemail .= " OR email = '$email'";
		}
		*/
		$qry = "select dfno, fullnm, idno, tel_hp, email
		       FROM msmemb 
		       where $dtaktp $dtanohp $dtaemail";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}


	function checkDoubleData($table, $field, $value) {
		if($table =="msmemb") {
			$tampil = " idno, dfno, fullnm, tel_hp, email";
		}
		$qry = "select $tampil 
		       FROM $table 
		       where $field = '".$value."'";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getListAreaForUpline() {
		$qry = "SELECT DISTINCT(a.[state]) as state, a.provinsi 
				FROM V_ECOMM_WILAYAH2 a 
				WHERE a.[state] is not null
				GROUP BY a.[state], a.provinsi
				ORDER BY a.provinsi";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListUplineByArea($area) {

		$qry = "SELECT top 10 a.dfno, a.fullnm
				FROM msmemb a 
					 INNER JOIN HILAL_STBNS_HDR B ON A.dfno=B.distributorcode 
				WHERE a.[state] = '$area' 
					AND a.fullnm NOT LIKE 'RESIGN%' AND a.fullnm NOT LIKE 'TERMIN%'
				    AND B.bonusmonth=(SELECT TOP 1 C.bonusmonth FROM HILAL_STBNS_HDR C ORDER BY C.bonusyear DESC, C.bonusmonth DESC ) 
				    AND B.bonusyear=(SELECT TOP 1 C.bonusyear FROM HILAL_STBNS_HDR C ORDER BY C.bonusyear DESC, C.bonusmonth DESC ) 
				    AND B.ppv>=400
				    AND (ABS(CAST((BINARY_CHECKSUM(*) * RAND()) as int)) % 100) < 10
				GROUP BY a.dfno, a.fullnm, a.idno
				ORDER BY a.idno";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}


	function getSponsorInfo($id, $loginid) {
		$this->db = $this->load->database('klink_mlm2010', true);
		$id = strtoupper($id);
		$stored_procedure = "exec klink_mlm2010.[dbo].SP_HILAL_WEB_CHECKUPLINE ?,?";
		//echo $stored_procedure;
		$query = $this->db->query($stored_procedure,array('dfno'=> $id,'login'=> $loginid));
		$dta = $query->result();
		if($dta[0]->hasil ==  1)
		{
			$sponsor = $this->getFullNameActiveMember("dfno", $id);
			if($sponsor > 0)
			{
				$x = jsonTrueResponse($sponsor, "Data sponsor valid..");
			}else{
				//$x = array("response" => "false", "message" => "ID Sponsor tidak ditemukan");
				$x = jsonFalseResponse("ID Sponsor tidak ditemukan");
			}
		}else{
			//$sponsor = $this->getFullNameActiveMember($id);
			$x = jsonFalseResponse("Id Sponsor Tidak 1 Jaringan Dengan user login");
			/*if($sponsor > 0)
            {
               $x = array("response" => "false", "message" => "Id Sponsor Tidak 1 Jaringan Dengan user login", "sponsorname" => $sponsor[0]->fullnm);
            }else{
               $x = array("response" => "false", "message" => "ID Sponsor tidak ditemukan");
            }*/

		}
		return $x;
	}

	function getFullNameActiveMember($field, $param) {
		$param = strtoupper($param);
		if($field == "dfno") {
			$str = "a.dfno = '$param' ";
		} else if($field == "fullnm") {
			$str = "a.fullnm LIKE '%$param%' ";
		}

		$slc = "select dfno, fullnm, tel_hp, email 
		        from msmemb a 
		        where $str
		        AND fullnm NOT LIKE 'RESIGN%' AND fullnm NOT LIKE 'TERMIN%'";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}

	function getPasswordMember($idmember) {
		$idmember = strtoupper($idmember);
		$qry = "SELECT dfno, password, email, tel_hp 
		        FROM msmemb WHERE dfno = '$idmember'";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function getPasswordStockist($id) {
		$id = strtoupper($id);
		$qry = "SELECT a.loccd, b.[password], a.latitude, a.longitude, a.tel_hp	, a.email 
		        FROM 
				mssc a 
				LEFT OUTER JOIN sc_users b ON (a.loccd = b.username)
				WHERE a.loccd = '$id'";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}


	function getDistributorInfo($idmember) {
		$idmember = strtoupper($idmember);
		$slc = "select 
		        a.*,
				CASE WHEN b.balance is null THEN 0 ELSE b.balance
				END as cashback_point
				FROM V_HILAL_API_LOGIN a
		        LEFT OUTER JOIN balance_hdr b ON (a.dfno COLLATE SQL_Latin1_General_CP1_CI_AS = b.dfno)
		        where a.dfno = '".$idmember."'";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	function myPersonalInfo($idmember) {
		$idmember = strtoupper($idmember);
		$slc = "SELECT a.*,
				CASE WHEN b.balance is null THEN 0 ELSE b.balance
				END as cashback_point
				FROM V_HILAL_API_LOGIN a
		        LEFT OUTER JOIN balance_hdr b ON (a.dfno COLLATE SQL_Latin1_General_CP1_CI_AS = b.dfno)
		        where a.dfno = '".$idmember."'";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	function getDistributorInfo2($param, $value) {
		$value = strtoupper($value);
		$str = "";
		if($param == "dfno") {
			$str = " a.dfno = '$value'";
		} else if($param == "membername") {
			$str = " a.fullnm LIKE '%$value%'";
		} else if($param == "noktp") {
			$str = " a.idno LIKE '%$value%'";
		} else if($param == "idsponsor") {
			$str = " a.sfno = '$value'";
		} else if($param == "idrecruiter") {
			$str = " a.sfno_reg = '$value'";
		}
		$slc = "SELECT a.*,
				CASE WHEN b.balance is null THEN 0 ELSE b.balance
				END as cashback_point
				FROM V_HILAL_API_LOGIN a
		        LEFT OUTER JOIN balance_hdr b ON (a.dfno COLLATE SQL_Latin1_General_CP1_CI_AS = b.dfno)
		       WHERE $str";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	function checkAuthLogin($idmember, $password) {
		$idmember = strtoupper($idmember);
		$slc = "select a.*, ISNULL(b.balance, 0) as cashback_point
		       from V_HILAL_API_LOGIN a 
		       LEFT OUTER JOIN balance_hdr b ON (a.dfno COLLATE SQL_Latin1_General_CP1_CI_AS = b.dfno)
		       where a.dfno = '".$idmember."' AND a.password = '".$password."'";

		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	function updatePassword($idmember, $newPassword) {
		$qry = "UPDATE msmemb SET password = '$newPassword' WHERE dfno = '$idmember'";
		$res = $this->executeQuery($qry, $this->db2);
		return $res;
	}

	function checkValidasiVoucher($voucherno, $voucherkey) {
		$qry = "SELECT  a.formno, a.prdcd, a.vchkey, a.status, a.activate_dfno, b.fullnm
				FROM starterkit a
				LEFT JOIN msmemb b ON (a.activate_dfno = b.dfno)
				WHERE a.formno = '$voucherno' and a.vchkey = '$voucherkey'";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function getListStarterkit(){
		/*$qry = "select * from master_prd_cat_inv a
                where a.is_starterkit='1' and a.ecomm_status='1' and a.[status]='1'";*/
		$tblx = $this->tbl_pricelist;
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_mb as img_url, 
                    a.price_w, 
                    a.price_e, 
                    a.bv,
                    a.weight,
                    a.is_charge_ship
                from $tblx a
                where a.is_starterkit = '1'and a.price_w is not null and a.price_e is not null";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function getDetailPrdcdByID($prdcd) {
		/*$qry = "select a.prdcd,
				      a.prdnm, 
				      a.prdcdcat, 
				      a.prdnmcatnm,
				      a.img_mb as img_url, 
				      a.price_w, 
				      a.price_e,
				      a.price_ce,
                      a.price_cw, 
				      a.bv,
				      a.weight,
				      a.is_starterkit,
		              a.is_charge_ship,
		              a.is_discontinue
				from V_Ecomm_PriceList a
				where a.prdcd='$prdcd'
					  AND A.ecomm_status='1'";*/
		$tblx = $this->tbl_pricelistDion;
		$qry = "select a.prdcd,
				      a.prdnm,
				      a.prdcdcat,
				      a.prdnmcatnm,
				      a.price_w,
				      a.price_e,
				      a.price_ce,
                      a.price_cw,
				      a.bv,
				      a.weight,
				      a.is_starterkit,
		              a.is_charge_ship,
		              a.is_discontinue
				from $tblx a
				where a.prdcd='$prdcd' and a.price_w is not null and a.price_e is not null";

		return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function getListProductDisplay() {
		$qry = "SELECT a.id_head as kode_produk, a.prd_desc
				FROM master_prd_cat_inv_mobile a  
				WHERE a.prd_desc != '' AND a.prd_desc is not null
				AND a.ecomm_status = '1' AND a.web_status = '1'
				GROUP BY a.id_head, a.prd_desc";
		return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function getCatalogPrd() {
		//$arr = array();
		//$arrx = array();
		$conx = $this->load->database("db_ecommerce", true);
		/*$qry = " SELECT DISTINCT(a.cat) as catalog_id, a.cat_desc as catalog_desc, 
				   CONVERT(VARCHAR(10), a.createdt , 103) as createdt
				 FROM catalog_product a";
				 */
		$qry = "SELECT a.id, a.cat as catalog_id, a.cat_desc as catalog_desc,
				 CONVERT(VARCHAR(10), a.createdt , 103) as createdt,
				 a.kode_produk, a.title as product_desc, a.imageUrl, a.tipe 
				FROM catalog_product a
				ORDER BY a.cat";
		$res = $conx->query($qry);
		$arrTemp = array();
		$arr = array();
		$i=0;
		$j=0;
		$count = 0;
		if($res->result() != null) {
			$resxc = $res->result();
			foreach ($resxc as $data) {
				if(!in_array($data->catalog_id, $arrTemp)) {
					$j = 0;
					if($count > 0) {
						$i++;
					}
					array_push($arrTemp, $data->catalog_id);

					$arr[$i]['id']= $data->catalog_id;
					$arr[$i]['cat_desc'] = $data->catalog_desc;
					$arr[$i]['date'] = $data->createdt;
					$arr[$i]['tipe'] = $data->tipe;

					$arr[$i]['products'][$j]['id'] = $data->id;
					$arr[$i]['products'][$j]['kode_produk'] = $data->kode_produk;
					$arr[$i]['products'][$j]['product_desc'] = $data->product_desc;
					$arr[$i]['products'][$j]['imageUrl'] = $data->imageUrl;
					$count++;
					//array_push($arrx, $arr);
				} else {

					$j++;
					$arr[$i]['products'][$j]['id'] = $data->id;
					$arr[$i]['products'][$j]['kode_produk'] = $data->kode_produk;
					$arr[$i]['products'][$j]['product_desc'] = $data->product_desc;
					$arr[$i]['products'][$j]['imageUrl'] = $data->imageUrl;
				}
			}
		} else {
			$arr = null;
		}

		return $arr;
	}


	function getListProductDisplayDetail($id) {
		$arr = array();
		$conx = $this->load->database("db_ecommerce", true);
		$qry = "SELECT a.cat_inv_id, a.is_series, kode_produk, a.prd_desc, a.is_type, a.is_size, 
		           a.is_color, a.img_url,
		           a.prd_specification, a.prd_howtouse, a.prd_benefit, a.prd_recommend
				FROM V_API_PRD_LIST_HEAD a  
				WHERE a.kode_produk = '$id'
				
				ORDER BY CAST(kode_produk AS INT) ";
		//$type =  $this->getRecordset($qry, null, $this->setDB(1));
		$res = $conx->query($qry);

		if($res->result() != null) {
			$type = array();
			$type_temp = array();
			$isi = array();
			$isi_temp = array();
			$warna = array();
			$warna_temp = array();
			$resxc = $res->result();
			$arr['prdcd'] = $resxc[0]->cat_inv_id;
			$arr['kode_produk'] = $resxc[0]->kode_produk;
			$arr['desc_produk'] = $resxc[0]->prd_desc;
			$arr['varian'] = $resxc[0]->is_series;
			$arr['img_url'] = $resxc[0]->img_url;
			//if($resxc[0]->is_series == "0") {
			$arr['prd_specification'] = $resxc[0]->prd_specification;
			$arr['prd_howtouse'] = $resxc[0]->prd_howtouse;
			$arr['prd_benefit'] = $resxc[0]->prd_benefit;

			//}
			$is_type = $resxc[0]->is_type;
			$is_size = $resxc[0]->is_size;
			$is_warna = $resxc[0]->is_color;
			$i_tipe = 0;
			$i_size = 0;
			$i_warna = 0;

			foreach ($res->result() as $row) {
				if($row->is_type != null && $row->is_type != "") {

					if (!in_array($row->is_type, $type_temp)) {
						array_push($type_temp, $row->is_type);
						//array_push($type, $row->is_type);
						//array_push($type['img_url'], $row->img_url);
						//$type[$i_tipe]['urutan'] = $i_tipe;
						$type[$i_tipe]['nama'] = $row->is_type;
						$type[$i_tipe]['img_url'] = $row->img_url;
						$type[$i_tipe]['prd_specification'] = $resxc[0]->prd_specification;
						$type[$i_tipe]['prd_howtouse'] = $resxc[0]->prd_howtouse;
						$type[$i_tipe]['prd_benefit'] = $resxc[0]->prd_benefit;
						$i_tipe++;
					}
				}


				if($row->is_size != null && $row->is_size != "") {
					if (!in_array($row->is_size, $isi_temp)) {
						array_push($isi_temp, $row->is_size);
						//array_push($isi, $row->is_size);
						$isi[$i_size]['nama'] = $row->is_size;
						$isi[$i_size]['img_url'] = $row->img_url;
						$i_size++;
					}
				}

				if($row->is_color != null && $row->is_color != "") {
					if (!in_array($row->is_color, $warna_temp)) {
						array_push($warna_temp, $row->is_color);
						$warna[$i_warna]['nama'] = $row->is_color;
						$warna[$i_warna]['img_url'] = $row->img_url;
						$i_warna++;
					}
				}

				//array_push($arr['type'], $row->is_type);
			}
			if(isset($type)) {
				$arr['jenis_produk'] = $type;
			}else {
				$arr['jenis_produk'] = null;
			}

			if(isset($isi)) {
				$arr['ukuran_isi'] = $isi;
			}else {
				$arr['ukuran_isi'] = null;
			}

			if(isset($warna)) {
				$arr['warna'] = $warna;
			}else {
				$arr['warna'] = null;
			}
		} else {
			$arr = null;
		}

		return $arr;
	}

	function getDetailProduk($head, $jenis_prd=null, $ukuran=null, $warna=null) {
		$tblx = $this->tbl_pricelist;
		$qry = "select a.prdcd, 
				      a.prdnm, 
				      a.prdcdcat, 
				      a.prdnmcatnm,
				      a.img_mb as img_url, 
				      a.price_w, 
				      a.price_e,
				      a.price_ce,
				      a.price_cw, 
				      a.bv,
				      a.weight,
				      a.is_starterkit,
				      a.is_charge_ship,
				      a.is_discontinue,
				      b.prd_recommend
				FROM $tblx a
				LEFT JOIN master_prd_cat_inv_mobile b on (a.prdcd = b.cat_inv_id)
				WHERE b.id_head = '$head' 
				  AND b.is_type = '$jenis_prd' 
				  AND b.is_size = '$ukuran'
				  AND b.is_color = '$warna'
				  AND a.price_w is not null and a.price_e is not null";
		//echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function getDetailProduk2($head) {
		$tblx = $this->tbl_pricelist;
		$qry = "select a.prdcd, 
				      a.prdnm, 
				      a.prdcdcat, 
				      a.prdnmcatnm,
				      a.img_mb as img_url, 
				      a.price_w, 
				      a.price_e,
				      a.price_ce,
				      a.price_cw, 
				      a.bv,
				      a.weight,
				      a.is_starterkit,
				      a.is_charge_ship,
				      a.is_discontinue,
				      b.prd_recommend,
				      b.is_type as jenis_produk,
				      b.is_size as ukuran_isi,
				      b.is_color as warna
				FROM $tblx a
				LEFT JOIN master_prd_cat_inv_mobile b on (a.prdcd = b.cat_inv_id)
				WHERE b.id_head = '$head'";
		return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function setShippingPriceInfo($id_shipper, $arr) {

		if($id_shipper == "1") {
			if(!array_key_exists("status", $arr)) {
				$test = array();
				$arrx = null;
				foreach($arr->price as $dta) {
					if($dta->service_code != "OKE15" && $dta->service_code != "CTCOKE15") {
						$arrx = array(
							"service_code" => $dta->service_code,
							"service_display" => $dta->service_display,
							"origin_name" => $dta->origin_name,
							"destination_name" => $dta->destination_name,
							"price" => $dta->price,
							"etd_from" => $dta->etd_from,
							"etd_thru" => $dta->etd_thru,
							"times" => $dta->times,
						);
						array_push($test, $arrx);
					}

				}
				//$temp_price =
				$lowest = $test[0]['price'];
				//print_r($test[0]['price']);
				//echo "Harga pertama : $lowest<br />";
				foreach($test as $dta) {
					if($dta['price'] <= $lowest) {
						$lowest = $dta['price'];
						$arrx = array(
							"service_code" => $dta['service_code'],
							"service_display" => $dta['service_display'],
							"origin_name" => $dta['origin_name'],
							"destination_name" => $dta['destination_name'],
							"price" => $dta['price'],
							"etd_from" => $dta['etd_from'],
							"etd_thru" => $dta['etd_thru'],
							"times" => $dta['times']
						);
					}
				}
				$dtaxc = array();
				$dtaxc[0] = $arrx;
				return $dtaxc;
			} else {

			}

		} else if($id_shipper == "2") {
			$arrx = array();
			if($arr->status == "000") {
				$harga = $arr->Tarif + $arr->PPN;
				$arrx[0] = array(
					"origin_name" => $arr->NamaOrigin,
					"destination_name" => $arr->NamaDestinasi,
					"service_code" => "",
					"service_display" => "",
					"price" => $harga,
					"etd_from" => "",
					"etd_thru" => "",
					"times" => "",
				);
			} else {
				$arrx[0] = array(
					"origin_name" => "",
					"destination_name" => "",
					"service_code" => "",
					"service_display" => "",
					"price" => "",
					"etd_from" => "",
					"etd_thru" => "",
					"times" => "",
				);
			}
			//print_r($arrx);
			return $arrx;

		}
	}

	function showHeaderTrans($idmember){
		$qry = "SELECT dfno, fullnm FROM klink_mlm2010.[dbo].msmemb
                WHERE dfno = '".$idmember."'";

		//echo "query ini ".$qry;
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function showDetailTrans($idmember,$bln,$thn){
		$qry = "SELECT
                      a.dfno AS idmember,
                      a.trtype,
                      a.trcd,
                      a.trcd2,
                      a.orderno,
                      a.sc_dfno,
                      a.ndp,
                      a.nbv,
                      MONTH(a.bnsperiod) AS bln,
                      YEAR(a.bnsperiod) AS thn,
                      CONVERT(VARCHAR(30), a.etdt, 103) AS tglc,
                      sc_newtrh.csno 
                    FROM
                      klink_mlm2010.[dbo].newtrh a
                      LEFT OUTER JOIN klink_mlm2010.[dbo].sc_newtrh ON (a.trcd = sc_newtrh.trcd)
                    WHERE
                      (a.dfno = '".$idmember."') AND
                      (MONTH(a.bnsperiod) = ".$bln.") AND
                      (YEAR(a.bnsperiod) = ".$thn.") AND (a.nbv <> 0) AND LEFT(a.trcd,2)='ID'";
		//echo "query itu ".$qry;

		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	//function detailKnetTrx($idmember,$bln,$thn) {
	function detailKnetTrxByBnsPeriod($idmember,$bln,$thn, $delivery = null) {
		$conn  = $this->load->database("db_ecommerce", TRUE);
		$str = "";
		if($delivery != null) {
			$str = " AND a.delivery_status = '$delivery'";
		}
		$qry = "SELECT * FROM V_ECOMM_DETAIL_KNET_TRX a
    	        WHERE log_usrlogin = '".$idmember."' and
				bonusmonth = '".$bln."/".$thn."' AND a.orderno LIKE 'IDEC%' $str
				ORDER BY orderno ASC";
		//echo $qry;
		$resx = $conn->query($qry);
		$dtax = $resx->result();
		$arrTemp = array();
		$arr = array();
		$i=0;
		$j=0;
		$count = 0;
		foreach ($resx->result() as $data) {
			if(!in_array($data->orderno, $arrTemp)) {
				$j = 0;
				if($count > 0) {
					$i++;
				}
				array_push($arrTemp, $data->orderno);
				$arr[$i]['orderno'] = $data->orderno;
				$arr[$i]['memberid'] = $data->memberid;
				$arr[$i]['membername'] = $data->membername;
				$arr[$i]['bonusmonth'] = $data->bonusmonth;
				$arr[$i]['pricecode'] = $data->pricecode;
				$arr[$i]['idstockist'] = $data->idstk;
				$arr[$i]['trx_date'] = $data->trx_date;
				$arr[$i]['bonusmonth'] = $data->bonusmonth;
				$arr[$i]['total_pay'] = $data->total_pay;
				$arr[$i]['total_bv'] = $data->total_bv;
				$arr[$i]['shipping_cost'] = $data->shipping_cost;
				$arr[$i]['admin_cost'] = $data->admin_cost;
				$arr[$i]['shipping_info']['sendTo'] =  $data->sentTo;
				if($data->sentTo == "2") {

					$arr[$i]['shipping_info']['receiver_name'] =  $data->receiver_name;
					$arr[$i]['shipping_info']['receiver_addr'] =  $data->receiver_addr;
					$arr[$i]['shipping_info']['conote'] =  $data->conoteJNE;
					$arr[$i]['shipping_info']['delivery_status'] =  $data->delivery_status;
					//$arr['shipping_info']['secno_pickup_stockist'] =  null;
				} else {
					//$arr['shipping_info']['send_to'] =  "Stockist";
					$stk_res = $this->getStkAddress($data->idstk);
					$arr[$i]['shipping_info']['receiver_name'] =  $stk_res[0]->loccd." - ".$stk_res[0]->fullnm;
					$arr[$i]['shipping_info']['receiver_addr'] =  $stk_res[0]->address;
					$arr[$i]['shipping_info']['conote'] =  null;
					$arr[$i]['shipping_info']['delivery_status'] =  $data->delivery_status;
					//$arr['shipping_info']['secno_pickup_stockist'] =  $dtax[0]->secno;
				}
				/*
				$arr[$i]['products'][$j]['prdcd'] = $data->prdcd;
	    		$arr[$i]['products'][$j]['prdnm'] = $data->prdnm;
	    		$arr[$i]['products'][$j]['qty'] = $data->qty;
	    		$arr[$i]['products'][$j]['bv'] = $data->bv;
	    		$arr[$i]['products'][$j]['price'] = $data->price;
	    		$arr[$i]['products'][$j]['sub_tot_bv'] = $data->sub_tot_bv;
	    		$arr[$i]['products'][$j]['sub_tot_price'] = $data->sub_tot_price;
				$arr[$i]['products'][$j]['img_url'] = $data->img_url;
				*/
				$count++;

			} else {
				///$x=$i-1;
				$j++;
				/*$arr[$i]['products'][$j]['prdcd'] = $data->prdcd;
                $arr[$i]['products'][$j]['prdnm'] = $data->prdnm;
                $arr[$i]['products'][$j]['qty'] = $data->qty;
                $arr[$i]['products'][$j]['bv'] = $data->bv;
                $arr[$i]['products'][$j]['price'] = $data->price;
                $arr[$i]['products'][$j]['sub_tot_bv'] = $data->sub_tot_bv;
                $arr[$i]['products'][$j]['sub_tot_price'] = $data->sub_tot_price;
                $arr[$i]['products'][$j]['img_url'] = $data->img_url;
               */
			}

		}
		return $arr;

	}

	function detailKnetTrxByOrderNo($idmember, $orderno) {
		$conn  = $this->load->database("db_ecommerce", TRUE);
		/*$qry = "SELECT * FROM V_ECOMM_DETAIL_KNET_TRX a
                WHERE log_dfno = '".$idmember."' and
                bonusmonth = '".$bln."/".$thn."'
                ORDER BY orderno ASC"; */

		$qry = "SELECT * FROM V_ECOMM_DETAIL_KNET_TRX a 
    	        WHERE log_usrlogin = '".$idmember."' and orderno = '$orderno'
				ORDER BY orderno ASC";
		//echo $qry;
		$resx = $conn->query($qry);
		$dtax = $resx->result();
		$arrTemp = array();
		$arr = array();
		$i=0;
		$j=0;
		$count = 0;
		foreach ($resx->result() as $data) {
			if(!in_array($data->orderno, $arrTemp)) {
				$j = 0;
				if($count > 0) {
					$i++;
				}
				array_push($arrTemp, $data->orderno);
				$arr[$i]['orderno'] = $data->orderno;
				$arr[$i]['memberid'] = $data->memberid;
				$arr[$i]['membername'] = $data->membername;
				$arr[$i]['bonusmonth'] = $data->bonusmonth;
				$arr[$i]['pricecode'] = $data->pricecode;
				$arr[$i]['idstockist'] = $data->idstk;
				$arr[$i]['trx_date'] = $data->trx_date;
				$arr[$i]['bonusmonth'] = $data->bonusmonth;
				$arr[$i]['total_pay'] = $data->total_pay;
				$arr[$i]['total_bv'] = $data->total_bv;
				$arr[$i]['shipping_cost'] = $data->shipping_cost;
				$arr[$i]['admin_cost'] = $data->admin_cost;
				$arr[$i]['shipping_info']['sendTo'] =  $data->sentTo;
				if($data->sentTo == "2") {

					$arr[$i]['shipping_info']['receiver_name'] =  $data->receiver_name;
					$arr[$i]['shipping_info']['receiver_addr'] =  $data->receiver_addr;
					$arr[$i]['shipping_info']['conote'] =  $data->conoteJNE;
					$arr[$i]['shipping_info']['delivery_status'] =  $data->delivery_status;
					$arr[$i]['shipping_info']['lat_dest'] =  $data->lat_dest;
					$arr[$i]['shipping_info']['long_dest'] =  $data->long_dest;
					//$arr['shipping_info']['secno_pickup_stockist'] =  null;
				} else {
					//$arr['shipping_info']['send_to'] =  "Stockist";
					$stk_res = $this->getStkAddress($data->idstk);
					$arr[$i]['shipping_info']['receiver_name'] =  $stk_res[0]->loccd." - ".$stk_res[0]->fullnm;
					$arr[$i]['shipping_info']['receiver_addr'] =  $stk_res[0]->address;
					$arr[$i]['shipping_info']['conote'] =  null;
					$arr[$i]['shipping_info']['delivery_status'] =  $data->delivery_status;
					//$arr['shipping_info']['secno_pickup_stockist'] =  $dtax[0]->secno;
				}

				$arr[$i]['products'][$j]['prdcd'] = $data->prdcd;
				$arr[$i]['products'][$j]['prdnm'] = $data->prdnm;
				$arr[$i]['products'][$j]['qty'] = $data->qty;
				$arr[$i]['products'][$j]['bv'] = $data->bv;
				$arr[$i]['products'][$j]['price'] = $data->price;
				$arr[$i]['products'][$j]['sub_tot_bv'] = $data->sub_tot_bv;
				$arr[$i]['products'][$j]['sub_tot_price'] = $data->sub_tot_price;
				$arr[$i]['products'][$j]['img_url'] = $data->img_url;

				$count++;

			} else {
				///$x=$i-1;
				$j++;
				$arr[$i]['products'][$j]['prdcd'] = $data->prdcd;
				$arr[$i]['products'][$j]['prdnm'] = $data->prdnm;
				$arr[$i]['products'][$j]['qty'] = $data->qty;
				$arr[$i]['products'][$j]['bv'] = $data->bv;
				$arr[$i]['products'][$j]['price'] = $data->price;
				$arr[$i]['products'][$j]['sub_tot_bv'] = $data->sub_tot_bv;
				$arr[$i]['products'][$j]['sub_tot_price'] = $data->sub_tot_price;
				$arr[$i]['products'][$j]['img_url'] = $data->img_url;

			}

		}
		return $arr;
		//print_r($arr);
	}

	function showDetailTransKnetDetailUtama($dtax) {

		if($dtax != null) {
			$arr = array();
			$arr['orderno'] = $dtax[0]->orderno;
			$arr['memberid'] = $dtax[0]->memberid;
			$arr['membername'] = $dtax[0]->membername;
			$arr['bonusmonth'] = $dtax[0]->bonusmonth;
			$arr['pricecode'] = $dtax[0]->pricecode;
			$arr['idstockist'] = $dtax[0]->idstk;
			$arr['trx_date'] = $dtax[0]->trx_date;
			$arr['bonusmonth'] = $dtax[0]->bonusmonth;
			$arr['total_pay'] = $dtax[0]->total_pay;
			$arr['total_bv'] = $dtax[0]->total_bv;
			$arr['shipping_cost'] = $dtax[0]->shipping_cost;
			$arr['admin_cost'] = $dtax[0]->admin_cost;
			$arr['shipping_info']['sendTo'] =  $dtax[0]->sentTo;
			if($dtax[0]->sentTo == "2") {

				$arr['shipping_info']['receiver_name'] =  $dtax[0]->receiver_name;
				$arr['shipping_info']['receiver_addr'] =  $dtax[0]->receiver_addr;
				$arr['shipping_info']['conote'] =  $dtax[0]->conoteJNE;
				$arr['shipping_info']['delivery_status'] =  $dtax[0]->delivery_status;
				//$arr['shipping_info']['secno_pickup_stockist'] =  null;
			} else {
				//$arr['shipping_info']['send_to'] =  "Stockist";
				$stk_res = $this->getStkAddress($dtax[0]->idstk);
				$arr['shipping_info']['receiver_name'] =  $stk_res[0]->loccd." - ".$stk_res[0]->fullnm;
				$arr['shipping_info']['receiver_addr'] =  $stk_res[0]->address;
				$arr['shipping_info']['conote'] =  null;
				$arr['shipping_info']['delivery_status'] =  $dtax[0]->delivery_status;
				//$arr['shipping_info']['secno_pickup_stockist'] =  $dtax[0]->secno;
			}
			$i=0;
			foreach ($dtax as $rowx) {
				$arr['products'][$i]['prdcd'] = $rowx->prdcd;
				$arr['products'][$i]['prdnm'] = $rowx->prdnm;
				$arr['products'][$i]['qty'] = $rowx->qty;
				$arr['products'][$i]['bv'] = $rowx->bv;
				$arr['products'][$i]['price'] = $rowx->price;
				$arr['products'][$i]['sub_tot_bv'] = $rowx->sub_tot_bv;
				$arr['products'][$i]['sub_tot_price'] = $rowx->sub_tot_price;
				$arr['products'][$i]['img_url'] = $rowx->img_url;
				$i++;
			}
		} else {
			$arr = null;
		}
		return $arr;
	}

	function showDetailTransKnetDetail($dtax) {

		if($dtax != null) {
			$arr = array();
			$arr['orderno'] = $dtax[0]->orderno;
			$arr['memberid'] = $dtax[0]->memberid;
			$arr['membername'] = $dtax[0]->membername;
			$arr['bonusmonth'] = $dtax[0]->bonusmonth;
			$arr['pricecode'] = $dtax[0]->pricecode;
			$arr['idstockist'] = $dtax[0]->idstk;
			$arr['trx_date'] = $dtax[0]->trx_date;
			$arr['bonusmonth'] = $dtax[0]->bonusmonth;
			$arr['total_pay'] = $dtax[0]->total_pay;
			$arr['total_bv'] = $dtax[0]->total_bv;
			$arr['shipping_cost'] = $dtax[0]->shipping_cost;
			$arr['admin_cost'] = $dtax[0]->admin_cost;
			$arr['shipping_info']['sendTo'] =  $dtax[0]->sentTo;
			if($dtax[0]->sentTo == "2") {

				$arr['shipping_info']['receiver_name'] =  $dtax[0]->receiver_name;
				$arr['shipping_info']['receiver_addr'] =  $dtax[0]->receiver_addr;
				$arr['shipping_info']['conote'] =  $dtax[0]->conoteJNE;
				$arr['shipping_info']['delivery_status'] =  $dtax[0]->delivery_status;
				//$arr['shipping_info']['secno_pickup_stockist'] =  null;
			} else {
				//$arr['shipping_info']['send_to'] =  "Stockist";
				$stk_res = $this->getStkAddress($dtax[0]->idstk);
				$arr['shipping_info']['receiver_name'] =  $stk_res[0]->loccd." - ".$stk_res[0]->fullnm;
				$arr['shipping_info']['receiver_addr'] =  $stk_res[0]->address;
				$arr['shipping_info']['conote'] =  null;
				$arr['shipping_info']['delivery_status'] =  $dtax[0]->delivery_status;
				//$arr['shipping_info']['secno_pickup_stockist'] =  $dtax[0]->secno;
			}
			$i=0;
			foreach ($dtax as $rowx) {
				$arr['products'][$i]['prdcd'] = $rowx->prdcd;
				$arr['products'][$i]['prdnm'] = $rowx->prdnm;
				$arr['products'][$i]['qty'] = $rowx->qty;
				$arr['products'][$i]['bv'] = $rowx->bv;
				$arr['products'][$i]['price'] = $rowx->price;
				$arr['products'][$i]['sub_tot_bv'] = $rowx->sub_tot_bv;
				$arr['products'][$i]['sub_tot_price'] = $rowx->sub_tot_price;
				$arr['products'][$i]['img_url'] = $rowx->img_url;
				$i++;
			}
		} else {
			$arr = null;
		}
		return $arr;
	}

	function showDetailTransKnet($idmember,$bln,$thn){
		$qry= "
		SELECT a.orderno, a.id_memb as idmember,
					a.nmmember as membername,
					c.conoteJNE, a.idstk, d.fullnm as stkname,
					d.addr1+' '+d.addr2+' '+d.addr3
					as address,
					a.sentTo,
					a.total_pay, a.total_bv,
					a.payShip - a.discount_shipping as shipping_cost,
					a.payAdm as admin_cost
				FROM ecomm_trans_hdr a
				INNER JOIN log_trans b ON (a.orderno = b.log_trcd)
				LEFT OUTER JOIN ecomm_trans_shipaddr c ON (a.orderno = c.orderno)
				INNER JOIN klink_mlm2010.dbo.mssc d ON (a.idstk COLLATE SQL_Latin1_General_CP1_CS_AS = d.loccd)
				WHERE b.log_dfno = '".$idmember."'
				and
				a.bonusmonth = '".$bln."/".$thn."'
		";
//    	$qry = "SELECT a.orderno, a.id_memb as idmember,
//					a.nmmember as membername,
//					c.conoteJNE, a.idstk, d.fullnm as stkname, d.addr1, d.addr2, d.addr3, a.sentTo,
//					a.total_pay, a.total_bv,
//					a.payShip - a.discount_shipping as shipping_cost,
//					a.payAdm as admin_cost
//				FROM ecomm_trans_hdr a
//				INNER JOIN log_trans b ON (a.orderno = b.log_trcd)
//				LEFT OUTER JOIN ecomm_trans_shipaddr c ON (a.orderno = c.orderno)
//				INNER JOIN klink_mlm2010.dbo.mssc d ON (a.idstk COLLATE SQL_Latin1_General_CP1_CS_AS = d.loccd)
//				WHERE b.log_dfno = '$idmember' AND
//				a.bonusmonth = '$bln/$thn'";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function ListProdByCatHeader($value) {
		$qry = "SELECT
				    	ROW_NUMBER() OVER (ORDER BY a.id_head desc) as RowNumber,
				    	a.id_head as kode_produk, 
				    	a.prd_desc
					FROM master_prd_cat_inv_mobile a  
					INNER JOIN master_prd_cat_inv b ON (a.cat_inv_id = b.cat_inv_id)
					WHERE a.prd_desc != '' 
					AND a.prd_desc is not null
					AND b.ecomm_status = '1' 
					AND b.web_status = '1'
					AND b.[status] = '1'
					AND a.cat_id = '$value'
					GROUP BY a.id_head, a.prd_desc";
		return $this->getRecordset($qry, null, $this->db1);
	}

	function getProdByName($name) {
		$tblx = $this->tbl_pricelist;
		$qry = "select
	                c.id_head as varian_id,
	                a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight
                from $tblx a
                LEFT JOIN master_prd_cat_inv_mobile c ON (a.prdcd = c.cat_inv_id)
                WHERE prdnm LIKE '%$name%' AND a.is_starterkit='0' 
                	  AND A.ecomm_status='1' and a.price_w is not null and a.price_e is not null";
		return $this->getRecordset($qry, null, $this->db1);
	}

	function listDetailProdByName($searchBy, $value, $limit, $offset) {
		if($searchBy == "prdcdcat") {
			$str = "A.prdcdcat = '".$value."' AND A.ecomm_status='1' ";
		} else if($searchBy == "prdnm") {
			$str = "A.prdnm LIKE '%".$value."%' AND A.ecomm_status='1' z";
		}
		$first = 0;
		$last = 0;
		if($offset == 0 || $offset == 1){
			$first = 1;
			$last  = $limit;
			//echo "test ".$no_page."-".$first."-".$last."<br>";
		} else {
			$xx = $offset - 1;
			if($xx == 0){
				$xx = 1;
			}
			//echo $xx;
			$first = $xx * $limit + 1;
			$last  = $first + ($limit - 1);
		}
		$tblx = $this->tbl_pricelist;
		$qry = "SELECT XX.RowNumber,
		               XX.varian_id,
                	   XX.prdcd,
                	   XX.prdnm,
                	   XX.prdcdcat,
                       XX.prdnmcatnm,
                	   XX.img_url,
                	   XX.price_w,
                	   XX.price_e,
                	   XX.price_cw,
                	   XX.price_ce,
                	   XX.bv,
                	   XX.weight,
                	   XX.is_starterkit,
				       XX.is_charge_ship,
				       XX.is_discontinue
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY a.prdcd desc) as RowNumber,
                    	       c.id_head as varian_id,
                        	   A.prdcd,
                    	       A.prdnm,
                    	       A.prdcdcat,
                               a.prdnmcatnm,
                    	       A.img_mb as img_url,
                    	       A.price_w,
                    	       A.price_e,
                    	       A.price_cw,
                    	       A.price_ce,
                    	       A.bv,
                    	       A.weight,
                    	       A.is_starterkit,
				               A.is_charge_ship,
				               A.is_discontinue
                    	FROM $tblx a 
                    	LEFT JOIN master_prd_cat_inv_mobile c ON (a.prdcd = c.cat_inv_id)
                    	WHERE a.price_w is not null and a.price_e is not null AND $str 
                    	and A.is_starterkit='0'
                     ) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
		//echo $qry;
		//return array("offset" => $offset, "first" => $first, "last" => $last);
		return $this->getRecordset($qry, null, $this->db1);
	}

	function ListProdByName($searchBy, $value, $limit, $offset) {
		$first = 0;
		$last = 0;
		if($offset == 0 || $offset == 1){
			$first = 1;
			$last  = $limit;
			//echo "test ".$no_page."-".$first."-".$last."<br>";
		} else {
			$xx = $offset - 1;
			if($xx == 0){
				$xx = 1;
			}
			//echo $xx;
			$first = $xx * $limit + 1;
			$last  = $first + ($limit - 1);
		}

		$qry = "SELECT XX.RowNumber,
				       XX.kode_produk,
				       XX.prd_desc
				FROM(
					SELECT 
				    	ROW_NUMBER() OVER (ORDER BY a.id_head desc) as RowNumber,
				    	a.id_head as kode_produk, 
				    	a.prd_desc
					FROM master_prd_cat_inv_mobile a  
					WHERE a.prd_desc != '' AND a.prd_desc is not null
					AND a.ecomm_status = '1' AND a.web_status = '1'
					AND a.$searchBy LIKE '%$value%'
					GROUP BY a.id_head, a.prd_desc
				) XX
				WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
		//echo $qry;
		//return array("offset" => $offset, "first" => $first, "last" => $last);
		return $this->getRecordset($qry, null, $this->db1);
	}

	function ListProdByCat($searchBy, $value, $limit, $offset){
		if($searchBy == "prdcdcat") {
			$str = "A.prdcdcat = '".$value."' ";
		} else if($searchBy == "prdnm") {
			$str = "A.prdnm LIKE '%".$value."%' ";
		}
		$first = 0;
		$last = 0;
		if($offset == 0 || $offset == 1){
			$first = 1;
			$last  = $limit;
			//echo "test ".$no_page."-".$first."-".$last."<br>";
		} else {
			$xx = $offset - 1;
			if($xx == 0){
				$xx = 1;
			}
			//echo $xx;
			$first = $xx * $limit + 1;
			$last  = $first + ($limit - 1);
		}

		/*$qry = "SELECT XX.RowNumber,
                       XX.prdcd,
                       XX.prdnm,
                       XX.prdcdcat,
                       XX.prdnmcatnm,
                       XX.img_url,
                       XX.price_w,
                       XX.price_e,
                       XX.price_cw,
                       XX.price_ce,
                       XX.bv,
                       XX.weight,
                       XX.is_starterkit,
                       XX.is_charge_ship,
                       XX.is_discontinue
                FROM(
                        SELECT ROW_NUMBER() OVER (ORDER BY a.prdcd desc) as RowNumber,
                               A.prdcd,
                               A.prdnm,
                               A.prdcdcat,
                               a.prdnmcatnm,
                               A.img_url,
                               A.price_w,
                               A.price_e,
                               A.price_cw,
                               A.price_ce,
                               A.bv,
                               A.weight,
                               A.is_starterkit,
                               A.is_charge_ship,
                               A.is_discontinue
                        FROM V_Ecomm_PriceList a
                        WHERE $str
                        and A.is_starterkit='0'
                     ) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last.""; */
		$tblx = $this->tbl_pricelist;
		$qry = "SELECT XX.RowNumber,
				       XX.kode_produk,
				       XX.prd_desc
				FROM(
					SELECT 
				    	ROW_NUMBER() OVER (ORDER BY a.id_head desc) as RowNumber,
				    	a.id_head as kode_produk, 
				    	a.prd_desc
					FROM master_prd_cat_inv_mobile a  
					INNER JOIN master_prd_cat_inv b ON (a.cat_inv_id = b.cat_inv_id)
					WHERE a.prd_desc != '' 
					 AND a.prd_desc is not null
					 AND b.ecomm_status = '1' 
					 AND b.web_status = '1'
					 AND b.[status] = '1'
					 AND a.cat_id = '$value'
					GROUP BY a.id_head, a.prd_desc
				) XX
				WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
		//return array("offset" => $offset, "first" => $first, "last" => $last);
		return $this->getRecordset($qry, null, $this->db1);

	}

	function getListRecruit($idmember, $bln, $thn) {
		$qry = "SELECT a.dfno, a.fullnm, CONVERT(VARCHAR(30),a.jointdt,103) as jointdt, 
					a.sponsorid, b.fullnm as sponsorname 
				FROM msmemb a 
				INNER JOIN msmemb b ON (a.sponsorid = b.dfno) 
				WHERE a.sfno_reg = '$idmember' AND 
				month(a.jointdt)=$bln and year(a.jointdt)=$thn
				ORDER BY a.jointdt, a.fullnm";
		//	echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}



	function getStateByStk($idstk) {
		$qry = "select [state] from mssc where loccd = '$idstk'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function getStkAddress($idstk) {
		$qry = "select loccd, fullnm,  addr1+' '+addr2+' '+addr3
					as address
		        from mssc where loccd = '$idstk'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function getDataPaymentSGOByOrderIDDev($orderid) {
		$tglskrg = date("y-m-d");
		$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype = 'cbp'
				   ) 
				   AS total_pay,
				   a.total_bv, 
				   a.payShip - a.discount_shipping as payShip,
				   b.charge_connectivity, 
				   b.charge_admin, 
				   a.idstk, 
				   a.bank_code_payment, 
				   b.bankCode, 
				   b.bankDesc, 
				   b.bankDisplayNm, 
				   a.discount_shipping,
				   a.cashback_point
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, a.total_bv, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping, a.cashback_point
				  ";
		//echo $qry;
		return $this->getRecordset($qry, null, $this->db1);
	}

	function getDataPaymentSGOByOrderID_apps($orderid) {
		$tglskrg = date("y-m-d");
		$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN (a.total_pay + a.payShip - a.discount_shipping ) - 0 ELSE (a.total_pay + a.payShip - a.discount_shipping ) - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype != 'sgo'
				   ) 
				   AS total_pay,
				   a.total_bv, 
				   a.payShip,
				   b.charge_connectivity, 
				   b.charge_admin, 
				   a.idstk, 
				   a.bank_code_payment, 
				   b.bankCode, 
				   b.bankDesc, 
				   b.bankDisplayNm, 
				   a.discount_shipping,
				   a.cashback_point,
				   (SELECT              
					     CASE 
					       WHEN sum(c.payamt) IS NULL THEN 0 ELSE sum(c.payamt) 
					     END          	
					   FROM ecomm_trans_paydet_sgo c          
					   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and (c.paytype != 'sgo' and c.paytype != 'cbp' and c.paytype != 'CBP')
					   ) 
					   AS total_voucher
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, a.total_bv, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping, a.cashback_point
				  ";
		//echo $qry;
		return $this->getRecordset($qry, null, $this->db1);
	}

	function getDataPaymentSGOByOrderIDBeforePayment($orderid) {
		$tglskrg = date("y-m-d");
		$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype != 'sgo'
				   ) 
				   AS total_pay,
				   a.total_bv, 
				   a.payShip,
				   b.charge_connectivity, 
				   b.charge_admin, 
				   a.idstk, 
				   a.bank_code_payment, 
				   b.bankCode, 
				   b.bankDesc, 
				   b.bankDisplayNm, 
				   a.discount_shipping,
				   a.cashback_point,
				   (SELECT              
					     CASE 
					       WHEN sum(c.payamt) IS NULL THEN 0 ELSE sum(c.payamt) 
					     END          	
					   FROM ecomm_trans_paydet_sgo c          
					   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and (c.paytype != 'sgo' and c.paytype != 'cbp' and c.paytype != 'CBP')
					   ) 
					   AS total_voucher
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, a.total_bv, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping, a.cashback_point
				  ";
		//echo $qry;
		return $this->getRecordset($qry, null, $this->db1);
	}

	function setMainInfo($data, $tempID) {

		$arr = array();
		if($data['trx_type'] == "RM") {
			if($data['pay_tipe'] == 1) {
				$voucherno = $data['voucherno'];
				$voucherkey = $data['voucherkey'];
				$idstockist =  $data['id_stockist'];
			} else {
				$voucherno = "";
				$voucherkey = "";
				$idstockist = "";
			}
			$arr = array (
				"trx_type" => $data['trx_type'],
				"pay_tipe" => $data['pay_tipe'],
				"voucherno" => $voucherno,
				"voucherkey" => $voucherkey,
				"recruiterid" => $data['recruiterid'],
				"recruitername" => $data['recruitername'],
				"sponsorid" => $data['sponsorid'],
				"sponsorname" => $data['sponsorname'],
				"id_lp" => $data['id_lp'],
				"membername" => strtoupper($data['membername']),
				"idno" => $data['idno'],
				"sex" => $data['sex'],
				"birthdt" => $data['birthdt'],
				"addr" => strtoupper($data['addr']),
				"city" => strtoupper($data['city']),
				"tel_hp" => $data['tel_hp'],
				"pilBank" => $data['pilBank'],
				"no_rek" => $data['no_rek'],
				"nama_rekening" => strtoupper($data['nama_rekening']),
				"memb_email" => $data['memb_email'],
				"pricecode" => $data['pricecode'],
				"idstockist" => $idstockist,
				"temp_id" => $tempID,
				"userlogin" => $data['userlogin'],
				"userlogin_telhp" => $data['userlogin_telhp'],
				"userlogin_name" => $data['userlogin_name'],
				"is_login" => "1",
				//"is_login" => $data['is_login'],
				"totPayDP" => $data['totPayDP'],
				"totPayCp" => $data['totPayCp'],
				"profit_member" => $data['profit_member']
			);
		} else if($data['trx_type'] == "EC") {
			$arr = array(
				"trx_type" => $data['trx_type'],
				"idmember" => $data['idmember'],
				"membername" => $data['membername'],
				"bnsperiod" => $data['bnsperiod'],
				"pricecode" => $data['pricecode'],
				"temp_id" => $tempID,
				"id_lp" => $data['id_lp'],
				"userlogin" => $data['userlogin'],
				"userlogin_telhp" => $data['userlogin_telhp'],
				"userlogin_name" => $data['userlogin_name'],
				"is_login" => "1",
				//"is_login" => $data['is_login'],
				"totPayDP" => $data['totPayDP'],
				"totPayCp" => $data['totPayCp'],
				"profit_member" => $data['profit_member']
			);
		} else if($data['trx_type'] == "EL") {
			$arr = array(
				"trx_type" => $data['trx_type'],
				"idmember" => $data['idmember'],
				"membername" => $data['membername'],
				"bnsperiod" => $data['bnsperiod'],
				"pricecode" => $data['pricecode'],
				"temp_id" => $tempID,
				"id_lp" => $data['id_lp'],
				"userlogin" => $data['userlogin'],
				"userlogin_telhp" => $data['userlogin_telhp'],
				"userlogin_name" => $data['userlogin_name'],
				"is_login" => "2",
				//"is_login" => $data['is_login'],
				"totPayDP" => $data['totPayDP'],
				"totPayCp" => $data['totPayCp'],
				"profit_member" => $data['profit_member']
			);
		}
		return $arr;
	}

	function setProductInfo($data) {
		$prd = json_decode($data['products']);
		//echo "tes : ";
		//print_r($prd);
		$prdinfo = array();
		$totProd = 0;
		foreach($prd as $datax) {
			//echo $datax->prdcd;
			$prdx = $this->getDetailPrdcdByID($datax->prdcd);
			if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4") {
				$totProd += $datax->qty * $prdx[0]->price_w;
			} else {
				$totProd += $datax->qty * $prdx[0]->price_e;
			}
			$arr = array(
				"prdcd" =>  $prdx[0]->prdcd,
				"prdnm" => $prdx[0]->prdnm,
				"price_w" => $prdx[0]->price_w,
				"price_e" => $prdx[0]->price_e,
				"price_ce" => $prdx[0]->price_cw,
				"price_cw" => $prdx[0]->price_ce,
				"bv" => $prdx[0]->bv,
				"qty" => $datax->qty,
				"weight" => $prdx[0]->weight
			);
			array_push($prdinfo, $arr);
		}

		if($data['delivery'] == "1") {
			$kirim = 0;
		} else {
			$kirim = $data['price'];
		}

		$minUseCashback = 0.5 * ($totProd + $kirim + $data['admin_cost']);
		if($data['cashback_point'] > 0) {
			if($data['cashback_point'] <= $minUseCashback) {
				return $prdinfo;
			} else {
				return null;
			}

		} else {
			//return array("response" => "true", "message" => "Transaksi tanpacashback point valid..");
			return $prdinfo;
		}


	}

	function setProductInfo2KmartOnline($data) {
		$prd = json_decode($data['products']);
		//print_r($prd);
		$prdinfo = array();
		$totProd = 0;
		foreach($prd as $datax) {
			//echo $datax->prdcd;
			/*
			$prdx = $this->getDetailPrdcdByID($datax->prdcd);
			if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4") {
				$totProd += $datax->qty * $prdx[0]->price_w;
			} else {
				$totProd += $datax->qty * $prdx[0]->price_e;
			}
			*/
			$arr = array(
				"prdcd" =>  $datax->prdcd,
				"prdnm" => $datax->prdnm,
				"price_w" => $datax->price_w,
				"price_e" => $datax->price_e,
				"price_ce" => $datax->price_cw,
				"price_cw" => $datax->price_ce,
				"bv" => $datax->bv,
				"qty" => $datax->qty,
				"weight" => $datax->weight
			);
			array_push($prdinfo, $arr);
		}

		return $prdinfo;
	}

	function setProductInfo2($data) {
		$prd = json_decode($data['products']);
		//print_r($prd);
		$prdinfo = array();
		$totProd = 0;
		foreach($prd as $datax) {
			//echo $datax->prdcd;
			$prdx = $this->getDetailPrdcdByID($datax->prdcd);
			if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4") {
				$totProd += $datax->qty * $prdx[0]->price_w;
			} else {
				$totProd += $datax->qty * $prdx[0]->price_e;
			}
			$arr = array(
				"prdcd" =>  $prdx[0]->prdcd,
				"prdnm" => $prdx[0]->prdnm,
				"price_w" => $prdx[0]->price_w,
				"price_e" => $prdx[0]->price_e,
				"price_ce" => $prdx[0]->price_cw,
				"price_cw" => $prdx[0]->price_ce,
				"bv" => $prdx[0]->bv,
				"qty" => $datax->qty,
				"weight" => $prdx[0]->weight
			);
			array_push($prdinfo, $arr);
		}

		/*if($data['delivery'] == "1") {
			$kirim = 0;
		} else {
			$kirim = $data['price'];
		}
		 
        $minUseCashback = 0.5 * ($totProd + $kirim + $data['admin_cost']);
        if($data['cashback_point'] > 0) {
        	if($data['cashback_point'] <= $minUseCashback) {
        	   return $prdinfo;	
        	} else {
        		return null;
        	}
        	
        } else {
        	//return array("response" => "true", "message" => "Transaksi tanpacashback point valid..");	
        	return $prdinfo;	
        }*/

		return $prdinfo;
	}

	function setProductInfoNoBVKmartOnline($data) {
		$prd = json_decode($data['products']);
		//print_r($prd);
		$prdinfo = array();
		$totProd = 0;
		foreach($prd as $datax) {
			//echo $datax->prdcd;
			/*
			$prdx = $this->getDetailPrdcdByID($datax->prdcd);
			if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4") {
				$totProd += $datax->qty * $prdx[0]->price_w;
			} else {
				$totProd += $datax->qty * $prdx[0]->price_e;
			}
			*/
			$arr = array(
				"prdcd" =>  $datax->prdcd,
				"prdnm" => $datax->prdnm,
				"price_w" => $datax->price_w,
				"price_e" => $datax->price_e,
				"price_ce" => $datax->price_cw,
				"price_cw" => $datax->price_ce,
				"bv" => 0,
				"qty" => $datax->qty,
				"weight" => $datax->weight
			);
			array_push($prdinfo, $arr);
		}

		return $prdinfo;
	}

	function setProductInfoNoBV($data) {
		$prd = json_decode($data['products']);
		//$vch = json_decode($data['voucher']);
		//print_r($prd);
		$prdinfo = array();
		$totProd = 0;
		foreach($prd as $datax) {
			//echo $datax->prdcd;
			$prdx = $this->getDetailPrdcdByID($datax->prdcd);
			if($data['pricecode'] == "12W3" || $data['pricecode'] == "12W4") {
				$totProd += $datax->qty * $prdx[0]->price_w;
			} else {
				$totProd += $datax->qty * $prdx[0]->price_e;
			}
			$arr = array(
				"prdcd" =>  $prdx[0]->prdcd,
				"prdnm" => $prdx[0]->prdnm,
				"price_w" => $prdx[0]->price_w,
				"price_e" => $prdx[0]->price_e,
				"price_ce" => $prdx[0]->price_cw,
				"price_cw" => $prdx[0]->price_ce,
				"bv" => 0,
				"qty" => $datax->qty,
				"weight" => $prdx[0]->weight
			);
			array_push($prdinfo, $arr);
		}

		if($data['delivery'] == "1") {
			$kirim = 0;
		} else {
			$kirim = $data['price'];
		}

		/* 
        $minUseCashback = 0.5 * ($totProd + $kirim + $data['admin_cost']);
        if($data['cashback_point'] > 0) {
        	if($data['cashback_point'] <= $minUseCashback) {
        	   return $prdinfo;	
        	} else {
        		return null;
        	}
        	
        } else {
        	//return array("response" => "true", "message" => "Transaksi tanpacashback point valid..");	
        	return $prdinfo;	
        } */
		return $prdinfo;

	}

	function setShippingInfo($data) {

		$arr = array(
			"id_shipper" => $data['id_shipper'],
			"delivery" => $data['delivery'],
			"prov_code" => $data['prov_code'],
			"prov_name" => $data['prov_name'],
			"city_code" => $data['city_code'],
			"city_name" => $data['city_name'],
			"region_code" => $data['region_code'],
			"region_name" => $data['region_name'],
			"id_stockist" => $data['id_stockist'],
			"stockist_name" => $data['stockist_name'],
			"from" => $data['from'],
			"to" => $data['to'],
			"branch" => $data['branch'],
			"service_code" => $data['service_code'],
			"service_display" => $data['service_display'],
			"origin_name" => $data['origin_name'],
			"destination_name" => $data['destination_name'],
			"price" => $data['price'],
			"receiver_name" => $data['receiver_name'],
			"receiver_email" => $data['receiver_email'],
			"receiver_telp" => $data['receiver_telp'],
			"receiver_addr" => $data['receiver_addr'],
		);

		if(array_key_exists("lat_dest", $data)) {
			$arr['lat_dest'] = $data['lat_dest'];
		}

		if(array_key_exists("long_dest", $data)) {
			$arr['long_dest'] = $data['long_dest'];
		}
		return $arr;
	}

	function setPaymentInfo($data) {
		$arr = array(
			"bank_code_payment" =>  $data['bank_code_payment'],
			"admin_cost" => $data['admin_cost'],
			"connectivity_cost" => $data['connectivity_cost'],
			"cashback_point" => $data['cashback_point']
		);
		return $arr;
	}

	function setPaymentInfoMember($data) {
		$arr = array(
			"bank_code_payment" =>  $data['bank_code_payment'],
			"admin_cost" => $data['admin_cost'],
			"connectivity_cost" => $data['connectivity_cost'],
		);
		return $arr;
	}

	function setPaymentInfo2($data, $vchlist) {
		$arr = array(
			"bank_code_payment" =>  $data['bank_code_payment'],
			"admin_cost" => $data['admin_cost'],
			"connectivity_cost" => $data['connectivity_cost'],
			//"cashback_point" => $data['cashback_point'],
			"voucher" => $vchlist
		);
		return $arr;
	}

	function showCurrentPeriod() {
		//$this->db = $this->load->database('alternate', true);

		/*$qry = "SELECT a.currentperiodkcash as lastperiod,
                DATEADD(month, 1, a.currentperiodkcash) as nextperiod
                from klink_mlm2010.[dbo].syspref a";*/

		$qry = "SELECT
                   CONVERT(VARCHAR(10),a.currentperiodkcash, 103) as lastperiod,
				   a.periodKCashRange as rangeperiod
				from syspref a";

		return $this->getRecordset($qry, null, $this->db2);
	}

	function insertTempTable($main_info, $product_info, $shipping_info = null, $payment_info = null) {
		$gross_amount = 0;
		$gross_bv = 0;
		$gross_weight = 0;
		$gross_item = 0;
		$datetrans = date("Y-m-d H:i:s");
		$db_qryx = $this->load->database('db_ecommerce', true);

		$db_qryx->trans_begin();

		foreach($product_info as $row) {
			if($main_info['pricecode'] == "12W3" || $main_info['pricecode'] == "12W4") {
				$price = $row['price_w'];
			} else if($main_info['pricecode'] == "12E3" || $main_info['pricecode'] == "12E4") {
				$price = $row['price_e'];
			}
			$inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$main_info['temp_id']."','".$row['prdcd']."','".$row['prdnm']."',".$row['qty'].",".$row['bv'].",".$price.",'".$main_info['pricecode']."','')";

			//echo "produk ".$inputProd."</br >";
			$gross_amount += $row['qty'] * $price;
			$gross_bv += $row['qty'] * $row['bv'];
			$gross_weight +=  $row['qty'] * $row['weight'];
			$gross_item += $row['qty'];
			//echo "gross item : $gross_item, gross amount : $gross_amount, gross bv : $gross_bv, gross weight : $gross_weight";]
			// echo $inputProd."<br /><br />";
			//$exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));
			$exeInputProdr = $db_qryx->query($inputProd);
		}

		/*-------------------------------------
		 * SET NILAI JIKA BARANG TIDAK DIKIRIM
		 * -----------------------------------*/
		$shipCost = 0;
		$adminCost= 0;
		$total_pay_net = $gross_amount;
		$freeship = 0;
		$tot_pay_after_disc_shipp = $total_pay_net;
		$discount = 0;


		/*------------------------------------
		 * JIKA BARANG DIKIRIM
		 * ----------------------------------*/
		if($shipping_info['delivery'] == "2") {

			if($payment_info['admin_cost'] == "" || $payment_info['admin_cost'] == null || $payment_info['admin_cost'] == "0") {
				$adminCost = 0;
			} else {
				$adminCost = $payment_info['admin_cost'];
			}

			if($shipping_info['price'] == "" || $shipping_info['price'] == null) {
				$shipCost = 0;
			} else {
				$shipCost = $shipping_info['price'];
			}
			$total_pay_net = $gross_amount + $shipCost;

			$freeship = "0";

			/*----------------------
             * CHECK PROMO SHIPPING
             * --------------------*/
			$promo_ship = $this->checkListPromo();
			$ass = array();
			if($promo_ship != null) {
				foreach($promo_ship as $dta) {
					$key = $dta->id;
					$ass[$key] = $dta->max_discount;
					$ass['full_discount'] = $dta->full_discount;
				}

				if($ass['full_discount'] == "1") {
					$discount = $shipCost;
				} else if($ass['full_discount'] != "1") {
				  if(array_key_exists("FREESHIP", $ass)) {
						$discount = $ass['FREESHIP'];
					} else {
						$discount = 0;
					}
				}
			} else {
				$ass = null;
				$discount = 0;
			}



			$tot_pay_after_disc_shipp = $total_pay_net - $discount;
			/*--------------
             * END
             * ------------*/
			//if($shipping_info['delivery'] == "2") {

			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code,
					                     kab_code, kec_code,
					                     addr1, email, tel_hp1, 
					                     total_item, total_weight, service_type_id, 
					                     service_type_name, receiver_name, stockist_name, 
					                     kabupaten_name, province_name, sender_address, 
					                     dest_address, jne_branch, shipper_telhp, 
					                     total_pay_net, cargo_id)
					                VALUES ('".$main_info['temp_id']."', '".$shipping_info['id_stockist']."', '".$shipping_info['prov_code']."', 
					                        '".$shipping_info['city_code']."', '".$shipping_info['region_code']."', 
					                        '".$shipping_info['receiver_addr']."', '".$shipping_info['receiver_email']."', '".$shipping_info['receiver_telp']."', 
					                        $gross_item, $gross_weight, '".$shipping_info['service_code']."', 
					                        '".$shipping_info['service_display']."', '".$shipping_info['receiver_name']."','".$shipping_info['stockist_name']."',
					                        '".trim(strtoupper($shipping_info['city_name']))."',
					                        '".trim(strtoupper($shipping_info['prov_name']))."', '".$shipping_info['from']."',
					                         '".$shipping_info['to']."','".$shipping_info['branch']."', '".$main_info['userlogin_telhp']."', 
					                         ".$total_pay_net.", '".$shipping_info['id_shipper']."')";
			//echo $insShipAddr."<br /><br />";;
			//$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));
			$exeShipAddr = $db_qryx->query($insShipAddr);
			//}

		}

		/* ------------------------------------
         * END JIKA BARANG DIKIRIM
         * ----------------------------------*/

		/*-------------------------------------------------------------------------
		 * CHECK APAKAH PEMBAYARAN MELIBATKAN CASHBACK POINT 
		 * -----------------------------------------------------------------------*/
		$cashback_point = $payment_info['cashback_point'];
		$val_cashback = 0;
		if($cashback_point > 0) {
			if($tot_pay_after_disc_shipp <= $cashback_point) {
				$tot_pay_after_cashback = $tot_pay_after_disc_shipp;
				$val_cashback = $tot_pay_after_cashback;
				$insCashBack = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
		                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
						values('".$main_info['temp_id']."','1','cbp','',".$tot_pay_after_cashback.",'pending','pending','22', 0)";
				//echo "Paydet ".$insPaydet."<br>";
				//$exeInsCashBack = $this->executeQuery($insCashBack, $this->setDB(1));
				$exeInsCashBack = $db_qryx->query($insCashBack);
			} else {

				$tot_pay_after_cashback = $tot_pay_after_disc_shipp - $cashback_point;
				$insCashBack = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
		                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
						values('".$main_info['temp_id']."','1','cbp','',".$cashback_point.",'pending','pending','22', 0)";
				//echo "Paydet ".$insPaydet."<br>";
				$val_cashback = $cashback_point;
				//$exeInsCashBack = $this->executeQuery($insCashBack, $this->setDB(1));
				$exeInsCashBack = $db_qryx->query($insCashBack);
				$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
		                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
						values('".$main_info['temp_id']."','2','sgo','',".$tot_pay_after_cashback.",'pending','pending','".$payment_info['bank_code_payment']."', ".$adminCost.")";
				//echo "Paydet ".$insPaydet."<br>";
				//$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
				$exeInsPaydet = $db_qryx->query($insPaydet);
			}

		} else {
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
			                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
							values('".$main_info['temp_id']."','1','sgo','',".$tot_pay_after_disc_shipp.",'pending','pending','".$payment_info['bank_code_payment']."', ".$adminCost.")";
			//echo "Paydet ".$insPaydet."<br>";
			//$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
			$exeInsPaydet = $db_qryx->query($insPaydet);
		}

		if($main_info['trx_type'] == "RM" || $main_info['trx_type'] == "LP") {
			$id_member = $main_info['userlogin'];
			$nm_member = $main_info['userlogin_name'];
			$bns = date("m")."/".date("Y");
			//$is_login = "1";
			$is_login = $main_info['is_login'];
			$tot_paynet = 0;

			$birth = explode("/", $main_info['birthdt']);
			$thn = substr($birth[2], 2, 2);
			$password = $birth[0]."".$birth[1]."".$thn;

			$bankaccnm1 = strtoupper($main_info['nama_rekening']);
			//$bankcode = strtoupper($member_info['pilBank']);
			$kdpos1 =  "000004";
			$ip = $_SERVER['REMOTE_ADDR'];

			$st = $this->getStateByStk($shipping_info['id_stockist']);
			$st_id = $st[0]->state;
			$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok_sgo (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, id_landingpage)
                    values ('".$main_info['sponsorid']."','".$main_info['temp_id']."','".$main_info['membername']."','".$main_info['idno']."',
                            '".$main_info['addr']."', '','','".$main_info['tel_hp']."','".$main_info['memb_email']."','".$shipping_info['id_stockist']."','".$main_info['sex']."','".$main_info['birthdt']."',
                            '".$main_info['no_rek']."','".$bankaccnm1."','".$main_info['pilBank']."',
                            '".$this->dateTime."','".$main_info['temp_id']."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".$main_info['userlogin']."','".$main_info['recruiterid']."',
                            '0','','', '".$main_info['id_lp']."')";
			//echo "<br />".$insMemb;
			//$res = $this->executeQuery($insMemb, $this->setDB(1));
			//$exeInsMemb =  $this->executeQuery($insMemb, $this->setDB(1));
			$exeInsMemb = $db_qryx->query($insMemb);
		} else if($main_info['trx_type'] == "EC" || $main_info['trx_type'] == "EL") {
			$id_member = $main_info['idmember'];
			$nm_member = $main_info['membername'];

			$bnsexp = explode("/", $main_info['bnsperiod']);
			//$thn = substr($thn[2], 2, 2);
			//$bns = $bnsexp[1]."/".$bnsexp[2];
			if($bnsexp>0){
				$bns = $bnsexp[1]."/".$bnsexp[2];	
			}else{
				$bnsexp = explode("-", $main_info['bnsperiod']);
				$bns = $bnsexp[2]."/".$bnsexp[1];	
			}
			//$is_login = "1";
			$is_login = $main_info['is_login'];
			$tot_paynet = 0;
		}

		$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo 
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, 
                             bank_code_payment, payConnectivity, userlogin,
                             is_login, totPayCP,id_lp, free_shipping, discount_shipping, cashback_point) 
			          values('".$main_info['temp_id']."','','".$main_info['temp_id']."','".trim($id_member)."','".trim($nm_member)."',
			                    $gross_amount,$gross_bv,'".$main_info['pricecode']."','".$bns."','".$datetrans."', '".trim($shipping_info['id_stockist'])."','".trim($shipping_info['stockist_name'])."','0','W','".$shipping_info['delivery']."',
			                    '0','',$shipCost,".$adminCost.",
			                    ".$payment_info['bank_code_payment'].", ".$payment_info['connectivity_cost'].", '".$main_info['userlogin']."',
			                    '$is_login',$tot_paynet,'".$main_info['id_lp']."', '$freeship', $discount, $val_cashback)";
		//echo "<br /><br />".$insHeader."<br>";
		//$exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));
		$exeInsHeader = $db_qryx->query($insHeader);

		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			return false;
		} else {
			$db_qryx->trans_commit();
			return true;
		}

	}

	function insertTempTable2($main_info, $product_info, $shipping_info = null, $payment_info = null) {
		$gross_amount = 0;
		$gross_bv = 0;
		$gross_weight = 0;
		$gross_item = 0;
		$datetrans = date("Y-m-d H:i:s");
		$db_qryx = $this->load->database('db_ecommerce', true);

		$db_qryx->trans_begin();



		foreach($product_info as $row) {

			if($main_info['pricecode'] == "12W3" || $main_info['pricecode'] == "12W4") {
				$price = $row['price_w'];
			} else if($main_info['pricecode'] == "12E3" || $main_info['pricecode'] == "12E4") {
				$price = $row['price_e'];
			}

			/*$totPayDP = 0;
			$totPayCp = 0;
			$profit_member = 0;*/

			$totPayDP = $main_info['totPayDP'];
			$totPayCp = $main_info['totPayCp'];
			$profit_member = $main_info['profit_member'];

			$inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo,cpr,profit_d)
              		        values('".$main_info['temp_id']."','".$row['prdcd']."','".$row['prdnm']."',".$row['qty'].",".$row['bv'].",".$price.",'".$main_info['pricecode']."','',$totPayCp,$profit_member)";

			//echo "produk ".$inputProd."</br >";
			$gross_amount += $row['qty'] * $price;
			$gross_bv += $row['qty'] * $row['bv'];
			$gross_weight +=  $row['qty'] * $row['weight'];
			$gross_item += $row['qty'];
			//echo "gross item : $gross_item, gross amount : $gross_amount, gross bv : $gross_bv, gross weight : $gross_weight";]
			// echo $inputProd."<br /><br />";
			//$exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));
			//echo $inputProd;
			$exeInputProdr = $db_qryx->query($inputProd);
		}

		/*-------------------------------------
		 * SET NILAI JIKA BARANG TIDAK DIKIRIM
		 * -----------------------------------*/
		$shipCost = 0;
		$total_pay_net = $gross_amount;
		$freeship = 0;
		$tot_pay_after_disc_shipp = $total_pay_net;
		$discount = 0;

		/*------------------------------------
		 * JIKA BARANG DIKIRIM
		 * ----------------------------------*/
		if($shipping_info['delivery'] == "2") {

			$lat_dest = "";
			$long_dest = "";
			if(array_key_exists("lat_dest", $shipping_info)) {
				$lat_dest = $shipping_info['lat_dest'];
			}

			if(array_key_exists("long_dest", $shipping_info)) {
				$long_dest = $shipping_info['long_dest'];
			}

			if($shipping_info['price'] == "" || $shipping_info['price'] == null) {
				$shipCost = 0;
			} else {
				$shipCost = $shipping_info['price'];
			}
			$total_pay_net = $gross_amount + $shipCost;

			$freeship = "0";

			/*----------------------
             * CHECK PROMO SHIPPING (HANYA UNTUK FREESHIP)
             * --------------------*/
			$promo_ship = $this->checkListPromo();
			$ass = array();
			if($promo_ship != null) {
				foreach($promo_ship as $dta) {
					$key = $dta->id;
					$ass[$key] = $dta->max_discount;
					$ass['full_discount'] = $dta->full_discount;
				}

				if($ass['full_discount'] == "1") {
					$discount = $shipCost;
				} else if($ass['full_discount'] != "1") {
				    if(array_key_exists("FREESHIP", $ass)) {
						$discount = $ass['FREESHIP'];
					} else {
						$discount = 0;
					}
				}
			} else {
				$ass = null;
				$discount = 0;
			}



			$tot_pay_after_disc_shipp = $total_pay_net - $discount;
			/*--------------
             * END
             * ------------*/
			//if($shipping_info['delivery'] == "2") {

			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code,
					                     kab_code, kec_code,
					                     addr1, email, tel_hp1, 
					                     total_item, total_weight, service_type_id, 
					                     service_type_name, receiver_name, stockist_name, 
					                     kabupaten_name, province_name, sender_address, 
					                     dest_address, jne_branch, shipper_telhp, 
					                     total_pay_net, cargo_id, lat_dest, long_dest)
					                VALUES ('".$main_info['temp_id']."', '".$shipping_info['id_stockist']."', '".$shipping_info['prov_code']."', 
					                        '".$shipping_info['city_code']."', '".$shipping_info['region_code']."', 
					                        '".$shipping_info['receiver_addr']."', '".$shipping_info['receiver_email']."', '".$shipping_info['receiver_telp']."', 
					                        $gross_item, $gross_weight, '".$shipping_info['service_code']."', 
					                        '".$shipping_info['service_display']."', '".$shipping_info['receiver_name']."','".$shipping_info['stockist_name']."',
					                        '".trim(strtoupper($shipping_info['city_name']))."',
					                        '".trim(strtoupper($shipping_info['prov_name']))."', '".$shipping_info['from']."',
					                         '".$shipping_info['to']."','".$shipping_info['branch']."', '".$main_info['userlogin_telhp']."', 
					                         ".$total_pay_net.", '".$shipping_info['id_shipper']."', '$lat_dest', '$long_dest')";
			//echo $insShipAddr."<br /><br />";;
			//$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));
			//echo $insShipAddr;
			$exeShipAddr = $db_qryx->query($insShipAddr);
			//}

		}

		/* ------------------------------------
         * END JIKA BARANG DIKIRIM
         * ----------------------------------*/

		/*-------------------------------------------------------------------------
		 * CHECK APAKAH PEMBAYARAN MELIBATKAN CASHBACK POINT / VOUCHER PRODUK/CASH
		 * -----------------------------------------------------------------------*/
		/*$cashback_point = $payment_info['cashback_point'];
        $val_cashback = 0;
        if($cashback_point > 0) {
            if($tot_pay_after_disc_shipp <= $cashback_point) {
               $tot_pay_after_cashback = $tot_pay_after_disc_shipp;
               $val_cashback = $tot_pay_after_cashback;
               $insCashBack = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
                             docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
                        values('".$main_info['temp_id']."','1','cbp','',".$tot_pay_after_cashback.",'pending','pending','22', 0)";
                //echo "Paydet ".$insPaydet."<br>";
               //$exeInsCashBack = $this->executeQuery($insCashBack, $this->setDB(1));
               //$exeInsCashBack = $db_qryx->query($insCashBack);
            } else {

               $tot_pay_after_cashback = $tot_pay_after_disc_shipp - $cashback_point;
               $insCashBack = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
                             docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
                        values('".$main_info['temp_id']."','1','cbp','',".$cashback_point.",'pending','pending','22', 0)";
                //echo "Paydet ".$insPaydet."<br>";
                $val_cashback = $cashback_point;
               //$exeInsCashBack = $this->executeQuery($insCashBack, $this->setDB(1));
               $exeInsCashBack = $db_qryx->query($insCashBack);
               $insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
                             docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
                        values('".$main_info['temp_id']."','2','sgo','',".$tot_pay_after_cashback.",'pending','pending','".$payment_info['bank_code_payment']."', ".$payment_info['admin_cost'].")";
                //echo "Paydet ".$insPaydet."<br>";
               //$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
               //$exeInsPaydet = $db_qryx->query($insPaydet);
            }

        } else {
            $insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
                                 docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
                            values('".$main_info['temp_id']."','1','sgo','',".$tot_pay_after_disc_shipp.",'pending','pending','".$payment_info['bank_code_payment']."', ".$payment_info['admin_cost'].")";
            //echo "Paydet ".$insPaydet."<br>";
            //$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
            //$exeInsPaydet = $db_qryx->query($insPaydet);
        }*/


		/*--------------------------------------------------
		 * CHECK APAKAH PEMBAYARAN MELIBATKAN VOUCHER CASH/PRODUK
		 * ------------------------------------------------*/
		$pay_vch = $payment_info['voucher'];
		//print_r($pay_vch);
		$total_nilai_diskon = 0;
		$val_cashback = 0;
		$sgo_flag_payment = 0;
		if(isset($pay_vch)) {
			foreach($pay_vch as $vch1) {
				if($vch1['tipe'] == "CBP") {
					$kd = "22";
					$val_cashback = $vch1['amount'];
				} else if($vch1['tipe'] == "C") {
					$kd = "23";
				} else if($vch1['tipe'] == "P") {
					$kd = "24";
				} else {
					$kd = "19";
				}
				$insVchCbp = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
			                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
							values('".$main_info['temp_id']."','1','".$vch1['tipe']."','".$vch1['voucherno']."',".$vch1['amount'].",'pending','pending','".$kd."', 0)";
				//echo $insVchCbp."<br>";;
				$exeInsPaydet = $db_qryx->query($insVchCbp);
				$total_nilai_diskon += $vch1['amount'];
			}

			//tambahan jika total pembayaran setelah dikurangi cashback/voucher tapi masih sisa
			if($tot_pay_after_disc_shipp > $total_nilai_diskon) {
				$sisa = $tot_pay_after_disc_shipp - $total_nilai_diskon;
				$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
				                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
								values('".$main_info['temp_id']."','1','sgo','',".$sisa.",'pending','pending','".$payment_info['bank_code_payment']."', ".$payment_info['admin_cost'].")";
				//echo $insPaydet."<br>";
				$sgo_flag_payment++;
				$exeInsPaydet = $db_qryx->query($insPaydet);
			}
		} else {
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
				                     docno,payamt,notes,paystatus, bank_code_payment, charge_admin) 
								values('".$main_info['temp_id']."','1','sgo','',".$tot_pay_after_disc_shipp.",'pending','pending','".$payment_info['bank_code_payment']."', ".$payment_info['admin_cost'].")";
			$sgo_flag_payment++;
			//echo $insPaydet."<br>";
			$exeInsPaydet = $db_qryx->query($insPaydet);
		}

		if($main_info['trx_type'] == "RM" || $main_info['trx_type'] == "LP") {
			$id_member = $main_info['userlogin'];
			$nm_member = $main_info['userlogin_name'];
			$bns = date("m")."/".date("Y");
			//$is_login = "1";
			$is_login = $main_info['is_login'];
			$tot_paynet = 0;

			$birth = explode("/", $main_info['birthdt']);
			$thn = substr($birth[2], 2, 2);
			$password = $birth[0]."".$birth[1]."".$thn;

			$bankaccnm1 = strtoupper($main_info['nama_rekening']);
			//$bankcode = strtoupper($member_info['pilBank']);
			$kdpos1 =  "000004";
			$ip = $_SERVER['REMOTE_ADDR'];

			$st = $this->getStateByStk($shipping_info['id_stockist']);
			$st_id = $st[0]->state;
			$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok_sgo (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, id_landingpage)
                    values ('".$main_info['sponsorid']."','".$main_info['temp_id']."','".$main_info['membername']."','".$main_info['idno']."',
                            '".$main_info['addr']."', '','','".$main_info['tel_hp']."','".$main_info['memb_email']."','".$shipping_info['id_stockist']."','".$main_info['sex']."','".$main_info['birthdt']."',
                            '".$main_info['no_rek']."','".$bankaccnm1."','".$main_info['pilBank']."',
                            '".$this->dateTime."','".$main_info['temp_id']."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".$main_info['userlogin']."','".$main_info['recruiterid']."',
                            '0','','', '".$main_info['id_lp']."')";
			//echo "<br />".$insMemb;
			//$res = $this->executeQuery($insMemb, $this->setDB(1));
			//$exeInsMemb =  $this->executeQuery($insMemb, $this->setDB(1));
			$exeInsMemb = $db_qryx->query($insMemb);
		} else if($main_info['trx_type'] == "EC" || $main_info['trx_type'] == "EL") {
			$id_member = $main_info['idmember'];
			$nm_member = $main_info['membername'];

			$bnsexp = explode("/", $main_info['bnsperiod']);
			//$thn = substr($thn[2], 2, 2);
			//$bns = $bnsexp[1]."/".$bnsexp[2];
			if($bnsexp>0){
				$bns = $bnsexp[1]."/".$bnsexp[2];	
			}else{
				$bnsexp = explode("-", $main_info['bnsperiod']);
				$bns = $bnsexp[2]."/".$bnsexp[1];	
			}

			//$is_login = "1";
			$is_login = $main_info['is_login'];

			/*$totPayDP = 0;
			$totPayCp = 0;
			$profit_member = 0;*/

			$totPayDP = $main_info['totPayDP'];
			$totPayCp = $main_info['totPayCp'];
			$profit_member = $main_info['profit_member'];


			$tot_paynet = 0;
		}
		$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo 
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, 
                             bank_code_payment, payConnectivity, userlogin,
                             is_login, totPayDP, totPayCP, profit_member,id_lp, free_shipping, discount_shipping, cashback_point)
			          values('".$main_info['temp_id']."','','".$main_info['temp_id']."','".trim($id_member)."','".trim($nm_member)."',
			                    $gross_amount,$gross_bv,'".$main_info['pricecode']."','".$bns."','".$datetrans."', '".trim($shipping_info['id_stockist'])."','".trim($shipping_info['stockist_name'])."','0','W','".$shipping_info['delivery']."',
			                    '0','',$shipCost,".$payment_info['admin_cost'].",
			                    ".$payment_info['bank_code_payment'].", ".$payment_info['connectivity_cost'].", '".$main_info['userlogin']."',
			                    '$is_login',$totPayDP,$totPayCp,$profit_member,'".$main_info['id_lp']."', '$freeship', $discount, $val_cashback)";
		//echo "<br /><br />".$insHeader."<br>";
		//$exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));
		$exeInsHeader = $db_qryx->query($insHeader);


		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			return array("response" => "false");
			//return false;
		} else {
			$db_qryx->trans_commit();
			return array("response" => "true", "sgo" => $sgo_flag_payment);
			//return true;
		}
		//return true;

	}

	function updateInfoMember($data) {
		$qry = "UPDATE msmemb SET email = '$data[email]',
					tel_hp = '$data[tel_hp]'
		        WHERE dfno = '$data[idmember]'";
		//echo $qry; , bankid = '$data[bankid]', bankaccno = '$data[bankaccno]', bankaccnm = '$data[bankaccnm]'
		$res = $this->executeQuery($qry, $this->setDB(2));
		return $res;
	}

	function updateInfoMember2($data) {
		$str = "";
		if(array_key_exists("email", $data)) {
			$str .= "email = '$data[email]', ";
		}

		if(array_key_exists("tel_hp", $data)) {
			$str .= "tel_hp = '$data[tel_hp]', ";
		}

		if(array_key_exists("bankid", $data)) {
			$str .= "bankid = '$data[bankid]', ";
		}

		if(array_key_exists("bankaccno", $data)) {
			$str .= "bankaccno = '$data[bankaccno]', ";
		}

		if(array_key_exists("bankaccnm", $data)) {
			$str .= "bankaccnm = '$data[bankaccnm]', ";
		}

		if(array_key_exists("password", $data)) {
			$str .= "password = '$data[password]', ";
		}
		$str = substr($str, 0, -2);
		$qry = "UPDATE msmemb SET $str WHERE dfno = '$data[idmember]'";
		//echo $qry;
		$res = $this->executeQuery($qry, $this->setDB(2));
		return $res;
	}

	function checkPersonalBV($idmember, $month, $year) {
		$qry = "select A.period, A.dfno, A.ppv, A.pobv, A.pgpv
				from hsttree a 
				where month(a.period)=$month and year(a.period)=$year 
					  and a.dfno='$idmember'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function groupbv($idmember,$month,$year){
		/*
		$qry = "SELECT n.dfno, j.fullnm, CONVERT(char(10), j.jointdt,103) as jointdt,
      j.level, r.shortnm as ranknm, n.ppv, n.popv, n.pgpv,n.sfno,
      year(n.period) as year,month(n.period) as month
FROM hsttree n
      INNER JOIN  msmemb j ON (n.dfno = j.dfno)
      LEFT OUTER JOIN msrank r on (j.[level] = r.[level])
                    WHERE n.sfno='".$idmember."' AND year(n.period)='".$year."'
                    AND	month(n.period)='".$month."' AND n.dfno=j.dfno
                    ORDER BY j.fullnm ASC";
          */          
                    
					
		$qry = "SELECT j.dfno, j.fullnm, j.sfno, CONVERT(char(10), j.jointdt,103) as jointdt,
					   CAST(ISNULL((SELECT TOP 1 A.adjustedrank
				       	FROM tbonus A
				        WHERE A.distributorcode=J.dfno
				        ORDER BY A.bonusyear DESC, A.bonusmonth DESC), 0) AS INT) AS [level],
					   isnull(k.ppv, 0) as ppv, 
				       isnull(k.popv, 0) as popv, 
				       isnull(k.pgpv, 0) as pgpv,
				       isnull(YEAR(k.period), ".$year.") as YEAR, 
				       isnull(MONTH(k.period), ".$month.") AS MONTH
				FROM msmemb j 
					 left outer join hsttree k ON j.dfno=k.dfno and YEAR(k.period)=".$year." and MONTH(k.period)=".$month."
				WHERE j.sfno='".$idmember."' 
				ORDER BY j.dfno ASC";
		//echo "gbv ".$qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	/*--------------
	 * ACARA
	 * -------------*/
	function getListSpeaker($namaPembicara = null) {
		$conx = $this->load->database("klaim_acara", true);
		$where = "";
		if($namaPembicara != null) {
			$where = " WHERE namaAlias LIKE '%".strtoupper($namaPembicara)."%' and a.[status] = '1'";
		}

		$qry = "SELECT A.dfno,
        			   a.kodePembicara, 
        			   (b.shortName + '. ' + a.nama) as namaPembicara,
        	           CASE
				       	 WHEN A.[status]=0 THEN 'Tidak Aktif'
				         WHEN A.[status]=1 THEN 'Aktif'
				         WHEN A.[status]=2 THEN 'Terminate'
				       END AS statusx
               FROM tblMasterPembicara a
               LEFT JOIN tblMasterPeringkat b ON (a.kodePeringkat = b.kodePeringkat)
            
               $where ORDER BY namaAlias";
		$res = $conx->query($qry);
		return $res->result();
	}

	function getListRegional($namaRegional = null) {
		$where = "";
		$conx = $this->load->database("klaim_acara", true);
		if($namaRegional != null) {
			$where = " WHERE namaWilayah LIKE '%".strtoupper($namaRegional)."%'";
		}

		$qry = "SELECT a.kodeWilayah,a.namaWilayah,a.kodeMgrArea,b.nama
                FROM tblMasterWilayah a
                    INNER JOIN  tblMasterMgrArea b on a.kodeMgrArea  = b.kodeMgrArea COLLATE SQL_Latin1_General_CP1_CS_AS
                $where
                order by kodeWilayah";

		$res = $conx->query($qry);
		return $res->result();
	}

	function getSummaryAcaraByPeriod($wilayah = null, $pembicara=null, $month, $year, $limit, $offset) {
		$qryWil = "";
		$qrySpeaker = "";
		if($wilayah != null) {
			$qryWil = " c.kodeWilayah = '$wilayah' AND ";
		}
		if($pembicara != null) {
			$qrySpeaker = " f.kodePembicaraR = '$pembicara' AND ";
		}
		if($month == 12) {
			$nextMonth = 1;
			$nextYear = $year+1;
		} else {
			$nextMonth = $month + 1;
			$nextYear = $year;
		}
		$conx = $this->load->database("klaim_acara", true);
		$first = 0;
		$last = 0;
		if($offset == 0 || $offset == 1){
			$first = 1;
			$last  = $limit;
			//echo "test ".$no_page."-".$first."-".$last."<br>";
		} else {
			$xx = $offset - 1;
			if($xx == 0){
				$xx = 1;
			}
			//echo $xx;
			$first = $xx * $limit + 1;
			$last  = $first + ($limit - 1);
		}
		$qry = "SELECT XX.RowNumber,
				       XX.kodeAcara,
				       XX.tanggalAcara,
				       XX.tanggalAcaraEnd,
				       XX.jamAcara,
				       XX.namaAcara,
				       XX.kodeWilayah,
				       XX.namaWilayah,
				       XX.lokasiAcara,
				       XX.namaDaerah,
				       XX.manager_area
				FROM (       
				   SELECT  ROW_NUMBER() OVER (ORDER BY a.trcd desc) as RowNumber,
				       a.trcd as kodeAcara, 
				       CONVERT(VARCHAR(25), a.tanggalAcara, 103) as tanggalAcara,
				       CONVERT(VARCHAR(25), a.tanggalAcaraEnd, 103) as tanggalAcaraEnd, 
				       LEFT(CONVERT(VARCHAR(9), a.tanggalAcara, 108),5) as jamAcara,
				       b.deskripsi as namaAcara,
				       c.kodeWilayah,
				       d.namaWilayah, 
				       a.lokasi as lokasiAcara, 
				       c.namaDaerah, 
				       e.nama as manager_area 
				   FROM tblJadwal_Draft_hdr a
				    LEFT JOIN tblMasterJenisAcara b ON (a.kodeAcara = b.kodeAcara)
				    LEFT JOIN tblMasterDaerah c ON (a.kodeDaerah = c.kodeDaerah)
				    LEFT JOIN tblMasterWilayah d ON (c.kodeWilayah = d.kodeWilayah)
				    LEFT JOIN tblMasterMgrArea e ON (d.kodeMgrArea = e.kodeMgrArea)
					LEFT JOIN klaim_acara.dbo.tblJadwal_Draft_det f ON (a.trcd = f.trcd)
     				LEFT JOIN klaim_acara.dbo.tblMasterPembicara g ON (f.kodePembicara = g.kodePembicara)
     				LEFT JOIN klaim_acara.dbo.tblMasterPeringkat h ON (g.kodePeringkat = h.kodePeringkat)
				   WHERE $qryWil $qrySpeaker
				     ((MONTH(a.periodeAcara) = $month AND YEAR(a.periodeAcara) = $year) OR
				     (MONTH(a.periodeAcara) = $nextMonth AND YEAR(a.periodeAcara) = $nextYear))
					 
				) XX
				WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
		//echo $qry;
		$res = $conx->query($qry);
		$arr = array();
		$arx = array();
		foreach ($res->result() as $row) {
			$arr['RowNumber'] = $row->RowNumber;
			$arr['kodeAcara'] = $row->kodeAcara;
			$arr['tanggalAcara'] = $row->tanggalAcara;
			$arr['tanggalAcaraEnd'] = $row->tanggalAcaraEnd;
			$arr['jamAcara'] = $row->jamAcara;
			$arr['namaAcara'] =  strtoupper($row->namaAcara);
			$arr['kodeWilayah'] =  $row->kodeWilayah;
			$arr['namaWilayah'] =  strtoupper($row->namaWilayah);
			$arr['lokasiAcara'] =  strtoupper($row->lokasiAcara);
			$arr['namaDaerah'] =  strtoupper($row->namaDaerah);
			$arr['manager_area'] =  strtoupper($row->manager_area);


			$qryPembicara = "SELECT c.kodePeringkat, a.nourut, a.trcd, a.kodePembicaraR,
		                       (c.shortName + '. ' + b.namaAlias) as namaAlias
						    FROM tblJadwal_Draft_det a
						    LEFT JOIN tblMasterPembicara b ON (a.kodePembicara = b.kodePembicara)
						    LEFT JOIN tblMasterPeringkat c ON (b.kodePeringkat = c.kodePeringkat)
						    WHERE a.trcd = '$row->kodeAcara'
						    UNION 
						    SELECT a.kodePeringkat, a.nourut, a.trcd, a.kodePeringkat AS kodePembicaraR, 
						           (CASE WHEN LEN(LTRIM(RTRIM(b.shortName)))>0 THEN 
						            b.shortName + '. ' + a.nmPembicara ELSE a.nmPembicara END) as namaAlias
						    FROM tblJadwal_Draft_det_tb a
						    LEFT JOIN tblMasterPeringkat b ON (a.kodePeringkat = b.kodePeringkat)
						    WHERE a.trcd = '$row->kodeAcara'";
			$resx = $conx->query($qryPembicara);
			$arrPembicara = array();
			$i=0;
			foreach ($resx->result() as $rowx) {
				$arrPembicara[$i]['kodePembicara'] =  $rowx->kodePembicaraR;
				$arrPembicara[$i]['namaPembicara'] =  strtoupper($rowx->namaAlias);
				$i++;
				//array_push($arrPembicara, $arrPemb);
			}
			$arr['daftarPembicara'] = $arrPembicara;
			array_push($arx, $arr);
		}
		return $arx;
	}

	/*-----------------------
     * BONUS REPORT
     * ---------------------*/

	function getBonusReportMember($idmember, $month_period, $year_period) {
		$dataMember = "SELECT dfno as idmember, fullnm as fullname, addr1, addr2, addr3, loccd as stokis
		  	               FROM msmemb WHERE dfno = '$idmember'";
		$res1 = $this->getRecordset($dataMember, null, $this->db2);
		$tbonus = "select sponsorcode, currentrank, adjustedrank, effectiverank, planbincome, gpincome, ldbincome, ldbtotalpoint,
				   		   a.ldbpointvalue, sredincome, crownincome, caincome, scaincome, rcaincome, chincome, infinityincome,
						   gpsharing3income, gpsharing3points, netincome, lastmonthyend,currentyend, accyend, lastmonthoversea,
						   currentoversea, accoversea, capointvalue, scapointvalue, rcapointvalue, chpointvalue, addinfinityincome,
						   pspointvalue, gpsharing1income, gpsharing1points, gpsharing2income, gpsharing2points,
						   gps3pointvalue, gps1pointvalue, gps2pointvalue, totaladjustedamt, remark,
						   CAShare, CHShare, scashare, crownshare, rcashare, psharingincome, psharingpoints
	             FROM
	                      tbonus a
						  LEFT OUTER JOIN tbonusadjust c ON (a.distributorcode = c.distributorcode)
						  AND (a.bonusyear = c.bonusyear)
						  AND (a.bonusmonth = c.bonusmonth)
						  LEFT OUTER JOIN tbonussummary b ON (a.bonusmonth = b.bonusmonth)
						  AND (a.bonusyear = b.bonusyear)
	             WHERE
						  (a.distributorcode = '$idmember') AND 
						  (a.bonusmonth = $month_period) AND 
						  (a.bonusyear = $year_period)";
		$res2 = $this->getRecordset($tbonus, null, $this->db2);
		if($res1 == null) {
			$arr = jsonFalseResponse("ID Member invalid atau salah..");
		} else {
			$arr = array(
				"response" => "true",
				"arrayData" => array (
					"dataMember" => $res1,
					"dataBonus" => $res2
				)
			);
		}
		return $arr;
	}

	function updateDeliveryStt($idmember_login, $orderno, $value) {
		$res = null;
		//if($res2 != null) {
		$qry = "UPDATE a
        			SET a.delivery_status = '$value'
        	        FROM ecomm_trans_hdr a
        	        INNER JOIN log_trans b ON (a.orderno = b.log_trcd)
	  				WHERE b.log_usrlogin = '$idmember_login' AND a.orderno = '$orderno'";
		//echo $qry;
		$resx = $this->executeQuery($qry, $this->setDB(1));
		if($resx > 0) {
			$qryCheck  = "SELECT a.orderno, b.log_dfno, a.delivery_status
	  				  FROM ecomm_trans_hdr a
	  				  INNER JOIN log_trans b ON (a.orderno = b.log_trcd)
	  				  WHERE b.log_usrlogin = '$idmember_login' AND a.orderno = '$orderno'";
			$res = $this->getRecordset($qryCheck, null, $this->db1);
		}
		/*}  else {
            $res = null;
        } */
		/*$qry = "UPDATE ecomm_trans_shipaddr
                   SET delivery_status = '$value'
                WHERE orderno = '$orderno'";
      $res = $this->executeQuery($qry, $this->setDB(1));*/
		return $res;
	}

	function updateDeliverySttByStockist($idstockist, $orderno, $value) {
		$qry = "UPDATE ecomm_trans_hdr SET delivery_status = '$value' WHERE orderno = '$orderno'";
		$resx = $this->executeQuery($qry, $this->setDB(1));
		
		$qry2 = "UPDATE ecomm_trans_shipaddr SET delivery_status = '$value' WHERE orderno = '$orderno'";
		$resx2 = $this->executeQuery($qry2, $this->setDB(1));
		
		if($resx > 0 && $resx2 > 0) {
			$qryCheck  = "SELECT a.orderno, a.delivery_status
	  				  FROM ecomm_trans_hdr a
	  				  WHERE a.orderno = '$orderno'";
			$res = $this->getRecordset($qryCheck, null, $this->db1);
		}
		return $res;
	}

	function showListKota2($prov) {
		/*$qry = "SELECT
                  a.provinsi,
                  b.kode_kabupaten,
                  b.kabupaten
                FROM
                  master_wil_provinsi a
                  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
                WHERE
                  a.kode_provinsi = '".$prov."'"; */

		/*===============================edit hilal @2015-05-19=================================*/
		/*$qry = "SELECT
                  a.provinsi,
                  b.kode_kabupaten,
                  b.kabupaten
                FROM
                  master_wil_provinsi a
                  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
                  INNER JOIN master_wil_kecamatan c ON (b.kode_kabupaten = c.kode_kabupaten)
                WHERE
                  c.kode_kab_JNE is not null AND
                  a.kode_provinsi = '".$prov."'
                GROUP BY a.provinsi,
                b.kode_kabupaten, b.kabupaten
                ORDER BY a.provinsi";
        */
		$qry = "SELECT a.provinsi, a.kode_kabupaten, a.kabupaten
				FROM V_ECOMM_WILAYAH3 A
				WHERE A.kode_kab_JNE is not null AND a.kode_provinsi = '".$prov."'
				  and a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
				GROUP BY a.provinsi, a.kode_kabupaten, a.kabupaten
				ORDER BY A.kabupaten";

		/*===============================end edit hilal @2015-05-19=================================*/
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showListKecamatan2($kota) {
		/*$qry = "SELECT
                b.kode_kecamatan,
                b.kecamatan,
                a.kabupaten
                FROM
                    master_wil_kabupaten a
                    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
                WHERE
                a.kode_kabupaten = '".$kota."'"; */

		/*===============================edit hilal @2015-05-19=================================*/
		/*$qry = "SELECT
                    b.kode_kecamatan,
                    b.kecamatan,
                    a.kabupaten,
                    b.kode_kec_JNE
                FROM
                    master_wil_kabupaten a
                    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
                WHERE
                a.kode_kabupaten = '".$kota."' AND
                b.kode_kec_JNE is not null";
         */

		$qry = "SELECT A.kode_kecamatan, A.kecamatan, A.kabupaten, A.kode_kec_JNE
				FROM V_ECOMM_WILAYAH3 A
				WHERE A.kode_kabupaten = '".$kota."' AND A.kode_kec_JNE is not null
				  and a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
				GROUP BY A.kode_kecamatan, A.kecamatan, A.kabupaten, A.kode_kec_JNE
				ORDER BY A.kecamatan";
		/*===============================end edit hilal @2015-05-19=================================*/
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showListKelurahan2($kecamatan) {
		$this->db = $this->load->database("db_ecommerce", true);
		/*$qry = "SELECT A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan
                FROM V_ECOMM_WILAYAH2 A
                WHERE A.kode_kecamatan = '".$kecamatan."'"; */

		$qry = "SELECT A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan, 
				    A.kode_kec_JNE_Origin,a.state, a.loccd, a.fullnm
				FROM V_ECOMM_WILAYAH3 A
				WHERE A.kode_kecamatan = '".$kecamatan."'
				  AND a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
				  AND (A.kode_kec_JNE_Origin is not null or A.kode_kec_JNE_Origin != '')";
		$query = $this->db->query($qry);
		$res = $query->result();
		if ($res == null)
		{
			return null;
		}
		else
		{
			/*$kec_jne = "SELECT a.KEC_JNE, a.loccd, a.fullnm, A.kode_kec_JNE_Origin,a.state
                FROM V_ECOMM_WILAYAH2 A
                WHERE A.kode_kec_JNE='".$res[0]->kode_kec_JNE."'
                      AND a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
                      AND (A.kode_kec_JNE_Origin is not null or A.kode_kec_JNE_Origin != '')"; */
			/*===============================end edit hilal @2015-05-19=================================*/

			//echo "query ".$qry;
			//$query2 = $this->db->query($kec_jne);
			//$res2 = $query2->result();

			$arr[0] = array(
				"kode_kecamatan" =>$res[0]->kode_kecamatan,
				"nama_kecamatan" =>$res[0]->kecamatan,
				//"origin" => $res2[0]->kode_kec_JNE_Origin,
				//"destination" => $res[0]->kode_kec_JNE,
				//"idstokis" => $res2[0]->loccd,
				//"stokis_info" => $res2[0]->loccd." - ".$res2[0]->fullnm,
				"idstokis" => $res[0]->loccd,
				"stokis_info" => $res[0]->loccd." - ".$res[0]->fullnm,
			);
			return $arr;


		}
	}

	function listCargoKota($prov) {
		$qry = "SELECT a.kode_provinsi as provinsi,
				      a.KotaKode_kgp as kode_kabupaten, 
				      a.KotaNama as kabupaten
				  FROM jne_Tabel_Zona_Induk a
				  INNER JOIN master_kgp_wil b ON (a.KotaKode_kgp = b.KotaKode)
				  WHERE a.kode_provinsi = '$prov' AND (a.KotaKode_kgp is not null OR a.KotaKode_kgp != '')
				    AND b.WaktuTempuh > 0
				  GROUP BY  a.kode_provinsi, a.KotaKode_kgp, a.KotaNama
				  ORDER by a.kode_provinsi, a.KotaKode_kgp, a.KotaNama";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function listProvince() {
		$qry = "SELECT A.kode_provinsi, A.PROPINSI as provinsi
				  FROM jne_Tabel_Zona_Induk A
				  GROUP BY A.kode_provinsi, A.PROPINSI
				  ORDER BY A.PROPINSI";
		//echo $qry;	
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showPricecodeStockistX($idshipper, $kec, $loccd) {
		if($idshipper == "1") {
		    //update a.pricecode2 as pricecode utk bln maret 2019
			//by dion
			$qry = "select b.City_Code_Dest as destination,
				               a.loccd as idstockist, 
				               a.fullnm,
				               a.pricecode2 as pricecode,
							   b.whcd_origin as origin, 
							   b.wh_jne_branchcd AS jne_branch
						from klink_mlm2010.dbo.mssc a
						inner join db_ecommerce.dbo.jne_Tabel_Zona_Induk b 
							  on a.loccd=b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS
						where b.kode_kecamatan = '$kec' 
								AND a.loccd = '$loccd' 
						and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')"; 
			/*$qry = "select a.City_Code_Dest AS KEC_JNE, a.loccd, a.fullnm,a.pricecode, a.[state],
							a.whcd_origin as kode_kec_JNE_Origin, a.wh_jne_branchcd AS jne_branchcd,
							a.provinsi as provinsi, a.Kota_Kabupaten as kabupaten, a.Kecamatanx as kecamatan
					from V_ECOMM_WILAYAH3 a
					where a.kode_kecamatan = '$kec' 
							AND a.loccd = '$loccd' 
							and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')"; */
		} else if($idshipper == "2") {
			$qry = "select b.KotaKode_kgp as destination,
				               a.loccd as idstockist, 
				               a.fullnm,
				               a.pricecode,
							   '' as origin, 
							   '' AS jne_branch
						from klink_mlm2010.dbo.mssc a
						inner join db_ecommerce.dbo.jne_Tabel_Zona_Induk b 
							  on a.loccd=b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS
						where b.kode_kecamatan = '$kec' 
								AND a.loccd = '$loccd' 
						and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')";
		}
		//echo "qry ".$qry;
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function NumberToMonth($bln)
	{
		switch($bln)
		{
			case 1 :
				$jum = "Jan";
				break;
			case 2 :
				$jum = "Feb";
				break;
			case 3 :
				$jum = "Mar";
				break;
			case 4 :
				$jum = "Apr";
				break;
			case 5 :
				$jum = "May";
				break;
			case 6 :
				$jum = "Jun";
				break;
			case 7 :
				$jum = "Jul";
				break;
			case 8 :
				$jum = "Aug";
				break;
			case 9 :
				$jum = "Sep";
				break;
			case 10 :
				$jum = "Oct";
				break;
			case 11 :
				$jum = "Nov";
				break;
			case 12 :
				$jum = "Dec";
				break;
		}
		return $jum;
	}
	//dari cahyono
	function getReportBonus($dfno,$token,$year,$usr,$pwd){
		//$cbd = base64_decode($token);
		//$z = explode("::", $cbd);
		//$dfno=$z[0];

		$conn  = $this->load->database("klink_mlm2010", TRUE);

		$slc = "SELECT a.distributorcode, a.bonusyear, a.bonusmonth,
						   isnull(a.netincome, 0) as netincome,
					       isnull(a.totBonus, 0) as totBonus,
					       isnull(a.tax, 0) as tax,
					       isnull(a.ar_total, 0) as ar_total,
					       isnull(a.totBonus_Nett, 0) as  totBonus_Nett
			       FROM V_HILAL_TOTBONUS_NETT A
                    WHERE A.distributorcode='$dfno' AND A.bonusyear=$year
                    ORDER BY bonusmonth";
		$resx = $conn->query($slc);
		$arrTemp = array();
		$arr = array();
		$i=0;
		$j=0;
		$count = 0;
		foreach ($resx->result() as $data) {
			if($count > 0) {
				$i++;
			}
			$bulan = $this->NumberToMonth($data->bonusmonth);
			array_push($arrTemp, $data->bonusyear);
			$deduction = $data->tax + $data->ar_total;
			$arr[$i]['statement_id'] = $data->bonusmonth;
			$arr[$i]['date'] = $bulan." ".$data->bonusyear;
			$arr[$i]['bonus'] = intval($data->totBonus);
			$arr[$i]['deduction'] = intval($deduction);
			$arr[$i]['nett'] = intval($data->totBonus_Nett);
			$arr[$i]['status'] = null;
			//$tokenEncoded = urlencode($token);
			$tokenEncoded = base64_encode($token);
			$arr[$i]['statement_url'] = "https://www.k-net.co.id/api/bonusm/".$usr."/".$pwd."/".$dfno."/".$tokenEncoded."/".$data->bonusmonth."/".$data->bonusyear;
//			$usr,$pwd
			$count++;

		}
		return $arr;

	}

	function getReportBonus2($dfno,$token,$year,$usr,$pwd){
		//$cbd = base64_decode($token);
		//$z = explode("::", $cbd);
		//$dfno=$z[0];

		$conn  = $this->load->database("klink_mlm2010", TRUE);

		$slc = "SELECT a.distributorcode, a.bonusyear, a.bonusmonth,
						   isnull(a.netincome, 0) as netincome,
					       isnull(a.totBonus, 0) as totBonus,
					       isnull(a.tax, 0) as tax,
					       isnull(a.ar_total, 0) as ar_total,
					       isnull(a.totBonus_Nett, 0) as  totBonus_Nett
			       FROM V_HILAL_TOTBONUS_NETT A
                    WHERE A.distributorcode='$dfno' AND A.bonusyear=$year
                    ORDER BY bonusmonth";
		$resx = $conn->query($slc);
		$arrTemp = array();
		$arr = array();
		$i=0;
		$j=0;
		$count = 0;
		foreach ($resx->result() as $data) {
			if($count > 0) {
				$i++;
			}
			$bulan = $this->NumberToMonth($data->bonusmonth);
			array_push($arrTemp, $data->bonusyear);
			$deduction = $data->ar_total;
			$arr[$i]['statement_id'] = $data->bonusmonth;
			$arr[$i]['date'] = $bulan." ".$data->bonusyear;
			$arr[$i]['bonus'] = number_format($data->totBonus,0,".",".");
			$arr[$i]['deduction'] = number_format($deduction,0,".",".");
			$arr[$i]['nett'] = number_format($data->totBonus_Nett,0,".",".");
			//$arr[$i]['status'] = null;
			//$tokenEncoded = urlencode($token);
			$tokenEncoded = base64_encode($token);
			//$arr[$i]['statement_url'] = "https://www.k-net.co.id/api/bonusm/".$usr."/".$pwd."/".$dfno."/".$tokenEncoded."/".$data->bonusmonth."/".$data->bonusyear;
//			$usr,$pwd
			$count++;

		}
		return $arr;

	}

	function listVoucherMember($idmember, $bns, $year) {
		$qry = "SELECT *,
				    DATENAME(MONTH, CAST(A.BonusYear AS VARCHAR(4)) + '-' + 
			       	RIGHT('0' + CAST(A.BonusMonth AS VARCHAR(4)), 2) + '-01') + '/' +
			        DATENAME(YEAR, CAST(A.BonusYear AS VARCHAR(4)) + '-' + 
			       	RIGHT('0' + CAST(A.BonusMonth AS VARCHAR(4)), 2) + '-01') AS PERIODEX 
       		  FROM klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER a
			  WHERE a.DistributorCode = '$idmember'
				   AND a.BonusMonth = '$bns' and a.BonusYear = '$year'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function listVoucherMemberDion($idmember, $bns, $year) {
		$qry = "SELECT *,
				    DATENAME(MONTH, CAST(A.BonusYear AS VARCHAR(4)) + '-' + 
			       	RIGHT('0' + CAST(A.BonusMonth AS VARCHAR(4)), 2) + '-01') + ' ' +
			        DATENAME(YEAR, CAST(A.BonusYear AS VARCHAR(4)) + '-' + 
			       	RIGHT('0' + CAST(A.BonusMonth AS VARCHAR(4)), 2) + '-01') AS periode_bns 
       		  FROM klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER a
			  WHERE a.DistributorCode = '$idmember'
				   AND a.BonusMonth = '$bns' and a.BonusYear = '$year'";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function listVoucherMember2($idmember, $month, $year) {
		$qry = "SELECT *, 
				    DATENAME(MONTH, CAST(A.BonusYear AS VARCHAR(4)) + '-' + 
			       	RIGHT('0' + CAST(A.BonusMonth AS VARCHAR(4)), 2) + '-01') + '/' +
			        DATENAME(YEAR, CAST(A.BonusYear AS VARCHAR(4)) + '-' + 
			       	RIGHT('0' + CAST(A.BonusMonth AS VARCHAR(4)), 2) + '-01') AS PERIODEX 
			    FROM klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER a
				WHERE a.DistributorCode = '$idmember'
				   AND a.BonusMonth = $month and a.BonusYear = $year";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListVoucherDiskon($idmmember) {
		//field ExpireDate di ganti tgl_expire
		/*$qry = "SELECT a.voucherkey as VoucherNo, a.VoucherAmt, a.[status], a.vchtype,
              CONVERT(VARCHAR(10), a.ExpireDate , 103) as ExpireDate
            FROM klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER a
            WHERE a.DistributorCode = '$idmmember'
              AND (LEFT(a.VoucherNo, 3) != 'XPV' and LEFT(a.VoucherNo, 3) !=  'ZPV')
              and a.[status] = '0' and a.claimstatus = '0' and a.tgl_expire >= GETDATE()";*/
		$qry = "SELECT
			   CASE 
			      WHEN a.vchtype = 'C' THEN a.voucherkey 
			      WHEN a.vchtype = 'P' THEN a.VoucherNo
			   END AS VoucherNo, 
			   a.VoucherAmt, 
			   a.[status], 
			   a.vchtype, 
			   CONVERT(VARCHAR(10), a.ExpireDate , 103) as ExpireDate
			FROM klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER a
			WHERE a.DistributorCode = '$idmmember'
			  AND (LEFT(a.VoucherNo, 3) != 'XPV' and LEFT(a.VoucherNo, 3) !=  'ZPV' and LEFT(a.VoucherNo, 3) !=  'XPP')
			  and a.[status] = '0' and a.claimstatus = '0' and a.tgl_expire >= GETDATE()";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function checkValidVch($idmember, $voucherno, $tipe) {
		/*$qry = "SELECT a.voucherkey as VoucherNo, a.VoucherAmt, a.[status], a.vchtype,
              CONVERT(VARCHAR(10), a.ExpireDate , 103) as ExpireDate
            FROM klink_mlm2010.dbo.tcvoucher a
            WHERE a.DistributorCode = '$idmember' AND a.voucherkey = '$voucherno'
              and a.[status] = '0' and a.claimstatus = '0' and a.ExpireDate >= GETDATE()"; */

		$updField = "a.VoucherNo";
		if($tipe == "C") {
			$updField = "a.voucherkey";
		}
		$qry = "SELECT $updField as VoucherNo, a.VoucherAmt, a.[status], a.vchtype,
			  CONVERT(VARCHAR(10), a.ExpireDate , 103) as ExpireDate
			FROM klink_mlm2010.dbo.tcvoucher a
			WHERE a.DistributorCode = '$idmember' AND $updField = '$voucherno'
			  and a.[status] = '0' and a.ExpireDate >= GETDATE()";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function checkValidVchBlmDiKlaim($idmember, $voucherno) {
		$qry = "SELECT a.voucherkey as VoucherNo, a.VoucherAmt, a.[status], a.vchtype,
			  CONVERT(VARCHAR(10), a.ExpireDate , 103) as ExpireDate
			FROM klink_mlm2010.dbo.tcvoucher a
			WHERE a.DistributorCode = '$idmember' AND a.voucherkey = '$voucherno'
			  and a.[status] = '0' and a.claimstatus = '0' and a.ExpireDate >= GETDATE()";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	/*----------------------------------
     * PROMO NOVEMBER 2017 APRIL 2018
     * ----------------------------------*/
	function listPeriodeBnsPromo() {
		$qry = "select DISTINCT(CONVERT(VARCHAR(10), a.bnsperiod , 103)) as bnsperiod
				from TWA_KLPromo_Oct17_H a
				order By CONVERT(VARCHAR(10), a.bnsperiod , 103) DESC";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function listWinnerBnsNovApr() {


//		$qry = "select VoucherNo, dfno, fullnm, CONVERT(VARCHAR(10), a.bnsperiod , 103) as bnsperiod
//				from TWA_KLPromo_Oct17_H a
//				WHERE CONVERT(VARCHAR(10), a.bnsperiod , 103) = '$bnsPeriod'
//				order By a.fullnm";
		$qry = "select VoucherNo, dfno, fullnm, CONVERT(VARCHAR(10), a.bnsperiod , 103) as bnsperiod
				from TWA_KLPromo_Oct17_H a
				where CONVERT(VARCHAR(10), a.bnsperiod , 103) = '01/05/2018'
				order By a.fullnm";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function listWnrBnsNovApr($searchBy, $searchValue, $bnsPeriod) {
		$cari = "";
		if($searchBy != "") {
			$cari = " WHERE Fullnm LIKE '%".$searchValue."%'";
			if($searchBy == "dfno") {
				$cari = " WHERE dfno = '".$searchValue."'";
			}

			$bnsP = "";
			if($bnsPeriod != "") {
				$bnsP = " AND CONVERT(VARCHAR(10), a.bnsperiod , 103) = '$bnsPeriod'";
			}
		} else {
			$bnsP = "";
			if($bnsPeriod != "") {
				$bnsP = " WHERE CONVERT(VARCHAR(10), a.bnsperiod , 103) = '$bnsPeriod'";
			}
		}



		$qry = "select VoucherNo, dfno, fullnm, CONVERT(VARCHAR(10), a.bnsperiod , 103) as bnsperiod
				from TWA_KLPromo_Oct17_H a $cari $bnsP ";
		// $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function listWinnerBnsNovAprAll() {


		$qry = "select dfno, fullnm, CONVERT(VARCHAR(10), a.bnsperiod , 103) as bnsperiod
				from TWA_KLPromo_Oct17_H a
				order By a.fullnm";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getHeaderVchNovApr($idmember) {
		$qry = "SELECT a.VoucherNo, a.dfno, a.Fullnm, CONVERT(VARCHAR(10), a.bnsperiod , 103) as bnsperiod,
    	            CONVERT(VARCHAR(10), a.createdt , 103) as createdt,
    	            CONVERT(VARCHAR(10), b.ExpireDate , 103) as ExpireDate,
    	            CONVERT(VARCHAR(10), b.claim_date , 103) as claim_date, b.loccd,C.fullnm as sc_fullnm,
    	            b.claimstatus    
    	        FROM TWA_KLPromo_Oct17_H a
    	        INNER JOIN tcvoucher b ON (a.VoucherNo = b.VoucherNo)
				LEFT OUTER JOIN mssc C ON B.loccd COLLATE SQL_Latin1_General_CP1_CS_AS=C.loccd
    	        WHERE a.dfno = '$idmember'";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getDetailPrdVchNovApr($voucherno) {
		$qry = "SELECT *
    	        FROM TWA_KLPromo_Oct17_D a
    	        WHERE Voucherno = '$voucherno'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function insertNewMemberWithVoucher($new_id, $ordernoo, $member_info, $shippinginfo) {
		$userlogin = trim(strtoupper($member_info['userlogin']));
		$sponsorid = trim(strtoupper($member_info['sponsorid']));
		$recruiterid = trim(strtoupper($member_info['recruiterid']));
		$nmmember = trim(strtoupper($member_info['membername']));
		$noktp = trim(strtoupper($member_info['idno']));
		$addr11 = trim(strtoupper($member_info['addr']));
		$sex = trim(strtoupper($member_info['sex']));
		$idstk = trim(strtoupper($shippinginfo['id_stockist']));

		$bankaccno = trim(strtoupper($member_info['no_rek']));
		$bankaccnm1 = trim(strtoupper($member_info['nama_rekening']));
		$bankcode = trim(strtoupper($member_info['pilBank']));
		$kdpos1 =  "000004";

		$tel_hp = trim(strtoupper($member_info['tel_hp']));
		$email = trim(strtoupper($member_info['memb_email']));
		$id_lp = trim(strtoupper($member_info['id_lp']));

		$pay_tipe = strtoupper($member_info['pay_tipe']);
		$voucherno = strtoupper($member_info['voucherno']);
		$voucherkey = strtoupper($member_info['voucherkey']);

		$ip = $_SERVER['REMOTE_ADDR'];

		$tgllahir = explode("/", trim(strtoupper($member_info['birthdt'])));
		$birthdt = $tgllahir[2]."-".$tgllahir[1]."-".$tgllahir[0];
		$password = $tgllahir[0]."".$tgllahir[1]."".$tgllahir[2];
		$res = $this->getStateByStk($idstk);
		$st_id = $res[0]->state;

		$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, id_landingpage)
                    values ('".$sponsorid."','".$new_id."','".$nmmember."','".$noktp."','".$addr11."',
                            '','','".$tel_hp."','".$email."','".$idstk."','".$sex."',
                            '".$birthdt."','".$bankaccno."','".$bankaccnm1."','".$bankcode."',
                            '".$this->dateTime."','".$ordernoo."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".$userlogin."','$recruiterid',
                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '$id_lp')";
		//echo $insMemb;
		$res = $this->executeQuery($insMemb, $this->setDB(2));
		if($res > 0) {
			$res = $this->show_member_newVoucher($new_id);
		}
		return $res;
	}

	function show_member_newVoucher($idmember) {
		$sql = "SELECT a.memberid, a.membername,a.password,a.sponsorid,a.stk_code,
				b.fullnm as sponsorname, a.trx_no, a.email,
				a.recruiterid, c.fullnm as recruitername,
				b.loccd, a.tel_hp, '' AS secno
				from db_ecommerce.[dbo].ecomm_memb_ok a 
				    inner join klink_mlm2010.[dbo].msmemb b on a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS                
				    inner join klink_mlm2010.[dbo].msmemb c on a.recruiterid=c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				where a.memberid = '$idmember'";
		return $this->getRecordset($sql, null, $this->setDB(1));
	}

	function checkSalesVchGeneral($NoVoucher) {
		$qry = "SELECT a.VoucherNo,
    	               a.VoucherAmt, 
    	               a.tipe_voucher as vchtype, 
			           CONVERT(VARCHAR(10), a.expiredt , 103) as ExpireDate
			FROM ecomm_sales_voucher a
			WHERE a.VoucherNo = '$NoVoucher' AND a.tipe_voucher = 'D' AND
			  a.statusclaim = '0' and a.expiredt >= GETDATE()";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function saveHifiTrx($data, $detail_trx, $ppob = "1") {
		//ACCUMULATE DATA FORM
		$trx_id = $data['trx_id'];
		$memberid = $data['memberid'];
		$membername = $data['membername'];
		$trx_type = $detail_trx[0]->trx_type;
		$member_no_hp = $data['no_hp'];
		$cashback_ori = $data['cashback_ori'];
		$cashback_reff = $data['cashback_reff'];
		$tgl = explode("/", trim(strtoupper($data['trx_date'])));
		$jam = date('H:i:s');
		$trx_date = $tgl[2]."-".$tgl[1]."-".$tgl[0]." ".$jam;

		$dbqryx  = $this->load->database("db_ecommerce", TRUE);

		//QUERY FOR CHECK DOUBLE TRANSACTION ID
		$dbqryx->select('trx_id');
		$dbqryx->from('ppob_trx_hdr');
		$dbqryx->where('trx_id', $trx_id);
		$query = $dbqryx->get();

		if( $query->num_rows() > 0) {
			return 2;
		} else {
			//START TRANSACTION
			$dbqryx->trans_start();

			$nominal = 0;

			//INSERT TABEL ppob_trx_det
			//PPOB = 1, TRX HI-FI JATIS
			if($ppob == "1") {
				foreach($detail_trx as $dtax) {
					$nominal += $dtax->nominal;
					$dbqryx->set('trx_id', $trx_id);
					$dbqryx->set('nominal', $dtax->nominal);
					$dbqryx->set('cust_no', $dtax->cust_no);
					$dbqryx->set('qty', 1);

					$dbqryx->insert('ppob_trx_det');

				}

				//INSERT TABEL ppob_trx_hdr
				$dbqryx->set('trx_id', $trx_id);
				$dbqryx->set('memberid', $memberid);
				$dbqryx->set('membername', $membername);
				$dbqryx->set('member_no_hp', $member_no_hp);
				$dbqryx->set('trx_type', $trx_type);
				$dbqryx->set('createdt', $trx_date);
				$dbqryx->set('nominal', $nominal);
				$dbqryx->set('cashback_ori', $cashback_ori);
				$dbqryx->set('cashback_reff', $cashback_reff);
				$dbqryx->set('ppob_type', $ppob);

				$dbqryx->insert('ppob_trx_hdr');
			} else {

			}



			//END TRANSACTION	
			$dbqryx->trans_complete();

			//CHECK IF TRANSACTION PROCESS IS COMMITED
			if ($dbqryx->trans_status() === FALSE) {
				//if something went wrong, rollback everything
				$dbqryx->trans_rollback();
				return 0;
			} else {
				//if everything went right, insert the data to the database
				$dbqryx->trans_commit();
				return 1;
			}
		}
	}

	//tambahan Hilal Tax @12/Feb/2018
	function listTaxMember($idmember, $bns, $year) {
		$qry = "SELECT No, Nomor_Bukti_Potong, NPWP, NIK, Nama, Alamat, Kode_Pajak, Jumlah_Bruto,
					Jumlah_DPP, Tanpa_NPWP, Tarif, Jumlah_PPh, NPWP_Pemotong, Nama_Pemotong, 
					Tanggal_Bukti_Potong, Bulan_Bonus, KDSTOCKIST, ID, NMSTOCKIST, NAMAKIRIM,
					ALM1, ALM2, ALM3, ALM4
			  FROM V_HILAL_PAJAK_DETAIL A
			  WHERE ID = '$idmember'
				   AND MONTH(Bulan_Bonus) = $bns and YEAR(Bulan_Bonus)  = $year";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
	//End tambahan Hilal Tax @12/Feb/2018

	function checkAccessToken($data) {
		$dbqryx = $this->load->database("db_ecommerce", TRUE);

		if(array_key_exists("idmember", $data)) {
			$user_id = $data['idmember'];
			$type = "1";
		}

		if(array_key_exists("idstockist", $data)) {
			$user_id = $data['idstockist'];
			$type = "2";
		}

		/*$dbqryx->select('a.device_id, a.device_type, a.user_id, a.usr_type');
		$dbqryx->from('apps_device a');
		$dbqryx->where('device_id', $data['device_id']);
		$dbqryx->where('user_id', $user_id);
		$query = $dbqryx->get();	*/
		//echo $dbqryx->last_query();
		//if($query->num_rows() > 0) {
		$resx = $this->getDataToken($user_id, $data['device_id']);
		//print_r($resx);
		if($resx == null) {


			$apps_device = array(
				"user_id" => $user_id,
				"device_id" => $data['device_id'],
				"device_type" => $data['device_type'],
				"usr_type" => $type
			);
			//$dbqryx->insert('apps_users', $apps_users);				
			$dbqryx->insert('apps_device', $apps_device);
			//echo "s";
			return $this->getDataToken($user_id, $data['device_id']);
			//return $has->result();

		} else {
			//echo "ssd";
			return $resx;
		}
	}

	function getDataToken($user_id, $device) {
		$dbqryx = $this->load->database("db_ecommerce", TRUE);

		$dbqryx->select('a.device_id, a.device_type, a.user_id, a.usr_type');
		$dbqryx->from('apps_device a');
		$dbqryx->where('device_id', $device);
		$dbqryx->where('user_id', $user_id);
		$query = $dbqryx->get();
		return $query->result();
	}

	function updateLocStockist($data) {
		$dbqryx = $this->load->database("klink_mlm2010", TRUE);
		$dbqryx->set('latitude', $data['latitude']);
		$dbqryx->set('longitude', $data['longitude']);
		$dbqryx->where('loccd', $data['idstockist']);
		$dbqryx->update('mssc');

		if ($dbqryx->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function getStockistLoc($stockistid) {
		$dbqryx = $this->load->database("klink_mlm2010", TRUE);
		$dbqryx->select('a.loccd, a.fullnm, a.latitude, a.longitude');
		$dbqryx->from('mssc a');
		$dbqryx->where('loccd', $stockistid);
		$query = $dbqryx->get();
		return $query->result();
	}

	function listOrderByStockistId($id, $status = null,$limit, $offset) {
		//SET KONDISI AWAL, ORDER BLM ADA YG AMBIL/TAKE
		$whereStatus = "AND (a.order_taken <> '1' or A.order_taken is null)";
		$expCek = "AND exp >= GETDATE()";
		if($status != null) {
			
			//ORDER SUDAH DI TAKE/AMBIL OLEH MS/STOKIS, PENGIRIMAN BARANG MSH DLM PROSES/BELUM SAMPAI
			if($status == "2") {
				$plus = "b.delivery_status <> '4' AND a.taken_by = '$id'";
				$whereStatus = "AND a.order_taken = '1' AND $plus";
				$expCek = "";
			}
			
			//ORDER SUDAH DI TAKE/AMBIL OLEH MS/STOKIS, PENGIRIMAN BARANG SUDAH SAMPAI
			if($status == "3") {
				$plus = "b.delivery_status = '4' AND a.taken_by = '$id'";
				$whereStatus = "AND a.order_taken = '1' AND $plus";
				$expCek = "";
			}						
		} 
		//$from = "2018-02-01";
		$from = date("Y-m-d");
		$to = date("Y-m-d");
		$first = 0;
		$last = 0;
		if($offset == 0 || $offset == 1){
			$first = 1;
			$last  = $limit;
			//echo "test ".$no_page."-".$first."-".$last."<br>";
		} else {
			$xx = $offset - 1;
			if($xx == 0){
				$xx = 1;
			}
			//echo $xx;
			$first = $xx * $limit + 1;
			$last  = $first + ($limit - 1);
		}
		
		$latitudeStk = null;
		$longitudeStk = null;
		$resStockist = $this->getLatLongStk($id);
		//print_r($resStockist);
		if($resStockist != null) {
			$latitudeStk = $resStockist[0]->latitude;
		    $longitudeStk = $resStockist[0]->longitude;
		
		

		//$from = date("Y-m-d");
		//$to = date("Y-m-d");
		$qry = "SELECT XX.RowNumber,
				       XX.orderno,
				       XX.id_memb,nmmember,idstk,receiver_name, addr1,
				  	   receiver_phone_number,
				  	   total_weight,conoteJNE, payShip,
				  	   order_taken, taken_by,trx_dt,trx_time,delivery_status,
				  	   lat_dest,long_dest, pricecode, sentTo, image_name,exp, exp_delivery, distance
				FROM(
				SELECT
				  ROW_NUMBER() OVER (ORDER BY a.orderno desc) as RowNumber,
				  a.orderno, a.id_memb, a.nmmember, a.idstk,
				  b.receiver_name, b.addr1,
				  b.tel_hp1 as receiver_phone_number,
				  b.total_weight, b.conoteJNE, a.payShip,
				  a.order_taken, a.taken_by,
				  convert(char(10), a.datetrans, 120) as trx_dt,
				  convert(char(5), a.datetrans, 108) as trx_time,
				  b.delivery_status,
				  b.lat_dest, b.long_dest, c.pricecode2, a.sentTo, 
				  d.image_name,
				  DATEADD(mi, 30,   a.datetrans)as exp,
				  DATEADD(mi, 120,   a.datetrans)as exp_delivery,
				  (
					6371 * acos (
							  cos ( radians(CONVERT(float, REPLACE(b.lat_dest, ',', '.'))) )
							  * cos( radians( CONVERT(float, REPLACE($latitudeStk, ',', '.')) ) )
							  * cos( radians( CONVERT(float, REPLACE($longitudeStk, ',', '.')) ) - radians(CONVERT(float, REPLACE(b.long_dest, ',', '.'))) )
							  + sin ( radians(CONVERT(float, REPLACE(b.lat_dest, ',', '.'))) )
							  * sin( radians( CONVERT(float, REPLACE($latitudeStk, ',', '.')) ) )
							)
				  ) AS distance
				  
				FROM ecomm_trans_hdr a
				INNER JOIN ecomm_trans_shipaddr b ON (a.orderno = b.orderno)
				LEFT JOIN klink_mlm2010.dbo.mssc c ON a.idstk=c.loccd COLLATE SQL_Latin1_General_CP1_CI_AS
				LEFT JOIN ecomm_trans_ver_image d ON a.orderno=d.orderno

				WHERE a.sentTo='2'
                AND b.lat_dest is not null AND b.lat_dest != '' AND b.long_dest is not null AND b.long_dest != '' 

				$whereStatus
				
				) XX
				WHERE XX.RowNumber BETWEEN ".$first." AND ".$last." 
				  and XX.distance <= 50 $expCek
				";
		//		
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db1);
		$i=0;
		if($res == null) {
			$hasil = null;
		} else {
			foreach($res as $dta) {
				$hasil[$i]['trx_info']['RowNumber'] = $dta->RowNumber;

				$hasil[$i]['trx_info']['orderno'] = $dta->orderno;
				$hasil[$i]['trx_info']['memberid'] = $dta->id_memb;
				$hasil[$i]['trx_info']['membername'] = $dta->nmmember;
				$hasil[$i]['trx_info']['idstk'] = $dta->idstk;
				$hasil[$i]['trx_info']['pricecode'] = $dta->pricecode;
				$hasil[$i]['trx_info']['exp'] = $dta->exp;
				$hasil[$i]['trx_info']['exp_delivery'] = $dta->exp_delivery;
				if($dta->sentTo==2){
					$hasil[$i]['trx_info']['delivery_type'] = "Dikirim ke alamat";
				}
				else{
					$hasil[$i]['trx_info']['delivery_type'] = "Dikirim ke stockis";
				}

				if($dta->image_name!=null){
					$hasil[$i]['trx_info']['image_name'] ="https://www.k-net.co.id/assets/upload_order_ver/$dta->orderno/$dta->image_name";
				}else{
					$hasil[$i]['trx_info']['image_name'] ="";
				}



				$prd = "SELECT a.prdcd, a.prdcd, a.prdnm, a.qty, 'https://www.k-net.co.id/assets/images/products/'+b.img_name as img_name, c.dp as price, (c.dp * a.qty) as subtotal
						FROM ecomm_trans_det_prd a
						LEFT JOIN master_prd_cat_inv b
						on a.prdcd=b.cat_inv_id
						LEFT JOIN klink_mlm2010.dbo.pricetab c
						ON a.prdcd=c.prdcd COLLATE SQL_Latin1_General_CP1_CI_AS
				        WHERE a.orderno = '".$dta->orderno."' AND c.pricecode='$dta->pricecode' ";
//				echo $prd;
				$prdRes = $this->getRecordset($prd, null, $this->db1);
				$hasil[$i]['product_list'] = $prdRes;

				$hasil[$i]['shipping_info']['receiver_name'] = $dta->receiver_name;
				$alamat = str_replace("\r", "", $dta->addr1);
				$alamat2 = str_replace("\n", "", $alamat);
				$hasil[$i]['shipping_info']['address'] = $alamat2;
				$hasil[$i]['shipping_info']['receiver_phone_number'] = $dta->receiver_phone_number;
				$hasil[$i]['shipping_info']['total_weight'] = $dta->total_weight;
				$hasil[$i]['shipping_info']['conoteJNE'] = $dta->conoteJNE;
				$hasil[$i]['shipping_info']['ship_cost'] = $dta->payShip;
				$hasil[$i]['shipping_info']['delivery_status'] = $dta->delivery_status;
				$hasil[$i]['shipping_info']['lat_dest'] = $dta->lat_dest;
				$hasil[$i]['shipping_info']['long_dest'] = $dta->long_dest;
				$hasil[$i]['shipping_info']['trx_date'] = $dta->trx_dt;
				$hasil[$i]['shipping_info']['trx_time'] = $dta->trx_time;
				$hasil[$i]['shipping_info']['order_taken'] = $dta->order_taken;
				$hasil[$i]['shipping_info']['taken_by'] = $dta->taken_by;
				$hasil[$i]['shipping_info']['distance'] = $dta->distance;
				$i++;
			}
		}
	   } else {
	   	 $hasil = null;
	   }
		return $hasil;
	}

	function detailOrderByStockistId($orderno) {
//		$qry = "SELECT a.orderno, a.id_memb, a.nmmember, a.idstk,
//				  b.receiver_name, b.addr1,
//				  b.tel_hp1 as receiver_phone_number,
//				  b.total_weight, b.conoteJNE, a.payShip,
//				  a.order_taken, a.taken_by,
//				  convert(char(10), a.datetrans, 120) as trx_dt,
//				  convert(char(5), a.datetrans, 108) as trx_time,
//				  b.delivery_status,
//				  b.lat_dest, b.long_dest
//				FROM ecomm_trans_hdr a
//				INNER JOIN ecomm_trans_shipaddr b ON (a.orderno = b.orderno)
//				WHERE a.orderno = '$orderno'";
		$qry = "SELECT top 1 a.orderno, a.id_memb, a.nmmember, a.idstk,
				  b.receiver_name, b.addr1,
				  b.tel_hp1 as receiver_phone_number,
				  b.total_weight, b.conoteJNE, a.payShip,
				  a.order_taken, a.taken_by,
				  convert(char(10), a.datetrans, 120) as trx_dt,
				  convert(char(5), a.datetrans, 108) as trx_time,
				  b.delivery_status,
				  b.lat_dest, b.long_dest,
					d.image_name,
					c.dpr,
					(c.dpr * c.qty) as sub_total,
					c.pricecode,
					a.sentTo,
					DATEADD(mi, 30, a.datetrans) as exp,
					DATEADD(mi, 120, a.datetrans) as exp_delivery,
					(
					6371 * acos (
							  cos ( radians(CONVERT(float, REPLACE(b.lat_dest, ',', '.'))) )
							  * cos( radians( CONVERT(float, REPLACE(e.latitude, ',', '.')) ) )
							  * cos( radians( CONVERT(float, REPLACE(e.longitude, ',', '.')) ) - radians(CONVERT(float, REPLACE(b.long_dest, ',', '.'))) )
							  + sin ( radians(CONVERT(float, REPLACE(b.lat_dest, ',', '.'))) )
							  * sin( radians( CONVERT(float, REPLACE(e.latitude, ',', '.')) ) )
							)
						  ) AS distance
				FROM ecomm_trans_hdr a
				INNER JOIN ecomm_trans_shipaddr b ON (a.orderno = b.orderno)
				INNER JOIN ecomm_trans_det_prd c ON (b.orderno = c.orderno)
 				LEFT JOIN ecomm_trans_ver_image d ON a.orderno=d.orderno
                LEFT JOIN klink_mlm2010.dbo.mssc e ON a.idstk=e.loccd COLLATE SQL_Latin1_General_CP1_CI_AS
				WHERE b.lat_dest is not null AND b.lat_dest != '' AND b.long_dest is not null AND b.long_dest != '' 
				AND a.orderno = '$orderno'";
		$res = $this->getRecordset($qry, null, $this->db1);
		$i=0;
		if($res == null) {
			$hasil = null;
		} else {
			foreach($res as $dta) {

				$hasil[$i]['trx_info']['orderno'] = $dta->orderno;
				$hasil[$i]['trx_info']['memberid'] = $dta->id_memb;
				$hasil[$i]['trx_info']['membername'] = $dta->nmmember;
				$hasil[$i]['trx_info']['idstk'] = $dta->idstk;
				$hasil[$i]['trx_info']['exp'] = $dta->exp;
				$hasil[$i]['trx_info']['exp_delivery'] = $dta->exp_delivery;
				if($dta->sentTo==2){
					$hasil[$i]['trx_info']['delivery_type'] = "Dikirim ke alamat";
				}
				else{
					$hasil[$i]['trx_info']['delivery_type'] = "Dikirim ke stockis";
				}

				if($dta->image_name!=null){
					$hasil[$i]['trx_info']['image_name'] ="https://www.k-net.co.id/assets/upload_order_ver/$dta->orderno/$dta->image_name";
				}else{
					$hasil[$i]['trx_info']['image_name'] ="";
				}
				$hasil[$i]['trx_info']['pricecode'] = $dta->pricecode;

//				$prd = "SELECT a.prdcd, a.prdcd, a.prdnm, a.qty
//				        FROM ecomm_trans_det_prd a WHERE a.orderno = '".$dta->orderno."'";
				$prd = "SELECT a.prdcd, a.prdcd, a.prdnm, a.qty, 'https://www.k-net.co.id/assets/images/products/'+b.img_name as img_name, c.dp as price, (c.dp * a.qty) as subtotal
						FROM ecomm_trans_det_prd a
						LEFT JOIN master_prd_cat_inv b
						on a.prdcd=b.cat_inv_id
						LEFT JOIN klink_mlm2010.dbo.pricetab c
						ON a.prdcd=c.prdcd COLLATE SQL_Latin1_General_CP1_CI_AS
				        WHERE a.orderno = '".$dta->orderno."' AND c.pricecode='$dta->pricecode' ";
				//echo $prd;
				$prdRes = $this->getRecordset($prd, null, $this->db1);
				$hasil[$i]['product_list'] = $prdRes;


				$hasil[$i]['shipping_info']['receiver_name'] = $dta->receiver_name;
				$alamat = str_replace("\r", "", $dta->addr1);
				$alamat2 = str_replace("\n", "", $alamat);
				$hasil[$i]['shipping_info']['address'] = $alamat2;
				$hasil[$i]['shipping_info']['receiver_phone_number'] = $dta->receiver_phone_number;
				$hasil[$i]['shipping_info']['total_weight'] = $dta->total_weight;
				$hasil[$i]['shipping_info']['conoteJNE'] = $dta->conoteJNE;
				$hasil[$i]['shipping_info']['ship_cost'] = $dta->payShip;
				$hasil[$i]['shipping_info']['delivery_status'] = $dta->delivery_status;
				$hasil[$i]['shipping_info']['lat_dest'] = $dta->lat_dest;
				$hasil[$i]['shipping_info']['long_dest'] = $dta->long_dest;
				$hasil[$i]['shipping_info']['trx_date'] = $dta->trx_dt;
				$hasil[$i]['shipping_info']['trx_time'] = $dta->trx_time;
				$hasil[$i]['shipping_info']['order_taken'] = $dta->order_taken;
				$hasil[$i]['shipping_info']['taken_by'] = $dta->taken_by;
                $hasil[$i]['shipping_info']['distance'] = $dta->distance;
				$i++;
			}
		}
		return $hasil;
	}

	function TrxOrderTakebyMS($data) {
		$dbqryx = $this->load->database("db_ecommerce", TRUE);
		/*$dbqryx->set('order_taken', $data['take_status']);
		$dbqryx->set('taken_by', $data['idstockist']);
		$dbqryx->where('orderno', $data['orderno']);
		$dbqryx->where('order_taken !=', '1');
		$dbqryx->update('ecomm_trans_hdr');*/
		
		$qry = "UPDATE ecomm_trans_hdr SET order_taken = '$data[take_status]', taken_by = '$data[idstockist]'
		        WHERE orderno = '$data[orderno]' AND order_taken != 1";
		$dbqryx->query($qry);
		//echo $qry;
		//$res = $this->getRecordset($qry, null, $this->db2);
		//return $res;

		if ($dbqryx->affected_rows() > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

	function blastNotifByOrderId($orderid) {
		//IDEC1803000037
		/*$qry = "SELECT 
				  b.orderno, b.lat_dest, b.long_dest
				FROM ecomm_trans_shipaddr b 
				WHERE b.orderno = '$orderid' AND b.lat_dest is not null AND b.long_dest is not null";*/
		$qry = "SELECT 
				  b.orderno, b.lat_dest, b.long_dest, 
				  a.datetrans,DATEADD(mi, 120, a.datetrans) as exp,  
				  a.order_taken, a.taken_by, a.sentTo, a.delivery_status
				FROM ecomm_trans_shipaddr b 
				INNER JOIN ecomm_trans_hdr a ON (a.orderno = b.orderno)
				WHERE b.orderno = '$orderid' 
				  AND b.lat_dest is not null AND b.long_dest is not null
				  AND DATEADD(mi, 120, a.datetrans) >= GETDATE()
				  AND a.delivery_status = '1' and a.order_taken = '0'";
		$res = $this->getRecordset($qry, null, $this->db1);
		$i=0;
		if($res == null) {
			$res2 = null;
		} else {
			$lat_dest = $res[0]->lat_dest;
			$long_dest = $res[0]->long_dest;
			$qry2 = "SELECT * FROM(
							SELECT (
						      6371 * acos (
								  cos ( radians(CONVERT(float, REPLACE($lat_dest, ',', '.'))) )
								  * cos( radians( CONVERT(float, REPLACE(c.latitude, ',', '.')) ) )
								  * cos( radians( CONVERT(float, REPLACE(c.longitude, ',', '.')) ) - radians(CONVERT(float, REPLACE($long_dest, ',', '.'))) )
								  + sin ( radians(CONVERT(float, REPLACE($lat_dest, ',', '.'))) )
								  * sin( radians( CONVERT(float, REPLACE(c.latitude, ',', '.')) ) )
								  )
							  ) as distance, loccd, fullnm, addr1, '$lat_dest' as lat_dest, '$long_dest' as long_dest, 
							  c.latitude as lat_source, c.longitude as long_source,
							  d.device_id as token_firebase
							FROM klink_mlm2010.dbo.mssc c
							INNER JOIN apps_device d ON (c.loccd COLLATE SQL_Latin1_General_CP1_CI_AS = d.user_id)   
							where 
                              c.latitude is not null and c.longitude is not null
						  )  x
					where x.distance <= 20";
					// '
			//echo $qry2;		
			$hasil2 = $this->getRecordset($qry2, null, $this->db1);
			if($hasil2 != null) {
				$arrTokenFirebase = array();
				foreach($hasil2 as $dta) {
					array_push($arrTokenFirebase, $dta->token_firebase);
				}	
				$res2 = $arrTokenFirebase;
			} else {
			   $res2 = null;
			}
			
		}	
		
		
		
		return $res2;
	}
	
	function blastNotifByStockist($idstk) {
		$qry2 = "SELECT c.loccd, d.device_id as token_firebase 
		        FROM klink_mlm2010.dbo.mssc c
				INNER JOIN db_ecommerce.dbo.apps_device d ON (c.loccd COLLATE SQL_Latin1_General_CP1_CI_AS = d.user_id)
				WHERE d.user_id IN ($idstk)";
		//echo $qry2;
		$hasil2 = $this->getRecordset($qry2, null, $this->db1);
		if($hasil2 == null) {
			return null;
        } else {		
			$arrTokenFirebase = array();
			foreach($hasil2 as $dta) {
				array_push($arrTokenFirebase, $dta->token_firebase);
		    }	
		    $res2 = $arrTokenFirebase;
		return $res2;
		}
	}

}
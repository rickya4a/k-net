<?php
class M_audio_store extends MY_Model{
    
	function __construct() {
		// Call the Model constructor
		parent::__construct();

	}
	//request 02/10/2018
	function listWebinar($limit = null, $offset = null) {
		if($limit == null && $offset == null) {
			$qry = "SELECT a.id, 
						   a.title,
						   a.speaker,
						   CONVERT(VARCHAR(10), a.date, 120) as date,
						   a.imageUrl,
						   a.youtubeVideoId
					FROM mapps_webinar a";
		} else {
			$first = 0;
			$last = 0;
			if($offset == 0 || $offset == 1){
				$first = 1;
				$last  = $limit;
				//echo "test ".$no_page."-".$first."-".$last."<br>";
			} else {
				$xx = $offset - 1;
				if($xx == 0){
					$xx = 1;
				}
				//echo $xx;
				$first = $xx * $limit + 1;
				$last  = $first + ($limit - 1);
			}
			
			$qry = "SELECT
                       XX.RowNumber,			
			           XX.id,
			           XX.title,
					   XX.speaker,
					   XX.date,
					   XX.imageUrl,
					   XX.youtubeVideoId
					FROM(
							SELECT 
							   ROW_NUMBER() OVER (ORDER BY a.id desc) as RowNumber,
							   a.id, 
							   a.title,
							   a.speaker,
							   CONVERT(VARCHAR(10), a.date, 120) as date,
							   a.imageUrl,
							   a.youtubeVideoId
							FROM mapps_webinar a
							
					) XX
					WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."
					ORDER BY date DESC";
		}			
		$resCheck = $this->getRecordset($qry, null, "db_ecommerce");
		return $resCheck;
	}
	
	function listRegion($limit = null, $offset = null) {
	    if($limit == null && $offset == null) {
			$qry = "SELECT DISTINCT(a.[state]) as id, b.[description] as regionName
					FROM mssc a
					INNER JOIN [state] b ON (a.[state] = b.st_id)
					WHERE a.[state] is not null AND a.[state] != ''
					AND a.[state] NOT IN ('MR', 'JD', 'AU', 'TM')
					GROUP BY a.[state], b.[description]";
		} else {
			$first = 0;
			$last = 0;
			if($offset == 0 || $offset == 1){
				$first = 1;
				$last  = $limit;
				//echo "test ".$no_page."-".$first."-".$last."<br>";
			} else {
				$xx = $offset - 1;
				if($xx == 0){
					$xx = 1;
				}
				//echo $xx;
				$first = $xx * $limit + 1;
				$last  = $first + ($limit - 1);
			}
			
			$qry = "SELECT
                       XX.RowNumber,			
			           XX.id,
			           XX.regionName
					FROM(
							SELECT 
							   ROW_NUMBER() OVER (ORDER BY a.[state] desc) as RowNumber,
							   a.[state] as id, 
							   b.[description] as regionName
							FROM mssc a
							INNER JOIN [state] b ON (a.[state] = b.st_id)
							WHERE a.[state] is not null AND a.[state] != ''
							AND a.[state] NOT IN ('MR', 'JD', 'AU', 'TM')
							GROUP BY a.[state], b.[description]
							
					) XX
					WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."
					ORDER BY RowNumber, regionName DESC";
		}			
		$resCheck = $this->getRecordset($qry, null, "klink_mlm2010");
		return $resCheck;
	}
	
	function listLbcNews($cat, $limit = null, $offset = nul) {
	    $whereCat = "";
		if($cat != null) {
			$whereCat = "WHERE category = $cat";
		}
		
		if($limit == null && $offset == null) {
		    
			$qry = "SELECT 
					   a.id,
					   a.category as categoryId,
					   b.cat_name as category,
							   a.image,
							   a.title,
							   a.content,
							   CONVERT(VARCHAR(10), a.postdate, 111) AS postdate
							FROM mapps_lbc a 
							INNER JOIN mapps_lbc_cat b ON (a.category = b.id) $whereCat";
		} else {
			$first = 0;
			$last = 0;
			if($offset == 0 || $offset == 1){
				$first = 1;
				$last  = $limit;
				//echo "test ".$no_page."-".$first."-".$last."<br>";
			} else {
				$xx = $offset - 1;
				if($xx == 0){
					$xx = 1;
				}
				//echo $xx;
				$first = $xx * $limit + 1;
				$last  = $first + ($limit - 1);
			}

			$qry = "SELECT
                       XX.RowNumber,			
			           XX.id,
					   XX.categoryId,
			           XX.category,
                	   XX.image,
                	   XX.title,
                	   XX.content,
                	   XX.postdate
        
					FROM(
							SELECT 
							   ROW_NUMBER() OVER (ORDER BY a.id desc) as RowNumber,
							   a.id,
							   a.category as categoryId,
							   b.cat_name as category,
							   a.image,
							   a.title,
							   a.content,
							   CONVERT(VARCHAR(10), a.postdate, 111) AS postdate
							FROM mapps_lbc a 
							INNER JOIN mapps_lbc_cat b ON (a.category = b.id) $whereCat 
							
					) XX
					WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."
					ORDER BY RowNumber, id DESC";

		}
        //echo $qry;
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
	
	function listStockistByRegion($state = null, $limit = null, $offset = null) {
		$str = "";
		if($state != null) {
			$str .= " WHERE a.[state] = '$state' and a.fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL')";
		}

		if($limit == null && $offset == null) {
			$qry = "SELECT a.loccd as id, 
						   a.fullnm as owner,
						   a.addr1 + ' ' + a.addr2 + ' ' + a.addr3 as address,
						   b.[description] as stateStockist,
						   a.tel_hp,
						   a.tel_hm,
						   a.tel_of,
						   a.klink_event,	
						   a.stk_area
					FROM mssc a 
					INNER JOIN [state] b ON (a.[state] = b.st_id)
					WHERE a.[state] = '$state'";
		} else {
			$first = 0;
			$last = 0;
			if($offset == 0 || $offset == 1){
				$first = 1;
				$last  = $limit;
				//echo "test ".$no_page."-".$first."-".$last."<br>";
			} else {
				$xx = $offset - 1;
				if($xx == 0){
					$xx = 1;
				}
				//echo $xx;
				$first = $xx * $limit + 1;
				$last  = $first + ($limit - 1);
			}

			$qry = "SELECT
                       XX.RowNumber,			
			           XX.id,
			           XX.owner,
                	   XX.address,
                	   XX.stateStockist,
                	   XX.tel_hp,
                	   XX.tel_hm,
                       XX.tel_of,
					   XX.klink_event,
					   XX.stk_area
        
                FROM(
                    	SELECT 
						   ROW_NUMBER() OVER (ORDER BY a.loccd desc) as RowNumber,
						   a.loccd as id, 
						   a.fullnm as owner,
						   a.addr1 + ' ' + a.addr2 + ' ' + a.addr3 as address,
						   b.[description] as stateStockist,
						   a.tel_hp,
						   a.tel_hm,
						   a.tel_of,
						   a.klink_event,
						   a.stk_area	
						FROM mssc a 
						INNER JOIN [state] b ON (a.[state] = b.st_id) $str
						
				) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."
                ORDER BY RowNumber, owner DESC";

		}
        //echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
	
	//end
   	
	function checkValidVoucher($voucherno, $voucherkey, $memberid)
	{
		$arr = array();	
		$responMessage = "";	
		$currentdt = date('Y/m/d');
		//$this->db = $this->load->database('alternate', true);
		
		//step 1, check apakah distributor ada voucher langganan yang masih aktif 
		$qryCheck = "SELECT TOP 1 memberid, vcno, 
			         	CONVERT(VARCHAR(10), startdt, 111) AS startdt,
					 	CONVERT(VARCHAR(10), enddt, 111) AS enddt,
					 	CONVERT(VARCHAR(10), GETDATE(), 111) AS datenowx, 
						act, prd_bundling
					 FROM  webol_langganan_voucher 
		             WHERE vcno = '".$voucherno."' and vckey = '".$voucherkey."'";
					 //CONVERT(VARCHAR(10), enddt, 111) >= '".$currentdt."'
		             //AND 
	    //echo $qryCheck;
	    $resCheck = $this->getRecordset($qryCheck, null, "klink_mlm2010");
		if($resCheck <= 0) {
			 $responMessage .= "Invalid Voucher No / Key";	
			 //$arr = array(array('err' => true, 'voucherno' => $voucherno, 'status' => 'invalid', 'responseMessage' => $responMessage, 'qry' => $qryCheck),'struct');
			 $arr = array("response" => "false", "status" => "invalid", "message" => $responMessage);
		} else {
			$enddt = $resCheck[0]->enddt;
			$now = $resCheck[0]->datenowx;
			if($enddt < $now && $enddt != "") {
				$responMessage .= "Voucher Expired, enddt : $enddt, skrg tgl $now";	
			 	//$arr = array(array('err' => true, 'voucherno' => $voucherno, 'status' => 'expired', 'responseMessage' => $responMessage, 'qry' => $qryCheck),'struct');
				$arr = array("response" => "false", "status" => "expired", "message" => $responMessage);
			} else {
				if(($resCheck[0]->memberid == "" || $resCheck[0]->memberid == null) && ($enddt == null || $enddt == "") && $resCheck[0]->act == '0') {
					/*$responMessage .= "Update ID = empty";
					$responMessage .= "<br />";
					$responMessage .= "View available list";
					$arr = array(array('err' => false, 'voucherno' => $voucherno, 'status' => 'updateReady', 'responseMessage' => $responMessage, 'qry' => $qryCheck),'struct');
					*/
					$arr = array("response" => "true", "status" => "updateReady", "message" => null);
				} else {
					if($resCheck[0]->memberid == $memberid) {
						/*$responMessage .= "View available list";
						$arr = array(array('err' => false, 'voucherno' => $voucherno, 'status' => 'Available', 'responseMessage' => $responMessage, 'qry' => $qryCheck),'struct');*/
						$arr = array("response" => "true", "status" => "available", "arrayData" => $resCheck, "message" => null);
					} else {
						$responMessage .= "Voucher sudah dipakai oleh ID lain ".$resCheck[0]->memberid.", enddt : $enddt, skrg tgl $now";
						//$arr = array(array('err' => true, 'voucherno' => $voucherno, 'status' => 'Reject', 'responseMessage' => $responMessage, 'qry' => $qryCheck),'struct');
						$arr = array("response" => "true", "status" => "reject", "message" => $responMessage);
					}
				}
			}
				
			
		}	
		
		
		//print_r($arr);
		return $arr;
	}
	
	function getLatestPeriodPrd(){
		//$qryTrx = $this->load->database('klink', true);
        $slc = "select c.`kd_kaset` AS prdcdDet, c.title, c.`kd_kaset2`, c.`descript` AS prdcdNmDet, 
		               CONCAT('http://v1.k-link.co.id/index.php?/store/download/', c.file) as fileUrl, 
						CONCAT('http://v1.k-link.co.id/store/download/thumbnail/', c.cover_kaset) as imageUrl, 
                       c.`12W` as westPrice, 
                       DATE_FORMAT(c.`period`, '%M/%Y') AS period,
                       DATE_FORMAT(c.`period`, '%Y-%m-%d') as latestperiod
				from kaset_download c 
				where (c.`flag_status`=1 or c.`flag_status`=3) 
				and active = '1'
				order by c.period desc, c.kd_kaset
				LIMIT 1";
        // echo $slc;
        $res =$this->getRecordset($slc, null, "klinkmember");
       	//echo $slc;
        return $res; 
    }
	
	function cekMasaAktifVcr2($idmember){
		
		/*$this->db = $this->load->database('alternate', true);
		  $qry = " select top 1 memberid, vcno, CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(enddt)), DATEADD(M, 1,enddt))), 111) AS enddt, CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(startdt)-1), startdt)), 111) AS startdt
				from webol_langganan_voucher 
				where memberid='".$idmember."' and 
					  CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(enddt)), DATEADD(M, 1,enddt))), 111)>='".$enddt."'";
		 */
		  $qry = " SELECT top 1 memberid, vcno, 
					   CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(enddt)), 
					   DATEADD(M, 1,enddt))), 111) AS enddt, 
					   CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(startdt)-1), startdt)), 111) AS startdt
				   FROM webol_langganan_voucher 
				   WHERE memberid='".$idmember."' 
				   ORDER BY enddt DESC";
	 
       // echo $qry;
       $res =$this->getRecordset($qry, null, "klink_mlm2010");
       	//echo $qry;
        return $res;
    }
	
	function ListBundling2($startdt, $enddt, $idmember,  $page, $max_limit_show){
	    $paging = "";
		if($max_limit_show != null && $page != null) {
			$nilaiPage = (int) $page;
			$nilaiLimit = (int) $max_limit_show;
			
			if($nilaiPage == 1 || $nilaiPage == 0) {
				$offset = 0;
			} else if($nilaiPage == 2) {
				$offset = $max_limit_show;
			} else {
				$offset = $nilaiPage * $max_limit_show;
			}
			
			$paging .= "LIMIT $offset, $max_limit_show";
		}
        $slc = "SELECT c.`kd_kaset` AS id, 
		               c.title,
		               c.`descript` AS description, 
                      CONCAT('http://v1.k-link.co.id/index.php?/store/download/', c.file) as fileUrl, 
					  CONCAT('http://v1.k-link.co.id/store/download/thumbnail/', c.cover_kaset) as imageUrl, 
					   c.`12W` as westPrice, 
					   DATE_FORMAT(c.`period`, '%M/%Y') AS period,
					   'false' as is_my_audio
				FROM kaset_download c 
				WHERE (c.`flag_status`=1 or c.`flag_status`=3) 
					AND (c.`period` between '".$startdt."' and '".$enddt."') 
					AND c.`kd_kaset` not in (
						 SELECT prdcd 
						 FROM history_langganan 
						 WHERE idmember='".$idmember."' AND hist_type=1
					)
				    AND active = '1'
				ORDER BY c.period desc, c.kd_kaset $paging";
        //echo $slc;
         $res = $this->getRecordset($slc, null, "klinkmember");
       	//echo $slc;
        return $res; 
    }
	
	function listAllBundling($idmember, $page, $max_limit_show) {
	
		$paging = "";
		if($max_limit_show != null && $page != null) {
			$nilaiPage = (int) $page;
			$nilaiLimit = (int) $max_limit_show;
			
			if($nilaiPage == 1 || $nilaiPage == 0) {
				$offset = 0;
			} else if($nilaiPage == 2) {
				$offset = $max_limit_show;
			} else {
				$offset = $nilaiPage * $max_limit_show;
			}
			
			$paging .= "LIMIT $offset, $max_limit_show";
		}
		
		$slc = "SELECT c.kd_kaset AS id, 
		           c.title,
				   c.descript AS description, 
					CONCAT('http://v1.k-link.co.id/index.php?/store/download/', c.file) as fileUrl, 
				   CONCAT('http://v1.k-link.co.id/store/download/thumbnail/', c.cover_kaset) as imageUrl, 
				   c.12W as westPrice, 
				   DATE_FORMAT(c.period, '%M/%Y') AS period,
				   'false' as is_my_audio
				FROM kaset_download c
				WHERE c.`kd_kaset` not in (
						 SELECT prdcd 
						 FROM history_langganan 
						 WHERE idmember='".$idmember."' AND hist_type=1
					)
					AND active = '1' 
					ORDER BY c.period desc, c.kd_kaset $paging";
        //echo $slc;
         $res = $this->getRecordset($slc, null, "klinkmember");
       	//echo $slc;
        return $res; 
	}
	
	function cekMasaAktifVcr($idmember){
		
		//$this->db = $this->load->database('alternate', true);
        $qry = " SELECT top 1 memberid, 
					vcno, 
					CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(enddt)), DATEADD(M, 1,enddt))), 111) AS enddt, 
					CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(startdt)-1), startdt)), 111) AS startdt
				FROM webol_langganan_voucher 
				WHERE memberid='".$idmember."' 
				  AND CONVERT(VARCHAR(10), (DATEADD(D, -(DAY(enddt)), DATEADD(M, 1,enddt))), 111) >= CONVERT(VARCHAR(10), getdate(), 111)";
        //echo $qry;
        $res =$this->getRecordset($qry, null, "klink_mlm2010");
       	//echo $qry;
        return $res;
    }
	
	function activateVoucher($voucherno, $voucherkey, $idmember) {
	    $prdcd = NULL;
		$ip = "";	
		$createdt = date('Y-m-d H:i:s');
		$qry = "update webol_langganan_voucher 
		        SET memberid = '".$idmember."',
					act = '1',ip_address = '".$ip."',
					startdt = GETDATE(),
					updateddt = GETDATE(), 
					enddt =  DATEADD(DAY, -(DAY(DATEADD(MONTH,0,GETDATE()))), DATEADD(MONTH, CONVERT(INTEGER, typeVcr), GETDATE()))
				WHERE vcno = '".$voucherno."' and vckey = '".$voucherkey."'";
		//echo $qry;
		$query = $this->executeQuery($qry, "klink_mlm2010");
		
		$ins = "INSERT INTO history_langganan(idmember,createdt,ip_addr,vchno,hist_type,prdcdCat)
				VALUES('".$idmember."','".$createdt."','".$ip."','".$voucherno."',0,'".$prdcd."')";
		//echo $qry;
		$queryMsyql = $this->executeQuery($ins, "klinkmember");
		
		if($query > 0 && $queryMsyql > 0) {
			return true;
		} else {
		   return false;
		}
		
	}
	
	function myListAudio($idmember, $page = null, $max_limit_show = null) {
		$paging = "";
		if($max_limit_show != null && $page != null) {
			$nilaiPage = (int) $page;
			$nilaiLimit = (int) $max_limit_show;
			
			if($nilaiPage == 1 || $nilaiPage == 0) {
				$offset = 0;
			} else if($nilaiPage == 2) {
				$offset = $max_limit_show;
			} else {
				$offset = $nilaiPage * $max_limit_show;
			}
			
			$paging .= "LIMIT $offset, $max_limit_show";
		}
	    $qry = "SELECT c.`kd_kaset` AS id, 
			           c.title,
		               c.`descript` AS description, 
					   CONCAT('http://v1.k-link.co.id/index.php?/store/download/', c.file) as fileUrl, 
					   CONCAT('http://v1.k-link.co.id/store/download/thumbnail/', c.cover_kaset) as imageUrl, 
					   c.`12W` as westPrice, 
					   DATE_FORMAT(c.`period`, '%M/%Y') AS period,
					   'true' as is_my_audio
				FROM kaset_download c 
				WHERE c.`kd_kaset` in (
						 SELECT prdcd 
					FROM history_langganan 
					WHERE idmember='".$idmember."' AND hist_type <> 0
					)
				    AND active = '1'
				ORDER BY c.period desc, c.kd_kaset $paging";
	//echo $qry;
		//$qry = "";
		$res = $this->getRecordset($qry, null, "klinkmember");
       	//echo $slc;
        return $res; 		
	}
	
	function getDetailAudio($audioId) {
		$qry = "SELECT c.`kd_kaset` AS id, 
					   c.title,	
		               c.`descript` AS description, 
                      
					   CONCAT('http://v1.k-link.co.id/index.php?/store/download/', c.file) as fileUrl, 
   CONCAT('http://v1.k-link.co.id/store/download/thumbnail/', c.cover_kaset) as imageUrl, 
					   c.`12W` as westPrice, 
					   DATE_FORMAT(c.`period`, '%M/%Y') AS period
				FROM kaset_download c 
				WHERE c.`kd_kaset` in (
						 SELECT prdcd 
					FROM history_langganan 
					WHERE prdcd='".$audioId."' AND hist_type <> 0
					)
				    AND active = '1'
				ORDER BY c.period desc, c.kd_kaset";
	
		//$qry = "";
		$res = $this->getRecordset($qry, null, "klinkmember");
       	//echo $slc;
        return $res; 		
	}
	
	function addMyAudioStore($idmember, $audioId, $voucherno = null) {
	    $createdt = date('Y-m-d H:i:s');
		$ip = "";	
		//$voucherno = 
		$qry = "SELECT * 
				FROM history_langganan 
				WHERE idmember='".$idmember."' and prdcd = '".$audioId."'";
		$res = $this->getRecordset($qry, null, "klinkmember");
		if($res != null && $res[0]->hist_type == 0) {
		//if($res != null && $res[0]->prdcd != null && $res[0]->prdcd != "" && $res[0]->hist_type == 0) {
				$upd = "UPDATE history_langganan
						SET hist_type = 1
						WHERE idmember='".$idmember."' and prdcd = '".$audioId."'";
				$queryMsyql = $this->executeQuery($upd, "klinkmember");
				$hasil = 2;
		} else if($res != null && $res[0]->hist_type != 0) {
			$hasil = 0;
		} else {
			$ins = "INSERT INTO history_langganan(idmember,createdt,ip_addr,vchno,hist_type,prdcd)
				VALUES('".$idmember."','".$createdt."','".$ip."','".$voucherno."',1,'".$audioId."')";
			//echo $qry;
			$queryMsyql = $this->executeQuery($ins, "klinkmember");
			$hasil = 1;
		}
		
		return $hasil;
	}

}

?>
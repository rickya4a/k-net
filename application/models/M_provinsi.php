<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class m_provinsi extends CI_Model
{
    var $column_order = array('PROVINSI_CODE','NAMA_PROVINSI','CREATED_DATE',null); //set column field database for datatable orderable
    var $column_search = array('PROVINSI_CODE','NAMA_PROVINSI','CREATED_DATE'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('ID_PROVINSI' => 'desc'); // default order
    private $db2;
    private $db;
    function __construct()
    {
        parent::__construct();

        $this->db = $this->load->database('default',TRUE);
    }


    function get_table_kw()
    {

        $result = array();
        $this->db->select('ID_PROVINSI, PROVINSI_CODE, NAMA_PROVINSI, CREATED_DATE')
            ->from('MASTER_PROVINSI')
            ->where('IS_ACTIVE','0');


        $q = $this->db->get();
        //echo $this->db->last_query();
        foreach($q->result() as $row)
        {
            $result[] = $row;
        }

        return $result;
    }

    function get_datatables($start,$lenght,$dir,$field,$s_value)
    {
        if($start == 0){
            $nilai = $start;
        }else{
            $nilai = $start;
        }

        $limit = $start + $lenght;

        $result = array();
        $this->db->select('ID_PROVINSI, PROVINSI_CODE, NAMA_PROVINSI, CREATED_DATE')
            ->from('MASTER_PROVINSI')
            ->limit($lenght,$start)
            ->where('IS_ACTIVE','0');

        if($dir == '')
            $this->db->order_by('ID_PROVINSI','ASC');
        else
            $this->db->order_by($field,$dir);

        if($s_value != '')
        {
            $this->db->like('ID_PROVINSI',$s_value);
            $this->db->or_like('PROVINSI_CODE',$s_value);
            $this->db->or_like('NAMA_PROVINSI',$s_value);
            $this->db->or_like('CREATED_DATE',$s_value);

        }

        $q = $this->db->get();
        //echo $this->db->last_query();
        foreach($q->result() as $row)
        {
            $result[] = $row;
        }

        return $result;
    }

    private function _get_datatables_query()
    {

        $this->db->select('ID_PROVINSI, PROVINSI_CODE, NAMA_PROVINSI, CREATED_DATE')
            ->from('MASTER_PROVINSI')

            //->limit($limit,$nilai)
            ->where('IS_ACTIVE','0');

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('MASTER_PROVINSI');
        return $this->db->count_all_results();
    }

    function add($data)
    {
        $this->db->trans_start();
        $this->db->insert('MASTER_PROVINSI',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, delete the data from the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function addUser($data)
    {
        $this->db->trans_start();
        $this->db->insert('user',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, delete the data from the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function update($id,$data)
    {
        $this->db->trans_start();
        $this->db->where('ID_PROVINSI',$id);
        $this->db->update('MASTER_PROVINSI',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, delete the data from the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function del($id)
    {
        $this->db->trans_start();
        $this->db->where('ID_PROVINSI',$id);
        $this->db->update('MASTER_PROVINSI',array('IS_ACTIVE'=>1));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, delete the data from the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function getDataEdit($id)
    {
        $this->db->select('*');
        $this->db->where('ID_PROVINSI',$id);
        return $this->db->get('MASTER_PROVINSI');
    }

    function getSub($id)
    {
        $result = array();
        $this->db->select('ID_KOTA, NAMA_KOTA')
            ->from('r_kota')
            ->where('ID_PROVINSI', $id)
            ->where('IS_ACTIVE', '0')
            ->where('IS_ORIG', '0');


        $q = $this->db->get();
        //echo $this->db->last_query();
        foreach($q->result() as $row)
        {
            $result[] = $row;
        }

        return $result;
    }

    function getDataCluster()
    {
        $result = array();
        $this->db->select('ID_PROVINSI, NAMA_PROVINSI')
            ->from('r_provinsi')
            ->where('IS_ACTIVE','0')
            ->order_by('ID_PROVINSI','ASC');

        $q = $this->db->get();

        foreach($q->result() as $row)
        {
            $result[] = $row;
        }

        return $result;
    }




    function get_all_usernames( $username ) {
        $this->db->select('*');
        $this->db->where('PROVINSI_CODE', $username);
        $this->db->from('MASTER_PROVINSI');
        $query = $this->db->get();
        if( $query->num_rows() > 0 ){
            return 0;
        }else{
            return 1;
        }
    }
}
?>
<?php
class Test_db extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	function testmysql() {
		$db = $this->load->database('alternate2', TRUE);
		$qry = "SELECT * FROM outbox";
		$query = $db->query($qry);
		return $query->result();
	}
	
	function tesdion() {
	    $header = array();
		$qry = "SELECT a.orderno, a.id_memb
		        FROM ecomm_trans_hdr a
				WHERE a.orderno IN ('IDEC1809000506', 'IDEC1809000507')";
		$hasil = $this->getRecordset($qry, NULL, "db_ecommerce");
		$i = 0;
		foreach($hasil as $tes) {
		    $header[$i]['orderno'] = $tes->orderno;
			$header[$i]['id_memb'] = $tes->id_memb;
			
			$detail = "SELECT  b.prdcd, b.prdnm 
					   FROM ecomm_trans_det_prd b
					   WHERE b.orderno = '".$tes->orderno."'";
			$hasil2 = $this->getRecordset($detail, NULL, "db_ecommerce");	
			$header[$i]['detail_prd'] = $hasil2;
			$i++;	
		}
		
		return $header;
	}
	
	function tesdion2() {
	    $header = array();
		$qry = "SELECT b.orderno, b.id_memb, a.prdcd, a.prdnm
		        FROM ecomm_trans_hdr b
		        INNER JOIN ecomm_trans_det_prd a ON (a.orderno = b.orderno)
				WHERE b.orderno IN ('IDEC1809000506', 'IDEC1809000507')";
		$hasil = $this->getRecordset($qry, NULL, "db_ecommerce");
		
		return $hasil;
	}
}
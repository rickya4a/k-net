<?php
class Adm_location_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	function getListAllLocation() {	
		$qry = "SELECT * FROM location";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Locations is empty..!!", 1);
		}
		return $res;
	}	
	
	function getListLocationByID($id) {
		$qry = "SELECT * FROM location WHERE locationid = '$id'";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Location is not exist..!!", 1);
		}
		return $res;
	}
	
	function saveLocation() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO location (locationid, locationname, remarks, createddate, ipaddress)
		        VALUES ('$data[id]', '$data[locationname]', '$data[remarks]', '$this->dateTime', '$data[ipaddress]')";
		$query = $this->db->query($qry);
		if(!$query) {			
			throw new Exception("Save location failed..!!", 1);
		} 
		return $query;
	}
	
	function updateLocation() {
		$data = $this->input->post(NULL, TRUE);
		$date = date("Y-m-d h:m:s");
		$qry = "UPDATE location SET locationname = '$data[locationname]', remarks = '$data[remarks]',
		               ipaddress = '$data[ipaddress]'
		        WHERE locationid = '$data[id]'";
		$query = $this->db->query($qry);
		if(!$query) {			
			throw new Exception("Update location failed..!!", 1);
		} 
		return $query;
	}
	
	
}
	
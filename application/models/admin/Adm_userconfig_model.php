<?php
class Adm_userconfig_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	function tesd() {
		
	}
	
	function getListAllLocation() {	
		$qry = "SELECT * FROM location";
		$re = 1;
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Locations is empty..!!", 1);
		}
		return $res;
	}	
	
	function getListAllAdminUser() {	
		$qry = "SELECT a.id, a.userid, a.username, a.createddate, b.locationname 
				FROM admin_user a
				LEFT JOIN location b ON (a.locationid = b.locationid)";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Admin User is empty..!!", 1);
		}
		return $res;
	}	
	
	function getListAdminUserByID($id) {
		$qry = "SELECT * FROM admin_user a
				WHERE a.userid = '$id'";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Admin User is empty..!!", 1);
		}
		return $res;
	}
	
	function getListAllClientUser() {	
		$qry = "SELECT a.id_client as id, a.userid_client as userid, a.username_client as username, a.createddate, b.locationname 
				FROM digitalsign_client a
				LEFT JOIN location b ON (a.locationid_client = b.locationid)";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Admin User is empty..!!", 1);
		}
		return $res;
	}	
	
	function getListClientUserByID($id) {
		$qry = "SELECT * FROM digitalsign_client a
				WHERE a.userid_client = '$id'";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Data Admin User is empty..!!", 1);
		}
		return $res;
	}
	
		
}
	
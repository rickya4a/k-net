<?php
 class Promo_campaign_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
	
	function cekPromo500BV($id, $promotype) {
		$arr = jsonFalseResponse("Sales BV kurang dari 500 BV..");
		$check = $this->checkDoubleInsert($id, $promotype);
		if($check == null) {
	 		$result = $this->checkIfValidBV($id);
			if($result != null) {
				//$datax = $this->promo_limit();
				$datay = $this->checkCalenderAvaibility($promotype);
				if($datay[0]->xlimit > 0) {
					$arr = array("response" => "true", "arrayData" => $result, "dataLimit" => $datay, "message" => "Data Valid");
				} else {
					$arr = jsonFalseResponse("Stock kalender sedang habis saat ini..");
				}	
			}
		} else {
			$arr = array("response" => "double", 
						 "message" => "Promo sudah pernah di klaim pada ".$check[0]->tgl_klaim. " ".$check[0]->jam_klaim,
						 "orderno" => $check[0]->orderno);
		}	
		return $arr;
	} 
	
	function checkIfValidBV($id) {
		$qry = "SELECT * 
				FROM V_HILAL_CHECK_BV A 
				WHERE A.bv>=500 AND A.dfno='$id' and (a.bnsperiod='2015-12-01' or a.bnsperiod='2016-01-01')";
		//echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
	
	function checkDoubleInsert($id, $promotype) {
		$qry = "SELECT orderno, 
		        CONVERT(VARCHAR(10), datetrans, 103) as tgl_klaim,
		        CONVERT(VARCHAR(10), datetrans, 108) as jam_klaim 
		        FROM ecomm_promo_claim_hdr WHERE id_memb = '$id' AND promotype = '$promotype'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}
	
	function promo_limit() {
		$qry = "SELECT * FROM HILAL_PROMO_LIMIT WHERE status = '1'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
	
	function checkCalenderAvaibility($id) {
		$qry = "select A.promocode, A.limitqty, A.prdcd,
					   ISNULL(sum(a.qty), 0) as qtyord, 
				       A.limitqty-ISNULL(sum(B.qtyord), 0) as xlimit
				from klink_mlm2010.dbo.HILAL_PROMO_LIMIT a
					 left outer join ecomm_promo_claim_hdr b 
				     on a.promocode=b.promotype COLLATE SQL_Latin1_General_CP1_CI_AS
				where a.promocode='$id'
				group by A.promocode, A.limitqty, A.prdcd";
		//echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;	
	}
	
	function getListStokistM() {
		$slc = "select a.loccd,a.fullnm, a.pricecode from mssc a
                where a.sctype = '1' and a.onlinetype = 'O' 
                and a.fullnm not in('TERMINATION','CANCEL','CENCEL')
                AND a.scstatus = '1' order by a.loccd";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
	}
	
	function savePromoCalendar($form) {
		$date = date("ym");
		$random = randomNumber(5);
		$orderno = "O".$date.$random;
		$token = "T".$date.$random;
		$qry = "INSERT INTO ecomm_promo_claim_hdr (orderno, token, id_memb, nmmember, total_pay,
					total_bv, pricecode, idstk, nmstkk, promotype, qtyord, userlogin)
				VALUES ('$orderno','$token', '$form[idmember]', '$form[membername]', 0,
				    0, '$form[pricecode]', '$form[idstkx]', '$form[stkname]', '$form[promotype]', 1,'".getUserID()."')";
		$result = $this->executeQuery($qry, $this->setDB(1));
		if($result > 0) {
			$result2 = $this->getDataPromoByOrderNo($orderno);		
		}
		return $result2;
	}

	function getDataPromoByOrderNo($orderno) {
		$list = "SELECT * FROM ecomm_promo_claim_hdr WHERE orderno = '$orderno'";
		$result2 = $this->getRecordset($list, $this->setDB(1));
		return $result2;
	}
	
	function sendSMSConfirm($data) {
		$alternate2 = $this->load->database('alternate2', true);
		$text = 'K-Link Promo : Kode Promo:'.$data[0]->promotype.',No Order: '.$data[0]->orderno.',ID Member: '.$data[0]->id_memb.'';
		$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
		                    VALUES ('".getUserPhone()."','".$text."','K-Link Promo')";
		        //echo "Ins SMS : $insSms"; getUserPhone()
	    $query = $alternate2->query($insSms);	
	}

	function getListCollagen($promotype, $period){
		$qryCheck = "SELECT TOP 1 * FROM klink_mlm2010.dbo.HILAL_PROMO_COLLAGEN_2016 A WHERE A.period = '$period'";
		$res1 = $this->getRecordset($qryCheck, NULL, $this->db2);
		if ($res1 > 0 || $res1 != null){
			if($promotype == 'collagen'){
				$param = " A.prdcd='IDHD014' AND ";
			}
			$qry = "SELECT TOP 150 dfno, fullnm, prdcd, prdnm, 
					       SUM(qtyorder) AS QTY, jointdt, 
					       SUM(totalbv) AS totalbv, SUM(totaldp) AS totaldp
					FROM klink_mlm2010.dbo.HILAL_PROMO_COLLAGEN_2016 A
					WHERE $param period<='$period'
					GROUP BY dfno, fullnm, prdcd, prdnm,  jointdt
					ORDER BY SUM(qtyorder) DESC";
			//echo $qry;
			$res = $this->getRecordset($qry, NULL, $this->db2);
		}else{
			$res = "No record(s) founded.";
		}
		return $res;
	}
 }


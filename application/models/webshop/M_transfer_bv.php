<?php
class M_transfer_bv extends MY_Model {
	function __construct() {
        // Call the Model constructor
        parent::__construct();	
	}
	
	function checkLastProcessBonus($notrx) {
		$cekTrx = "SELECT MONTH(a.bnsperiod) as monthbonus, YEAR(a.bnsperiod) as yearbonus,
				  CAST(YEAR(a.bnsperiod) AS varchar(4)) +'-'+ CAST(MONTH(a.bnsperiod) as varchar(2)) as fullbns
		           FROM newtrh a WHERE a.trcd = '$notrx'";
		$cekTrxE = $this->getRecordset($cekTrx, NULL, $this->db2);
		if($cekTrxE != null) {
			$qry = "SELECT top 1 MONTH(a.period) as bln, YEAR(a.period) as thn,
				CAST(YEAR(a.period) AS varchar(4)) +'-'+ CAST(MONTH(a.period) as varchar(2)) as fullbns
				FROM HSTSCBNS a
				ORDER BY a.period DESC";
			$arr = $this->getRecordset($qry, NULL, $this->db2);
			$dsf = array(
					"trx" => $cekTrxE,
					"HSTSCBNS" => $arr,
				);
			
			if($arr[0]->fullbns < $cekTrxE[0]->fullbns) {
				return jsonTrueResponse($dsf, "ok");
			} else {
				return jsonFalseResponse("ht :  ".$cekTrxE[0]->fullbns."trx : ".$arr[0]->fullbns."Bonus sudah diproses, tidak bisa update bv..");
			}
		} else {
			return jsonFalseResponse("Transaksi tersebut tidak ada..");
		}	
				
	}
	
	function getPeriodForUpdateBv() {
		$qry = "SELECT currperiod, 
		        currperiodSCO, 
				currperiodBO, 
				currentperiodkcash, 
				CONVERT(VARCHAR(10),update_bv,121) as update_bv
				FROM syspref";
		$arr = $this->getRecordset($qry, NULL, $this->db2);
		return $arr;
	}
	
	function checkSalesMemberByTrx($trxno, $tipe) {
		if($tipe == "1") {
			$qry = "SELECT a.trcd, a.dfno, c.fullnm, a.nbv, a.ndp
					FROM newtrh a 
					INNER JOIN ordtrh b ON (a.trcd = b.trcd)
					LEFT OUTER JOIN msmemb c ON (a.dfno = c.dfno)
					WHERE a.trcd = '$trxno'";
		} else {
			$qry = "SELECT a.trcd, a.dfno, c.fullnm, a.nbv, a.ndp
					FROM newtrh a 
					LEFT OUTER JOIN msmemb c ON (a.dfno = c.dfno)
					WHERE a.trcd = '$trxno'";
		}
		//echo $qry;
	    $arr = $this->getRecordset($qry, NULL, $this->db2);
		return $arr;
	}
	
	
	function updateBV($array) {
		$tglupd = date("Y-m-d");
		$remark = " KNET PREV ".$array['idmember'];
		$tipe = $array['tipetrx'];
		if($tipe == "1") {
			$updNewtrh = "UPDATE NEWTRH 
        	                  set dfno='$array[new_idmember]', 
							  updatenm='$array[updateby]', 
							  updatedt='$tglupd', 
							  remarks=remarks + '$remark',
        	               flag_syn='0' WHERE trcd='$array[notrx]'";
        	 
        	 //echo "$updNewtrh<br>";
			 $hasil1 = $this->executeQuery($updNewtrh, "klink_mlm2010");
			
        	 $updInv = "UPDATE ordtrh 
        	                  set dfno='$array[new_idmember]', 
							  updatenm='$array[updateby]', 
							  updatedt='$tglupd', 
							  remarks=remarks + '$remark'
        	                  WHERE invoiceno='$array[notrx]'";
			 //echo "$updInv<br>";				  
			 $hasil2 = $this->executeQuery($updInv, "klink_mlm2010");
        	 
		} else {
			$updNewtrh = "UPDATE NEWTRH set dfno='$array[new_idmember]', 
			                updatenm='$array[updateby]', 
							updatedt='$tglupd', remarks=remarks + '$remark',
        	               flag_syn='0' WHERE trcd='$array[notrx]'";
        	
        	//echo "$updNewtrh<br>";
			$hasil1 = $this->executeQuery($updNewtrh, "klink_mlm2010");
			
        	$updsc_newtrh = "UPDATE sc_newtrh set dfno='$array[new_idmember]', 
			                updatenm='$array[updateby]', 
							updatedt='$tglupd', remarks=remarks + '$remark'
        	                WHERE trcd='$array[notrx]'";
			//echo "$updsc_newtrh<br>";				
			$hasil2 = $this->executeQuery($updsc_newtrh, "klink_mlm2010");
            
			$checkIfEcomm = "SELECT orderno, id_memb, nmmember 
							 FROM db_ecommerce.dbo.ecomm_trans_hdr a 
							 WHERE a.orderno = '$array[notrx]'";
            $resCheck = $this->getRecordset($checkIfEcomm, NULL, $this->db1);
			 if($resCheck != null) {
				 $insLogKnet = "INSERT INTO knet_bv_update_log (notrx, prev_id, trf_to) 
								VALUES ('$array[notrx]', '$array[updateby]', '$array[new_idmember]')"; 
				 $hasilinsLogKnet = $this->executeQuery($insLogKnet, "klink_mlm2010");				
				
				 $updEcommTranshdr = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr 
									  SET id_memb = '$array[new_idmember]', 
									  nmmember = '$array[new_nmmember]' 
									  WHERE orderno = '$array[notrx]'";			
				 
				 //echo "$updEcommTranshdr<br>";
				 $hasil3 = $this->executeQuery($updEcommTranshdr, "db_ecommerce");
             }
		}
		
		if($hasil1 > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function showFullNameById($param, $idmember) {
			$qry ="SELECT dfno, fullnm, state, tel_hp, status, sfno FROM msmemb
				   WHERE $param = '$idmember'";
			return $this->getRecordset($qry, null, $this->db2);
			
	}

	function checkDownline($id_downline, $id_upline) {
		$this->db = $this->load->database('klink_mlm2010', true);
		$id = strtoupper($id_downline);
		$stored_procedure = "exec klink_mlm2010.[dbo].SP_HILAL_WEB_CHECKUPLINE ?,?";
		//echo $stored_procedure;
		$query = $this->db->query($stored_procedure,array('dfno'=> $id_downline,'login'=> $id_upline));
		$dta = $query->result();
		return $dta;
	}

	function checkValidmember($id) {
		$qry = "SELECT dfno,fullnm,state,tel_hp, status FROM msmemb
				   WHERE dfno = '$id' AND fullnm NOT IN ('TERMINATION', 'RESIGNATION')";
			return $this->getRecordset($qry, null, $this->db2);
	}

	function checkTransaksiBenerNggaPunyaYgLogin($arr) {
		/*$arr = array(
			"updateby" => getUserID(),
			"idmember" => $data['idmember'],
			"nmmember" => $data['nmmember'],
			"new_idmember" => $data['new_idmember'],
			"new_nmmember" => $data['new_nmmember'],
			"notrx" => $data['notrx'],
			"tipetrx" => $data['tipetrx'],
		);*/

		//1 = invoice 2 = stk/knet
		$hasil = null;
		if($arr['tipetrx'] == "1") {
			$qry = "SELECT trcd, dfno FROM newtrh WHERE trcd = '$arr[notrx]' AND dfno = '$arr[updateby]'";
			$hasil = $this->getRecordset($qry, null, $this->db2);
		} else {
			$qry = "SELECT trcd, dfno FROM newtrh WHERE trcd = '$arr[notrx]' AND dfno = '$arr[updateby]'";
			$hasil = $this->getRecordset($qry, null, $this->db2);
		}

		if($hasil != null) {
			return true;
		} else {
			return false;
		}
	}

	function createChaptcha() {
		$vals = array(
	        'img_path'      => './captcha/',
	        //'img_url'       => 'http://www.k-linkmember.co.id/k-commerce/captcha/',
	        'img_url'       => base_url().'captcha/',
	        'img_width'     => '150',
		);
		$cap = $this->antispam->get_antispam_image($vals);
		//$this->db = $this->load->database('local', true);
		//$cap = create_captcha($vals);
		$query = "INSERT INTO captcha (captcha_time, ip_address, word) 
		          VALUES ($cap[time],'".$this->input->ip_address()."','$cap[word]')";
		$this->db->query($query);
		
		//$this->load->database
		return $cap;   		
    }	
	
	function captchaCheck($captcha) {
		$expiration = time()-3600; 
        $ip_address = $this->input->ip_address();
        //$this->db = $this->load->database('local', true);
        $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration); 
        //$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = '$captcha' AND ip_address = '$ip_address' AND captcha_time > $expiration"; 
        $sql = "SELECT * FROM captcha WHERE word = '$captcha' AND captcha_time > $expiration";
        //$binds = array($captcha, $ip_address, $expiration);
        $arr = NULL;
        $query = $this->db->query($sql); 
        if($query->num_rows() > 0)  {
            foreach($query->result() as $dta) { 	
            	$arr = array("response" => TRUE, "word" => $dta->word);
			}   
        } 
        //$this->db->query("DELETE FROM captcha WHERE word = '$captcha'"); 
        return $arr;
	}
}
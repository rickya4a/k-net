<?php
class Shared_module extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

		function show_idmember($idmember)
		{
				//$qry = "SELECT dfno,fullnm,tel_hp,state from klink_mlm2010.[dbo].msmemb where dfno = '$idmember'";
				$qry ="SELECT dfno,fullnm,state,tel_hp FROM msmemb
								WHERE dfno = '$idmember'
								and fullnm not like 'RESIGNATION%'
								and fullnm not like 'TERMINATION%'
								and status = '1'";
				//echo $qry;
				return $this->getRecordset($qry, null, $this->db2);
				//$res = $this->get_result_alternate($qry);
				//return $res;
		}

	function updateBankCodePayment($temp_id, $arr) {
		$this->db = $this->load->database('db_ecommerce', true);
		$upd = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo SET
		          bank_code_payment = ".$arr['bank_code_payment'].", payShip = ".$arr['payShip'].",
		          sentTo = '".$arr['sentTo']."', userlogin = '".$arr['userlogin']."',
		          payAdm = ".$arr['payAdm'].", payConnectivity = ".$arr['payConnectivity']."
		        WHERE orderno = '$temp_id'";
		$upd2 = $this->db->query($upd);
		return $upd;
	}
	
	function updateBankCodePayment2($temp_id, $arr) {
		$this->db = $this->load->database('db_ecommerce', true);
		$upd = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo SET
		          bank_code_payment = ".$arr['bank_code_payment'].", payShip = ".$arr['payShip'].",
		          sentTo = '".$arr['sentTo']."', userlogin = '".$arr['userlogin']."',
		          payAdm = ".$arr['payAdm'].", payConnectivity = ".$arr['payConnectivity'].",
		          discount_shipping = ".$arr['disc_shipping'].",kode_pay= '".$arr['kode_pay']."',
		          kode_ref_bank = '".$arr['kode_ref_bank']."',date_expired= '".$arr['date_expired']."',
		          kode_unik = '".$arr['kode_unik']."',nom_pay = ".$arr['nom_pay']."
		        WHERE orderno = '$temp_id'";

		//echo $upd;
		$upd2 = $this->db->query($upd);
		return $upd;
	}
	
	
	
	function insertVoucherList($arr) {
		$ins = "INSERT INTO ecomm_trans_paydet_sgo (orderno, seqno, paytype, docno, payamt, paystatus, bank_code_payment, charge_admin) VALUES 
		        ('".$arr['orderno']."', ".$arr['seqno'].", '".$arr['paytype']."', 
		        '".$arr['docno']."', ".$arr['payamt'].", 'pending', ".$arr['bank_code_payment'].", ".$arr['charge_admin'].")";
		$exe = $this->executeQuery($ins, $this->db1);
		return $exe;
	}
	
	function updatePaydetSGO($arr) {
		$upd = "UPDATE ecomm_trans_paydet_sgo SET charge_admin = ".$arr['charge_admin'].", 
		          bank_code_payment = ".$arr['bank_code_payment']."
		        WHERE orderno = '".$arr['orderno']."'";
		//echo $upd;
		$exe = $this->executeQuery($upd, $this->db1);
		return $exe;		
	}

	//tambahan transfer ATM manual Vera
	function updateHdrdetSGO($arr) {
		$today=date("Y-m-d H:i:s");
		$tgl_exp = date('Y-m-d H:i:s', strtotime('+4 hour', strtotime($today))); //penambahan 4jam

		$upd = "UPDATE ecomm_trans_hdr_sgo SET payAdm = '".$arr['payAdm']."',kode_pay = '".$arr['kodepay']."',
		          kode_ref_bank = '".$arr['koderef']."',date_expired = '".$tgl_exp."',kode_unik = '".$arr['kodeunik']."'
		          ,nom_pay = '".$arr['nompay']."'
		        WHERE orderno = '".$arr['orderno']."'";
		//echo $upd;
		$exe = $this->executeQuery($upd, $this->db1);
		return $exe;
	}

	function updNewTrh($arr) {
		$today=date("Y-m-d H:i:s");
		$tgl_exp = date('Y-m-d H:i:s', strtotime('+4 hour', strtotime($today))); //penambahan 4jam

		$upd = "UPDATE newtrh SET pay3amt = '".$arr['pay3amt']."',paynote3 = '".$arr['paynote3']."'
		        WHERE trcd= '".$arr['trcd']."'";
		//echo $upd;
		$exe = $this->executeQuery($upd, $this->db2);
		return $exe;
	}
	
	function getDataPaymentSGOByOrderIDDevxxx($orderid) {
		$tglskrg = date("y-m-d");
		$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype = 'vch'
				   ) 
				   AS total_pay,
				   a.payShip - a.discount_shipping as payShip,
				   b.charge_connectivity, b.charge_admin, a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping  
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getDataPaymentSGOByOrderIDDev($orderid) {
		$tglskrg = date("y-m-d");
		 /*$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype = 'vch'
				   ) 
				   AS total_pay,
				   CASE
				      WHEN EXISTS 
				       (
				          SELECT d.id, d.max_discount
				  		  FROM promo_period d 
				          WHERE d.[status] = 1
				          AND d.period_start <= '$tglskrg' AND d.period_end >= '$tglskrg'
				        )
				      THEN a.payShip - a.discount_shipping
				      ELSE a.payShip
				   END as payShip,
				   b.charge_connectivity, b.charge_admin, a.idstk, a.bank_code_payment, a.discount_shipping  
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, a.discount_shipping"; */
		$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and (c.paytype = 'vch' or c.paytype = 'cbp')
				   ) 
				   AS total_pay,
				   a.total_bv, a.payShip - a.discount_shipping as payShip,
				   b.charge_connectivity, 
				   b.charge_admin, a.idstk, 
				   a.bank_code_payment, 
				   b.bankCode, 
				   b.bankDesc, 
				   b.bankDisplayNm, 
				   a.discount_shipping,
				   a.cashback_point  
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, a.total_bv, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping, a.cashback_point";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getDataPaymentSGOByOrderID_withVch($orderid) {
		$qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype != 'sgo'
				   ) 
				   AS total_pay,
				   a.total_bv, a.payShip - a.discount_shipping as payShip,
				   b.charge_connectivity, 
				   b.charge_admin, a.idstk, 
				   a.bank_code_payment, 
				   b.bankCode, 
				   b.bankDesc, 
				   b.bankDisplayNm, 
				   a.discount_shipping,
				   a.cashback_point  
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, a.total_bv, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping, a.cashback_point;";
		
		//echo "$qry";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getDataPaymentSGOByOrderIDMapps($orderid) {
		$tglskrg = date("y-m-d");
		 $qry = "SELECT a.orderno, a.total_pay as tot_prev,
				  (SELECT              
				     CASE 
				       WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) 
				     END          	
				   FROM ecomm_trans_paydet_sgo c          
				   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and (c.paytype = 'cbp'
				   ) 
				   AS total_pay,
				   a.total_bv, a.payShip - a.discount_shipping as payShip,
				   b.charge_connectivity, b.charge_admin, a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping  
				FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
				WHERE a.orderno = '$orderid' 
				GROUP BY a.orderno, a.total_pay, a.total_bv, payShip,  b.charge_connectivity, b.charge_admin,          
				  a.idstk, a.bank_code_payment, b.bankCode, b.bankDesc, b.bankDisplayNm, a.discount_shipping";
		return $this->getRecordset($qry, null, $this->db1);
	}

	function getDataPaymentSGOByOrderID($orderid) {
		//$qry = "SELECT * FROM ecomm_trans_hdr WHERE token = '$orderid'";
		/*$qry = "SELECT a.orderno, a.total_pay, a.payShip, b.charge_connectivity, b.charge_admin, a.idstk, a.bank_code_payment
		        FROM ecomm_trans_hdr_sgo a
		        INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)
		        WHERE a.orderno = '$orderid'"; */
		        
		/*$qry = "SELECT a.orderno, a.total_pay - SUM(C.payamt) as total_pay,  
				   C.paytype, 
				   a.total_pay AS total_wo_vch,         
				   a.payShip, b.charge_connectivity, b.charge_admin,          
				   a.idstk, a.bank_code_payment  
				FROM ecomm_trans_hdr_sgo a  
				INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)      
				LEFT OUTER JOIN ecomm_trans_paydet_sgo c ON (a.orderno = c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS)  
				WHERE a.orderno = '$orderid' AND C.paytype = 'vch' 
				GROUP BY a.orderno, a.total_pay, 
				a.payShip,  b.charge_connectivity, b.charge_admin,          
				a.idstk, a.bank_code_payment, C.paytype";		*/
		
		 $tglskrg = date("y-m-d");
		 $qry = "SELECT a.orderno, a.total_pay as tot_prev,  
						  (SELECT              
						   CASE WHEN sum(c.payamt) IS NULL THEN a.total_pay - 0 ELSE a.total_pay - sum(c.payamt) END          	
						   FROM ecomm_trans_paydet_sgo c          
						   WHERE c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS= A.orderno and c.paytype = 'vch') 
						   AS total_pay, a.payShip, b.charge_connectivity, b.charge_admin, a.idstk, a.bank_code_payment  
						FROM ecomm_trans_hdr_sgo a  INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)  
						WHERE a.orderno = '$orderid' 
						GROUP BY a.orderno, a.total_pay, a.payShip,  b.charge_connectivity, b.charge_admin,          
						  a.idstk, a.bank_code_payment";
		return $this->getRecordset($qry, null, $this->db1);
	}

	function getListCatProd(){
        $qry = "select * 
               from master_prd_cat where status='1' order by cat_id";
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));
    }
	
	function getListCatProd2(){
        $qry = "
			   select cat_id, cat_desc, 'www.k-net.co.id/assets/images/kat/'+ cat_id + '.jpg' as img_url 
               from master_prd_cat where status='1' 
			   ORDER BY (cat_id) ";
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));
    }

	function getBank(){
        $slc = "select * from ecomm_bank WHERE status = '1' AND is_installment = '0' order by bankDesc";

        $result = $this->getRecordset($slc,null,$this->setDB(1));
        return $result;
    }

	function getCCBank($total){
		if($total < 2000000){
			$slc = "select * from ecomm_bank
                WHERE status = '1' AND is_installment = '1' AND min_pay = '0' order by id ASC";
		}else{
			$slc = "select * from ecomm_bank
                WHERE status = '1' AND is_installment = '1' order by id ASC";
		}

		/*echo $slc;
		echo "<br>";
		echo $total;*/
		$result = $this->getRecordset($slc,null,$this->setDB(1));
		return $result;
	}

	function getBank2(){
		$slc = "select * from ecomm_bank WHERE status = '1' AND id !='8' order by bankDesc";

		$result = $this->getRecordset($slc,null,$this->setDB(1));
		return $result;
	}
	
	function getBankNonVABaru(){
        $slc = "select * from ecomm_bank WHERE status = '1' 
		        AND id != 25 AND is_installment = '0'
				order by bankDesc";

        $result = $this->getRecordset($slc,null,$this->setDB(1));
        return $result;
    }
    
    function getPaymentInfoByID($id){
        $slc = "select * from ecomm_bank WHERE id = $id and status = '1' order by bankDesc";
        $result = $this->getRecordset($slc,null,$this->setDB(1));
        return $result;
    }

	function getCOD(){
		$slc = "select * from ecomm_bank
                WHERE id IN (40) order by bankDesc";

		$result = $this->getRecordset($slc,null,$this->setDB(1));
		return $result;
	}

    function getBankJohnDoe(){
        $slc = "select * from ecomm_bank
                WHERE id IN (1, 4, 8, 20, 16, 21, 25,30,31) order by bankDesc";

        $result = $this->getRecordset($slc,null,$this->setDB(1));
        return $result;
    }

	function getBankJohnDoe2(){
		$slc = "select * from ecomm_bank
                WHERE id IN (1, 4, 8 ,20, 16, 21, 25,30,31) order by bankDesc";

		$result = $this->getRecordset($slc,null,$this->setDB(1));
		return $result;
	}

	function getCCJohnDoe($total){
		if($total >= 2000000){
			$slc = "select * from ecomm_bank
                WHERE is_installment = '1' order by id ASC";
		}else{
			$slc = "select * from ecomm_bank
                WHERE is_installment = '1' AND min_pay = '0' order by id ASC";
		}


		$result = $this->getRecordset($slc,null,$this->setDB(1));
		return $result;
	}
	
	function getBankNonVA() {
		$slc = "select * from ecomm_bank
                WHERE id IN (1, 4, 8, 20, 16, 21) order by bankDesc";

        $result = $this->getRecordset($slc,null,$this->setDB(1));
        return $result;
	}
	
	function geListBankVa() {
		$slc = "select * from ecomm_bank_va
                WHERE status = '1'";

        $result = $this->getRecordset($slc,null,$this->setDB(1));
        return $result;
	}

	//Mencari Kodepos / COD
	function getListProvinsiKP() {

		$qry = "SELECT A.kode_provinsi, A.provinsi
				FROM Q_Wil_kodepos A
				GROUP BY A.kode_provinsi, A.provinsi
				ORDER BY A.provinsi";

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showListKotaKP($val) {

		$qry = "SELECT A.kode_kabupaten, A.kabupaten
				FROM Q_Wil_kodepos A where A.kode_provinsi='$val'
				GROUP BY A.kode_kabupaten, A.kabupaten
				ORDER BY A.kabupaten";

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showListKecamatanKP($val) {

		$qry = "SELECT A.kode_kecamatan, A.kecamatan
				FROM Q_Wil_kodepos A where A.kode_kabupaten='$val'
				GROUP BY A.kode_kecamatan, A.kecamatan
				ORDER BY A.kecamatan";

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showListKelurahanKP($val) {

		$qry = "SELECT A.kode_kelurahan, A.kelurahan
				FROM Q_Wil_kodepos A where A.kode_kecamatan='$val'
				GROUP BY A.kode_kelurahan, A.kelurahan
				ORDER BY A.kelurahan";

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function showKodePos($prov,$kota,$kec,$kel) {

		$qry = "SELECT A.kodepos FROM Q_Wil_kodepos A where
				A.kode_provinsi='$prov' AND A.kode_kabupaten='$kota' AND A.kode_kecamatan='$kec' AND A.kode_kelurahan='$kel'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}


	function getListProvinsi2() {
        //$qry = "select * from master_wil_provinsi";
        /*===============================edit hilal @2015-05-19=================================*/
		/*
		 $qry = "SELECT
				  dbo.master_wil_provinsi.kode_provinsi,
				  dbo.master_wil_provinsi.provinsi
				FROM
				  dbo.master_wil_provinsi
				  INNER JOIN dbo.master_wil_kabupaten ON (dbo.master_wil_provinsi.kode_provinsi = dbo.master_wil_kabupaten.kode_provinsi)
				  INNER JOIN dbo.master_wil_kecamatan ON (dbo.master_wil_kabupaten.kode_kabupaten = dbo.master_wil_kecamatan.kode_kabupaten)
				WHERE
				  (dbo.master_wil_kecamatan.kode_kab_JNE is not null)
				GROUP BY dbo.master_wil_provinsi.kode_provinsi,
				dbo.master_wil_provinsi.provinsi
				ORDER BY dbo.master_wil_provinsi.provinsi";
		 */

		$qry = "SELECT A.kode_provinsi, A.provinsi
				FROM V_ECOMM_WILAYAH2 A
				GROUP BY A.kode_provinsi, A.provinsi
				ORDER BY A.provinsi";
		/*===============================end edit hilal @2015-05-19=================================*/

        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

	function listProvinceKGB() {
		$qry = "select a.ProvinsiNama as provinsi
				from master_kgp_wil a
				group by a.ProvinsiNama
				order by a.ProvinsiNama";
		//echo $qry;	
		$res = $this->getRecordset($qry, null, $this->db1);
        return $res;
	}
	
	function listCargoKota($prov) {
		$qry = "select a.KotaKode, a.KotaNama
				from db_ecommerce.dbo.master_kgp_wil a
				where a.ProvinsiNama='$prov' AND a.WaktuTempuh > 0
				order by a.ProvinsiNama, a.KotaNama";
		$res = $this->getRecordset($qry, null, $this->db1);
        return $res;
	}
	
	function listCargoStockist($kota) {
		//EDIT HILAL @2018-01-31
		 /*
		 $qry = "select a.KotaKode, a.loccd, a.fullnm
				from db_ecommerce.dbo.master_kgp_wil a
				where a.KotaKode='$kota'
				order by a.KotaKode, a.loccd";
		 */
		 $qry = "select A.KotaKode, B.loccd, B.fullnm
				from db_ecommerce.dbo.master_kgp_wil a
					 inner join db_ecommerce.dbo.master_wil_stk b on a.id=b.id_kota
				where a.KotaKode='$kota'
				GROUP BY A.KotaKode, B.loccd, B.fullnm
				order by a.KotaKode, b.loccd";
		  
		$res = $this->getRecordset($qry, null, $this->db1);
        return $res;
	}
	
	function setPricecodeKGB($loccd) {
		$qry = "SELECT a.KotaKode, a.loccd, a.fullnm, a.pricecode
				FROM db_ecommerce.dbo.master_kgp_wil a
				WHERE a.KotaKode = '$loccd'";
		$res = $this->getRecordset($qry, null, $this->db1);
        return $res;
	}

	function showListKota2($prov) {
        /*$qry = "SELECT
                  a.provinsi,
                  b.kode_kabupaten,
                  b.kabupaten
                FROM
                  master_wil_provinsi a
                  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
                WHERE
                  a.kode_provinsi = '".$prov."'"; */

        /*===============================edit hilal @2015-05-19=================================*/
        /*$qry = "SELECT
				  a.provinsi,
				  b.kode_kabupaten,
				  b.kabupaten
				FROM
				  master_wil_provinsi a
				  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
				  INNER JOIN master_wil_kecamatan c ON (b.kode_kabupaten = c.kode_kabupaten)
				WHERE
				  c.kode_kab_JNE is not null AND
				  a.kode_provinsi = '".$prov."'
				GROUP BY a.provinsi,
				b.kode_kabupaten, b.kabupaten
				ORDER BY a.provinsi";
		*/
		$qry = "SELECT a.provinsi, a.kode_kabupaten, a.kabupaten
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kab_JNE is not null AND a.kode_provinsi = '".$prov."'
				GROUP BY a.provinsi, a.kode_kabupaten, a.kabupaten
				ORDER BY A.kabupaten";

		/*===============================end edit hilal @2015-05-19=================================*/
		//echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

	function showListKecamatan2($kota) {
    	/*$qry = "SELECT
                b.kode_kecamatan,
                b.kecamatan,
                a.kabupaten
                FROM
                    master_wil_kabupaten a
                    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
                WHERE
                a.kode_kabupaten = '".$kota."'"; */

        /*===============================edit hilal @2015-05-19=================================*/
        /*$qry = "SELECT
					b.kode_kecamatan,
					b.kecamatan,
					a.kabupaten,
				    b.kode_kec_JNE
				FROM
				    master_wil_kabupaten a
				    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
				WHERE
				a.kode_kabupaten = '".$kota."' AND
				b.kode_kec_JNE is not null";
		 */

		 $qry = "SELECT A.kode_kecamatan, A.kecamatan, A.kabupaten, A.kode_kec_JNE
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kabupaten = '".$kota."' AND A.kode_kec_JNE is not null
				GROUP BY A.kode_kecamatan, A.kecamatan, A.kabupaten, A.kode_kec_JNE
				ORDER BY A.kecamatan";
		 /*===============================end edit hilal @2015-05-19=================================*/
		//echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

	function showListKelurahan2($kecamatan) {
    	$this->db = $this->load->database("db_ecommerce", true);
		//remark by DION on 26/04/2018
    	/*$qry = "SELECT A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kecamatan = '".$kecamatan."'";*/
				
		$qry = "SELECT A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan, 
				    A.kode_kec_JNE_Origin,a.state, a.loccd, a.fullnm
				FROM V_ECOMM_WILAYAH3 A
				WHERE A.kode_kecamatan = '".$kecamatan."'
				  AND a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
				  AND (A.kode_kec_JNE_Origin is not null or A.kode_kec_JNE_Origin != '')
				GROUP BY A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan, 
				    A.kode_kec_JNE_Origin,a.state, a.loccd, a.fullnm";
		//echo $qry;
				
		$query = $this->db->query($qry);
		$res = $query->result();
		


		$arr = $valReturn = array(
			  "arrayData" => $res,
			  "listStk" => $res
			);
        return $arr;
		
		
		// Start edit hilal @2019-03-18
		
		/*
		if ($res == null)
        {
            return null;
        }
        else
        {
        	//change V_ECOMM_WILAYAH2 to V_ECOMM_WILAYAH3  by DION on 26/04/2018
            $kec_jne = "SELECT a.KEC_JNE, a.loccd, a.fullnm, A.kode_kec_JNE_Origin,a.state
				FROM V_ECOMM_WILAYAH3 A
				WHERE A.kode_kec_JNE='".$res[0]->kode_kec_JNE."' and a.state ='".$res[0]->state."' 
					  AND a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
					  AND (A.kode_kec_JNE_Origin is not null or A.kode_kec_JNE_Origin != '')
				GROUP BY a.KEC_JNE, a.loccd, a.fullnm, A.kode_kec_JNE_Origin,a.state";
			 //===============================end edit hilal @2015-05-19=================================

			//echo "query 2 = ".$kec_jne;
	        $query2 = $this->db->query($kec_jne);
			$res2 = $query2->result();

			$arr = $valReturn = array(
				  "arrayData" => $res,
				  "listStk" => $res2
				);
	        return $arr;


        }
	*/
		// end edit hilal @2019-03-18
    }

	function showListKecamatanCOD($kecamatan) {
		$this->db = $this->load->database("db_ecommerce", true);

		$qry = "SELECT A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan,
				    A.kode_kec_JNE_Origin,a.state, a.loccd, a.fullnm, a.postcd
				FROM V_ECOMM_WILAYAH3 A
				WHERE A.kode_kecamatan = '".$kecamatan."' AND A.sctype='1'
				  AND a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
				  AND (A.kode_kec_JNE_Origin is not null or A.kode_kec_JNE_Origin != '')
				GROUP BY A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan,
				    A.kode_kec_JNE_Origin,a.state, a.loccd, a.fullnm, a.postcd";
		//echo $qry;

		$query = $this->db->query($qry);
		$res = $query->result();



		$arr = $valReturn = array(
			"arrayData" => $res,
			"listStk" => $res
		);
		return $arr;
	}

	function showCurrentPeriod(){
        //$this->db = $this->load->database('alternate', true);

        /*$qry = "SELECT a.currentperiodkcash as lastperiod,
                DATEADD(month, 1, a.currentperiodkcash) as nextperiod
                from klink_mlm2010.[dbo].syspref a";*/

        $qry = "SELECT CONVERT(VARCHAR(10),a.currentperiodkcash, 21) as lastperiod,
		        CONVERT(VARCHAR(10),a.currentperiodkcash, 103) as lastperiod2,
				a.periodKCashRange as rangeperiod
				from syspref a ORDER BY a.currentperiodkcash DESC";
		//echo "$qry";
        return $this->getRecordset($qry, null, $this->db2);
    }


	function showCurrentPeriod_hilal(){
		//$db = $this->load->database('db_ecommerce', true);

        $qry = "SELECT top 1 CONVERT(varchar, a.bnsperiod, 23) as bnsperiod, 
        					CONVERT(varchar, a.bnsperiod2, 23) as bnsperiod2, 
        					a.range, 
        					CONVERT(varchar, a.endofdatebnsperiod, 23) as endofdatebnsperiod, 
        					CONVERT(varchar, getdate(), 23) as date_now, 
        					DAY(GETDATE()) as date_only_now,
        					CONVERT(varchar, DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0), 23) as bnsperiod_now,
        					CONVERT(varchar, DATEADD(m, DATEDIFF(m, 0, GETDATE()) -1, 0), 23) as bnsperiod_prev
				FROM master_period a
				ORDER BY a.id DESC";
		//echo "$qry";
        return $this->getRecordset($qry, null, $this->db1);
    }

    
	function getAddressReferenceByID($memberid) {
		$db = $this->load->database('db_ecommerce', true);
		$qry = "SELECT * FROM log_dest_address WHERE memberid = '$memberid'";
		$res = $db->query($qry);
		return $res->result();
	 }

	function getMemberInfo($userid) {

	}
}

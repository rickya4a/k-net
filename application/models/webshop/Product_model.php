<?php
class Product_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	
    function getHeaderBanner(){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                    	inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        
        $qry = "select id, hdr_desc, goup_hdr, img_url, hdr_status
                from ecomm_master_header_pic
                WHERE hdr_status='1' order by id desc";
		//echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
	
	function getHeaderBannerCat(){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                    	inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        
        $qry = "select id, hdr_desc, goup_hdr, img_url, hdr_status
                from ecomm_master_header_cat_pic
                WHERE hdr_status='1'";
		//echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
	
    function getProductShop($promo = '0'){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                    	inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        if($promo == '1'){
        	$wherepromo = " and a.prdcdcat<>'13'"; //jika promo, maka bundling tidak disertakan
        }else if($promo == '0'){
        	$wherepromo = " "; //jika reguler, maka bundling tidak disertakan
        }
      
        $qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue
                from V_Ecomm_PriceList a
                WHERE A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null $wherepromo";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function getProductShopByID($id) {
    	$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue
                from V_Ecomm_PriceList a
                WHERE A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null AND a.prdcd = '$id'";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function getProductCat(){
        $qry = "select * from master_prd_cat order by cat_id";
        $res = $this->getRecordset($this->db1, $qry);
        return $res;
    }
	
	function getProdByName($name) {
	    $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue
                from V_Ecomm_PriceList a
                WHERE prdnm LIKE '%$name%' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null";
        return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function getListCatProd(){
        $qry = "select * from master_prd_cat where status='1' order by cat_id";
        //$this->db->last_query($qry);
        return $this->getRecordset($qry, null, $this->setDB(1));
    }

    
    function ListProdByCatHeader($prdCat)
    {       
        $qry = "SELECT * FROM V_Ecomm_PriceList a 
                WHERE a.prdcdcat = '".$prdCat."' AND a.is_starterkit='0'
                order by a.prdnm";
        
        //echo $qry;
        /*$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));
        
    }
	
	function getDetailPrdcdByID($prdcd) {
		$qry = "select a.prdcd, 
				      a.prdnm, 
				      a.prdcdcat, 
				      a.prdnmcatnm,
				      a.img_url, 
				      a.price_w, 
				      a.price_e,
				      a.price_ce,
                      a.price_cw, 
				      a.bv,
				      a.weight,
				      a.is_starterkit,
		              a.is_charge_ship,
		              a.is_discontinue,
                      a.ecomm_status
				from V_Ecomm_PriceList a
				where a.prdcd='$prdcd' AND a.price_w is not null and a.price_e is not null";
		return $this->getRecordset($qry, null, $this->setDB(1));
	}

    function getDetailPrdcdByIDVera($prdcd) {
        $qry = "select a.prdcd,
				      a.prdnm,
				      a.prdcdcat,
				      a.prdnmcatnm,
				      a.img_url,
				      a.price_w,
				      a.price_e,
				      a.price_ce,
                      a.price_cw,
				      a.bv,
				      a.weight,
				      a.is_starterkit,
		              a.is_charge_ship,
		              a.is_discontinue,
                      a.ecomm_status
				from V_Ecomm_PriceList a
				where a.prdcd='HD006B' AND a.price_w is not null and a.price_e is not null";
        return $this->getRecordset($qry, null, $this->setDB(1));
    }
    
    function ListProdByCat($prdCat, $limit, $offset){
    	$first = 0;
		$last = 0;
        if($offset == 0){
            $first = 1;
            $last  = $limit; 
            //echo "test ".$no_page."-".$first."-".$last."<br>";
        }else {
            $xx = ($offset - 1);
            if($xx == 0){
                $xx = 1;
            }
            
            $first = $xx * $limit + 1;
            $last  = $first + ($limit -1);
        }
        
        $qry = "SELECT XX.RowNumber,
                	   XX.prdcd,
                	   XX.prdnm,
                	   XX.prdcdcat,
                       XX.prdnmcatnm,
                	   XX.img_url,
                	   XX.price_w,
                	   XX.price_e,
                	   XX.price_cw,
                	   XX.price_ce,
                	   XX.bv,
                	   XX.weight,
	                   XX.ecomm_status,
	                   XX.is_discontinue
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY a.prdcd desc) as RowNumber,
                        	   A.prdcd,
                    	       A.prdnm,
                    	       A.prdcdcat,
                               a.prdnmcatnm,
                    	       A.img_url,
                    	       A.price_w,
                    	       A.price_e,
                    	       A.price_cw,
                    	       A.price_ce,
                    	       A.bv,
                    	       A.weight,
			                   a.ecomm_status,
			                   a.is_discontinue
                    	FROM V_Ecomm_PriceList a 
                    	WHERE A.prdcdcat = '".$prdCat."' and A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                     ) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
        
        //echo $qry;
        /*$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));
        
    }
    
    function getCountProd(){
        $this->db = $this->load->database('db_ecommerce', true);
        $qry = "select * from V_Ecomm_PriceList";
        $query = $this->db->query($qry);
        $jumlah = $query->num_rows();
        return $jumlah;
    }
    
    function getCountProdByID($id){
        $this->db = $this->load->database('db_ecommerce', true);
        $qry = "select prdcd from V_Ecomm_PriceList a WHERE prdcdcat = '$id' AND a.price_w is not null and a.price_e is not null";
        $query = $this->db->query($qry);
        $jumlah = $query->num_rows();
        return $jumlah;
    }
    
    function getListCurrentPromo($year, $month) {
    	$qry = "SELECT * FROM ecomm_promo_list WHERE year = '$year' AND month = '$month' AND status = '1'";
    	return $this->getRecordset($qry, null, $this->setDB(1));
    }
//CAHYONO
    function ListProdByCahyono1($prdCat, $limit, $offset){
        $first = 0;
        $last = 0;
        if($offset == 0){
            $first = 1;
            $last  = $limit;
            //echo "test ".$no_page."-".$first."-".$last."<br>";
        }else {
            $xx = ($offset - 1);
            if($xx == 0){
                $xx = 1;
            }

            $first = $xx * $limit + 1;
            $last  = $first + ($limit -1);
        }

        $qry = "SELECT XX.RowNumber,
                	   XX.prdcd,
                	   XX.prdnm,
                	   XX.prdcdcat,
                       XX.prdnmcatnm,
                	   XX.img_url,
                	   XX.price_w,
                	   XX.price_e,
                	   XX.price_cw,
                	   XX.price_ce,
                	   XX.bv,
                	   XX.weight,
	                   XX.ecomm_status,
	                   XX.is_discontinue
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY a.prdcd desc) as RowNumber,
                        	   A.prdcd,
                    	       A.prdnm,
                    	       A.prdcdcat,
                               a.prdnmcatnm,
                    	       A.img_url,
                    	       A.price_w,
                    	       A.price_e,
                    	       A.price_cw,
                    	       A.price_ce,
                    	       A.bv,
                    	       A.weight,
			                   a.ecomm_status,
			                   a.is_discontinue
                    	FROM V_Ecomm_PriceList a
                    	WHERE A.prdcd LIKE '%$prdCat%' and A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                     ) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";

        //echo $qry;
        /*$qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));

    }

    function ListProdByCahyono($prdCat)
    {
        /*
		 $qry = "SELECT * FROM V_Ecomm_PriceList a
                WHERE a.prdcd like '%$prdCat%' AND a.is_starterkit='0'
                order by a.prdnm";
		 */
		 $qry = "SELECT * FROM V_Ecomm_PriceList a
                WHERE a.prdcd like '$prdCat' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                order by a.prdnm";

        //echo $qry;
        /*$qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));

    }

    //vera
    function getProductShopVera(){
        $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight,
                    a.isheader,
                    a.isdetail
                from pricelist_vera a where isheader='1' /*AND isdetail='0'*/ AND status='1'/*prdcd='BIOP06' OR prdcd='VRA01'*/ order by a.prdcd ASC";
        //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraD1(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a 
                where SUBSTRING(prdcd,1,4)='HC07' and a.status='1' 
                order by a.prdcd ASC";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraD2(){
        /*$qry = "select DISTINCT(SUBSTRING(prdcd,6,1)) as ukuran
                from pricelist_vera  where SUBSTRING(prdcd,1,4)='HC05'
				group by SUBSTRING(prdcd,6,1) order by SUBSTRING(prdcd,6,1) DESC";*/
        $qry = "select DISTINCT(ukuran) as ukuran,prefix,end_prefix, ukuran_desc 
        		from pricelist_vera
				where prdcd LIKE '%HC05%' and status='1' 
				group by ukuran,ukuran_desc,prefix,end_prefix 
				order by ukuran DESC";

        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraD3(){
        /*$qry = "select DISTINCT(SUBSTRING(prdcd,8,2)) as warna
                from pricelist_vera  where SUBSTRING(prdcd,1,4)='HC05'
				group by SUBSTRING(prdcd,8,2) order by SUBSTRING(prdcd,8,2) ASC";*/
        $qry = "select DISTINCT(warna) as warna, prefix,end_prefix, id_warna 
        		from pricelist_vera
				where prdcd LIKE '%HC05%' and status='1' 
				group by warna,prefix,end_prefix,id_warna 
				order by warna ASC";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProdDet($val){
        /*$qry = "SELECT * FROM V_Ecomm_PriceList a
                WHERE a.prdcd = '$val' AND a.is_starterkit='0'";*/
        $qry = "SELECT * FROM pricelist_vera a
                WHERE a.prdcd = '$val' and status='1' ";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

   
}
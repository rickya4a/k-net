<?php
class klaim_model extends MY_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getListingKlaim($idmember){
        $qry = "
                SELECT NoTrxV, VoucherNo, dfno, Fullnm, loccd, nmSTK, jmlPaket_Ladies, jmlPaket_Umum, ttbv, bnsperiod, Claim_Status
                FROM QTW_KLPromo_Oct17
                WHERE dfno = '".$idmember."'
                GROUP BY NoTrxV, VoucherNo, dfno, Fullnm, loccd, nmSTK, jmlPaket_Ladies, jmlPaket_Umum, ttbv,bnsperiod, Claim_Status";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getListingDetail($vc){
        $qry = "
                SELECT * FROM QTW_KLPromo_Oct17 where VoucherNo = '$vc';";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getProdukKlaim($bnsperiod,$idmembers){
        $qry = "
                SELECT *
                FROM TWA_KLPromo_Oct17_MH A
                LEFT JOIN TWA_KLPromo_Oct17_MD B
                ON A.bnsperiod=B.bnsperiod AND A.dfno=  B.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
                LEFT JOIN msprd c
                on b.prdcd=c.prdcd
                WHERE A.dfno = '$idmembers' AND a.bnsperiod ='$bnsperiod' ;

                ";
//        echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }


    function getFormDetail($vc){
        $qry = "
                SELECT * FROM TWA_KLPromo_Oct17_H where VoucherNo = '$vc';";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getReportBonus($idmember,$year){
        $slc = "SELECT a.distributorcode, a.bonusyear, a.bonusmonth, 
						   isnull(a.netincome, 0) as netincome, 
					       isnull(a.totBonus, 0) as totBonus, 
					       isnull(a.tax, 0) as tax, 
					       isnull(a.ar_total, 0) as ar_total, 
					       isnull(a.totBonus_Nett, 0) as  totBonus_Nett 
			       FROM V_HILAL_TOTBONUS_NETT A
                    WHERE A.distributorcode='$idmember' AND A.bonusyear=$year
                    ORDER BY bonusmonth";
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
    }
    function showTableHilal($idmember,$month,$year){
        $qry = "SELECT * FROM tbonus_hilal_nett A WHERE A.dfno='$idmember' AND A.bonusmonth=$month AND A.bonusyear=$year;";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }
    function getListingstokis(){
        $qry = "select a.loccd, a.fullnm, a.sctype, a.scstatus, b.[password]
				from klink_mlm2010.dbo.mssc a
					 inner join klink_mlm2010.dbo.sc_users b on a.loccd=b.username
				where a.sctype in ('1','2') and a.fullnm not like '%TERMINAT%' AND A.scstatus='1'
				ORDER BY A.sctype, A.fullnm;";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }



    function cek_seQ($tipe_pay)
    {
        $this->db = $this->load->database('klink_mlm2010', true);
        $y1=date("y");
        $m=date("m");

        $this->db->trans_begin();

        //if(in_array('p',$tipe_pay))
        if($tipe_pay == 'pv')
        {
            $tbl = "SEQ_PV"."$y1"."$m";
        }
        else if($tipe_pay == 'cv')
        {
            //$tbl = "SEQ_CV1703";
            $tbl = "SEQ_CV"."$y1"."$m";
        }else{
            $tbl = "SEQ_ID"."$y1"."$m";
        }

        $cek = "select * from $tbl";

        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('a')";
            $query = $this->db->query($input);
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('a')";
            $query = $this->db->query($input);
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

        return $query;
    }

    function get_idno($tipe_pay)
    {
        $this->db = $this->load->database('klink_mlm2010', true);
        $y1=date("y");
        $m=date("m");

        $this->db->trans_begin();

        //if(in_array('p',$tipe_pay))
        if($tipe_pay == 'pv' || $tipe_pay == 'pc'){
            $tbl = "SEQ_PV"."$y1"."$m";
        }else if($tipe_pay == 'cv'){
            $tbl = "SEQ_CV"."$y1"."$m";
        }else{
            $tbl = "SEQ_ID"."$y1"."$m";
        }

        $qry = "SELECT * FROM $tbl
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";

        $query = $this->db->query($qry);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }
        }
        $jumlah = $query->num_rows();

        $next_seq = sprintf("%06s",$ss);
        $prefix = date('ym');

        if($tipe_pay == 'pv')
            //if(in_array('10',$tipe_pay))
        {
            $y =  strval("PV".$prefix.$next_seq);
        }else if($tipe_pay == 'pc'){
            //$y =  strval("CV17050004TEST");
            $y =  strval("PC".$prefix.$next_seq);
        }
        else if($tipe_pay == 'cv'){
            //$y =  strval("CV17050004TEST");
            $y =  strval("CV".$prefix.$next_seq);
        }else{
            $y =  strval("ID".$prefix.$next_seq);
        }
        /*echo "<br>";
        echo $y;
        echo "<br>";*/

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

        return $y;
    }


    function Submit($vc,$st,$dfno){
        $data['cek_seQ'] = $this->cek_seQ("pc");
        $d = $this->get_idno("pc");
        echo $d;
        $slc = "SELECT * FROM mssc where loccd = '$st'";
        $resx = $this->getRecordset($slc,null,$this->db2);

        foreach ($resx as $data) {
            $fullnm = $data->fullnm;
            $pricecodePvr = $data->pricecodePvr;
            $loccd = $data->loccd;
        }

        $slc = "SELECT * FROM msmemb where dfno = '$dfno'";
        $resx = $this->getRecordset($slc,null,$this->db2);

        foreach ($resx as $data) {
            $fullnm2 = $data->fullnm;
        }
        $slc = "SELECT MONTH(bnsperiod) as bulan,YEAR(bnsperiod) as year,* FROM TWA_KLPromo_Oct17_H where Voucherno = '$vc'";
        $resx = $this->getRecordset($slc,null,$this->db2);

        foreach ($resx as $data) {
            $bonusperiod=  str_pad($data->bulan,2,'0',STR_PAD_LEFT)."/".$data->year;
        }

//


        $upd = "UPDATE TWA_KLPromo_Oct17_H SET Claim_status =  1, Claim_dt=GETDATE(), Claim_loccd= '$loccd', Claim_STK = '$fullnm'  WHERE VoucherNo = '$vc' ";

        $this->executeQuery($upd, $this->db2);

        $db_qryx = $this->load->database('db_ecommerce', true);

        $db_qryx->trans_begin();

        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm,
                             bank_code_payment, payConnectivity)
			          values('".$d."','','','".$dfno."','".$fullnm2."',
			                    '0','0','".$pricecodePvr."','".$bonusperiod."',GETDATE(), '".$loccd."','".$fullnm."','0','W','1',
			                    '0','','0','0',
			                    '0', '0' )";
        //echo "<br /><br />".$insHeader."<br>";
        //$exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));
        $db_qryx->query($insHeader);

        $slc = "SELECT * FROM TWA_KLPromo_Oct17_D where Voucherno = '$vc'";
        $resxx = $this->getRecordset($slc,null,$this->db2);
        $arr = array();
        $i=0;
        $count = 0;

        foreach($resxx as $row) {

            $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
              		        values('". $d."','".$row->prdcd."','".$row->prdnm."',".$row->qtyord.",".$row->bv.",'0','".$pricecodePvr."','')";
            $db_qryx->query($inputProd);
        }


        $insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,
		                     docno,payamt,notes,paystatus)
						values('".$d."','1','sgo','','0','pending','pending')";
        $db_qryx->query($insPaydet);



        if ($db_qryx->trans_status() === FALSE) {
            $db_qryx->trans_rollback();
            return false;
        } else {
            $db_qryx->trans_commit();
            return true;
        }


//        $conn  = $this->load->database("db_ecommerce", TRUE);
//
//        $slc = "INSERT INTO ecomm_trans_hdr_sgo (order_no, idmemb)";
//        $resx = $conn->query($slc);

        //   return true;

    }


}
<?php
class Ecomm_trans_model extends MY_Model {
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
	
	function randomChars($length){
        
       $karakter= 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789';
       $string = ''; 
       for ($i = 0; $i < $length; $i++) 
       { 
            $pos = rand(0, strlen($karakter)-1); 
            $string .= $karakter{$pos}; 
       } 
       return $string; 
    }
	
	//$trcd = no IDEC
	//$orderid = no EM/EC/RM dll
	function insertTrxTblEcomm($order_id, $trcd, $res){
		$execQuery = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		$secno = $this->randomChars(5);
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $execQuery->query($qry);
		foreach($query->result() as $row) {
			$digit_orderno = substr($order_id, 0, 4);
			//JIKA BUKAN TRANSAKSI DTC/TICKET, MAKA AKAN DI BUATKAN CN/KW
			if($digit_orderno != "IDTA") {
				//prepare array for ecomm_trans_hdr
				$ecomm_trans_hdr = array(
					"orderno" => $trcd,
					"bankaccno" => $trcd,
					"token" => $trcd,
					"id_memb" => $trcd,
					"nmmember" => $trcd,
					"total_pay" => $trcd,
					"total_bv" => $trcd,
					"pricecode" => $trcd,
					"bonusmonth" => $trcd,
					"datetrans" => $trcd,
					"idstk" => $trcd,
					"nmstkk" => $trcd,
					"secno" => $trcd,
					"flag_trx" => $trcd,
					"sentTo" => $trcd,
					"status_vt_pay" => $trcd,
					"status_vt_reject_dt" => $trcd,
					"payShip" => $trcd,
					"payAdm" => $trcd, 
					"is_ship" => $trcd, 
					"bank_code_payment" => $trcd, 
					"payConnectivity" => $trcd, 
					"is_login" => $trcd, 
					"totPayCP" => $trcd,
					"id_lp" => $trcd, 
					"free_shipping" => $trcd, 
					"discount_shipping" => $trcd, 
					"cashback_point" => $trcd, 
					"no_hp_konfirmasi" => $trcd
				);
			}
		}	
	}
}
<?php

    class M_heboh_banget extends MY_Model{

    public function __construct(){

        parent::__construct();
    }

    public function getKupon($dfno){

//        $qry = "SELECT b.fullnm, a.dfno, CONVERT(VARCHAR(10), a.bnsperiod, 103) as bnsperiod, x.total
//                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
//                LEFT JOIN msmemb b
//                on a.dfno = b.dfno
//                LEFT JOIN
//                (SELECT b.dfno, COUNT(*) as total
//                 FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
//                 LEFT JOIN msmemb b
//                 on a.dfno = b.dfno
//                 GROUP BY b.dfno) x
//                ON x.dfno=b.dfno
//                GROUP BY b.fullnm, a.dfno, bnsperiod, x.total";
//        $qry = "SELECT b.fullnm, a.dfno, CONVERT(VARCHAR(10), a.bnsperiod, 103) as bnsperiod, x.total
//                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
//                LEFT JOIN msmemb b on a.dfno = b.dfno
//                LEFT JOIN
//        (SELECT b.dfno, COUNT(*) as total
//                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
//                LEFT JOIN msmemb b on a.dfno = b.dfno
//                GROUP BY b.dfno) x ON x.dfno=b.dfno
//                where bnsperiod >= '2018/08/01'
//                GROUP BY b.fullnm, a.dfno, bnsperiod, x.total
//                order by bnsperiod";
        $qry = "SELECT b.fullnm, a.dfno, CONVERT(VARCHAR(10), a.bnsperiod, 103) as bnsperiod, x.total
                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                LEFT JOIN msmemb b on a.dfno = b.dfno
                LEFT JOIN
        (SELECT b.dfno, COUNT(*) as total
                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                LEFT JOIN msmemb b on a.dfno = b.dfno
                GROUP BY b.dfno) x ON x.dfno=b.dfno
                where a.bnsperiod >= '2018/12/01' and a.dfno = '$dfno'
                GROUP BY b.fullnm, a.dfno, bnsperiod, x.total
                order by bnsperiod
                ";
        //echo $qry;
        $arr = $this->getRecordset($qry, NULL, $this->db2);

        return $arr;

    }

    public function getDetail($dfno){

        $qry = "select dfno, no_undian, CONVERT(VARCHAR(10), bnsperiod, 103) as bnsperiod, flag, fullnm
                from TWA_NONSUPLEMEN_UNDIAN where dfno = '$dfno'";
        //echo $qry;
        $arr = $this->getRecordset($qry, NULL, $this->db2);

        return $arr;
    }

    public function getDetailHdr($dfno){

        $sql = "select fullnm,CONVERT(VARCHAR(10), bnsperiod, 103) as bnsperiod
                from TWA_NONSUPLEMEN_UNDIAN where dfno = '$dfno'
                GROUP BY dfno,fullnm,bnsperiod
                ";
        $arr = $this->getRecordset($sql, NULL, $this->db2);

        return $arr;
    }

}
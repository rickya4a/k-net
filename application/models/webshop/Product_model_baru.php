<?php
class Product_model_baru extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->driver('cache', array('adapter' => 'redis', 'backup' => 'file'));
		
    }
	
	
    function getHeaderBanner(){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                    	inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        
        $qry = "select id, hdr_desc, goup_hdr, img_url, hdr_status
                from ecomm_master_header_pic
                WHERE hdr_status='1' order by id desc";
		//echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
	
function getHeaderBannerCache(){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                        inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        
        $qry = "select id, hdr_desc, goup_hdr, img_url, hdr_status
                from ecomm_master_header_pic
                WHERE hdr_status='1' order by id desc";
        //echo $qry;
        $res = $this->getRecordsetCache($qry, null, $this->db1);
       $this->cache->save('cache:mainbanner', $res, -1);
        
        return $res;
    }

	function getHeaderBannerCat(){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                    	inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        
        $qry = "select id, hdr_desc, goup_hdr, img_url, hdr_status
                from ecomm_master_header_cat_pic
                WHERE hdr_status='1'";
		//echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
	
    function getProductShop($promo = '0'){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                    	inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        if($promo == '1'){
        	$wherepromo = " and a.prdcdcat<>'13'"; //jika promo, maka bundling tidak disertakan
        }else if($promo == '0'){
        	$wherepromo = " "; //jika reguler, maka bundling tidak disertakan
        }
        $qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue,
                    a.max_order
                from V_Ecomm_PriceList_Baru a
                WHERE A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null $wherepromo";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function getProductShopCache($promo = '0'){
        /*$qry = "select a.cat_inv_id,a.cat_inv_desc,a.img_name,b.pricecode,b.dp
                    from master_prd_cat_inv a
                        inner join master_prd_pricetab b on a.cat_inv_id = b.cat_inv_id
                    order by a.cat_inv_id";*/
        if($promo == '1'){
            $wherepromo = " and a.prdcdcat<>'13'"; //jika promo, maka bundling tidak disertakan
        }else if($promo == '0'){
            $wherepromo = " "; //jika reguler, maka bundling tidak disertakan
        }
        $qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue,
                    a.max_order
                from V_Ecomm_PriceList_Baru a
                WHERE A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null $wherepromo";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        $this->cache->save('cache:prod', $res, -1);
        return $res;
    }

    function getProductShopByID($id) {
    	$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue
                from V_Ecomm_PriceList_Baru a
                WHERE A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null AND a.prdcd = '$id'";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function getProductCat(){
        $qry = "select * from master_prd_cat order by cat_id";
        $res = $this->getRecordset($this->db1, $qry);
        return $res;
    }
	
	function getProdByName($name) {
	    $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue,
                    a.max_order
                from V_Ecomm_PriceList_Baru a
                WHERE prdnm LIKE '%$name%' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null";
        return $this->getRecordset($qry, null, $this->setDB(1));
	}

    function getProdByNameCache($name) {

        $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight,
                    a.ecomm_status,
                    a.is_discontinue,
                    a.max_order
                from V_Ecomm_PriceList_Baru a
                WHERE prdnm LIKE '%$name%' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null";
         $res = $this->getRecordsetCache($qry, null, $this->db1);
         $this->cache->redis->hset('cache:prodbyname', $name, $res);
        return $res;
    }

	function getListCatProd(){
        $qry = "select * from master_prd_cat where status='1' order by cat_id";
        //$this->db->last_query($qry);
        return $this->getRecordset($qry, null, $this->setDB(1));
    }

    function getListCatProdCache(){
        $qry = "select * from master_prd_cat where status='1' order by cat_id";
        //$this->db->last_query($qry);
        $res = $this->getRecordsetCache($qry, null, $this->setDB(1));
        $this->cache->save('cache:prodcategory', $res, -1);
        return $res;
    }
    
    function ListProdByCatHeader($prdCat)
    {       
        $qry = "SELECT * FROM V_Ecomm_PriceList_Baru a 
                WHERE a.prdcdcat = '".$prdCat."' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                order by a.prdnm";
        
        //echo $qry;
        /*$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Baru a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));
        
    }

     function ListProdByCatHeaderCache($prdCat)
    {       
        $qry = "SELECT * FROM V_Ecomm_PriceList_Baru a 
                WHERE a.prdcdcat = '".$prdCat."' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                order by a.prdnm";
        
        //echo $qry;
        /*$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Baru a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        $res = $this->getRecordsetCache($qry, null, $this->setDB(1));
        $this->cache->redis->hset('cache:prodbycatheader', $prdCat, $res);
        return $res;
        
    }
	
	function getDetailPrdcdByID($prdcd) {
		$qry = "select a.prdcd, 
				      a.prdnm, 
				      a.prdcdcat, 
				      a.prdnmcatnm,
				      a.img_url, 
				      a.price_w, 
				      a.price_e,
				      a.price_ce,
                      a.price_cw, 
				      a.bv,
				      a.weight,
				      a.is_starterkit,
		              a.is_charge_ship,
		              a.is_discontinue,
                      a.ecomm_status,
                      a.max_order
				from V_Ecomm_PriceList_Baru a
				where a.prdcd='$prdcd' AND a.price_w is not null and a.price_e is not null";
		return $this->getRecordset($qry, null, $this->setDB(1));
	}
    
    function ListProdByCat($prdCat, $limit, $offset){
    	$first = 0;
		$last = 0;
        if($offset == 0){
            $first = 1;
            $last  = $limit; 
            //echo "test ".$no_page."-".$first."-".$last."<br>";
        }else {
            $xx = ($offset - 1);
            if($xx == 0){
                $xx = 1;
            }
            
            $first = $xx * $limit + 1;
            $last  = $first + ($limit -1);
        }
        
        $qry = "SELECT XX.RowNumber,
                	   XX.prdcd,
                	   XX.prdnm,
                	   XX.prdcdcat,
                       XX.prdnmcatnm,
                	   XX.img_url,
                	   XX.price_w,
                	   XX.price_e,
                	   XX.price_cw,
                	   XX.price_ce,
                	   XX.bv,
                	   XX.weight,
	                   XX.ecomm_status,
	                   XX.is_discontinue
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY a.prdcd desc) as RowNumber,
                        	   A.prdcd,
                    	       A.prdnm,
                    	       A.prdcdcat,
                               a.prdnmcatnm,
                    	       A.img_url,
                    	       A.price_w,
                    	       A.price_e,
                    	       A.price_cw,
                    	       A.price_ce,
                    	       A.bv,
                    	       A.weight,
			                   a.ecomm_status,
			                   a.is_discontinue
                    	FROM V_Ecomm_PriceList_Baru a 
                    	WHERE A.prdcdcat = '".$prdCat."' and A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                     ) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";
        
        //echo $qry;
        /*$qry = "select a.prdcd, 
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Baru a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));
        
    }
    
    function getCountProd(){
        $this->db = $this->load->database('db_ecommerce', true);
        $qry = "select * from V_Ecomm_PriceList_Baru a WHERE a.price_w is not null and a.price_e is not null";
        $query = $this->db->query($qry);
        $jumlah = $query->num_rows();
        return $jumlah;
    }
    
    function getCountProdByID($id){
        $this->db = $this->load->database('db_ecommerce', true);
        $qry = "select prdcd from V_Ecomm_PriceList_Baru a WHERE prdcdcat = '$id' AND a.price_w is not null and a.price_e is not null";
        $query = $this->db->query($qry);
        $jumlah = $query->num_rows();
        return $jumlah;
    }
    
    function getListCurrentPromo($year, $month) {
    	$qry = "SELECT * FROM ecomm_promo_list WHERE year = '$year' AND month = '$month' AND status = '1'";
    	return $this->getRecordset($qry, null, $this->setDB(1));
    }
//CAHYONO
    function ListProdByCahyono1($prdCat, $limit, $offset){
        $first = 0;
        $last = 0;
        if($offset == 0){
            $first = 1;
            $last  = $limit;
            //echo "test ".$no_page."-".$first."-".$last."<br>";
        }else {
            $xx = ($offset - 1);
            if($xx == 0){
                $xx = 1;
            }

            $first = $xx * $limit + 1;
            $last  = $first + ($limit -1);
        }

        $qry = "SELECT XX.RowNumber,
                	   XX.prdcd,
                	   XX.prdnm,
                	   XX.prdcdcat,
                       XX.prdnmcatnm,
                	   XX.img_url,
                	   XX.price_w,
                	   XX.price_e,
                	   XX.price_cw,
                	   XX.price_ce,
                	   XX.bv,
                	   XX.weight,
	                   XX.ecomm_status,
	                   XX.is_discontinue
                FROM(
                    	SELECT ROW_NUMBER() OVER (ORDER BY a.prdcd desc) as RowNumber,
                        	   A.prdcd,
                    	       A.prdnm,
                    	       A.prdcdcat,
                               a.prdnmcatnm,
                    	       A.img_url,
                    	       A.price_w,
                    	       A.price_e,
                    	       A.price_cw,
                    	       A.price_ce,
                    	       A.bv,
                    	       A.weight,
			                   a.ecomm_status,
			                   a.is_discontinue
                    	FROM V_Ecomm_PriceList_Baru a
                    	WHERE A.prdcd LIKE '%$prdCat%' and A.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                     ) XX
                WHERE XX.RowNumber BETWEEN ".$first." AND ".$last."";

        //echo $qry;
        /*$qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Baru a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));

    }

    function ListProdByCahyono($prdCat)
    {
        /*
		 $qry = "SELECT * FROM V_Ecomm_PriceList_Baru a
                WHERE a.prdcd like '%$prdCat%' AND a.is_starterkit='0'
                order by a.prdnm";
		 */
		 $qry = "SELECT * FROM V_Ecomm_PriceList_Baru a
                WHERE a.prdcd like '$prdCat' AND a.is_starterkit='0' AND a.price_w is not null and a.price_e is not null
                order by a.prdnm";

        //echo $qry;
        /*$qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Baru a
                where a.prdcdcat = '".$prdCat."'";*/
        //echo $qry;
        return $this->getRecordset($qry, null, $this->setDB(1));

    }

    //vera
    function getProductShopVera(){
        $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight,
                    a.isheader,
                    a.isdetail
                from pricelist_vera a where isheader='1' /*AND isdetail='0'*/ AND status='1'/*prdcd='BIOP06' OR prdcd='VRA01'*/ order by a.prdcd ASC";
        //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        $this->cache->save('cache:prodvera', $res, -1);
        return $res;
    }

    function getProductShopVera1(){
        $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.price_cw,
                    a.price_ce,
                    a.bv,
                    a.weight,
                    a.isheader,
                    a.isdetail
                from pricelist_vera a where isdetail='0'
                /*AND isdetail='0'*/ AND status='1'/*prdcd='BIOP06' OR prdcd='VRA01'*/ order by a.prdcd ASC";
        //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        $this->cache->save('cache:prodvera1', $res, -1);
        return $res;
    }

    function getProductShopVeraD1(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a 
                where SUBSTRING(prdcd,1,4)='HC07' and a.status='1' and prdcd_hdr='VRA01'
                order by a.prdcd ASC";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraD2(){
        /*$qry = "select DISTINCT(SUBSTRING(prdcd,6,1)) as ukuran
                from pricelist_vera  where SUBSTRING(prdcd,1,4)='HC05'
				group by SUBSTRING(prdcd,6,1) order by SUBSTRING(prdcd,6,1) DESC";*/
        $qry = "select DISTINCT(ukuran) as ukuran,prefix,end_prefix, ukuran_desc 
        		from pricelist_vera
				where prdcd LIKE '%HC05%' and status='1' 
				group by ukuran,ukuran_desc,prefix,end_prefix 
				order by ukuran DESC";

        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraD3(){
        /*$qry = "select DISTINCT(SUBSTRING(prdcd,8,2)) as warna
                from pricelist_vera  where SUBSTRING(prdcd,1,4)='HC05'
				group by SUBSTRING(prdcd,8,2) order by SUBSTRING(prdcd,8,2) ASC";*/
        $qry = "select DISTINCT(warna) as warna, prefix,end_prefix, id_warna 
        		from pricelist_vera
				where prdcd LIKE '%HC05%' and status='1' 
				group by warna,prefix,end_prefix,id_warna 
				order by warna ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }


    function getProductShopVeraP1(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where SUBSTRING(prdcd,1,7)='IDBC014' and SUBSTRING(prdcd,10,1) !='F'and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraP2(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where SUBSTRING(prdcd,1,7)='IDBC014' and SUBSTRING(prdcd,10,1)='F' and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraP3(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where SUBSTRING(prdcd,1,7)='IDBC016' and end_prefix='CP' and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraP4(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where SUBSTRING(prdcd,1,7)='IDBC016' and end_prefix='LP' and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraP5(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where SUBSTRING(prdcd,1,7)='IDBC015' and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraP6(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where prdcd='IDBC017BRF' and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraP7(){
        $qry = "select a.prdcd,a.warna,a.ukuran
                from pricelist_vera a
                where prdcd='IDBC017PC' and a.status='1' and prdcd_hdr='VRA03'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraT1(){
        $qry = "select *
                from pricelist_vera a
                where prdcd='TAN2019' and a.status='1' and prdcd_hdr='VRA02'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProductShopVeraT2(){
        $qry = "select *
                from pricelist_vera a
                where prdcd='ANNIV2019SHIRT' and a.status='1' and prdcd_hdr='VRA02'
                order by a.prdcd ASC";
        $res = $this->getRecordsetCache($qry, null, $this->db1);
        return $res;
    }

    function getProdDet($val){
        /*$qry = "SELECT * FROM V_Ecomm_PriceList_Baru a
                WHERE a.prdcd = '$val' AND a.is_starterkit='0'";*/
        $qry = "SELECT * FROM pricelist_vera a
                WHERE a.prdcd = '$val' and status='1' ";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProdDetFlora($value1,$value2,$value3,$value4,$value5,$value6,$value7){
        $qry = "	SELECT * FROM pricelist_vera a
                WHERE (a.child1 = '$value1' and a.child2 = '$value2' OR
				 a.child2 = '$value1' and a.child1 = '$value2') AND
                 a.child3 = '$value3' and a.child4 = '$value4' AND
                 a.child5 = '$value5' AND a.child6 = '$value6' AND
                 a.child7 = '$value7' and status='1' ";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProdDetFlora2($value1,$value2,$value3,$value4){
        $qry = "SELECT * FROM pricelist_vera a
                WHERE a.child1 = '$value1' AND a.child2=''
                AND a.child3 = '$value2' AND a.child4=''
                AND a.child5 = '$value3' AND a.child6=''
                AND a.child7 = '$value4' AND status='1' ";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function getProdDetAnniv2019($value1,$value2){
        $qry = "	SELECT * FROM pricelist_vera a
                WHERE (a.child1 = '$value1' and a.child2 = '$value2') OR
				(a.child2 = '$value1' and a.child1 = '$value2') and status='1' ";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
}
<?php 
                        
class PromoApr2019 extends MY_Model {
                        
    public function checkPromoApril($data) {                    
        /*$qry = "SELECT * FROM promo_bangkok_april_prd a 
                WHERE a.[status] = '1' and prdcd = '$prdcd' AND min_qty <= $qty";
        //echo $qry;        
        $result = $this->getRecordset($qry, NULL, $this->db1); 
        if($result != null) {
            return 1;
        } else {
            return 0;
        }     */       

        $validPromo = 0;
        $jum = count($data['prdcd']);
		for($i = 0; $i < $jum; $i++) {
			$qry = "SELECT * FROM promo_bangkok_april_prd a 
                WHERE a.[status] = '1' and prdcd = '".$data['prdcd'][$i]."' AND min_qty <= ".$data['qty'][$i]." 
                	AND (typOfPromo = null or typOfPromo = '' or typOfPromo = null or typOfPromo = 'or')";
            //echo $qry;        
            $result = $this->getRecordset($qry, NULL, $this->db1); 

            if($result != null) {
                $validPromo = 1;
                break;
            }  
        }
        return $validPromo;
    }

    public function checkPromoAprilAnd() {   
		$qry = "SELECT * FROM promo_bangkok_april_prd a 
            	WHERE a.[status] = '1' AND (typOfPromo = 'and')";
        //echo $qry;        
        $result = $this->getRecordset($qry, NULL, $this->db1); 
        return $result;
    }


    public function getTotalBvandPrice($data) {
        $jum = count($data['prdcd']);
        $totalBv = 0;
        $totalPrice = 0;
        //echo "<pre>";
        //print_r($data);
        //echo "</pre>";
		for($i = 0; $i < $jum; $i++) {
            $qty = (int) $data['qty'][$i];
            $bv = (int) $data['bv'][$i];
            $totalBv +=  $qty * $bv;
            //$totalBv = $data['qty'][$i] * $data['bv'][$i];    
        }    
        return $totalBv;
    }
                        
    public function getProdukSpecialOffer() {
        $qry = "SELECT * FROM V_Ecomm_PriceList_Dion_Baru a 
                WHERE a.prdcd = 'HF011SPC'
                AND a.price_w is not null and a.price_e is not null";
        $result = $this->getRecordset($qry, NULL, $this->db1); 
        return $result;        
    }                        
                        
}
                        
/* End of file webshop/promoApr2019.php.php */
    
                        

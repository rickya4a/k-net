<?php
class M_api_sms extends MY_Model {
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
	
	function sendSMSMember($id) {
		$qry = "SELECT notelp, berita 
				FROM TWA_SMSMEMB a 
				WHERE a.trxid = $id";
		$result = $this->getRecordset($qry, null, "data_mining");
		return $result;
	}
	
	function updateFlagSMS($id, $flag) {
		$qry = "UPDATE TWA_SMSMEMB 
				SET flagstatus = '$flag'
				WHERE trxid = $id";
		$result = $this->executeQuery($qry, "data_mining");
		return $result;
	}
	
}
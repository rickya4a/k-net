<?php
class Payment_model extends MY_Model {
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /*QUERY-QUERY TRANSACTIOBN*/
    function cek_seQ(){
        date_default_timezone_set("Asia/Jakarta");
        $y1=date("Y");
        $m=date("m");

        $tbl = "ecomm_"."$y1"."$m";

        $cek = "select * from $tbl";

        $query = $this->executeQuery($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input);
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input);
            //echo "<br>ada<br>";
        }
    }

    function getOrderno(){
        //$this->db = $this->load->database('default', true);

        date_default_timezone_set("Asia/Jakarta");
        $y1=date("Y");
        $m=date("m");

        $tbl = "ecomm_"."$y1"."$m";

        $query = "SELECT * FROM $tbl 
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";
        $res = $this->getRecordset($query);
        return $res;

    }

    function generateOrderNO(){
        $data['sequal'] = $this->cek_seQ();
        $orderno1 = $this->getOrderno();

        foreach($orderno1 as $data)
        {
            date_default_timezone_set("Asia/Jakarta");
            $tgl_updt=date("Y-m-d H:i:s ");
            $y1=date("y");
            $m=date("m");
            //$nilai[]=$data;
            $sequence = $data->SeqID;
            $next_seq = sprintf("%06s",$sequence);
            $orderno  = "IDEC"."$y1"."$m"."$next_seq";
        }
        return $orderno;
    }

    function insertCheckOutData($orderno) {
        $insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno)
          		        values('".$idmember."','".$idstk."',".$totalpay.",'".$trans_id."','".$datetrans."','".$userlogin."','".$orderno."')";

        $queryCheckout = $this->executeQuery($insCheckout);
    }

    function insertLogTrx($veritrans) {
        if($veritrans->transaction_status == 'capture' || $veritrans->transaction_status == 'authorize'
            || $veritrans->transaction_status == 'settlement') {
            $status = '1';
            $log_status = '1';
        }else if($veritrans->transaction_status == 'cancel' ) {
            $status = '2';
            $log_status = '0';
        }else{
            $log_status = '2';
        }

        $insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin)
          		        values('".$idmember."','".$datetrans."','".$orderno."','".$addr1."',".$totalpay.",".$gross_amount.",'".$log_status."','".$userlogin."')";
        //echo "LogTrans ".$insLogTrans."<br>";           
        $queryLogTrans = $this->db->query($insLogTrans);
    }




    function getKATALOG($token) {



        //select all stockist for view
        $slc = "
        SELECT c.*
        FROM
        [db_ecommerce].[dbo].[ecomm_trans_hdr] a
        LEFT JOIN
        [db_ecommerce].[dbo].[ecomm_trans_det_prd] b
        ON a.orderno= b.orderno
        LEFT JOIN
        [db_ecommerce].[dbo].[master_prd_cat_inv] c
        ON b.prdcd = c.cat_inv_id
        where a.token = '$token' AND c.download_filename is not null

        ";
        //echo $slc;

        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	function checkExcludePromo($orderno) {
		$total_belanja = 0;
		$qry = "SELECT SUM(a.qty * a.dpr) as total_belanja 
				FROM ecomm_trans_det_prd a 
				WHERE a.orderno = '$orderno' and a.prdcd NOT IN (
					SELECT prdcd FROM exclude_promo
				)";
		$result = $this->getRecordset($qry);
		if($result != null) {
			$total_belanja = $result[0]->total_belanja;
		}
		return $total_belanja;
	}

    function getPertama($token, $duit,$bv, $id_memb, $hp,$orderno){

        $QgetPromo="SELECT * FROM [dbo].[master_promo_h] where status = 0  AND promo_start <= GETDATE() AND promo_end >= GETDATE() ";
        $resultPromo = $this->getRecordset($QgetPromo);
        if($resultPromo !=null)
            foreach ($resultPromo as $rowq) {
                if($rowq->minimal_dp<=$duit && $rowq->minimal_bv<=$bv){
                    if($rowq->is_pertama == 0){
                        $slc = "
                        SELECT log_usrlogin,COUNT(log_usrlogin) as login from log_trans WHERE log_usrlogin in (
                        SELECT log_usrlogin
                        FROM log_trans a
                        inner JOIN ecomm_trans_hdr b
                        on b.orderno=a.log_trcd
                        inner join klink_mlm2010.dbo.msmemb c
                        ON a.log_usrlogin=c.dfno COLLATE SQL_Latin1_General_CP1_CI_AS
                        WHERE
                        b.token = '$token')
                        AND log_trcd LIKE 'IDEC%'
                        GROUP BY log_usrlogin
                        ";
                        $result = $this->getRecordset($slc);
                        //$result = $this->getRecordset($slc,null,$this->db1);
                        foreach($result as $row){
                            {
                                if($row->login ==1){
								    //update DION @13/09/2018
								    $kelipatan = floor($duit / 350000);
									//$nilai_voucher = $kelipatan * $rowq->nilai_voucher;
									
									$count = 1;
									for($i = $count; $i <= $kelipatan; $i++) {
								
										$trcd = $this->my_counter->getCounter2('P', $this->uuid->v4());

										$input = "insert into ecomm_voucher_list (voucherno, voucherkey, dfno, createdt, expiredt, vouchertype, claim_status, nominal, minvalue,REMARKS )
												  values('$trcd','$trcd', '$row->log_usrlogin', getdate(),DATEADD(day, $rowq->expired_voucher, GETDATE()), '$rowq->kode_promo', '0'
												  ,'$rowq->nilai_voucher', '$rowq->minimal_usage_dp','PROMO K-NET' )";
												  //,'$nilai_voucher', '$rowq->minimal_usage_dp','PROMO K-NET' )";
										$query = $this->executeQuery($input);
										//echo $input."<br />";									

										$inputtr = "insert into tracking_promo (
												  no_trx,
												  no_voucher,
												  dfno,
												  createdt,
												  status,
												  id_promo
												  )
												  values('$orderno','$trcd', '$row->log_usrlogin', getdate(),0,  '$rowq->id' )";
										//echo $inputtr."<br />";;
										$query = $this->executeQuery($inputtr);
									}
									
									$insertproduk="

										INSERT INTO ecomm_trans_det_prd (orderno, prdcd, prdnm, qty, bvr, dpr, pricecode, sentTo, ByrSisaSales)
										SELECT '$orderno',prdcd, prdnm,qty, bv, dp, '12W3','1',NULL
										FROM master_promo_d
										WHERE id_header='$rowq->id';

										";
										//echo $insertproduk."<br />";;
										$queryproduk = $this->executeQuery($insertproduk);
									
									$link="Selamat, anda berhak mendapatkan $kelipatan voucher sebesar $rowq->nilai_voucher! Gunakan sekarang juga di www.k-net.co.id.";
										smsTemplate( $hp, $link);
										smsTemplate("087784698951", $link);
                                }


                            }

                        }
                    }else
                    {
                        $trcd = $this->my_counter->getCounter2('P', $this->uuid->v4());

                        $input = "insert into ecomm_voucher_list (voucherno, voucherkey, dfno, createdt, expiredt, vouchertype, claim_status, nominal, minvalue,REMARKS )
                                              values('$trcd','$trcd', '$id_memb', getdate(),DATEADD(day, $rowq->expired_voucher, GETDATE()), '$rowq->kode_promo', '0'
                                              ,'$rowq->nilai_voucher', '$rowq->minimal_usage_dp','PROMO K-NET' )";
                        $query = $this->executeQuery($input);
                        $link="Selamat, anda berhak mendapatkan voucher sebesar $rowq->nilai_voucher ! Gunakan sekarang juga di www.k-net.co.id.";
                        smsTemplate( $hp, $link);
						smsTemplate("087784698951", $link);


                        $insertproduk="

                                    INSERT INTO ecomm_trans_det_prd (orderno, prdcd, prdnm, qty, bvr, dpr, pricecode, sentTo, ByrSisaSales)
                                    SELECT '$orderno',prdcd, prdnm,qty, bv, dp, '12W3','1',NULL
                                    FROM master_promo_d
                                    WHERE id_header='$rowq->id';

                                    ";
                        $queryproduk = $this->executeQuery($insertproduk);




                        $inputtr = "insert into tracking_promo (
                                              no_trx,
                                              no_voucher,
                                              dfno,
                                              createdt,
                                              status,
                                              id_promo
                                              )
                                              values('$orderno','$trcd', '$row->log_usrlogin', getdate(),0,  '$rowq->id' )";
                        $query = $this->executeQuery($inputtr);

                    }
                }
            }


//        if($duit>=350000){
//            $slc = "
//        SELECT log_usrlogin,COUNT(log_usrlogin) as login from log_trans WHERE log_usrlogin in (
//        SELECT log_usrlogin
//        FROM log_trans a
//        inner JOIN ecomm_trans_hdr b
//        on b.orderno=a.log_trcd
//        inner join klink_mlm2010.dbo.msmemb c
//        ON a.log_usrlogin=c.dfno COLLATE SQL_Latin1_General_CP1_CI_AS
//        WHERE
//        b.token = '$token')
//        AND log_trcd LIKE 'IDEC%'
//
//        GROUP BY log_usrlogin
//        ";
//            $result = $this->getRecordset($slc);
//            //$result = $this->getRecordset($slc,null,$this->db1);
//            foreach($result as $row){
//                {
//                    if($row->login ==1){
//                        $trcd = $this->my_counter->getCounter2('P', $this->uuid->v4());
//
//                        $input = "insert into ecomm_voucher_list (voucherno, voucherkey, dfno, createdt, expiredt, vouchertype, claim_status, nominal, minvalue,REMARKS )
//                          values('$trcd','$trcd', '$row->log_usrlogin', getdate(),DATEADD(day, 35, GETDATE()), 'BLKNET', '0'
//                        , '50000', '750000','PROMO K-NET' )";
//                        $query = $this->executeQuery($input);
//
//                        $link="Selamat, anda berhak mendapatkan voucher sebesar 50.000! Gunakan sekarang juga di www.k-net.co.id.";
//                        smsTemplate( $hp, $link);
//                    }
//
//
//                }
//
//            }
//        }


        return true;

    }


    function getDownload($token, $produk) {



        //select all stockist for view
        $slc = "
        SELECT c.*
        FROM
        [db_ecommerce].[dbo].[ecomm_trans_hdr] a
        LEFT JOIN
        [db_ecommerce].[dbo].[ecomm_trans_det_prd] b
        ON a.orderno= b.orderno
        LEFT JOIN
        [db_ecommerce].[dbo].[master_prd_cat_inv] c
        ON b.prdcd = c.cat_inv_id
        where a.token = '$token' AND c.cat_inv_id='$produk'
        ";
        //echo $slc;

        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }

	function insertTblEcommTransShipAddr($resultInsert, $orderid, $noresi) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
	
		$qry = "SELECT * FROM [db_ecommerce].[dbo].ecomm_trans_shipaddr_sgo where orderid = '$orderid'";
		$resultQry = $this->getRecordset($qry);
		if($resultQry != null) {
			$array = array(
				"orderno" => $resultInsert[0]->orderno,
				"idstk" => $resultQry[0]->idstk,
				"kec_code" => $resultQry[0]->idstk,
				"receiver_name" => $resultQry[0]->idstk,
				"addr1" => $resultQry[0]->idstk,
				"email" => $resultQry[0]->email,
				"conoteJNE" => $noresi,
				"service_type_id" => $resultQry[0]->service_type_id,
				"service_type_name" => $resultQry[0]->service_type_name,
				"total_item" => $resultQry[0]->total_item,
				"total_weight" => $resultQry[0]->total_weight,
				"lat_dest" => $resultQry[0]->lat_dest,
				"long_dest" => $resultQry[0]->long_dest,
			);
			
			$dbqryx->insert("db_ecommerce.dbo.ecomm_trans_shipaddr", $array);	
		}
	}


}
	
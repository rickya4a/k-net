<?php
class Ecomm_trans_model extends MY_Model {
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
	//MOVED TO SHARED MODULE
	/*
	function getBank(){
        $slc = "select * from ecomm_bank WHERE status = '1' order by bankDesc";
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }*/
	function checkDoubleTokenTransHdr($token) {
		$qry = "SELECT token FROM ecomm_trans_hdr a WHERE a.token = '$token'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function checkIfTrxExist($orderno) {
		$qry = "SELECT orderno FROM ecomm_trans_hdr a WHERE a.orderno = '$orderno'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
    function getPromo17Agustus() {
    	$qry = "SELECT prdcd FROM master_prd_promo17 WHERE status = 1";
		$this->db = $this->load->database($this->db1, true);
		//}	
        $query = $this->db->query($qry); 
		if($query->num_rows() > 0)  {
            foreach ($query->result() as $row) {
            	$nilai[] = $row->prdcd;
			}	  
        } 
        return $nilai; 
    }
	
	
	
    function getEcommerceTrxByUserLogin($usr) {
    	$qry = "SELECT a.orderno, a.id_memb, a.nmmember, 
					 CONVERT(VARCHAR(10), a.datetrans , 103) as datetrans, c.cargo_id,
					CASE WHEN c.conoteJNE is null OR c.conoteJNE = '' THEN '--' 
					  ELSE c.conoteJNE  
					END AS conoteJNE,
					CASE WHEN c.cargo_id is null THEN '1' 
					  ELSE c.cargo_id  
					END AS cargo_id
				FROM ecomm_trans_hdr a
				INNER JOIN log_trans b ON (a.orderno = b.log_trcd)
				INNER JOIN ecomm_trans_shipaddr c ON (a.orderno = c.orderno)
				WHERE (b.log_usrlogin = '$usr' OR a.id_memb = '$usr') AND a.sentTo = '2' 
				AND a.orderno LIKE 'IDEC%' 
				order by a.datetrans DESC";
		return $this->getRecordset($qry, null, $this->db1);
    }
    function getVoucherValidCheck($vchno, $dfno) {
    	$qry = "SELECT voucherno, dfno, nominal
    	        FROM ecomm_voucher_list
    	        WHERE voucherno = '$vchno' AND dfno = '$dfno' AND expiredt > '".date("Y-m-d")."' AND claim_status = 0";
		return $this->getRecordset($qry, null, $this->db1);
    }
	
	function getListValidVoucher($dfno) {
		$qry = "SELECT voucherno, nominal, vouchertype, minvalue
    	        FROM ecomm_voucher_list
    	        WHERE dfno = '$dfno' AND expiredt > '".date("Y-m-d")."' AND claim_status = 0 ";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getListValidVoucherPemenang($voucherno){
		$qry = "SELECT voucherno, nominal
    	        FROM ecomm_voucher_list
    	        WHERE voucherno = '$voucherno' AND expiredt > '".date("Y-m-d")."' AND claim_status = 0";
		return $this->getRecordset($qry, null, $this->db1);
	}
    
    function getStatusCnoteByID($id) {
    	$qry = "SELECT a.service_type_name, a.conoteJNE, a.orderno, a.flag_send_conote 
				from ecomm_trans_shipaddr a
				WHERE a.conoteJNE = '$id'";
		return $this->getRecordset($qry, null, $this->db1);
    }
	
	function getListCargo() {
		$qry = "SELECT *
				from master_logistic_shipper a
				WHERE a.shipper_status = '1'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getListCargoJhonDoe() {
		$qry = "SELECT *
				from master_logistic_shipper a";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getListCargoJNE() {
		$qry = "SELECT *
				from master_logistic_shipper a
				WHERE a.shipper_id = '1'";
		return $this->getRecordset($qry, null, $this->db1);
	}

	function insert_temp_trx_sgo_forLP($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		$rtes = "";
		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$sender_address = $this->session->userdata('sender_address');
		$dest_address = $this->session->userdata('destination_address');
		$destination_address = str_replace("â??", "", $dest_address);
		$jne_branch = $this->session->userdata('jne_branch');
		$promo = $this->session->userdata('promo');
		$bns = substr($personal_info['bnsperiod'], 3, 7);
		//$totPay = getTotalPayNet();
		$harga = getTotalPayNetCust();
		$totPay = $harga['total_pay'];
		$tot_pay_cp = $harga['total_pay'];
		$tot_pay_dp= $harga['totalDPay'];
		$selisihHargaCust = $tot_pay_cp - $tot_pay_dp;

		$totalbv = $this->cart->total_bv();
		$freeship = "0";	
		
		$no_hp_konfirmasi = $this->session->userdata('no_hp_konfirmasi');
		
		//if(getUserID() == "IDSPAAA66834") {
			//print_r($shipping_jne_info);
			/*$biaya = getTotalPayNetBaru();
		    $gross_amount = $biaya['total_pay'] + $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
		    $freeship = $biaya['freeship'];
		
			$discount = $shipping_jne_info['ship_discount']; */
		//} else {
			/*$gross_amount = getTotalPayNetAndShipCost();
			if($shipping_jne_info == null) {
				$freeship = "1";
			} else {
				$freeship = "0";
			}*/
			$biaya = getTotalPayNetAndShipCost2();
		    $gross_amount = $biaya['total_pay'];
		    $freeship = $biaya['freeship'];
			$discount = $biaya['tot_discount'];
			
		//}	
		
		$shipper_telhp = getUserPhone();
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$produk = $this->cart->contents();
		
		$cust_memb = $this->session->userdata("non_member_info");
		if(isset($cust_memb)) {
			$is_login = "2";
			$tot_paynet = getTotalPayNet();		
		} else {			
			 $memb = $this->session->userdata("store_info");
			 if(isset($memb)) {
			 	$is_login = "1";
				$tot_paynet = 0;				
			 }
		}
			
		$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$trans_id."','1','sgo','',".$gross_amount.",'pending','pending')";
        //echo "Paydet ".$insPaydet."<br>"; 
		$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
        
        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo 
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_login,totPayDP,totPayCP,profit_member,id_lp, free_shipping, discount_shipping, no_hp_konfirmasi)
			          values('".$trans_id."','','".$trans_id."','".trim($personal_info['idmemberx'])."','".trim($personal_info['membername'])."',
			                    $totPay,$totalbv,'".$pricecode."','".$bns."','".$datetrans."',
			                    '".trim($personal_info['stockist'])."','".trim($nmstkSS)."','0','W','".$personal_info['delivery']."','0','',0,0,'$is_login','$tot_pay_dp','$tot_pay_cp','$selisihHargaCust','".$personal_info['id_lp']."', '$freeship', $discount,
								'$no_hp_konfirmasi')";
                //echo "header gagal ".$insHeader."<br>";
        $exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));

		//echo $insHeader;
        
        foreach($produk as $row)
            {
                //echo "sebelum eksekusi db produk<br>";
                $prdcd = $row['id'];
                $qty = (int)$row['qty'];
                $bv = (int)$row['bv'];
                $prdnm = $row['name'];

				$personal_info  = $this->session->userdata('personal_info');
				$is_drop=$personal_info['id_lp'];

                if($pricecode == '12W3' || $pricecode == '12W4'){
                    $price = (int)$row['west_price'];

					//if($is_drop!='' && ($is_drop == "DROPSHIP" || $is_drop == "CANVAS" || $is_drop == "n_promo")){
						$hargaC= (int)$row['west_Cprice'];
						$selisih= $hargaC - $price;
					//}
                }else{
                    $price = (int)$row['east_price'];

					//if($is_drop!='' && ($is_drop == "DROPSHIP" || $is_drop == "CANVAS" || $is_drop == "n_promo")){
						$hargaC= (int)$row['east_Cprice'];
						$selisih= $hargaC - $price;
					//}
                }
                
                $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo,cpr,profit_d)
              		        values('".$trans_id."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$hargaC.",'".$pricecode."','','".$price."','".$selisih."')";
           	    
               //echo "produk ".$inputProd."";
               $exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));
            }
        $exeShipAddr = true;    
        if($personal_info['delivery'] == "2") {
        	$tot_item_prd = $this->cart->total_items();
			$tot_weight_prd = $this->cart->total_weight();  
			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);	 
            
			if(array_key_exists("shipper", $personal_info)) {
				$shipper_id = $personal_info['shipper'];	
			} else {
				$shipper_id = "1";
			}
			
			
			//$al1 = str_replace("â??", "", $personal_info['alamat']);
			$all = cleanAddress($personal_info['alamat']);
            $alamat = trim(strtoupper($all));
			
			$all2 = cleanAddress($personal_info['email']);
            $email = trim($all2);
			
            $receiver = trim(strtoupper(cleanAddress($personal_info['nama_penerima'])));
			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code, kab_code, kec_code,
			                     addr1, email, tel_hp1, total_item, total_weight, service_type_id, service_type_name,
			                     receiver_name, stockist_name, kabupaten_name, province_name,
								 sender_address, dest_address, jne_branch, shipper_telhp, total_pay_net, cargo_id)
			                VALUES ('".$trans_id."', '".$personal_info['stockist']."', '".$personal_info['provinsi']."', '".$personal_info['kota']."',
			                        '".$personal_info['kecamatan']."', '".$alamat."', 
			                        '".$email."', '".$personal_info['notlp']."',
			                        $tot_item_prd, $tot_weight_prd, '".$shipping_jne_info['service_code']."', '".$shipping_jne_info['service_display']."',
			                        '".$receiver."','$nama_stk','".trim(strtoupper($personal_info['nama_kota']))."','".trim(strtoupper($personal_info['nama_provinsi']))."',
			                        '".$sender_address."', '".$destination_address."','".$jne_branch."', '$shipper_telhp', ".$totPay.", '".$shipper_id."')";
						
			$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));

		}   
							
		if($exeInsPaydet && $exeInsHeader && $exeInputProdr)
        {
            return 1;
        }
        else
        {
            return 0;
        }
	}

	
	function insert_temp_trx_sgo($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$sender_address = $this->session->userdata('sender_address');
		$dest_address = $this->session->userdata('destination_address');
		$destination_address = str_replace("â??", "", $dest_address);
		$jne_branch = $this->session->userdata('jne_branch');
		$promo = $this->session->userdata('promo');
		$bns = substr($personal_info['bnsperiod'], 3, 7);
		//$totPay = getTotalPayNet();
		$harga = getTotalPayNetCust();
		$totPay = $harga['total_pay'];
		$tot_pay_cp = $harga['total_pay'];
		$tot_pay_dp= $harga['totalDPay'];
		$selisihHargaCust = $tot_pay_cp - $tot_pay_dp;

		$totalbv = $this->cart->total_bv();
		$freeship = "0";	
		
		$no_hp_konfirmasi = $this->session->userdata('no_hp_konfirmasi');
		
		//if(getUserID() == "IDSPAAA66834") {
			//print_r($shipping_jne_info);
			/*$biaya = getTotalPayNetBaru();
		    $gross_amount = $biaya['total_pay'] + $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
		    $freeship = $biaya['freeship'];
		
			$discount = $shipping_jne_info['ship_discount']; */
		//} else {
			/*$gross_amount = getTotalPayNetAndShipCost();
			if($shipping_jne_info == null) {
				$freeship = "1";
			} else {
				$freeship = "0";
			}*/
			$biaya = getTotalPayNetAndShipCost2();
		    $gross_amount = $biaya['total_pay'];
		    $freeship = $biaya['freeship'];
			$discount = $biaya['tot_discount'];
			
		//}	
		
		$shipper_telhp = getUserPhone();
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$produk = $this->cart->contents();
		
		$cust_memb = $this->session->userdata("non_member_info");
		if(isset($cust_memb)) {
			$is_login = "2";
			$tot_paynet = getTotalPayNet();		
		} else {			
			 $memb = $this->session->userdata("store_info");
			 if(isset($memb)) {
			 	$is_login = "1";
				$tot_paynet = 0;				
			 }
		}
			
		$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$trans_id."','1','sgo','',".$gross_amount.",'pending','pending')";
        //echo "Paydet ".$insPaydet."<br>"; 
		$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
        
        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo 
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_login,totPayDP,totPayCP,profit_member,id_lp, free_shipping, discount_shipping, no_hp_konfirmasi)
			          values('".$trans_id."','','".$trans_id."','".trim($personal_info['idmemberx'])."','".trim($personal_info['membername'])."',
			                    $totPay,$totalbv,'".$pricecode."','".$bns."','".$datetrans."',
			                    '".trim($personal_info['stockist'])."','".trim($nmstkSS)."','0','W','".$personal_info['delivery']."','0','',0,0,'$is_login','$tot_pay_dp','$tot_pay_cp','$selisihHargaCust','".$personal_info['id_lp']."', '$freeship', $discount,
								'$no_hp_konfirmasi')";
                //echo "header gagal ".$insHeader."<br>";
        $exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));

		//echo $insHeader;
        
        foreach($produk as $row)
            {
                //echo "sebelum eksekusi db produk<br>";
                $prdcd = $row['id'];
                $qty = (int)$row['qty'];
                $bv = (int)$row['bv'];
                $prdnm = $row['name'];

				$personal_info  = $this->session->userdata('personal_info');
				$is_drop=$personal_info['id_lp'];

                if($pricecode == '12W3' || $pricecode == '12W4'){
                    $price = (int)$row['west_price'];

					if($is_drop!='' && ($is_drop == "DROPSHIP" || $is_drop == "CANVAS" || $is_drop == "n_promo")){
						$hargaC= (int)$row['west_Cprice'];
						$selisih= $hargaC - $price;
					}
                }else{
                    $price = (int)$row['east_price'];

					if($is_drop!='' && ($is_drop == "DROPSHIP" || $is_drop == "CANVAS" || $is_drop == "n_promo")){
						$hargaC= (int)$row['east_Cprice'];
						$selisih= $hargaC - $price;
					}
                }
                
                $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo,cpr,profit_d)
              		        values('".$trans_id."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$hargaC.",'".$pricecode."','','".$price."','".$selisih."')";
           	    
               //echo "produk ".$inputProd."";
               $exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));
            }
        $exeShipAddr = true;    
        if($personal_info['delivery'] == "2") {
        	$tot_item_prd = $this->cart->total_items();
			$tot_weight_prd = $this->cart->total_weight();  
			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);	 
            
			if(array_key_exists("shipper", $personal_info)) {
				$shipper_id = $personal_info['shipper'];	
			} else {
				$shipper_id = "1";
			}
			
			
			//$al1 = str_replace("â??", "", $personal_info['alamat']);
			$all = cleanAddress($personal_info['alamat']);
            $alamat = trim(strtoupper($all));
			
			$all2 = cleanAddress($personal_info['email']);
            $email = trim($all2);
			
            $receiver = trim(strtoupper(cleanAddress($personal_info['nama_penerima'])));
			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code, kab_code, kec_code,
			                     addr1, email, tel_hp1, total_item, total_weight, service_type_id, service_type_name,
			                     receiver_name, stockist_name, kabupaten_name, province_name,
								 sender_address, dest_address, jne_branch, shipper_telhp, total_pay_net, cargo_id)
			                VALUES ('".$trans_id."', '".$personal_info['stockist']."', '".$personal_info['provinsi']."', '".$personal_info['kota']."',
			                        '".$personal_info['kecamatan']."', '".$alamat."', 
			                        '".$email."', '".$personal_info['notlp']."',
			                        $tot_item_prd, $tot_weight_prd, '".$shipping_jne_info['service_code']."', '".$shipping_jne_info['service_display']."',
			                        '".$receiver."','$nama_stk','".trim(strtoupper($personal_info['nama_kota']))."','".trim(strtoupper($personal_info['nama_provinsi']))."',
			                        '".$sender_address."', '".$destination_address."','".$jne_branch."', '$shipper_telhp', ".$totPay.", '".$shipper_id."')";
						
			$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));

		}   
							
		if($exeInsPaydet && $exeInsHeader && $exeInputProdr)
        {
            return 1;
        }
        else
        {
            return 0;
        }	
	}

	function delete_temp_trx_sgo($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$del1 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_paydet_sgo WHERE orderno = '$trans_id'";
		$resDel1 = $this->db->query($del1);
		
		$del2 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_hdr_sgo WHERE orderno = '$trans_id'";
		$resDel2 = $this->db->query($del2);
		
		$del3 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_det_prd_sgo WHERE orderno = '$trans_id'";
		$resDel3 = $this->db->query($del3);

		//echo $del2;
		
		$del4 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_shipaddr_sgo WHERE orderno = '$trans_id'";
		$resDel4 = $this->db->query($del4);
	}
	
	//MOVED TO SHARED MODULE
	/*
	function updateBankCodePayment($temp_id, $arr) {
		$this->db = $this->load->database('db_ecommerce', true);
		$upd = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo SET 
		          bank_code_payment = ".$arr['bank_code_payment'].", payShip = ".$arr['payShip'].", 
		          sentTo = '".$arr['sentTo']."', userlogin = '".$arr['userlogin']."',
		          payAdm = ".$arr['payAdm'].", payConnectivity = ".$arr['payConnectivity']."
		        WHERE orderno = '$temp_id'";
		$upd2 = $this->db->query($upd);
	}
	*/
	function updatePayStatusSGO($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);
		$upd1 = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo 
				 SET status_vt_pay = '1'
				 WHERE orderno = '$trans_id'";
		$exeUpd1 = $this->db->query($upd1);
		
		$upd2 = "UPDATE db_ecommerce.dbo.ecomm_trans_paydet_sgo 
				 SET paystatus = 'ok'
				 WHERE orderno = '$trans_id'";
		$exeUpd2 = $this->db->query($upd2);
		
		if($exeUpd1 > 0 && $exeUpd2 > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function updateSgoTrxMampirKak($trans_id, $memberid, $membername) {
		$this->db = $this->load->database('db_ecommerce', true);
		
		$upd1 = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo 
				 SET 
				   status_vt_pay = '1',
				   id_memb = '$memberid',
				   nmmember = '$membername' 
				 WHERE orderno = '$trans_id'";
		$exeUpd1 = $this->db->query($upd1);
		
		$upd2 = "UPDATE db_ecommerce.dbo.ecomm_trans_paydet_sgo 
				 SET paystatus = 'ok'
				 WHERE orderno = '$trans_id'";
		$exeUpd2 = $this->db->query($upd2);
		
		$upd3 = "UPDATE db_ecommerce.dbo.ecomm_memb_ok_sgo 
				 SET memberid = '$memberid', membername = '$membername'
				 WHERE trx_no = '$trans_id'";
		$exeUpd3 = $this->db->query($upd3);
		
		if($exeUpd1 > 0 && $exeUpd2 > 0 && $exeUpd3 > 0) {
			return true;
		} else {
			return false;
		}
		
	}
	
	function checkAddressReferenceByID($memberid) {
		$db = $this->load->database('db_ecommerce', true);
		$qry = "SELECT memberid FROM log_dest_address WHERE memberid = '$memberid'";
		$res = $db->query($qry);
		return $res->num_rows();
	}
	
	
	
	function insertAddressReference($personal_info, $arr) {
		$this->db = $this->load->database('db_ecommerce', true);
		$qry = "INSERT INTO log_dest_address 
			        (memberid, nama_penerima, email, notlp, stkarea, nama_stkarea,
			         stockist, nama_stockist, provinsi, nama_provinsi, kota, nama_kota,
					 kecamatan, nama_kecamatan, alamat, sender_address, destination_address,
					 jne_branch)
			    VALUES ('".$arr['memberid']."', '".$personal_info['nama_penerima']."', '".$personal_info['email']."', '".$personal_info['notlp']."', '".$personal_info['stkarea']."', '".$personal_info['nama_stkarea']."', 
			            '".$personal_info['stockist']."', '".$personal_info['nama_stockist']."', '".$personal_info['provinsi']."', '".$personal_info['nama_provinsi']."', '".$personal_info['kota']."', '".$personal_info['nama_kota']."',
			            '".$personal_info['kecamatan']."', '".$personal_info['nama_kecamatan']."', '".$personal_info['alamat']."', '".$arr['sender_address']."', '".$arr['destination_address']."',
				        '".$arr['jne_branch']."'
				      )";
		$qry2 = $this->db->query($qry);
		return $qry2;
	}
	
	function cek_seQ(){
        $this->db = $this->load->database('db_ecommerce', true);	
        date_default_timezone_set("Asia/Jakarta");
        $y1=date("Y");
        $m=date("m");
        
        $tbl = "ecomm_"."$y1"."$m";
        
        $cek = "select * from $tbl";
        
        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->db->query($input);
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->db->query($input);
            //echo "<br>ada<br>";
        }        
    }

	function getsOrderno() {
		$data['sequal'] = $this->cek_seQ();

		date_default_timezone_set("Asia/Jakarta");
		$tgl_updt=date("Y-m-d H:i:s ");
		$yGEDE=date("Y");
		$m=date("m");
		$y1=date("y");
		$tbl = "ecomm_"."$yGEDE"."$m";

		$query = "SELECT * FROM $tbl
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";

		//echo $query;
		$orderno1 = $this->db->query($query);

		foreach($orderno1->result() as $data)
		{
			/*date_default_timezone_set("Asia/Jakarta");
            $tgl_updt=date("Y-m-d H:i:s ");
            $y1=date("y");
            $m=date("m"); */
			//$nilai[]=$data;
			$sequence = $data->SeqID;
			$next_seq = sprintf("%06s",$sequence);
			$orderno  = "IDEC"."$y1"."$m"."$next_seq";
		}
		return $orderno;
	}
	function getsOrdernoDigital() {
		$data['sequal'] = $this->cek_seQ();

		date_default_timezone_set("Asia/Jakarta");
		$tgl_updt=date("Y-m-d H:i:s ");
		$yGEDE=date("Y");
		$m=date("m");
		$y1=date("y");
		$tbl = "ecomm_"."$yGEDE"."$m";

		$query = "SELECT * FROM $tbl
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";

		//echo $query;
		$orderno1 = $this->db->query($query);

		foreach($orderno1->result() as $data)
		{
			/*date_default_timezone_set("Asia/Jakarta");
            $tgl_updt=date("Y-m-d H:i:s ");
            $y1=date("y");
            $m=date("m"); */
			//$nilai[]=$data;
			$sequence = $data->SeqID;
			$next_seq = sprintf("%06s",$sequence);
			$orderno  = "E"."$y1"."$m"."$next_seq";
		}
		return $orderno;
	}
	function randomChars($length){
        
       $karakter= 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789';
       $string = ''; 
       for ($i = 0; $i < $length; $i++) 
       { 
            $pos = rand(0, strlen($karakter)-1); 
            $string .= $karakter{$pos}; 
       } 
       return $string; 
    }
	
	function updateListVoucher($order_id, $dfno) {
		$qry = "UPDATE a
				SET a.claim_dt = GETDATE(), a.claim_by = '$dfno', a.claim_status = 1
				FROM ecomm_voucher_list a
				INNER JOIN ecomm_trans_paydet_sgo b ON (a.voucherno = b.docno COLLATE SQL_Latin1_General_CP1_CS_AS)
				WHERE b.orderno = '$order_id'";
		$this->executeQuery($qry, $this->db1);
	}
	
	function insertTrxTblEcommTest($order_id, $orderno, $resi) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		$order_id = trim($order_id);
		$secno = $this->randomChars(5);
		
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $this->db->query($qry);
		foreach($query->result() as $row) {

			$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";
            //echo "checout <br>".$insCheckout."<br><br>";          
            //$queryCheckout = $this->db->query($insCheckout);
            
			//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
			$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;
            
            /*$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".getUserID()."')";
			*/			
		    $insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
            //echo "LogTrans <br>".$insLogTrans."<br><br>";         
            //$queryLogTrans = $this->db->query($insLogTrans);
			
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            //$queryPaydet = $this->db->query($insPaydet);
            
            
			//di set 1 saat live
			$st_vt_pay = "1";
			
			//tambahan dion 09/10/2015
			if($row->bank_code_payment == null || $row->bank_code_payment == "") {
				$charge_admin = 0;
				$charge_connectivity = 0;
				$bank_code_pay = 1;
			} else {
				$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
				$querypayADM = $this->db->query($payADM);
				$payresadm = $querypayADM->result();
				$charge_admin = $payresadm[0]->charge_admin;
				$charge_connectivity = $payresadm[0]->charge_connectivity;
				$bank_code_pay = $row->bank_code_payment;
			}
			
			$is_ship = 0;
			if($row->sentTo == "2") {
				$is_ship = 1;
			}

			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login, totPayCP) 
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			             '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."', ".$row->totPayCP.");

			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
						SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						FROM 	ecomm_trans_det_prd_sgo a
						WHERE 	a.orderno='".$order_id."'"; 
         
		}	
		
		$arr = array("insHeader" => $insHeader);
		return $arr; //aslinya code mas dion
	}
	
	function setCashBackPointBalance($order_id, $orderno, $balltype) {
		$datetrans = date("Y-m-d H:i:s");
		
		if($balltype == "D") {
			$qry = "SELECT a.orderno, a.paytype, 
				       a.docno, a.payamt, b.log_usrlogin as dfno, c.fullnm
				FROM db_ecommerce.dbo.ecomm_trans_paydet a
				INNER JOIN log_trans b ON (a.orderno COLLATE SQL_Latin1_General_CP1_CI_AS = b.log_trcd)
				INNER JOIN klink_mlm2010.dbo.msmemb c ON (b.log_usrlogin COLLATE SQL_Latin1_General_CP1_CS_AS = c.dfno)
				WHERE a.orderno='$orderno' AND (a.paytype = 'cbp' or a.paytype = 'CBP')";
	    	$res = $this->getRecordset($qry, null, $this->db1);	
	    	if($res != null) {
	    		
				$payamt = $res[0]->payamt;
				$ins = "INSERT INTO balance_det (dfno, fullnm, createdt, createnm, status, balltype, balance, orderno) VALUES 
				        ('".$res[0]->dfno."', '".$res[0]->fullnm."', '$datetrans', '".$res[0]->dfno."', '1','D', ".$payamt.", '".$res[0]->orderno."')";
				$bal = $this->executeQuery($ins, $this->db1);
		        if($bal > 0) {
		        	$upd = "UPDATE balance_hdr SET balance = balance - $payamt WHERE dfno = '".$res[0]->dfno."'";
					$upd2 = $this->executeQuery($upd, $this->db1);
				}	
			}	
		}	
	}
	
	function updatePaymentVchCashback($order_id, $orderno) {
		$datetrans = date("Y-m-d H:i:s");
		
	    $qry = "SELECT a.orderno, a.paytype, 
				       a.docno, a.payamt, b.log_usrlogin as dfno, c.fullnm
				FROM db_ecommerce.dbo.ecomm_trans_paydet a
				INNER JOIN log_trans b ON (a.orderno COLLATE SQL_Latin1_General_CP1_CI_AS = b.log_trcd)
				INNER JOIN klink_mlm2010.dbo.msmemb c ON (b.log_usrlogin COLLATE SQL_Latin1_General_CP1_CS_AS = c.dfno)
				WHERE a.orderno='$orderno' AND a.paytype != 'sgo' AND a.paytype != 'SGO'";
	    	$res = $this->getRecordset($qry, null, $this->db1);	
	    	if($res != null) {
	    		foreach($res as $dataPay) {
	    			//check if any cashback included
	    			if($dataPay->paytype == "cbp" || $dataPay->paytype == "CBP") {
	    				$payamt = $dataPay->payamt;
						$ins = "INSERT INTO balance_det (dfno, fullnm, createdt, createnm, status, balltype, balance, orderno) VALUES 
						        ('".$dataPay->dfno."', '".$dataPay->fullnm."', '$datetrans', '".$dataPay->dfno."', '1','D', ".$payamt.", '".$dataPay->orderno."')";
						$bal = $this->executeQuery($ins, $this->db1);
				        if($bal > 0) {
				        	$upd = "UPDATE balance_hdr SET balance = balance - $payamt WHERE dfno = '".$dataPay->dfno."'";
							$upd2 = $this->executeQuery($upd, $this->db1);
						}	
	    			} else if($dataPay->paytype == "C" || $dataPay->paytype == "P") {
	    				//$updField = "VoucherNo";
	    				$updField = "VoucherNo";	
	    				if($dataPay->paytype == "C") {
	    					$updField = "voucherkey";
	    				} 
	    				$upd = "UPDATE tcvoucher SET claimstatus = '1', claim_date = '".$datetrans."',
	    				             updatenm = '".$dataPay->dfno."', loccd = 'K-NET'
	    				        WHERE DistributorCode = '".$dataPay->dfno."' AND $updField = '".$dataPay->docno."'";
						$upd2 = $this->executeQuery($upd, $this->db2);
	    			}
	    		}
				
			}	
		
	}
	
	function insertTrxTblEcommV2($order_id, $orderno, $resi) {
		$execQuery = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		
		
        $secno = $this->randomChars(5);
		
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $execQuery->query($qry);
		foreach($query->result() as $row) {
			//$execQuery->trans_start();
		
			//dipindah
            $gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;		
            
			//di set 1 saat live
			$st_vt_pay = "1";
			
			//tambahan dion 09/10/2015
			if($row->bank_code_payment == null || $row->bank_code_payment == "") {
				$charge_admin = 0;
				$charge_connectivity = 0;
				$bank_code_pay = 1;
			} else {
				$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
				$querypayADM = $execQuery->query($payADM);
				$payresadm = $querypayADM->result();
				$charge_admin = $payresadm[0]->charge_admin;
				$charge_connectivity = $payresadm[0]->charge_connectivity;
				$bank_code_pay = $row->bank_code_payment;
			}
			
			$is_ship = 0;
			if($row->sentTo == "2") {
				$is_ship = 1;
			}
			
			$cshbck = 0;
			if($row->cashback_point != null || $row->cashback_point != "") {
				$cshbck = $row->cashback_point;
			}
			
			/*$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login, 
							  totPayCP,id_lp, free_shipping, discount_shipping, cashback_point, no_hp_konfirmasi) 
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                    '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."', 
			                    ".$row->totPayCP.", '".$row->id_lp."', '".$row->free_shipping."', ".$row->discount_shipping.", ".$cshbck.", '".$row->no_hp_konfirmasi."');
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd 
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
						 SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						 FROM ecomm_trans_det_prd_sgo a
						 WHERE a.orderno='".$order_id."'"; */
						 
			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login, 
							  totPayCP,id_lp, free_shipping, discount_shipping, cashback_point, no_hp_konfirmasi) 
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                    '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."', 
			                    ".$row->totPayCP.", '".$row->id_lp."', '".$row->free_shipping."', ".$row->discount_shipping.", ".$cshbck.", '".$row->no_hp_konfirmasi."');
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd 
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
						 SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						 FROM ecomm_trans_det_prd_sgo a
						 INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.token)
						 WHERE a.orderno='".$order_id."'"; 
            //echo "header<br>".$insHeader."<br><br>";
            $queryHeader = $execQuery->query($insHeader);
			
			if($queryHeader > 0) {
			
				echo "isi : ".$queryHeader;
				
				//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
				
				$insCheckoutSS = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
							values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";
				//echo "checout <br>".$insCheckoutSS."<br><br>";          
				$queryCheckout = $execQuery->query($insCheckoutSS);
				
								
				$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
							values('".$row->id_memb."','".$datetrans."','".$orderno."','',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
				//echo "LogTrans <br>".$insLogTrans."<br><br>";         
				$queryLogTrans = $execQuery->query($insLogTrans);
				
				
				
				
				$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
							  SELECT '".$orderno."', seqno,paytype,docno, payamt, 'trx ok','capture', bank_code_payment, charge_admin
							  FROM 	db_ecommerce.dbo.ecomm_trans_paydet_sgo a
							  WHERE a.orderno='".$order_id."'"; 
							//"values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
				//echo "Paydet <br>".$insPaydet."<br><br>"; 
				$queryPaydet = $execQuery->query($insPaydet);
				
				//UPDATE VOUCHER BELANJA
				//Add by DION @ 09/08/2016
				$updVch = "UPDATE a
					SET a.claim_dt = GETDATE(), a.claim_by = '".$row->userlogin."', a.claim_status = 1
					FROM ecomm_voucher_list a
					INNER JOIN ecomm_trans_paydet_sgo b ON (a.voucherno = b.docno COLLATE SQL_Latin1_General_CP1_CS_AS)
					WHERE b.orderno = '$order_id'";
				$queryUpdVch = $execQuery->query($updVch);
				
			
				//update DION @28/11/2016
				if($row->is_login == "2" || $row->is_login == "3") {
					$qryASu = "SELECT A.orderno, SUM((b.cp * a.qty)) - SUM((b.dp * a.qty)) as hrg_selisih
							FROM db_ecommerce.dbo.ecomm_trans_det_prd a
							INNER JOIN db_ecommerce.dbo.master_prd_pricetab b ON (a.prdcd = b.cat_inv_id)
							WHERE a.orderno = '".$orderno."' AND b.pricecode = a.pricecode
							GROUP BY a.orderno";	
					//echo $qryXX;
					//$hasilxxc = $this->getRecordset($qry, null, $this->db1);	
					$hasilxxc = $execQuery->query($qryASu);
					if($hasilxxc > 0) {
							$hasilx = $hasilxxc->result();	
							if($hasilx != null){
							  $updrs = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr 
										SET totPayCP = ".$hasilx[0]->hrg_selisih." 
										WHERE orderno = '$orderno'";
							  //echo $updrs;
							  $updrs_exe = $execQuery->query($updrs);
							}
					}
				}
				//end update
				
				if($queryCheckout > 0 && $queryLogTrans > 0 && $queryPaydet > 0 &&  
				   $queryHeader > 0)
				{
					
					return 1;
				}
				else
				{
					return 0;
				}	
			} else {
				return 0;
			}
			 
		}	
	}
	
	function insertTrxTblEcomm($order_id, $orderno, $resi) {
		$execQuery = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		
		
        $secno = $this->randomChars(5);
		
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $execQuery->query($qry);
		foreach($query->result() as $row) {

			//$execQuery->trans_start();
		
			$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;		
			$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";
            //echo "checout <br>".$insCheckout."<br><br>";          
            $queryCheckout = $execQuery->query($insCheckout);
            
			//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
			
                      		
		    $insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
            //echo "LogTrans <br>".$insLogTrans."<br><br>";         
            $queryLogTrans = $execQuery->query($insLogTrans);
			
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
			              SELECT '".$orderno."', seqno,paytype,docno, payamt, 'trx ok','capture', bank_code_payment, charge_admin
						  FROM 	db_ecommerce.dbo.ecomm_trans_paydet_sgo a
						  WHERE a.orderno='".$order_id."'"; 
						//"values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            $queryPaydet = $execQuery->query($insPaydet);
			
			//UPDATE VOUCHER BELANJA
			//Add by DION @ 09/08/2016
			$updVch = "UPDATE a
				SET a.claim_dt = GETDATE(), a.claim_by = '".$row->userlogin."', a.claim_status = 1
				FROM ecomm_voucher_list a
				INNER JOIN ecomm_trans_paydet_sgo b ON (a.voucherno = b.docno COLLATE SQL_Latin1_General_CP1_CS_AS)
				WHERE b.orderno = '$order_id'";
		    $queryUpdVch = $execQuery->query($updVch);
            
            
			//di set 1 saat live
			$st_vt_pay = "1";
			
			//tambahan dion 09/10/2015
			if($row->bank_code_payment == null || $row->bank_code_payment == "") {
				$charge_admin = 0;
				$charge_connectivity = 0;
				$bank_code_pay = 1;
			} else {

				if($row->bank_code_payment == "30"){
					$charge_admin = $row->payAdm;
					$charge_connectivity = 0;
					$bank_code_pay = $row->bank_code_payment;
				}else{
					$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
					$querypayADM = $execQuery->query($payADM);
					$payresadm = $querypayADM->result();
					$charge_admin = $payresadm[0]->charge_admin;
					$charge_connectivity = $payresadm[0]->charge_connectivity;
					$bank_code_pay = $row->bank_code_payment;
				}

			}
			
			$is_ship = 0;
			if($row->sentTo == "2") {
				$is_ship = 1;
			}
			
			$cshbck = 0;
			if($row->cashback_point != null || $row->cashback_point != "") {
				$cshbck = $row->cashback_point;
			}
			
			$discount_shipping = 0;
			if($row->discount_shipping != null || $row->discount_shipping != "") {
				$discount_shipping = $row->discount_shipping;
			}
			
			/*$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login, 
							  totPayCP,id_lp, free_shipping, discount_shipping, cashback_point, no_hp_konfirmasi) 
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                    '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."', 
			                    ".$row->totPayCP.", '".$row->id_lp."', '".$row->free_shipping."', ".$row->discount_shipping.", ".$cshbck.", '".$row->no_hp_konfirmasi."');
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd 
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
						 SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						 FROM ecomm_trans_det_prd_sgo a
						 WHERE a.orderno='".$order_id."'"; */

			$totPayDP= $row->totPayDP;
			$totPayCP= $row->totPayCP;
			$profit_member=$row->profit_member;
			$payship=$row->payShip;

			if($totPayDP == "NULL" || $totPayDP == ""){
				$totPayDP=0;
			}else{
				$totPayDP= $totPayDP;
			}

			if($totPayCP == "NULL" || $totPayCP == ""){
				$totPayCP=0;
			}else{
				$totPayCP= $totPayCP;
			}

			if($profit_member == "NULL" || $profit_member == ""){
				$profit_member=0;
			}else{
				$profit_member= $profit_member;
			}

			if($payship == "NULL" || $payship == ""){
				$payship=0;
			}else{
				$payship= $payship;
			}


			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login, 
							  totPayDP,totPayCP,profit_member,id_lp, free_shipping, discount_shipping, cashback_point, no_hp_konfirmasi)
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                    '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$payship.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."',
			                    ".$totPayDP.",".$totPayCP.", ".$profit_member.",'".$row->id_lp."', '".$row->free_shipping."', ".$discount_shipping.", ".$cshbck.", '".$row->no_hp_konfirmasi."');
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd 
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,cpr,pricecode,sentTo,profit_d)
						 SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr,a.cpr, a.pricecode, a.sentTo,a.profit_d
						 FROM ecomm_trans_det_prd_sgo a
						 INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.token)
						 WHERE a.orderno='".$order_id."'"; 
            //echo "header<br>".$insHeader."<br><br>";
            $queryHeader = $execQuery->query($insHeader);


			//update DION @28/11/2016
			if($row->is_login == "2" || $row->is_login == "3") {
				$qryASu = "SELECT A.orderno, SUM((b.cp * a.qty)) - SUM((b.dp * a.qty)) as hrg_selisih
						FROM db_ecommerce.dbo.ecomm_trans_det_prd a
						INNER JOIN db_ecommerce.dbo.master_prd_pricetab b ON (a.prdcd = b.cat_inv_id)
						WHERE a.orderno = '".$orderno."' AND b.pricecode = a.pricecode
						GROUP BY a.orderno";	
				//echo $qryXX;
				//$hasilxxc = $this->getRecordset($qry, null, $this->db1);	
				$hasilxxc = $execQuery->query($qryASu);
			    if($hasilxxc > 0) {
						$hasilx = $hasilxxc->result();	
						if($hasilx != null){
						  $updrs = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr 
						            SET totPayCP = ".$hasilx[0]->hrg_selisih." 
						            WHERE orderno = '$orderno'";
						  //echo $updrs;
						  $updrs_exe = $execQuery->query($updrs);
						}
				}
			}
			//end update

			//update vera jika ada produk bundling ion nano 08.02.2019
			/*if ($row ->userlogin == "IDSPAAA66834") {
				$qryPrd = "SELECT a.*
		              FROM ecomm_trans_det_prd a
		              WHERE a.orderno = '$orderno'";
				$queryExec1 = $this->db->query($qryPrd);

				if($queryExec1->num_rows() > 0) {
					foreach($queryExec1->result() as $rows)
					{
						$nprdcd= $rows->prdcd;

						if(substr($nprdcd,0,2) == '00'){
							$nprdcd1=str_replace("00","",$nprdcd);

							$updtPrd="update db_ecommerce.dbo.ecomm_trans_det_prd set prdcd='$nprdcd1' where orderno='".$orderno."' AND prdcd='$nprdcd'";
							$this->db->query($updtPrd);
						}
					}
				}
			}*/
			
			if($queryCheckout > 0 && $queryLogTrans > 0 && $queryPaydet > 0 &&  
			   $queryHeader > 0)
	        {
	            
				return 1;
	        }
	        else
	        {
	            return 0;
	        }	
			
			//CHECK IF TRANSACTION PROCESS IS COMMITED
			//print_r($execQuery->trans_status());
			//SUDAH PERNAH DICOBA DAN GAGAL
			/* ($execQuery->trans_status() === FALSE) {
				//if something went wrong, rollback everything
				$execQuery->trans_rollback();
				return 0;
			} else {
				//if everything went right, insert the data to the database
				$execQuery->trans_commit();
				return 1;
			} */
			  
		}		
			
	}

	function insertTrxTblEcommDev($order_id, $orderno, $resi) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		
        $secno = $this->randomChars(5);
		
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $this->db->query($qry);
		foreach ($query->result() as $row) {
			/*$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".getUserID()."','".$orderno."')";
			*/			
			$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";
            //echo "checout <br>".$insCheckout."<br><br>";          
            $queryCheckout = $this->db->query($insCheckout);
            
			//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
			$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;
            
            /*$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".getUserID()."')";
			*/			
		    $insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
            //echo "LogTrans <br>".$insLogTrans."<br><br>";         
            $queryLogTrans = $this->db->query($insLogTrans);
			
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            $queryPaydet = $this->db->query($insPaydet);
            
            
			//di set 1 saat live
			$st_vt_pay = "1";
			
			//tambahan dion 09/10/2015
			if($row->bank_code_payment == null || $row->bank_code_payment == "") {
				$charge_admin = 0;
				$charge_connectivity = 0;
				$bank_code_pay = 1;
			} else {
				$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
				$querypayADM = $this->db->query($payADM);
				$payresadm = $querypayADM->result();
				$charge_admin = $payresadm[0]->charge_admin;
				$charge_connectivity = $payresadm[0]->charge_connectivity;
				$bank_code_pay = $row->bank_code_payment;
			}
			
			$is_ship = 0;
			if($row->sentTo == "2") {
				$is_ship = 1;
			}
			
			
			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity) 
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			             '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.");
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd 
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
						SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						FROM 	ecomm_trans_det_prd_sgo a
						WHERE 	a.orderno='".$order_id."'"; 
						
						 
            //echo "header<br>".$insHeader."<br><br>";
			//print_r($insHeader);
            $queryHeader = $this->db->query($insHeader);
            
			//$qryDet = "SELECT * FROM ecomm_trans_det_prd_sgo WHERE orderno = '$order_id'";
			
	/*
			$qryDet = "INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd 
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
						SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						FROM 	ecomm_trans_det_prd_sgo a
						WHERE 	a.orderno='".$order_id."'";
		    echo "$qryDet< br />";
		    $queryDet = $this->db->query($qryDet);
	 */
			//$num = $queryDet->num_rows();
			/*
			$jum = 0;
			foreach($queryDet->result() as $rowx)
            {
                //echo "sebelum eksekusi db produk<br>";
                $prdcd = $rowx->prdcd;
                $qty = (int) $rowx->qty;
                $bv = (int) $rowx->bvr;
                $prdnm = $rowx->prdnm;
                $price = (int) $rowx->dpr;
                
           	    $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$rowx->pricecode."','".$rowx->sentTo."')";
                //echo "produk <br>".$inputProd."<br><br>";
                $queryProd = $this->db->query($inputProd);
				if($queryProd > 0) {
					$jum++;
				} 
            }
            */
			
			/*if($queryCheckout > 0 && $queryLogTrans > 0 && $queryPaydet > 0 &&  
			   $queryHeader > 0)
	        {
	            
				return 1;
	        }
	        else
	        {
	            return 0;
	        }*/		
		}		
			
	}

	function insertEcommShipAddr($orderid, $personal_info, $resJNE) {
		//$personal_info = $this->session->userdata('personal_info');
		$this->db = $this->load->database('db_ecommerce', true);	
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		if($personal_info['delivery'] === "2") {
			$prov_code = $personal_info['provinsi'];
		    $kab_code = $personal_info['kota'];
			$kec_code = $personal_info['kecamatan'];
			$kel_code = "";
			$addr1 = trim($personal_info['alamat']);
			$addr2 = "";
			$addr3 = "";
	        $tel_hp1 = $personal_info['notlp'];
	        $tel_hp2 = $personal_info['notlp'];
	        $email = $personal_info['email'];
			$service_type_id = $shipping_jne_info['service_code'];
			$service_type_name = $shipping_jne_info['service_display'];
			$is_ship = "1";
		} else {
			$prov_code = $personal_info['provinsi'];
		    $kab_code = $personal_info['kota'];
			$kec_code = $personal_info['kecamatan'];
			$kel_code = "";
			$addr1 = "";
			$addr2 = "";
			$addr3 = "";
			$tel_hp1 = "";
	        $tel_hp2 = "";
	        $email = "";
			$service_type_id = "";
			$service_type_name = "";
			$is_ship = "0";
		}	
		
		$qry = "SELECT * FROM ecomm_trans_hdr WHERE token = '$orderid'";
		$query = $this->db->query($qry);
		 foreach ($query->result() as $row) {
		 	 $insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr 
            					(orderno,idstk,prov_code,kab_code,kec_code,kel_code,addr1,addr2,addr3,email,tel_hp1,tel_hp2, 
            					 conoteJNE, service_type_id, service_type_name)
                             values('".$row->orderno."','".$row->idstk."','".$prov_code."','".$kab_code."',
                                    '".$kec_code."','".$kel_code."','".$addr1."','".$addr2."','".$addr3."',
                                    '".$email."','".$tel_hp1."','".$tel_hp2."', 
                                    '$resJNE','$service_type_id','$service_type_name')";
          //echo "ShipAddr <br>".$insShipAddr."<br><br>"; 
          $queryShipAddr = $this->db->query($insShipAddr);
		 }
		 return $queryShipAddr;
	}

	function updateEcommShipAddr() {
		
	}

	function insertTrxTblMemberReg($newid, $order_id, $orderno, $resi) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		/*
		$personal_info = $this->session->userdata('personal_info');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$pricecode = $this->session->userdata('pricecode');
		
		if($personal_info['delivery'] === "2") {
			$prov_code = $personal_info['provinsi'];
		    $kab_code = $personal_info['kota'];
			$kec_code = $personal_info['kecamatan'];
			$kel_code = "";
			$addr1 = trim($personal_info['alamat']);
			$addr2 = "";
			$addr3 = "";
	        $tel_hp1 = $personal_info['notlp'];
	        $tel_hp2 = $personal_info['notlp'];
	        $email = $personal_info['email'];
			$service_type_id = $shipping_jne_info['service_code'];
			$service_type_name = $shipping_jne_info['service_display'];
			$is_ship = "1";
		} else {
			$prov_code = $personal_info['provinsi'];
		    $kab_code = $personal_info['kota'];
			$kec_code = $personal_info['kecamatan'];
			$kel_code = "";
			$addr1 = "";
			$addr2 = "";
			$addr3 = "";
			$tel_hp1 = "";
	        $tel_hp2 = "";
	        $email = "";
			$service_type_id = "";
			$service_type_name = "";
			$is_ship = "0";
		}	*/
		//$orderno = $this->getsOrderno();
        $secno = $this->randomChars(5);
		
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $this->db->query($qry);
		foreach ($query->result() as $row) {
			/*$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$newid."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".getUserID()."','".$orderno."')";
            
			 */
		    $insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$newid."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";	 
			//echo "checout <br>".$insCheckout."<br><br>";          
            $queryCheckout = $this->db->query($insCheckout);
            
			//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
			$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;
            /*
            $insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$newid."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".getUserID()."')";
			 */
			
			$addr1 = "";
			
			$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$newid."','".$datetrans."','".$orderno."','".strtoupper($addr1)."',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')"; 
            //echo "LogTrans <br>".$insLogTrans."<br><br>";         
            $queryLogTrans = $this->db->query($insLogTrans);
			
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            $queryPaydet = $this->db->query($insPaydet);
            
            /*$insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr 
            					(orderno,idstk,prov_code,kab_code,kec_code,kel_code,addr1,addr2,addr3,email,tel_hp1,tel_hp2, 
            					 conoteJNE, service_type_id, service_type_name)
                             values('".$orderno."','".$row->idstk."','".$prov_code."','".$kab_code."',
                                    '".$kec_code."','".$kel_code."','".$addr1."','".$addr2."','".$addr3."',
                                    '".$email."','".$tel_hp1."','".$tel_hp2."', 
                                    '$resi','$service_type_id','$service_type_name')";
            //echo "ShipAddr <br>".$insShipAddr."<br><br>"; 
            $queryShipAddr = $this->db->query($insShipAddr);
			*/
			//di set 1 saat live
			$st_vt_pay = "1";
			
			//tambahan dion 09/10/2015
			$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
			$querypayADM = $this->db->query($payADM);
			$payresadm = $querypayADM->result();
			
			/*$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity) 
			             values('".$orderno."','0','".$order_id."','".$newid."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			             '".$secno."','W','".$personal_info['delivery']."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$row->payAdm.", '".$is_ship."',".$payresadm[0]->charge_admin.", '".$is_ship."', 
			             ".$row->bank_code_payment.", ".$payresadm[0]->charge_connectivity.")";
		
			 */
			 if($row->sentTo == "1") {
			 	$is_ship = "0";
			 }  else {
			 	$is_ship = "1";
			 }
			 
			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr 
			                  (orderno,bankaccno,token,id_memb,nmmember,total_pay,
			                   total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							   secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,
							   payShip, payAdm, is_ship, 
							   bank_code_payment, payConnectivity) 
			             values('".$orderno."','0','".$order_id."','".$newid."','".strtoupper($row->nmmember)."',$row->total_pay,
			                  $row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".strtoupper($row->nmstkk)."',
			                  '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',
			                  ".$row->payShip.",".$row->payAdm.", '".$is_ship."',  
			                 ".$row->bank_code_payment.", ".$payresadm[0]->charge_connectivity.")"; 
            //echo "header<br>".$insHeader."<br><br>";
            $queryHeader = $this->db->query($insHeader);
            
			$qryDet = "SELECT * FROM ecomm_trans_det_prd_sgo WHERE orderno = '$order_id'";
		    //echo "$qryDet< br />";
		    $queryDet = $this->db->query($qryDet);
			//$num = $queryDet->num_rows();
			$jum = 0;
			foreach($queryDet->result() as $rowx)
            {
                //echo "sebelum eksekusi db produk<br>";
                $prdcd = $rowx->prdcd;
                $qty = (int) $rowx->qty;
                $bv = (int) $rowx->bvr;
                $prdnm = $rowx->prdnm;
                $price = (int) $rowx->dpr;
                
                /*$inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$pricecode."','".$personal_info['delivery']."')";
           	    */
           	    $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$rowx->pricecode."','".$row->sentTo."')";
                //echo "produk <br>".$inputProd."<br><br>";
                $queryProd = $this->db->query($inputProd);
				if($queryProd > 0) {
					$jum++;
				} 
            }
            
            
			$pay_tipe = "0";
			$voucherno = "";
			$voucherkey = "";
			$kdpos1 =  "000004";
			$qryInsMemb = "SELECT a.sponsorid, a.membername, a.idno, a.addr1, a.addr2,
					    a.addr3,a.tel_hp,a.email,a.stk_code,a.sex,birthdt,
					    a.acc_no,a.acc_name,a.bankid,a.joindt,a.trx_no,a.kdpos,
					    a.[state],a.ip_address,a.[password],a.userlogin,a.recruiterid,
					    a.flag_voucher, a.voucher_no, a.voucher_key,
					    c.prdcd, a.is_landingpage, a.id_landingpage
					FROM ecomm_memb_ok_sgo a
					INNER JOIN ecomm_trans_det_prd_sgo c ON (a.memberid = c.orderno) 
					WHERE a.memberid = '$order_id'";
			$checkQry = $this->db->query($qryInsMemb);
			foreach($checkQry->result() as $dtax) {
			    $membername = strtoupper($dtax->membername);
			    $addr1 = strtoupper($dtax->addr1);
				$addr2 = strtoupper($dtax->addr2);
				$addr3 = strtoupper($dtax->addr3);
				$acc_name = strtoupper($dtax->acc_name);
				$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
		                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
		                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
		                    flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
		                    values ('".$dtax->sponsorid."','".$newid."','".strtoupper($membername)."','".$dtax->idno."','".strtoupper($addr1)."',
		                            '".strtoupper($addr2)."','".strtoupper($addr3)."','".$dtax->tel_hp."','".$dtax->email."','".$dtax->stk_code."','".$dtax->sex."',
		                            '".$dtax->birthdt."','".$dtax->acc_no."','".strtoupper($acc_name)."','".$dtax->bankid."',
		                            '".$this->dateTime."','".$orderno."','".$kdpos1."','".$dtax->state."','".$dtax->ip_address."','".$dtax->prdcd."','".$dtax->password."','".$dtax->recruiterid."','".$dtax->recruiterid."',
		                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '0' , '".$this->id_landingpage."')";
		        //echo "<br />".$insMemb;
				//$res = $this->executeQuery($insMemb, $this->setDB(1));
				$query = $this->db->query($insMemb);
				/*if($res > 0) {
					$res = $this->show_member_new2($new_id);
				} */
			
			}
			
			
		}
		
		
	    return 1;
	       	
	}
	
	function insertTrxAndDataMember($newid, $order_id, $orderno, $is_landingpage) {
		$qryx_db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		 $secno = $this->randomChars(5);
		
		$qry = "SELECT a.idstk, a.nmstkk, a.total_pay, a.total_bv, a.payShip, a.payAdm, a.payConnectivity, 
				       a.bank_code_payment,a.sentTo, b.membername, a.pricecode,
				       a.bonusmonth, c.charge_connectivity,
				       b.sponsorid, b.idno, b.addr1, b.addr2,
					   b.addr3,b.tel_hp,b.email,b.stk_code,b.sex,b.birthdt,
					   b.acc_no,b.acc_name,b.bankid,b.joindt,b.trx_no,b.kdpos,
					   b.[state],b.ip_address,b.[password],b.recruiterid,
					   b.flag_voucher, b.voucher_no, b.voucher_key,
				       a.userlogin, d.prdcd, d.prdnm, d.qty, d.dpr, d.bvr, b.is_landingpage, b.id_landingpage, a.free_shipping, a.discount_shipping
				FROM ecomm_trans_hdr_sgo a
				INNER JOIN ecomm_memb_ok_sgo b ON (a.orderno = b.trx_no)
				INNER JOIN ecomm_bank c ON (a.bank_code_payment = c.id)
				INNER JOIN ecomm_trans_det_prd_sgo d ON (a.orderno = d.orderno)
				WHERE a.orderno = '$order_id'";
		$query = $qryx_db->query($qry);
		foreach ($query->result() as $row) {
			//Jika daftar dari landing page, userlogin adalah ID Member itu sndr
			if($is_landingpage == "1") {
				$usrlogina = $newid;
				//$id_lp = $id_lp;
			} else {
				$usrlogina = $row->userlogin;
			}
			
			$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$newid."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$usrlogina."','".$orderno."')";	 
			//echo "checout <br>".$insCheckout."<br><br>";          
            $queryCheckout = $qryx_db->query($insCheckout);
            
            $gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;
            
            $addr1 = "";
			$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$newid."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')"; 
            //echo "LogTrans <br>".$insLogTrans."<br><br>";         
            $queryLogTrans = $qryx_db->query($insLogTrans);
			
			//UPDATED by DION @ 09/08/2016
			//from this
			/*$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            $queryPaydet = $this->db->query($insPaydet);
			*/
			//to this
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
			              SELECT '".$orderno."', seqno,paytype,docno, payamt, 'trx ok','capture', bank_code_payment, charge_admin
						  FROM 	db_ecommerce.dbo.ecomm_trans_paydet_sgo a
						  WHERE a.orderno='".$order_id."'"; 
						//"values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            $queryPaydet = $qryx_db->query($insPaydet);
			
			//end updated
			
			if($row->sentTo == "1") {
				$sentTo = "1";
			 	$is_ship = "0";
			 }  else {
			 	$sentTo = "2";
			 	$is_ship = "1";
			 }
			//di set 1 saat live
			$st_vt_pay = "1"; 
			
			
			
			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr 
			                  (orderno,bankaccno,token,id_memb,nmmember,total_pay,
			                   total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							   secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,
							   payShip, payAdm, is_ship, 
							   bank_code_payment, payConnectivity, free_shipping, discount_shipping) 
			             values('".$orderno."','0','".$order_id."','".$newid."','".strtoupper($row->membername)."',$row->total_pay,
			                  $row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                  '".$secno."','W','$sentTo','$st_vt_pay','".$datetrans."',
			                  ".$row->payShip.",".$row->payAdm.", '".$is_ship."',  
			                 ".$row->bank_code_payment.", ".$row->payConnectivity.", '".$row->free_shipping."', ".$row->discount_shipping.")"; 
            //echo "header<br>".$insHeader."<br><br>";
            $queryHeader = $qryx_db->query($insHeader); 
			
			$prdcd = $row->prdcd;
            $qty = (int) $row->qty;
            $bv = (int) $row->bvr;
            $prdnm = $row->prdnm;
            $price = (int) $row->dpr;
            
			//if($queryHeader > 0) {
				$inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
							  values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$row->pricecode."','".$row->sentTo."')";
				//echo "produk <br>".$inputProd."<br><br>";
				//echo $inputProd;
				$queryProd = $qryx_db->query($inputProd);
				
				$pay_tipe = "0";
				$voucherno = "";
				$voucherkey = "";
				$kdpos1 =  "000004";
				
				//Jika daftar dari landing page, userlogin adalah ID Member itu sndr
				if($is_landingpage == "1") {
					$usrloginx = $newid;
				} else {
					//$usrloginx = $row->recruiterid;
					$usrloginx = $row->userlogin;
				}
				
				//update by dion 21/01/2019
				//klo di ecomm_trans_hdr ngga ada, jgn diinsert ke ecomm_memb_ok
				
				$cekHdr = $this->checkIfTrxExist($orderno);
				if($cekHdr != null) {
				
					$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
									addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
									acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
									flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
									values ('".$row->sponsorid."','".$newid."','".strtoupper($row->membername)."','".$row->idno."','".strtoupper($row->addr1)."',
											'".strtoupper($row->addr2)."','".strtoupper($row->addr3)."','".$row->tel_hp."','".$row->email."','".$row->stk_code."','".$row->sex."',
											'".$row->birthdt."','".$row->acc_no."','".strtoupper($row->acc_name)."','".$row->bankid."',
											'".$this->dateTime."','".$orderno."','".$kdpos1."','".$row->state."','".$row->ip_address."','".$row->prdcd."',
											'".$row->password."','".$usrloginx."','".$row->recruiterid."',
											'".$pay_tipe."','".$voucherno."','".$voucherkey."', '".$row->is_landingpage."', '".$row->id_landingpage."')";
					//echo "<br />".$insMemb;
					//$res = $this->executeQuery($insMemb, $this->setDB(1));
					$queryInsMemb = $qryx_db->query($insMemb);
				}
			//}
		}	
		
		return 1;	
	 }

	function insertTrxTblEcommLP($newid, $order_id, $orderno, $resi) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		/*
		$personal_info = $this->session->userdata('personal_info');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$pricecode = $this->session->userdata('pricecode');
		
		if($personal_info['delivery'] === "2") {
			$prov_code = $personal_info['provinsi'];
		    $kab_code = $personal_info['kota'];
			$kec_code = $personal_info['kecamatan'];
			$kel_code = "";
			$addr1 = trim($personal_info['alamat']);
			$addr2 = "";
			$addr3 = "";
	        $tel_hp1 = $personal_info['notlp'];
	        $tel_hp2 = $personal_info['notlp'];
	        $email = $personal_info['email'];
			$service_type_id = $shipping_jne_info['service_code'];
			$service_type_name = $shipping_jne_info['service_display'];
			$is_ship = "1";
		} else {
			$prov_code = $personal_info['provinsi'];
		    $kab_code = $personal_info['kota'];
			$kec_code = $personal_info['kecamatan'];
			$kel_code = "";
			$addr1 = "";
			$addr2 = "";
			$addr3 = "";
			$tel_hp1 = "";
	        $tel_hp2 = "";
	        $email = "";
			$service_type_id = "";
			$service_type_name = "";
			$is_ship = "0";
		}	*/
		//$orderno = $this->getsOrderno();
        $secno = $this->randomChars(5);
		
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $this->db->query($qry);
		foreach ($query->result() as $row) {
			/*$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$newid."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".getUserID()."','".$orderno."')";
            
			 */
		    $insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno) 
          		        values('".$newid."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";	 
			//echo "checout <br>".$insCheckout."<br><br>";          
            $queryCheckout = $this->db->query($insCheckout);
            
			//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
			$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;
            /*
            $insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$newid."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".getUserID()."')";
			 */
			
			$addr1 = "";
			
			$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin) 
          		        values('".$newid."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')"; 
            //echo "LogTrans <br>".$insLogTrans."<br><br>";         
            $queryLogTrans = $this->db->query($insLogTrans);
			
			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>"; 
            $queryPaydet = $this->db->query($insPaydet);
            
            /*$insShipAddr = "insert into db_ecommerce.dbo.ecomm_trans_shipaddr 
            					(orderno,idstk,prov_code,kab_code,kec_code,kel_code,addr1,addr2,addr3,email,tel_hp1,tel_hp2, 
            					 conoteJNE, service_type_id, service_type_name)
                             values('".$orderno."','".$row->idstk."','".$prov_code."','".$kab_code."',
                                    '".$kec_code."','".$kel_code."','".$addr1."','".$addr2."','".$addr3."',
                                    '".$email."','".$tel_hp1."','".$tel_hp2."', 
                                    '$resi','$service_type_id','$service_type_name')";
            //echo "ShipAddr <br>".$insShipAddr."<br><br>"; 
            $queryShipAddr = $this->db->query($insShipAddr);
			*/
			//di set 1 saat live
			$st_vt_pay = "1";
			
			//tambahan dion 09/10/2015
			$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
			$querypayADM = $this->db->query($payADM);
			$payresadm = $querypayADM->result();
			
			/*$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity) 
			             values('".$orderno."','0','".$order_id."','".$newid."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			             '".$secno."','W','".$personal_info['delivery']."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$row->payAdm.", '".$is_ship."',".$payresadm[0]->charge_admin.", '".$is_ship."', 
			             ".$row->bank_code_payment.", ".$payresadm[0]->charge_connectivity.")";
		
			 */
			 if($row->sentTo == "1") {
			 	$is_ship = "0";
			 }  else {
			 	$is_ship = "1";
			 }
			 
			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr 
			                  (orderno,bankaccno,token,id_memb,nmmember,total_pay,
			                   total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							   secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,
							   payShip, payAdm, is_ship, 
							   bank_code_payment, payConnectivity) 
			             values('".$orderno."','0','".$order_id."','".$newid."','".$row->nmmember."',$row->total_pay,
			                  $row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                  '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',
			                  ".$row->payShip.",".$row->payAdm.", '".$is_ship."',  
			                 ".$row->bank_code_payment.", ".$payresadm[0]->charge_connectivity.")"; 
            //echo "header<br>".$insHeader."<br><br>";
            $queryHeader = $this->db->query($insHeader);
            
			$qryDet = "SELECT * FROM ecomm_trans_det_prd_sgo WHERE orderno = '$order_id'";
		    //echo "$qryDet< br />";
		    $queryDet = $this->db->query($qryDet);
			//$num = $queryDet->num_rows();
			$jum = 0;
			foreach($queryDet->result() as $rowx)
            {
                //echo "sebelum eksekusi db produk<br>";
                $prdcd = $rowx->prdcd;
                $qty = (int) $rowx->qty;
                $bv = (int) $rowx->bvr;
                $prdnm = $rowx->prdnm;
                $price = (int) $rowx->dpr;
                
                /*$inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$pricecode."','".$personal_info['delivery']."')";
           	    */
           	    $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$orderno."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$rowx->pricecode."','".$row->sentTo."')";
                //echo "produk <br>".$inputProd."<br><br>";
                $queryProd = $this->db->query($inputProd);
				if($queryProd > 0) {
					$jum++;
				} 
            }
            
            
			$pay_tipe = "0";
			$voucherno = "";
			$voucherkey = "";
			$kdpos1 =  "000004";
			$qryInsMemb = "SELECT a.sponsorid, a.membername, a.idno, a.addr1, a.addr2,
					    a.addr3,a.tel_hp,a.email,a.stk_code,a.sex,birthdt,
					    a.acc_no,a.acc_name,a.bankid,a.joindt,a.trx_no,a.kdpos,
					    a.[state],a.ip_address,a.[password],a.userlogin,a.recruiterid,
					    a.flag_voucher, a.voucher_no, a.voucher_key,
					    c.prdcd, a.id_landingpage
					FROM ecomm_memb_ok_sgo a
					INNER JOIN ecomm_trans_det_prd_sgo c ON (a.memberid = c.orderno) 
					WHERE a.memberid = '$order_id'";
			$checkQry = $this->db->query($qryInsMemb);
			foreach($checkQry->result() as $dtax) {
			
				$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
		                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
		                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
		                    flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
		                    values ('".$dtax->sponsorid."','".$newid."','".$dtax->membername."','".$dtax->idno."','".$dtax->addr1."',
		                            '".$dtax->addr2."','".$dtax->addr3."','".$dtax->tel_hp."','".$dtax->email."','".$dtax->stk_code."','".$dtax->sex."',
		                            '".$dtax->birthdt."','".$dtax->acc_no."','".$dtax->acc_name."','".$dtax->bankid."',
		                            '".$this->dateTime."','".$orderno."','".$kdpos1."','".$dtax->state."','".$dtax->ip_address."','".$dtax->prdcd."','".$dtax->password."','".$dtax->recruiterid."','".$dtax->recruiterid."',
		                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '1', '".$dtax->id_landingpage."')";
		        //echo "<br />".$insMemb;
				//$res = $this->executeQuery($insMemb, $this->setDB(1));
				$query = $this->db->query($insMemb);
				/*if($res > 0) {
					$res = $this->show_member_new2($new_id);
				} */
			
			}
			
			
		}
		
		
	    return 1;
	       	
			
	}

	function sendTrxSMS($orderno, $total, $secno) {
		$alternate2 = $this->load->database('alternate2', true);
		//$text = 'K-LINK : Trx berhasil,No Order: '.$orderno.',Jumlah Trx: '.$total.',Kode Ambil Brg: '.$secno.'';
		$text = 'K-LINK : Trx berhasil,No Order: '.$orderno.',Kode Ambil Brg: '.$secno.'';
		$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID) 
		                    VALUES ('".getUserPhone()."','".$text."','K-Link Transaction Status', 'MyPhone6')";
		        //echo "Ins SMS : $insSms";
	    $query = $alternate2->query($insSms);	
	}

	function sendTrxSmsMampirKak($data) {

		if($data[0]->sentTo == "1") {
			$ship = 'Kode Ambil Brg: '.$data[0]->secno;
			$shipRyan = 'SC : '.$data[0]->secno;
		} else {
			if($data[0]->cargo_id == "2") {
			    $shipRyan = 'Resi : '.$data[0]->conoteJNE;
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
			} elseif($data[0]->cargo_id == "4") {
				/*$this->load->model('lokasi/M_location', 'M_location');
				$getKey= $this->M_location->getUserKey();
				$user_key= $getKey[0]->username;
				$pass_key= $getKey[0]->key_user;
				$tracking="-";

				$orderno = $data[0]->conoteJNE;
				$dt =status_gsend($user_key,$pass_key,$orderno);
				$tracking= $dt['liveTrackingUrl'];*/

				$shipRyan = 'Resi : '.$data[0]->conoteJNE;
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/gosendtrack';
			}else
			 {
			    $shipRyan = 'Resi : '.$data[0]->conoteJNE;
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';
			}
			
		}
		//$alternate2 = $this->load->database('alternate2', true);
		//$text = 'Trx sukses,No Order: '.$data[0]->orderno.',Jumlah Trx: '.$data[0]->total_pay.','.$ship.'';
		$text = 'Trx sukses,No Order: '.$data[0]->orderno.','.$ship.'';
		if($data[0]->tel_hp != null) {
			
			$send = smsTemplate($data[0]->tel_hp, $text);
		}
		
		if($data[0]->no_hp_konfirmasi != null) {
			
			$send = smsTemplate($data[0]->no_hp_konfirmasi, $text);
		}
		
		//$send = smsTemplate("087780441874", $text);
		
		$textDion = 'K-NET : '.$data[0]->orderno.' / '.$data[0]->token.', Pay/BV : '.$data[0]->total_pay.' / '.$data[0]->total_bv.', MEMBER : '.$data[0]->id_memb.' / '.$data[0]->nmmember.'';
		//$sendDion = smsTemplate("087780441874", $textDion);
		
		$textRyan = 'Orderno: '.$data[0]->orderno.', Pay/BV : '.$data[0]->total_pay.' / '.$data[0]->total_bv.', MEMBER : '.$data[0]->nmmember.', '.$shipRyan.'';
		$sendRyan = smsTemplate("087784698951", $textRyan);
		
		
		$recName = substr($data[0]->receiver_name,10);
		$textCust = 'K-NET : '.$recName.', Trx sukses,No Order: '.$data[0]->orderno.','.$ship.'';
		//$sendDion = smsTemplate("087780441874", $textDion);
		//$send = smsTemplate("087784698951", $text);
		if($data[0]->tel_hp1 != null || $data[0]->tel_hp1 != "") {
			$send = smsTemplate($data[0]->tel_hp1, $textCust);
		}
			
	}
	
	function getTrxSalesPlusReg($orderno) {
		$qry = "SELECT c.*, b.log_usrlogin, z1.tel_hp as tel_hp_login,
				b.log_dfno, z2.tel_hp as tel_hp_as,
				d.conoteJNE, d.cargo_id, d.addr1, d.receiver_name, d.tel_hp1, d.tel_hp1 as telp_customer,
				z1.email
				FROM ecomm_trans_hdr c
					inner join log_trans b on c.orderno = b.log_trcd
					inner join klink_mlm2010.dbo.msmemb z1 on (b.log_usrlogin COLLATE SQL_Latin1_General_CP1_CS_AS = z1.dfno)
					inner join klink_mlm2010.dbo.msmemb z2 on (c.id_memb COLLATE SQL_Latin1_General_CP1_CS_AS = z2.dfno)
					left outer join ecomm_trans_shipaddr d ON c.orderno=d.orderno
				where c.token='$orderno'";
		$res = $this->getRecordset($qry);
		return $res;
	}
	
	function sendSMSPromoSalesAndReg($data) {
		if($data[0]->sentTo == "1") {
			$ship = 'Kode Ambil Brg: '.$data[0]->secno;
			$shipRyan = 'SC : '.$data[0]->secno;
		} else if($data[0]->sentTo != "1" && $data[0]->cargo_id == "2") {
			$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
			
			
		} else if($data[0]->sentTo != "1" && $data[0]->cargo_id == "1"){
			$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';	
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
		}

		//gosend
		else if($data[0]->sentTo != "1" && $data[0]->cargo_id == "4"){
			$ship = 'Conote: '.$data[0]->conoteJNE.', Tracking : http://bit.ly/gosendtrack';
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
		}
		
		$text = 'Trx sukses,No Order: '.$data[0]->orderno.','.$ship.'';
		//BILA YG BELANJA DAN PEMILIK BV ADALAH ORG YG SAMA (BELANJA UNTUK DIRI NYA SENDIRI)
		/*if($data[0]->tel_hp_login != null && $data[0]->tel_hp_login != "" && $data[0]->tel_hp_login == $data[0]->tel_hp_as) {
			$send = smsTemplate($data[0]->tel_hp_login, $text);
		} else { */
			if($data[0]->tel_hp_login != null && $data[0]->tel_hp_login != "") {
				$send = smsTemplate($data[0]->tel_hp_login, $text);
			}
			
			if($data[0]->tel_hp_as != null && $data[0]->tel_hp_as != "") {
				$send = smsTemplate($data[0]->tel_hp_as, $text);
			}
			//$send = smsTemplate("087780441874", $text);
		//}
		//$send = smsTemplate("087780441874", $text);
		//$send = smsTemplate("087784698943", $text);
		$textRyan = 'Orderno: '.$data[0]->orderno.', Pay/BV : '.$data[0]->total_pay.' / '.$data[0]->total_bv.', MEMBER : '.$data[0]->nmmember.', '.$shipRyan.'';
		$sendRyan = smsTemplate("087784698951", $textRyan);
		//$textElin = smsTemplate("087780441874", $textRyan);
	}


	function sendTrxSMS2($data) {
	    $ship = "";
		$shipRyan = "";
		
		if($data[0]->sentTo == "1") {
			$ship = 'Kode Ambil Brg: '.$data[0]->secno;
			$shipRyan = 'SC : '.$data[0]->secno;
		} else if($data[0]->sentTo != "1" && $data[0]->cargo_id == "2") {
			$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
			/*if($data[0]->cargo_id == "2") {
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
			} else {
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';	
			}*/
			
		} else if($data[0]->sentTo != "1" && $data[0]->cargo_id == "1"){
			$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';	
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
		}

		//gosend
		else if($data[0]->sentTo != "1" && $data[0]->cargo_id == "4"){
			/*$this->load->model('lokasi/M_location', 'M_location');
			$getKey= $this->M_location->getUserKey();
			$user_key= $getKey[0]->username;
			$pass_key= $getKey[0]->key_user;
			$tracking="-";

			$orderno = $data[0]->conoteJNE;
			$dt =status_gsend($user_key,$pass_key,$orderno);
			$tracking= $dt['liveTrackingUrl'];*/
			$ship = 'Conote: '.$data[0]->conoteJNE.', Tracking : http://bit.ly/gosendtrack';
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
		}
		//$alternate2 = $this->load->database('alternate2', true);
		//$text = 'Trx sukses,No Order: '.$data[0]->orderno.',Jumlah Trx: '.$data[0]->total_pay.','.$ship.'';
		$text = 'Trx sukses,No Order: '.$data[0]->orderno.','.$ship.'';
		if($data[0]->log_usrlogin == $data[0]->log_dfno && $data[0]->tel_hp_login != null && $data[0]->tel_hp_login != "") {
			$send = smsTemplate($data[0]->tel_hp_login, $text);
			//$send = smsTemplate("081513622655", $text);
			//$send = smsTemplate("087780441874", $text);
		} else if($data[0]->log_usrlogin != $data[0]->log_dfno){
			//$send = smsTemplate($data[0]->tel_hp_as, $text);
			//$send = smsTemplate("087784698951", $text);
			if($data[0]->tel_hp_login != null && $data[0]->tel_hp_login != "") {
				$send = smsTemplate($data[0]->tel_hp_login, $text);
			}
			
			
			if($data[0]->tel_hp_as != null && $data[0]->tel_hp_as != "") {
				
				$send = smsTemplate($data[0]->tel_hp_as, $text);
			}
		}
		//$send = smsTemplate("087780441874", $text);
		//$textCust = 'K-NET : Trx sukses,No Order: '.$data[0]->orderno.','.$ship.'';
		$recName = substr($data[0]->receiver_name,10);
		$textCust = 'K-NET : '.$recName.', Trx sukses,No Order: '.$data[0]->orderno.','.$ship.'';
		if($data[0]->tel_hp1 != null || $data[0]->tel_hp1 != "") {
			$send = smsTemplate($data[0]->tel_hp1, $textCust);
		}
		//$sendx = smsTemplate("081210111511", $textCust);
		//$sendx = smsTemplate("087780441874", $textCust);
		
		
		$textDion = 'K-NET : '.$data[0]->orderno.' / '.$data[0]->token.', Pay/BV : '.$data[0]->total_pay.' / '.$data[0]->total_bv.', MEMBER : '.$data[0]->id_memb.' / '.$data[0]->nmmember.'';
		
		$textRyan = 'Orderno: '.$data[0]->orderno.', Pay/BV : '.$data[0]->total_pay.' / '.$data[0]->total_bv.', MEMBER : '.$data[0]->nmmember.', '.$shipRyan.'';
		$sendRyan = smsTemplate("087784698951", $textRyan);
		//$sendElin = smsTemplate("087780441874", $textRyan);
		//$send = smsTemplate("08121028554", $text);
		//$send = smsTemplate("087780441874", $textDion);
		//$send = smsTemplate("087780441874", $textRyan);

		
		//$sendDion = smsTemplate("087780441874", $textDion);
			
	}
	
	function sendTrxSMSNonMember($data) {
		
		if($data[0]->sentTo == "1") {
			$ship = 'Kode Ambil Brg: '.$data[0]->secno;
		} else {
			$ship = 'Conote: '.$data[0]->conoteJNE;
		}
		$alternate2 = $this->load->database('alternate2', true);
		$text = 'K-LINK : Trx sukses,No Order: '.$data[0]->orderno.',Jumlah Trx: '.$data[0]->total_pay.','.$ship.'';
		$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID) 
		                    VALUES ('".$data[0]->telp_customer."','".$text."','K-Link Transaction Status', SenderID)";
		        //echo "Ins SMS : $insSms";
	    $query = $alternate2->query($insSms);
		//return $query;
	    //return $insSms;	
	}

	function getTrxSGOByOrderID($orderid) {
		//$qry = "SELECT * FROM ecomm_trans_hdr WHERE token = '$orderid'";
		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$orderid'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	//MOVED TO SHARED MODULE
	/*
	function getDataPaymentSGOByOrderID($orderid) {
		//$qry = "SELECT * FROM ecomm_trans_hdr WHERE token = '$orderid'";
		$qry = "SELECT a.orderno, a.total_pay, a.payShip, b.charge_connectivity, b.charge_admin, a.idstk 
		        FROM ecomm_trans_hdr_sgo a
		        INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id) 
		        WHERE a.orderno = '$orderid'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	*/
	function getInsertTrxOrderID($orderid) {
		//$qry = "SELECT * FROM ecomm_trans_hdr WHERE token = '$orderid'";
		/*$qry = "SELECT TOP 1 A.*, B.conoteJNE, B.tel_hp1 as telp_customer
				FROM ecomm_trans_hdr A
					 LEFT OUTER JOIN ecomm_trans_shipaddr B ON A.orderno=B.orderno 
				WHERE token = '$orderid'"; */
				
		/*$qry = "SELECT a.dfno, b.log_usrlogin, 
					   a.tel_hp as tel_hp_login, b.log_dfno, a1.tel_hp as tel_hp_as,
					   b.log_sento, b.log_status_trx, 
				       c.*, d.conoteJNE
				from klink_mlm2010.dbo.msmemb a
					 inner join log_trans b on a.dfno=b.log_usrlogin collate SQL_Latin1_General_CP1_CS_AS
				     inner join klink_mlm2010.dbo.msmemb a1 on b.log_dfno=a1.dfno collate SQL_Latin1_General_CP1_CS_AS
				     inner join ecomm_trans_hdr c on b.log_trcd=c.orderno
				     left outer join ecomm_trans_shipaddr d ON c.orderno=d.orderno 
				where c.token='$orderid'";*/
				
		$qry = "DECLARE @is_login as CHAR(1)
			DECLARE @token as VARCHAR(25)
			
			SET @token = '$orderid'
			
			SELECT @is_login = A.is_login
			FROM ecomm_trans_hdr A
			WHERE A.token=@token
			
			--PRINT @is_login
			
			IF @is_login = '1'
				BEGIN
					SELECT a.dfno, b.log_usrlogin, 
					   a.tel_hp as tel_hp_login, b.log_dfno, a1.tel_hp as tel_hp_as,
					   b.log_sento, b.log_status_trx, d.tel_hp1 as telp_customer, a.email,
					    c.*, d.conoteJNE, d.cargo_id, d.addr1, d.receiver_name, d.tel_hp1
					from klink_mlm2010.dbo.msmemb a
					inner join log_trans b on a.dfno=b.log_usrlogin collate SQL_Latin1_General_CP1_CS_AS
					     inner join klink_mlm2010.dbo.msmemb a1 on b.log_dfno=a1.dfno collate SQL_Latin1_General_CP1_CS_AS
					     inner join ecomm_trans_hdr c 
						 on (b.log_trcd=c.orderno and c.total_pay = b.log_totaldp_sales and c.id_memb = b.log_dfno)
					     left outer join ecomm_trans_shipaddr d ON c.orderno=d.orderno 
					where c.token=@token
				END
			ELSE
				BEGIN
					SELECT a.dfno, b.log_usrlogin, 
					   a.tel_hp as tel_hp_login, b.log_dfno, a1.tel_hp as tel_hp_as,
					   b.log_sento, b.log_status_trx, d.tel_hp1 as telp_customer, a.email,
					       c.*, d.conoteJNE, d.cargo_id, d.addr1, d.receiver_name, d.tel_hp1
					from klink_mlm2010.dbo.msmemb a
					inner join log_trans b on a.dfno=b.log_dfno collate SQL_Latin1_General_CP1_CS_AS
					     inner join klink_mlm2010.dbo.msmemb a1 on b.log_dfno=a1.dfno collate SQL_Latin1_General_CP1_CS_AS
					     inner join ecomm_trans_hdr c on b.log_trcd=c.orderno
					     left outer join ecomm_trans_shipaddr d ON c.orderno=d.orderno 
					where c.token=@token
				END";		
		return $this->getRecordset($qry, null, $this->db1);
		//update by dion @06/03/2019
		//on b.log_trcd=c.orderno
	}
	
	function getInsertTrxOrderIDNonMember($orderid) {
		$qry = "SELECT
					a.orderno, a.token, a.secno, a.total_pay, a.total_bv, a.totPayCP, 
					a.sentTo, a.payShip, a.payAdm,
					b.conoteJNE, b.receiver_name, b.tel_hp1 as telp_customer, b.email
				FROM ecomm_trans_hdr a
				   	left join ecomm_trans_shipaddr b ON a.orderno = b.orderno
				WHERE a.token='$orderid'";
		return $this->getRecordset($qry, null, $this->db1);
	}

	function getInsertTrxOrderIDNonMemberDigital($orderid) {
		$qry = "SELECT
					a.orderno, a.token, a.secno, a.total_pay, a.total_bv, a.totPayCP,
					a.sentTo, a.payShip, a.payAdm,
					b.conoteJNE, b.receiver_name, b.tel_hp1 as telp_customer, b.email, a.datetrans
				FROM ecomm_trans_hdr a
				   	left join ecomm_trans_shipaddr_sgo b ON a.token = b.orderno
				WHERE a.token='$orderid'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	function getInsertTrxMampirKak($orderid) {
		$qry = "SELECT a.dfno, b.log_usrlogin, 
				   a.tel_hp as tel_hp_login, b.log_dfno, a1.tel_hp as tel_hp_as,
				   b.log_sento, b.log_status_trx, d.tel_hp1 as telp_customer, a.email,
				       c.*, d.conoteJNE, d.cargo_id, d.addr1, d.receiver_name, d.tel_hp1,
				       a.dfno as memberid, a.fullnm as membername, a.[password], a.tel_hp, e.memberid as new_member_reg,
					   c.no_hp_konfirmasi
				from klink_mlm2010.dbo.msmemb a
				inner join log_trans b on a.dfno=b.log_dfno collate SQL_Latin1_General_CP1_CS_AS
				     inner join klink_mlm2010.dbo.msmemb a1 on b.log_dfno=a1.dfno collate SQL_Latin1_General_CP1_CS_AS
				     inner join ecomm_trans_hdr c on b.log_trcd=c.orderno
				     left outer join ecomm_trans_shipaddr d ON c.orderno=d.orderno 
				     left outer join ecomm_memb_ok e ON c.orderno = e.trx_no
				where c.token='$orderid'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getRegMemberKnetPlus($orderno) {
		$qry = "SELECT b.orderno, a.memberid, a.membername, a.[password], c.log_usrlogin, 
				 d.tel_hp as hp_login, a.tel_hp as hp_new_member
			  FROM ecomm_memb_ok a
			  INNER JOIN ecomm_trans_hdr b ON (a.trx_no = b.orderno)
			  INNER JOIN log_trans c ON (a.trx_no = c.log_trcd)
			  LEFT OUTER JOIN klink_mlm2010.dbo.msmemb d ON 
				(c.log_usrlogin collate SQL_Latin1_General_CP1_CS_AS = d.dfno)
			  WHERE a.trx_no = '$orderno'";
		$hasil = $this->getRecordset($qry, null, $this->db1);
		
		if($hasil != null) {
			$text = 'Registrasi member sukses,ID Member: '.$hasil[0]->memberid.' / '.$hasil[0]->membername.',Password: '.$hasil[0]->password.'';
		
			if($hasil[0]->hp_login != "" && $hasil[0]->hp_login != null) {
				$send = smsTemplate($hasil[0]->hp_login, $text);
			}
			
			if($hasil[0]->hp_new_member != "" && $hasil[0]->hp_new_member != null) {
				$send = smsTemplate($hasil[0]->hp_new_member, $text);
			}
			//$send = smsTemplate("087780441874", $text);
		}
	}
	
	function getConoteJNE($orderno) {
		$qry = "SELECT TOP 1 conoteJNE FROM ecomm_trans_shipaddr WHERE orderno = '$orderno'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function getInsertTrxOrderNo($orderid) {
		//$qry = "SELECT * FROM ecomm_trans_hdr WHERE token = '$orderid'";
		$qry = "SELECT * FROM ecomm_trans_hdr WHERE orderno = '$orderid'";
		return $this->getRecordset($qry, null, $this->db1);
	}
	
	function updateConotByOrderNo($orderno, $resi) {
		$this->db = $this->load->database('db_ecommerce', true);
		$qry = "UPDATE ecomm_trans_shipaddr
				SET conoteJNE = '$resi' 
		        WHERE orderno = '$orderno'";
		$upd = $this->db->query($qry);	
		return $upd;
	}

	function updateConotByOrderNo2($orderno) { //tambahan Vera
		$this->db = $this->load->database('db_ecommerce', true);
		$qry = "UPDATE ecomm_trans_shipaddr
				SET conoteJNE = ''
		        WHERE orderno = '$orderno'";
		$upd = $this->db->query($qry);
		return $upd;
	}

	function cancelConotByOrderNo($orderno,$resi) { //tambahan Vera
		$resi="CANCEL-".$resi;
		$this->db = $this->load->database('db_ecommerce', true);
		$qry = "UPDATE ecomm_trans_shipaddr
				SET conoteJNE='$resi'
		        WHERE orderno = '$orderno'";
		$upd = $this->db->query($qry);
		return $upd;
	}

	function showCurrentPeriod(){
        //$this->db = $this->load->database('alternate', true);
        
        /*$qry = "SELECT a.currentperiodkcash as lastperiod, 
                DATEADD(month, 1, a.currentperiodkcash) as nextperiod 
                from klink_mlm2010.[dbo].syspref a";*/
        
        $qry = "SELECT CONVERT(VARCHAR(10),a.currentperiodkcash, 21) as lastperiod, 
				a.periodKCashRange as rangeperiod
				from syspref a";
        
        return $this->getRecordset($qry, null, $this->db2);
    }
	
	
	
}
	
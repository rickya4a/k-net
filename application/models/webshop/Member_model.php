<?php
class Member_model extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

	function getStockistInfo($idstk) {
		$qry = "SELECT a.loccd, a.[state]
		        from mssc a WHERE a.loccd = '$idstk'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function getStatusBnsSttView($idmember) {
		$qry = "SELECT a.dfno, a.bnsstt_view_type from msmemb a WHERE a.dfno = '$idmember'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	//nandang 201804
	function getStatusVAView($idmember) {
		$qry = "SELECT a.dfno, a.fullnm, a.novac from msmemb a WHERE a.dfno = '$idmember'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
	function getDataVA($idmember) {
		$qry ="Select a.trcd, CONVERT(VARCHAR(20), a.etdt , 120) as trdt, a.tipe_dk, a.amount, a.bankDesc, b.amount as saldoAkhir
				from va_cust_pay_det a
					 inner join va_cust_pay_bal B on a.dfno=b.dfno
		       where a.dfno='$idmember'
			   order by a.etdt ASC";
		//echo "$qry";
	    $result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}
	//end nandang 201804

	function updateStatusBnsSttView($idmember, $stt) {
		$qry = "UPDATE msmemb SET bnsstt_view_type = '$stt' WHERE dfno = '$idmember'";
        //echo "Paydet ".$insPaydet."<br>";
		$exeInsPaydet = $this->executeQuery($qry, $this->setDB(2));
        return $exeInsPaydet;
	}

	function getListLandingPageRecruit($idrecruiter, $type) {
    	$ss	= "";
    	if($type == "kiv") {
    		$ss .= "AND sponsorid = '0000999'";
    	} else {
    		$ss .= "AND sponsorid != '0000999'";
    	}
    	$qry = "SELECT sponsorid, memberid, membername, joindt, idno
    		    FROM ecomm_memb_ok
    		    WHERE recruiterid = '$idrecruiter' $ss
                ORDER BY membername, joindt";
        //echo "gbv ".$qry;
        $result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
    }

	function checkSponsorLP($recruiterid, $idmember) {
    	$qry = "SELECT sponsorid, memberid, membername, joindt, idno
    		    FROM ecomm_memb_ok
    		    WHERE memberid = '$idmember'
    		    	AND recruiterid = '$recruiterid'
    		    	AND sponsorid = '0000999'
                ORDER BY membername, joindt";
        //echo "gbv ".$qry;
        $result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
    }

	 function getDetailMember($idmember) {
    	$qry = "SELECT sponsorid, memberid, membername, joindt, idno
    		    FROM ecomm_memb_ok
    		    WHERE memberid = '$idmember'";
        //echo "gbv ".$qry;
        $result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
    }

	function getListRecruit($data) {
		//$dtfrom = date("Y-m-d", strtotime($data['from']));
		//$dtto = date("Y-m-d", strtotime($data['to']));
		/*$from = explode("/", $data['from']);
		$to = explode("/", $data['to']);
		$dtfrom = $from[2]."-".$from[1]."-".$from[1];
		$dtto = $to[2]."-".$to[1]."-".$to[1];
		$qry = "SELECT a.dfno, a.fullnm,
				   CONVERT(VARCHAR(30),a.jointdt,103) as  jointdt,
				   a.sponsorid, b.fullnm as sponsorname
				FROM msmemb a INNER JOIN msmemb b ON (a.sponsorid = b.dfno)
				WHERE a.sfno_reg = '$data[recruitid]'
				AND CONVERT(VARCHAR(10), a.jointdt, 126) between '$dtfrom' and '$dtto'
				ORDER BY a.jointdt, a.fullnm";
		//echo $qry;	*/
		$qry = "SELECT a.dfno, a.fullnm, CONVERT(VARCHAR(30),a.jointdt,103) as jointdt,
					a.sponsorid, b.fullnm as sponsorname
				FROM msmemb a
				INNER JOIN msmemb b ON (a.sponsorid = b.dfno)
				WHERE a.sfno_reg = '$data[recruitid]' AND
				month(a.jointdt)=$data[bln] and year(a.jointdt)=$data[thn]
				ORDER BY a.jointdt, a.fullnm";
				//	echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function getListPayment($data) {
		$nstart=$data['start'];
		$start=date('Y-m-d H:i:s',strtotime($nstart));

		$nend=$data['end'];
		$end=date('Y-m-d',strtotime($nend));
		$end=date('Y-m-d H:i:s', strtotime('+1 days', strtotime($end)));

		$bln=$data['bln'];
		$thn=$data['thn'];
		$bns_month= $data['bln']."/".$data['thn'];

		if(($nstart !="" && $nend !="") && ($bln != "" && $thn !="")){
			$qry="select orderno as dfno, nmmember as fullnm, datetrans as jointdt,bonusmonth as sponsorid,
			  total_pay as sponsorname, flag_payment, date_expired
 			  from ecomm_trans_hdr_sgo where id_memb='$data[recruitid]'
			  AND kode_pay !='' AND bonusmonth='$bns_month' AND (datetrans between '$start' AND '$end') order by datetrans DESC";

		}elseif(($nstart == "" && $nend == "") && ($bln != "" && $thn !="")){
			$qry="select orderno as dfno, nmmember as fullnm, datetrans as jointdt,bonusmonth as sponsorid,
			  total_pay as sponsorname, flag_payment
 			  from ecomm_trans_hdr_sgo where id_memb='$data[recruitid]'
			  AND kode_pay !='' AND bonusmonth='$bns_month' order by datetrans DESC";

		}else{
			$qry="select orderno as dfno, nmmember as fullnm, datetrans as jointdt,bonusmonth as sponsorid,
			  total_pay as sponsorname, flag_payment
 			  from ecomm_trans_hdr_sgo where id_memb='$data[recruitid]'
			  AND kode_pay !='' AND (datetrans between '$start' AND '$end') order by datetrans DESC";
		}
		//echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}

	/*--------------------------
	 * INSERT MEMBER SGO MODULE
	 ---------------------------*/

	function insertTempMemberSGO($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);
		$datetrans = date("Y-m-d H:i:s");

		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$priceSK = $this->session->userdata('starterkit_prd');
		$sender_address = $this->session->userdata('sender_address');
		$destination_address = $this->session->userdata('destination_address');
		$jne_branch = $this->session->userdata('jne_branch');
		$pay_memb_sgo_id = $this->session->userdata('pay_memb_sgo_id');
		$member_info = $this->session->userdata('member_info');
		$promo = $this->session->userdata('promo');
		$bns = date("m")."/".date("Y");
		$totPay = $priceSK['price'];
		$totalbv = 0;

		/*
		 if(getUserID() == "IDSPAAA66834") {
			$biaya = getTotalSKandShipCost2();
		    $gross_amount = $biaya['total_pay'];
		    $freeship = $biaya['freeship'];

		} else {
			$gross_amount = getTotalSKandShipCost();
			if($shipping_jne_info == null) {
				$freeship = "1";
			} else {
				$freeship = "0";
			}

		}	*/

		$biaya = getTotalSKandShipCost2();
		$gross_amount = $biaya['total_pay'];


		$hrgbv = $this->getHargaBv($priceSK['prdcd'], $pricecode);
		if($hrgbv != null) {
			$totalbv = $hrgbv[0]->bv;
		}
		$freeship = $biaya['freeship'];
		//$freeship = $biaya['freeship'];
		$discount = $biaya['tot_discount'];

		//$gross_amount = getTotalSKandShipCost();

		$shipper_telhp = getUserPhone();
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$nmstkSS1 = strtoupper($nmstkSS);
        //$produk = $this->cart->contents();

		$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,docno,payamt,notes,paystatus)
						values('".$trans_id."','1','sgo','',".$gross_amount.",'pending','pending')";
        //echo "Paydet ".$insPaydet."<br>";
		$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));

        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, free_shipping, discount_shipping)
			          values('".$trans_id."','','".$trans_id."','".getUserID()."','".getUserName()."',
			                    $totPay,$totalbv,'".$pricecode."','".$bns."','".$datetrans."',
			                    '".$personal_info['stockist']."','".$nmstkSS1."','0','W','','0','',0,0, '$freeship', $discount)";
                //echo "header gagal ".$insHeader."<br>";
        $exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));


        $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
      		          values('".$trans_id."','".$priceSK['prdcd']."','".$priceSK['prdnm']."',1,$totalbv,".$priceSK['price'].",'".$pricecode."','')";

        //echo "produk ".$inputProd."";
        $exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));

        $birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
        //$bankaccno = strtoupper($member_info['no_rek']);
		$bankaccnm1 = strtoupper($member_info['membername']);
		//$bankcode = strtoupper($member_info['pilBank']);
		$kdpos1 =  "000004";
		$st_id = $personal_info['state'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;
        $pay_tipe = "0";
		$voucherno = "";
		$voucherkey = "";

        //maap di edit sm ana
        $sponsorid = trim(strtoupper($member_info['sponsorid']));
        $membername = trim(strtoupper($member_info['membername']));
        $noktp = trim(strtoupper($member_info['idno']));
        $addr1 = trim(strtoupper($member_info['addr1']));
        $tel_hp = trim(strtoupper($member_info['tel_hp']));
        $email = trim(strtoupper($member_info['memb_email']));
        $recruiterid = trim(strtoupper($member_info['recruiterid']));
        $id_lp = trim(strtoupper($member_info['id_lp']));
		$no_rek = trim(strtoupper($member_info['no_rek']));
        $pilBank = trim(strtoupper($member_info['pilBank']));

        $insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok_sgo (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, id_landingpage)
                    values ('".$sponsorid."','".$pay_memb_sgo_id."','".$membername."','".$noktp."',
                            '".$addr1."', '','','".$tel_hp."','".$email."','".$personal_info['stockist']."','".$member_info['sex']."','".$birthdt."',
                            '$no_rek','".$bankaccnm1."','$pilBank',
                            '".$this->dateTime."','".$pay_memb_sgo_id."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".getUserID()."','".$recruiterid."',
                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '$id_lp')";
        //echo "<br />".$insMemb;
		//$res = $this->executeQuery($insMemb, $this->setDB(1));
		$exeInsMemb =  $this->executeQuery($insMemb, $this->setDB(1));

		$exeShipAddr = true;
        if($personal_info['delivery'] == "2") {
        	$tot_item_prd = $priceSK['qty'];
			$tot_weight_prd = $priceSK['weight'];
			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);

			if(array_key_exists("shipper", $personal_info)) {
				$shipper_id = $personal_info['shipper'];
			} else {
				$shipper_id = "1";
			}

			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code, kab_code, kec_code,
			                     addr1, email, tel_hp1, total_item, total_weight, service_type_id, service_type_name,
			                     receiver_name, stockist_name, kabupaten_name, province_name,
								 sender_address, dest_address, jne_branch, shipper_telhp, total_pay_net, cargo_id)
			                VALUES ('".$trans_id."', '".$personal_info['stockist']."', '".$personal_info['provinsi']."', '".$personal_info['kota']."',
			                        '".$personal_info['kecamatan']."', '".trim($personal_info['alamat'])."',
			                        '".trim($personal_info['email'])."', '".$personal_info['notlp']."',
			                        $tot_item_prd, $tot_weight_prd, '".$shipping_jne_info['service_code']."', '".$shipping_jne_info['service_display']."',
			                        '".$personal_info['nama_penerima']."','$nama_stk','".$personal_info['nama_kota']."','".$personal_info['nama_provinsi']."',
			                        '".$sender_address."', '".$destination_address."','".$jne_branch."', '$shipper_telhp', ".$totPay.", '".$shipper_id."')";

			$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));
		}


		if($exeInsPaydet && $exeInsHeader && $exeInputProdr && $exeInsMemb)
        {
            return 1;
        }
        else
        {
            return 0;
        }
	}

	function insertTempMemberLPSGO($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);
		$datetrans = date("Y-m-d H:i:s");

		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$priceSK = $this->session->userdata('starterkit_prd');
		$sender_address = $this->session->userdata('sender_address');
		$destination_address = $this->session->userdata('destination_address');
		$jne_branch = $this->session->userdata('jne_branch');
		$pay_memb_sgo_id = $this->session->userdata('pay_memb_sgo_id');
		$member_info = $this->session->userdata('member_info');
		$bns = date("m")."/".date("Y");
		$totPay = $priceSK['price'];
		$totalbv = 0;
		//$gross_amount = getTotalSKandShipCost();

		$biaya = getTotalSKandShipForLP();
	    $gross_amount = $biaya['total_pay'];
		$freeship = $biaya['freeship'];
		//$freeship = $biaya['freeship'];
		$discount = $biaya['tot_discount'];

		$shipper_telhp = $member_info['tel_hp'];
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$nmstkSS1 = strtoupper($nmstkSS);
        //$produk = $this->cart->contents();

		$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,docno,payamt,notes,paystatus)
						values('".$trans_id."','1','sgo','',".$gross_amount.",'pending','pending')";
        //echo "Paydet ".$insPaydet."<br>";
		$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));

        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, free_shipping, discount_shipping)
			          values('".$trans_id."','','".$trans_id."','".getUserID()."','".getUserName()."',
			                    $totPay,$totalbv,'".$pricecode."','".$bns."','".$datetrans."',
			                    '".$personal_info['stockist']."','".$nmstkSS1."','0','W','','0','',0,0, '$freeship', $discount)";
                //echo "header gagal ".$insHeader."<br>";
        $exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));


        $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
      		          values('".$trans_id."','".$priceSK['prdcd']."','".$priceSK['prdnm']."',1,0,".$priceSK['price'].",'".$pricecode."','')";

        //echo "produk ".$inputProd."";
        $exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));

        $birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
        //$bankaccno = strtoupper($member_info['no_rek']);
		$bankaccnm1 = strtoupper($member_info['membername']);
		//$bankcode = strtoupper($member_info['pilBank']);
		$kdpos1 =  "000004";

		$resStk = $this->getStockistInfo($personal_info['stockist']);
		//$st_id = $personal_info['state'];
		$st_id = $resStk[0]->state;

		$ip = $_SERVER['REMOTE_ADDR'];
		$birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;
        $pay_tipe = "0";
		$voucherno = "";
		$voucherkey = "";

        //maap di edit sm ana
        $sponsorid = trim(strtoupper($member_info['sponsorid']));
        $membername = trim(strtoupper($member_info['membername']));
        $noktp = trim(strtoupper($member_info['idno']));
        $addr1 = trim(strtoupper($member_info['addr1']));
        $tel_hp = trim(strtoupper($member_info['tel_hp']));
        $email = trim(strtoupper($member_info['memb_email']));
        $recruiterid = trim(strtoupper($member_info['recruiterid']));
        $id_lp = trim(strtoupper($member_info['id_lp']));

        $insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok_sgo (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
                    values ('".$sponsorid."','".$pay_memb_sgo_id."','".$membername."','".$noktp."','".$addr1."',
                            '','','".$tel_hp."','".$email."','".$personal_info['stockist']."','".$member_info['sex']."',
                            '".$birthdt."','','".$bankaccnm1."','',
                            '".$this->dateTime."','".$pay_memb_sgo_id."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".getUserID()."','".$recruiterid."',
                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '1', '$id_lp')";
        //echo "<br />".$insMemb;
		//$res = $this->executeQuery($insMemb, $this->setDB(1));
		$exeInsMemb =  $this->executeQuery($insMemb, $this->setDB(1));

		$exeShipAddr = true;
        if($personal_info['delivery'] == "2") {
        	$tot_item_prd = $priceSK['qty'];
			$tot_weight_prd = $priceSK['weight'];
			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);
			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code, kab_code, kec_code,
			                     addr1, email, tel_hp1, total_item, total_weight, service_type_id, service_type_name,
			                     receiver_name, stockist_name, kabupaten_name, province_name,
								 sender_address, dest_address, jne_branch, shipper_telhp, total_pay_net, cargo_id)
			                VALUES ('".$trans_id."', '".$personal_info['stockist']."', '".$personal_info['provinsi']."', '".$personal_info['kota']."',
			                        '".$personal_info['kecamatan']."', '".trim($personal_info['alamat'])."',
			                        '".trim($personal_info['email'])."', '".$personal_info['notlp']."',
			                        $tot_item_prd, $tot_weight_prd, '".$shipping_jne_info['service_code']."', '".$shipping_jne_info['service_display']."',
			                        '".$personal_info['nama_penerima']."','$nama_stk','".$personal_info['nama_kota']."','".$personal_info['nama_provinsi']."',
			                        '".$sender_address."', '".$destination_address."','".$jne_branch."', '$shipper_telhp', ".$totPay.", '".$personal_info['shipper']."')";

			$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));
		}


		if($exeInsPaydet && $exeInsHeader && $exeInputProdr && $exeInsMemb)
        {
            return 1;
        }
        else
        {
            return 0;
        }
	}

	function deleteTempMemberSGO($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);
		$del1 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_paydet_sgo WHERE orderno = '$trans_id'";
		$resDel1 = $this->db->query($del1);

		$del2 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_hdr_sgo WHERE orderno = '$trans_id'";
		$resDel2 = $this->db->query($del2);

		$del3 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_det_prd_sgo WHERE orderno = '$trans_id'";
		$resDel3 = $this->db->query($del3);

		$del4 = "DELETE FROM db_ecommerce.dbo.ecomm_memb_ok_sgo WHERE trx_no = '$trans_id'";
		$resDel4 = $this->db->query($del4);

		$del5 = "DELETE FROM db_ecommerce.dbo.ecomm_trans_shipaddr_sgo WHERE orderno = '$trans_id'";
		$resDel5 = $this->db->query($del5);
	}
	/*--------------------------
	 * END INSERT MEMBER SGO MODULE
	 ---------------------------*/

	function createNewShoppingVoucher($data) {
		$tgl = date("ym");
		$prefix = "V".$tgl;
	    $memberid = $data[0]->memberid;
		$qry = "SELECT * FROM ecomm_voucherNextNo WHERE prefix = '$prefix'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		if($result != null) {
			$nextNo = $result[0]->next;
			$next_id = sprintf("%03s",$nextNo);
			$vch = strval($prefix.$next_id);

			$list = "SELECT TOP 1 * FROM voucher_param WHERE status = '1'";
			$listRes = $this->getRecordset($list, null, $this->setDB(1));
			//INSERT ke tabel voucher agar member baru dapat mempergunakan voucher nya
			$insVch = "INSERT INTO ecomm_voucher_list (voucherno, voucherkey, dfno, vouchertype, nominal)
			           VALUES ('$vch', '$vch', '$memberid', '".$listRes[0]->voucher_type."', ".$listRes[0]->voucher_amt.")";
			$insVch2 = $this->executeQuery($insVch, $this->setDB(1));

			$upd = "UPDATE ecomm_voucherNextNo SET next = next + 1 WHERE prefix = '$prefix'";
			$upd2 = $this->executeQuery($upd, $this->setDB(1));
			return $vch;
		} else {
			$ins = "INSERT INTO ecomm_voucherNextNo (prefix, startno, next) VALUES ('$prefix',1,1)";
			$query = $this->executeQuery($ins, $this->setDB(1));
			if($query > 0) {
				$this->createNewShoppingVoucher($data);
			}
		}

	}

	function showLastkitno($idstk) {
		$result = null;
		$sql = "select memberprefix, lastcodememb, lastkitno from mssc where loccd = '$idstk'";
        $result = $this->getRecordset($sql, null, $this->setDB(2));

		if($result == null) {
			$input = "insert into mssc (lastkitno) values(1)";
            $query = $this->executeQuery($input, $this->setDB(2));
			if($query == 0) {
				throw new Exception("Failed when insert lastkit", 1);
			}
		}
		return $result;
        /*else
        {
            $input = "insert into mssc (lastkitno) values(1)";
            $query = $this->db->query($input);
        }*/
	}

	function createNewID($lastkit) {
		$counter = $lastkit[0]->lastkitno + 1;
        $next_id = sprintf("%05s",$counter);
        $alph = chr($lastkit[0]->lastcodememb);
        $new_id = strval("ID".$lastkit[0]->memberprefix.$alph.$next_id);
		return $new_id;
	}

	function createNewIDRec($lastkit, $stk) {
		$counter = $lastkit[0]->lastkitno + 1;
        $next_id = sprintf("%05s",$counter);
        $alph = chr($lastkit[0]->lastcodememb);
        $new_id = strval("ID".$lastkit[0]->memberprefix.$alph.$next_id);

		$qry = "SELECT dfno FROM msmemb WHERE dfno = '$new_id'";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		if($result == null) {
			return $new_id;
		} else {
			$hasil = $this->setLastKitNo($stk);
			$lastkitbaru = $this->showLastkitno($stk);
			$this->createNewIDRec($lastkitbaru[0]->lastkitno, $stk);
		}
	}

	function setLastKitNo($stk) {
        $conn = $this->load->database("klink_mlm2010", true);
        $sql = "update mssc SET lastkitno = lastkitno + 1 where loccd = '$stk'";
		//echo $sql;
        //$query = $this->executeQuery($sql, $this->setDB(2));
        $query = $conn->query($sql);
        if($query == 0) {
			throw new Exception("Failed when update lastkit", 1);
		}
        return $query;
	}

	function cek_seQMemb() {
		$this->db = $this->load->database($this->setDB(1), true);
         $y1=date("Y");
         $m=date("m");

        $this->db->trans_begin();

        $tbl = "ecomm_seq_memb"."$y1"."$m";

        $cek = "select * from $tbl";

        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('a')";
            //$query = $this->db->query($input);
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('a')";
            //$query = $this->db->query($input);
        }

        if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $query = $this->db->query($input);
                $this->db->trans_commit();
            }

        return $query;
	}

	function get_idnoMemb() {
		$this->db = $this->load->database($this->setDB(1), true);
        $y1=date("Y");
         $m=date("m");

        $this->db->trans_begin();

        $tbl = "ecomm_seq_memb"."$y1"."$m";
        $qry = "SELECT * FROM $tbl
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";

        //echo $qry;
        $query = $this->db->query($qry);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }
        }
         $jumlah = $query->num_rows();

       	$next_seq = sprintf("%06s",$ss);
        $prefix = date('ym');
        $y =  strval("E".$prefix.$next_seq);

          if ($query->result() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }

         return $y;
	}

	function show_member_new2($token) {
		/*$sql = "SELECT a.memberid, a.membername,a.password,a.sponsorid,a.stk_code,
                 b.fullnm as sponsorname, a.trx_no, c.total_pay + c.payShip as total_bayar,
                 c.total_pay, c.payShip, b.loccd, a.tel_hp, c.secno,
                 CONVERT(VARCHAR(10),c.datetrans,103) as datetrans
                 from db_ecommerce.[dbo].ecomm_memb_ok a
                 	  inner join db_ecommerce.[dbo].ecomm_trans_hdr c on a.trx_no = c.orderno
                      inner join klink_mlm2010.[dbo].msmemb b on a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS

                 where c.token = '$token'";*/
		$qry = "SELECT a.memberid, a.membername,a.password, a.tel_hp,
					a.sponsorid, b2.fullnm, a.stk_code,
				    d.log_usrlogin, b1.tel_hp as tel_hp_login,
					b.fullnm as sponsorname, a.trx_no,
					c.total_pay + c.payShip as total_bayar,
					c.total_pay, c.payShip, b.loccd, c.secno,
					e.conoteJNE, e.cargo_id, c.sentTo, c.orderno,
					CONVERT(VARCHAR(10),c.datetrans,103) as datetrans,
					c.free_shipping, c.payAdm, c.payConnectivity, a.email, b.email as sponsoremail,
					b1.email as email_pendaftar
				from db_ecommerce.[dbo].ecomm_memb_ok a
				    inner join db_ecommerce.[dbo].ecomm_trans_hdr c on a.trx_no = c.orderno
				    inner join klink_mlm2010.[dbo].msmemb b on a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				    left outer join db_ecommerce.[dbo].log_trans d on d.log_trcd=a.trx_no
					left outer join klink_mlm2010.[dbo].msmemb b1 on d.log_usrlogin=b1.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
					inner join klink_mlm2010.[dbo].msmemb b2 on a.memberid=b2.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
					left outer join db_ecommerce.[dbo].ecomm_trans_shipaddr e on c.orderno = e.orderno
				where c.token = '$token'";
	    return $this->getRecordset($qry, null, $this->setDB(1));

	}

	function show_member_newVA($token) {
		$qry = "SELECT a.memberid, a.membername,a.password, a.tel_hp,
					a.sponsorid, b2.fullnm, a.stk_code,
					d.log_usrlogin, b1.tel_hp as tel_hp_login,
					b.fullnm as sponsorname, a.trx_no,
					c.total_pay + c.payShip as total_bayar,
					c.total_pay, c.payShip, b.loccd, c.secno,
					e.conoteJNE, e.cargo_id, c.sentTo, c.orderno,
					CONVERT(VARCHAR(10),c.datetrans,103) as datetrans,
					c.free_shipping, c.payAdm, c.payConnectivity, a.email, b.email as sponsoremail,
					b1.email as email_pendaftar
				from db_ecommerce.[dbo].ecomm_trans_hdr c
					left outer join  db_ecommerce.[dbo].ecomm_memb_ok a on a.trx_no = c.orderno
					left outer join klink_mlm2010.[dbo].msmemb b on a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
					left outer join db_ecommerce.[dbo].log_trans d on d.log_trcd=a.trx_no
					left outer join klink_mlm2010.[dbo].msmemb b1 on d.log_usrlogin=b1.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
					left outer join klink_mlm2010.[dbo].msmemb b2 on a.memberid=b2.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
					left outer join db_ecommerce.[dbo].ecomm_trans_shipaddr e on c.orderno = e.orderno
				where c.token = '$token'";
		 return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function show_member_newVoucher($idmember) {
		$sql = "SELECT a.memberid, a.membername,a.password,a.sponsorid,a.stk_code,
				b.fullnm as sponsorname, a.trx_no, a.email,
				a.recruiterid, c.fullnm as recruitername,
				b.loccd, a.tel_hp, '' AS secno
				from db_ecommerce.[dbo].ecomm_memb_ok a
				    inner join klink_mlm2010.[dbo].msmemb b on a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				    inner join klink_mlm2010.[dbo].msmemb c on a.recruiterid=c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				where a.memberid = '$idmember'";
	    return $this->getRecordset($sql, null, $this->setDB(1));
	}

	function insertNewMemberUsingSGO($new_id, $ordernoo, $islandingPage = "0") {
		//03/11/2016, ubah reruiter ID

		$this->db = $this->load->database($this->setDB(1), true);
		$res = null;
		$member_info = $this->session->userdata('member_info');
		$personal_info = $this->session->userdata('personal_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');

		$sponsorid = strtoupper($member_info['sponsorid']);
		$recruiterid = trim(strtoupper($member_info['recruiterid']));
		$nmmember = strtoupper($member_info['membername']);
		$noktp = strtoupper($member_info['idno']);
		$addr11 = strtoupper($member_info['addr1']);
		$addr22 = strtoupper($member_info['addr2']);
		$addr33 = strtoupper($member_info['addr3']);
		$tel_hp = strtoupper($member_info['tel_hp']);
		$email = strtoupper($member_info['memb_email']);
		$id_lp = strtoupper($member_info['id_lp']);
		$idstk = $personal_info['stockist'];

		$sex = strtoupper($member_info['sex']);
		$birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
		$bankaccno = strtoupper($member_info['no_rek']);
		$bankaccnm1 = strtoupper($member_info['membername']);
		$bankcode = strtoupper($member_info['pilBank']);
		$kdpos1 =  "000004";
		$st_id = $personal_info['state'];
		$ip = $_SERVER['REMOTE_ADDR'];

		$birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;

		/*
		$pay_tipe = strtoupper($memb_choose_pay['pay_tipe']);
		$voucherno = strtoupper($memb_choose_pay['voucherno']);
		$voucherkey = strtoupper($memb_choose_pay['voucherkey']);
		*/
		$pay_tipe = "0";
		$voucherno = "";
		$voucherkey = "";

		$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
                    values ('".$sponsorid."','".$new_id."','".$nmmember."','".$noktp."','".$addr11."',
                            '".$addr22."','".$addr33."','".$tel_hp."','".$email."','".$idstk."','".$sex."',
                            '".$birthdt."','".$bankaccno."','".$bankaccnm1."','".$bankcode."',
                            '".$this->dateTime."','".$ordernoo."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".getUserID()."','$recruiterid',
                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '$islandingPage', '$id_lp')";
        //echo "<br />".$insMemb;
		//$res = $this->executeQuery($insMemb, $this->setDB(1));
		$query = $this->db->query($insMemb);
		/*if($res > 0) {
			$res = $this->show_member_new2($new_id);
		} */
		return $query;
	}

	function insertNewMemberPromoMampirKak($arr) {
		$queryExe = $this->load->database($this->setDB(1), true);

		$orderno = $arr['orderno'];
		$new_id = $arr['new_id'];
		$arrHasil = $arr['datamember'];
		$kdpos1 = "000004";
		$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
                    values ('".$arrHasil[0]->sponsorid."',
                            '".$new_id."',
                            '".$arrHasil[0]->membername."',
                            '".$arrHasil[0]->idno."',
                            '".$arrHasil[0]->addr1."',
                            '".$arrHasil[0]->addr2."',
                            '".$arrHasil[0]->addr3."',
                            '".$arrHasil[0]->tel_hp."',
                            '".$arrHasil[0]->email."',
                            '".$arrHasil[0]->stk_code."',
                            '".$arrHasil[0]->sex."',
                            '".$arrHasil[0]->birthdt."',
                            '".$arrHasil[0]->acc_no."',
                            '".$arrHasil[0]->acc_name."',
                            '".$arrHasil[0]->bankid."',
                            '".$this->dateTime."',
                            '".$orderno."',
                            '".$kdpos1."',
                            '".$arrHasil[0]->state."',
                            '".$arrHasil[0]->ip_address."',
                            '".$arrHasil[0]->prdcd."',
                            '".$arrHasil[0]->password."',
                            '".$arrHasil[0]->userlogin."',
                            '".$arrHasil[0]->recruiterid."',
                            '".$arrHasil[0]->flag_voucher."',
                            '".$arrHasil[0]->voucher_no."',
                            '".$arrHasil[0]->voucher_key."',
                            '".$arrHasil[0]->is_landingpage."',
                            '".$arrHasil[0]->id_landingpage."')";
        //echo $insMemb;
	    $res = $queryExe->query($insMemb);
		return $res;
	}

	function getHargaBv($prdcd, $pricecode) {
		$qry = "SELECT a.prdcd,
				       a.prdnm,
				       a.category,
				       a.status,
				       a.webstatus,
				       a.scstatus,
				       b.dp ,
				       b.cp ,
				       b.bv,
				       b.pricecode AS [pricecode]
				from msprd a
				     inner join pricetab b on a.prdcd=b.prdcd and b.pricecode='$pricecode'
				     AND a.prdcd = '$prdcd'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function insertNewMemberLPUsingSGO($orderno, $new_id, $ordernoo, $islandingPage = "0") {
		$this->db = $this->load->database($this->setDB(1), true);
		$res = null;
		/*$member_info = $this->session->userdata('member_info');
		$personal_info = $this->session->userdata('personal_info');
		$starterkit_prd = $this->session->userdata('starterkit_prd');

		$sponsorid = strtoupper($member_info['sponsorid']);
		$nmmember = strtoupper($member_info['membername']);
		$noktp = strtoupper($member_info['idno']);
		$addr11 = strtoupper($member_info['addr1']);
		//$addr22 = strtoupper($member_info['addr2']);
		//$addr33 = strtoupper($member_info['addr3']);
		$addr22 = "";
		$addr33 = "";
		$tel_hp = strtoupper($member_info['tel_hp']);
		$email = strtoupper($member_info['memb_email']);
		$idstk = $personal_info['stockist'];

		$sex = strtoupper($member_info['sex']);
		$birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
		//$bankaccno = strtoupper($member_info['no_rek']);
		$bankaccno = "";
		$bankaccnm1 = strtoupper($member_info['membername']);
		//$bankcode = strtoupper($member_info['pilBank']);
		$bankcode = "";
		$kdpos1 =  "000004";
		$ssx = explode(" - ", $personal_info['nama_stockist']);
		$st_id = $ssx[2];
		$ip = $_SERVER['REMOTE_ADDR'];

		$birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;


		$pay_tipe = strtoupper($memb_choose_pay['pay_tipe']);
		$voucherno = strtoupper($memb_choose_pay['voucherno']);
		$voucherkey = strtoupper($memb_choose_pay['voucherkey']);
		*/
		$pay_tipe = "0";
		$voucherno = "";
		$voucherkey = "";
		$kdpos1 =  "000004";
		$qry = "SELECT a.sponsorid, a.membername, a.idno, a.addr1, a.addr2,
				    a.addr3,a.tel_hp,a.email,a.stk_code,a.sex,birthdt,
				    a.acc_no,a.acc_name,a.bankid,a.joindt,a.trx_no,a.kdpos,
				    a.[state],a.ip_address,a.[password],a.userlogin,a.recruiterid,
				    a.flag_voucher, a.voucher_no, a.voucher_key,b.orderno,
				    c.prdcd, a.id_landingpage
				FROM ecomm_memb_ok_sgo a
				INNER JOIN ecomm_trans_hdr_sgo b ON (a.memberid = b.orderno)
				INNER JOIN ecomm_trans_det_prd_sgo c ON (a.memberid = c.orderno)
				WHERE a.memberid = '$orderno'";
		$checkQry = $this->db->query($qry);
		foreach($checkQry->result() as $dtax) {

			$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
	                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
	                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
	                    flag_voucher, voucher_no, voucher_key, is_landingpage, id_landingpage)
	                    values ('".$dtax->sponsorid."','".$new_id."','".$dtax->membername."','".$dtax->idno."','".$dtax->addr1."',
	                            '".$dtax->addr2."','".$dtax->addr3."','".$dtax->tel_hp."','".$dtax->email."','".$dtax->stk_code."','".$dtax->sex."',
	                            '".$dtax->birthdt."','".$dtax->acc_no."','".$dtax->acc_name."','".$dtax->bankid."',
	                            '".$this->dateTime."','".$dtax->orderno."','".$kdpos1."','".$dtax->state."','".$dtax->ip_address."','".$dtax->prdcd."','".$dtax->password."','".$dtax->recruiterid."','".$dtax->recruiterid."',
	                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '$islandingPage', '".$dtax->id_landingpage."')";
	        //echo "<br />".$insMemb;
			//$res = $this->executeQuery($insMemb, $this->setDB(1));
			$query = $this->db->query($insMemb);
			/*if($res > 0) {
				$res = $this->show_member_new2($new_id);
			} */

		}

		return $query;
	}

	function insertNewMemberWithVoucher($new_id, $ordernoo) {
		$res = null;
		$member_info = $this->session->userdata('member_info');
		$memb_choose_pay = $this->session->userdata('memb_choose_pay');
		$sponsorid = trim(strtoupper($member_info['sponsorid']));
		$recruiterid = trim(strtoupper($member_info['recruiterid']));
		$nmmember = trim(strtoupper($member_info['membername']));
		$noktp = trim(strtoupper($member_info['idno']));
		$addr11 = trim(strtoupper($member_info['addr1']));
		$addr22 = trim(strtoupper($member_info['addr2']));
		$addr33 = trim(strtoupper($member_info['addr3']));
		$tel_hp = trim(strtoupper($member_info['tel_hp']));
		$email = trim(strtoupper($member_info['memb_email']));
		$id_lp = '0'; //trim(strtoupper($member_info['id_lp']));
		$stk = explode("|", $member_info['stk']);
		$idstk = $stk[0];
		$sex = strtoupper($member_info['sex']);
		$birthdt = $member_info['thnlhr']."-".$member_info['blnlhr']."-".$member_info['tgllhr'];
		$bankaccno = trim(strtoupper($member_info['no_rek']));
		$bankaccnm1 = trim(strtoupper($member_info['membername']));
		$bankcode = trim(strtoupper($member_info['pilBank']));
		$kdpos1 =  "000004";
		$st_id = $stk[1];
		$ip = $_SERVER['REMOTE_ADDR'];

		$birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;

		$pay_tipe = strtoupper($memb_choose_pay['pay_tipe']);
		$voucherno = strtoupper($memb_choose_pay['voucherno']);
		$voucherkey = strtoupper($memb_choose_pay['voucherkey']);

		$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, id_landingpage)
                    values ('".$sponsorid."','".$new_id."','".$nmmember."','".$noktp."','".$addr11."',
                            '".$addr22."','".$addr33."','".$tel_hp."','".$email."','".$idstk."','".$sex."',
                            '".$birthdt."','".$bankaccno."','".$bankaccnm1."','".$bankcode."',
                            '".$this->dateTime."','".$ordernoo."','".$kdpos1."','".$st_id."','".$ip."','','".$password."','".getUserID()."','$recruiterid',
                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '$id_lp')";
        //echo $insMemb;
		$res = $this->executeQuery($insMemb, $this->setDB(2));
		if($res > 0) {
			$res = $this->show_member_newVoucher($new_id);
		}
		return $res;
	}

	function getNewMemberData($orderNo) {
		$qry = "SELECT
				  dbo.ecomm_memb_ok.memberid,
				  dbo.ecomm_memb_ok.membername,
				  dbo.ecomm_memb_ok.password,
				  dbo.ecomm_memb_ok.sponsorid,
				  dbo.ecomm_memb_ok.trx_no,
				  dbo.ecomm_memb_ok.stk_code,
				  dbo.ecomm_trans_hdr.token,
				  dbo.ecomm_trans_hdr.orderno,
				  dbo.ecomm_trans_hdr.datetrans,
				  dbo.ecomm_trans_hdr.total_pay + dbo.ecomm_trans_hdr.payShip as total_bayar
				FROM
				  dbo.ecomm_trans_hdr
				  INNER JOIN dbo.ecomm_memb_ok ON (dbo.ecomm_trans_hdr.orderno = dbo.ecomm_memb_ok.trx_no)
				WHERE dbo.ecomm_memb_ok.memberid = '$orderNo'";
		return $this->getRecordset($qry, null, $this->setDB(1));
	}

	function DecrementingLastKitNo($stk) {
        $this->db = $this->load->database($this->setDB(2), true);

        $sql = "update mssc SET lastkitno = lastkitno - 1 where loccd = '$stk'";
        $query = $this->executeQuery($sql, $this->setDB(2));
        if($query == 0) {
			throw new Exception("Failed when update lastkit", 1);
		}
        return $query;
    }

	function sendSMSNotification($ins, $hp_login) {
		$this->db = $this->load->database("alternate2", true);
		$text = 'K-LINK : Registrasi member sukses,No Order: '.$ins[0]->trx_no.', Idmember: '.$ins[0]->memberid.',Password: '.$ins[0]->password.', Kode Ambil Brg:'.$ins[0]->secno.'';
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
                    VALUES ('".$hp_login."','".$text."','K-Link Transaction Status', 'MyPhone6')";

        //echo "Ins SMS : $insSms";
        $query = $this->db->query($insSms);
		return $query;
	}

	function sendSmsRegMembMampirKak($data) {
		if($data[0]->sentTo == "1") {
				$ship = 'Kode Ambil Brg: '.$data[0]->secno;
				$shipRyan = 'SC : '.$data[0]->secno;
		} else {
			//$ship = 'Conote: '.$data[0]->conoteJNE;
			if($data[0]->cargo_id == "2") {
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
			} else {
				$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';
			}
			$shipRyan = 'Resi : '.$data[0]->conoteJNE;
		}

		$text = 'Registrasi Mampirkak sukses,No Order: '.$data[0]->orderno.',Idmember: '.$data[0]->memberid.',Password: '.$data[0]->password.', '.$ship.'';

		$textRyan = 'No Order: '.$data[0]->orderno.',Idmember: '.$data[0]->memberid.',Password: '.$data[0]->password.', '.$shipRyan.'';

		if($data[0]->no_hp_konfirmasi != null) {

			$sendX = smsTemplate($data[0]->no_hp_konfirmasi, $text);
		}

		if($data[0]->tel_hp != null && $data[0]->tel_hp != "") {
			$send = smsTemplate($data[0]->tel_hp, $text);
			$send = smsTemplate("087784698951", $textRyan);
	    }
	}

	function sendTrxSmsRegMember($data) {
		$qrydbx = $this->load->database("db_ecommerce", true);
		/*$checkAyu = "SELECT a.orderno FROM ecomm_trans_det_prd a
					 WHERE a.prdcd = 'SK004AO' and a.orderno = '".$data[0]->orderno."'";
		$hasCek = $qrydbx->query($checkAyu);
		if($hasCek->num_rows() > 0) {
			$ship = "Kartu dan AyuArtis Oil akan dikirim ke alamat member";
		} else {		*/

			if($data[0]->sentTo == "1") {
				$ship = 'Kode Ambil Brg: '.$data[0]->secno;
				$shipRyan = 'SC : '.$data[0]->secno;
			} else {
				//$ship = 'Conote: '.$data[0]->conoteJNE;
				if($data[0]->cargo_id == "2") {
					$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
				} else {
					$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';
				}
				$shipRyan = 'Resi : '.$data[0]->conoteJNE;
			}
		//}
		//$alternate2 = $this->load->database('alternate2', true);
		$text = 'Registrasi member sukses,No Order: '.$data[0]->orderno.',Idmember: '.$data[0]->memberid.',Password: '.$data[0]->password.', '.$ship.'';

		if($data[0]->tel_hp_login != null || $data[0]->tel_hp_login != "") {
			/*$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
		                    VALUES ('".$data[0]->tel_hp_login."','".$text."','K-Link Transaction Status', 'MyPhone6')";
		        //echo "Ins SMS : $insSms";
	    	$query = $alternate2->query($insSms);*/
	    	$send = smsTemplate($data[0]->tel_hp_login, $text);
	    }
	    //$send = smsTemplate("087780441874", $text);
	    if($data[0]->tel_hp != null || $data[0]->tel_hp != "") {

			/*$insSms2 = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
		                    VALUES ('".$data[0]->tel_hp."','".$text."','K-Link Transaction Status', 'MyPhone6')";
		        //echo "<br />Ins SMS : $insSms2";
	    	$query2 = $alternate2->query($insSms2);*/
			$send = smsTemplate($data[0]->tel_hp, $text);
	    }
		//$send = smsTemplate("087784698943", $text);
		//$send = smsTemplate("087780441874", $text);
		//$send = smsTemplate("087780441874", $text);

		$textRyan = 'No Order: '.$data[0]->orderno.',Idmember: '.$data[0]->memberid.',Password: '.$data[0]->password.', '.$shipRyan.'';
		$send = smsTemplate("087784698951", $textRyan);

	}

	function sendSMSNotificationLP($ins, $hp_login) {

		$text = 'Registrasi member sukses,No Order: '.$ins[0]->trx_no.', Idmember: '.$ins[0]->memberid.',Password: '.$ins[0]->password.', Kode Ambil Brg:'.$ins[0]->secno.'';
        /*$this->db = $this->load->database("alternate2", true);
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
                    VALUES ('".$hp_login."','".$text."','K-Link Transaction Status', 'MyPhone6')";

        //echo "Ins SMS : $insSms";
        $query = $this->db->query($insSms);
		return $query;
		*/
		$send = smsTemplate($hp_login, $text);
		$exp = explode("&", $send);

		if($exp[0] == "Status=1" ) {
			return 1;
		} else {
			return 0;
		}
	}

	function setLastKitToZero($idstk) {
		$this->db = $this->load->database($this->setDB(2), true);

        $sql = "update mssc SET lastkitno = 0,
                lastcodememb = lastcodememb + 1 where loccd = '$idstk'";
        $query = $this->executeQuery($sql, $this->setDB(2));
		if($query == 0) {
			throw new Exception("Failed when update lastkit", 1);
		}
		return $query;
	}

	function getListStockist() {
		$qry = "SELECT * FROM V_HILAL_ECOMM_SC_LIST";
		//echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
	}

	function getListStockistDion() {
		$qry = "SELECT * FROM klink_mlm2010.dbo.V_HILAL_ECOMM_SC_LIST
				WHERE loccd NOT IN ('ANP', 'IDBBG322', 'IDBM383', 'JNE', 'MKT', 'PR', 'SNX', 'WR', 'WR01', 'WR02', 'WR03', 'WR04')";
		//echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
	}

	function checkDoubleData($field, $value) {
		$qry = "SELECT dfno, fullnm, idno FROM msmemb WHERE $field = '$value'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}

	function getListStarterkit(){
        /*$qry = "select * from master_prd_cat_inv a
                where a.is_starterkit='1' and a.ecomm_status='1' and a.[status]='1'";*/
		$date = date("Y-m-d");
		$tblbaru = "V_Ecomm_PriceList";
		if($date >= "2019-03-01") {
			$tblbaru = "V_Ecomm_PriceList_Baru";
		} else {
			$tblbaru = "V_Ecomm_PriceList";
		}

        $qry = "select a.prdcd,
                    a.prdnm,
                    a.prdcdcat,
                    a.prdnmcatnm,
                    a.img_url,
                    a.price_w,
                    a.price_e,
                    a.bv,
                    a.weight,
                    a.is_charge_ship
                from $tblbaru a
                where a.is_starterkit = '1'";
        return $this->getRecordset($qry, null, $this->setDB(1));
    }

    function getListProvinsi(){
        //$qry = "select * from master_wil_provinsi";
        /*===============================edit hilal @2015-05-19=================================*/
		/*
		 $qry = "SELECT
				  dbo.master_wil_provinsi.kode_provinsi,
				  dbo.master_wil_provinsi.provinsi
				FROM
				  dbo.master_wil_provinsi
				  INNER JOIN dbo.master_wil_kabupaten ON (dbo.master_wil_provinsi.kode_provinsi = dbo.master_wil_kabupaten.kode_provinsi)
				  INNER JOIN dbo.master_wil_kecamatan ON (dbo.master_wil_kabupaten.kode_kabupaten = dbo.master_wil_kecamatan.kode_kabupaten)
				WHERE
				  (dbo.master_wil_kecamatan.kode_kab_JNE is not null)
				GROUP BY dbo.master_wil_provinsi.kode_provinsi,
				dbo.master_wil_provinsi.provinsi
				ORDER BY dbo.master_wil_provinsi.provinsi";
		 */

		$qry = "SELECT A.kode_provinsi, A.provinsi
				FROM V_ECOMM_WILAYAH2 A
				GROUP BY A.kode_provinsi, A.provinsi
				ORDER BY A.provinsi";
		/*===============================end edit hilal @2015-05-19=================================*/

        return $this->getRecordset($qry, null, $this->setDB(1));
    }


	function checkValidationMemberID($memberid) {
		$this->db = $this->load->database($this->setDB(2), true);
		$qry = "SELECT dfno, fullnm, email FROM msmemb WHERE dfno = '$memberid'";
		$query = $this->db->query($qry);
		return $query->num_rows();
	}

	function checkValidationVoucher2($voucherno, $voucherkey) {
		$qry = "select  formno, prdcd, vchkey from starterkit
        	   where formno = '$voucherno' and vchkey = '$voucherkey'
        	   and status = '1'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}

	function checkValidationVoucherNew($voucherno, $voucherkey) {
		$qry = "select  a.formno, a.prdcd, a.vchkey, a.status,
				a.activate_dfno, a.activate_by, a.activate_dt, b.fullnm
				from starterkit a
				left outer join msmemb b ON (a.activate_dfno = b.dfno AND a.activate_dfno is not null AND a.activate_dfno != '')
				where a.formno = '$voucherno' and a.vchkey = '$voucherkey'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}

	function checkMemberValid2($memberid) {
		$qry = "SELECT dfno, fullnm, email FROM msmemb WHERE dfno = '$memberid'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}

	function getListAffiliateBank() {
		 $sql = "select bankid,description from bank
                WHERE bankid not in('VCA','NA','BLK','CIC','BKU')
                and web_status='1' order by bankid";
        //echo "sql ".$sql;
        return $this->getRecordset($sql, null, $this->setDB(2));
	}

    function getEmail(){
        $sql = "select email,dfno,fullnm,password from msmemb
                where dfno = '".getUserID()."'";
        return $this->getRecordset($sql, null, $this->setDB(2));
    }

	function checkAuthLoginKnet($idmember) {
		$qry = "SELECT a.dfno,a.fullnm,a.memberstatus,
                a.password,a.tel_hp,a.tel_hm,a.email,
                a.addr1,a.addr2,a.addr3, a.idno, a.vc_count,
                a.sponsorid,b.fullnm as sponsorname,a.loccd,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,a.novac
                FROM msmemb a
                    left outer join msmemb b on (a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                WHERE a.dfno = '".$idmember."'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

    function updateEmail($newemail){
        $sql = "update msmemb set email = '".$newemail."'
                where dfno = '".getUserID()."'";
        //echo $sql;
        $query = $this->executeQuery($sql, $this->setDB(2));
		if(!$query) {
			$a = 0;
		}else{
		  $a = 1;
		}

		return $a;
    }

	function updateEmaildanHP($data) {
	   if($data['new_hp'] == "" || $data['new_hp'] == " "){
	       $updt = "set email = '".$data['new_email']."' where dfno = '".getUserID()."'";
	   }else if($data['new_email'] == "" && $data['new_hp'] == ""){
	       $updt = "set email = '".$data['curr_email']."',tel_hp = '".$data['curr_hp']."' where dfno = '".getUserID()."'";
	   }elseif($data['new_hp'] != "" && $data['curr_email'] != ""){
	       $updt = "set email = '".$data['curr_email']."',tel_hp = '".$data['new_hp']."' where dfno = '".getUserID()."'";
	   }elseif($data['new_hp'] == "" && $data['new_email'] != ""){
	       $updt = "set email = '".$data['new_email']."',tel_hp = '".$data['curr_hp']."' where dfno = '".getUserID()."'";
	   }else{
	       $updt = "set email = '".$data['new_email']."',tel_hp = '".$data['new_hp']."' where dfno = '".getUserID()."'";
	   }
		$sql = "update msmemb $updt";
        //echo $sql;
        $query = $this->executeQuery($sql, $this->setDB(2));
		if(!$query) {
			$a = 0;
		}else{
		  $a = 1;
		}

		return $a;
	}

    function toHistoryUpdate($data){
        $ins = "insert into ecomm_history_email(idmember,current_email,current_hp,new_email,new_hp)
                values('".getUserID()."','".$data['curr_email']."','".$data['curr_hp']."','".$data['new_email']."','".$data['new_hp']."')";
        $query = $this->executeQuery($ins, $this->setDB(1));
		if(!$query) {
			$a = 0;
		}else{
		  $a = 1;
		}
        //$a = 1;

		return $a;
    }

    function cekHistoryUpdtEmail(){
        $slc = "select count(idmember) as jml
                from ecomm_history_email where idmember = '".getUserID()."'";
        $res = $this->getRecordset($slc, NULL, $this->db1);
		return $res;
    }

    function cekNewRegisterEmail(){
        /*$slc = "select count(a.email) as jml,a.email
                from msmemb a where a.dfno = '".getUserID()."'
                group by a.email";*/
        $slc = "SELECT CASE
                		  WHEN a.email IS NULL THEN '0'
                          WHEN a.email = '' THEN '0'
                          WHEN a.email NOT LIKE '%@%.%' THEN '0'
                          WHEN a.email LIKE '%@%.%' THEN '1'
                          ELSE '1'
                       END as email_stat,
                       a.dfno
                FROM msmemb a
                WHERE a.dfno='".getUserID()."'";

        //echo "query ".$slc;
        $res = $this->getRecordset($slc, NULL, $this->db2);
		return $res;
    }

    function cekNewHP(){
       $slc = "SELECT CASE
                		  WHEN a.tel_hp IS NULL THEN '0'
                          WHEN a.tel_hp = '' THEN '0'
                          WHEN a.tel_hp LIKE '0%' THEN '1'
                          WHEN a.tel_hp LIKE '+62%' THEN '1'
                          ELSE '1'
                       END as hp_stat,
                       a.dfno
                FROM msmemb a
                WHERE a.dfno='".getUserID()."'";

        //echo "query ".$slc;
        $res = $this->getRecordset($slc, NULL, $this->db2);
		return $res;
    }

    function updtPassMember($x){
        $upd = "update msmemb set password =  '".$x['new_pass']."'
                where dfno = '".getUserID()."'";
        $query = $this->executeQuery($upd, $this->setDB(2));
		if(!$query) {
			$a = 0;
		}else{
		  $a = 1;
		}

		return $a;
    }

    function getVoucher($id){
        $karakter = str_replace("I",'N',$id);
        $panjang = 5;
         $string = '';
        for($i = 0;$i < $panjang;$i++){
            $pos = rand(0,strlen($karakter)-1);
            $string .= $id{$pos};
        }
        $vch = 'VE'.$string;
        return $vch;
    }

    function generateVoucher(){
        $VoucherNo = $this->getVoucher(getUserID());
        $addMonths = 3;
        $now = strtotime(date("Y-m-d"));
        $expiredDt = date('Y-m-d', strtotime('+'.$addMonths.' month', $now));

        $ins = "insert into ecomm_voucher_list(voucherno,dfno,vouchertype,claim_status)
                values('".$VoucherNo."','".getUserID()."','New Email','0')";

        /*$ins = "insert into ecomm_voucher_list(voucherno,dfno,vouchertype,claim_status,nominal,expiredt)
                values('".$VoucherNo."','".getUserID()."','New Email','0',50000,'".$expiredDt."')";*/
        $query = $this->executeQuery($ins, $this->setDB(1));
		if(!$query) {
			$a = array("response" => "0","voucher" => "gagal");
		}else{
		    $a = array("response" => "1","voucher" => "".$VoucherNo."");
		}

		return $a;
    }

    function sendSmsVoucher($text){
        $this->db = $this->load->database("alternate2", true);
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
		                    VALUES ('".getUserPhone()."','".$text."','K-Link Register Email', 'MyPhone6')";
		        //echo "<br />Ins SMS : $insSms";
   	    $query = $this->db->query($insSms);
    }

 	function checkSatujaringan($id, $idlogin) {$this->db = $this->load->database('klink_mlm2010', true);
        $id = strtoupper($id);
        $stored_procedure = "exec klink_mlm2010.[dbo].SP_HILAL_WEB_CHECKUPLINE ?,?";
        //echo $stored_procedure;
        $query = $this->db->query($stored_procedure,array('dfno'=> $id,'login'=> $idlogin));
        $dta = $query->result();
        if($dta[0]->hasil ==  1)
        {
            $sponsor = $this->getDistributorInfo($id);
            if($sponsor > 0)
            {
                $x = array("response" => "true", "message" => "ID Sponsor 1 jaringan", "sponsorname" => $sponsor[0]->fullnm);
            }else{
                $x = array("response" => "false", "message" => "ID Sponsor tidak ditemukan");
            }
        }else{
            $sponsor = $this->getDistributorInfo($id);
             if($sponsor > 0)
             {
                $x = array("response" => "false", "message" => "Id Sponsor Tidak 1 Jaringan Dengan user login", "sponsorname" => $sponsor[0]->fullnm);
             }else{
                $x = array("response" => "false", "message" => "ID Sponsor tidak ditemukan");
             }

        }
        return $x;


 	}

	function getDataMember($id) {
	    $qry = "SELECT dfno, fullnm FROM msmemb WHERE dfno = '$id'
				AND fullnm NOT IN ('TERMINATION', 'RESIGNATION')";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function getMemberInfo($id) {
		$this->db = $this->load->database('klink_mlm2010', true);
        $id = strtoupper($id);
        $stored_procedure = "exec klink_mlm2010.[dbo].SP_HILAL_WEB_CHECKUPLINE ?,?";
        //echo $stored_procedure;
        $query = $this->db->query($stored_procedure,array('dfno'=> $id,'login'=> getUserID()));
        $dta = $query->result();
        if($dta[0]->hasil ==  1)
        {
            $sponsor = $this->getDistributorInfo($id);
            if($sponsor > 0)
            {
                $x = array("response" => "true", "message" => "ID Sponsor 1 jaringan", "sponsorname" => $sponsor[0]->fullnm);
            }else{
                $x = array("response" => "false", "message" => "ID Sponsor tidak ditemukan");
            }
        }else{
            $sponsor = $this->getDistributorInfo($id);
             if($sponsor > 0)
             {
                $x = array("response" => "true", "message" => "Id Sponsor Tidak 1 Jaringan Dengan user login", "sponsorname" => $sponsor[0]->fullnm);
             }else{
                $x = array("response" => "false", "message" => "ID Sponsor tidak ditemukan");
             }

        }
        return $x;
	}

	function cekValidSponsor($idmember) {
		$idmember = strtoupper($idmember);
		$slc = "select a.dfno,a.fullnm
				from msmemb a
				where a.dfno = '$idmember' 
				AND a.fullnm NOT IN ('TERMINATION','TERMINATE', 'TERMINATED', 'RESIGNATION')
				and a.fullnm NOT LIKE 'TRANSFER%'";
		$result = $this->getRecordset($slc,null,$this->db2);
        return $result;
	}

	function getDistributorInfo($idmember) {
	       $idmember = strtoupper($idmember);
		 $slc = "select a.dfno,a.fullnm,a.addr1,a.tel_hp,
                a.tel_hm,a.idno,a.email, a.sex, a.bnsstmsc,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,
                a.sponsorid, a.birthpl, b.fullnm as sponsorname
                from msmemb a
                LEFT OUTER JOIN msmemb b ON (a.sponsorid = b.dfno)
				where a.dfno = '".$idmember."' 
				AND a.fullnm NOT IN ('TERMINATION','TERMINATE', 'TERMINATED', 'RESIGNATION')
				and a.fullnm NOT LIKE 'TRANSFER%'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
	}

	function getConcat($table,$field) {
		$qry = "select (fullnm + ' ' + convert(varchar, X) + ' ' + convert(varchar, Y)) as R ,* from $table ORDER BY $field ASC";
		$result = $this->getRecordset($qry, null, $this->db2);
		return $result;
	}

	function getConcat2($table,$field) {
		//$qry = "select (fullnm + ' ' + convert(varchar, latitude) + ' ' + convert(varchar, longitude)) as R ,* from $table ORDER BY $field ASC";
		$qry = "select * from $table WHERE latitude !='' AND longitude !='' ORDER BY $field ASC";
		$result = $this->getRecordset($qry, null, $this->db1);
		return $result;
	}


	function getConcat3($table,$field) {
		//$qry = "select (fullnm + ' ' + convert(varchar, latitude) + ' ' + convert(varchar, longitude)) as R ,* from $table ORDER BY $field ASC";
		$qry = "select * from $table WHERE latitude !='' AND longitude !='' AND is_gosend_active='1' ORDER BY $field ASC";
		$result = $this->getRecordset($qry, null, $this->db1);
		return $result;
	}

	function getwh($table,$field,$val1,$val2) {
		//$qry = "select (fullnm + ' ' + convert(varchar, latitude) + ' ' + convert(varchar, longitude)) as R ,* from $table ORDER BY $field ASC";
		$qry = "select * from $table WHERE whcd='$val1'AND loccd='$val2' AND latitude !='' AND longitude !='' AND is_gosend_active='1' ORDER BY $field ASC";
		$result = $this->getRecordset($qry, null, $this->db1);
		return $result;

		print_r($qry);
	}

	function getschedule($table,$field,$val,$day) {
		$kolom1= $day."_open";
		$kolom2= $day."_closed";
		//$qry = "select (fullnm + ' ' + convert(varchar, latitude) + ' ' + convert(varchar, longitude)) as R ,* from $table ORDER BY $field ASC";
		$qry = "select $kolom1 as buka, $kolom2 as tutup from $table WHERE loccd='$val' AND metode='Instant' ORDER BY $field ASC";
		$result = $this->getRecordset($qry, null, $this->db1);
		return $result;

		//echo $qry;
	}
}
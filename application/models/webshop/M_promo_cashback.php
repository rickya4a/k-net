<?php
class M_promo_cashback extends MY_Model {
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
	
	function checkValidMember($id) {
		$qry = "SELECT * FROM msmemb WHERE dfno = '$id'";
		//echo $qry;
		$arr = $this->getRecordset($qry, NULL, $this->db2);
		if($arr == null) {
			throw new Exception("ID Member salah, silahkan cek kembali", 1);			
		}
		return $arr;
	}




	function getCashback($xd){
		$slc = "select * from cashback_rekruiter_summary WHERE sfno_reg = '$xd' " ;
		$result = $this->getRecordset($slc, null, $this->db1);
		return $result;
	}

	function getListingDetail($vc){
		$x = getUserID();
		$qry = "
                SELECT * FROM cashback_rekruiter_detail where sfno_reg = '$x' AND batchno='$vc';";
		//echo "query member ".$qry;
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}





	function getListDataPromo($param, $value) {
		$qry = "SELECT * FROM toto_promo_rekrut_header WHERE $param = '$value'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		
		return $arr;
	}
	
	function getListIec($period, $recruitid) {
		/* $qry = "SELECT TOP 100 a.dfno, a.fullnm, a.jointdt, a.sponsor, b.fullnm as sponsorname 
				FROM V_HILAL_REC_BV a
				INNER JOIN msmemb b ON (a.sponsor = b.dfno)
				WHERE a.recruiter = '$id'";
		 * 
		 */
		 $periodX = explode("/", $period);
		 $mth = $periodX[0];
		 $mth2 = substr("0".$mth, -2, 2);
		 $yr = $periodX[1];
		 $bnsperiod = $yr."-".$mth2."-01";
		 //echo $bnsperiod;
		  
		 $qry = "SELECT a.dfno, a.fullnm, a.jointdt, a.sponsor, a.sp_name AS sponsorname, 
					   (SELECT isnull(sum(n.tbv),0)
				        FROM newtrh n
				        WHERE n.dfno=a.dfno and n.bnsperiod='$bnsperiod'
				       ) as totbv
		 		 FROM V_HILAL_REC_BV a
				 WHERE a.recruiter = '$recruitid'
				 ORDER BY A.jointdt";
		//echo $qry;
		$arr = $this->getRecordset($qry, NULL, $this->db2);

		return $arr;
	}
	
	//$route['member/voucher/check/(:any)/(:any)'] = 'webshop/member/getVoucherCheck/$1/$2';
	public function getVoucherCheck($voucherno, $voucherkey) {
		$arr = $this->memberService->checkValidationVoucher($voucherno, $voucherkey);
		echo json_encode($arr);
	}
	
	
	
	function getListDataRedemptionRekrut($param, $value) {
		$qry = "SELECT dfno, trx_code, point_redemp, stockist, CONVERT(VARCHAR(10),trx_date, 21) as trx_date 
		        FROM toto_promo_rekrut_redemption 
		        WHERE $param = '$value'";
		//echo $qry;		
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getUndianPromoKolagen() {
		$qry = "SELECT a.trcd, a.dfno, a.tdp, a.bnsperiod, a.tbv, SUM(a.xqtyord) as jum
				FROM QTW_NEWTRH_NEWTRD4 a
				WHERE a.xprdcd = 'IDHD014'
				GROUP BY a.trcd, a.dfno, a.tdp,a.bnsperiod, a.tbv
				HAVING SUM(a.xqtyord) >= 4";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;			
	}
	
	function checkUndianKolagen($id) {
		$qry = "SELECT * FROM HILAL_COLLAGEN_ANNIV2016 WHERE dfno = '$id'";
		//echo $qry;
		$arr = $this->getRecordset($qry, NULL, $this->db2);
		return $arr;
	}
	
    function getListOliveHoney(){
        $currentMonth = date('m');
        $previousMonth = intval($currentMonth) - 1;
        $strMonth = sprintf('%02s', $previousMonth);
        //$lastMonth = date('m', strtotime($currentMonth . " last month"));
        $year = date('Y');
        $bnsperiod = $year." / ".$strMonth." / 01";
        //$bnsperiod = date("Y/m/01",strtotime('last month'));
        $slc = "select ax.dfno,ax.fullnm,ax.jml_o,ax.jml_h,	ax.bobot_jml
                from (
                        SELECT  
                        	a.dfno,a.fullnm,sum(a.jml_O) as jml_o,
                            sum(a.jml_H) as jml_h,
                        	sum(a.bobot_jml) as bobot_jml
                        from TWA_OLIVEHONEY2 a
                        where a.bnsperiod BETWEEN  '2017/03/01' and '".$bnsperiod."'
                        group by a.dfno,a.fullnm
                        having (sum(a.jml_O)<>0 and sum(a.jml_H)<>0 )  
                ) ax
                order by ax.bobot_jml desc,ax.jml_o desc,ax.jml_h desc,ax.dfno";
        //echo $slc;
        $arr = $this->getRecordset($slc, NULL, $this->db2);
		return $arr;
    }
}
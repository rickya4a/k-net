<?php
class Sales_model extends MY_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function showHeadTransOnline($idmember,$bnsperiod){
        /*
           $qry = "select a.orderno,a.CNno,a.id_memb, memb.fullnm as fullnm,a.bonusmonth,
                           a.total_pay,a.total_bv,a.idstk,a.datetrans
                   from ecomm_trans_hdr a
                        left outer join klink_mlm2010.dbo.msmemb memb on a.id_memb COLLATE SQL_Latin1_General_CP1_CS_AS=memb.dfno
                       inner join ecomm_checkout b on (a.orderno = b.orderno)
                   where b.userlogin = '".$idmember."' AND a.bonusmonth = '".$bnsperiod."'
                   order by a.CNno";
         // ORIGINAL */

        /*$qry = "SELECT A.orderno, A.CNno, A.id_memb, A.fullnm, A.bonusmonth, A.total_pay, A.total_bv, A.idstk, A.datetrans
                  FROM V_ECOMM_LIST_TRANS A
                  WHERE A.userlogin = '".$idmember."' AND A.bonusmonth = '".$bnsperiod."'
                ORDER BY A.CNno
  ";*/
        $qry = "SELECT A.orderno, A.CNno, A.id_memb,
				  a.nmmember as fullnm, A.bonusmonth, A.total_pay, 
				  A.total_bv, A.idstk, A.datetrans
				FROM ecomm_trans_hdr a 
				WHERE a.id_memb = '".$idmember."'
				and a.bonusmonth = '".$bnsperiod."'";
				        //echo "query itu ".$qry;

        $res = $this->getRecordset($qry,null);
        return $res;
    }

    function showDetTransOnline($idmember,$orderno,$bnsperiod){
        /*
		 $qry = "select c.userlogin AS idmember,a.orderno,a.token,a.status_vt_pay,a.bonusmonth as period,
                        a.status_vt_app_dt,a.status_vt_reject_dt,b.paytype,b.notes,b.payamt
                from ecomm_trans_hdr a
                    inner join ecomm_trans_paydet b on (a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS)
                    inner join ecomm_checkout c on (a.orderno = c.orderno)
                where a.orderno = '".$orderno."' and c.userlogin='".$idmember."'
                    and a.bonusmonth = '".$bnsperiod."'";
		 */

        $qry = "select c.userlogin AS idmember,a.orderno,a.token,a.status_vt_pay,a.bonusmonth as period,
				        a.status_vt_app_dt,a.status_vt_reject_dt,b.paytype,b.notes,b.payamt, 
				        a.sentTo,
				        CASE 
				        	WHEN a.sentTo='2' THEN d.conoteJNE
				            WHEN a.sentTo='1' THEN a.secno
				        end as sccode,
				        a.[status]
				from ecomm_trans_hdr a
				    inner join ecomm_trans_paydet b on (a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS)
				    inner join ecomm_checkout c on (a.orderno = c.orderno)
				    left outer join ecomm_trans_shipaddr d on a.orderno=d.orderno
				where a.orderno = '".$orderno."' and c.userlogin='".$idmember."'
                    and a.bonusmonth = '".$bnsperiod."'";
        //echo $qry;
        $qry = "select a.id_memb AS idmember,
				a.orderno,a.token,a.status_vt_pay,
				a.bonusmonth as period, 
				a.datetrans as status_vt_app_dt,
				a.status_vt_reject_dt,
				b.paytype,b.notes,b.payamt, a.sentTo, 
				CASE WHEN a.sentTo='2' THEN d.conoteJNE 
				WHEN a.sentTo='1' THEN a.secno end as sccode, a.[status] 
			from ecomm_trans_hdr a 
			inner join ecomm_trans_paydet b 
			on (a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS)  
			left outer join ecomm_trans_shipaddr d on a.orderno=d.orderno 
			where a.orderno = '".$orderno."' and 
			a.id_memb='".$idmember."' and a.bonusmonth = '".$bnsperiod."'";
        //echo $qry;
        $res = $this->getRecordset($qry,null);
        return $res;
    }

    function showDetProdTransOnline($orderno){

        $qry = "select a.prdcd,a.prdnm,a.dpr,a.bvr,a.qty, b.download_filename,
				      (a.dpr * a.qty) as TDP,
				      (a.bvr * a.qty) as TBV
				from ecomm_trans_det_prd a
				left join master_prd_cat_inv b ON (a.prdcd = b.cat_inv_id)
                 where a.orderno = '".$orderno."'";

        $res = $this->getRecordset($qry,null);
        return $res;
    }

    function showHeaderTrans($idmember){
        $qry = "SELECT dfno, fullnm FROM klink_mlm2010.[dbo].msmemb 
                WHERE dfno = '".$idmember."'";

        //echo "query ini ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function cekMemberLog($idmember, $pwd){
        $qry = "SELECT dfno, fullnm FROM klink_mlm2010.[dbo].msmemb 
                WHERE dfno = '".$idmember."' AND password = '".$pwd."'";

        //echo "query ini ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function showDetTrans($tipe, $trcd, $orderno){

        if($tipe == "1")
        {
            $qry = "SELECT a.orderno,a.prdcd, a.qty as qtyord, a.dpr as dp , a.bvr as bv, a.qty * a.dpr as TDP, a.qty * a.bvr as TBV, a.prdnm
                  FROM klink_mlm2010.[dbo].webol_det_prod a
        		  WHERE a.orderno = '".$trcd."'";

            //echo "masuk yg tipe 1 ".$qry;
        }
        elseif($tipe == "4")
        {
            $qry = "SELECT a.prdcd, a.qtyord, a.dp, a.bv, a.qtyord * a.dp as TDP, a.qtyord * a.bv as TBV, b.prdnm
                  FROM sc_newtrd a
                  inner join msprd b on a.prdcd = b. prdcd
        		  WHERE a.trcd = '".$trcd."'";

            //echo "masuk yg tipe 4 ".$qry;
        }
        else
        {
            $qry = "SELECT a.prdcd, a.qtyord, a.dp, a.bv, a.qtyord * a.dp as TDP, a.qtyord * a.bv as TBV, b.prdnm
                  FROM newtrd a
                    inner join msprd b on a.prdcd = b. prdcd
        		  WHERE a.trcd = '".$trcd."'";

            //echo "masuk yg tipe 2 ".$qry;
        }


        $result = $this->getRecordset($qry,null,$this->db2);
		if($result == null) {
		  $qry = "SELECT a.prdcd, a.qtyord, a.dp, a.bv, a.qtyord * a.dp as TDP, a.qtyord * a.bv as TBV, b.prdnm
                  FROM newtrd a
                    inner join msprd b on a.prdcd = b. prdcd
        		  WHERE a.trcd = '".$trcd."'";
		  $result = $this->getRecordset($qry,null,$this->db2);		  
		}
        return $result;
    }

    function showDetailTrans($idmember,$bln,$thn){
        $qry = "SELECT
                      a.dfno AS idmember,
                      msmemb.fullnm,
                      a.trtype,
                      a.trcd,
                      a.trcd2,
                      a.orderno,
                      a.sc_dfno,
                      a.ndp,
                      a.nbv,
                      MONTH(a.bnsperiod) AS bln,
                      YEAR(a.bnsperiod) AS thn,
                      CONVERT(VARCHAR(30), a.etdt, 103) AS tglc,
                      sc_newtrh.csno 
                    FROM
                      klink_mlm2010.[dbo].newtrh a
                      LEFT OUTER JOIN klink_mlm2010.[dbo].sc_newtrh ON (a.trcd = sc_newtrh.trcd)
					  LEFT OUTER JOIN klink_mlm2010.[dbo].msmemb ON (a.dfno = msmemb.dfno)
                    WHERE
                      (a.dfno = '".$idmember."') AND
                      (MONTH(a.bnsperiod) = ".$bln.") AND
                      (YEAR(a.bnsperiod) = ".$thn.") AND (a.nbv <> 0)";
        //AND LEFT(a.trcd,2)='ID'";
        //echo "query itu ".$qry;

        $result = $this->getRecordset($qry,null,$this->db2);
        //print_r($result);
        return $result;
    }

    function showBonusmonth($idmember1){
        $idmember = strtoupper($idmember1);
        $qry = "SELECT TOP 4 CONVERT(VARCHAR(7), period, 111) as bnsmonth
                FROM hsttree
                WHERE dfno='".$idmember."' ORDER BY bnsmonth DESC";
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function personalbv($idmember,$month,$year){

        /*$qry1 = "SELECT n.dfno, j.fullnm, j.level, n.ppv, n.popv, 
                    n.pgpv,year(n.period) as year,month(n.period) as month
                    FROM hsttree n, msmemb j
                    WHERE n.dfno='".$idmember."' AND year(n.period)='".$year."'
                    AND month(n.period)='".$month."' AND n.dfno=j.dfno
                    ORDER BY j.fullnm ASC";*/
        /*
		 $qry = "SELECT n.dfno, j.fullnm, j.level, n.ppv, n.popv, n.sfno,
                 n.pgpv,year(n.period) as year,month(n.period) as month
                	FROM hsttree n
                    INNER JOIN  msmemb j ON (n.dfno = j.dfno)
                	WHERE n.dfno='".$idmember."' AND year(n.period)='".$year."'
                AND month(n.period)='".$month."' AND n.dfno=j.dfno
                ORDER BY j.fullnm ASC";
		 */
        $qry = "SELECT j.dfno, j.fullnm, j.sfno,
					   CAST(ISNULL((SELECT TOP 1 A.adjustedrank
				       	FROM tbonus A
				        WHERE A.distributorcode=J.dfno
				        ORDER BY A.bonusyear DESC, A.bonusmonth DESC), 0) AS INT) AS [level],
					   isnull(k.ppv, 0) as ppv, 
				       isnull(k.popv, 0) as popv, 
				       isnull(k.pgpv, 0) as pgpv,
				       isnull(YEAR(k.period), ".$year.") as year, 
				       isnull(MONTH(k.period), ".$month.") AS month
				FROM msmemb j 
					 left outer join hsttree k ON j.dfno=k.dfno and YEAR(k.period)=".$year." and MONTH(k.period)=".$month."
				WHERE j.dfno='".$idmember."' 
				ORDER BY j.dfno ASC";


        $result = $this->getRecordset($qry,null,$this->db2);

//        echo $qry;
        return $result;
    }
    function dt_diri_rec($idmembers){

        $qry = "
                SELECT top 1 b.fullnm, b.bankaccno, b.banknm, b.dfno, b.bankaccnm FROM [dbo].[msmemb_recruiter_bonus] a
                left join msmemb b
                on a.sfno_reg  = b.dfno  COLLATE SQL_Latin1_General_CP1_CS_AS
                where
                a.sfno_reg ='".$idmembers."'
                ;

        ";


        $result = $this->getRecordset($qry,null,$this->db2);
//        print_r($result);
//        echo $this->db->last_query();
        return $result;
    }
    function dt_table_rec($idmembers,$month,$year){

        $qry = "
                SELECT * FROM [dbo].[msmemb_recruiter_bonus]
                where
                sfno_reg ='$idmembers'
                AND
                joindt
                BETWEEN '$month' and DATEADD (DAY , 1 ,'$year');

                ;

        ";

        $result = $this->getRecordset($qry,null,$this->db2);
//        print_r($result);
//        echo $this->db->last_query();

        return $result;
    }

    //tambahan dari FENDI
    public function get_bonus($idmembers, $dari, $ke){
        /*
                $sql = "SELECT dfno, tgl1, tgl2, fullnm, jml_rekrutan, NAMAREK, REKENING, BANK, NOMINAL, tglFINANCE,
                            CASE
                                WHEN flagFINANCE = 'p' THEN 'pending'
                                ELSE 'pending'
                            END as status
                            FROM [dbo].[TWA_CASHBACK_STK80K]
                            where dfno = '$idmembers'
                            AND tgl1 >= '$dari'
                            AND tgl1 <= '$ke'
                            ORDER BY tgl1";
        */
        $sql = "SELECT dfno, tgl1, tgl2, fullnm, jml_rekrutan, NAMAREK, REKENING, BANK, NOMINAL, flagFINANCE, NOMINAL_TRF, tglFINANCE,
                    CASE
                        WHEN flagFINANCE = 'p' THEN 'transfer'
                        ELSE 'pending'
                    END as status
                    FROM [dbo].[TWA_CASHBACK_STK80K]
                    where dfno = '$idmembers'
                    AND tgl1 >= '$dari'
                    AND tgl1 <= '$ke'
                    ORDER BY tgl1";
        $result = $this->getRecordset($sql,null,$this->db2);
        //echo $this->db->last_query($sql);
        return $result;
    }

    //tambahan dari FENDI
    public function get_detail($idmember, $dari, $ke){

        $sql = "SELECT b.dfno, b.fullnm, b.jointdt
                    FROM [dbo].[TWA_CASHBACK_STK80K_DET] b
                    LEFT JOIN [dbo].[TWA_CASHBACK_STK80K] a
                    on a.dfno = b.sfno_reg COLLATE SQL_Latin1_General_CP1_CS_AS and a.NOTRX = b.NOTRX
                    WHERE a.dfno = '$idmember'
                    AND b.jointdt >= '$dari'
                    AND b.jointdt <= DATEADD(day, 1, '$ke')";

        $result = $this->getRecordset($sql,null,$this->db2);
        return $result;
    }

    function groupbv($idmember,$month,$year){
        /*
        $qry = "SELECT n.dfno, j.fullnm, j.level, n.ppv, n.popv, n.pgpv,n.sfno,
                    year(n.period) as year,month(n.period) as month
                    FROM hsttree n
                    INNER JOIN  msmemb j ON (n.dfno = j.dfno)
                    WHERE n.sfno='".$idmember."' AND year(n.period)='".$year."'
                    AND	month(n.period)='".$month."' AND n.dfno=j.dfno
                    ORDER BY j.fullnm ASC";
		 */
        $qry = "SELECT j.dfno, j.fullnm, j.sfno,
                       j.sfno_reg as recruiter_id,
					   CAST(ISNULL((SELECT TOP 1 A.adjustedrank
				       	FROM tbonus A
				        WHERE A.distributorcode=J.dfno
				        ORDER BY A.bonusyear DESC, A.bonusmonth DESC), 0) AS INT) AS [level],
					   isnull(k.ppv, 0) as ppv, 
				       isnull(k.popv, 0) as popv, 
				       isnull(k.pgpv, 0) as pgpv,
				       isnull(YEAR(k.period), ".$year.") as YEAR, 
				       isnull(MONTH(k.period), ".$month.") AS MONTH
				FROM msmemb j 
					 left outer join hsttree k ON j.dfno=k.dfno and YEAR(k.period)=".$year." and MONTH(k.period)=".$month."
				WHERE j.sfno='".$idmember."' 
				ORDER BY j.dfno ASC";
//        echo "gbv ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }


    function showDataMember($idmember,$month,$year){
        $qry = "SELECT a.dfno, a.fullnm, a.addr1, a.addr2, a.addr3, b.sponsorcode, a1.fullnm as sp_nm,
                       b.currentrank, c1.ranknm , c1.shortnm as prev, c1.percentage as perc_prev,
                       b.adjustedrank, c2.ranknm , c2.shortnm as adjust, c2.percentage as perc_adj,
                       b.effectiverank, c3.ranknm , c3.shortnm as effective, c3.percentage as perc_eff
                FROM msmemb a
                    inner join tbonus b on(a.dfno = b.distributorcode)
                    inner join msrank c1 on CAST(b.currentrank as int)=c1.[level]
                    inner join msrank c2 on CAST(b.adjustedrank as int)=c2.[level]
                    inner join msrank c3 on CAST(b.effectiverank as int)=c3.[level]
                    inner join msmemb a1 on b.sponsorcode=a1.dfno

                WHERE b.distributorcode = '".$idmember."' AND
				(b.bonusmonth ='".$month."') AND (b.bonusyear = '".$year."')";
        //echo $qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getSponsorCode($idmember) {
        $qry = "SELECT sfno FROM msmemb a WHERE a.dfno = '$idmember'";
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function cekmonthly($idmember,$month,$year){
        $qry = "select a.trcd,a.ndp,a.nbv,CONVERT(VARCHAR(30), a.etdt,103)as tglc
				from klink_mlm2010.dbo.newtrh a 
                LEFT OUTER JOIN klink_mlm2010.dbo.sc_newtrh on (a.trcd = sc_newtrh.trcd)
				where (a.dfno = '".$idmember."') AND (MONTH(a.bnsperiod)='".$month."')
				    AND (YEAR(a.bnsperiod)= '".$year."') AND (a.nbv<>0) AND LEFT (a.trcd,2) IN ('ID', 'CV')";
        //echo "query monthly ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function downline($idmember,$month,$year){
        $qry = "select a.distributorcode,b.fullnm,a.currentrank,a.adjustedrank,a.effectiverank,
		      a.ppv,a.pgpv,a.gpv,a.AccPGPV,a.pbv,a.gbv,a.pbvb,a.gbvb, a.pgbv 
              from tbonus a
                inner join msmemb b on(a.distributorcode=b.dfno )
              where a.sponsorcode = '".$idmember."' and a.bonusmonth = '".$month."' and a.bonusyear = '".$year."'";
        //echo "query downline ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function selfBonus($idmember,$month,$year){
        $qry = "select a.distributorcode,b.fullnm,a.currentrank,a.adjustedrank,a.effectiverank,
				a.pgbv,a.ppv,a.pgpv,a.gpv,a.AccPGPV,a.pbv,a.gbv,a.pbvb,a.gbvb 
                from tbonus a
                inner join msmemb b on(a.distributorcode=b.dfno)
				where a.distributorcode = '".$idmember."' and a.bonusmonth = '".$month."' and a.bonusyear = '".$year."'";
        //echo "query selfbonus ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function showBonusMember($idmember,$month,$year){
        $qry = "select a.bonusyear,a.bonusmonth,sponsorcode, currentrank, adjustedrank, effectiverank, planbincome, gpincome, ldbincome, ldbtotalpoint,
			   		   a.ldbpointvalue, sredincome, crownincome, caincome, scaincome, rcaincome, chincome, infinityincome,
					   gpsharing3income, gpsharing3points, netincome, lastmonthyend,currentyend, accyend, lastmonthoversea,
					   currentoversea, accoversea, capointvalue, scapointvalue, rcapointvalue, chpointvalue, addinfinityincome,
					   pspointvalue, gpsharing1income, gpsharing1points, gpsharing2income, gpsharing2points,
					   gps3pointvalue, gps1pointvalue, gps2pointvalue, totaladjustedamt, remark,
					   CAShare, CHShare, scashare, crownshare, rcashare, psharingincome, psharingpoints
					   
				FROM
					  tbonus a
					  LEFT OUTER JOIN tbonusadjust c ON (a.distributorcode = c.distributorcode)
					  AND (a.bonusyear = c.bonusyear)
					  AND (a.bonusmonth = c.bonusmonth)
					  LEFT OUTER JOIN tbonussummary b ON (a.bonusmonth = b.bonusmonth)
					  AND (a.bonusyear = b.bonusyear)
					  
					WHERE
					  (a.distributorcode = '$idmember') AND
					  (a.bonusmonth ='$month') AND
					  (a.bonusyear = '$year')";
        //echo "query bonus ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }


    function developmentBonus($idmember,$month,$year){
        $qry = "SELECT * FROM tbonusdev a WHERE a.distributorcode = '".$idmember."' 
			  AND a.bonusyear = '".$year."' and a.bonusmonth = '".$month."'
			  order by a.overridep desc";
        //echo "query devbonus ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function leadershipBonus($idmember,$month,$year){
        $qry = "select a.odistributorcode, b.fullnm, a.effectiverank, a.pgbv, a.incomepgbv1,
				       a.incomepgbv2, a.incomepgbv3, a.incomepgbv4, a.incomepgbv5,
				       a.incomepgbv6, a.incomepgbv7, a.incomepgbv8, a.incomepgbv9, a.totalpgbv
				from tbonusldr1 a 
                    left outer join msmemb b ON (a.odistributorcode = b.dfno)
				where a.bonusmonth = ".$month." and a.bonusyear = ".$year."
				      and a.distributorcode = '".$idmember."'";
        //echo "query leadershipBonus ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }



    function unilevelBonus($idmember,$month,$year){
        $qry = "select a.distributorcode,c.fullnm as sponsorname,a.odistributorcode,b.fullnm,
				a.pbv,a.incomepbv1,a.incomepbv2,a.incomepbv3,a.incomepbv4,a.incomepbv5,
				a.incomepbv6,a.incomepbv7,a.incomepbv8,a.incomepbv9, a.totalpbv 
				from tbonuspbu1 a,msmemb b,msmemb c
				where distributorcode = '".$idmember."' 
				AND bonusmonth='".$month."' AND bonusyear='".$year."' 
				AND a.odistributorcode=b.dfno AND a.distributorcode = c.dfno";
        //echo "query unilevelBonus ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function levelMember($idmember,$month,$year){
        $qry = "select a.distributorcode,a.distributorlevel,b.addinfinityincome,
					a.levelpbv,a.levelrate,a.levelincome
					from tbonuspbu2 a LEFT OUTER JOIN  tbonus b ON (a.distributorcode=b.distributorcode)
					AND (a.bonusmonth=b.bonusmonth) AND (a.bonusyear=b.bonusyear)
					where a.bonusmonth='".$month."' and a.bonusyear='".$year."' 
					and a.distributorcode='".$idmember."'";
        //echo "query level ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function showBonusVoucher($idmember,$month,$year){
        $qry = "SELECT dbo.tcvoucher.VoucherNo, CONVERT(VARCHAR(30),dbo.tcvoucher.IssueDate,103) as tglterbit,
					   dbo.tcvoucher.ExpireDate,dbo.tcvoucher.VoucherAmt,
					   dbo.tcvoucher.VoucherAmtCurr,dbo.tcvoucher.CurrencyNote,
			           dbo.tcvoucher.voucherkey,dbo.tcvoucher.status,
			           dbo.tcvoucher.claimstatus,dbo.tcvoucher.vchtype, dbo.tcvoucher.DistributorCode, msmemb.fullnm
		        FROM dbo.tcvoucher, msmemb 
		        WHERE dbo.tcvoucher.DistributorCode = msmemb.dfno AND
		              dbo.tcvoucher.DistributorCode = '".$idmember."'
					  AND dbo.tcvoucher.BonusMonth = ".$month."
  					  AND dbo.tcvoucher.BonusYear = ".$year." AND dbo.tcvoucher.vchtype = 'P'";
        //echo "query showBonusVoucher ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getCurrency(){
        $qry = "SELECT top 1 a.exchangerate FROM mexchangerate a
                where a.countrycode = 'ID'
                order by a.bonusyear desc, a.bonusmonth desc";
        //echo "query currency ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function showSumInitiative($idmember,$month,$year){
        $slc = "select sum(a.TotalBonus) as initiateBns
                from TBONUSINCOMEDETAILS a WITH (NOLOCK)
                WHERE a.distributorcode='".$idmember."' AND a.BonusType  = 'H0' AND   
                 		a.bonusmonth=".$month." AND 
                        a.bonusyear=".$year."
                group by a.distributorcode";
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
    }

    function infinityBonus($idmember,$month,$year){
        $qry = "select a.DistributorCode,b.fullnm as sponsor,a.ODistributorCode,c.fullnm,
				a.pbv as infinitybv,a.BonusRate,a.TotalBonus
				from TBONUSPBI1 a,msmemb b, msmemb c
				where DistributorCode = '".$idmember."'
				AND BonusYear = '".$year."' and BonusMonth = '".$month."'
				and a.DistributorCode=b.dfno and a.ODistributorCode = c.dfno";
        //echo "query infinityBonus ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function initiativeBonus($idmember,$month,$year){
        $slc = "SELECT a.distributorcode AS RECRUITER, a.bonusyear, a.bonusmonth, A.ODistributorCode, b.fullnm, A.BonusBV, A.BonusRate, A.TotalBonus
                FROM TBONUSINCOMEDETAILS A WITH (NOLOCK)   
                  		LEFT JOIN msmemb b WITH (NOLOCK) on a.odistributorcode = b.dfno   
                WHERE a.distributorcode='".$idmember."' AND A.BonusType  = 'H0' AND   
                 		a.bonusmonth=".$month." AND a.bonusyear=".$year."
                ORDER BY A.odistributorcode";
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
    }

    function getReportBonus($idmember,$year){

//        $slc = "SELECT a.distributorcode, a.bonusyear, a.bonusmonth,
//						   isnull(a.netincome, 0) as netincome,
//					       isnull(a.totBonus, 0) as totBonus,
//					       isnull(a.tax, 0) as tax,
//					       isnull(a.ar_total, 0) as ar_total,
//					       isnull(a.totBonus_Nett, 0) as  totBonus_Nett
//                ,b.nama, a.distributorcode as idmember
//                FROM V_HILAL_TOTBONUS_NETT A
//                 LEFT JOIN V_HILAL_PAJAK_DETAIL b
//                on A.distributorcode = b.ID COLLATE SQL_Latin1_General_CP1_CS_AS and
//                Bulan_Bonus =
//                RIGHT('00'+CAST(1 AS VARCHAR(2)),2)+'/'+RIGHT('00'+CAST(a.bonusmonth AS VARCHAR(2)),2)+'/'+CAST(A.bonusyear AS VARCHAR(4))
//
//                WHERE A.distributorcode='$idmember' AND A.bonusyear='$year'
//                ORDER BY a.bonusmonth";

        $slc = "SELECT DISTINCT a.distributorcode, a.bonusyear, a.bonusmonth,
						   isnull(a.netincome, 0) as netincome,
					       isnull(a.totBonus, 0) as totBonus,
					       isnull(a.tax, 0) as tax,
					       isnull(a.ar_total, 0) as ar_total,
					       isnull(a.totBonus_Nett, 0) as  totBonus_Nett
                ,b.nama, a.distributorcode as idmember,
								(SELECT COUNT(VoucherNo) from klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER where BonusYear = '$year' and bonusmonth = a.bonusmonth and distributorcode = '$idmember') as VoucherNo
                FROM V_HILAL_TOTBONUS_NETT A
                 LEFT JOIN V_HILAL_PAJAK_DETAIL b on A.distributorcode = b.ID COLLATE SQL_Latin1_General_CP1_CS_AS AND
                Bulan_Bonus =
                RIGHT('00'+CAST(1 AS VARCHAR(2)),2)+'/'+RIGHT('00'+CAST(a.bonusmonth AS VARCHAR(2)),2)+'/'+CAST(A.bonusyear AS VARCHAR(4))
                WHERE A.distributorcode='$idmember' AND A.bonusyear='$year'
								ORDER BY a.bonusmonth
        ";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        $this->cache->redis->hset('cache:bonus:'.$year,$idmember,$result);
        return $result;
    }

    function getReportBonus2($idmember,$year){

        $slc = "SELECT a.distributorcode, a.bonusyear, a.bonusmonth,
						   isnull(a.netincome, 0) as netincome,
					       isnull(a.totBonus, 0) as totBonus,
					       isnull(a.tax, 0) as tax,
					       isnull(a.ar_total, 0) as ar_total,
					       isnull(a.totBonus_Nett, 0) as  totBonus_Nett
                ,b.nama,
								(SELECT COUNT(VoucherNo) from klink_mlm2010.dbo.V_HILAL_PRINT_VOUCHER where BonusYear = '$year' and bonusmonth = a.bonusmonth and distributorcode = '$idmember') as VoucherNo
                FROM V_HILAL_TOTBONUS_NETT A
                 LEFT JOIN V_HILAL_PAJAK_DETAIL b on A.distributorcode = b.ID COLLATE SQL_Latin1_General_CP1_CS_AS AND
                Bulan_Bonus =
                RIGHT('00'+CAST(1 AS VARCHAR(2)),2)+'/'+RIGHT('00'+CAST(a.bonusmonth AS VARCHAR(2)),2)+'/'+CAST(A.bonusyear AS VARCHAR(4))
                WHERE A.distributorcode='$idmember' AND A.bonusyear='$year'
								ORDER BY a.bonusmonth";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
    }

    function showTableHilal($idmember,$month,$year){
        $qry = "SELECT * FROM tbonus_hilal_nett A WHERE A.dfno='$idmember' AND A.bonusmonth=$month AND A.bonusyear=$year";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getDetailPajak($idmember, $date){

        $qry = "SELECT * FROM [dbo].[V_HILAL_PAJAK_DETAIL] where ID='$idmember' AND Bulan_Bonus = '$date'";
        $result = $this->getRecordset($qry, NULL, $this->db2);
        //echo $qry;
        return $result;
    }

    function getKomisiPPOB($bnsperiod, $dfno){

        $qry = "SELECT a.*, b.fullnm
                from QPPOB_CASHBACK a
                LEFT JOIN [klink_mlm2010].[dbo].[msmemb] b
                on a.dfno = b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.bnsperiod = '$bnsperiod' AND a.dfno = '$dfno'
                ORDER BY a.bnsperiod, a.dfno, a.alvl, a.percentage, a.tbv, a.CashBackORI, a.CashBackPPOB, b.fullnm";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        //echo $qry;
        return $result;
    }

    function getListKuponHdr($bnsperiod, $dfno){

        $qry = "select a.dfno, b.fullnm, CONVERT(VARCHAR(10), a.bnsperiod, 103) as bnsperiod, c.total
                from TWA_NONSUPLEMEN_UNDIAN a
                LEFT JOIN msmemb b
                on a.dfno = b.dfno
                LEFT JOIN
                                (SELECT b.dfno, COUNT(*) as total
                                 FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                                 LEFT JOIN msmemb b
                                 on a.dfno = b.dfno
                                 GROUP BY b.dfno) c
                                ON c.dfno=b.dfno
                where a.dfno = '$dfno' and a.bnsperiod = '$bnsperiod'
                GROUP BY a.dfno, b.fullnm, a.bnsperiod, c.total
                ";

        $result = $this->getRecordset($qry, NULL, $this->db2);

        return $result;
    }

    function getListKupon($dfno){

        $qry = "SELECT * FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN]
                where dfno = '$dfno'";

        $result = $this->getRecordset($qry, NULL, $this->db2);

        return $result;
    }

    function headerReportBackend($dfno, $year){

        $sql = "SELECT top 1 a.dfno, a.fullnm, b.sponsorcode, a1.fullnm as sp_nm, c2.percentage as perc_adj, c2.shortnm as adjust
                FROM msmemb a
                    inner join tbonus b on(a.dfno = b.distributorcode)
                    inner join msrank c1 on CAST(b.currentrank as int)=c1.[level]
                    inner join msrank c2 on CAST(b.adjustedrank as int)=c2.[level]
                    inner join msrank c3 on CAST(b.effectiverank as int)=c3.[level]
                    inner join msmemb a1 on b.sponsorcode=a1.dfno
                WHERE b.distributorcode = '$dfno' AND b.bonusyear = '$year'
                order by b.adjustedrank DESC";

        $result = $this->getRecordset($sql, NULL, $this->db2);

        return $result;
    }

    function getProductOrder($orderno){

        $qry = "SELECT a.receiver_name,a.addr1,a.tel_hp1,a.conoteJNE,a.service_type_id,a.service_type_name,a.lat_dest,a.long_dest,b.total_pay, b.datetrans
                from ecomm_trans_shipaddr a
                LEFT JOIN ecomm_trans_hdr b
                on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.orderno = '$orderno' AND b.orderno = '$orderno'";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        //echo $qry;
        return $result;
    }

    function getTrackingWebhook($orderno){
        $qry = "SELECT *
                from ecomm_tracking_webhook a
                where a.orderno = '$orderno' order by id DESC";
        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;
    }

    function getTracking($orderno){
        $qry = "SELECT *
                from ecomm_tracking a
                where a.orderno = '$orderno' order by created ASC";
        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;
    }

    function insertTrackingApi($orderno,$no_ship,$status_ship,$desc,$shipper_id,$create_time,$dispatch_time,$arrival_time,$closed_time) {
        //insert Tracking
        $qry2 = "INSERT INTO ecomm_tracking_api (orderno,no_shipping,status_shipping,description,shipper_id,created,CreatedTime,DispatchTime,ArrivalTime,ClosedTime)
                 VALUES ('$orderno','$no_ship','$status_ship','$desc','$shipper_id','".$this->dateTime."','','','','')";
        return $this->executeQuery($qry2,$this->db1);
    }

    function updateShippaddr($orderno,$des_name,$des_addr,$des_phone,$no_gosend,$lat,$long) {
        $qry2 = "UPDATE ecomm_trans_shipaddr SET receiver_name='$des_name',addr1='$des_addr',tel_hp1='$des_phone',conoteJNE='$no_gosend', service_type_id='$no_gosend',lat_dest='$lat',long_dest='$long' WHERE orderno='$orderno'";
        return $this->executeQuery($qry2,$this->db1);
    }

    function updateShippaddr_url($orderno,$url) {
        $qry2 = "UPDATE ecomm_tracking_webhook SET live_tracking='$url' WHERE orderno='$orderno'";
        return $this->executeQuery($qry2,$this->db1);
    }

    function delTracking($orderno) {
        $qry2 = "DELETE from ecomm_tracking_webhook WHERE orderno='$orderno'";
        return $this->executeQuery($qry2,$this->db1);
    }

    function delTracking1($orderno) {
        $qry2 = "DELETE from ecomm_tracking_webhook WHERE orderno='$orderno' AND status LIKE '%no_driver%'";
        return $this->executeQuery($qry2,$this->db1);
    }

    function getListHadiah($total_pay){

//        $qry = "select * from nonmember_free_product_promo
//                where free_cat <> 'NULL'
//                and($total_pay > min_pay) and ($total_pay <= max_pay)
//                ORDER BY free_cat";
        $qry = "select a.*, b.weight, c.price_w, c.price_e, c.bv
                from db_ecommerce.dbo.nonmember_free_product_promo a
                left JOIN db_ecommerce.dbo.master_prd_cat_inv b ON a.prdcd = b.cat_inv_id
                left join klink_mlm2010.dbo.DION_msprd_pricetab c ON a.prdcd = c.prdcd COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.free_cat is not NULL and($total_pay > min_pay) and ($total_pay <= max_pay)
                ORDER BY a.free_cat";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;
        
    }

    function getListHadiah2(){

        $qry = "select free_Cat
                from nonmember_free_product_promo
                where free_cat <> 'NULL' AND min_pay = '2000001'
                GROUP BY free_cat";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;

    }

    function getListHadiah3(){

        $qry = "select a.*, b.weight, c.price_w, c.price_e, c.bv
                from db_ecommerce.dbo.nonmember_free_product_promo a
                left JOIN db_ecommerce.dbo.master_prd_cat_inv b ON a.prdcd = b.cat_inv_id
                left join klink_mlm2010.dbo.DION_msprd_pricetab c ON a.prdcd = c.prdcd COLLATE SQL_Latin1_General_CP1_CS_AS
                where free_cat is not null and a.min_pay = '600000'
                ORDER BY a.free_cat";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;

    }

    function pilihProduk($val){

//        $qry = "select * from nonmember_free_product_promo
//                where free_cat <> 'NULL' and free_Cat = '$val'";

        $qry = "select a.*, b.weight, c.price_w, c.price_e, c.bv
                from db_ecommerce.dbo.nonmember_free_product_promo a
                left JOIN db_ecommerce.dbo.master_prd_cat_inv b ON a.prdcd = b.cat_inv_id
                left join klink_mlm2010.dbo.DION_msprd_pricetab c ON a.prdcd = c.prdcd COLLATE SQL_Latin1_General_CP1_CS_AS
                where free_cat is not null and free_Cat = '$val'
                ORDER BY a.free_cat";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;

    }

    function getCashback($idmember,$month,$year){

        $qry = "select * from klink_mlm2010.dbo.V_HILAL_CASHBACK_HYDRO where dfno = '$idmember' AND
                MONTH(processdt) = '$month' AND YEAR(processdt) = '$year' ";

        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;
    }

    function getListTrans($data) {

        $bln=$data['bln'];
        $thn=$data['thn'];
        $start=$data['start_date'];
        $end=$data['end_date'];

        $bns_month= $bln."/".$thn;
        $dtfrom = date("Y-m-d", strtotime($start));
        $dtto = date("Y-m-d", strtotime($end));
        $idmember=$data['recruitid'];

        //1970-01-01
       If(($bln == "" || $thn =="") && ($start != "" && $end != "")){
            $qry="select * from ecomm_trans_hdr_sgo where id_memb='$idmember'
                AND kode_pay !='' AND date_expired != null  AND datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
                order by datetrans DESC";

        }elseif(($bln != "" || $thn !="") && ($start != "" && $end!= "")){
           $bns_month= $data['bln']."/".$data['thn'];
           $qry="select * from ecomm_trans_hdr_sgo where id_memb='$idmember'
                AND kode_pay !='' AND date_expired != null AND bonusmonth='$bns_month'
                AND datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
                order by datetrans DESC";
       }else{
           $qry="select * from ecomm_trans_hdr_sgo where id_memb='$idmember'
                AND kode_pay !='' AND date_expired != null AND bonusmonth='$bns_month'
                order by datetrans DESC";
        }

        /*$qry="select * from ecomm_trans_hdr_sgo where id_memb='$idmember'
                AND kode_pay !='' AND bonusmonth='$bns_month'
                AND datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
                order by orderno ASC";*/
        //echo $qry;
        //$result = $this->getRecordset($qry, null, $this->setDB(2));
        $result = $this->getRecordset($qry, NULL, $this->db1);
        return $result;
    }

    function getListTaxStk(){

        $qry = "select IDMEMBER
                from DATA_PAJAK_JOIN_STK
                where bonus_year = '2018'
                and IDMEMBER in
                (select dfno
                from MSSC)";

        $result = $this->getRecordset($qry, null, $this->db2);

        return $result;
    }
	
	function getPricecodeStk($idstk) {
		$qry = "SELECT pricecode, pricecode2 FROM mssc WHERE loccd = '$idstk'";
		$result = $this->getRecordset($qry, null, $this->db2);
        return $result;
	}

    function getDetailPajak2($idmember, $thn){

        $qry = "SELECT * FROM [dbo].[DATA_PAJAK_JOIN_STK] where bonus_year = '$thn' and IDMEMBER = '$idmember' ORDER BY bonus_month";

//        $qry = "SELECT *
//                from DATA_PAJAK_JOIN_STK
//                where IDMEMBER = '$idmember' and bulan_bonus_bu = 'Dec  1 2017 12:00AM'";

        $result = $this->getRecordset($qry, NULL, $this->db2);
        //echo $qry;
        return $result;
    }

}
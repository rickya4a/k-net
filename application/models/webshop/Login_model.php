<?php
class Login_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	/*function checkListPromo() {
		//$this->db = $this->load->database($this->db1);
		$tgl = date("Y-m-d");
		$qry = "SELECT id, max_discount
				FROM promo_period a WHERE a.[status] = 1
				AND a.period_start <= '$tgl' AND a.period_end >= '$tgl'";	
		//echo $qry;			
        $res = $this->getRecordset($qry, NULL, $this->db1);
        /*$query = $this->db->query($qry);
        $res = array();
		foreach ($query->result() as $row) {
			array_push($res, $row);
		}
		return $res;
	}*/
    
    function checkAuthLogin() {	
        $username = $this->input->post('username');
        $password = $this->input->post('password');
		$qry = "SELECT dfno, password,fullnm
		        FROM msmemb WHERE upper(dfno) = upper('".$username."')
				AND password = '".$password."' ";
        $res = $this->getRecordset($this->db2, $qry);
		
		return $res;
	} 
	
	function checkAuthLoginKnet($idmember, $password) {
		$qry = "SELECT a.dfno,a.fullnm,a.memberstatus,
                a.password,a.tel_hp,a.tel_hm,a.email,
                a.addr1,a.addr2,a.addr3, a.idno, a.vc_count,
                a.sponsorid,b.fullnm as sponsorname,a.loccd,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,a.novac, a.bankaccno, a.bankaccnm
                FROM msmemb a
                    left outer join msmemb b on (a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                WHERE a.dfno = '".$idmember."' 
                and a.password = '".$password."' and a.fullnm != 'TERMINATION'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function showDataMemberByNovac($novac) {
		$qry = "SELECT dfno, fullnm, tel_hp FROM msmemb WHERE novac = '$novac'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function createChaptcha()
    {
        
		$vals = array(
	        'img_path'      => './captcha/',
	        //'img_url'       => 'http://www.k-linkmember.co.id/k-commerce/captcha/',
	        'img_url'       => base_url().'captcha/',
	        'img_width'     => '150',
	        
		);
		$cap = $this->antispam->get_antispam_image($vals);
		//$this->db = $this->load->database('local', true);
		//$cap = create_captcha($vals);
		$query = "INSERT INTO captcha (captcha_time, ip_address, word) 
		          VALUES ($cap[time],'".$this->input->ip_address()."','$cap[word]')";
		$this->db->query($query);
		
		//$this->load->database
		return $cap;   		
    }	
	
	function captchaCheck($captcha) {
		$expiration = time()-3600; 
        $ip_address = $this->input->ip_address();
        //$this->db = $this->load->database('local', true);
        $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration); 
        //$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = '$captcha' AND ip_address = '$ip_address' AND captcha_time > $expiration"; 
        $sql = "SELECT * FROM captcha WHERE word = '$captcha' AND captcha_time > $expiration";
        //$binds = array($captcha, $ip_address, $expiration);
        $arr = NULL;
        $query = $this->db->query($sql); 
        if($query->num_rows() > 0)  {
            foreach($query->result() as $dta) { 	
            	$arr = array("response" => TRUE, "word" => $dta->word);
			}   
        } 
        //$this->db->query("DELETE FROM captcha WHERE word = '$captcha'"); 
        return $arr;
	}
 }
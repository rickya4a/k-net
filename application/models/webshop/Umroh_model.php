<?php
class Umroh_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
    
    function showDistInfo($dfno){
        $slc = "select a.dfno,a.fullnm,a.addr1,a.tel_hp,
                a.tel_hm,a.idno,a.email,a.sfno,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,loccd
                from msmemb a
                where a.dfno = '".$dfno."' AND a.fullnm != 'TERMINATION'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
    }
    
    function showKtpInfo($idno){
        $slc = "select a.fullnm,a.idno,a.dfno,b.dfno
                    from ecomm_umroh_save_child a
                    inner join klink_mlm2010.dbo.msmemb b on (a.idno = b.idno COLLATE SQL_Latin1_General_CP1_CS_AS)
                    where a.idno = '".$idno."' AND a.depart_status = '0'
                    and a.idno is not null";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function showHpInfo($nohp){
        $slc = "select a.fullnm,a.tel_hp
                from ecomm_umroh_save_child a
                where a.tel_hp = '".$nohp."' AND a.depart_status = '0'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function showRegnoInfo($regno){
        
         
        $slc = "select a.registerno,a.dfno,a.fullnm,a.departureid,a.tipe_perjalanan,
                b.departuredesc,b.departuredt,sum(c.tot_fund) as saldo
                from ecomm_umroh_save_child a
                	inner join ecomm_umroh_departure b on(a.departureid = b.id)
                    inner join ecomm_umroh_save_mut c on(a.id = c.id)
                where a.registerno = '".$regno."' and a.tipe_perjalanan = '1'
                AND c.mutate_flag = '2' and c.is_voucher = '0' and c.pay_status = '1'
                group by a.registerno,a.dfno,a.fullnm,a.departureid,
                b.departuredesc,b.departuredt,a.tipe_perjalanan";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function showRegnoInfo1($regno){
        
         
        $slc = "select a.registerno,a.dfno,a.fullnm,a.departureid,a.tipe_perjalanan,
                b.departuredesc,b.departuredt,sum(c.tot_fund) as saldo
                from ecomm_umroh_save_child a
                	inner join ecomm_umroh_departure b on(a.departureid = b.id)
                    inner join ecomm_umroh_save_mut c on(a.id = c.id)
                where a.registerno = '".$regno."' and a.tipe_perjalanan = '2'
                AND c.mutate_flag = '2' and c.is_voucher = '0' and c.pay_status = '1'
                group by a.registerno,a.dfno,a.fullnm,a.departureid,
                b.departuredesc,b.departuredt,a.tipe_perjalanan";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function checkSameSchedule($jdwl,$idmember,$idno){
    //function checkSameSchedule($jdwl,$idno){
        
        $jdwl1 = substr($jdwl,0,1);
        
        if($idmember != ""){
            $xx = "a.departureid = '".$jdwl1."' and a.depart_status = '0' and a.dfno = '".$idmember."'";
        }else{
            $xx = "a.departureid = '".$jdwl1."' and a.depart_status = '0' and a.idno = '".$idno."'";
        }
        
         $slc = "select a.fullnm
                from ecomm_umroh_save_child a
                where $xx";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getJadwalKeberangkatan($tipeprjlanan){
        if($tipeprjlanan == 'umr'){
            $xx = 'a.type_ks = 1';
        }else{
            $xx = 'a.type_ks = 2';
        }
        $slc = "select a.id,a.departuredt,a.departuredesc,a.type_ks
                from ecomm_umroh_departure a
                where a.dep_status = '1' and $xx
                order by a.departuredt";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getIdstk(){
        $slc = "select a.loccd,a.fullnm from mssc a
                where a.sctype = '1' and a.onlinetype = 'O' 
                and a.fullnm not in('TERMINATION','CANCEL','CENCEL') or a.loccd= 'IDBSS07'
                AND a.scstatus = '1' order by a.loccd";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
    }
    
    function getAmtUmrohh($orderid){
        //$this->db = $this->load->database('db_ecommerce', true);
        $aaa = substr(strtoupper($orderid),0,1);
        
        if($aaa == 'U' || $aaa == 'Z'){
            $slc = "select a.tot_fund,a.charge_connectivity,a.charge_admin from ecomm_umroh_pre_cicilan a 
                    where a.regno = '".$orderid."' AND a.status = '0'";
        }else{
            $slc = "select a.tot_fund,a.charge_connectivity,a.charge_admin from ecomm_umroh_save_mut a 
                    where a.paymentID = '".$orderid."' and a.pay_status = '0'";
        }
        
        //echo  $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getDtVchno($vchnoo,$idmember){
        $slc = "select a.voucherno,c.DistributorCode,b.fullnm,c.ExpireDate,
                c.updatedt,c.createdt,c.claimstatus,b.registerno,b.tipe_perjalanan
                 from db_ecommerce.dbo.ecomm_umroh_save_mut a
                 	inner join db_ecommerce.dbo.ecomm_umroh_save_child b on (a.id = b.id)
                    inner join klink_mlm2010.dbo.tcvoucher c on (c.VoucherNo = a.voucherno COLLATE SQL_Latin1_General_CP1_CS_AS)
                where a.voucherno = '".$vchnoo."' and c.DistributorCode = '".$idmember."'
                and a.pay_status = '1' and b.approval_status='1'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    public function cekSeq($tipe){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
        $y1=date("Y");
        
        if($tipe == 'umr'){
            $tbl = "ecomm_umroh_seq";
            //$pref = 'U';
        }else{
            $tbl = "ecomm_ziarah_seq";
            //$pref = 'Z';
        }
        
        $cek = "select * from $tbl";
        
        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "<br>ada<br>";
        }      
    }
    
    public function getRegisterNo($tipe){
        $this->db = $this->load->database('db_ecommerce', true);
        
        if($tipe == 'umr'){
            $tbl = "ecomm_umroh_seq";
            $pref = 'U';
        }else{
            $tbl = "ecomm_ziarah_seq";
            $pref = 'Z';
        }
        	
                   
        $sql = "SELECT * FROM $tbl 
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";
        
       $query = $this->db->query($sql);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }  
        }
        //$jumlah = $query->num_rows();
        
        $next_seq = sprintf("%08s",$ss);
        $registerNo  = "$pref"."$next_seq";
		
        return $registerNo;
    }
    
    
    
    //====JIKA BISA BEBERAPA JAMAAH DIDAFTARKAN DALAM 1X REGISTER
    /*function setUmrohData($tot_pass,$totbiaya){
        $this->db = $this->load->database('db_ecommerce', true);
        
        $dp = $totbiaya - 1000000;
        $arr = $this->session->userdata('umroh');
        $jum = count($arr);
        $sequence = $this->cekSeq();
        $regNo = $this->getRegisterNo();
        
        $insParent = "insert into ecomm_umroh_save_parent(dfno,fullnm,tel_hp,email,tot_pass,registerno,
                        tot_transfer,createnm)
                        values('".getUserID()."','".getUsername()."','".getUserPhone()."','".getUserEmail()."',
                        ".$tot_pass.",'".$regNo."',".$totbiaya.",'".getUserID()."')";
        echo "qry parent ".$insParent."<br>";
        //$qryParent = $this->executeQuery($insParent, $this->setDB(1));
        
        $n=1;
        for($i=0; $i < $jum; $i++)
        {
            if($arr[$i]['idmember'] == ""){
                $dfno = "";
            }else{
                $dfno = $arr[$i]['idmember'];
            }
            $birthdate = $arr[$i]['tgllhr']."/".$arr[$i]['blnlhr']."/".$arr[$i]['thnlhr'];
            
            $insChild = "insert into ecomm_umroh_save_child(registerno,seqno,dfno,fullnm,idno,birthplace,
                        birthdate,tel_hp,email,departureid,createnm)
                        values('".$regNo."',".$n++.",'".$dfno."','".$arr[$i]['fullnm']."',
                        ".$arr[$i]['idno'].",'".$arr[$i]['birthplace']."',".$birthdate.",
                        '".$arr[$i]['tel_hp']."','".$arr[$i]['email']."','".$arr[$i]['jdwlbrkt']."',
                        '".getUserID()."')";
            echo "qry child ".$insChild."<br>";
            //$qryChild = $this->executeQuery($insChild, $this->setDB(1));
        }
        
        $slc = "select id from ecomm_umroh_save_child
                where registerno = '".$regNo."'";
        echo "wuery scl ".$slc;
        //$query = $this->db->query($slc);
        if($query->num_rows() > 0){
            foreach($query->result() as $data)
            {   
                $insMutasi = "insert into ecomm_umroh_save_mut(id,mutate_flag,tot_fund,createnm)
                             values('".$data->id."','2',".$dp.",'".getUserID()."')";
                echo "qry mutasi ".$insMutasi."<br>";
                //$qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));
            }
        }else{
            echo "no data";
        }
            
        if($qryParent > 0 && $qryChild > 0 && $qryMutasi > 0){
            return $regNo;
        }else{
            return 0;
        }
    }*/
    
    
    function setUmrohData($tot_biaya,$bankCode,$connectivity,$charge_admin){
        $this->db = $this->load->database('db_ecommerce', true);
        
        $arr = $this->session->userdata('umroh');
        $biaya = $this->session->userdata('dp_umroh');
        
        //jika ziarah belum berjalan
        /*$tipePrjlnan = 'umr';
        $sequence = $this->cekSeq($tipePrjlnan);
        $regNo = $this->getRegisterNo($tipePrjlnan);*/
        
        //jika ziarah sudah berjalan
        $sequence = $this->cekSeq($arr[0]['tipePrjlanan']);
        $regNo = $this->getRegisterNo($arr[0]['tipePrjlanan']);
        
        $this->db->trans_begin();
        
        $insParent = "insert into ecomm_umroh_save_parent(dfno,fullnm,tel_hp,email,tot_pass,registerno,
                        tot_transfer,createnm)
                        values('".getUserID()."','".getUsername()."','".getUserPhone()."','".getUserEmail()."',
                        1,'".$regNo."',0,'".getUserID()."')";
        //echo "qry parent ".$insParent."<br>";
        $qryParent = $this->executeQuery($insParent, $this->setDB(1));
        
        
        foreach($arr as $dt)
        {
            if($dt['idmember'] == ""){
                $dfno = "";
            }else{
                $dfno = strtoupper($dt['idmember']);
            }
            
            $fullnm = strtoupper($dt['fullnm']);
            $idno = $dt['idno'];
            $birthplace = strtoupper($dt['birthplace']);
            $birthdate1 = $dt['blnlhr']."/".$dt['tgllhr']."/".$dt['thnlhr']."";
            $birthdate = date("Y-m-d",strtotime($birthdate1));
            $tel_hp = $dt['tel_hp'];
            $emailjamaah = $dt['email'];
            $jdwl = substr($dt['jdwlbrkt'],0,1);
            $tipe = $dt['tipeJamaah'];
            $addr1 = strtoupper($dt['addr1']);
            $addr2 = strtoupper($dt['addr2']);
            /*$kecamatan = strtoupper($dt['kecamatan']);
            $kota = strtoupper($dt['kota']);
            $kelurahan = strtoupper($dt['kelurahan']);
            $kota = strtoupper($dt['kota']);*/
            $passportno = strtoupper($dt['passportno']);
            $passportnm = strtoupper($dt['passportnm']);
            $sex = strtoupper($dt['sex']);
            $fathername = strtoupper($dt['fathersnm']);
            //$prov = strtoupper($dt['prov']);
            $idsponsor = strtoupper($dt['idsponsor']);
            $nmsponsor = strtoupper($dt['nmsponsor']);
            $tipe_perjalanan = $dt['tipePrjlanan'];
            //$tipe_perjalanan = "umr";
            if($tipe == "2"){
                $loccd = $this->session->userdata('store_info');
                $idstk = $loccd[0]->loccd;
            }else{
                $idstk = strtoupper($dt['idstk']);
                
            }
            //$idstk = strtoupper($dt['idstk']);
        }
            if($tipe_perjalanan == 'ysr'){
                $tipePrjalanan = '2';
            }else{
                $tipePrjalanan = '1';
            }
        
            $slc = "select id from ecomm_umroh_save_parent
                    where registerno = '".$regNo."'";
            $query = $this->db->query($slc);
            if($query->num_rows() > 0){
                foreach($query->result() as $data)
                {   
                    $id = $data->id;
                }
            }else{
                echo "no data";
            }
            
            if($tipe == "2"){
                $insChild = "insert into ecomm_umroh_save_child
                            (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,createnm,flag_tipe,idparent,
                            addr1,passportno,passportnm,flag_paytype,sex,father_name,loccd,sfno,tipe_perjalanan)
                            values('".$regNo."','".$dfno."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."',
                            '".$passportno."','".$passportnm."','0','".$sex."','".$fathername."','".$idstk."','".getUserID()."','".$tipePrjalanan."')";
                            
				
                
            }elseif($tipe == "3"){
                
                //PERCOBAAN
                /*$insChild = "insert into ecomm_umroh_save_child
                            (registerno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,
                            createnm,flag_tipe,idparent,addr1,passportno,passportnm,flag_paytype,
                            sex,father_name,loccd,sfno,approval_status,approvaldt)
                            values('".$regNo."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',
                            ".$id.",'".$addr1."','".$passportno."','".$passportnm."','0','".$sex."',
                            '".$fathername."','".$idstk."','".$idsponsor."','1','31/07/2015')";*/
                
                //live
                $insChild = "insert into ecomm_umroh_save_child
                            (registerno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,
                            createnm,flag_tipe,idparent,addr1,passportno,passportnm,flag_paytype,
                            sex,father_name,loccd,sfno,tipe_perjalanan)
                            values('".$regNo."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."',
                            '".$passportno."','".$passportnm."','0','".$sex."','".$fathername."','".$idstk."','".$idsponsor."','".$tipePrjalanan."')";
							
			    
                
            }else{
                $insChild = "insert into ecomm_umroh_save_child
                            (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,
                            createnm,flag_tipe,idparent,addr1,passportno,passportnm,flag_paytype,
                            sex,father_name,loccd,sfno,tipe_perjalanan)
                            values('".$regNo."','".$dfno."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."',
                            '".$passportno."','".$passportnm."','0','".$sex."','".$fathername."','".$idstk."','".$idsponsor."','".$tipePrjalanan."')";
            }
            
            //echo "qry child ".$insChild."<br>";
            $qryChild = $this->executeQuery($insChild, $this->setDB(1));
            
            
            
            $slcChild = "select id from ecomm_umroh_save_child
                    where registerno = '".$regNo."'";
            $queryChild = $this->db->query($slcChild);
            if($queryChild->num_rows() > 0){
                foreach($queryChild->result() as $data)
                {   
                    $idChild = $data->id;
                }
            }else{
                echo "no data";
            }
            
            if($tipe == "2"){
                $insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',0,
                        '".getUserID()."','".$biaya['order_id']."','DP Via SGO')";
            }else{
                $insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description,kd_bank,charge_connectivity,charge_admin)
                        values('".$idChild."','2',".$tot_biaya.",
                        '".getUserID()."','".$biaya['order_id']."','DP Via SGO','".$bankCode."',".$connectivity.",".$charge_admin.")";
            }
            $qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));
            
            //LIVE
            /*$insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',".$tot_biaya.",
                        '".getUserID()."','".$biaya['order_id']."','DP Via SGO')"; */
            
            //Percobaan            
            /*$insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description,paymentRef,pay_status)
                        values('".$idChild."','2',".$tot_biaya.",
                        '".getUserID()."','".$biaya['order_id']."','Payment Via SGO','SGO123456748','1')";*/
            //echo "qry mutasi ".$insMutasi."<br>";
            
            //$qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));
        
       
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            
            if($qryParent > 0 && $qryChild > 0 && $qryMutasi > 0){
                $arr = array('response' => 1,
                             'regno' => $regNo);
                return $arr;
            }else{
                $arr = array('response' => 0);
                return $arr;
            }
        }
    }
    
    function dtUmrohPending($tot_biaya){
        $exec_db = $this->load->database('db_ecommerce', true);
        
        
        $arr = $this->session->userdata('umroh');
        $biaya = $this->session->userdata('dp_umroh');
        
        //jika ziarah belum berjalan
        /*$tipePrjlnan = 'umr';
        $sequence = $this->cekSeq($tipePrjlnan);
        $regNo = $this->getRegisterNo($tipePrjlnan);*/
        
        //jika ziarah sudah berjalan
        $sequence = $this->cekSeq($arr[0]['tipePrjlanan']);
        $regNo = $this->getRegisterNo($arr[0]['tipePrjlanan']);
        
        $exec_db->trans_begin();
        
        $insParent = "insert into ecomm_umroh_save_parent(dfno,fullnm,tel_hp,email,tot_pass,registerno,
                        tot_transfer,createnm)
                        values('".getUserID()."','".getUsername()."','".getUserPhone()."','".getUserEmail()."',
                        1,'".$regNo."',0,'".getUserID()."')";
        //echo "qry parent ".$insParent."<br>";
        //$qryParent = $this->executeQuery($insParent, $this->setDB(1));
		//$qryParent = $this->executeQuery($insParent, $this->db1);
        $qryParent = $exec_db->query($insParent);  
        foreach($arr as $dt)
        {
            if($dt['idmember'] == ""){
                $dfno = "";
            }else{
                $dfno = strtoupper($dt['idmember']);
            }
            
            $fullnm = strtoupper($dt['fullnm']);
            $idno = strtoupper($dt['idno']);
            $birthplace = strtoupper($dt['birthplace']);
            $birthdate1 = $dt['blnlhr']."/".$dt['tgllhr']."/".$dt['thnlhr']."";
            $birthdate = date("Y-m-d",strtotime($birthdate1));
            $tel_hp = $dt['tel_hp'];
            $emailjamaah = $dt['email'];
            $jdwl = substr($dt['jdwlbrkt'],0,1);
            $tipe = $dt['tipeJamaah'];
            $addr1 = strtoupper($dt['addr1']);
            $addr2 = strtoupper($dt['addr2']);
            $passportno = strtoupper($dt['passportno']);
            $passportnm = strtoupper($dt['passportnm']);
            $sex = strtoupper($dt['sex']);
            $fathername = strtoupper($dt['fathersnm']);
            $idsponsor = strtoupper($dt['idsponsor']);
            $nmsponsor = strtoupper($dt['nmsponsor']);
            $tipe_perjalanan = $dt['tipePrjlanan'];
            //$tipe_perjalanan = "umr";
            if($tipe == "2"){
                $loccd = $this->session->userdata('store_info');
                $idstk = $loccd[0]->loccd;
            }else{
                $idstk = strtoupper($dt['idstk']); 
            }
        }
            if($tipe_perjalanan == 'ysr'){
                $tipePrjalanan = '2';
            }else{
                $tipePrjalanan = '1';
            }
            $slc = "select id from ecomm_umroh_save_parent
                    where registerno = '".$regNo."'";
            //$query = $this->db->query($slc);
			$query = $exec_db->query($slc); 
            if($query->num_rows() > 0){
                foreach($query->result() as $data)
                {   
                    $id = $data->id;
                }
            }
            
            if($tipe == "2"){
                $insChild = "insert into ecomm_umroh_save_child
                            (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,createnm,flag_tipe,idparent,
                            addr1,passportno,passportnm,flag_paytype,sex,father_name,loccd,tipe_perjalanan)
                            values('".$regNo."','".$dfno."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."',
                            '".$passportno."','".$passportnm."','1','".$sex."','".$fathername."','".$idstk."','".$tipePrjalanan."')";
                
            }elseif($tipe == "3"){
                
                //live
                $insChild = "insert into ecomm_umroh_save_child
                            (registerno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,
                            createnm,flag_tipe,idparent,addr1,passportno,passportnm,flag_paytype,
                            sex,father_name,loccd,sfno,tipe_perjalanan)
                            values('".$regNo."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."',
                            '".$passportno."','".$passportnm."','1','".$sex."','".$fathername."','".$idstk."','".$idsponsor."','".$tipePrjalanan."')";
               
                
            }else{
                $insChild = "insert into ecomm_umroh_save_child
                            (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,
                            createnm,flag_tipe,idparent,addr1,passportno,passportnm,flag_paytype,
                            sex,father_name,loccd,sfno,tipe_perjalanan)
                            values('".$regNo."','".$dfno."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                            '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."',
                            '".$passportno."','".$passportnm."','1','".$sex."','".$fathername."','".$idstk."','".$idsponsor."','".$tipePrjalanan."')";
                
            }
            
            //echo "qry child ".$insChild."<br>";
            //$qryChild = $this->executeQuery($insChild, $this->setDB(1));
			$qryChild = $exec_db->query($insChild); 
            //echo "qry mutasi ".$insMutasi."<br>";
            
            
            $slcChild = "select id from ecomm_umroh_save_child
                    where registerno = '".$regNo."'";
            //$queryChild = $this->db->query($slcChild);
            $queryChild = $exec_db->query($slcChild);
            if($queryChild->num_rows() > 0){
                foreach($queryChild->result() as $data)
                {   
                    $idChild = $data->id;
                }
            }else{
                echo "no data";
            }
            
            
            if($tipe == "2"){
                $insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',0,
                        '".getUserID()."','Transfer','DP Via Transfer')";
                
            }else{
                $insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',".$tot_biaya.",
                        '".getUserID()."','Transfer','DP Via Transfer')";
            }
            //$qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));
            $qryMutasi = $exec_db->query($insMutasi);
            
            if ($exec_db->trans_status() === FALSE)
            {
                $exec_db->trans_rollback();
            }
            else
            {
                $exec_db->trans_commit();
                
                if($qryParent > 0 && $qryChild > 0 && $qryMutasi > 0){
                    $arr = array('response' => 1,
                                 'regno' => $regNo);
                    return $arr;
                }else{
                    $arr = array('response' => 0);
                    return $arr;
                }
            }
    }
    
    function updttblMutasi($order_id,$paymentRef,$amount,$nmbank){
        $today = date("Y-m-d H:i:s");
        
        //develop
        /*$updtMutasi = "update ecomm_umroh_save_mut set paymentRef = '".$paymentRef."',
                       pay_status = '0',tot_fund = ".$amount."
                       where paymentID = '".$order_id."'";*/
        
        //live
        $updtMutasi = "update ecomm_umroh_save_mut set paymentRef = '".$paymentRef."',
                       pay_status = '1',tot_fund = ".$amount."
                       where paymentID = '".$order_id."'";
        //echo "update ".$updtMutasi;
        $qryUpdt = $this->executeQuery($updtMutasi, $this->setDB(1));
                       
        $slc = "select id from ecomm_umroh_save_mut where paymentID = '".$order_id."'";
        $querySlc = $this->db->query($slc);
            if($querySlc->num_rows() > 0){
                foreach($querySlc->result() as $data)
                {   
                    $idChild = $data->id;
                }
            }
        //develop
        /*$updtChild = "update ecomm_umroh_save_child set approval_status = '0'
                       where id = '".$idChild."'";*/
        
        //live              
        $updtChild = "update ecomm_umroh_save_child set approval_status = '1',
                       approvaldt = '".$today."',
                       where id = '".$idChild."'";
        //echo "update ".$updtMutasi;
        $qryUpdtChild = $this->executeQuery($updtChild, $this->setDB(1));
        
        
        if($qryUpdt > 0 && $qryUpdtChild > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    function updttblMutasi1($order_id,$paymentRef,$amount,$nmbank){
        $today = date("Y-m-d H:i:s");
        
        //develop
        $updtMutasi = "update ecomm_umroh_save_mut set paymentRef = '".$paymentRef."',
                       pay_status = '0',tot_fund = ".$amount."
                       where paymentID = '".$order_id."'";
        
        //live
        /*$updtMutasi = "update ecomm_umroh_save_mut set paymentRef = '".$paymentRef."',
                       pay_status = '1',tot_fund = ".$amount."
                       where paymentID = '".$order_id."'";*/
        //echo "update ".$updtMutasi;
        $qryUpdt = $this->executeQuery($updtMutasi, $this->setDB(1));
                       
        $slc = "select id from ecomm_umroh_save_mut where paymentID = '".$order_id."'";
        $querySlc = $this->db->query($slc);
            if($querySlc->num_rows() > 0){
                foreach($querySlc->result() as $data)
                {   
                    $idChild = $data->id;
                }
            }
        //develop
        /*$updtChild = "update ecomm_umroh_save_child set approval_status = '0'
                       where id = '".$idChild."'";*/
        
        //live              
        $updtChild = "update ecomm_umroh_save_child set approval_status = '1',
                       approvaldt = '".$today."',
                       where id = '".$idChild."'";
        //echo "update ".$updtMutasi;
        $qryUpdtChild = $this->executeQuery($updtChild, $this->setDB(1));
        
        
        if($qryUpdt > 0 && $qryUpdtChild > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    
    function updtMutasiCicilan($order_id,$paymentRef,$amount,$nmbank){
        $today = date("Y-m-d H:i:s");
        $slc = "select a.id from ecomm_umroh_save_child a where a.registerno = '".$order_id."'";
        $querySlc = $this->db->query($slc);
        if($querySlc->num_rows() > 0){
            foreach($querySlc->result() as $data)
            {   
                $idChild = $data->id;
            }
        }
        
        $slcPre = "select a.createnm,a.charge_connectivity,a.charge_admin
                    from ecomm_umroh_pre_cicilan a where a.regno = '".$order_id."'";
        $queryPre = $this->db->query($slcPre);
        if($queryPre->num_rows() > 0){
            foreach($queryPre->result() as $data)
            {   
                $createnm = $data->createnm;
                $connectivity = $data->charge_connectivity;
                $admin = $data->charge_admin;
            }
        }
        
        $insCicilan = "insert into ecomm_umroh_save_mut (id,mutate_flag,tot_fund,createnm,description,is_voucher,paymentID,paymentRef,pay_status,kd_bank,charge_connectivity,charge_admin)
                values(".$idChild.",'2',".$amount.",'".$createnm."','Cicilan K-smart Umroh','0','".$order_id."','".$paymentRef."','1','".$nmbank."',".$connectivity.",".$admin.")";
        $qryIns = $this->executeQuery($insCicilan, $this->setDB(1));
        
        $updtCicilan = "update ecomm_umroh_pre_cicilan set status = '1' where regno = '".$order_id."'";
        $qryCicilan = $this->executeQuery($updtCicilan, $this->setDB(1));
        //echo $updtCicilan;
        /*if($qryIns > 0 && $qryCicilan > 0){
            return 1;
        }else{
            return 0;
        }*/
    }
    
    function getDtJamaah($orderid){
        $slc = "select a.paymentID,a.paymentRef,a.id,a.voucherno,a.is_voucher,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp,
                b.birthplace,b.birthdate,b.departureid,b.id,c.departuredesc,
                c.departuredt,d.dfno as idrekruiter,d.fullnm as rekruiternm,
                e.secno,e.CNno,b.novac,b.tipe_perjalanan
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    inner join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
                    inner join ecomm_trans_hdr e on(b.registerno = e.orderno)
                where a.paymentID = '".$orderid."' and a.pay_status = '0' and a.is_voucher = '2'";
         
         
         //develop       
        /*$slc = "select a.paymentID,a.paymentRef,a.id,a.voucherno,a.is_voucher,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp,
                b.birthplace,b.birthdate,b.departureid,b.id,c.departuredesc,
                c.departuredt,d.dfno as idrekruiter,d.fullnm as rekruiternm,e.secno,e.CNno,b.novac,b.tipe_perjalanan
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    inner join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
                    left join ecomm_trans_hdr e on(b.registerno = e.orderno)
                where a.paymentID = '".$orderid."' and a.pay_status = '0'";*/
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function getDtCicilan($orderid){
        $slc = "select a.idmember,a.regno,a.nmmember,a.tot_fund,
                b.approval_status,b.tel_hp,b.tipe_perjalanan,b.departureid,d.departuredesc
                 from ecomm_umroh_pre_cicilan a
                 	inner join ecomm_umroh_save_child b on a.regno = b.registerno 
                 	inner join ecomm_umroh_save_mut c on c.id = b.id
                    inner join ecomm_umroh_departure d on b.departureid = d.id
                where a.[status] = '1' and a.regno = '".$orderid."' 
                AND C.pay_status = '1' and c.is_voucher = '0'";
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getDtparent($noreg){
        $slc = "select a.dfno,a.fullnm,a.tel_hp as parenthp,b.registerno,
                b.dfno,b.fullnm,c.departuredesc,d.voucherno
                from ecomm_umroh_save_parent a
                    inner join ecomm_umroh_save_child b on (a.registerno = b.registerno)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    inner join ecomm_umroh_save_mut d on(d.id = b.id)
                where a.registerno = '".$noreg."' and d.pay_status = '1'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function pdfCicilanUmroh($regnos){
        $slc = "SELECT a.dfno,a.fullnm,a.registerno,a.idno,a.tipe_perjalanan,
                a.tel_hp,a.departureid,c.departuredesc,c.departuredt,
                d.paymentRef,d.id,d.kd_bank,e.bankDesc,d.tot_fund,SUM(d.tot_fund) AS tot_tab
                from ecomm_umroh_save_child a
                inner join ecomm_umroh_departure c on (c.id = a.departureid)
                inner join ecomm_umroh_save_mut d on(a.id = d.id)
                inner join ecomm_umroh_bank e on(d.kd_bank = e.bankCode)
                where a.registerno = '".$regnos."' and d.pay_status = '1'
                AND d.[description] = 'Cicilan K-smart Umroh'
                group by a.dfno,a.fullnm,a.registerno,a.idno,
                a.tel_hp,a.departureid,c.departuredesc,c.departuredt,
                d.paymentRef,d.id,d.kd_bank,e.bankDesc,d.tot_fund";
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function pdfUmroh($registerno){
        $slc = "SELECT a.dfno,a.fullnm,a.registerno,a.idno,a.sex,
                a.addr1,a.kota,a.kecamatan,a.father_name,a.tel_hp,
                a.birthdate,a.birthplace,a.flag_tipe,a.departureid,a.tipe_perjalanan,
                b.dfno as idrekruiter,b.fullnm as rekruiternm,
                c.departuredesc,c.departuredt,d.paymentID,d.paymentRef,d.id
                from ecomm_umroh_save_child a
                	inner join ecomm_umroh_save_parent b on(a.registerno = b.registerno)
                	inner join ecomm_umroh_departure c on (c.id = a.departureid)
                    inner join ecomm_umroh_save_mut d on(a.id = d.id)
                where a.registerno = '".$registerno."'";
                //and d.pay_status = '1' and a.approval_status='1' and d.mutate_flag = '2'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function pdfUmroh1($registerno){
        $slc = "SELECT a.dfno,a.fullnm,a.registerno,a.idno,a.sex,
                a.addr1,a.kota,a.kecamatan,a.father_name,a.tel_hp,
                a.birthdate,a.birthplace,a.flag_tipe,a.departureid,a.tipe_perjalanan,
                b.dfno as idrekruiter,b.fullnm as rekruiternm,
                c.departuredesc,c.departuredt,d.paymentID,d.paymentRef,d.id
                from ecomm_umroh_save_child a
                	inner join ecomm_umroh_save_parent b on(a.registerno = b.registerno)
                	inner join ecomm_umroh_departure c on (c.id = a.departureid)
                    inner join ecomm_umroh_save_mut d on(a.id = d.id)
                where a.registerno = '".$registerno."' and d.pay_status = '0'
                and a.flag_paytype = '1'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function getDtJamaahByTrf($registerno){
        $slc = "SELECT a.dfno,a.fullnm,a.registerno,a.idno,a.sex,
                a.addr1,a.kota,a.kecamatan,a.father_name,a.tel_hp,
                a.birthdate,a.birthplace,a.flag_tipe,a.departureid,
                b.dfno as idrekruiter,b.fullnm as rekruiternm,
                c.departuredesc,c.departuredt,a.tipe_perjalanan
                from ecomm_umroh_save_child a
                	inner join ecomm_umroh_save_parent b on(a.registerno = b.registerno)
                	inner join ecomm_umroh_departure c on (c.id = a.departureid)
                    inner join ecomm_umroh_save_mut d on(a.id = d.id)
                where a.registerno = '".$registerno."' and d.pay_status = '0' 
                and a.flag_paytype='1' and a.approval_status ='0'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function headMutasi(){
        $dt = $this->input->post(null,true);
		//echo "hello....".$dt['idmembers'];
		if($dt['idmembers'] != ""){
			if($dt['searchMutation']== "idmember"){
	            $xx = "a.dfno = '".$dt['idmembers']."'";
	        }else{
	            $xx = "a.registerno = '".$dt['noregisters']."'";
	        }
	        $slc = "select a.fullnm,a.dfno,
	                a.departureid,a.depart_status,a.tipe_perjalanan,
	                b.departuredesc,b.departuredt,
	                cc.novac
	                from ecomm_umroh_save_child a
	                inner join ecomm_umroh_departure b on(a.departureid = b.id)
					left outer join klink_mlm2010.dbo.msmemb cc on cc.dfno=a.dfno collate SQL_Latin1_General_CP1_CS_AS 
	                where $xx";
	        //echo $slc;
	        $result = $this->getRecordset($slc,null,$this->db1);
		}else{
	    	$result = null;
		}
		
		return $result;
		
        
    }
    
    function mutasiUmroh(){
        $dt = $this->input->post(null,true);
        if($dt['searchMutation']== "idmember"){
            $xx = "a.dfno = '".$dt['idmembers']."' and b.pay_status = '1' and b.mutate_flag = '2' and b.is_voucher = '0' order by b.createdt";
        }else{
            $xx = "a.registerno = '".$dt['noregisters']."' and b.pay_status = '1' and b.mutate_flag = '2' and b.is_voucher = '0' order by b.createdt";
        }
        $slc = "select a.fullnm,a.dfno,a.tipe_perjalanan,
                b.mutate_flag,b.tot_fund,b.is_voucher,b.createdt,b.description
                from ecomm_umroh_save_child a
                	inner join ecomm_umroh_save_mut b on(a.id = b.id)
                where $xx";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function insPrePayment($dt,$bankCode,$connectivity,$admin){
        $ins = "insert into ecomm_umroh_pre_cicilan(regno,idmember,nmmember,tot_fund,createnm,charge_connectivity,charge_admin)
                values('".strtoupper($dt['regnos'])."','".$dt['idmember']."','".$dt['nmmember']."',".$dt['amtUmroh'].",'".getUserID()."',".$connectivity.",".$admin.")";
        $result = $this->executeQuery($ins, $this->setDB(1));
        return $result;
    }
    
    function getBank(){
        $slc = "select * from ecomm_bank a where a.status = '1' AND a.bankDesc !='CREDITCARD'
                order by bankDesc";
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getCharge($bankCode){
        $slc = "select * from ecomm_bank where bankCode = '".$bankCode."'";
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getPrintVch($regno){
        $slc = "select a.id,a.voucherno,c.createdt,a.tot_fund,
                b.dfno,b.fullnm,b.registerno,b.tipe_perjalanan
                 from ecomm_umroh_save_mut a
                 	INNER join ecomm_umroh_save_child b on(a.id = b.id)
                    inner join klink_mlm2010.dbo.tcvoucher c on(a.voucherno = c.VoucherNo COLLATE SQL_Latin1_General_CP1_CS_AS)
                 where b.registerno = '".$regno."' and a.is_voucher = '2'
                 and a.[description] = 'VOUCHER' AND c.claimstatus = '0'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    
    
    
 /*==== TESTING ====*/   
 
    function testtQuery(){
        $slc = "select * from ecomm_umroh_departure";
        $query = $this->db->query($slc);
        $xx = $query->num_rows();
        
        return $xx;
    }
    
    function getRegno(){
        $this->db = $this->load->database('db_test', true);
        
        $slc = "SELECT nomer FROM register
                WHERE nomer = ( SELECT MAX(nomer) FROM register )";
        
        $query = $this->db->query($slc);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                
                $ss = $data->nomer+1;
            }  
        }
        //$jumlah = $query->num_rows();
        $month = date("m");
        $year = date("y");
        $next_seq = sprintf("%02s",$ss);
        echo $next_seq;
        $registerNo  = "P".$month."".$year."".$next_seq."";
        
        return $registerNo;
    }
    
    
    function insDbTest(){
        $this->db = $this->load->database('db_test', true);
        
        $regno = $this->getRegno();
        
        $insert_member = array(
			'registerno' => "".$regno."",
			'fullnm' => "fgdf",
			'ktpno' => "46546456",
			'sex' => "F",
			'tel_hp' => "05645454",
		);
	
		$insert = $this->db->insert('register', $insert_member);
		if($this->db->affected_rows()){
		  /*$arr = array('response' => '1',
                'regno' => $regno);
            return $arr;*/
            return 1;
		}
        else{
            /*$arr = array('response' => '0');
            return $arr;*/
            return 0;
        }
        //return $regno;
    }
    
 }
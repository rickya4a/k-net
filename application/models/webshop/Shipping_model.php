<?php
class Shipping_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }

    function getArea(){
        $qry = "select st_id, description 
                FROM state where cn_id = 'ID' and description != 'TEMPORARY'";
        $res = $this->getRecordset($this->db2, $qry);
        return $res;
    }    
    
    function showStockistByArea($username){
        $qry ="SELECT a.dfno,a.fullnm,a.loccd,b.[state]
                FROM MSMEMB a
	               inner join mssc b on a.loccd = b.loccd
                WHERE a.DFNO = '".$username."'
                AND b.fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL')";
        
        //echo $qry;
        $res = $this->getRecordset($this->db2, $qry);
        return $res;
    }
    
    function showStockistByArea2($state){
        $arr = jsonFalseResponse();
        $sql = "select loccd, fullnm
                FROM mssc 
                where state = '".$state."' and sctype != '3' 
                AND fullnm not in('TERMINATION','CANCEL','BUSSINESS & DEVELOPMENT','INTERNET ID','CENCEL') and
		scstatus='1' order by sctype, fullnm ";
        
        $res = $this->getRecordset($this->db2, $sql);
        if($res != null) {
            $arr = jsonTrueResponse($res);
        }
        return $arr;
    }
    
    function showPricecodeStockist($loccd){
         $arr = jsonFalseResponse();
        $sql = "select loccd, fullnm,pricecode
                FROM mssc 
                where loccd = '".$loccd."'";
        
        $res = $this->getRecordset($this->db2, $sql);
        if($res != null) {
            $arr = jsonTrueResponse($res);
        }
        return $arr;
    }
    
    function getListProvinsi(){
        $qry = "select * from master_wil_provinsi";
        $res = $this->getRecordset($this->db1, $qry);
        return $res;
    }
    
    function getListProvinsi2() {
        //$qry = "select * from master_wil_provinsi";
        /*===============================edit hilal @2015-05-19=================================*/
		/*  
		 $qry = "SELECT 
				  dbo.master_wil_provinsi.kode_provinsi,
				  dbo.master_wil_provinsi.provinsi
				FROM
				  dbo.master_wil_provinsi
				  INNER JOIN dbo.master_wil_kabupaten ON (dbo.master_wil_provinsi.kode_provinsi = dbo.master_wil_kabupaten.kode_provinsi)
				  INNER JOIN dbo.master_wil_kecamatan ON (dbo.master_wil_kabupaten.kode_kabupaten = dbo.master_wil_kecamatan.kode_kabupaten)
				WHERE
				  (dbo.master_wil_kecamatan.kode_kab_JNE is not null)
				GROUP BY dbo.master_wil_provinsi.kode_provinsi,
				dbo.master_wil_provinsi.provinsi
				ORDER BY dbo.master_wil_provinsi.provinsi";
		 */
		
		$qry = "SELECT A.kode_provinsi, A.provinsi
				FROM V_ECOMM_WILAYAH2 A
				GROUP BY A.kode_provinsi, A.provinsi
				ORDER BY A.provinsi";
		/*===============================end edit hilal @2015-05-19=================================*/
		
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function showListKota($prov){
        $arr = jsonFalseResponse();
        $sql = "SELECT 
                  a.provinsi,
                  b.kode_kabupaten,
                  b.kabupaten
                FROM
                  master_wil_provinsi a
                  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
                WHERE
                  a.kode_provinsi = '".$prov."'";
        
        $res = $this->getRecordset($this->db1, $sql);
        if($res != null) {
            $arr = jsonTrueResponse($res);
        }
        return $arr;
    }
    
    function showListKota2($prov) {
        /*$qry = "SELECT 
                  a.provinsi,
                  b.kode_kabupaten,
                  b.kabupaten
                FROM
                  master_wil_provinsi a
                  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
                WHERE
                  a.kode_provinsi = '".$prov."'"; */
                  
        /*===============================edit hilal @2015-05-19=================================*/          
        /*$qry = "SELECT 
				  a.provinsi,
				  b.kode_kabupaten,
				  b.kabupaten
				FROM
				  master_wil_provinsi a
				  INNER JOIN master_wil_kabupaten b ON (a.kode_provinsi = b.kode_provinsi)
				  INNER JOIN master_wil_kecamatan c ON (b.kode_kabupaten = c.kode_kabupaten)
				WHERE
				  c.kode_kab_JNE is not null AND
				  a.kode_provinsi = '".$prov."'
				GROUP BY a.provinsi,
				b.kode_kabupaten, b.kabupaten
				ORDER BY a.provinsi";	
		*/
		$qry = "SELECT a.provinsi, a.kode_kabupaten, a.kabupaten
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kab_JNE is not null AND a.kode_provinsi = '".$prov."'
				GROUP BY a.provinsi, a.kode_kabupaten, a.kabupaten
				ORDER BY A.kabupaten";
				
		/*===============================end edit hilal @2015-05-19=================================*/
		//echo $qry;	
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function showListKecamatan($kota){
        $arr = jsonFalseResponse();
        $sql = "SELECT 
                b.kode_kecamatan,
                b.kecamatan,
                a.kabupaten
                FROM
                    master_wil_kabupaten a
                    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
                WHERE
                a.kode_kabupaten = '".$kota."'";
        
        $res = $this->getRecordset($this->db1, $sql);
        if($res != null) {
            $arr = jsonTrueResponse($res);
        }
        return $arr;
    }
    
    function showListKecamatan2($kota) {
    	/*$qry = "SELECT 
                b.kode_kecamatan,
                b.kecamatan,
                a.kabupaten
                FROM
                    master_wil_kabupaten a
                    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
                WHERE
                a.kode_kabupaten = '".$kota."'"; */
                  
        /*===============================edit hilal @2015-05-19=================================*/          
        /*$qry = "SELECT 
					b.kode_kecamatan,
					b.kecamatan,
					a.kabupaten,
				    b.kode_kec_JNE
				FROM
				    master_wil_kabupaten a
				    INNER JOIN master_wil_kecamatan b ON (a.kode_kabupaten = b.kode_kabupaten)
				WHERE
				a.kode_kabupaten = '".$kota."' AND
				b.kode_kec_JNE is not null";    
		 */
		 
		 $qry = "SELECT A.kode_kecamatan, A.kecamatan, A.kabupaten, A.kode_kec_JNE
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kabupaten = '".$kota."' AND A.kode_kec_JNE is not null 
				GROUP BY A.kode_kecamatan, A.kecamatan, A.kabupaten, A.kode_kec_JNE
				ORDER BY A.kecamatan";
		 /*===============================end edit hilal @2015-05-19=================================*/    
		//echo $qry;		
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
    
    function showListKelurahan($kec){
        $arr = jsonFalseResponse();
        $sql = "SELECT 
                  distinct (a.kelurahan),
                  a.kodepos,
                  a.kode_kelurahan,
                  b.kecamatan
                FROM
                  master_wil_kecamatan b
                  INNER JOIN master_wil_kelurahan a ON (b.kode_kecamatan = a.kode_kecamatan)
                WHERE
                  b.kode_kecamatan = '".$kec."'";
        
        $res = $this->getRecordset($this->db1, $sql);
        if($res != null) {
            $arr = jsonTrueResponse($res);
        }
        return $arr;
    }
    
    function showListKelurahan2($kecamatan) {
    	$this->db = $this->load->database("db_ecommerce", true);	
    	$qry = "SELECT A.kode_kec_JNE,  A.kode_kecamatan,  A.kecamatan
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kecamatan = '".$kecamatan."'";
		$query = $this->db->query($qry);
		$res = $query->result();
		if ($res == 0) 
        {
            return null;
        } 
        else
        {
            $kec_jne = "SELECT a.KEC_JNE, a.loccd, a.fullnm, A.kode_kec_JNE_Origin,a.state
				FROM V_ECOMM_WILAYAH2 A
				WHERE A.kode_kec_JNE='".$res[0]->kode_kec_JNE."' 
					  AND a.fullnm not in ('TERMINATION','CANCEL','CENCEL','TWA TEST')
					  AND (A.kode_kec_JNE_Origin is not null or A.kode_kec_JNE_Origin != '')";
			 /*===============================end edit hilal @2015-05-19=================================*/  
			 
			//echo "query ".$qry;
	        $query2 = $this->db->query($kec_jne);
			$res2 = $query2->result();
			
			$arr = $valReturn = array(
				  "arrayData" => $res,
				  "listStk" => $res2
				);
	        return $arr;		
            
            
        }
    }
    
    function showPricecodeStockistX($loccd, $kec){
		 //edit hilal @27/09/2018
		/*$qry = "select b.City_Code_Dest AS KEC_JNE, a.loccd, a.fullnm,a.pricecode, a.[state],
					  	b.whcd_origin as kode_kec_JNE_Origin, b.wh_jne_branchcd AS jne_branchcd,
						b.ProvinsiNama as provinsi, b.Kota_Kabupaten as kabupaten, b.Kecamatan as kecamatan
				from klink_mlm2010.dbo.mssc a
				inner join db_ecommerce.dbo.jne_Tabel_Zona_Induk b 
					  on a.loccd=b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS
				where b.kode_kecamatan = '$kec' 
						AND a.loccd = '$loccd' 
						and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')";
		*/
		 $date = date("Y-m-d");
		 $tbl = "V_ECOMM_WILAYAH3";
		if($date >= "2019-03-01") {
			$tbl = "V_ECOMM_WILAYAH3_BARU";
		}
		
		$qry = "select a.City_Code_Dest AS KEC_JNE, a.loccd, a.fullnm, ltrim(rtrim(a.pricecode)) as pricecode, a.[state],
				        a.whcd_origin as kode_kec_JNE_Origin, a.wh_jne_branchcd AS jne_branchcd,
				        a.provinsi as provinsi, a.Kota_Kabupaten as kabupaten, a.Kecamatanx as kecamatan
				from $tbl a
				where a.kode_kecamatan = '$kec' 
				        AND a.loccd = '$loccd' 
				        and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')"; 
        //echo "qry ".$qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }

    function showPricecodeStockistCOD($loccd, $kec){
        //edit hilal @27/09/2018
        /*$qry = "select b.City_Code_Dest AS KEC_JNE, a.loccd, a.fullnm,a.pricecode, a.[state],
                          b.whcd_origin as kode_kec_JNE_Origin, b.wh_jne_branchcd AS jne_branchcd,
                        b.ProvinsiNama as provinsi, b.Kota_Kabupaten as kabupaten, b.Kecamatan as kecamatan
                from klink_mlm2010.dbo.mssc a
                inner join db_ecommerce.dbo.jne_Tabel_Zona_Induk b
                      on a.loccd=b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS
                where b.kode_kecamatan = '$kec'
                        AND a.loccd = '$loccd'
                        and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')";
        */
        $kec=str_replace("%20"," ",$kec);
        $date = date("Y-m-d");
        $tbl = "V_ECOMM_WILAYAH3";
        if($date >= "2019-03-01") {
            $tbl = "V_ECOMM_WILAYAH3_BARU";
        }

       /* $qry = "select a.City_Code_Dest AS KEC_JNE, a.loccd, a.fullnm, ltrim(rtrim(a.pricecode)) as pricecode, a.[state],
				        a.whcd_origin as kode_kec_JNE_Origin, a.wh_jne_branchcd AS jne_branchcd,
				        a.provinsi as provinsi, a.Kota_Kabupaten as kabupaten, a.Kecamatanx as kecamatan
				from $tbl a
				where a.kecamatan LIKE '%$kec%'
				        AND a.loccd = '$loccd'
				        and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')";*/
        $qry="select a.loccd,ltrim(rtrim(a.pricecode)) as pricecode
				from master_warehouse_list a
				where --a.kecamatan LIKE '%$kec%' AND
						a.loccd = '$loccd'
				        and a.fullnm not in('TERMINATION','CANCEL','CENCEL','TWA TEST')
						group by a.loccd, a.pricecode";
        //echo "qry ".$qry;
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;
    }
}
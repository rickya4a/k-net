<?php
class M_ppob_trx extends MY_Model {
	function __construct() {
        // Call the Model constructor
        parent::__construct();		
    }
	
	function getDetailTempPPOBTrxByID($id) {
		$qry = "SELECT a.*, b.cust_no, b.reff_pay_id  
		        FROM ppob_trx_hdr_tempsgo a
		        LEFT JOIN ppob_trx_det_tempsgo b ON (a.trx_id = b.trx_id)
		        WHERE a.trx_id = '$id'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getPpobTrxById($id, $type) {
		if($type == "temp") {
			$hdr = "ppob_trx_hdr_tempsgo";
			$det = "ppob_trx_det_tempsgo";
			
			$qry = "SELECT a.*, b.cust_no, b.reff_pay_id 
		        FROM $hdr a
		        LEFT JOIN $det b ON (a.trx_id = b.trx_id)
		        WHERE a.trx_id = '$id'";
		} else {
			$hdr = "ppob_trx_hdr1";
			$det = "ppob_trx_det1";
			
			$qry = "SELECT a.*, b.cust_no, b.reff_pay_id , c.param_data
		        FROM $hdr a
		        INNER JOIN ppob_trx_hdr_tempsgo c ON (a.trx_id = c.trx_id)
		        LEFT JOIN $det b ON (a.trx_id = b.trx_id)
		        WHERE a.trx_id = '$id'";
		}
		
		return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function insertPpobFromTemp($orderid) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();
		$ins = "INSERT INTO ppob_trx_hdr1 (trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, ppob_type, amount_sgo)	
		        SELECT trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, ppob_type, amount_sgo 
		        FROM ppob_trx_hdr_tempsgo WHERE trx_id = '$orderid'"; 
		$res = $dbqryx->query($ins);
		$ins2 = "INSERT INTO ppob_trx_det1 (trx_id, memberid_target, qty, nominal, no_hp_simcard, cust_no, reff_pay_id)
		         SELECT trx_id, memberid_target, qty, nominal, no_hp_simcard, cust_no, reff_pay_id 
		         FROM ppob_trx_det_tempsgo WHERE trx_id = '$orderid'"; 
		$res2 = $dbqryx->query($ins2);
		//END TRANSACTION	
		$dbqryx->trans_complete();
		
		if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			);
        } 
	}
}
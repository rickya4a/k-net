<?php

class M_promo_lbc extends MY_Model{

    public function __construct(){

        parent::__construct();
    }

    public function getKupon(){

        $qry = "SELECT b.fullnm, a.dfno, CONVERT(VARCHAR(10), a.bnsperiod, 103) as bnsperiod, x.total
                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                LEFT JOIN msmemb b
                on a.dfno = b.dfno
                LEFT JOIN
                (SELECT b.dfno, COUNT(*) as total
                 FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                 LEFT JOIN msmemb b
                 on a.dfno = b.dfno
                 GROUP BY b.dfno) x
                ON x.dfno=b.dfno
                GROUP BY b.fullnm, a.dfno, bnsperiod, x.total";
        //echo $qry;
        $arr = $this->getRecordset($qry, NULL, $this->db2);

        return $arr;

    }

    public function getDetail($dfno){

        $qry = "select * from dummy_LBC where id_member = '$dfno'";
        //echo $qry;
        $arr = $this->getRecordset($qry, NULL, $this->db2);

        return $arr;
    }

    public function getDetailHdr($dfno){

        $sql = "select fullnm,CONVERT(VARCHAR(10), bnsperiod, 103) as bnsperiod
                from TWA_NONSUPLEMEN_UNDIAN where dfno = '$dfno'
                GROUP BY dfno,fullnm,bnsperiod
                ";
        $arr = $this->getRecordset($sql, NULL, $this->db2);

        return $arr;
    }

    public function cekLBC($dfno){

        $sql = "select *
                from TWA_LBCPROMO2018 a
                where a.dfno = '$dfno' --and a.bnsperiod = '2018-10-01'";

        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function cekLBC2($dfno){

        $sql = "select dfno, fullnm, sex, sfno_reg, jointdt, bnsperiod, qDP, qBV
                from QTWA_NEWTRHTRD3_GROUP
                where dfno = '$dfno' and bnsperiod >= '2018-10-01'";
        //echo $sql;
        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function cekLBC3($dfno){

        $sql = "select *
                from TWA_LBCPROMO2018 a
                where a.dfno = '$dfno' order by BNSPERIOD --and a.bnsperiod = ''";
        //echo $sql;
        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function detLBC1($dfno, $bnsperiod){

        $sql = "select * FROM QTWA_NEWTRH_TRDLBC
                where dfno = '$dfno' and bnsperiod = '$bnsperiod'";

        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function detLBC2($dfno, $bnsperiod){


        $sql = "select * from QTWA_SCNEWTRH_TRDLBC
                where dfno = '$dfno' and bnsperiod = '$bnsperiod'";

        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function getDetailLBC($dfno, $bnsperiod){

        $sql = "select top 1000 * from QTWA_SCNEWTRH_TRDLBC
                where bnsperiod = '$bnsperiod' and dfno = '$dfno' ";
        //echo $sql;
        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function getDetailRekrutanLBC($dfno, $bnsperiod){

//        $sql = "select * from QSCNEWTRD_LBC
//                where bnsperiod = '$bnsperiod' and sfno_reg = '$dfno' and jointdt >= '2018-10-01'";
//        $sql = "select * from QSCNEWTRD_LBC
//                where sfno_reg = '$dfno' and jointdt >= '2018-10-01' ";
        $sql = "select a.DFNO,a.FULLNMR,a.JOINTDT,a.JSEX,a.STATUS_REKRUTAN,a.TBV_R
                from TWA_LBCPROMO2018_DETAIL a
                where a.SFNO_REG = '$dfno'
                order by a.jointdt,a.dfno";
        //echo $sql;
        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

    public function totBV($dfno){

        $sql = "select sum(TBV_R) as total_bv
                from TWA_LBCPROMO2018_DETAIL a
                where a.SFNO_REG = '$dfno'";

        $result = $this->getRecordset($sql, null, $this->db7);

        return $result;
    }

}
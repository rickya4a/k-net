<?php
class Event_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();	
    }
    
    function showEventInfo(){
        $slc = "select a.event_id, 
    				   a.event_desc, 
    				   a.event_goals, 
    				   a.event_venue, 
    				   a.event_date, 
    				   a.event_status, 
    				   a.event_phone, 
    				   a.event_email, 
    				   a.event_fax, 
    				   a.event_website, 
    				   a.event_address
                from event_name a
                where a.event_status = '1'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	function showTrainerInfo(){
        $slc = "select a.trainer_id,
						a.trainer_name,
						a.trainer_status,
						a.event_id,
						a.trainer_tweeter,
						a.trainer_email,
						a.trainer_hp,
						a.trainer_fb,
						a.trainer_path,
						a.trainer_image
                from event_trainer a
                where a.trainer_status = '1'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	
	function showEventPriceCat(){
		/*
		$this->db = $this->load->database('alternate2', true);
		$sqlInsert ="insert into dokter(nama_dokter,id_spesialis,ax_antrian)
					 values('aaaaa','111','40')";
		$sql="select * from dokter";
		$query = $this->db->query($sql);
		return $query;
		 * 
		 */
		 
		 $qry = "SELECT a.event_id, a.event_desc, a.event_address, a.event_date, a.event_venue,
					   b.price_tab_id, b.price_tab_dp
				FROM event_name a INNER JOIN event_price_tab b ON a.event_id=b.event_id
				WHERE a.event_status='1' and b.price_tab_status='1'
				ORDER BY a.event_id";
		$result = $this->getRecordset($qry,null,$this->db1);
        return $result;
	}
    
    function getEventPlace($id){
       $slc = "select a.event_desc,a.event_address,a.event_date
                 from event_name a
                where a.event_id = '".$id."'";
       $result = $this->getRecordset($slc,null,$this->db1);
       return $result;
    }
    
    function getHTM($id){
        $slc = "select a.price_tab_dp,a.price_tab_id
                from event_price_tab a
                where a.event_id = '".$id."'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    public function seqTiket(){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
    
        $y1=date("y");
        $m=date("m");
        
        $tbl = "event_seq_tkt".$y1."".$m."";
        
        $cek = "select * from $tbl";
        
        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            
        }      
    }
    
    public function getTiketnum(){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
 
        $y1=date("y");
        $m=date("m");
        
        $year = substr($y1,2,2);
        
        $tbl = "event_seq_tkt".$y1."".$m."";
        
                   
        $sql = "SELECT * FROM $tbl 
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";
        
       $query = $this->db->query($sql);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }  
        }
        //$jumlah = $query->num_rows();
        
        $next_seq = sprintf("%04s",$ss);
        //$registerNo  = "TE"."$next_seq";
        $registerNo  = "T".$year."".$m."".$next_seq."";
		
        return $registerNo;
    }
    
    public function getTicketNo(){
        $seq = $this->seqTiket();
        $tiket = $this->getTiketnum();
        
        return $tiket;
    }
    
    public function cekSeq(){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
 
        $y1=date("Y");
        $m=date("m");
        
        $tbl = "event_seq".$y1."".$m."";
        
        $cek = "select * from $tbl";
        
        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "<br>ada<br>";
        }      
    }
    
    public function createBookingNo(){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
 
        $y1=date("Y");
        $m=date("m");
        
        $year = substr($y1,2,2);
        
        $tbl = "event_seq".$y1."".$m."";
        
                   
        $sql = "SELECT * FROM $tbl 
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";
        
       $query = $this->db->query($sql);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }  
        }
        //$jumlah = $query->num_rows();
        
        $next_seq = sprintf("%05s",$ss);
        //$registerNo  = "TE"."$next_seq";
        $registerNo  = "".$year."".$m."".$next_seq."";
		
        return $registerNo;
    }
    
    public function getBookingNo(){
        $sequence = $this->cekSeq();
        $bookingNo = $this->createBookingNo();
        
        return $bookingNo;
    }
    
    function setPreTrf($ticketing){
        $arr = $this->input->post(NULL, TRUE);
        $ip = $_SERVER['REMOTE_ADDR'];
        //$tiket = $this->getTicketNo();
        $jum = count($arr['name']);
        $tot_dp = $jum * $htm;
        $insHdr = "insert into event_trans_hdr_prepaid (trans_hdr_name,trans_hdr_hp,trans_hdr_email,
                    trans_qty_ticket,trans_hdr_dp,trans_hdr_ip,bookingNo)
                    values('".$arr['nameHdr']."','".$arr['phoneHdr']."','".$arr['emailHdr']."',".$jum.",
                    ".$tot_dp.",'".$ip."','".$randomid."')";
        $qryHdr = $this->executeQuery($insHdr, $this->setDB(1));
        
        $slcHdr = "select trans_hdr_id from event_trans_hdr_prepaid
                    where bookingNo = '".$randomid."'";
            $queryHdr = $this->db->query($slcHdr);
            if($queryHdr->num_rows() > 0){
                foreach($queryHdr->result() as $data)
                {   
                    $idHdr = $data->trans_hdr_id;
                }
            }
        
        for($i=0;$i < $jum;$i++){
            $y = $i+1;
			
			$tiket = $this->getTicketNo();
			
            $insDet = "insert into event_trans_det_prepaid (trans_hdr_id,trans_det_seq,
                        event_id,price_tab_id,trans_det_dp,trans_det_name,
                        trans_det_hp,trans_hdr_email,trans_tiket_no)
                        values(".$idHdr.",".$y.",".$eventname.",".$pricetabid.",".$htm.",'".$arr['name'][$i]."',
                        '".$arr['phone'][$i]."','".$arr['emailHdr']."','$tiket')";
            $qryDet = $this->executeQuery($insDet, $this->setDB(1)); 
        }
        
        if($qryHdr > 0 && $qryDet > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    function setPrePayment($htm,$randomid,$pricetabid,$arr,$totDP){
       
        $ip = $_SERVER['REMOTE_ADDR'];
        
        $jum = count($arr['name']);
        //$tot_dp = $jum * $htm;
        $insHdr = "insert into event_trans_hdr_prepaid (trans_hdr_name,trans_hdr_hp,trans_hdr_email,
                    trans_qty_ticket,trans_hdr_dp,trans_hdr_ip,bookingNo,pay_type)
                    values('".strtoupper($arr['nameHdr'])."','".$arr['phoneHdr']."','".$arr['emailHdr']."',".$jum.",
                    ".$totDP.",'".$ip."','".$randomid."','".$arr['tipePay']."')";
        $qryHdr = $this->executeQuery($insHdr, $this->setDB(1));
        
        $slcHdr = "select trans_hdr_id from event_trans_hdr_prepaid
                    where bookingNo = '".$randomid."'";
            $queryHdr = $this->db->query($slcHdr);
            if($queryHdr->num_rows() > 0){
                foreach($queryHdr->result() as $data)
                {   
                    $idHdr = $data->trans_hdr_id;
                }
            }
        
        for($i=0;$i < $jum;$i++){
            $y = $i+1;
			
			$tiket = $this->getTicketNo();
			
            $insDet = "insert into event_trans_det_prepaid (trans_hdr_id,trans_det_seq,
                        event_id,price_tab_id,trans_det_dp,trans_det_name,
                        trans_det_hp,trans_hdr_email,trans_tiket_no)
                        values(".$idHdr.",".$y.",".$arr['ticket'].",".$pricetabid.",".$htm.",'".strtoupper($arr['name'][$i])."',
                        '".$arr['phone'][$i]."','".$arr['emailHdr']."','".$tiket."')";
            $qryDet = $this->executeQuery($insDet, $this->setDB(1)); 
        }
        
        if($qryHdr > 0 && $qryDet > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    function getAmtTiket($randomid){
        $slc = "select a.trans_hdr_dp as dp from event_trans_hdr_prepaid a
                where a.bookingNo = '".$randomid."' ";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function randomInt($length){
        
       $karakter= '0123456';
       $string = ''; 
       for ($i = 0; $i < $length; $i++) 
       { 
            $pos = rand(0, strlen($karakter)-1); 
            $string .= $karakter{$pos}; 
       } 
       return $string; 
    }
    
    
    function updTabel($randomid,$amount){
        
        //$secno = $this->randomChars(5);
       $updtHdrPre = "update event_trans_hdr_prepaid set trans_hdr_status = '1',
                       flag_paid = '1'
                       where bookingNo = '".$randomid."'";
        //echo "update ".$updtMutasi;
        $qryUpdtHdrPre = $this->executeQuery($updtHdrPre, $this->setDB(1));
        
        $insHdr = "INSERT INTO event_trans_hdr(
                        trans_hdr_id,
                        trans_hdr_name,
                        trans_hdr_hp,
                        trans_hdr_email,
                        trans_qty_ticket,
                        trans_hdr_dp,
                        trans_hdr_ip,
                        flag_paid,
                        bookingNo,
                        pay_type)
                    SELECT trans_hdr_id,
                        trans_hdr_name,
                        trans_hdr_hp,
                        trans_hdr_email,
                        trans_qty_ticket,
                        trans_hdr_dp,
                        trans_hdr_ip,
                        flag_paid,
                        bookingNo,
                        pay_type
                    FROM event_trans_hdr_prepaid A
                    WHERE A.bookingNo ='".$randomid."'";
        $qryHdr = $this->executeQuery($insHdr, $this->setDB(1));
        
        
        $updtHdr = "update event_trans_hdr set trans_hdr_status = '1',
                       flag_paid = '1'
                       where bookingNo = '".$randomid."'";
        //echo "update ".$updtMutasi;
        $qryUpdtHdr = $this->executeQuery($updtHdr, $this->setDB(1));
        
        $slcHdr = "select trans_hdr_id from event_trans_hdr_prepaid
                    where bookingNo = '".$randomid."'";
            $queryHdr = $this->db->query($slcHdr);
            if($queryHdr->num_rows() > 0){
                foreach($queryHdr->result() as $data)
                {   
                    $idHdr = $data->trans_hdr_id;
                }
            }
        
        $insDet = "insert into event_trans_det (
                        trans_hdr_id,
                        trans_det_seq,
                        event_id,
                        price_tab_id,
                        trans_det_dp,
                        trans_det_name,
                        trans_det_hp,
                        trans_hdr_email,
						trans_tiket_no)
                    SELECT trans_hdr_id,
                        trans_det_seq,
                        event_id,
                        price_tab_id,
                        trans_det_dp,
                        trans_det_name,
                        trans_det_hp,
                        trans_hdr_email,
                        trans_tiket_no
                    FROM event_trans_det_prepaid B
                    where B.trans_hdr_id = '".$idHdr."'";
        $qryDet = $this->executeQuery($insDet, $this->setDB(1));
        
        /*$insSecno = "insert into event_trans_secno (trans_hdr_id,secno)
                    values('".$idHdr."','".$secno."')";
        $qrySecno = $this->executeQuery($insSecno, $this->setDB(1));*/
        
    }
    
    function getDtRegister($bookingNo){
        $slc = "select distinct a.bookingNo,a.trans_hdr_hp,a.trans_hdr_name,a.trans_qty_ticket,
                a.trans_hdr_email,c.event_desc,c.event_address,c.event_date as event_date, 
                CONVERT(VARCHAR(24),c.event_date,113) as event_date1,a.trans_hdr_dp,a.pay_type,a.flag_paid
                 from event_trans_hdr a
                inner join event_trans_det b on(a.trans_hdr_id = b.trans_hdr_id)
                inner join event_name c on (c.event_id = b.event_id)
                where a.flag_paid = '1' and a.bookingNo = '".$bookingNo."' and a.pay_type = '1'";
        //echo $slc;
        //echo "<br>";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getDtRegisterPre($bookingNo){
        $slc = "select distinct a.bookingNo,a.trans_hdr_hp,a.trans_hdr_name,a.trans_qty_ticket,
                a.trans_hdr_email,c.event_desc,c.event_address,c.event_date as event_date, 
                CONVERT(VARCHAR(24),c.event_date,113) as event_date1,a.trans_hdr_dp,a.pay_type,a.flag_paid
                 from event_trans_hdr_prepaid a
                inner join event_trans_det_prepaid b on(a.trans_hdr_id = b.trans_hdr_id)
                inner join event_name c on (c.event_id = b.event_id)
                where a.flag_paid = '0' and a.bookingNo = '".$bookingNo."' and a.pay_type = '0'";
        //echo $slc;
       // echo "<br>";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
    
    function getDtDetail($randomid){
        
        $slcHdr = "select trans_hdr_id from event_trans_hdr
                    where bookingNo = '".$randomid."'";
        $queryHdr = $this->db->query($slcHdr);
        //echo $slcHdr;
        if($queryHdr->num_rows() > 0)
        {
            foreach($queryHdr->result() as $data)
            {   
                $idHdr = $data->trans_hdr_id;
            }
        }
        
        $slcDet = "select * from event_trans_det where trans_hdr_id = ".$idHdr."";
        //echo $slcDet;
        $result = $this->getRecordset($slcDet,null,$this->db1);
        return $result;
    }
    
    function getDtDetailPre($randomid){
        
        $slcHdr = "select trans_hdr_id from event_trans_hdr_prepaid
                    where bookingNo = '".$randomid."'";
            $queryHdr = $this->db->query($slcHdr);
            if($queryHdr->num_rows() > 0){
                foreach($queryHdr->result() as $data)
                {   
                    $idHdr = $data->trans_hdr_id;
                }
            }
        
        $slcDet = "select * from event_trans_det_prepaid where trans_hdr_id = ".$idHdr."";
        //echo $slcDet;
        $result = $this->getRecordset($slcDet,null,$this->db1);
        return $result;
    }
    
    function getBank(){
        $slc = "select * from ecomm_bank order by bankDesc";
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
  
    function getloopp(){
        
        
        for($i=0;$i < 4;$i++){
            $tiket = $this->getTicketNo();
            echo "$tiket";
            echo "<br>";
            
        }
    }
 }
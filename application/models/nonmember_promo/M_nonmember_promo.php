<?php
class M_nonmember_promo extends MY_Model {


    function getListFree(){
        $result= array();
        $this->db->select('a.prdcd, b.prdnm, b.bv, b.price_w as westPrice,b.price_e as eastPrice,b.weight')
            ->from('nonmember_free_product_promo a')
            ->join('V_Ecomm_PriceList_Dion b','a.prdcd=b.prdcd','left')
            ->where('a.status',1);
        $q = $this->db->get();
        foreach($q->result() as $row)
        {
            $result[] = $row;
        }
        return $result;
    }
	
	function getListFreePromoNov2018() {
		$qry = "SELECT a.prdcd, b.prdnm, b.bv, b.price_w as westPrice,b.price_e as eastPrice,b.weight
		        FROM nonmember_free_product_promo a
				LEFT OUTER JOIN V_Ecomm_PriceList_Dion b on a.prdcd = b.prdcd
				where a.status = '1'";//  AND a.prdcd != 'HD009F'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
	
	function checkDoubleKtpAndCellPhone($no_hp, $idno) {
		$qry = "select dfno, fullnm
				FROM klink_mlm2010.dbo.msmemb a
				where a.idno = '$idno' or a.tel_hp = '$no_hp'";
				//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
        if($res != null) {
        	//throw new Exception("No KTP / No HP sudah terdaftar..", 1);
			return false;
        } else {
        	return true;
        }
	}

    
    function getMinTrxPromo(){
       // $res=$this->db->query('select min_trx_amount  from nonmember_min_trx where status=1');
       // return $res;
        $qry="select min_trx_amount from nonmember_min_trx where status=1";
        $res = $this->getRecordset($qry, null, $this->db1);
        return $res;

    }

    function getState($stockist){
        $qry="select state from mssc where loccd='$stockist'";
        $res = $this->getRecordset($qry, null, $this->db2);
        return $res;
    }

    function getSponsorName($sponsorid){

        $dfno = strtoupper($sponsorid);
        $qry="select fullnm, dfno from msmemb where dfno='$dfno'";
        $res = $this->getRecordset($qry, null, $this->db2);
        return $res;
    }

    function insertEcommerceTrxSGO($trans_id) {
        $this->db = $this->load->database('db_ecommerce', true);
        $datetrans = date("Y-m-d H:i:s");

        $personal_info = $this->session->userdata('personal_info');
        $pricecode = $this->session->userdata('pricecode');
        $shipping_jne_info = $this->session->userdata('shipping_jne_info');
        $sender_address = $this->session->userdata('sender_address');
        $dest_address = $this->session->userdata('destination_address');
        $destination_address = str_replace("�??", "", $dest_address);
        $jne_branch = $this->session->userdata('jne_branch');
        $promo = $this->session->userdata('promo');
        $bns = substr($personal_info['bnsperiod'], 3, 7);
        $totPay = $this->input->post('subtotalx');
        $bankid = $this->input->post('bankid');
        $charge_admin = $this->input->post('charge_admin');

        $totalbv = $this->cart->total_bv();
        $freeship = "0";

        //if(getUserID() == "IDSPAAA66834") {
        //print_r($shipping_jne_info);
        /*$biaya = getTotalPayNetBaru();
        $gross_amount = $biaya['total_pay'] + $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
        $freeship = $biaya['freeship'];

        $discount = $shipping_jne_info['ship_discount']; */
        //} else {
        /*$gross_amount = getTotalPayNetAndShipCost();
        if($shipping_jne_info == null) {
            $freeship = "1";
        } else {
            $freeship = "0";
        }*/
//        $biaya = getTotalPayNetAndShipCost2();
//        $gross_amount = $biaya['total_pay'];
//        $freeship = $biaya['freeship'];
//        $discount = $biaya['tot_discount'];

        //}

        $shipper_telhp = getUserPhone();
        $admin_cost = 0;
//        $nmstk = explode(" - ", $personal_info['nama_stockist']);
//        $nmstkSS = $nmstk[0]." - ".$nmstk[1];
        $produk = $this->cart->contents();

        $cust_memb = $this->session->userdata("non_member_info");
        if(isset($cust_memb)) {
            $is_login = "2";
            $tot_paynet = getTotalPayNet();
        } else {
            $memb = $this->session->userdata("store_info");
            if(isset($memb)) {
                $is_login = "1";
                $tot_paynet = 0;
            }
        }

        $insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
						values('".$trans_id."','1','sgo','','".$totPay."','pending','pending','$bankid', '$charge_admin')";
//        echo "Paydet ".$insPaydet."<br>";
        $exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));

        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_login, totPayCP,id_lp, free_shipping, discount_shipping,bank_code_payment)
			          values('".$trans_id."','','".$trans_id."','".trim($personal_info['idmemberx'])."','".trim($personal_info['membername'])."',
			                    $totPay,0,'".$pricecode."','".$bns."','".$datetrans."',
			                    'BID06','PUSAT','0','W','1','0','',0,0,'9',$tot_paynet,'', '1', '0','$bankid')";
        //echo "header gagal ".$insHeader."<br>";
        $exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));
        $insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code, kab_code, kec_code,
			                     addr1, email, tel_hp1, total_item, total_weight, service_type_id, service_type_name,
			                     receiver_name, stockist_name, kabupaten_name, province_name,
								 sender_address, dest_address, jne_branch, shipper_telhp, total_pay_net, cargo_id)
			                VALUES ('".$trans_id."', 'BID06', '0', '0',
			                        '0', '".$personal_info['alamat']."',
			                        '".$personal_info['email']."', '".$personal_info['notlp']."',
			                        0, 0, '0', '0',
			                        '".$personal_info['nama_penerima']."','0','0','0',
			                        '0', '0','0', '0', ".$totPay.", '0')";

        $exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));
        foreach($produk as $row)
        {
            //echo "sebelum eksekusi db produk<br>";
            $prdcd = $row['id'];
            $qty = (int)$row['qty'];
            $bv = (int)$row['bv'];
            $prdnm = $row['name'];
            if($pricecode == '12W3'){
                $price = (int)$row['west_price'];
            }else{
                $price = (int)$row['east_price'];
            }

            $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
              		        values('".$trans_id."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$pricecode."','')";

            //echo "produk ".$inputProd."";
            $exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));
        }
        $exeShipAddr = true;

        if($exeInsPaydet && $exeInsHeader && $exeInputProdr)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }


}




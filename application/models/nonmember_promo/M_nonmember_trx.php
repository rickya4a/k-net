<?php
class M_nonmember_trx extends MY_Model {

function insert_temp_trx_sgo($trans_id, $dataForm) {
		$this->db = $this->load->database('db_ecommerce', true);	
		$datetrans = date("Y-m-d H:i:s");
		
		$new_member_reg = $this->session->userdata("new_member_reg");
		$promo_free_prd = $this->session->userdata("promo_free_prd");
		$personal_info = $this->session->userdata('personal_info');
		$pricecode = $this->session->userdata('pricecode');
		$shipping_jne_info = $this->session->userdata('shipping_jne_info');
		$sender_address = $this->session->userdata('sender_address');
		$dest_address = $this->session->userdata('destination_address');
		$destination_address = str_replace("â??", "", $dest_address);
		$jne_branch = $this->session->userdata('jne_branch');
		$promo = $this->session->userdata('promo');
		$bns = substr($personal_info['bnsperiod'], 3, 7);
		$totPay = getTotalPayNet();
		$totalbv = $this->cart->total_bv();
		$freeship = "0";	
		
		$no_hp_konfirmasi = $this->session->userdata('no_hp_konfirmasi');
		
		//if(getUserID() == "IDSPAAA66834") {
			//print_r($shipping_jne_info);
			/*$biaya = getTotalPayNetBaru();
		    $gross_amount = $biaya['total_pay'] + $shipping_jne_info['price'] - $shipping_jne_info['ship_discount'];
		    $freeship = $biaya['freeship'];
		
			$discount = $shipping_jne_info['ship_discount']; */
		//} else {
			/*$gross_amount = getTotalPayNetAndShipCost();
			if($shipping_jne_info == null) {
				$freeship = "1";
			} else {
				$freeship = "0";
			}*/
			$biaya = getTotalPayNetAndShipCost2();
		    $gross_amount = $biaya['total_pay'];
		    $freeship = $biaya['freeship'];
			$discount = $biaya['tot_discount'];
			
		//}	
		
		$shipper_telhp = getUserPhone();
		$admin_cost = 0;
		$nmstk = explode(" - ", $personal_info['nama_stockist']);
		$nmstkSS = $nmstk[0]." - ".$nmstk[1];
		$produk = $this->cart->contents();
		
		$cust_memb = $this->session->userdata("non_member_info");
		if(isset($cust_memb)) {
			$is_login = "2";
			$tot_paynet = getTotalPayNet();		
		} else {			
			 $memb = $this->session->userdata("store_info");
			 if(isset($memb)) {
			 	$is_login = "1";
				$tot_paynet = 0;				
			 }
		}
		
		if($new_member_reg['sponsorid'] == "IS_MEMBER") {
			$point_idmember = $new_member_reg['noktp'];
			$point_nmmember = $new_member_reg['membername'];
		} else {
			$point_idmember = $personal_info['idmemberx'];
			$point_nmmember = str_replace("'", "`", $personal_info['membername']);
		}
			
		/*$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet_sgo (orderno,seqno,paytype,docno,payamt,notes,paystatus) 
						values('".$trans_id."','1','sgo','',".$gross_amount.",'pending','pending')";
		*/				
	    $insPaydet = "INSERT INTO ecomm_trans_paydet_sgo (orderno, seqno, paytype, docno, payamt, paystatus, bank_code_payment, charge_admin) VALUES 
		        ('$trans_id', '1', 'sgo', 
		        '', ".$gross_amount.", 'pending', ".$dataForm['bankid'].", ".$dataForm['charge_admin'].")";
        //echo "Paydet ".$insPaydet."<br>"; 
		$exeInsPaydet = $this->executeQuery($insPaydet, $this->setDB(1));
        
        $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr_sgo 
                            (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,
                             pricecode,bonusmonth,datetrans,idstk,nmstkk,secno,flag_trx,sentTo,
                             status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_login, totPayCP,id_lp, free_shipping, discount_shipping,
							 bank_code_payment, no_hp_konfirmasi) 
			          values('".$trans_id."','','".$trans_id."','".trim($point_idmember)."','".trim($point_nmmember)."',
			                    $totPay,$totalbv,'".$pricecode."','".$bns."','".$datetrans."',
			                    '".trim($personal_info['stockist'])."','".trim($nmstkSS)."','0','W','".$personal_info['delivery']."','0','',".$shipping_jne_info['price'].",".$dataForm['charge_admin'].",'$is_login',$tot_paynet,'".$personal_info['id_lp']."', '$freeship', $discount,
			                    ".$dataForm['bankid'].", '$no_hp_konfirmasi')";
         //echo "header : ".$insHeader."<br>";
        $exeInsHeader = $this->executeQuery($insHeader, $this->setDB(1));
        	
        foreach($produk as $row)
            {
                //echo "sebelum eksekusi db produk<br>";
                $prdcd = $row['id'];
                $qty = (int)$row['qty'];
                $bv = (int)$row['bv'];
                $prdnm = $row['name'];
                if($pricecode == '12W3'){
                    $price = (int)$row['west_price'];
                }else{
                    $price = (int)$row['east_price'];
                }
                
                $inputProd = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$trans_id."','".$prdcd."','".$prdnm."',".$qty.",".$bv.",".$price.",'".$pricecode."','')";
           	    
               //echo $inputProd."</br>";
               $exeInputProdr = $this->executeQuery($inputProd, $this->setDB(1));
            }
			
		/*----------------------
		 * INPUT PRODUK PROMO FREE
		 * --------------------*/
		$prdcdFree = $promo_free_prd['prdcd'];
		$prdnmFree = $promo_free_prd['prdnm'];
		$qtyFree = $promo_free_prd['qty'];
		$bvFree = $promo_free_prd['bv'];
		if($pricecode == '12W3'){
			$priceFree = $promo_free_prd['westPrice'];
		} else {
			$priceFree = $promo_free_prd['eastPrice'];
		}	
		
		
		$inputProd2 = "insert into db_ecommerce.dbo.ecomm_trans_det_prd_sgo (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo) 
              		        values('".$trans_id."','".$prdcdFree."','".$prdnmFree."',".$qtyFree.",".$bvFree.",".$priceFree.",'".$pricecode."','')";
           	    
        //echo $inputProd2."</br>";
        $exeInputProdr2 = $this->executeQuery($inputProd2, $this->setDB(1));
       /*---------
	    * END
	    * -------*/
			
        $exeShipAddr = true;    
        if($personal_info['delivery'] == "2") {
        	$tot_item_prd = $this->cart->total_items();
			$tot_weight_prd = $this->cart->total_weight();  
			$ssx = explode(" - ", $personal_info['nama_stockist']);
			$nama_stk2 = str_replace("-", "", $ssx[1]);
			$nama_stk = substr($nama_stk2, 0, 20);	 
            
			if(array_key_exists("shipper", $personal_info)) {
				$shipper_id = $personal_info['shipper'];	
			} else {
				$shipper_id = "1";
			}
			
			
			$al1 = str_replace("â??", "", $personal_info['alamat']);
            $alamat = trim(strtoupper($al1));
            $receiver = trim(strtoupper($personal_info['nama_penerima']));
			$insShipAddr = "INSERT INTO ecomm_trans_shipaddr_sgo (orderno, idstk, prov_code, kab_code, kec_code,
			                     addr1, email, tel_hp1, total_item, total_weight, service_type_id, service_type_name,
			                     receiver_name, stockist_name, kabupaten_name, province_name,
								 sender_address, dest_address, jne_branch, shipper_telhp, total_pay_net, cargo_id)
			                VALUES ('".$trans_id."', '".$personal_info['stockist']."', '".$personal_info['provinsi']."', '".$personal_info['kota']."',
			                        '".$personal_info['kecamatan']."', '".$alamat."', 
			                        '".$personal_info['email']."', '".$personal_info['notlp']."',
			                        $tot_item_prd, $tot_weight_prd, '".$shipping_jne_info['service_code']."', '".$shipping_jne_info['service_display']."',
			                        '".$receiver."','$nama_stk','".trim(strtoupper($personal_info['nama_kota']))."','".trim(strtoupper($personal_info['nama_provinsi']))."',
			                        '".$sender_address."', '".$destination_address."','".$jne_branch."', '$shipper_telhp', ".$totPay.", '".$shipper_id."')";
			//echo $insShipAddr."</br>";		
			$exeShipAddr = $this->executeQuery($insShipAddr, $this->setDB(1));
		}   
							
		if($exeInsPaydet && $exeInsHeader && $exeInputProdr)
        {
            return 1;
        }
        else
        {
            return 0;
        }	
	}

	function insertEcommMembSgo($arr) {
		$prdcd = $arr['prdcd'];
		$sponsorid = $arr['sponsorid'];
		$pay_memb_sgo_id = $arr['trx_id'];
		$membername = $arr['membername'];
		$noktp = $arr['noktp'];
		$tel_hp = $arr['no_hp'];
		$tgllhr = $arr['tgllhr'];
		$sponsorid = $arr['sponsorid'];
		$email = $arr['email'];
		$sex = $arr['sex'];
		$idstk = $arr['idstk'];
		$no_rek =  $arr['no_rek'];
		$pilBank = $arr['bank'];
		$bankaccnm1 = $arr['bank_acc_name'];
		$id_lp = $arr['id_lp'];
		$pay_tipe = $arr['flag_voucher'];
		$voucherno = $arr['voucherno'];
		$voucherkey = $arr['voucherkey'];
		$kdpos1 =  "000004";
		$st_id = $arr['state'];
		$recruiterid = $arr['rekruiterid'];
		$addr = $arr['address'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$birth = explode("-", $tgllhr);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;
		
		$insMemb = "insert into db_ecommerce.dbo.ecomm_memb_ok_sgo (sponsorid,memberid,membername,idno,
                    addr1,addr2,addr3,tel_hp,email,stk_code,sex,birthdt,
                    acc_no,acc_name,bankid,joindt,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                    flag_voucher, voucher_no, voucher_key, id_landingpage)
                    values ('".$sponsorid."','".$pay_memb_sgo_id."','".$membername."','".$noktp."',
                            '".$addr."', '','','".$tel_hp."','".$email."','".$idstk."','".$sex."','".$tgllhr."',
                            '$no_rek','".$bankaccnm1."','$pilBank',
                            '".$this->dateTime."','".$pay_memb_sgo_id."','".$kdpos1."','".$st_id."','".$ip."','".$prdcd."','".$password."','".getUserID()."','".$recruiterid."',
                            '".$pay_tipe."','".$voucherno."','".$voucherkey."', '$id_lp')";
        //echo "<br />".$insMemb;
		$res = $this->executeQuery($insMemb, $this->setDB(1));
		return $res;
	}
	
	function saveMemberSgo($orderid) {
		
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

    var $query = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        // $this->klink_app = $this->load->database('klink_apps', TRUE);
        //$this->orchid_hrm = $this->load->database('orchid_hrm', TRUE);

    }



    // API Mutasi
    public function cek_mutasi(){
        $this->db->select('*');
        $this->db->from('mutasi_transaction');
        $id_account = $this->input->get('id_account') != '' ? $this->input->get('id_account') : '290';
        $date_from = $this->input->get('date_from') != '' ? $this->input->get('date_from') : date('Y-m-d', strtotime('-30 days'));
        $date_to = $this->input->get('date_to') != '' ? $this->input->get('date_to') : date('Y-m-d');
        $this->db->where(array('id_account'=>$id_account, 'transaction_date  >='=>$date_from, 'transaction_date <='=>$date_to));
        $this->db->order_by('rec_id','ASC');
        $query = $this->db->get();
        return $query->result();
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        // CURLOPT_URL => "https://mutasibank.co.id/api/v1/statements/290",
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => "",
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 0,
        // CURLOPT_FOLLOWLOCATION => false,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => "POST",
        // CURLOPT_POSTFIELDS => array('date_from' => '2019-01-01','date_to' => '2019-02-14'),
        // CURLOPT_HTTPHEADER => array(
        //     "Authorization: bWdYdnFZb0p6d2RHV0pybXBIcVJzUTJCWjk5dHJobzdrQjlGNXAxeENVek02bzRDUWVQdllpTE1iR0Qw5c624d281a00b"
        //   ),
        // ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // if ($err) {
        //    return $err;
        // } else {
        //   return $response;
        // }
    }

    // API Mutasi
    public function cek_mutasi_set($id_account, $date_from, $date_to){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://mutasibank.co.id/api/v1/statements/".$id_account,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('date_from' => $date_from,'date_to' => $date_to),
            CURLOPT_HTTPHEADER => array(
                "Authorization: bWdYdnFZb0p6d2RHV0pybXBIcVJzUTJCWjk5dHJobzdrQjlGNXAxeENVek02bzRDUWVQdllpTE1iR0Qw5c624d281a00b"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }

    public function cek_account(){
        $this->db->select('*');
        $this->db->from('mutasi_account');
        $this->db->order_by('rec_id','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function save_payment($kd_pay, $kd_ref, $nominal, $no_rek_tujuan, $nama_rek_tujuan, $type, $total_bayar, $kode_unik){
        //$kd_pay, $kd_ref, $nominal, $no_rek_tujuan, $nama_rek_tujuan, $type, $total_bayar, $kode_unik
        $data = array(
            'rec_id' => '',
            'kd_pay'=> $kd_pay,
            'kd_ref'=> $kd_ref,
            'nominal' => $nominal,
            'no_rek_tujuan' => $no_rek_tujuan,
            'nama_rek_tujuan' => $nama_rek_tujuan,
            'type' => $type,
            'total_bayar' => $total_bayar,
            'kode_unik' => $kode_unik,
            'datetime' => date("Y-m-d H:i:s")
        );

        return $this->db->insert('mutasi_payment', $data);
    }

    public function receive_no_order($kd_pay, $no_order){
        $data = array(
            'rec_id' => '' ,
            'kd_pay' => $kd_pay ,
            'no_order' => $no_order
        );
        $query=$this->db->insert('mutasi_no_order', $data);
        return $query->result();
    }

    public function kd_pay(){
        $this->db->select('kd_pay');
        $this->db->from('mutasi_payment');
        $this->db->order_by('rec_id','DESC');
        $query = $this->db->get();
        $row = $query->row();
        $last_kd_pay = isset($row->kd_pay) ? $row->kd_pay : '0';
        $new_kd_pay = $last_kd_pay + 1;
        return $new_kd_pay;
    }

    public function kd_ref(){
        $this->db->select('kd_pay');
        $this->db->from('mutasi_payment');
        $this->db->order_by('rec_id','DESC');
        $query = $this->db->get();
        $row = $query->row();
        $last_kd_pay = isset($row->kd_pay) ? $row->kd_pay : '0';
        $new_kd_pay = $last_kd_pay + 1;
        return sprintf("%06s",$new_kd_pay);
    }





    // // insert post data
    // public function country() {

    // 	$keyword = $this->input->post('query');

    // 	$this->db->select('*');
    // 	$this->db->from('tbl_country');
    // 	$this->db->like('country_name', $keyword, 'after');
    // 	$this->db->order_by('country_name','ASC');
    // 	$q = $this->db->get();
    // 	//return $query->row();

    // 	if ($q->num_rows() > 0) {
    // 		//while($row = $q->result()) {
    // 		//$countryResult[] = $row;
    // 		//}
    // 		foreach($q->result() as $row){
    // 			$countryResult[] = $row->country_name;
    // 		}
    // 		return json_encode($countryResult);
    // 	}else{
    // 		return json_encode(array());
    // 	}


    // }

    // public function data_santri() {
    // 	$this->fiwa_app->select('*');
    // 	$this->fiwa_app->from('cubi_studentbiodata');
    // 	$this->fiwa_app->order_by('fullname','ASC');
    // 	$q = $this->fiwa_app->get();
    // //return $query->row();

    // 	if ($q->num_rows() > 0) {
    // 		foreach($q->result() as $row){
    // 			$stdResult[] = $row->fullname;
    // 		}
    // 		return json_encode($stdResult);
    // 	}else{
    // 		return json_encode(array());
    // 	}
    // }

    // // insert post data
    // public function data_karyawan() {
    // 	$this->fiwa_app->select('*');
    // 	$this->fiwa_app->from('cubi_teacher');
    // 	$this->fiwa_app->order_by('name','ASC');
    // 	$q = $this->fiwa_app->get();
    // 	//return $query->row();

    // 	if ($q->num_rows() > 0) {
    // 		foreach($q->result() as $row){
    // 			$stdResult[] = $row->name;
    // 		}
    // 		return json_encode($stdResult);
    // 	}else{
    // 		return json_encode(array());
    // 	}
    // }

    // // insert post data
    // public function data_karyawan_query() {
    // 	/*
    // 	$this->fiwa_app->select('*');
    // 	$this->fiwa_app->from('cubi_teacher');
    // 	$this->fiwa_app->order_by('name','ASC');
    // 	$q = $this->fiwa_app->get();
    // 	*/
    // 	//return $query->row();
    // 	$q = $this->orchid_hrm->query("SELECT a.nik, firstname, positionname
    // 			FROM emp_biodata AS a
    // 			INNER JOIN emp_adminattribute AS b ON a.nik = b.nik
    // 			INNER JOIN emp_generalattribute AS c ON b.nik=c.nik
    // 			INNER JOIN cfg_position AS d ON b.positioncode=d.pos_id
    // 			WHERE resigndate IS NULL");

    // 	if ($q->num_rows() > 0) {
    // 		foreach($q->result() as $row){
    // 			$stdResult[] = $row->firstname;
    // 		}
    // 		return json_encode($stdResult);
    // 	}else{
    // 		return json_encode(array());
    // 	}
    // }

    // public function get_nomor_induk(){

    // 	if($this->input->post('group') == 'karyawan'){
    // 		$this->fiwa_app->select('*');
    // 		$this->fiwa_app->from('cubi_teacher');
    // 		$this->fiwa_app->order_by('name','ASC');
    // 		$this->fiwa_app->where('name',$this->input->post('name'));
    // 		$q = $this->fiwa_app->get();
    // 		$r = $q->row();
    // 		 return $r->teacherid;// $r->teacherid;
    // 	}
    // 	elseif($this->input->post('group') == 'santri'){
    // 		$this->fiwa_app->select('*');
    // 		$this->fiwa_app->from('cubi_studentbiodata');
    // 		$this->fiwa_app->order_by('fullname','ASC');
    // 		$this->fiwa_app->where('fullname',$this->input->post('name'));
    // 		$q = $this->fiwa_app->get();
    // 		$r = $q->row();
    // 		return $r->studentid;
    // 	}
    // }


    // public function get_keterangan(){

    // 	if($this->input->post('group') == 'karyawan'){
    // 		$this->fiwa_app->select('*');
    // 		$this->fiwa_app->from('cubi_teacher');
    // 		$this->fiwa_app->order_by('name','ASC');
    // 		$this->fiwa_app->where('name',$this->input->post('name'));
    // 		$q = $this->fiwa_app->get();
    // 		$r = $q->row();
    // 		 return $r->grouop;// $r->teacherid;
    // 	}
    // 	elseif($this->input->post('group') == 'santri'){
    // 		$this->fiwa_app->select('*');
    // 		$this->fiwa_app->from('cubi_student');
    // 		$this->fiwa_app->order_by('fullname','ASC');

    // 		$this->fiwa_app->like('student_firstname', $v);
    // 		foreach(explode($this->input->post('name')) as $k => $v){
    // 			$this->fiwa_app->or_like('student_firstname', $v);
    // 		}

    // 		$q = $this->fiwa_app->get();
    // 		$r = $q->row();
    // 		return $r->;
    // 	}
    // }

    // public function update_status_fix(){
    // 	$array_set = array('troubleshoot'=>$this->input->post('troubleshoot'),
    // 					   'soft_date'=>$this->input->post('softdate'),
    // 					   'retrieval_date'=>$this->input->post('retrievaldate'),
    // 					   'status'=>'1');
    // 	$this->db->set($array_set);
    // 			$this->db->where('id_log_book',$this->input->post('id_log_book'));

    // 		if($this->db->update('log_book')){
    // 			return 'sukses';
    // 		}else{
    // 			return 'gagal';
    // 		}

    // }



    // // upload
    // public function upload() {

    // 	$data = array(
    // 	   'nama_file' => $nama_file ,
    // 	   'token' => $filteredarray ,
    // 	   'tokenstem' => $hasil
    // 	);

    // 	$this->db->insert('dokumen', $data);

    // 	return $query->result();
    // }

    // // Listing data
    // public function listing() {
    // 	$this->db->select('berita.*, kategori.nama_kategori, users.nama');
    // 	$this->db->from('berita');
    // 	// Join dg 2 tabel
    // 	$this->db->join('kategori','kategori.id_kategori = berita.id_kategori','LEFT');
    // 	$this->db->join('users','users.id_user = berita.id_user','LEFT');
    // 	// End join
    // 	$this->db->order_by('id_berita','DESC');
    // 	$query = $this->db->get();
    // 	return $query->result();
    // }



    // // Read data
    // public function read($slug_berita) {
    // 	$this->db->select('*');
    // 	$this->db->from('berita');
    // 	$this->db->where('slug_berita',$slug_berita);
    // 	$this->db->order_by('id_berita','DESC');
    // 	$query = $this->db->get();
    // 	return $query->row();
    // }

    // // Detail data
    // public function detail($id_berita) {
    // 	$this->db->select('*');
    // 	$this->db->from('berita');
    // 	$this->db->where('id_berita',$id_berita);
    // 	$this->db->order_by('id_berita','DESC');
    // 	$query = $this->db->get();
    // 	return $query->row();
    // }

    // // Tambah
    // public function tambah($data) {
    // 	$this->db->insert('berita',$data);
    // }

    // // Edit
    // public function edit($data) {
    // 	$this->db->where('id_berita',$data['id_berita']);
    // 	$this->db->update('berita',$data);
    // }

    // // Delete
    // public function delete($data) {
    // 	$this->db->where('id_berita',$data['id_berita']);
    // 	$this->db->delete('berita',$data);
    // }
}

/* End of file Dashboared_model.php */
/* Location: ./application/models/Dashboard_model.php */
<?php
class Mdtc_mbr_2 extends MY_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
        //		$this->db = $this->load->database('db_ecommerce',TRUE);
//		$this->db2 = $this->load->database('klink_mlm2010',TRUE);
    }
    public function getAll()
    {
        return $this->db->get('post');
    }
    
    public function getNoTiket($pref)
    {
        $getEtiketCodeId = "SELECT a.etiket_code_id FROM master_etiket a WHERE a.id = '$pref'";
        $resEtiket = $this->getRecordset($getEtiketCodeId, null, "db_ecommerce");
        if ($resEtiket != null) {
            $codeEtiket = $resEtiket[0]->etiket_code_id;
           
            $qry = "SELECT * FROM master_etiket_no_seq a WHERE a.etiket_codename = '$codeEtiket'";
            $res = $this->getRecordset($qry, null, "db_ecommerce");
            $no_tiket = "";
           
            if ($res == null) {
                $ins = "INSERT INTO master_etiket_no_seq (etiket_codename, lastno) VALUES ('$codeEtiket', '2')";
                $query = $this->executeQuery($ins, $this->setDB(1));
               
                //$this->getNoTiket($pref);
                $noawal = sprintf("%04s", 1);
                $no_tiket = $codeEtiket.$noawal;
                return $no_tiket;
            //$nextRound = sprintf("%03s", "1");
               //$codeEtiketNumber = $pref.$nextRound;
            } else {
                $prefix = $res[0]->etiket_codename;
                if ($res[0]->lastno == 9999) {
                    $no_tiket = $prefix."9999";
                    $newLastno = 1;
                } else {
                    $newLastno = $res[0]->lastno + 1;
                    $nextRound = sprintf("%04s", $res[0]->lastno);
                    $no_tiket = $prefix.$nextRound;
                }
                
                $upd = "UPDATE master_etiket_no_seq SET lastno = '$newLastno' WHERE etiket_codename = '$prefix'";
                $updExe = $this->executeQuery($upd, $this->setDB(1));
                
                return $no_tiket;
            }
        } else {
            return false;
        }
    }
    
    public function getSuffixIncPay($orderID)
    {
        $qry = "SELECT a.orderno, a.token, a.bank_code_payment, b.bankCode, b.bankDisplayNm,
					c.bankcode_ip, a.total_pay
				FROM ecomm_trans_hdr_sgo a
				INNER JOIN ecomm_bank b ON (a.bank_code_payment = b.id)
				INNER JOIN ecomm_bank_master c ON (b.bank_master = c.id)
				WHERE a.orderno = '$orderID'";
        //echo $qry;
        $res = $this->getRecordset($qry, null, "db_ecommerce");
        return $res;
    }
    
    public function getLastNoIncPay($prefix)
    {
        //$qry = "SELECT * FROM db_ecommerce.dbo.ecomm_sysno a where a.[prefix] = '$prefix'";
        //$res = $this->getRecordset($qry, NULL, "db_ecommerce");
        $res = $this->getLastNoIp($prefix);
        if ($res != null) {
            $updLastNo = $this->setNextLastNoIP($prefix);
            $lastno = $res[0]->lastno;
            return $lastno;
        } else {
            $ins = "INSERT INTO db_ecommerce.dbo.ecomm_sysno (prefix, lastno) VALUES ('$prefix','1')";
            $queryPaydet = $this->db->query($ins);
            $this->getLastNoIncPay($prefix);
        }
    }
    
    public function getLastNoIp($prefix)
    {
        $qry = "SELECT * FROM db_ecommerce.dbo.ecomm_sysno a where a.[prefix] = '$prefix'";
        $res = $this->getRecordset($qry, null, "db_ecommerce");
        return $res;
    }
    
    public function checkIPExist($orderno)
    {
        $qry = "SELECT b.token, a.refno, b.IPno 
				FROM db_ecommerce.dbo.ecomm_trans_hdr b
				LEFT OUTER JOIN  klink_mlm2010.dbo.bbhdr a 
				  ON (a.refno COLLATE SQL_Latin1_General_CP1_CI_AS = b.token)
				where b.token = '$orderno'";
        $res = $this->getRecordset($qry, null, "db_ecommerce");
        return $res;
    }
    
    public function setNextLastNoIP($prefix)
    {
        $upd = "UPDATE db_ecommerce.dbo.ecomm_sysno SET lastno = lastno + 1 WHERE prefix = '$prefix'";
        $queryPaydet = $this->db->query($upd);
        return $queryPaydet;
    }
    
    public function setIncomingPayment($arr)
    {
        $dbqryx  = $this->load->database("db_ecommerce", true);
        $dbqryx->trans_start();
    
        //$updEccommHeader = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr SET IPno = '$ip' WHERE token = '$token'";
        //$dbqryx->query($updEccommHeader);
        
        
        $upd = array(
            'IPno' => $arr['ip_no'],
            'dateIP' => date("Y-m-d H:i:s")
        );
        $dbqryx->where('token', $arr['token']);
        $dbqryx->update('db_ecommerce.dbo.ecomm_trans_hdr', $upd);
        
        //$updEtiketHeader = "UPDATE db_ecommerce.dbo.etiket_header SET ip_no = '$ip' WHERE orderno = '$token'";
        //$dbqryx->query($updEtiketHeader);
        
        $upd2 = array(
            'ip_no' => $arr['ip_no'],
            'status' => '1'
        );
        $dbqryx->where('orderno', $arr['token']);
        $dbqryx->update('db_ecommerce.dbo.etiket_header', $upd2);
        
        $updEt = array(
            'status' => '0'
        );
        $dbqryx->where('order_no', $arr['token']);
        $dbqryx->update('db_ecommerce.dbo.trx_etiket', $updEt);
        
        $arrIns = array(
            "trcd" => $arr['ip_no'],
            "type" => "I",
            "trtype" => "IP",
            "bankacccd" => "BL01",
            "description" => "PEMBAYARAN TOM ONLINE (ECOMMERCE) DARI 0",
            "status" => "O",
            "dfno" => "MKT",
            "createnm" => "ECOMMERCE",
            "effect" => "-",
            "PT_SVRID" => "ID",
            "custtype" => "S",
            "refno" => $arr['orderno'],
            "amount" => $arr['amount']
        );
        $dbqryx->insert("klink_mlm2010.dbo.bbhdr", $arrIns);
        
        
        //END TRANSACTION
        $dbqryx->trans_complete();

        //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === false) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false",
              "message" => "create IP gagal"
            );
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
              "message" => "create IP berhasil : $arr[ip_no]",
              "success" => "success create IP",
              //"arraySuccess" => $successArray,
              "ip_no" => $arr['ip_no'],
              //"arrayDouble" => $doubleArray,
            );
        }
    }
    
    public function generateIP($orderID)
    {
        //get suffix inc payment
        $incPay = $this->getSuffixIncPay($orderID);
        if ($incPay != null && $incPay[0]->bankcode_ip != null) {
            //proses get Incoming Payment Number
            //$noawal = "IPT".date("ym");
            $noawal = "IPT".date("ym");
            $prefix = $noawal.$incPay[0]->bankcode_ip;
            $lastno = $this->getLastNoIncPay($prefix);
            $digit_lastno = sprintf('%04d', $lastno);
            $ip_no = $noawal.$digit_lastno."/".$incPay[0]->bankcode_ip;
            //echo $ip_no;
            //$amount = $incPay[0]->total_pay;
            $dataInc = array(
                "ip_no" => $ip_no,
                "token" => $orderID,
                "orderno" => $incPay[0]->orderno,
                "amount" => $incPay[0]->total_pay
            );
            $setInc = $this->setIncomingPayment($dataInc);
            return $setInc;
        } else {
            return null;
        }
    }

    public function getAcara()
    {
        $qry ="SELECT A.KdAcara, A.Kota, A.Alamat,CONVERT(VARCHAR(30),A.TglAcara1,106) as TglAcr, A.tglAcara2
		       FROM nh_dtc_loc A where status=0 order by A.Kota, A.tglAcara1";
        $rec = $this->getRecordset($qry, null, $this->db2);
        return $rec;
    }


    public function getTrxReturn($dfno)
    {
        $result = array();
        $this->db->select('*')
            ->from('db_ecommerce.dbo.etiket_header')
            ->where('dfno', $dfno)
            ->where('status', 0);

        $q = $this->db->get();
        //		echo $this->db->last_query();
        foreach ($q->result() as $row) {
            $result[] = $row;
        }

        return $result;
    }


    public function checkAuthLoginKnet()
    {
        $qry = "SELECT a.dfno,a.fullnm,a.memberstatus,
                a.password,a.tel_hp,a.tel_hm,a.email,
                a.addr1,a.addr2,a.addr3, a.idno, a.vc_count,
                a.sponsorid,b.fullnm as sponsorname,a.loccd,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,a.novac, a.bankaccno, a.bankaccnm
                FROM msmemb a
                    left outer join msmemb b on (a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                WHERE a.dfno = 'IDSPAAA91433'";
        $res = $this->getRecordset($qry, null, $this->db2);
        return $res;
    }



    public function getTrxSGO($orderno)
    {
        $result = array();
        $this->db->select('a.orderno, a.token, a.total_pay, b.bankCode,b.bankDesc')
            ->from('ecomm_trans_hdr_sgo a')
            ->join('ecomm_bank b', 'a.bank_code_payment=b.id', 'left')

            ->where('orderno', $orderno);

        $q = $this->db->get();
        //		echo $this->db->last_query();
        foreach ($q->result() as $row) {
            $result[] = $row;
        }
        //		print_r($result);
        return $result;
    }

    public function TrxDetail($ord)
    {
        $qry = "SELECT a.id, a.urut, a.order_no, a.valid_dfno, a.valid_fullnm, a.price, b.lokasi, b.nama, b.event_date, a.email FROM trx_etiket a
			LEFT JOIN master_etiket b
			ON a.id_etiket=b.id
			 WHERE order_no= '$ord'";
        $res = $this->getRecordset($qry, null, $this->db1);
        if ($res == null) {
        }
        return $res;
    }

    public function getAcara2()
    {
        $result = array();
        $this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
            ->from('db_ecommerce.dbo.master_etiket')
            ->where('exp_date_offline >= GETDATE()')
            ->where('event_date >= GETDATE()')
            ->where('status= 0')

            ->order_by('createdt', 'desc');

        $q = $this->db->get();
        //echo $this->db->last_query();
        foreach ($q->result() as $row) {
            $result[] = $row;
        }

        return $result;
    }

    //diganti function ini setelah production
    function getListTipeAcara() {
		$qry = "SELECT a.*
				FROM master_etiket_cat a
				WHERE a.cat_id IN (
					SELECT DISTINCT (b.cat_id )
					FROM master_etiket b 
					WHERE b.[status] = 0 and b.exp_date_online >= GETDATE()
				) AND status = '1'";
		$res = $this->getRecordset($qry, "db_ecommerce");
		return $res;
	}
    
    
    public function getListTipeAcaraVera()
    {
        /*$qry = "SELECT a.*
				FROM master_etiket_cat a
				WHERE a.cat_id IN (
					SELECT DISTINCT (b.cat_id )
					FROM master_etiket b 
					WHERE b.[status] = 0 and b.exp_date_online >= GETDATE()
                    OR b.id = 182
                ) AND status = '1' OR a.cat_id = 6"; */
                
                $qry = "SELECT a.*
				FROM master_etiket_cat a
				WHERE a.cat_id IN (
					SELECT DISTINCT (b.cat_id )
					FROM master_etiket b 
					WHERE b.id = 214
				)";
        $res = $this->getRecordset($qry, "db_ecommerce");
        return $res;
    } 
    
    
    public function getAcaraOnlineByCategory($cat_id)
    {
        $this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
      ->from('db_ecommerce.dbo.master_etiket')
      ->where('exp_date_online >= GETDATE()')
      ->where('event_date >= GETDATE()')
      ->where('status= 0')
      ->where('cat_id', $cat_id)
      ->order_by('nama', 'ASC')
      ->order_by('lokasi', 'ASC');
        $q = $this->db->get();
        return $q->result();
    }

    //nanti di set status = 0;
    public function getAcaraOnlineByCategoryNew($cat_id) {
        $this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
      ->from('db_ecommerce.dbo.master_etiket')
      ->where('exp_date_online >= GETDATE()')
      ->where('event_date >= GETDATE()')
      ->where('status= 0')
      ->where('cat_id', $cat_id)
      ->order_by('nama', 'ASC')
      ->order_by('lokasi', 'ASC');
        $q = $this->db->get();
        return $q->result();
    }

    public function getAcaraOnlineByCategoryVera($cat_id) {
        $this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
      ->from('db_ecommerce.dbo.master_etiket')
      //->where('exp_date_online >= GETDATE()')
      //->where('event_date >= GETDATE()')
      //->where('status= 0')
      ->where('id', '214')
      ->order_by('nama', 'ASC')
      ->order_by('lokasi', 'ASC');
        $q = $this->db->get();
        return $q->result();
    }
    
    public function getAcaraOnline()
    {
        $result = array();
        $this->db->select('*')
            ->from('db_ecommerce.dbo.master_etiket')
            ->where('exp_date_online >= GETDATE()')
            ->where('status= 0')

            ->order_by('createdt', 'desc');

        $q = $this->db->get();
        //echo $this->db->last_query();
        foreach ($q->result() as $row) {
            $result[] = $row;
        }

        return $result;
    }
    
    public function getListPrdBundlingTiketById($id)
    {
        $qry = "SELECT a.kode_produk, c.prdnm as nama, b.dp as harga, a.tipe, a.status 
			FROM master_etiket_merchandise_product a 
			INNER JOIN klink_mlm2010.dbo.msprd c ON (a.kode_produk collate SQL_Latin1_General_CP1_CS_AS = c.prdcd)
			INNER JOIN klink_mlm2010.dbo.pricetab b ON (a.kode_produk collate SQL_Latin1_General_CP1_CS_AS = b.prdcd)		
			WHERE a.etiket_id = '$id' AND b.pricecode = '12W3'";
            
        return $this->getRecordset($qry, null, "db_ecommerce");
    }

    public function insertPrdBundlingTicketPowerNet($no_tiket, $tkt) {
        $exeQry = $this->load->database("db_ecommerce", true);
        $qry = "SELECT a.kode_produk, c.prdnm as nama, b.dp as harga 
			FROM master_etiket_merchandise_product a 
			INNER JOIN klink_mlm2010.dbo.msprd c ON (a.kode_produk collate SQL_Latin1_General_CP1_CS_AS = c.prdcd)
			INNER JOIN klink_mlm2010.dbo.pricetab b ON (a.kode_produk collate SQL_Latin1_General_CP1_CS_AS = b.prdcd)		
			WHERE a.etiket_id = '$tkt'
            and b.pricecode = '12W3'";
        //echo $qry."<br />";
        $query = $exeQry->query($qry, null);
        if ($query->num_rows() > 0) {
            $nox = 1;
            foreach ($query->result() as $row) {
                $qry = "INSERT INTO db_ecommerce.dbo.trx_etiket_product (no_tiket, prdcd, prdnm, qty, harga) 
				        values ('".$no_tiket."', '".$row->kode_produk."', '".$row->nama."', '1', '".$row->harga."')";
                //echo $qry;
                $exeQry->query($qry);
                $nox++;
            }
        }       
    }

    public function listUangMakan($tkt) {
        $qry = "SELECT a.kode_produk 
                FROM master_etiket_merchandise_product a 
                WHERE a.etiket_id = '$tkt' and a.[status] = '0'";
        return $this->getRecordset($qry, null, "db_ecommerce");
    }
    
    public function insertPrdBundlingTicket($no_tiket, $tkt, $prd_bundling)
    {
        $free_prd = substr($prd_bundling, 0, -1);
        $free_prd2 = $free_prd.",UM001";

        //$uang_makan = $this->listUangMakan($tkt);
		$str = "";
		$exeQry = $this->load->database("db_ecommerce", true);
        $uangMakan = "SELECT a.kode_produk 
                FROM db_ecommerce.dbo.master_etiket_merchandise_product a 
                WHERE a.etiket_id = '$tkt' and a.[status] = '0'";
        $query = $exeQry->query($uangMakan, null);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $str .= $row->kode_produk.",";
            }    
            $str2 = substr($str, 0, -1);
            $free_prd2 = $free_prd.",".$str2;
        }  
		
		$split = explode(",", $free_prd2);
		$free_prd1 = set_list_array_to_string($split);
        
        
        $qry = "SELECT a.kode_produk, c.prdnm as nama, b.dp as harga 
			FROM master_etiket_merchandise_product a 
			INNER JOIN klink_mlm2010.dbo.msprd c ON (a.kode_produk collate SQL_Latin1_General_CP1_CS_AS = c.prdcd)
			INNER JOIN klink_mlm2010.dbo.pricetab b ON (a.kode_produk collate SQL_Latin1_General_CP1_CS_AS = b.prdcd)		
			WHERE a.etiket_id = '$tkt' AND a.kode_produk IN ($free_prd1)
			and b.pricecode = '12W3'";
        //echo $qry;
        //echo "<br />";
        
        //}
        $query = $exeQry->query($qry, null);
        if ($query->num_rows() > 0) {
            $nox = 1;
            foreach ($query->result() as $row) {
                $warna = "";
                $ukuran = "";
                $nox = 1;
                //echo $nox;
                //echo "<br />";
                /*
                if($row->warna != null && $row->warna != "" && $row->warna != " ") {
                  $warna = " ".$row->warna;
                }

                $ukuran = "";
                if($row->ukuran != null && $row->ukuran != "" && $row->ukuran != " ") {
                  $ukuran = " ".$row->ukuran;
                }

                $dtl2 = null;
                $dtl2 = array(
                 "no_tiket" => $no_tiket,
                 "prdcd" => $row->kode_produk,
                 "prdnm" => $row->nama.$warna.$ukuran,
                 "qty" => 1,
                 "harga" => $row->harga,
                );
                $exeQry->insert('db_ecommerce.dbo.trx_etiket_product', $dtl2);
                */
                
                $qry = "INSERT INTO db_ecommerce.dbo.trx_etiket_product (no_tiket, prdcd, prdnm, qty, harga) 
				        values ('".$no_tiket."', '".$row->kode_produk."', '".$row->nama.$warna.$ukuran."', '1', '".$row->harga."')";
                //echo $qry;
                $exeQry->query($qry);
                $nox++;
            }
            
            /*
            $uangMakan = "SELECT a.prdcd, a.prdnm, b.dp
                          FROM klink_mlm2010.dbo.msprd a
                          INNER JOIN klink_mlm2010.dbo.pricetab b ON (a.prdcd = b.prdcd)
                          WHERE a.prdcd = 'UM001' and b.pricecode = '12W3'";
            $query2 = $exeQry->query($uangMakan, null);
            if($query2->num_rows() > 0)  {
                foreach ($query2->result() as $row2) {
                    $dtl3 = array(
                         "no_tiket" => $no_tiket,
                         "prdcd" => $row2->prdcd,
                         "prdnm" => $row2->prdnm,
                         "qty" => 1,
                         "harga" => (int) $row2->dp,
                        );
                    $exeQry->insert('db_ecommerce.dbo.trx_etiket_product', $dtl3);
                }
            }*/
        }
    }
    
    public function getDetailTiket($ID_PRODUCT)
    {
        $this->db->select('*');
        $this->db->where('id', $ID_PRODUCT);
        return $this->db->get('db_ecommerce.dbo.master_etiket');
    }
    
    public function getDetailTiketV2($id_etiket)
    {
        $qry = "SELECT 
					ISNULL(COUNT(a.order_no), 0) as jumlah_terjual, 
					b.max_online, 
					CONVERT(VARCHAR(10), b.exp_date_online, 121) as exp_date_online, 
					ISNULL(b.max_kuota, 50) AS max_kuota,
					b.earlybird_date, b.price_offline, b.max_offline, b.price_online,
					b.max_online, b.price_earlybird_online, b.max_offline, b.price_earlybird_online
				FROM master_etiket b
				LEFT OUTER JOIN trx_etiket a ON (a.id_etiket = b.id and a.[status] = '0')
				WHERE b.id = '$id_etiket' 
				GROUP BY b.max_online, b.exp_date_online, b.max_kuota,
				b.earlybird_date, b.price_offline, b.max_offline, b.price_online,
					b.max_online, b.price_earlybird_online, b.max_offline, b.price_earlybird_online";
        //echo $qry;
        $res = $this->getRecordset($qry, null, "db_ecommerce");
        return $res;
    }
    
    public function checkKuotaDanExpAcara($id_etiket)
    {
        $qry = "SELECT 
					ISNULL(COUNT(a.order_no), 0) as jumlah_terjual, 
					b.max_online, 
					CONVERT(VARCHAR(10), b.exp_date_online, 121) as exp_date_online, 
					ISNULL(b.max_kuota, 50) AS max_kuota
				FROM master_etiket b
				LEFT OUTER JOIN trx_etiket a ON (a.id_etiket = b.id and a.[status] = '0')
				WHERE b.id = '$id_etiket' 
				GROUP BY b.max_online, b.exp_date_online, b.max_kuota";
        //echo $qry;
        $res = $this->getRecordset($qry, null, "db_ecommerce");
        return $res;
    }
    
    public function checkJumlahpendaftar($orderno)
    {
        $qry = "SELECT 
					ISNULL(COUNT(a.order_no), 0) as jumlah_pendaftar, a.id_etiket, a.[status]
				FROM trx_etiket a WHERE a.order_no = '$orderno' and a.[status] = '1'
				GROUP BY a.id_etiket, a.[status]";
        //echo $qry;
        $res = $this->getRecordset($qry, null, "db_ecommerce");
        return $res;
    }
    
    public function checkValidAcara($id_tiket, $jumlah_pendaftar)
    {
        $today = date("Y-m-d");
        $res = $this->checkKuotaDanExpAcara($id_tiket);
        if ($res != null) {
            $sisa = $res[0]->max_kuota - $res[0]->jumlah_terjual;
            $tglexpire = $res[0]->exp_date_online;
            
            //bila pendaftaran online sudah ditutup, FALSE
            if ($tglexpire < $today) {
                $resJson = jsonFalseResponse("Pendaftaran sudah ditutup tanggal $tglexpire");
                return $resJson;
            }
            //jika sisa kuota lebih kecil dari jumlah pendaftar
            if ($sisa < $jumlah_pendaftar) {
                if ($sisa < 0) {
                    $msg = "Kuota sudah habis..";
                } else {
                    $msg = "Sisa kuota hanya $sisa, jumlah pendaftar : $jumlah_pendaftar";
                }
                $resJson = jsonFalseResponse($msg);
                return $resJson;
            } else {
                $arrsisa = array("sisa_kuota" => $sisa);
                $resJson = jsonTrueResponse($arrsisa, "Jumlah pendaftar masih kurang dari sisa kuota");
                return $resJson;
            }
        } else {
            $resJson = jsonFalseResponse("id E-ticket tidak ditemukan..");
            return $resJson;
        }
    }
    
    public function getMerchandiseByIdEtiket($id)
    {
        $getCatPrd = "SELECT DISTINCT(a.tipe) as tipe
						FROM master_etiket_merchandise_product a
						WHERE a.etiket_id = '$id' and a.status= '1' and a.tipe IN (
							SELECT b.tipe_merchandise 
							FROM master_etiket_merchandise_cat b 
							WHERE b.ukuran = '1' or b.warna = '1'
						)";
        //echo $getCatPrd;
        $hasilGetCatPrd = $this->getRecordset($getCatPrd, null, "db_ecommerce");
        
        if ($hasilGetCatPrd == null) {
            $arr = null;
            $arr2 = null;
        } else {
            $i = 0;
            foreach ($hasilGetCatPrd as $hasilCat) {
                $qry1 = "SELECT * FROM master_etiket_merchandise_product a 
					WHERE a.etiket_id = '$id' and status= '1' and a.tipe = '".$hasilCat->tipe."'";
                
                $arr[$hasilCat->tipe] = $this->getRecordset($qry1, null, "db_ecommerce");
                $i++;
            }
            
                
            $getCatPrd2 = "SELECT DISTINCT(a.tipe) as tipe
								FROM master_etiket_merchandise_product a
								WHERE a.etiket_id = '$id' and a.status= '1' and a.tipe IN (
									SELECT b.tipe_merchandise 
									FROM master_etiket_merchandise_cat b 
									WHERE b.ukuran = '0' AND b.warna = '0'
								)";
            $hasilGetCatPrd2 = $this->getRecordset($getCatPrd2, null, "db_ecommerce");
            foreach ($hasilGetCatPrd2 as $hasilCat2) {
                $qry1 = "SELECT * FROM master_etiket_merchandise_product a 
					WHERE a.etiket_id = '$id' and status= '1' and a.tipe = '".$hasilCat2->tipe."'";
                
                $arr2[$hasilCat2->tipe] = $this->getRecordset($qry1, null, "db_ecommerce");
                $i++;
            }
                
            /*
            $qry2 = "SELECT * FROM master_etiket_merchandise_product a
                WHERE a.etiket_id = '$id' and a.status= '1' and a.tipe IN (
                    SELECT b.tipe_merchandise
                    FROM master_etiket_merchandise_cat b
                    WHERE b.ukuran = '0' AND b.warna = '0'
                )";

            $hasil12 = $this->getRecordset($qry2, null, "db_ecommerce"); */
        }
        $array = array(
            "prdPilihan" => $arr,
            "prdPasti" => $arr2,
        );
        return $array;
    }

    public function getDetailBank($ID_PRODUCT)
    {
        $this->db->select('*');
        $this->db->where('id', $ID_PRODUCT);
        return $this->db->get('db_ecommerce.dbo.ecomm_bank');
    }

    public function getBE($data)
    {
        $qry ="SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm";
        $rec = $this->getRecordset($qry, null, $this->db2);
        return $rec;
    }

    public function checkID1Versi2($data) {
        $qry = "select aa.dfno, 
				   aa.fullnm, 
				   ISNULL(aa.rank, '-') AS rank, 
				   ISNULL(c.shortnm, '-') AS shortnm,
				   aa.tel_hp, aa.email
				from (
					SELECT A.dfno, A.fullnm, A.tel_hp, A.email,
						(
							SELECT TOP 1 X.adjustedrank
							FROM tbonus X 
							WHERE A.dfno=X.distributorcode 
							ORDER BY X.bonusyear DESC, X.bonusmonth DESC) as rank
					FROM msmemb A 
					WHERE A.dfno= '$data'
					AND A.DFNO NOT IN (
					SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC
					)
				) aa 
				left outer join msrank c on aa.rank=c.level";
        //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db2);
        return $res;
    }

    public function checkID1($data)
    {
        /*
         $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
                FROM msmemb A inner join tbonus B
                On A.dfno=B.distributorCode
                where A.dfno= '$data'
                AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
                Group by A.dfno, A.fullnm ";
         */
        $qry = "select aa.dfno, 
				   aa.fullnm, 
				   ISNULL(aa.rank, '-') AS rank, 
				   ISNULL(c.shortnm, '-') AS shortnm,
				   aa.tel_hp, aa.email
				from (
					SELECT A.dfno, A.fullnm, A.tel_hp, A.email,
						(
							SELECT TOP 1 X.adjustedrank
							FROM tbonus X 
							WHERE A.dfno=X.distributorcode 
							ORDER BY X.bonusyear DESC, X.bonusmonth DESC) as rank
					FROM msmemb A 
					WHERE A.dfno= '$data'
					AND A.DFNO NOT IN (
					SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC
					)
				) aa 
				left outer join msrank c on aa.rank=c.level";
        //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db2);

        if ($res == null) {
            //		throw new Exception("Data tidak ada..!!", 1);
            $res='Err';
        }
        return $res;
    }
    public function checkIDGO($data)
    {
        $qry = "SELECT 
					A.dfno, 
					A.fullnm,
					   (SELECT TOP 1 X.adjustedrank 
				       	FROM tbonus X
				        WHERE A.dfno=X.distributorcode
				        ORDER BY X.bonusyear DESC, X.bonusmonth DESC) 
					as rank, 
					a.tel_hp, a.email, 
					a.addr1, a.addr2, a.addr3
				FROM msmemb A
				where A.dfno= '$data'";
        //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db2);

        if ($res == null) {
            //		throw new Exception("Data tidak ada..!!", 1);
            $res='Err';
        }
        return $res;
    }
    
    public function getBankForTicket()
    {
        $slc = "select * from ecomm_bank
                WHERE id IN (1, 4, 16, 21, 26, 27, 28) order by bankDesc";

        $result = $this->getRecordset($slc, null, $this->setDB(1));
        return $result;
    }
    
    public function getBank()
    {
        $qry ="SELECT *
		       FROM db_ecommerce.dbo.ecomm_bank where status='1' ";
        $rec = $this->getRecordset($qry, null, $this->db2);
        return $rec;
    }
    //TAMPILKAN DATA YANG SUDAH DAFTAR DESCENDING TGL
    public function tabelData()
    {
        $qry = "SELECT A.noPeserta,A.dfno, A.fullnm,A.tgldaftar,A.lokasi,A.peringkat
		        FROM NH_DTC A ORDER BY A.tgldaftar desc, A.fullnm";
        $res = $this->getRecordset($qry, null, $this->db2);

        if ($res == null) {
            throw new Exception("Data tidak ada..!!", 1);
        }
        return $res;
    }

    public function simpanPeserta($data)
    {
        $rekam ="insert into NH_DTC (IDMember,fullnm,tgldaftar,peringkat,
		lokasi) values ()";
    }

    //		function

    public function Counter2($DOC)
    {
        $CI =&get_instance();

        $waktu = date('y').date('m').date('d');

        //		$this->db = $CI->load->database('default',TRUE);

        $this->db->where(array('id_etiket'=>$DOC));
        $count = $this->db->count_all_results('db_ecommerce.dbo.trx_etiket');

        if ($count == 0) {
            //			echo $this->db->last_query();
            $value = 1;
        //			$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
        } else {
            $query = 'SELECT MAX(urut) as urut FROM db_ecommerce.dbo.trx_etiket WHERE id_etiket = ? ';
            $result = $this->db->query($query, array($DOC ));
            $row = $result->row();

            $value = $row->urut+1;

            //			$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
        }
        return $value;
    }

    public function simpanTrx($dtl1, $dtl3, $status)
    {
        $this->db->insert('db_ecommerce.dbo.ecomm_trans_hdr_sgo', $dtl1);
        //		echo $this->db->last_query();

        $this->db->insert('db_ecommerce.dbo.ecomm_trans_paydet_sgo', $dtl3);
        //		echo $this->db->last_query();
        //		$this->db->trans_complete();
        $this->db->insert('db_ecommerce.dbo.etiket_header', $status);

        //		return $this->db->last_query();

        $qry = "SELECT A.orderno,A.total_pay
		        FROM ecomm_trans_hdr_sgo A WHERE A.orderno= '".$dtl1['orderno']."'  ";
        $res = $this->getRecordset($qry, null, $this->db1);

        return $res;
    }

    public function ReqSGO($dtl)
    {
        $qry = "SELECT A.orderno,A.total_pay, A.payConnectivity, A.bank_code_payment
		        FROM ecomm_trans_hdr_sgo A WHERE A.orderno= '".$dtl."'  ";
        $res = $this->getRecordset($qry, null, $this->db1);

        return $res;
    }



    public function simpanDetTrx($dtl2)
    {

//		print_r($dtl2);
        $this->db->insert('db_ecommerce.dbo.ecomm_trans_det_prd_sgo', $dtl2);

        //		$this->db->trans_complete();


//		return $this->db->last_query();
    }

    public function saveDetail($dtl)
    {
        //		print_r($dtl);
        $this->db->insert('db_ecommerce.dbo.trx_etiket', $dtl);
        //		$this->db->trans_complete();
//		 return $this->db->last_query();
    }
    public function insertVA($dtl)
    {
        //		print_r($dtl);
        $this->db->insert('db_ecommerce.dbo.va_cust_pay_det', $dtl);
        //		$this->db->trans_complete();
//		 return $this->db->last_query();
    }

    public function updateStok($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('master_etiket', $data);
        //		$this->db->trans_complete();
//		return $this->db->last_query();
    }



    public function updatePayStatusSGO($trans_id)
    {
        $this->db = $this->load->database('db_ecommerce', true);
        $upd1 = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo
				 SET status_vt_pay = '1'
				 WHERE orderno = '$trans_id'";
        $exeUpd1 = $this->db->query($upd1);

        $upd2 = "UPDATE db_ecommerce.dbo.ecomm_trans_paydet_sgo
				 SET paystatus = 'ok'
				 WHERE orderno = '$trans_id'";
        $exeUpd2 = $this->db->query($upd2);

        if ($exeUpd1 > 0 && $exeUpd2 > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function insertTrxTblEcomm($order_id, $orderno)
    {
        $this->db = $this->load->database('db_ecommerce', true);
        $datetrans = date("Y-m-d H:i:s");


        $secno = substr(md5(microtime()), rand(0, 26), 5);

        $qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
        $query = $this->db->query($qry);
        foreach ($query->result() as $row) {
            /*$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno)
                          values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".getUserID()."','".$orderno."')";
            */
            //			$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno)
//          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";
            //			//echo "checout <br>".$insCheckout."<br><br>";
            //			$queryCheckout = $this->db->query($insCheckout);

            //$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
            $gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;

            /*$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin)
                          values('".$row->id_memb."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".getUserID()."')";
            */
            //			$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin)
//          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
            //			//echo "LogTrans <br>".$insLogTrans."<br><br>";
            //			$queryLogTrans = $this->db->query($insLogTrans);

            $insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
			              SELECT '".$orderno."', seqno,paytype,docno, payamt, 'trx ok','capture', bank_code_payment, charge_admin
						  FROM 	db_ecommerce.dbo.ecomm_trans_paydet_sgo a
						  WHERE a.orderno='".$order_id."'";
            //"values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
            //echo "Paydet <br>".$insPaydet."<br><br>";
            $queryPaydet = $this->db->query($insPaydet);

            //UPDATE VOUCHER BELANJA
            //Add by DION @ 09/08/2016
            //			$updVch = "UPDATE a
            //				SET a.claim_dt = GETDATE(), a.claim_by = '".$row->userlogin."', a.claim_status = 1
            //				FROM ecomm_voucher_list a
            //				INNER JOIN ecomm_trans_paydet_sgo b ON (a.voucherno = b.docno COLLATE SQL_Latin1_General_CP1_CS_AS)
            //				WHERE b.orderno = '$order_id'";
            //			$queryUpdVch = $this->db->query($updVch);


            //di set 1 saat live
            $st_vt_pay = "1";

            //tambahan dion 09/10/2015
            if ($row->bank_code_payment == null || $row->bank_code_payment == "") {
                $charge_admin = 0;
                $charge_connectivity = 0;
                $bank_code_pay = 1;
            } else {
                $payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
                $querypayADM = $this->db->query($payADM);
                $payresadm = $querypayADM->result();
                $charge_admin = $payresadm[0]->charge_admin;
                $charge_connectivity = $payresadm[0]->charge_connectivity;
                $bank_code_pay = $row->bank_code_payment;
            }

            $is_ship = 0;
            if ($row->sentTo == "2") {
                $is_ship = 1;
            }

            $cshbck = 0;
            if ($row->cashback_point != null || $row->cashback_point != "") {
                $cshbck = $row->cashback_point;
            }

            $insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login,
							  totPayCP,id_lp, free_shipping, discount_shipping, cashback_point)
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                    '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."',
			                    ".$row->totPayCP.", '".$row->id_lp."', '".$row->free_shipping."', ".$row->discount_shipping.", ".$cshbck.");
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
						SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						FROM 	ecomm_trans_det_prd_sgo a
						WHERE 	a.orderno='".$order_id."'";
            //echo "header<br>".$insHeader."<br><br>";
            $queryHeader = $this->db->query($insHeader);

            //update DION @28/11/2016
            if ($row->is_login == "2" || $row->is_login == "3") {
                $qry = "SELECT A.orderno, SUM((b.cp * a.qty)) - SUM((b.dp * a.qty)) as hrg_selisih
						FROM ecomm_trans_det_prd a
						INNER JOIN master_prd_pricetab b ON (a.prdcd = b.cat_inv_id) AND b.pricecode = a.pricecode
						WHERE a.orderno = '$orderno' AND b.pricecode = a.pricecode
						GROUP BY a.orderno";
                //$hasilxxc = $this->getRecordset($qry, null, $this->db1);
                $hasilxxc = $this->db->query($qry);
                $hasilx = $hasilxxc->result();

                $updrs = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr SET totPayCP = ".$hasilx[0]->hrg_selisih." WHERE orderno = '$orderno'";
                $updrs_exe = $this->db->query($updrs);
                $sts = "UPDATE db_ecommerce.dbo.etiket_header SET status = '1' WHERE orderno = '$order_id'";
                $sts_exe = $this->db->query($sts);
            }
            //end update

            if ($queryPaydet > 0 &&
                $queryHeader > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }



    public function duplicate($valid, $tiket)
    {
        $qry = "SELECT * FROM trx_etiket WHERE valid_dfno='".$valid."' and id_etiket= '".$tiket."' and status = 0 ";
        //$hasilxxc = $this->getRecordset($qry, null, $this->db1);
        $hasilxxc = $this->db->query($qry);
        $hasilx = $hasilxxc->result();
        if ($hasilx !=null) {
            return true;
        } else {
            return false;
        }
    }

    public function updateTrxSGO($order_id)
    {
        $res['jne'] = "";
        $return = false;
        //		$personal_info = $this->session->userdata('personal_info');
        //$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
        //$trcd = $this->ecomm_trans_model->getsOrderno();
        $arr = $this->updatePayStatusSGO($order_id);
        //$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());
        if ($arr) {
            $trcd = $this->my_counter->getCounter2('IDTA', $this->uuid->v4());
            $upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd, $res['jne']);
            if ($upd > 0) {
                //				$this->generateCNoteJNE_baru($order_id, $trcd);
                $return = true;
            }
        }
        $result= $this->isSukses($order_id);
        if ($result==0) {
            $this->mdtc->hangus($order_id);
        } else {
            $this->mdtc->proses($order_id);
        }



        return $return;
    }

    public function isSukses($XD)
    {
        $result = array();
        $this->db->from('ecomm_trans_hdr');
        $this->db->where('token', $XD);
        return $this->db->count_all_results();
    }

    public function isMetodeTranfer($XD)
    {
        $result = array();
        $this->db->from('ecomm_trans_hdr_sgo');
        $this->db->where('orderno', $XD);
        $this->db->where('bank_code_payment', '16');
        $this->db->where('datetrans>=dateadd(HOUR, 2, getdate())');

        return $this->db->count_all_results();
    }

    public function checkID2($data, $acara, $tipe_acara) {
        $counter=0;

        $this->db->from('trx_etiket');
        $this->db->where('valid_dfno', $data);
        $this->db->where('id_etiket', $acara);
        $this->db->where('status', '0');

        $counter= $this->db->count_all_results();

        if ($counter == 0) {
            if($tipe_acara == "6") {
                /*$qryChkPne = "SELECT top 1 a.valid_dfno, a.id_etiket, b.event_date, a.[status]
                        FROM trx_etiket a
                        LEFT OUTER JOIN master_etiket b ON (a.id_etiket = b.id)
                        WHERE b.cat_id = '5' and a.valid_dfno = '$data'
                        AND a.[status] = '0'
                        and B.event_date <= GETDATE()";
                $checkQryChkPne = $this->getRecordset($qryChkPne, NULL, $this->db1);
                if($checkQryChkPne == null) {
                    return array("response" => "false", "message" => "Peserta belum pernah mengikuti TOM / sudah pernah terdaftar di TOM tapi acara belum berlangsung");    
                } */
                $checkSyarat = $this->checkSyaratPernahIkutTOM($data);
                if($checkSyarat['response'] == "false") {
                    return $checkSyarat;
                }
            } 

            if($tipe_acara == "8") {
                $checkSyarat = $this->checkIfMemberisLadies($data);
                if($checkSyarat['response'] == "false") {
                    return $checkSyarat;
                }
            }

            $res = $this->checkID1Versi2($data);
            if ($res == null) {
                return array("response" => "false", "message" => "Data member tidak valid..");
            } else {
                return jsonTrueResponse($res);   
            } 
        } else {
            return array("response" => "false", "message" => "Peserta tersebut sudah terdaftar di acara ini..");
        }
    }

    function checkIfMemberisLadies($dfno) {
        $qry = "SELECT a.dfno, a.sex
                FROM klink_mlm2010.dbo.msmemb a
                WHERE a.dfno = '$dfno'";
        $checkIfLadies = $this->getRecordset($qry, NULL, $this->db1);
        if($checkIfLadies != null) {
            if($checkIfLadies[0]->sex == "F") {
                return array("response" => "true", "message" => "Peserta adalah wanita");
            } else {
                return array("response" => "false", "message" => "Peserta hanya khusus untuk wanita..");
            }
        } else {
            return array("response" => "false", "message" => "Data member tidak ditemukan..");
        }
    }

    function checkSyaratPernahIkutTOM($data) {
        $qryChkPne = "SELECT top 1 a.valid_dfno, a.id_etiket, b.event_date, a.[status]
                FROM trx_etiket a
                LEFT OUTER JOIN master_etiket b ON (a.id_etiket = b.id)
                WHERE b.cat_id = '5' and a.valid_dfno = '$data'
                AND a.[status] = '0'
                and B.event_date <= GETDATE()";
        $checkQryChkPne = $this->getRecordset($qryChkPne, NULL, $this->db1);
        if($checkQryChkPne == null) {
            return array("response" => "false", "message" => "Peserta belum pernah mengikuti TOM / sudah pernah terdaftar di TOM tapi acara belum berlangsung");    
        } else {
            return array("response" => "true", "message" => "Valid, udah pernah ikut TOM dan acara TOM sudah berlangsung");    
        }
    }

    public function hangus($XD)
    {
        $result = array();
        $this->db->from('trx_etiket');
        $this->db->where('order_no', $XD);
        $hitung= $this->db->count_all_results();



        $payADM = "SELECT * FROM trx_etiket WHERE order_no = '$XD' ";
        $querypayADM = $this->db->query($payADM);
        $payresadm = $querypayADM->result();
        $idx = $payresadm[0]->id_etiket;

        /*
        $updrs = "UPDATE db_ecommerce.dbo.master_etiket SET max_online = max_online+".$hitung." WHERE id = '$idx'";
        $updrs_exe = $this->db->query($updrs);
        */
        $sts = "UPDATE db_ecommerce.dbo.etiket_header SET status='2' WHERE orderno = '$XD'";
        $sts_exe = $this->db->query($sts);
        
        //TAMBAHAN DION @23/12/2018, hapus data merchandise bila ada
        /*$delMerchandise = "DELETE FROM trx_etiket_product where no_tiket IN (
                    SELECT b.notiket FROM trx_etiket b  where b.order_no = '$XD'
                )";
        $delMerchandiseExe = $this->db->query($delMerchandise);*/
    }



    public function proses($XD)
    {
        $updrs = "UPDATE db_ecommerce.dbo.trx_etiket SET status = 0 WHERE order_no = '$XD'";
        $updrs_exe = $this->db->query($updrs);
        $sts = "UPDATE db_ecommerce.dbo.etiket_header SET status='1' WHERE orderno = '$XD'";
        $sts_exe = $this->db->query($sts);

        $this->sms2($XD);
    }
    
    public function sms2Dion($id)
    {
        $query="SELECT a.valid_fullnm, b.nama, b.lokasi, a.notiket, a.notelp, a.email,
		  CONVERT(VARCHAR(10),b.event_date, 103) as createdt
 		FROM db_ecommerce.dbo.trx_etiket A
		LEFT JOIN db_ecommerce.dbo.master_etiket b
		on a.id_etiket=b.id
		WHERE order_no = '$id'";
        $qrysms = $this->db->query($query);

        foreach ($qrysms->result() as $row) {
            $alternate2 = $this->load->database('alternate2', true);

            $text = 'Selamat, a/n '.$row->valid_fullnm.' terdaftar dalam : '.$row->nama.'-'.$row->lokasi.' '.$row->createdt.'. Nomor e-tiket anda: '.$row->notiket;

            //			$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
            //		                    VALUES ('".$row->notelp."','".$text."','K-Link Transaction Status', 'MyPhone6')";
            //			//echo "Ins SMS : $insSms";
            //			$query = $alternate2->query($insSms);

            $send = smsTemplate("087780441874", $text);



            $link="www.k-net.co.id/vtiket2/".$row->notiket;
            $content = 'Selamat, a/n '.$row->valid_fullnm.' telah terdaftar dalam : '.$row->nama.'-'.$row->lokasi.'. Nomor e-tiket anda: '.$row->lokasi.' atau klik '.$link;

            emailSend('DTC', "dionrespati@gmail.com", $content);
        }
    }
    
    public function smsByNoTiket($notiket)
    {
        $query = "SELECT a.valid_fullnm, b.nama, b.lokasi, a.notiket, a.notelp, a.email,
		  CONVERT(VARCHAR(10),b.event_date, 103) as createdt
 		FROM db_ecommerce.dbo.trx_etiket A
		LEFT JOIN db_ecommerce.dbo.master_etiket b
		on a.id_etiket=b.id
		WHERE a.notiket = '$notiket'";
        
        $qrysms = $this->db->query($query);

        foreach ($qrysms->result() as $row) {
            $alternate2 = $this->load->database('alternate2', true);

            $text = 'Selamat, a/n '.$row->valid_fullnm.' terdaftar dalam : '.$row->nama.'-'.$row->lokasi.' '.$row->createdt.'. Nomor e-tiket anda: '.$row->notiket;

            //			$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
            //		                    VALUES ('".$row->notelp."','".$text."','K-Link Transaction Status', 'MyPhone6')";
            //			//echo "Ins SMS : $insSms";
            //			$query = $alternate2->query($insSms);

            $send = smsTemplate($row->notelp, $text);
            //$send2 = smsTemplate("087780441874", $text);
            $send3 = smsTemplate("087878479101", $text);


            $link="www.k-net.co.id/vtiket2/".$row->notiket;
            $content = 'Selamat, a/n '.$row->valid_fullnm.' telah terdaftar dalam : '.$row->nama.'-'.$row->lokasi.'. Nomor e-tiket anda: '.$row->lokasi.' atau klik '.$link;

            emailSend('DTC', $row->email, $content);
        }
    }

    public function sms2($id)
    {
        $query="SELECT a.valid_fullnm, b.nama, b.lokasi, a.notiket, a.notelp, a.email,
		  CONVERT(VARCHAR(10),b.event_date, 103) as createdt
 		FROM db_ecommerce.dbo.trx_etiket A
		LEFT JOIN db_ecommerce.dbo.master_etiket b
		on a.id_etiket=b.id
		WHERE order_no = '$id'";
        $qrysms = $this->db->query($query);

        foreach ($qrysms->result() as $row) {
            $alternate2 = $this->load->database('alternate2', true);

            $text = 'Selamat, a/n '.$row->valid_fullnm.' terdaftar dalam : '.$row->nama.'-'.$row->lokasi.' '.$row->createdt.'. Nomor e-tiket anda: '.$row->notiket;

            //			$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
            //		                    VALUES ('".$row->notelp."','".$text."','K-Link Transaction Status', 'MyPhone6')";
            //			//echo "Ins SMS : $insSms";
            //			$query = $alternate2->query($insSms);

            $send = smsTemplate($row->notelp, $text);
            //$send2 = smsTemplate("087780441874", $text);
            $send3 = smsTemplate("087878479101", $text);

            $link="www.k-net.co.id/vtiket2/".$row->notiket;
            $content = 'Selamat, a/n '.$row->valid_fullnm.' telah terdaftar dalam : '.$row->nama.'-'.$row->lokasi.'. Nomor e-tiket anda: '.$row->lokasi.' atau klik '.$link;

            emailSend('DTC', $row->email, $content);
        }
    }



    public function TabelTiketBuyer($dfno)
    {
        $qry = "
		SELECT a.dfno, a.fullnm, a.notiket, a.valid_dfno,a.valid_fullnm, a.createdt, b.id, b.nama, b.lokasi,b.event_date,a.email
		FROM trx_etiket a
		LEFT JOIN master_etiket b
		ON a.id_etiket=b.id
		WHERE a.dfno='$dfno' AND a.status=0;
		";
        $res = $this->getRecordset($qry, null, $this->db1);

        return $res;
    }
    public function TabelTiketValid($dfno)
    {
        $qry = "
		SELECT a.dfno, a.fullnm, a.notiket, a.valid_dfno,a.valid_fullnm, a.createdt, b.id, b.nama, b.lokasi,b.event_date,a.email
		FROM trx_etiket a
		LEFT JOIN master_etiket b
		ON a.id_etiket=b.id
		WHERE a.valid_dfno='$dfno' AND a.status=0;
		";
        $res = $this->getRecordset($qry, null, $this->db1);
        //		echo $qry;

        return $res;
    }
    public function getPemenang($XD)
    {
        $result = array();
        $this->db->select('*')
            ->from('undian')
            ->where('FLAG', $XD)
        ;

        $q = $this->db->get();

        foreach ($q->result() as $row) {
            $result[] = $row;
        }

        return $result;
    }


    public function getPemenang20($XD, $mulai, $habis)
    {
        $result = array();
        //		$this->db->select('*')
        //			->from('undian')
        //			->where('FLAG',$XD)
        //			->limit($habis,$mulai)
        //		;
//

        //		$this->db->query("SELECT TOP $mulai * FROM (SELECT TOP $habis FROM undian ORDER BY id) ORDER BY id DESC");
        //		$q = $this->db->get();
        //		echo $this->db->last_query();
        //		echo '<br>';
        //		foreach($q->result() as $row)
        //		{
        //			$result[] = $row;
        //		}
        $qry="SELECT * FROM (SELECT TOP $mulai * FROM (SELECT TOP $habis * FROM undian where FLAG = $XD ORDER BY NOUNDIAN ) y ORDER BY NOUNDIAN DESC) x ORDER BY NOUNDIAN ASC";

        $result = $this->getRecordset($qry, null, $this->db1);


        //		echo $this->db->last_query()."; ";

        return $result;
    }

    public function getPemenangSemua()
    {
        $result = array();
        $this->db->select('*')
            ->from('undian a')
            ->join('hadiah b', 'a.FLAG=b.nohadiah', 'left')

        ;

        $q = $this->db->get();

        foreach ($q->result() as $row) {
            $result[] = $row;
        }

        return $result;
    }











    public function showDataPrint($id)
    {
        $slc = "select * from trx_etiket a
		LEFT JOIN master_etiket b
		on a.id_etiket=b.id

 		where notiket = '".$id."'";
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    public function sendTrxSMS($name, $dtc, $etiket, $link, $nope)
    {
        $alternate2 = $this->load->database('alternate2', true);
        $text = 'Selamat, a/n '.$name.' terdaftar dalam : '.$dtc.'. Nomor e-tiket anda: '.$etiket;
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
		                    VALUES ('".$nope."','".$text."','K-Link Transaction Status', 'MyPhone6')";
        //echo "Ins SMS : $insSms";
        $query = $alternate2->query($insSms);
    }
}

<?php
class Mdtc_mbrDEV extends MY_Model {
	function __construct() {
		// Call the Model constructor
		parent::__construct();
		$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
//		$this->db = $this->load->database('db_ecommerce',TRUE);
//		$this->db2 = $this->load->database('klink_mlm2010',TRUE);
	}
	public function getAll()
	{
		return $this->db->get('post');
	}

	function getAcara()
	{
		$qry ="SELECT A.KdAcara, A.Kota, A.Alamat,CONVERT(VARCHAR(30),A.TglAcara1,106) as TglAcr, A.tglAcara2
		       FROM nh_dtc_loc A where status=0 order by A.Kota, A.tglAcara1";
		$rec = $this->getRecordset($qry, NULL, $this->db2);
		return $rec;
	}


	function getTrxReturn($dfno)
	{
		$result = array();
		$this->db->select('*')
			->from('db_ecommerce.dbo.etiket_header')
			->where('dfno',$dfno)
			->where('status',0);

		$q = $this->db->get();
//		echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}


	function checkAuthLoginKnet() {
		$qry = "SELECT a.dfno,a.fullnm,a.memberstatus,
                a.password,a.tel_hp,a.tel_hm,a.email,
                a.addr1,a.addr2,a.addr3, a.idno, a.vc_count,
                a.sponsorid,b.fullnm as sponsorname,a.loccd,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,a.novac, a.bankaccno, a.bankaccnm
                FROM msmemb a
                    left outer join msmemb b on (a.sponsorid=b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                WHERE a.dfno = 'IDSPAAA91433'
                and a.password = '010198' and a.fullnm != 'TERMINATION'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}



	function getTrxSGO($orderno)
	{
		$result = array();
		$this->db->select('a.orderno, a.token, a.total_pay, b.bankCode,b.bankDesc')
			->from('ecomm_trans_hdr_sgo a')
			->join('ecomm_bank b', 'a.bank_code_payment=b.id', 'left')

			->where('orderno',$orderno);

		$q = $this->db->get();
//		echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}
//		print_r($result);
		return $result;
	}

	function TrxDetail($ord) {
		$qry = "SELECT a.id, a.urut, a.order_no, a.valid_dfno, a.valid_fullnm, a.price, b.lokasi, b.nama, b.event_date, a.email FROM trx_etiket a
			LEFT JOIN master_etiket b
			ON a.id_etiket=b.id
			 WHERE order_no= '$ord'";
		$res = $this->getRecordset($qry, NULL, $this->db1);

		if($res == null) {
//			throw new Exception("Data tidak ada..!!", 1);
		}
		return $res;
	}

	function getAcara2()
	{
		$result = array();
		$this->db->select('*')
			->from('db_ecommerce.dbo.master_etiket')
			->where('exp_date_offline >= GETDATE()')
			->where('status= 0')

			->order_by('createdt','desc');

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}
	function getAcaraOnline()
	{
		$result = array();
		$this->db->select('*')
			->from('db_ecommerce.dbo.master_etiket')
			->where('exp_date_online >= GETDATE()')
			->where('status= 33')

			->order_by('createdt','desc');

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}
	function getDetailTiket($ID_PRODUCT)
	{
		$this->db->select('*');
		$this->db->where('id',$ID_PRODUCT);
		return $this->db->get('db_ecommerce.dbo.master_etiket');
	}

	function getDetailBank($ID_PRODUCT)
	{
		$this->db->select('*');
		$this->db->where('id',$ID_PRODUCT);
		return $this->db->get('db_ecommerce.dbo.ecomm_bank');
	}

	function getBE($data)
	{
		$qry ="SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm";
		$rec = $this->getRecordset($qry, NULL, $this->db2);
		return $rec;
	}



	function checkID1($data) {
		/*
		 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank 
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode  
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm ";
		 */
		$qry = "select aa.dfno, aa.fullnm, ISNULL(aa.rank, '-') AS rank, ISNULL(c.shortnm, '-') AS shortnm
				from (
				SELECT A.dfno, A.fullnm, 
					   (SELECT TOP 1 X.adjustedrank
				       	FROM tbonus X 
				        WHERE A.dfno=X.distributorcode 
				        ORDER BY X.bonusyear DESC, X.bonusmonth DESC) as rank
				FROM msmemb A 
				where A.dfno= '$data'
				    AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
				) aa left outer join msrank c on aa.rank=c.level";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}
	function checkIDGO($data) {
		$qry = "SELECT A.dfno, A.fullnm,
					   (SELECT TOP 1 X.adjustedrank
				       	FROM tbonus X
				        WHERE A.dfno=X.distributorcode
				        ORDER BY X.bonusyear DESC, X.bonusmonth DESC) as rank, a.tel_hp, a.email, a.addr1, a.addr2, a.addr3
				FROM msmemb A
				where A.dfno= '$data'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}
	function getBank()
	{
		$qry ="SELECT *
		       FROM db_ecommerce.dbo.ecomm_bank where status='1' ";
		$rec = $this->getRecordset($qry, NULL, $this->db2);
		return $rec;
	}
	//TAMPILKAN DATA YANG SUDAH DAFTAR DESCENDING TGL
	function tabelData() {
		$qry = "SELECT A.noPeserta,A.dfno, A.fullnm,A.tgldaftar,A.lokasi,A.peringkat
		        FROM NH_DTC A ORDER BY A.tgldaftar desc, A.fullnm";
		$res = $this->getRecordset($qry, NULL, $this->db2);

		if($res == null) {
			throw new Exception("Data tidak ada..!!", 1);
		}
		return $res;
	}

	function simpanPeserta($data){
		$rekam ="insert into NH_DTC (IDMember,fullnm,tgldaftar,peringkat,
		lokasi) values ()";
	}

//		function

	function Counter2($DOC)
	{
		$CI =&get_instance();

		$waktu = date('y').date('m').date('d');

//		$this->db = $CI->load->database('default',TRUE);

		$this->db->where(array('id_etiket'=>$DOC));
		$count = $this->db->count_all_results('db_ecommerce.dbo.trx_etiket');

		if($count == 0) {
//			echo $this->db->last_query();
			$value = 1;
//			$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
		}else{
			$query = 'SELECT MAX(urut) as urut FROM db_ecommerce.dbo.trx_etiket WHERE id_etiket = ? ';
			$result = $this->db->query($query,array($DOC ));
			$row = $result->row();

			$value = $row->urut+1;

//			$no = $DOC.$waktu.str_pad($value,3,"00",STR_PAD_LEFT);
		}
		return $value;
	}

	function simpanTrx($dtl1,$dtl3,$status){

		$this->db->insert('db_ecommerce.dbo.ecomm_trans_hdr_sgo', $dtl1);
//		echo $this->db->last_query();

		$this->db->insert('db_ecommerce.dbo.ecomm_trans_paydet_sgo', $dtl3);
//		echo $this->db->last_query();
//		$this->db->trans_complete();
		$this->db->insert('db_ecommerce.dbo.etiket_header', $status);

//		return $this->db->last_query();

		$qry = "SELECT A.orderno,A.total_pay
		        FROM ecomm_trans_hdr_sgo A WHERE A.orderno= '".$dtl1['orderno']."'  ";
		$res = $this->getRecordset($qry, NULL, $this->db1);

		return $res;


	}

	function ReqSGO($dtl){

		$qry = "SELECT A.orderno,A.total_pay, A.payConnectivity
		        FROM ecomm_trans_hdr_sgo A WHERE A.orderno= '".$dtl."'  ";
		$res = $this->getRecordset($qry, NULL, $this->db1);

		return $res;


	}



	function simpanDetTrx($dtl2){

//		print_r($dtl2);
		$this->db->insert('db_ecommerce.dbo.ecomm_trans_det_prd_sgo', $dtl2);

//		$this->db->trans_complete();


//		return $this->db->last_query();
	}

	function saveDetail($dtl)
	{
//		print_r($dtl);
		$this->db->insert('db_ecommerce.dbo.trx_etiket', $dtl);
//		$this->db->trans_complete();
//		 return $this->db->last_query();

	}
	function insertVA($dtl)
	{
//		print_r($dtl);
		$this->db->insert('db_ecommerce.dbo.va_cust_pay_det', $dtl);
//		$this->db->trans_complete();
//		 return $this->db->last_query();

	}

	function updateStok($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('master_etiket',$data);
//		$this->db->trans_complete();
//		return $this->db->last_query();


	}



	function updatePayStatusSGO($trans_id) {
		$this->db = $this->load->database('db_ecommerce', true);
		$upd1 = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo
				 SET status_vt_pay = '1'
				 WHERE orderno = '$trans_id'";
		$exeUpd1 = $this->db->query($upd1);

		$upd2 = "UPDATE db_ecommerce.dbo.ecomm_trans_paydet_sgo
				 SET paystatus = 'ok'
				 WHERE orderno = '$trans_id'";
		$exeUpd2 = $this->db->query($upd2);

		if($exeUpd1 > 0 && $exeUpd2 > 0) {
			return true;
		} else {
			return false;
		}
	}

	function insertTrxTblEcomm($order_id, $orderno) {
		$this->db = $this->load->database('db_ecommerce', true);
		$datetrans = date("Y-m-d H:i:s");


		$secno = substr(md5(microtime()),rand(0,26),5);

		$qry = "SELECT * FROM ecomm_trans_hdr_sgo WHERE orderno = '$order_id'";
		$query = $this->db->query($qry);
		foreach($query->result() as $row) {
			/*$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno)
          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".getUserID()."','".$orderno."')";
			*/
//			$insCheckout = "insert into db_ecommerce.dbo.ecomm_checkout (idmember,idstk,total,token,datetrans,userlogin,orderno)
//          		        values('".$row->id_memb."','".$row->idstk."',".$row->total_pay.",'".$order_id."','".$datetrans."','".$row->userlogin."','".$orderno."')";
//			//echo "checout <br>".$insCheckout."<br><br>";
//			$queryCheckout = $this->db->query($insCheckout);

			//$gross_amount = $row->total_pay + $row->payShip + $row->payAdm;
			$gross_amount = $row->total_pay + $row->payShip + $row->payAdm + $row->payConnectivity;

			/*$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin)
                          values('".$row->id_memb."','".$datetrans."','".$orderno."','".$addr1."',".$row->total_pay.",".$gross_amount.",'1','".getUserID()."')";
            */
//			$insLogTrans = "insert into db_ecommerce.dbo.log_trans (log_dfno,log_date,log_trcd,log_sento,log_totaldp_sales,log_totaldp_pay,log_status_trx,log_usrlogin)
//          		        values('".$row->id_memb."','".$datetrans."','".$orderno."','',".$row->total_pay.",".$gross_amount.",'1','".$row->userlogin."')";
//			//echo "LogTrans <br>".$insLogTrans."<br><br>";
//			$queryLogTrans = $this->db->query($insLogTrans);

			$insPaydet = "insert into db_ecommerce.dbo.ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,notes,paystatus, bank_code_payment, charge_admin)
			              SELECT '".$orderno."', seqno,paytype,docno, payamt, 'trx ok','capture', bank_code_payment, charge_admin
						  FROM 	db_ecommerce.dbo.ecomm_trans_paydet_sgo a
						  WHERE a.orderno='".$order_id."'";
			//"values('".$orderno."','1','sgo','0',".$gross_amount.",'trx ok','capture')";
			//echo "Paydet <br>".$insPaydet."<br><br>";
			$queryPaydet = $this->db->query($insPaydet);

			//UPDATE VOUCHER BELANJA
			//Add by DION @ 09/08/2016
//			$updVch = "UPDATE a
//				SET a.claim_dt = GETDATE(), a.claim_by = '".$row->userlogin."', a.claim_status = 1
//				FROM ecomm_voucher_list a
//				INNER JOIN ecomm_trans_paydet_sgo b ON (a.voucherno = b.docno COLLATE SQL_Latin1_General_CP1_CS_AS)
//				WHERE b.orderno = '$order_id'";
//			$queryUpdVch = $this->db->query($updVch);


			//di set 1 saat live
			$st_vt_pay = "1";

			//tambahan dion 09/10/2015
			if($row->bank_code_payment == null || $row->bank_code_payment == "") {
				$charge_admin = 0;
				$charge_connectivity = 0;
				$bank_code_pay = 1;
			} else {
				$payADM = "SELECT * FROM ecomm_bank WHERE id = ".$row->bank_code_payment."";
				$querypayADM = $this->db->query($payADM);
				$payresadm = $querypayADM->result();
				$charge_admin = $payresadm[0]->charge_admin;
				$charge_connectivity = $payresadm[0]->charge_connectivity;
				$bank_code_pay = $row->bank_code_payment;
			}

			$is_ship = 0;
			if($row->sentTo == "2") {
				$is_ship = 1;
			}

			$cshbck = 0;
			if($row->cashback_point != null || $row->cashback_point != "") {
				$cshbck = $row->cashback_point;
			}

			$insHeader = "insert into db_ecommerce.dbo.ecomm_trans_hdr (orderno,bankaccno,token,id_memb,nmmember,total_pay,total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
							  secno,flag_trx,sentTo,status_vt_pay,status_vt_reject_dt,payShip,payAdm, is_ship, bank_code_payment, payConnectivity, is_login,
							  totPayCP,id_lp, free_shipping, discount_shipping, cashback_point)
			             values('".$orderno."','0','".$order_id."','".$row->id_memb."','".$row->nmmember."',$row->total_pay,$row->total_bv,'".$row->pricecode."','".$row->bonusmonth."','".$datetrans."','".$row->idstk."','".$row->nmstkk."',
			                    '".$secno."','W','".$row->sentTo."','$st_vt_pay','".$datetrans."',".$row->payShip.",".$charge_admin.", '".$is_ship."', ".$bank_code_pay.", ".$charge_connectivity.", '".$row->is_login."',
			                    ".$row->totPayCP.", '".$row->id_lp."', '".$row->free_shipping."', ".$row->discount_shipping.", ".$cshbck.");
			             INSERT INTO db_ecommerce.dbo.ecomm_trans_det_prd
				    		   (orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
						SELECT 	'".$orderno."', a.prdcd, a.prdnm, a.qty, a.bvr, a.dpr, a.pricecode, a.sentTo
						FROM 	ecomm_trans_det_prd_sgo a
						WHERE 	a.orderno='".$order_id."'";
			//echo "header<br>".$insHeader."<br><br>";
			$queryHeader = $this->db->query($insHeader);

			//update DION @28/11/2016
			if($row->is_login == "2" || $row->is_login == "3") {
				$qry = "SELECT A.orderno, SUM((b.cp * a.qty)) - SUM((b.dp * a.qty)) as hrg_selisih
						FROM ecomm_trans_det_prd a
						INNER JOIN master_prd_pricetab b ON (a.prdcd = b.cat_inv_id) AND b.pricecode = a.pricecode
						WHERE a.orderno = '$orderno' AND b.pricecode = a.pricecode
						GROUP BY a.orderno";
				//$hasilxxc = $this->getRecordset($qry, null, $this->db1);
				$hasilxxc = $this->db->query($qry);
				$hasilx = $hasilxxc->result();

				$updrs = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr SET totPayCP = ".$hasilx[0]->hrg_selisih." WHERE orderno = '$orderno'";
				$updrs_exe = $this->db->query($updrs);
				$sts = "UPDATE db_ecommerce.dbo.etiket_header SET status = '1' WHERE orderno = '$order_id'";
				$sts_exe = $this->db->query($sts);

			}
			//end update

			if( $queryPaydet > 0 &&
				$queryHeader > 0)
			{

				return 1;
			}
			else
			{
				return 0;
			}
		}

	}



	function duplicate($valid,$tiket){
		$qry = "SELECT * FROM trx_etiket WHERE valid_dfno='".$valid."' and id_etiket= '".$tiket."' and status = 0 ";
		//$hasilxxc = $this->getRecordset($qry, null, $this->db1);
		$hasilxxc = $this->db->query($qry);
		$hasilx = $hasilxxc->result();
		if($hasilx !=null){
			return true;
		}
		else return false;
	}

	function updateTrxSGO($order_id) {
		$res['jne'] = "";
		$return = false;
//		$personal_info = $this->session->userdata('personal_info');
		//$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
		//$trcd = $this->ecomm_trans_model->getsOrderno();
		$arr = $this->updatePayStatusSGO($order_id);
		//$updVch = $this->ecomm_trans_model->updateListVoucher($order_id, getUserID());
		if($arr) {
			$trcd = $this->my_counter->getCounter2('IDTA', $this->uuid->v4());
			$upd = $this->ecomm_trans_model->insertTrxTblEcomm($order_id, $trcd,$res['jne']);
			if($upd > 0) {
//				$this->generateCNoteJNE_baru($order_id, $trcd);
				$return = true;
			}
		}
		$result= $this->isSukses($order_id);
		if($result==0){
			$this->mdtc->hangus($order_id);
		}
		else
		{
			$this->mdtc->proses($order_id);


		}



		return $return;
	}

	function isSukses($XD)
	{
		$result = array();
		$this->db->from('ecomm_trans_hdr');
		$this->db->where('token',$XD);
		return $this->db->count_all_results();
	}

	function isMetodeTranfer($XD)
	{
		$result = array();
		$this->db->from('ecomm_trans_hdr_sgo');
		$this->db->where('orderno',$XD);
		$this->db->where('bank_code_payment','16');
		$this->db->where('datetrans>=dateadd(HOUR, 2, getdate())');

		return $this->db->count_all_results();
	}

	function checkID2($data,$acara) {
		$counter=0;

		$this->db->from('trx_etiket');
		$this->db->where('valid_dfno',$data);
		$this->db->where('id_etiket',$acara);
		$this->db->where('status','0');

		$counter= $this->db->count_all_results();

		if($counter==0){
			/*
			 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
			        FROM msmemb A LEFT OUTER JOIN tbonus B
			        	On A.dfno=B.distributorCode
			        where A.dfno= '$data'
			        	AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
			        Group by A.dfno, A.fullnm ";
			 * 
			 */

			$qry = "select aa.dfno, aa.fullnm, ISNULL(aa.rank, '-') AS rank, ISNULL(c.shortnm, '-') AS shortnm
					from (
					SELECT A.dfno, A.fullnm, 
						   (SELECT TOP 1 X.adjustedrank
					       	FROM tbonus X 
					        WHERE A.dfno=X.distributorcode 
					        ORDER BY X.bonusyear DESC, X.bonusmonth DESC) as rank
					FROM msmemb A 
					where A.dfno= '$data'
					    AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
					) aa left outer join msrank c on aa.rank=c.level";
			//echo $qry;
//			print_r($qry);
			$res = $this->getRecordset($qry, NULL, $this->db2);

			if($res == null) {
				//		throw new Exception("Data tidak ada..!!", 1);
				$res='Err';
			}
			return $res;
		}else

			return 'Err';



	}

	function hangus($XD)
	{
		$result = array();
		$this->db->from('trx_etiket');
		$this->db->where('order_no',$XD);
		$hitung= $this->db->count_all_results();



		$payADM = "SELECT * FROM trx_etiket WHERE order_no = '$XD' ";
		$querypayADM = $this->db->query($payADM);
		$payresadm = $querypayADM->result();
		$idx = $payresadm[0]->id_etiket;


		$updrs = "UPDATE db_ecommerce.dbo.master_etiket SET max_online = max_online+".$hitung." WHERE id = '$idx'";
		$updrs_exe = $this->db->query($updrs);
		$sts = "UPDATE db_ecommerce.dbo.etiket_header SET status='2' WHERE orderno = '$XD'";
		$sts_exe = $this->db->query($sts);

	}



	function proses($XD)
	{

		$updrs = "UPDATE db_ecommerce.dbo.trx_etiket SET status = 0 WHERE order_no = '$XD'";
		$updrs_exe = $this->db->query($updrs);
		$sts = "UPDATE db_ecommerce.dbo.etiket_header SET status='1' WHERE orderno = '$XD'";
		$sts_exe = $this->db->query($sts);

		$this->sms2($XD);

	}

	function sms2($id){



		$query="SELECT a.valid_fullnm, b.nama, b.lokasi, a.notiket, a.notelp, a.email
 		FROM db_ecommerce.dbo.trx_etiket A
		LEFT JOIN db_ecommerce.dbo.master_etiket b
		on a.id_etiket=b.id
		WHERE order_no = '$id'";
		$qrysms = $this->db->query($query);

		foreach($qrysms->result() as $row) {
			$alternate2 = $this->load->database('alternate2', true);

			$text = 'Selamat, a/n '.$row->valid_fullnm.' terdaftar dalam : '.$row->nama.'-'.$row->lokasi.'. Nomor e-tiket anda: '.$row->notiket;

//			$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
//		                    VALUES ('".$row->notelp."','".$text."','K-Link Transaction Status', 'MyPhone6')";
//			//echo "Ins SMS : $insSms";
//			$query = $alternate2->query($insSms);

			$send = smsTemplate($row->notelp, $text);



			$link="www.k-net.co.id/vtiket2/".$row->notiket;
			$content = 'Selamat, a/n '.$row->valid_fullnm.' telah terdaftar dalam : '.$row->nama.'-'.$row->lokasi.'. Nomor e-tiket anda: '.$row->lokasi.' atau klik '.$link;

			emailSend('DTC', $row->email, $content);

		}





	}



	function TabelTiketBuyer($dfno) {
		$qry = "
		SELECT a.dfno, a.fullnm, a.notiket, a.valid_dfno,a.valid_fullnm, a.createdt, b.id, b.nama, b.lokasi,b.event_date,a.email
		FROM trx_etiket a
		LEFT JOIN master_etiket b
		ON a.id_etiket=b.id
		WHERE a.dfno='$dfno' AND a.status=0;
		";
		$res = $this->getRecordset($qry, NULL, $this->db1);

		return $res;
	}
	function TabelTiketValid($dfno) {
		$qry = "
		SELECT a.dfno, a.fullnm, a.notiket, a.valid_dfno,a.valid_fullnm, a.createdt, b.id, b.nama, b.lokasi,b.event_date,a.email
		FROM trx_etiket a
		LEFT JOIN master_etiket b
		ON a.id_etiket=b.id
		WHERE a.valid_dfno='$dfno' AND a.status=0;
		";
		$res = $this->getRecordset($qry, NULL, $this->db1);
//		echo $qry;

		return $res;
	}
	function getPemenang($XD)
	{
		$result = array();
		$this->db->select('*')
			->from('undian')
			->where('FLAG',$XD)
		;

		$q = $this->db->get();

		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}


	function getPemenang20($XD,$mulai, $habis)
	{
		$result = array();
//		$this->db->select('*')
//			->from('undian')
//			->where('FLAG',$XD)
//			->limit($habis,$mulai)
//		;
//

//		$this->db->query("SELECT TOP $mulai * FROM (SELECT TOP $habis FROM undian ORDER BY id) ORDER BY id DESC");
//		$q = $this->db->get();
//		echo $this->db->last_query();
//		echo '<br>';
//		foreach($q->result() as $row)
//		{
//			$result[] = $row;
//		}
		$qry="SELECT * FROM (SELECT TOP $mulai * FROM (SELECT TOP $habis * FROM undian where FLAG = $XD ORDER BY NOUNDIAN ) y ORDER BY NOUNDIAN DESC) x ORDER BY NOUNDIAN ASC";

		$result = $this->getRecordset($qry, NULL, $this->db1);


//		echo $this->db->last_query()."; ";

		return $result;
	}

	function getPemenangSemua()
	{
		$result = array();
		$this->db->select('*')
			->from('undian a')
			->join('hadiah b','a.FLAG=b.nohadiah','left')

		;

		$q = $this->db->get();

		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}











	function showDataPrint($id){
		$slc = "select * from trx_etiket a
		LEFT JOIN master_etiket b
		on a.id_etiket=b.id

 		where notiket = '".$id."'";
		$result = $this->getRecordset($slc, null, $this->db1);
		return $result;
	}

	function sendTrxSMS($name, $dtc,$etiket,$link, $nope) {
		$alternate2 = $this->load->database('alternate2', true);
		$text = 'Selamat, a/n '.$name.' terdaftar dalam : '.$dtc.'. Nomor e-tiket anda: '.$etiket;
		$insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
		                    VALUES ('".$nope."','".$text."','K-Link Transaction Status', 'MyPhone6')";
		//echo "Ins SMS : $insSms";
		$query = $alternate2->query($insSms);
	}



}
?>
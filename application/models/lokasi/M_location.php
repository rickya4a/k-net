<?php
class m_location extends MY_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }



    function emptyOTP($loccd)
    {
        //hapus OTP sebelumnya
        $qry1 = "DELETE from otp_location where loccd ='$loccd'";
        return $this->executeQuery($qry1, $this->db2);
    }

    function insertOTP($loccd, $otp,$telp) {
        //insert OTP yang baru
        $qry2 = "INSERT INTO otp_location (loccd,otp,created,is_expire,hp) VALUES ('$loccd', '$otp','".$this->dateTime."','0','$telp')";
        return $this->executeQuery($qry2,$this->db2);
    }

    function insertBooking($metode,$des_name,$des_phone,$des_add,$des_X,$des_Y,$item,$price) {
        //insert Booking
        $qry2 = "INSERT INTO vera_bookgsend (orderno,metode,penerima,telp,X,Y,alamat,item,price,created,creator)
                 VALUES ('','$metode','$des_name','$des_phone','$des_add','$des_X','$des_Y','$item','$price','".$this->dateTime."','')";
        return $this->executeQuery($qry2,$this->db2);
    }

    function getSelect($value)
    {
        $qry = "select top 1 * from otp_location where loccd='$value' order by created DESC";
        return $this->getRecordset($qry, null, $this->db2);
    }



    function updateOTP($id) {
        $qry = "UPDATE otp_location SET is_expire = '1' WHERE loccd= '$id'";
        return $this->executeQuery($qry, $this->db2);
    }

    function insertCoord($lat,$long,$loccd) {
        //get nomor hp stokist
        $qry = "select * from mssc where loccd='$loccd'";
        $dt= $this->getRecordset($qry, null, $this->db2);
        $sctype= $dt[0]->sctype;
        $fullnm= $dt[0]->fullnm;
        $dfno= $dt[0]->dfno;
        //$addr= $dt[0]->addr1." ".$dt[0]->addr2." ".$dt[0]->addr3." ".$dt[0]->city;
        $addr1= $dt[0]->addr1;
        $addr2= $dt[0]->addr2;
        $addr3= $dt[0]->addr3;
        $city= $dt[0]->city;
        $state= $dt[0]->state;
        $postcd= $dt[0]->postcd;
        $country= $dt[0]->country;
        $tel_hm= $dt[0]->tel_hm;
        $tel_of= $dt[0]->tel_of;
        $tel_hp= $dt[0]->tel_hp;
        $fax= $dt[0]->fax;
        $email= $dt[0]->email;
        $branch= $dt[0]->branch;
        $whcd= $dt[0]->whcd;
        $is_warehouse= $dt[0]->is_warehouse;


        //insert OTP yang baru
        //$qry2 = "INSERT INTO vera_location (loccd,fullnm,sctype,alamat,X,Y,state) VALUES ('$loccd','$fullnm','$sctype','$addr','$lat','$long','$state')";
        $qry2 = "INSERT INTO master_warehouse_list
            ([whcd]
           ,[whnm]
           ,[loccd]
           ,[fullnm]
           ,[addr1]
           ,[addr2]
           ,[addr3]
           ,[city]
           ,[tel_hp]
           ,[tel_of]
           ,[latitude]
           ,[longitude]
           ,[take_order_allowed]
           ,[is_warehouse]
           ,[is_stockist]
           ,[WH_HEAD]
           ,[WH_AREA]
           ,[is_gosend_active]
           ,[is_grab_active]
           ,[hp_to_confirm]
           ,[state]
           ,[province]
           ,[kab]
           ,[kec]
           ,[bus_open]
           ,[bus_close])
            VALUES ('$whcd','','$loccd','$fullnm','$addr1','$addr2','$addr3','$city','$tel_hp','$tel_of','$lat','$long','','$is_warehouse',
            '','','','','','','$state','','','','','')";
        /* $qry2 = "INSERT INTO master_warehouse_list
             (whcd
            ,loccd
            ,fullnm
            ,addr1
            ,addr2
            ,addr3
            ,city
            ,tel_hp
            ,tel_of
            ,latitude
            ,longitude
            ,hp_to_confirm)
             VALUES ('$whcd','$loccd','$fullnm','$addr1','$addr2','$addr3','$city','$tel_hp','$tel_of','$lat','$long','$tel_hp')";*/

        //print_r($qry2);
        return $this->executeQuery($qry2,$this->db1);

    }

    function updateCoord($lat,$long,$loccd) {
        //$qry = "UPDATE vera_location SET X= '$lat', Y='$long' WHERE loccd= '$loccd'";
        $qry = "UPDATE mssc SET latitude='$lat', longitude='$long' WHERE loccd= '$loccd'";
        return $this->executeQuery($qry, $this->db2);
    }

    function updateCoord2($lat,$long,$loccd) {
        //$qry = "UPDATE vera_location SET X= '$lat', Y='$long' WHERE loccd= '$loccd'";
        $qry = "UPDATE master_warehouse_list SET latitude='$lat', longitude='$long' WHERE loccd= '$loccd'";
        return $this->executeQuery($qry, $this->db1);
    }

    function updateAddress1($loccd,$address) {
        $addr1= substr($address,0,100);
        $addr2= substr($address,101,100);
        $addr3= substr($address,201,100);

        $addr1= str_replace(" ","",$addr1);
        $addr2= str_replace(" ","",$addr2);
        $addr3= str_replace(" ","",$addr3);

        $addr1= str_replace("%20"," ",$addr1);
        $addr2= str_replace("%20"," ",$addr2);
        $addr3= str_replace("%20"," ",$addr3);



        $qry = "UPDATE mssc SET addr1='$addr1',addr2='$addr2',addr3='$addr3' WHERE loccd= '$loccd'";
        return $this->executeQuery($qry, $this->db2);
    }

    function updateAddress2($loccd,$address) {
        $addr1= substr($address,0,100);
        $addr2= substr($address,101,100);
        $addr3= substr($address,201,100);

        $addr1= str_replace(" ","",$addr1);
        $addr2= str_replace(" ","",$addr2);
        $addr3= str_replace(" ","",$addr3);

        $addr1= str_replace("%20"," ",$addr1);
        $addr2= str_replace("%20"," ",$addr2);
        $addr3= str_replace("%20"," ",$addr3);


        $qry = "UPDATE master_warehouse_list SET addr1='$addr1',addr2='$addr2',addr3='$addr3' WHERE loccd= '$loccd'";
        return $this->executeQuery($qry, $this->db1);
    }

    function getInfo($value)
    {
        $qry = "select * from mssc where loccd='$value'";
        return $this->getRecordset($qry, null, $this->db2);
    }

    function cekLokasi($value)
    {
        $qry = "select count(*) as qty from master_warehouse_list where loccd='$value'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function insertTracking($orderno,$no_ship,$status_ship,$desc,$pickup_time,$delivery_time,$shipper_id) {
        //insert Tracking
        $qry2 = "INSERT INTO ecomm_tracking (orderno,no_shipping,status_shipping,description,pickup_eta,delivery_eta,shipper_id,created)
                 VALUES ('$orderno','$no_ship','$status_ship','$desc','$pickup_time','$delivery_time','$shipper_id','".$this->dateTime."')";
        return $this->executeQuery($qry2,$this->db1);
    }

    function insertTrackingWebhook($orderno,$entity_id,$type,$status,$cancel,$booking_type,$driver_name,$driver_phone,$receiver_name,$total_km,$pickup_time,$delivery_time,$price,$cancel_reason,$live_tracking,$shipper_id) {
        //insert Tracking
        $qry2 = "INSERT INTO ecomm_tracking_webhook (orderno,entity_id,status,type_status,cancelled_by,booking_type,driver_name,driver_phone,receiver_name,total_km,pickup_eta,delivery_eta,price,cancellation_reason,live_tracking,shipper_id,created)
                 VALUES ('$orderno','$entity_id','$status','$type','$cancel','$booking_type','$driver_name','$driver_phone','$receiver_name','$total_km','$pickup_time','$delivery_time','$price','$cancel_reason','$live_tracking','$shipper_id','".$this->dateTime."')";
        return $this->executeQuery($qry2,$this->db1);
    }

    function insertTrackingApi($orderno,$no_ship,$status_ship,$desc,$shipper_id,$create_time,$dispatch_time,$arrival_time,$closed_time) {
        //insert Tracking
        $qry2 = "INSERT INTO ecomm_tracking_api (orderno,no_shipping,status_shipping,description,shipper_id,created,CreatedTime,DispatchTime,ArrivalTime,ClosedTime)
                 VALUES ('$orderno','$no_ship','$status_ship','$desc','$shipper_id','".$this->dateTime."','$create_time','$dispatch_time','$arrival_time','$closed_time')";
        return $this->executeQuery($qry2,$this->db1);
    }

    function getOrderNo($value)
    {
        $qry = "select * from ecomm_trans_shipaddr where conoteJNE='$value'";
        $result= $this->getRecordset($qry, null, $this->db1);
        return $result;
    }

    function getAuthDev()
    {
        $qry = "select * from ecomm_tracking_auth";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getAuth()
    {
        $qry = "select * from ecomm_webhook_auth where desc_text LIKE 'token_payment%'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getSchedule($day,$metode)
    {
        $qry = "select * from master_schedule_gosend where nama_hari='$day' AND metode='$metode'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getCargo($ship)
    {
        $qry = "select * from master_logistic_shipper where shipper_id='$ship'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getUserkey()
    {
        $qry = "select * from master_gosend_key where status_key=1";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getUserkeyDev()
    {
        $qry = "select * from master_gosend_key where status_key=0";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getLibur($value)
    {
        $qry = "select count(tanggal) as holiday from master_libur_nasional where tanggal='$value'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getSchedule2($day,$metode,$stk)
    {
        $kolom1=$day."_open";
        $kolom2=$day."_closed";
        $kolom3=$day."_pay_closed";

        $qry = "select $kolom1 As opened, $kolom3 As closed_pay from master_schedule_gosend2 where loccd='$stk' AND metode='$metode'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getstok_avail($table,$val1,$val2,$val3)
    {
        $qry = "select count(prdcd) as jumlah, prddesc
                from $table where whcd='$val1' AND loccd='$val2'AND prdcd='$val3' group by prddesc";
        return $this->getRecordset($qry, null, $this->db1);

        /* $q = $this->db1->query("select count(prdcd) as jumlah, prddesc
                 from $table where whcd='$val1' AND loccd='$val2'AND prdcd='$val3' group by prddesc");
         return $q;*/

    }

    function getRek($id)
    {
        $qry = "select * from ecomm_rek_bank where id_bank='$id'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function cekTrans($kdpay,$nominal)
    {
        $qry = "select * from ecomm_trans_hdr_sgo where kode_pay='$kdpay' AND total_pay='$nominal' AND (flag_payment is null OR flag_payment = '')";
        $result= $this->getRecordset($qry, null, $this->db1);
        return $result;
    }

    function cekTrans1($kdpay,$nom)
    {
        //$qry = "select TOP 1 * from ecomm_trans_hdr_sgo where kode_pay='$kdpay' AND nom_pay='$nom' AND date_expired > '$jam_bayar' AND (flag_payment is null OR flag_payment = '') order by id DESC";
        $qry = "select TOP 1 * from ecomm_trans_hdr_sgo where kode_pay='$kdpay' AND nom_pay='$nom' AND (flag_payment is null OR flag_payment = '') order by id DESC";
        $result= $this->getRecordset($qry, null, $this->db1);
        return $result;
    }

    function cekTrans2($kdpay,$nom)
    {
        $qry = "select TOP 1 * from ecomm_trans_hdr_sgo where kode_pay='$kdpay' AND nom_pay='$nom' AND flag_payment = '1' order by id DESC";
        $result= $this->getRecordset($qry, null, $this->db1);
        return $result;
    }

    function cekRTrans1($kdpay,$nom,$jam_bayar)
    {
        $qry = "select TOP 1 * from ecomm_trans_hdr_sgo where kode_pay='$kdpay' AND nom_pay='$nom' AND date_expired > '$jam_bayar' AND (flag_production is null OR flag_production ='') order by id DESC";
        $result= $this->getRecordset($qry, null, $this->db1);
        return $result;
    }

    function cekRTrans2($kdpay,$nom)
    {
        $qry = "select TOP 1 * from ecomm_trans_hdr_sgo where kode_pay='$kdpay' AND  nom_pay='$nom' AND (flag_production is null OR flag_production ='') order by id DESC";
        $result= $this->getRecordset($qry, null, $this->db1);
        return $result;
    }

    public function kd_pay(){
        /*$this->db->select('kd_pay');
        $this->db->from('mutasi_payment');
        $this->db->order_by('rec_id','DESC');
        $query = $this->db10->get();

        $row = $query->row();
        $last_kd_pay = isset($row->kd_pay) ? $row->kd_pay : '0';
        $new_kd_pay = $last_kd_pay + 1;
        return $new_kd_pay;*/

        $qry = "select kd_pay from mutasi_payment order by kd_pay DESC LIMIT 1";
        $result= $this->getRecordset($qry, null, $this->db10);
        return $result;
    }

    public function kd_ref(){
        /*$this->db->select('kd_pay');
        $this->db->from('mutasi_payment');
        $this->db->order_by('rec_id','DESC');
        $query = $this->db10->get();

        $row = $query->row();
        $last_kd_pay = isset($row->kd_pay) ? $row->kd_pay : '0';
        $new_kd_pay = $last_kd_pay + 1;
        return sprintf("%06s",$new_kd_pay);*/
        $qry = "select kd_pay from mutasi_payment order by kd_pay DESC LIMIT 1";
        $result= $this->getRecordset($qry, null, $this->db10);
        return $result;


    }

    public function save_payment($kd_pay, $kd_ref, $nominal, $no_rek_tujuan, $nama_rek_tujuan, $type, $total_bayar, $kode_unik){
        //$kd_pay, $kd_ref, $nominal, $no_rek_tujuan, $nama_rek_tujuan, $type, $total_bayar, $kode_unik
        /* $data = array(
             'rec_id' => '',
             'kd_pay'=> $kd_pay,
             'kd_ref'=> $kd_ref,
             'nominal' => $nominal,
             'no_rek_tujuan' => $no_rek_tujuan,
             'nama_rek_tujuan' => $nama_rek_tujuan,
             'type' => $type,
             'total_bayar' => $total_bayar,
             'kode_unik' => $kode_unik,
             'datetime' => date("Y-m-d H:i:s")
         );

         return $this->db10->insert('mutasi_payment', $data);*/

        $today=date("Y-m-d H:i:s");
        $qry2 = "INSERT INTO mutasi_payment (rec_id,kd_pay,kd_ref,nominal,no_rek_tujuan,nama_rek_tujuan,type,total_bayar,kode_unik,datetime)
                  VALUES ('','$kd_pay','$kd_ref','$nominal','$no_rek_tujuan','$nama_rek_tujuan','$type','$total_bayar','$kode_unik','$today')";
        return $this->executeQuery($qry2,$this->db10);
    }

    public function save_log1($nominal,$norek,$nmpemilik,$type,$response,$payship){
        $today=date("Y-m-d H:i:s");
        $source="www.k-net.co.id";
        $destination= "https://www.k-net.co.id/getUnique";
        $arr = array("nominal" =>$nominal,"payShip" =>$payship,"no_rek_tujuan" => $norek,"nama_rek_tujuan" => $nmpemilik,"type" => $type);
        $data=json_encode($arr);

        $type="MySQL - Insert Data Payment K-Net";

        $qry2 = "INSERT INTO log_access(rec_id,type,data,source,destination,response,datetime)
                  VALUES ('','$type','$data','$source','$destination','$response','$today')";
        return $this->executeQuery($qry2,$this->db10);
    }

    public function save_log2($kode_unik,$total_bayar,$kd_pay,$nominal,$kd_ref,$response,$payship){
        $today=date("Y-m-d H:i:s");
        $source="www.k-net.co.id";
        $destination= "https://www.k-net.co.id/getUnique";
        $arr = array("koneksi" =>$response,"payShip" =>$payship,"kode_unik" => $kode_unik,"total_bayar" => $total_bayar,"kd_pay" => $kd_pay,"total_belanja" => $nominal,"kd_ref" =>$kd_ref);
        $data=json_encode($arr);

        $type="MySQL - Insert Data Total Bayar";

        $qry2 = "INSERT INTO log_access(rec_id,type,data,source,destination,response,datetime)
                  VALUES ('','$type','$data','$source','$destination','$response','$today')";
        return $this->executeQuery($qry2,$this->db10);
    }

    function updateFlag($kdpay,$nominal,$id) {
        $qry = "UPDATE ecomm_trans_hdr_sgo SET flag_payment='1' WHERE kode_pay='$kdpay' AND nom_pay='$nominal' AND orderno= '$id'";
        //echo $qry;
        return $this->executeQuery($qry, $this->db1);
    }

    function updateFlag2($kdpay,$id) {
        $qry = "UPDATE ecomm_trans_hdr_sgo SET flag_production='1' WHERE kode_pay='$kdpay' AND orderno= '$id'";
        return $this->executeQuery($qry, $this->db1);
    }

    function updateNewTrh($kodeunik,$id) {
        $qry = "select * from ecomm_trans_hdr where token='$id'";
        $dt= $this->getRecordset($qry, null, $this->db1);
        $orderno= $dt[0]->orderno;

        //echo $orderno;

        $qry = "UPDATE newtrh SET pay3amt='$kodeunik',paynote3='Kode Unik Pay' WHERE trcd= '$orderno'";
        return $this->executeQuery($qry, $this->db2);
    }


    function getConoteG($orderno)
    {
        $qry = "select conoteJNE as conote from ecomm_trans_shipaddr where orderno='$orderno'";
        return $this->getRecordset($qry, null, $this->db1);
    }

    function getWH($tabel)
    {
        $qry = "select * from $tabel order by loccd ASC";
        return $this->getRecordset($qry, null, $this->db1);
    }
}

?>
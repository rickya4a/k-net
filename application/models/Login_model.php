<?php
class Login_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	function showListPromoAgustus2018() {
		$tgl = date("Y-m-d");

		$qry = "SELECT *
				FROM promo_period a WHERE a.id = 'AGUST2018'
				AND a.period_start <= '$tgl' AND a.period_end >= '$tgl' and status = '1'";
		//echo $qry;
        $res = $this->getRecordset($qry, NULL, $this->db1);
        /*$query = $this->db->query($qry);
        $res = array();
		foreach ($query->result() as $row) {
			array_push($res, $row);
		}*/
		return $res;
	}

	function showListPromoNovember2018() {
		$tgl = date("Y-m-d");

		$qry = "SELECT *
				FROM promo_period a WHERE a.id = 'NOV2018'
				AND a.period_start <= '$tgl' AND a.period_end >= '$tgl' and status = '1'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		
		return $res;
	}
	
	function checkAuthLogin($data) {	
		$qry = "SELECT * FROM bbuser WHERE USER_NAME = '$data[username]' 
				AND USER_PWD = '$data[password]'";
		$res = $this->getRecordset($this->db1, $qry);
		if($res == null) {
			throw new Exception("Login Gagal, check username dan password anda..!!", 1);
		}
		return $res;
	}	
	
	function fetchingMenu($user_config) {
		//echo $this->db1;	
		$this->db = $this->load->database($this->db1, true);	
		$qry = "SELECT id, parent_id, name, url_laravel FROM menu WHERE ID IN ($user_config) ORDER BY id,parent_id";
		$query = $this->db->query($qry);
		$data = array();
		if($query->num_rows() > 0) {
			
			foreach($query->result() as $dta) {
				$data[$dta->parent_id][] = $dta;
			}
			//$res = $this->loopMenu($data,0);
			//$res .= '<ul parent="0" style="display:block"><li><a href="'.base_url('logout').'"><img src="folderclose2.jpg" border="0">Sign Out</a></li></ul>'; 
		} else {
			throw new Exception("Data menu gagal di generate..!!", 1);
		}
		return $data;
	}
	
	
	
}
	
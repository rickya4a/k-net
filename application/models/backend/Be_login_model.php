<?php
class Be_login_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	function checkAuthLogin($data) {	
		$qry = "SELECT a.username, a.password, a.groupid, b.groupname 
		        FROM ecomm_user a LEFT JOIN ecomm_usergroup b
		        ON(a.groupid = b.groupid)
		        WHERE username = '$data[username]' 
				AND password = '$data[password]'";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Login Gagal, check username dan password anda..!!", 1);
		}
		return $res;
	}	
	
	function fetchingMenu($usertype) {
		
			$qry = "SELECT 
					  dbo.ecomm_userauthority.menuid as menu_id,
					  dbo.app_tabprg.app_menu_parent_id as parent_id,
					  dbo.app_tabprg.app_menu_desc as menu_desc,
					  dbo.app_tabprg.app_menu_url as menu_url
					  
					FROM
					  dbo.ecomm_userauthority
					  INNER JOIN dbo.app_tabprg ON (dbo.ecomm_userauthority.menuid = dbo.app_tabprg.app_menu_id)
					WHERE
					  (dbo.ecomm_userauthority.groupid = '$usertype')
					ORDER BY dbo.app_tabprg.app_menu_parent_id, dbo.app_tabprg.menu_order";
        //echo $qry;   
        $query = mssql_query($qry);
        $menu = array('items' => array(), 'parents' => array());

        
     // Builds the array lists with data from the menu table
        while ($items = mssql_fetch_assoc($query))
        {
            // Creates entry into items array with current menu item id ie. $menu['items'][1]
            $menu['items'][$items['menu_id']] = $items;
            // Creates entry into parents array. Parents array contains a list of all items with children
            $menu['parents'][$items['parent_id']][] = $items['menu_id'];
        }
        return $menu;
	}
	
	function fetchingMenu2($usertype) {
		
			$qry = "SELECT 
					  dbo.ecomm_userauthority.menuid as menu_id,
					  dbo.app_tabprg.app_menu_parent_id as parent_id,
					  dbo.app_tabprg.app_menu_desc as menu_desc,
					  dbo.app_tabprg.app_menu_url as menu_url,
					  dbo.ecomm_userauthority.toggle_add as toogle_add,
					  dbo.ecomm_userauthority.toggle_edit as toogle_edit,
					  dbo.ecomm_userauthority.toggle_view as toogle_view,
					  dbo.ecomm_userauthority.toggle_delete as toogle_delete
					FROM
					  dbo.ecomm_userauthority
					  INNER JOIN dbo.app_tabprg ON (dbo.ecomm_userauthority.menuid = dbo.app_tabprg.app_menu_id)
					WHERE
					  (dbo.ecomm_userauthority.groupid = '$usertype')
					ORDER BY dbo.app_tabprg.app_menu_parent_id, dbo.app_tabprg.menu_order";
        //echo $qry;   
        $query = mssql_query($qry);
        $menu = array('items' => array(), 'parents' => array());

        
     // Builds the array lists with data from the menu table
        while ($items = mssql_fetch_assoc($query))
        {
            // Creates entry into items array with current menu item id ie. $menu['items'][1]
            $menu['items'][$items['menu_id']] = $items;
            // Creates entry into parents array. Parents array contains a list of all items with children
            $menu['parents'][$items['parent_id']][] = $items['menu_id'];
        }
        return $menu;
	}
	
	function fetchingAllMenu() {
		
			$qry = "SELECT 
					  dbo.app_tabprg.app_menu_id as menu_id,
					  dbo.app_tabprg.app_menu_parent_id as parent_id,
					  dbo.app_tabprg.app_menu_desc as menu_desc,
					  dbo.app_tabprg.app_menu_url as menu_url
					FROM
					  dbo.app_tabprg 
					ORDER BY dbo.app_tabprg.app_menu_parent_id";
        //echo $qry;   
        $query = mssql_query($qry);
        $menu = array('items' => array(), 'parents' => array());

        
     // Builds the array lists with data from the menu table
        while ($items = mssql_fetch_assoc($query))
        {
            // Creates entry into items array with current menu item id ie. $menu['items'][1]
            $menu['items'][$items['menu_id']] = $items;
            // Creates entry into parents array. Parents array contains a list of all items with children
            $menu['parents'][$items['parent_id']][] = $items['menu_id'];
        }
        return $menu;
	}
	
	public function getListUserByRoleID($role, $menu_id, $type = "array") {
        $qry = "SELECT groupid as role_id, 
                       menuid as menu_id,
                       toggle_add,
					   toggle_edit,
					   toggle_delete,
					   toggle_view
                FROM ecomm_userauthority 
                WHERE groupid = '".$role."' AND menuid = '".$menu_id."'";
        //echo $qry;
        return $this->getRecordset($qry, NULL, $this->db1);
    }
	
}
	
<?php

    class Tracking_api_model extends MY_Model{

        public function __construct(){

            parent::__construct();
        }

        public function getAllTracking(){

            $qry = "SELECT * FROM tb_tracking_hdr";

            $res = $this->getRecordset($qry, null, $this->db7);

            if($res == null) {

                throw new Exception("Record doesn't exist in table", 1);
            }
            return $res;
        }

        public function getDataTracking($dfno){

            $qry = "SELECT * FROM tb_tracking_hdr
                    where dfno = '$dfno'";

            $res = $this->getRecordset($qry, null, $this->db7);

            if($res == null) {

                throw new Exception("Record with ID $dfno doesn't exist in table", 1);
            }
            return $res;
        }
    }
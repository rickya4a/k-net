<?php
class Be_member_model extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();

	}

	function checkTokenNew($notoken) {
		$qry = "SELECT a.idno, a.dfno, a.fullnm, b.trx_no as temp_trx_id, c.memberid,
				c.trx_no, d.orderno, d.token, b.idno as temp_idno
				FROM klink_mlm2010.dbo.msmemb a
				LEFT OUTER JOIN ecomm_memb_ok_sgo b
					ON (b.idno is not null AND b.idno != ''
						AND a.idno COLLATE SQL_Latin1_General_CP1_CI_AS = b.idno)
				LEFT OUTER JOIN ecomm_memb_ok c
					ON (c.idno COLLATE SQL_Latin1_General_CP1_CI_AS = a.idno)
				LEFT OUTER JOIN ecomm_trans_hdr d
					ON (c.trx_no COLLATE SQL_Latin1_General_CP1_CI_AS = d.orderno)
				WHERE b.trx_no = '$notoken'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		if($result != null) {
			//cek apakah token dengan no ktp tsb sama, jika iya berarti ada 2 transaksi
			//dengan no ktp / idmember yg sama
		    //seharus nya member boleh mengganti data membership
			$arr = null;
			if($result[0]->temp_trx_id != $result[0]->token && $result[0]->dfno == $result[0]->memberid &&
			$result[0]->idno == $result[0]->temp_idno) {
				$hasil = $this->checkNoToken_sgo($notoken);
				$arr = array(
					"response" => "blh_ganti",
					"arrayData" => $hasil,
					"message" => "KTP sudah terdaftar dengan ".$result[0]->dfno." / ".$result[0]->fullnm
				);
			} else if($result[0]->temp_trx_id == $result[0]->token && $result[0]->dfno == $result[0]->memberid &&
			$result[0]->idno == $result[0]->temp_idno) {
				$arr = array(
					"response" => "tak_boleh_ganti",
					"arrayData" => null,
					"message" => "KTP sudah terdaftar dengan ".$result[0]->dfno." / ".$result[0]->fullnm.", No Trx : ".$result[0]->orderno
				);
			}
		} else {
				$hasil = $this->checkNoToken_sgo($notoken);
				$arr = array(
					"response" => "blh_ganti",
					"arrayData" => $hasil,
					"message" => "Ok"
				);
		}

		return $arr;

	}

	function checkFromMsmemb($param, $value) {
		$qry = "SELECT a.dfno, a.fullnm, a.idno, a.sponsorid, a.jointdt,
					a.sfno_reg, a.loccd, b.trx_no, b.flag_voucher, b.voucher_no,
				CASE
				   WHEN c.token = d.trx_no collate SQL_Latin1_General_CP1_CI_AS
						AND a.idno = d.idno collate SQL_Latin1_General_CP1_CI_AS
						and b.flag_voucher = '0' THEN '0'
				   WHEN c.token = d.trx_no collate SQL_Latin1_General_CP1_CI_AS
						AND a.idno = d.idno collate SQL_Latin1_General_CP1_CI_AS
						and b.flag_voucher = '1' THEN '1'
				   WHEN c.token is null OR c.token = '' THEN '2'
				END AS stat_reg
				FROM klink_mlm2010.dbo.msmemb a
				LEFT OUTER JOIN db_ecommerce.dbo.ecomm_memb_ok b
					ON (a.dfno collate SQL_Latin1_General_CP1_CI_AS = b.memberid)
				LEFT OUTER JOIN ecomm_trans_hdr c
					ON (b.trx_no collate SQL_Latin1_General_CP1_CI_AS = c.orderno)
				LEFT OUTER JOIN ecomm_memb_ok_sgo d
					ON (c.token collate SQL_Latin1_General_CP1_CI_AS = d.trx_no )
				WHERE $param = '$value'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}

	function checkEcommMembOk($param, $value) {
		$qry = "SELECT * FROM db_ecommerce.dbo.ecomm_memb_ok a WHERE $param = '$value'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}

	function checkEcommMembOkSgo($param, $value) {
		$qry = "SELECT idno FROM db_ecommerce.dbo.ecomm_memb_ok_sgo a WHERE $param = '$value'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}

	function checkNoToken($token) {
		$qry = "SELECT a.orderno, a.token, a.total_pay,
					a.nmmember, a.id_memb, b.state,
					CONVERT(VARCHAR(10), a.datetrans, 20) as tgljoin,
					a.idstk, b.fullnm as namastk, a.no_hp_konfirmasi,
					d.memberid as generate_idmemb, d.membername as generate_nmmemb,
					c.memberid as temp_idmemb, c.membername as temp_nmmemb,
					c.sponsorid, c1.fullnm as sponsorname,
					c.recruiterid, c2.fullnm as recruitername,
					c.idno, c.addr1, c.tel_hp, c.email,
					c.stk_code, b1.fullnm as stk_name,c.sex,
					RIGHT('0' + CAST(DAY(c.birthdt) as varchar(2)), 2) as tgllhr,
					RIGHT('0' + CAST(MONTH(c.birthdt) as varchar(2)), 2) as blnlhr,
					YEAR(c.birthdt) as thnlhr, c.bankid, c.acc_name
				FROM ecomm_trans_hdr a
				LEFT OUTER JOIN klink_mlm2010.dbo.mssc b ON (a.idstk COLLATE SQL_Latin1_General_CP1_CS_AS = b.loccd)
				LEFT OUTER JOIN ecomm_memb_ok_sgo c ON (a.token = c.trx_no)
				LEFT OUTER JOIN klink_mlm2010.dbo.mssc b1 ON (c.stk_code COLLATE SQL_Latin1_General_CP1_CS_AS = b1.loccd)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb c1 ON (c.sponsorid collate SQL_Latin1_General_CP1_CS_AS = c1.dfno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb c2 ON (c.recruiterid collate SQL_Latin1_General_CP1_CS_AS = c2.dfno)
				LEFT OUTER JOIN ecomm_memb_ok d ON (a.orderno = d.trx_no)
				WHERE a.token = '$token'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}

	function checkNoToken_sgo($token) {
		$qry = "SELECT a.orderno, a.token, a.total_pay,
					a.nmmember, a.id_memb, b.state,
					CONVERT(VARCHAR(10), a.datetrans, 20) as tgljoin,
					a.idstk, b.fullnm as namastk, a.no_hp_konfirmasi,
					d.memberid as generate_idmemb, d.membername as generate_nmmemb,
					c.memberid as temp_idmemb, c.membername as temp_nmmemb,
					c.sponsorid, c1.fullnm as sponsorname,
					c.recruiterid, c2.fullnm as recruitername,
					c.idno, c.addr1, c.tel_hp, c.email,
					c.stk_code, b1.fullnm as stk_name,c.sex,
					RIGHT('0' + CAST(DAY(c.birthdt) as varchar(2)), 2) as tgllhr,
					RIGHT('0' + CAST(MONTH(c.birthdt) as varchar(2)), 2) as blnlhr,
					YEAR(c.birthdt) as thnlhr, c.bankid, c.acc_name
				FROM ecomm_trans_hdr_sgo a
				LEFT OUTER JOIN klink_mlm2010.dbo.mssc b ON (a.idstk COLLATE SQL_Latin1_General_CP1_CS_AS = b.loccd)
				LEFT OUTER JOIN ecomm_memb_ok_sgo c ON (a.token = c.trx_no)
				LEFT OUTER JOIN klink_mlm2010.dbo.mssc b1 ON (c.stk_code COLLATE SQL_Latin1_General_CP1_CS_AS = b1.loccd)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb c1 ON (c.sponsorid collate SQL_Latin1_General_CP1_CS_AS = c1.dfno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb c2 ON (c.recruiterid collate SQL_Latin1_General_CP1_CS_AS = c2.dfno)
				LEFT OUTER JOIN ecomm_memb_ok d ON (a.orderno = d.trx_no)
				WHERE a.orderno = '$token'";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
	}

	function updateDataEcommMembSGO_EM($data) {
		$thnlahir = substr($data['thnlhr'], 2, 2);
		$birthdt = $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'];
		$password = $data['tgllhr'].$data['blnlhr'].$thnlahir;

		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();
		$noktp = trim($data['idno']);

		$stkinfo = $this->getDetailStockistByID(trim($data['stk_code']));
		$state = $stkinfo[0]->state;

		$sponsorid = trim(str_replace(' ', '', strtoupper($data['sponsorid'])));
		$rekruiterid = trim(str_replace(' ', '', strtoupper($data['rekruiterid'])));

		$arr = array(
			"sponsorid" => $sponsorid,
			"recruiterid" => $rekruiterid,
			"membername" => trim(strtoupper($data['membername'])),
			"password" => $password,
			"idno" => $noktp,
			"addr1" => trim(strtoupper($data['addr1'])),
			//"addr1" => $data['addr1'],
			"tel_hp" => $data['tel_hp'],
			"email" => trim($data['email']),
			"stk_code" => trim($data['stk_code']),
			"sex" => $data['sex'],
			"birthdt" => $birthdt,
			//"joindt" => date("Y-m-d"),
			//"trx_no" => $data['orderno'],
			//"memberid" => $data['orderno'],
			"prdcd" => $data['prdcd'],
			"state" => $state,
		);

		//$dbqryx->insert('db_ecommerce.dbo.ecomm_memb_ok_sgo',$arr);

		$dbqryx->where('trx_no', $data['orderno']);
		$dbqryx->update('db_ecommerce.dbo.ecomm_memb_ok_sgo', $arr);

		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();

			$dbqryx->select('memberid, membername, sponsorid, recruiterid, stk_code');
			$dbqryx->from('db_ecommerce.dbo.ecomm_memb_ok_sgo');
			$dbqryx->where('trx_no', $data['orderno']);
			$hasil = $dbqryx->get();
            return array(
              "response" => "true",
			  "arrayData" => $hasil->result()
			);
        }
	}

	function reconcileDataEcommMembSGO_EM($data) {
		$thnlahir = substr($data['thnlhr'], 2, 2);
		$birthdt = $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'];
		$password = $data['tgllhr'].$data['blnlhr'].$thnlahir;
		//echo $password;

		$dbqryx  = $this->load->database("db_ecommerce", TRUE);

		$dbqryx->trans_start();
		$noktp = trim($data['idno']);

		$stkinfo = $this->getDetailStockistByID(trim($data['stk_code']));
		$state = $stkinfo[0]->state;

		//$sponsorid = trim(preg_replace('/\s+/', ' ', $data['sponsorid']));
		//$rekruiterid = trim(preg_replace('/\s+/', ' ', $data['rekruiterid']));

		$sponsorid = trim(str_replace(' ', '', strtoupper($data['sponsorid'])));
		$rekruiterid = trim(str_replace(' ', '', strtoupper($data['rekruiterid'])));

		$arr = array(
			"sponsorid" => $sponsorid,
			"recruiterid" => $rekruiterid,
			"membername" => trim(strtoupper($data['membername'])),
			"password" => $password,
			"idno" => $noktp,
			"addr1" => trim(strtoupper($data['addr1'])),
			//"addr1" => $data['addr1'],
			"tel_hp" => $data['tel_hp'],
			"email" => trim($data['email']),
			"stk_code" => trim($data['stk_code']),
			"sex" => $data['sex'],
			"birthdt" => $birthdt,
			"joindt" => date("Y-m-d"),
			"trx_no" => $data['orderno'],
			"memberid" => $data['orderno'],
			"prdcd" => $data['prdcd'],
			"state" => $state,
		);

		$dbqryx->insert('db_ecommerce.dbo.ecomm_memb_ok_sgo',$arr);

		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();

			$dbqryx->select('memberid, membername, sponsorid, recruiterid, stk_code');
			$dbqryx->from('db_ecommerce.dbo.ecomm_memb_ok_sgo');
			$dbqryx->where('trx_no', $data['orderno']);
			$hasil = $dbqryx->get();
            return array(
              "response" => "true",
			  "arrayData" => $hasil->result()
			);
        }
	}

	function reconcileDataEcommMembSGO_mampirkak($data) {
		$thnlahir = substr($data['thnlhr'], 2, 2);
		$birthdt = $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'];
		$password = $data['tgllhr'].$data['blnlhr'].$thnlahir;
		//echo $password;

		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$this->load->model('webshop/member_model', 'member_model');
        $CI =&get_instance();
        //$CI->memb->hello_decision();

		$dbqryx->trans_start();
		$noktp = trim($data['idno']);
		$new_id = "";
		$lastkit = $this->member_model->showLastkitno($data['stk_code']);
		//print_r($lastkit);
		if(!empty($lastkit)) {

			if($lastkit[0]->lastkitno < 99999) {

				$updlastkitno = $CI->member_model->setLastKitNo($data['stk_code']);
				if($updlastkitno > 0) {
					 //echo "masuk di lastkitno kurang dari 99999 <br>";
					 $new_id = $CI->member_model->createNewID($lastkit);
					 //return $this->insertMember($orderno, $new_id, $dta[0]->idstk);
				} else {
					 //echo "masuk di lastkitno mulai dari awal atau 0";
					 $setLastKid = $CI->member_model->setLastKitToZero($data['stk_code']);
					 $lastkit = $CI->member_model->showLastkitno($data['stk_code']);
					 $new_id = $CI->member_model->createNewID($lastkit);
					 $updlastkitno = $CI->member_model->setLastKitNo($data['stk_code']);

				}
			}
		}




		$arr = array(
			"sponsorid" => $data['sponsorid'],
			"recruiterid" => $data['rekruiterid'],
			"membername" => trim(strtoupper($data['membername'])),
			"password" => $password,
			"idno" => $noktp,
			"addr1" => trim(strtoupper($data['addr1'])),
			//"addr1" => $data['addr1'],
			"tel_hp" => $data['tel_hp'],
			"email" => trim($data['email']),
			"stk_code" => trim($data['stk_code']),
			"sex" => $data['sex'],
			"birthdt" => $birthdt,
			"joindt" => $data['tgljoin'],
			"trx_no" => $data['orderno'],
			"memberid" => $new_id,
			"prdcd" => $data['prdcd'],
			"state" => $data['state'],
		);

		$dbqryx->insert('db_ecommerce.dbo.ecomm_memb_ok_sgo',$arr);

		//UPDATE TRANSAKSI HEADER
		$dbqryx->set('id_memb', $new_id);
        $dbqryx->where('token', $data['orderno']);
        $dbqryx->update('db_ecommerce.dbo.ecomm_trans_hdr');

		//UPDATE LOG_TRANS
		$dbqryx->set('log_dfno', $new_id);
        $dbqryx->where('log_trcd', $data['trx_id']);
        $dbqryx->update('db_ecommerce.dbo.log_trans');

		//UPDATE ECOMM_CHECKOUT
		$dbqryx->set('idmember', $new_id);
        $dbqryx->where('orderno', $data['trx_id']);
        $dbqryx->update('db_ecommerce.dbo.ecomm_checkout');

		//UPDATE KE NEWERA


		$fieldToUpdate1 = array(
			"dfno" => $new_id,
			"updatenm" => $this->username,
			"updatedt" => date("Y-m-d"),
			"flag_syn" => "0",
			"remarks" => "UPD MAMPIRKAK ERROR FROM ".$noktp
		);

        $dbqryx->where('trcd', $data['trx_id']);
        $dbqryx->update('klink_mlm2010.dbo.newtrh', $fieldToUpdate1);

		$fieldToUpdate2 = array(
			"dfno" => $new_id,
			"updatenm" => $this->username,
			"updatedt" => date("Y-m-d"),
			"remarks" => "UPD MAMPIRKAK ERROR FROM ".$noktp
		);

        $dbqryx->where('trcd', $data['trx_id']);
        $dbqryx->update('klink_mlm2010.dbo.sc_newtrh', $fieldToUpdate2);

		/*
		$updNewtrh = "UPDATE NEWTRH set dfno='$new_idmember', updatenm='$user_name', updatedt='$tglupd', remarks='$remark',
        	               flag_syn='0' WHERE trcd='$trcd'";

        $updsc_newtrh = "UPDATE sc_newtrh set dfno='$new_idmember', updatenm='$user_name', updatedt='$tglupd', remarks='$remark'
        	                WHERE trcd='$trcd'";
        */

		//END TRANSACTION
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();

			$dbqryx->select('memberid, membername, sponsorid, recruiterid, stk_code');
			$dbqryx->from('db_ecommerce.dbo.ecomm_memb_ok_sgo');
			$dbqryx->where('trx_no', $data['orderno']);
			$hasil = $dbqryx->get();
            return array(
              "response" => "true",
			  "arrayData" => $hasil->result()
			);
        }
	}

	function getListRecruit($data) {
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));
		$qry = "SELECT a.dfno, a.fullnm,
				   CONVERT(VARCHAR(30),a.jointdt,103) as  jointdt,
				   a.sponsorid, b.fullnm as sponsorname
				FROM msmemb a INNER JOIN msmemb b ON (a.sponsorid = b.dfno)
				WHERE a.sfno_reg = '$data[recruiterid]'
				AND CONVERT(VARCHAR(10), a.jointdt, 111) between '$dtfrom' and '$dtto'";
		//echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}

	function get_member($searchBy, $paramValue){
      	if($searchBy == 'memberid'){
      		//echo "MASUK SINI....";
		    $where = "where a.dfno like '%$paramValue%'";
		    }else if($searchBy == 'membernm'){
		    $where = "where a.fullnm like '%$paramValue%'";
		    }else{
		    $where = "where a.idno like '%$paramValue%'";
		    }

		    $slc = "select a.dfno,a.fullnm,a.idno,a.addr1,a.tel_hp, a.password,a.novac
					from msmemb a $where";
			//echo $searchBy." --- ".$slc;
			$result = $this->getRecordset($slc,null,$this->db2);
			return $result;
	}

	function get_headmem($id){
		$slc = "SELECT a.dfno,a.fullnm, a.idno,a.sex, a.addr1, a.addr2, a.addr3,
					a.loccd as stockist, a.bnsstmsc, sc.fullnm as sc_fullnm,
					sc.addr1+ ' ' + sc.addr1+ ' ' + sc.addr1 as sc_addr, sc.city as sc_city, a.email, a.password,
					CONVERT (VARCHAR(30), a.birthdt,103) as birth,
					CONVERT (VARCHAR(30),a.jointdt,103) as datejoin, a.tel_hp, a.tel_hm,
					a.country,a.sfno as idsponsor,  b.fullnm as sponsorname,a.novac,
					a.createnm,a.sfno_reg as recruiter,c.fullnm as recruiternm,
				    d.formno, d.vchkey,
				    f.prdcd, f.prdnm, g.dp,
				    CASE
				       WHEN a.dfno = d.formno AND (e.flag_voucher is null OR e.flag_voucher = '0')  AND e.memberid  IS NULL THEN 'PENDING VOUCHER'
				       WHEN d.formno is null AND (e.flag_voucher is null OR e.flag_voucher = '0')  AND e.memberid  IS NULL THEN 'REGISTERED FROM MLMWIN'
				       WHEN a.dfno = d.formno AND e.is_landingpage = '1' AND (e.flag_voucher is null OR e.flag_voucher = '0') AND e.memberid  IS NOT NULL THEN 'E-COMMERCE / LP'
				       WHEN a.dfno = d.formno  AND (e.flag_voucher is null OR e.flag_voucher = '0') AND e.memberid  IS NOT NULL THEN 'E-COMMERCE'
				       WHEN a.dfno != d.formno  AND e.flag_voucher = '1' THEN 'VOUCHER'
				        WHEN a.dfno != d.formno AND (e.flag_voucher is null OR e.flag_voucher = '1')  AND e.memberid  IS NULL THEN  'VOUCHER'
				    END as keteranganx,
				    e.flag_voucher,
				     e.memberid
				FROM msmemb a
					LEFT JOIN msmemb b on (a.sfno = b.dfno)
					LEFT JOIN msmemb c on (a.sfno_reg = c.dfno)
					LEFT JOIN mssc sc on (a.bnsstmsc = sc.loccd)
					LEFT JOIN starterkit d ON (a.dfno = d.activate_dfno)
					LEFT OUTER JOIN db_ecommerce.dbo.ecomm_memb_ok e ON (a.dfno = e.memberid COLLATE SQL_Latin1_General_CP1_CS_AS)
					LEFT OUTER JOIN msprd f ON d.prdcd=f.prdcd
					LEFT OUTER JOIN pricetab g ON f.prdcd=g.prdcd AND (g.pricecode='12W3' OR g.pricecode='12W4')
				WHERE a.dfno = '$id'";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}

	function getDatRekening($idmember) {
		$qry = "SELECT top 1 a.IDMEMBER, a.BANKCODE, a.REKNAME, a.REKNUM, a.REMARK, a.USERID,
				CONVERT (VARCHAR(30), a.ADDDATE,103) as ADDDATE 
				FROM MAINMEMBER.dbo.REKACC a
				WHERE a.IDMEMBER = '$idmember'
				ORDER BY a.ADDDATE DESC";
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function get_headmemCellNo($phoneNumber) {
		$slc = "SELECT a.dfno,a.fullnm, a.idno,a.sex, a.addr1, a.addr2, a.addr3,
					a.loccd as stockist, a.bnsstmsc, sc.fullnm as sc_fullnm,
					sc.addr1+ ' ' + sc.addr1+ ' ' + sc.addr1 as sc_addr, sc.city as sc_city, a.email, a.password,
					CONVERT (VARCHAR(30), a.birthdt,103) as birth,
					CONVERT (VARCHAR(30),a.jointdt,103) as datejoin, a.tel_hp, a.tel_hm,
					a.country,a.sfno as idsponsor,  b.fullnm as sponsorname,a.novac,
					a.createnm,a.sfno_reg as recruiter,c.fullnm as recruiternm,
				    d.formno, d.vchkey,
				    f.prdcd, f.prdnm, g.dp,
				    CASE
				       WHEN a.dfno = d.formno AND (e.flag_voucher is null OR e.flag_voucher = '0')  AND e.memberid  IS NULL THEN 'PENDING VOUCHER'
				       WHEN d.formno is null AND (e.flag_voucher is null OR e.flag_voucher = '0')  AND e.memberid  IS NULL THEN 'REGISTERED FROM MLMWIN'
				       WHEN a.dfno = d.formno AND e.is_landingpage = '1' AND (e.flag_voucher is null OR e.flag_voucher = '0') AND e.memberid  IS NOT NULL THEN 'E-COMMERCE / LP'
				       WHEN a.dfno = d.formno  AND (e.flag_voucher is null OR e.flag_voucher = '0') AND e.memberid  IS NOT NULL THEN 'E-COMMERCE'
				       WHEN a.dfno != d.formno  AND e.flag_voucher = '1' THEN 'VOUCHER'
				        WHEN a.dfno != d.formno AND (e.flag_voucher is null OR e.flag_voucher = '1')  AND e.memberid  IS NULL THEN  'VOUCHER'
				    END as keteranganx,
				    e.flag_voucher,
				     e.memberid
				FROM msmemb a
					LEFT JOIN msmemb b on (a.sfno = b.dfno)
					LEFT JOIN msmemb c on (a.sfno_reg = c.dfno)
					LEFT JOIN mssc sc on (a.bnsstmsc = sc.loccd)
					LEFT JOIN starterkit d ON (a.dfno = d.activate_dfno)
					LEFT OUTER JOIN db_ecommerce.dbo.ecomm_memb_ok e ON (a.dfno = e.memberid COLLATE SQL_Latin1_General_CP1_CS_AS)
					LEFT OUTER JOIN msprd f ON d.prdcd=f.prdcd
					LEFT OUTER JOIN pricetab g ON f.prdcd=g.prdcd AND g.pricecode='12W3'
				WHERE a.tel_hp = '$phoneNumber'";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}

	function checkId($dfno){
		$slc = "select top 1 a.bonusyear, a.bonusmonth,
				c.dfno, c.fullnm, a.currentrank,
				b.ranknm, b.shortnm from tbonus a
				inner join msrank b on a.currentrank=b.[level]
				inner join msmemb c on a.distributorcode=c.dfno where a.distributorcode='$dfno'
				order by a.bonusyear desc, a.bonusmonth desc";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}

	function checkRank($level){
		$slc = "select a.level, a.ranknm from msrank";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}

	function get_rank(){
		$slc = "select a.level, a.ranknm from msrank a
				where a.ranknm != ''
				order by a.ranknm asc";
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
	}

	function get_update($form){
		$bln = sprintf("%02s",$form['newperiod']);
		$date = date('Y-m-d H:i:s');
		//print_r($form);
		$slc = "insert into nhperingkat (idmember, nama, awal, akhir, bulan, bulan1,
				lvldesk, kirim, status, catatan, tglinput, tglrecog, tempat, status_recog,
				createnm, createdt) values ('$form[dfno]', '$form[fullnm]', '$form[currentrank]', '$form[newrank]',
				'$form[year2]".$bln."', '$form[newperiod]', '$form[newranknm]', '', '', '', '".$date."', '', '', '', '', '".$date."')";
		//print_r($slc);
		//echo $slc;
		$result = $this->executeQuery($slc, $this->setDB(2));
		if($result > 0) {
			$arr = array("response" => "true", "message" => "Penginputan berhasil");
		} else {
			$arr = array("response" => "false", "message" => "Penginputan gagal! field tidak boleh kosong!");
		}
		return $arr;
	}

	function getRankSearch($searchBy, $paramValue1, $paramValue2, $paramValue3, $paramValue4){
		if($searchBy == 'memberid'){
		    $where = "where a.idmember = '$paramValue1'";
		    }else if($searchBy == 'period'){
		    $where = "where a.bulan1 = '$paramValue2' and a.bulan like '%$paramValue3%'";
		    }else{
		    $where = "where b.level = '$paramValue4'";
		    }

		    $slc = "select a.idmember,a.nama,a.bulan,a.bulan1,b.ranknm as lvldesk, b.shortnm
					from nhperingkat a left outer join msrank b on a.akhir=b.[level]
					$where order by a.akhir";
			//echo $slc;
			$result = $this->getRecordset($slc,null,$this->db2);
			return $result;
	}

	function inputPlace(){

		$slc = "insert into place (place, dfno, ranknm, bulan, tahun, tanggal)
				values ('tempat', 'dfno', 'ranknm', 'bulan', 'tahun', 'tanggal')";
		//echo $slc;
		$result = $this->executeQuery($slc, $this->setDB(6));
		if($result > 0) {
			$arr = array("response" => "true", "message" => "Penginputan berhasil");
		} else {
			$arr = array("response" => "false", "message" => "Penginputan gagal! field tidak boleh kosong!");
		}
		return $arr;
	}

    /*function getRegno(){
        $this->db = $this->load->database('db_test', true);

        $slc = "SELECT nomer FROM register
                WHERE nomer = ( SELECT MAX(nomer) FROM register )";

        $query = $this->db->query($slc);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                //echo
                $ss = $data->nomer+1;
            }
        }
        //$jumlah = $query->num_rows();
        $month = date("m");
        $year = date("y");
        //$next_seq = sprintf("%03s",$ss);
        //echo $next_seq;
        $registerNo  = "PP".$month."".$year."".$ss."";

        return $registerNo;
    }

	function get_insert(){
		$this->db = $this->load->database('db_test', true);
		$registerno = $this->getRegno();
		if(isset($_POST['regMember'])){
				$data = array(
					'registerno' => "".$registerno."",
					'fullnm' => $_POST['fullnm'],
					'ktpno' => $_POST['ktpno'],
					'sex' => $_POST['sex'],
					'tel_hp' => $_POST['tel_hp']
				);
		  $insert = $this->db->insert('register', $data);

       	    if($this->db->affected_rows()){
                return 1;
    		}
            else{

                return 0;
            }
		}
	}

	function get_data(){
		$this->load->database('db_test', true);
		$q = $this->db->get('register');
		if($q->num_rows() > 0){
			return $q->result();
		}
		return array();
	}

	function update($id) {
		$this->load->database('db_test', true);
        $fullnm  = $this->input->post('fullnm');
        $ktpno = $this->input->post('ktpno');
		$tel_hp = $this->input->post('tel_hp');
		$data = array (
            'fullnm'  => $fullnm,
            'ktpno'=> $ktpno,
            'tel_hp' => $tel_hp
        );

		$qry = "update register set fullnm='$fullnm', ktpno='$ktpno', tel_hp='$tel_hp' where registerno='$id'";
        //echo $qry;
        $update = $this->db->query($qry);
        if(!$update){
            return 0;
        }else{
            return 1;
        }
        //$this->db->where('registerno', $id);
        //$this->db->update('register', $data);
    }

    function getById($id) {
		$this->load->database('db_test', true);
        return $this->db->get_where('register', array('registerno' => $id))->row();
    }

	function delete($id){
		$this->load->database('db_test', true);
		$this->db->where('registerno',$id);
		$this->db->delete('register');
	}*/

	function checkTlp($tel_hp){
        $slc = "select a.fullnm, a.tel_hp from msmemb a
                where a.tel_hp = '".$tel_hp."'";
        $result = $this->getRecordset($slc,null,$this->setDB(2));
		return $result;
    }

    function checkKtp($ktpno){
		$slc = "select a.memberid, a.idno from ecomm_memb_ok a
                where a.idno = '".$ktpno."'";
        $result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	function getListRegMembKNET($from, $to) {
		$qry = "SELECT dfno, fullnm, loccd, a.bnsstmsc,
		         CONVERT(VARCHAR(10),a.jointdt,103) as jointdt1,
		         CONVERT(VARCHAR(4), YEAR(a.jointdt))  + '-' +
		         RIGHT('0' + CONVERT(VARCHAR(2), MONTH(a.jointdt)), 2) + '-' +
		         RIGHT('0' + CONVERT(VARCHAR(2), DAY(a.jointdt)), 2)
		          AS jointdt,
		         a.sponsorid
				 FROM klink_mlm2010.dbo.msmemb a
				 INNER JOIN db_ecommerce.dbo.ecomm_memb_ok b
				 ON (a.dfno = b.memberid COLLATE SQL_Latin1_General_CP1_CI_AS)
				 WHERE a.entrytype = '7'
				 AND a.jointdt BETWEEN '$from' AND '$to'
				 ORDER BY loccd, dfno";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function getDetailMemberByID($id) {
		/*$qry = "SELECT a.dfno, a.fullnm, a.loccd, a.bnsstmsc, a.sponsorid,
				    e.fullnm as sponsorname,
					c.fullnm as loccd_name,
				    d.fullnm as bnsstmt_name,
				    b.memberid,b.password
				FROM klink_mlm2010.dbo.msmemb a
				LEFT JOIN klink_mlm2010.dbo.msmemb e ON (a.sponsorid = e.dfno)
				LEFT JOIN mssc c ON (a.loccd = c.loccd)
				LEFT JOIN mssc d ON (a.bnsstmsc = d.loccd)
				LEFT OUTER JOIN db_ecommerce.dbo.ecomm_memb_ok b
				   ON (a.dfno = b.memberid COLLATE SQL_Latin1_General_CP1_CI_AS)
				WHERE a.dfno = '$id'";*/

        $qry = " select a.*,CONVERT(varchar(2),a.birthdt, 103) as tgllhr,
					 CONVERT(varchar(2),a.birthdt, 101) as blnlhr,
					 CONVERT(varchar(4),a.birthdt, 102) as thnlhr,
					 b.fullnm as sponsorname,
					 f.fullnm as rekruitername,
					 c.fullnm as stkname,
					 d.token
				from db_ecommerce.dbo.ecomm_memb_ok a
				    left join klink_mlm2010.dbo.msmemb b on b.dfno = a.sponsorid collate SQL_Latin1_General_CP1_CI_AS
				    left join klink_mlm2010.dbo.msmemb f on f.dfno = a.recruiterid collate SQL_Latin1_General_CP1_CI_AS
				    left join klink_mlm2010.dbo.mssc c on c.loccd = a.stk_code collate SQL_Latin1_General_CP1_CI_AS
				    left join db_ecommerce.dbo.ecomm_trans_hdr d on a.trx_no = d.orderno collate SQL_Latin1_General_CP1_CI_AS
				where a.trx_no = '".$id."'";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function getDetailStockistByID($id) {
		$qry = "SELECT a.loccd, a.fullnm, a.state
				FROM mssc a
				WHERE a.loccd = '$id'";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function saveMemberUpdate($data) {
	   $birthdt = $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'];
       $birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;
        $bankaccnm1 = strtoupper($data['membername']);
        $state = $this->getStateByStk($data['bnsstmt']);

		$commerceHdr = "UPDATE ecomm_trans_hdr SET nmmember = '".$data['membername']."'
		              WHERE orderno = '".$data['orderno']."'";
		//echo $commerceHdr;
        $res1 = $this->executeQuery($commerceHdr, $this->setDB(1));

        $commerceMembOk = "UPDATE ecomm_memb_ok SET
                                sponsorid = '".$data['sponsorid']."',
                                recruiterid = '".$data['rekruiterid']."',
                                membername = '".$data['membername']."',
                                idno = '".$data['ktpno']."',
                                addr1 = '".strtoupper($data['addr1'])."',
                                tel_hm = '".$data['tel_hm']."',
                                tel_hp = '".$data['tel_hp']."',
                                email = '".$data['email']."',
                                stk_code = '".$data['bnsstmt']."',
                                sex = '".$data['sex']."',
                                birthdt = '".$birthdt."',
                                acc_no = '".$data['norek']."',
                                acc_name = '".$bankaccnm1."',
                                bankid = '".$data['pilBank']."',
                                state = '".$state."',
                                password = '".$password."'
		                   WHERE trx_no = '".$data['orderno']."'";
		//echo $commerceMembOk;
        $res2 = $this->executeQuery($commerceMembOk, $this->setDB(1));

		$insMsmemb = "UPDATE msmemb SET
                            sfno = '".$data['sponsorid']."',
                            password = '".$password."',
                            sponsorid = '".$data['sponsorid']."',
                            sponsorregid = '".$data['rekruiterid']."',
                            fullnm = '".$data['membername']."',
                            idno = '".$data['ktpno']."',
                            birthdt = '".$birthdt."',
                            sex = '".$data['sex']."',
                            addr1 = '".strtoupper($data['addr1'])."',
                            state = '".$state."',
                            tel_hp = '".$data['tel_hp']."',
                            tel_hm = '".$data['tel_hm']."',
                            email = '".$data['email']."',
                            bankaccno = '".$data['norek']."',
                            bankaccnm = '".$bankaccnm1."',
                            bankid = '".$data['pilBank']."',
                            loccd = '".$data['bnsstmt']."',
                            bnsstmsc = '".$data['bnsstmt']."',
                            loccd_et = '".$data['bnsstmt']."',
                            loccd_co = '".$data['bnsstmt']."'
                     WHERE trcd = '".$data['orderno']."'";
		//echo $insMsmemb;
        $res3 = $this->executeQuery($insMsmemb, $this->setDB(2));

        $membOkSgo = "UPDATE ecomm_memb_ok_sgo SET
                        sponsorid = '".$data['sponsorid']."',
                        recruiterid = '".$data['rekruiterid']."',
                        membername = '".$data['membername']."',
                        idno = '".$data['ktpno']."',
                        addr1 = '".strtoupper($data['addr1'])."',
                        tel_hm = '".$data['tel_hm']."',
                        tel_hp = '".$data['tel_hp']."',
                        email = '".$data['email']."',
                        stk_code = '".$data['bnsstmt']."',
                        sex = '".$data['sex']."',
                        birthdt = '".$birthdt."',
                        acc_no = '".$data['norek']."',
                        acc_name = '".$bankaccnm1."',
                        bankid = '".$data['pilBank']."',
                        state = '".$state."',
                        password = '".$password."'
                   WHERE trx_no = '".$data['token']."'";
		//echo $commerceMembOk;
        $res4 = $this->executeQuery($membOkSgo, $this->setDB(1));

        $commerceTransHdr = false;
        $commerceMembOKk = false;
		$klinkMsmemb = false;

		if($res1 > 0) {
			$commerceTransHdr = true;
		}
		if($res2 > 0) {
			$commerceMembOKk = true;
		}
        if($res3 > 0){
            $klinkMsmemb = true;
        }
		$arr = array("hdrTrans" => $commerceTransHdr, "membOkk" => $commerceMembOKk,"msmembb" => $klinkMsmemb);
		return $arr;
	}

    /*function checkValidOrderid($orderid){
        $qry = "SELECT a.Order_ID,a.id_memb,a.nmmember,a.Amount,a.Transaction_Date AS Transaction_Date1,CONVERT(VARCHAR(10),
		 				a.Transaction_Date , 126) as Transaction_Date,a.[Status_sgo],
		 				a.status_trx,a.MEMB_STATUS,a.ID_hdr,a.MEM_SGO,a.MEMB_OK,a.MSMEMBX,MEMB_STATUS
				 FROM V_ECOMM_LIST_RECONCILE_SGO a
				 WHERE  $paramStatus
				 GROUP BY a.Order_ID,a.id_memb,a.nmmember,a.Amount,
				 		a.Transaction_Date, CONVERT(VARCHAR(10), a.Transaction_Date , 126),a.[Status_sgo],
					   	a.status_trx,a.MEMB_STATUS, a.ID_hdr, a.MEM_SGO, a.MEMB_OK, a.MSMEMBX, A.MEMB_STATUS
				 ORDER BY a.Transaction_Date  DESC";

		//echo $qry;

		$res = $this->getRecordset($qry);
		return $res;
    }*/

    function checkDtByOrderId($orderno){
        $slc = "  select a.*, b.fullnm as sponsornm,c.orderno as ordernumb,
                 CONVERT(varchar(2),a.birthdt, 103) as tgllhr,
                 CONVERT(varchar(2),a.birthdt, 101) as blnlhr,
                 CONVERT(varchar(4),a.birthdt, 102) as thnlhr
                 from ecomm_memb_ok_sgo a
                  inner join klink_mlm2010.dbo.msmemb b on a.sponsorid = b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
                  inner join ecomm_trans_hdr c on a.trx_no = c.token COLLATE SQL_Latin1_General_CP1_CS_AS
                 where a.trx_no = '".$orderno."'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }

    function getStateByStk($idstk){
        $this->db = $this->load->database('klink_mlm2010', true);
        $slc = "select state from mssc where loccd = '".$idstk."'";
        $qry = $this->db->query($slc);

        if($qry == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($qry->result() as $data)
            {
                //echo
                $ss = $data->state;
            }
        }
        return $ss;
    }

    function updMembSgoOk($dt) {
    	$state = $this->getStateByStk($dt['idstk']);
        $birthdt = $dt['thnlhr']."-".$dt['blnlhr']."-".$dt['tgllhr'];
        $bankaccnm1 = strtoupper($dt['membername']);
        $kdpos1 =  "000004";
        $ip = $_SERVER['REMOTE_ADDR'];
        $birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;
        $jointdt = date("d-m-Y H:i:s");
		$memberid = $dt['idmember'];
    }

    function insMembSgoOk($dt){
        $state = $this->getStateByStk($dt['idstk']);
        $birthdt = $dt['thnlhr']."-".$dt['blnlhr']."-".$dt['tgllhr'];
        $bankaccnm1 = strtoupper($dt['membername']);
        $kdpos1 =  "000004";
        $ip = $_SERVER['REMOTE_ADDR'];
        $birth = explode("-", $birthdt);
        $thn = substr($birth[0], 2, 2);
        $password = $birth[2]."".$birth[1]."".$thn;
        $jointdt = date("d-m-Y H:i:s");



        //if($dt['idmember'] != '0'){
            $memberid = $dt['idmember'];

            $insMembOK = "insert into ecomm_memb_ok (sponsorid,memberid,membername,idno,
                        addr1,tel_hp,tel_hm,email,stk_code,sex,birthdt,
                        acc_no,acc_name,bankid,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid,
                        flag_voucher)
                        values ('".strtoupper($dt['sponsorid'])."','".$memberid."','".strtoupper($dt['membername'])."',
                                '".$dt['ktpno']."','".strtoupper($dt['addr1'])."','".$dt['tel_hp']."','".$dt['tel_hm']."',
                                '".$dt['email']."','".$dt['idstk']."','".$dt['sex']."',
                                '".$birthdt."','".$dt['norek']."','".$bankaccnm1."','".$dt['pilBank']."',
                                '".$dt['orderid']."','".$kdpos1."','".$state."','".$ip."','".$dt['prdcd']."','".$password."','".$dt['userlogin']."','".$dt['rekruiterid']."',
                                '0')";
            //echo "$insMembOK";
            $resMembOK = $this->executeQuery($insMembOK,$this->setDB(1));

            /*$insMsmemb = "insert into msmemb (dfno,sfno,sfno_reg,memberid,password,
                            sponsorid,fullnm,idno,birthdt,sex,addr1,postcd,tel_hp,jointdt,regtype,loccd,trcd,bnsstmsc,loccd_co,loccd_et,sponsorregid)
                            values('".$memberid."','".strtoupper($dt['sponsorid'])."','".$dt['userlogin']."','".$memberid."',
                                    '".$password."','".strtoupper($dt['sponsorid'])."','".strtoupper($dt['membername'])."','".$dt['ktpno']."',
                                    '".$birthdt."','".$dt['sex']."','".strtoupper($dt['addr1'])."','".$kdpos1."','".$dt['tel_hp']."','".$jointdt."',
                                    '".$dt['prdcd']."','".$dt['idstk']."','".$dt['orderid']."','".$dt['idstk']."','".$dt['idstk']."','".$dt['idstk']."','".strtoupper($dt['sponsorid'])."')";

            $resMsmemb = $this->executeQuery($insMsmemb,$this->setDB(2));*/

            $updtTransHdr = "update ecomm_trans_hdr set nmmember = '".strtoupper($dt['membername'])."' where orderno = '".$dt['orderid']."'";

            $resupdt = $this->executeQuery($updtTransHdr,$this->setDB(1));
        //}else{
            /*$insMembSgo = "insert into ecomm_memb_ok_sgo (sponsorid,memberid,membername,idno,
                        addr1,tel_hp,tel_hm,email,stk_code,sex,birthdt,
                        acc_no,acc_name,bankid,trx_no,kdpos,state,ip_address,prdcd,password,userlogin,recruiterid)
                        values ('".strtoupper($dt['sponsorid'])."','".$dt['tokenid']."','".strtoupper($dt['membername'])."',
                            '".$dt['ktpno']."','".strtoupper($dt['addr1'])."','".$dt['tel_hp']."','".$dt['tel_hm']."',
                            '".$dt['email']."','".$dt['idstk']."','".$dt['sex']."',
                            '".$birthdt."','".$dt['norek']."','".$bankaccnm1."','".$dt['pilBank']."','".$dt['tokenid']."',
                            '".$kdpos1."','".$state."','".$ip."','".$dt['prdcd']."','".$password."','".$dt['userlogin']."','".$dt['userlogin']."')";*/
            $insMembSgo = "UPDATE ecomm_memb_ok_sgo SET
                        sponsorid = '".strtoupper($dt['sponsorid'])."',
                        membername = '".strtoupper($dt['membername'])."',
                        idno = '".$dt['ktpno']."',
                        addr1 = '".strtoupper($dt['addr1'])."',
                        tel_hm = '".$dt['tel_hm']."',
                        tel_hp = '".$dt['tel_hp']."',
                        email = '".$dt['email']."',
                        stk_code = '".$dt['idstk']."',
                        sex = '".$dt['sex']."',
                        birthdt = '".$birthdt."',
                        acc_no = '".$dt['norek']."',
                        acc_name = '".$bankaccnm1."',
                        bankid = '".$dt['pilBank']."',
                        state = '".$state."',
                        password = '".$password."',
                        recruiterid = '".$dt['rekruiterid']."'
                   WHERE trx_no = '".$dt['tokenid']."'";
            //echo $updMemb;
            $resMembSgo = $this->executeQuery($insMembSgo);
        //}



		$membSgoOk = false;
        if($resMembSgo > 0 || $resMembOK > 0) {
			$membSgoOk = true;
		}
        $arr = array("membSgoOke" => $membSgoOk);
		return $arr;
        /*if(!$resMembSgo){
		  return 0;
		}else{
		  return 1;
		}*/
        //return 1;
    }

	function getMemberInfoByID($id) {
		$slc = "select a.dfno,a.fullnm, a.idno,a.sex, a.addr1, a.addr2, a.addr3,
					 a.loccd, a.bnsstmsc, c.fullnm as loccd_name, d.fullnm as bnsstmsc_name,
					 a.email, a.password,
					 CONVERT(varchar(2),a.birthdt, 103) as tgllhr,
					 CONVERT(varchar(2),a.birthdt, 101) as blnlhr,
					 CONVERT(varchar(4),a.birthdt, 102) as thnlhr,
					 CONVERT (VARCHAR(30),a.jointdt,103) as datejoin, a.tel_hp, a.tel_hm,
					 a.country,a.sfno as idsponsor,  b.fullnm as sponsorname,
					 a.sfno_reg as idrecruiter, z.fullnm as recruitername,
					 a.novac, a.email, a.bankid, a.bankaccno
				 from msmemb a
				 left join msmemb b on a.sfno = b.dfno
				 left join msmemb z on a.sfno_reg = z.dfno
				 left join mssc c on a.loccd = c.loccd
				 left join mssc d on a.bnsstmsc = d.loccd
				 where a.dfno = '$id'";

		$result = $this->getRecordset($slc,null,$this->db2);
		if($result != null) {
			if($result[0]->sex == "F") {
				$lbc = "SELECT CONVERT(VARCHAR(10),a.register_dt,103) as register_dt,
						    CONVERT(VARCHAR(10),a.expired_dt,103) as expired_dt,
						    bnsstmsc, REPRINT,
						    CASE
						    WHEN GETDATE() >= a.expired_dt THEN '1'
						    ELSE '0'
						    END
						    AS status_expire
						FROM ASH_LBC_MEMB a
						WHERE a.dfno =  '$id'";
				$resultLBC = $this->getRecordset($lbc,null,$this->db2);
				$arr = array("response" => "true", "arrayData" => $result, "lbc" => $resultLBC);
			} else {
				$arr = array("response" => "true", "arrayData" => $result, "lbc" => null);
			}

		} else {
			$arr = jsonFalseResponse("No result found, try another ID..");
		}
		/*if($res1['response'] == "true" && $res1['arrayData'][0]->sex == "F") {

		}*/
		return $arr;
	}

	function saveMemberData($data) {
		$hasil = 0;
		$tgllahir = $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'];
		$thnx = substr($data['thnlhr'], 2, 2);
		$password = $data['tgllhr'].$data['blnlhr'].$thnx;

		$addr1 = strtoupper($data['addr1']);
		$membername = strtoupper($data['membername']);

		//print_r($data);

		$updKlinkMlm = "UPDATE msmemb SET bnsstmsc = '$data[bnsstmt]', fullnm = '$membername',
		                  addr1 = '$addr1', addr2 = '$data[addr2]', addr3 = '$data[addr3]',
						  tel_hp = '$data[tel_hp]', sfno_reg = '$data[recruiterid]', sfno = '$data[sponsorid]',
						  bankid = '$data[bankid]', idno = '$data[ktpno]', birthdt = '$tgllahir', bankaccno = '$data[bankaccno]', email = '$data[email]', sex = '$data[sex]', password = '$password',
						  loccd = '$data[loccd]'
                        WHERE dfno = '$data[memberid]'";
		$res3 = $this->executeQuery($updKlinkMlm, $this->setDB(2));
		//echo $updKlinkMlm."<br />";

		$updDbEcommerce = "UPDATE ecomm_memb_ok SET acc_no = '$data[bankaccno]', bankid = '$data[bankid]', membername = '$membername',
								 recruiterid = '$data[recruiterid]', sponsorid = '$data[sponsorid]', sex = '$data[sex]',
								 addr1 = '$addr1', birthdt = '$tgllahir',
								 email = '$data[email]', idno = '$data[ktpno]', password = '$password', tel_hp = '$data[tel_hp]'
							   WHERE memberid = '$data[memberid]'";

		$resEcomm = $this->executeQuery($updDbEcommerce, $this->setDB(1));
		//echo $updDbEcommerce."<br />";

		if($res3 > 0) {
			$hasil++;
			$updNewera = "UPDATE distributor SET BONUSSTT = '$data[bnsstmt]', HP1 = '$data[tel_hp]', NMMEMBER = '$membername',
							  ADDR1 = '$addr1', ADDR2 = '$data[addr2]', ADD3 = '$data[addr3]',
							  TGLJOIN = '$tgllahir', STKINFO = '$data[loccd]', UPLINE = '$data[sponsorid]'
						  WHERE IDMEMBER = '$data[memberid]'";
			//echo $updNewera."<br />";
			$res4 = $this->executeQuery($updNewera, $this->setDB(4));

				if($res4 > 0) {
					$hasil++;
				}
		}

		return $hasil;
	}

	function saveMemberInfoUpdate($data) {
		//print_r($data);

		$hasil = 0;
		$tgllahir = $data['thnlhr']."-".$data['blnlhr']."-".$data['tgllhr'];
		$thnx = substr($data['thnlhr'], 2, 2);
		$password = $data['tgllhr'].$data['blnlhr'].$data['thnlhr'].$thnx;

		$updKlinkMlm = "UPDATE msmemb SET bnsstmsc = '$data[bnsstmt]',tel_hp = '$data[tel_hp]', fullnm = '$data[membername]',
		                  addr1 = '$data[addr1]', addr2 = '$data[addr2]', addr3 = '$data[addr3]'
                        WHERE dfno = '$data[memberid]'";
		//echo $updKlinkMlm;
        $res3 = $this->executeQuery($updKlinkMlm, $this->setDB(2));

        if($res3 > 0) {
        	$hasil++;

        	$updNewera = "UPDATE distributor SET BONUSSTT = '$data[bnsstmt]', HP1 = '$data[tel_hp]', NMMEMBER = '$data[membername]',
        			      ADDR1 = '$data[addr1]', ADDR2 = '$data[addr2]', ADD3 = '$data[addr3]'
                          WHERE IDMEMBER = '$data[memberid]'";
			//echo $updNewera;
        	$res4 = $this->executeQuery($updNewera, $this->setDB(4));

        	if($res4 > 0) {
        		$hasil++;
        	}
        }

		return $hasil;
	}

    function getDtIDmember($token){
        $xx = substr($token, 0, 1);
        //if($xx == 'E'){
            $slc="select a.id_memb,a.orderno,a.nmmember,a.token
                    from ecomm_trans_hdr a where a.token = '$token'";
        /*}else{
            $slc="select a.id_memb,a.orderno,a.nmmember
                    from ecomm_trans_hdr where a.orderno = '$orderno'";
        }*/

        $result = $this->getRecordset($slc,null,$this->db1);
        if($result!= null && $result[0]->id_memb != null) {
        	$set = "SELECT a.memberid as id_memb, a.trx_no as orderno, a.membername as nmmember
        	        FROM ecomm_memb_ok a
        	        WHERE a.trx_no = '".$result[0]->orderno."'";
			$result1 = $this->getRecordset($set,null,$this->db1);
			if($result1 != null) {
				$arr = array("response" => "true", "arrayData" => $result1, "update" => "1");
			} else {
               $arr = array("response" => "true", "arrayData" => $result, "update" => "0");
            }
        }else{
            $arr = array("response" => "false", "arrayData" => "no result");
        }
        return $arr;

    }

    function getListMembLP($from,$to){
        $slc = "select a.sponsorid,a.memberid,a.membername,a.idno,
                a.joindt,a.addr1,a.birthdt,a.tel_hp,a.email,a.trx_no,a.stk_code,
                a.userlogin,a.recruiterid,a.id_landingpage,b.[description] as description
                 from ecomm_memb_ok a
                 LEFT JOIN klink_mlm2010.dbo.[state] b on a.state = b.st_id COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.is_landingpage = '1'
                and (CONVERT(VARCHAR(10), a.joindt, 111)
                between '".$from."' and '".$to."')
                order by a.joindt";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
	    return $result;
    }

	function getListReconcileMemberRegVoucher($from, $to) {
		$qry = "SELECT a.memberid, a.idno, a.membername, CONVERT(varchar, a.joindt, 101) as joindt, a.stk_code,
				    a.sponsorid
				FROM [dbo].[ecomm_memb_ok] a WHERE flag_voucher =1 and joindt BETWEEN '".$from."' AND '".$to."'
				  AND memberid COLLATE SQL_Latin1_General_CP1_CI_AS not IN
				  (
				    SELECT dfno FROM [klink_mlm2010].[dbo].[msmemb]
				    WHERE jointdt BETWEEN '".$from."' AND '".$to."'
				  )

				  and a.idno COLLATE SQL_Latin1_General_CP1_CI_AS NOT IN
				   (
				    SELECT idno FROM [klink_mlm2010].[dbo].[msmemb]
				    WHERE jointdt BETWEEN '".$from."' AND '".$to."'

				   )";
		$result = $this->getRecordset($qry,null,$this->db1);
	    return $result;
	}

	function saveReconcileMemberRegVoucher($idmember) {
		$qry = "SELECT a.memberid, LEFT(a.memberid, 7) as prefixID, a.idno, a.tel_hp,
				 	replace(ltrim(replace(RIGHT(a.memberid, 5),'0',' ')),' ','0') as lastDigit,
					a.stk_code, b.lastkitno, a.voucher_no, a.voucher_key, a.userlogin
				FROM [dbo].[ecomm_memb_ok] a
				INNER JOIN [klink_mlm2010].[dbo].mssc b
				   ON (a.stk_code = b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS)
				WHERE a.memberid = '$idmember'";
		$result = $this->getRecordset($qry,null,$this->db1);

		if($result[0]->lastDigit <= $result[0]->lastkitno) {
            $idmember = $result[0]->memberid;
			$save = $this->insertMsmembFromMembOK($idmember, $result);
			return $save;
		} else {

			$updLastkit = "UPDATE [klink_mlm2010].[dbo].mssc SET laskitno = '".$result[0]->lastkitno."'
			               WHERE loccd = '".$result[0]->stk_code."'";
			//$updLastkitRes = $this->executeQuery($updLastkit, $this->$this->db1);
			echo $updLastKit;
			if($updLastkitRes > 0) {
				$sssd = printf("%05d", $result[0]->lastDigit);
				$newMemberId = $result[0]->prefixID."".$sssd;
				$updEcommMembOK = "UPDATE db_ecommerce.dbo.ecomm_memb_ok SET memberid = '$newMemberId' WHERE memberid = '$idmember'";
				echo $updEcommMembOK;
				/*$resUpdMembOk = $this->executeQuery($updEcommMembOK, $this->$this->db1);
				if($resUpdMembOk > 0) {
					$save = $this->insertMsmembFromMembOK($newMemberId, $result);
					return $save;
				} else {
					return jsonFalseResponse("Gagal update ke tabel Ecomm_memb_ok..");
				}*/
				//$save = $this->insertMsmembFromMembOK($newMemberId, $result);
				//return $save;
			} else {
				return jsonFalseResponse("Update laskitno Stokis gagal..");
			}

			//return jsonFalseResponse("Blm ada Script Update laskitno..");

		}
	}

	function insertMsmembFromMembOK($idmember, $result) {
		$db_qryx = $this->load->database('db_ecommerce', true);

		$cek = "SELECT dfno, fullnm, idno
		        FROM klink_mlm2010.dbo.msmemb
		        WHERE idno = '".$result[0]->idno."'";
		//echo $cek;
		$resultCek = $db_qryx->query($cek);
		$hasilx = $resultCek->result();
		if($hasilx == null) {


		    $db_qryx->trans_begin();

			$save = "INSERT INTO klink_mlm2010.dbo.msmemb (dfno, idno, memberid,  fullnm,
							jointdt, sfno, sponsorid, sfno_reg, sponsorregid,
							addr1, addr2, addr3, password, birthdt, bankid,
							bankaccno, bankaccnm, country, email, sex, tel_hp, state,
							loccd, bnsstmsc, postcd)
						SELECT a.memberid, a.idno, a.memberid as dfd, a.membername,
						   a.joindt, a.sponsorid as sfno, a.sponsorid, a.recruiterid as sfno_reg, a.recruiterid,
						   a.addr1, a.addr2, a.addr3, a.[password], a.birthdt, a.bankid,
						   a.acc_no, a.acc_name, a.country, a.email, a.sex, a.tel_hp, a.[state],
						   a.stk_code, a.stk_code as loccd, '000004'
						FROM db_ecommerce.dbo.ecomm_memb_ok a
						WHERE a.memberid = '".$idmember."'";
			//echo $save;
			$resultsave = $db_qryx->query($save);

			$updSk = "UPDATE klink_mlm2010.dbo.starterkit SET
			             activate_dfno = '".$idmember."',
			             status='2',
			             activate_by = '".$result[0]->userlogin."'
			          WHERE formno = '".$result[0]->voucher_no."' AND vchkey = '".$result[0]->voucher_key."'";
		    //echo $updSk;
		    $resultUpdSK = $db_qryx->query($updSk);

		    if ($db_qryx->trans_status() === FALSE) {
				$db_qryx->trans_rollback();
				return jsonFalseResponse("Proses Reconcile data member gagal..");
			} else {
				$db_qryx->trans_commit();
				return jsonTrueResponse(null, "Proses Reconcile data member berhasil..");
			}


		} else {
			return jsonFalseResponse("No KTP ".$hasilx[0]->idno." sudah terdaftar a/n ".$hasilx[0]->dfno." / ".$hasilx[0]->fullnm);
		}
	}

	function showInfoVch($char) {
		$i=0;
		foreach($char as $dta) {
			$qry = "SELECT formno, vchkey, status, activate_dfno, sold_trcd
			        FROM klink_mlm2010.dbo.starterkit WHERE formno = '$dta[0]'";
		    //echo $qry;
			$result = $this->getRecordset($qry,null,$this->db1);
			if($result != null) {
				$arr[$i]['formno'] = $result[0]->formno;
				$arr[$i]['vchkey'] = $result[0]->vchkey;
				$arr[$i]['status'] = $result[0]->status;
				$arr[$i]['activate_dfno'] = $result[0]->activate_dfno;
				$arr[$i]['sold_trcd'] = $result[0]->sold_trcd;
			} else {
				$arr[$i]['formno'] = $dta[0];
				$arr[$i]['vchkey'] = "";
				$arr[$i]['status'] = "";
				$arr[$i]['activate_dfno'] = "";
				$arr[$i]['sold_trcd'] = "";
			}
			$i++;
		}
		return $arr;
	}

	function uploadNewVoucherKey($data) {
		$db_qryx = $this->load->database('db_ecommerce', true);
		$errUpd = 0;
		$successUpd = 0;
		$jumRec = 0;
		foreach($data as $dta) {
			$qry = "UPDATE klink_mlm2010.dbo.starterkit SET vchkey = '$dta[1]'
			        WHERE formno = '$dta[0]' AND status != '2'";
		    //echo $qry;
			//echo "<br />";
			$hasil = $db_qryx->query($qry);
			if($hasil > 0) {
				$successUpd++;
				$arrSuccess[]['voucherno']=$dta[0];
			} else {
				$errUpd++;
				$arrFail[]['voucherno']=$dta[0];
			}
			$jumRec++;

		}

		$return = array("success" => $successUpd, "fail" => $errUpd, "jumRec" => $jumRec);
		return $return;

	}

	function updateVchKeyByVchNo($vchno, $newvchkey) {
		$db_qryx = $this->load->database('db_ecommerce', true);
		$qry = "UPDATE klink_mlm2010.dbo.starterkit SET vchkey = '$newvchkey'
			        WHERE formno = '$vchno' AND status != '2'";
		$db_qryx->query($qry);
		return ($db_qryx->affected_rows() > 0) ? TRUE : FALSE;
	}

	function getInfoMemberStokis($table, $param, $value) {
		if($table == "msmemb") {
			$qry = "SELECT a.fullnm
					FROM msmemb a
					WHERE a.$param = '$value'";
			//echo $qry;
		} else if($table == "mssc") {
			$qry = "SELECT fullnm FROM mssc WHERE $param = '$value'";
		}

		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	// getlistnonreconciledmember
	function getListNonReconciledMember($from, $to) {
		$db_qryx = $this->load->database('db_ecommerce', true);
		$qry = "SELECT b.orderno, b.id_memb, b.token, a.trx_no, a.membername, d.dfno, a.idno,
					CONVERT(VARCHAR(10), b.datetrans, 111) as datetrans,
					d.dfno + ' - ' +d.fullnm as ktp_sudah_terdaftar, y.trx_no as no_transaksi,
					e.fullnm + ' - ' +e.tel_hp as konfirmasi,
					a.sponsorid, a.recruiterid
				FROM ecomm_trans_hdr b
				LEFT OUTER JOIN log_trans z ON (b.orderno = z.log_trcd)
				LEFT OUTER JOIN ecomm_memb_ok_sgo a
					ON (a.trx_no = b.token)
				LEFT OUTER JOIN ecomm_memb_ok c
					ON (b.orderno = c.trx_no)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb d
					ON (a.idno != '' and a.idno is not null and a.idno COLLATE SQL_Latin1_General_CP1_CS_AS = d.idno)
				LEFT OUTER JOIN ecomm_memb_ok y
					ON (d.dfno COLLATE SQL_Latin1_General_CP1_CI_AS = y.memberid)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb e
					ON (z.log_usrlogin != '' and z.log_usrlogin is not null and z.log_usrlogin COLLATE SQL_Latin1_General_CP1_CS_AS = e.dfno)
				WHERE (b.token LIKE 'EM%' OR b.token LIKE 'RM%' OR b.token LIKE 'RA%' OR b.token LIKE 'RB%')
					AND c.memberid is null
					AND a.membername is not null
					AND a.membername != ''
					AND CONVERT(VARCHAR(10), b.datetrans, 121) BETWEEN '".$from."' AND '".$to."'
				ORDER BY a.idno DESC";
		$result = $this->getRecordset($qry, null, $this->setDB(1));
		return $result;
		// print_r($qry);
	}

	function releaseVoucher($voucherNo) {
		$qry = "UPDATE klink_mlm2010.dbo.starterkit SET starterkit.status = '1' WHERE starterkit.formno = '$voucherNo'";
		echo "$qry";
		// $result = $this->executeQuery($qry, NULL, $this->setDB(2));
		// return $result;
	}
}
?>
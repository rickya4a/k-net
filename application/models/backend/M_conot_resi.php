<?php
class M_conot_resi extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();

	}
	
	function listConoteResiFisikBlmUpdate($whcd, $from, $to, $print) {
		if($print == "0") {
			$add = " AND b.print_count = 0";
			//$date_add="dateadd(day,-7,'$dtfrom')";
			
			
		} else {
			$add = " AND b.print_count > 0";
			//$date_add="'$dtfrom'";
		}
	
		$qry = "SELECT a.orderno, 
					a.conoteJNE, 
					a.receiver_name,
					b.conote_new, 
					a.cargo_id, 
					c.shipper_name,
					a.kec_code,
					z.whcd,
				CONVERT(VARCHAR(10), b.datetrans , 120) as datetrans
				FROM ecomm_trans_shipaddr a
				INNER JOIN jne_Tabel_Zona_Induk z ON (a.kec_code = z.kode_kecamatan)
				INNER JOIN master_logistic_shipper c ON (a.cargo_id = c.shipper_id)
				INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno) 
				WHERE (a.conoteJNE is not NULL AND a.conoteJNE != '') AND
				  (b.conote_new is null OR b.conote_new = '') AND
				  z.whcd = '$whcd' AND
				  CONVERT(VARCHAR(10), b.datetrans , 120) BETWEEN '$from' and '$to'
				  $add
				ORDER BY a.orderno";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}
	
	function listConoteResiFisikSudahUpdate($whcd, $from, $to, $print) {
		if($print == "0") {
			$add = " AND b.print_count = 0";
			//$date_add="dateadd(day,-7,'$dtfrom')";
			
			
		} else {
			$add = " AND b.print_count > 0";
			//$date_add="'$dtfrom'";
		}
	
		$qry = "SELECT a.orderno, 
					a.conoteJNE, 
					a.receiver_name,
					b.conote_new, 
					a.cargo_id, 
					c.shipper_name,
					a.kec_code,
					z.whcd,
				CONVERT(VARCHAR(10), b.datetrans , 120) as datetrans
				FROM ecomm_trans_shipaddr a
				INNER JOIN jne_Tabel_Zona_Induk z ON (a.kec_code = z.kode_kecamatan)
				INNER JOIN master_logistic_shipper c ON (a.cargo_id = c.shipper_id)
				INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno) 
				WHERE (a.conoteJNE is not NULL AND a.conoteJNE != '') AND
				  (b.conote_new is not null OR b.conote_new != '') AND
				  z.whcd = '$whcd' AND
				  CONVERT(VARCHAR(10), b.datetrans , 120) BETWEEN '$from' and '$to'
				  $add
				ORDER BY a.orderno";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}
	
	function updateResiFisik($orderno) {
		$qry = "UPDATE a
					SET a.conote_new = b.conoteJNE
				FROM ecomm_trans_hdr a
				INNER JOIN ecomm_trans_shipaddr b ON (a.orderno = b.orderno)
				WHERE a.orderno IN ($orderno)";
		$hasil = $this->executeQuery($qry, $this->setDB(1));
		return $hasil;
	}
}
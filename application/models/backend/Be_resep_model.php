<?php
class Be_resep_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getPrd(){
        //get list product from klink_mlm2010.dbo.msprd
        $q = $this->db->query("SELECT prdcd, prdnm FROM klink_mlm2010.dbo.msprd WHERE status = '1' ORDER BY prdnm");

        return $q->result_array();
    }
}

?>
<?php

	class Be_search_model extends MY_Model{

		public function __construct(){

			parent::__construct();
		}

		public function listRekMember($idname){

			$qry = "select *,
					CONVERT(varchar, ADDDATE, 105) as tgl
					from rekacc where(idmember like '%$idname%' or nmmember like '%$idname%')
					order by idmember, adddate desc";

			$result = $this->getRecordset($qry, null, $this->db9);
			//echo $qry;

			return $result;
		}

		public function listRekStockist($idname){

			$qry = "select * from REKACCSTOCKIST where(STOCKIST like '%$idname%' or IDMEMBER like '%$idname%' or NAME like '%$idname%')
					order by STOCKIST, adddate desc";

			$result = $this->getRecordset($qry, null, $this->db9);
			//echo $qry;
			return $result;
		}

		public function getDetPrd($prdcd){

			$qry = "select prdcdDet, prdcdNmDet
					from newera_prddet
					where prdcdCat = '$prdcd'";

			$result = $this->getRecordset($qry, null, $this->db2);
			//echo $qry;
			return $result;
		}

		public function getBonusMember($id_memb,$year){

			$qry = "select bonusmonth as bulan,dfno as idmemb,totBonus as tot_gross, tax, adm_bank, totBonus_Nett as tot_net from tbonus_hilal_nett
					where bonusyear='$year' AND dfno='$id_memb' order by bonusmonth ASC";

			$result = $this->getRecordset($qry, null, $this->db2);
			//echo $qry;
			return $result;
		}

		public function getNameMember($id_memb){

			$qry = "select * from msmemb where dfno LIKE '%$id_memb%'";
			$result = $this->getRecordset($qry, null, $this->db2);
			//echo $qry;
			return $result;
		}

	}
<?php
class K_wallet extends MY_Model {

	public function __construct() {
		parent::__construct();
	}

	function getFailedTrx($from = NULL, $to = NULL) {

		$str = NULL;
		if (!empty($from) || !empty($to)) {
			$str = " AND CONVERT(VARCHAR(10), a.createdt, 121) BETWEEN '$from' AND '$to'";
		}
		$qry = "SELECT a.trcd,
		b1.orderno as delete_orderno,
		CASE
			WHEN b.orderno is null and b1.orderno is null THEN ba.orderno
			ELSE ba.token
		END AS order_number,
		CASE
			WHEN b.orderno is null and b1.orderno is null THEN bsgo.nmmember
			ELSE b.nmmember
		END AS penerima_bv,
        CASE
			WHEN b.orderno is null and b1.orderno is null and bsgo.sentTo = '2'
               THEN ship_sgo.receiver_name
            WHEN b.orderno is null and b1.orderno is null and bsgo.sentTo = '1'
               THEN bsgo.idstk
            WHEN b.orderno is not null and b.sentTo = '2' THEN ship.receiver_name
            WHEN b.orderno is not null and b.sentTo = '1' THEN b.idstk
			ELSE '--'
		END AS nama_penerima,
		CONVERT(VARCHAR(10), a.createdt, 111) as createdt,
		a.createnm, c.fullnm, c.tel_hp,
		a.effect, a.amount
		FROM va_cust_pay_det a
		LEFT OUTER JOIN va_cust_pay_det a1 ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = a1.refno)
		LEFT OUTER JOIN ecomm_trans_hdr b ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = b.orderno)
		LEFT OUTER JOIN ecomm_trans_hdr ba ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = ba.token)
        LEFT OUTER JOIN ecomm_trans_hdr_sgo bsgo ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = bsgo.orderno)
        LEFT OUTER JOIN ecomm_trans_shipaddr_sgo ship_sgo ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = ship_sgo.orderno)
        LEFT OUTER JOIN ecomm_trans_shipaddr ship ON (b.orderno COLLATE SQL_Latin1_General_CP1_CI_AS = ship.orderno)
		LEFT OUTER JOIN ecomm_trans_hdr_DEL b1 ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = b1.orderno)
		LEFT OUTER JOIN klink_mlm2010.dbo.msmemb c ON (a.createnm = c.dfno)
		WHERE b.orderno is null and a.effect = '-' and a1.refno is null
		$str
		and SUBSTRING(a.trcd,0,3) NOT IN ('PX', 'PS', 'TA')
		and a.createnm NOT IN ('IDSPAAA66834', 'ID9999A00251')";


		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function listXlTrxGagal($from = NULL, $to = NULL) {

		$str = NULL;
		if (!empty($from) || !empty($to)) {
			$str = " AND CONVERT(VARCHAR(10), a.createdt, 121) BETWEEN '$from' AND '$to' ";
		}
		$qry = "SELECT a.trcd, a.amount, a.createnm, b.memberid, b.membername, b.trx_type,
				c.reff_pay_id, c.cust_no, CONVERT(VARCHAR(10), a.createdt, 111) as datetrans
				FROM va_cust_pay_det a
				LEFT OUTER JOIN va_cust_pay_det a1 ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = a1.refno)
				LEFT OUTER JOIN ppob_trx_hdr b
				ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = b.trx_id)
				LEFT OUTER JOIN ppob_trx_det c ON (b.trx_id COLLATE SQL_Latin1_General_CP1_CI_AS = c.trx_id)
				WHERE (c.reff_pay_id is null OR c.reff_pay_id = '' ) $str
				and SUBSTRING(a.trcd,0,3) IN ('PX') and a1.refno is null
				and a.createnm NOT IN ('IDSPAAA66834','ID9999A00251')";
				//and a.createnm NOT IN ('IDSPAAA66834', 'ID9999A00251')
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function listTransaksiKnetGagalPotongsaldo($from = NULL, $to = NULL) {

		$str = NULL;
		if (!empty($from) || !empty($to)) {
			$str .= " AND CONVERT(VARCHAR(10), a.datetrans, 121) BETWEEN '$from' AND '$to'";
		}
		$qry = "SELECT
					a.orderno, a.token, b.trcd, a.total_pay + a.payShip as total_trx,
					CONVERT(VARCHAR(10), a.datetrans, 111) as datetrans,
					c.userlogin, d.fullnm as username, d.tel_hp,
					e.amount as sisa_saldo
				FROM ecomm_trans_hdr a
				LEFT OUTER JOIN va_cust_pay_det b ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd OR a.token COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
				LEFT OUTER JOIN ecomm_trans_hdr_sgo c ON (a.token = c.orderno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb d ON (c.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS = d.dfno)
				LEFT OUTER JOIN va_cust_pay_bal e ON (c.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS = e.dfno)
				WHERE a.bank_code_payment = '25' and b.trcd is null
				$str
				ORDER BY c.userlogin";
		//--AND CONVERT(VARCHAR(10), a.datetrans, 121) BETWEEN '$from' AND '$to'
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function checkDoubleInputKwallet($token, $orderno) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);

		$dbqryx->select('trcd');
		$dbqryx->from('va_cust_pay_det');
		$dbqryx->where('trcd =', $token);
		$dbqryx->or_where('trcd =', $orderno);
		$query = $dbqryx->get();
		return $query->result();
	}

	function getDetailTrx($orderno) {
		$qry = "SELECT
					a.orderno, a.token, b.trcd, a.total_pay + a.payShip as total_trx,
					CONVERT(VARCHAR(10), a.datetrans, 111) as datetrans,
					c.userlogin, d.fullnm as username, d.tel_hp, d.novac,
					e.amount as sisa_saldo
				FROM ecomm_trans_hdr a
				LEFT OUTER JOIN va_cust_pay_det b ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd OR a.token COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
				LEFT OUTER JOIN ecomm_trans_hdr_sgo c ON (a.token = c.orderno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb d ON (c.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS = d.dfno)
				LEFT OUTER JOIN va_cust_pay_bal e ON (c.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS = e.dfno)
				WHERE a.bank_code_payment = '25' and b.trcd is null and a.orderno = '$orderno'
				ORDER BY c.userlogin";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function getDetailTrxEcommSudahDihapus($orderno) {
		$qry = "SELECT a.orderno, a.total_pay + a.payShip as total_trx
				FROM ecomm_trans_hdr_DEL a
				LEFT OUTER JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
				LEFT OUTER JOIN klink_mlm2010.dbo.newtrh c ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = c.trcd)
				LEFT OUTER JOIN klink_mlm2010.dbo.sc_newtrh d ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = d.trcd)
				LEFT OUTER JOIN va_cust_pay_det e ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = e.trcd)
				WHERE a.orderno = '$orderno' AND b.orderno is null
				AND c.trcd is null AND d.trcd is null AND e.trcd is not null";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function getTrcd($token) {
		$qry = "SELECT a.trcd, b.orderno, b.token FROM db_ecommerce.dbo.va_cust_pay_det a
			    INNER JOIN db_ecommerce.dbo.ecomm_trans_hdr b
				ON a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = b.token COLLATE SQL_Latin1_General_CP1_CI_AS
			    WHERE a.trcd = '$token'";
		$result = $this->getRecordset($qry, $this->db1);
		return $result;
	}

	function updateTrcd($orderno, $token) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();

		$qry = "UPDATE db_ecommerce.dbo.va_cust_pay_det
						SET trcd = '$orderno'
						WHERE trcd = '$token'";
		$dbqryx->query($qry);
		//$result = $this->executeQuery($qry, $this->db1);

		$qry1 = "UPDATE db_ecommerce.dbo.va_cust_pay_det_acc
						SET trcd = '$orderno'
						WHERE trcd = '$token'";
		$dbqryx->query($qry1);
	    //$result1 = $this->executeQuery($qry1, $this->db1);

		//END TRANSACTION
		$dbqryx->trans_complete();

		if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			 // "arrayData" => $recData
			);
        }
	}

	function saveImportToDB($data) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$totalRecord = 0;
		$ins = 0;
		$double = 0;
		$fail = 0;
		$nontop = 0;
		$membernotfound = 0;
		$arrDouble = array();
		$arrIns = array();
		$arrFail = array();
		$arrMembNotFound = array();
		$arrNonTop = array();

		foreach($data['csvData'] as $field) {
			$payment_ref = $field[0];
			$comm_code = "TOPKLINK";
			$datetrx = $field[3];
			$product_value = $field[14];
			$membercode = substr($product_value, -9);

			$amount =  str_replace(array(",","."), "", substr($field[4], 0, -2));
			$fee =  str_replace(array(",","."), "", substr($field[5], 0, -2));
			$total_amount = $amount + $fee;
			$channel = $field[8];
			$status = trim($field[9]);

			if(($status == "Success" || $status == "Suspect") && $field[2] == "TOP UP KLINK") {


					// //QUERY FOR CHECK DOUBLE TRANSACTION ID
					$dbqryx->select('payment_ref, product_value');
					$dbqryx->from('db_ecommerce.dbo.va_sgo_paynotif_req');
					//$dbqryx->where('rq_uuid', $data['rq_uuid']);
					$dbqryx->where('payment_ref',$payment_ref);
					$query = $dbqryx->get();
					$result = $query->result();
					//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
					if($result == null) {
						$this->load->model('webshop/login_model', 'login');
						$res = $this->login->showDataMemberByNovac($membercode);

						if($res != null)  {
							$dfno = $res[0]->dfno;
							$fullnm = $res[0]->fullnm;

							$insArr = array(
								"rq_datetime" => $datetrx,
								"sender_id" => "SGOPLUS",
								"receiver_id" => $comm_code,
								"password" => "k-net181183",
								"comm_code" => $comm_code,
								"member_code" => $membercode,
								"member_cust_id" => $membercode,
								"member_cust_name" => $fullnm,
								"amount" => $amount,
								"payment_datetime" => $datetrx,
								"payment_ref" => $payment_ref,
								"order_id" => $membercode,
								"product_code" => $channel,
								"product_value" => $product_value,
								"status" => "0",
								"total_amount" => $total_amount,
								"member_id" => $dfno,
								"payment_remark" => "IMPORT ESPAY"
								/*
								"rq_uuid" => $data['rq_uuid'],
								"debit_from" => $data['debit_from'],
								"debit_from_name" => $data['debit_from_name'],
								"debit_from_bank" => $data['debit_from_bank'],
								"receiver_id" => $data['receiver_id'],
								"credit_to" => $data['credit_to'],
								"credit_to_name" => $data['credit_to_name'],
								"credit_to_bank" => $data['credit_to_bank'],
								"payment_remark" => $data['payment_remark'],
								"message" => $data['message'],
								"token" => $data['token'],
								"approval_code_full_bca" => $data['approval_code_full_bca'],
								"signature" => $data['signature']
								*/
							);
							//$ins++;
							//array_push($arrIns, $insArr);
							$insertVA = $dbqryx->insert('db_ecommerce.dbo.va_sgo_paynotif_req', $insArr);
							if($insertVA > 0) {
								$ins++;
								array_push($arrIns, $insArr);
								$text = "Saldo VA anda sudah di Top Up sebesar Rp ".$amount;
								$text .= ", potongan sebesar Rp ".$amount. " dari Rp ". $total_amount;
								$send = smsTemplate($res[0]->tel_hp, $text);
								//$send = smsTemplate("087780441874", $text);
							} else {
								$fail++;
								array_push($arrFail, $insArr);
							}

						} else {
							$membernotfound++;
							$text = $payment_ref." - ".$product_value;
							array_push($arrMembNotFound, $text);
						}
					} else {
						$double++;
						$text = $result[0]->payment_ref." - ".$result[0]->product_value;
						array_push($arrDouble, $text);
					}

			} else {
				$nontop++;
				$text = $payment_ref." - ".$product_value;
				array_push($arrNonTop, $text);
			}
			$totalRecord++;
		}

		$arr = array(
			"success" => $arrIns,
			"fail" => $arrFail,
			"double" => $arrDouble,
			"non_topup" => $arrNonTop,
			"member_not_found" => $arrMembNotFound,
			"insSuccess" => $ins,
			"double" => $double,
			"insFail" => $fail,
			"rec_member_not_found" => $membernotfound,
			"rec_nontop" => $nontop,
			"totalRecord" => $totalRecord
		);
		return $arr;

	}

	function checkIfRecoverTrxExist($trx_id) {
		$qry = "SELECT a.trcd, b.trcd as recover_trx_id, b.refno, a.amount, b.amount as amount_recover,
					CONVERT(VARCHAR(10), a.createdt, 111) as datetrans,
					CONVERT(VARCHAR(10), b.createdt, 111) as recoverdate,
					a.effect, b.effect as effect_recover, a.createnm, a.dfno, a.novac
				FROM va_cust_pay_det a
				LEFT OUTER JOIN va_cust_pay_det b ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = b.refno)
				WHERE a.trcd = '$trx_id'";
		$result = $this->getRecordset($qry, null, $this->db1);
		if($result != null) {
			$tgl = $result[0]->recoverdate;
			if($result[0]->trcd == $result[0]->refno && $result[0]->effect == "-" && $result[0]->effect_recover == "+" ) {
				throw new Exception("Transaksi $trx_id sudah di recover pada tanggal $tgl");
			}
			return $result;

		} else {
			throw new Exception('Transaksi $trx_id tidak ditemukan');
			return $result;
		}
	}

	function recoverSaldoKwalletFromXLTrx($arr) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();

		$dbqryx->insert('va_cust_pay_det', $arr);

		$dbqryx->where('trx_id', $arr['refno']);
		$dbqryx->delete('ppob_trx_hdr');
		$dbqryx->where('trx_id', $arr['refno']);
		$dbqryx->delete('ppob_trx_det');
		$dbqryx->where('trx_id', $arr['refno']);
		$dbqryx->delete('ppob_trx_pay');

		//END TRANSACTION
		$dbqryx->trans_complete();

		if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			 // "arrayData" => $recData
			);
        }


	}

	function checkIfTrxDeleted($trcd) {
		$qry = "SELECT orderno, token FROM ecomm_trans_hdr a WHERE a.orderno = '$trcd' OR a.token = '$trcd'";
		$result = $this->getRecordset($qry, null, $this->db1);
		if($result != null) {
			throw new Exception("Transaksi $trcd belum di hapus, silahkan hapus terlebih dahulu..");
		}
		return $result;

	}

	function recoverSaldoKwalletFromKNET($arr) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		//$dbqryx->trans_start();

		$dbqryx->insert('va_cust_pay_det', $arr);

		$dbqryx->select('trcd');
		$dbqryx->from('va_cust_pay_det');
		$dbqryx->where('trcd', $arr['trcd']);
		$result = $dbqryx->get();

		if($result->result() != null) {
			return array(
              "response" => "true",
			 // "arrayData" => $recData
			);
		} else {
			return array(
              "response" => "false"
			);
		}


	}

	function tesTrx($param, $value) {
		$qry = "SELECT * FROM va_cust_pay_det WHERE $param = '$value'";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function kwalletReconTrxOnly($trxno) {
		$query_det = "UPDATE dbo.ppob_trx_det SET reff_pay_id = 'REC' WHERE ppob_trx_det.trx_id = '$trxno'";
		$result_det = $this->executeQuery($query_det);
		$query_check = "SELECT * FROM dbo.ppob_trx_det WHERE ppob_trx_det.trx_id = '$trxno'";
		$res = $this->getRecordset($query_check);
		return $res;
	}
}

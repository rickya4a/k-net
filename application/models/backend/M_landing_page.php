<?php
/**
 * @author Ricky
 * @desc Landing Page Cashback
 */
class M_landing_page extends MY_Model {
  function __construct() {
    parent::__construct();
  }

  function listCashbackBelumTransfer() {
		$qry = "SELECT a.CNno, a.trcd, a.bank, a.bank_acc, a.status, a.bank_accname,a.dfno, a.fullnm,
					a.proc_tax * 100 as proc_tax, a.potongan_tax, a.nominal_cashback as cashback_sblm_pajak, a.biaya_bank,
					a.nominal_cashback - (a.nominal_cashback * a.proc_tax) as nominal_cashback
		        FROM CASHBACK_WATERH a
				WHERE status != '1' and status != '3' and status != '2' AND
				a.bank_acc != null AND bank_acc != '' AND a.no_telp != null AND a.no_telp != ''
				ORDER BY CNno";
		//AND a.no_telp != null AND a.no_telp != ''
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function listCashbackNotComplete() {
		$qry = "SELECT a.CNno, a.trcd, a.bank, a.bank_acc, a.bank_accname,a.dfno, a.fullnm,
					a.nominal_cashback - (a.nominal_cashback * a.proc_tax) as nominal_cashback,
					a.no_telp
		        FROM CASHBACK_WATERH a
				WHERE status != '1' and status != '3' AND
				(a.bank_acc = null OR bank_acc = '' OR a.no_telp = null OR a.no_telp = '')
				ORDER BY CNno";
				//OR a.no_telp = null OR a.no_telp = ''
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function listCashbackRejected($from, $to) {
		$qry = "SELECT a.BatchProses, a.dfno, a.fullnm, a.bank,
					a.bank_acc, a.bank_accname, a.status,
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
					a.remarkTolakan,
					SUM(a.nominal_cashback) as total_cashback
				FROM CASHBACK_WATERH a
				WHERE status = '2'
				GROUP BY a.BatchProses, a.dfno, a.fullnm, a.bank,
					a.bank_acc, a.bank_accname, a.status,
				    CONVERT(VARCHAR(10),a.tglproses, 121),
					a.remarkTolakan
				ORDER BY a.BatchProses"; //	echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function listCashbackTransferred($from, $to, $status) {
		$qry = "SELECT a.BatchProses, a.transferby,
				CONVERT(VARCHAR(10),a.transferdt, 121) as transferdt,
				SUM(a.nominal_cashback) as total_cashback
			FROM CASHBACK_WATERH a
			WHERE status = '$status'
			  AND CONVERT(VARCHAR(10),a.transferdt, 121) BETWEEN '$from' AND '$to'
			GROUP BY a.BatchProses, a.transferby,
			    CONVERT(VARCHAR(10),a.transferdt, 121)
			ORDER BY a.BatchProses"; //echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function listCashbackBatch($from, $to, $status) {
		$qry = "SELECT a.BatchProses, a.processBy,
				CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
				SUM(a.nominal_cashback) as total_cashback
			FROM CASHBACK_WATERH a
			WHERE status = '$status'
			  AND CONVERT(VARCHAR(10),a.tglproses, 121) BETWEEN '$from' AND '$to'
			GROUP BY a.BatchProses, a.processBy,
			    CONVERT(VARCHAR(10),a.tglproses, 121)
			ORDER BY a.BatchProses"; //echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function checkTransferByMemberID($id) {
		$qry = "SELECT a.dfno, a.BatchProses,
					CASE
							when a.[status] = '0' THEN null
							when a.[status] = '1' OR a.[status] = '2' THEN CONVERT(VARCHAR(10), A.batchdt , 120)
							--when a.[status] = '2' THEN 'Transfer Rejected'
							when a.[status] = '3' THEN CONVERT(VARCHAR(10), A.transferdt , 120)
					   END AS processdt,
					case
						when a.[status] = '0' THEN 'Pending'
						when a.[status] = '1' THEN 'Process'
						when a.[status] = '2' THEN 'Transfer Rejected'
						when a.[status] = '3' THEN 'Transfer To Account'
					end as status_transfer,
					a.cashback_sblm_pajak as nom_cashback,
					a.potongan_tax ,
					a.biaya_bank,
					a.cashback_setelah_pajak as nett_cashback
				FROM V_HILAL_CASHBACK_HYDRO_ALL a
				WHERE a.dfno = '$idmember'";
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function rekapListCashbackByMember($id) {
		$qry = "SELECT a.dfno, a.fullnm,
				  CASE
					 WHEN LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' AND B.orderno is not null
					 THEN b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
					 WHEN LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' AND B.orderno is null
					 THEN a.trcd
					 ELSE a.trcd
				  END AS ttp_no,
				a.CNno,
				a.bank, a.bank_acc, a.no_telp,
				a.bank_accname, a.BatchProses,
				CASE WHEN a.[status] = '0' THEN 'PENDING'
				WHEN a.[status] = '1' THEN 'DALAM PROSES'
				WHEN a.[status] = '2' THEN 'TOLAKAN TRANSFER'
				WHEN a.[status] = '3' THEN 'TRF TO ACCOUNT'
				END as status_name,
				a.[status], CONVERT(VARCHAR(10), a.transferdt, 111) as transferdt
				FROM CASHBACK_WATERH a
				LEFT OUTER JOIN sc_newtrh b ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
				where a.dfno = '$idmember'";
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function listTransferByBatchNo($no_batch) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses,
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt, a.remarkTolakan,
					SUM(a.nominal_cashback) as total_cashback
				FROM CASHBACK_WATERH a
				WHERE a.BatchProses = '$no_batch'
				GROUP BY a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses,
					CONVERT(VARCHAR(10),a.tglproses, 121), a.remarktolakan
				ORDER BY a.bank, a.dfno";
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function transferredComplete($listid, $transferdt) {

  }

  function rekapCashbackByIdMember($listtrx) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.bank_acc,
					a.bank_accname,
					a.biaya_bank,
					SUM(a.nominal_cashback - (a.proc_tax * a.nominal_cashback))- a.biaya_bank
                       as cashback_setelah_pajak
				FROM CASHBACK_WATERH a
				WHERE (a.status = '0' OR a.status = '2') and a.trcd IN ($list)
				GROUP BY a.dfno, a.fullnm, a.bank, a.bank_acc, a.bank_accname,a.biaya_bank
				ORDER BY a.bank, a.dfno";
		//echo $qry;
		//a.proc_tax * 100 as proc_tax, a.proc_tax,
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function saveBatchCashback($param) {
		$db_qryx = $this->load->database($this->setDB(2), true);

	    $noBatch = "BT".date("ymd-His");
		$listTrx = $datax['listTrx'];

		$db_qryx->trans_begin();

		$upd = "UPDATE CASHBACK_WATERH
				SET status = '1',
				    tglproses = '".$this->dateTime."' ,
					processBy = '".$this->username."',
					BatchProses = '$noBatch'
				WHERE trcd IN ($listTrx)";
		$db_qryx->query($upd);

		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			$return = jsonFalseResponse("Batch gagal..");
		} else {
			$db_qryx->trans_commit();
			$return = jsonTrueResponse(null, "Proses Batch berhasil, No : $noBatch ..");

		}

		return $return;
  }

  function listTransferByBatchNoPrint($listid) {

  }

  function rejectTransfer($param) {

  }

  function rejectTransferPrev($listid, $batchno) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses,
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
					SUM(a.nominal_cashback) as total_cashback
				FROM CASHBACK_WATERH a
				WHERE a.BatchProses = '$batchno' AND a.dfno IN ($lisdID)
				GROUP BY a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses,
					CONVERT(VARCHAR(10),a.tglproses, 121)
				ORDER BY a.bank, a.dfno";
		return $this->getRecordset($qry, null, $this->setDB(2));
  }

  function listTransferByBatchID($batch) {

  }
}
?>
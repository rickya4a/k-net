<?php

    class Update_model extends MY_Model{

        public function __construct(){

            parent::__construct();
        }

        public function getVoucherNo($vch){

            //$arr = array("response" => "false", "message" => "Invalid Voucher");

            $qry = "select voucherno, voucherkey, DistributorCode, BonusMonth, BonusYear, ExpireDate, VoucherAmt, CurrencyNote, voucheramt_ori
                    from tcvoucher
                    where voucherno = '$vch'";

            $result = $this->getRecordset($qry,null,$this->db2);

            //$arr = array("response" => "true", "arrayData" => $result);

            return $result;
        }

        public function getVoucherKey($vch){

            $qry = "select voucherno, voucherkey, DistributorCode, BonusMonth, BonusYear, ExpireDate, VoucherAmt, CurrencyNote, voucheramt_ori
                    from tcvoucher
                    where voucherkey = '$vch'";

            $result = $this->getRecordset($qry,null,$this->db2);

            return $result;
        }

        public function updateVoucher($vchkey, $curr, $currRp){

            $qry = "update tcvoucher
                    set VoucherAmt = '$currRp', CurrencyNote = 'ID', voucheramt_ori = '$curr'
                    where voucherkey = '$vchkey'";
            //echo $qry;
            $result = $this->executeQuery($qry, $this->db2);

            return $result;
        }
    }
<?php
class Be_hifi_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		//$this->tbl_hdr = "ppob_trx_hdr1";
		//$this->tbl_det = "ppob_trx_det1";
    }
	
	function saveVaMemb($data, $username) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		//$dbqryx->trans_start();
		
		//set data detail transaksi
		$arrData = array(
				'dfno'=> $username,
				'va_number'=> $data['va_number'],
				'fee' => $data['fee'],
				'kode_bank'=> $data['kode_bank'],
				'nama_bank' => $data['nama_bank']
		);
		//print_r($arrData);
		
		$hasil = $dbqryx->insert("ecomm_va_memb", $arrData);		
		return $hasil;
	}
	
	function checkDoubleVaMemb($userId, $va) {
		$qry = "SELECT * FROM ecomm_va_memb a WHERE a.dfno = '$userId' AND va_number = '$va'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function listVaMemb($userId) {
		$qry = "SELECT * FROM ecomm_va_memb a WHERE a.dfno = '$userId'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	//XL
	
	function paketDataXLBoughtFrom() {
		$qry = "SELECT a.kode_paket as trx_type, 
					'K-NET' as bought_from,
				COUNT(b.trx_type) AS 'qty_record'
				FROM ppob_xl_master a 
				LEFT JOIN ppob_trx_hdr b ON a.kode_paket = b.trx_type
				WHERE LEFT(b.trx_id, 2) = 'PX'
				GROUP BY a.kode_paket,a.nama_paket
				UNION
				SELECT a.kode_paket as trx_type, 
					'K-MART' as bought_from,
				COUNT(b.trx_type) AS 'qty_record'
				FROM ppob_xl_master a 
				LEFT JOIN ppob_trx_hdr b ON a.kode_paket = b.trx_type
				WHERE LEFT(b.trx_id, 2) = 'KX'
				GROUP BY a.kode_paket,a.nama_paket";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function lastTrxXL() {
		$qry = "SELECT TOP 5 a.trx_id, a.trx_type, c.nama_paket, a.memberid, a.membername,
				CONVERT(VARCHAR(20), A.createdt, 120) as createdt
				FROM ppob_trx_hdr a
				INNER JOIN ppob_xl_master c ON (a.trx_type = c.kode_paket)
				WHERE a.ppob_type = '4'
				ORDER BY a.createdt DESC";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function listRekapPaketDataXl() {
		/*$qry = "SELECT COUNT(a.trx_id) as qty_record, a.trx_type, c.nama_paket, c.kuota
				FROM ppob_trx_hdr a
				INNER JOIN ppob_xl_master c ON (a.trx_type = c.kode_paket)
				WHERE a.ppob_type = '4'
				GROUP BY a.trx_type, c.nama_paket, c.kuota";*/
		$qry = "SELECT a.kode_paket as trx_type, 
					   a.nama_paket, 
					   b.kuota, 
				       COUNT(c.trx_type) AS 'qty_record'
				FROM ppob_xl_master a
				LEFT JOIN (SELECT trx_type, sum(kuota) as kuota from ppob_xl_kuota_topup GROUP BY trx_type) b
					on a.kode_paket=b.trx_type
				LEFT JOIN ppob_trx_hdr c ON a.kode_paket = c.trx_type
				GROUP BY a.kode_paket,a.nama_paket, b.kuota";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function listXlWithNoMsidnOrReffId() {
		$qry = "SELECT a.trx_id, a.memberid, a.membername, a.trx_type,
					c.nama_paket, b.cust_no, b.reff_pay_id, c.kuota
				FROM ppob_trx_hdr a
				INNER JOIN ppob_xl_master c ON (a.trx_type = c.kode_paket)
				LEFT OUTER JOIN ppob_trx_det b ON (a.trx_id = b.trx_id)
				WHERE a.ppob_type = '4' and 
				  (b.cust_no is null OR b.cust_no = '' OR b.reff_pay_id is null OR b.reff_pay_id = '')";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getDetailTrxXlKmart($trx_id) {
		$qry = "SELECT a.trx_id, a.amount, a.member_no_hp,
					a.memberid, a.kode_paket, a.trx_dt,
					b.fullnm, b.tel_hp, c.price_cashback,
					0.06 * c.price_cashback as cashback_ori,
					0.06 * c.price_cashback as cashback_ref
				FROM db_ecommerce.dbo.ppob_xl_API_TWA a
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb b ON (a.memberid COLLATE SQL_Latin1_General_CP1_CS_AS = b.dfno)
				INNER JOIN db_ecommerce.dbo.ppob_xl_master c ON (a.kode_paket COLLATE SQL_Latin1_General_CP1_CS_AS = c.kode_paket) 
				WHERE a.trx_id = '$trx_id'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function insertXlFromKmart($dta, $resXL) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();
		
		$reff_pay_id = "REC";
		if($resXL->reff_id != null && $resXL->reff_id != "") {
			$reff_pay_id = $resXL->reff_id;
		}
		
		//set data detail transaksi
		$detail = array(
				'trx_id'=> $dta[0]->trx_id,
				'qty'=> 1,
				'nominal' => $dta[0]->amount,
				'cust_no'=> $dta[0]->member_no_hp,
				//'reff_pay_id' => $resXL->reff_id
				'reff_pay_id' => $reff_pay_id
		);
		//set data header transaksi 
		$header = array(
				'trx_id' => $dta[0]->trx_id,
				'memberid'=> $dta[0]->memberid,
				'membername'=> $dta[0]->fullnm,
				'member_no_hp'=> $dta[0]->tel_hp,
				'trx_type'=> $dta[0]->kode_paket,
				'createdt'=> $dta[0]->trx_dt,
				'nominal'=> $dta[0]->amount,
				'cashback_ori'=> $dta[0]->cashback_ori,
				'cashback_reff'=> $dta[0]->cashback_ref,
				'ppob_type'=> "4",
				'param_data' => json_encode($resXL)
		); 
		
		$dbqryx->insert($this->tbl_det, $detail);				
		$dbqryx->insert($this->tbl_hdr, $header);
		
		$upd = array(
			'status' => "1",
			//'reff_id' => $resXL->reff_id
			'reff_id' => $reff_pay_id
		);
		$dbqryx->where('trx_id',$dta[0]->trx_id);
        $dbqryx->update('ppob_xl_API_TWA',$upd);
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return false;
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return true;
        } 
	}
	
	function updateReffIdXL($trx_id, $reff_id) {
		$qry = "UPDATE db_ecommerce.dbo.ppob_trx_det SET reff_pay_id = '$reff_id' WHERE trx_id = '$trx_id'";
		//echo $qry;
		$res = $this->executeQuery($qry, "db_ecommerce");
		return $res;
	}
	
	function getListPrdXlPaketData() {
		$qry = "SELECT * FROM ppob_xl_master a WHERE a.status = '1'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getListKuotaXLFromDB() {
		$qry = "SELECT a.kode_paket, a.nama_paket, a.price, SUM(b.kuota)
				FROM ppob_xl_master a 
				INNER JOIN ppob_xl_kuota_topup b ON (a.kode_paket = b.trx_type)
				WHERE a.status = '1'
				GROUP BY a.kode_paket, a.nama_paket, a.price
				ORDER BY a.price DESC";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}

	function getDataMemberXLbyMsidn($no_hp) {
		$qry = "SELECT a.dfno, b.fullnm, msidn, a.loccd, a.status
		        FROM db_ecommerce.dbo.ppob_xl_msidn a
						LEFT JOIN klink_mlm2010.dbo.msmemb b
						ON a.dfno=b.dfno COLLATE SQL_Latin1_General_CP1_CI_AS
		        WHERE a.msidn = '$no_hp'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getDetailTempPPOBTrxByID($id) {
		$qry = "SELECT a.*, b.cust_no, b.reff_pay_id, c.bank_code_payment  
		        FROM ppob_trx_hdr_tempsgo a
		        LEFT JOIN ppob_trx_det_tempsgo b ON (a.trx_id = b.trx_id)
				LEFT JOIN ppob_trx_pay_tempsgo c ON (a.trx_id = c.trx_id)
		        WHERE a.trx_id = '$id'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getListVaTrxType() {
		$qry = "SELECT * FROM va_trx_type a WHERE a.show_status = '1'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getDetailPPOBTrxByID($id) {
		$hdr = $this->tbl_hdr;
		$det = $this->tbl_det;
		
		$qry = "SELECT a.*, c.product_name, b.cust_no, b.reff_pay_id  
		        FROM $hdr a
		        LEFT JOIN $det b ON (a.trx_id = b.trx_id)
		        LEFT JOIN ppob_sgo_trxtype c ON (a.trx_type = c.product_code)
		        WHERE a.trx_id = '$id'";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function getDetailTrxKnet($id) {
		$qry = "SELECT * FROM ecomm_trans_hdr a WHERE a.orderno = '$id'";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}
	
	function getFullNameActiveMember($field, $param) {
		$param = strtoupper($param);
		if($field == "dfno") {
			$str = "a.dfno = '$param' ";
		} else if($field == "fullnm") {
			$str = "a.fullnm LIKE '%$param%' ";
		}

		$slc = "select dfno, fullnm, tel_hp, email 
		        from msmemb a 
		        where $str
		        AND fullnm NOT LIKE 'RESIGN%' AND fullnm NOT LIKE 'TERMIN%'";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}
	
	function getListPpobXLTrx($arr) {
		$dtfrom = date("Y/m/d", strtotime($arr['from']));
		$dtto = date("Y/m/d", strtotime($arr['to']));
		$hdr = $this->tbl_hdr;
		$det = $this->tbl_det;
		
		$str = "a.ppob_type <> '1' AND a.ppob_type <> '2' AND";
		if($arr['trx_type'] != "all") {
			$str = "a.ppob_type = '$arr[trx_type]' AND ";
		}
		
		$qry = "SELECT a.trx_id, a.trx_type, 
					CASE 
				      WHEN a.ppob_type = '3' THEN 'PPOB' 
				      WHEN a.ppob_type = '4' THEN 'XL PAKET DATA'
				    END AS jenis_trx,
				    CASE 
				      WHEN a.ppob_type = '3' THEN e.product_name
				      WHEN a.ppob_type = '4' THEN f.[description] 
				    END AS shortdesc, 
				    CONVERT(VARCHAR(20), a.createdt , 120) as createdt,
				    a.nominal,
				    g.bankDisplayNm as pay_type
				FROM $hdr a 
				     LEFT JOIN $det c ON (a.trx_id = c.trx_id)
				     LEFT JOIN ppob_trx_pay d ON (a.trx_id = d.trx_id)
				     LEFT JOIN ppob_sgo_trxtype e ON (a.trx_type = e.product_code)
				     LEFT JOIN ppob_xl_master f ON (a.trx_type = f.kode_paket)
				     LEFT JOIN ecomm_bank g ON (d.bank_code_payment = g.id)
				WHERE $str a.memberid = '$arr[memberid]' AND
				  CONVERT(VARCHAR(10), A.createdt, 111) between '$dtfrom' and '$dtto'
				ORDER BY CONVERT(VARCHAR(20), a.createdt , 120)";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
	
	function importHifiTrxFromFile($data, $ppob = "1") {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$double = 0;
		$success = 0;
		$fail = 0;
		$failArray=array();
		$doubleArray=array();
		$successArray=array();
        
    	//START TRANSACTION	
    	$dbqryx->trans_start();
		//PPOB = 1, TRX HI-FI JATIS
		if($ppob == "1") {
			
			foreach($data as $dta) {
				//set data detail transaksi
				$detailTrx = array(
						'trx_id'=> $dta['trx_id'],
						'qty'=> 1,
						'nominal' => $dta['nominal'],
						'cust_no'=> $dta['cust_no'],
				);
				//set data header transaksi 
				$headerTrx = array(
						'trx_id' => $dta['trx_id'],
						'memberid'=> $dta['memberid'],
						'membername'=> strtoupper($dta['membername']),
						'member_no_hp'=> $dta['no_hp'],
						'trx_type'=> $dta['trx_type'],
						'createdt'=> $dta['trx_date'],
						'nominal'=> $dta['nominal'],
						'cashback_ori'=> $dta['cashback_ori'],
						'cashback_reff'=> $dta['cashback_reff'],
						'ppob_type'=> $ppob
			    ); 
				
				//QUERY FOR CHECK DOUBLE TRANSACTION ID
		        $dbqryx->select('trx_id');
				$dbqryx->from('ppob_trx_hdr1');
				$dbqryx->where('trx_id', $dta['trx_id']);
				$query = $dbqryx->get();	
				//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
				if($query->num_rows() > 0) {
					$double++;
					array_push($doubleArray, $dta['trx_id']);
					/*
					$dbqryx->where('trx_id', $dta->trx_id);
					$dbqryx->delete('ppob_trx_det1');
					$dbqryx->insert('ppob_trx_det1', $detailTrx);
					
					$dbqryx->where('trx_id', $dta->trx_id);
					$dbqryx->delete('ppob_trx_hdr1');
					$dbqryx->insert('ppob_trx_hdr1', $headerTrx); */
				} else {
					//$dbqryx->insert('ppob_trx_det1', $detailTrx);				
					//$dbqryx->insert('ppob_trx_hdr1', $headerTrx);
					
					$success++;
					array_push($successArray, $dta['trx_id']);
				}
			}
		} else {
			
		}
		
		//END TRANSACTION	
		/*$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			  "success" => $success,
			  //"arraySuccess" => $successArray,
			  "double" => $double,
			  "arrayDouble" => $doubleArray,
			);
        } */
        
        return array(
              "response" => "true",
			  "success" => $success,
			  //"arraySuccess" => $successArray,
			  "double" => $double,
			  "arrayDouble" => $doubleArray,
			);
	}

	function importHifiTrx($data, $ppob = "1") {
    	$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$double = 0;
		$success = 0;
		$fail = 0;
		$failArray=array();
		$doubleArray=array();
		$successArray=array();
        
    	//START TRANSACTION	
    	$dbqryx->trans_start();
		//PPOB = 1, TRX HI-FI JATIS
		if($ppob == "1") {
			
			foreach($data as $dta) {
				//set data detail transaksi
				$detailTrx = array(
						'trx_id'=> $dta->trx_id,
						'qty'=> 1,
						'nominal' => $dta->nominal,
						'cust_no'=> $dta->cust_no,
				);
				//set data header transaksi 
				$headerTrx = array(
						'trx_id' => $dta->trx_id,
						'memberid'=> $dta->memberid,
						'membername'=> strtoupper($dta->membername),
						'member_no_hp'=> $dta->no_hp,
						'trx_type'=> $dta->trx_type,
						'createdt'=> $dta->trx_date,
						'nominal'=> $dta->nominal,
						'cashback_ori'=> $dta->cashback_ori,
						'cashback_reff'=> $dta->cashback_reff,
						'ppob_type'=> $ppob
			    ); 
				
				//QUERY FOR CHECK DOUBLE TRANSACTION ID
		        $dbqryx->select('trx_id');
				$dbqryx->from('ppob_trx_hdr1');
				$dbqryx->where('trx_id', $dta->trx_id);
				$query = $dbqryx->get();	
				//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
				if($query->num_rows() > 0) {
					$double++;
					array_push($doubleArray, $dta->trx_id);
					/*
					$dbqryx->where('trx_id', $dta->trx_id);
					$dbqryx->delete('ppob_trx_det1');
					$dbqryx->insert('ppob_trx_det1', $detailTrx);
					
					$dbqryx->where('trx_id', $dta->trx_id);
					$dbqryx->delete('ppob_trx_hdr1');
					$dbqryx->insert('ppob_trx_hdr1', $headerTrx); */
				} else {
					$dbqryx->insert('ppob_trx_det1', $detailTrx);				
					$dbqryx->insert('ppob_trx_hdr1', $headerTrx);
					
					$success++;
					array_push($successArray, $dta->trx_id);
				}
			}
		} else {
			
		}
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			  "success" => $success,
			  //"arraySuccess" => $successArray,
			  "double" => $double,
			  "arrayDouble" => $doubleArray,
			);
        } 
        
    }

	function getListHifiTrxType() {
		$qry ="SELECT distinct(trx_type) as trx_type, trx_type as trx_name 
				from ppob_trx_hdr a 
				WHERE a.ppob_type = '1' AND (trx_type is not null AND trx_type != '')
				order by trx_type";
	    return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function getListSgoPPobTrxCat() {
	    $qry ="SELECT * FROM ppob_sgo_trx_cat";
	    return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function getListSgoPPobTrxType() {
	    $qry ="SELECT product_code as trx_type, product_name as trx_name 
			   from ppob_sgo_trxtype a 
			   order by a.product_code";
		//where cat = $type	   
	    return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function getListPaketDataXL() {
		$qry ="SELECT kode_paket as trx_type, nama_paket as trx_name 
			   from ppob_xl_master a 
			   order by a.kode_paket";
	    return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function getListReportHifiTrx($data) {
		$str = "";
		if($data['ppob_type'] != "") {
		   $str .= "AND ppob_type = '$data[ppob_type]' ";
		}
		
		if($data['hifi_trx_type'] != "") {
			$str .= "AND trx_type = '$data[hifi_trx_type]'";
		} 

		if($data['hifi_memberid'] != "") {
		  	$str .= "AND memberid = '$data[hifi_memberid]' ";
		}	
		
		if($data['hifi_custno'] != "") {
		  	$str .= "AND b.cust_no = '$data[hifi_custno]' ";
		}	

		//a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, CONVERT(varchar(16), a.createdt, 121) as trx_date
		//a.*, b.*
		$qry = "SELECT a.trx_id, a.memberid, 
                       b.cust_no,
		               a.membername, 
					   a.trx_type, 
					   a.nominal, a.cashback_ori,
                  CONVERT(varchar(16), a.createdt, 121) as trx_date
		        FROM ppob_trx_hdr a
		        LEFT OUTER JOIN ppob_trx_det b ON (a.trx_id = b.trx_id)
		        WHERE a.memberid <> 'IDSPAAA66834' AND 
				(a.createdt between '$data[from]' and '$data[to] 23:59:59') $str";
				//LEFT OUTER JOIN ppob_trx_det b ON (a.trx_id = b.trx_id)
		//echo $qry;
		return $this->getRecordset($qry, null, 'db_ecommerce');	
	}
	
	function getListCatSgoTrx() {
		$qry = "SELECT * FROM ppob_sgo_trx_cat WHERE status = '1'";
		return $this->getRecordset($qry, null, 'db_ecommerce');	
	}
	
	function listPpobTrxByCat($id) {
		$qry = "SELECT * FROM ppob_sgo_trxtype WHERE cat = $id";
		return $this->getRecordset($qry, null, 'db_ecommerce');	
	}
	
	function getPpobTrxById($id, $type) {
		if($type == "temp") {
			$hdr = "ppob_trx_hdr_tempsgo";
			$det = "ppob_trx_det_tempsgo";
		} else {
			$hdr = $this->tbl_hdr;
			$det = $this->tbl_det;
		}
		$qry = "SELECT a.*, b.cust_no, b.reff_pay_id 
		        FROM $hdr a
		        LEFT JOIN $det b ON (a.trx_id = b.trx_id)
		        WHERE a.trx_id = '$id'";
		return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function updateHeaderParamData($id, $param_data) {
		$hdr = $this->tbl_hdr;
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$qry = "UPDATE $hdr SET param_data = '$param_data' WHERE trx_id = '$id'";
		$res = $dbqryx->query($qry);
		return $res;
	}
	
	function saveTrxPpoB_Temp($trx_id, $header, $detail, $payment, $type) {
		//type trx PPOB SGO	
		if($type == "3" || $type == "4") {
			/*	
			$dbqryx->select('trx_id, trx_type, nominal, ');
			$dbqryx->from('ppob_trx_hdr_tempsgo');
			$dbqryx->where('trx_id',$trx_id);
			$query = $dbqryx->get();	
			*/
			$res = $this->getPpobTrxById($trx_id, "temp");
			//JIKA RECORD SUDAH ADA, DELETE KEMUDIAN INSERT BARU
			if($res != null) {
				$temp_id = "PS".randomNumber(8);
				$this->saveTrxPpoB_Temp($temp_id, $header, $detail, $payment, $type);
			} else {
				$dbqryx  = $this->load->database("db_ecommerce", TRUE);
				$dbqryx->trans_start();
				
				$dbqryx->insert('ppob_trx_det_tempsgo', $detail);				
				$dbqryx->insert('ppob_trx_hdr_tempsgo', $header);
				$dbqryx->insert('ppob_trx_pay_tempsgo', $payment);
				$recData = $this->getPpobTrxById($trx_id, "temp");
				
			}
		}
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			  "arrayData" => $recData
			);
        } 
	}
	
	function saveTrxPPOB_XlSgoVa_baru($trx_id, $arrData,  $type) {
		$header = $arrData['header'];
		$detail = $arrData['detail'];
		$payment = $arrData['payment'];
		$additional = null;
		
		if(array_key_exists("additional", $arrData)) {
			$additional = $arrData['additional'];
		}
		//print_r($additional);
		
		if($type == "3" || $type == "4") {
			$dbqryx  = $this->load->database("db_ecommerce", TRUE);
			$dbqryx->trans_start();
			
			//$dbqryx->insert($this->tbl_det, $detail);				
			//$dbqryx->insert($this->tbl_hdr, $header);
			//$dbqryx->insert('ppob_trx_pay', $payment);
			
			if($type == "4") {
				$trtype = "PX2";
				$dbqryx->insert('ppob_xl_trx',$additional);
					
			}		
		}
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			  "arrayData" => $trx_id
			);
        } 

	}

	function saveTrxPPOB_XlSgoVa($trx_id, $arrData,  $type) {
		$header = $arrData['header'];
		$detail = $arrData['detail'];
		$payment = $arrData['payment'];
		$additional = null;
		if(array_key_exists("additional", $arrData)) {
			$additional = $arrData['additional'];
		}	
		if($type == "3" || $type == "4") {
			
				$dbqryx  = $this->load->database("db_ecommerce", TRUE);
				$dbqryx->trans_start();
				
				$dbqryx->insert($this->tbl_det, $detail);				
				$dbqryx->insert($this->tbl_hdr, $header);
				$dbqryx->insert('ppob_trx_pay', $payment);
				//$recData = $this->getPpobTrxById($trx_id, "real");
				//if($recData != null) {
				if($type == "4") {
					$trtype = "PX2";
					$dbqryx->insert('ppob_xl_trx',$additional);
					
					$arr = array(
						"trcd"    => $trx_id,
						"trtype"  => $trtype,
						"effect"  => "-",
						"effect_acc"  => "+",
						"custtype"=> "M",
						"novac"   => getUserNovac(),
						//"dfno"    => $header['memberid'],
						"dfno"    => getUserID(),
						"docno"   => "",
						"refno"   => "",
						"amount"  => $header['nominal'],
						"createnm"=> getUserID(),
						"tipe_dk" => "D",
						"tipe_dk_acc" => "K"
					);
					$dbqryx->insert('va_cust_pay_det',$arr);
					$hasil = $this->getDataInsertXLTrxVa($trx_id);
				} else if($type == "3") {
					$trtype = "PB1";
					$arr = array(
						"trcd"    => $trx_id,
						"trtype"  => $trtype,
						"effect"  => "-",
						"effect_acc"  => "+",
						"custtype"=> "M",
						"novac"   => getUserNovac(),
						//"dfno"    => $header['memberid'],
						"dfno"    => getUserID(),
						"docno"   => "",
						"refno"   => "",
						"amount"  => $header['nominal'],
						"createnm"=> getUserID(),
						"tipe_dk" => "D",
						"tipe_dk_acc" => "K"
					);
					$dbqryx->insert('va_cust_pay_det',$arr);
					$hasil = $this->getDataInsertPpobTrxVa($trx_id);
				}
				
				
		}
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			  "arrayData" => $hasil
			);
        } 
	}

	function getDataInsertPpobTrxVa($trx_id) {
		$hdr = $this->tbl_hdr;
	    $det = $this->tbl_det;
		$qry = "SELECT a.trx_id, a.trx_type, b.reff_pay_id,
		               a.nominal, b.cust_no, a.param_data, CONVERT(varchar(16), a.createdt, 121) as createdt,
		               b.trx_id as ppob_det_trx_id, 
		               c.trx_id as ppob_pay_trx_id, 
		               e.trcd as va_trcd,
		               f.trcd as va_acc_trcd
		        FROM $hdr a 
			        LEFT JOIN $det b ON (a.trx_id = b.trx_id)
			        LEFT JOIN ppob_trx_pay c ON (a.trx_id  = c.trx_id)
			        LEFT JOIN va_cust_pay_det e ON (a.trx_id COLLATE SQL_Latin1_General_CP1_CI_AS = e.trcd)
			        LEFT JOIN va_cust_pay_det_acc f ON (a.trx_id COLLATE SQL_Latin1_General_CP1_CI_AS = f.trcd)
			    WHERE a.trx_id = '$trx_id'"; 
		$ppob = $this->getRecordset($qry, null, 'db_ecommerce');
		
		if($ppob != null) {
			//$arr['response'] = "true";
			$arr[0]['trx_id'] = $ppob[0]->trx_id;
			$arr[0]['trx_type'] = $ppob[0]->trx_type;
			$arr[0]['nominal'] = $ppob[0]->nominal;
			$arr[0]['cust_no'] = $ppob[0]->cust_no;
			$arr[0]['createdt'] = $ppob[0]->createdt;
			$arr[0]['param_data'] = json_decode($ppob[0]->param_data);
			
			$arr[0]['PPOB_HDR_STT'] = "0";
			if($ppob[0]->trx_id != null) {
				$arr[0]['PPOB_HDR_STT'] = "1";
			}
			
			$arr[0]['PPOB_DET_STT'] = "0";
			if($ppob[0]->ppob_det_trx_id != null) {
				$arr[0]['PPOB_DET_STT'] = "1";
			}
			
			$arr[0]['PPOB_PAY_STT'] = "0";
			if($ppob[0]->ppob_pay_trx_id != null) {
				$arr[0]['PPOB_PAY_STT'] = "1";
			}
			
			/*$arr[0]['PPOB_XL_STT'] = "0";
			if($ppob[0]->va_id_xl != null) {
				$arr[0]['PPOB_XL_STT'] = "1";
			}*/
			
			$arr[0]['PPOB_VA_STT'] = "0";
			if($ppob[0]->va_trcd != null) {
				$arr[0]['PPOB_VA_STT'] = "1";
			}
			
			$arr[0]['PPOB_VACC_STT'] = "0";
			if($ppob[0]->va_acc_trcd != null) {
				$arr[0]['PPOB_VACC_STT'] = "1";
			}
		}
		return $arr;
	}

	function getDataInsertXLTrxVa($trx_id) {
		
		
		$hdr = $this->tbl_hdr;
	    $det = $this->tbl_det;
		$qry = "SELECT a.trx_id, a.trx_type, 
		               a.nominal, b.cust_no, a.param_data, CONVERT(varchar(16), a.createdt, 121) as createdt,
		               b.trx_id as ppob_det_trx_id, 
		               c.trx_id as ppob_pay_trx_id, 
		               d.id_xl as va_id_xl, 
		               e.trcd as va_trcd,
		               f.trcd as va_acc_trcd
		        FROM $hdr a 
			        LEFT JOIN $det b ON (a.trx_id = b.trx_id)
			        LEFT JOIN ppob_trx_pay c ON (a.trx_id  = c.trx_id)
			        LEFT JOIN ppob_xl_trx d ON (a.trx_id = d.trcd)
			        LEFT JOIN va_cust_pay_det e ON (a.trx_id COLLATE SQL_Latin1_General_CP1_CI_AS = e.trcd)
			        LEFT JOIN va_cust_pay_det_acc f ON (a.trx_id COLLATE SQL_Latin1_General_CP1_CI_AS = f.trcd)
			    WHERE a.trx_id = '$trx_id'"; 
		/*echo "<pre>";		
		echo $qry;
		echo "</pre>";*/
		$ppob = $this->getRecordset($qry, null, 'db_ecommerce');
		
		if($ppob != null) {
			//$arr['response'] = "true";
			$arr[0]['trx_id'] = $ppob[0]->trx_id;
			$arr[0]['trx_type'] = $ppob[0]->trx_type;
			$arr[0]['nominal'] = $ppob[0]->nominal;
			$arr[0]['cust_no'] = $ppob[0]->cust_no;
			$arr[0]['createdt'] = $ppob[0]->createdt;
			$arr[0]['param_data'] = json_decode($ppob[0]->param_data);
			
			$arr[0]['PPOB_HDR_STT'] = "0";
			if($ppob[0]->trx_id != null) {
				$arr[0]['PPOB_HDR_STT'] = "1";
			}
			
			$arr[0]['PPOB_DET_STT'] = "0";
			if($ppob[0]->ppob_det_trx_id != null) {
				$arr[0]['PPOB_DET_STT'] = "1";
			}
			
			$arr[0]['PPOB_PAY_STT'] = "0";
			if($ppob[0]->ppob_pay_trx_id != null) {
				$arr[0]['PPOB_PAY_STT'] = "1";
			}
			
			$arr[0]['PPOB_XL_STT'] = "0";
			if($ppob[0]->va_id_xl != null) {
				$arr[0]['PPOB_XL_STT'] = "1";
			}
			
			$arr['PPOB_VA_STT'] = "0";
			if($ppob[0]->va_trcd != null) {
				$arr['PPOB_VA_STT'] = "1";
			}
			
			$arr[0]['PPOB_VACC_STT'] = "0";
			if($ppob[0]->va_acc_trcd != null) {
				$arr[0]['PPOB_VACC_STT'] = "1";
			}
		}
		return $arr;
		
		
	}
	
	function updateStatusResponseXL($trx_id, $redeem) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->set('param_data', $redeem);
		$dbqryx->where('trx_id', $trx_id);
		$dbqryx->update('ppob_trx_hdr');
	}
	
	function updateStatusResponseXL_tempsgo($trx_id, $redeem) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->set('param_data', $redeem);
		$dbqryx->where('trx_id', $trx_id);
		$dbqryx->update('ppob_trx_hdr_tempsgo');
	}
	
	function updateReffIdXL_new($reff_id, $trx_id, $redeem) {
		//ppob_trx_hdr set param_data where trx_id
		//ppob_trx_det set reff_pay_id where trx_id
		//ppob_xl_trx set reff_id, status = 0 where trcd = $trx_id
		
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();
		
		$dbqryx->set('param_data', $redeem);
		$dbqryx->where('trx_id', $trx_id);
		$dbqryx->update('ppob_trx_hdr');
		
		$dbqryx->set('reff_pay_id', $reff_id);
		$dbqryx->where('trx_id', $trx_id);
		$dbqryx->update('ppob_trx_det');
		
		$dbqryx->set('reff_id', $reff_id);
		$dbqryx->set('status', '0');
		$dbqryx->where('trcd', $trx_id);
		$dbqryx->update('ppob_xl_trx');
		
		//END TRANSACTION	
		$dbqryx->trans_complete();

	    //CHECK IF TRANSACTION PROCESS IS COMMITED
        if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false",
			  "message" => "Transaksi $trx_id berhasil dengan no reff XL : $reff_id"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			  "message" => "Transaksi sukses, no transaksi $trx_id"
			);
        } 
		
	}
	

	function insertPpobFromTemp2($orderid, $param_data, $paystatus=null) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();
		$hdr = $this->tbl_hdr;
		$det = $this->tbl_det;
		
		$reff_pay_id = null;
		$reff_trx = json_decode($param_data);
		if(property_exists($reff_trx, "ref_id")) {
			//$reff_pay_id = $reff_trx->ref_id;
			$reff_pay_id = "REC";
			if($reff_trx->ref_id != null && $reff_trx->ref_id != "") {
				$reff_pay_id = $reff_trx->ref_id;
			}
		}
		
		if(property_exists($reff_trx, "reff_id")) {
			//$reff_pay_id = $reff_trx->reff_id;
		    $reff_pay_id = "REC";
			if($reff_trx->reff_id != null && $reff_trx->reff_id != "") {
				$reff_pay_id = $reff_trx->reff_id;
			}
		}	
		
		if($paystatus != null) {
			$commision = (double) $paystatus->commission_fee;
			
			$cashback_ori = 0.6 * $commision;
			$cashback_reff = 0.6 * $commision;
			$ins = "INSERT INTO $hdr (trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, cashback_ori, cashback_reff, ppob_type, amount_sgo, param_data)	
		            SELECT trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, '$cashback_ori', '$cashback_reff', ppob_type, amount_sgo, '$param_data' 
		            FROM ppob_trx_hdr_tempsgo WHERE trx_id = '$orderid'"; 
			//echo "Commision: ".$commision;
		} else {
			$ins = "INSERT INTO $hdr (trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, cashback_ori, cashback_reff, ppob_type, amount_sgo, param_data)	
		            SELECT trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, cashback_ori, cashback_reff, ppob_type, amount_sgo, '$param_data' 
		            FROM ppob_trx_hdr_tempsgo WHERE trx_id = '$orderid'"; 
		}
		
		
		$res = $dbqryx->query($ins);
		$ins2 = "INSERT INTO $det (trx_id, memberid_target, qty, nominal, no_hp_simcard, cust_no, reff_pay_id)
		         SELECT trx_id, memberid_target, qty, nominal, no_hp_simcard, cust_no, '$reff_pay_id' 
		         FROM ppob_trx_det_tempsgo WHERE trx_id = '$orderid'"; 
		//echo $ins2;
		$res2 = $dbqryx->query($ins2);
		
		$ins3 = "INSERT INTO ppob_trx_pay (trx_id, bank_code_payment, charge_admin, docno, payamt)
		         SELECT trx_id, bank_code_payment, charge_admin, docno, payamt
		         FROM ppob_trx_pay_tempsgo WHERE trx_id = '$orderid'";
		$res3 = $dbqryx->query($ins3);
		//END TRANSACTION	
		$dbqryx->trans_complete();
		
		if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			);
        } 
	}

	function insertPpobFromTemp($orderid) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		$dbqryx->trans_start();
		$hdr = $this->tbl_hdr;
		$det = $this->tbl_det;
		$ins = "INSERT INTO $hdr (trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, ppob_type, amount_sgo)	
		        SELECT trx_id, memberid, membername, member_no_hp, trx_type,createdt, nominal, ppob_type, amount_sgo 
		        FROM ppob_trx_hdr_tempsgo WHERE trx_id = '$orderid'"; 
		$res = $dbqryx->query($ins);
		$ins2 = "INSERT INTO $det (trx_id, memberid_target, qty, nominal, no_hp_simcard, cust_no, reff_pay_id)
		         SELECT trx_id, memberid_target, qty, nominal, no_hp_simcard, cust_no, reff_pay_id 
		         FROM ppob_trx_det_tempsgo WHERE trx_id = '$orderid'"; 
		$res2 = $dbqryx->query($ins2);
		//END TRANSACTION	
		$dbqryx->trans_complete();
		
		if ($dbqryx->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $dbqryx->trans_rollback();
            return array(
              "response" => "false"
			);
        } else {
            //if everything went right, insert the data to the database
            $dbqryx->trans_commit();
            return array(
              "response" => "true",
			);
        } 
	}


	function SaveTRXPaket($data)
	{
		$this->db->trans_start();
		$this->db->insert('ppob_xl_trx',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			//if something went wrong, rollback everything
			$this->db->trans_rollback();
			return FALSE;
		} else {
			//if everything went right, delete the data from the database
			$this->db->trans_commit();
			return TRUE;
		}
	}

	function getDescXL($id) {
		$qry = "SELECT * FROM ppob_xl_master WHERE kode_paket = '$id'";
		return $this->getRecordset($qry, null, 'db_ecommerce');
	}

	public function getHeader1($id){
		$this->db->select('*')
			->from('ppob_trx_hdr')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getHeader3($id){
		$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							b.product_code, b.product_name')
			->from('ppob_trx_hdr a')
			->join('ppob_sgo_trxtype b', 'a.trx_type = b.product_code')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getHeader4($id){
		$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							a.member_no_hp, a.createdt, b.kode_paket, b.nama_paket')
			->from('ppob_trx_hdr a')
			->join('ppob_xl_master b', 'a.trx_type = b.kode_paket')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getDetail($id){
		$this->db->select('*')
			->from('ppob_trx_det')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getPayment1($id){
		$this->db->select('*')
			->from('ppob_trx_pay')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getPayment($id){
		$this->db->select('a.bank_code_payment, a.charge_admin, a.payamt,
						   b.bankDesc, b.bankDisplayNm')
			->from('ppob_trx_pay a')
			->join('ecomm_bank b', 'a.bank_code_payment = b.id', 'left')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getPayKMart($id){
		$this->db->select('a.createNM, b.cashtype, b.cashnm')
			->from('ppob_xl_API_TWA a')
			->join('ppob_xl_paypaket_KMART b', 'a.trx_id = b.trx_id')
			->where('a.trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}
}

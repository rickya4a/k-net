<?php
class Be_office_model extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();

	}

	function checkBVMember($id) {
		$qry = "";
	}


	function getlistAllInputOffice() {
//		$qry = "SELECT
//				 *
//				FROM
//				  master_etiket
//				ORDER BY  createdt DESC
//				";
//		$res = $this->getRecordset($qry, NULL, $this->db);

		$result = array();
		$this->db->select('*')
			->from('klink_mlm2010.dbo.lounge_h')
			->order_by('createdt','desc');

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;


//		return $res;
	}

	function saveInputOffice() {
		$id=$this->uuid->v4();

		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO klink_mlm2010.dbo.lounge_h (
				id,
				dfno,
				name,
				rank,
				status,
				remark,
				createdt,
				payment,
				payment_s,
				qty,
				createnm


				)
		        VALUES ('".$id."', '$data[distributorcode]', '$data[fullnm]',
		              	'$data[shortnm]','0','$data[remark]',
					  	GETDATE(),'$data[bayar]','$data[bayar_s]','$data[qty]',


		               '".$this->username."')";
		$query = $this->db->query($qry);


//		echo $this->db->last_query();
//		if(($this->db->affected_rows() != 1) ) {
//			throw new Exception("Save failed..!!", 1);
//		}
		return $id;
	}
	function saveInputOffice2() {
		print_r($this->input->post(NULL, TRUE));
		$id=$this->uuid->v4();
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO klink_mlm2010.dbo.lounge_h (
				id,
				dfno,
				name,
				rank,
				status,
				remark,
				createdt,
				payment,
				payment_s,
				createnm


				)
		        VALUES ('".$id."', '$data[distributorcode]', '$data[fullnm]',
		              	'$data[shortnm]','0','$data[remark]',
					  	GETDATE(),'$data[bayar]','$data[bayar_s]','$data[qty]',


		               '".$this->username."')";
		$query = $this->db->query($qry);


//		echo $this->db->last_query();
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $id;
	}

	function getListAllPriceCode() {
		$qry = "SELECT * FROM master_prd_pricecode";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data price code is empty..!", 1);
		}
		return $res;
	}




	function getNota($id){
		$slc = "SELECT *
				from lounge_h
				where id='$id' ";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}


	function getReportBonus($idmember){


		$slc = "SELECT TOP 1 a.distributorcode, b.fullnm, a.adjustedrank, c.shortnm,
				a.bonusmonth, a.bonusyear, a.psharingincome, a.psharingpoints
				from tbonus a
					 left outer join msmemb b on a.distributorcode=b.dfno
					 inner join msrank c on CAST(a.adjustedrank AS int)=c.[level]
					  and a.distributorcode='$idmember'
					order by bonusyear DESC ";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;

		// EDIT HILAL ON 2018-02-01

		/*
		//1. CHECK APAKAH DISTRIBUTOR TERDAFTAR ATAU TIDAK
		$qryMem = "SELECT XX.dfno, XX.fullnm, XX.sex, XX.jointdt, XX.idno, XX.RANKX, C.SHORTNM
					FROM (
					SELECT A.dfno, A.fullnm, A.sex, A.jointdt, A.idno, MAX(CAST(B.adjustedrank AS INT)) AS RANKX
					FROM msmemb A
						 LEFT OUTER JOIN tbonus B ON A.dfno=B.distributorcode
					WHERE A.dfno='$idmember'     
					GROUP BY A.dfno, A.fullnm, A.sex, A.jointdt, A.idno
					) XX INNER JOIN msrank C ON XX.RANKX=C.LEVEL";
		
		
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
		 * 
		 * 
		 */
		//END EDIT HILAL
	}

	function getStokis($id){
		$slc = "SELECT loccd as distributorcode, fullnm, 'Stockist' as shortnm
				from mssc
				where loccd='$id' ";
		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}


	function getHilal($idmember){

		////ORI	
		/*$sqlGetPeriod = "select a.currperiod, 
						   		  DATEADD(m, -1, a.currperiod) as period, 
						   		  month(DATEADD(m, -1, a.currperiod)) as bonusmonth, 
						   		  year(DATEADD(m, -1, a.currperiod)) as bonusyear 
						    from syspref a";
		*/

		$sqlGetPeriod = "SELECT top 1 CAST(A.bonusyear AS VARCHAR(4)) + '-' + RIGHT( ('0' + CAST(A.bonusmonth AS VARCHAR(4))), 2) + '-01' AS period,
									CAST(A.bonusyear AS VARCHAR(4)) + '-' + RIGHT( ('0' + CAST(A.bonusmonth AS VARCHAR(4))), 2) + '-01' AS currperiod,
									a.bonusyear, a.bonusmonth 
						 FROM tbonussummary a
						 order by a.bonusyear desc, a.bonusmonth desc";
		//--, (select b.currperiod from syspref b ) as curreriod

		//echo "sqlGetPeriod = $sqlGetPeriod";
		
		$resPeriod = $this->getRecordset($sqlGetPeriod ,null,$this->db2);
		$isFree = 0;
		$data['tblCheck'] = null;
		$data['isFree'] = null;

		if ($resPeriod != 0 && $resPeriod != null) { //CEK PERIODE BONUS YANG AKAN DICEK
			foreach($resPeriod as $row) {
				$currperiod = $row->currperiod;
				$period = $row->period;
				$bonusmonth = $row->bonusmonth;
				$bonusyear = $row->bonusyear;

				//MULAI CHECK
				//1. CHECK APAKAH DISTRIBUTOR TERDAFTAR ATAU TIDAK

				$qryMem = "SELECT XX.dfno, XX.fullnm, XX.sex, XX.jointdt, XX.idno, XX.RANKX, C.SHORTNM, XX.netincome
								FROM ( SELECT A.dfno, A.fullnm, A.sex, A.jointdt, 
											  A.idno, CAST(B.adjustedrank AS INT) AS RANKX, 
										      B.bonusmonth, B.bonusyear, B.netincome 
									   FROM msmemb A 
								       		LEFT OUTER JOIN tbonus B ON A.dfno=B.distributorcode 
								       WHERE A.dfno = '$idmember' AND B.bonusmonth = $bonusmonth AND B.bonusyear = $bonusyear 
								       GROUP BY A.dfno, A.fullnm, A.sex, A.jointdt, 
											  A.idno, CAST(B.adjustedrank AS INT), 
										      B.bonusmonth, B.bonusyear, B.netincome ) XX 
								INNER JOIN msrank C ON XX.RANKX=C.LEVEL";

				/*
                 *
                 $qryMem = "SELECT XX.dfno, XX.fullnm, XX.sex, XX.jointdt, XX.idno, XX.RANKX, C.SHORTNM,
                                    XX.bonusmonth, XX.bonusyear, XX.netincome, XX.totBonus, XX.totBonus_Nett
                            FROM (
                            SELECT A.dfno, A.fullnm, A.sex, A.jointdt, A.idno, CAST(B.adjustedrank AS INT) AS RANKX,
                                   B.bonusmonth, B.bonusyear, B.netincome, D.totBonus, d.totBonus_Nett
                            FROM msmemb A
                                 LEFT OUTER JOIN tbonus B ON A.dfno=B.distributorcode
                                 LEFT OUTER JOIN tbonus_hilal_nett D ON A.dfno=D.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
                                                                     and B.bonusmonth=d.bonusmonth and B.bonusyear=d.bonusyear
                            WHERE A.dfno = 'IDJHID000065' AND B.bonusmonth = 12 AND B.bonusyear = 2017
                            GROUP BY A.dfno, A.fullnm, A.sex, A.jointdt, A.idno, CAST(B.adjustedrank AS INT),
                                   B.bonusmonth, B.bonusyear, B.netincome, D.totBonus, d.totBonus_Nett
                            ) XX INNER JOIN msrank C ON XX.RANKX=C.LEVEL";
                 *
                 */

				// echo $qryMem;
				$result = $this->getRecordset($qryMem,null,$this->db2);
				if($result==null) {
						
					////ORI	
					/*$sqlGetPeriod = "select a.currperiod, 
									   		  DATEADD(m, -1, a.currperiod) as period, 
									   		  month(DATEADD(m, -1, a.currperiod)) as bonusmonth, 
									   		  year(DATEADD(m, -1, a.currperiod)) as bonusyear 
									    from syspref a";
					*/

					$sqlGetPeriod = "SELECT top 1 CAST(A.bonusyear AS VARCHAR(4)) + '-' + RIGHT( ('0' + CAST(A.bonusmonth AS VARCHAR(4))), 2) + '-01' AS period,
												CAST(A.bonusyear AS VARCHAR(4)) + '-' + RIGHT( ('0' + CAST(A.bonusmonth AS VARCHAR(4))), 2) + '-01' AS curreriod,
	   											a.bonusyear, a.bonusmonth 
									 FROM tbonussummary a
									 order by a.bonusyear desc, a.bonusmonth desc";
					//--, (select b.currperiod from syspref b ) as curreriod
					
					$resPeriod = $this->getRecordset($sqlGetPeriod, null, $this->db2);
					if ($resPeriod != 0 && $resPeriod != null) {
						foreach ($resPeriod as $row) {
							$currperiod = $row->currperiod;
							$period = $row->period;
							$bonusmonth = $row->bonusmonth;
							$bonusyear = $row->bonusyear;

							//MULAI CHECK
							//1. CHECK APAKAH DISTRIBUTOR TERDAFTAR ATAU TIDAK

							$qryMem = "SELECT XX.dfno, XX.fullnm, XX.sex, XX.jointdt, XX.idno, XX.RANKX, C.SHORTNM, XX.netincome
								FROM ( SELECT A.dfno, A.fullnm, A.sex, A.jointdt,
											  A.idno, CAST(B.adjustedrank AS INT) AS RANKX,
										      B.bonusmonth, B.bonusyear, B.netincome
									   FROM msmemb A
								       		LEFT OUTER JOIN tbonus B ON A.dfno=B.distributorcode
								       WHERE A.dfno = '$idmember' AND B.bonusmonth = $bonusmonth AND B.bonusyear = $bonusyear
								       GROUP BY A.dfno, A.fullnm, A.sex, A.jointdt,
											  A.idno, CAST(B.adjustedrank AS INT),
										      B.bonusmonth, B.bonusyear, B.netincome ) XX
								INNER JOIN msrank C ON XX.RANKX=C.LEVEL";
							$result = $this->getRecordset($qryMem, null, $this->db2);

						}
					}
				}
				//print_r($result);

				if ($result != 0 or $result !=null) { //jika terdaftar sebagai member, maka cek selanjutnya
					foreach($result as $row) {
						$sex = $row->sex;
						$rank = $row->RANKX;
						$rankDesc = $row->SHORTNM;
						$dfno = $row->dfno;
						$fullnm = $row->fullnm;
						$jointdt = $row->jointdt;

						if($sex != null || $sex !=''){ //2. CEK JIKA GENDER TIDAK NULL
							if($sex == 'M'){ //3A. CEK JIKA GENDER ADALAH PRIA
								$sqlCekPPV = "SELECT TOP 1 A.distributorcode, A.bonusmonth, A.bonusyear, A.netincome, A.ppv, A.pgpv,
					    									   a.psharingpoints, CAST(a.adjustedrank AS INT) AS RANKX, b.SHORTNM, 
					    									   b.pgpv as minpgpv
												  FROM tbonus a
												  		INNER JOIN msrank b ON CAST(a.adjustedrank AS INT)=b.LEVEL
												  WHERE a.distributorcode='$idmember' AND A.bonusmonth = $bonusmonth AND A.bonusyear = $bonusyear
												  ORDER BY A.bonusyear DESC, A.bonusmonth DESC";

								//echo $sqlCekPPV;
								$resPPV = $this->getRecordset($sqlCekPPV,null,$this->db2);

								if ($resPPV != 0) { //cek jika BV bulan sebelumnya ada di table tsonus, maka teruskan cek selanjutnya

									$ppvHdr = "<th>BV</th>"; //var untuk hasil cek 400 BV, Header
									$ppvVal = "<td>0</td>"; //var untuk hasil cek 400 BV, Hasil Value
									$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header
									$rankVal = "<td>0</td>"; //var untuk hasil cek Rank, Hasil Value
									$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
									$dynFundVal = "<td>0</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value
									$pgpvHdr = "<th>PGBV</th>"; //var untuk hasil cek PGPV, Header
									$pgpvVal = "<td>0</td>"; //var untuk hasil cek PGPV, Hasil Value
									$gbvLegHdr = "<th>Total Leg 3000</th>"; //var untuk hasil cek GBV, Header
									$gbvLegVal = "<td>0</td>"; //var untuk hasil cek GBV, Hasil Value
									$lcBonusHdr = "<th width=\"20%\">Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
									$lcBonusVal = "<td>0</td>"; //var untuk hasil cek Bonus LC, Hasil Value



									foreach($resPPV as $rowPPV) {
										$bonusmonth = $rowPPV->bonusmonth;
										$bonusyear = $rowPPV->bonusyear;
										$netincome = $rowPPV->netincome;
										$ppv = $rowPPV->ppv;
										$pgpv = $rowPPV->pgpv;
										$minpgpv = $rowPPV->minpgpv;
										$psharingpoints = $rowPPV->psharingpoints;
										$RANKX = $rowPPV->RANKX;
										$SHORTNM = $rowPPV->SHORTNM;

										if($ppv >= 400){ //4a. jika PPV >= 400, maka cek selanjutnya

											//$ppvHdr = "<th>Rank</th>"; //var untuk hasil cek 400 BV, Header
											$ppvVal = "<td><div align=right>$ppv</div></td>"; //var untuk hasil cek 400 BV, Hasil Value
											//$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header
											$rankVal = "<td>$rankDesc</td>"; //var untuk hasil cek Rank, Hasil Value

											if($rank <= 6 ) { //5A. jika peringkat Manager kebawah, maka CEK 1 ATAU 2 POIN DYNAMIC FUND

												if($psharingpoints >= 1){ //6A. jika mencapai dynamic fund, maka GRATIS

													//$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
													$dynFundVal = "<td>$psharingpoints Points DF</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value

													$msg = "Hai $idmember, Peringkat Anda = $rankDesc, </br>Dynamic Fund ANDA = $psharingpoints, </br>MEMENUHI PERSYARATAN DYNAMIC FUND dipersilahkan masuk secara GRATIS.";
													$isFree = 1;
												}else{ //6A. jika TIDAK mencapai dynamic fund, maka bayar 50RB

													//$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
													$dynFundVal = "<td>No Points DF</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value

													$msg = "Hai $idmember, Peringkat Anda = $rankDesc, </br>Anda diharuskan membayar 50.000 karena TIDAK MEMENUHI PERSYARATAN Dynamic Fund.";
													$isFree = 0;
												}

											}elseif($rank == 7 || $rank == 8 || $rank == 9){ //5B. jika peringkat SM s/d EM, maka cek PGBV

												if($pgpv >= $minpgpv){ //Jika pgpv memenui persyaratan pgpv minimal, maka GRATIS

													//$pgpvHdr = "<th>PGBV</th>"; //var untuk hasil cek PGPV, Header
													$pgpvVal = "<td><div align=right>$pgpv</div></td>"; //var untuk hasil cek PGPV, Hasil Value

													$msg = "Peringkat Anda = $rankDesc, </br>PGBV ANDA = $pgpv, </br>MEMENUHI PERSYARATAN MINIMAL PGBV dipersilahkan masuk secara GRATIS.";
													$isFree = 1;
												}elseif($pgpv < $minpgpv){

													//$pgpvHdr = "<th>PGBV</th>"; //var untuk hasil cek PGPV, Header
													$pgpvVal = "<td>PGBV Anda kurang dari $minpgpv</td>"; //var untuk hasil cek PGPV, Hasil Value

													$msg = "Peringkat Anda = $rankDesc, PGBV ANDA = $pgpv, </br>Anda diharuskan membayar 50.000 karena TIDAK MEMENUHI PERSYARATAN MINIMAL PGBV.";
													$isFree = 0;
												}

											}elseif($rank == 10 || $rank == 11){ //5C. jika peringkat DM s/d CM, maka cek minimal 3 LEG masing-masing 3000

												$sqlGPV = "SELECT TOP 3 a.distributorcode, a.bonusyear, a.bonusmonth, a.distributorcode, a.ppv, a.pgpv, a.gpv
																FROM tbonus A
																WHERE  A.bonusmonth = $bonusmonth AND A.bonusyear = $bonusyear  
																	  AND A.sponsorcode='$idmember'  AND a.gpv >= 3000
																ORDER BY A.bonusyear DESC, A.bonusmonth DESC, A.gpv DESC";
												$resGPV = $this->getDataResult($sqlGPV ,null,$this->db2);

												$totGBV3000 = $resGPV['rowCount'];
												if($totGBV3000 >= 3){ // jika downline min 3 dengan gpv min 3000, maka GRATIS

													//$gbvLegHdr = "<th>Total Leg 3K</th>"; //var untuk hasil cek GBV, Header
													$gbvLegVal = "<td><div align=right>$totGBV3000</div></td>"; //var untuk hasil cek GBV, Hasil Value

													$msg = "Peringkat Anda = $rankDesc, </br>LEG 3000 Anda sebanyak = $totGBV3000, </br>MEMENUHI PERSYARATAN MINIMAL OMSET 3000 3 LEG, </br>Anda dipersilahkan masuk secara GRATIS.";
													$isFree = 1;
												}else{ // jika downline dengan gpv min 3000 kurang dari 3, maka BAYAR

													//$gbvLegHdr = "<th>Total Leg 3K</th>"; //var untuk hasil cek GBV, Header
													$gbvLegVal = "<td>Leg 3000 Anda hanya = $totGBV3000</td>"; //var untuk hasil cek GBV, Hasil Value

													$msg = "Peringkat Anda = $rankDesc, </br>LEG 3000 Anda sebanyak = $totGBV3000, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN MINIMAL OMSET 3000 3 LEG.";
													$isFree = 0;
												}

											}elseif($rank > 11){ //5C. jika peringkat CA Above, maka cek minimal bonus

												/*
												$sqlBonusLC = "SELECT A.dfno, A.fullnm, A.totBonus, A.totBonus_Nett, A.bonusmonth, A.bonusyear
																	FROM tbonus_hilal_nett a
																	WHERE a.dfno='$idmember' AND A.bonusmonth=$bonusmonth AND A.bonusyear=$bonusyear";
												 */
												$sqlBonusLC = "SELECT a.distributorcode as dfno, a.fullnm, a.totbns as totBonus, a.totbns as totBonus_Nett, a.bonusmonth, a.bonusyear
																FROM HILAL_STBNS_HDR a
																WHERE a.distributorcode='$idmember' AND A.bonusmonth=$bonusmonth AND A.bonusyear=$bonusyear";
																	
												//echo "sqlBonusLC = $sqlBonusLC";
												$resBonusLC = $this->getRecordset($sqlBonusLC ,null,$this->db2);

												$totBonus_NettLC = 0;
												if ($resBonusLC != 0 && $resBonusLC != null) { //cek jika BV bulan sebelumnya ada di table tsonus, maka teruskan cek selanjutnya

													foreach($resBonusLC as $rowLC) {
														//print_r($rowLC);
														$totBonusLC = $rowLC->totBonus;
														$totBonus_NettLC = $rowLC->totBonus_Nett;

														$totBonus_Nett = number_format($totBonus_NettLC,2,",",".");
														//slc = 10jt, glc = 25jt, plc = 50jt, elc = 80jt
														if($totBonus_NettLC >= 10000000){ //jika bonus CA ABOUVE minimal 10jt/SLC, maka GRATIS

															//$lcBonusHdr = "<th>Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
															$lcBonusVal = "<td><div align=right>$totBonus_Nett</div></td>"; //var untuk hasil cek Bonus LC, Hasil Value

															$msg = "Peringkat Anda = $rankDesc, </br>Bonus Anda Bulan lalu = $totBonus_Nett, </br>MEMENUHI PERSYARATAN MINIMAL BONUS SLC, </br>Anda dipersilahkan masuk secara GRATIS.";
															$isFree = 1;
														}else{ //jika bonus CA ABOVE kurang dari 25jt, maka BAYAR 50RB

															//$lcBonusHdr = "<th>Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
															$lcBonusVal = "<td>Bonus Anda kurang dari SLC</td>"; //var untuk hasil cek Bonus LC, Hasil Value

															$msg = "Peringkat Anda = $rankDesc, </br>Bonus Anda Bulan lalu = $totBonus_Nett, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN MINIMAL BONUS SLC.";
															$isFree = 0;
														}
													}

												}else{ //jika bonus CA ABOVE kurang dari 80jt, maka BAYAR 50RB

													//$lcBonusHdr = "<th>Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
													$lcBonusVal = "<td>Bonus Anda kurang dari SLC</td>"; //var untuk hasil cek Bonus LC, Hasil Value

													$msg = "Peringkat Anda = $rankDesc, </br>Bonus Anda Bulan lalu = $totBonus_NettLC, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN MINIMAL BONUS SLC.";
													$isFree = 0;
												}

											}
										}else{ //4B. jika PPV < 400, maka bAYAR 50RB

											//$ppvHdr = "<th>Rank</th>"; //var untuk hasil cek 400 BV, Header
											$ppvVal = "<td>BV Anda Kurang dari $ppv</td>"; //var untuk hasil cek 400 BV, Hasil Value
											//$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header

											$msg = "Hai $idmember, Peringkat Anda = $rankDesc, </br>Anda diharuskan membayar 50.000 </br>karena BULAN SEBELUMNYA ANDA TIDAK MENCAPAI 400 BV";
											$isFree = 0;
										}
									}

								}else{

									//$ppvHdr = "<th>Rank</th>"; //var untuk hasil cek 400 BV, Header
									$ppvVal = "<td>BV Anda Kurang dari $ppv</td>"; //var untuk hasil cek 400 BV, Hasil Value
									//$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header

									$msg = "Tidak ada pembelanjaan bulan sebelumnya.";
									$isFree = 0;
								}



							}
							elseif($sex='F'){ //3B. JIKA GENDER ADALAH WANITA


								$sqlCekPPV = "SELECT TOP 1 A.distributorcode, A.bonusmonth, A.bonusyear, A.netincome, A.ppv, A.pgpv,
					    									   a.psharingpoints, CAST(a.adjustedrank AS INT) AS RANKX, b.SHORTNM, 
					    									   b.pgpv as minpgpv
												  FROM tbonus a
												  		INNER JOIN msrank b ON CAST(a.adjustedrank AS INT)=b.LEVEL
												  WHERE a.distributorcode='$idmember' AND A.bonusmonth = $bonusmonth AND A.bonusyear = $bonusyear
												  ORDER BY A.bonusyear DESC, A.bonusmonth DESC";

								//echo $sqlCekPPV;
								$resPPV = $this->getRecordset($sqlCekPPV,null,$this->db2);

								if ($resPPV != 0) { //cek jika BV bulan sebelumnya ada di table tsonus, maka teruskan cek selanjutnya

									$ppvHdr = "<th>BV</th>"; //var untuk hasil cek 400 BV, Header
									$ppvVal = "<td>0</td>"; //var untuk hasil cek 400 BV, Hasil Value
									$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header
									$rankVal = "<td>0</td>"; //var untuk hasil cek Rank, Hasil Value
									$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
									$dynFundVal = "<td>0</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value
									$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
									$lbcVal = "<td>0</td>"; //var untuk hasil cek LBC, Hasil Value
									$pgpvHdr = "<th>PGBV</th>"; //var untuk hasil cek PGPV, Header
									$pgpvVal = "<td>0</td>"; //var untuk hasil cek PGPV, Hasil Value
									$gbvLegHdr = "<th>Total Leg 3000</th>"; //var untuk hasil cek GBV, Header
									$gbvLegVal = "<td>0</td>"; //var untuk hasil cek GBV, Hasil Value
									$lcBonusHdr = "<th width=\"20%\">Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
									$lcBonusVal = "<td>0</td>"; //var untuk hasil cek Bonus LC, Hasil Value


									foreach($resPPV as $rowPPV) {
										$bonusmonth = $rowPPV->bonusmonth;
										$bonusyear = $rowPPV->bonusyear;
										$netincome = $rowPPV->netincome;
										$ppv = $rowPPV->ppv;
										$pgpv = $rowPPV->pgpv;
										$minpgpv = $rowPPV->minpgpv;
										$psharingpoints = $rowPPV->psharingpoints;
										$RANKX = $rowPPV->RANKX;
										$SHORTNM = $rowPPV->SHORTNM;

										if($ppv >= 400){ //4a. jika PPV >= 400, maka cek selanjutnya

											//$ppvHdr = "<th>Rank</th>"; //var untuk hasil cek 400 BV, Header
											$ppvVal = "<td><div align=right>$ppv</div></td>"; //var untuk hasil cek 400 BV, Hasil Value
											//$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header
											$rankVal = "<td>$rankDesc</td>"; //var untuk hasil cek Rank, Hasil Value

											//check LBC Status
											$today = date("Y-m-d");
											$sqlLBC = "SELECT TOP 1 * FROM ASH_LBC_MEMB A WHERE A.dfno = '$idmember' AND A.expired_dt>=$today";
											$resLBC = $this->getDataResult($sqlLBC ,null,$this->db2);

											$statusLBC = $resLBC['rowCount'];

											//echo "$sqlLBC, hasil = $statusLBC";
											//$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
											$dynFundVal = "<td>No Points DF</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value

											$msg1 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN Dynamic Fund";
											$isFree = 0;
											$msg = "";

											if($statusLBC >= 1){

												//$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
												$lbcVal = "<td>Active</td>"; //var untuk hasil cek LBC, Hasil Value

												$msg2 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Status LBC ANDA = aktif, </br>MEMENUHI PERSYARATAN LBC dipersilahkan masuk secara GRATIS.";
												$isFree = 1;
											}else{

												//$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
												$lbcVal = "<td>Inactive</td>"; //var untuk hasil cek LBC, Hasil Value

												$msg2 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Status LBC ANDA = Tidak Aktif, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN LBC.";
												$isFree = 0;
											}
											//echo "LBC STATUS = $statusLBC</br>$msg2</br>$msg1</br>";


											$msg = "$msg1 $msg2";
											//end check LBC status

											if($rank <= 6 ) { //5A. jika peringkat Manager kebawah, maka CEK 1 ATAU 2 POIN DYNAMIC FUND

												if($psharingpoints >= 1){ //6A. jika mencapai dynamic fund, maka GRATIS

													//$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
													$dynFundVal = "<td>$psharingpoints Points DF</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value

													$msg = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Dynamic Fund ANDA = $psharingpoints, </br>MEMENUHI PERSYARATAN DYNAMIC FUND dipersilahkan masuk secara GRATIS.";
													$isFree = 1;
												}else{ //6A. jika TIDAK mencapai dynamic fund, maka CEK KEANGGOTAAN LBC

													$today = date("Y-m-d");
													$sqlLBC = "SELECT TOP 1 * FROM ASH_LBC_MEMB A WHERE A.dfno = '$idmember' AND A.expired_dt>=$today";
													$resLBC = $this->getDataResult($sqlLBC ,null,$this->db2);

													$statusLBC = $resLBC['rowCount'];


													//$dynFundHdr = "<th>Dynamic Fund</th>"; //var untuk hasil cek Dynamic Fund, Header
													$dynFundVal = "<td>No Points DF</td>"; //var untuk hasil cek Dynamic Fund, Hasil Value

													$msg1 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN Dynamic Fund";
													$isFree = 0;
													$msg = "";

													if($statusLBC >= 1){

														//$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
														$lbcVal = "<td>Active</td>"; //var untuk hasil cek LBC, Hasil Value

														$msg2 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Status LBC ANDA = aktif, </br>MEMENUHI PERSYARATAN LBC dipersilahkan masuk secara GRATIS.";
														$isFree = 1;
													}else{

														//$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
														$lbcVal = "<td>Inactive</td>"; //var untuk hasil cek LBC, Hasil Value

														$msg2 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Status LBC ANDA = Tidak Aktif, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN LBC.";
														$isFree = 0;
													}

													$msg = "$msg1 $msg2";
												}

											}elseif($rank==6 || $rank == 7 || $rank == 8 || $rank == 9){ //5B. jika peringkat SM s/d EM, maka cek PGBV


												$msg1 = "";
												$isFree = 0;
												$msg = "";

												if($statusLBC >= 1){

													//$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
													$lbcVal = "<td>Active</td>"; //var untuk hasil cek LBC, Hasil Value

													$msg2 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Status LBC ANDA = aktif, </br>MEMENUHI PERSYARATAN LBC dipersilahkan masuk secara GRATIS.";
													$isFree = 1;
												}else{

													//$lbcHdr = "<th>LBC Status</th>"; //var untuk hasil cek LBC, Header
													$lbcVal = "<td>Inactive</td>"; //var untuk hasil cek LBC, Hasil Value

													$msg2 = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Status LBC ANDA = Tidak Aktif, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN LBC.";
													$isFree = 0;
												}

												$msg = "$msg1 $msg2";


											}elseif($rank >= 10){ //5C. jika peringkat DM Above, maka cek minimal bonus

												/*
												 $sqlBonusLC = "SELECT A.dfno, A.fullnm, A.totBonus, A.totBonus_Nett, A.bonusmonth, A.bonusyear
																	FROM tbonus_hilal_nett a
																	WHERE a.dfno='$idmember' AND A.bonusmonth=$bonusmonth AND A.bonusyear=$bonusyear";
												
												*/
												$sqlBonusLC = "SELECT a.distributorcode as dfno, a.fullnm, a.totbns as totBonus, a.totbns as totBonus_Nett, a.bonusmonth, a.bonusyear
																FROM HILAL_STBNS_HDR a
																WHERE a.distributorcode='$idmember' AND A.bonusmonth=$bonusmonth AND A.bonusyear=$bonusyear";
												
												$resBonusLC = $this->getRecordset($sqlBonusLC ,null,$this->db2);

												$totBonus_NettLC = 0;
												if ($resBonusLC != 0 && $resBonusLC != null) { //cek jika BV bulan sebelumnya ada di table tsonus, maka teruskan cek selanjutnya
													foreach($resBonusLC as $rowLC) {
														//print_r($rowLC);
														$totBonusLC = $rowLC->totBonus;
														$totBonus_NettLC = $rowLC->totBonus_Nett;
														$totBonus_Nett = number_format($totBonus_NettLC,2,",",".");

														//slc = 10jt, glc = 25jt, plc = 50jt, elc = 80jt
//														if($totBonus_NettLC >= 10000000  && $statusLBC >=1){ //jika bonus CA ABOUVE minimal 10jt, maka GRATIS

															//$lcBonusHdr = "<th>Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
															$lcBonusVal = "<td><div align=right>$totBonus_Nett</div></td>"; //var untuk hasil cek Bonus LC, Hasil Value

															$msg = "Peringkat Anda = $rankDesc, </br>Status LBC ANDA = aktif, </br>Bonus Anda Bulan lalu = $totBonus_Nett, </br>MEMENUHI PERSYARATAN MINIMAL BONUS SLC, </br>Anda dipersilahkan masuk secara GRATIS.";
															$isFree = 1;
//														}else{ //jika bonus CA ABOUVE kurang dari 80jt, maka BAYAR 50RB
//
//															//$lcBonusHdr = "<th>Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
//															$lcBonusVal = "<td>Bonus Anda kurang dari SLC</td>"; //var untuk hasil cek Bonus LC, Hasil Value
//
//															$msg = "Peringkat Anda = $rankDesc, </br>Bonus Anda Bulan lalu = $totBonus_Nett, </br>Anda diharuskan membayar 50.000 </br>karena TIDAK MEMENUHI PERSYARATAN MINIMAL BONUS SLC.";
//															$isFree = 0;
//														}


													}
												}else{ //jika bonus CA ABOUVE kurang dari 10jt, maka BAYAR 50RB

													//$lcBonusHdr = "<th>Bonus LC</th>"; //var untuk hasil cek Bonus LC, Header
													$lcBonusVal = "<td>Bonus Anda kurang dari SLC</td>"; //var untuk hasil cek Bonus LC, Hasil Value

													$msg = "Peringkat Anda = $rankDesc, </br>Bonus Anda Bulan lalu = $totBonus_Nett, </br>Anda diharuskan membayar 50.000 </br>zkarena TIDAK MEMENUHI PERSYARATAN MINIMAL BONUS SLC.";
													$isFree = 0;
												}

											}
										}else{ //4B. jika PPV < 400, maka bAYAR 50RB

											//$ppvHdr = "<th>Rank</th>"; //var untuk hasil cek 400 BV, Header
											$ppvVal = "<td>BV Anda Kurang dari $ppv</td>"; //var untuk hasil cek 400 BV, Hasil Value
											//$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header

											$msg = "Hai $idmember, </br>Peringkat Anda = $rankDesc, </br>Anda diharuskan membayar 50.000 </br>karena BULAN SEBELUMNYA ANDA TIDAK MENCAPAI 400 BV";
											$isFree = 0;
										}
									}

								}else{

									//$ppvHdr = "<th>Rank</th>"; //var untuk hasil cek 400 BV, Header
									$ppvVal = "<td>BV Anda Kurang dari $ppv</td>"; //var untuk hasil cek 400 BV, Hasil Value
									//$rankHdr = "<th>Rank</th>"; //var untuk hasil cek Rank, Header

									$msg = "Tidak ada pembelanjaan bulan sebelumnya.";
									$isFree = 0;
								}


							}
						}else{ //JIKA GENDER KOSONG
							$isFree = 0;
						}
					}
					//echo "ada";


					// EDIT HILAL ON 2018-02-01

					$periodx = date_format(date_create($period), 'Y-M');
					$periodHdr = "<th width=\"7%\">Period</th>"; //var untuk hasil Period BV, Header
					$periodVal = "<td>$periodx</td>"; //var untuk hasil Period BV, Hasil Value

					$tblHdr = $periodHdr.$rankHdr.$ppvHdr;
					$tblDet = "<tr>
											$periodVal
											$rankVal
											$ppvVal";

					if($rank <= 6 && $sex == "M"){ //jika peringkat maximal manager dan gender Laki2, maka Cek Dynamic Fund saja
						$tblHdr = $tblHdr.$dynFundHdr;
						$tblDet = "$tblDet$dynFundVal";
						//echo "masuk ke A</br>";
					}elseif($rank <= 6 && $sex == "F"){ //jika peringkat maximal manager dan gender Wanita, maka Cek Status LBC saja
						$tblHdr = $tblHdr.$dynFundHdr; //$tblHdr.$lbcHdr;
						$tblDet = "$tblDet$dynFundVal";
						//echo "masuk ke B</br>";
					}elseif(($rank > 6 && $rank < 10) && $sex == "M"){ //jika peringkat SM-EM dan gender Laki2, maka Cek PGBV
						$tblHdr = $tblHdr.$pgpvHdr;
						$tblDet = "$tblDet$pgpvVal";
						//echo "masuk ke C</br>";
					}elseif(($rank > 9 && $rank < 12) && $sex == "M"){ //jika peringkat DM-CM dan gender Laki2, maka Cek GBV 3 Leg 3000
						$tblHdr = $tblHdr.$gbvLegHdr;
						$tblDet = "$tblDet$gbvLegVal";
						//echo "masuk ke D</br>";
					}elseif($rank > 11 && $sex == "M"){ //jika peringkat CA Abovve dan gender Laki2, maka Cek SLC min 10jt
						$tblHdr = $tblHdr.$lcBonusHdr;
						$tblDet = "$tblDet$lcBonusVal";
						//echo "masuk ke E</br>";
					}elseif(($rank >= 6 && $rank <= 9) && $sex == "F"){ //jika peringkat M - EM, harus 400BV dan terdaftar LBC
						$tblHdr = $tblHdr.$lbcHdr;
						$tblDet = "$tblDet$lbcVal";
//									echo "masuk ke F</br>";
						//echo "masuk ke F, peringkat M-EM Above $lbcVal</br>";
					}elseif(($rank >= 10 && $rank <= 14) && $sex == "F"){ //jika peringkat CA Above dan gender Wanita, maka Cek SLC min 10jt
						$tblHdr = $tblHdr.$lbcHdr.$lcBonusHdr;
						$tblDet = "$tblDet$lbcVal$lcBonusVal";
						//echo "masuk ke G</br>";
						//echo "masuk ke F, peringkat CA Above $lbcVal</br>";
					}

					$tblHdr = "<table width=\"100%\" class=\"table table-striped table-bordered bootstrap-datatable datatable\">
							                <thead><tr><th colspan=6>Status DYL</th></tr>
							                <tr bgcolor=#f4f4f4> $tblHdr <th width=\"45%\">Desc</th></thead></tr>
						                	<tbody>";
					//isinya
					$tblHdr = "$tblHdr
										   $tblDet
										   <td>$msg</td>
										   </tr></tbody></tr></table>";
					//end isinya


					$data['tblCheck'] = $tblHdr;
					$data['isFree'] = $isFree;

					$result = $data;

					//END EDIT HILAL


				} else {
					$msg = "Data distributor tidak dapat ditemukan.";
					//echo $msg;
					$result = $msg;
					$isFree = 1;
				}
				//SELESAI CHEK
			}
		}

		return $result;
	}

	function getReportBonus2($idmember){
		//ORI
		/*
		 $slc = "SELECT COUNT(a.distributorcode) AS TOT400_LEG, a.bonusmonth, a.bonusyear
				FROM tbonus a
				WHERE --a.bonusmonth =  10  AND
					  a.bonusyear =  (SELECT )
					  AND a.SponsorCode = '$idmember'
					  AND A.gbvb>=200
				group by  a.bonusmonth, a.bonusyear
				ORDER BY a.bonusyear, a.bonusmonth;";
		*/
				
		$slc = "SELECT COUNT(a.distributorcode) AS TOT400_LEG, a.bonusmonth, a.bonusyear
				FROM tbonus a
				WHERE a.bonusmonth =  (SELECT TOP 1 BONUSMONTH FROM tbonussummary ORDER BY BONUSYEAR DESC, BONUSMONTH DESC)  AND
				      a.bonusyear =  (SELECT TOP 1 BONUSYEAR FROM tbonussummary ORDER BY BONUSYEAR DESC, BONUSMONTH DESC)
				      AND a.SponsorCode = '$idmember'
				      AND A.gbvb>=200
				group by  a.bonusmonth, a.bonusyear
				ORDER BY a.bonusyear, a.bonusmonth";

		$result = $this->getRecordset($slc,null,$this->db2);
		return $result;
	}


	function isfree($idmember){
		$RETURN=FALSE;
		$query = "SELECT max(adjustedrank) as rank
			from tbonus a
				 left outer join msmemb b on a.distributorcode=b.dfno
				 inner join msrank c on CAST(a.adjustedrank AS int)=c.[level]
			where a.bonusyear=year( getdate() )
				  and a.distributorcode= ? ; ";
		$result = $this->db->query($query,array($idmember));
		foreach($result->result() as $row) {
			if($row->rank > 6){
				
				////ORI	
					/*$sqlGetPeriod = "select a.currperiod, 
									   		  DATEADD(m, -1, a.currperiod) as period, 
									   		  month(DATEADD(m, -1, a.currperiod)) as bonusmonth, 
									   		  year(DATEADD(m, -1, a.currperiod)) as bonusyear 
									    from syspref a";
					*/

					$sqlGetPeriod = "SELECT top 1 CAST(A.bonusyear AS VARCHAR(4)) + RIGHT( ('0' + CAST(A.bonusyear AS VARCHAR(4))), 2) + '01' AS period,
												CAST(A.bonusyear AS VARCHAR(4)) + RIGHT( ('0' + CAST(A.bonusyear AS VARCHAR(4))), 2) + '01' AS curreriod,
	   											a.bonusyear, a.bonusmonth 
									 FROM tbonussummary a
									 order by a.bonusyear desc, a.bonusmonth desc";
					//--, (select b.currperiod from syspref b ) as curreriod
					
				//ORI	
				/*
				 
				 $query7="SELECT COUNT(a.distributorcode) AS TOT400_LEG
					FROM tbonus a
					WHERE a.bonusmonth =  (SELECT MONTH (DATEADD(m, -1, a.currperiod)) prevMonth
					from syspref a)
  					AND a.bonusyear =  2017
					AND a.SponsorCode = ?
     				AND A.gbvb>=200;";
				*/
				
					
				$query7="SELECT COUNT(a.distributorcode) AS TOT400_LEG
					FROM tbonus a
					WHERE a.bonusmonth =  (SELECT TOP 1 BONUSMONTH FROM tbonussummary ORDER BY BONUSYEAR DESC, BONUSMONTH DESC) 
  					AND a.bonusyear =  (SELECT TOP 1 BONUSYEAR FROM tbonussummary ORDER BY BONUSYEAR DESC, BONUSMONTH DESC)
					AND a.SponsorCode = ?
     				AND A.gbvb>=200;";
					
				$result7 = $this->db->query($query7,array($idmember));
				foreach($result7->result() as $row7){
					if($row7->TOT400_LEG>0)
						$RETURN=TRUE;
				}
			}
			else
			{
				//ORI
				/*
				 
				 $query6="SELECT a.distributorcode, b.fullnm, a.adjustedrank, c.shortnm,
							   a.bonusmonth, a.bonusyear, a.psharingincome, a.psharingpoints
						from tbonus a
							 left outer join msmemb b on a.distributorcode=b.dfno
							 inner join msrank c on CAST(a.adjustedrank AS int)=c.[level]
						where a.bonusyear=year( getdate() ) AND a.bonusmonth =  (SELECT MONTH (DATEADD(m, -1, a.currperiod)) prevMonth
											from syspref a)
							  and a.distributorcode=?;";
				*/
							  
				$query6="SELECT a.distributorcode, b.fullnm, a.adjustedrank, c.shortnm,
							   a.bonusmonth, a.bonusyear, a.psharingincome, a.psharingpoints
						from tbonus a
							 left outer join msmemb b on a.distributorcode=b.dfno
							 inner join msrank c on CAST(a.adjustedrank AS int)=c.[level]
						where a.bonusyear= (SELECT TOP 1 BONUSYEAR FROM tbonussummary ORDER BY BONUSYEAR DESC, BONUSMONTH DESC) 
							  AND a.bonusmonth =  (SELECT TOP 1 BONUSMONTH FROM tbonussummary ORDER BY BONUSYEAR DESC, BONUSMONTH DESC)
							  and a.distributorcode=?;";
				$result6 = $this->db->query($query6,array($idmember));
				foreach($result6->result() as $row6){
					if($row6->psharingpoints>0)
						$RETURN=TRUE;
				}
			}
		}
		return $RETURN;
	}
	public function getReportLounge($tgl_awal,$tgl_akhir,$category)
	{

		$result = array();
		$this->db->select('*');
		$this->db->from('klink_mlm2010.dbo.lounge_h');
		$this->db->where('status','0');
		if($category=='1'){

		}
		if($category=='2'){
			$this->db->where('payment = "FREE" or payment = "" ');
		}
		if($category=='3'){
			$this->db->where('payment != "FREE" and payment is not null and payment != "" ');
		}
		$this->db->where("  createdt BETWEEN '".$tgl_awal." 00:00:00.000' AND '".$tgl_akhir." 23:59:59.000'",null,false);
		$this->db->order_by("createdt","DESC")
		;


		$q = $this->db->get();
//		 echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;

	}

}
<?php
class M_idc_promo extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
	
	function totalAkumulasiPoin($idmember) {
		$qry = "SELECT  ax.dfno, 
				        ax.fullnm, 
				        ax.bnsperiod,
				        sum(ax.tot400Above) as tot400Above,
				        sum(ax.tot400Less) as tot400Less,
				        sum(ax.totlvl) as totlvl,
				        sum(ax.totRU400) as totRU400,
				        sum(ax.totRL400) as totRL400,
				        sum(ax.totBVR) as totBVR,
				        sum(ax.checkPoin) as checkPoin,
				        sum(ax.checkVal) as checkVal
				FROM (
				    SELECT 
				        a.dfno, 
				        a.fullnm, 
				        a.bnsperiod,
				        a.tot400Above,
				        a.tot400Less,
				        a.totlvl,
				        a.totRU400,
				        a.totRL400,
				        a.totBVR,
				        a.checkPoin,
				        a.checkVal
				    FROM QTW_TEMPMEMNET_PR a
				    WHERE a.dfno = '$idmember'
				    
				    group by a.dfno, a.fullnm, a.bnsperiod,
				        a.tot400Above,
				        a.tot400Less,
				        a.totlvl,
				        a.totRU400,
				        a.totRL400,
				        a.totBVR,
				        a.checkPoin,
				        a.checkVal
				    having a.checkPoin > 0
				) AX
				group by ax.dfno, 
				        ax.fullnm, 
				        ax.bnsperiod,
				        ax.checkVal
				ORDER BY ax.checkVal DESC";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
   
    function checkValidIDC($idmemb, $bnsperiod) {
    	$qry = "SELECT  ax.dfno, 
				        ax.fullnm, 
				        ax.bnsperiod,
				        sum(ax.tot400Above) as tot400Above,
				        sum(ax.tot400Less) as tot400Less,
				        sum(ax.totlvl) as totlvl,
				        sum(ax.totRU400) as totRU400,
				        sum(ax.totRL400) as totRL400,
				        sum(ax.totBVR) as totBVR,
				        sum(ax.checkPoin) as checkPoin,
				        sum(ax.checkVal) as checkVal
				FROM (
				    SELECT 
				        a.dfno, 
				        a.fullnm, 
				        a.bnsperiod,
				        a.tot400Above,
				        a.tot400Less,
				        a.totlvl,
				        a.totRU400,
				        a.totRL400,
				        a.totBVR,
				        a.checkPoin,
				        a.checkVal
				    FROM QTW_TEMPMEMNET_PR a
				    WHERE a.dfno = '$idmemb'
				    AND a.bnsperiod = '$bnsperiod'
				    group by a.dfno, a.fullnm, a.bnsperiod,
				        a.tot400Above,
				        a.tot400Less,
				        a.totlvl,
				        a.totRU400,
				        a.totRL400,
				        a.totBVR,
				        a.checkPoin,
				        a.checkVal
				    having a.checkPoin > 0
				) AX
				group by ax.dfno, 
				        ax.fullnm, 
				        ax.bnsperiod,
				        ax.checkVal
				ORDER BY ax.checkVal DESC";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
    }
	
	function getListIDCWinner($rep_bnsperiod, $orderby) {
    	$qry = "SELECT  ax.dfno, 
				        ax.fullnm, 
				        ax.bnsperiod,
				        sum(ax.tot400Above) as tot400Above,
				        sum(ax.tot400Less) as tot400Less,
				        sum(ax.totlvl) as totlvl,
				        sum(ax.totRU400) as totRU400,
				        sum(ax.totRL400) as totRL400,
				        sum(ax.totBVR) as totBVR,
				        sum(ax.checkPoin) as checkPoin,
				        sum(ax.checkVal) as checkVal
				FROM (
				    SELECT 
				        a.dfno, 
				        a.fullnm, 
				        a.bnsperiod,
				        a.tot400Above,
				        a.tot400Less,
				        a.totlvl,
				        a.totRU400,
				        a.totRL400,
				        a.totBVR,
				        a.checkPoin,
				        a.checkVal
				    FROM QTW_TEMPMEMNET_PR a
				    WHERE CONVERT(VARCHAR(10), a.bnsperiod, 20) = '$rep_bnsperiod'
				    group by a.dfno, a.fullnm, a.bnsperiod,
				        a.tot400Above,
				        a.tot400Less,
				        a.totlvl,
				        a.totRU400,
				        a.totRL400,
				        a.totBVR,
				        a.checkPoin,
				        a.checkVal
				    having a.checkPoin > 0
				) AX
				group by ax.dfno, 
				        ax.fullnm, 
				        ax.bnsperiod,
				        ax.checkVal,
				        ax.$orderby
				ORDER BY ax.$orderby 
				
				DESC";
		//echo $qry;
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
    }
	
	function listBnsPeriod($idmemb) {
		$qry = "SELECT CONVERT(VARCHAR(10), a.bnsperiod, 20) as bnsperiod
		        FROM QTW_TEMPMEMNET_PR a 
		        WHERE a.dfno = '$idmemb'
		        GROUP BY CONVERT(VARCHAR(10), a.bnsperiod, 20)";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
	
	function getListIDCDetail($idmemb, $bnsperiod) {
		$qry = "SELECT a.leg,
		               a.bDfno, 
				       a.bFullnm, 
				       CONVERT(VARCHAR(10), a.bnsperiod, 20) as bnsperiod, 
				       CONVERT(VARCHAR(10), a.jointdt, 20) as jointdt, 
				       a.sfno_reg,
				       a.bv
				FROM QTW_TEMPMEMNET_PR a 
				WHERE a.dfno = '$idmemb' and a.checkPoin > 0
				and CONVERT(VARCHAR(10), a.bnsperiod, 20) = '$bnsperiod'
				ORDER BY a.bv DESC";
		$result = $this->getRecordset($qry, null, $this->setDB(2));
		return $result;
	}
	
	function checkValidIDC2($idmemb) {
		
	}
}
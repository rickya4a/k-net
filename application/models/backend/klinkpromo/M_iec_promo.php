<?php
class M_iec_promo extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function headerIEC($idmember){
        $slc = "select a.dfno,a.fullnm from msmemb a where a.dfno = '".$idmember."'";
        $result = $this->getRecordset($slc, null, $this->setDB(2));
		return $result;
    }
    
    function totalSummaryIEC($idmember){
        $slc = "select * from TWA_IEC a where a.dfno = '".$idmember."'";
        $result = $this->getRecordset($slc, null, $this->setDB(2));
		return $result;
    }
    
    function detRecIEC($idmember,$bnsperiod){
        $slc = "select a.dfno,a.fullnm,a.ppv from TWA_IEC2 a 
                where a.sfnoreg = '".$idmember."' and a.bnsperiod = '".$bnsperiod."'
                order by a.ppv desc";
        $result = $this->getRecordset($slc, null, $this->setDB(2));
		return $result;
    }


    function getIAC(){
        $slc = "select * from V_HILAL_PROMO_IAC order by flag_IAC_type asc, totalbonus_rec_IAC_Summ_RP desc";
        $result = $this->getRecordset($slc, null, $this->setDB(2));
        return $result;
    }


    function getPemenang($idmember){

        $slc = "select * from V_HILAL_PROMO_IAC WHERE dfno = '".$idmember."'";
        $result = $this->getRecordset($slc, null, $this->setDB(2));
        $x=count ($result);
        if ($x>=1){
            return true;
        }else
            return false;

    }

    function showDataPrint($id){
        $slc = "select * from msmemb where dfno = '".$id."'";
        $result = $this->getRecordset($slc, null, $this->setDB(2));
        return $result;
    }

}
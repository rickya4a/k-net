<?php
class Be_stockist_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getListStockist() {
        $dt = $this->input->post(null,true);
		
		//select all stockist for view
        $slc = "select a.loccd, a.fullnm, a.sctype, a.scstatus, b.[password]
				from klink_mlm2010.dbo.mssc a
					 inner join klink_mlm2010.dbo.sc_users b on a.loccd=b.username
				where a.sctype in ('1','2') and a.fullnm not like '%TERMINAT%' AND A.scstatus='1'
				ORDER BY A.sctype, A.fullnm";
        //echo $slc;
        
        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	function getListStockistPromo($startDT, $endDT,$loccd) {
		if($startDT == null && $endDT == null){
			$dtRange = "";
		}else{
			$dtRange = " AND (d.[start_date] BETWEEN '$startDT' AND '$endDT') AND 
						c.userid='$loccd' and d.loccd='$loccd' and b.username='$loccd'";
		}
		
		//select all stockist for view
        $slc = "select a.loccd, a.fullnm, a.sctype, a.scstatus, b.[password], c.prgid, c.expireddt, d.[start_date], d.expired_date
				from klink_mlm2010.dbo.mssc a
					 inner join klink_mlm2010.dbo.sc_users b on a.loccd=b.username
				     inner join klink_mlm2010.dbo.sc_userauthority c on c.userid=a.loccd 
				     inner join klink_mlm2010.dbo.sc_user_promo_menu d on 
				     						a.loccd=d.loccd COLLATE SQL_Latin1_General_CP1_CS_AS and 
				     						c.prgid=d.prgid COLLATE SQL_Latin1_General_CP1_CS_AS
				where a.sctype in ('1','2') and a.fullnm not like '%TERMINAT%' AND A.scstatus='1' 
						$dtRange
				ORDER BY A.sctype, A.fullnm";
        //echo $slc;
        
        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	function saveStockistPromo($startDT, $endDT,$loccd){
		$qryExp = "SELECT TOP 1 * FROM klink_mlm2010.dbo.sc_user_promo_menu WHERE loccd='$loccd' and expired_date >= '$endDT'";
		//echo "$qryExp";
		$res1 = $this->db->query($qryExp);
		if($res1->num_rows() > 0){
			$return = 0;
			
			foreach($res1->result() as $data){   
                $expired_date = $data->expired_date;
            }
            echo "This stockist ready to promo untill ".date('d-m-Y',strtotime($expired_date));
        }else{
            $sqlInsert = "INSERT INTO klink_mlm2010.dbo.sc_user_promo_menu(loccd, prgid, promo_code, start_date, expired_date) 
            			  VALUES('$loccd','MEMBER_REGISTER_100','','$startDT','$endDT')";
            //echo "$sqlInsert </br>";
			$qryInst = $this->executeQuery($sqlInsert);
        	if(!$qryInst){
			  	$return = 0;
			}else{
				$sqlUpd = "UPDATE klink_mlm2010.dbo.sc_userauthority 
						   SET expireddt = '$endDT' 
						   WHERE prgid = 'MEMBER_REGISTER_100' AND userid = '$loccd'";
				$qryUpd = $this->executeQuery($sqlUpd);
	        	if(!$qryUpd){
	        		$sqlInstMenuSCO = "INSERT INTO from klink_mlm2010.dbo.sc_userauthority(
	        							  userid, prgid, add, edit, delete, view, print, 
	        							  createnm, createdt, expireddt) 
	        						   VALUES('$loccd', 'MEMBER_REGISTER_100', '1', '1', '1', '1', '1', 
	        						   		  '".$this->username."', getdate(), '$endDT')";
					$qryInstSCOMenu = $this->executeQuery($sqlInstMenuSCO);
	        		if(!$qryInstSCOMenu){
				  		$return =  0;
					}else{
				  		$return = 1;
					}
				}
				$return = 1;
			}
        }
        
		return $return;
        
	}
	
	
	function deleteStockistPromo($loccd, $startDT, $expiredDT) {
        $startDT1 = explode('-',$startDT);
		$startDT = $startDT1[2]."-".$startDT1[1]."-".$startDT1[0];
		$expiredDT1 = explode('-',$expiredDT);
		$expiredDT = $expiredDT1[2]."-".$expiredDT1[1]."-".$expiredDT1[0];
		
        $slc = "delete from klink_mlm2010.dbo.sc_user_promo_menu where loccd = '$loccd' AND start_date='$startDT' AND expired_date='$expiredDT'";
        //echo $slc;
        $query = $this->db->query($slc);
		if(!$query) {			
			throw new Exception("Delete Promo Stockist failed..!!", 1);
		}else{
			$updUser = "update klink_mlm2010.dbo.sc_userauthority set expireddt= null where userid = '$loccd' AND prgid='MEMBER_REGISTER_100'";
        	$qUpdUser = $this->db->query($updUser);
		} 
		echo true;
    }
	
	function getListStkEcomm(){
	   /*$slc = "select a.loccd as stkJne,b.loccd,b.fullnm
                from jne_Tabel_Zona_Induk a 
                INNER join klink_mlm2010.dbo.mssc b on a.loccd = b.loccd collate SQL_Latin1_General_CP1_CI_AS
                group by a.loccd,b.fullnm,b.loccd
                order by a.loccd";*/
                
        $slc = "select a.loccd,a.fullnm,a.scstatus,a.sctype
                 from klink_mlm2010.dbo.mssc a 
                 where a.sctype = '1' and
                 a.loccd not in('FODA','IDJD01','IDET01','IDMSYG15467',
                 'IDSB02','IDBM383','SAIDB01','IDSS05')
                 order by a.loccd";
       $result = $this->getRecordset($slc);
        
       return $result;
	}
    
    function getListProvince(){
       $slc = "select PROPINSI from jne_tabel_Zona_induk a 
                GROUP BY PROPINSI ORDER BY PROPINSI";
       
       
       $result = $this->getRecordset($slc);
        
       return $result;
    }
    
    function getListKabupaten($prov){
        /*$slc = "select a.Kota_Kabupaten from jne_tabel_Zona_induk a 
                where a.propinsi = 'PAPUA'
                group by a.Kota_Kabupaten
                ORDER BY a.Kota_Kabupaten";*/
        $slc = "select a.Kota_Kabupaten from jne_tabel_Zona_induk a 
                where a.propinsi = '$prov'
                group by a.Kota_Kabupaten
                ORDER BY a.Kota_Kabupaten";
        $result = $this->getRecordset($slc);
        
        return $result;
    }
    
    function getListKecamatan($dt){
        if($dt['kabupaten'] =='kabb'){
            $y = "a.propinsi = '".$dt['prov']."' ";
        }else{
            //$y = "(a.propinsi = '".$dt['prov']."' and a.Kota_Kabupaten = '".$dt['kabupaten']."' ) or (a.loccd='".$dt['idstk']."' or a.loccd = '".$dt['newstkk']."') ";
            $y = "(a.propinsi = '".$dt['prov']."' and a.Kota_Kabupaten = '".$dt['kabupaten']."' ) or (a.loccd='".$dt['idstk']."') ";
        }
        $slc = "select a.Kecamatan,a.propinsi,a.Kota_Kabupaten,a.loccd
                from jne_tabel_Zona_induk a 
                where $y
                ORDER BY a.Kota_Kabupaten,a.Kecamatan";
        $result = $this->getRecordset($slc);
        
        return $result;
    }
	
    function kecByStks($dt){
        $slc = "select a.Kecamatan,a.propinsi,a.Kota_Kabupaten,a.loccd
                from jne_tabel_Zona_induk a 
                where a.loccd = '".$dt['idstk']."' order by a.Kecamatan";
        //echo $slc;
        $result = $this->getRecordset($slc);
        
        return $result;
    }
    
    function updtStkEcomm($dt){
        $jml = count($dt['stkMaintenance']);
        if($dt['idstkk'] != ""){
            $loccd = $dt['idstkk'];
        }else{
            //$loccd = $dt['newstk'];
        }
        
        $unsetDtLama = "update jne_Tabel_Zona_Induk set loccd = 'NO STK'
                        WHERE loccd = '".$dt['idstkk']."'";
        //echo "unset dlu ".$updtStkLama."<br>";
        $y = $this->executeQuery($unsetDtLama);
        if(!$y){
            $ress = array('response' => 'false','message'=>'Data Gagal Di Update');
        }else{
            $res1 = 0;
            $res2 = 0;
            for($i=0;$i<$jml;$i++){
                $res1++;
                
                $y = explode("-",$dt['stkMaintenance'][$i]);    
                $updStk = "update jne_Tabel_Zona_Induk set loccd = '".$loccd."'
                        WHERE PROPINSI = '".$y[0]."' 
                        and Kota_Kabupaten = '".$y[1]."' and Kecamatan = '".$y[2]."'";
                //echo $updStk;
                $r = $this->executeQuery($updStk);
                if($r > 0){
                    $res2++;
                }
            }
            if($res1 == $res2){
                $ress = array('response' => 'true','message'=>'Data Berhasil Di Update');
            }else{
                $ress = array('response' => 'false','message'=>'Data Gagal Di Update');
            }
        }
        return $ress;
    }
    
    function updtNewStkEcomm($dt){
        $jml = count($dt['stkMaintenance']);
        if($dt['idstkk'] != ""){
            $loccd = $dt['idstkk'];
        }else{
            $loccd = $dt['newstk'];
        }
        
        $res1 = 0;
        $res2 = 0;
        for($i=0;$i<$jml;$i++){
            $res1++;
            $y = explode("-",$dt['stkMaintenance'][$i]);    
            $updStk = "update jne_Tabel_Zona_Induk set loccd = '".$loccd."'
                    WHERE PROPINSI = '".$y[0]."' 
                    and Kota_Kabupaten = '".$y[1]."' and Kecamatan = '".$y[2]."'";
            //echo $updStk;
            $r = $this->executeQuery($updStk);
            if($r > 0){
                $res2++;
            }
        }
        if($res1 == $res2){
            $ress = array('response' => 'true','message'=>'Data Berhasil Di Update');
        }else{
            $ress = array('response' => 'false','message'=>'Data Gagal Di Update');
        }
        return $ress;
    }
    
    function unsetAllByIdstk($dt){
        $unsetAllByStk = "update jne_Tabel_Zona_Induk set loccd = 'NO STK'
                        WHERE loccd = '".$dt['idstk']."'";
        //echo "unset dlu ".$updtStkLama."<br>";
        $y = $this->executeQuery($unsetAllByStk);
        if(!$y){
            $ress = array('response' => 'false','message'=>'Data Gagal Di Update');
        }else{
            $ress = array('response' => 'true','message'=>'Data Berhasil Di Update');
        }
        return $ress;
        
    }
    
    function cekStkExist($dt){
        $slc = "select a.loccd from jne_Tabel_Zona_Induk a where a.loccd = '".$dt['idstkk']."'";
        
        $result = $this->getRecordset($slc);
        
        return $result;
    }
    function getSSR() {
        $category=$this->input->post('category');
        $search=$this->input->post('search');


        //select all stockist for view
        $slc = "
        SELECT batchno, batchdt, a.loccd, b.fullnm FROM [klink_mlm2010].[dbo].[sc_newtrh] a LEFT JOIN   [klink_mlm2010].[dbo].mssc b ON a.loccd= b.loccd where $category LIKE '%$search%'  GROUP BY batchno, batchdt, a.loccd, b.fullnm ORDER BY batchdt DESC
        ";
        //echo $slc;

        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }

    function getSSRDetail($param) {
//        $category=$this->input->post('category');


        //select all stockist for view
        $slc = "
        SELECT * FROM [klink_mlm2010].[dbo].[sc_newtrh]  where batchno = '$param'
        ";
        //echo $slc;

        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }


}
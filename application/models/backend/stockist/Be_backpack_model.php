<?php
class Be_backpack_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getStockistInfo($idstk) {
		$slc = "SELECT fullnm
			    FROM mssc
				WHERE loccd = '$idstk'
				and fullnm not like 'TERMINATION%'";
        $result = $this->getRecordset($slc, null ,$this->db2);
        return $result;
	}
	
	function getMemberInfo($idmemb) {
		$slc = "SELECT fullnm
			    FROM msmemb
				WHERE dfno = '$idmemb'and fullnm not like 'RESIGNATION%'
                and fullnm not like 'TERMINATION%'
                and status = '1'";
        $result = $this->getRecordset($slc, null ,$this->db2);
        return $result;
	}
	
	
	
	function saveTrfBackpack($form) {
		$this->db = $this->load->database('db_ecommerce', true);
		
		$qry = "INSERT INTO adis_trf_backpack_hdr (id_stockist, nm_stockist, total_trf, createdt) 
		        VALUES ('$form[id_stockist]', '$form[nm_stockist]', ".$form['total_trf_real'].", '".$this->dateTime."')
		        ";
		$query = $this->db->query($qry);
		
		$sc = "select top 1 id 
			   from [adis_trf_backpack_hdr] a
			   order by id desc";
		$scss = $this->db->query($sc);
		if($query > 0) {
		   foreach($scss->result() as $dta) {
		   	    $id = "";
				$x = count($form['idmember']);
				for($i = 0; $i < $x; $i++) {
					  if($form['idmember'][$i] != "") {
				      $ins = "INSERT INTO adis_trf_backpack_det (trf_id, idmember, nmmember, trf) 
						      VALUES (".$dta->id.", '".$form['idmember'][$i]."', '".$form['nmmember'][$i]."', '".$form['trf'][$i]."')";
					  $exeIns = $this->db->query($ins);
					  //echo $ins;
					  }	
					  $id = $dta->id;
				}		
			}
             return array("id" => $id);
		} else {
			return null;
		}
	
		
	}
	
	function getReportTrfBackpack($form) {
		$status = "";	
		if($form['status'] != "all") {
			$status = " AND status = '$form[status]'";
		} 
		
		if($form['search_type'] == "id_member") {
			$qry = "SELECT a.trf_id, a.idmember, a.nmmember, a.trf, b.id_stockist, b.status,
					CONVERT(VARCHAR(10), b.createdt, 103) as tgl, 
			        CONVERT(VARCHAR(10), b.createdt, 108) as jam
					FROM adis_trf_backpack_det a
					INNER JOIN adis_trf_backpack_hdr b ON (a.trf_id = b.id)
					WHERE a.idmember = '$form[param]' $status";
		} else {
			$qry = "SELECT *, 
			        CONVERT(VARCHAR(10), a.createdt, 103) as tgl, 
			        CONVERT(VARCHAR(10), a.createdt, 108) as jam
			        FROM adis_trf_backpack_hdr a
					WHERE a.id_stockist = '$form[param]' $status";
		}
		
		$result = $this->getRecordset($qry, null ,$this->db1);
        return $result;
	}
	
	function updateStatusPakai($form) {
		$qry = "UPDATE adis_trf_backpack_hdr SET status = '$form[status]'
		        WHERE id = ".$form['id']."";
		$query = $this->executeQuery($qry, $this->db1);
		return $query;
	}
	
	function getDetailTrf($id) {
		$qry = "SELECT *
				FROM adis_trf_backpack_det a
				WHERE a.trf_id = '$id'";
		$result = $this->getRecordset($qry, null ,$this->db1);
        return $result;
	}
}	

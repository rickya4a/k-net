<?php
class Be_barcode_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
	function getValidDO($do) {
		$arr = jsonFalseResponse("Invalid DO Number..");
		//$slc = "SELECT trcd, whcd FROM gdohdr a WHERE a.shipto = '$shipto' and trcd = '$do'";
		$slc = "SELECT trcd, whcd, shipto FROM gdohdr a WHERE trcd = '$do'";
        $result = $this->getRecordset($slc, null ,$this->db2);
		if($result != null) {
			$check = "SELECT * FROM Hilal_BC_newtrh_wh_sub WHERE trcd = '$do'";
			$result2 = $this->getRecordset($check, null ,$this->db2);
			if($result2 != null) {
				$arr = array("response" => "true", "ins_do" => "1", "arrayData" => $result2);
			} else {
				$arr = array("response" => "true", "ins_do" => "0", "arrayData" => $result);
			}      
			
		} else {
			$gnv = "SELECT TOP 1 b.do_number as trcd, a.kode as shipto, b.warehouseid_id as whcd
					FROM wmssalessimulation b
					INNER JOIN  wmswarehousemaster a  ON (a.id = b.stockiestid_id)
					WHERE b.do_number = '$do'";
			$resultGNV = $this->getRecordset($gnv, null ,$this->db2);
			if($resultGNV != null) {
				$arr = array("response" => "true", "ins_do" => "3", "arrayData" => $resultGNV);
			}
		}
        return $arr;
	}
	
	function saveUploadPrdBarcode($data, $listBarcode, $jum) {
		$exeQ = $this->load->database($this->db2, true);
		$list = 0;
		if($data['ins_do'] == "0") {
			$qry = "INSERT INTO Hilal_BC_newtrh_wh_sub (trcd, whcd, loccdTo, createnm) VALUES ('$data[do_number]', '$data[whcd]', '$data[idstk]', '".$this->username."')";
			$exe = $exeQ->query($qry);
		} 
		foreach($listBarcode as $dta) {
			$qry = "INSERT INTO Hilal_BC_newtrd_wh_sub (trcd, prdcd, prdcd_bc, qty, createnm) VALUES ('$data[do_number]', '$data[prdcd]', '".trim($dta)."', 1, '".$this->username."')";
			$exe = $exeQ->query($qry);
			if($exe > 0) {
				$list++;
			}
		}
		
		$arrx = array("jum_barcode" => $jum, "success" => $list);
		return $arrx;
	}
}
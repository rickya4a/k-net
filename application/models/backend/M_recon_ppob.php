<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_recon_ppob extends MY_Model{
	function __construct() {
		parent::__construct();
	}

	function reconPpob($trxno) {
		$query_hdr = "INSERT INTO ppob_trx_hdr (
										trx_id,
										memberid,
										membername,
										member_no_hp,
										trx_type,
										createdt,
										nominal,
										cashback_ori,
										cashback_reff,
										ppob_type,
										amount_sgo
									)
									SELECT
										trx_id,
										memberid,
										membername,
										member_no_hp,
										trx_type,
										createdt,
										nominal,
										cashback_ori,
										cashback_reff,
										ppob_type,
										amount_sgo
									FROM ppob_trx_hdr_tempsgo WHERE trx_id = '$trxno'";
		$result_hdr = $this->executeQuery($query_hdr);

		$query_det = "INSERT INTO ppob_trx_det (
										trx_id,
											memberid_target,
											qty,
											nominal,
											no_hp_simcard,
											cust_no,
											reff_pay_id
									)
									SELECT
										trx_id,
											memberid_target,
											qty,
											nominal,
											no_hp_simcard,
											cust_no,
											'REC'
									FROM ppob_trx_det_tempsgo  WHERE trx_id = '$trxno'";
		$result_det = $this->executeQuery($query_det);

		$query_pay = "INSERT INTO ppob_trx_pay(
										trx_id,
											bank_code_payment,
											charge_admin,
											docno,
											payamt
									)
									SELECT
										trx_id,
										bank_code_payment,
										charge_admin,
										docno,
										payamt
									FROM ppob_trx_pay_tempsgo WHERE trx_id = '$trxno'";
		$result_pay = $this->executeQuery($query_pay);

		$query_check = "SELECT ppob_trx_hdr.trx_id FROM dbo.ppob_trx_hdr
										INNER JOIN dbo.ppob_trx_det ON ppob_trx_hdr.trx_id = ppob_trx_det.trx_id
										INNER JOIN dbo.ppob_trx_pay ON ppob_trx_det.trx_id = ppob_trx_pay.trx_id
										WHERE ppob_trx_hdr.trx_id = '$trxno'";
		$res = $this->getRecordset($query_check);
		return $res;
		// echo $query;
	}
}

<?php

class Be_bank_model extends MY_Model{

    public function __construct(){

        parent::__construct();
    }

    public function savetolakbank($data){

       $idmember = trim($data['id_member']);
       $nmmember = $data['nm_member'];
       $tgl_tolakan = $data['tgl_tolak'];
       $tgl_tolakan= date("Y-m-d", strtotime($tgl_tolakan));
       $alasan = $data['alasan'];

        $rtn = false;
        $ins = "INSERT INTO tbl_tolakan_dummy (idmember,nama,tgl_tolak,alasan_tolak) VALUES ('$idmember','$nmmember','$tgl_tolakan','$alasan')";
        $qry = $this->executeQuery($ins, $this->db2);
        if ($qry > 0) {
            $rtn = true;
        }
        return $qry;
    }

    public function getListTolak(){

        $qry = "select * from tbl_tolakan_dummy order by tgl_tolak desc";
        $result = $this->getRecordset($qry, null, $this->db2);
        //echo $qry;
        return $result;
    }

    public function getNameMember($id_memb){

        $qry = "select * from msmemb where dfno LIKE '%$id_memb%'";
        $result = $this->getRecordset($qry, null, $this->db2);
        //echo $qry;
        return $result;
    }

}
<?php
class Be_mutasi_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
	}
	
	function get_data($y, $z, $status, $appstatus){
        /*
		 $slc = "select a.id,a.dfno,a.fullnm,sum(b.tot_fund) as tot_mutasi
				from ecomm_umroh_save_child a
				inner join ecomm_umroh_save_mut b on (a.id = b.id)
				where b.mutate_flag = '2' and a.approval_status = '1' 
				and b.pay_status = '1' and
				CONVERT(VARCHAR(10), a.createdt, 111) between '$y' and '$z' 
				group by a.id,a.dfno,a.fullnm";
		 */
        $x = $this->input->post(null,true);
        
         
		$pStatus = null;
		$pAppstatus = null;
		if($status != 'all'){
			$pStatus = " AND A.post_status = '$status' ";
		}
		if($appstatus != 'all'){
			$pAppstatus = " AND A.approval_status = '$appstatus' AND b.pay_status = '$appstatus' ";
		}
        
        //echo "dfdf ".$x['searchBy'];
        if($x['searchBy'] == "id"){
            $search = "a.dfno = '".$x['paramValue']."'";
        }elseif($x['searchBy'] == "nmmember"){
            $search = "a.fullnm like '%".$x['paramValue']."%'";
        }elseif($x['searchBy'] == "regno"){
            $search = "a.registerno = '".$x['paramValue']."'";
        }elseif($x['searchBy'] == "va"){
            $search = "c.novac = '".$x['paramValue']."'";
        }else{
            $search = "CONVERT(VARCHAR(10), a.createdt, 111) between '$y' and '$z' ";
        }
		
		$slc = "select a.id,a.dfno,a.fullnm,sum(b.tot_fund) as tot_mutasi,a.registerno,c.novac 
				from ecomm_umroh_save_child a
				inner join ecomm_umroh_save_mut b on (a.id = b.id)
                inner join klink_mlm2010.dbo.msmemb c on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				where b.mutate_flag = '2' $pAppstatus $pStatus AND
				$search
				group by a.id,a.dfno,a.fullnm,a.registerno,c.novac";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	function get_head($id){
		 $slc = "select a.fullnm,a.dfno,a.tel_hp,a.registerno,
				 		a.departureid,a.depart_status,a.post_status,a.post_dt,
				 		b.departuredesc,b.departuredt,c.novac,
				 		(SELECT D.voucherno FROM ecomm_umroh_save_mut D WHERE D.id = A.id AND D.mutate_flag='1' AND D.is_voucher='2') AS voucherno
				 from ecomm_umroh_save_child a
						 inner join ecomm_umroh_departure b on(a.departureid = b.id)
		                 inner join klink_mlm2010.dbo.msmemb c on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				 where a.id = $id";
				 
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	//buat cetak detail (CAHYONO)
	function getKWDetail($id){
		$slc = "select a.dfno,a.fullnm,b.tot_fund,c.novac,
					   b.tot_fund as total_mutasi,b.createdt,b.description, b.id2 as child
				from ecomm_umroh_save_child a
				inner join ecomm_umroh_save_mut b on (a.id = b.id)
                inner join klink_mlm2010.dbo.msmemb c on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				where b.id2 = $id and b.mutate_flag = '2'
				group by a.dfno,a.fullnm,b.tot_fund,b.createdt,b.description,c.novac, b.tot_fund, b.id2
				order by b.createdt";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	function get_details($id){
		$slc = "select a.dfno,a.fullnm,b.tot_fund,c.novac, 
					   b.tot_fund as total_mutasi,b.createdt,b.description, b.id2 as child
				from ecomm_umroh_save_child a
				inner join ecomm_umroh_save_mut b on (a.id = b.id)
                inner join klink_mlm2010.dbo.msmemb c on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				where a.id = $id and b.mutate_flag = '2'
				group by a.dfno,a.fullnm,b.tot_fund,b.createdt,b.description,c.novac, b.tot_fund, b.id2
				order by b.createdt";
		//echo $slc;
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}
	
	function get_listing(){
		$slc = "select *
				from ecomm_umroh_save_child
				inner join ecomm_umroh_save_mut on (a.id = b.id)
				";
		
		$result = $this->getRecordset($slc,null,$this->db1);
		return $result;
	}

	//PRIN KW (CAHYONO)
	function getPrintInst($regno, $no){
		$slc = "
		select a.dfno,a.fullnm,b.tot_fund,c.novac,
					   b.tot_fund as total_mutasi,b.createdt,b.description, $no as nomor
				from ecomm_umroh_save_child a
				inner join ecomm_umroh_save_mut b on (a.id = b.id)
                inner join klink_mlm2010.dbo.msmemb c on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				where a.id = $regno and b.mutate_flag = '2'
				group by a.dfno,a.fullnm,b.tot_fund,b.createdt,b.description,c.novac, b.tot_fund
				order by b.createdt
		";
		//echo $slc;
		$result = $this->getRecordset($slc);
		return $result;
	}
}	
?>
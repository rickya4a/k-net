<?php
class Be_product_model extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
		$this->load->driver('cache', array('adapter' => 'redis', 'backup' => 'file'));
		$this->load->service("webshop/Redis_service",'redisService');
	}

	/*------------------
	 * PRODUCT CATEGORY
	 --------------------*/

	function getListProductCat($param, $value) {
		$qry = "SELECT * FROM master_prd_cat WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("No result", 1);
		}
		return $res;
	}

	function saveInputProductCat() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_prd_cat (cat_id, cat_desc, status, 
		               remarks, createnm)
		        VALUES ('$data[cat_id]', '$data[cat_desc]', '$data[status]', 
		              '$data[remarks]', '".$this->username."')";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Save Product Category failed..!!", 1);
		}
		return $query;
	}

	function getListAllProductCat() {
		$qry = "SELECT * FROM master_prd_cat";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data user group is empty..!", 1);
		}
		return $res;
	}

	function saveUpdateProductCat() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "UPDATE master_prd_cat SET cat_desc = '$data[cat_desc]', 
		               remarks = '$data[remarks]', status = '$data[status]'
		        WHERE cat_id = '$data[cat_id]'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Update Product Category failed..!!", 1);
		}
		return $query;
	}

	function deleteProductCat($id) {
		$qry = "DELETE FROM master_prd_cat WHERE cat_id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Product Category failed..!!", 1);
		}
		return $query;
	}

	/*------------------
	 * PRICE CODE
	 --------------------*/
	function getListAllPriceCode() {
		$qry = "SELECT * FROM master_prd_pricecode";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data price code is empty..!", 1);
		}
		return $res;
	}

	function getListPriceCode($param, $value) {
		$qry = "SELECT * FROM master_prd_pricecode WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("No result", 1);
		}
		return $res;
	}

	function savePriceCode($data) {
		//$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_prd_pricecode (pricecode, pricecode_desc, status, 
		               country_id, hq_id, branch_id, remarks, createnm)
		        VALUES ('$data[pricecode]', '$data[pricecode_desc]', '$data[status]',
		                '$data[country_id]', '$data[hq_id]', '$data[branch_id]',
		              '$data[remarks]', '".$this->username."')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Price Code DB Commerce failed..!!", 1);
		}
		return $query;
	}

	function savePriceCodeKlinkMlm($data) {
		$qry = "INSERT INTO pricecode (code, description, status,
		               PT_SVRID, notes, createnm, createdt)
		        VALUES ('$data[pricecode]', '$data[pricecode_desc]', '$data[status]',
		                '$data[country_id]', '$data[remarks]', '".$this->username."', '".$this->dateTime."')";

		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Price Code Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function updatePriceCode($data) {
		//$data = $this->input->post(NULL, TRUE);
		$qry = "update master_prd_pricecode SET pricecode_desc = '$data[pricecode_desc]', status = '$data[status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]', 
		             remarks = '$data[remarks]'
		        WHERE pricecode = '$data[pricecode]'";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Update Price Code DB Commerce failed..!!", 1);
		}
		return $query;
	}

	function updatePriceCodeKlinkMlm($data) {
		$qry = "update pricecode SET description = '$data[pricecode_desc]', status = '$data[status]',
		             PT_SVRID = '$data[country_id]', notes = '$data[remarks]'
		        WHERE code = '$data[pricecode]'";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Update Price Code KlinkMlm failed..!!", 1);
		}
		return $query;
	}

	function deletePriceCode($id) {
		$qry = "DELETE FROM master_prd_pricecode WHERE pricecode = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Price Code failed..!!", 1);
		}
		return $query;
	}

	/*------------------
     PRODUCT
    --------------------*/
	function getlistAllProduct($noexception = true) {
		$qry = "SELECT
				  dbo.master_prd_cat_inv.cat_inv_id,
				  dbo.master_prd_cat_inv.cat_id,
				  dbo.master_prd_cat.cat_desc,
				  dbo.master_prd_cat_inv.cat_inv_desc,
				  dbo.master_prd_cat_inv.is_discontinue,
				  dbo.master_prd_cat_inv.max_order
				FROM
				  dbo.master_prd_cat_inv
				INNER JOIN dbo.master_prd_cat ON (dbo.master_prd_cat_inv.cat_id = dbo.master_prd_cat.cat_id)";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Product is empty..!", 1);
			}
		}
		return $res;
	}

	function getListProduct($param, $value) {
		$qry = "SELECT * FROM master_prd_cat_inv WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("GA ADA ISIIII", 1);
		}
		return $res;
	}

	function getListBanner($param, $value) {
		$qry = "SELECT * FROM ecomm_master_header_pic WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("gA ADA ISIIII", 1);
		}
		return $res;
	}

	function inputProduct($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		if($parent_cat_inv_id == "") {
			$parent_cat_inv_id = $data['cat_inv_id'];
		}
		$qry = "INSERT INTO master_prd_cat_inv (cat_inv_id, cat_id, parent_cat_inv_id, 
		               cat_inv_desc, inv_type, status, bo_inv_status, bo_status,
					   sc_status, sub_status, ms_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, img_name, weight,
					   is_starterkit, is_discontinue, is_charge_ship, flag_is_do,max_order)
		        VALUES ('$data[cat_inv_id]', '$data[cat_id]', '$parent_cat_inv_id',
		                '$data[cat_inv_desc]', '$data[inv_type]', '$data[status]',
		              '$data[bo_inv_status]', '$data[bo_status]', '$data[sc_status]',
		              '$data[sub_status]', '$data[ms_status]', '$data[ecomm_status]',
					  '$data[web_status]', '$data[country_id]', '$data[hq_id]',
					  '$data[branch_id]', '".$this->username."', '".$_FILES["myfile"]["name"]."', ".$data['weight'].",
					  '$data[is_starterkit]', '$data[is_discontinue]', '$data[is_charge_ship]', '$data[flag_is_do]', '$data[max_order]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function inputBanner($data) {

		$qry = "INSERT INTO ecomm_master_header_pic (hdr_desc, goup_hdr, img_url,
		               hdr_status, tipe_banner,  uploadDate)
		        VALUES ('$data[hdr_desc]', 'banner1',  'assets/images/banner_home/".$_FILES["myfile"]["name"]."',
		                '$data[hdr_status]', '1', getdate()
		                )";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}
	function getlistAllBanner($noexception = true) {
		$qry = "SELECT
				  id,
				  hdr_desc,
				  hdr_status,
				  uploadDate
				FROM
				  dbo.ecomm_master_header_pic
				  ;
				";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Banner is empty..!", 1);
			}
		}
		return $res;
	}

	function inputProductWMS($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		$bundle=$data['inv_type'];
		if($bundle =='S')
		{
			if($parent_cat_inv_id == "") {
				$parent_cat_inv_id = $data['cat_inv_id'];

				$qry = "INSERT INTO klink_whm.dbo.MASTER_PRODUK (
				ID_PRODUCT,
				PRODUCT_CODE,
				PRODUCT_NAME,
				DESCRIPTION,
				PIECE,
				BOX,
				PALLETE,
				IS_ACTIVE,
				CREATED_DATE,
				CREATED_BY,

				ID_UOM,
				BERAT

				)
		        VALUES ('$data[cat_inv_id]', '$data[cat_inv_id]',
		                '$data[cat_inv_desc]', '$data[cat_inv_desc]', 1,
		              '$data[box]', 1, 0,GETDATE(), 'K-NET', '1',
					   ".$data['weight']."
					  );";
				//echo $qry;
				$query = $this->executeQuery($qry, $this->setDB(1));

				$qry2 = "INSERT INTO klink_whm.dbo.MASTER_PRODUK_ALIAS (
					PRODUK_ALIAS_ID,
					ID_PRODUCT,
					ALIAS_CODE,
					ALIAS_NAME,
					IS_ACTIVE,
					CREATED_DATE,
					CREATED_BY,
					IS_BUNDLE
				)
		        VALUES ('$parent_cat_inv_id', '$parent_cat_inv_id',
		                '$data[cat_inv_id]', '$data[cat_inv_desc]', 0,
		           		GETDATE(), 'K-NET', '0'
					  );";
				//echo $qry;
				$query2 = $this->executeQuery($qry2, $this->setDB(1));
			}
			else
			{

					$qry2 = "INSERT INTO klink_whm.dbo.MASTER_PRODUK_ALIAS (
						PRODUK_ALIAS_ID,
						ID_PRODUCT,
						ALIAS_CODE,
						ALIAS_NAME,
						IS_ACTIVE,
						CREATED_DATE,
						CREATED_BY,
						IS_BUNDLE
					)
							VALUES ('".$this->uuid->v4()."', '$parent_cat_inv_id',
											'$data[cat_inv_id]', '$data[cat_inv_desc]', 0,
										 GETDATE(), 'K-NET', '0'
							);";
					//echo $qry;
					$query2 = $this->executeQuery($qry2, $this->setDB(1));
				
			}
		}
		else
		{
			$qry2 = "INSERT INTO klink_whm.dbo.MASTER_PRODUK_ALIAS (
					PRODUK_ALIAS_ID,
					ALIAS_CODE,
					ALIAS_NAME,
					IS_ACTIVE,
					CREATED_DATE,
					CREATED_BY,
					IS_BUNDLE
				)
		        VALUES ('".$this->uuid->v4()."',
		                '$data[cat_inv_id]', '$data[cat_inv_desc]', 0,
		           		GETDATE(), 'K-NET', '1'
					  );";
			//echo $qry;
			$query2 = $this->executeQuery($qry2, $this->setDB(1));

		}

		if(!$query2) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query2;
	}

	
	function inputProductMapforMobile($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		if($parent_cat_inv_id == "") {
			$parent_cat_inv_id = $data['cat_inv_id'];
		}
		$qry = "INSERT INTO master_prd_cat_inv_mobile (cat_inv_id, cat_id, parent_cat_inv_id, 
		               cat_inv_desc, inv_type, status, bo_inv_status, bo_status,
					   sc_status, sub_status, ms_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, img_name, weight,
					   is_starterkit, is_discontinue, is_charge_ship, flag_is_do)
		        VALUES ('$data[cat_inv_id]', '$data[cat_id]', '$parent_cat_inv_id',
		                '$data[cat_inv_desc]', '$data[inv_type]', '$data[status]',
		              '$data[bo_inv_status]', '$data[bo_status]', '$data[sc_status]',
		              '$data[sub_status]', '$data[ms_status]', '$data[ecomm_status]',
					  '$data[web_status]', '$data[country_id]', '$data[hq_id]',
					  '$data[branch_id]', '".$this->username."', '".$_FILES["myfile"]["name"]."', ".$data['weight'].",
					  '$data[is_starterkit]', '$data[is_discontinue]', '$data[is_charge_ship]', '$data[flag_is_do]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function inputProductKlinkMlm($data) {
		$qry = "INSERT INTO msprd (prdcd, prdnm, description, category, status, webstatus, scstatus, PT_SVRID, glposting_cd,
		               createnm, createdt)
	 	        VALUES ('$data[cat_inv_id]', '$data[cat_inv_desc]', '$data[cat_inv_desc]', '$data[cat_id]', '$data[status]',
				   '$data[web_status]', '$data[sc_status]', 'ID', 'GO', '".$this->username."', '".$this->dateTime."')";
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Product DB Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function updateProduct($data) {
		//$data = $this->input->post(NULL, TRUE);
		$ss = "";
		if($_FILES["myfile"]["name"] != "") {
			$ss .= ", img_name = '".$_FILES["myfile"]["name"]."' ";
		}
		//print_r($data);
		
		$prdStatus = '1';
		if($data['ecomm_status'] == '2' || $data['ecomm_status'] == '0' || $data['status'] == '0' ){
			$prdStatus = '0';
		}
		
		$qry = "UPDATE master_prd_cat_inv SET cat_id = '$data[cat_id]', parent_cat_inv_id = '$data[parent_cat_inv_id]',
		             cat_inv_desc = '$data[cat_inv_desc]', inv_type = '$data[inv_type]', status = '$data[status]', bo_inv_status = '$data[bo_inv_status]',
		             bo_status = '$data[bo_status]', sc_status = '$data[sc_status]', sub_status = '$data[sub_status]', ms_status = '$data[ms_status]',
		             ecomm_status = '$data[ecomm_status]', web_status = '$data[web_status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]', weight = ".$data['weight'].",
		             is_starterkit = '$data[is_starterkit]', is_discontinue = '$data[is_discontinue]', is_charge_ship = '$data[is_charge_ship]', flag_is_do = ".$data['flag_is_do'].",
		             max_order = '$data[max_order]'
		             $ss
		        WHERE cat_inv_id = '$data[cat_inv_id]';
		        
		        UPDATE pricelist_vera SET status = '$prdStatus' WHERE prdcd = '$data[cat_inv_id]'";
		//echo $qry;
		
		//delete cache
		$this->db->cache_delete('default', 'index');
		$this->db->cache_delete('shop', 'product');
		$this->db->cache_delete('shop', 'product_org');
		$this->db->cache_delete('shop', 'productHeader');
		$this->redisService->delProdRelatedCache();

		
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Update Product failed..!!", 1);
		}
		return $query;
	}
	function updateBanner($data) {
		//$data = $this->input->post(NULL, TRUE);
		$ss = "";
		if($_FILES["myfile"]["name"] != "") {
			$ss .= ", img_url = '".$_FILES["myfile"]["name"]."' ";
		}
		$qry = "UPDATE ecomm_master_header_pic SET hdr_desc = '$data[hdr_desc]',
		             hdr_status = '$data[hdr_status]' $ss
		        WHERE id = '$data[ixd]'";
//		echo $qry;
		$query = $this->db->query($qry);
		$this->db->cache_delete('default', 'index');
		$this->db->cache_delete('shop', 'product');
		$this->cache->delete('cache:mainbanner');

		if(!$query) {
			throw new Exception("Update Product failed..!!", 1);
		}
		return $query;
	}
	function updateProductKlinkMlm($data) {
		$qry = "UPDATE msprd SET prdnm = '$data[cat_inv_desc]', description = '$data[cat_inv_desc]',
	 		             category = '$data[cat_id]', status = '$data[status]', webstatus = '$data[web_status]', 
	 		             scstatus = '$data[sc_status]', PT_SVRID = 'ID', glposting_cd = 'GO',
		               updatenm = '".$this->username."', updatedt = '".$this->dateTime."'
		            WHERE prdcd = '$data[cat_inv_id]'";
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Product DB Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function deleteProduct($id) {
		$qry = "DELETE FROM master_prd_cat_inv WHERE cat_inv_id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Product failed..!!", 1);
		}
		return $query;
	}

	/*---------------------
     * PRODUCT PRICE
     -----------------------*/
	function showProductPriceCode($data) {
		$qry = "SELECT
				  dbo.master_prd_pricetab.pricecode,
				  dbo.master_prd_pricecode.pricecode_desc,
				  dbo.master_prd_pricetab.cat_inv_id,
				  dbo.master_prd_pricetab.cp,
				  dbo.master_prd_pricetab.dp,
				  dbo.master_prd_pricetab.bv,
				  dbo.master_prd_pricetab.tax
				FROM
				  dbo.master_prd_pricetab
				  INNER JOIN dbo.master_prd_pricecode ON (dbo.master_prd_pricetab.pricecode = dbo.master_prd_pricecode.pricecode)
				WHERE
				  (dbo.master_prd_pricetab.cat_inv_id = '$data[cat_inv_id]') AND 
				  (dbo.master_prd_pricetab.country_id = '$data[country_id]') AND 
				  (dbo.master_prd_pricetab.hq_id = '$data[hq_id]') AND 
				  (dbo.master_prd_pricetab.branch_id = '$data[branch_id]') AND status = '1'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		return $res;
	}

	function saveProductPrice($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		for($i=0; $i < $jum; $i++) {
			$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('$data[month]', '$data[year]', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$data[cat_inv_id]', '".$data['pricecode'][$i]."',
				              ".$data['cp'][$i].", ".$data['dp'][$i].", ".$data['bv'][$i].", '".$this->username."')";
			$query = $this->executeQuery($qry, $this->setDB(1));
			if(!$query) {
				$err++;
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB-ECOMMERCE failed..!!", 1);
		}
		return $query;
	}

	function saveproductPriceKlinkMLM($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;
		for($i=0; $i < $jum; $i++) {
			$check = "SELECT pricecode FROM pricetab WHERE pricecode = '".$data['pricecode'][$i]."' AND prdcd = '$data[cat_inv_id]'";
			$check2 = $this->getRecordset($check, NULL, $this->db2);

			if($check2 == null) {
				$qry = "INSERT INTO pricetab (prdcd, pricecode, cp, dp, bv, pv, createnm)
					               
					        VALUES ('$data[cat_inv_id]', '".$data['pricecode'][$i]."',
					              ".$data['cp'][$i].", ".$data['dp'][$i].", ".$data['bv'][$i].", ".$data['bv'][$i].", '".$this->username."')";
				$query = $this->executeQuery($qry, $this->setDB(2));
				if(!$query) {
					$err++;
				}
			} else {
				$redundant++;
				$prd = "Product $data[cat_inv_id] with pricecode ".$data['pricecode'][$i]." already exist in DB KlinkMLM";
				break;

			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB klinkmlm failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;
	}

	function deleteProductPrice($data) {
		//$jum = count($data['pricecode']);
		//for($i=0; $i < $jum; $i++) {
		/*
		$qry = "DELETE FROM master_prd_pricetab
		  	          WHERE cat_inv_id = '".$data['cat_inv_id']."' 
									AND country_id = '".$data['country_id']."'
		  	          AND hq_id = '".$data['hq_id']."' 
									AND branch_id = '".$data['branch_id']."'
									AND period_month = ".$data['month']."
									AND period_year = ".$data['year'].""; */
		$qry = "DELETE FROM master_prd_pricetab
		  	          WHERE cat_inv_id = '".$data['cat_inv_id']."'";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		//}
		if(!$query) {
			throw new Exception("Delete Price Code failed..!!", 1);
		}
		return $query;
	}

	function deleteProductPriceKlinkMlm($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		for($i=0; $i < $jum; $i++) {
			$qry = "DELETE FROM pricetab WHERE prdcd = '$data[cat_inv_id]' AND pricecode = '".$data['pricecode'][$i]."'";
			$query = $this->executeQuery($qry, $this->setDB(2));
			if(!$query) {
				$err++;
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function getProductClaim($data) {
		$qry = "SELECT a.invoiceno, a.remark,
				CONVERT(VARCHAR(10), a.invoicedt, 103) AS invoicedt, 
				a.tdp, a.usernya
				FROM KL_TEMPTRANS a 
				where a.remark like '$data[name]%' and
				(a.registerdt>='$data[claim_from]' and a.registerdt <= '$data[claim_to]')
				order by a.invoicedt";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			throw new Exception(setErrorMessage(), 1);
		}
		return $res;
	}

	function getProductClaimDetail($id) {
		$qry = "SELECT b.prdcd, b.prdnm, b.qtyord,
			     b.tdp, b.qtyord * b.tdp as totaldp  
				FROM QKL_TRANS b 
				WHERE b.invoiceno = '$id'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			throw new Exception(setErrorMessage(), 1);
		}
		return $res;
	}

	/*-----------------------
     * PRODUCT BUNDLING
     * ---------------------*/
	function getListAllProductBundling() {
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Dion_Baru a 
                WHERE a.prdcdcat = '13' and a.price_w is not null and a.price_e is not null order by prdnm";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListProductBundling($param, $id) {

	}

	function getListPrdBundlingHeader($id) {
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Dion_Baru a
                WHERE a.prdcd = '$id' and a.price_w is not null and a.price_e is not null";
				//and a.price_w is not null and a.price_e is not null
		//echo "Query Bundling 01 : $qry";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListPrdBundlingDetail($id) {
		//V_Ecomm_PriceList_Prd_Bundling_Baru
		$qry = "select * from V_Ecomm_PriceList_Prd_Bundling_BaruV2 a WHERE a.prdcd_parent = '$id'";
		//echo "Query Bundling Detail 02 : $qry";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
/*
	function getListPrdBundlingDetailKlinkMlm($id) {
		$qry = "select * FROM DION_prdbundling_detail a  WHERE a.prdcd_parent = '$id'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
*/
	function getListPrdBundlingDetailKlinkMlm($id)
	{
		/*
		a.prdcd_parent, a.prdcd, a.prdnm, a.qty, a.bv, a.tot_bv, a.price_w, a.tot_price_w,
						a.price_e, a.tot_price_e, a.price_cw, a.tot_price_cw, a.price_ce, a.tot_price_ce,
						b.weight AS weight1,
		*/
		$qry = "select 
						
					a.prdcd_parent, a.prdcd, a.prdnm, 
					a.qty, a.bv, a.tot_bv, a.price_w, a.tot_price_w,
					ISNULL(a.price_e, 0) AS price_e, 
					ISNULL(a.tot_price_e, 0) AS tot_price_e, 
					a.price_cw, 
					a.tot_price_cw, 
					ISNULL(a.price_ce, 0) AS price_ce, 
					ISNULL(a.tot_price_ce, 0) AS tot_price_ce,	
						(SELECT X.weight FROM [db_ecommerce].[dbo].[master_prd_cat_inv] X WHERE X.cat_inv_id COLLATE SQL_Latin1_General_CP1_CS_AS=a.prdcd) AS weight
		FROM [klink_mlm2010].[dbo].[DION_prdbundling_detailV2] a
		LEFT JOIN [db_ecommerce].[dbo].[master_prd_cat_inv] b
		ON a.prdcd_parent = b.cat_inv_id COLLATE SQL_Latin1_General_CP1_CS_AS
		WHERE a.prdcd_parent = '$id'";
		//echo $qry;
		//update from b.parent_cat_inv_id to b.cat_inv_id by DION @24/08/2018
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	//tambahan fendi
	function updateWeight($data) {

		$srvReturn = false;
		$berat = $data['tot_all_weight'];
		$idbarang = $data['cat_inv_id_parent'];

		$qry = "UPDATE master_prd_cat_inv
				SET weight = '$berat'
				WHERE cat_inv_id = '$idbarang'";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Update data Product to master_prd_cat_inv DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function saveInputProductBundlingDetail($data) {
		$jum = count($data['productcode']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;

		$prdcd = $data['cat_inv_id_parent'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}

		$del = "DELETE FROM master_prd_bundling WHERE cat_inv_id_parent = '".$prdcd."'";
		$delQuery = $this->executeQuery($del, $this->setDB(1));

		for($i=0; $i < $jum; $i++) {
			if($data['productcode'][$i] != "" || $data['productcode'][$i] != " ") {
				$qry = "INSERT INTO master_prd_bundling (cat_inv_id_child, cat_inv_id_parent, cat_desc, qty, status, createnm, createdt)
					    VALUES ('".$data['productcode'][$i]."', '".$prdcd."', '".$data['productname'][$i]."', ".$data['qty_real'][$i].",
					              '1', '".$this->username."', '".$this->dateTime."')";
				//echo $qry;
				$query = $this->executeQuery($qry, $this->setDB(1));
				if(!$query) {
					$err++;

				}
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product Bundling failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;


	}

	function saveInputProductBundlingPricetab($data) {
		$err = 0;
		$prdcd = $data['cat_inv_id_parent'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}
		$check = "SELECT pricecode FROM master_prd_pricetab WHERE cat_inv_id = '".$prdcd."'";
		$check2 = $this->getRecordset($check, NULL, $this->db1);
		if($check2 != null) {
			$delQry = "DELETE FROM master_prd_pricetab WHERE cat_inv_id = '".$prdcd."'";
			$del = $this->executeQuery($delQry, $this->setDB(1));
		}

		$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$prdcd', '12W4',
				              ".$data['cust_west_price_real'].", ".$data['west_price_real'].", 
				              ".$data['bv_real'].", '".$this->username."')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			$err++;
		}

		$qry2 = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$prdcd', '12E4',
				              ".$data['cust_east_price_real'].", ".$data['east_price_real'].", 
				              ".$data['bv_real'].", '".$this->username."')";
		$query2 = $this->executeQuery($qry2, $this->setDB(1));
		if(!$query2) {
			$err++;
		}

		if($err > 0) {
			throw new Exception("Input pricetab product $data[prdid] failed..", 1);
		}
		return $err;



	}

	function saveInputProductBundlingDetailKlink() {


	}

	function saveInputProductBundlingPricetabKlink($data) {

	}

	function inputProductBundlingKlinkMLM($data) {
		$prdcd = $data['cat_inv_id_parent'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}
		$SP_HILAL_insertGrpPrdHDR = "EXEC SP_HILAL_insertGrpPrdHDR
                '".$prdcd."',
                '".$data['cat_desc']."',
                '".$data['cat_desc']."',
                '".$this->username."',
                '".$this->username."',
                ".$data['west_price_real'].",
                ".$data['cust_west_price_real'].",
                ".$data['bv_real'].",
                '',
                '13'";
		//echo $qry;
		//echo "<br />";
		$query2 = $this->executeQuery($SP_HILAL_insertGrpPrdHDR, $this->setDB(2));


		$i = 0;
		for($i = 0; $i < count($data['productcode']); $i++)
		{
			if($data['productcode'][$i] != "")
			{
				$det = "EXEC SP_HILAL_insertGrpPrdDET
                            '".$this->username."',
                            '".$this->username."',
                            '',
                            '".$data['productcode'][$i]."',
                            '".$data['productname'][$i]."',
                            '".$prdcd."',
                            ".$data['west_real'][$i].",
                            ".$data['qty_real'][$i].",
                            ".$data['west_real'][$i].",
                            ".$data['tbv_real'][$i].",
                            '13'";
				//echo $det;
				//echo "<br />";
				$det2 = $this->executeQuery($det, $this->setDB(2));


			}

		}


		$price_tab = "EXEC SP_HILAL_insertPrdPricetab
                '".$prdcd."',
                '".$data['cat_desc']."',
                '".$data['cat_desc']."',
                '".$this->username."',
                '".$this->username."',
                ".$data['west_price_real'].",
                ".$data['cust_west_price_real'].", 
                ".$data['bv_real'].",
                '12W4',
                '13',
                ".$data['east_price_real'].",
                ".$data['cust_east_price_real'].",
                '12E4'";

		/*
         CREATE PROCEDURE dbo.SP_HILAL_insertPrdPricetab
        @grpPrd  varchar(35),
        @grpPrdNm  varchar(100),
        @grpPrdDesc varchar(100),
        @createNm varchar(30),
        @updateNm varchar(30),
        @TotpriceDistW numeric(15, 2),
        @TotpriceCustW numeric(15, 2),
        @TotbvDist numeric(5, 2),
        @priceCodeW varchar(4),
        @categoryPrd varchar(5),
        @TotpriceDistE numeric(15, 2),
        @TotpriceCustE numeric(15, 2),
        @priceCodeE varchar(4)
         */
		//".$data['bv_real'].",
		//echo $price_tab."<br />";
		//echo "<br />";
		//echo "CUSTOMER EAST = ".$data['cust_east_price_real'];
		$price_tab2 = $this->executeQuery($price_tab, $this->setDB(2));
		//$detprice_tab2 = $this->executeQuery($price_tab2, $this->setDB(2));

	}

	function deleteProductBundlingKlinkMLM($data) {
		$srvReturn = false;
		//DELETE table newera_PRDDET
		/*$delnewera_PRDDET = "DELETE FROM newera_PRDDET WHERE prdcdCat = '".$data['prdid']."'";
     $stt_newera_PRDDET = $this->executeQuery($delnewera_PRDDET, $this->setDB(2));
     if($stt_newera_PRDDET > 0) {
           //DELETE table pricetab
           $delnewera_pricetab = "DELETE FROM pricetab WHERE prdcd = '".$data['prdid']."'";
           $stt_pricetab = $this->executeQuery($delnewera_pricetab, $this->setDB(2));
         if($stt_pricetab > 0) {
               //DELETE msprd
               $delnewera_msprd = "DELETE FROM msprd WHERE prdcd = '".$data['prdid']."'";
               $stt_msprd = $this->executeQuery($delnewera_msprd, $this->setDB(2));
             if($stt_msprd > 0) {
                   //DELETE newera_PRDCAT
                   $delnewera_PRDCAT = "DELETE FROM newera_PRDCAT WHERE prdcd = '".$data['prdid']."'";
                   $stt_newera_PRDCAT = $this->executeQuery($delnewera_PRDCAT, $this->setDB(2));
                 if($stt_newera_PRDCAT > 0) {
                      $srvReturn = true;
                 } else {
                      throw new Exception("(Step 4) : DELETE data newera_PRDCAT failed..", 1);
                 }
             } else {
                  throw new Exception("(Step 3) : DELETE data msprd failed..", 1);
             }
         } else {
              throw new Exception("(Step 2) : DELETE data pricetab failed..", 1);
         }
     } else {
          throw new Exception("(Step 1) : DELETE data newera_PRDDET failed..", 1);
     }*/
		$prdcd = $data['cat_inv_id_parent'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}

		$delnewera_PRDDET = "DELETE FROM newera_PRDDET WHERE prdcdCat = '".$prdcd."'";
		$stt_newera_PRDDET = $this->executeQuery($delnewera_PRDDET, $this->setDB(2));

		$delnewera_pricetab = "DELETE FROM pricetab WHERE prdcd = '".$prdcd."'";
		$stt_pricetab = $this->executeQuery($delnewera_pricetab, $this->setDB(2));

		$delnewera_msprd = "DELETE FROM msprd WHERE prdcd = '".$prdcd."'";
		$stt_msprd = $this->executeQuery($delnewera_msprd, $this->setDB(2));

		$delnewera_PRDCAT = "DELETE FROM newera_PRDCAT WHERE prdcd = '".$prdcd."'";
		$stt_newera_PRDCAT = $this->executeQuery($delnewera_PRDCAT, $this->setDB(2));
		return true;
	}

	/*----------------------------------------------
     PRODUCT IMPORT FROM PINEAPPLE TO DB COMMERCE
    ----------------------------------------------*/
	function getHeaderPrdPine($id) {
		$qry = "select * from DION_msprd_pricetab a WHERE a.prdcd = '$id'";
		$res = $this->getRecordset($qry, null, $this->db2);
		if($res ==  null) {
			throw new Exception("Pricetab product $id is not exist..", 1);
		}
		return $res;
	}

	function importProductFromPine($data) {
		$qry = "INSERT INTO master_prd_cat_inv (cat_inv_id, cat_id, parent_cat_inv_id,
		               cat_inv_desc, inv_type, status, 
					   sc_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, 
					   is_starterkit, weight)
		        VALUES ('$data[head_prdcd]', '$data[cat_id]', '$data[head_prdcd]',
		                '$data[head_prdnm]', '$data[inv_type]', '$data[status]',
		              '$data[sc_status]', '0',
					  '$data[web_status]', 'ID', 'BID006',
					  'B001', '".$this->username."', 
					  '0','$data[weight]')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Import data Product to master_prd_cat_inv DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function importPricetabFromPine($data) {
		$err = 0;

		$check = "SELECT pricecode FROM master_prd_pricetab WHERE cat_inv_id = '".$data['head_prdcd']."'";
		$check2 = $this->getRecordset($check, NULL, $this->db1);
		if($check2 != null) {
			$delQry = "DELETE FROM master_prd_pricetab WHERE cat_inv_id = '".$data['head_prdcd']."'";
			$del = $this->executeQuery($delQry, $this->setDB(1));
		}

		$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', 'ID',
				                'BID06', 'B001', '$data[head_prdcd]', '12W4',
				              ".$data['total_cpw'].", ".$data['total_dpw'].", 
				              ".$data['total_bv'].", '".$this->username."')";
		//echo $qry."<br />";					  
	    $query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			$err++;
		}

		$qry2 = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', 'ID',
				                'BID06', 'B001', '$data[head_prdcd]', '12E4',
				              ".$data['total_cpe'].", ".$data['total_dpe'].", 
				              ".$data['total_bv'].", '".$this->username."')";
	    //echo $qry2;
		$query2 = $this->executeQuery($qry2, $this->setDB(1));
		if(!$query2) {
			$err++;
		}

		if($err > 0) {
			throw new Exception("Input pricetab product $data[prdid] failed..", 1);
		}
		return $err;
	}

	function importProductBundlingDetail($data) {
		$jum = count($data['prdcd']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;
		for($i=0; $i < $jum; $i++) {
			$qry = "INSERT INTO master_prd_bundling (cat_inv_id_child, cat_inv_id_parent, cat_desc, qty, status, createnm, createdt)
					    VALUES ('".$data['prdcd'][$i]."', '".$data['head_prdcd']."', '".$data['prdnm'][$i]."', ".$data['qty'][$i].",
					              '1', '".$this->username."', '".$this->dateTime."')";
			$query = $this->executeQuery($qry, $this->setDB(1));
			if(!$query) {
				$err++;

			}
		}
		if($err > 0) {
			throw new Exception("Import Detail Bundling on master_prd_bundling DB E-Commerce failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;

	}

	function prosesCekStokist($loccd){

		$qry = "SELECT fullnm from mssc where loccd='".$loccd."'";
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function prosesSaveNonAvailable($loccd,$prdcd,$prdnm){
		/*$qry="INSERT INTO master_stock_not_avail_dummy(whcd,loccd,prdcd,prddesc,status,remarks,createnm,createdt)
     	VALUES('".$loccd."','".$loccd."','".$prdcd."','".$prdnm."','1','KOSONG','".$this->username."','".$this->dateTime."')";*/

		$qry="INSERT INTO master_warehouse_stock_not_avail(whcd,loccd,prdcd,prddesc,status,remarks,createnm,createdt)
     	VALUES('".$loccd."','".$loccd."','".$prdcd."','".$prdnm."','1','KOSONG','".$this->username."','".$this->dateTime."')";
		return $this->executeQuery($qry,$this->db1);
	}

	function prosesUpdtNonAvailable($prdcd){

		$qry2 = "UPDATE master_prd_cat_inv SET flag_non_available = 'OFF' WHERE cat_inv_id='".$prdcd."'";
		return $this->executeQuery($qry2,$this->db1);
	}

	function prosesDelNonAvailable($loccd,$prdcd,$prdnm){
		//$qry="DELETE FROM master_stock_not_avail_dummy WHERE loccd='".$loccd."' AND prdcd='".$prdcd."'";

		$qry="DELETE FROM master_warehouse_stock_not_avail WHERE loccd='".$loccd."' AND prdcd='".$prdcd."'";
		return $this->executeQuery($qry,$this->db1);
	}

	function prosesUpdtNonAvailable2($prdcd){

		$qry2 = "UPDATE master_prd_cat_inv SET flag_non_available = '' WHERE cat_inv_id='".$prdcd."'";
		return $this->executeQuery($qry2,$this->db1);
	}


	function prosesCekNonAvailable($loccd,$prdcd){

		/*$qry = "SELECT * from master_stock_not_avail_dummy
			    where whcd='".$loccd."' AND loccd='".$loccd."' AND prdcd='".$prdcd."'";*/

		$qry = "SELECT * from master_warehouse_stock_not_avail
			    where whcd='".$loccd."' AND loccd='".$loccd."' AND prdcd='".$prdcd."'";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function prosesCekProduct($prdcd){

		$qry = "SELECT * from msprd where prdcd='".$prdcd."'";
		$result = $this->getRecordset($qry,null,$this->db2);
		return $result;
	}

	function getlistAllProduct2($loccd) {
		/*$qry = "SELECT
				  dbo.master_prd_cat_inv.cat_inv_id,
				  dbo.master_prd_cat_inv.cat_id,
				  dbo.master_prd_cat.cat_desc,
				  dbo.master_prd_cat_inv.cat_inv_desc,
				  dbo.master_prd_cat_inv.is_discontinue,
				  dbo.master_prd_cat_inv.flag_non_available
				FROM
				  dbo.master_prd_cat_inv
				INNER JOIN dbo.master_prd_cat ON (dbo.master_prd_cat_inv.cat_id = dbo.master_prd_cat.cat_id)";*/
		$noexception = true;
		$qry = "SELECT
				  a.cat_inv_id,
				  a.cat_id,
				  b.cat_desc,
				  a.cat_inv_desc,
				 a.is_discontinue,
				  c.loccd,
				  c.remarks as flag_non_available
				FROM
				  dbo.master_prd_cat_inv a
				INNER JOIN dbo.master_prd_cat b ON (a.cat_id = b.cat_id)
				/*LEFT JOIN dbo.master_stock_not_avail_dummy c ON (a.cat_inv_id = c.prdcd)*/
				LEFT JOIN dbo.master_warehouse_stock_not_avail c ON (a.cat_inv_id = c.prdcd)
				WHERE loccd IS NULL OR loccd ='$loccd'";

		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Product is empty..!", 1);
			}
		}
		return $res;
	}
}
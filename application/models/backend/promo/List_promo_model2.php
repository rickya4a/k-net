<?php

class List_promo_model2 extends MY_Model{

    function __construct(){

        parent::__construct();
    }

    function get_prom($iddfno){

        $sql = "SELECT b.dfno, b.fullnm, b.nmSTK, a.VoucherNo, CONVERT(VARCHAR(10), b.bnsperiod , 103) as bnsperiod
                    FROM [dbo].[TWA_KLPromo_Oct17_H] b
                    left join TWA_KLPromo_Oct17_D a
                    on a.VoucherNo  = b.VoucherNo  COLLATE SQL_Latin1_General_CP1_CS_AS
                    WHERE b.dfno = '$iddfno'
                    GROUP BY b.dfno, b.fullnm, b.nmSTK, a.VoucherNo, b.bnsperiod";


        $result = $this->getRecordset($sql,null,$this->db2);
        //echo $this->db->last_query();
        return $result;
    }


    function get_detailprom2($iddfno){

        $sql = "SELECT a.prdcd, a.prdnm, a.qtyord, b.Claim_loccd, b.Claim_STK, CONVERT(VARCHAR(10), b.bnsperiod , 103) as bnsperiod
                FROM [dbo].[TWA_KLPromo_Oct17_H] b
                left join TWA_KLPromo_Oct17_D a
                on a.VoucherNo  = b.VoucherNo  COLLATE SQL_Latin1_General_CP1_CS_AS
                WHERE b.dfno = '$iddfno'";

        $result = $this->getRecordset($sql,null,$this->db2);
        return $result;
    }


}
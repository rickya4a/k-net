<?php

 class Bonus_AyuArtis_model extends MY_Model{

     public function __construct(){

         parent::__construct();
     }

     public function get_bonus($idmember, $from, $to){

         $sql = "SELECT tgl1, tgl2, fullnm, jml_rekrutan, NAMAREK, REKENING, BANK, NOMINAL, NOMINAL_TRF, tglFINANCE,
                    CASE
                        WHEN flagFINANCE = 'p' THEN 'transfer'
                        ELSE 'pending'
                    END as status
                    FROM [dbo].[TWA_CASHBACK_STK80K]
                    where dfno = '$idmember'
                    AND tgl1 >= '$from'
                    AND tgl1 <= DATEADD(day, 1, '$to')
                    ORDER BY tgl1";

         $result = $this->getRecordset($sql,null,$this->db2);
         //echo $this->db->last_query($sql);
         return $result;
     }

     public function get_detail($dfno, $date1, $date2){

         $sql = "SELECT b.dfno, b.fullnm, b.jointdt
                    FROM [dbo].[TWA_CASHBACK_STK80K_DET] b
                    LEFT JOIN [dbo].[TWA_CASHBACK_STK80K] a
                    on a.dfno = b.sfno_reg COLLATE SQL_Latin1_General_CP1_CS_AS and a.NOTRX = b.NOTRX
                    WHERE a.dfno = '$dfno'
                    AND b.jointdt >= '$date1'
                    AND b.jointdt <= DATEADD(day, 1, '$date2')";

         $result = $this->getRecordset($sql,null,$this->db2);
         return $result;
     }
 }
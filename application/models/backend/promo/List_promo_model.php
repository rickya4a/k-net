<?php

class List_promo_model extends MY_Model{

    function __construct(){

        parent::__construct();
    }

    function get_prom($iddfno){

        $sql = "SELECT b.dfno, b.fullnm, b.nmSTK, b.VoucherNo, CONVERT(VARCHAR(10), b.bnsperiod , 103) as bnsperiod, a.loccd, a.claim_date
                    FROM [dbo].[TWA_KLPromo_Oct17_H] b
                    left join tcvoucher a
                    on a.VoucherNo  = b.VoucherNo  COLLATE SQL_Latin1_General_CP1_CS_AS
                    WHERE b.dfno = '$iddfno'
                    GROUP BY b.dfno, b.fullnm, b.nmSTK, b.VoucherNo, b.bnsperiod, a.loccd, a.claim_date";


        $result = $this->getRecordset($sql,null,$this->db2);
        //echo $this->db->last_query();
        return $result;
    }

    function get_detailprom($iddfno, $bnsperiod){

        $sql = "SELECT a.prdcd, a.prdnm, a.qtyord
                FROM [dbo].[TWA_KLPromo_Oct17_H] b
                left join TWA_KLPromo_Oct17_D a
                on a.VoucherNo  = b.VoucherNo  COLLATE SQL_Latin1_General_CP1_CS_AS
                WHERE b.dfno = '$iddfno' and b.bnsperiod = '$bnsperiod'";

        $result = $this->getRecordset($sql,null,$this->db2);
        return $result;
    }

}
<?php

    class List_shipping_model extends MY_Model{

        public function __construct(){

            parent::__construct();

        }

        public function get_ship($no_trans){

            $sql = "SELECT a.conoteJNE,
                    CASE
                            WHEN a.cargo_id = 1 THEN 'JNE'
                            WHEN a.cargo_id = 2 THEN 'KGP'
                            ELSE 'belum terdaftar'
                    END as tes,
                    a.cargo_id, CONVERT(VARCHAR(10), b.datetrans, 111) as datetrans
                    FROM ecomm_trans_shipaddr a
                    LEFT JOIN ecomm_trans_hdr b
                    on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                    where a.orderno = '$no_trans'";
            //$result = $this->getRecordset($sql,null,$this->db2);
			//return $result;
            $res = $this->getRecordset($sql);
            return $res;
        }
    }
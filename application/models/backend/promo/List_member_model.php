<?php 

	class List_member_model extends MY_Model{

		function __construct(){

			parent::__construct();
		}

		function get_memb($idrekrut, $dt1, $dt2) {
		
			$sql = "SELECT dfno, fullnm, joindt, status_transfer, bonusrec, transfer_dt, tax_decrease, (bonusrec - tax_decrease) as total FROM msmemb_recruiter_bonus WHERE sfno_reg='$idrekrut' AND joindt BETWEEN  '$dt1' AND '$dt2' ";
				 
			$result = $this->getRecordset($sql,null,$this->db2);
			//echo $this->db->last_query();

			return $result;
		}

		function get_membname($idrekrut){

			$sql = "SELECT top 1 b.fullnm, b.bankaccno, b.banknm, a.sfno_reg, b.bankaccnm
				FROM [dbo].[msmemb_recruiter_bonus] a
                left join msmemb b
                on a.sfno_reg  = b.dfno  COLLATE SQL_Latin1_General_CP1_CS_AS
                where
                a.sfno_reg ='$idrekrut'";
			$result = $this->getRecordset($sql,null,$this->db2);
			return $result;
		}
	}


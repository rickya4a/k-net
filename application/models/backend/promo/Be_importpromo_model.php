<?php

    class Be_importpromo_model extends MY_Model{

        function __construct(){

            parent::__construct();

        }

        public function upload($filename){
            $this->load->library('upload');

            $config['upload_path'] = './assets/excel/';
            $config['allowed_types'] = 'xlsx';
            $config['max_size']	= '2048';
            $config['overwrite'] = true;
            $config['file_name'] = $filename;

            $this->upload->initialize($config);
            if($this->upload->do_upload('file')){
                $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                return $return;
            }else{
                $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                return $return;
            }
        }

        public function insert_multiple($data){
            $this->db->insert_batch('msmemb_recruiter_bonus', $data);
        }

        public function upload_file($data){

            $this->db2->insert('msmemb_recruiter_bonus', $data);

        }
    }
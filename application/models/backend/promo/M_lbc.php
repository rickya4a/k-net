<?php
class M_lbc extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
	
	function checkSalesLbc($idmember, $bnsperiod) {
		$qry = "SELECT
					XXY.prdcd, 
					SUM(XXY.qtyord) as qtyord,
					SUM(XXY.bv) as bv,
					SUM(XXY.qtyord) * SUM(XXY.bv) as total_bv
			    FROM (			
					  SELECT a.dfno, a.trcd, a.tdp, a.tbv,
					   CASE WHEN b.prdcd is null THEN c.prdcd
					   ELSE b.prdcd END AS prdcd,
					   CASE WHEN b.qtyord is null THEN c.qtyord
					   ELSE b.qtyord END AS qtyord,
					   CASE WHEN b.bv is null THEN c.bv
					   ELSE b.bv END AS bv
					 from newtrh a
					 LEFT OUTER JOIN newtrd b ON (a.trcd = b.trcd)
					 LEFT OUTER JOIN sc_newtrd c ON (a.trcd = c.trcd)
					 WHERE a.dfno = '$idmember' and a.bnsperiod = '$bnsperiod'
					 and (b.bv > 0 OR c.bv > 0)
				 ) AS XXY
				 GROUP BY XXY.prdcd";
		$res = $this->getRecordset($qry, null, $this->setDB(2));
		$arrPrd = array();
		$produkLBC = array();
		$bvLbcProduct = 0;
		$cnt = 0;
		//echo "<pre>";
		//print_r($res);
		//echo "</pre>";
		if($res != null) {
			foreach($res as $dta) {
				//CHECK PRODUK LBC, JIKA ADA APAKAH MINIMAL 200 BV
				//$listPrdLBC = "SELECT prdcd, prdnm, tbv FROM LBC_list_product";
				//$sx = array("prdcd" => $dta->prdcd, "qtyord" => $dta->qtyord);
				//$arr = array_push($arrPrd, $sx); 
				$listPrdLBC = "SELECT a.prdcd, a.prdnm, b.bv as tbv, a.tbv as bv_lbc 
								 FROM LBC_list_product a
								 LEFT OUTER JOIN pricetab b ON (a.prdcd COLLATE SQL_Latin1_General_CP1_CS_AS = b.prdcd)
								 WHERE b.pricecode = '12W3' AND a.prdcd = '".$dta->prdcd."'";
				
				$resListPrdLBC = $this->getRecordset($listPrdLBC, null, $this->setDB(2));
				//$resListPrdLBC = $qryListPrdLBC->result();
				//print_r($arr);
				if($resListPrdLBC != null) {
					//echo $listPrdLBC;
				    //echo "<br />";
					//echo $dta->prdcd. " - ".$dta->qtyord;
				    //echo "<br />";
					
					$produkLBC[$cnt]['prdcd'] = $resListPrdLBC[0]->prdcd;
					$produkLBC[$cnt]['prdnm'] = $resListPrdLBC[0]->prdnm;
				    $produkLBC[$cnt]['qtyord'] = $dta->qtyord;
					$produkLBC[$cnt]['bv'] = $resListPrdLBC[0]->tbv;
					$produkLBC[$cnt]['total_bv'] = $dta->qtyord * $resListPrdLBC[0]->tbv;
					$cnt++;
				}
					
					/*foreach($resListPrdLBC as $obj) {
					   $prd1 = trim($dta->prdcd); 
					   $prd2 = trim($obj->prdcd);
					   
					   if($prd1 == $prd2) {
						  //echo "Prdcd : ".$obj->prdcd."<br />";
						  //echo "Prdcd : ".$dta->qtyord."<br />";
						  //echo "BV : ".$obj->tbv."<br />";
						  $bvLbcProduct += $dta->total_bv;
						  //echo "Tot BV : ".$dta->total_bv."<br />";
						  $produkLBC[$cnt]['prdcd'] = $prd1;
						  $produkLBC[$cnt]['prdnm'] = $obj->prdnm;
						  $produkLBC[$cnt]['qtyord'] = $dta->qtyord;
						  $produkLBC[$cnt]['bv'] = $obj->tbv;
						  $produkLBC[$cnt]['total_bv'] = $dta->qtyord * $obj->tbv;
						  $cnt++;
					   } 
					}*/
			
			}
		}
		return $produkLBC;
	}
}
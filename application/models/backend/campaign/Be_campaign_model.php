<?php
class Be_campaign_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	/*------------------
	 * CAMPAIGN
	 --------------------*/
	 
     function getlistAllCampaign($noexception = true) {
     	$qry = "SELECT * FROM FT_header order by id";
		$res = $this->getRecordset($qry, NULL, $this->db7);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Campaign is empty..!", 1);
			}
		}
		return $res;
     }
	 
	function getDetCampaign($param, $value, $noexception = true) {
     	$qry = "SELECT * FROM FT_header WHERE $param = '$value' order by id";
		$res = $this->getRecordset($qry, NULL, $this->db7);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Campaign is empty..!", 1);
			}
		}
		return $res;
     }
	 
	 
	 function getListCampaign($param, $value) {
	 	$qry = "SELECT * FROM FT_header WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db7);
		if($res == null) {
			throw new Exception("No record available", 1);
		}
		//var_dump($res);
		return $res;
	 }
	 
	 function uploadCampaign() {
	 	$data = $this->input->post(NULL, TRUE);
		
		$dfno = $data['dfno'];
		$fullnm = $data['fullnm'];
		$pic_desc = $data['pic_desc'];
		$status = $data['status'];
		$userfile = $_FILES["userfile"]["name"]; //'NAMA FILE';//
		$username = $this->username;
		/*
		 * 
		 echo "Distributor Code = $dfno</br>";	
		echo "Distributor Name = $fullnm</br>";	
		echo "Picture Desc = $pic_desc</br>";	
		echo "Status = $status</br>";		
		echo "User File = $userfile</br>";	
		*/
		$output_dir = "assets/images/campaign/campaign_img/original/$userfile";
        $thumb_dir = "assets/images/campaign/campaign_img/thumbnail/$userfile";
		
		if($dfno != "" && $fullnm != "" && $pic_desc != "" && $userfile != "") {
			$qry = "INSERT INTO FT_header (createnm, ft_url, ft_url_thumb, ft_name, ft_desc, dfno, fullnm, status)
			        VALUES ('$username', '$output_dir', '$thumb_dir', '$userfile', '$pic_desc', '$dfno', '$fullnm', '$status')";
			//$result = $this->getRecordset($slc,null,$this->db1);
            $query = $this->executeQuery($qry, $this->setDB(7));
			if(!$query) {			
				throw new Exception("Upload Campaign failed..!!", 1);
			}
		} 
		return $query;
	 }

	 function updateCampaign() {
	 	$data = $this->input->post(NULL, TRUE);
		$ss = "";
		if(isset($_FILES["myfile"]["name"])) {
			$ss .= " img_name = '".$_FILES["myfile"]["name"]."' ";
		}
		$qry = "UPDATE master_prd_cat_inv SET cat_id = '$data[cat_id]', parent_cat_inv_id = '$data[parent_cat_inv_id]',
		             cat_inv_desc = '$data[cat_inv_desc]', inv_type = '$data[inv_type]', status = '$data[status]', bo_inv_status = '$data[bo_inv_status]',
		             bo_status = '$data[bo_status]', sc_status = '$data[sc_status]', sub_status = '$data[sub_status]', ms_status = '$data[ms_status]',
		             ecomm_status = '$data[ecomm_status]', web_status = '$data[web_status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]'
		             $ss
		        WHERE cat_inv_id = '$data[cat_inv_id]'";
		//echo $qry;
		$query = $this->db->query($qry);
		if(!$query) {			
			throw new Exception("Update Product failed..!!", 1);
		} 
		return $query;
	 }
	 
	 function deleteCampaign($id) {
	 	$qry = "DELETE FROM master_prd_cat_inv WHERE cat_inv_id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {			
			throw new Exception("Delete Product failed..!!", 1);
		} 
		return $query;
	 }
	 
}
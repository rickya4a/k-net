<?php
class Be_transaction_model extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();
		//$this->ppob_hdr = "ppob_trx_hdr1";

    }

	function checkHeaderDetail($value, $bnsperiod) {
		if ($value == "1") {
			//header dan detail tidak balance nilai uang nya
			$qry = "SELECT a.bnsperiod,a.trcd,a.trcd2,a.etdt as updatedt,a.dfno,a.tbv,a.tdp,
							a.pricecode, a.loccd,
							SUM(b.qtyord * b.dp) as xDP, SUM(b.qtyord * b.bv) as xbv
							FROM newtrh a LEFT OUTER JOIN QTW_NEWTRD4 b ON (a.trcd=b.trcd)
							WHERE a.bnsperiod='$bnsperiod' AND (left(a.trcd,2)='ID' OR left(a.trcd,2)='CV')
							GROUP BY a.bnsperiod,a.trcd,a.trcd2,a.etdt,a.dfno,a.tbv,a.tdp,
							a.pricecode, a.loccd
							HAVING (a.tdp<>sum(b.qtyord * b.dp) or a.tbv<>sum(b.qtyord * b.bv))
							ORDER BY a.bnsperiod,a.trcd";
		} elseif ($value == "2") {
			//header ada tapi detail null
			$qry = "SELECT a.bnsperiod,a.trcd,a.trcd2,a.updatedt,a.dfno,a.tbv,a.tdp,
							a.pricecode, a.loccd,
							SUM(b.qtyord * b.dp) as xDP, SUM(b.qtyord * b.bv) as xbv
							FROM newtrh a LEFT OUTER JOIN QTW_NEWTRD4 b ON (a.trcd=b.trcd)
							WHERE a.bnsperiod='$bnsperiod' AND (left(a.trcd,2)='ID' OR left(a.trcd,2)='CV')
							GROUP BY a.bnsperiod,a.trcd,a.trcd2,a.updatedt,a.dfno,a.tbv,a.tdp,
							a.pricecode, a.loccd
							HAVING (a.tdp IS NULL OR SUM(b.qtyord * b.dp) IS NULL  or a.tbv IS NULL OR SUM(b.qtyord * b.bv) IS NULL)
							ORDER BY a.bnsperiod,a.trcd";
		} elseif ($value == "3") {
			$qry = "SELECT a.bnsperiod,a.trcd,a.trcd2,a.updatedt,a.dfno,a.tbv,a.tdp,
							a.pricecode, a.loccd, a.createnm
							FROM newtrh a
							WHERE a.bnsperiod='$bnsperiod' AND
							(a.loccd IS NULL)";
		} elseif ($value == '6') {
			$qry = "SELECT xxx.*
							FROM (
								SELECT
									a.orderno,
									a.CNno,
									CONVERT(VARCHAR(10), A.datetrans, 111) as datetrans,
									a.total_pay,
									a.total_bv,
									a.sentTo,
									a.idstk,
									b.orderno as orderno_prd,
									SUM(b.qty * b.dpr) as jum_nilai_produk,
									SUM(b.qty * b.bvr) as jum_bv_produk
								FROM ecomm_trans_hdr a
								LEFT OUTER JOIN ecomm_trans_det_prd b ON (a.orderno = b.orderno)
								WHERE a.bonusmonth = '$bnsperiod'
								GROUP BY
								a.orderno,
								a.CNno,
								CONVERT(VARCHAR(10), A.datetrans, 111),
								a.total_pay,
								a.total_bv,
								a.sentTo,
								a.idstk,
								b.orderno
							) xxx
							WHERE
							xxx.total_pay <> xxx.jum_nilai_produk
							OR xxx.orderno_prd IS NULL
							ORDER BY xxx.orderno";
		} elseif ($value == "7") {
			$qry = "SELECT a.orderno, a.token, CONVERT(VARCHAR(10), a.datetrans, 111) as datetrans, a.total_pay,
								a1.idno, a1.membername,
								COUNT(b.trx_no) as jum_record_memb
							FROM ecomm_trans_hdr a
							LEFT OUTER JOIN ecomm_memb_ok_sgo a1 ON (a.token = a1.trx_no)
							LEFT OUTER JOIN ecomm_memb_ok b ON (a.orderno = b.trx_no)
							WHERE a.bonusmonth = '$bnsperiod'
							GROUP BY a.orderno, a.token, CONVERT(VARCHAR(10), a.datetrans, 111), a.total_pay,
								a1.idno, a1.membername
							HAVING COUNT(b.trx_no) > 1";
		} elseif ($value == "8") {
			$qry = "SELECT a.trcd, b.dfno, b.totpay, b.tbv, 'sc_newtrd' as tbl, COUNT(a.trcd) as jum
							FROM sc_newtrd a
							INNER JOIN sc_newtrh b ON (a.trcd = b.trcd)
							WHERE b.bnsperiod = '$bnsperiod' and (a.qtyord = 0 or a.qtyord is null)
							GROUP BY a.trcd, b.dfno, b.totpay, b.tbv
							UNION
							SELECT a.trcd, b.dfno, b.totpay, b.tbv, 'newtrd' as tbl, COUNT(a.trcd) as jum
							FROM newtrd a
							INNER JOIN newtrh b ON (a.trcd = b.trcd)
							WHERE b.bnsperiod = '$bnsperiod' and (a.qtyord = 0 or a.qtyord is null)
							GROUP BY a.trcd, b.dfno, b.totpay, b.tbv";
		} elseif ($value == "9") {
			$qry = "SELECT a.trcd, a.dfno, a.bnsperiod, a.tdp, a.totpay, a.ndp, a.nbv, a.npv, a.tbv, a.tpv
							FROM newtrh a
							WHERE a.bnsperiod = '$bnsperiod'
							AND (a.tdp <> a.totpay OR a.tdp <> a.ndp OR a.totpay <> a.ndp
							OR a.nbv <> a.npv OR a.npv <> a.tbv OR a.tbv <> a.tpv
							OR a.nbv <> a.tpv)
							UNION
							SELECT a.trcd, a.dfno, a.bnsperiod, a.tdp, a.totpay, a.ndp, a.nbv, a.npv, a.tbv, a.tpv
							FROM sc_newtrh a
							WHERE a.bnsperiod = '$bnsperiod'
							AND (a.tdp <> a.totpay OR a.tdp <> a.ndp OR a.totpay <> a.ndp
							OR a.nbv <> a.npv OR a.npv <> a.tbv OR a.tbv <> a.tpv
							OR a.nbv <> a.tpv)";
		} else {
			$qry = "SELECT a.bnsperiod,a.trcd,a.trcd2,a.updatedt,a.dfno,a.tbv,a.tdp,
							a.pricecode, a.loccd, a.createnm, a.orderno
							FROM sc_newtrh a
							WHERE a.bnsperiod='$bnsperiod' AND
							(a.loccd != a.createnm) AND a.createnm NOT IN ('PUTRI','NUR','VIRA','MELLY','YANTI','DESI','SARI','LILIK','EVI','ECOMMERCE')";
		}
		//echo $qry;
		if ($value == "6" || $value == "7") {
			$res = $this->getRecordset($qry, null, "db_ecommerce");
		} else {
			$res = $this->getRecordset($qry, null, "klink_mlm2010");
		}
		return $res;
	}

	function updateEcommTransDetPrd($data) {
		$db_qryx = $this->load->database('db_ecommerce', true);

		$db_qryx->trans_begin();


		$qryDetPrd = "DELETE FROM ecomm_trans_det_prd where orderno='$data[orderno]'";
		//echo $qryDetPrd;
		//echo "<br />";
		$db_qryx->query($qryDetPrd);

		$qryDetWebol = "DELETE FROM klink_mlm2010.dbo.webol_det_prod where orderno='$data[orderno]'";
		//echo $qryDetWebol;
		//echo "<br />";
		$db_qryx->query($qryDetWebol);


		$qryDetSctrd = "DELETE FROM klink_mlm2010.dbo.sc_newtrd where trcd='$data[orderno]'";
		//echo $qryDetSctrd;
		//echo "<br />";
		$db_qryx->query($qryDetSctrd);


		/*
		$jum = count($data['prdcd']);

		for($i=0; $i < $jum; $i++) {
			$ins_scnewtrd = "INSERT INTO klink_mlm2010.dbo.sc_newtrd (trcd, prdcd, uom, qtyord, qtyship, qtyremain,
								dp, pv, bv, taxrate, sp, sb, sentby, filenm,
								taxcd, scdisc, seqno, scdiscamt, syn2web, qty_used,
								qty_avail, filename, PT_SVRID, promocd, pricecode, indexfree)
							VALUES ('".$data['orderno']."', '".$data['prdcd'][$i]."', NULL, ".$data['qty'][$i].",
								".$data['qty'][$i].", ".$data['qty'][$i].", ".$data['dpr'][$i].", ".$data['bvr'][$i].", ".$data['bvr'][$i].",
								'0', '0', '0', NULL, NULL,
								NULL, '0', '0', '0', '0', '0',
								'0', NULL, 'ID', '', '".$data['pricecode']."', '0')";
		    $db_qryx->query($ins_scnewtrd);
			//'".$data['prdcd'][$i]."','".$data['prdnm'][$i]."', ".$data['qty'][$i].", , ".$data['dpr'][$i].", , '".$data['sentToStt']."'
		}	*/

		$qryDetOrdivtrd = "DELETE FROM klink_mlm2010.dbo.ordivtrd  where invoiceno='$data[CNno]'";
		//echo $qryDetOrdivtrd;
		//echo "<br />";
		$db_qryx->query($qryDetOrdivtrd);

		$qryDetNewivtrd = "DELETE FROM klink_mlm2010.dbo.newivtrd WHERE trcd = '$data[CNno]'";
		//echo $qryDetNewivtrd;
		//echo "<br />";
		$db_qryx->query($qryDetNewivtrd);

		$qryBillivPrd = "DELETE FROM klink_mlm2010.dbo.billivprd where trcd='$data[KWno]'";
		//echo $qryBillivPrd;
		//echo "<br />";
		$db_qryx->query($qryBillivPrd);

		$jum = count($data['prdcd']);

		for($i=0; $i < $jum; $i++) {
			$insEcommPrd = "INSERT INTO ecomm_trans_det_prd (orderno, prdcd, prdnm, qty, bvr, dpr, pricecode, sentTo)
							VALUES ('".$data['orderno']."','".$data['prdcd'][$i]."','".$data['prdnm'][$i]."', ".$data['qty'][$i].", ".$data['bvr'][$i].", ".$data['dpr'][$i].", '".$data['pricecode']."', '".$data['sentToStt']."')";
			//echo $insEcommPrd;
			//echo "<br />";
			$db_qryx->query($insEcommPrd);
		}

		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			$return = jsonFalseResponse("Update data produk gagal..");
		} else {
			$db_qryx->trans_commit();
			$return = jsonTrueResponse(null, "Update Data produ berhasil");

		}


		return $return;
	}

	function vaBalanceRekap() {
		$qry = "SELECT a.dfno, d.fullnm,
					top_up = (
					 SELECT ISNULL (SUM(c.amount), 0)
					 FROM va_cust_pay_det c
					 WHERE effect = '+' AND c.dfno = a.dfno
					),
					terpakai = (
					 SELECT ISNULL (SUM(amount), 0)
					 FROM va_cust_pay_det  c
					 WHERE effect = '-' AND c.dfno = a.dfno
					),
					balance = (
					 SELECT b.amount
					 FROM va_cust_pay_bal B
					 WHERE B.dfno = a.dfno
					)
				FROM va_cust_pay_bal a
				INNER JOIN klink_mlm2010.dbo.msmemb d ON (a.dfno = d.dfno)
				WHERE a.dfno NOT IN ('IDSPAAA66834', 'ID9999A00251')
				GROUP BY a.dfno, d.fullnm
				ORDER BY a.dfno";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function rekapTrxmember($idmember) {
		$qry = "SELECT * FROM V_HISTORY_VA A
				WHERE A.dfno = '$idmember'
				ORDER BY A.createdt";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function rekapHistoryTrx($data) {
		$dtfrom = $data['from'];
		$dtto = $data['to'];

		$add = "";
		if($data['tipe_dk'] != "") {
			$add .= " AND tipe_dk = '$data[tipe_dk]'";

		}

		if($data['trxtype'] != "") {
			$add .= " AND trtype = '$data[trxtype]'";
		}

		$qry = "SELECT * FROM V_HISTORY_VA A
				WHERE a.createdt1 between '$dtfrom' and '$dtto' $add
				ORDER BY A.createdt";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getVaHistoryTrx($data) {
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));

		$str = "";
		if(array_key_exists("memberid", $data)) {
			$str .= "a.dfno = '$data[memberid]' ";
		}

		if($data['tipe_dk'] != "") {
			$str .= "AND a.tipe_dk = '$data[tipe_dk]' ";
		}

		//$str = "a.ppob_type <> '1' AND a.ppob_type <> '2' AND";
		if($data['trx_type'] != "all") {
			$str .= "AND a.trtype = '$data[trx_type]' ";
		}

		/*$qry = "SELECT a.id, a.trcd,
				       a.amount,
				       a.tipe_dk,
				       a.trtype,
				       a.trcd,
				       CASE
        WHEN a.trtype = '01' THEN b.shortdescription + ' ' +a.bankDesc COLLATE SQL_Latin1_General_CP1_CS_AS
        ELSE b.shortdescription END AS shortdescription,
				       CONVERT(VARCHAR(20), a.createdt , 120) as createdt
				FROM va_cust_pay_det a
				     inner join va_trx_type b ON (a.trtype = b.trxtype)
				WHERE $str AND
				  CONVERT(VARCHAR(10), A.createdt, 111) between '$dtfrom' and '$dtto'
				ORDER BY CONVERT(VARCHAR(20), a.createdt , 120)"; */
//		echo $qry;
        $qry = "SELECT a.id, a.trcd,
				       a.amount,
				       a.tipe_dk,
				       a.trtype,
				       a.trcd,
				CASE
					WHEN a.trtype = 'PX1' OR a.trtype = 'TRM' OR a.trtype = 'TRS' THEN z.token ELSE NULL
				END AS token,
				CASE
					WHEN a.trtype = 'PX1' OR a.trtype = 'TRM' OR a.trtype = 'TRS' THEN z.CNno ELSE NULL
				END AS CNno,
				CASE
				  WHEN a.trtype = '01'
				    THEN b.shortdescription + ' ' +a.bankDesc COLLATE SQL_Latin1_General_CP1_CS_AS
				  WHEN a.trtype = 'PX2'
				    THEN d.[description]
				  WHEN a.trtype = 'PB1' and c.ppob_type = '1'
				    THEN c.trx_type
				  WHEN a.trtype = 'PB1' and c.ppob_type = '3'
				    THEN e.product_name
				  WHEN a.trtype = 'PB1' and c.ppob_type = '4'
				    THEN e.product_name
				  WHEN a.trtype = 'PX1' OR a.trtype = 'TRM' OR a.trtype = 'TRS'
				    THEN b.shortdescription
				  WHEN a.trtype = 'SR' THEN b.shortdescription + ' - ' + a.refno
				ELSE b.shortdescription END AS shortdescription,
				CONVERT(VARCHAR(20), a.createdt , 120) as createdt
				FROM va_cust_pay_det a
				     INNER JOIN va_trx_type b ON (a.trtype = b.trxtype)
					 LEFT OUTER JOIN ecomm_trans_hdr z ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = z.orderno)
				     LEFT OUTER JOIN $this->tbl_hdr c ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = c.trx_id)
				     LEFT OUTER JOIN ppob_xl_master d ON (c.trx_type COLLATE SQL_Latin1_General_CP1_CI_AS = d.kode_paket)
				     LEFT OUTER JOIN ppob_sgo_trxtype e ON (c.trx_type COLLATE SQL_Latin1_General_CP1_CI_AS = e.product_code)
				WHERE $str AND
				  CONVERT(VARCHAR(10), A.createdt, 111) between '$dtfrom' and '$dtto'
				ORDER BY CONVERT(VARCHAR(20), a.createdt , 120)";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListTransactionToPost($data) {
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));
		$qry = "SELECT * FROM V_ECOMM_SEARCH_CN A
				WHERE CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto' and KWstatus='0'";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListTopUpVaReport($data) {
		$tambahan = " ";
		$dtfrom = date("Y/m/d", strtotime($data['trx_from']));
		$dtto = date("Y/m/d", strtotime($data['trx_to']));

		if($data['trx_from'] != "" || $data['trx_to'] != "") {
			$where = " CONVERT(VARCHAR(10), a.createdt, 111) between '$dtfrom' and '$dtto' ";
		}

		if($data['searchBy'] != "") {
			$tambahan = " AND a.dfno = '$data[paramValue]'";
		}

		$qry = "SELECT
					 a.trcd, a.dfno, b.fullnm, a.trtype,
					 CONVERT(VARCHAR(18), a.createdt, 103) as createdt,
					 a.amount, c.total_amount, c.total_amount - a.amount as potongan,
					  a.tipe_dk, a.novac, bankDesc, c.credit_to_bank, c.credit_to, c.credit_to_name
				FROM va_cust_pay_det_acc a
				LEFT OUTER JOIN va_sgo_paynotif_req c ON (c.payment_ref COLLATE SQL_Latin1_General_CP1_CS_AS = a.trcd)
				INNER JOIN klink_mlm2010.dbo.msmemb b ON (a.dfno = b.dfno)
				WHERE a.dfno != 'IDSPAAA66834' AND trtype = '01' AND $where
				$tambahan
				ORDER BY a.createdt, a.dfno";
		 //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function listTransaksiDenganDataTidakLengkap() {
		$qry = "SELECT a.orderno as token, a.total_pay, a.total_bv,
				   a.sentTo, a.idstk,
				   a.datetrans, a.userlogin, b.fullnm as pemilik_hp_konfirmasi,
				   b.tel_hp as no_hp_konfirmasi,
				   CONVERT(VARCHAR(10), a.datetrans , 126) as datetrans,
				   d.bankDisplayNm
				FROM ecomm_trans_hdr_sgo a
				INNER JOIN klink_mlm2010.dbo.msmemb b ON (a.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS = b.dfno)
				INNER JOIN REPORT_DOWNLOAD_SGO c ON (a.orderno = c.Order_ID)
				INNER JOIN ecomm_bank d ON (a.bank_code_payment = d.id)
				LEFT OUTER JOIN ecomm_trans_hdr z ON (a.orderno = z.token)
				WHERE (a.id_memb is null OR a.id_memb = '' OR a.idstk is null OR a.idstk = ''
				OR a.sentTo is null OR a.sentTo = '' OR a.bonusmonth = '' OR a.bonusmonth is null OR a.bonusmonth = '/' OR a.bonusmonth = '01/1970')
				AND z.token is null AND YEAR(a.datetrans) >= 2019";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function listTransaksiMampirkakDenganRegMemberGagal() {
		$qry = "SELECT a.orderno, a.token, CONVERT(VARCHAR(10), a.datetrans, 111) as datetrans,
				 a.id_memb, a.nmmember, a.no_hp_konfirmasi,
				a.total_pay, a.total_bv,
				(
				  SELECT TOP 1 b.fullnm
				  FROM klink_mlm2010.dbo.msmemb b
				  WHERE b.tel_hp = a.no_hp_konfirmasi COLLATE SQL_Latin1_General_CP1_CS_AS
				) as pemilik_hp_konfirmasi
				FROM ecomm_trans_hdr a
				WHERE a.id_memb NOT LIKE 'EID%'
				AND a.id_memb NOT LIKE 'ID%'
				AND a.id_memb NOT LIKE 'ETW%'
				AND a.id_memb NOT LIKE 'EHK%'
				AND a.id_memb NOT LIKE 'HKL%'
				AND a.id_memb NOT LIKE 'EMY%'
				AND a.id_memb NOT LIKE 'MY%'
				AND a.id_memb != '0000999'
				AND a.token LIKE 'EP%'
				AND a.datetrans >= '2018-05-01'";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function checkDoubleTransaction($data) {
		$where = "";
		if($data['trx_from'] != "" || $data['trx_to'] != "") {
			$dtfrom = date("Y/m/d", strtotime($data['trx_from']));
			$dtto = date("Y/m/d", strtotime($data['trx_to']));
			$where .= "WHERE CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'";
		}
		/*
		$bnsmonth = null;
		if($data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$bnsmonth = " AND bonusmonth = '$bns' ";
		}
		 */

			$qry = "SELECT b.orderno, CONVERT(VARCHAR(20), b.datetrans , 120) as datetrans,
					  b.bonusmonth, b.token, b.total_pay, b.total_bv,
					  b.id_memb, b.nmmember, b.sentTo, b.nmstkk,
					  CASE
					   WHEN LTRIM(RTRIM(b.sentTo)) = '1' THEN b.nmstkk + ' (' + ISNULL(CONVERT(VARCHAR(20), d.tgl_ambil , 120), '') + ')'
					   WHEN LTRIM(RTRIM(b.sentTo)) = '2' THEN c.conoteJNE + ' - ' + c.receiver_name + ' ( ' + c.tel_hp1 + ' )'
					   ELSE 'xxxx'
					  END AS sent

					FROM ecomm_trans_hdr b
					LEFT OUTER JOIN ecomm_trans_shipaddr c ON (b.orderno = c.orderno)
					LEFT OUTER JOIN klink_mlm2010.dbo.webol_logs_stockist d ON (b.orderno = d.orderno)
					WHERE b.token IN (
						SELECT a.token
						FROM ecomm_trans_hdr a
						$where
						GROUP BY a.token
						HAVING COUNT(a.token) > 1
					)
					ORDER BY b.token";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListTrxUsingVoucher($data) {
		$where = "";
		if($data['trx_from'] != "" || $data['trx_to'] != "") {
			$dtfrom = date("Y/m/d", strtotime($data['trx_from']));
		    $dtto = date("Y/m/d", strtotime($data['trx_to']));
			$where = "WHERE CONVERT(VARCHAR(10), XX.datetrans, 111) between '$dtfrom' and '$dtto'";
		}


		//$qryxRes = $this->load->database($this->setDB(1), true);
		$qry = "SELECT
				  XX.CNno,
				  XX.datetrans,
				  XX.orderno,
				  XX.total_pay,
				  XX.payAdm,
				  XX.ship_cost,
				  XX.paytype,
				  XX.docno,
				  XX.payamt
				FROM (
					SELECT a.CNno,
					CONVERT(VARCHAR(10), a.datetrans, 111) as datetrans,
					a.orderno,
					a.total_pay,
					a.payAdm,
					CASE
					 WHEN (a.payShip - a.discount_shipping) = 1 THEN 0
					 ELSE a.payShip - a.discount_shipping
					 END AS ship_cost,
					 b.paytype,
					 b.docno,
					 b.payamt
					FROM ecomm_trans_hdr a
					INNER JOIN ecomm_trans_paydet b ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = b.orderno)
					WHERE a.orderno IN (
						SELECT  DISTINCT(c.orderno) COLLATE SQL_Latin1_General_CP1_CS_AS
						FROM ecomm_trans_paydet c
						WHERE c.paytype = 'vch'
					)
				) XX
				$where
				ORDER BY XX.CNno";
		$res = $this->getRecordset($qry);
		return $res;


		/*$query = $qryxRes->query($qry, null);

		if($query->num_rows() > 0)  {
			$header = 0;
			$tempHeader = "";
			$arr = array();
			foreach($query->result_array() as $dta) {
				if($tempHeader != $dta['CNno']) {
						$tempHeader = $dta['CNno'];
						$detail = 0;
						$arr[$header]['CNno'] = $dta['CNno'];
						$arr[$header]['datetrans'] = $dta['datetrans'];
						$arr[$header]['orderno'] = $dta['orderno'];
						$arr[$header]['total_pay'] = $dta['total_pay'];
						$arr[$header]['payAdm'] = $dta['payAdm'];
						$arr[$header]['ship_cost'] = $dta['ship_cost'];
						$arr[$header]['listPayment'][$detail]['paytype'] = $dta['paytype'];
						$arr[$header]['listPayment'][$detail]['docno'] = $dta['docno'];
						$arr[$header]['listPayment'][$detail]['payamt'] = $dta['payamt'];


						$header++;
					} else {
						 $jHead = $header-1;
						 $jDet = $detail+1;
						 //$arr[$jHead]['listTrf'][$jDet]['dfno'] = $dta['dfno'];
						 $arr[$jHead]['listPayment'][$jDet]['paytype'] = $dta['paytype'];
						 $arr[$jHead]['listPayment'][$jDet]['docno'] = $dta['docno'];
						 $arr[$jHead]['listPayment'][$jDet]['payamt'] = $dta['payamt'];

						 $detail++;
					}
			}
		} else {
			$arr = null;
		}
		return $arr;
		*/
	}


	function getListTransactionReport($data) {
		//print_r($data);
		$tambahan = " ";
		$tambahan2 = " ";
		$sentTo = " ";
		//$bnsmonth = " ";
		$dtfrom = date("Y/m/d", strtotime($data['trx_from']));
		$dtto = date("Y/m/d", strtotime($data['trx_to']));

		/*
		if(($data['trx_from'] != "" || $data['trx_to'] != "") && $data['bnsmonth'] == "") {
			$where = " CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'";
		} else if(($data['trx_from'] != "" || $data['trx_to'] != "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			//$where = "bonusmonth = '$bns' ";
			$where = " CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto' AND bonusmonth = '$bns'";
		} else if(($data['trx_from'] == "" || $data['trx_to'] == "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];

			$where = "bonusmonth = '$bns'";
		}*/

		if($data['trx_from'] != "" || $data['trx_to'] != "") {
			$where = " CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'";
		}

		if($data['searchBy'] != "") {
			$tambahan = " AND a.$data[searchBy] = '$data[paramValue]'";
		}

		if($data['trx_type'] != "") {
			$tambahan2 = " AND A.orderno LIKE '$data[trx_type]%'";
		}

		if($data['pay_type'] != "") {
			//$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$tambahan2 .= " AND bank_code_payment = ".$data['pay_type']."";
		}

		if($data['sentTo'] != "") {
			$sentTo = " AND sentTo = '$data[sentTo]'";
		}

		$bnsmonth = null;
		if($data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$bnsmonth = " AND bonusmonth = '$bns' ";
		}


		/*$qry = "SELECT A.idstk, A.datetrans1, A.CNno, A.KWno, A.token, A.orderno,
					A.nmmember, A.total_pay, A.total_bv ,A.CN_STAT,A.BV_STAT,A.sumofPrd,
					A.sumofPay, A.generate_idmemb,
					B.dest_address, D.whcd, E.fullnm as wh_name, A.conoteJNE
				FROM V_ECOMM_SEARCH_CN A
				LEFT OUTER JOIN ecomm_trans_shipaddr_sgo B ON (A.token = b.orderno)
				LEFT OUTER JOIN jne_Tabel_Zona_Induk D ON (B.dest_address = D.City_Code_Dest)
				LEFT OUTER JOIN klink_mlm2010.dbo.mssc E ON (D.whcd COLLATE SQL_Latin1_General_CP1_CI_AS = E.loccd)
				WHERE $where
				$tambahan $tambahan2 $sentTo
				ORDER BY datetrans, a.orderno";*/
        //echo $qry;
		$qry = "SELECT DISTINCT knet.* FROM (
					SELECT A.idstk,
					  A.bonusmonth, A.datetrans1, A.CNno, A.KWno, A.token, A.orderno,
					  A.nmmember, A.total_pay, A.total_bv ,A.CN_STAT,A.BV_STAT,A.sumofPrd,
					  A.sumofPay, A.generate_idmemb,
					  B.dest_address,D.whcd, E.fullnm as wh_name, A.conoteJNE, A.SHIP_STAT
					FROM V_ECOMM_SEARCH_CN A
					LEFT OUTER JOIN ecomm_trans_shipaddr_sgo B ON (A.token = b.orderno)
					LEFT OUTER JOIN jne_Tabel_Zona_Induk D ON (B.dest_address = D.City_Code_Dest)
					LEFT OUTER JOIN klink_mlm2010.dbo.mssc E ON (D.whcd COLLATE SQL_Latin1_General_CP1_CI_AS = E.loccd)
					WHERE  $where
				$tambahan $tambahan2 $sentTo $bnsmonth
				) as knet
				ORDER BY
				 knet.datetrans1, knet.orderno";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListTrxById($param, $value) {
		$data = $this->input->post(NULL, TRUE);

		if($param == "orderno" || $param == "CNno" || $param == "KWno" || $param == "token" || $param == "conoteJNE") {
		   $xparam = "a.".$param;
		}

		$bnsmonth = null;
		if($data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$bnsmonth = " AND bonusmonth = '$bns' ";
		}
			$qry = "SELECT DISTINCT knet.* FROM (
						SELECT A.idstk,
						  A.bonusmonth, A.datetrans1, A.CNno, A.KWno, A.token, A.orderno,
						  A.nmmember, A.total_pay, A.total_bv ,A.CN_STAT,A.BV_STAT,A.sumofPrd,
						  A.sumofPay, A.generate_idmemb,
						  B.dest_address,D.whcd, E.fullnm as wh_name, A.conoteJNE
						FROM V_ECOMM_SEARCH_CN A
						LEFT OUTER JOIN ecomm_trans_shipaddr_sgo B ON (A.token = b.orderno)
						LEFT OUTER JOIN jne_Tabel_Zona_Induk D ON (B.dest_address = D.City_Code_Dest)
						LEFT OUTER JOIN klink_mlm2010.dbo.mssc E ON (D.whcd COLLATE SQL_Latin1_General_CP1_CI_AS = E.loccd)
						WHERE $xparam = '$value' $bnsmonth
					) as knet
					ORDER BY
					 knet.datetrans1, knet.orderno";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getProductSummary($data) {
		$tambahan = " ";
		//$tambahan2 = " ";
		$sentTo = " ";
		$bnsmonth = " ";
		$dtfrom = date("Y/m/d", strtotime($data['trx_from']));
		$dtto = date("Y/m/d", strtotime($data['trx_to']));
		//$data['bnsmonth'] != ""
		if($data['trx_from'] != "" || $data['trx_to'] != "") {
			$where = " CONVERT(VARCHAR(10), b.datetrans, 111) between '$dtfrom' and '$dtto' ";
		}
		/*else if(($data['trx_from'] != "" || $data['trx_to'] != "") && ) {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			//$where = "bonusmonth = '$bns' ";
			$where = " CONVERT(VARCHAR(10), b.datetrans, 111) between '$dtfrom' and '$dtto' AND bonusmonth = '$bns'";
		} else if(($data['trx_from'] == "" || $data['trx_to'] == "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];

			$where = "b.bonusmonth = '$bns'";
		}*/

		if($data['sentTo'] != "") {
			$sentTo = " AND b.sentTo = '$data[sentTo]'";
		}

		/*if($data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$bnsmonth = " AND b.bonusmonth = '$bns'";
		}*/

		$qry = "SELECT a.prdcd, a.prdnm, SUM(a.qty) as total_qty
				FROM ecomm_trans_det_prd a
				INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
				WHERE a.prdnm NOT LIKE 'DTC%' and $where $tambahan  $sentTo
				GROUP BY a.prdcd, a.prdnm
				ORDER BY SUM(a.qty) DESC";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function rekapProductByWarehouse($from, $to, $wh) {

		if($from != "" || $to != "")  {
			$dtfrom = date("Y/m/d", strtotime($from));
			$dtto = date("Y/m/d", strtotime($to));
			$where = " CONVERT(VARCHAR(10), b.datetrans, 111) between '$dtfrom' and '$dtto' ";
		}
		$qry = "SELECT
					a.prdcd,
					a.prdnm,
					SUM(a.qty) as total_qty
				FROM ecomm_trans_det_prd a
				INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
				WHERE a.prdnm NOT LIKE 'DTC%' and
					$where
					and b.idstk IN (
						SELECT DISTINCT(loccd) as loccd
						FROM jne_Tabel_Zona_Induk z WHERE z.whcd = '$wh'
						AND z.loccd != 'NO STK'
					)
				GROUP BY a.prdcd, a.prdnm
				ORDER BY SUM(a.qty) DESC";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListTrxToDelete($data) {
		/*$tambahan = " ";
		if($data['searchBy'] != "") {
			$tambahan = " AND $data[searchBy] = '$data[paramValue]' and KWstatus='0'";
		}
		$qry = "SELECT idstk, datetrans1, token, orderno, nmmember, total_pay, total_bv
				FROM V_ECOMM_SEARCH_CN A
				WHERE A.datetrans between '$data[from]' and '$data[to]'
				$tambahan
				ORDER BY datetrans, orderno";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;	*/

		$tambahan = " ";
		$tambahan2 = " ";
		$sentTo = " ";
		$bnsmonth = " ";
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));
		if(($data['from'] != "" || $data['to'] != "") && $data['bnsmonth'] == "") {
			$where = "CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto' ";
		} else if(($data['from'] != "" || $data['to'] != "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			//$where = "bonusmonth = '$bns' ";
			$where = "CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto' AND bonusmonth = '$bns'";
		} else if(($data['from'] == "" || $data['to'] == "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];

			$where = "bonusmonth = '$bns'";
		}

		if($data['searchBy'] != "") {
			$tambahan = " AND $data[searchBy] = '$data[paramValue]'";
		}

		if($data['trx_type'] != "") {
			$tambahan2 = " AND orderno LIKE '$data[trx_type]%'";
		}

		if($data['sentTo'] != "") {
			$sentTo = " AND sentTo = '$data[sentTo]'";
		}

		if($data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$bnsmonth = " AND bonusmonth = '$bns'";
		}
		$qry = "SELECT idstk, datetrans1, token, orderno, nmmember, total_pay, total_bv
				FROM V_ECOMM_SEARCH_CN A
				WHERE $where
				$tambahan $tambahan2 $sentTo
				ORDER BY datetrans, orderno";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListTransactionReportToSMSResend($data) {
		$tambahan = " ";
		$tglx = " ";
		$dtfrom = date("Y/m/d", strtotime($data['sms_from']));
		$dtto = date("Y/m/d", strtotime($data['sms_to']));
		if($data['searchBy'] != "") {
			$tambahan = " AND a.$data[searchBy] = '$data[paramValue]'";
		}

		if($data['sms_from'] != "" || $data['sms_from'] != "sms_to") {

		}

		/*$qry = "SELECT a.idstk, a.datetrans1, a.orderno, a.nmmember, a.total_pay, a.total_bv, b.tel_hp
				FROM V_ECOMM_SEARCH_CN A
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb b on a.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS=b.dfno
				WHERE (CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto') AND b.tel_hp is not null
				$tambahan
				ORDER BY datetrans, orderno"; */

		$qry = "SELECT a.idstk, a.datetrans1, a.orderno, a.nmmember, a.total_pay, a.total_bv,CASE
				   WHEN b.tel_hp is null THEN c.tel_hp
				   ELSE b.tel_hp
				  END AS tel_hp,
				  a.no_hp_konfirmasi, a.token
				FROM V_ECOMM_SEARCH_CN A
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb b on a.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS=b.dfno
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb c on a.id_memb COLLATE SQL_Latin1_General_CP1_CS_AS=c.dfno
				WHERE (CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto')
				$tambahan
				ORDER BY datetrans, orderno";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListCN($data, $status) {

		$dtfrom = date("Y/m/d", strtotime($data['cn_from']));
		$dtto = date("Y/m/d", strtotime($data['cn_to']));
		$qry = "SELECT CNno,
                    KWno,
					total_pay,
					total_bv,
					idstk,
					nmstkk,
					id_memb, nmmember
		        FROM V_ECOMM_SEARCH_CN A
				WHERE CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'
				AND CNno is not null AND CNPrintStatus = '$status'";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListKW($data, $status) {
		$dtfrom = date("Y/m/d", strtotime($data['kw_from']));
		$dtto = date("Y/m/d", strtotime($data['kw_to']));
		$qry = "SELECT KWno,
					total_pay,
					total_bv,
					idstk,
					nmstkk,
					id_memb, nmmember
		        FROM V_ECOMM_SEARCH_CN A
				WHERE CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'
				AND KWno is not null AND KWPrintStatus = '$status'";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function updateLoccdTrx($loccd, $orderno) {

		$db_qryx = $this->load->database('db_ecommerce', true);
		$qry = "SELECT loccd, fullnm, pricecode FROM klink_mlm2010.dbo.mssc
		        WHERE loccd = '$loccd'";
		//echo $qry;
		$res = $db_qryx->query($qry);
		$resx = $res->result();
		if($resx != null) {

			$db_qryx->trans_begin();

			$set = "SELECT a.orderno, a.REGISTERno, a.CNno, a.KWno, a.IPno, b.kab_code
			        FROM db_ecommerce.dbo.ecomm_trans_hdr a
			        LEFT JOIN db_ecommerce.dbo.ecomm_trans_shipaddr b ON (a.orderno = b.orderno)
			        WHERE a.orderno = '".$orderno."'";
			$rest = $db_qryx->query($set);
			$rest = $rest->result();

			$qryS = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr_sgo SET idstk = '".$resx[0]->loccd."',
			           nmstkk = '".$resx[0]->loccd." - ".$resx[0]->fullnm."', pricecode = '".$resx[0]->pricecode."'
			         WHERE orderno = '".$orderno."'";
			//echo $qry1."<br />";
			$resS = $db_qryx->query($qryS);

			$qry1 = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr SET idstk = '".$resx[0]->loccd."',
			           nmstkk = '".$resx[0]->loccd." - ".$resx[0]->fullnm."', pricecode = '".$resx[0]->pricecode."'
			         WHERE orderno = '".$orderno."'";
			//echo $qry1."<br />";
			$res = $db_qryx->query($qry1);

			$qry2 = "UPDATE db_ecommerce.dbo.ecomm_trans_shipaddr SET idstk = '".$resx[0]->loccd."'
			         WHERE orderno = '".$orderno."'";
			//echo $qry2."<br />";
			$res2 = $db_qryx->query($qry2);

			$qry3 = "UPDATE klink_mlm2010.dbo.newtrh SET sc_dfno = '".$resx[0]->loccd."', sc_co = '".$resx[0]->loccd."', loccd = '".$resx[0]->loccd."', pricecode = '".$resx[0]->pricecode."'
			         WHERE orderno = '".$orderno."'";
			//echo $qry3."<br />";
			$res3 = $db_qryx->query($qry3);

			$qry4 = "UPDATE klink_mlm2010.dbo.sc_newtrh SET sc_dfno = '".$resx[0]->loccd."', sc_co = '".$resx[0]->loccd."', loccd = '".$resx[0]->loccd."', pricecode = '".$resx[0]->pricecode."'
			         WHERE orderno = '".$orderno."'";
			//echo $qry4."<br />";
			$res4 = $db_qryx->query($qry4);

			$qry5 = "UPDATE klink_mlm2010.dbo.newivtrh SET sc_dfno = '".$resx[0]->loccd."', dfno = '".$resx[0]->loccd."', loccd='".$resx[0]->loccd."', sc_co = '".$resx[0]->loccd."', pricecode = '".$resx[0]->pricecode."'
			         WHERE trcd = '".$rest[0]->CNno."'";
			//echo $qry5."<br />";
			$res5 = $db_qryx->query($qry5);

			$qry6 = "UPDATE klink_mlm2010.dbo.ordivhdr SET dfno = '".$resx[0]->loccd."', loccd = '".$resx[0]->loccd."', pricecode = '".$resx[0]->pricecode."'
			         WHERE registerno = '".$rest[0]->REGISTERno."'";
			//echo $qry6."<br />";
			$res6 = $db_qryx->query($qry6);

			$qry7 = "UPDATE klink_mlm2010.dbo.ordivtrh SET sc_dfno = '".$resx[0]->loccd."', dfno = '".$resx[0]->loccd."', loccd = '".$resx[0]->loccd."', pricecode = '".$resx[0]->pricecode."'
			         WHERE invoiceno = '".$rest[0]->CNno."'";
			//echo $qry7."<br />";
			$res7 = $db_qryx->query($qry7);

			$qryxx = "UPDATE klink_mlm2010.dbo.billivhdr SET dfno = '".$resx[0]->loccd."', loccd = '".$resx[0]->loccd."'
			         WHERE trcd = '".$rest[0]->KWno."'";
			//echo $qry5."<br />";
			$resXX = $db_qryx->query($qryxx);

			$qryWebol = "UPDATE klink_mlm2010.dbo.webol_trans_ok SET idstk = '".$resx[0]->loccd."', nmstkk = '".$resx[0]->loccd." - ".$resx[0]->fullnm."'
			         WHERE orderno = '$orderno'";
			//echo $qry5."<br />";
			$resqryWebol = $db_qryx->query($qryWebol);

			/*$qry8 = "UPDATE db_ecommerce.dbo.master_kgp_wil SET loccd = '".$resx[0]->loccd."', fullnm = '".$resx[0]->fullnm."', pricecode = '".$resx[0]->pricecode."'
			         WHERE KotaKode = '".$rest[0]->kab_code."'";
			//echo $qry8."<br />";
			$res8 = $db_qryx->query($qry8);*/

			$qry9 = "UPDATE klink_mlm2010.dbo.custpaybal SET dfno = '".$resx[0]->loccd."'
			         WHERE trcd = '".$rest[0]->IPno."'";
			//echo $qry7."<br />";
			$res9 = $db_qryx->query($qry9);

			$qry10 = "UPDATE klink_mlm2010.dbo.bbhdr SET dfno = '".$resx[0]->loccd."'
			         WHERE trcd = '".$rest[0]->IPno."'";
			//echo $qry7."<br />";
			$res10 = $db_qryx->query($qry10);

		    if ($db_qryx->trans_status() === FALSE) {
	            $db_qryx->trans_rollback();
			    $return = jsonFalseResponse("Update data stockist gagal..");
	        } else {
	        	$db_qryx->trans_commit();
				$return = jsonTrueResponse(null, "Update Data Stockist berhasil");

			}
		} else {
			$return = jsonFalseResponse("ID Stockist Invalid..");
		}
		return $return;
		/*$db_qryx->trans_begin();


		if ($db_qryx->trans_status() === FALSE) {
            $db_qryx->trans_rollback();
		    return false;
        } else {
        	$db_qryx->trans_commit();
			return true;
		}*/
	}

	function getEcommMemb($id) {
		$qry = "SELECT * FROM ecomm_memb_ok a WHERE a.trx_no = '$id'";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getTrxHeader($id) {
		$qry = "SELECT a.*, b.tel_hp
			    FROM V_ECOMM_SEARCH_CN A
			    	LEFT OUTER JOIN klink_mlm2010.dbo.msmemb b on a.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS=b.dfno
				WHERE A.orderno = '$id'
				ORDER BY datetrans, orderno";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getTrxByID2($id){
		$q = $this->db->query("select * from ecomm_trans_hdr_sgo where token = '$id'");

		return $q->result();
	}

	function getTrxDetail($orderno) {
		$qry = "SELECT * FROM ecomm_trans_det_prd A
				WHERE A.orderno = '$orderno'";
        //echo $qry;

		$res = $this->getRecordset($qry);
		return $res;
	}

	function getShipAddr($orderno) {
		$qry = "SELECT * FROM V_ECOM_LIST_PICKUP_READY_HDR A
				WHERE A.orderno = '$orderno'";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getReconcileData($orderno) {
		$qry = "SELECT orderno, token, KWno, CNno, REGISTERno, sentTo FROM ecomm_trans_hdr WHERE orderno = '$orderno'";
		$hdr = $this->getRecordset($qry);
		$arrHead = array(
			"orderno" => $hdr[0]->orderno,
			"token" => $hdr[0]->token,
			"KWno" => $hdr[0]->KWno,
			"CNno" => $hdr[0]->CNno,
			"REGISTERno" => $hdr[0]->REGISTERno,
		);

		$qry2 = "SELECT orderno FROM ecomm_trans_det_prd WHERE orderno = '$orderno'";
		$has1 = $this->getRecordset($qry2);
		if($has1 != null) {
			$res = $has1[0]->orderno;
		} else {
			$res = null;
		}

		if($hdr[0]->sentTo == "2") {
		 $qry3 = "SELECT orderno FROM ecomm_trans_shipaddr WHERE orderno = '$orderno'";
		 $has2= $this->getRecordset($qry3);
			 if($has2 != null) {
				$res2 = $has2[0]->orderno;
			} else {
				$res2 = null;
			}
		} else {
			$res2 = 1;
		}
		$qry4 = "SELECT orderno FROM ecomm_trans_paydet WHERE orderno = '$orderno'";
		$has3 = $this->getRecordset($qry4);
		if($has3 != null) {
				$res3 = $has3[0]->orderno;
			} else {
				$res3 = null;
			}

		$hasil = array("hdr" => $arrHead, "product" => $res, "shipping" => $res2, "payment" => $res3);
		return $hasil;
	}

	function saveReconcileTrxData($orderno, $prd_stt, $ship_stt, $pay_stt) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);

		$ces = "SELECT orderno, token, KWno, CNno, sentTo FROM ecomm_trans_hdr WHERE orderno = '$orderno'";
		$hasil = $dbqryx->query($ces);
		$res = $hasil->result();
		$prd = null;
		$ship = null;
		if($prd_stt == "0") {
			$qry_prd = "INSERT INTO ecomm_trans_det_prd (orderno, prdcd, prdnm, qty, bvr, dpr, pricecode, sentTo)
	                SELECT '".$res[0]->orderno."', prdcd, prdnm, qty, bvr, dpr, pricecode, sentTo
	                FROM ecomm_trans_det_prd_sgo WHERE orderno = '".$res[0]->token."'";
			//$prd = $dbqryx->query($qry_prd);

			echo $qry_prd."<br />";
		}

		if($ship_stt == "0") {
			$qry_ship = "INSERT INTO ecomm_trans_shipaddr (orderno, idstk, prov_code, kab_code, kec_code, kel_code, receiver_name, addr1,
			              addr2, addr3, email, tel_hp1, tel_hp2, conoteJNE, service_type_id, service_type_name, flag_send_conote,
			              total_item, total_weight, cargo_id)
	                SELECT '".$res[0]->orderno."', idstk, prov_code, kab_code, kec_code, kel_code, receiver_name, addr1,
			              addr2, addr3, email, tel_hp1, tel_hp2, conoteJNE, service_type_id, service_type_name, flag_send_conote,
			              total_item, total_weight, cargo_id
	                FROM ecomm_trans_shipaddr_sgo WHERE orderno = '".$res[0]->token."'";
	        $ship = $dbqryx->query($qry_ship);
	        //echo $qry_ship."<br />";
		}

		return array("prd" => $prd, "ship" => $ship);
	}

	function partReconcile($datax) {
		$dbqryx  = $this->load->database("db_ecommerce", TRUE);
		if($datax['orderno'] == "") {
			$hasil = jsonFalseResponse("Order No tidak boleh kosong..");
			return;
		}

		$qry = "SELECT orderno, token, KWno, CNno, REGISTERno, sentTo FROM ecomm_trans_hdr WHERE orderno = '$datax[orderno]'";
		$hdr = $this->getRecordset($qry);
		$arrHead = array(
			"orderno" => $hdr[0]->orderno,
			"token" => $hdr[0]->token,
			"KWno" => $hdr[0]->KWno,
			"CNno" => $hdr[0]->CNno,
			"REGISTERno" => $hdr[0]->REGISTERno,
		);

		$prdStatus = $datax['prd_stt'];
		$payStatus = $datax['pay_stt'];
		$err = 0;
		if($datax['prd_stt'] == "0") {
			$qryEcommDetPrd = "INSERT INTO ecomm_trans_det_prd (orderno, prdcd, a.prdnm, qty, dpr, bvr, pricecode, sentTo)
					SELECT '$datax[orderno]', a.prdcd, a.prdnm, a.qty, a.dpr, a.bvr, a.pricecode, a.sentTo
					FROM ecomm_trans_det_prd_sgo a
					WHERE a.orderno = '$arrHead[token]'
			";
			$exe = $dbqryx->query($qryEcommDetPrd);
			if($exe > 0) {
			   $prdStatus = 1;
			} else {
			   $prdStatus = 2;
			   $err++;
			}

			/*$qryScNewtrd = "INSERT INTO klink_mlm2010.dbo.sc_newtrd (
							   trcd, prdcd, qtyord, qtyship, qtyremain, dp, pv, bv, pricecode)
							SELECT '$datax[orderno]', a.prdcd, a.qty as qtyord, a.qty as qtyship, a.qty as qtyremain,
							a.dpr as dp, a.bvr as pv, a.bvr as bv, a.pricecode
							FROM db_ecommerce.dbo.ecomm_trans_det_prd_sgo a
							WHERE a.orderno = '$arrHead[token]'";
			echo $qryScNewtrd."<br /><br />";

			$qryBillivprd = "INSERT INTO klink_mlm2010.dbo.billivprd (
							   trcd, prdcd, qtyord, qtyship, qtyremain, dp, pv, bv, pricecode, OLtrcd)
							SELECT '$arrHead[KWno]', a.prdcd, a.qty as qtyord, a.qty as qtyship, a.qty as qtyremain,
							a.dpr as dp, a.bvr as pv, a.bvr as bv, a.pricecode, '$arrHead[KWno]'
							FROM db_ecommerce.dbo.ecomm_trans_det_prd_sgo a
							WHERE a.orderno = '$arrHead[token]'";
			echo $qryBillivprd."<br /><br />";

			$qryOrdivtrd = "INSERT INTO ordivtrd (
							registerno, invoiceno, prdcd, qtyord, dp,pv, bv, pricecode, OLinvoiceno, OLregisterno
							)
							SELECT '$arrHead[REGISTERno]', '$arrHead[CNno]', a.prdcd, a.qty as qtyord,
							a.dpr as dp, a.bvr as pv, a.bvr as bv, a.pricecode, '$arrHead[CNno]', '$arrHead[REGISTERno]'
							FROM db_ecommerce.dbo.ecomm_trans_det_prd_sgo a
							WHERE a.orderno = '$arrHead[token]'";
			echo $qryOrdivtrd."<br /><br />";

			$qryWebol_detprod = "INSERT INTO klink_mlm2010.dbo.webol_det_prod (
								 orderno, prdcd, prdnm, qty, dpr, bvr, pricecode
								)
								SELECT '$datax[orderno]', a.prdcd, a.prdnm, a.qty as qtyord,
								a.dpr as dp, a.bvr as pv, a.pricecode
								FROM db_ecommerce.dbo.ecomm_trans_det_prd_sgo a
								WHERE a.orderno = '$arrHead[token]'";
			echo $qryWebol_detprod."<br /><br />"; */
		}

		if($datax['pay_stt'] == "0") {
			$qryEcommDetPay = "INSERT INTO ecomm_trans_paydet (orderno, seqno, paytype, docno, payamt, deposit, notes, paystatus, bank_code_payment, charge_admin)
					SELECT '$datax[orderno]', seqno, paytype, docno, payamt, deposit, notes, paystatus, bank_code_payment,charge_admin
					FROM ecomm_trans_paydet_sgo a
					WHERE a.orderno = '$arrHead[token]'";
			//echo $qryEcommDetPay;
			$exe2 = $dbqryx->query($qryEcommDetPay);
			if($exe2 > 0) {
			   $payStatus = 1;
			} else {
			   $payStatus = 2;
			   $err++;
			}
		}

		$arr = array(
			"prdStatus" => $prdStatus,
			"payStatus" => $payStatus,
		);

		if($err > 0) {
			return array("response" => "false", "arrayData" => $arr, "message" => "Reconcile Gagal");
		} else {
			return array("response" => "true", "arrayData" => $arr, "message" => "Reconcile Berhasil");
		}
	}

	function getTrxPayment($orderno) {
		/*$qry = "SELECT paytype, docno, payamt, notes
			    FROM ecomm_trans_paydet A
				WHERE A.orderno = '$orderno'";*/
        //echo $qry;
        /*$qry = "SELECT a.paytype, a.docno, a.payamt, a.notes,
				   b.bank_code_payment, c.bankDisplayNm , b.token
				FROM ecomm_trans_paydet a
				LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CI_AS)
				LEFT JOIN ecomm_bank c ON (b.bank_code_payment = c.id )
				WHERE A.orderno = '$orderno'";	*/

		$qry = "SELECT a.paytype, a.docno, a.payamt, a.notes,
				   a.bank_code_payment, c.bankDisplayNm , b.token, a.charge_admin
				FROM ecomm_trans_paydet a
				LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CI_AS)
				LEFT JOIN ecomm_bank c ON (a.bank_code_payment = c.id )
				WHERE A.orderno = '$orderno'";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function savePostingTransaction($param) {
		$qry = "UPDATE ecomm_trans_hdr SET CNStatus = '1', KWStatus = '1', IPStatus = '1',
		           eod_status = '1', CNPosteddt = '".$this->dateTime."',
		           KWPosteddt = '".$this->dateTime."', IPPosteddt = '".$this->dateTime."'
		        WHERE orderno IN ($param)";
	    $res = $this->executeQuery($qry);
		return $res;
	}

	function getCNReportToPrint($data) {
		$jum = count($data['cnno']);
		$arr = array();
		for($i = 0; $i < $jum; $i++) {
			$cn = "SELECT CNno, total_pay, total_bv, orderno, nmstkk,sentTo,
						  id_memb, nmmember, bonusmonth, payShip, payAdm
				   FROM V_ECOMM_SEARCH_CN A
				   WHERE CNno = '".$data['cnno'][$i]."'";
		    $exeCN = $this->db->query($cn);
		    $header = $exeCN->result();

		    $det = "SELECT prdcd, prdnm, qty, bvr, dpr
		    		FROM ecomm_trans_det_prd
		    		WHERE orderno = '".$header[0]->orderno."'";
		    $exeCNdet = $this->db->query($det);
			$detail = $exeCNdet->result();

			$pay = "SELECT paytype, payamt
		    		FROM ecomm_trans_paydet
		    		WHERE orderno = '".$header[0]->orderno."'";
		    $exePay = $this->db->query($pay);
			$payment = $exePay->result();

		    $arr[$i] = array("header" => $header, "detail" => $detail, "payment" => $payment);
		}
		return $arr;

	}

	function getKWReportToPrint($data) {

		$cn = "SELECT KWno, IPno, CNno, id_memb, idstk, total_pay
			   FROM V_ECOMM_SEARCH_CN A
			   WHERE KWno IN ($data)";
	    $exeCN = $this->db->query($cn);
	    $header = $exeCN->result();

		return $header;
	}

	function updateCNStatus($param, $value) {
		$qry = "UPDATE ecomm_trans_hdr SET CNPrintStatus = '$value'
		        WHERE CNno IN ($param)";
	    $res = $this->executeQuery($qry);
		return $res;
	}

	function updateKWStatus($param, $value) {
		$qry = "UPDATE ecomm_trans_hdr SET KWPrintStatus = '$value'
		        WHERE KWno IN ($param)";
	    $res = $this->executeQuery($qry);
		return $res;
	}

	function updateIPStatus($param, $value) {
		$qry = "UPDATE ecomm_trans_hdr SET IPPrintStatus = '$value'
		        WHERE IPno IN ($param)";
	    $res = $this->executeQuery($qry);
		return $res;
	}

	function resendSMSNotification($hp_login, $text) {
		$this->db = $this->load->database("alternate2", true);
		//$text = 'K-LINK : Trx berhasil diproses,No Order: '.$ins[0]->trx_no.', Idmember : '.$ins[0]->memberid.',Password : '.$ins[0]->password.'';
        $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
                    VALUES ('".$hp_login."','".$text."','K-Link Transaction Status', 'MyPhone6')";
        //echo "Ins SMS : $insSms";
        $query = $this->db->query($insSms);
		return $query;
	}


	function getTrxShopOL($orderno) {
		/*$qry = "SELECT A.secno, A.orderno,
				       B.userlogin, C.tel_hp, A.sentTo, D.conoteJNE, D.cargo_id, A.total_pay
				FROM db_ecommerce.dbo.ecomm_trans_hdr A
				LEFT OUTER JOIN ecomm_trans_shipaddr D ON A.orderno=D.orderno
				INNER JOIN db_ecommerce.dbo.ecomm_checkout B
				    ON(A.orderno = B.orderno)
				INNER JOIN klink_mlm2010.dbo.msmemb C
				    ON(B.userlogin collate SQL_Latin1_General_CP1_CI_AS = C.dfno)
				WHERE A.orderno = '$orderno'";*/
		//echo $qry;
		$qry = "SELECT A.secno,
					A.orderno,
					a.token,
					C.email,
					B.userlogin,
					CASE
					WHEN C.tel_hp is null THEN E.tel_hp ELSE C.tel_hp
					END as tel_hp,
					A.sentTo,
					D.conoteJNE,
					D.cargo_id, A.total_pay, A.total_bv, A.token, E.password,
					E.dfno as idmember, E.fullnm as namamember , F.memberid as memberbaru,
					X.tel_hp AS tel_hp_login, a.no_hp_konfirmasi,
					d.tel_hp1, d.receiver_name
				FROM db_ecommerce.dbo.ecomm_trans_hdr A
				LEFT OUTER JOIN log_trans G ON (A.orderno = G.log_trcd)
				LEFT OUTER JOIN ecomm_trans_shipaddr D ON A.orderno=D.orderno
				LEFT OUTER JOIN db_ecommerce.dbo.ecomm_checkout B ON(A.orderno = B.orderno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb C ON(B.userlogin collate SQL_Latin1_General_CP1_CI_AS = C.dfno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb E ON(a.id_memb collate SQL_Latin1_General_CP1_CI_AS = E.dfno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb X ON(G.log_usrlogin collate SQL_Latin1_General_CP1_CI_AS = X.dfno)
				LEFT OUTER JOIN db_ecommerce.dbo.ecomm_memb_ok F ON (a.id_memb = F.memberid)
				WHERE A.orderno = '$orderno' ";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getTrxInputMember($orderno) {
		/*$qry = "SELECT A.trx_no, A.memberid, A.membername,
				   A.[password], ISNULL(c.tel_hp, c1.tel_hp) as tel_hp,E.sentTo, D.conoteJNE, E.secno
				FROM db_ecommerce.dbo.ecomm_memb_ok A
					LEFT OUTER JOIN ecomm_trans_shipaddr D ON(A.trx_no = D.orderno)
					INNER JOIN db_ecommerce.dbo.ecomm_trans_hdr E ON(A.trx_no = E.orderno)
					INNER JOIN db_ecommerce.dbo.ecomm_checkout B ON(A.trx_no = B.orderno)
					LEFT OUTER JOIN klink_mlm2010.dbo.msmemb C ON(B.userlogin collate SQL_Latin1_General_CP1_CI_AS = C.dfno)
					LEFT OUTER JOIN ecomm_memb_ok_sgo m_sgo ON m_sgo.trx_no COLLATE SQL_Latin1_General_CP1_CI_AS=b.token COLLATE SQL_Latin1_General_CP1_CI_AS
					LEFT OUTER JOIN klink_mlm2010.dbo.msmemb C1 ON(m_sgo.userlogin collate SQL_Latin1_General_CP1_CS_AS = C1.dfno)
				WHERE A.trx_no = '$orderno'"; */
		//echo $qry;
		//update by DION 02/03/2016
		$qry = "SELECT A.trx_no, A.memberid, A.membername,
				   A.[password], ISNULL(c.tel_hp, c1.tel_hp) as tel_hp, C2.tel_hp as telp_member_hp, E.sentTo, D.conoteJNE, D.cargo_id, E.secno,
				   A.email, d2.email as sponsoremail,
				   A.sponsorid, d2.fullnm as sponsorname, GG.prdcd
				FROM db_ecommerce.dbo.ecomm_memb_ok A
				    LEFT OUTER JOIN ecomm_trans_shipaddr D ON(A.trx_no = D.orderno)
				    INNER JOIN db_ecommerce.dbo.ecomm_trans_hdr E ON(A.trx_no = E.orderno)
				    INNER JOIN db_ecommerce.dbo.ecomm_checkout B ON(A.trx_no = B.orderno)
				    LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_det_prd GG ON (A.trx_no = GG.orderno)
				    LEFT OUTER JOIN klink_mlm2010.dbo.msmemb C ON(B.userlogin collate SQL_Latin1_General_CP1_CI_AS = C.dfno)
				    LEFT OUTER JOIN ecomm_memb_ok_sgo m_sgo ON m_sgo.trx_no COLLATE SQL_Latin1_General_CP1_CI_AS=b.token COLLATE SQL_Latin1_General_CP1_CI_AS
				    LEFT OUTER JOIN klink_mlm2010.dbo.msmemb C1 ON(m_sgo.userlogin collate SQL_Latin1_General_CP1_CS_AS = C1.dfno)
				    LEFT OUTER JOIN klink_mlm2010.dbo.msmemb C2 ON(A.memberid collate SQL_Latin1_General_CP1_CS_AS = C2.dfno)
				    LEFT OUTER JOIN klink_mlm2010.dbo.msmemb d2 ON(A.sponsorid collate SQL_Latin1_General_CP1_CS_AS = d2.dfno)
				WHERE A.trx_no = '$orderno'";
		$res = $this->getRecordset($qry);
		//echo $qry;
		return $res;
	}

	function getListIP($data) {
		$dtfrom = date("Y/m/d", strtotime($data['ip_from']));
		$dtto = date("Y/m/d", strtotime($data['ip_to']));
		$qry = "select *
				FROM V_ECOMM_LIST_IP a
				WHERE CONVERT(VARCHAR(10), A.dateIP , 111) BETWEEN '$dtfrom' AND '$dtto'
					  AND IPPrintStatus = '$data[ipPrintStt]'";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getIPReportToPrint($data) {
		$qry = "select *
				FROM V_ECOMM_LIST_IP a
				WHERE a.IPNo IN ($data)";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListJNEConote($data, $param, $value) {
		$dtfrom = date("Y/m/d", strtotime($data['send_from']));
		$dtto = date("Y/m/d", strtotime($data['send_to']));
		$qry = "select a.orderno, a.conoteJNE, a.service_type_id,
					a.service_type_name, CONVERT(VARCHAR(10), b.datetrans , 126) as datetrans
				FROM ecomm_trans_shipaddr a LEFT JOIN ecomm_trans_hdr b
				ON(a.orderno = b.orderno)
				WHERE $param = '$value'
				AND CONVERT(VARCHAR(10), b.datetrans, 111) BETWEEN '$dtfrom' AND '$dtto'
				AND (a.conoteJNE is not null AND a.conoteJNE != '') AND a.service_type_id is not null";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function updateFlagSendConote($upd, $pickup_datetime) {
		$pickup = "P".randomNumber(8);
		$newstr = str_replace("`", "'", $upd);
		$pickup_datetime = date("Y-m-d h:i:s", strtotime($pickup_datetime));

		$qry = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr
				SET is_pickup = '1', pickup_date = '".date("Y-m-d h:i:s")."' ,
				pickup_number = '$pickup', pickup_datetime = '$pickup_datetime'
		        WHERE orderno IN ($newstr)";
		//echo $qry ."<br />";
		$sss = $this->db->query($qry);
		return $sss;
	}

	function getListCargo() {
		//$qry = "SELECT * FROM V_ECOM_LIST_PICKUP_READY A WHERE A.CARGO_ID='1' AND A.order_taken = 0";

		$qry = "SELECT shipper_id, shipper_code, shipper_name, remarks, shipper_status from master_logistic_shipper order by shipper_id";

		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListEmailToSend($cargo_id) {
		//$qry = "SELECT * FROM V_ECOM_LIST_PICKUP_READY A WHERE A.CARGO_ID='1' AND A.order_taken = 0";

		/*
		$qry = "SELECT a.*,
		DATEADD(mi, 120, a.datetrans) as exp, GETDATE() as date_now
		FROM V_ECOM_LIST_PICKUP_READY A
		WHERE A.CARGO_ID='$cargo_id' AND A.delivery_status != 4 AND (A.order_taken = 0 OR
		(A.order_taken=1 and GETDATE() <= DATEADD(mi, 120, a.datetrans)))";
		*/

		$qry = "SELECT a.taken_by, a.delivery_status, a.order_taken, a.CP_NAME, a.NATIONAL_ACCOUNT, a.ADDR,
						a.area, a.PIC, a.TEL_NO, a.PACKAGE, a.CARGO_ID, a.IS_PICKUP, a.shipper_code,
						a.shipper_name, a.QTY, a.DIMENS, a.SERVICE_TYPE, a.PICKUP_VHC, a.PICKUP_TIME,
						a.DEST_CITY, a.REMARK, a.WHCD, a.ORDERNO, GETDATE() as date_now
				FROM V_ECOM_LIST_PICKUP_READY A
				WHERE A.CARGO_ID='$cargo_id'
					  AND A.delivery_status != 4
					  AND (A.order_taken = 0 OR (A.order_taken=1 and GETDATE() <= DATEADD(mi, 120, a.datetrans)))
				group by a.taken_by, a.delivery_status, a.order_taken, a.CP_NAME, a.NATIONAL_ACCOUNT, a.ADDR,
						 a.area, a.PIC, a.TEL_NO, a.PACKAGE, a.CARGO_ID, a.IS_PICKUP, a.shipper_code,
						 a.shipper_name, a.QTY, a.DIMENS, a.SERVICE_TYPE, a.PICKUP_VHC, a.PICKUP_TIME,
						 a.DEST_CITY, a.REMARK, a.WHCD, a.ORDERNO";

		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListPickUP($cargo_id, $datepick) {
		$qry = "select CONVERT(VARCHAR(10), a.pickup_date , 103) as pickup_date, A.pickup_datetime,
						CONVERT(VARCHAR(10), a.pickup_date , 108) as pickup_time, a.pickup_number,
				    STUFF((
				              SELECT ', ' + (B.ORDERNO)
				                FROM V_ECOMM_LIST_PICKUP2 B
				                WHERE B.pickup_number=A.pickup_number
				                GROUP BY B.ORDERNO
				                ORDER BY B.ORDERNO
				                FOR XML PATH ('')),1,2,''
				                ) AS ORDERNO,
				       a.WHCD, c.fullnm,
				       a.cargo_id, a.shipper_code
				from V_ECOMM_LIST_PICKUP2 a
				left outer join klink_mlm2010.dbo.mssc c on a.WHCD=c.loccd
				where a.cargo_id = '$cargo_id' and CONVERT(VARCHAR(10), a.pickup_date , 103)='$datepick'
				group by A.pickup_datetime, a.pickup_date, a.pickup_number,a.WHCD, c.fullnm, a.cargo_id, a.shipper_code
				order by a.pickup_datetime desc, a.pickup_date desc, a.pickup_number";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListVaTrxType() {
		$qry = "SELECT a.trxtype, a.shortdescription
				FROM va_trx_type a
				WHERE a.effect = '-' and a.trxtype IN ('TRM', 'TRS', 'PX2')";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getDetailPickUpProduct($id) {
		$qry = "select A.pickup_number, B.prdcd, B.prdnm, SUM(B.qty) AS qty, B.dpr, B.bvr
				FROM ecomm_trans_hdr A
				INNER JOIN ecomm_trans_det_prd B ON A.orderno=B.orderno
				WHERE A.is_pickup='1' AND
				  A.sentTo='2' AND status_vt_pay='1'
				  and b.prdcd<>'SK50E' and b.prdcd<>'SK004AO'
				      AND A.pickup_number='$id'
				GROUP BY A.pickup_number, B.prdcd, B.prdnm, B.dpr, B.bvr";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListShippingLabel($from, $to, $print_count, $cargo_id) {
		$dtfrom = date("Y/m/d", strtotime($from));
		$dtto = date("Y/m/d", strtotime($to));
		if($print_count == "0") {
			$add = " AND a.print_count = 0";
			$date_add="dateadd(day,-7,'$dtfrom')";


		} else {
			$add = " AND a.print_count > 0";
			$date_add="'$dtfrom'";
		}

		$usergroup = $this->session->userdata("ecom_groupname");

		if($usergroup == "SUPER ADMINISTRATOR"){
			$whcd = "";
		}else{
			$whcd = " AND whcd = '$usergroup' ";
		}

//		$qry = "SELECT * FROM  V_ECOM_LIST_PICKUP_READY_HDR
//			    WHERE CONVERT(VARCHAR(10), datetrans, 111) BETWEEN '$dtfrom' AND '$dtto'
//			    $whcd
//			    $add";

		$qry = "SELECT a.*, b.shipper_name
				FROM V_ECOM_LIST_PICKUP_READY_HDR a
				LEFT JOIN master_logistic_shipper b
				ON a.cargo_id = b.shipper_id
				WHERE CONVERT(VARCHAR(10), datetrans, 111) BETWEEN $date_add AND '$dtto'
				$add
				AND a.cargo_id = $cargo_id $whcd";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}



	function getDetailProductByOrderNo($orderno) {
		//echo $orderno;
		$orderno = explode("**", $orderno);
		$orderno = $orderno[0];
		$conote_new = $orderno[1];

		$qry = "SELECT a.orderno, a.prdcd, a.prdnm,  SUM(a.qty) as qty,  MAX(a.dpr) as dpr, MAX(a.bvr) as bvr
			    FROM  V_ECOM_LIST_PICKUP_READY_DET_BDL A WHERE A.orderno='$orderno'
			    GROUP BY a.orderno, a.prdcd, a.prdnm";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function printLabelShippingTes($orderno) {
		$conn = $this->load->database("db_ecommerce", true);
		$qry = "SELECT * FROM  V_ECOM_LIST_PICKUP_READY_HDR
			    WHERE orderno IN ($orderno)";
	    //echo "".$qry."<br>";
		$query = $conn->query($qry);
		$arr = array();
		foreach($query->result() as $dta) {
			 $nilai['orderno'] = $dta->orderno;
			 $nilai['payShip'] = $dta->payShip;
			 $nilai['conoteJNE'] = $dta->conoteJNE;
			 $nilai['id_memb'] = $dta->id_memb;
			 $nilai['nmmember'] = $dta->nmmember;
			 $nilai['receiver_name'] = $dta->receiver_name;
			 $nilai['contactno'] = $dta->contactno;
			 $nilai['addr'] = $dta->addr;
             $nilai['propinsi'] = $dta->PROPINSI;
             $nilai['kec'] = $dta->Kecamatan;

			 $nilai['datetrans2'] = $dta->datetrans2;
             //echo "nilai addr ny ".$nilai['addr'];
             $nilai['wh_name'] = $dta->wh_name;
			 $nilai['whcd_addr1'] = $dta->whcd_addr1;
			 $nilai['whcd_addr2'] = $dta->whcd_addr2;
			 $nilai['whcd_addr3'] = $dta->whcd_addr3;
			 $nilai['cargo_id'] = $dta->cargo_id;
			 $nilai['shipper_code'] = $dta->shipper_code;
			 $nilai['shipper_name'] = $dta->shipper_name;
			 $nilai['service_type_name'] = $dta->service_type_name;
			 $nilai['logo_url'] = $dta-> logo_url;

			 $kota = "";
			 if($dta->cargo_id == "1"){
			 	$kota = $dta->kode_kabupaten;
			 }elseif($dta->cargo_id == "2"){
				$kota = $dta->kota_nama_kgp;
			 }
			 $nilai['kode_kabupaten'] = $kota;

			 //$nilai['kode_kabupaten'] = $dta->kode_kabupaten;
			 $nilai['total_weight'] = $dta->total_weight;
			 $nilai['total_item'] = $dta->total_item;
			 $nilai['payShip'] = $dta->payShip;
			 $det = "SELECT * FROM   V_ECOM_LIST_PICKUP_READY_DET_BDL
			         WHERE orderno = '$dta->orderno'";
			 //echo $det."<br />";
			 $query2 = $conn->query($det);
			 $nilai['detPrd'] = $query2->result();
			 array_push($arr, $nilai);

		}
        //echo $qry;
		//$res = $this->getRecordset($qry);
		return $arr;
	}

	function printLabelShipping($orderno) {
		$conn = $this->load->database("db_ecommerce", true);
		$qry = "SELECT * FROM  V_ECOM_LIST_PICKUP_READY_HDR
			    WHERE orderno IN ($orderno)";
	    //echo "TEST ".$qry."<br>";
		$query = $conn->query($qry);
		$arr = array();
		foreach($query->result() as $dta) {
			 $nilai['orderno'] = $dta->orderno;
			 $nilai['conoteJNE'] = $dta->conoteJNE;
			 $nilai['id_memb'] = $dta->id_memb;
			 $nilai['nmmember'] = $dta->nmmember;
			 $nilai['receiver_name'] = $dta->receiver_name;
			 $nilai['contactno'] = $dta->contactno;
			 $nilai['addr'] = $dta->addr;
             $nilai['propinsi'] = $dta->PROPINSI;
             $nilai['kec'] = $dta->Kecamatan;

			 $nilai['datetrans2'] = $dta->datetrans2;
             //echo "nilai addr ny ".$nilai['addr'];
             $nilai['wh_name'] = $dta->wh_name;
			 $nilai['whcd_addr1'] = $dta->whcd_addr1;
			 $nilai['whcd_addr2'] = $dta->whcd_addr2;
			 $nilai['whcd_addr3'] = $dta->whcd_addr3;
			 $nilai['cargo_id'] = $dta->cargo_id;
			 $nilai['shipper_code'] = $dta->shipper_code;
			 $nilai['shipper_name'] = $dta->shipper_name;
			 $nilai['service_type_name'] = $dta->service_type_name;
			 $nilai['logo_url'] = $dta->logo_url;

			 $kota = "";
			 if($dta->cargo_id == "1"){
			 	$kota = $dta->kode_kabupaten;
			 }elseif($dta->cargo_id == "2"){
				$kota = $dta->kota_nama_kgp;
			 }
			 $nilai['kode_kabupaten'] = $kota;

			 //$nilai['kode_kabupaten'] = $dta->kode_kabupaten;
			 $nilai['total_weight'] = $dta->total_weight;
			 $nilai['total_item'] = $dta->total_item;
			 $nilai['payShip'] = $dta->payShip;
			 $det = "SELECT * FROM   V_ECOM_LIST_PICKUP_READY_DET_BDL
			         WHERE orderno = '$dta->orderno'";
			 //echo $det;
			 $query2 = $conn->query($det);
			 $nilai['detPrd'] = $query2->result();
			 array_push($arr, $nilai);

		}
        //echo $qry;
		//$res = $this->getRecordset($qry);
		return $arr;
	}

	function updateStatusPrintShipping($orderno) {
		$tgl = date("Y-m-d H:i:s");
		$qry = "UPDATE ecomm_trans_hdr
				SET print_count = print_count + 1, print_date = '$tgl'
		        WHERE orderno IN ($orderno)";
		$upd = $this->executeQuery($qry, $this->db1);
		return $upd;
	}

	function printLabelShippingDetProduct($orderno) {
		$qry = "SELECT * FROM   V_ECOM_LIST_PICKUP_READY_DET_BDL
			    WHERE orderno IN ($orderno)";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function deleteTransaction($orderid) {
		$qry = "DECLARE @orderno AS VARCHAR(25),
					    @ipno	 AS VARCHAR(25),
				        @kwno	 AS VARCHAR(25),
				        @regno	 AS VARCHAR(25),
				        @dfno	 AS VARCHAR(25)

				SELECT @orderno	= a.orderno,
				       @ipno	= a.IPno,
				       @kwno	= a.KWno,
				       @regno	= a.REGISTERno,
				       @dfno	= a.id_memb
				FROM ecomm_trans_hdr A
				WHERE A.orderno='$orderid'

				DELETE from klink_mlm2010.dbo.webol_trans_ok
				where orderno in (@orderno)

				delete from klink_mlm2010.dbo.webol_det_prod
				where orderno in  (@orderno)

				delete from klink_mlm2010.dbo.webol_logs_trans
				where orderno in  (@orderno)

				DELETE FROM log_trans
				WHERE log_trcd IN (@orderno)

				DELETE FROM ecomm_trans_hdr
				WHERE orderno IN  (@orderno)

				DELETE FROM ecomm_trans_det_prd
				WHERE orderno IN  (@orderno)

				DELETE FROM ecomm_trans_paydet
				WHERE orderno IN (@orderno)

				DELETE FROM ecomm_trans_shipaddr
				WHERE orderno IN  (@orderno)

				delete from ecomm_checkout
				where orderno in  (@orderno)

				delete from klink_mlm2010.dbo.newtrh
				where trcd in  (@orderno)

				delete from klink_mlm2010.dbo.newtrd
				where trcd in   (@orderno)

				delete from klink_mlm2010.dbo.sc_newtrh
				where trcd in   (@orderno)

				delete from klink_mlm2010.dbo.sc_newtrd
				where trcd in   (@orderno)

				DELETE FROM klink_mlm2010.dbo.bbhdr
				where trcd in (@ipno)

				DELETE FROM klink_mlm2010.dbo.custpaybal
				where trcd in  (@ipno)

				DELETE FROM klink_mlm2010.dbo.custpaydet
				where applyto in (@ipno)

				delete from klink_mlm2010.dbo.billivhdr
				where trcd in (@kwno)

				delete from klink_mlm2010.dbo.billivdetp
				where trcd in (@kwno)

				delete from klink_mlm2010.dbo.billivprd
				where trcd in (@kwno)

				DELETE from klink_mlm2010.dbo.ordivhdr
				where registerno in (@regno)

				delete from klink_mlm2010.dbo.ordivtrh
				where registerno in  (@regno)

				delete from klink_mlm2010.dbo.orddetp
				where trcd in (@regno)

				delete from klink_mlm2010.dbo.ordtrd
				where registerno in  (@regno)";
		//echo $qry;
		$res = $this->executeQuery($qry);
		return $res;
	}

	function deleteTrxIfMember($orderid) {
		$qry = "SELECT memberid FROM ecomm_memb_ok WHERE trx_no = '$orderid'";
		$res = $this->getRecordset($qry, null, $this->db1);
		if($res != null) {
			$del = "DELETE FROM ecomm_memb_ok WHERE memberid = '".$res[0]->memberid."'";
			$delExec = $this->executeQuery($del, $this->db1);
		}
	}

	function checkDataReportDownloadSGO($id) {
		$mdReturn = false;
		$qry = "SELECT Transaction_ID FROM REPORT_DOWNLOAD_SGO WHERE Transaction_ID = '$id'";
		$res = $this->getRecordset($qry, null, $this->db1);
		if($res == null) {
			$mdReturn = true;
		}
		return $mdReturn;
	}

	function updateSgoTrxId($data) {
		$totalRecord = 0;
		$double = 0;
		$insSuccess = 0;
		$insFail = 0;
		$arrFail = array();
		$arrDouble = array();

		foreach($data['csvData'] as $list) {
			$totalRecord++;
			/*if($list[14] != null && $list[14]) {
				$hasil = explode(" = ", $list[14]);
				//print_r($hasil);
				$hasil2 = explode(" ", $hasil[1]);
				$order_id = $hasil2[0];
			} else {
			   $order_id = null;
			}*/
		     $tx_id    = $list[0];
			 $payment_ref = $list[11];


			 if($payment_ref != "" && $payment_ref != null) {
				 $check = $this->checkDataReportDownloadSGO($payment_ref);

				 if(!$check) {
					$qry = "UPDATE REPORT_DOWNLOAD_SGO
							SET Transaction_ID_Backup = Transaction_ID,
							Transaction_ID = '$tx_id'
							WHERE Transaction_ID = '$payment_ref'";
					//echo $qry;
					$insExec = $this->executeQuery($qry, $this->db1);
					//echo "<br />";
					if($insExec > 0) {
					$insSuccess++;
					} else {
						$insFail++;
						array_push($arrFail, $tx_id);
					}
				 }
			 }


		}

		$arr = array("totalRecord" => $totalRecord,
					"double" => $double,
					"insSuccess" => $insSuccess,
					"insFail" => $insFail,
					"arrFail" => $arrFail,
					"arrDouble" => $arrDouble
					);
		return $arr;
	}

	function saveImportToDBGW($data) {
		$totalRecord = 0;
		$double = 0;
		$insSuccess = 0;
		$insFail = 0;
		$arrFail = array();
		$arrDouble = array();
		//print_r($data['csvData']);
		foreach($data['csvData'] as $list) {
			$totalRecord++;

			$hasil = explode(" = ", $list[14]);
		    //print_r($hasil);
		    $hasil2 = explode(" ", $hasil[1]);
		    $order_id = $hasil2[0];
			$community = $list[3];
		     $tx_id    = $list[0];
		     //$tx_id = str_replace('"', '', $tx_idx);
			 //$order_id = $list[1];
			 $merchant = $list[2];
			 $date     = $list[7];
			 $amount   = $list[6];
			 //$ccy   = $list['CCY'];
			 $bank     = $list[9];
			 $channel  = $list[10];
			 $status   = $list[8];
			 //$paid_status = $list['Paid Status'];
			 $member   = "SYSTEM";
			 $payment_ref = $list[11];
			 $prd_value   = str_replace("'", "`", $list[10]);
			 $desc     = $list[14];
			 $ccy = $list[5];

			 $check = $this->checkDataReportDownloadSGO($tx_id);
			 /*if($check) {
			 	echo "$order_id";
			 } else {
			 	echo "Trx $order_id sudah ada<br />";
			 }*/

			 if($check) {

				$num = str_replace(".", "", $amount);
				$num2 = str_replace(",", ".", $num);

				$date = date("Y-m-d h:i:s", strtotime($date));

				$qry = "INSERT INTO REPORT_DOWNLOAD_SGO (Transaction_ID, Anchor, Business_Scheme,
				              Community, Member, CCY, Amount,
				              Transaction_Date, Status, Bank_Product,
				              Product_Value, Payment_Ref, Created,
				              Lastupdated, Description, Order_ID)
					    VALUES ('".$tx_id."', '".$order_id."', '".$merchant."',
					            '".$community."', '".$member."', '$ccy', $num2,
					            '".$date."', '".$status."', '".$bank."',
					            '".$prd_value."', '".$payment_ref."', '".$date."',
					            '".$date."', '".$desc."', '".$order_id."')";
				//echo $qry;
				//echo "<br />";
				//echo "<br />";
				//array_push($arrQry, $qry);
			    $insExec = $this->executeQuery($qry, $this->db1);
				if($insExec > 0) {
					$insSuccess++;
				} else {
					$insFail++;
					array_push($arrFail, $tx_id);
				}
				//echo $qry;
				//echo "<br />";
			} else {
				$double++;
				array_push($arrDouble, $tx_id);
			}
	    }

		$arr = array("totalRecord" => $totalRecord,
					"double" => $double,
					"insSuccess" => $insSuccess,
					"insFail" => $insFail,
					"arrFail" => $arrFail,
					"arrDouble" => $arrDouble
					);
		return $arr;
	}

	function saveImportToDB($data) {
		$totalRecord = 0;
		$double = 0;
		$insSuccess = 0;
		$insFail = 0;
		$arrFail = array();
		$arrDouble = array();
		//print_r($data['csvData']);
		foreach($data['csvData'] as $list) {
			$totalRecord++;
		     $tx_id    = $list[0];
		     //$tx_id = str_replace('"', '', $tx_idx);
			 $order_id = $list[1];
			 $merchant = $list[2];
			 $date     = $list[3];
			 $amount   = $list[4];
			 //$ccy   = $list['CCY'];
			 $bank     = $list[7];
			 $channel  = $list[8];
			 $status   = $list[9];
			 //$paid_status = $list['Paid Status'];
			 $member   = "SYSTEM";
			 $payment_ref = $list[13];
			 $prd_value   = str_replace("'", "`", $list[14]);
			 $desc     = $list[16];

			 $check = $this->checkDataReportDownloadSGO($tx_id);
			 /*if($check) {
			 	echo "$order_id";
			 } else {
			 	echo "Trx $order_id sudah ada<br />";
			 }*/

			 if($check) {

				$num = str_replace(".", "", $amount);
				$num2 = str_replace(",", ".", $num);

				$date = date("Y-m-d h:i:s", strtotime($date));

				$qry = "INSERT INTO REPORT_DOWNLOAD_SGO (Transaction_ID, Anchor, Business_Scheme,
				              Community, Member, CCY, Amount,
				              Transaction_Date, Status, Bank_Product,
				              Product_Value, Payment_Ref, Created,
				              Lastupdated, Description, Order_ID)
					    VALUES ('".$tx_id."', '".$order_id."', '".$merchant."',
					            '".$merchant."', '".$member."', '', $num2,
					            '".$date."', '".$status."', '".$channel."',
					            '".$prd_value."', '".$payment_ref."', '".$date."',
					            '".$date."', '".$desc."', '".$order_id."')";
				//echo $qry;
				//array_push($arrQry, $qry);
			    $insExec = $this->executeQuery($qry, $this->db1);
				if($insExec > 0) {
					$insSuccess++;
				} else {
					$insFail++;
					array_push($arrFail, $tx_id);
				}
				//echo $qry;
				//echo "<br />";
			} else {
				$double++;
				array_push($arrDouble, $tx_id);
			}
	    }

		$arr = array("totalRecord" => $totalRecord,
					"double" => $double,
					"insSuccess" => $insSuccess,
					"insFail" => $insFail,
					"arrFail" => $arrFail,
					"arrDouble" => $arrDouble
					);
		return $arr;
	}

	function updateCargoShipper($orderno, $cargo_id) {
		$qry = "UPDATE db_ecommerce.dbo.ecomm_trans_shipaddr SET cargo_id = ".$cargo_id." WHERE orderno = '$orderno'";
		$insExec = $this->executeQuery($qry, $this->db1);
		return $insExec;
	}

	function getListAllCargo() {
		$qry = "SELECT * FROM master_logistic_shipper";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListSGOTrxPpobById($orderid) {
		$qry = "SELECT a.*, CONVERT(VARCHAR(20),
		 				a.Transaction_Date , 20) as Transaction_Datex
				FROM V_ECOMM_LIST_RECONCILE_SGO_TRX_PPOB A WHERE a.Order_ID = '$orderid'";
	    //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListSGOTrxPpob($data) {
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));

		//All=0, Success=1, Failed=2, Suspect=3
		$status = $data['status'];
		$bank = $data['bank'];

		if($status == "1"){
			$status_sgo = "Success";
		}elseif($status == "2"){
			$status_sgo = "Failed";
		}elseif($status == "3"){
			$status_sgo = "Suspect";
		}

		if($status == "0" && $bank == "all"){
			$paramStatus = ""; //all transaction status
		}elseif($status != "0" && $bank == "all"){
			$paramStatus = " AND Status_sgo = '$status_sgo' ";
		}elseif($status == "0" && $bank != "all"){
			$paramStatus = " AND A.bankCode = '$bank' ";
		}elseif($status != "0" && $bank != "all"){
			$paramStatus = " AND A.bankCode = '$bank' AND Status_sgo = '$status_sgo' ";
		}

		if($data['trx_type'] == "1") {
		     $paramStatus .= "AND (SUBSTRING(a.Order_ID, 1, 2) <> 'PX' AND SUBSTRING(a.Order_ID, 1, 2) <> 'PS')";
		} else {
			 $paramStatus .= "AND (SUBSTRING(a.Order_ID, 1, 2) = 'PX' OR SUBSTRING(a.Order_ID, 1, 2) = 'PS')";
		}

		//echo "$status<BR/>$bank<BR/>$paramStatus";

		/* ===========ORI=====================*/
		 /*
		  $qry = "SELECT a.*, CONVERT(VARCHAR(10), a.Transaction_Date , 126) as Transaction_Date
				FROM V_ECOMM_LIST_RECONCILE_SGO a
				WHERE (a.Transaction_Date BETWEEN '$data[from]' AND '$data[to]') $paramStatus
				ORDER BY a.Transaction_Date";
		  *
		  */
		 /*$qry = "SELECT a.Order_ID,a.id_memb,a.nmmember,a.Amount,a.Transaction_Date AS Transaction_Date1,CONVERT(VARCHAR(10),
		 				a.Transaction_Date , 126) as Transaction_Date,a.[Status_sgo],
		 				a.status_trx,a.MEMB_STATUS,a.ID_hdr,a.MEM_SGO,a.MEMB_OK,a.MSMEMBX,MEMB_STATUS,
		 				A.bankCode, B.bankDisplayNm, B.bankDesc
				 FROM V_ECOMM_LIST_RECONCILE_SGO a
				 	  LEFT OUTER JOIN ecomm_bank B ON A.bankCode=B.ID
				 WHERE (CONVERT(VARCHAR(10), A.Transaction_Date, 111) BETWEEN '$dtfrom' AND '$dtto') $paramStatus

				 GROUP BY a.Order_ID,a.id_memb,a.nmmember,a.Amount,
				 		a.Transaction_Date, CONVERT(VARCHAR(10), a.Transaction_Date , 126),a.[Status_sgo],
					   	a.status_trx,a.MEMB_STATUS, a.ID_hdr, a.MEM_SGO, a.MEMB_OK, a.MSMEMBX, A.MEMB_STATUS,
					   	A.bankCode, B.bankDisplayNm, B.bankDesc
				 ORDER BY a.Transaction_Date  DESC";*/

		//echo $qry;
		$qry = "SELECT a.*, CONVERT(VARCHAR(20),
		 				a.Transaction_Date , 100) as Transaction_Datex
				FROM V_ECOMM_LIST_RECONCILE_SGO_TRX_PPOB A
				WHERE (CONVERT(VARCHAR(10), A.Transaction_Date, 111) BETWEEN '$dtfrom' AND '$dtto') $paramStatus";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListSGOTrxForReconcile($data) {
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));

		//All=0, Success=1, Failed=2, Suspect=3
		$status = $data['status'];
		$bank = $data['bank'];

		if($status == "1"){
			$status_sgo = "Success";
			//$status_sgo = "Success";
		}elseif($status == "2"){
			$status_sgo = "Failed";
		}elseif($status == "3"){
			$status_sgo = "Suspect";
		}

		$tambah = "";
		if($status == "4") {
			$tambah = " AND a.status_trx NOT IN ('1', '2') and (a.Order_ID is not null and a.Order_ID != '') ";
		}

		$paramStatus = "";
		if($status == "0" && $bank == "all"){
			$paramStatus = ""; //all transaction status
		}elseif($status != "0" && $bank == "all" && $status != "4"){
			$paramStatus = " AND Status_sgo = '$status_sgo' ";
		}elseif($status == "0" && $bank != "all" && $status != "4"){
			$paramStatus = " AND A.bankCode = '$bank' ";
		}elseif($status != "0" && $bank != "all" && $status != "4"){
			$paramStatus = " AND A.bankCode = '$bank' AND Status_sgo = '$status_sgo' ";
		}


		if($data['trx_type'] == "1") {
		     $paramStatus .= "AND (SUBSTRING(a.Order_ID, 1, 2) <> 'PX' AND SUBSTRING(a.Order_ID, 1, 2) <> 'PS') AND SUBSTRING(a.Order_ID, 1, 2) IN ('EC', 'EM', 'RM', 'EB', 'RB', 'LP', 'RA', 'EA', 'TA')";
		} else {
			 $paramStatus .= "AND (SUBSTRING(a.Order_ID, 1, 2) = 'PX' OR SUBSTRING(a.Order_ID, 1, 2) = 'PS')";
		}

		$paramStatus .= " AND a.Order_ID is not null and a.Order_ID != ''";
		//echo "$status<BR/>$bank<BR/>$paramStatus";

		/* ===========ORI=====================*/
		 /*
		  $qry = "SELECT a.*, CONVERT(VARCHAR(10), a.Transaction_Date , 126) as Transaction_Date
				FROM V_ECOMM_LIST_RECONCILE_SGO a
				WHERE (a.Transaction_Date BETWEEN '$data[from]' AND '$data[to]') $paramStatus
				ORDER BY a.Transaction_Date";
		  *
		  */
		 $qry = "SELECT a.Order_ID,a.id_memb,a.nmmember,a.Amount,a.Transaction_Date AS Transaction_Date1,CONVERT(VARCHAR(10),
		 				a.Transaction_Date , 126) as Transaction_Date,a.[Status_sgo],
		 				a.status_trx,a.MEMB_STATUS,a.ID_hdr,a.MEM_SGO,a.MEMB_OK,a.MSMEMBX,MEMB_STATUS,
		 				A.bankCode, B.bankDisplayNm, B.bankDesc, z.memberid
				 FROM V_ECOMM_LIST_RECONCILE_SGO a
				 	  LEFT OUTER JOIN ecomm_bank B ON A.bankCode=B.ID
					   LEFT OUTER JOIN ecomm_memb_ok_sgo z ON (a.Order_ID = z.trx_no)
				 WHERE (CONVERT(VARCHAR(10), A.Transaction_Date, 111) BETWEEN '$dtfrom' AND '$dtto') $tambah $paramStatus

				 GROUP BY a.Order_ID,a.id_memb,a.nmmember,a.Amount,
				 		a.Transaction_Date, CONVERT(VARCHAR(10), a.Transaction_Date , 126),a.[Status_sgo],
					   	a.status_trx,a.MEMB_STATUS, a.ID_hdr, a.MEM_SGO, a.MEMB_OK, a.MSMEMBX, A.MEMB_STATUS,
					   	A.bankCode, B.bankDisplayNm, B.bankDesc, z.memberid
				 ORDER BY a.Transaction_Date  DESC";

		//echo $qry;

		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListManualTrxForReconcile($data) {
		$dtfrom = date("Y-m-d", strtotime($data['from']));
		$dtto = date("Y-m-d", strtotime($data['to']));
		$type=$data['rptType'];

		if($type == '0'){
			$add="AND statusX != 'NULL'";
		}else{
			$add="AND statusX ='$type'";
		}

		/*$qry = "select * from ecomm_trans_hdr_sgo
				where kode_pay !='' AND (datetrans BETWEEN '$dtfrom 00:00:00' AND '$dtto 23:59:59') order by id DESC";*/
		$qry = "select * from V_HILAL_MUTASIBANK
				where kode_pay !='' AND (datetrans BETWEEN '$dtfrom 00:00:00' AND '$dtto 23:59:59') $add order by datetrans DESC";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListSGOTrx($data) {
		//print_r($data);
		$dtfrom = date("Y/m/d", strtotime($data['from']));
		$dtto = date("Y/m/d", strtotime($data['to']));

		//All=0, Success=1, Failed=2, Suspect=3
		$status = $data['status'];
		$bank = $data['bank'];

		if($status == "1"){
			$status_sgo = "Success";
		}elseif($status == "2"){
			$status_sgo = "Failed";
		}elseif($status == "3"){
			$status_sgo = "Suspect";
		}

		if($status == "0" && $bank == "all"){
			$paramStatus = ""; //all transaction status
		}elseif($status != "0" && $bank == "all"){
			$paramStatus = " AND Status_sgo = '$status_sgo' ";
		}elseif($status == "0" && $bank != "all"){
			$paramStatus = " AND A.bankCode = '$bank' ";
		}elseif($status != "0" && $bank != "all"){
			$paramStatus = " AND A.bankCode = '$bank' AND Status_sgo = '$status_sgo' ";
		}

		/*
		if($data['trx_type'] == "1") {
		     $paramStatus .= "AND (SUBSTRING(a.Order_ID, 1, 2) <> 'PX' AND SUBSTRING(a.Order_ID, 1, 2) <> 'PS')";
		} else {
			 $paramStatus .= "AND (SUBSTRING(a.Order_ID, 1, 2) = 'PX' OR SUBSTRING(a.Order_ID, 1, 2) = 'PS')";
		}
		 */
		$paramStatus .= " AND a.Order_ID is not null and a.Order_ID != ''";
		//echo "$status<BR/>$bank<BR/>$paramStatus";

		/* ===========ORI=====================*/
		 /*
		  $qry = "SELECT a.*, CONVERT(VARCHAR(10), a.Transaction_Date , 126) as Transaction_Date
				FROM V_ECOMM_LIST_RECONCILE_SGO a
				WHERE (a.Transaction_Date BETWEEN '$data[from]' AND '$data[to]') $paramStatus
				ORDER BY a.Transaction_Date";
		  *
		  */
		 $qry = "SELECT a.Order_ID,a.id_memb,a.nmmember,a.Amount,a.Transaction_Date AS Transaction_Date1,CONVERT(VARCHAR(10),
		 				a.Transaction_Date , 126) as Transaction_Date,a.[Status_sgo],
		 				a.status_trx,a.MEMB_STATUS,a.ID_hdr,a.MEM_SGO,a.MEMB_OK,a.MSMEMBX,MEMB_STATUS,
		 				A.bankCode, B.bankDisplayNm, B.bankDesc, z.memberid
				 FROM V_ECOMM_LIST_RECONCILE_SGO a
				 	  LEFT OUTER JOIN ecomm_bank B ON A.bankCode=B.ID
					   LEFT OUTER JOIN ecomm_memb_ok_sgo z ON (a.Order_ID = z.trx_no)
				 WHERE (CONVERT(VARCHAR(10), A.Transaction_Date, 111) BETWEEN '$dtfrom' AND '$dtto') $paramStatus

				 GROUP BY a.Order_ID,a.id_memb,a.nmmember,a.Amount,
				 		a.Transaction_Date, CONVERT(VARCHAR(10), a.Transaction_Date , 126),a.[Status_sgo],
					   	a.status_trx,a.MEMB_STATUS, a.ID_hdr, a.MEM_SGO, a.MEMB_OK, a.MSMEMBX, A.MEMB_STATUS,
					   	A.bankCode, B.bankDisplayNm, B.bankDesc, z.memberid
				 ORDER BY a.Transaction_Date  DESC";

		//echo $qry;

		$res = $this->getRecordset($qry);
		return $res;
	}

	function checkValidDataNewMember($token) {
		$qry = "SELECT a.sponsorid, a.recruiterid, a.idno, a.membername, a.stk_code
				FROM db_ecommerce.dbo.ecomm_memb_ok_sgo a
				WHERE a.trx_no = '$token'";
		$res = $this->getRecordset($qry);

		if($res == null) {
			$arr = array("response" => "false", "message" => "Data Membership tidak ditemukan di history..");
			return $arr;
		} else if($res[0]->sponsorid == null || $res[0]->sponsorid == "" || $res[0]->sponsorid == " " ||
		  $res[0]->recruiterid == null || $res[0]->recruiterid == "" || $res[0]->recruiterid == " " ||
		  $res[0]->idno == null || $res[0]->idno == "" || $res[0]->idno == " " ||
		  $res[0]->membername == null || $res[0]->membername == "" || $res[0]->membername == " " ||
		  $res[0]->stk_code == null || $res[0]->stk_code == "" || $res[0]->stk_code == " ") {
			$arr = array("response" => "false", "message" => "Data Sponsor, Recruiter, KTP, Nama dan Kode Stokis kurang lengkap dan harus diisi, silahkan cek di menu Reconcile Null Registration Member");
			return $arr;
		} else {
			$arr = array("response" => "true");
			return $arr;
		}





	}

	function saveTrxToDB($orderid) {
		$qryRecover = "EXEC RECOVERY_TRX '$orderid'";
		//echo $qryRecover;
	    $insExec = $this->executeQuery($qryRecover, $this->db1);
		$insSuccess = 0;
		if($insExec > 0) {
			$insSuccess = 1;
		}
		$arr = array("insSuccess" => $insSuccess,
					"orderid" => $orderid);
		return $arr;
	}

	function saveMEMToDB($memb_status, $memberid, $orderid) {
	    $checkKTP = "SELECT b.idno, a.dfno, a.fullnm, a.idno AS no_ktp_msmemb, a.sponsorid, a.sponsorregid, a.sfno_reg
					 FROM db_ecommerce.dbo.ecomm_memb_ok_sgo b
					 LEFT OUTER JOIN klink_mlm2010.dbo.msmemb a ON (b.idno COLLATE SQL_Latin1_General_CP1_CS_AS = a.idno)
					 where b.trx_no = '$orderid'";
		$hasilCek = $this->getRecordset($checkKTP, null, "klink_mlm2010");
		if($hasilCek != null) {
		    if($hasilCek[0]->no_ktp_msmemb == null) {
				$qryRecover = "EXEC RECOVERY_MEMB '$memb_status', '$memberid', '$orderid'";
				//echo $qryRecover;
				$insExec = $this->executeQuery($qryRecover, $this->db1);
				$insSuccess = 0;
				if($insExec > 0) {
					$insSuccess = 1;

					$no = substr($orderid, 0, 2);
					if($no == "EM") {
						$this->load->model('webshop/ecomm_trans_model', 'ecomm_trans_model');
						$resultInsert = $this->ecomm_trans_model->getTrxSalesPlusReg($orderid);

						if($resultInsert != null) {
							//$this->paymentService->sendTrxSMS2($resultInsert);
							//$this->ecomm_trans_model->sendSMSPromoSalesAndReg($resultInsert);
							//sms ke member yg di daftarkan dan yg mendaftarkan
							$this->ecomm_trans_model->getRegMemberKnetPlus($resultInsert[0]->orderno);
							//$srvReturn = jsonTrueResponse(NULL, "SMS sudah dikirim");
						}
					} else if($no == "RM" || $no == "RC" || $no == "LP" || $no == "RA") {
						$this->load->model('webshop/member_model', 'member_model');
						$result = $this->member_model->show_member_new2($orderid);
						$upd = $this->member_model->sendTrxSmsRegMember($result);
					}

				}
				$arr = array("insSuccess" => 1,
							"orderid" => $orderid,
							"message" => "Data $orderid berhasil di reconcile..");
				return $arr;
			} else {
			    $paramX = $hasilCek[0]->dfno. " / ".$hasilCek[0]->fullnm;
				$arr = array("insSuccess" => 0,
						  "orderid" => $orderid,
						  "memberid" => $hasilCek,
						  "message" => "KTP sudah terdaftar dengan $paramX");
				return $arr;
			}

		} else {
			$arr = array("insSuccess" => 0,
						  "orderid" => $orderid,
						  "memberid" => null,
						  "message" => "Data tidak ada");
				return $arr;
		}
	}

	function getListBankCharge() {
		$qry = "SELECT * FROM ecomm_bank";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListActivePaymentType() {
		$qry = "SELECT * FROM ecomm_bank WHERE status = '1'";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListBankChargeByParam($param, $value) {
		$qry = "SELECT * FROM ecomm_bank WHERE $param = '$value'";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function saveBankCharge($data) {
		$qry = "INSERT ecomm_bank (bankCode,bankDesc,charge_connectivity,charge_admin,
						status,status_be,bankDisplayNm)
		        VALUES ('$data[bankCode]','$data[bankDesc]',
					".$data['charge_connectivity'].",
					".$data['charge_admin'].",
					'$data[status]', '$data[status_be]',
					'$data[bankDisplayNm]')";
	    //echo($qry);
	    $res = $this->executeQuery($qry);
		return $res;
	}

	function updateBankCharge($data) {
		$qry = "UPDATE ecomm_bank SET bankCode = '$data[bankCode]', bankDesc = '$data[bankDesc]',
					charge_connectivity = ".$data['charge_connectivity'].",
					charge_admin = ".$data['charge_admin'].",
					status = '$data[status]', status_be = '$data[status_be]',
					bankDisplayNm = '$data[bankDisplayNm]'
		        WHERE id = ".$data['id']."";
	    //echo($qry);
	    $res = $this->executeQuery($qry);
		return $res;
	}

    function getListDO($xx){
		$dtfrom = date("Y/m/d", strtotime($xx['from']));
		$dtto = date("Y/m/d", strtotime($xx['to']));
        if($xx['trx_type'] == 'stk' && $xx['destTo'] == 'stockist'){
            //$sentTo = '1';
            //$y = $xx['sentToStk'];
            $txt = "a.sentTo = '1' and a.idstk = '".$xx['sentToStk']."'";
        }else if($xx['trx_type'] == 'stk' && $xx['destTo'] == 'warehouse'){
            //$sentTo = '1';
            //$y = $xx['sentToWH'];
            $txt = "a.sentTo = '1' and a.whcd = '".$xx['sentToWH']."'";
        }
        else{
            //$sentTo = '2';
            //$y = $xx['sentToWH'];
            $txt = "a.sentTo = '2' and a.whcd = '".$xx['sentToWH']."'";
        }
        $slc = "SELECT a.orderno,a.CNno,a.KWno,sum(a.qty) as qty_prd,a.datetrans
                 FROM v_ecomm_list_for_do_gen a
                 where $txt
                 and CONVERT(VARCHAR(10), a.datetrans, 111) between '".$dtfrom."' and '".$dtto."'
                 and flagdo = '0'
                 group by a.orderno,a.CNno,a.KWno,a.datetrans";
        //echo $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;

    }

    function getGroupPrd($cek){
        $kw = "";
        for($i=0;$i < count($cek);$i++)
        {
            $kw .= "'".$cek[$i]."', ";
        }
            $kw = substr($kw, 0, -2);

        $slc = "select sum(a.qty) as qty_prd,a.prdcd,a.prdnm
                from V_ECOMM_LIST_FOR_DO_GEN a
                 where a.KWno in($kw)
                 group by a.prdcd,a.prdnm
                 order by a.prdcd";
        //echo $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function getDetGroup($cek){
        $kw = "";
        for($i=0;$i < count($cek);$i++)
        {
            $kw .= "'".$cek[$i]."', ";
        }
            $kw = substr($kw, 0, -2);

        $slc = " select a.KWno,a.qty as qty_prd,a.prdcd,a.prdnm
                 from V_ECOMM_LIST_FOR_DO_GEN a
                 where a.KWno in($kw)
                 order by a.prdcd";
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function getAddrDO($xx){
        if($xx['trx_type'] == "stk" && $xx['destTo'] == "stockist"){
            $slc = "select loccd as kode,fullnm as nama,addr1 as alamat1,
                    addr2 as alamat2,tel_hm,tel_hp
                    from klink_mlm2010.dbo.mssc where loccd = '".$xx['sentToStk']."'";
        }else{
            $slc = "select whcd as kode,description as nama,addr1 as alamat1,
                    addr2 as alamat2,tel_hm,tel_hp
                    from klink_mlm2010.dbo.mswh where whcd = '".$xx['sentToWH']."'";
        }

        //echo $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function getKurir(){
        $slc = "select code,description from tabcourier order by code";

        $result = $this->getRecordset($slc, null, $this->db2);
        return $result;
    }

    function cekSeq(){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
        $y1=date("Y");
        $m=date("m");

        $tbl = "ecomm_seq_do".$y1."".$m."";

        $cek = "select * from $tbl";

        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "<br>ada<br>";
        }
    }

    function getDONo(){
        $this->db = $this->load->database('db_ecommerce', true);

        $y1=date("Y");
        $m=date("m");

        $tbl = "ecomm_seq_do".$y1."".$m."";

        $sql = "SELECT * FROM $tbl
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";

       $query = $this->db->query($sql);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }
        }
        //$jumlah = $query->num_rows();

        $next_seq = sprintf("%07s",$ss);
        //$registerNo  = "GDO"."$next_seq";
        $registerNo  = "GDO".$y1."".$m."".$next_seq."";

        return $registerNo;
    }


    function generateDO($cek,$kurir){
        $conn1 = $this->load->database('klink_mlm2010', true);
        $conn2 = $this->load->database('db_ecommerce', true);
        $sequence = $this->cekSeq();
        $doNo = $this->getDONo();
        $abc = $this->session->userdata('group_do');

        $kw = "";
        $kw1 = "";
        for($i=0;$i < count($cek['cekArray']);$i++)
        {
            $kw .= "".$cek['cekArray'][$i].", ";
            $kw1 .= "'".$cek['cekArray'][$i]."', ";
        }
            $kw = substr($kw, 0, -2);
            $kw1 = substr($kw1, 0, -2);


        if($abc['trx_type'] == 'stk' && $abc['destTo'] == 'stockist' ){

            $ship_type = '0';
            $addrSentTo = "select addr1,addr2,addr3 from klink_mlm2010.dbo.mssc
                            where loccd = '".$abc['sentToStk']."'";
            $query = $conn1->query($addrSentTo);
            if($query->num_rows() > 0){
                foreach($query->result() as $data)
                {
                    $addr1 = $data->addr1;
                    $addr2 = $data->addr2;
                    $addr3 = $data->addr3;
                }
            }else{
                echo "no data";
            }
            $x = "values('".$doNo."','".$ship_type."','".$abc['sentToStk']."','".getBeUsername()."','".$kw."','".$kurir."','".getBeGroupname()."','".$addr1."','".$addr2."','".$addr3."')";

        }else{
            if($abc['trx_type'] == 'addr '){
                $ship_type = '1';
            }else{
                $ship_type = '0';
            }
            $addrSentTo = "select addr1,addr2,addr3
                            from klink_mlm2010.dbo.mswh
                            where whcd = '".$abc['sentToWH']."'";
            //echo $addrSentTo;
            $query = $conn1->query($addrSentTo);
            if($query->num_rows() > 0){
                foreach($query->result() as $data)
                {
                    $addr1 = $data->addr1;
                    $addr2 = $data->addr2;
                    $addr3 = $data->addr3;
                }
            }else{
                echo "no data";
            }
            $x = "values('".$doNo."','".$ship_type."','".$abc['sentToWH']."','".getBeUsername()."','".$kw."','".$kurir."','".getBeGroupname()."','".$addr1."','".$addr2."','".$addr3."')";
        }

        $insHdr = "insert into ecomm_do_hdr (trcd,ship_type,shipto,createnm,receiptno,shipby,whcd,addr1,addr2,addr3)
                    $x";
        //echo $insHdr;
        $resHdr = $this->executeQuery($insHdr, $this->setDB(1));

        $slcDet = "select sum(a.qty) as qty_prd,a.prdcd,a.prdnm
                     from V_ECOMM_LIST_FOR_DO_GEN a
                     where a.KWno in($kw1)
                     group by a.prdcd,a.prdnm
                     order by a.prdcd";
        //echo $slcDet;
        $query = $conn2->query($slcDet);
        if($query->num_rows() > 0){
            foreach($query->result() as $data)
            {
                $insDet = "insert into ecomm_do_det (trcd,prdcd,qtyord,qtyship,qtybo)
                            values('".$doNo."','".$data->prdcd."',".$data->qty_prd.",".$data->qty_prd.",0)";

                //echo $insDet;
               $resDet = $this->executeQuery($insDet, $this->setDB(1));
            }
        }

        $updTransHdr = "update ecomm_trans_hdr set DOno = '".$doNo."'
                        where KWno in ($kw1)";
        $qryUpdt =  $this->executeQuery($updTransHdr, $this->setDB(1));

        if(!$resHdr && !$resDet && !$qryUpdt){
            $arr = array('response' => 0);
                return $arr;
        }else{
            $arr = array('response' => 1,
                             'DOno' => $doNo);
                return $arr;
        }
    }

    function getStkList1(){
        $qry = "select distinct a.idstk,b.fullnm,b.addr1,b.addr2,b.tel_hm,b.tel_hp
                 from V_ECOMM_LIST_FOR_DO_GEN a
                 left join klink_mlm2010.dbo.mssc b on (a.idstk = b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS)
                 order by a.idstk asc";
         $res = $this->getRecordset($qry);
		return $res;
    }

    function getHdrDO($dono,$abc){

        if($abc['trx_type'] == 'stk' && $abc['destTo'] == 'stockist' ){
            $y = "b.fullnm as desc_addr,b.tel_hp as telp";
            $x = "left join klink_mlm2010.dbo.mssc b on(a.shipto = b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS)";
        }else{
            $y = "b.tel_hp as telp,b.description as desc_addr";
            $x = "left join klink_mlm2010.dbo.mswh b on(a.shipto = b.whcd COLLATE SQL_Latin1_General_CP1_CS_AS)";
        }
        $slc = "select a.trcd,a.shipto,a.shipby,a.addr1,a.addr2,a.addr3,a.whcd,a.createdt,$y
                from ecomm_do_hdr a
                $x
                where a.trcd = '".$dono."'";
        //echo $slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

    function getHdrDO1($dono,$shipto){
        $shiptoStr = substr($shipto,0,2);
        if($shiptoStr == 'WH'){
            $y = "b.tel_hp as telp,b.description as desc_addr";
            $x = "left join klink_mlm2010.dbo.mswh b on(a.shipto = b.whcd COLLATE SQL_Latin1_General_CP1_CS_AS)";

        }else{
            $y = "b.fullnm as desc_addr,b.tel_hp as telp";
            $x = "left join klink_mlm2010.dbo.mssc b on(a.shipto = b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS)";
        }
        $slc = "select a.trcd,a.shipto,a.shipby,a.addr1,a.addr2,a.addr3,a.whcd,a.createdt,$y
                from ecomm_do_hdr a
                $x
                where a.trcd = '".$dono."'";
        //echo $slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

    function getDetDO($dono){
        $slc = " select a.trcd,a.prdcd,b.cat_inv_desc as prdnm,sum(a.qtyord) as qty,sum(a.qtyship) as ship,sum(a.qtybo) as bo
                 from ecomm_do_det a
                 LEFT JOIN master_prd_cat_inv b ON (a.prdcd=b.parent_cat_inv_id COLLATE SQL_Latin1_General_CP1_CS_AS)
                 where a.trcd = '".$dono."'
                 group by a.trcd,a.prdcd,b.cat_inv_desc";
        //echo $slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

    function getCountPRDDO($dono){
        $slc = "  select count(a.prdcd) as jum
                  from ecomm_do_det a
                  where a.trcd = '".$dono."'";
        $res = $this->getRecordset($slc);
		return $res;
    }

    function getListGroupDO($x){
		$dtfrom = date("Y/m/d", strtotime($xx['from']));
		$dtto = date("Y/m/d", strtotime($xx['to']));
        $slc = "select a.trcd,a.createdt,a.createnm,a.shipto,a.shipby,a.whcd
                 from ecomm_do_hdr a
                 where (CONVERT(VARCHAR(10), createdt, 111) between '".$dtfrom."' and '".$dtto."')";
        $res = $this->getRecordset($slc);
		return $res;
    }

    function getListNoConot($x){
        $slc = "select a.orderno,a.id_memb,a.total_pay,a.datetrans,
                   a.total_Items,a.total_weight,a.servicetypeID,a.token, a.conote_new, a.cargo_id,
				   a.orderno_ship
                from V_ECOMM_LIST_CONOTE_FAILED a
                where (CONVERT(VARCHAR(10), a.datetrans
                , 111) between '".$x['dtfroms']."' and '".$x['dttos']."') ";
		//echo $slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

		function getEmptyListConnote($from, $to) {
			$query = "SELECT a.orderno, a.token, a2.fullnm + ' - '+ a2.tel_hp as no_hp_konfirmasi
								FROM db_ecommerce.dbo.ecomm_trans_hdr a
								LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_hdr_sgo a1 ON (a.token = a1.orderno)
								LEFT OUTER JOIN klink_mlm2010.dbo.msmemb a2 ON (a1.userlogin COLLATE SQL_Latin1_General_CP1_CS_AS = a2.dfno)
								LEFT OUTER JOIN ecomm_trans_shipaddr c ON (a.orderno = c.orderno)
								LEFT OUTER JOIN ecomm_trans_shipaddr_sgo b ON (a.token = b.orderno)
								WHERE b.orderno IS NULL AND a.sentTo = '2' AND (c.conoteJNE IS NULL OR c.conoteJNE = '')
								AND (CONVERT(VARCHAR(10), a.datetrans, 111)
								BETWEEN '$from' AND '$to')";
			$result = $this->getRecordset($query);
			return $result;
		}
	function getListNoConotBaru($x) {
		$slc = "SELECT a.orderno, a.token, a.id_memb,
				   a.datetrans, c.receiver_name, a.total_pay, c.total_weight,
				   a.conote_new, c.conoteJNE, c.cargo_id, c.service_type_id as servicetypeID,
					c.orderno as orderno_ship
				FROM ecomm_trans_hdr a
				LEFT OUTER join ecomm_trans_shipaddr c on a.orderno=c.orderno
				where (CONVERT(VARCHAR(10), a.datetrans , 111) between '".$x['dtfroms']."' and '".$x['dttos']."')
				and (c.conoteJNE is null or c.conoteJNE='' )
				and a.sentTo='2'";
		//echo $slc;
		$res = $this->getRecordset($slc);
		return $res;
	}

    function getListNullResiFisik($x){
    	$qry = "SELECT a.orderno, a.conoteJNE, a.datetrans2, kota_nama_kgp, wh_name
    	        FROM V_ECOM_LIST_PICKUP_READY_HDR A
				WHERE (A.conote_new = '' OR A.conote_new is null)
				AND (CONVERT(VARCHAR(10), a.datetrans, 111) between '".$x['dtfroms']."' and '".$x['dttos']."')
				and a.sentTo = '2'";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
    }

    function getListPengirimanWH($x) {
    	$qry = "SELECT a.orderno, a.conoteJNE, a.datetrans2, kota_nama_kgp, wh_name
    	        FROM V_ECOM_LIST_PICKUP_READY_HDR A
				WHERE (CONVERT(VARCHAR(10), a.datetrans, 111) between '".$x['dtfroms']."' and '".$x['dttos']."')
				and a.sentTo = '2'";
	    //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
    }

    function getDataFailedConot($orderno){
        $slc = "select * from V_ECOMM_LIST_CONOTE_FAILED where orderno = '".$orderno."'";
        $res = $this->getRecordset($slc);
		return $res;
    }


	function getShipAddrSgo($orderno) {
		$slc = "select * from ecomm_trans_shipaddr_sgo where orderno = '".$orderno."'";
        $res = $this->getRecordset($slc);
		return $res;
	}

	function insShipAddrFromSGO($orderno, $token) {

	}

    function getDataFailedConot1($orderno,$token){
    	$slc = "select orderno from ecomm_trans_shipaddr where orderno = '".$orderno."'";
        $res = $this->getRecordset($slc);
    	if($res == null) {
	        $ins = "insert into ecomm_trans_shipaddr(
	                orderno,
	                idstk,
	                prov_code,
	                kab_code,
	                kec_code,
	                receiver_name,
	                addr1,
	                email,
	                tel_hp1,
	                service_type_id,
	                service_type_name,
	                flag_send_conote,
	                total_item, total_weight, cargo_id)
	                select '".$orderno."',
	                	   a.idstk, a.prov_code, a.kab_code, a.kec_code,
	                       a.receiver_name, a.addr1, a.email, a.tel_hp1,
	                       a.service_type_id, a.service_type_name,
	                       a.flag_send_conote, a.total_item, a.total_weight, a.cargo_id
	                from ecomm_trans_shipaddr_sgo a
	                where a.orderno='".$token."'";
	        $res = $this->executeQuery($ins);
	        if(!$res){
	            $arr = array('response' => 0);
	                return $arr;
	        }else{
	            $arr = array('response' => 1);
	                return $arr;
	        }
	    } else {
	    	 $arr = array('response' => 1);
	                return $arr;
	    }
    }

    function getReportSgo($dt){
        /*$slc = "select * from report_download_sgo
                where (CONVERT(VARCHAR(10), Created, 111) between '".$dt['from']."' and '".$dt['to']."')
                order by status";
		 *
		 * 	            		<option value="0">All</option>
	            		<option value='1'>Success</option>
	            		<option value='2'>Suspended</option>
	            		<option value='3'>Failed</option>
		 */

		 $status = $dt['status'];
		if($status == 0){
			$paramStatus = " and A.status <> 'Failed'";
		}elseif($status == 1){//Success
			$paramStatus = " and A.status = 'Success' ";
		}elseif($status == 2){//Suspend
			$paramStatus = " and A.status = 'Suspect' ";
		}elseif($status == 3){ // Failed
			$paramStatus = " and A.status = 'Failed' ";
		}

		$dtFrom = date("Y/m/d", strtotime($dt['from']));
		$dtTo = date("Y/m/d", strtotime($dt['to']));
		/*$slc = "select * from V_ECOMM_LIST_SGO_SALES_RPT A
				where (CONVERT(VARCHAR(10), A.Created, 111) between '$dtFrom' and '$dtTo') $paramStatus
                order by A.Created DESC, A.status";*/

		$slc = "select a.* from V_HILAL_REPORT_SGO A
			    where (CONVERT(VARCHAR(10), A.Created, 111)
	            between '$dtFrom' and '$dtTo') $paramStatus";
        $res = $this->getRecordset($slc);
		//echo $slc;
		return $res;
    }

	function getReportSgo3($dt) {
		$status = $dt['status'];
		if($status == 0){
			$paramStatus = " and a.status <> 'Failed'";
		}elseif($status == 1){//Success
			$paramStatus = " and a.status = 'Success' ";
		}elseif($status == 2){//Suspend
			$paramStatus = " and a.status = 'Suspect' ";
		}elseif($status == 3){ // Failed
			$paramStatus = " and a.status = 'Failed' ";
		}

		$dtFrom = date("Y/m/d", strtotime($dt['from']));
		$dtTo = date("Y/m/d", strtotime($dt['to']));
		$listBank = $dt['listBank'];
		$paramListBank = "";
		if($listBank != "") { // Failed
			$paramListBank = " and a.bank_master = '$listBank'";
		}

		//print_r($listBank);

		if($listBank != '11' && $listBank !='12' && $listBank !='5'){ //list bank dari sgo
			$slc = "select a.* from V_HILAL_REPORT_SGO A
				    where A.Order_ID NOT like 'TA%' AND (CONVERT(VARCHAR(10), A.Created, 111)
		            between '$dtFrom' and '$dtTo') $paramStatus $paramListBank";

			$slc = "SELECT
						XX.Order_ID, XX.Product_Value, XX.Transaction_ID,
						XX.Transaction_Date, XX.Bank_Product, XX.id_memb,
						XX.nmmember,
						XX.bankCode,
						XX.CNno, XX.KWno, XX.IPno,
						XX.Amount, XX.total_pay, XX.payAdm, XX.payShip, XX.payamt,
						XX.P_VOUCHER, XX.selisihCustPrice, XX.tipeLogin
					FROM (
					SELECT
						a.Order_ID, a.Product_Value, a.Transaction_ID,
						a.Transaction_Date, a.Bank_Product,
						CASE WHEN a.Order_ID is null OR a.Order_ID = ''
						  THEN b.member_id ELSE a.id_memb
						END AS id_memb,
						CASE WHEN a.Order_ID is null OR a.Order_ID = ''
						  THEN b.member_cust_name ELSE a.nmmember
						END AS nmmember,
						a.bankCode,
						a.CNno, a.KWno, a.IPno,
						a.Amount,
						CASE WHEN a.Order_ID is null OR a.Order_ID = ''
						  THEN b.amount	ELSE a.total_pay
						END AS total_pay,
						CASE WHEN a.Order_ID is null OR a.Order_ID = ''
							THEN b.total_amount - b.amount ELSE a.payAdm
						END AS payAdm,
						a.payShip, a.payamt, a.P_VOUCHER, a.selisihCustPrice, a.tipeLogin
					FROM V_HILAL_REPORT_SGO a
					LEFT OUTER JOIN va_sgo_paynotif_req b ON (a.Product_Value = b.product_value)
					WHERE a.Order_ID NOT like 'TA%'
						AND (CONVERT(VARCHAR(10), a.Created, 111) BETWEEN '$dtFrom' and '$dtTo')
						$paramStatus $paramListBank
					) XX
					GROUP BY
					  XX.Order_ID, XX.Product_Value, XX.Transaction_ID,
						XX.Transaction_Date, XX.Bank_Product, XX.id_memb,
						XX.nmmember,
						XX.bankCode,
						XX.CNno, XX.KWno, XX.IPno,
						XX.Amount, XX.total_pay, XX.payAdm, XX.payShip, XX.payamt,
						XX.P_VOUCHER, XX.selisihCustPrice, XX.tipeLogin";
		}else if($listBank == '11' || $listBank =='12'){ //list bank dari sgo
			$slc = "SELECT Order_ID, Product_Value, Transaction_ID, Transaction_Date, Bank_Product,
							id_memb, nmmember, bankCode_master, bankCode, CNno, KWno, IPno, Amount, total_pay, payAdm,
							payShip, payamt, P_VOUCHER, selisihCustPrice, tipeLogin
					FROM V_HILAL_REPORT_Transfer_manual a
					WHERE a.bankCode_master = '$listBank'
       		  AND (CONVERT(VARCHAR(10), a.Transaction_Date, 111) BETWEEN '$dtFrom' and '$dtTo') ";
		}
		else if($listBank == '5'){ //list bank transaksi dengan K-wallet
			$slc = "SELECT Order_ID, Product_Value, Transaction_ID, Transaction_Date, Bank_Product,
							id_memb, nmmember, bankCode_master, bankCode, CNno, KWno, IPno, Amount, total_pay, payAdm,
							payShip, payamt, P_VOUCHER, selisihCustPrice, tipeLogin
					FROM V_HILAL_REPORT_Trans_Kwallet a
					WHERE a.bankCode_master = '$listBank'
       		  AND (CONVERT(VARCHAR(10), a.Transaction_Date, 111) BETWEEN '$dtFrom' and '$dtTo') ";
		}
		//echo $slc; //, A.Product_Value
		$res = $this->getRecordset($slc);
		//echo $slc; //, A.Product_Value
		return $res;
	}

	function getReportSgo2($dt){
		/*$slc = "select * from report_download_sgo
                where (CONVERT(VARCHAR(10), Created, 111) between '".$dt['from']."' and '".$dt['to']."')
                order by status";
         *
         * 	            		<option value="0">All</option>
                        <option value='1'>Success</option>
                        <option value='2'>Suspended</option>
                        <option value='3'>Failed</option>
         */

		$status = $dt['status'];
		if($status == 0){
			$paramStatus = " and A.status <> 'Failed'";
		}elseif($status == 1){//Success
			$paramStatus = " and A.status = 'Success' ";
		}elseif($status == 2){//Suspend
			$paramStatus = " and A.status = 'Suspect' ";
		}elseif($status == 3){ // Failed
			$paramStatus = " and A.status = 'Failed' ";
		}

		$dtFrom = date("Y/m/d", strtotime($dt['from']));
		$dtTo = date("Y/m/d", strtotime($dt['to']));
		$listBank = $dt['listBank'];
		$paramListBank = "";
		if($listBank != "") { // Failed
			$paramListBank = " and A.bank_master = '$listBank'";
		}
		/*$slc = "select * from V_ECOMM_LIST_SGO_SALES_RPT A
				where (CONVERT(VARCHAR(10), A.Created, 111) between '$dtFrom' and '$dtTo') $paramStatus
                order by A.Created DESC, A.status";*/

		/*
		 $slc = "select a.* from V_HILAL_REPORT_SGO A
			    where (CONVERT(VARCHAR(10), A.Created, 111)
	            between '$dtFrom' and '$dtTo') $paramStatus and A.bankCode = '$listBank'";
		 */
		 $slc = "select a.* from V_HILAL_REPORT_SGO A
			    where A.Order_ID NOT like 'TA%' AND (CONVERT(VARCHAR(10), A.Created, 111)
	            between '$dtFrom' and '$dtTo') $paramStatus $paramListBank";
		 $res = $this->getRecordset($slc);
		//echo $slc; //, A.Product_Value
		return $res;
	}

	function getReportSgoticket($dt) {
		$status = $dt['status'];
		if($status == 0){
			$paramStatus = " and A.status <> 'Failed'";
		}elseif($status == 1){//Success
			$paramStatus = " and A.status = 'Success' ";
		}elseif($status == 2){//Suspend
			$paramStatus = " and A.status = 'Suspect' ";
		}elseif($status == 3){ // Failed
			$paramStatus = " and A.status = 'Failed' ";
		}

		$dtFrom = date("Y/m/d", strtotime($dt['from']));
		$dtTo = date("Y/m/d", strtotime($dt['to']));
		$listBank = $dt['listBank'];
		$paramListBank = "";
		if($listBank != "") { // Failed
			$paramListBank = " and A.bank_master = '$listBank'";
		}
		/*$slc = "select * from V_ECOMM_LIST_SGO_SALES_RPT A
				where (CONVERT(VARCHAR(10), A.Created, 111) between '$dtFrom' and '$dtTo') $paramStatus
                order by A.Created DESC, A.status";*/

		/*
		 $slc = "select a.* from V_HILAL_REPORT_SGO A
			    where (CONVERT(VARCHAR(10), A.Created, 111)
	            between '$dtFrom' and '$dtTo') $paramStatus and A.bankCode = '$listBank'";
		 */
		 $slc = "select a.*
		         from V_HILAL_REPORT_SGO A
			     where A.Order_ID like 'TA%' and (CONVERT(VARCHAR(10), A.Created, 111)
	             between '$dtFrom' and '$dtTo') $paramStatus $paramListBank";
		 //echo $slc;

		 $slc = "select
					A.Product_Value,
					A.Transaction_ID,
					A.Order_ID,
					A.Transaction_Date,
					A.Bank_Product,
					A.CNno,
					A.KWno,
					A.IPno,
					A.id_memb,
					A.nmmember,
					A.bank_code_payment,
					A.total_pay,
					A.payAdm,
					A.bankCode,
					sum(b.price) as harga_tiket,
					c.etiket_code_id,
					c.nama + ' - ' + c.lokasi + ' - ' + CONVERT(VARCHAR(10), c.event_date, 111) as acara
					from V_HILAL_REPORT_SGO A
					inner join db_ecommerce.dbo.trx_etiket b ON (a.Order_ID = b.order_no)
					inner join db_ecommerce.dbo.master_etiket c ON (b.id_etiket = c.id)
					where A.Order_ID like 'TA%' and (CONVERT(VARCHAR(10), A.Created, 111)
	                 between '$dtFrom' and '$dtTo') $paramStatus $paramListBank
					group by
					 A.Product_Value,
					A.Transaction_ID,
					A.Order_ID,
					A.Transaction_Date,
					A.Bank_Product,
					A.CNno,
					A.KWno,
					A.IPno,
					A.id_memb,
					A.nmmember,
					A.bank_code_payment,
					A.total_pay,
					A.payAdm,
					A.bankCode,
					c.etiket_code_id,
					c.nama,
					c.lokasi,
					c.event_date
					";
		$res = $this->getRecordset($slc);
		//echo $slc; //, A.Product_Value
		return $res;
	}

	function getReportSgoTopUpKwallet($dt) {

		$status = $dt['status'];
		if($status == 0){
			$paramStatus = " and A.status <> 'Failed'";
		}elseif($status == 1){//Success
			$paramStatus = " and A.status = 'Success' ";
		}elseif($status == 2){//Suspend
			$paramStatus = " and A.status = 'Suspect' ";
		}elseif($status == 3){ // Failed
			$paramStatus = " and A.status = 'Failed' ";
		}

		$dtFrom = date("Y/m/d", strtotime($dt['from']));
		$dtTo = date("Y/m/d", strtotime($dt['to']));
		$listBank = $dt['listBank'];
		$paramListBank = "";
		if($listBank != "") { // Failed
			$paramListBank = " and d.id = '$listBank'";
		}


		$whereTopup = "";
		if ($listBank == 5) { //k-wallet
			if($dt['tipe_trx'] == "2") { //top up k-wallet
				$whereTopup = " and a.trtype = '01' ";
			}
			$qryKwallet = "select  a.trcd as Order_ID, a.trcd as Transaction_ID, a.novac as product_value,
							      	a.amount, a.amount as total_amount, a.id, a.dfno as member_id, b.fullnm as member_cust_name, b.banknm as bankDisplayNm,
								    a.trdt as Transaction_Date, a.etdt as Created, CONVERT(VARCHAR(20), a.trdt, 121) as payment_datetime
							from va_cust_pay_det a
								 inner join klink_mlm2010.dbo.msmemb b on a.dfno=b.dfno
							where a.etdt between '$dtFrom' and '$dtTo' $whereTopup
						  ORDER BY a.trcd";
			//echo $qryKwallet;
			$res = $this->getRecordset($qryKwallet);
		}

		/*else{
			$qry = "SELECT a.Order_ID, a.Transaction_ID, b.product_value,
					b.amount,b.total_amount, d.id, b.member_id, b.member_cust_name, d.bankDisplayNm,
					a.Transaction_Date, a.Created, CONVERT(VARCHAR(20), b.payment_datetime, 121) as payment_datetime
					FROM va_sgo_paynotif_req b
					LEFT OUTER JOIN REPORT_DOWNLOAD_SGO a
					   ON (a.Transaction_ID = b.payment_ref)
					LEFT OUTER JOIN ecomm_bank c ON (b.debit_from_bank = c.bankCode)
					INNER JOIN ecomm_bank_master d ON (c.bankcode_ip = d.bankcode_ip)
					WHERE a.Community = 'TOP UP KLINK' and (CONVERT(VARCHAR(10), b.payment_datetime, 111)
		                 between '$dtFrom' and '$dtTo') $paramStatus $paramListBank
					GROUP BY
					 a.Order_ID, a.Transaction_ID, b.product_value,
					b.amount, b.total_amount, a.Created, d.id, b.member_id, b.member_cust_name,
					d.bankDisplayNm, a.Transaction_Date, a.Created, b.payment_datetime
					ORDER BY a.Transaction_ID";
			//echo $qry;
			$res = $this->getRecordset($qry);
		}
		 */
		//echo $slc; //, A.Product_Value
		return $res;
	}

    function getReportSgoCsv($dt){
        //$this->load->dbutil();
        //$delimiter = ";";
        //$newline = "\r\n";
		$dtfrom = date("Y/m/d", strtotime($dt['from']));
		$dtto = date("Y/m/d", strtotime($dt['to']));

        $query1 = "select * from report_download_sgo
                where (CONVERT(VARCHAR(10), Created, 111) between '".$dtfrom."' and '".$dtto."')
                order by status";
        //$result1 = $this->db->query($query1);
        $res = $this->getRecordset($query1);
		return $res;

        //$data1 = $this->dbutil->csv_from_result($result1, $delimiter, $newline);


        //return $data1;
    }

    function getTotalSgo($dt){
        $query1 = " select a.[Status], a.Bank_Product, sum(a.Amount) as Amount
                     from REPORT_DOWNLOAD_SGO a
                     where (CONVERT(VARCHAR(10), Created, 111) between '".$dt['from']."' and '".$dt['to']."')
                     group by a.[Status], a.Bank_Product
                     order by a.[Status]";

        $res = $this->getRecordset($query1);
		return $res;
    }

    function getGoodReceiptStatus($orderno) {
        $slc = "SELECT a.orderno, a.secno, a.status,
                 CONVERT(VARCHAR(10), b.tgl_ambil, 103) as tgl_ambil,
                 CONVERT(VARCHAR(10), b.tgl_ambil, 108) as jam_ambil,
                b.userlogin
				FROM webol_trans_ok a
				LEFT JOIN webol_logs_stockist b
				   ON(a.orderno = b.orderno)
				WHERE a.orderno = '$orderno'";

        $result = $this->getRecordset($slc, null, $this->db2);
        return $result;
    }


   /*===============TEST HILAL========================*/
   	function getConfigEmail($param) {
		$qry = "select * FROM ecomm_mail_listing a where a.status='1' and a.flag_jobs='$param'";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListVoucherByParam($searchby, $param1, $param2=null, $param3=null) {
		if($searchby == 'stockist'){
			$qry1 = " a.loccd = '$param1' AND (CONVERT(VARCHAR(10), a.etdt, 111) BETWEEN '$param2' AND '$param3')";
		}elseif($searchby == 'vocuherno'){
			$qry1 = " a.VoucherNo = '$param1' ";
		}elseif($searchby == 'ip'){
			$qry1 = " a.IPNO = '$param1' ";
		}
		$qry = "SELECT A.trcd, A.batchno, A.CNNO, A.VoucherNo, A.IPNO, A.paytype_SC, A.paytype_CN,
					   A.loccd, A.etdt, A.payamt_SC, A.payamt_CN, A.dfno, A.fullnm, A.tbv, A.tdp,
					   A.claimstatus, A.ExpireDate, A.SCNM
	   			 FROM V_HILAL_VOUCHER_CHECK a
	   			 WHERE $qry1" ;
		/*
		$qry = " SELECT a.VoucherNo, a.claimstatus, a.IssueDate, a.ExpireDate, a.BonusMonth, a.BonusYear,
					   a.updatedt, b.trcd, c.etdt, c.loccd, d.fullnm, c.createnm, A.DistributorCode, E.fullnm AS distname
				FROM tcvoucher a
					 LEFT OUTER JOIN sc_newtrp b on a.VoucherNo=b.docno and b.voucher='1'
				     LEFT OUTER JOIN sc_newtrh c on b.trcd=c.trcd
				     LEFT OUTER JOIN mssc d on c.loccd=d.loccd
				     LEFT OUTER JOIN msmemb e ON A.DistributorCode=E.dfno
				 WHERE $qry1 ";
		 */
	   	//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getCheckConoteIsNULL($orderno) {
		//echo $orderno;
		$orderno = explode("**", $orderno);
		$orderno = $orderno[0];
		$conote_new = $orderno[1];

		$qry = "SELECT A.conote_new, A.orderno
				FROM ecomm_trans_hdr A
				WHERE A.orderno='$orderno'"; // AND (conote_new IS NULL OR conote_new = '')";
        //echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function updateCnoteByOrderno($data) {
		$qry = "UPDATE ecomm_trans_hdr
				SET conote_new = '$data[conoteno]', delivery_status='3'
		        WHERE orderno = '$data[orderno]'";
	    //echo($qry);
	    $res = $this->executeQuery($qry);
		return $res;
	}

    function show_product_price($prdcdcode, $pricecode){
         $qry = "SELECT  b.prdcd,b.prdnm,c.bv,c.dp
                from klink_mlm2010.dbo.pricetab c
                     LEFT JOIN klink_mlm2010.dbo.msprd b on c.prdcd=b.prdcd
                where c.pricecode='$pricecode' and
                      c.prdcd='$prdcdcode' and b.webstatus='1' and b.status='1'";
         //echo $qry;
        $res = $this->getRecordset($qry, null, $this->db2);
		return $res;
    }

	function getListBank() {
		$qry = "SELECT A.id, A.bankCode, A.bankDesc, A.charge_connectivity,
					   A.charge_admin, A.[status], A.bankDisplayNm, A.status_be
				FROM ecomm_bank A
				ORDER BY a.[status] desc, A.id ";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListBankMaster() {
		$qry = "SELECT A.id, A.bankCode, A.bankDesc, A.charge_connectivity,
					   A.charge_admin, A.[status], A.bankDisplayNm, A.status_be
				FROM ecomm_bank_master A
				ORDER BY a.[status] desc, A.id ";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListMerchantBank() {
		$qry = "SELECT A.id, A.bankCode, A.bankDesc, A.charge_connectivity,
					   A.charge_admin, A.[status], A.bankDisplayNm, A.status_be
				FROM ecomm_bank A
				WHERE A.status = '1'
				ORDER BY a.[status] desc, A.id ";
		$res = $this->getRecordset($qry);
		return $res;
	}
   /*===============END TEST HILAL=====================*/

    function getListTrxPending($data)
    {
        $tambahan = " ";
		$tambahan2 = " ";
		$sentTo = " ";
		$bnsmonth = " ";
		$dtfrom = date("Y/m/d", strtotime($data['trx_froms']));
		$dtto = date("Y/m/d", strtotime($data['trx_tos']));
		if(($data['trx_froms'] != "" || $data['trx_tos'] != "") && $data['bnsmonth'] == "") {
			$where = " CONVERT(VARCHAR(10), a.datetrans, 111) between '$dtfrom' and '$dtto' ";
		} else if(($data['trx_froms'] != "" || $data['trx_tos'] != "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			//$where = "bonusmonth = '$bns' ";
			$where = " CONVERT(VARCHAR(10), a.datetrans, 111) between '$dtfrom' and '$dtto' AND bonusmonth = '$bns'";
		} else if(($data['trx_froms'] == "" || $data['trx_tos'] == "") && $data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];

			$where = "bonusmonth = '$bns'";
		}

		if($data['searchBy'] != "") {
			if($data['searchBy'] == "id_memb") {
				$ss = "a.id_memb";
			} else if($data['searchBy'] == "idstk") {
				$ss = "a.idstk";
			} else if($data['searchBy'] == "log_usrlogin") {
				$ss = "a.log_usrlogin";
			}
			$tambahan = " AND $ss = '$data[paramValue]'";
		}

		if($data['trx_type'] != "") {
			$tambahan2 = " AND a.orderno LIKE '$data[trx_type]%'";
		}

		if($data['sentTo'] != "") {
			$sentTo = " AND a.sentTo = '$data[sentTo]'";
		}

		if($data['bnsmonth'] != "") {
			$bns = $data['bnsmonth']."/".$data['bnsyear'];
			$bnsmonth = " AND a.bonusmonth = '$bns'";
		}
 		$slc = "select a.orderno,a.token,a.id_memb,a.nmmember,a.idstk,
                a.datetrans,a.total_pay,a.total_bv,sum(b.qty) as qty,c.CNstatus
                from ecomm_trans_hdr_sgo a
                inner join ecomm_trans_det_prd_sgo b on (a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS)
                left join klink_mlm2010.dbo.webol_trans_ok c on (a.token = c.token COLLATE SQL_Latin1_General_CP1_CS_AS)
                where $where $tambahan $tambahan2 $sentTo AND c.CNstatus is null
                group by a.orderno,a.token,a.id_memb,a.nmmember,a.idstk,a.datetrans,a.total_pay,a.total_bv,c.CNstatus
                order by a.datetrans,a.id_memb,c.CNstatus";
        //echo "ss ".$slc;
        $res = $this->getRecordset($slc, null, $this->db1);
		return $res;
    }

    function get_TrxPendingHeader($orderno){
        $slc = "select a.orderno,a.total_pay,a.total_bv,a.payShip,a.payAdm,
                a.id_memb,a.payConnectivity,a.userlogin,a.datetrans,a.idstk,a.nmstkk,a.bonusmonth,
                 b.CNno,b.REGISTERno,b.KWno,b.IPno,c.fullnm as member,d.tel_hp
                from ecomm_trans_hdr_sgo a
                left join ecomm_trans_hdr b on (a.orderno = b.token COLLATE SQL_Latin1_General_CP1_CS_AS)
                inner join klink_mlm2010.dbo.msmemb c on (a.id_memb = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                left join klink_mlm2010.dbo.msmemb d on (a.userlogin = d.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                where a.orderno = '".$orderno."'";
        //echo "query ".$slc;
        $res = $this->getRecordset($slc, null, $this->db1);
		return $res;
    }

    function get_TrxPendingDetail($orderno){
        $slc = "select * from ecomm_trans_det_prd_sgo a where a.orderno = '".$orderno."'";
        $res = $this->getRecordset($slc);
		return $res;
    }

    function get_TrxPendingPayment($orderno){
        $slc = "SELECT a.paytype, a.docno, a.payamt, d.notes,
				    a.bank_code_payment, c.bankDisplayNm , b.token, a.charge_admin
    				FROM ecomm_trans_paydet_sgo a
    				LEFT JOIN ecomm_trans_hdr b ON (a.orderno = b.token COLLATE SQL_Latin1_General_CP1_CI_AS)
    				LEFT JOIN ecomm_bank c ON (a.bank_code_payment = c.id )
                    left join ecomm_trans_paydet d on (b.orderno = d.orderno COLLATE SQL_Latin1_General_CP1_CI_AS)
   				WHERE A.orderno = '".$orderno."'";
		//echo "query ".$slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

    function getShipInvList($from, $to, $list_shipping) {
		 //$cargo_name = $data['list_shipping'];
		 //echo "$cargo_name";
		 $cargo ="";
		 if($list_shipping == ""){
		 	$cargo = $cargo;
		 }else{
		 	$cargo = " AND B.cargo_id = '$list_shipping' ";
		 }
    	 $slc = "Select CONVERT(VARCHAR(10), a.datetrans, 111) as datetrans, a.orderno, a.id_memb, a.nmmember, a.nmstkk,
				       a.is_ship, a.free_shipping, a.total_bv, a.total_pay, a.payAdm, a.payShip,
				       b.conoteJNE, isnull(a.conote_new, b.conoteJNE) as conote_new, b.receiver_name, B.cargo_id, C.shipper_name,
				       b.service_type_name, b.addr1, b.addr2, b.addr3, b.total_weight
				from ecomm_trans_hdr a
				     inner join ecomm_trans_shipaddr b on a.orderno=b.orderno
     				 LEFT OUTER JOIN master_logistic_shipper C ON B.cargo_id=C.shipper_id
				where CONVERT(VARCHAR(10), a.datetrans, 126) between '$from' and '$to' $cargo
				order by B.cargo_id, a.datetrans";
		//echo "query ".$slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

	function getShipInvListByID($id) {
    	 $slc = "Select a.datetrans, a.orderno, a.id_memb, a.nmmember, a.nmstkk,
				       a.is_ship, a.free_shipping, a.total_bv, a.total_pay, a.payAdm, a.payShip,
				       b.conoteJNE, a.conote_new, b.receiver_name,
				       b.service_type_name, b.addr1, b.addr2, b.addr3, b.total_weight
				from ecomm_trans_hdr a
				     inner join ecomm_trans_shipaddr b on a.orderno=b.orderno
				where a.orderno = '$id'
				order by a.datetrans";
		//echo "query ".$slc;
        $res = $this->getRecordset($slc);
		return $res;
    }

    function personalbv($month,$year){
		$slc = "select top 2 a.dfno, a.fullnm, a.email, ISNULL(B.level, 0) AS level,
                ISNULL(B.ppv, 0) AS ppv,ISNULL(B.popv, 0) AS popv,
                ISNULL(B.pgpv, 0) AS pgpv, ISNULL(B.sfno, 0) AS sfno,
                ISNULL(year(B.period), 2016) as year, ISNULL(month(B.period), 9) as month
                from msmemb a
                 INNER join hsttree b on a.dfno=b.dfno
                where year(b.period)= ".$year." and MONTH(b.period)=".$month."
                and a.email not in('<div ','a@NK.com','2','NK@A.COM','....','....','your@email.com','003','00','I','00000000000','BASRI@','@gmail.com','NK@T.com','NK@T.COM','NK@t.COM','a@a.com','-','0000000','000000','online@k-link.co.id')
                and (a.email != '' AND a.email is not NULL)
                and a.fullnm != 'TERMINATION'";

        //echo "pbv ".$slc;
        $res = $this->getRecordset($slc, null, $this->db2);
        return $res;
    }

    function randomsData(){
        $slc = "select top 100 a.dfno,a.fullnm from msmemb a where a.[state] = 'JK'
                AND a.fullnm not in('TERMINATION',
                'MERCHANT 1',
                'MERCHANT 2',
                'MERCHANT 3',
                'MERCHANT 4',
                'MERCHANT 5',
                'MERCHANT 6',
                'MERCHANT 7',
                'MERCHANT 8',
                'MERCHANT 9',
                'MERCHANT 10',
                'RESIGNATION') and a.fullnm not like 'TRANSFER NAME%'
                ";
        $res = $this->getRecordset($slc, null, $this->db2);
        return $res;
    }

	public function getPPOBType($id){
		$this->db->select('trx_id, memberid, membername, trx_type, nominal, cashback_ori, ppob_type')
			->from('ppob_trx_hdr_tempsgo')
			->where('trx_id', $id);

		/*$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							b.kode_paket, b.nama_paket')
			->from('ppob_trx_hdr a')
			->join('ppob_xl_master b', 'a.trx_type = b.kode_paket')
			->where('trx_id', $id);*/

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxHdr($id){
		$this->db->select('trx_id, memberid, membername, trx_type, nominal, cashback_ori, ppob_type')
			->from('ppob_trx_hdr_tempsgo')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxHdr1($id){
		$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							b.kode_paket, b.nama_paket')
			->from('ppob_trx_hdr_tempsgo a')
			->join('ppob_xl_master b', 'a.trx_type = b.kode_paket')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxHdr2($id){
		$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							b.product_code, b.product_name')
			->from('ppob_trx_hdr_tempsgo a')
			->join('ppob_sgo_trxtype b', 'a.trx_type = b.product_code')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxHdrReal($id){
		$this->db->select('trx_id, memberid, membername, trx_type, nominal, cashback_ori, ppob_type')
			->from('ppob_trx_hdr')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxHdrReal1($id){
		$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							b.kode_paket, b.nama_paket')
			->from('ppob_trx_hdr a')
			->join('ppob_xl_master b', 'a.trx_type = b.kode_paket')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxHdrReal2($id){
		$this->db->select('a.trx_id, a.memberid, a.membername, a.trx_type, a.nominal, a.cashback_ori, a.ppob_type,
							b.product_code, b.product_name')
			->from('ppob_trx_hdr a')
			->join('ppob_sgo_trxtype b', 'a.trx_type = b.product_code')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}


	public function getTrxDet($id){

		$this->db->select('cust_no, reff_pay_id')
			->from('ppob_trx_det_tempsgo')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxDetReal($id){
		$this->db->select('cust_no, reff_pay_id')
			->from('ppob_trx_det')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxPay($id){
		$this->db->select('a.bank_code_payment, a.charge_admin, a.payamt,
						   b.bankDesc, b.bankDisplayNm')
				 ->from('ppob_trx_pay_tempsgo a')
			     ->join('ecomm_bank b', 'a.bank_code_payment = b.id', 'left')
				 ->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function getTrxPayReal($id){
		$this->db->select('a.bank_code_payment, a.charge_admin, a.payamt,
						   b.bankDesc, b.bankDisplayNm')
			->from('ppob_trx_pay a')
			->join('ecomm_bank b', 'a.bank_code_payment = b.id', 'left')
			->where('trx_id', $id);

		$res=$this->db->get();

		return $res->result();

	}

	public function Insert($table, $data){
		$res = $this->db->insert($table, $data);
		return $res;
	}

	public function Update($table, $data, $where){
		$res = $this->db->insert($table, $data, $where);
		return $res;
	}

	public function getSelectBank($type_bank){
		$this->db->select('*');
		$this->db->where('bankCode',$type_bank);

		return $this->db->get('ecomm_bank');

	}

	function getWhere($table,$field,$id)
	{
		$q = $this->db->query("select * from $table where $field='$id' ORDER BY $field ASC");

		return $q;
	}

	function getMemberSaldo($param, $value) {

		$qry = "SELECT top 1 a.dfno, a.fullnm,
				    		 a.novac, ISNULL(b.amount, 0 ) AS amount
				FROM klink_mlm2010.[dbo].msmemb a
				LEFT OUTER JOIN db_ecommerce.[dbo].va_cust_pay_bal b ON a.dfno = b.dfno
				WHERE a.$param = '$value' or a.novac = '$value' ";

		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	public function getTotalWeight($id){
		$qry = "select a.orderno, SUM(a.qty * b.[weight]) as total_weight, SUM(a.qty) as total_item
				from ecomm_trans_det_prd_sgo a
				inner join master_prd_cat_inv b ON (a.prdcd = b.cat_inv_id)
				WHERE a.orderno = '$id'
				GROUP BY a.orderno";

		$res = $this->getRecordset($qry);
		return $res;

	}


	public function getTotalPay($id){
		$qry = "select total_pay
				from ecomm_trans_hdr_sgo
				WHERE orderno = '$id'";

		$res = $this->getRecordset($qry);
		return $res;

	}

	function cekOrderNo($id) {
		$qry = "select orderno
				from ecomm_trans_hdr_sgo
				WHERE orderno = '$id'";

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function cekTransShipAddrSgo($id){
		$qry = "select a.orderno, b.* from ecomm_trans_hdr_sgo  a
				JOIN ecomm_trans_shipaddr_sgo b ON a.orderno = b.orderno WHERE a.orderno = '$id'";

		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	/*function showListPriceJne($from, $thru, $weight) {
		//$data = $this->input->post(NULL, TRUE);
		//$xx = $this->session->userdata('shipping_jne_info');
		$jne_username = "KLINK";
		$jne_api_key = "76270305bef5d402220c96d59ac61977";
		$generate_url_ship_priceBaru = "http://api.jne.co.id:8889/tracing/klink/generateCnote";

		$curl_post_data = array(
			"username" => urlencode($jne_username),
			"api_key" => urlencode($jne_api_key),
			"from" => urlencode($from),
			"thru" => urlencode($thru),
			"weight" => urlencode($weight)
		);

		$postData = setFieldPost($curl_post_data);

		$curl = curl_init();
		$arx = array(
			CURLOPT_URL => $generate_url_ship_priceBaru,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/x-www-form-urlencoded")
		);
		curl_setopt_array($curl, $arx);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}*/

	function checkNmStockist($id){
		$qry = "select fullnm
				from mssc
				WHERE loccd = '$id'";

		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function checkDataProdukKnet($orderno) {
		$qry = "SELECT a.orderno, a.CNno, a.total_pay, a.total_bv,
				 a.KWno, a.sentTo, a.nmstkk, b.conoteJNE, b.receiver_name, a.pricecode, c.GDO
				FROM ecomm_trans_hdr a
				LEFT OUTER JOIN ecomm_trans_shipaddr b ON (a.orderno = b.orderno)
				LEFT OUTER JOIN klink_mlm2010.dbo.intrh c ON (a.KWno COLLATE SQL_Latin1_General_CP1_CS_AS = c.applyto)
				WHERE a.orderno = '$orderno'";
		$header = $this->getRecordset($qry, null, $this->db1);

		$qry2 = "SELECT * FROM ecomm_trans_det_prd a WHERE a.orderno = '$orderno'";
		$detail = $this->getRecordset($qry2, null, $this->db1);

		$res = array(
			"header" => $header,
			"produk" => $detail
		);

		return $res;
	}

	function getListSP($data) {

		$dtfrom = date("Y/m/d", strtotime($data['cn_from']));
		$dtto = date("Y/m/d", strtotime($data['cn_to']));
		/*$qry = "SELECT CNno,
                    KWno,
					total_pay,
					total_bv,
					idstk,
					nmstkk,
					id_memb, nmmember
		        FROM V_ECOMM_SEARCH_CN A
				WHERE CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'
				AND CNno is not null AND CNPrintStatus = '$status'";*/
		$qry="
		SELECT b.prdcd as no_code, b.prdnm as name_code, sum(b.qty) as tot_qty FROM ecomm_trans_hdr a
LEFT OUTER JOIN ecomm_trans_det_prd b ON (a.orderno = b.orderno)
WHERE b.prdcd !='' AND a.datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
group by b.prdcd,b.prdnm order by tot_qty DESC";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function getListSPBlmKirim($data) {

		$dtfrom = date("Y/m/d", strtotime($data['cn_from']));
		$dtto = date("Y/m/d", strtotime($data['cn_to']));
		$statusKirim = $data['statusKirim'];

		if($statusKirim == '0'){
			$where = " AND a.datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59' ";
		}elseif($statusKirim == '2'){// menunggu pembayaran <= 4 jam
		//AND a.datetrans between DATEADD(hour, -4, GETDATE()) AND GETDATE()
			$where = " AND a.datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
					   AND a.datetrans <= DATEADD(hour, -4, GETDATE()) " ;
		}elseif($statusKirim == '3'){// menunggu pembayaran > 4 jam
			$where = " AND a.datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
					   AND a.datetrans > DATEADD(hour, -4, GETDATE()) ";
		}

		$qry = "SELECT  b.prdcd as no_code, b.prdnm as name_code, sum(b.qty) as tot_qty
				FROM ecomm_trans_hdr_sgo a
					 LEFT OUTER JOIN ecomm_trans_det_prd_sgo b ON (a.orderno = b.orderno)
				     LEFT OUTER JOIN ecomm_trans_hdr C ON A.orderno=C.token
				WHERE b.prdcd !=''
					  $where
					  AND A.orderno IS NOT NULL AND C.token IS NULL
				group by b.prdcd,b.prdnm
				order by tot_qty DESC";

		//print_r($qry);
		$res = $this->getRecordset($qry);
		return $res;

	}


	function getSPReportToPrint($data) {
		$dtfrom = date("Y/m/d", strtotime($data['cn_from']));
		$dtto = date("Y/m/d", strtotime($data['cn_to']));
		/*$qry = "SELECT CNno,
                    KWno,
					total_pay,
					total_bv,
					idstk,
					nmstkk,
					id_memb, nmmember
		        FROM V_ECOMM_SEARCH_CN A
				WHERE CONVERT(VARCHAR(10), A.datetrans, 111) between '$dtfrom' and '$dtto'
				AND CNno is not null AND CNPrintStatus = '$status'";*/
		$qry="
		SELECT b.prdcd as no_code, b.prdnm as name_code, sum(b.qty) as tot_qty FROM ecomm_trans_hdr a
LEFT OUTER JOIN ecomm_trans_det_prd b ON (a.orderno = b.orderno)
WHERE b.prdcd !='' AND a.datetrans between '$dtfrom 00:00:00' AND '$dtto 23:59:59'
group by b.prdcd,b.prdnm order by tot_qty DESC";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;

	}

	function cekTransHdr($id){
		$q = $this->db->query("SELECT * FROM ecomm_trans_hdr WHERE token ='$id'");

		return $q->result_array();
	}

	function getRebookConot($orderno) {
		$qry = "SELECT * FROM ecomm_trans_shipaddr
				WHERE A.orderno = '$orderno'";
		//echo $qry;
		$res = $this->getRecordset($qry);
		return $res;
	}

	function failTrhTrx() {
		/*$query = "SELECT a.trcd, c.token, CONVERT(varchar(10), a.etdt, 120) AS etdt,
							CONVERT(varchar(10), c.datetrans, 120) AS datetrans,
								a.dfno, c1.userlogin, a.tbv, a.totpay, c.CNno, c.KWno,
								d.trcd AS bill_kw, e.trcd as bbhdr_id, f.trcd as newivtrh_id,
							CONVERT(varchar(10), a.bnsperiod, 120) AS bnsperiod, b.trcd AS trcd_newtrh
							FROM klink_mlm2010.dbo.sc_newtrh a
							INNER JOIN db_ecommerce.dbo.ecomm_trans_hdr c ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = c.orderno)
							LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_hdr_sgo c1 ON (c.token COLLATE SQL_Latin1_General_CP1_CI_AS = c1.orderno)
							LEFT OUTER JOIN klink_mlm2010.dbo.newtrh b ON (a.trcd = b.trcd)
							LEFT OUTER JOIN klink_mlm2010.dbo.billivhdr d ON (c.KWno COLLATE SQL_Latin1_General_CP1_CS_AS = d.trcd)
							LEFT OUTER JOIN klink_mlm2010.dbo.bbhdr e ON (c.IPno COLLATE SQL_Latin1_General_CP1_CS_AS = e.trcd)
							LEFT OUTER JOIN klink_mlm2010.dbo.newivtrh f ON (c.CNno COLLATE SQL_Latin1_General_CP1_CS_AS = f.trcd)
							WHERE (a.trcd LIKE 'IDEC%' AND YEAR(a.etdt) >= 2019)
							AND (b.trcd IS NULL OR d.trcd IS NULL OR e.trcd is null OR f.trcd is null)
							OR a.bnsperiod IS NULL OR a.bnsperiod = ''
							OR a.bnsperiod = '1970-01-01'
							OR a.dfno IS NULL OR a.dfno = ''"; */

		$query = "SELECT c.orderno as trcd, c.token, CONVERT(varchar(10), a.etdt, 120) AS etdt,
						CONVERT(varchar(10), c.datetrans, 120) AS datetrans,
							c.id_memb as dfno, c1.userlogin, a.tbv, c.total_pay as totpay,
							c.CNno, c.KWno,
							d.trcd AS bill_kw, e.trcd as bbhdr_id, f.trcd as newivtrh_id,
						CONVERT(varchar(10), a.bnsperiod, 120) AS bnsperiod, b.trcd AS trcd_newtrh
						FROM db_ecommerce.dbo.ecomm_trans_hdr c
						LEFT OUTER JOIN klink_mlm2010.dbo.sc_newtrh a ON (a.trcd COLLATE SQL_Latin1_General_CP1_CI_AS = c.orderno)
						LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_hdr_sgo c1 ON (c.token COLLATE SQL_Latin1_General_CP1_CI_AS = c1.orderno)
						LEFT OUTER JOIN klink_mlm2010.dbo.newtrh b ON (c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.billivhdr d ON (c.KWno COLLATE SQL_Latin1_General_CP1_CS_AS = d.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.bbhdr e ON (c.IPno COLLATE SQL_Latin1_General_CP1_CS_AS = e.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.newivtrh f ON (c.CNno COLLATE SQL_Latin1_General_CP1_CS_AS = f.trcd)
						WHERE (c.orderno LIKE 'IDEC%' AND YEAR(c.datetrans) >= 2019)
						AND (b.trcd IS NULL OR d.trcd IS NULL OR e.trcd is null OR f.trcd is null)
						OR c.bonusmonth IS NULL OR c.bonusmonth = ''
						OR c.bonusmonth = '01/1970'
						OR c.id_memb is null OR c.id_memb = ''";
		$res = $this->getRecordset($query);
		return $res;
	}

	function getDetail($orderno) {
		$query = "SELECT a.orderno, h.dfno, a.token,
						a.SSRno, a.CNno, a.REGISTERno, a.KWno, a.IPno,
						a.total_pay, a.total_bv, a.bonusmonth,
						b.orderno as webol_trans_ok_id,
						c.orderno as webol_log_trans_id,
						d.registerno as ordivhdr_id,
						e.registerno as ordivtrh_id,
						f.trcd as bbhdr_id,
						g.trcd as custpaydet_cn,
						h.trcd as sc_newtrh_id,
						i.trcd as newtrh_id,
						j.trcd as billivhdr_id,
						k.trcd as newivtrh_id,
						l.trcd2 as ordivtrp_id,
						m.trcd as ordivdetp_id,
						n.trcd as billivdetp_id,
						o.trcd as newivtrp_id,
						a.idstk
						FROM ecomm_trans_hdr a
						LEFT OUTER JOIN klink_mlm2010.dbo.webol_trans_ok b ON (a.orderno = b.orderno)
						LEFT OUTER JOIN klink_mlm2010.dbo.webol_logs_trans c ON (a.orderno = c.orderno)
						LEFT OUTER JOIN klink_mlm2010.dbo.ordivhdr d ON (a.REGISTERno COLLATE SQL_Latin1_General_CP1_CS_AS = d.registerno)
						LEFT OUTER JOIN klink_mlm2010.dbo.ordivtrh e ON (a.REGISTERno COLLATE SQL_Latin1_General_CP1_CS_AS = e.registerno)
						LEFT OUTER JOIN klink_mlm2010.dbo.bbhdr f ON (a.IPno COLLATE SQL_Latin1_General_CP1_CS_AS = f.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.custpaydet g ON (a.CNno COLLATE SQL_Latin1_General_CP1_CS_AS = g.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.sc_newtrh h ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = h.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.newtrh i ON (a.orderno COLLATE SQL_Latin1_General_CP1_CS_AS = i.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.billivhdr j ON (a.KWno COLLATE SQL_Latin1_General_CP1_CS_AS = j.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.newivtrh k ON (a.CNno COLLATE SQL_Latin1_General_CP1_CS_AS = k.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.ordivtrp l ON (a.REGISTERno COLLATE SQL_Latin1_General_CP1_CS_AS = l.trcd2)
						LEFT OUTER JOIN klink_mlm2010.dbo.ordivdetp m ON (a.REGISTERno COLLATE SQL_Latin1_General_CP1_CS_AS = m.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.billivdetp n ON (a.KWno COLLATE SQL_Latin1_General_CP1_CS_AS = n.trcd)
						LEFT OUTER JOIN klink_mlm2010.dbo.newivtrp o ON (a.CNno COLLATE SQL_Latin1_General_CP1_CS_AS = o.trcd)
					WHERE a.orderno = '$orderno'";
		$res = $this->getRecordset($query);
		return $res;
	}

	function checkingNewtrh($orderno){
		$query = "SELECT newtrh.trcd FROM klink_mlm2010.dbo.newtrh WHERE newtrh.orderno = '$orderno'";
		$res = $this->getRecordset($query);
		return $res;
	}

	function checkingBillivhdr($kwno) {
		$query = "SELECT billivhdr.trcd FROM klink_mlm2010.dbo.billivhdr WHERE billivhdr.trcd = '$kwno'";
		$res = $this->getRecordset($query);
		return $res;
	}

	function checkingBbhdr($ipno) {
		$query = "SELECT * FROM klink_mlm2010.dbo.bbhdr WHERE bbhdr.trcd = '$ipno'";
		$res = $this->getRecordset($query);
		return $res;
	}

	function checkingNewivtrh($cnno) {
		$query = "SELECT * FROM klink_mlm2010.dbo.newivtrh WHERE newivtrh.trcd = '$cnno'";
		$res = $this->getRecordset($query);
		return $res;
	}

	function reconcileNewtrh($orderno) {
		$query = "INSERT INTO klink_mlm2010.dbo.newtrh(
								paytype1, paytype2, sctype, paytype3, taxrate, branch,
								trcd2, trdt, createdt, dfno, loccd, sc_dfno, sc_co,
								trcd, orderno, pricecode, trtype, batchno,
								tpv, tbv, npv, nbv, tdp, ndp, pay1amt, totpay,
								createnm, whcd, bnsperiod, ttptype, receiptno, registerno, etdt, updatedt)
							SELECT '1', '1', '1', '03', 10, 'B001',
								a.CNno, datetrans, datetrans, id_memb, idstk, idstk, idstk,
								orderno, orderno, pricecode, 'SB1', a.SSRno,
								total_bv, total_bv, total_bv, total_bv, total_pay, total_pay, total_pay, total_pay,
								'ECOMMERCE', 'WH001', RIGHT(bonusmonth, 4) + '-' + left(bonusmonth, 2) + '-01',
							CASE
								WHEN LEFT(a.orderno, 4) = 'IDEC' THEN 'SC'
								WHEN LEFT(a.orderno, 1) = 'E' THEN 'MEMB'
							ELSE 'SC'
								END AS ttptype,
									a.KWno, a.KWno, datetrans, datetrans
							FROM db_ecommerce.dbo.ecomm_trans_hdr a
							WHERE a.orderno = '$orderno'";
		$res = $this->executeQuery($query);
		return $res;
	}

	function reconcileBillivhdr($orderno) {
		$query = "INSERT INTO
								klink_mlm2010.dbo.billivhdr(
									totinvoice,
									trcd,
									applyto,
									trdt,
									dfno,
									tpv,
									tbv,
									npv,
									nbv,
									tdp,
									ndp,
									totpay,
									createnm,
									flagOL,
									createdt,
									etdt,
									updatedt)
							SELECT 1, a.KWno, a.REGISTERno, datetrans, idstk, total_bv, total_bv, total_bv, total_bv, total_pay, total_pay, total_pay, 'ECOMMERCE', '0', datetrans, datetrans, datetrans
							FROM
								db_ecommerce.dbo.ecomm_trans_hdr a
							WHERE
								a.orderno = '$orderno'";
		$res = $this->executeQuery($query);
		return $res;
	}

	function reconcileBbhdr($IPno, $orderno) {
		$query = "DECLARE
			@trcd varchar(20) ,
			@trtype nvarchar(8),
			@effect varchar(1),
			@etdt datetime ,
			@trdt datetime ,
			@dfno varchar(20),
			@custtype varchar(1),
			@docno varchar(200),
			@amount numeric(15, 2),
			@createnm varchar(20),
			@createdt datetime,
			@updatenm varchar(20),
			@updatedt datetime,
			@PT_SVRID varchar(20),
			@applyto varchar(20),
			@OLtrcd varchar(20),
			@ipnoVar varchar(20),
			@orderno varchar(20)

		SELECT @ipnoVar = '$IPno', @orderno = '$orderno';

		SELECT @trcd = a.trcd,
		@trtype = a.trtype,
		@effect = a.effect,
		@etdt = a.etdt,
		@trdt = a.trdt,
		@dfno = a.dfno,
		@custtype = a.custtype,
		@docno = a.docno,
		@amount = a.amount,
		@createnm = a.createnm,
		@createdt = a.createdt,
		@updatenm = a.updatenm,
		@updatedt = a.updatedt,
		@PT_SVRID = a.PT_SVRID,
		@applyto = a.applyto,
		@OLtrcd = a.OLtrcd
		FROM klink_mlm2010.dbo.custpaydet a
		WHERE A.applyto=@ipnoVar AND A.effect='-';

		DELETE klink_mlm2010.dbo.custpaydet where applyto=@ipnoVar AND effect='-'; ;

		INSERT INTO klink_mlm2010.dbo.bbhdr([type], dfno, trtype, trcd, [status], custtype,
											amount, createnm, bankacccd, refno, [description],
											etdt, trdt, createdt, updatedt)
		SELECT 'I', idstk, 'IP', a.IPno, 'O', 'S',
					 total_pay, 'ECOMMERCE', 'BL01', bankaccno, 'PEMBAYARAN ONLINE (ECOMMERCE) DARI ' + bankaccno,
					 datetrans, datetrans, datetrans, datetrans
		FROM db_ecommerce.dbo.ecomm_trans_hdr a
		where a.orderno = @orderno;

		INSERT INTO klink_mlm2010.dbo.custpaydet(trcd, trtype, effect, etdt, trdt, dfno,
		custtype, docno, amount, createnm, createdt, updatenm,
		updatedt, PT_SVRID, applyto, OLtrcd)
		VALUES (@trcd, @trtype, @effect, @etdt, @trdt, @dfno,
		@custtype, @docno, @amount, @createnm, @createdt, @updatenm,
		@updatedt, @PT_SVRID, @applyto, @OLtrcd)";
		$res = $this->executeQuery($query);
		return $res;
	}

	function reconcileNewivtrh($orderno) {
		$query = "INSERT INTO klink_mlm2010.dbo.newivtrh(shipto, ship, ordtype, trcd, applyto, receiptno, batchscno,
								branch, bnsperiod, invoiceno,  dfno, loccd, sc_dfno, pricecode,
								trtype, tpv, tbv, npv, nbv, tdp, ndp, totpay, trdt,
								createnm, updatenm, whcd, etdt, createdt, updatedt)
							SELECT '1',
							CASE
								WHEN a.sentTo = '2'
								THEN '1'
							ELSE '2'
							END AS shipto ,
								'2', a.CNno , a.CNno, a.KWno, a.SSRno,
								'B001', RIGHT(bonusmonth, 4) + '-' + left(bonusmonth, 2) + '-01', 'CNI' + 'E' + SUBSTRING(a.CNno, 5, len(a.CNno)) ,
							CASE
								WHEN a.sentTo = '2'
								THEN 'BID06'
							ELSE a.idstk
							END AS stk1,
							CASE
								WHEN a.sentTo = '2'
								THEN 'BID06'
							ELSE a.idstk
							END AS stk2,
							CASE
								WHEN a.sentTo = '2'
								THEN 'BID06'
							ELSE a.idstk
							END AS stk3,
								pricecode,
								'SA0', total_bv, total_bv, total_bv, total_bv,
								total_pay, total_pay, total_pay, datetrans,
								'ECOMMERCE', 'ECOMMERCE', 'WH002', datetrans, datetrans, datetrans
							FROM db_ecommerce.dbo.ecomm_trans_hdr a
							WHERE a.orderno IN ('$orderno')";
		$res = $this->executeQuery($query);
		return $res;
	}

	function reconcileBonusMonth($bnsperiod, $registerno, $cnno, $orderno, $bnsmth) {
		$db_qryx = $this->load->database('klink_mlm2010', true);
		$db_qryx->trans_begin();

		$qry_ordivhdr = "UPDATE klink_mlm2010.dbo.ordivhdr SET bnsperiod = '$bnsperiod' WHERE registerno = '$registerno'";
		$db_qryx->query($qry_ordivhdr);
		//echo $qry_ordivhdr."<br />";

		$qry_ordivtrh = "UPDATE klink_mlm2010.dbo.ordivtrh SET bnsperiod = '$bnsperiod' WHERE registerno = '$registerno'";
		$db_qryx->query($qry_ordivtrh);
		//echo $qry_ordivtrh."<br />";

		$qry_newivtrh = "UPDATE klink_mlm2010.dbo.newivtrh SET bnsperiod = '$bnsperiod' WHERE trcd = '$cnno'";
		$db_qryx->query($qry_newivtrh);
		//echo $qry_newivtrh."<br />";

		$qry_newtrh = "UPDATE klink_mlm2010.dbo.newtrh SET bnsperiod = '$bnsperiod' WHERE trcd = '$orderno'";
		$db_qryx->query($qry_newtrh);
		//echo $qry_newtrh."<br />";

		$qry_sc_newtrh = "UPDATE klink_mlm2010.dbo.sc_newtrh SET bnsperiod = '$bnsperiod' WHERE trcd = '$orderno'";
		$db_qryx->query($qry_sc_newtrh);
		//echo $qry_sc_newtrh."<br />";

		$qry_webol_trans_ok = "UPDATE klink_mlm2010.dbo.webol_trans_ok SET bonusmonth = '$bnsmth' WHERE orderno = '$orderno'";
		$db_qryx->query($qry_webol_trans_ok);
		//echo $qry_webol_trans_ok."<br />";

		$qry_trans_hdr = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr SET bonusmonth = '$bnsmth' WHERE orderno = '$orderno'";
		$db_qryx->query($qry_trans_hdr);

		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			$res = jsonFalseResponse("Update data periode bonus gagal");
		} else {
			$db_qryx->trans_commit();
			$res = jsonTrueResponse(null, "Update data berhasil");
		}

		return $res;
	}

	function reconcileIdMember($id, $namamember, $orderno) {
		$db_qryx = $this->load->database('klink_mlm2010', TRUE);
		$db_qryx->trans_begin();

		$qry_newtrh = "UPDATE klink_mlm2010.dbo.newtrh SET dfno = '$id' WHERE trcd = '$orderno'";
		$db_qryx->query($qry_newtrh);
		//echo $qry_newtrh."<br />";

		$qry_sc_newtrh = "UPDATE klink_mlm2010.dbo.sc_newtrh SET dfno = '$id' WHERE trcd = '$orderno'";
		$db_qryx->query($qry_sc_newtrh);
		//echo $qry_sc_newtrh."<br />";

		$qry_webol_trans_ok = "UPDATE klink_mlm2010.dbo.webol_trans_ok SET id_memb = '$id', nmmember='$namamember'
								WHERE orderno = '$orderno'";
		$db_qryx->query($qry_webol_trans_ok);
		//echo $qry_webol_trans_ok."<br />";

		$qry_trans_hdr = "UPDATE db_ecommerce.dbo.ecomm_trans_hdr SET id_memb = '$id', nmmember = '$namamember'
							   WHERE orderno = '$orderno'";
		//echo $qry_webol_trans_ok."<br />";
		$db_qryx->query($qry_trans_hdr);


		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			$res = jsonFalseResponse("Update data member gagal");
		} else {
			$db_qryx->trans_commit();
			$res = jsonTrueResponse(null, "Update data berhasil");
		}
		return $res;
	}

	function memberRecordDetail($orderno) {
		$qry = "SELECT c.trx_no as token, a.flag_voucher,
							a.trx_no, a.idno, a.memberid, a.membername, b.id_memb,
							d.[Transaction_ID] as sgo, e.novac as kwallet
						FROM ecomm_memb_ok a
						LEFT OUTER JOIN ecomm_trans_hdr b ON (a.memberid = b.id_memb AND a.trx_no = b.orderno)
						LEFT OUTER JOIN ecomm_memb_ok_sgo c ON (a.idno = c.idno)
						LEFT OUTER JOIN REPORT_DOWNLOAD_SGO d ON (c.trx_no = d.Order_ID)
						LEFT OUTER JOIN va_cust_pay_det e ON (c.trx_no collate SQL_Latin1_General_CP1_CS_AS = e.trcd)
						WHERE a.trx_no = '$orderno'";
		$result = $this->getRecordset($qry);
		return $result;
	}
}
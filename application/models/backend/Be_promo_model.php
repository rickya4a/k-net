<?php
class Be_promo_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
	}
    
    function getIdstk(){
        $slc = "select distinct a.idstk 
                from ecomm_trans_hdr a where a.orderno like 'E1601%'";
        $result = $this->getRecordset($slc,null,$this->db1);
	   return $result;    
    }
    
    function getListPromo($dt){
        //kalo udh jalan
        /*$slc = "select a.idstk,b.prdcd,sum(b.qty) as qty
                 from ecomm_trans_hdr a
                  inner join ecomm_trans_det_prd b on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.flag_trx = 'P' and  
                (CONVERT(VARCHAR(10), a.datetrans, 111) between '".$dt['from_date']."' and '".$dt['to_date']."') 
                group by a.idstk,b.prdcd";*/
        if($dt['idstkk'] == ""){
            $x = "a.orderno like 'O%' and a.promoPrintStatus = '".$dt['printst']."' 
                    and (CONVERT(VARCHAR(10), a.datetrans, 111) between '".$dt['from_date']."' and '".$dt['to_date']."') "; 
        }else{
            $x = "a.orderno like 'O%' and a.promoPrintStatus = '".$dt['printst']."' and a.idstk = '".$dt['idstkk']."' and
                    (CONVERT(VARCHAR(10), a.datetrans, 111) between '".$dt['from_date']."' and '".$dt['to_date']."') ";
        }
        
        $slc = "select a.idstk,b.prdcd,b.prdnm,sum(b.qty) as qty
                 from ecomm_trans_hdr a
                  inner join ecomm_trans_det_prd b on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where $x                
                group by a.idstk,b.prdcd,b.prdnm
                order by a.idstk";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
	   return $result; 
    }
    
    function getHeadStk($idstk){
        $slc = "select a.loccd,a.fullnm,addr1,addr2,addr3,tel_hm,tel_hp from mssc a where a.loccd = '".$idstk."'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
	   return $result; 
    }
    
    function getListPromoDet($idstk,$froms,$tos){
        /*$slc = "select a.orderno,a.id_memb,b.prdcd,b.prdnm,b.qty
                from ecomm_trans_hdr a
                	inner join ecomm_trans_det_prd b on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.idstk = '".$idstk."' AND a.flag_trx = 'P'
                and a.orderno like 'E1601%' and (CONVERT(VARCHAR(10), a.datetrans
                , 111) between '".$froms."' and '".$froms."')
                order by a.orderno";*/
                
        $slc = "select a.orderno,a.id_memb,a.nmmember,b.prdcd,b.prdnm,b.qty
                from ecomm_trans_hdr a
                	inner join ecomm_trans_det_prd b on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.idstk = '".$idstk."'
                and a.orderno like 'O%' and (CONVERT(VARCHAR(10), a.datetrans
                , 111) between '".$froms."' and '".$tos."')
                order by a.orderno";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function getHeadPromoList($dt){
        $param = set_list_array_to_string($dt['ceks']);
        
        /*$updt = "update ecomm_trans_hdr set promoPrintStatus = '1' 
                where loccd in ($param) and (CONVERT(VARCHAR(10), a.datetrans
                , 111) between '".$dt['from_date']."' and '".$dt['to_date']."')
                and  AND flag_trx = 'P'";*/
        
        $updt = "update ecomm_trans_hdr set promoPrintStatus = '1' 
                where idstk in ($param) and (CONVERT(VARCHAR(10), datetrans
                , 111) between '".$dt['from_date']."' and '".$dt['to_date']."')
                and  orderno like 'E1601%' ";
        echo $updt;
        $res1 = $this->executeQuery($updt);
		//return $res;
        
        $head = "select a.loccd,a.fullnm,addr1,addr2,addr3,tel_hm,tel_hp 
                    from klink_mlm2010.dbo.mssc a where a.loccd in ($param)";
        
        $result = $this->getRecordset($head,null,$this->db2);
        return $result; 
        
    }
    
    function getDetPromoList($dt){
        $param = set_list_array_to_string($dt['ceks']);
        $det = "select a.orderno,a.id_memb,a.nmmember,b.prdcd,b.prdnm,b.qty,a.idstk
                from db_ecommerce.dbo.ecomm_trans_hdr a
                	inner join db_ecommerce.dbo.ecomm_trans_det_prd b on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.idstk in($param)
                and a.orderno like 'E1601%' and (CONVERT(VARCHAR(10), a.datetrans
                , 111) between '".$dt['from_date']."' and '".$dt['to_date']."')
                order by a.orderno";
        $result = $this->getRecordset($det,null,$this->db1);
        return $result; 
        /*$db1 = $this->load->database("klink_mlm2010", true);
        $countt = count($dt['ceks']);
        //echo "brpa ".$countt;
        
        $x = array();
        for($i = 0; $i < count($countt); $i++){
            $det = "select a.orderno,a.id_memb,a.nmmember,b.prdcd,b.prdnm,b.qty,a.idstk
                from db_ecommerce.dbo.ecomm_trans_hdr a
                	inner join db_ecommerce.dbo.ecomm_trans_det_prd b on a.orderno = b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.idstk = '".$dt['ceks'][$i]."'
                and a.orderno like 'E1601%' and (CONVERT(VARCHAR(10), a.datetrans
                , 111) between '".$dt['from_date']."' and '".$dt['to_date']."')
                order by a.orderno";
            $qry2 = $db1->query($det);
            $detail = $qry2->result();
            //echo $det;
            //$x[$i] = array("header" => $header, "detail" => $detail); 
            $x[$i] = array("detail" => $detail); 
            //print_r($x[$i]);
        }
        return $x;*/
    }
    
	function getListPromoConfig() {
		$qry = "SElECT id, name, 
		         CASE WHEN status = '1' THEN 'Active' ELSE 'Inactive' END AS status, 
		         CONVERT(VARCHAR(10),period_start,21) as period_start,
		         CONVERT(VARCHAR(10),period_end,21) as period_end,
		         max_discount
		        FROM promo_period";	
		$result = $this->getRecordset($qry,null,$this->db1);
        return $result; 
	}
	
	function getListPromoConfigByID($id) {
		$qry = "SElECT id, name, status,
		         CONVERT(VARCHAR(10),period_start,21) as period_start,
		         CONVERT(VARCHAR(10),period_end,21) as period_end,
		         max_discount
		        FROM promo_period
		        WHERE id = '$id'";	
		$result = $this->getRecordset($qry,null,$this->db1);
        return $result; 
	}
	
	function savePromoConfig($form) {
		$qry = "INSERT INTO promo_period (id, name, status, period_start, period_end, max_discount) VALUES 
		        ('$form[id]', '$form[name]', '$form[status]', '$form[period_start]', '$form[period_end]', ".$form['max_discount'].")";
		$res = $this->executeQuery($qry, $this->db1);
		return $res;
	}
	
	function updatePromoConfig($form) {
		$qry = "UPDATE promo_period SET name = '$form[name]', max_discount = ".$form['max_discount'].",
					status = '$form[status]', period_start = '$form[period_start]',
					period_end = '$form[period_end]'
		        WHERE id = '$form[id]'";
	    $res = $this->executeQuery($qry, $this->db1);
		return $res;
	}
	
	function getListBannerPromo() {
		$qry = "SElECT * FROM ecomm_promo_list";	
		$result = $this->getRecordset($qry,null,$this->db1);
        return $result; 
	}
	
	function saveBannerPromo($form, $fileName) {
		$qry = "INSERT INTO ecomm_promo_list (promo_name, year, month, status, url_pic) VALUES 
		        ('$form[promo_name]', '$form[year]', '$form[month]', '$form[status]', '$fileName')";
		$res = $this->executeQuery($qry, $this->db1);
		return $res;
	}
	
	function getBannerPromoById($id) {
		$qry = "SElECT * FROM ecomm_promo_list WHERE id = $id";	
		$result = $this->getRecordset($qry,null,$this->db1);
        return $result; 
	}
	
	function updateBannerPromo($form, $fileName) {
		$qry = "UPDATE ecomm_promo_list SET promo_name = '$form[promo_name]', year = '$form[year]',
					status = '$form[status]', month = '$form[month]', url_pic = '$fileName'
		        WHERE id = ".$form['id']."";
	    $res = $this->executeQuery($qry, $this->db1);
		return $res;
	}
	
	function exportAnnivKolagenHeader() {
		$qry = "SELECT DISTINCT(dfno) as dfno, fullnm, idno, ISNULL(tel_hp, '') as tel_hp, CONVERT(VARCHAR(10),etdt,126) as etdt
				FROM HILAL_COLLAGEN_ANNIV2016
                GROUP BY dfno, fullnm, idno, ISNULL(tel_hp, ''), CONVERT(VARCHAR(10),etdt,126)
                ORDER BY dfno";
		$result = $this->getRecordset($qry,null,$this->db2);
        return $result; 
	}
	
	function exportAnnivKolagenDetail() {
		$qry = "SELECT dfno, vno, CONVERT(VARCHAR(10),etdt,126) as etdt
				FROM HILAL_COLLAGEN_ANNIV2016
				ORDER BY vno, dfno";
		$result = $this->getRecordset($qry,null,$this->db2);
        return $result; 
	}
    
    function getListIdstk(){
        $slc = "select distinct a.loccd,b.fullnm 
                from sc_newtrh a 
                inner join mssc b on (a.loccd = b.loccd COLLATE SQL_Latin1_General_CP1_CS_AS)
                where a.trcd like 'PV%' order by loccd,fullnm";
        $res = $this->getRecordset($slc, null, $this->db2);
		return $res;
    }
    
    function getlistRedempRecruit($dt){
        $dateFrom = date("Y/m/d",strtotime($dt['redemp_from']));
        $dateTo = date("Y/m/d",strtotime($dt['redemp_to']));
        if($dt['idstk'] == ''){
            $x = "";
        }else{
            $x = "and a.loccd = '".$dt['idstk']."' and (CONVERT(VARCHAR(10), a.etdt, 111) between '".$dateFrom."' and '".$dateTo."')";
        }
        $slc = "select count(a.trcd) as tottrx,a.csno,sum(a.tdp) as totPoin,
                a.loccd,a.bnsperiod,a.trdt
                from sc_newtrh a 
                where a.trcd like 'PV%' and a.csno is not NULL $x
                group by a.csno,a.loccd,a.bnsperiod,a.trdt
                order  by a.csno";
        //echo $slc;
        $res = $this->getRecordset($slc, null, $this->db2);
		return $res;
    }
    
    function detRedempRecruit($cnNo){
        $slc = "select a.trcd,a.dfno,b.fullnm,a.loccd,a.csno,a.tdp,a.createdt
                from sc_newtrh a 
                inner join msmemb b on (a.dfno = b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                where a.csno = '".$cnNo."'";
        $res = $this->getRecordset($slc, null, $this->db2);
		return $res;
    }
	
	/*function checkRedempRemain($idmember) {
		$slc = "select dfno, fullnm, total_box, total_redemp, total_remain
                from toto_promo_kolagen_header a 
                where a.dfno = '".$idmember."'";
        $res = $this->getRecordset($slc, null, $this->db1);
		return $res;
	}*/
	
	function getPeriodBo() {
		$qry = "select 
				CONVERT(VARCHAR(10), DateAdd(month,-1,currperiodBO), 20) as period
				FROM syspref
				UNION
		        select 
				CONVERT(VARCHAR(10), currperiodBO, 20) as period
				FROM syspref";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
	
	function checkRedempRemain($idmember, $bns) {
		$qry = "SELECT A.dfno, b.fullnm, SUM(A.tbv) AS tbv, A.bnsperiod
				FROM newtrh A
				INNER JOIN msmemb b ON (A.dfno = b.dfno) 
				WHERE A.dfno='$idmember' AND A.bnsperiod = '$bns'
				GROUP BY A.dfno, b.fullnm, A.bnsperiod
				ORDER BY A.bnsperiod";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
	
	function checkPoinRedemp($idmember, $bns) {
		$bnsmonth = date("d/m/Y",strtotime($bns));
		$qry = "SELECT dfno, SUM(box_redemp) as total_redemp, 
				CONVERT(VARCHAR(10), bonusmonth, 111) as bns
				FROM toto_promo_kolagen_redemption			
				WHERE dfno='$idmember' AND CONVERT(VARCHAR(10), bonusmonth, 111) = '$bnsmonth'	
				GROUP BY dfno, CONVERT(VARCHAR(10), bonusmonth, 111)";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}
	
	function updateRedemp() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "UPDATE toto_promo_kolagen_header 
		            SET total_redemp = total_redemp + ".$data['qty']."
		        WHERE dfno = ".$data['idmember']."";
	    $res = $this->executeQuery($qry, $this->db1);
		
		$qry2 = "UPDATE toto_promo_kolagen_header 
		            SET total_remain = total_box - total_redemp
		        WHERE dfno = ".$data['idmember']."";
	    $res2 = $this->executeQuery($qry2, $this->db1);
	    
	    $ins = "INSERT INTO toto_promo_kolagen_redemption (dfno, fullnm, trx_date, box_redemp, stockist) VALUES
	            ('".$data['idmember']."', '".$data['membernm']."', '".$this->dateTime."', ".$data['qty'].", '".$data['idstk']."')";
		$ins = $this->executeQuery($ins, $this->db1);
	}
	
	/*function saveKolagenRedemp() {
		$data = $this->input->post(NULL, TRUE);
		
	}*/
	
	function checkDataStockist($id) {
		$qry = "SELECT a.loccd, a.fullnm, a.pricecode, a.sfno, b.fullnm as costkname
				FROM mssc a 
				INNER JOIN mssc b ON (a.sfno = b.loccd)
				WHERE a.loccd = '$id'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
	
	function checkDataProduct($prdcd, $pricecode) { 
		$qry = "SELECT a.prdcd, a.prdnm, CONVERT(INT, b.dp) as dp, CONVERT(INT, b.bv) as bv
				FROM msprd a
				INNER JOIN pricetab b ON (a.prdcd = b.prdcd)
				WHERE a.prdcd = '$prdcd' AND b.pricecode = '$pricecode' AND a.status = '0' AND a.webstatus = '0'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
	
	function listPayType() {
		$qry = "SELECT id, description	FROM paytype where id='01'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}
	
	function cek_seQ_promo(){
        $this->db = $this->load->database('klink_mlm2010', true);
        $y1=date("y");
        //$m=date("m");
        $m=date("m", strtotime('- 1 months'));
        
        $this->db->trans_begin();
        
        //if(in_array('p',$tipe_pay))
        
        $tbl = "redemp_"."$m"."$y1";
        $cek = "select * from $tbl";
        
        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (seqVal) values('a')";
            $query = $this->db->query($input);
        }
        else
        {
            $input = "insert into $tbl (seqVal) values('a')";
            $query = $this->db->query($input);
        }
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }      
        
        return $query;
    }
	
	function get_idno_promo(){
        $this->db = $this->load->database('klink_mlm2010', true);
        $y1=date("y");
        //$m=date("m");
        $m=date("m", strtotime('- 1 months'));
        
        $this->db->trans_begin();
        
        $tbl = "redemp_"."$m"."$y1";
 
        $qry = "SELECT * FROM $tbl 
           		 WHERE seqID = ( SELECT MAX(seqID) FROM $tbl )";
        
        $query = $this->db->query($qry);
        if($query == null)
        {
            $ss = 0;
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->seqID;
            }  
        }
        $jumlah = $query->num_rows();
         
       	$next_seq = sprintf("%06s",$ss);
        $prefix = date('ym');
        $y =  strval("RDM".$prefix.$next_seq);
	
          if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }      
       
         return $y;
    }

	function saveKolagenRedemp($idnoo) {
		$createdt = date('Y-m-d H:i:s');
		//$data = $this->input->post(NULL, TRUE);
		//$idnoo = $this->get_idno_promo();
		$distributorcode = $this->input->post('idmember');
        $distributorname = $this->input->post('membernm');
        $stockistcode = $this->input->post('idstk');
        $stkname = $this->input->post('loccdnm');
        $secno = $this->input->post('remark');
        $qtyAvail = $this->input->post('total_remain');
        $totalQty_redemp = $this->input->post('total_redemp');
        $bnsperiod = $this->input->post('bnsperiod');
        $orderno = $this->input->post('orderno');
        $uplinesub = $this->input->post('sc_dfno');
        $uplinesubnm = $this->input->post('costkname');
        $pricecode = $this->input->post('pricecode');
        $remark = $this->input->post('remark');
		$username = $this->username;
        $totalDp = $this->input->post('tdp');
        $paynominal = $this->input->post('nominal');
        
        $search_for = "'";
        $replace = "`";
        
        $stkname = str_replace($search_for,$replace,$stkname);
        $uplinesubnm = str_replace($search_for,$replace,$uplinesubnm);
        $remark = str_replace($search_for,$replace,$remark);
        
        $folderGets = explode('-', $bnsperiod);
        $data['month'] = $folderGets[1];
        $data['year'] = $folderGets[0];
        $bonusperiod = $data['month']."/"."1"."/".$data['year'];
        
        $prdcdcode = $this->input->post('prdcd');
        $productname = $this->input->post('prdnm');
        $qty = $this->input->post('qty');
        $dp = $this->input->post('dp');
        
		$total_bv = 0;
		$bv		  = 0;
        
        //$totalDp = $totPoinRedempt;
        
        //$newSecno = $this->getNewSecno($distributorcode);
        //$cekPoin = $this->cekPoinByID($distributorcode);
        //$cekPoinRedem = $this->cekPoinRedemp($distributorcode);
        //$totAllRedemp = $cekPoinRedem + $totalDp;
        //$poinRemain = $cekPoin - $totAllRedemp;
        
		$this->db->trans_begin();
		
        $checkSC = "select loccd,sctype,fullnm from klink_mlm2010.dbo.mssc where loccd = '".$stockistcode."'";
	    $sctypes = $this->db->query($checkSC);		
			
        if($sctypes->num_rows() >  0)
        {
      	     //echo "masuk inputxx";
	         $row = $sctypes->row();
	         $sctypee = $row->sctype;
             //$aaa = strval("PV".$aa);
	         $bv = 0;
	         $total_bv = 0;
	          
             /*
            --------------------------------------
                QUERY HEADER REDEMPTION AND SALES
            --------------------------------------       
            */               
	         $insHead = "insert into klink_mlm2010.dbo.sc_newtrh 
	                    (trcd,trtype,trdt,dfno,loccd,tdp,taxrate,taxamt,discamt,shcharge,
	                    othcharge,tpv,tbv,npv,nbv,ndp,whcd,branch,pricecode,
	                    paytype1,paytype2,paytype3,pay1amt,pay2amt,pay3amt,totpay,createnm,
	                    updatenm,post,sp,sb,taxable,taxableamt,ordtype,createdt,
	                    orderno,type,scdiscrate,scdiscamt,sctype,scdisc,generate,statusbo,
	                    syn2web,n_bc,status,autorecon,first_trx,bc,PT_SVRID,sc_dfno,
	                    sc_co,bnsperiod,remarks,othdisc,flag_batch,batchstatus,flag_recover,
	                    system,ttptype,entrytype,flag_show,flag_approval)
	                    values
	                    ('".$idnoo."','SB1','".$createdt."','".$distributorcode."',
	                     '".$stockistcode."',".$totalDp.",10,0,0,0,0,0,0,
	                     0,0,".$totalDp.",'WH001','B001','".$pricecode."',
	                     '01','01','03',".$totalDp.",0,0,".$totalDp.",'".$username."','".$username."',
	                     '0',0,0,0,0,'0','".$createdt."','".$idnoo."','0',0,0,'".$sctypee."','1','0','0','0',
	                     0,'0','0','0','1','ID','".$stockistcode."','".$stockistcode."','".$bonusperiod."','".$secno."',
	                     0,'0','0','0','0','REDEMPTION',4,'0','0')";
	         //echo "QUERY 1 = ".$insHead."<br>";
	         $qrySalesHead = $this->db->query($insHead);
             
             $insHeadRedempt = "insert into db_ecommerce.dbo.toto_promo_kolagen_redemption
                                (dfno,fullnm,trx_date,trx_code,box_redemp,stockist,box_awal)
                                values('".$distributorcode."','".$distributorname."','".$createdt."','".$idnoo."',".$totalQty_redemp.",'".$stockistcode."',".$qtyAvail.")";
             
             //echo "QUERY 2 = ".$insHeadRedempt."<br>";
             $qryHeadRedemp = $this->db->query($insHeadRedempt);
	                
	                 
            /*
            --------------------------------------------
                END QUERY HEADER REDEMPTION AND SALES
            --------------------------------------------
            */
            
            
            /*
            ----------------------------------
                QUERY PRODUCT
            ----------------------------------       
            */
            
            $jml_trx = count($prdcdcode);
             for($i=0;$i < $jml_trx;$i++){
                if($prdcdcode[$i] != ""){
                    $productcode = strtoupper($prdcdcode[$i]);
                    //echo $productcode;
                    $insDet = "insert into klink_mlm2010.dbo.sc_newtrd 
                                (trcd,prdcd,qtyord,qtyship,qtyremain,dp,pv,bv,taxrate,sp,sb,scdisc,seqno,scdiscamt,syn2web,qty_used,qty_avail,
                                PT_SVRID,pricecode) VALUES
                                ('".$idnoo."', '".$productcode."',".$qty[$i].",0,0,".$dp[$i].",0,0,0,0,0,0,0,0,'0',0,0,'ID','".$pricecode."')";
                    //echo "QUERY 3 = ".$insDet."<br>";
                    $qryProdDet = $this->db->query($insDet);
                    //echo $insDet;
                }
             }
        
            /*
            ----------------------------
            QUERY PEMBAYARAN
            ----------------------------
            */
            $insDetTrf = "insert into klink_mlm2010.dbo.sc_newtrp (trcd,seqno,paytype,payamt,deposit,trcd2,PT_SVRID) 
                                VALUES ('".$idnoo."',1,'01',".$totalDp.",0,'0','ID')";
            //echo "QUERY 4 = ".$insDetTrf."<br>";
            $qryTrfSales = $this->db->query($insDetTrf);
            
            
            /*
            ----------------------------------
            QUERY UPDATE TOTO_REKRUT_HEADER
            ----------------------------------
            */
            $updHeadRedemp = "update db_ecommerce.dbo.toto_promo_kolagen_header set total_redemp = ".$totalQty_redemp.",
                            updatenm = '".$username."',updatedt = '".$createdt."'
                            where dfno = '".$distributorcode."'";
            //echo "QUERY 5 ".$updHeadRedemp;
            $qryUpdt = $this->db->query($updHeadRedemp);
            
                     
        }
        
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }      
          
          	if(!$qrySalesHead && !$qryHeadRedemp && !$qryProdDet && !$qryTrfSales && !$qryUpdt){
          		//echo "query1";
                return 0;
                //return 1;
            }else{
                return 1;
            }
            
            //return 1;
	}

	function getGenerateByPvr($from,$to,$username,$bnsperiod) // dipake
    {
    	//$this->getUpdPeriodPvr($from, $to, $username, $bnsperiod); //update bonus period sesuai dengan syspref
		
		/*if($tipe == 'pr'){
		  $xx = "and a.trtype = 'VP1' and a.pricecode = '1609P'";
		}elseif($tipe == 'pvr'){
		  $xx = "and a.trtype = 'VP1' and a.pricecode != '1609P'";
		}elseif($tipe == 'rdm'){
		  $xx = "and a.trtype = 'SB1' and a.ttptype = 'REDEMPTION'";
		}else{
		  $xx = "and a.trtype = 'SB1' and a.trcd like 'CV%'";
		}*/
        
        //$this->db = $this->load->database('klink_mlm2010', true);
        $froms = date('Y/m/d', strtotime($from));
        $tos = date('Y/m/d', strtotime($to));
        
        $folderGets = explode('-', $bnsperiod);
        $data['month'] = $folderGets[1];
        $data['year'] = $folderGets[0];
        $bonusperiod = $data['month']."/"."1"."/".$data['year'];
        
        $qry = "select a.trcd,a.dfno,a.totpay,a.etdt,a.bnsperiod,b.fullnm 
                    from klink_mlm2010.dbo.sc_newtrh a 
                    	inner join klink_mlm2010.dbo.msmemb b on (a.dfno = b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS) 
                    where a.sc_dfno = '".$username."'
                    and a.bnsperiod = '".$bonusperiod."'
                    and a.sc_co = '".$username."' 
                    --and a.createnm = '".$username."'
                    and (a.ttptype = 'SUBSC' or a.ttptype = 'SC' or a.ttptype = 'REDEMPTION')
                    and (a.batchno = '' or a.batchno is null)
                    and (CONVERT(VARCHAR(10), a.etdt, 111) 
                    between '".$froms."' and '".$tos."') and a.ttptype <> 'MEMB' 
                    and a.ttptype <> 'MEMBP' and a.trtype = 'SB1' and a.ttptype = 'REDEMPTION'
                    order by a.etdt";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
   }

	function getGroupProduct($cek,$user) //dipake
    { 
        //$this->db = $this->load->database('klinkmlm2010', true);
        $m = date('m');
        $y = date('Y');
        
        
            $trcd = "";
            for($i=0;$i < count($cek);$i++)
            {
                $trcd .= "'".$cek[$i]."', ";
            }
                $trcd = substr($trcd, 0, -2); 
            
            $slc = "select a.prdcd,sum(a.qtyord) as qty,a.dp,a.bv,b.prdnm
                    from klink_mlm2010.dbo.sc_newtrd a
                    	inner join klink_mlm2010.dbo.msprd b on (a.prdcd = b.prdcd)
                        inner join klink_mlm2010.dbo.sc_newtrh c on (a.trcd = c.trcd)
                    where a.trcd in($trcd) and 
                     (c.ttptype = 'SUBSC' or c.ttptype = 'SC' or c.ttptype = 'REDEMPTION')
                    
                    group by a.prdcd,a.dp,a.bv,b.prdnm";
					//c.createnm = '".$user."' and and c.loccd = '".$user."'
        //echo $slc;
        $res = $this->getRecordset($slc, null, $this->db2);
		return $res; 
    }
    
	//==========tambahan hilal @25/05/2025===========================================
	
    function getGroupPayment($cek) //dipake
    { 
       // $this->db = $this->load->database('klinkmlm2010', true);
        
        $trcd = "";
        for($i=0;$i < count($cek);$i++)
        {
            $trcd .= "'".$cek[$i]."', ";
        }
            $trcd = substr($trcd, 0, -2); 
        
        $slc = "select a.paytype, b.[description], sum(a.payamt) as payamt, a.vchtype
				from sc_newtrp a
				     inner join paytype b on a.paytype=b.id
				where a.trcd in ($trcd)
				group by a.paytype, b.[description], a.vchtype
				order by a.paytype";
   
        //echo $slc;
        $res = $this->getRecordset($slc, null, $this->db2);
		return $res; 
    }
    
    function get_SSRno($type,$bnsperiod) //dipake
    {
        $username = $this->username;	
        $this->load->dbforge();
        $this->db = $this->load->database('klink_mlm2010', true);
        $m = date('m');
        $y = date('y');
        
        $bonus = date("Y-d-m",strtotime($bnsperiod));
        //$this->db->trans_begin();
        if($type == 'pvr')
        {
            $prefixs = '04';
        }elseif($type == 'vc'){
            $prefixs = '02';
        }else if($type = 'rdm'){
            $prefixs = '05';
        }
        else{
            $prefixs = '50';
        }
        /*elseif($type == 'apl')
        {
            $prefixs = '05';
        }
        else
        {
            $prefixs = '04';
        }*/
        
        $bran = 'B001';
        $this->dbforge->add_field(array(
                    'hasil' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '20',
                    ),
                ));
        $today = date('Y-m-d');
        $this->dbforge->create_table('#TempTable'); 
        $ins =" INSERT INTO #TempTable
                EXEC getbatchno 'B001', '".$today."', '".$prefixs."', '".$username."', '".$username."', 'ID', null"; 
        //echo $ins;
        $qryIns = $this->db->query($ins);
        if($qryIns > 0)
        {
            $sql = "SELECT hasil FROM #TempTable";
            $qrySlc = $this->db->query($sql);
            
        }
        
        if($qrySlc->num_rows() > 0)
        {
            foreach($qrySlc->result() as $data)
			{
				$nilai[] = $data;
                //echo $nilai;
			}
            
        }
        
        $this->dbforge->drop_table('#TempTable');
        return $nilai;
    }

	function generate_sales($new_id,$trcd,$bonusperiod,$tipee) //dipake
    {
    	$username = $this->username;	
    	//echo "masuk awal";
        $this->db = $this->load->database('klink_mlm2010', true);
        $createdt = date('Y-m-d');
        
        $bnsperiod = date('d/m/Y', strtotime($bonusperiod));
        //echo "MASUK SINI";
        $trcds = "";
        for($i=0;$i < count($trcd);$i++)
        {
            $trcds .= "'".$trcd[$i]."', ";
        }
            $trcds = substr($trcds, 0, -2);
            
            $updte = "update sc_newtrh set batchno = '$new_id',batchdt = '$createdt',
                        	flag_batch='1',flag_approval = '0', flag_show = '1'
                      where trcd in ($trcds) and bnsperiod = '$bnsperiod'";
						//and createnm = '".$username."'
            //echo $updte;
            $query = $this->db->query($updte);           
       
           
        //echo $updte;
        if(!$query)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
 }

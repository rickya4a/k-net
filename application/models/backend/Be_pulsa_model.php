<?php
class Be_pulsa_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
	}
	
	function searchTrxPulsaByDate($from, $to) {
		try {
			$slc = "SELECT id, no_tujuan, harga_jual, tgl_trans, jam, client, 
						lunas, order_lgs
				    FROM pulsa_trx
					WHERE tgl_trans BETWEEN :from AND :to";
			$bindVal = array(
		        ":from" => $from,
		        ":to" => $to,
		    );
        	$result = $this->getRecordset2($slc, $bindVal, $this->db6);
		} catch(Exception $e) {
			throw new Exception($e->getMessage(), 1);		
		}
		
        return $result;
	}
	
	function searchTrxPulsaByID($id) {
		$slc = "SELECT *
			    FROM pulsa_trx
				WHERE id = $id";
        $result = $this->getRecordset($slc,null,$this->db4);
        return $result;
	}
	
	function deletePulsaTrx($id) {
		$slc = "DELETE FROM pulsa_trx WHERE id = $id";
        $query = $this->executeQuery($slc, $this->db4);
		return $query;		
	}
	
	function listTrxOnDebt($order_lgs) {
		$qry = "SELECT a.id, a.client, count(id) as jum_trx, sum(a.harga_jual) as harga_jual, order_lgs
                FROM pulsa_trx a
                WHERE a.order_lgs = '$order_lgs' and a.lunas = ''
                GROUP BY client
                ORDER BY a.client";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function detailClientDebt($client_name, $order_lgs) {
		$qry = "SELECT a.id, a.client, a.no_tujuan, a.harga_jual, a.order_lgs, a.tgl_trans, a.jam
                FROM pulsa_trx a
                WHERE a.order_lgs = '$order_lgs' and a.client = '$client_name' AND a.lunas = ''
                ORDER BY a.tgl_trans";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function updatePayDebtClient($order_lgs, $op, $value) {
		$add = "  AND client = '$value'";	
		if($op == "IN") {
			$add = " AND client IN ($value)";
		}
		$qry = "UPDATE pulsa_trx SET lunas = 'Y' 
				WHERE order_lgs = '$order_lgs' AND lunas = '' 
				      $add";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->db4);
		return $query;		
	}
	
	function updatePayDebtSelected($op, $value) {
		$add = "  AND id = '$value'";	
		if($op == "IN") {
			$add = " AND id IN ($value)";
		}
		$qry = "UPDATE pulsa_trx SET lunas = 'Y' 
				WHERE lunas = '' 
				      $add";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->db4);
		return $query;		
	}
	
	function listTrxByPhoneNumber($param, $from, $to) {
		$qry = "SELECT a.`kode`, a.`full_code`, a.`harga_jual`, DATE_FORMAT(a.`tgl_trans`,'%d/%m/%Y') as tgl_trans, 
		         a.jam, a.client, a.`no_tujuan`
				FROM pulsa_trx a
				WHERE a.`no_tujuan` = '$param' 
				  AND a.`tgl_trans` BETWEEN '$from' AND '$to'
				ORDER BY a.tgl_trans, a.jam";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function listTrxByClient($param, $from, $to) {
		$qry = "SELECT a.`kode`, a.`full_code`, a.`harga_jual`, DATE_FORMAT(a.`tgl_trans`,'%d/%m/%Y') as tgl_trans, 
		         a.jam, a.client, a.`no_tujuan`
				FROM pulsa_trx a
				WHERE a.`client` = '$param' 
				  AND a.`tgl_trans` BETWEEN '$from' AND '$to'
				ORDER BY a.tgl_trans, a.jam";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function listCellPhoneByCustName($param) {
		$qry = "SELECT  a.client, a.no_tujuan
				FROM pulsa_no_client a
				WHERE a.client = '$param' 
				ORDER BY a.no_tujuan";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function checkCodePulsa($fullcode) {
		$qry = "SELECT a.tgl_trans, a.jam
				FROM pulsa_trx a
				WHERE a.full_code = '$fullcode'";
		
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function checkPriceByCode($code) {
		$qry = "SELECT a.harga_jual
				FROM pulsa_harga a
				WHERE a.kode = '$code'";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function insertNewPrice($code, $harga_beli, $setPrice) {
		$qry = "INSERT INTO pulsa_harga (kode, harga_beli, harga_jual)
		        VALUES ('$code', $harga_beli, $setPrice)";
		$res = $this->executeQuery($qry, $this->db4);
		return $res;
	}
	
	function getClientNameByPhoneNumber($phone) {
		$qry = "SELECT a.client, a.no_tujuan
				FROM pulsa_no_client a
				WHERE a.no_tujuan = '$phone'";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
	
	function saveTrxPulsa($formData) {
		$qry = "INSERT INTO pulsa_trx (kode, no_tujuan, harga_beli, harga_jual, tgl_trans, jam,
		            client, input_by, full_code, lunas, order_lgs)
		        VALUES ('$formData[code]', '$formData[no_tujuan]', ".$formData['harga_beli'].", ".$formData['harga_jual'].", '$formData[tgl_trans]', '$formData[jam]',
		            '$formData[client]', '".$this->username."', '$formData[trx_id]', '$formData[lunas]', '$formData[order_lgs]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->db4);
		return $query;	
	}
	
	function updateTrxPulsa($formData) {
		$qry = "UPDATE pulsa_trx SET client = '$formData[client]', harga_jual = '$formData[harga_jual]',
		            order_lgs = '$formData[order_lgs]', lunas = '$formData[lunas]', tgl_trans = '$formData[tgl_trans]'
			    WHERE id = ".$formData['idx']."";
	    //echo $qry;
		$query = $this->executeQuery($qry, $this->db4);
		return $query;		
	}
	
	function trxReport($thn) {
		$qry = "SELECT MONTH(tgl_trans) as bln,
				       SUM(harga_beli) as beli, 
					   SUM(harga_jual) as jual,
				       SUM(harga_jual) - SUM(harga_beli) as profit
				FROM pulsa_trx
				WHERE pulsa_trx.`tgl_trans` LIKE '$thn%'
				GROUP BY  MONTH(tgl_trans)
				ORDER BY MONTH(tgl_trans)";
		//echo $qry;
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;	
	}
	
	function summaryReport($from, $to) {
		$qry = "SELECT YEAR(tgl_trans) as thn,
				       SUM(harga_beli) as beli, 
				       SUM(harga_jual) as jual,
				       SUM(harga_jual) - SUM(harga_beli) as profit
				FROM pulsa_trx
				WHERE YEAR(pulsa_trx.`tgl_trans`) BETWEEN $from AND $to
				GROUP BY  YEAR(tgl_trans)
				ORDER BY YEAR(tgl_trans)";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;	
	}
	
	function getListClient() {
		$qry = "SELECT *
				FROM pulsa_no_client
				WHERE client != '' ORDER BY client";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;	
	}
	
	function getDataClient($no_tujuan) {
		$qry = "SELECT *
				FROM pulsa_no_client
				WHERE no_tujuan = '$no_tujuan'";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;	
	}
	
	function saveNewClient($data) {
		$qry = "INSERT INTO pulsa_no_client (no_tujuan, client)
		        VALUES ('$data[no_tujuan]','$data[client]')";
		$res = $this->executeQuery($qry, $this->db4);
		return $res;
	}
	
	function deleteClientByPhoneNo($no_tujuan) {
		$qry = "DELETE FROM pulsa_no_client WHERE no_tujuan = '$no_tujuan'";
		$res = $this->executeQuery($qry, $this->db4);
		return $res;
	}
	
	function apiUserPulsaLogin($usrname, $password) {
		$qry = "SELECT *
				FROM pulsa_client_user
				WHERE client = '$usrname' AND password = '$password'";
		$result = $this->getRecordset($qry,null,$this->db4);
        return $result;
	}
}
<?php
class Be_finance_umroh_model extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getListPayUmroh() {
        $dt = $this->input->post(null,true);

		//check apakah ada mutasi yang error approvalnya
		$updtChild = "update b
						set b.pay_status='0'
						from ecomm_umroh_save_child a
							 inner join ecomm_umroh_save_mut b on a.id=b.id
						where a.approval_status='0' and b.pay_status='1'";

        //echo $updtChild;
        $qryChild = $this->executeQuery($updtChild);


		//select untuk view
        $slc = "select a.registerno,a.dfno,a.fullnm,b.tot_fund,a.createdt, a.tipe_perjalanan
                 from ecomm_umroh_save_child a
                 	inner join ecomm_umroh_save_mut b on (b.id = a.id)
                 where b.pay_status = '0' and
                 (CONVERT(VARCHAR(10), b.createdt, 111) between '".date('Y/m/d',strtotime($dt['reg_from']))."'
                 and '".date('Y/m/d',strtotime($dt['reg_to']))."')
                 and a.flag_paytype = '1' AND a.approval_status = '0'";
        //echo $slc;

        $result = $this->getRecordset($slc);
        //$result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }

    public function setAppPayment($regno){
        $this->db = $this->load->database('db_ecommerce', true);
        $today = date("Y-m-d H:i:s");
        //echo  "".date("d/m/Y H:i:s")."";


        //========================================= TAMBAHAN HILAL ===========================================
        $slcChild = "SELECT id, registerno, seqno, dfno, fullnm, idno, birthplace, birthdate,
							RIGHT('0' + CONVERT(VARCHAR(2), day(GETDATE())), 2) +
							RIGHT('0' + CONVERT(VARCHAR(2), MONTH(GETDATE())), 2) +
						    RIGHT(CONVERT(VARCHAR(4), YEAR(GETDATE())), 4) as passwordx,
							tel_hp, email, novac, departureid, remarks, depart_status, post_status,
							post_dt, createdt, createnm, GETDATE() as datex,
							RIGHT('0' + CONVERT(VARCHAR(2), MONTH(GETDATE())), 2) + '/' +
						    RIGHT(CONVERT(VARCHAR(4), YEAR(GETDATE())), 4) as bonusmth,
							updatenm, flag_tipe, idparent, addr1, flag_paytype, approvaldt, kecamatan, kota,
							kelurahan, passportno, passportnm, sex, father_name, provinsi, loccd, sfno,
							tipe_perjalanan
					 FROM ecomm_umroh_save_child
					 where registerno = '".$regno."'";
        $queryChild2= $this->db->query($slcChild);
        $id 		= $queryChild2->row()->id;
        $loccd 		= $queryChild2->row()->loccd;
		$flag_tipe	= $queryChild2->row()->flag_tipe;

        $registerno = $queryChild2->row()->registerno;
        $seqno = $queryChild2->row()->seqno;
        $dfno = $queryChild2->row()->dfno;
        $fullnm = $queryChild2->row()->fullnm;
        $idno = $queryChild2->row()->idno;
		$birthplace = $queryChild2->row()->birthplace;
		$birthdate = date("Y-m-d H:i:s",strtotime($queryChild2->row()->birthdate));
		$passwordx = $queryChild2->row()->passwordx;
		$tel_hp = $queryChild2->row()->tel_hp;
		$email = $queryChild2->row()->email;
		$departureid = $queryChild2->row()->departureid;
		$remarks = $queryChild2->row()->remarks;
		$depart_status = $queryChild2->row()->depart_status;
		$post_status = $queryChild2->row()->post_status;
		$post_dt = date("Y-m-d H:i:s",strtotime($queryChild2->row()->post_dt));
		$createdt = date("Y-m-d H:i:s",strtotime($queryChild2->row()->createdt));
		$createnm = $queryChild2->row()->createnm;
		$datex = date("Y-m-d H:i:s",strtotime($queryChild2->row()->datex));
		$bonusmth = $queryChild2->row()->bonusmth;
		$updatenm = $queryChild2->row()->updatenm;
		$flag_tipe = $queryChild2->row()->flag_tipe;
		$idparent = $queryChild2->row()->idparent;
		$addr1 = $queryChild2->row()->addr1;
		$flag_paytype = $queryChild2->row()->flag_paytype;
		$approvaldt = date("Y-m-d H:i:s",strtotime($queryChild2->row()->approvaldt));
		$kecamatan = $queryChild2->row()->kecamatan;
		$kota = $queryChild2->row()->kota;
		$kelurahan = $queryChild2->row()->kelurahan;
		$passportno = $queryChild2->row()->passportno;
		$passportnm = $queryChild2->row()->passportnm;
		$sex = $queryChild2->row()->sex;
		$father_name = $queryChild2->row()->father_name;
		$provinsi = $queryChild2->row()->provinsi;
		$loccd = $queryChild2->row()->loccd;
		$sfno = $queryChild2->row()->sfno;
	    $tipe_perjalanan = $queryChild2->row()->tipe_perjalanan;

        //echo $queryChild2->row()->id;

        //jika $tipe_perjalanan, 1=umroh, 2=ziarah

        if($tipe_perjalanan == "1"){ //umroh
        	$umr = "UMR";
			$umro = "UMRO";
			$umroh = "UMROH";
        }if($tipe_perjalanan == "2"){ //umroh
        	$umr = "ZIA";
			$umro = "ZIAR";
			$umroh = "ZIARAH";
        }

        $stk	=	"SELECT a.[state], a.pricecode, a.fullnm, 'B001' as B001, 'SK004$umr' as SK004PRO
					 FROM klink_mlm2010.dbo.mssc a
					 WHERE a.loccd = '$loccd'";
		//echo $stk;
        $queryStk 	= $this->db->query($stk);
        $state		= $queryStk->row()->state;
        $pricecode	= $queryStk->row()->pricecode;
        $stk_fullnm	= $queryStk->row()->fullnm;
        $B001		= $queryStk->row()->B001;
        $SK004PRO	= $queryStk->row()->SK004PRO;


        $distCode = $dfno;

        //===========START flag_tipe = 3 MEMBER BARU ===================
        //JIKA flag_tipe = 3 (BUKAN MEMBER DAN USIA 18 KE ATAS), MAKA BUAT ID MEMBER BARU
        if($flag_tipe == '3'){
        	$distCode	= '';
        	$nNEXTNO 	= '0';
			$PREFIXNOW  = 'ID'.$umro;
	        $prefix		=	"SELECT a.[prefix], a.[next] FROM ecomm_membNextNo A with (REPEATABLEREAD) where a.prefix = '$PREFIXNOW'";
	        $qprefix 	= $this->db->query($prefix);

	        if($qprefix->num_rows() == 0){
	        	$preInsert	= "insert into ecomm_membNextNo([next], [prefix], startno) values(1,'$PREFIXNOW',1);";
	        	$nNEXTNO	= '1';
	        }else{
	        	$nNEXTNO		= $qprefix->row()->next;
				$updtPrefix = "update ecomm_membNextNo set [next] = [next] + 1 where prefix = '$PREFIXNOW'";
		        $qryupdtPrefix = $this->executeQuery($updtPrefix);
	        }

			$distCodeTemp =	'00000'.$nNEXTNO;
	        $distCode = $PREFIXNOW.'A'.trim(substr($distCodeTemp, -5));

			/*
	        $updtChild = "DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];

						 DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];

						 update ecomm_umroh_save_child
	        			  set dfno='$distCode',
	        				  approval_status = '1',
	        				  approvaldt = '".$today."',
	        				  updatedt = '".$today."',
	        				  updatenm = '".$this->username."'
	        			  where registerno = '".$regno."';

	        			  ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];

						  ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];
						  ";
			*/
			$this->db->trans_start();
			$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query("update ecomm_umroh_save_child
		        			  set dfno='$distCode',
		        				  approval_status = '1',
		        				  approvaldt = '".$today."',
		        				  updatedt = '".$today."',
		        				  updatenm = '".$this->username."'
		        			  where registerno = '".$regno."';");
			$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->trans_complete();
	        //echo $updtChild;
	        //$qryChild = $this->executeQuery($updtChild);
        //===========END flag_tipe = 3 MEMBER BARU ===================

        }elseif($flag_tipe == '1'){ //jika member lama
        	/*$updtChild = "DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];

						 DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];

						 update ecomm_umroh_save_child
        				  set dfno='$distCode',
        				  	  approval_status = '1',
        				  	  approvaldt = '".$today."' ,
	        				  updatedt = '".$today."',
	        				  updatenm = '".$this->username."'
        				  where registerno = '".$regno."';

	        			  ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];

						  ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];
						  ";*/
			$this->db->trans_start();
			$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query("update ecomm_umroh_save_child
		        			  set dfno='$distCode',
		        				  approval_status = '1',
		        				  approvaldt = '".$today."',
		        				  updatedt = '".$today."',
		        				  updatenm = '".$this->username."'
		        			  where registerno = '".$regno."';");
			$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->trans_complete();
	        //$qryChild = $this->executeQuery($updtChild);
        }elseif($flag_tipe == '2'){ //jika bkn member dibawah 18tahun
        		$this->db->trans_start();
			$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query("update ecomm_umroh_save_child
		        			  set dfno=null,
		        				  approval_status = '1',
		        				  approvaldt = '".$today."',
		        				  updatedt = '".$today."',
		        				  updatenm = '".$this->username."'
		        			  where registerno = '".$regno."';");
			$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
			$this->db->trans_complete();
	        //$qryChild = $this->executeQuery($updtChild);
        }

        //===========START flag_tipe = 3 atau flag_tipe = 1 MAKA DAPAT VOUCHER ===================
        if($flag_tipe == '1' || $flag_tipe == '3'){
        	$VnNEXTNO 	= '0';
			$VPREFIXNOW = 'V'.$umr;
	        $Vprefix	= "SELECT a.[prefix], a.[next] FROM ecomm_voucherNextNo A with (REPEATABLEREAD) where a.prefix = '$VPREFIXNOW'";
	        $Vqprefix 	= $this->db->query($Vprefix);

	        if($Vqprefix->num_rows() == 0){
	        	$VpreInsert	= "insert into ecomm_voucherNextNo([next], [prefix], startno) values(1,'$VPREFIXNOW',1);";
	        	$VnNEXTNO	= '1';
	        }else{
	        	$VnNEXTNO		= $Vqprefix->row()->next;
				$VupdtPrefix = "update ecomm_voucherNextNo set [next] = [next] + 1 where prefix = '$VPREFIXNOW'";
		        $VqryupdtPrefix = $this->executeQuery($VupdtPrefix);
	        }

			$vUmrCodeTemp 	= "0000$VnNEXTNO";
	        $vUmrCode 		= $VPREFIXNOW.'A'.trim(substr($vUmrCodeTemp, -5));

			//echo $vUmrCodeTemp;
			//echo $VPREFIXNOW;

			//-- INSERT TO ecomm_umroh_save_mut DEBIT 2000000 AS VOUCHER
        	$checkVcr	= "SELECT TOP 1 * FROM ecomm_umroh_save_mut A WHERE  A.id='$id' AND A.[description]='VOUCHER'";
			$qcheckVcr 	= $this->db->query($checkVcr);

			if($qcheckVcr->num_rows() == 0){
				$istMutVcr	= "INSERT INTO ecomm_umroh_save_mut(mutate_flag, pay_status, is_voucher, tot_fund, createdt,
                                                 createnm, id, [description], voucherno, paymentID, paymentRef)
				               VALUES('1', '1', '2', '2000000', '$createdt',
				                      '$createnm', '$id', 'VOUCHER', '$vUmrCode', '$vUmrCode', '$vUmrCode')";
				$qistMutVcr = $this->executeQuery($istMutVcr);

                //-- INSERT INTO klink_mlm2010.dbo.tcvoucher 2000000
                $updVAtblUmr = "UPDATE A
							    SET	   A.novac=B.novac
							    FROM   ecomm_umroh_save_child A
								   	   INNER JOIN klink_mlm2010.dbo.msmemb B ON A.dfno COLLATE SQL_Latin1_General_CP1_CS_AS=B.dfno
							    WHERE  A.id = '$id'";
		        //$qupdVAtblUmr = $this->executeQuery($updVAtblUmr);

				$this->db->trans_start();
				$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->query("$updVAtblUmr;");
				$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->trans_complete();

				$istKlinkVcr= "INSERT INTO klink_mlm2010.dbo.tcvoucher(VoucherNo, DistributorCode, IssueDate, BonusMonth, BonusYear, ExpireDate,
                                                        VoucherAmt, VoucherAmtCurr, CurrencyNote, createnm, createdt, updatenm,
                                                    updatedt, voucherkey, [status], claimstatus, PT_SVRID, vchtype, countrycode, voucherkey_ori)
				                VALUES('$vUmrCode', '$distCode', '$createdt', MONTH('$createdt'), YEAR('$createdt'), DATEADD(M, 3, '$createdt'),
				                       '2000000', '2000000', 'IND', '$createnm', '$createdt', '$createnm',
				                       '$createdt', '$vUmrCode', '0', '0', 'ID', 'C', 'ID', '$vUmrCode' + RIGHT('000' + '$id', 4))";
				//echo $istKlinkVcr;
                $qistKlinkVcr = $this->executeQuery($istKlinkVcr);
			}

        }
        //===========END flag_tipe = 3 atau flag_tipe = 1 DAPAT VOUCHER ========================


        //===============START IF flag_tipe = 3 JOIN NEW DISTRIBUTOR====================================
        //INSERT MUTATION FOR STARTERKIT
        if($flag_tipe == '3'){
        	$qryMurSK = "SELECT TOP 1 * FROM ecomm_umroh_save_mut A WHERE  A.id='$id' AND A.[description]='REGISTRATION'";
			$qryMurSKX 	= $this->db->query($qryMurSK);

	        if($qryMurSKX->num_rows() == 0){
	        	/*
				$id $loccd  $flag_tipe $registerno $seqno $dfno $fullnm $idno
				$birthplace	$birthdate $passwordx $tel_hp $email $departureid $remarks $depart_status $post_status
				$post_dt $createdt $createnm $datex $bonusmth $updatenm $flag_tipe $idparent $addr1
				$flag_paytype $approvaldt $kecamatan $kota $kelurahan $passportno $passportnm $sex
				$father_name $provinsi $loccd $sfno $id	 $loccd $flag_tipe

		        $state	$pricecode	$stk_fullnm	$B001  $SK004PRO
	        	*/

	        	  //-- RANDOMIZE STRING FOR SECNO
	    		$secno		= "select char(rand()*26+65)+char(rand()*26+65)+char(rand()*26+65)+char(rand()*26+65)+char(rand()*26+65) as secno";
				$qsecno 	= $this->db->query($secno);
        		$secnoR		= $qsecno->row()->secno;

	    		$checkHdr	= "SELECT TOP 1 * FROM ecomm_trans_hdr A WHERE A.orderno='$registerno'";
	    		$qcheckHdr 	= $this->db->query($checkHdr);

				if($qcheckHdr->num_rows() == 0){ //jika tidak ada maka insert
					//-- INSERT INTO TBL TRANSACTION HDR ecomm_trans_hdr
					$instHdrComm	= "INSERT INTO ecomm_trans_hdr(orderno,bankaccno,token,id_memb,nmmember,total_pay,
																	total_bv,pricecode,bonusmonth,datetrans,idstk,nmstkk,
																	[status],secno,flag_trx,sentTo,CNstatus,KWstatus,IPstatus,
																	dateKW,dateCN,dateIP,usrKW,eod_status,status_vt_pay,
																	status_vt_app_dt,status_vt_reject_dt,payShip,payAdm,is_umroh)
										VALUES('$registerno','$registerno','$registerno','$distCode','$fullnm',200000,
												'0','$pricecode','$bonusmth','$createdt','$loccd','$stk_fullnm',
												'0','$secnoR','W','1','0','0','0',
												'$createdt','$createdt','$createdt',NULL,'0','1',
												'$createdt',NULL,'0','0','1')";
					//echo $instHdrComm;
					$qinstHdrComm = $this->executeQuery($instHdrComm);

					//-- INSERT INTO TBL TRANSACTION DET PRD ecomm_trans_det_prd
					$instDetComm	= "INSERT INTO ecomm_trans_det_prd(orderno,prdcd,prdnm,qty,bvr,dpr,pricecode,sentTo)
					     			   VALUES('$registerno','$SK004PRO',NULL,'1','0','200000','$pricecode','2')";
					$qinstDetComm 	= $this->executeQuery($instDetComm);

		    		//-- INSERT INTO TBL TRANSACTION DET PAY ecomm_trans_det_prd
				    $instPayComm	= "INSERT INTO ecomm_trans_paydet (orderno,seqno,paytype,docno,payamt,deposit,notes,paystatus)
				    				   VALUES('$registerno','1','VREG$umroh','$vUmrCode'+'-REG','200000','0','REG FROM $umroh',NULL)";
					$qinstPayComm 	= $this->executeQuery($instPayComm);
				}


	        	//Start insert to table klink_mlm2010.dbo.msmemb for new distributor
			    $instMsmemb	=	"INSERT INTO klink_mlm2010.dbo.msmemb(addr1, branch, birthdt, benbirthdt, birth2dt, createdt, etdt, jointdt,
													 lastlogin, lupddt, receivedt, trdt, updatedt, createnm, dfno, memberid,
													 fullnm, idno, addr3, addr2, city, bnsstmsc, loccd, loccd_co,
													 trcd, sex, sfno, sfno_reg, sponsorid, sponsorregid, regtype,
													 tel_hp, email, [password], postcd, [state], [status])
								 VALUES('$addr1', '$B001', '$birthdate', '$datex', '$datex', '$datex', '$datex', '$datex',
									    '$datex', '$datex', '$datex', '$datex', '$datex', '$createnm', '$distCode', '$distCode',
									    '$fullnm', '$idno', '$kecamatan', '$kelurahan', '$kota', '$loccd', '$loccd', '$loccd',
									    '$registerno', '$sex', '$sfno', '$sfno','$sfno', '$sfno', '$SK004PRO',
									    '$tel_hp', '$email', '$passwordx', '000004', '$state', '1')";
		        $qinstMsmemb = $this->executeQuery($instMsmemb);

	        	//Start insert to table klink_mlm2010.dbo.starterkit for new distributor
			    $instSkit	=	"INSERT INTO klink_mlm2010.dbo.starterkit([status], createdt, activate_dt, updatedt, formno,
			    														  vchkey, activate_dfno, createnm, activate_by, prdcd)
							     VALUES('2', '$datex', '$datex', '$datex', '$distCode', '$distCode', '$distCode', '$createnm', '$createnm', '$SK004PRO')";
		        $qinstSkit = $this->executeQuery($instSkit);

	        	//Start update VA to table klink_mlm2010.dbo.ecomm_umroh_save_child for new distributor
				$updVAtblUmr = "UPDATE A
							    SET	   A.novac=B.novac
							    FROM   ecomm_umroh_save_child A
								   	   INNER JOIN klink_mlm2010.dbo.msmemb B ON A.dfno COLLATE SQL_Latin1_General_CP1_CS_AS=B.dfno
							    WHERE  A.id = '$id'";
		        //$qupdVAtblUmr = $this->executeQuery($updVAtblUmr);

				$this->db->trans_start();
				$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->query('DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->query("$updVAtblUmr;");
				$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->query('ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];');
				$this->db->trans_complete();


				//-- INSERT TO ecomm_umroh_save_mut DEBIT 200000 AS REGISTRATION
	        	//Start insert to table klink_mlm2010.dbo.ecomm_umroh_save_mut for new distributor
			    $instMut	= "INSERT INTO ecomm_umroh_save_mut(mutate_flag, pay_status, is_voucher, tot_fund, createdt,
												 createnm, id, [description], voucherno, paymentID, paymentRef)
							    VALUES('1', '1', '1', '200000', '$datex',
									   '$createnm', '$id', 'REGISTRATION', '$distCode', '$distCode', '$distCode')";
		        $qinstMut = $this->executeQuery($instMut);

	        }
        }
        //=============== END IF flag_tipe = 3 JOIN NEW DISTRIBUTOR====================================

        //========================================= END TAMBAHAN HILAL =======================================


        $slcChild = "select id from ecomm_umroh_save_child
                    where registerno = '".$regno."'";
        $queryChild2 = $this->db->query($slcChild);
        if($queryChild2->num_rows() > 0){
            foreach($queryChild2->result() as $data)
            {
                $idChild = $data->id;
            }
        }else{
            echo "no data";
        }

        $updtMut = "update ecomm_umroh_save_mut set pay_status = '1',
                    app_luarkota = '1',app_luarkota_date = '".$today."',
                    app_nm = '".$this->username."'
                    where id = '".$idChild."'";
        //echo $updtMut;
        $qryMut = $this->executeQuery($updtMut);

        //if(!$qryChild && !$qryMut){
        if(!$qryMut){
		  return 0;
		}else{
		  return 1;
		}
        //return $res;
    }

    function detPayUmroh($regNo){
        /*$slc = "select a.registerno,a.dfno,a.fullnm,b.tot_fund
                 from ecomm_umroh_save_child a
                 	inner join ecomm_umroh_save_mut b on (b.id = a.id)
                 where b.pay_status = '1' and a.registerno = '".$regNo."'";*/

        $slc = "select a.voucherno,a.is_voucher,a.tot_fund,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp as hpChild,
                b.birthplace,b.birthdate,b.departureid,b.id,b.loccd,
                c.departuredesc, c.desc2, c.departuredt,d.dfno as idrekruiter,
                d.fullnm as rekruiternm,d.tel_hp as telHp,e.secno,e.CNno,b.novac
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    left outer join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
                    left outer join ecomm_trans_hdr e on(b.registerno = e.orderno)
                where b.registerno = '".$regNo."' and
                a.pay_status = '1' and b.approval_status = '1'
                and a.is_voucher = '2'";
        //echo $slc;

        $result = $this->getRecordset($slc);
        return $result;
    }

    function getPrintKW($regno){
         $slc = "select a.orderno,a.id_memb,a.nmmember,
                a.total_pay,a.KWno,a.CNno,a.IPno,a.idstk
                from ecomm_trans_hdr a
                	inner join ecomm_umroh_save_child b on(a.orderno = b.registerno)
                where a.orderno = '".$regno."' and b.approval_status = '1'";

        $result = $this->getRecordset($slc);

        if($result == null){
        	$slc = "select a.registerno as orderno,a.DFNO as id_memb,a.fullnm as nmmember,
	                b.tot_fund as total_pay,NULL AS KWno,NULL AS CNno,NULL AS IPno,a.loccd as idstk
	                from ecomm_umroh_save_child a
	                	inner join ecomm_umroh_save_mut b on(a.id = b.id)
	                where a.registerno = '".$regno."' and
	                	  a.approval_status = '1' and
	                	  (b.description like 'DP K-Smart %' or b.description like 'Payment Manual%')";
			$result = $this->getRecordset($slc);
        }

        //echo $slc;
        //echo "hasilnya $result";
        return $result;
    }

    function getPrintVch($regno){
        $slc = "select a.id,a.voucherno,c.createdt,a.tot_fund,
                b.dfno,b.fullnm,b.registerno,c.claimstatus
                 from ecomm_umroh_save_mut a
                 	INNER join ecomm_umroh_save_child b on(a.id = b.id)
                    inner join klink_mlm2010.dbo.tcvoucher c on(a.voucherno = c.VoucherNo COLLATE SQL_Latin1_General_CP1_CS_AS)
                 where b.registerno = '".$regno."' and a.is_voucher = '2'
                 and a.[description] = 'VOUCHER' and a.pay_status = '1' and b.approval_status='1'";
        //echo $slc;
        $result = $this->getRecordset($slc);
        return $result;
    }


	/*kwitansi (cahyono)*/
	  function getPrintInst($regno){
        $slc = "
		SELECT top(1) * FROM(
		select a.dfno, a.createnm, a.fullnm, GETDATE() AS now, b.tot_fund, a.createdt, (b.createdt) as mins
		from ecomm_umroh_save_child a
		LEFT JOIN ecomm_umroh_save_mut b
		ON b.id=a.id
		where a.registerno = '".$regno."'
		) y
		";
        //echo $slc;
        $result = $this->getRecordset($slc);
        return $result;
    }


    /*function getDtVchUmroh($regnos){
        $slc = "select a.id,a.voucherno,a.createdt,a.tot_fund,
                b.dfno,b.fullnm,b.registerno
                 from ecomm_umroh_save_mut a
                 	INNER join ecomm_umroh_save_child b on(a.id = b.id)
                    inner join klink_mlm2010.dbo.tcvoucher c on(a.voucherno = c.VoucherNo COLLATE SQL_Latin1_General_CP1_CS_AS)
                 where b.registerno = '".$regnos."' and a.is_voucher = '2'
                 and a.[description] = 'VOUCHER' AND c.claimstatus = '0'";
        //echo $slc;
        $result = $this->getRecordset($slc);
        return $result;
    }*/

    function getListRegUmroh() {
		$data = $this->input->post(null, true);
		//upd by DION 11/01/2016
		$qry = "SELECT *
				FROM V_ECOMM_UMROH_LIST_REG a
				WHERE CONVERT(VARCHAR(10), a.createdt2, 111) BETWEEN '".$data['umreg_from']."' and '".$data['umreg_to']."'
				AND a.pay_status = '".$data['pay_status']."' AND
				a.approval_status = '".$data['pay_status']."'";
	    //echo $qry;
		$result = $this->getRecordset($qry, null, $this->db1);
        return $result;
	}

    function getBankList(){
        $slc = "select id, bankCode, bankDesc as bankDescSGO,
                charge_connectivity, charge_admin,
        			   status, bankDisplayNm as bankDesc, status_be
        		from ecomm_bank where status_be='1' order by bankDesc";
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function saveInstallmentUmroh($dt){
        $this->db = $this->load->database('db_ecommerce', true);
        $slc = "select a.id from ecomm_umroh_save_child a where a.registerno = '".strtoupper($dt['regnos'])."'";
        $querySlc = $this->db->query($slc);
        if($querySlc->num_rows() > 0){
            foreach($querySlc->result() as $data)
            {
                $idChild = $data->id;
            }
        }

        $insInstallMut = "insert into ecomm_umroh_save_mut (id,mutate_flag,tot_fund,createnm,description,is_voucher,paymentID,paymentRef,pay_status,kd_bank)
                            values(".$idChild.",'2',".$dt['amtUmroh'].",'".$this->username."','Cicilan K-smart Umroh','0','".$dt['regnos']."','".$dt['paymentreff']."','1','".$dt['banklist']."')";

        $qryMut = $this->executeQuery($insInstallMut);

        $insLog = "insert into ecomm_umroh_pre_cicilan(regno,idmember,nmmember,tot_fund,createnm,status)
                values('".strtoupper($dt['regnos'])."','".$dt['idmember']."','".$dt['nmmember']."',".$dt['amtUmroh'].",'".$this->username."','1')";
        $qryLog = $this->executeQuery($insLog);

        if($qryMut > 0 && $qryLog > 0){
            return 1;
        }else{
            return 0;
        }
    }

    function getListPost($dt){ //, $sortby = "1", $sortpos = "1"

		//var_dump($dt);
		$dtFrom1 	= $dt['date_from'];
		$dtTo1 		= $dt['date_to'];
		$status 	= $dt['status'];
		$sortby 	= $dt['sort'];
		$sortpos 	= $dt['acdc'];

		$dtFrom = explode('/',$dtFrom1);
		$dtTo 	= explode('/',$dtTo1);

		$dtFromX = $dtFrom[2]."/".$dtFrom[1]."/".$dtFrom[0];
		$dtToX 	= $dtTo[2]."/".$dtTo[1]."/".$dtTo[0];

		//0=UNPOSTED, 1=POSTED, 2=CANCEL/REJECED
		$qryStatus = " AND a.post_status = '$status' ";

		if ($sortpos == "0"){
			$sortposx = "ASC";
		}else{
			$sortposx = "DESC";
		}

		if ($sortby == "0"){ //1=ID MEMBER, 2=NAME, 3=SALDO
			$orderby = " ORDER BY A.dfno $sortposx";
		}if ($sortby == "1"){
			$orderby = " ORDER BY A.fullnm $sortposx";
		}if ($sortby == "2"){
			$orderby = " ORDER BY A.tot_fund $sortposx";
		}

		$slc = "SELECT A.id, A.createdt, A.createdt2, A.dfno, A.fullnm, ISNULL(A.installment_c,0) AS installment_c,
		 		 	    ISNULL(A.installment_d,0) AS installment_d, A.voucherno, A.is_voucher, A.registerno, A.CNno,
		 		 	    A.KWno, a.novac, a.IPno, a.installment_createdt, a.addr1, a.kelurahan, a.kecamatan, a.kota, a.tel_hp,
		 		 	    a.tipe_perjalanan, a.post_status, a.depart_status, a.tot_fund
		 		 FROM V_ECOMM_UMROH_LIST_REG2 A
		 		 WHERE (CONVERT(VARCHAR(10), a.createdt, 111) between '$dtFromX' and '$dtToX') $qryStatus
		 		 $orderby";
		//echo  "$sortpos \n $sortby \n$slc";
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;

    }

	function getUpdatePost($registerno, $postVal, $username) {
		if($postVal == '1'){ //approved & departured
			$paramChild = " depart_status = '1', post_status = '1', post_dt = GETDATE() ";
			$paramMut = " B.post_flag = '1', B.postdt = GETDATE(), B.postnm = '$username', B.app_walkin = '1', B.app_luarkota = '1' ";
		}elseif($postVal == '2'){ //approved & departured
			$paramChild = " depart_status = '2', post_status = '2', post_dt = GETDATE() ";
			$paramMut = " B.post_flag = '2', B.postdt = GETDATE(), B.postnm = '$username', B.app_walkin = '2', B.app_luarkota = '2' ";
		}elseif($postVal == '0'){ //approved & departured
			$paramChild = " depart_status = '0', post_status = '0', post_dt = GETDATE() ";
			$paramMut = " B.post_flag = '0', B.postdt = GETDATE(), B.postnm = '$username', B.app_walkin = '0', B.app_luarkota = '0' ";
		}

		$sql = "DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];
				DISABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];
				UPDATE ecomm_umroh_save_child SET $paramChild WHERE registerno IN ($registerno);
				ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child];
				ENABLE TRIGGER [ecomm_umroh_save_child_TO_msmemb] ON [dbo].[ecomm_umroh_save_child];

				DISABLE TRIGGER [ecomm_umroh_save_mut_DEL2] ON [dbo].[ecomm_umroh_save_mut];
				UPDATE B
				SET $paramMut
				FROM ecomm_umroh_save_child A
					 INNER JOIN ecomm_umroh_save_mut B ON A.id=B.id
				WHERE A.registerno IN ($registerno);
				ENABLE TRIGGER [ecomm_umroh_save_mut_DEL2] ON [dbo].[ecomm_umroh_save_mut];";

		//echo "hasilnya == $sql";
        $this->db->trans_start();
		$this->db->query($sql);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
		    //$this->db->trans_rollback();
		    $result = FALSE;
		} else {
		    //$this->db->trans_commit();
		    $result = TRUE;
		}
        return $result;
	}

    function getListInstall($dt){

		$dtFrom = explode('/',$dt['date_from']);
		$dtTo 	= explode('/',$dt['date_to']);
		$tipe_perjalanan 	= $dt['journeyType'];
		$status 			= $dt['status'];
		$ordertype 			= $dt['ordertype'];
		$orderby 			= $dt['orderby'];

		//echo "$tipe_perjalanan";
		if($ordertype == 'dfno'){
			$qryOrderby = " ORDER BY A.dfno ";
		}elseif($ordertype == 'fullnm'){
			$qryOrderby = " ORDER BY A.fullnm ";
		}if($ordertype == 'total'){
			$qryOrderby = " ORDER BY A.total ";
		}

		if($orderby == 'asc'){
			$qryOrderby = "$qryOrderby ASC";
		}else{
			$qryOrderby = "$qryOrderby DESC ";
		}

		if($tipe_perjalanan == '0'){
			$qryType = "";
		}else{
			$qryType = " AND tipe_perjalanan='$tipe_perjalanan' ";
		}

		$pStatus = null;
		if($status != 'all'){
			$pStatus = " AND A.post_status = '$status' ";
		}

		$dtFromX = $dtFrom[2]."/".$dtFrom[1]."/".$dtFrom[0];
		$dtToX 	= $dtTo[2]."/".$dtTo[1]."/".$dtTo[0];

		/*
        $slc = "select a.id,a.tot_fund,b.registerno,b.dfno,b.fullnm,c.novac
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on(a.id = b.id)
                    inner join klink_mlm2010.dbo.msmemb c on (b.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS)
                where a.mutate_flag = '2' and a.kd_bank = '".$dt['banklist']."'
                and (CONVERT(VARCHAR(10), a.createdt, 111) between '$dtFromX' and '$dtToX')
                --and a.pay_status = '1'  ";
		 *
		 */
		if($status == '1'){
			 $upd = "  update b
						set b.pay_status='1'
						from ecomm_umroh_save_mut b
							 inner join ecomm_umroh_save_child a on a.id=b.id
						where b.pay_status='0' and
							  b.is_voucher='0' and
							  a.approval_status = '1'";
			 $qryUpd = $this->executeQuery($upd);
		}
//A.dfno, A.fullnm,
		/* $slc = "SELECT A.id, A.createdt, A.createdt2, A.dfno, A.fullnm, ISNULL(A.installment_c,0) AS installment_c,
		 		 	    ISNULL(A.installment_d,0) AS installment_d, A.voucherno, A.is_voucher, A.registerno, A.CNno,
		 		 	    A.KWno, a.novac, a.IPno, a.installment_createdt, a.addr1, a.kelurahan, a.kecamatan, a.kota, a.tel_hp,
		 		 	    a.tipe_perjalanan, A.post_status, A.depart_status
		 		 FROM V_ECOMM_UMROH_LIST_REG2 A
		 		 WHERE (CONVERT(VARCHAR(10), a.createdt, 111) between '$dtFromX' and '$dtToX') $qryType $pStatus ";
		 *
		 */
		$slc = "SELECT A.id, A.createdt, A.createdt2, A.dfno, A.fullnm, ISNULL(A.installment_c,0) AS installment_c,
		 		 	    ISNULL(A.installment_d,0) AS installment_d, A.voucherno, A.is_voucher, A.registerno, A.CNno,
		 		 	    A.KWno, a.novac, a.IPno, a.installment_createdt, a.addr1, a.kelurahan, a.kecamatan, a.kota, a.tel_hp,
		 		 	    a.tipe_perjalanan, A.post_status, A.depart_status, A.TOTAL
		 		 FROM V_ECOMM_UMROH_LIST_REG3 A
		 		 WHERE (CONVERT(VARCHAR(10), a.createdt, 111) between '$dtFromX' and '$dtToX') $qryType $pStatus $qryOrderby";
		//echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function getListInstallMDR($dt){

		$dtFrom = explode('/',$dt['date_from']);
		$dtTo 	= explode('/',$dt['date_to']);
		$tipe_perjalanan 	= $dt['dataView'];
		$status 			= $dt['status'];
		$ordertype 			= $dt['ordertype'];
		$orderby 			= $dt['orderby'];

		//echo "$tipe_perjalanan";
		if($ordertype == 'dfno'){
			$qryOrderby = " ORDER BY A.ID_MEMBER ";
		}elseif($ordertype == 'fullnm'){
			$qryOrderby = " ORDER BY A.NAMA ";
		}if($ordertype == 'total'){
			$qryOrderby = " ORDER BY SUM(A.Saldo) ";
		}

		if($orderby == 'asc'){
			$qryOrderby = "$qryOrderby ASC";
		}else{
			$qryOrderby = "$qryOrderby DESC ";
		}

		/* $pStatus = null;
		if($status != 'all'){
			$pStatus = " AND b.post_status = '$status' ";
		}*/

		if($tipe_perjalanan == '0' && $status == 'all'){
			$param = " ";
		}elseif($tipe_perjalanan == '0' && $status != 'all'){
			$param = " WHERE b.post_status = '$status' ";
		}else{
			$param = " where b.tipe_perjalanan='$tipe_perjalanan' AND b.post_status = '$status' ";
		}

		$dtFromX = $dtFrom[2]."/".$dtFrom[1]."/".$dtFrom[0];
		$dtToX 	= $dtTo[2]."/".$dtTo[1]."/".$dtTo[0];

		 $upd = "  update b
					set b.pay_status='1'
					from ecomm_umroh_save_mut b
						 inner join ecomm_umroh_save_child a on a.id=b.id
					where b.pay_status='0' and
						  b.is_voucher='0' and
						  a.approval_status = '1'";
		 $qryUpd = $this->executeQuery($upd);

		 $slc = "SELECT '\\\\\\' AS BACKSLASH, '~' AS ENDRPT, '20150708' AS STARTDT, '20250708' AS ENDDT,
		 	 			A.Nomor_MVA, A.NAMA, A.ID_MEMBER, SUM(A.Saldo) AS SALDO, b.addr1, b.kelurahan,
		 	 			b.kecamatan, b.kota, b.tel_hp, count(A.ID_MEMBER) as totalDep
				 FROM V_ECOMM_UMROH_LIST_MDR A
				 	  LEFT OUTER JOIN ecomm_umroh_save_child B ON A.ID_MEMBER=B.dfno and b.approval_status='1'
				  $param
				 GROUP BY A.Nomor_MVA, A.NAMA, A.ID_MEMBER, b.addr1, b.kelurahan,
		 	 			b.kecamatan, b.kota, b.tel_hp
				 $qryOrderby";
		//echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function getBankInfo($dt){
        $slc = "select * from ecomm_umroh_bank where bankCode ='".$dt['banklist']."'";
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

	 function getListUmrToWeb($dt){

		$journey_type 	= $dt['journey_type'];

		if($journey_type == 0){
			$param = "";
		}else{
			$param = " AND tipe_perjalanan = '$journey_type' ";
		}

		$dtFrom = explode('/',$dt['date_from']);
		$dtTo 	= explode('/',$dt['date_to']);

		$dtFromX = $dtFrom[2]."/".$dtFrom[1]."/".$dtFrom[0];
		$dtToX 	= $dtTo[2]."/".$dtTo[1]."/".$dtTo[0];

		 $slc = "SELECT a.dfno, a.fullnm, a.idno, a.sex, a.birthplace,
					   CONVERT(VARCHAR(10),a.birthdate,103) as birthdt,
				       CASE
				       		WHEN a.addr1 IS NULL THEN ''
				            ELSE a.addr1
				       END + ' ' +
				       CASE
				       		WHEN a.kota IS NULL THEN ''
				            ELSE a.kota
				       END + ' ' +
				       CASE
				       		WHEN a.kecamatan IS NULL THEN ''
				            ELSE a.kecamatan
				       END + ' ' +
				       CASE
				       		WHEN a.kelurahan IS NULL THEN ''
				            ELSE a.kelurahan
				       END as addr,
				       '' as relation, a.tel_hp, '' as tlp ,CONVERT(VARCHAR(10),a.createdt,20) as createdt,
				       CONVERT(VARCHAR(10),b.departuredt,20) as departuredt, a.passportno, a.passportnm
				FROM ecomm_umroh_save_child a
					 inner join ecomm_umroh_departure b on a.departureid=b.id
				where a.approval_status='1' AND
					 (CONVERT(VARCHAR(10), a.createdt, 111) between '$dtFromX' and '$dtToX')
					 $param";
		//echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function getListUmrToWebCSV($dt){

		$journey_type 	= $dt['journey_type'];

		if($journey_type == 0){
			$param = "";
		}else{
			$param = " AND tipe_perjalanan = '$journey_type' ";
		}

		$dtFrom = explode('/',$dt['date_from']);
		$dtTo 	= explode('/',$dt['date_to']);

		$dtFromX = $dtFrom[2]."/".$dtFrom[1]."/".$dtFrom[0];
		$dtToX 	= $dtTo[2]."/".$dtTo[1]."/".$dtTo[0];

		 $slc = "SELECT a.dfno + ';' + a.fullnm + ';' + a.idno + ';' +
		 				CASE
				       		WHEN a.sex = 'M' THEN 'L'
				       		WHEN a.sex = 'F' THEN 'P'
				       	END  + ';' +
		 				a.birthplace + ';' +
					   CONVERT(VARCHAR(10),a.birthdate,103) + ';' +
				       CASE
				       		WHEN a.addr1 IS NULL THEN ''
				            ELSE a.addr1
				       END + ' ' +
				       CASE
				       		WHEN a.kota IS NULL THEN ''
				            ELSE a.kota
				       END + ' ' +
				       CASE
				       		WHEN a.kecamatan IS NULL THEN ''
				            ELSE a.kecamatan
				       END + ' ' +
				       CASE
				       		WHEN a.kelurahan IS NULL THEN ''
				            ELSE a.kelurahan
				       END + ';' +
				       '' + ';' + a.tel_hp + ';' + CONVERT(VARCHAR(10),a.createdt,20) + ';' +
				       CONVERT(VARCHAR(10),b.departuredt,20) + ';' + a.passportno + ';' + a.passportnm
				       as recNya
				FROM ecomm_umroh_save_child a
					 inner join ecomm_umroh_departure b on a.departureid=b.id
				where a.approval_status='1' AND
					  (CONVERT(VARCHAR(10), a.createdt, 111) between '$dtFromX' and '$dtToX')
					  $param";
		//echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result;
    }

    function showRegnoInfo($regno){


        $slc = "SELECT a.registerno,a.dfno,a.fullnm,a.departureid,a.tipe_perjalanan,
                b.departuredesc,b.departuredt,sum(c.tot_fund) AS saldo
                FROM ecomm_umroh_save_child a
                	INNER JOIN ecomm_umroh_departure b ON(a.departureid = b.id)
                    INNER JOIN ecomm_umroh_save_mut c ON(a.id = c.id)
                WHERE a.registerno = '".$regno."'
                AND c.mutate_flag = '2' AND c.is_voucher = '0' AND c.pay_status = '1'
                GROUP BY a.registerno,a.dfno,a.fullnm,a.departureid,
                b.departuredesc,b.departuredt,a.tipe_perjalanan";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }

	function cancelUmroh($idmember){

		$sql="SELECT a.registerno, a.dfno, a.fullnm, a.novac, a.credit AS installment_c, a.debit AS installment_d,
					 a.saldo AS sisa, a.banknm, a.bankaccno, a.bankaccnm, a.tipe_perjalanan
			  FROM V_ECOMM_UMROH_SALDO a
			  WHERE a.tipe_perjalanan = '1' AND a.dfno = '$idmember'";
		//echo $sql;
		$result = $this->getRecordset($sql,null,$this->db1);
		return $result;
	}

	function getMemberInfo($idmember) {
		$sql="SELECT a.registerno, a.dfno, a.fullnm, a.novac, a.credit AS installment_c, a.debit AS installment_d,
					 a.saldo AS sisa, a.banknm, a.bankaccno, a.bankaccnm, a.tipe_perjalanan, a.depart_status
			  FROM V_ECOMM_UMROH_SALDO a
			  WHERE a.dfno = '$idmember'";
		$result = $this->getRecordset($sql,null,$this->db1);
		return $result;
	}

	function getMemberInfoCancel($idmember) {
		$sql="SELECT a.registerno, a.dfno, a.fullnm, a.novac, a.credit AS installment_c, a.debit AS installment_d,
					 a.saldo AS sisa, a.banknm, a.bankaccno, a.bankaccnm, a.tipe_perjalanan, a.depart_status, a.approval_status
			  FROM V_ECOMM_UMROH_SALDO_DION a
			  WHERE a.dfno = '$idmember'";
		$result = $this->getRecordset($sql,null,$this->db1);
		return $result;
	}

	function checkCancel($registerno){
		$sql = "SELECT a.registerno, a.depart_status, a.approval_status, a.dfno, a.id
				FROM ecomm_umroh_save_child a
				WHERE a.registerno = '$registerno' AND a.depart_status = '0' AND a.approval_status = '1'";
		$res = $this->getRecordset($sql, null, $this->db1);
		return $res;
	}

	function getMutation($id) {
		$sql = "SELECT a.*, b.depart_status, b.fullnm, b.idno, b.dfno, b.registerno
				FROM
				db_ecommerce.dbo.ecomm_umroh_save_mut a
				LEFT OUTER JOIN
				db_ecommerce.dbo.ecomm_umroh_save_child b
				ON
				(a.id = b.id)
				WHERE b.dfno = '$id' and b.approval_status = '1'";
		$res = $this->getRecordset($sql, null, $this->db1);
		return $res;
	}

	function saveCancelUmroh($dfno, $ket){

		$cek = "select max(id) as id
				from ECOMM_UMROH_KETERANGAN";
		$hasil = $this->getRecordset($cek,null,$this->db1);
		$seq_id = $hasil++;

		$sqlUpd = "DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child]
					GO
					UPDATE A
					SET A.depart_status='2'
					FROM ecomm_umroh_save_child A
					WHERE  a.dfno in ('$dfno');

					ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child]
					GO";
		$resultUpd = $this->executeQuery($sqlUpd,$this->db1);

		$sql = "INSERT INTO [dbo].[ECOMM_UMROH_KETERANGAN] (dfno, ket, id)
				VALUES ('$dfno', '$ket', $seq_id)";
		$result = $this->executeQuery($sql,$this->db1);

		$ambil_seq = "select id from ECOMM_UMROH_KETERANGAN
					where dfno = '$dfno'";
		$ambil = $this->getRecordset($ambil_seq,null,$this->db1);
		return $ambil;

	}

	function getBatchNo($prefix) {
		$year = date("y");
		$month = date("m");
		//$date = date("d");

		$seqno = $prefix.$year.$month;
		$qry = "SELECT * FROM db_ecommerce.dbo.cancel_umroh_seq a WHERE a.seqno = '$seqno'";
		$ambil = $this->getRecordset($qry,null,$this->db1);

		if($ambil != null) {
			$nextRound = sprintf("%03s", $ambil[0]->urut);
			$noBatch = $seqno."-".$nextRound;

			$upd = "UPDATE db_ecommerce.dbo.cancel_umroh_seq
					SET urut = urut + 1
					WHERE seqno = '$seqno'";
			$result = $this->executeQuery($upd,$this->db1);
		} else {
			$ins = "INSERT INTO db_ecommerce.dbo.cancel_umroh_seq (seqno, urut) VALUES ('$seqno', '2')";
			$resultUpd = $this->executeQuery($ins,$this->db1);

			$nextRound = sprintf("%03s", 1);
			$noBatch = $seqno."-".$nextRound;
		}

		return $noBatch;
	}

	function saveCancelUmroh2($batchno, $data) {
		//BATAL DIPAKE KARENA MENGGUNAKAN NO URUT AUTO INCREMENT
		/*
		$cek = "select max(id) as id
				from ECOMM_UMROH_KETERANGAN";
		$hasil = $this->getRecordset($cek,null,$this->db1);
		$seq_id = $hasil[0]->id + 1;
		*/


		//disable dulu..

		$updStatus = 0;
		$insKeterangan = 0;
		$insMutasi = 0;

		$sqlUpd = " DISABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child]
					UPDATE A
					SET A.depart_status='2'
					FROM ecomm_umroh_save_child A
					WHERE  a.registerno = '".$data['registerno']."';
					ENABLE TRIGGER [ecomm_umroh_save_child_UPD] ON [dbo].[ecomm_umroh_save_child]";
		$resultUpd = $this->executeQuery($sqlUpd, $this->db1);
		if($resultUpd > 0) {
			$updStatus++;


			$sql = "INSERT INTO db_ecommerce.[dbo].[ECOMM_UMROH_KETERANGAN] (dfno, fullnm, ket, registerno, batchno,
						bank, bankaccno, total_pay, total_vch_sk, pengembalian, nama_rekening)
					VALUES ('".$data['dfno']."', '".$data['fullnm']."', '".$data['alasan']."',
					'".$data['registerno']."', '$batchno', '".$data['banknm']."',
					'".$data['bankaccno']."', '".$data['installment_c']."', '".$data['installment_d']."', '".$data['sisa']."', '".$data['nama_rekening']."')";
			//echo $sql;
			//echo "<br />";
			$result = $this->executeQuery($sql, $this->db1);
			if($result > 0) {
				$insKeterangan++;
			}



			$istMutBatal = "INSERT INTO db_ecommerce.[dbo].ecomm_umroh_save_mut(mutate_flag, pay_status, is_voucher, tot_fund, createdt,
													 createnm, id, [description], voucherno, paymentID, paymentRef)
								   VALUES('1', '1', '2', '".$data['sisa']."', GETDATE(),
										  '".$this->username."', '".$data['id']."', 'REFUND CANCEL', '', '', '')";
			//echo $istMutBatal;
			//echo "<br />";
			$qistMutVcr = $this->executeQuery($istMutBatal, $this->db1);
			if($qistMutVcr > 0) {
				$insMutasi++;
			}
		}


		/*$ambil_seq = "select id from ECOMM_UMROH_KETERANGAN
					where dfno = '$dfno'";
		$ambil = $this->getRecordset($ambil_seq,null,$this->db1);
		return $ambil;	*/

		$resAkhir = array(
		 "status_batal" => $updStatus,
		 "insert_ket" => $insKeterangan,
		 "insert_mutasi" => $insMutasi,
		);

		return $resAkhir;

	}

	function getListCancelByParam($param, $value) {
		$qry = "SELECT * FROM db_ecommerce.dbo.ECOMM_UMROH_KETERANGAN WHERE $param = '$value'";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}

	function getListCancel(){
		$qry = "SELECT * FROM ECOMM_UMROH_KETERANGAN ORDER BY id";
		$result = $this->getRecordset($qry,null,$this->db1);
		return $result;
	}
}

<?php
class M_cashback_hydro extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}
	
	function getReportByTrcd($trcd) {
		$qry = "SELECT * FROM CASHBACK_WATERH a WHERE a.trcd IN ($trcd)";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function rekapListCashbackByMember($idmember) {
		$qry = "SELECT a.dfno, a.fullnm, 
				  CASE 
					 WHEN LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' AND B.orderno is not null 
					 THEN b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
					 WHEN LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' AND B.orderno is null 
					 THEN a.trcd 
					 ELSE a.trcd 
				  END AS ttp_no,
				a.CNno, 
				a.bank, a.bank_acc, a.no_telp, 
				a.bank_accname, a.BatchProses, 
				CASE WHEN a.[status] = '0' THEN 'PENDING'
				WHEN a.[status] = '1' THEN 'DALAM PROSES'
				WHEN a.[status] = '2' THEN 'TOLAKAN TRANSFER'
				WHEN a.[status] = '3' THEN 'TRF TO ACCOUNT'
				END as status_name,
				a.[status], CONVERT(VARCHAR(10), a.transferdt, 111) as transferdt
				FROM CASHBACK_WATERH a 
				LEFT OUTER JOIN sc_newtrh b ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd) 
				where a.dfno = '$idmember'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function checkTransferByMemberID($idmember) {
		$qry = "SELECT a.dfno, a.BatchProses,
					CASE
							when a.[status] = '0' THEN null
							when a.[status] = '1' OR a.[status] = '2' THEN CONVERT(VARCHAR(10), A.batchdt , 120)
							--when a.[status] = '2' THEN 'Transfer Rejected'
							when a.[status] = '3' THEN CONVERT(VARCHAR(10), A.transferdt , 120)
					   END AS processdt,
					case
						when a.[status] = '0' THEN 'Pending'
						when a.[status] = '1' THEN 'Process'
						when a.[status] = '2' THEN 'Transfer Rejected'
						when a.[status] = '3' THEN 'Transfer To Account'
					end as status_transfer,
					a.cashback_sblm_pajak as nom_cashback,
					a.potongan_tax ,
					a.biaya_bank,
					a.cashback_setelah_pajak as nett_cashback
				FROM V_HILAL_CASHBACK_HYDRO_ALL a
				WHERE a.dfno = '$idmember'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function pendingTrf($idmember) {
		$qry = "SELECT * FROM CASHBACK_WATERH WHERE status = '0' AND dfno = '$idmember'";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listCashbackTanpaNoRekAtauHP() {
		$qry = "SELECT a.CNno, a.trcd, a.bank, a.bank_acc, a.bank_accname,a.dfno, a.fullnm, 
					a.nominal_cashback - (a.nominal_cashback * a.proc_tax) as nominal_cashback,
					a.no_telp
		        FROM CASHBACK_WATERH a		
				WHERE status != '1' and status != '3' AND 
				(a.bank_acc = null OR bank_acc = '' OR a.no_telp = null OR a.no_telp = '')
				ORDER BY CNno";
				//OR a.no_telp = null OR a.no_telp = ''
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listCashbackBelumDitransferV2() {
		$qry = "SELECT a.CNno, a.trcd, a.bank, a.bank_acc, a.status, a.bank_accname,a.dfno, a.fullnm,
					a.proc_tax * 100 as proc_tax, a.potongan_tax, a.nominal_cashback as cashback_sblm_pajak, a.biaya_bank,
					a.nominal_cashback - (a.nominal_cashback * a.proc_tax) as nominal_cashback
		        FROM CASHBACK_WATERH a		
				WHERE status != '1' and status != '3' and status != '2' AND 
				a.bank_acc != null AND bank_acc != '' AND a.no_telp != null AND a.no_telp != ''  
				ORDER BY CNno"; 
		//AND a.no_telp != null AND a.no_telp != ''
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listCashbackBelumDitransfer() {
		/*
		 $qry = "SELECT a.CNno, a.trcd, a.bank, a.bank_acc, a.bank_accname,a.dfno, a.fullnm,
					a.proc_tax * 100 as proc_tax, a.potongan_tax, a.nominal_cashback as cashback_sblm_pajak, a.biaya_bank,
					a.nominal_cashback - (a.nominal_cashback * a.proc_tax) as nominal_cashback
		        FROM CASHBACK_WATERH a		
				WHERE status != '1' and status != '3' AND 
				a.bank_acc != null AND bank_acc != '' 
				AND a.no_telp != null AND a.no_telp != ''
				ORDER BY CNno"; 
		 */
		 
		 // cashback tetap ditransfer walaupun tidak ada no hp
		 $qry = "SELECT a.CNno, a.trcd, a.bank, a.bank_acc, a.status, a.bank_accname,a.dfno, a.fullnm,
					a.proc_tax * 100 as proc_tax, a.potongan_tax, a.nominal_cashback as cashback_sblm_pajak, a.biaya_bank,
					a.nominal_cashback - (a.nominal_cashback * a.proc_tax) as nominal_cashback
		        FROM CASHBACK_WATERH a		
				WHERE status != '1' and status != '3' AND 
				a.bank_acc != null AND bank_acc != '' 
				AND a.no_telp != null AND a.no_telp != ''
				ORDER BY CNno"; 
		 //echo $qry;
		//$this->load->database("MAINMEMBER", true);		
				
		/*$qry = "SELECT a.dfno, 
				  b.NMMEMBER as fullnm,
				  a.trcd,
				  a.CNno,
				  b.BANKCODE as bank, 
				  b.REKNUM as bank_acc, 
				  b.REKNAME as bank_accname,
				  b.TEL_HP as no_telp, 
				  a.[status],
				  a.nominal_cashback
				FROM CASHBACK_WATERH a
				LEFT OUTER JOIN [192.168.0.15].MAINMEMBER.dbo.REKACCTOTO2B b	
				  ON (a.dfno COLLATE Latin1_General_CI_AS = b.IDMEMBER)	
				WHERE a.status NOT IN ('1','3') AND 
				b.REKNUM is not null AND b.REKNUM != '' 
				AND b.TEL_HP is not null AND b.TEL_HP != ''
				ORDER BY CNno";*/
				
		//$qry = "SELECT top 5 * FROM MAINMEMBER.dbo.REKACCTOTO2B";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listCashbackRejected($from, $to) {
		$qry = "SELECT a.BatchProses, a.dfno, a.fullnm, a.bank,
					a.bank_acc, a.bank_accname, a.status,
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
					a.remarkTolakan,
					SUM(a.nominal_cashback) as total_cashback 
				FROM CASHBACK_WATERH a
				WHERE status = '2'  
				  AND CONVERT(VARCHAR(10),a.tglproses, 121) BETWEEN '$from' AND '$to'
				GROUP BY a.BatchProses, a.dfno, a.fullnm, a.bank,
					a.bank_acc, a.bank_accname, a.status,
				    CONVERT(VARCHAR(10),a.tglproses, 121),
					a.remarkTolakan
				ORDER BY a.BatchProses"; //	echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function updateAdminBankKosong() {
		$qry = "UPDATE CASHBACK_WATERH SET biaya_bank = 5000
				WHERE biaya_bank != 5000";
		$upd = $this->executeQuery($qry, "klink_mlm2010");
		return $upd;
	}
	
	function listCashbackRejectedV2($from, $to) {
		$qry = "SELECT a.BatchProses, a.dfno, a.fullnm, a.bank,
					a.bank_acc, a.bank_accname, a.status,
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
					a.remarkTolakan,
					SUM(a.nominal_cashback) as total_cashback 
				FROM CASHBACK_WATERH a
				WHERE status = '2'  
				GROUP BY a.BatchProses, a.dfno, a.fullnm, a.bank,
					a.bank_acc, a.bank_accname, a.status,
				    CONVERT(VARCHAR(10),a.tglproses, 121),
					a.remarkTolakan
				ORDER BY a.BatchProses"; //	echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listBatchTransfer($from, $to, $status) {
		$qry = "SELECT a.BatchProses, a.processBy,
				CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
				SUM(a.nominal_cashback) as total_cashback 
			FROM CASHBACK_WATERH a
			WHERE status = '$status'  
			  AND CONVERT(VARCHAR(10),a.tglproses, 121) BETWEEN '$from' AND '$to'
			GROUP BY a.BatchProses, a.processBy, 
			    CONVERT(VARCHAR(10),a.tglproses, 121)
			ORDER BY a.BatchProses"; //echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listBatchTransferOK($from, $to, $status) {
		$qry = "SELECT a.BatchProses, a.transferby,
				CONVERT(VARCHAR(10),a.transferdt, 121) as transferdt,
				SUM(a.nominal_cashback) as total_cashback 
			FROM CASHBACK_WATERH a
			WHERE status = '$status'  
			  AND CONVERT(VARCHAR(10),a.transferdt, 121) BETWEEN '$from' AND '$to'
			GROUP BY a.BatchProses, a.transferby, 
			    CONVERT(VARCHAR(10),a.transferdt, 121)
			ORDER BY a.BatchProses"; //echo $qry;
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function rekapCashbackByIdmember($list) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.bank_acc, 
					a.bank_accname, 
					a.biaya_bank,
					SUM(a.nominal_cashback - (a.proc_tax * a.nominal_cashback))- a.biaya_bank 
                       as cashback_setelah_pajak
				FROM CASHBACK_WATERH a
				WHERE (a.status = '0' OR a.status = '2') and a.trcd IN ($list)
				GROUP BY a.dfno, a.fullnm, a.bank, a.bank_acc, a.bank_accname,a.biaya_bank
				ORDER BY a.bank, a.dfno";
		//echo $qry; 
		//a.proc_tax * 100 as proc_tax, a.proc_tax, 
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function cancelBatchHydroWater($batchno) {
		$db_qryx = $this->load->database($this->setDB(2), true);
		$cancel = "UPDATE CASHBACK_WATERH 
					SET status = '0', 
					tglproses = '', 
					BatchProses = '',
					updatedt = '',
					updateby = ''
				   WHERE BatchProses = '$batchno'";
		$hasil = $db_qryx->query($cancel);
		if($hasil > 0) {
			$return = jsonTrueResponse(null, "$batchno sudah dibatalkan..");
		} else {
			$return = jsonFalseResponse("$batchno gagal dibatalkan..");
		}
		return $return;
	}
	
	function saveBatchCashbackHydroWater($datax) {
		$db_qryx = $this->load->database($this->setDB(2), true);
	
	    $noBatch = "BT".date("ymd-His");
		$listTrx = $datax['listTrx'];
		
		$db_qryx->trans_begin();
		
		$upd = "UPDATE CASHBACK_WATERH 
				SET status = '1', 
				    tglproses = '".$this->dateTime."' ,
					processBy = '".$this->username."',
					BatchProses = '$noBatch'
				WHERE trcd IN ($listTrx)";
		$db_qryx->query($upd);
		
		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			$return = jsonFalseResponse("Batch gagal..");
		} else {
			$db_qryx->trans_commit();
			$return = jsonTrueResponse(null, "Proses Batch berhasil, No : $noBatch ..");
			
		}
		
		return $return;
	}
	
	function listTransferByBatchNo($batchno) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses, 
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt, a.remarkTolakan,
					SUM(a.nominal_cashback) as total_cashback 
				FROM CASHBACK_WATERH a
				WHERE a.BatchProses = '$batchno'
				GROUP BY a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses, 
					CONVERT(VARCHAR(10),a.tglproses, 121), a.remarktolakan
				ORDER BY a.bank, a.dfno";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listTransferByBatchID($batchno) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses, 
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt, a.remarkTolakan,
					a.proc_tax, a.biaya_bank, a.nominal_cashback,
					SUM(a.nominal_cashback - (a.proc_tax * a.nominal_cashback))- a.biaya_bank 
						 as total_cashback
				FROM CASHBACK_WATERH a
				WHERE a.BatchProses = '$batchno'
				GROUP BY a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses, 
					CONVERT(VARCHAR(10),a.tglproses, 121), a.remarktolakan,
					a.proc_tax, a.biaya_bank, a.nominal_cashback
				ORDER BY a.bank, a.dfno";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function rejectTransferPrev($lisdID, $batchno) {
		$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses, 
					CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
					SUM(a.nominal_cashback) as total_cashback 
				FROM CASHBACK_WATERH a
				WHERE a.BatchProses = '$batchno' AND a.dfno IN ($lisdID)
				GROUP BY a.dfno, a.fullnm, a.bank, a.status,
					a.bank_acc, a.bank_accname, a.BatchProses, 
					CONVERT(VARCHAR(10),a.tglproses, 121)
				ORDER BY a.bank, a.dfno";
		return $this->getRecordset($qry, null, $this->setDB(2));
	}
	
	function listTransferByBatchNo2($batchno) {
		$qryxRes = $this->load->database($this->setDB(2), true);
		/*$i = 0;
		foreach($batchno as $dta) {
			$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
						a.bank_acc, a.bank_accname, a.BatchProses, 
						CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
						SUM(a.nominal_cashback) as total_cashback 
					FROM CASHBACK_WATERH a
					WHERE a.BatchProses = '$dta'
					GROUP BY a.dfno, a.fullnm, a.bank, a.status,
						a.bank_acc, a.bank_accname, a.BatchProses, 
						CONVERT(VARCHAR(10),a.tglproses, 121)
					ORDER BY a.BatchProses, a.bank, a.dfno";
			$query = $qryxRes->query($qry, null); 
			if($query->num_rows() > 0)  {
				$hasil[] = $query->result_array();   
			} 		
		}
		return $hasil;*/
		
		/*$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
						a.bank_acc, a.bank_accname, a.BatchProses, 
						CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
						CONVERT(VARCHAR(10),a.transferdt, 121) as transferdt,
						SUM(a.nominal_cashback - (a.proc_tax * a.nominal_cashback))- a.biaya_bank 
                         as cashback_setelah_pajak
					FROM CASHBACK_WATERH a
					WHERE a.BatchProses in ($batchno)
					GROUP BY a.dfno, a.fullnm, a.bank, a.status,
						a.bank_acc, a.bank_accname, a.BatchProses, 
						CONVERT(VARCHAR(10),a.tglproses, 121),
						CONVERT(VARCHAR(10),a.transferdt, 121),
						a.biaya_bank
					ORDER BY a.BatchProses, a.bank, a.dfno"; */
			/*$qry = "SELECT a.dfno, a.fullnm, a.bank, a.status,
						a.bank_acc, a.bank_accname, a.BatchProses, 
						CONVERT(VARCHAR(10),a.tglproses, 121) as batchdt,
						CONVERT(VARCHAR(10),a.transferdt, 121) as transferdt,
						a.proc_tax * 100 as proc_tax, 
						SUM(a.potongan_tax) as potongan_tax, 
						SUM(a.nominal_cashback) as cashback_sblm_pajak, 
						a.biaya_bank,
						SUM(a.nominal_cashback - (a.proc_tax * a.nominal_cashback))- a.biaya_bank 
						 as cashback_setelah_pajak,
						(STUFF((SELECT ', ' + (B.CNno)
						  FROM CASHBACK_WATERH B
						  WHERE  B.dfno = A.dfno and a.status=b.status -- and B.BatchProses=A.BatchProses 
					      GROUP BY B.CNno		
						  ORDER BY B.CNno
						  FOR XML PATH ('')
						  ),1,2,''
						))  AS CNno 
					FROM CASHBACK_WATERH a
					WHERE a.BatchProses in ($batchno)
					GROUP BY a.dfno, a.fullnm, a.bank, a.status,
						a.bank_acc, a.bank_accname, a.BatchProses, 
						CONVERT(VARCHAR(10),a.tglproses, 121),
						CONVERT(VARCHAR(10),a.transferdt, 121),
						a.proc_tax * 100,
						
						a.biaya_bank
					ORDER BY a.BatchProses, a.bank, a.dfno"; */			
			$qry = "SELECT * FROM V_HILAL_CASHBACK_HYDRO_ALL a 
					WHERE a.BatchProses in ($batchno)
					ORDER BY a.BatchProses, a.bank, a.dfno";		
			$query = $qryxRes->query($qry, null); 
		
			if($query->num_rows() > 0)  {
				$header = 0;
				$tempHeader = "";
				$arr = array();
				foreach($query->result_array() as $dta) {
				    if($dta['status'] == "1") {
							$stt = "Dalam Proses";
					} else if($dta['status'] == "2") {
							$stt = "Ditolak";
					} else if($dta['status'] == "3") {
							$stt = "Trf OK";
					}
					if($tempHeader != $dta['BatchProses']) {
						$tempHeader = $dta['BatchProses']; 
						$detail = 0; 
						$arr[$header]['BatchProses'] = $dta['BatchProses'];  
						$arr[$header]['batchdt'] = $dta['batchdt'];
						$arr[$header]['transferdt'] = $dta['transferdt'];
						$arr[$header]['listTrf'][$detail]['dfno'] = $dta['dfno']; 
						$arr[$header]['listTrf'][$detail]['fullnm'] = $dta['fullnm'];
						$arr[$header]['listTrf'][$detail]['bank'] = $dta['bank'];
						$arr[$header]['listTrf'][$detail]['bank_acc'] = $dta['bank_acc'];
						$arr[$header]['listTrf'][$detail]['bank_accname'] = $dta['bank_accname'];
						//$arr[$header]['listTrf'][$detail]['total_cashback'] = $dta['total_cashback'];
						//$arr[$header]['listTrf'][$detail]['proc_tax'] = $dta['proc_tax'];
						//$arr[$header]['listTrf'][$detail]['biaya_bank'] = $dta['biaya_bank'];
						$arr[$header]['listTrf'][$detail]['cashback_sblm_pajak'] = $dta['cashback_sblm_pajak'];
						$arr[$header]['listTrf'][$detail]['proc_tax'] = $dta['proc_tax'];
						$arr[$header]['listTrf'][$detail]['potongan_tax'] = $dta['potongan_tax'];
						$arr[$header]['listTrf'][$detail]['biaya_bank'] = $dta['biaya_bank'];
						$arr[$header]['listTrf'][$detail]['cashback_setelah_pajak'] = $dta['cashback_setelah_pajak'];
						$arr[$header]['listTrf'][$detail]['status'] = $stt;
						$arr[$header]['listTrf'][$detail]['CNno'] = $dta['CNno'];
						//$arr[$header]['listTrf'][$detail]['remarkTolakan'] = $dta['remarkTolakan'];
						$header++; 
					} else {
						 $jHead = $header-1;
						 $jDet = $detail+1;
						 $arr[$jHead]['listTrf'][$jDet]['dfno'] = $dta['dfno']; 
						 $arr[$jHead]['listTrf'][$jDet]['fullnm'] = $dta['fullnm'];
						 $arr[$jHead]['listTrf'][$jDet]['bank'] = $dta['bank'];
						 $arr[$jHead]['listTrf'][$jDet]['bank_acc'] = $dta['bank_acc'];
						 $arr[$jHead]['listTrf'][$jDet]['bank_accname'] = $dta['bank_accname'];
						 //$arr[$jHead]['listTrf'][$jDet]['total_cashback'] = $dta['total_cashback'];
						 //$arr[$jHead]['listTrf'][$jDet]['proc_tax'] = $dta['proc_tax'];
						 //$arr[$jHead]['listTrf'][$jDet]['biaya_bank'] = $dta['biaya_bank'];
						 $arr[$jHead]['listTrf'][$jDet]['cashback_sblm_pajak'] = $dta['cashback_sblm_pajak'];
						$arr[$jHead]['listTrf'][$jDet]['proc_tax'] = $dta['proc_tax'];
						$arr[$jHead]['listTrf'][$jDet]['potongan_tax'] = $dta['potongan_tax'];
						$arr[$jHead]['listTrf'][$jDet]['biaya_bank'] = $dta['biaya_bank'];
						$arr[$jHead]['listTrf'][$jDet]['cashback_setelah_pajak'] = $dta['cashback_setelah_pajak'];
						 $arr[$jHead]['listTrf'][$jDet]['status'] = $stt;
						 $arr[$jHead]['listTrf'][$jDet]['CNno'] = $dta['CNno'];
						 //$arr[$jHead]['listTrf'][$jDet]['remarkTolakan'] = $dta['remarkTolakan'];
						 $detail++; 
					}
				}
			} 		
			
			return $arr;
		
	}
	
	function listAllBatch() {
	
	}
	
	function rejectTransfer($listID, $batchno) {
		$db_qryx = $this->load->database($this->setDB(2), true);
		$upd = "UPDATE CASHBACK_WATERH 
				SET status = '2', 
				    updatedt = '".$this->dateTime."',
					updateby = '".$this->username."',
					rejectdt = '".$this->dateTime."',
					rejectby = '".$this->username."'
				WHERE dfno IN ($listID) AND BatchProses = '$batchno'";
		//echo $upd;
		$res = $db_qryx->query($upd);
		if($res > 0) {
			$return = jsonTrueResponse(null, "Tolakan Transfer berhasil di update..");
		} else {
			$return = jsonFalseResponse("Tolakan Transfer gagal di update..");
		}
		return $return;
	}
	
	function rejectTransfer2($listID) {
		$db_qryx = $this->load->database($this->setDB(2), true);
		//print_r($listID);
		$total = count($listID['trcd']);
		$berhasil = 0;
		for($i=0; $i < $total; $i++) {
			$upd = "UPDATE CASHBACK_WATERH 
					SET status = '2', 
						updatedt = '".$this->dateTime."',
						updateby = '".$this->username."',
						rejectdt = '".$this->dateTime."',
						rejectby = '".$this->username."',
						remarkTolakan = '".$listID['tolakan'][$i]."'
					WHERE dfno = '".$listID['trcd'][$i]."' AND BatchProses = '$listID[batchno]'";
			$res = $db_qryx->query($upd);
			if($res > 0) {
				$berhasil++;
			}
		
	    }
		if($berhasil > 0) {
			$return = jsonTrueResponse(null, "Update Tolakan berhasil");
		} else {
			$return = jsonFalseResponse("Update Tolakan gagal..");
		}
		return $return;
		//echo $upd;
	}
	
	function transferSelesai($listID, $transferdt) {
		$db_qryx = $this->load->database($this->setDB(2), true);
		$upd = "UPDATE CASHBACK_WATERH 
				SET status = '3', 
				    updatedt = '".$this->dateTime."',
					updateby = '".$this->username."',
					transferdt = '".$transferdt."',
					transferby = '".$this->username."'
				WHERE BatchProses IN ($listID) and status = '1'";
		$res = $db_qryx->query($upd);
		
		$upd2 = "UPDATE CASHBACK_WATERH SET 
					updatedt = '".$this->dateTime."',
					updateby = '".$this->username."',
					transferdt = '".$transferdt."',
					transferby = '".$this->username."'
		        WHERE BatchProses IN ($listID) and status = '2'";
		//echo $upd;
		$res2 = $db_qryx->query($upd2);
		if($res > 0) {
			$return = jsonTrueResponse(null, "Batch Transfer status = OK");
		} else {
			$return = jsonFalseResponse("Batch Transfer gagal di update..");
		}
		return $return;
	}
	
	function tesSendSms() {
		$db_qryx = $this->load->database($this->setDB(2), true);
		$qry = "SELECT top 3 a.dfno, a.fullnm, a.bank_acc, 
					a.no_telp, a.BatchProses,
				(SUM(a.nominal_cashback) -  SUM(a.potongan_tax) - a.biaya_bank) as nominal_cashback
				FROM CASHBACK_WATERH a
				WHERE a.status = '3'
				GROUP BY a.dfno, a.fullnm, a.bank_acc, 
					a.no_telp, a.BatchProses, a.proc_tax,a.biaya_bank";
		$query = $db_qryx->query($qry); 
		$updSms = 0;
		$failSms = 0;
	    $i=0;
		$arr = array();
		if($query->num_rows() > 0)  {
            foreach($query->result() as $dta) {
				$listTrx = "SELECT a.trcd, 
								CASE WHEN 
								  LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' THEN b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
								  ELSE a.trcd 
								END AS ttp_no
							FROM CASHBACK_WATERH a 
							LEFT OUTER JOIN sc_newtrh b ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
							where a.dfno = '".$dta->dfno."' 
							AND a.BatchProses = '".$dta->BatchProses."'
							AND a.status = '3'";
				echo $listTrx."<br />";
				
				
				$exeListTrx = $db_qryx->query($listTrx); 
				$hasilExeLTrx = $exeListTrx->result();
				
				$noTrx = "dari Trx : ";
				if($hasilExeLTrx != null) {
					foreach($hasilExeLTrx as $noinvoice) {
						$noTrx .= $noinvoice->ttp_no.",";
					}
				} else {
				  $noTrx = "";
				}
				echo $noTrx."<br />"; 
				
				$message = "Cashback Hydrowater ".$dta->fullnm." rekening ".$dta->bank_acc." sebesar Rp.".number_format($dta->nominal_cashback, 0, ",", ".")." $noTrx";
				echo $message."<br />"; 
				$res = smsTemplate("087780441874", $message);
				$strx = substr($res, 0, 8);
				echo $strx."<br />"; 
			}
		}	
	}
	
	function sendSms() {
		$db_qryx = $this->load->database($this->setDB(2), true);
		$qry = "SELECT a.dfno, a.fullnm, a.bank_acc, 
					a.no_telp, a.BatchProses,
				(SUM(a.nominal_cashback) -  SUM(a.potongan_tax) - a.biaya_bank) as nominal_cashback
				FROM CASHBACK_WATERH a
				WHERE a.status = '3' and (flag_sms != '1' OR flag_sms is null OR flag_sms = '') 
				GROUP BY a.dfno, a.fullnm, a.bank_acc, 
					a.no_telp, a.BatchProses, a.proc_tax,a.biaya_bank";
		//SUM(a.nominal_cashback) as nominal_cashback,
		$query = $db_qryx->query($qry); 
		$updSms = 0;
		$failSms = 0;
	    $i=0;
		$arr = array();
		if($query->num_rows() > 0)  {
            foreach($query->result() as $dta) {
			    $noTrx = "";
			    
				//AND a.status = '3' and (a.flag_sms != '1' OR a.flag_sms is null OR a.flag_sms = '')
				$listTrx = "SELECT a.trcd, 
								CASE WHEN 
								  LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' 
								  THEN b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
								  ELSE a.trcd 
								END AS ttp_no
							FROM CASHBACK_WATERH a 
							LEFT OUTER JOIN sc_newtrh b 
							   ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
							WHERE a.dfno = '".$dta->dfno."' 
							AND a.BatchProses = '".$dta->BatchProses."'";
				$exeListTrx = $db_qryx->query($listTrx);
				$hasilExeLTrx = $exeListTrx->result();				
				$noTrx = " dari Trx : ";
				if($hasilExeLTrx != null) {
					foreach($hasilExeLTrx as $noinvoice) {
						$noTrx .= $noinvoice->ttp_no.",";
					}
				} else {
				  $noTrx = "";
				}
				
				
			    $nama = substr($dta->fullnm, 0, 10);
			    $message = "Keuntungan Langsung sudah ditransfer ke ".$nama." rekening ".$dta->bank_acc." sebesar Rp.".number_format($dta->nominal_cashback, 0, ",", ".");
				$message .= $noTrx;
				$res = smsTemplate($dta->no_telp, $message);
				$strx = substr($res, 0, 1);
				if($strx == "0") {
					$upd = "UPDATE CASHBACK_WATERH 
								SET flag_sms = '1', 
							    smsdt = '".$this->dateTime."'
							WHERE dfno = '".$dta->dfno."' AND BatchProses = '".$dta->BatchProses."'
							AND (flag_sms != '1' OR flag_sms is null OR flag_sms = '')";
					//echo $upd."<br />";
					$updQuery = $db_qryx->query($upd);	
					
					if($updQuery > 0) {
						$updSms++;
						$arr['list'][$i]['dfno']=$dta->dfno;
				        $arr['list'][$i]['fullnm']=$dta->fullnm;
				        $arr['list'][$i]['no_telp']=$dta->no_telp;
					    $arr['list'][$i]['resSMS']=$strx;
						$arr['list'][$i]['status_sms']="SMS OK";
					} else {
						$updSms++;
						$arr['list'][$i]['dfno']=$dta->dfno;
				        $arr['list'][$i]['fullnm']=$dta->fullnm;
				        $arr['list'][$i]['no_telp']=$dta->no_telp;
					    $arr['list'][$i]['resSMS']=$strx;
						$arr['list'][$i]['status_sms']="FAIL UPDATE DB";
					}
				} else {
					$failSms++;
					$arr['list'][$i]['dfno']=$dta->dfno;
					$arr['list'][$i]['fullnm']=$dta->fullnm;
				    $arr['list'][$i]['no_telp']=$dta->no_telp;
					$arr['list'][$i]['resSMS']=$strx;
					$arr['list'][$i]['status_sms']="SMS FAIL";
				}
				$i++;
			} 
			if($updSms > 0) {
				$res = smsTemplate("081807278131", "Keuntungan Lgs. Berhasil mengirimkan $updSms "."SMS, yang gagal terkirim ".$failSms. " SMS");
				$hasil = jsonTrueResponse($arr, "Berhasil mengirimkan $updSms "."SMS, yang gagal terkirim ".$failSms. " SMS");
			}
        } else {
			$hasil = jsonFalseResponse("Tidak ada SMS yang akan dikirim, pastikan anda sudah mengupdate status Transfer Selesai..");
		}
		return $hasil;
	}
	
	function kirimSms($idmember, $trcd) {
		$db_qryx = $this->load->database($this->setDB(2), true);
		$qry = "SELECT a.dfno, a.fullnm, a.bank_acc, 
					a.no_telp, a.BatchProses,
				(SUM(a.nominal_cashback) -  SUM(a.potongan_tax) - a.biaya_bank) as nominal_cashback
				FROM CASHBACK_WATERH a
				WHERE a.dfno = '$idmember' AND a.trcd = '$trcd'
				GROUP BY a.dfno, a.fullnm, a.bank_acc, 
					a.no_telp, a.BatchProses, a.proc_tax,a.biaya_bank";
		//SUM(a.nominal_cashback) as nominal_cashback,
		$query = $db_qryx->query($qry);
		if($query->num_rows() > 0)  {
            foreach($query->result() as $dta) {
				$noTrx = "";
			    
				//AND a.status = '3' and (a.flag_sms != '1' OR a.flag_sms is null OR a.flag_sms = '')
				$listTrx = "SELECT a.trcd, 
								CASE WHEN 
								  LEFT(A.CNno, 2) = 'CN' OR LEFT(A.CNno, 2) = 'MS' 
								  THEN b.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
								  ELSE a.trcd 
								END AS ttp_no
							FROM CASHBACK_WATERH a 
							LEFT OUTER JOIN sc_newtrh b 
							   ON (a.trcd COLLATE SQL_Latin1_General_CP1_CS_AS = b.trcd)
							WHERE a.dfno = '".$dta->dfno."' 
							AND a.BatchProses = '".$dta->BatchProses."'";
				$exeListTrx = $db_qryx->query($listTrx);
				$hasilExeLTrx = $exeListTrx->result();				
				$noTrx = " dari Trx : ";
				if($hasilExeLTrx != null) {
					foreach($hasilExeLTrx as $noinvoice) {
						$noTrx .= $noinvoice->ttp_no.",";
					}
				} else {
				  $noTrx = "";
				}
			
				$message = "Cashback Hydrowater ".$dta->fullnm." rekening ".$dta->bank_acc." sebesar Rp.".number_format($dta->nominal_cashback, 0, ",", ".");
				$message .= $noTrx;
				//echo $message;
				
				//$res = smsTemplate($dta->no_telp, $message);
				//$strx = substr($res, 0, 8);
				$strx = "Status=1";
				if($strx == "Status=1") {
				   $arr = array(
					"response" => "true",
					"notujuan" => $dta->no_telp,
					"status" => $strx,
					"message" => $message
				   );
				} else {
				   $arr = array(
					"response" => "false",
					"notujuan" => $dta->no_telp,
					"status" => $strx
				   );
				}
				
				print_r($arr);
			}
		} else {
		
		}	
	}
}
<?php

    class Be_m_all extends MY_Model{

        public function __construct(){

            parent::__construct();
        }

        public function getDataUser(){

            $qry = "select *
                    from master_user";

            $result = $this->getRecordSet($qry, null, $this->db7);

            return $result;
        }

        public function getName($idmember){

            $qry = "select fullnm
                    from msmemb where dfno = '$idmember'";

            $result = $this->getRecordSet($qry, null, $this->db2);

            return $result;
        }

        public function totBV($idmember){

            $qry = "select dfno, fullnm, tbv, bnsperiod
                    from TWA_KLPromo_Oct17_MH
                    WHERE bnsperiod >= '2019/01/01' and dfno = '$idmember'";

            $result = $this->getRecordSet($qry, null, $this->db2);

            return $result;
        }
    }
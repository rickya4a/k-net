<?php
class Be_trans_klink_model extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();

    }

	function getListTrxByParam($searchby, $param, $bnsmonth=null, $bnsyear=null, $from=null, $to=null) {
		//dfno, cnno, batchno, trcd, orderno
		//echo "param searchby = $searchby";
		$qryParam = "";
		if($searchby == 'cnno'){ //dfno, cnno, batchno
			$qryParam = " A.invoiceno = '$param'";
		}elseif($searchby == 'batchno'){
			$qryParam = " A.batchno = '$param' ";
		}elseif($searchby == 'trcd'){
			$qryParam = " A.trcd = '$param' OR A.orderno = '$param' ";
		}elseif($searchby == 'dfno'){
			if($bnsyear != "" || $bnsyear != null){
				if($bnsmonth != "all"){
					$period = "$bnsyear-$bnsmonth-01";
					$qryPeriod = " A.bnsperiod = '$period' AND";
				}else{
					$qryPeriod = " YEAR(A.bnsperiod) = '$bnsyear' AND";
				}

				if($from != NULL && $to != NULL){
					$dtfrom = date("Y/m/d", strtotime($from));
					$dtto = date("Y/m/d", strtotime($to));
					$qryParam = " (CONVERT(VARCHAR(10), A.etdt, 111) between '$dtfrom' and '$dtto') AND ";
				}
				//chk-dtrange
				$qryParam = " $qryParam $qryPeriod A.dfno='$param' ";
				//echo "$qryParam<BR/>";
			}
		}
		/*
		$qry = "SELECT  A.trcd, A.orderno, A.batchno, A.invoiceno, A.trtype,
						A.ttptype, A.etdt, A.batchdt, A.remarks, A.createdt,
						A.createnm, A.updatedt, A.updatenm, A.dfno, A.distnm, A.loccd, A.loccdnm,
						A.sc_co, A.sc_conm, A.sc_dfno, A.sc_dfnonm, A.tdp,
						A.tbv, A.bnsperiod, A.statusTrx
	   			 FROM V_HILAL_CHECK_BV_ONLINE_HDR a
	   			 WHERE $qryParam" ;
		 *
		 */
		$qry = "SELECT  A.trcd, A.orderno, A.batchno, A.invoiceno, A.trtype, A.ttptype,
					    MAX(A.etdt) AS etdt,  MAX(A.createdt) AS createdt,
					    A.dfno, A.distnm, A.loccd, A.loccdnm, A.sc_co, A.sc_conm, A.sc_dfno,
					    A.sc_dfnonm, A.tdp, A.tbv, A.bnsperiod, A.statusTrx
	   			 FROM V_HILAL_CHECK_BV_ONLINE_HDR a
	   			 WHERE $qryParam
	   			 GROUP BY A.trcd, A.orderno, A.batchno, A.invoiceno, A.trtype, A.ttptype,
					    A.dfno, A.distnm, A.loccd, A.loccdnm, A.sc_co, A.sc_conm, A.sc_dfno,
					    A.sc_dfnonm, A.tdp, A.tbv, A.bnsperiod, A.statusTrx
				ORDER BY MAX(A.etdt), A.trcd" ;
	   	//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getTrxByTrcdHead($param, $value) {
		$qry = "SELECT  TOP 1 a.trcd, a.orderno, a.batchno, a.invoiceno, a.trtype, a.ttptype,
						a.etdt, a.batchdt, a.remarks, a.createdt, a.createnm, A.updatedt, A.updatenm, a.dfno,
						a.distnm, a.loccd, a.loccdnm, a.sc_co, a.sc_conm, a.sc_dfno,
						a.sc_dfnonm, a.tdp, a.tbv, a.bnsperiod, a.statusTrx, a.id_deposit, a.flag_batch
	   			 FROM V_HILAL_CHECK_BV_ONLINE_HDR a
	   			 WHERE $param = '$value'" ;
	   	//echo $qry;
	   	//echo "<br />";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function geTrxByTrcdDet($param, $value) {
		$qry = "SELECT  a.trcd, a.prdcd, a.prdnm, a.qtyord, a.bv, a.dp, a.TOTBV, a.TOTDP
	   			 FROM V_HILAL_CHECK_BV_ONLINE_DET a
	   			 WHERE $param = '$value'" ;
	   		//echo $qry;
	   	//echo "<br />";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getTrxByTrcdPay($param, $value) {
		$qry = "SELECT * FROM sc_newtrp a WHERE $param = '$value'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getStockist($loccd) {
		$qry = "SELECT a.loccd, a.fullnm AS fullnm
	   			 FROM mssc a
	   			 WHERE a.loccd = '$loccd'" ;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getAllStockist() {
		$qry = "SELECT a.loccd, a.fullnm AS fullnm
	   			 FROM mssc a
	   			 WHERE a.fullnm NOT LIKE '%TERMINATION%'
	   			 	  AND A.LOCCD NOT IN ('ADJ', 'C/S', 'D/S','PCC','LINTAS')
	   			 	  AND A.LOCCD NOT LIKE '%C/S%'
	   			 	  AND A.LOCCD NOT LIKE '%D/S%'
	   			 	  AND A.LOCCD NOT LIKE '%BLK1%'
	   			 	  AND A.LOCCD NOT LIKE '%CPC%'
	   			 	  AND A.LOCCD NOT LIKE '%GCX%'
	   			 	  AND A.LOCCD NOT LIKE '%CELEBES%'
	   			 	  AND A.fullnm NOT LIKE '%CANCEL%'
	   			 	  AND A.fullnm NOT LIKE '%CENCEL%'
	   			 ORDER BY A.sctype, A.LOCCD";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getListRecapSales($param, $bnsmonth=null, $bnsyear=null) {
		$qryParam = "";
		if($bnsyear != "" || $bnsyear != null){
			$period = "$bnsyear-$bnsmonth-01";
			if($bnsmonth == "all"){
				$bnsmonthParam = "";
			}else{
				$bnsmonth = intval($bnsmonth);
				$bnsmonthParam = " AND A.bonusmth = '$bnsmonth'";
			}

			$qryPeriod = " WHERE A.bonusyr = '$bnsyear' $bnsmonthParam ";

			//echo $param;
			if($param != "all" && $param != "all-sc" && $param != "all-scandsub" && $param != "all-sub" && $param != "all-ms"){
				$qryParam = " AND A.sc_dfno = '$param' ";
			}elseif($param == "all-sc"){
				$qryParam = " AND A.sctype = '1' ";
			}elseif($param == "all-scandsub"){
				$qryParam = " AND A.sctype IN ('1','2') ";
			}elseif($param == "all-sub"){
				$qryParam = " AND A.sctype = '2' ";
			}elseif($param == "all-ms"){
				$qryParam = " AND A.sctype = '3' ";
			}

			$qryParam = " $qryPeriod $qryParam ";
			//echo "$qryParam<BR/>";
		}

		$qry = "SELECT A.sctype, A.sc_dfno, A.fullnm_sc, A.bonusmth, A.bonusyr,
					   SUM(A.tdp) AS tdp, SUM(A.ndp) AS ndp,
				       SUM(A.tbv) AS tbv, SUM(A.nbv) AS nbv
				FROM V_ECOMM_HILAL_SALES_STK a
				$qryParam
				GROUP BY A.sctype, A.sc_dfno, A.fullnm_sc, A.bonusmth, A.bonusyr
				ORDER BY a.sctype, a.fullnm_sc" ;
	   	//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}


	function getListBonus_Vcr($param, $rptType, $bnsmonth=null, $bnsyear=null) {
		$qryParam = "";
		if($bnsyear != "" || $bnsyear != null){
			$period = "$bnsyear-$bnsmonth-01";
			if($bnsmonth == "all"){
				$bnsmonthParam = "";
			}else{
				$bnsmonth = intval($bnsmonth);
				$bnsmonthParam = " AND A.bonusmonth = '$bnsmonth'";
			}

			$qryPeriod = " WHERE A.bonusyear = '$bnsyear' $bnsmonthParam ";

			//echo $param;
			if($param == "all"){
				$qryParam = " ";
			}elseif($param == "idr"){
				$qryParam = " AND A.countrycode = 'ID' AND ";
			}elseif($param == "sar"){
				$qryParam = " AND A.countrycode = 'SA' AND ";
			}elseif($param == "tl"){
				$qryParam = " AND A.countrycode = 'TL' AND ";
			}elseif($param == "xid"){
				$qryParam = " AND A.countrycode != 'ID' AND ";
			}

			$qryParam = " $qryPeriod $qryParam ";
			//echo "$qryParam<BR/>";
		}

		if($rptType == "cvr" or $rptType == "pvr"){
			if($rptType == "cvr"){
				$vchtype = "'C'";
			}else{
				$vchtype = "'P'";
			}
			$qry = "SELECT 	a.DistributorCode, a.VoucherNo, a.vchtype, a.VoucherAmtCurr, a.VoucherAmt,
							a.countrycode, a.bonusyear, a.bonusmonth
					FROM tcvoucher a
					$qryParam and vchtype=$vchtype
					ORDER BY a.bonusyear desc, a.bonusmonth desc, a.countrycode DESC, a.DistributorCode ASC" ;
		}if($rptType == "allvr" OR $rptType == "chq" OR $rptType == "chq_stk" OR $rptType == "novac"){

			$novac = "";

			if($rptType == "allvr"){
				$join = "INNER ";
				$netIncome = " ";
			}elseif($rptType == "chq"){
				$join = "LEFT OUTER ";
				$netIncome = " AND c.netincome >= 20 ";
			}elseif($rptType == "chq_stk"){
				$join = "LEFT OUTER ";
				$netIncome = " AND c.netincome BETWEEN 1 AND 50 ";
			}elseif($rptType == "novac"){
				$novac = " , d.novac";
				$join = " LEFT OUTER JOIN msmemb d ON c.DistributorCode=d.dfno
						  LEFT OUTER ";
				$netIncome = " AND c.netincome BETWEEN 1 AND 50 ";
			}


			$qry = "SELECT a.DistributorCode, d.fullnm, a.VoucherNo as 'CVoucherNo',
					   (SELECT B.VoucherNo
				       	FROM tcvoucher B
				        WHERE B.vchtype='P'
					  	      and B.VoucherAmtCurr < 100
				        	  and a.BonusMonth=B.BonusMonth
				              and a.BonusYear=B.BonusYear
				              AND a.DistributorCode=B.DistributorCode) AS 'PVoucherNo',
					   a.countrycode, a.bonusyear, a.bonusmonth, c.netincome, c.stockiestcode $novac
				FROM tbonus c
					 $join JOIN tcvoucher a ON a.BonusMonth=c.BonusMonth
				              and a.BonusYear=c.BonusYear AND a.DistributorCode=c.DistributorCode
				     LEFT OUTER JOIN msmemb d on c.distributorcode=d.dfno
				$qryParam AND a.vchtype='C' $netIncome";
		}

	   	echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function listWHFromGNV(){
		$qry = "select A.id, A.[description], A.kode, A.warehouse_name
				from wmswarehousemaster a
				where a.[description]='WAREHOUSE'
				order by a.[description]" ;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function saveImportToDB($data, $whcdx) {
		$totalRecord = 0;
		$double = 0;
		$insSuccess = 0;
		$insFail = 0;
		$arrFail = array();
		$arrDouble = array();

		$whcdy = explode("***", $whcdx);
		$whcd = $whcdy[0];
		$whnm = $whcdy[1];
		$whid = $whcdy[2];

		//$nmTbl = "WMS_STOCK_AKHIR_upload";
		$nmTbl = "WMS_STOCK_AKHIR";

		$i = 0;
		foreach($data['xlsData'] as $list) {
			if(is_numeric($list[0][1]) && $list[0][0] != ""){
				if($i > 0){
					$totalRecord++;
					$check = $this->checkDataStockAwal($list[0][0], $whcd, $nmTbl);
					if($check) {

						$createDate = date("Y-m-d h:i:s");

						$qry = "INSERT INTO $nmTbl (kode_barang, stock_akhir, wms_warehouse	, wms_warehouse_name)
							    VALUES ('".$list[0][0]."', ".$list[0][1].", '".$whcd."', '".$whnm."')";
						//array_push($arrQry, $qry);
						//echo $qry."<BR/>";

					    $insExec = $this->executeQuery($qry, $this->db2);
						if($insExec > 0) {
							$insSuccess++;
						} else {
							$insFail++;
							array_push($arrFail, $list[0][0]);
						}
						//echo $qry;
					} else {
						$double++;
						array_push($arrDouble, $list[0][0]);
					}
				}
				$i++;
			}

	    }

		if($insSuccess > 0){ //if upload stock more than 0 then update stock real to GNV
			$updqry = "EXEC SP_Hilal_UpdStockWMS_sentul '$whcd', '$whnm'";
			$SPExec = $this->executeQuery($updqry, $this->db2);
			if($SPExec > 0) {
				$msgSP = "Succes update stock.";
			} else {
				$msgSP = "Failed update stock.";
				array_push($arrFail, $msgSP);
			}
		}

		$arr = array("totalRecord" => $totalRecord,
					"double" => $double,
					"insSuccess" => $insSuccess,
					"insFail" => $insFail,
					"arrFail" => $arrFail,
					"arrDouble" => $arrDouble,
					//"msgSP" => $msgSP,
					);
		return $arr;
	}


	function checkDataStockAwal($id, $whcd, $nmTbl) {
		$mdReturn = false;
		$qry = "SELECT * FROM $nmTbl WHERE kode_barang = '$id' AND wms_warehouse ='$whcd'";
		$res = $this->getRecordset($qry, null, $this->db2);
		if($res == null) {
			$mdReturn = true;
		}
		return $mdReturn;
	}

	function delDataStockAwal($whcdx) {
		$whcdy = explode("***", $whcdx);
		$whcd = $whcdy[0];
		$whnm = $whcdy[1];
		$whid = $whcdy[2];
		//$nmTbl = "WMS_STOCK_AKHIR_upload";
		$nmTbl = "WMS_STOCK_AKHIR";

		$mdReturn = false;
		$qry = "DELETE FROM $nmTbl WHERE wms_warehouse ='$whcd'";
		$res = $this->executeQuery($qry, $this->db2);
		if($res == null) {
			$mdReturn = true;
		}
		return $mdReturn;
	}

	function listBnsPeriod(){
		$qry = "select CONVERT(VARCHAR(10), A.currperiod, 111) AS currperiod,
					   CONVERT(VARCHAR(10), A.currperiodSCO, 111) AS currperiodSCO,
					   CONVERT(VARCHAR(10), A.currperiod, 111) AS currperiod1
			 	from syspref a" ;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function showSysPref() {
		$qry = "SELECT CONVERT(VARCHAR(10), A.currperiodSCO, 111) as bns_stk,
		           CONVERT(VARCHAR(10), A.currperiodBO, 111) as bns_bo,
				   CONVERT(VARCHAR(10), A.currperiod, 111) as bns_inv
				   FROM syspref a";
		//echo $qry;
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function uptBnsPeriod($period, $param){
		$mdReturn = false;
		if($param == "1"){
			$paramUpd = "currperiodSCO = '$period'";
		}elseif($param == "2"){
			$paramUpd = "currperiodBO = '$period'";
		} elseif($param == "3") {
			$paramUpd = "currperiod = '$period'";
		}

		$qry = "UPDATE syspref SET $paramUpd" ;
		echo "$qry<BR/>";
		$res = $this->executeQuery($qry, $this->db2);
		if($res == null) {
			$mdReturn = true;
		}
		return $res;
	}

	function getTrxShippingByKWNo($id) {
		$qry = "select a.dfno, b.fullnm, a.registerno, a.trcd, a.tdp, a.tbv,
                    a.totpay, a.ship, a.whcd
                    from ordhdr a inner join msmemb b on (a.dfno=b.dfno)
                    where a.applyto = '$id'";
	    $res = $this->getRecordset($qry, null, $this->db2);
	    if($res == null) {
	    	$tipe_trans = "CN/MSN";
	    	//Jika null maka
	    	$cn = "select a.dfno, b.fullnm,a.registerno, a.trcd, a.tdp, a.tbv,
                    a.totpay, a.ship, a.whcd
                    from ordivhdr a inner join mssc b on (a.dfno=b.loccd)
                    where a.applyto = '$id'";
			$res2 = $this->getRecordset($cn, null, $this->db2);
			if($res2 != null) {
				$arr = array("response" => "true", "trx_type" => $tipe_trans, "arrayData" => $res2);
			} else {
				$arr = array("response" => "false", "message" => "Invalid Receipt No..");
			}
	    } else {
	    	$tipe_trans = "INVOICE";
			$arr = array("response" => "true", "trx_type" => $tipe_trans, "arrayData" => $res);
	    }
	}

	function getTrxShippingByKWNo2($id) {
		$arr = array("response" => "false", "message" => "Invalid Receiptno");
		/*$qry = "SELECT a.ordtype, a.receiptno, a.registerno, a.dfno, a.ship, a.tdp, a.tbv
		        FROM ordtrh a WHERE a.receiptno = '$id'";	*/
		$qry = "SELECT a.ordtype, a.receiptno, a.registerno,  a.ship, SUM(a.tdp) as tdp, SUM(a.tbv) as tbv
				FROM ordtrh a WHERE a.receiptno = '$id'
				GROUP BY a.ordtype, a.receiptno, a.registerno,  a.ship";
		$res = $this->getRecordset($qry, null, $this->db2);
		if($res == null) {
			$tipe_trans = "CN/MSN";
			$qryCN = "SELECT a.receiptno, a.registerno, a.loccd as dfno, a.ship, SUM(a.tdp) as tdp, SUM(a.tbv) as tbv, a.shipto
						FROM ordivtrh a WHERE a.receiptno = '$id'
						GROUP BY a.receiptno, a.registerno, a.loccd, a.ship,  a.shipto";
			$res2 = $this->getRecordset($qryCN, null, $this->db2);
			$arr = array("response" => "true", "trx_type" => $tipe_trans, "arrayData" => $res2);
		} else {
	    	$tipe_trans = "INVOICE";
			$arr = array("response" => "true", "trx_type" => $tipe_trans, "arrayData" => $res);
	    }
	    return $arr;
	}

	function updateShipping($no_kw, $no_reg, $ship) {
		$arr = $this->getTrxShippingByKWNo2($no_kw);
		if($arr['response'] == "true") {
			$db_qryx = $this->load->database('klink_mlm2010', true);
            $db_qryx->trans_begin();
			if($arr['trx_type'] == "INVOICE") {
				$upd1 = "UPDATE ordtrh SET ship = '$ship' WHERE receiptno = '$no_kw'";
				$upd2 = "UPDATE ordhdr SET ship = '$ship' WHERE registerno = '$no_reg'";
				$upd3 = "UPDATE newtrh SET ship = '$ship' WHERE receiptno = '$no_kw'";
				$upd4 = "UPDATE intrh SET ship = '$ship' WHERE applyto = '$no_kw'";

			} else if($arr['trx_type'] == "CN/MSN") {
				$upd1 = "UPDATE ordivtrh SET ship = '$ship' WHERE receiptno = '$no_kw'";
				$upd2 = "UPDATE ordivhdr SET ship = '$ship' WHERE registerno = '$no_reg'";
				$upd3 = "UPDATE newivtrh SET ship = '$ship' WHERE receiptno = '$no_kw'";
				$upd4 = "UPDATE intrh SET ship = '$ship' WHERE applyto = '$no_kw'";
			}
			$exeUpd1 = $db_qryx->query($upd1);
			$exeUpd2 = $db_qryx->query($upd2);
			$exeUpd3 = $db_qryx->query($upd3);
			$exeUpd4 = $db_qryx->query($upd4);

			if ($db_qryx->trans_status() === FALSE) {
				$db_qryx->trans_rollback();
				return array("response" => "false", "message" => "Update Shipping Gagal");
			} else {
				$db_qryx->trans_commit();
				return array("response" => "true", "message" => "Update Shipping berhasil..");
			}
		} else {
			return $arr;
		}
	}


    function getOperatorSMS() {
		$qry = "SELECT A.id, A.operator_desc, a.is_activated
				FROM sms_operator A
				ORDER BY A.id ";
		$res = $this->getRecordset($qry);
		return $res;
	}

	function listTtpById($field, $value) {

		$qry = "SELECT a.trcd, a.orderno, a.dfno, b.fullnm,
				   a.totpay, a.tbv, CONVERT(VARCHAR(10),a.bnsperiod, 121) as bnsperiod, a.no_deposit
				 FROM sc_newtrh a
				 INNER JOIN msmemb b ON (a.dfno = b.dfno)
				 WHERE a.$field = '$value'
				 ORDER BY trcd ASC";
		$hasil1 = $this->getRecordset($qry, null, $this->db2);
		return $hasil1;
	}

	function getDepositVchInfo($value) {
		$qry1 = "SELECT * FROM deposit_H a WHERE a.no_trx = '$value'";
		$hasil1 = $this->getRecordset($qry1, null, $this->db2);

		$qry2 = "SELECT a.voucher_scan, a.no_trx, a.nominal, a.dfno,
					b.fullnm, CONVERT(VARCHAR(10),a.createdt, 121) as createdt, a.kategori, a.is_active, a.createnm
				FROM deposit_D a
				LEFT OUTER JOIN msmemb b ON (a.dfno COLLATE SQL_Latin1_General_CP1_CS_AS = b.dfno)
				WHERE a.no_trx = '$value'
				ORDER BY a.voucher_scan";
		$hasil2 = $this->getRecordset($qry2, null, $this->db2);

		$qry3 = "SELECT a.orderno, a.trcd, a.dfno, a.bnsperiod, a.etdt,
							a.totpay, a.tdp, a.tbv,
							a.sc_dfno, a.sc_co, a.loccd, a.createnm,
							a.flag_batch, a.batchno, a.flag_recover, a.csno, a.no_deposit,
							CONVERT(VARCHAR(10),a.batchdt, 121) as batchdt, a.flag_approval,
							a.ttptype, a.trtype, a.pricecode
				FROM sc_newtrh a
				WHERE a.no_deposit = '$value'";
				//LEFT OUTER JOIN msmemb b ON (a.dfno = b.dfno)
		$hasil3 = $this->getRecordset($qry3, null, $this->db2);

		$arr = array(
			"deposit_header" => $hasil1,
			"list_vch" => $hasil2,
			"list_ttp" => $hasil3,
		);

		return $arr;
	}

	function getSummaryTrxBySSR($field, $ssrno) {
		$qry = "SELECT
				    COUNT(a.trcd) as jum_ttp,
					SUM(a.totpay) AS total_dp,
					SUM(a.tbv) as total_bv,
					a.sc_dfno, a.sc_co, a.loccd, a.createnm,
					a.flag_batch, a.batchno, a.flag_recover, a.csno, a.no_deposit,
					CONVERT(VARCHAR(10),a.batchdt, 121) as batchdt,
					CONVERT(VARCHAR(10),a.bnsperiod, 121) as bnsperiod
				FROM sc_newtrh a
				WHERE $field = '$ssrno'
				GROUP BY
				 a.sc_dfno, a.sc_co, a.loccd, a.createnm,
				 a.flag_recover, a.csno, a.no_deposit,
				 a.flag_batch, a.batchno, CONVERT(VARCHAR(10),a.batchdt, 121),
				 CONVERT(VARCHAR(10),a.bnsperiod, 121)";
		//echo $qry;
		$hasil1 = $this->getRecordset($qry, null, $this->db2);
		if($hasil1 != null) {

			$qry2 = "SELECT SUM(b.payamt) as amount,
							b.paytype, c.[description], b.trcd, b.trcd2, d.refno
					FROM sc_newtrp_vc_det b
					INNER JOIN paytype c ON (b.paytype = c.id)
					LEFT OUTER JOIN bbhdr d on (b.trcd2 = d.trcd)
					where b.trcd = '".$hasil1[0]->batchno."'
					group by b.paytype, c.[description], b.trcd , b.trcd2, d.refno";
			$hasil2 = $this->getRecordset($qry2, null, $this->db2);



			$qry4 = "SELECT a.no_trx, CONVERT(VARCHAR(10),a.createdt, 121) as createdt,
						a.dfno, a.loccd, a.total_deposit, a.total_keluar, a.[status]
					FROM deposit_H a
					WHERE a.no_trx = '".$hasil1[0]->no_deposit."'";
			$hasil4 = $this->getRecordset($qry4, null, $this->db2);

			$returnArr = array(
				"trx_ssr" => $hasil1,
				"newtrp" => $hasil2,
				//"bbhdr" => $hasil3,
				"deposit" => $hasil4
			);
		} else {
			$returnArr = array(
				"trx_ssr" => null,
			);
		}



		return $returnArr;

	}

	function getSummaryIPVchCash($ssrno) {
		$qry = "SELECT
		            COUNT(a.trcd) as jum_ttp,
					SUM(a.totpay) AS total_dp,
					SUM(a.tbv) as total_bv,
					a.sc_dfno, a.sc_co, a.loccd, a.createnm,
					a.flag_batch, a.batchno, a.flag_recover, a.csno, a.no_deposit, id_deposit
					CONVERT(VARCHAR(10),a.batchdt, 121) as batchdt
				FROM sc_newtrh a
				WHERE a.batchno = '$ssrno'
				GROUP BY
				 a.sc_dfno, a.sc_co, a.loccd, a.createnm,
				 a.flag_recover, a.csno, a.no_deposit,
				 a.flag_batch, a.batchno, CONVERT(VARCHAR(10),a.batchdt, 121)";
		$hasil1 = $this->getRecordset($qry, null, $this->db2);
		if($hasil1 != null) {

			$qry2 = "SELECT SUM(b.payamt) as amount,
							b.paytype, c.[description], b.trcd, b.trcd2, d.refno
					FROM sc_newtrp_vc_det b
					INNER JOIN paytype c ON (b.paytype = c.id)
					LEFT OUTER JOIN bbhdr d on (b.trcd2 = d.trcd)
					where b.trcd = '$ssrno'
					group by b.paytype, c.[description], b.trcd , b.trcd2, d.refno";
			$hasil2 = $this->getRecordset($qry2, null, $this->db2);



			$qry4 = "SELECT a.no_trx, CONVERT(VARCHAR(10),a.createdt, 121) as createdt,
						a.dfno, a.loccd, a.total_deposit, a.total_keluar, a.[status]
					FROM deposit_H a
					WHERE a.no_trx = '".$hasil1[0]->no_deposit."'";
			$hasil4 = $this->getRecordset($qry4, null, $this->db2);

			$returnArr = array(
				"trx_ssr" => $hasil1,
				"newtrp" => $hasil2,
				//"bbhdr" => $hasil3,
				"deposit" => $hasil4
			);
		} else {
			$returnArr = array(
				"trx_ssr" => null,
			);
		}



		return $returnArr;

	}

	function getListVcNewtrp($ssrno) {

		return $res;
	}

	function recoverGeneratedSSR($ssrno) {
		$upd = "UPDATE sc_newtrh SET flag_batch = '0', flag_recover = '1' batchdt = null
		        WHERE batchno = '$ssrno'";
	}

	function recoverSSR($arr) {
	    $db_qryx = $this->load->database('klink_mlm2010', true);
        $db_qryx->trans_begin();

		$ssrno = $arr['batchno'];
		/*--------------------------------------------------------
		INSERT DATA YANG AKAN DI RECOVER KE TABLE SSR_RECOVERY_TBL
		----------------------------------------------------------*/
		$qry = "INSERT INTO ssr_recover_tbl (trcd, batchno, total_dp, total_bv)
		        SELECT a.trcd, a.batchno, a.tdp, a.tbv
				FROM sc_newtrh a
				WHERE a.batchno = '$ssrno'";
		//echo $qry;
		$db_qryx->query($qry);

		/*--------------------------------------------------------
		RECOVER SALES REPORT
		----------------------------------------------------------*/
		$upd = "UPDATE sc_newtrh SET flag_batch = '0', flag_recover = '1' batchdt = null
		        WHERE batchno = '$ssrno'";
		//$recover = $db_qryx->query($upd);
		//diaktifkan saat LIVE

		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			return array("response" => "false", "message" => "Recover Gagal");
		} else {
			$db_qryx->trans_commit();
			return array("response" => "true", "message" => "Recover Sales Report $ssrno berhasil..");
		}
	}

	/**
	 * Get vcip details
	 * @param varchar $trcd
	 */
	function getVcipDetail($trcd) {

		// Voucher Cash Incoming Payment detail
		$query_b = "SELECT
						trcd,
						refno,
						description,
						amount,
						etdt,
						dfno
					FROM
						bbhdr
					WHERE
						trcd = '$trcd'";
		$res_b = $this->getRecordset($query_b, null, $this->db2);

		$query_s = "SELECT
						trcd,
						paytype,
						vhcno,
						payamt
					FROM
						sc_newtrp_vc_det
					WHERE
						trcd2 = '$trcd'";
		$res_s = $this->getRecordset($query_s, null, $this->db2);

		$query_c = "SELECT * FROM custpaydet a WHERE a.applyto = '$trcd'";
		$res_c = $this->getRecordset($query_c, null, $this->db2);
		// end

		if (!empty($res_b || $res_s)) {
			$recover = $this->checkingSSR($res_s[0]->trcd); // checking SSR
			// collect'em all as array
			$returnArr = array(
				"bbhdr" => $res_b,
				"sc_newtrp_vc_det" => $res_s,
				"custpaydet" => $res_c,
				"recover" => $recover
			);
		} else {
			$returnArr = NULL;
		}
		return $returnArr;
	}

	function checkingSSR($ssr){
		$qry = "SELECT batchno FROM klink_mlm2010.dbo.sc_newtrh WHERE batchno = '$ssr'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function recoverVoucher($ips, $vch) {
		$db_qryx = $this->load->database('klink_mlm2010', true);
		$db_qryx->trans_begin();

		$sql = "DELETE FROM bbhdr where trcd = '$ips'";
		$exeSql = $db_qryx->query($sql);
		$sql1 = "DELETE FROM sc_newtrp_vc_det where trcd2 = '$ips'";
		$exeSql1 = $db_qryx->query($sql1);
		$sql2 = "UPDATE deposit_H SET status='1' WHERE no_trx = '$vch'";
		$exeSql2 = $db_qryx->query($sql2);

		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			return array("response" => "false", "message" => "Recover $ips Gagal");
		} else {
			$db_qryx->trans_commit();
			return array("response" => "true", "message" => "Recover $ips berhasil..");
		}

		// $execQuery = $this->executeQuery($sql, $this->db1);
		// $execQuery1 = $this->executeQuery($sql1, $this->db1);
		// $execQuery2 = $this->executeQuery($sql2, $this->db1);

		/*echo "<pre>";
		echo $sql."<br />";
		echo $sql1."<br />";
		echo $sql2."<br />";
		echo "</pre>";*/
	}

	/**
	 * @Author: Ricky
	 * @Date: 2019-03-21 16:09:50
	 * @Desc: Recover Trx updating data
	 * @param mixed $loccd
	 * @param mixed $sc_co
	 * @param mixed $sc_dfno
	 * @param mixed $bnsperiod
	 * @param mixed $trcd
	 * @return string|array
	 */
	function updateRecoverTrx($loccd, $sc_co, $sc_dfno, $bnsperiod, $trcd) {
		$db_qryx = $this->load->database('klink_mlm2010', true);
		$db_qryx->trans_begin();
		$query = "UPDATE klink_mlm2010.dbo.sc_newtrh
							SET loccd = '$loccd', sc_co = '$sc_co', sc_dfno = '$sc_dfno', bnsperiod = '$bnsperiod'
							WHERE trcd = '$trcd'";
		$res = $db_qryx->query($query);
		if ($db_qryx->trans_status() === FALSE) {
			$db_qryx->trans_rollback();
			return array(
				"response" => "false",
				"message" => "Recover Gagal"
			);
		} else {
			$db_qryx->trans_commit();
			return array(
				"response" => "true",
				"message" => "Recover berhasil..."
			);
		}
	}

	/**
	 * @Author: Ricky
	 * @Date: 2019-05-07 10:07:28
	 * @Desc: Remove Trx
	 * @param string $trcd
	 * @return string|array
	 */
	function deleteTrx($trcd) {
		$db_qryx = $this->load->database('klink_mlm2010', TRUE);
		$query_check = "SELECT sc_newtrh.trcd, sc_newtrh.dfno, sc_newtrh.trtype, sc_newtrp.paytype, sc_newtrp.docno, tcvoucher.VoucherNo, sc_newtrp.payamt, tcvoucher.VoucherAmt, tcvoucher.DistributorCode, tcvoucher.claimstatus FROM dbo.sc_newtrh
										LEFT OUTER JOIN dbo.sc_newtrp ON sc_newtrh.trcd = sc_newtrp.trcd
										LEFT OUTER JOIN dbo.tcvoucher ON sc_newtrp.docno = tcvoucher.VoucherNo
										WHERE sc_newtrh.trcd = '$trcd'";
		$check =  $this->getRecordset($query_check, NULL, $this->db2);
		if ($check[0]->trtype == 'VP1') {
			if ($check[0]->docno === $check[0]->VoucherNo) {
				$db_qryx->trans_begin();
				$query_newtrh = "DELETE FROM klink_mlm2010.dbo.sc_newtrh WHERE trcd = '$trcd'";
				$query_newtrd = "DELETE FROM klink_mlm2010.dbo.sc_newtrd WHERE trcd = '$trcd'";
				echo $query_newtrh . '<br>' . $query_newtrd;
				/* $res_newtrh = $db_qryx->query($query_newtrh);
				$res_newtrd = $db_qryx->query($query_newtrd);
				if ($db_qryx->trans_status() === FALSE) {
					$db_qryx->trans_rollback();
					return array(
						"response" => "false",
						"message" => "Data gagal dihapus..."
					);
				} else {
					$db_qryx->trans_commit();
					return array(
						"response" => "true",
						"message" => "Data berhasil dihapus..."
					);
				} */
			}
		} elseif ($check[0]->trtype == 'SB1') {

		}
	}
}
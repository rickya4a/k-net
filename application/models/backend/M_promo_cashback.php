<?php
class M_promo_cashback extends MY_Model {

  function __construct() {
        // Call the Model constructor
        parent::__construct();

  }

  function rekapPromoRecruiter($from, $to) {
    $qry = "SELECT
          a.sfno_reg,
          a.rekruitername,
          COUNT(a.id) as jml_rekrutan,
          SUM(a.nominal_cashback) as total_cashback,
          b.bankaccno as accountno, b.bankid, b.bankaccnm as accountnm
        FROM cashback_rekruiter_detail a
        INNER JOIN klink_mlm2010.dbo.msmemb b ON (a.sfno_reg = b.dfno)
        WHERE a.[status] = '0' AND (batchno is null OR a.batchno = '')
           AND CONVERT(VARCHAR(10), a.jointdt, 111) between '$from' and '$to'
        GROUP BY a.sfno_reg, a.rekruitername, b.bankaccnm, b.bankaccno, b.bankid";
      //echo $qry;
    return $this->getRecordset($qry, NULL, $this->db1);
  }

  function listDataDetailCashback($from, $to) {
    $qry = "SELECT id FROM cashback_rekruiter_detail a
        WHERE a.[status] = '0' AND (batchno is null OR a.batchno = '')
          AND CONVERT(VARCHAR(10), a.jointdt, 111) between '$from' and '$to'";
      //echo $qry;
    return $this->getRecordset($qry, NULL, $this->db1);
  }

  function insertCashbackHdr($from, $to) {
    $batchno = randomNumber(3);
    $qry = "INSERT INTO cashback_rekruiter_summary (batchno, sfno_reg, total_cashback, jml_rekrutan, accountno, bankid, createdt)
            SELECT
              '$batchno',
          a.sfno_reg,
          SUM(a.nominal_cashback) as total_cashback,
          COUNT(a.id) as jml_rekrutan,
          b.bankaccno as accountno, b.bankid,
          '$this->dateTime'
        FROM cashback_rekruiter_detail a
        INNER JOIN klink_mlm2010.dbo.msmemb b ON (a.sfno_reg = b.dfno)
        WHERE a.[status] = '0' AND (batchno is null OR a.batchno = '')
           AND CONVERT(VARCHAR(10), a.jointdt, 111) between '$from' and '$to'
        GROUP BY a.sfno_reg, b.bankaccno, b.bankid";
    $res1 = $this->executeQuery($qry);
    if($res1 > 0) {
      $res = $this->listDataDetailCashback($from, $to);
      foreach($res as $id) {
        $arr[] = $id->id;
      }
      $ss = set_list_array_to_string($arr, "");
      $upd = "UPDATE cashback_rekruiter_detail
              SET batchno = '$batchno', batchdt = '".$this->dateTime."', status = '1'
              WHERE id IN ($ss)";
        $upd1 = $this->executeQuery($upd);
        if($upd1 > 0) {
          return array("batchno" => $batchno);
        }
    } else {
      return array("batchno" => null);
    }
  }

  function getListBatch($from, $to) {
    $qry = "SELECT DISTINCT(a.batchno) AS batchno, COUNT(a.sfno_reg) as jml_member, CONVERT(VARCHAR(10), a.createdt, 111) AS batchdt
        FROM cashback_rekruiter_summary a
        WHERE CONVERT(VARCHAR(10), a.createdt, 111) between '$from' and '$to'
        GROUP BY a.batchno, a.createdt";
    return $this->getRecordset($qry, NULL, $this->db1);
  }

  function getDetailByBatchNo($batchno) {
    $qry = "SELECT a.*, b.fullnm as rekruitername
            FROM cashback_rekruiter_summary a
            INNER JOIN klink_mlm2010.dbo.msmemb b ON (a.sfno_reg = b.dfno)
            WHERE batchno = '$batchno'";
    return $this->getRecordset($qry, NULL, $this->db1);
  }

  function updateDetailBatch($data) {
    $db_qryx = $this->load->database('db_ecommerce', true);

    $db_qryx->trans_begin();
    $count = count($data['batchno']);
    $success = 0;
    $fail = 0;
    for($i=0;$i<$count;$i++) {
      $qry = "UPDATE cashback_rekruiter_summary SET
                bankid = '".$data['bankid'][$i]."', accountno = '".$data['accountno'][$i]."',
                status_trf = '".$data['status_trf'][$i]."', tgl_trf = '".$data['tgl_trf'][$i]."',
                remark = '".$data['remark'][$i]."'
              WHERE batchno = '".$data['batchno'][$i]."' AND sfno_reg = '".$data['sfno_reg'][$i]."'";
      //echo $qry;
      $exeQry = $db_qryx->query($qry);
      if($exeQry > 0) {
        $success++;
      } else {
        $fail++;
      }
    }

    if ($db_qryx->trans_status() === FALSE) {
      $db_qryx->trans_rollback();
      return array("response" => "false","success" => $success,  "fail" => $fail, "message" => "Update Batch gagal..");
    } else {
      $db_qryx->trans_commit();
      return array("response" => "true","success" => $success,  "fail" => $fail, "message" => "Update Batch berhasil..");
    }
  }
}
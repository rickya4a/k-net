<?php
class Be_toto_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }

    function tambahan() {
        
    }
	
	function getListStokistM() {
		$slc = "select a.loccd,a.fullnm from mssc a
                where a.sctype = '1' and a.onlinetype = 'O' 
                and a.fullnm not in('TERMINATION','CANCEL','CENCEL')
                AND a.scstatus = '1' order by a.loccd";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
	}
	
	function getJadwalKeberangkatanM(){
        $slc = "select a.id,CONVERT(VARCHAR(10),a.departuredt,105) as departuredt,
                     a.departuredesc
                from ecomm_umroh_departure a
                where a.dep_status = '1' AND type_ks = 1
                order by a.departuredt";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	
	function getJadwalZiarahM() {
		$slc = "select a.id,CONVERT(VARCHAR(10),a.departuredt,105) as departuredt,
		           a.departuredesc
                from ecomm_umroh_departure a
                where a.dep_status = '1' AND type_ks = 2
                order by a.departuredt";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
	}
	
	function getDistributorInfo($idmember) {
		 $slc = "select a.dfno,a.fullnm,a.addr1,a.tel_hp,
                a.tel_hm,a.idno,a.email, a.sex, a.bnsstmsc,
                CONVERT(VARCHAR(10),a.birthdt,105) as birthdt,
                a.sponsorid, a.birthpl, b.fullnm as sponsorname
                from msmemb a
                LEFT OUTER JOIN msmemb b ON (a.sponsorid = b.dfno)
                where a.dfno = '".$idmember."' AND a.fullnm != 'TERMINATION'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db2);
        return $result;
	}
	
	function getCheckNoKTP($noktp) {
		$slc = "select top 1 a.fullnm,a.idno
                from ecomm_umroh_save_child a
                where a.idno = '".$noktp."' AND a.depart_status = '0'
                and a.idno is not null";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
	}
	
	function getCheckNoHP($nohp){
        $slc = "select TOP 1 a.fullnm,a.tel_hp
                from ecomm_umroh_save_child a
                where a.tel_hp = '".$nohp."' AND a.depart_status = '0'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
    }
	

	
	function cekSeq($type_ks){
        $this->db = $this->load->database('db_ecommerce', true);
        date_default_timezone_set("Asia/Jakarta");
        $y1=date("Y");
        
        $tbl = "";
		$prefid = "";
        if($type_ks == "1") {
        	 $prefid = "U";
			 $tbl = "ecomm_umroh_seq";	
        } else {
        	 $prefid = "Z";
			 $tbl = "ecomm_ziarah_seq";	
        }
        
        $cek = "select * from $tbl";
        
        $query = $this->db->query($cek);
        if($query->num_rows < 1)
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "sip<br>";
        }
        else
        {
            $input = "insert into $tbl (SeqVal) values('b')";
            $query = $this->executeQuery($input, $this->setDB(1));
            //echo "<br>ada<br>";
        }      
    }
    
    function getRegisterNo($type_ks){
        $this->db = $this->load->database('db_ecommerce', true);
        $tbl = "";
		$prefid = "";
        if($type_ks == "1") {
        	 $prefid = "U";
			 $tbl = "ecomm_umroh_seq";	
        } else {
        	 $prefid = "Z";
			 $tbl = "ecomm_ziarah_seq";	
        }
       
                   
        $sql = "SELECT * FROM $tbl 
           		 WHERE SeqID = ( SELECT MAX(SeqID) FROM $tbl )";
	    //echo $sql;
        $query = $this->db->query($sql);
        if($query == null)
        {
            $ss = 0;
			
        }
        else
        {
            foreach($query->result() as $data)
            {
                $ss = $data->SeqID;
            }  
        }
        //$jumlah = $query->num_rows();
        
        $next_seq = sprintf("%08s",$ss);
        $registerNo  = $prefid."$next_seq";
		
        return $registerNo; 
        //echo $sql;
    }
	
	function checkDoubleInputUmroh($arr) {
		$tipe = $arr['tipeJamaah'];
		$jdwl = substr($arr['jdwlbrkt'],0,1);	
		$dfno = strtoupper($arr['idmember']);
		$fullnm = strtoupper($arr['fullnm']);
		$slc = "select TOP 1 a.dfno, a.fullnm, a.idno, a.departureid, flag_tipe,
				a.createnm, CONVERT(VARCHAR(10),a.createdt,105) as createdt
                from ecomm_umroh_save_child a
                where a.idno = '".$arr['idno']."' 
                AND a.dfno = '$dfno'
                AND a.fullnm = '$fullnm'
                AND a.departureid = $jdwl
                AND a.flag_tipe = '$tipe'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
	}
	
	function saveDataUmroh($regNo, $sequence, $arr) {
		$ecomm = $this->session->userdata('ecom_user');	
		
		 $this->db->trans_begin();
		
		$insParent = "insert into ecomm_umroh_save_parent(dfno,fullnm,tel_hp,email,tot_pass,registerno,
                        tot_transfer,createnm)
                        values('$ecomm','$ecomm','00','00',
                        1,'".$regNo."',0,'$ecomm')";
        //echo "qry parent ".$insParent."<br>";
        $qryParent = $this->executeQuery($insParent, $this->setDB(1));
        
        if($arr['idmember'] == ""){
                $dfno = "";
        }else{
            $dfno = strtoupper($arr['idmember']);
        }
        
        $fullnm = strtoupper($arr['fullnm']);
        $idno = $arr['idno'];
        $birthplace = strtoupper($arr['birthplace']);
        //$birthdate1 = $arr['tgllhr']."/".$arr['blnlhr']."/".$arr['thnlhr'];
        //$birthdate = date("d/m/Y",strtotime($birthdate1));
		$birthdate1 = $arr['blnlhr']."/".$arr['tgllhr']."/".$arr['thnlhr']."";
        $birthdate = date("Y-m-d",strtotime($birthdate1));
        $tel_hp = $arr['tel_hp'];
        $emailjamaah = $arr['email'];
        $jdwl = substr($arr['jdwlbrkt'],0,1);
        $tipe = $arr['tipeJamaah'];
        $addr1 = strtoupper($arr['addr1']);
        $addr2 = strtoupper($arr['addr2']);
        /*$kecamatan = strtoupper($dt['kecamatan']);
        $kota = strtoupper($dt['kota']);
        $kelurahan = strtoupper($dt['kelurahan']);*/
        //$kota = strtoupper($arr['kota']);
        $passportno = strtoupper($arr['passportno']);
        $passportnm = strtoupper($arr['passportnm']);
        $sex = strtoupper($arr['sex']);
        $fathername = strtoupper($arr['fathersnm']);
        //$prov = strtoupper($arr['prov']);
        $idsponsor = strtoupper($arr['idsponsor']);
        $nmsponsor = strtoupper($arr['nmsponsor']);
        $idstk = strtoupper($arr['idstk']);
		
        /*if($tipe == "3"){
            $idstk = strtoupper($arr['idstk']);
        }else{
            $idstk = "";
        }*/
		
		$slc = "select id from ecomm_umroh_save_parent
                    where registerno = '".$regNo."'";
            $query = $this->db->query($slc);
            if($query->num_rows() > 0){
                foreach($query->result() as $data)
                {   
                    $id = $data->id;
                }
            }else{
                echo "no data";
            }
            
            if($tipe == "3") {
                $insChild = "insert into ecomm_umroh_save_child
                        (registerno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,createnm,flag_tipe,idparent,
                        addr1,passportno,passportnm,flag_paytype,sex,father_name,loccd,sfno, tipe_perjalanan)
                        values('".$regNo."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                        '".$tel_hp."','".$emailjamaah."','".$jdwl."','".$ecomm."','".$tipe."',".$id.",'".$addr1."',
                        '".$passportno."','".$passportnm."','1','".$sex."','".$fathername."','".$idstk."','".$idsponsor."', '$arr[type_ks]')";
                        
                
						
            } elseif($tipe == "2") {
            	 $insChild = "insert into ecomm_umroh_save_child
                        (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,createnm,flag_tipe,idparent,
                        addr1,passportno,passportnm,flag_paytype,sex,father_name, loccd, sfno, tipe_perjalanan)
                        values('".$regNo."','','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                        '".$tel_hp."','".$emailjamaah."','".$jdwl."','".$ecomm."','".$tipe."',".$id.",'".$addr1."',
                        '".$passportno."','".$passportnm."','1','".$sex."','".$fathername."', '".$idstk."', '".$arr['idmember']."', '$arr[type_ks]')";
						
				 	
            	
            }
            
            else {
                $insChild = "insert into ecomm_umroh_save_child
                        (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,createnm,flag_tipe,idparent,
                        addr1,passportno,passportnm,flag_paytype,sex,father_name, loccd, tipe_perjalanan)
                        values('".$regNo."','".$dfno."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                        '".$tel_hp."','".$emailjamaah."','".$jdwl."','".$ecomm."','".$tipe."',".$id.",'".$addr1."',
                        '".$passportno."','".$passportnm."','1','".$sex."','".$fathername."', '".$idstk."', '$arr[type_ks]')";
                        
				
                 //echo "qry mutasi ".$insMutasi."<br>";
            
            } 
            
            
            /*$insChild = "insert into ecomm_umroh_save_child
                        (registerno,dfno,fullnm,idno,birthplace,birthdate,tel_hp,email,departureid,createnm,flag_tipe,idparent,
                        addr1,kecamatan,kota,kelurahan,passportno,passportnm,flag_paytype,sex,father_name,provinsi,loccd,sfno)
                        values('".$regNo."','".$dfno."','".$fullnm."','".$idno."','".$birthplace."','".$birthdate."',
                        '".$tel_hp."','".$emailjamaah."','".$jdwl."','".getUserID()."','".$tipe."',".$id.",'".$addr1."','".$kecamatan."',
                        '".$kota."','".$kelurahan."','".$passportno."','".$passportnm."','0','".$sex."','".$fathername."','".$prov."','".$idstk."','".$idsponsor."')";*/
            //echo "qry child ".$insChild."<br>";
            $qryChild = $this->executeQuery($insChild, $this->setDB(1));
			
			
            
            $slcChild = "select id from ecomm_umroh_save_child
                    where registerno = '".$regNo."'";
            $queryChild = $this->db->query($slcChild);
            if($queryChild->num_rows() > 0){
                foreach($queryChild->result() as $data)
                {   
                    $idChild = $data->id;
                }
            }else{
                echo "no data";
            }
            
            if($arr['type_ks'] == "1"){
            	$umroh = "UMROH";
            }elseif($arr['type_ks'] == "2"){
            	$umroh = "ZIARAH";
            }
            
            if($tipe == "3") {
            	$insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',3500000,
                        '".$ecomm."','".$regNo."','Payment Manual $umroh')";
				
				$qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));
			} elseif($tipe == "2") {
				$insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',0,
                        '".$ecomm."','".$regNo."','Payment Manual $umroh')";	
                        
			    $qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));				
			} else {
				$insMutasi = "insert into ecomm_umroh_save_mut
                        (id,mutate_flag,tot_fund,createnm,paymentID,description)
                        values('".$idChild."','2',3500000,
                        '".$ecomm."','".$regNo."','Payment Manual $umroh')";
				
				$qryMutasi = $this->executeQuery($insMutasi, $this->setDB(1));	
			}		
            
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            
            if($qryParent > 0 && $qryChild > 0 && $qryMutasi > 0){
                $arr = array('response' => 1,
                             'regno' => $regNo);
                return $arr;
            }else{
                $arr = array('response' => 0);
                return $arr;
            }
        }
	}

	function getDtJamaah($regNo){
        $slc = "select a.paymentID,a.paymentRef,a.id,a.voucherno,a.is_voucher,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp,
                b.birthplace,b.birthdate,b.departureid,b.id,
                c.departuredesc,c.departuredt,d.dfno as idrekruiter,d.fullnm as rekruiternm
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    left outer join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
                where a.paymentID = '".$regNo."' and a.pay_status = '0'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
	
	
    function getDtUmroh(){
        /*$slc = "select a.paymentID,a.paymentRef,a.id,a.voucherno,a.is_voucher,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp,
                b.birthplace,b.birthdate,b.departureid,b.id,c.departuredesc,
                c.departuredt,d.dfno as idrekruiter,d.fullnm as rekruiternm,e.secno,e.CNno,b.novac
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    inner join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
                    inner join ecomm_trans_hdr e on(b.registerno = e.orderno)
                where a.pay_status = '1' and a.is_voucher = '2'";*/
        $slc = "select a.paymentID,a.paymentRef,a.id,a.voucherno,a.is_voucher,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp,
                b.birthplace,b.birthdate,b.departureid,b.id,b.createdt,c.departuredesc,
                c.departuredt,d.dfno as idrekruiter,d.fullnm as rekruiternm,b.novac,
                b.idno
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    left outer join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
        
                where a.pay_status = '1' and a.is_voucher = '2'";
        //echo $slc;
        
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }
    
    function getDtSmsUmroh($regno){
        //update by DION tambah field loccd, 31/8/2015	
        $slc = "select a.paymentID,a.paymentRef,a.id,a.voucherno,a.is_voucher,
                b.dfno,b.fullnm,b.registerno,b.flag_tipe,b.tel_hp,
                b.birthplace,b.birthdate,b.departureid,b.id,c.departuredesc, c.desc2, 
                c.departuredt,d.dfno as idrekruiter,d.fullnm as rekruiternm,e.secno,e.CNno,b.novac,
                b.loccd
                from ecomm_umroh_save_mut a
                	inner join ecomm_umroh_save_child b on (a.id = b.id)
                    inner join ecomm_umroh_departure c on (b.departureid = c.id)
                    left outer join ecomm_umroh_save_parent d on (b.registerno = d.registerno)
                    left outer join ecomm_trans_hdr e on(b.registerno = e.orderno)
                where b.registerno = '".$regno."' and a.pay_status = '1'
                 and a.is_voucher = '2'";
        //echo $slc;
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result; 
    }

	function getListingRegUmroh($from, $to) {
		$slc = "select a.id, a.registerno, a.dfno, a.fullnm, a.tel_hp, 
		            CONVERT(VARCHAR(10),a.createdt,103) as createdt, createnm
				from ecomm_umroh_save_child a
				where a.createdt between '$from' and '$to'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
	}
	
	function getDataUmrohByRegno($regno) {
		$slc = "select a.id, a.registerno, a.dfno, a.fullnm, a.tel_hp, addr1,
		            CONVERT(VARCHAR(10),a.createdt,103) as createdt,
		            a.idno, a.departureid, father_name, loccd, sfno, createnm
				from ecomm_umroh_save_child a
				where a.registerno = '$regno'";
        $result = $this->getRecordset($slc,null,$this->db1);
        return $result;
	}
}	
<?php
class Be_etiket_model extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();

	}
	
	function updateMerchandise($data) {
	
	}
	
	function listTicketTanpaMerchandise() {
		$qry = "SELECT a.notiket, a.order_no, a.id_etiket, 
					c.etiket_code_id, c.nama + ' - ' + c.lokasi + '(' + CONVERT(VARCHAR(10), c.event_date, 103) + ')' as acara,
					a.dfno, z.orderno as trx_id, a.fullnm, y.tel_hp as telp_yg_belanja
				FROM trx_etiket a
				LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_hdr z ON (a.order_no COLLATE SQL_Latin1_General_CP1_CS_AS = z.token)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb y ON (z.id_memb COLLATE SQL_Latin1_General_CP1_CS_AS = y.dfno)
				LEFT OUTER JOIN trx_etiket_product b ON (a.notiket = b.no_tiket)
				LEFT OUTER JOIN master_etiket c ON (a.id_etiket = c.id)
				WHERE a.createdt >= '2018-12-31' and a.[status] = '0'
				and b.no_tiket is null
				GROUP BY a.notiket, a.order_no, a.id_etiket,c.etiket_code_id, c.lokasi, c.nama, c.event_date,
				a.dfno, z.orderno, a.fullnm, y.tel_hp";
		$hasil = $this->getRecordset($qry, null, "db_ecommerce");
		return $hasil;
	}
	
	function listTicketTanpaIP() {
		$qry = "SELECT b.orderno, b.token, a.refno, b.IPno, c.[status], 
		           CONVERT(VARCHAR(20), c.createdt, 103) as createdt, c.dfno, d.fullnm 
				FROM db_ecommerce.dbo.ecomm_trans_hdr b
				LEFT OUTER JOIN db_ecommerce.dbo.etiket_header c ON (b.token =  c.orderno)
				LEFT OUTER JOIN klink_mlm2010.dbo.msmemb d ON (c.dfno COLLATE SQL_Latin1_General_CP1_CI_AS = d.dfno)
				LEFT OUTER JOIN  klink_mlm2010.dbo.bbhdr a 
				ON (a.refno COLLATE SQL_Latin1_General_CP1_CI_AS = b.token)
				where (a.refno is null OR a.refno = '')  and c.createdt >= '2018-12-01' AND b.token LIKE 'TA%'";
		$hasil = $this->getRecordset($qry, null, "db_ecommerce");
		return $hasil;
		//and c.[status] = '1'
	}
	
	function rekapTransaksiTiket($cat_id, $opsi, $id_acara, $tglawal,$tglakhir) {$result = explode('**', $cat_id);
		
		$data['cat_id'] = $result[0];
		$cat_id = $data['cat_id'];
		$data['is_dt_req'] = $result[1];
					
		$tambah = "";
		if($cat_id != "") {
		    $tambah .= " AND b.cat_id = '$cat_id'";
		}
		
		if($opsi != ""){
		    $tambah .= " AND a.id_etiket = '$opsi'";
		}
		
		if($tglawal != "" && $tglakhir != "") {
			$tambah .= " AND a.createdt BETWEEN '$tglawal' and '$tglakhir'";
		}
		
		$qry = "SELECT COUNT(a.order_no) as jumlah_trx, 
				  SUM(a.price) as total_harga, b.etiket_code_id, a.id_etiket,
				  b.nama + ' - ' + b.lokasi +' ( '+ CONVERT(VARCHAR(10),b.event_date, 103) +' ) ' as acara
				FROM trx_etiket a
				INNER JOIN master_etiket b ON (a.id_etiket = b.id)
				WHERE (b.etiket_code_id is not null OR b.etiket_code_id != null)
				and a.[status] = '0' $tambah 
				GROUP BY b.etiket_code_id, a.id_etiket, b.nama, b.lokasi, b.event_date
				";
		$hasil = $this->getRecordset($qry, null, "db_ecommerce");
		return $hasil;		
	}
	
	function rekapTransaksiTiketByIdAcara($id_etiket) {
		$qry = "SELECT a.*, a.createdt as tglbeli, b.cat_id
				FROM trx_etiket a
				LEFT OUTER JOIN master_etiket b ON (a.id_etiket = b.id)
				WHERE a.id_etiket = '$id_etiket' and a.status = '0'";
		//echo $qry;		
		return $this->getRecordset($qry, null, "db_ecommerce");
	}
	
	function listAllLocation() {
		$this->db->select('*')
			->from("master_etiket_kota");
		$q = $this->db->get()->result();
		return $q;
	}
	
	function getHargaProduct($id_etiket, $kode_produk) {
		$qry = "SELECT a.prdcd, a.prdnm, b.dp as harga, c.kode_produk,  c.tipe
				FROM klink_mlm2010.dbo.msprd a
				INNER JOIN klink_mlm2010.dbo.pricetab b ON (a.prdcd = b.prdcd)
				LEFT OUTER join master_etiket_merchandise_product c ON (a.prdcd COLLATE SQL_Latin1_General_CP1_CI_AS = c.kode_produk)
				WHERE b.pricecode = '12W3' and a.prdcd = '$kode_produk'
				and c.etiket_id = '$id_etiket'";
		//echo $qry;	
		$hasil = $this->getRecordset($qry, null, "db_ecommerce");
		
		if($hasil != null) {
			if($hasil[0]->prdcd == $hasil[0]->kode_produk) {
				$res = jsonFalseResponse("Data produk sudah ada di dalam list merchandise ini..");
			} else if($hasil[0]->kode_produk == null) {
				$res = jsonTrueResponse($hasil);
			}
		} else {
			$qry2 = "SELECT top 1 a.prdcd, a.prdnm, b.dp as harga, c.kode_produk,  c.tipe
				FROM klink_mlm2010.dbo.msprd a
				INNER JOIN klink_mlm2010.dbo.pricetab b ON (a.prdcd = b.prdcd)
				LEFT OUTER join master_etiket_merchandise_product c ON (a.prdcd COLLATE SQL_Latin1_General_CP1_CI_AS = c.kode_produk)
				WHERE b.pricecode = '12W3' and a.prdcd = '$kode_produk'";
		    //echo $qry2;
			$hasil2 = $this->getRecordset($qry2, null, "db_ecommerce");
			if($hasil2 != null) {
				$res = jsonTrueResponse($hasil2);
			} else {
				$res = jsonFalseResponse("Kode Produk belum ada..");
			}
		}
		
		return $res;
	}
	
	function getDataAcaraByID($id) {
		$qry = "SELECT a.id, a.nama, a.lokasi, a.price_online, a.price_offline,
				a.price_earlybird_online, a.price_earlybird_offline
				FROM master_etiket a
				WHERE a.id = $id";
		return $this->getRecordset($qry, null, "db_ecommerce");
	}
	
	/*---------------------------
	MERCHANDISE
	----------------------------*/
	
	function getListTipeMerchandise() {
		$qry = "SELECT * FROM master_etiket_merchandise_cat";
		return $this->getRecordset($qry, null, "db_ecommerce");
	}
	
	function getListUkuranMerchandise($tipe = null) {
		$qryTipe = "";
		if($tipe != null) {
			$qryTipe = " WHERE tipe = '$tipe'";
		} 
		$qry = "SELECT * FROM master_etiket_size_merchandise $qryTipe";
		return $this->getRecordset($qry, null, "db_ecommerce");
	}
	
	function getListWarnaMerchandise($tipe = null) {
		$qryTipe = "";
		if($tipe != null) {
			$qryTipe = " WHERE tipe = '$tipe'";
		} 
		$qry = "SELECT * FROM master_etiket_warna_merchandise $qryTipe";
		return $this->getRecordset($qry, null, "db_ecommerce");
	}

	/*------------------
	 * PRODUCT CATEGORY
	 --------------------*/
	 /*function insertToTable($table, $arr) {
		$dbqryx = $this->load->database("db_ecommerce", TRUE);
		$res = $dbqryx->insert($table, $d$arr);
		return $res;
	 }*/
	 
	 function checkDouble($table, $field, $value) {
		$this->db->select('cat_id')
			->from($table)
			->where($field, $value);
			//->order_by('shortname','asc');

		$q = $this->db->get();
		if($q->result() != null) {
			throw new Exception("Double $field", 1);			
		}
	 }
	 
	 function getEtiketCategoryById($id) {
		$this->db->select('*')
			->from('master_etiket_cat')
			->where('cat_id', $id);
			//->order_by('shortname','asc');

		$q = $this->db->get();
		return $q->result();
	 }
	 
	function getListCategoryTicket() {
		$status = array('0','1');
		$this->db->select('*')
			->from('master_etiket_cat')
			//->where('status', '1', '2)')
			->or_where_in('status', $status)
			->order_by('shortname','asc');

		$q = $this->db->get();
		//echo $this->db->last_query($q);
		return $q->result();
	}
	
	function getAcaraOnlineByCategory($cat_id) {
		$result = explode('**', $cat_id);
		$data['cat_id'] = $result[0];
		$data['is_dt_req'] = $result[1];
		//echo $data['is_dt_req'] ;

		if($data['is_dt_req'] == "0"){
			$this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
			->from('db_ecommerce.dbo.master_etiket')
            ->where('cat_id', $data['cat_id'])
			->order_by('nama','ASC')
			->order_by('lokasi','ASC');
		}elseif($data['is_dt_req'] == "1"){
			$this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
				->from('db_ecommerce.dbo.master_etiket')
	            ->where('cat_id', $data['cat_id'])
				->where('createdt >=', '2018-12-31')
				->order_by('nama','ASC')
				->order_by('lokasi','ASC');
		}	
		//->order_by('event_date', 'ASC', 'nama'+'lokasi','ASC');
			//->order_by('createdt','desc'); //ORI
		$q = $this->db->get();
		//echo $this->db->last_query($q);
		return $q->result();
	}
	
	function getAcaraByListOrderno($orderno) {
		$qry = "SELECT b.cat_id, a.order_no, b.lokasi, 
				CONVERT(VARCHAR(17), b.event_date , 113) as event_date2
				FROM trx_etiket a
				LEFT OUTER JOIN master_etiket b ON (a.id_etiket = b.id)
				WHERE a.order_no IN ($orderno)";
		return $this->getRecordset($qry, null, "db_ecommerce");
	}
	
	function getListAllEtiketCategory() {
		$this->db->select('*')
			->from('master_etiket_cat')
			->order_by('shortname','asc');

		$q = $this->db->get();
		return $q->result();
	}
	
	function getEtiketCodeId($pref) {
		$qry = "SELECT * FROM master_etiket_seq WHERE etiket_codename = '$pref'";
		$res = $this->getRecordset($qry, null, "db_ecommerce");
		$codeEtiketNumber = "";
		if($res == null) {
		   $ins = "INSERT INTO master_etiket_seq (etiket_codename, lastno) VALUES ('$pref', '1')";
		   $query = $this->executeQuery($ins, $this->setDB(1));
			 $nextRound = sprintf("%03s", "1");
			 $codeEtiketNumber = $pref.$nextRound;
			 return $codeEtiketNumber;
		   //$this->getEtiketCodeId($pref);
		   //$nextRound = sprintf("%03s", "1");
		   //$codeEtiketNumber = $pref.$nextRound;
		} else {
			if($res[0]->lastno == 999) {
				$codeEtiketNumber = $pref."999";
				$newLastno = 1;
			} else {
				$newLastno = $res[0]->lastno + 1;
				$nextRound = sprintf("%03s", $res[0]->lastno);
				$codeEtiketNumber = $pref.$nextRound;
				
			}
			
			$upd = "UPDATE master_etiket_seq SET lastno = '$newLastno' WHERE etiket_codename = '$pref'";
			$updExe = $this->executeQuery($upd, $this->setDB(1));
			
			return $codeEtiketNumber;
			
		}
		
		
	}
	
	 
	function saveInputEtiket() {
		$data = $this->input->post(NULL, TRUE);
		
		$year = date("y");
		$pref = $data['nm_event'].$year;
		$etiketCodeId = $this->getEtiketCodeId($pref);
		
		$qry = "INSERT INTO master_etiket (
				nama,
				lokasi,
				max_online,
				max_offline,
				event_date,
				pembicara,
				exp_date_online,
				exp_date_offline,
				price_online,
				price_offline,
				status,
				remark,
				createdt,
				createnm, earlybird_date, price_earlybird_online,
				cat_id,
				kode_lokasi,
				etiket_code_id,
				max_kuota
				)
		        VALUES ('$data[nm_event]', '$data[loc_event]', '$data[max_online]',
		              	'$data[max_offline]', '$data[event_date]',
					  	'$data[pembicara]','$data[exp_date_online]','$data[exp_date_offline]',
					  	'$data[price_online]','$data[price_offline]','1',
					  	'$data[remark]',GETDATE(),
		                '".$this->username."', '$data[bird_date]' ,'$data[price_bird]', 
						'$data[event_cat_id]', '$data[loc_id]', '$etiketCodeId', '$data[max_online]')";
		$query = $this->db->query($qry);
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $query;
	}
	
	function saveInputEtiketOLD20180130() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_etiket (
				nama,
				lokasi,
				max_online,
				act_online,
				max_offline,
				act_offline,
				total,
				act_total,
				event_date,
				pembicara,
				exp_date_online,
				exp_date_offline,
				price_online,
				price_offline,
				status,
				remark,
				createdt,
				createnm, earlybird_date, price_earlybird_online

				)
		        VALUES ('$data[nm_event]', '$data[loc_event]', '$data[max_online]',
		              	'$data[act_online]','$data[max_offline]','$data[act_offline]',
					  	'$data[total]','$data[act_total]','$data[event_date]',
					  	'$data[pembicara]','$data[exp_date_online]','$data[exp_date_offline]',
					  	'$data[price_online]','$data[price_offline]','0',
					  	'$data[remark]',GETDATE(),


		               '".$this->username."', '$data[bird_date]' ,'$data[price_bird]')";
		$query = $this->db->query($qry);
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $query;
	}

	function getlistAllEtiket() {
//		$qry = "SELECT
//				 *
//				FROM
//				  master_etiket
//				ORDER BY  createdt DESC
//				";
//		$res = $this->getRecordset($qry, NULL, $this->db);

		$result = array();
		$this->db->select('*, CONVERT(VARCHAR(17), event_date , 113) as event_date2')
			->from('master_etiket')
			->order_by('createdt','desc');
			
		/*$qry = "SELECT 
		        FROM master_etiket a
				";	*/

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;


//		return $res;
	}
	
	function getDataBaru($id) {
		$qry = "SELECT a.nama,a.lokasi, a.max_online, a.act_online, a.max_offline, 
				   a.act_offline, a.total, a.act_total, 
				   CONVERT(VARCHAR(19), a.event_date, 120) as event_date, 
				   a.pembicara, CONVERT(VARCHAR(19), a.exp_date_online, 120) as exp_date_online,
				   CONVERT(VARCHAR(19), a.exp_date_offline, 120) as exp_date_offline,
				   a.price_online, a.price_offline, a.remark, a.id, a.price_earlybird_online,  
				   CONVERT(VARCHAR(19), a.earlybird_date, 120) as earlybird_date,
				   a.nama +'|'+ CAST(a.cat_id AS VARCHAR(11)) as pilevent,
				   a.kode_lokasi +'|'+ CAST(a.lokasi AS VARCHAR(40)) as pilloc,
				   a.cat_id, a.kode_lokasi, a.etiket_code_id, a.max_kuota,
                   COUNT(b.order_no) as jml_terjual
				FROM master_etiket a 
				LEFT OUTER JOIN trx_etiket b ON (a.id = b.id_etiket and b.[status] = '0')
				WHERE a.id = $id 
				GROUP BY
				  a.nama,a.lokasi, a.max_online, a.act_online, a.max_offline, 
				  a.act_offline, a.total, a.act_total, a.event_date,
				  a.pembicara, a.exp_date_online, a.exp_date_offline,
				  a.price_online, a.price_offline, a.remark, a.id, a.price_earlybird_online,
				  a.earlybird_date, a.nama, a.cat_id, a.kode_lokasi, a.lokasi,
				  a.cat_id, a.kode_lokasi, a.etiket_code_id, a.max_kuota";
  
		//echo $qry;
		return $this->getRecordset($qry, null, 'db_ecommerce');
	}

	function getData($id)
	{
		/*$this->db->select('nama,lokasi, max_online, act_online, max_offline, act_offline, total, act_total, CONVERT(VARCHAR(19), event_date, 120) as event_date, pembicara, CONVERT(VARCHAR(19), exp_date_online, 120) as exp_date_online,
		 CONVERT(VARCHAR(19), exp_date_offline, 120) as exp_date_offline,
		price_online,price_offline, remark, id, price_earlybird_online,  CONVERT(VARCHAR(19), earlybird_date, 120) as earlybird_date

		');
		$this->db->where('id',$id);
		$this->db->from('master_etiket');

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
		*/
		$qry = "SELECT a.nama,a.lokasi, a.max_online, a.act_online, a.max_offline, 
				   a.act_offline, a.total, a.act_total, 
				   CONVERT(VARCHAR(19), a.event_date, 120) as event_date, 
				   a.pembicara, CONVERT(VARCHAR(19), a.exp_date_online, 120) as exp_date_online,
				   CONVERT(VARCHAR(19), a.exp_date_offline, 120) as exp_date_offline,
				   a.price_online, a.price_offline, a.remark, a.id, a.price_earlybird_online,  
				   CONVERT(VARCHAR(19), a.earlybird_date, 120) as earlybird_date,
				   a.nama +'|'+ CAST(a.cat_id AS VARCHAR(11)) as pilevent,
				   a.kode_lokasi +'|'+ CAST(a.lokasi AS VARCHAR(11)) as pilloc,
				   a.cat_id, a.kode_lokasi, a.etiket_code_id
				FROM master_etiket a WHERE a.id = $id";
		return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function getDataEtiketByIdV2($id) {
		$qry = "SELECT a.nama,a.lokasi, a.max_online, a.act_online, a.max_offline, 
				   a.act_offline, a.total, a.act_total, 
				   CONVERT(VARCHAR(19), a.event_date, 120) as event_date, 
				   a.pembicara, CONVERT(VARCHAR(19), a.exp_date_online, 120) as exp_date_online,
				   CONVERT(VARCHAR(19), a.exp_date_offline, 120) as exp_date_offline,
				   a.price_online, a.price_offline, a.remark, a.id, a.price_earlybird_online,  
				   CONVERT(VARCHAR(19), a.earlybird_date, 120) as earlybird_date,
				   a.nama +'|'+ CAST(a.cat_id AS VARCHAR(11)) as pilevent,
				   a.kode_lokasi +'|'+ CAST(a.lokasi AS VARCHAR(11)) as pilloc,
				   a.cat_id, a.kode_lokasi, a.etiket_code_id
				FROM master_etiket a WHERE a.id = $id";
		return $this->getRecordset($qry, null, 'db_ecommerce');
	}
	
	function getData2($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('trx_etiket');

		$q = $this->db->get();
//		echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}

	function updateEtiket($id,$save){

		$this->db->trans_start();
		$this->db->where('id',$id);
		$this->db->update('master_etiket',$save);
		$this->db->trans_complete();
//		echo $this->db->last_query();
		return true;
	}


	function klaimEtiket($id,$save){

		$this->db->trans_start();
		$this->db->where('id',$id);
		$this->db->update('trx_etiket',$save);
		$this->db->trans_complete();
//		echo $this->db->last_query();
		return true;
	}





	function getListAllTRXTiket() {
//		$qry = "SELECT
//				 *
//				FROM
//				  master_etiket
//				ORDER BY  createdt DESC
//				";
//		$res = $this->getRecordset($qry, NULL, $this->db);

		$result = array();
		$this->db->select('*, a.status as stats,a.id as idtrx, a.createdt as tanggalbeli, a.id as idtiket')
			->from('trx_etiket a')
			->join('master_etiket b','a.id_etiket = b.id')
			->order_by('a.id','DESC')
			->where('a.status','0')
		;

		$q = $this->db->get();
//		echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;


//		return $res;
	}





	function getListProductCat($param, $value) {
		$qry = "SELECT * FROM master_prd_cat WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("No result", 1);
		}
		return $res;
	}

	function saveInputProductCat() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_prd_cat (cat_id, cat_desc, status, 
		               remarks, createnm)
		        VALUES ('$data[cat_id]', '$data[cat_desc]', '$data[status]', 
		              '$data[remarks]', '".$this->username."')";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Save Product Category failed..!!", 1);
		}
		return $query;
	}

	function getListAllProductCat() {
		$qry = "SELECT * FROM master_prd_cat";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data user group is empty..!", 1);
		}
		return $res;
	}

	function saveUpdateProductCat() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "UPDATE master_prd_cat SET cat_desc = '$data[cat_desc]', 
		               remarks = '$data[remarks]', status = '$data[status]'
		        WHERE cat_id = '$data[cat_id]'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Update Product Category failed..!!", 1);
		}
		return $query;
	}

	function deletetiket($id) {
		$qry = "UPDATE trx_etiket SET status = '1'
		        WHERE id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Product Category failed..!!", 1);
		}
		return $query;
	}

	/*------------------
	 * PRICE CODE
	 --------------------*/
	function getListAllPriceCode() {
		$qry = "SELECT * FROM master_prd_pricecode";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data price code is empty..!", 1);
		}
		return $res;
	}

	function getListPriceCode($param, $value) {
		$qry = "SELECT * FROM master_prd_pricecode WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("No result", 1);
		}
		return $res;
	}

	function savePriceCode($data) {
		//$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_prd_pricecode (pricecode, pricecode_desc, status, 
		               country_id, hq_id, branch_id, remarks, createnm)
		        VALUES ('$data[pricecode]', '$data[pricecode_desc]', '$data[status]',
		                '$data[country_id]', '$data[hq_id]', '$data[branch_id]',
		              '$data[remarks]', '".$this->username."')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Price Code DB Commerce failed..!!", 1);
		}
		return $query;
	}

	function savePriceCodeKlinkMlm($data) {
		$qry = "INSERT INTO pricecode (code, description, status,
		               PT_SVRID, notes, createnm, createdt)
		        VALUES ('$data[pricecode]', '$data[pricecode_desc]', '$data[status]',
		                '$data[country_id]', '$data[remarks]', '".$this->username."', '".$this->dateTime."')";

		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Price Code Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function updatePriceCode($data) {
		//$data = $this->input->post(NULL, TRUE);
		$qry = "update master_prd_pricecode SET pricecode_desc = '$data[pricecode_desc]', status = '$data[status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]', 
		             remarks = '$data[remarks]'
		        WHERE pricecode = '$data[pricecode]'";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Update Price Code DB Commerce failed..!!", 1);
		}
		return $query;
	}

	function updatePriceCodeKlinkMlm($data) {
		$qry = "update pricecode SET description = '$data[pricecode_desc]', status = '$data[status]',
		             PT_SVRID = '$data[country_id]', notes = '$data[remarks]'
		        WHERE code = '$data[pricecode]'";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Update Price Code KlinkMlm failed..!!", 1);
		}
		return $query;
	}

	function deletePriceCode($id) {
		$qry = "DELETE FROM master_prd_pricecode WHERE pricecode = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Price Code failed..!!", 1);
		}
		return $query;
	}

	/*------------------
     PRODUCT
    --------------------*/
	function getlistAllProduct($noexception = true) {
		$qry = "SELECT
				  dbo.master_prd_cat_inv.cat_inv_id,
				  dbo.master_prd_cat_inv.cat_id,
				  dbo.master_prd_cat.cat_desc,
				  dbo.master_prd_cat_inv.cat_inv_desc,
				  dbo.master_prd_cat_inv.is_discontinue
				FROM
				  dbo.master_prd_cat_inv
				INNER JOIN dbo.master_prd_cat ON (dbo.master_prd_cat_inv.cat_id = dbo.master_prd_cat.cat_id)";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Product is empty..!", 1);
			}
		}
		return $res;
	}

	function getListProduct($param, $value) {
		$qry = "SELECT * FROM master_prd_cat_inv WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("gA ADA ISIIII", 1);
		}
		return $res;
	}



	function inputProduct($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		if($parent_cat_inv_id == "") {
			$parent_cat_inv_id = $data['cat_inv_id'];
		}
		$qry = "INSERT INTO master_prd_cat_inv (cat_inv_id, cat_id, parent_cat_inv_id, 
		               cat_inv_desc, inv_type, status, bo_inv_status, bo_status,
					   sc_status, sub_status, ms_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, img_name, weight,
					   is_starterkit, is_discontinue, is_charge_ship, flag_is_do)
		        VALUES ('$data[cat_inv_id]', '$data[cat_id]', '$parent_cat_inv_id',
		                '$data[cat_inv_desc]', '$data[inv_type]', '$data[status]',
		              '$data[bo_inv_status]', '$data[bo_status]', '$data[sc_status]',
		              '$data[sub_status]', '$data[ms_status]', '$data[ecomm_status]',
					  '$data[web_status]', '$data[country_id]', '$data[hq_id]',
					  '$data[branch_id]', '".$this->username."', '".$_FILES["myfile"]["name"]."', ".$data['weight'].",
					  '$data[is_starterkit]', '$data[is_discontinue]', '$data[is_charge_ship]', '$data[flag_is_do]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function inputProductMapforMobile($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		if($parent_cat_inv_id == "") {
			$parent_cat_inv_id = $data['cat_inv_id'];
		}
		$qry = "INSERT INTO master_prd_cat_inv_mobile (cat_inv_id, cat_id, parent_cat_inv_id, 
		               cat_inv_desc, inv_type, status, bo_inv_status, bo_status,
					   sc_status, sub_status, ms_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, img_name, weight,
					   is_starterkit, is_discontinue, is_charge_ship, flag_is_do)
		        VALUES ('$data[cat_inv_id]', '$data[cat_id]', '$parent_cat_inv_id',
		                '$data[cat_inv_desc]', '$data[inv_type]', '$data[status]',
		              '$data[bo_inv_status]', '$data[bo_status]', '$data[sc_status]',
		              '$data[sub_status]', '$data[ms_status]', '$data[ecomm_status]',
					  '$data[web_status]', '$data[country_id]', '$data[hq_id]',
					  '$data[branch_id]', '".$this->username."', '".$_FILES["myfile"]["name"]."', ".$data['weight'].",
					  '$data[is_starterkit]', '$data[is_discontinue]', '$data[is_charge_ship]', '$data[flag_is_do]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function inputProductKlinkMlm($data) {
		$qry = "INSERT INTO msprd (prdcd, prdnm, description, category, status, webstatus, scstatus, PT_SVRID, glposting_cd,
		               createnm, createdt)
	 	        VALUES ('$data[cat_inv_id]', '$data[cat_inv_desc]', '$data[cat_inv_desc]', '$data[cat_id]', '$data[status]',
				   '$data[web_status]', '$data[sc_status]', 'ID', 'GO', '".$this->username."', '".$this->dateTime."')";
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Product DB Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function updateProduct($data) {
		//$data = $this->input->post(NULL, TRUE);
		$ss = "";
		if($_FILES["myfile"]["name"] != "") {
			$ss .= ", img_name = '".$_FILES["myfile"]["name"]."' ";
		}
		$qry = "UPDATE master_prd_cat_inv SET cat_id = '$data[cat_id]', parent_cat_inv_id = '$data[parent_cat_inv_id]',
		             cat_inv_desc = '$data[cat_inv_desc]', inv_type = '$data[inv_type]', status = '$data[status]', bo_inv_status = '$data[bo_inv_status]',
		             bo_status = '$data[bo_status]', sc_status = '$data[sc_status]', sub_status = '$data[sub_status]', ms_status = '$data[ms_status]',
		             ecomm_status = '$data[ecomm_status]', web_status = '$data[web_status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]', weight = ".$data['weight'].",
		             is_starterkit = '$data[is_starterkit]', is_discontinue = '$data[is_discontinue]', is_charge_ship = '$data[is_charge_ship]', flag_is_do = ".$data['flag_is_do']."
		             $ss
		        WHERE cat_inv_id = '$data[cat_inv_id]'";
		//echo $qry;
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Update Product failed..!!", 1);
		}
		return $query;
	}

	function updateProductKlinkMlm($data) {
		$qry = "UPDATE msprd SET prdnm = '$data[cat_inv_desc]', description = '$data[cat_inv_desc]',
	 		             category = '$data[cat_id]', status = '$data[status]', webstatus = '$data[web_status]', 
	 		             scstatus = '$data[sc_status]', PT_SVRID = 'ID', glposting_cd = 'GO',
		               updatenm = '".$this->username."', updatedt = '".$this->dateTime."'
		            WHERE prdcd = '$data[cat_inv_id]'";
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Product DB Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function deleteProduct($id) {
		$qry = "DELETE FROM master_prd_cat_inv WHERE cat_inv_id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Product failed..!!", 1);
		}
		return $query;
	}

	/*---------------------
     * PRODUCT PRICE
     -----------------------*/
	function showProductPriceCode($data) {
		$qry = "SELECT
				  dbo.master_prd_pricetab.pricecode,
				  dbo.master_prd_pricecode.pricecode_desc,
				  dbo.master_prd_pricetab.cat_inv_id,
				  dbo.master_prd_pricetab.cp,
				  dbo.master_prd_pricetab.dp,
				  dbo.master_prd_pricetab.bv,
				  dbo.master_prd_pricetab.tax
				FROM
				  dbo.master_prd_pricetab
				  INNER JOIN dbo.master_prd_pricecode ON (dbo.master_prd_pricetab.pricecode = dbo.master_prd_pricecode.pricecode)
				WHERE
				  (dbo.master_prd_pricetab.cat_inv_id = '$data[cat_inv_id]') AND 
				  (dbo.master_prd_pricetab.country_id = '$data[country_id]') AND 
				  (dbo.master_prd_pricetab.hq_id = '$data[hq_id]') AND 
				  (dbo.master_prd_pricetab.branch_id = '$data[branch_id]')";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		return $res;
	}

	function saveProductPrice($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		for($i=0; $i < $jum; $i++) {
			$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('$data[month]', '$data[year]', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$data[cat_inv_id]', '".$data['pricecode'][$i]."',
				              ".$data['cp'][$i].", ".$data['dp'][$i].", ".$data['bv'][$i].", '".$this->username."')";
			$query = $this->executeQuery($qry, $this->setDB(1));
			if(!$query) {
				$err++;
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB-ECOMMERCE failed..!!", 1);
		}
		return $query;
	}

	function saveproductPriceKlinkMLM($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;
		for($i=0; $i < $jum; $i++) {
			$check = "SELECT pricecode FROM pricetab WHERE pricecode = '".$data['pricecode'][$i]."' AND prdcd = '$data[cat_inv_id]'";
			$check2 = $this->getRecordset($check, NULL, $this->db2);

			if($check2 == null) {
				$qry = "INSERT INTO pricetab (prdcd, pricecode, cp, dp, bv, pv, createnm)
					               
					        VALUES ('$data[cat_inv_id]', '".$data['pricecode'][$i]."',
					              ".$data['cp'][$i].", ".$data['dp'][$i].", ".$data['bv'][$i].", ".$data['bv'][$i].", '".$this->username."')";
				$query = $this->executeQuery($qry, $this->setDB(2));
				if(!$query) {
					$err++;
				}
			} else {
				$redundant++;
				$prd = "Product $data[cat_inv_id] with pricecode ".$data['pricecode'][$i]." already exist in DB KlinkMLM";
				break;

			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB klinkmlm failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;
	}

	function deleteProductPrice($data) {
		//$jum = count($data['pricecode']);
		//for($i=0; $i < $jum; $i++) {
		$qry = "DELETE FROM master_prd_pricetab
		  	          WHERE cat_inv_id = '".$data['cat_inv_id']."' AND country_id = '".$data['country_id']."'
		  	          AND hq_id = '".$data['hq_id']."' AND branch_id = '".$data['branch_id']."'
		  	          AND period_month = ".$data['month']." AND period_year = ".$data['year']."";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		//}
		if(!$query) {
			throw new Exception("Delete Price Code failed..!!", 1);
		}
		return $query;
	}

	function deleteProductPriceKlinkMlm($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		for($i=0; $i < $jum; $i++) {
			$qry = "DELETE FROM pricetab WHERE prdcd = '$data[cat_inv_id]' AND pricecode = '".$data['pricecode'][$i]."'";
			$query = $this->executeQuery($qry, $this->setDB(2));
			if(!$query) {
				$err++;
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function getProductClaim($data) {
		$qry = "SELECT a.invoiceno, a.remark,
				CONVERT(VARCHAR(10), a.invoicedt, 103) AS invoicedt, 
				a.tdp, a.usernya
				FROM KL_TEMPTRANS a 
				where a.remark like '$data[name]%' and
				(a.registerdt>='$data[claim_from]' and a.registerdt <= '$data[claim_to]')
				order by a.invoicedt";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			throw new Exception(setErrorMessage(), 1);
		}
		return $res;
	}

	function getProductClaimDetail($id) {
		$qry = "SELECT b.prdcd, b.prdnm, b.qtyord,
			     b.tdp, b.qtyord * b.tdp as totaldp  
				FROM QKL_TRANS b 
				WHERE b.invoiceno = '$id'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			throw new Exception(setErrorMessage(), 1);
		}
		return $res;
	}

	/*-----------------------
     * PRODUCT BUNDLING
     * ---------------------*/
	function getListAllProductBundling() {
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Dion a
                WHERE a.prdcdcat = '13' order by prdnm";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListProductBundling($param, $id) {

	}

	function getListPrdBundlingHeader($id) {
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Dion a
                WHERE a.prdcd = '$id'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListPrdBundlingDetail($id) {
		$qry = "select * from V_Ecomm_PriceList_Prd_Bundling a WHERE a.prdcd_parent = '$id'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListPrdBundlingDetailKlinkMlm($id) {
		$qry = "select * FROM DION_prdbundling_detail a  WHERE a.prdcd_parent = '$id'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function saveInputProductBundling($data) {

	}

	function saveInputProductBundlingDetail($data) {
		$jum = count($data['productcode']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;

		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}

		$del = "DELETE FROM master_prd_bundling WHERE cat_inv_id_parent = '".$prdcd."'";
		$delQuery = $this->executeQuery($del, $this->setDB(1));

		for($i=0; $i < $jum; $i++) {
			if($data['productcode'][$i] != "" || $data['productcode'][$i] != " ") {
				$qry = "INSERT INTO master_prd_bundling (cat_inv_id_child, cat_inv_id_parent, cat_desc, qty, status, createnm, createdt)
					    VALUES ('".$data['productcode'][$i]."', '".$prdcd."', '".$data['productname'][$i]."', ".$data['qty_real'][$i].",
					              '1', '".$this->username."', '".$this->dateTime."')";
				$query = $this->executeQuery($qry, $this->setDB(1));
				if(!$query) {
					$err++;

				}
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product Bundling failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;


	}

	function saveInputProductBundlingPricetab($data) {
		$err = 0;
		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}
		$check = "SELECT pricecode FROM master_prd_pricetab WHERE cat_inv_id = '".$prdcd."'";
		$check2 = $this->getRecordset($check, NULL, $this->db1);
		if($check2 != null) {
			$delQry = "DELETE FROM master_prd_pricetab WHERE cat_inv_id = '".$prdcd."'";
			$del = $this->executeQuery($delQry, $this->setDB(1));
		}

		$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$prdcd', '12W3',
				              ".$data['cust_west_price_real'].", ".$data['west_price_real'].", 
				              ".$data['bv_real'].", '".$this->username."')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			$err++;
		}

		$qry2 = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$prdcd', '12E3',
				              ".$data['cust_east_price_real'].", ".$data['east_price_real'].", 
				              ".$data['bv_real'].", '".$this->username."')";
		$query2 = $this->executeQuery($qry2, $this->setDB(1));
		if(!$query2) {
			$err++;
		}

		if($err > 0) {
			throw new Exception("Input pricetab product $data[prdid] failed..", 1);
		}
		return $err;



	}

	function saveInputProductBundlingDetailKlink() {


	}

	function saveInputProductBundlingPricetabKlink($data) {

	}

	function inputProductBundlingKlinkMLM($data) {
		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}
		$SP_HILAL_insertGrpPrdHDR = "EXEC SP_HILAL_insertGrpPrdHDR
                '".$prdcd."',
                '".$data['cat_desc']."',
                '".$data['cat_desc']."',
                '".$this->username."',
                '".$this->username."',
                ".$data['west_price_real'].",
                ".$data['cust_west_price_real'].",
                ".$data['bv_real'].",
                '',
                '13'";
		//echo $qry;
		//echo "<br />";
		$query2 = $this->executeQuery($SP_HILAL_insertGrpPrdHDR, $this->setDB(2));


		$i = 0;
		for($i = 0; $i < count($data['productcode']); $i++)
		{
			if($data['productcode'][$i] != "")
			{
				$det = "EXEC SP_HILAL_insertGrpPrdDET
                            '".$this->username."',
                            '".$this->username."',
                            '',
                            '".$data['productcode'][$i]."',
                            '".$data['productname'][$i]."',
                            '".$prdcd."',
                            ".$data['west_real'][$i].",
                            ".$data['qty_real'][$i].",
                            ".$data['west_real'][$i].",
                            ".$data['tbv_real'][$i].",
                            '13'";
				//echo $det;
				//echo "<br />";
				$det2 = $this->executeQuery($det, $this->setDB(2));


			}

		}


		$price_tab = "EXEC SP_HILAL_insertPrdPricetab
                '".$prdcd."',
                '".$data['cat_desc']."',
                '".$data['cat_desc']."',
                '".$this->username."',
                '".$this->username."',
                ".$data['west_price_real'].",
                ".$data['cust_west_price_real'].", 
                ".$data['bv_real'].",
                '12W3',
                '13',
                ".$data['east_price_real'].",
                ".$data['cust_east_price_real'].",
                 
                '12E3'";
		//".$data['bv_real'].",
		//echo $price_tab;
		//echo "<br />";
		$price_tab2 = $this->executeQuery($price_tab, $this->setDB(2));
		//$detprice_tab2 = $this->executeQuery($price_tab2, $this->setDB(2));

	}

	function deleteProductBundlingKlinkMLM($data) {
		$srvReturn = false;
		//DELETE table newera_PRDDET
		/*$delnewera_PRDDET = "DELETE FROM newera_PRDDET WHERE prdcdCat = '".$data['prdid']."'";
     $stt_newera_PRDDET = $this->executeQuery($delnewera_PRDDET, $this->setDB(2));
     if($stt_newera_PRDDET > 0) {
           //DELETE table pricetab
           $delnewera_pricetab = "DELETE FROM pricetab WHERE prdcd = '".$data['prdid']."'";
           $stt_pricetab = $this->executeQuery($delnewera_pricetab, $this->setDB(2));
         if($stt_pricetab > 0) {
               //DELETE msprd
               $delnewera_msprd = "DELETE FROM msprd WHERE prdcd = '".$data['prdid']."'";
               $stt_msprd = $this->executeQuery($delnewera_msprd, $this->setDB(2));
             if($stt_msprd > 0) {
                   //DELETE newera_PRDCAT
                   $delnewera_PRDCAT = "DELETE FROM newera_PRDCAT WHERE prdcd = '".$data['prdid']."'";
                   $stt_newera_PRDCAT = $this->executeQuery($delnewera_PRDCAT, $this->setDB(2));
                 if($stt_newera_PRDCAT > 0) {
                      $srvReturn = true;
                 } else {
                      throw new Exception("(Step 4) : DELETE data newera_PRDCAT failed..", 1);
                 }
             } else {
                  throw new Exception("(Step 3) : DELETE data msprd failed..", 1);
             }
         } else {
              throw new Exception("(Step 2) : DELETE data pricetab failed..", 1);
         }
     } else {
          throw new Exception("(Step 1) : DELETE data newera_PRDDET failed..", 1);
     }*/
		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}

		$delnewera_PRDDET = "DELETE FROM newera_PRDDET WHERE prdcdCat = '".$prdcd."'";
		$stt_newera_PRDDET = $this->executeQuery($delnewera_PRDDET, $this->setDB(2));

		$delnewera_pricetab = "DELETE FROM pricetab WHERE prdcd = '".$prdcd."'";
		$stt_pricetab = $this->executeQuery($delnewera_pricetab, $this->setDB(2));

		$delnewera_msprd = "DELETE FROM msprd WHERE prdcd = '".$prdcd."'";
		$stt_msprd = $this->executeQuery($delnewera_msprd, $this->setDB(2));

		$delnewera_PRDCAT = "DELETE FROM newera_PRDCAT WHERE prdcd = '".$prdcd."'";
		$stt_newera_PRDCAT = $this->executeQuery($delnewera_PRDCAT, $this->setDB(2));
		return true;
	}









	public function reportEtiketNew($tgl_awal,$tgl_akhir,$category,$search, $cat_id, $id_acara) {
	    $data = $this->input->post(NULL, TRUE);
	    //echo "isi cate : $category";
	    $categoryx = "";
		/*if($category=='1'){
		
		}*/
		$result = explode('**', $cat_id);
		$data['cat_id'] = $result[0];
		$cat_id = $data['cat_id'];
		$data['is_dt_req'] = $result[1];
		
		
		if($category == "2"){
			//$this->db->like('a.dfno',$search);
			$categoryx = " AND a.dfno LIKE '$search'";
		}
		if($category == "3"){
			//$this->db->like('a.fullnm', $search);
			$categoryx = " AND a.fullnm LIKE '%$search%'";
		}
		if($category == "4"){
			//$this->db->like('a.valid_dfno', $search);
			$categoryx = " AND a.valid_dfno LIKE '$search'";
		}

		if($category == "5"){
			//$this->db->like('a.valid_fullnm', $search);
			$categoryx = " AND a.valid_fullnm LIKE '%$search%'";
		}

		if($category == "6"){

//			$this->db->like('nama', $search);
			//$this->db->like('lokasi', $search);
			$categoryx = " AND lokasi LIKE '$search'";
		}
		
		$jenis = "";
		if($cat_id != "") {
			//$this->db->where("b.cat_id = $cat_id");
			$jenis = " AND b.cat_id = $cat_id";
		}
		
		$acarax = "";
		if($id_acara != "") {
			//$this->db->where("b.cat_id = $cat_id");
			$acarax = " AND a.id_etiket = '$id_acara'";
		}
		
		$typeTrx = "";
		if($data['typeTrx'] == "1") { //online
			$typeTrx = "";
		}elseif($data['typeTrx'] == "2") { //online
			$typeTrx = " AND a.flag_online = 'W' ";
		}elseif($data['typeTrx'] == "3") { //offline
			$typeTrx = " AND a.flag_online = 'F' ";
		}
		
		$qry = "SELECT *, a.createdt as tglbeli
				FROM trx_etiket a
				LEFT OUTER JOIN master_etiket b ON (a.id_etiket = b.id)
				WHERE a.status = '0'  and a.createdt BETWEEN '".$tgl_awal." 00:00:00.000' AND '".$tgl_akhir." 23:59:59.000' $categoryx $jenis $acarax $typeTrx";
		//echo $qry;		
		return $this->getRecordset($qry, "db_ecommerce");
	}

	public function getReportEtiket($tgl_awal,$tgl_akhir,$category,$search, $cat_id)
	{

		$result = array();
		$this->db->select('*, a.createdt as tglbeli');
		$this->db->from('trx_etiket a');
		$this->db->join('master_etiket b','a.id_etiket=b.id','left');
		$this->db->where('a.status','0');
		
		/*
		if($cat_id != "") {
			$this->db->where("b.cat_id = $cat_id");
		}
		*/
		
		$this->db->where("  a.createdt BETWEEN '".$tgl_awal." 00:00:00.000' AND '".$tgl_akhir." 23:59:59.000'",null,false);
		
		

		if($category=='1'){

		}
		if($category=='2'){
			$this->db->like('a.dfno',$search);
		}
		if($category=='3'){
			$this->db->like('a.fullnm', $search);
		}
		if($category=='4'){
			$this->db->like('a.valid_dfno', $search);
		}

		if($category=='5'){
			$this->db->like('a.valid_fullnm', $search);
		}

		if($category=='6'){

//			$this->db->like('nama', $search);
			$this->db->like('lokasi', $search);
		}

		;


		$q = $this->db->get();
//	 echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;

	}


	public function getReportEtiket2($tgl_awal,$tgl_akhir,$category,$search, $cat_id, $idacara)
	{

		$result = explode('**', $cat_id);
		$data['cat_id'] = $result[0];
		$cat_id = $data['cat_id'];
		$data['is_dt_req'] = $result[1];
		
		$result = array();
		
		$where = "";
		if($cat_id != "") {
			$where = " AND b.cat_id = $cat_id";
		}
		
		$qry = "
				SELECT *, a.tgltrf as tglbeli, c.fullnm as pembeli, d.fullnm as peserta, a.createdt as tanggal, a.dfno as dfno_pembeli, a.email as email_buyer,a.remark as marker, x.prdnm
				FROM trx_etiket a
				LEFT JOIN master_etiket b
									on a.id_etiket=b.id
				LEFT JOIN klink_mlm2010.dbo.msmemb c
									on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				LEFT JOIN klink_mlm2010.dbo.msmemb d
									on a.valid_dfno=d.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				LEFT OUTER JOIN trx_etiket_product x ON (a.notiket = x.no_tiket)
				WHERE a.status = 0
				AND a. createdt BETWEEN '$tgl_awal 00:00:00.000' AND '$tgl_akhir 23:59:59.000'
				and x.prdcd <> 'UM001' and x.prdcd <> 'XL001NS'
				$where
				 ";
				 
		

		if($category=='1'){

		}
		if($category=='2'){
			$qry.="AND a.dfno LIKE '%$search%' ";
		}
		if($category=='3'){
			$qry.="AND a.fullnm LIKE '%$search%' ";

		}
		if($category=='4'){
			$qry.="AND a.valid_dfno LIKE '%$search%' ";

		}

		if($category=='5'){
			$qry.="AND a.valid_fullnm LIKE '%$search%' ";

		}

		if($category=='6'){
			$qry.="AND b.lokasi LIKE '%$search%' ";

//			$this->db->like('nama', $search);
		}
		
		$acarax = "";
		if($idacara != "") {
			//$this->db->where("b.cat_id = $cat_id");
			$qry.="AND a.id_etiket = '$idacara' ";
		}

		$qry.="order by notiket ";


		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}


	public function getReportFinance($tgl_awal,$tgl_akhir,$category,$search,$cat_id, $id_acara)
	{

		$result = explode('**', $cat_id);
		$data['cat_id'] = $result[0];
		$cat_id = $data['cat_id'];
		$data['is_dt_req'] = $result[1];
		
		$result = array();
		$qry = "
				SELECT order_no, a.dfno, a.fullnm, b.datetrans, a.tgltrf,  a.flag_online, a.pembayaran,a.rekening, SUM(price) as nominal, a.remark as marker
				FROM [dbo].[trx_etiket] a
				LEFT JOIN ecomm_trans_hdr_sgo b
				ON a.order_no=b.token
				LEFT JOIN master_etiket c
				ON a.id_etiket=c.id

				WHERE a.status=0
				AND a. createdt BETWEEN '$tgl_awal 00:00:00.000' AND '$tgl_akhir 23:59:59.000'
				 ";

		if($category=='1'){

		}
		if($category=='2'){
			$qry.="AND a.dfno LIKE '%$search%' ";
		}
		if($category=='3'){
			$qry.="AND a.fullnm LIKE '%$search%' ";

		}
		if($category=='4'){
			$qry.="AND a.valid_dfno LIKE '%$search%' ";

		}

		if($category=='5'){
			$qry.="AND a.valid_fullnm LIKE '%$search%' ";

		}

		if($category=='6'){
			$qry.="AND c.lokasi LIKE '%$search%' ";

//			$this->db->like('nama', $search);
		}
		
		//$where = "";
		if($id_acara != "") {
			$qry.="AND a.id_etiket = $id_acara ";
		}

		$qry.="GROUP BY order_no,a.dfno, a.fullnm,  b.datetrans, a.tgltrf,  a.flag_online, a.pembayaran,a.rekening, a.remark
				order by order_no, datetrans ";


		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}

	function validasitiket($data) {
		/*
		 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm ";
		 */
		$qry = "select a.valid_dfno, a.valid_fullnm, b.nama, b.lokasi, a.status, a.id_etiket
				from trx_etiket A
				LEFT JOIN master_etiket B
				ON A.id_etiket = B.id
				where notiket = '".$data."'  ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}
	function validasitiket2($data, $acara) {
		/*
		 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm ";
		 */

		$qry = "select a.valid_dfno, a.valid_fullnm, b.nama, b.lokasi, a.status,a.id_etiket
				from trx_etiket A
				LEFT JOIN master_etiket B
				ON A.id_etiket = B.id
				where valid_dfno = '".$data."' and id_etiket = '".$acara."' and a.status=0 ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}

	function submitAttendee() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO attendee (
				id,
				id_acara,
				no_tiket,
				peserta,
				waktu,
				status,
				createnm

				)
		        VALUES ('".$this->uuid->v4()."', '$data[loc_event]', '$data[no_tiket]',
		              	'$data[peserta]',GETDATE(),'0',



		               '".$this->username."')";
		$query = $this->db->query($qry);
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $query;
	}
	
	function getNamaAcaraById($id) {
		$qry = "SELECT b.id,
				b.nama + ' - ' + b.lokasi +' ( '+ CONVERT(VARCHAR(17),b.event_date, 113) +' ) ' as acara
				FROM master_etiket b
				WHERE b.id = '$id'";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		return $res;
	}
	
	function pindahJadwal($id_acara, $list_orderno) {
		$acaraToUpdate = $this->getNamaAcaraById($id_acara);
		$returnArr = jsonFalseResponse(null, "Jadwal gagal di update..");
		if($acaraToUpdate != null) {
			$kode = $acaraToUpdate[0]->id;
			$nama = $acaraToUpdate[0]->acara;
			
			$updEcommPrd = "UPDATE a
								SET a.prdcd = '$kode', a.prdnm='".trim($nama)."' 
							FROM ecomm_trans_det_prd a 
							INNER JOIN ecomm_trans_hdr b ON (a.orderno = b.orderno)
							WHERE b.token IN ($list_orderno)";
			$updEcommPrdExe = $this->executeQuery($updEcommPrd, $this->setDB(1));	
			//echo $updEcommPrd;
			//echo "<br />";
			if($updEcommPrdExe > 0) {	
				$updTrxEtiket = "UPDATE trx_etiket SET id_etiket = '$id_acara' WHERE order_no IN ($list_orderno)";
				$updTrxEtiketExe = $this->executeQuery($updTrxEtiket , $this->setDB(1));				
				//echo $updTrxEtiket;
				//echo "<br />";
				if($updTrxEtiketExe > 0) {
					$returnArr = jsonTrueResponse(null, "Jadwal berhasil di update..");
				}
			}
		} 
		return $returnArr;
	}


}
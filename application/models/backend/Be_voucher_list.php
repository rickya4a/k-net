<?php

class Be_voucher_list extends MY_Model{

    public function getList($status)
    {

        $result = array();
        $this->db->select('*');
        $this->db->from('fendi_tes_listvoucher');
        $this->db->where('status', $status);

        $q = $this->db->get();
        //echo $this->db->last_query();
        foreach($q->result() as $row)
        {
            $result[] = $row;
        }

        return $result;

    }

//    public function getDetByID($id){
//        $this->db->select('A.vch_no, A.dfno, A.nama, A.email, A.no_telp, A.status,
//                               B.jenisbayar, B.totalbayar, B.tgltrx, B.tglbayar,
//                               C.kodeproduct, C.namaproduct, C.harga, C.qty
//                              ')
//                     ->from('fendi_tes_listvoucher A')
//                     ->join('vch_claimed_test B', 'A.vch_no = B.vch_no', 'left')
//                     ->join('det_vch_claimed_test C', 'A.vch_no = C.vch_no', 'left')
//                     ->where('A.vch_no', $id);
//
//        $res=$this->db->get();
////        return $res;
//
////        echo $this->db->last_query();
//        //echo '$res';
//        foreach($res->result() as $row)
//        {
//            $result[] = $row;
//        }
//
//        return $result;
//    }

    public function getHeader($id){
        $this->db->select('*')
                 ->from('fendi_tes_listvoucher')
                 ->where('vch_no', $id);

        $res=$this->db->get();

//        foreach($res->result() as $row)
//        {
//            $result[] = $row;
//        }

        return $res->result();

    }
    public function getOrderNo($id){
//        $x=null;
//        $query ="select top 1 orderno FROM ecomm_trans_paydet WHERE docno ='$id'";
//
////        $sisa = $qty;
//        $result = $this->db->query($query);
//        foreach($result as $row){
//            $x=$row->orderno;
//        }
//        return $x;
        $this->db->select('*')
            ->from('ecomm_trans_paydet')
            ->where('docno', $id);

        $res=$this->db->get();

//        foreach($res->result() as $row)
//        {
//            $result[] = $row;
//        }

        return $res->result();
    }
    public function getDetail($id){
        $this->db->select('*')
            ->from('ecomm_trans_det_prd')
            ->where('orderno', $id);

        $res=$this->db->get();

//        foreach($res->result() as $row)
//        {
//            $result[] = $row;
//        }

        return $res->result();
    }

    public function getPayment($id){
        $query = '
        select b.bankDesc, c.datetrans, a.payamt
        from ecomm_trans_paydet a
        left join ecomm_bank b
        on a.bank_code_payment = b.id
        left join ecomm_trans_hdr c
        on a.orderno = c.orderno COLLATE SQL_Latin1_General_CP1_CS_AS
        where a.orderno =?
        ';
        $result = $this->db->query($query,array($id));
//        echo $this->db->last_query();
//        foreach($res->result() as $row)
//        {
//            $result[] = $row;
//        }

        return $result->result();

    }

    public function getShipping($id){
        $this->db->select('*')
            ->from('ecomm_trans_shipaddr')
            ->where('orderno', $id);

        $res=$this->db->get();

//        foreach($res->result() as $row)
//        {
//            $result[] = $row;
//        }

        return $res->result();

    }










}
?>
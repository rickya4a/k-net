<?php
class M_follow_up extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}
	
	function getListDataToExcel($data) {
		$str = "";
		
		$dtfrom = date("Y/m/d", strtotime($data['fu_from']));
		$dtto = date("Y/m/d", strtotime($data['fu_to']));
		
		
		$d = 0;
		if ($data['formno'] != "") {
			$str .= " a.formno = '$data[formno]' ";
			$d++;
		} else if ($data['cust_id'] != "") {
			$str .= " a.cust_id = '$data[cust_id]' ";
			$d++;
		} else {
			 if ($data['followup_type'] != "") {
				$str .= " a.followup_type = '$data[followup_type]' ";
				$d++;
				
				if ($data['status_followup'] != "") {
					$str .= " AND a.status_followup = '$data[status_followup]' ";
					$d++;
					
					
				} 
				
				if($dtfrom != "" && $dtfrom != "1970/01/01") {
						if($d > 0) {
							$str .= " AND";
						}
						$str .= " CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto' "; 
					}
			} else {
				if ($data['status_followup'] != "") {
					$str .= "a.status_followup = '$data[status_followup]' ";
					$d++;
					
					
				} 
				
				if($dtfrom != "" && $dtfrom != "1970/01/01") {
						if($d > 0) {
							$str .= " AND";
						}
						$str .= " CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto' "; 
					}
			}
		}
		
		$resignQry = "SELECT a.dfno, a.fullnm, a.jointdt, a.trf_to_id, a.trf_to_nm, a.followup_type,	 
		                   CONVERT(VARCHAR(11), b.los_createdt, 113) as los_createdt, b.los_no, a.memberstatus, a.dfno_rank,
					  	 CASE a.memberstatus 
					  	   WHEN '4' THEN 'TERMINATION'
					  	   WHEN '2' THEN 'RESIGNATION'
					  	 ELSE 
					  	  CASE a.followup_type 
					  	    WHEN '2' THEN 'TRANSFER NAME' 
					  	    WHEN '3' THEN 'TRANSFER LINE'
					  	    WHEN '4' THEN 'EXCHANGE ACCOUNT'
					  	    WHEN '5' THEN 'EXCHANGE STATEMENT BONUS'
					  	    WHEN '8' THEN 'UPDATE RECRUITER'
					  	    WHEN '9' THEN 'NAME CORRECTION'
					  	  ELSE  'REACTIVE ID'
					  	  END
						 END as folup_type ,
						 b.formno, a.keterangan 
					  FROM DION_followup_trf_acc a
					  INNER JOIN DION_followup b ON (a.followup_id = b.id)
					  
					  WHERE $str 
					  ORDER BY b.los_no, a.followup_type, a.memberstatus, a.dfno ASC";
//			echo $resignQry;
			$resignReactive = $this->getRecordset($resignQry, NULL, $this->db2);
			return $resignReactive;
	}
	
	function getListCommMethod() {
		$qry = "SELECT id as selectID, comm_method as selectValue FROM DION_followup_comm_method";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function getPrimaryKey($table) {
		$qry = "SELECT KU.table_name as tablename,column_name as primarykeycolumn
				FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
				INNER JOIN
				INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
				ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
				TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
				and ku.table_name='$table'
				ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getDetFollowupNonBV($id) {
		$qry = "SELECT * FROM DION_followup_trf_acc WHERE id = $id";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getDetailResign($id, $idmember) {
		
		$qry = "SELECT id, followup_id,jointdt, dfno, fullnm, idno, address, stockist as loccd, memberstatus, 
		        updateby FROM DION_followup_trf_acc WHERE id = $id";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res[0]->updateby != null || $res[0]->updateby != "") {
			return $res;
		} else {
			return null;
		}
		
	}
	
	function getDetailTrfName($id, $idmember) {
		
		$qry = "SELECT id, followup_id,jointdt, 
		              dfno, fullnm, idno, address, stockist as loccd, memberstatus, 
		              trf_to_id, trf_to_nm, dob, sex, bank_code,acc_no, stockist, keterangan,
		        updateby FROM DION_followup_trf_acc WHERE id = $id";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res[0]->updateby != null || $res[0]->updateby != "") {
			return $res;
		} else {
			return null;
		}
		
	}

	function getMemberInfo($param, $value) {
		
	    $qry = "SELECT top 1 a.dfno, a.idno, a.fullnm, a.tel_hp, a.tel_hm, a.email, 
				       a.loccd, a.addr1,a.addr2,a.addr3, a.memberstatus, a.[status],
				    CONVERT(VARCHAR(10), a.jointdt, 111) as jointdt,
				    CONVERT(VARCHAR(10), a.birthdt, 111) as birthdt,
				    a.sponsorid, b.fullnm as sponsorname,
				    d.shortnm
				FROM msmemb a 
				   LEFT OUTER JOIN msmemb b ON (a.sponsorid = b.dfno)
				   LEFT OUTER JOIN tbonus c ON (a.dfno = c.distributorcode)
				   LEFT OUTER JOIN msrank d ON (CAST(c.currentrank AS int) = d.[level])
				WHERE a.$param = '$value'
				order by c.bonusyear DESC, c.bonusmonth DESC, CAST(c.currentrank AS int) desc";				
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getMemberInfoRecruiter($param, $value) {
		
	    $qry = "SELECT top 1 a.dfno, a.idno, a.fullnm, a.tel_hp, a.tel_hm, a.email, 
				       a.loccd, a.addr1,a.addr2,a.addr3, a.memberstatus, a.[status],
				    CONVERT(VARCHAR(10), a.jointdt, 111) as jointdt,
				    CONVERT(VARCHAR(10), a.birthdt, 111) as birthdt,
				    a.sfno_reg as sponsorid, b.fullnm as sponsorname,
				    d.shortnm
				FROM msmemb a 
				   LEFT OUTER JOIN msmemb b ON (a.sfno_reg = b.dfno)
				   LEFT OUTER JOIN tbonus c ON (a.dfno = c.distributorcode)
				   LEFT OUTER JOIN msrank d ON (CAST(c.currentrank AS int) = d.[level])
				WHERE a.$param = '$value'
				order by c.bonusyear DESC, c.bonusmonth DESC, CAST(c.currentrank AS int) desc";				
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function getOrderNo($prefix, $table) {
		$year = substr(date("Y"), 2, 2);
		$month = date("m");
		$primary = $this->getPrimaryKey($table);
		$key = $primary[0]->primarykeycolumn;
		$full_prefix = $prefix . $year . $month;
		$qry = "SELECT MAX(CAST(RIGHT(id, 3) as INT)) AS id FROM $table WHERE $key LIKE '$full_prefix%'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if ($res == null) {
			$next = "0001";
		} else {
			$next = $res[0]->id + 1;
			$nextTo = sprintf("%'.04d", $next);
		}
		return $prefix . $year . $month . $nextTo;

	}

	function saveFollowUpType($data) {
		$rtn = false;
		$ins = "INSERT INTO DION_followup_type (follow_type) VALUES ('$data[followup_type]')";
		$qry = $this->executeQuery($ins, $this->db2);
		if ($qry > 0) {
			$rtn = true;
		}
		return $rtn;
	}

	function getDataFollowTypeForSelect() {
		$qry = "SELECT id as selectID, follow_type as selectValue FROM DION_followup_type ORDER BY order_number";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getDataFollowTypeForlos() {
		$qry = "SELECT id as selectID, follow_type as selectValue 
                FROM DION_followup_type
                WHERE  id NOT IN (5, 7)
                ORDER BY order_number";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getDataFollowTypeByID($tipe) {
		$qry = "SELECT * FROM DION_followup_type WHERE id = $tipe";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function getDataCustTypeForSelect() {
		$qry = "SELECT id as selectID, cust_type as selectValue FROM DION_followup_cust_type";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function getListStatusForSelect() {
		$qry = "SELECT id as selectID, status as selectValue, warna, warna2 FROM DION_followup_status_list";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function checkIfFollowUpType($followupid, $value) {
		$qry = "SELECT followup_type FROM DION_followup WHERE id = '$followupid' AND followup_type = '$value'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function updateStatusFollowUp($data) {
		$rtn = false;
		$alasan = strtoupper($data['alasan']);
		$ins = "UPDATE DION_followup SET status_followup = $data[status_followup], feedback = '$alasan' 
				WHERE id = '$data[followup_id]'";
		$qry = $this->executeQuery($ins, $this->db2);

		$log = $this->insertLogFollowup($data['followup_id'], $data['status_followup'], $alasan, $this->username);

		if ($qry > 0 && $log > 0) {
			if($data['status_followup'] == "3") {
				$followupx = $this->checkIfFollowUpType($data['followup_id'], "5");
				if($followupx != null) {
					$upd = "UPDATE DION_followup_trf_acc SET status_followup = '1' WHERE followup_id = '$data[followup_id]'";
					$updQry = $this->executeQuery($upd, $this->db2);
				}
			}
			$rtn = true;
		}
		return $rtn;
	}

	function getListFollowup($data) {
		$str = "";
		$d = 0;
        if ($data['formno'] != "") {
			$str .= " AND a.formno = '$data[formno]' ";
			$d++;
		} else if ($data['cust_id'] != "") {
			$str .= " AND a.cust_id = '$data[cust_id]' ";
			$d++;
		}  else {
        
			if ($data['followup_type'] != "") {
				$str .= " AND a.followup_type = '$data[followup_type]'";
			}
	
			if ($data['status_followup'] != "") {
				$str .= " AND a.status_followup = $data[status_followup]";
			}

		
		}
		$dtfrom = date("Y/m/d", strtotime($data['fu_from']));
		$dtto = date("Y/m/d", strtotime($data['fu_to']));

		$qry = "SELECT a.id, a.formno, a.cust_id, a.cust_name, a.followup_type, a.followup_detail,
						a.tel_no,a.tel_hp, b.follow_type, CONVERT(VARCHAR(10), a.createdt, 111) as createdt,
						a.status_followup, c.status, c.warna as color, a.feedback, d.acc_by, d.acc_by2
				FROM DION_followup a
				INNER JOIN DION_followup_type b ON (a.followup_type = b.id)
				LEFT OUTER JOIN DION_followup_sign_acc d ON (a.signature_status = d.id)
				INNER JOIN DION_followup_status_list c ON (a.status_followup = c.id)
				WHERE a.createby != 'ADIS' $str ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getListFollowupAdis($data) {
		$str = "";
		
		$dtfrom = date("Y/m/d", strtotime($data['fu_from']));
		$dtto = date("Y/m/d", strtotime($data['fu_to']));
		
		
		$d = 0;
		if ($data['formno'] != "") {
			$str .= " a.formno = '$data[formno]' ";
			$d++;
		} else if ($data['cust_id'] != "") {
			$str .= " a.cust_id = '$data[cust_id]' ";
			$d++;
		} else {
			 if ($data['followup_type'] != "") {
				$str .= " a.followup_type = '$data[followup_type]'";
				$d++;
				
				if ($data['status_followup'] != "") {
					$str .= " AND a.status_followup = '$data[status_followup]'";
					$d++;
					
					
				} 
				
				if($dtfrom != "" && $dtfrom != "1970/01/01") {
						if($d > 0) {
							$str .= " AND";
						}
						$str .= " CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto'"; 
					}
			} else {
				if ($data['status_followup'] != "") {
					$str .= "a.status_followup = '$data[status_followup]'";
					$d++;
					
					
				} 
				
				if($dtfrom != "" && $dtfrom != "1970/01/01") {
						if($d > 0) {
							$str .= " AND";
						}
						$str .= " CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto'"; 
					}
			}
		}
		
		

		/*if ($data['status_followup'] != "") {
			$str .= " a.status_followup = $data[status_followup]";
			$d++;
		}*/

		/*if ($data['cust_id'] != "") {
			$str .= " a.cust_id = '$data[cust_id]' ";
			$d++;
		}*/
		
		/*if ($data['formno'] != "") {
			$str .= " a.formno = '$data[formno]' ";
			$d++;
		}
		
		if($dtfrom != "" && $dtfrom != "1970/01/01") {
			if($d > 0) {
				$str .= "AND";
			}
			$str .= " CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto'"; 
		}*/
		
		$qry = "SELECT a.id, a.formno, a.cust_id, a.cust_name, a.followup_type, a.followup_detail as followup_detail, a.followup_detail2 as followup_detail2,
						a.tel_no,a.tel_hp, b.follow_type, CONVERT(VARCHAR(10), a.createdt, 111) as createdt,
						a.status_followup, c.status, c.warna2 as color, a.feedback, d.acc_by, d.acc_by2
				FROM DION_followup a
				LEFT OUTER JOIN DION_followup_type b ON (a.followup_type = b.id)
				LEFT OUTER JOIN DION_followup_status_list c ON (a.status_followup = c.id)
				LEFT OUTER JOIN DION_followup_sign_acc d ON (a.signature_status = d.id)
				WHERE $str";
	    //echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	
	function getListUnprocesedFollowup($data) {
		$str = "";

		if ($data['followup_type'] != "") {
			$str .= " AND a.followup_type = '$data[followup_type]'";
		}

		//if ($data['status_followup'] != "") {
			$str .= " AND a.status_followup = 1	";
		//}

		if ($data['cust_id'] != "") {
			$str .= " AND a.cust_id = '$data[cust_id]'";
		}
		$dtfrom = date("Y/m/d", strtotime($data['fu_from']));
		$dtto = date("Y/m/d", strtotime($data['fu_to']));

		$qry = "SELECT a.id, a.formno, a.cust_id, a.cust_name, a.followup_type, a.followup_detail,
						a.tel_no,a.tel_hp, b.follow_type, CONVERT(VARCHAR(10), a.createdt, 111) as createdt,
						a.status_followup, c.status, c.warna as color, a.feedback, d.acc_by, d.acc_by2
				FROM DION_followup a
				INNER JOIN DION_followup_type b ON (a.followup_type = b.id)
				LEFT OUTER JOIN DION_followup_sign_acc d ON (a.signature_status = d.id)
				INNER JOIN DION_followup_status_list c ON (a.status_followup = c.id)
				WHERE CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto' AND a.createby != 'ADIS' $str ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	
	function getDetailFollowUpforUpdate($id) {
		$qry = "SELECT a.id, a.formno, a.cust_id, a.cust_name, 
		               a.followup_type, a.followup_detail, a.email, a.comm_method, a.attached_app,
						a.tel_no,a.tel_hp, a.status_followup, a.feedback, b.follow_type as nama_followup
				FROM DION_followup a
				INNER JOIN DION_followup_type b ON (a.followup_type = b.id)
				WHERE a.id = '$id'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getDataDetailFollowUpForUpdate($id) {
		$qry = "SELECT a.id, a.followup_id, a.dfno, a.fullnm, a.dfno_rank, a.idno
				FROM DION_followup_trf_acc a
				WHERE a.followup_id = '$id'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function getDataDetailFollowUpForUpdateBV($id) {
		$qry = "SELECT *
				FROM DION_followup_trf_bv a
				WHERE a.followup_id = '$id'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function getListFollowUpType() {
		$qry = "SELECT * FROM DION_followup_type";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function updateFollowUpType($data) {
		$rtn = false;
		$ins = "UPDATE DION_followup_type SET follow_type = '$data[followup_type]' WHERE id = $data[id]";
		$qry = $this->executeQuery($ins, $this->db2);
		if ($qry > 0) {
			$rtn = true;
		}
		return $rtn;
	}

	function getDetailFollowUpByID($id) {
		$qry = "SELECT a.id, a.formno, a.cust_id, a.cust_name, a.tel_no, a.tel_hp, a.createby,
					   a.email, f.comm_method, d.lampiran, a.sent_to, a.followup_type, 
					   a.followup_detail, b.follow_type, CONVERT(VARCHAR(20), a.createdt, 100) as createdt,
					   a.status_followup, c.status, c.warna as color , 
					   a.los_no, CONVERT(VARCHAR(20), a.los_createdt, 100) as los_createdt,
					   a.batch_no, CONVERT(VARCHAR(20), a.batch_createdt, 100) as batch_createdt,
					   e.acc_by, e.acc_by2, a.feedback
				FROM DION_followup a
				INNER JOIN DION_followup_type b ON (a.followup_type = b.id)
				INNER JOIN DION_followup_status_list c ON (a.status_followup = c.id)
				INNER JOIN DION_followup_lampiran d ON (a.attached_app = d.id)
				INNER JOIN DION_followup_sign_acc e ON (a.signature_status = e.id)
				INNER JOIN DION_followup_comm_method f ON (a.comm_method = f.id)
				WHERE a.id = '$id'";
		//echo $qry;
		$followup = $this->getRecordset($qry, NULL, $this->db2);

		$qryLog = "SELECT c.status, a.createby, CONVERT(VARCHAR(20), a.createdt, 13) as createdt, a.detail, a.description
		           FROM DION_followup_log a
		           INNER JOIN DION_followup_status_list c ON (a.status = c.id)
		           WHERE a.followup_id = '$id'
		           ORDER BY a.createdt";
		$log = $this->getRecordset($qryLog, NULL, $this->db2);

		$bv = null;
		$resignReactive = null;
		if ($followup[0]->followup_type == "7") {
			$bvQry = "SELECT a.id, a.followup_id, a.no_ttp, a.trcd, a.ndp, a.nbv,
						  a.dfno, a.fullnm, a.updateTo_dfno, a.updateTo_nm, a.status,
												  CONVERT(VARCHAR(20), a.updatedt, 100) as updatedt,
												  COUNT(b.[url]) as jum_file 
						FROM DION_followup_trf_bv a 
						LEFT OUTER JOIN DION_followup_los_image b ON (a.id = b.id)
						WHERE followup_id = '$id'
						GROUP BY a.id, a.followup_id, a.no_ttp, a.trcd, a.ndp, a.nbv,
						  a.dfno, a.fullnm, a.updateTo_dfno, a.updateTo_nm, a.status,
						  CONVERT(VARCHAR(20), a.updatedt, 100)";
			$bv = $this->getRecordset($bvQry, NULL, $this->db2);
		} 
		//else if ($followup[0]->followup_type == "1" || $followup[0]->followup_type == "6") {
		else {	
			$resignQry = "SELECT a.id, a.followup_id, a.dfno, a.followup_type, a.status_followup as status,
							  a.fullnm, a.trf_to_id, a.trf_to_nm,  a.trf_to_id_rank, a.memberstatus, 
							  CONVERT(VARCHAR(20), a.createdt, 100) as createdt, a.createby,
							  CONVERT(VARCHAR(20), updatedt, 100) as updatedt, a.keterangan, a.updateby,
							  COUNT(b.[url]) as jum_file   
							FROM DION_followup_trf_acc a
							LEFT OUTER JOIN DION_followup_los_image b ON (a.id = b.id)
							WHERE followup_id = '$id'
							GROUP BY a.id, a.followup_id, a.dfno, a.followup_type, a.status_followup,
							  a.fullnm, a.trf_to_id, a.trf_to_nm,  a.trf_to_id_rank, a.memberstatus, 
							  CONVERT(VARCHAR(20), a.createdt, 100), a.createby, CONVERT(VARCHAR(20), updatedt, 100),
							  a.keterangan, a.updateby";
			$resignReactive = $this->getRecordset($resignQry, NULL, $this->db2);
		}

		$arr = array("followup" => $followup, "log" => $log, "bv" => $bv, "los" => $resignReactive);
		return $arr;
	}

	function getListAllAttachment() {
		$qry = "SELECT * FROM DION_followup_lampiran ORDER BY orderno ASC";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}

	function checkSalesByOrderNo($id) {
		$arr = jsonFalseResponse("No TTP tidak ada di database");
		/*$prefix = substr($id, 0, 3);
		if($prefix == "IDM" || $prefix == "IDS") {
			$qry = "SELECT orderno, trcd, dfno, ndp, nbv 
					FROM newtrh a WHERE a.orderno = '$id'
					UNION
					SELECT orderno, trcd, dfno, ndp, nbv 
					FROM sc_newtrh a WHERE a.orderno = '$id'";
		} else {
			$qry = "SELECT orderno, trcd, dfno, ndp, nbv 
					FROM newtrh a WHERE a.trcd = '$id'
					UNION
					SELECT orderno, trcd, dfno, ndp, nbv 
					FROM sc_newtrh a WHERE a.trcd = '$id'";
		}
		*/
		$qry = "SELECT orderno, trcd, dfno, ndp, nbv 
					FROM newtrh a WHERE a.orderno = '$id'
					UNION
					SELECT orderno, trcd, dfno, ndp, nbv 
					FROM sc_newtrh a WHERE a.orderno = '$id'";
		
		$res = $this->getRecordset($qry, NULL, $this->db2); 
		if($res == null) {
			$qry = "SELECT orderno, trcd, dfno, ndp, nbv 
					FROM newtrh a WHERE a.trcd = '$id'
					UNION
					SELECT orderno, trcd, dfno, ndp, nbv 
					FROM sc_newtrh a WHERE a.trcd = '$id'";
			$res = $this->getRecordset($qry, NULL, $this->db2);
			if ($res != null) {
				$arr = jsonTrueResponse($res);
			}
		} else {
			$arr = jsonTrueResponse($res);
		}
		
		return $arr;
	}

	function insertLogFollowup($followupid, $status, $detail, $createby) {
		$ins_log = "INSERT INTO DION_followup_log (followup_id, status, detail, createby) 
			            VALUES ('$followupid', $status, '$detail', '$createby')";
		$qry = $this->executeQuery($ins_log, $this->db2);
		
		$upd = "UPDATE DION_followup SET status_followup = $status 
				WHERE id = '$followupid'";
		$qry2 = $this->executeQuery($upd, $this->db2);
		
		return $qry;
	}

	function getDetailTrfBV($followup_id, $trcd) {
		$qry = "SELECT a.followup_id, a.no_ttp, a.trcd, a.updateTo_dfno, b.fullnm 
				FROM DION_followup_trf_bv a
				INNER JOIN msmemb b ON (a.updateTo_dfno = b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS) 
				WHERE a.followup_id = '$followup_id' AND a.trcd = '$trcd'";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			$qry = "SELECT a.followup_id, a.no_ttp, a.trcd, a.updateTo_dfno, b.fullnm 
				FROM DION_followup_trf_bv a
				INNER JOIN msmemb b ON (a.updateTo_dfno = b.dfno COLLATE SQL_Latin1_General_CP1_CS_AS) 
				WHERE a.followup_id = '$followup_id' AND a.no_ttp = '$trcd'";
		     $res = $this->getRecordset($qry, NULL, $this->db2);
		}
		return $res;
	}

	function insertTransferBV($followup_id, $data) {
		$jum = count($data['ttp']);
		$success = 0;
		for ($i = 0; $i < $jum; $i++) {
			$qry = "INSERT INTO DION_followup_trf_bv (followup_id, no_ttp, ndp, nbv, dfno, updateTo_dfno) 
		        	VALUES ('$followup_id', '" . $data['ttp'][$i] . "', " . $data['ndp'][$i] . ", 
		        	   " . $data['nbv'][$i] . ", '" . $data['dfno'][$i] . "', '" . $data['updateTo_dfno'] . "')";
			$query = $this->executeQuery($qry, $this->db2);
			if ($query > 0) {
				$success++;
			}

		}

		if ($success == $jum) {
			return 1;
		} else {
			return 0;
		}
	}
	
	function deleteFollowUp($id) {
		$del = "DELETE FROM DION_followup WHERE id = '$id'";
		$delquery = $this->executeQuery($del, $this->db2);
		
		$del2 = "DELETE FROM DION_followup_trf_bv WHERE followup_id = '$id'";
		$delquery2 = $this->executeQuery($del2, $this->db2);	
		
		$del3 = "DELETE FROM DION_followup_trf_acc WHERE followup_id = '$id'";
		$delquery3 = $this->executeQuery($del3, $this->db2);	
		
		$arr = array("response" => "false", "message" => "Delete Follow Up gagal..");
		if ($delquery > 0) {
			$arr = array("response" => "true", "message" => "Delete Follow Up $id berhasil");
		}
		return $arr;	
	}
	
	function saveEditFollowup($data) {
		
		
		$log = $this->insertLogFollowup($data['followup_id'], 1, "Follow Up Updated", $this->username);
		
		$txt = "";
		$success = 0;
		if ($data['followup_type'] == "7") {
			$del = "DELETE FROM DION_followup_trf_bv WHERE followup_id = '$data[followup_id]'";
		    $delquery = $this->executeQuery($del, $this->db2);	
			
			$shortname = substrwords($data['updateToNm'], 15);
			
			$txt = "<table width=\"98%\" class=\"table table-bordered\"><tr><td colspan=3 align=center><b>TRF BV ke $data[updateTo_dfno]/$shortname</b></td></tr>";
			$txt .= "<tr><td align=center><b>Trx/TTP No</b></td></td><td align=center><b>Tot BV</b></td><td align=center><b>ID Member</b></td></tr>";

			$jum = count($data['ttp']);
			
			for ($i = 0; $i < $jum; $i++) {
				$qry = "INSERT INTO DION_followup_trf_bv (followup_id, no_ttp, ndp, nbv, dfno, updateTo_dfno, trcd) 
				        	VALUES ('$data[followup_id]', '" . $data['ttp'][$i] . "', " . $data['ndp'][$i] . ", 
				        	   " . $data['nbv'][$i] . ", '" . $data['dfno'][$i] . "', '" . $data['updateTo_dfno'] . "', '" . $data['trcd'][$i] . "')";
				$query = $this->executeQuery($qry, $this->db2);
				if ($query > 0) {
					$success++;
				}
				//$url = "'followup/update/bv/$dta->id/$no/$dta->trcd'";
				$url = "'followup/update/bv/".$data['followup_id']."/$no/" . $data['ttp'][$i] . "";
				$txt .= "<tr><td><a href='#' onclick=\"javascript:All.ajaxShowDetailonNextForm2($url)\">" . $data['ttp'][$i] . "</a></td><td align=right>" . $data['nbv'][$i] . "</td><td>" . $data['dfno'][$i] . "</td></tr>";
			}
		} else {
			if($data['dfno'] != "") {
				
				$del = "DELETE FROM DION_followup_trf_acc WHERE followup_id = '$data[followup_id]'";
		         $delquery = $this->executeQuery($del, $this->db2);	
				
			    $status_followup = $data['followup_type'] == "6" ? "1" : "0";
				$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, dfno_rank, status_followup, createby)
							 VALUES ('$data[followup_id]', '$data[followup_type]', '$data[dfno]', '$data[fullnm]', '$data[dfno_rank]', '$status_followup', '".$this->username."')";
				$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
			}
		}
		
		$upd = "UPDATE DION_followup SET 
		            formno = '$data[formno]', cust_type = '$data[cust_type]', 
		            cust_id = '$data[cust_id]', cust_name = '$data[cust_name]',
		            tel_no = '$data[tel_no]', tel_hp =  '$data[tel_hp]', 
		            email = '$data[email]', comm_method ='$data[comm_method]', 
		            attached_app = '$data[attached_app]', sent_to = '$data[sent_to]', followup_type = '$data[followup_type]', 
		            followup_detail = '$data[followup_detail]', 
					createby = '" . $this->username . "', status_followup = 1, followup_detail2 = '" . $txt . "'
				WHERE id = '$data[followup_id]'";
		$query = $this->executeQuery($upd, $this->db2);
				if ($query > 0) {
					$success++;
				}
				
	    $arr = array("response" => "false", "message" => "Input Follow Up gagal..");
		if ($success > 0) {
			$arr = array("response" => "true", "message" => "Update Follow Up No $data[followup_id] berhasil");
		}
		return $arr;			
	    //return $success;
		
							 
	}

	function saveFollowupInput($data) {
		//$qry = "INSERT INTO DION_followup ()";
		$bv = null;
		$exe_ins_followup = null;
		$no = $this->getOrderNo("FU", "DION_followup");

		//if($exe_ins_followup > 0) {
		$log = $this->insertLogFollowup($no, 1, "Follow Up diinput", $this->username);
		//Masukan data tambahan bila jenis followup adalah pelimpahan BV
		if ($data['followup_type'] == "7") {
			$shortname = substrwords($data['updateToNm'], 15);
			$txt = "";
			$txt = "<table width=\"98%\" class=\"table table-bordered\"><tr><td colspan=3 align=center><b>TRF BV ke $data[updateTo_dfno]/$shortname</b></td></tr>";
			$txt .= "<tr><td align=center><b>Trx/TTP No</b></td></td><td align=center><b>Tot BV</b></td><td align=center><b>ID Member</b></td></tr>";

			$jum = count($data['ttp']);
			$success = 0;
			$heccs = $this->load->database($this->db2, true);
			for ($i = 0; $i < $jum; $i++) {
				$qry = "INSERT INTO DION_followup_trf_bv (followup_id, no_ttp, ndp, nbv, dfno, fullnm, updateTo_dfno, updateTo_nm, trcd) 
				        	VALUES ('$no', '" . $data['ttp'][$i] . "', " . $data['ndp'][$i] . ", 
				        	   " . $data['nbv'][$i] . ", '" . $data['dfno'][$i] . "', '" . $data['cust_name'] . "', '" . $data['updateTo_dfno'] . "',
				        	   '" . $data['updateToNm'] . "',   '" . $data['trcd'][$i] . "')";
				//$query = $this->executeQuery($qry, $this->db2);
				
	            $query = $heccs->query($qry);
				//$idxc = $heccs->insert_id();
				if ($query > 0) {
					
					$insid = "SELECT id FROM DION_followup_trf_bv WHERE followup_id = '$no'";
					$insid2 = $heccs->query($insid);
					$res = $insid2->result();
					$idxc = $res[0]->id;
					$success++;
				}
				//$url = "'followup/update/bv/$dta->id/$no/$dta->trcd'";
				$url = "''followup/update/bv/$idxc/$no/" . $data['ttp'][$i] . "''";
				$txt .= "<tr><td><a href=\"#\" onclick=\"javascript:All.ajaxShowDetailonNextForm2($url)\">" . $data['ttp'][$i] . "</a></td><td align=right>" . $data['nbv'][$i] . "</td><td>" . $data['dfno'][$i] . "</td></tr>";
			}
			$txt .= "</table>";
			//$bv = $this->insertTransferBV($no, $data);

			$ins_followup = "INSERT INTO DION_followup (id, formno, cust_type, cust_id, cust_name, tel_no, tel_hp, email,
							comm_method, attached_app, sent_to, followup_type, followup_detail, 
							createby, status_followup, followup_detail2, signature_status)
						 VALUES ('$no', '$data[formno]', '$data[cust_type]', '$data[cust_id]', '$data[cust_name]', 
						    '$data[tel_no]', '$data[tel_hp]', '$data[email]',
						    '$data[comm_method]', '$data[attached_app]', '$data[sent_to]', '$data[followup_type]', '$data[followup_detail]', 
						    '" . $this->username . "', 1, '" . $txt . "', 8)";
			//echo $ins_followup;				
			$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		} /*else if ($data['followup_type'] == "1" || $data['followup_type'] == "6") {
			$shortname = substrwords($data['fullnm'], 15);
			$title = $data['followup_type'] == "1" ? "RESIGNATION" : "REACTIVATE ID";
			$txt = "$title : $data[dfno]/$data[fullnm]";
			$success = 0;
			$qry = "INSERT INTO DION_followup_resign_reactive (followup_id, followup_type, dfno, createby, createdt, status, idno, fullnm, memberstatus) 
				        	VALUES ('$no', '" . $data['followup_type'] . "', '" . $data['dfno'] . "', 
				        	   '" . $this->username . "', '" . $this->dateTime . "', '0', '" . $data['idno'] . "', '" . $data['fullnm'] . "', '" . $data['memberstatus'] . "')";
			$query = $this->executeQuery($qry, $this->db2);
			if ($query > 0) {
				$success++;
			}

			$ins_followup = "INSERT INTO DION_followup (id, formno, cust_type, cust_id, cust_name, tel_no, tel_hp, email,
							comm_method, attached_app, sent_to, followup_type, followup_detail, 
							createby, status_followup, followup_detail2)
						 VALUES ('$no', '$data[formno]', '$data[cust_type]', '$data[cust_id]', '$data[cust_name]', 
						    '$data[tel_no]', '$data[tel_hp]', '$data[email]',
						    '$data[comm_method]', '$data[attached_app]', '$data[sent_to]', '$data[followup_type]', '$data[followup_detail]', 
						    '" . $this->username . "', 1, '" . $txt . "')";
			$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);

		} */else {
				$sign_acc = 1;
				if($data['followup_type'] == "5" || $data['followup_type'] == "8") {
					$sign_acc = 8;
				}
				$ins_followup = "INSERT INTO DION_followup (id, formno, cust_type, cust_id, cust_name, tel_no, tel_hp, email,
								comm_method, attached_app, sent_to, followup_type, followup_detail, 
								createby, status_followup, signature_status)
							 VALUES ('$no', '$data[formno]', '$data[cust_type]', '$data[cust_id]', '$data[cust_name]', 
							    '$data[tel_no]', '$data[tel_hp]', '$data[email]',
							    '$data[comm_method]', '$data[attached_app]', '$data[sent_to]', '$data[followup_type]', '$data[followup_detail]', 
							    '" . $this->username . "', 1, $sign_acc)";
				$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
				
				//update 19/09/2016
				if($data['dfno'] != "") {
					
				    $status_followup = $data['followup_type'] == "6" ? "1" : "0";
					$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, dfno_rank, status_followup, createby)
								 VALUES ('$no', '$data[followup_type]', '$data[dfno]', '$data[fullnm]', '$data[dfno_rank]', '$status_followup', '".$this->username."')";
					$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
				}
		}
		//}

		$arr = array("response" => "false", "message" => "Input Follow Up gagal..");
		if ($exe_ins_followup > 0 && $log > 0) {
			$arr = array("response" => "true", "message" => "Input Follow Up No $data[formno] berhasil, ID : $no ");
		}
		return $arr;
	}
	
	function listUnprocesedFollowUpByType($tipe) {
		$qry = "SELECT a.formno, a.id, a.followup_type
				FROM DION_followup a
				WHERE a.status_followup != '3' AND a.status_followup != '5' AND a.batch_los_status = '0'
				AND a.followup_type = '$tipe'";	
	    //echo $qry;
	    $hasil = $this->getRecordset($qry, NULL, $this->db2);
		return $hasil;
	}

	function getDetailTrxByTrcd($field, $orderno) {
		$qry = "SELECT NEWTRH.trcd,  NEWTRH.dfno, msmemb.fullnm as nmmember, 
    	       		   NEWTRH.remarks as remarka,  ordtrh.remarks as remarkb,  NEWTRH.createnm, NEWTRH.updatenm,
    			       NEWTRH.loccd, mssc.fullnm as mainstkname, NEWTRH.sc_dfno, mssc1.fullnm as stkname, 
    				   NEWTRH.sc_co as scco, mssc2.fullnm as costkname, NEWTRH.nbv, NEWTRH.ndp, 
    				   CONVERT(VARCHAR(30),NEWTRH.updatedt,103) as tglupd ,  
    				   CONVERT(VARCHAR(30),NEWTRH.createdt,103) as tglinp ,
    				   CONVERT(VARCHAR(30),NEWTRH.bnsperiod,103) as tglbns,
                       DATEDIFF(MONTH, NEWTRH.bnsperiod, GETDATE()) as diffdate,
                       DAY(GETDATE()) as tglnow
        	    FROM  NEWTRH 
                  INNER JOIN mssc ON (NEWTRH.loccd = dbo.mssc.loccd)
                  INNER JOIN mssc mssc1 ON (NEWTRH.sc_dfno = mssc1.loccd)
                  INNER JOIN mssc mssc2 ON (NEWTRH.sc_co = mssc2.loccd)
                  INNER JOIN msmemb ON (NEWTRH.dfno = msmemb.dfno)
                  INNER JOIN ordtrh ON (NEWTRH.trcd = ordtrh.trcd)
                WHERE 
                  (NEWTRH.$field = '$orderno')";
		$hasil = $this->getRecordset($qry, NULL, $this->db2);
		if ($hasil == null) {
			$cek = "SELECT newtrh.trcd, newtrh.sc_dfno, newtrh.sc_co as sc_co, newtrh.loccd 
                        FROM newtrh
                        WHERE trcd = '$orderno' or orderno = '$orderno'
                        GROUP BY newtrh.trcd, newtrh.sc_dfno, newtrh.sc_co, newtrh.loccd";
			$cek3 = $this->getRecordset($cek, NULL, $this->db2);
			if ($cek3 != null) {
				if ($cek3[0]->sc_co == null OR $cek3[0]->sc_co == "" OR $cek3[0]->sc_co == " ") {

					//QUERY UTK CN / MSN
					$qry = "SELECT NEWTRH.trcd, NEWTRH.orderno, NEWTRH.dfno, msmemb.fullnm as nmmember, 
                                   NEWTRH.remarks , NEWTRH.createnm, NEWTRH.updatenm, 
                			       NEWTRH.loccd, mssc.fullnm as mainstkname, 
								   NEWTRH.sc_dfno, mssc1.fullnm as stkname, 
                				   NEWTRH.loccd as scco, mssc2.fullnm as costkname, 
								   NEWTRH.nbv, NEWTRH.ndp,
								   CONVERT(VARCHAR(30),NEWTRH.updatedt,103) as tglupd ,  
                				   CONVERT(VARCHAR(30),NEWTRH.createdt,103) as tglinp ,
                				   DATEDIFF(MONTH, NEWTRH.bnsperiod, GETDATE()) as diffdate,
                                   DAY(GETDATE()) as tglnow
                    	    FROM  NEWTRH 
                              INNER JOIN mssc ON (NEWTRH.loccd = dbo.mssc.loccd)
                              INNER JOIN mssc mssc1 ON (NEWTRH.sc_dfno = mssc1.loccd)
                              INNER JOIN mssc mssc2 ON (NEWTRH.loccd = mssc2.loccd)
                              INNER JOIN msmemb ON (NEWTRH.dfno = msmemb.dfno)
                            WHERE 
                              (NEWTRH.orderno = '$orderno' or NEWTRH.trcd = '$orderno')";
					//echo "if";
					$hasil = $this->getRecordset($qry, NULL, $this->db2);
				} else {

					$qry = "SELECT NEWTRH.trcd, NEWTRH.orderno, NEWTRH.dfno, msmemb.fullnm as nmmember, 
                                   NEWTRH.remarks , NEWTRH.createnm, NEWTRH.updatenm, 
                			       NEWTRH.loccd, mssc.fullnm as mainstkname, 
								   NEWTRH.sc_dfno, mssc1.fullnm as stkname, 
                				   NEWTRH.sc_co AS scco, mssc2.fullnm as costkname, 
								   NEWTRH.nbv, NEWTRH.ndp,
                				   CONVERT(VARCHAR(30),NEWTRH.updatedt,103) as tglupd ,  
                				   CONVERT(VARCHAR(30),NEWTRH.createdt,103) as tglinp ,
                				   CONVERT(VARCHAR(30),NEWTRH.bnsperiod,103) as tglbns,
                                   DATEDIFF(MONTH, NEWTRH.bnsperiod, GETDATE()) as diffdate,
                                   DAY(GETDATE()) as tglnow
                        	    FROM  NEWTRH 
                                  INNER JOIN mssc ON (NEWTRH.loccd = dbo.mssc.loccd)
                                  INNER JOIN mssc mssc1 ON (NEWTRH.sc_dfno = mssc1.loccd)
                                  INNER JOIN mssc mssc2 ON (NEWTRH.sc_co = mssc2.loccd)
                                  INNER JOIN msmemb ON (NEWTRH.dfno = msmemb.dfno)
                                WHERE 
                                  (NEWTRH.orderno = '$orderno' or NEWTRH.trcd = '$orderno')";
					//(LTRIM(RTRIM(NEWTRH.orderno)) = '$orderno')
					//echo $qry;
					$hasil = $this->getRecordset($qry, NULL, $this->db2);
				}
				$tipe = "cnms";
				$listprd = $this->getDetailPrdByTrcd($hasil[0]->trcd, $tipe);
			} else {
				$hasil = null;

			}

		} else {
			$tipe = "inv";
			//echo "sds";
			$listprd = $this->getDetailPrdByTrcd($hasil[0]->trcd, $tipe);
		}
		$batas_update = $this->getDaySysPref();
		return array("hasil" => $hasil, "tipe" => $tipe, "batas_update" => $batas_update, "product" => $listprd);
	}

	function getDaySysPref() {
		$updttpdt = "SELECT DAY(updatettpdt) as updatettpdt FROM syspref";
		$hasil = $this->getRecordset($updttpdt, NULL, $this->db2);
		return $hasil[0]->updatettpdt;
	}

	function getDetailPrdByTrcd($orderno, $tipe) {
		if ($tipe == "inv") {
			$qry = "SELECT newtrd.prdcd, qtyord, newtrd.dp, newtrd.pv, 
		         qtyord * newtrd.dp as sumharga, qtyord * newtrd.pv as sumbv, prdnm
    	         FROM newtrd, msprd WHERE msprd.prdcd = newtrd.prdcd AND trcd = '$orderno' ORDER BY prdnm";
		} else {
			$qry = "SELECT sc_newtrd.prdcd, qtyord, sc_newtrd.dp, 
		         sc_newtrd.pv, qtyord * sc_newtrd.dp as sumharga, qtyord * sc_newtrd.pv as sumbv, prdnm
    	         FROM sc_newtrd, msprd WHERE msprd.prdcd = sc_newtrd.prdcd AND trcd = '$orderno' ORDER BY prdnm";
		}
		//echo $qry;
		$hasil = $this->getRecordset($qry, NULL, $this->db2);
		return $hasil;
	}

	function updateStatusTrfBv($data) {
		$tglupd = $this->dateTime;
		$log = "UPDATE DION_followup_trf_bv SET status = '1', updatedt = '$tglupd'
			 	        WHERE id = $data[rec_id]";
		$inslog = $this->executeQuery($log, $this->db2);
		
		//$updFU = "UPDATE DION_followup set status_followup = '2' where ";
		return $inslog;
	}

	function updateCnMS($data) {
		$return = false;
		$tglupd = $this->dateTime;
		$remarks = trim(strtoupper($data['remarks']));
		$updNewtrh = "UPDATE newtrh set dfno='$data[new_memberid]', updatenm='" . $this->username . "', 
		     					updatedt='$tglupd', remarks='$remarks', flag_syn='0'
        	               WHERE trcd='$data[trcd]'";
		$newtrh = $this->executeQuery($updNewtrh, $this->db2);
		//echo "$updNewtrh<br>";
		$updsc_newtrh = "UPDATE sc_newtrh set dfno='$data[new_memberid]', updatenm='" . $this->username . "', 
        	                    updatedt='$tglupd', remarks='$remarks'
        	                  WHERE trcd='$data[trcd]'";
		$sc_newtrh = $this->executeQuery($updsc_newtrh, $this->db2);
		//echo $updsc_newtrh;
		if ($newtrh > 0 && $sc_newtrh > 0) {
			$this->updateStatusTrfBv($data);
			//$this->insertLogFollowup($followupid, $status, $detail, $createby)
			$return = true;
		}
		return $return;
	}

	function updateInvoice($data) {
		$return = false;
		$tglupd = $this->dateTime;
		$remarks = trim(strtoupper($data['remarks']));
		$updNewtrh = "UPDATE newtrh SET dfno='$data[new_memberid]', updatenm='" . $this->username . "', 
        	                  updatedt='$tglupd', remarks='$remarks',
        	               flag_syn='0' WHERE trcd='$data[trcd]'";
		$newtrh = $this->executeQuery($updNewtrh, $this->db2);
		//echo "$updNewtrh<br>";
		$updInv = "UPDATE ordtrh SET dfno='$data[new_memberid]', updatenm='" . $this->username . "', 
        	                 updatedt='$tglupd', remarks='$remarks'
        	                  WHERE invoiceno='$data[trcd]'";
		//echo "$updInv<br>";
		$invoice = $this->executeQuery($updInv, $this->db2);
		if ($newtrh > 0 && $invoice > 0) {
			$this->updateStatusTrfBv($data);
			$return = true;
		}
		return $return;
	}
	
	function checkDataBeforeTermination($act, $dfno) {
		if($act == "4") {
			
		} else if($act == "2") {
			
		}
	}
	
	function updateResignTermination($data) {
		$ip = $_SERVER['REMOTE_ADDR'];
		$tgl_updt = date("Y-m-d H:i:s A");
		$user_name = $this->username;
		$updatektp = "T".$data['idno'];
		
	}	

	function updateResignTermination2($data) {
		$ip = $_SERVER['REMOTE_ADDR'];
		$tgl_updt = date("Y-m-d H:i:s A");
		$user_name = $this->username;
		$updatektp = "T".$data['idno'];
		
		
			$stat2 = "select status, memberstatus from msmemb where dfno='$data[dfno]'";
			$res_stat2 = $this->getRecordset($stat2, NULL, $this->db2);
			if ($res_stat2 != null) {
				if ($res_stat2[0]->memberstatus == "4") {
					//echo "$idmember sudah pernah melakukan Termination pada tanggal $row[TGL]";
					$errMsg = "$data[dfno] sudah pernah melakukan Termination";
					
				} else if ($res_stat2[0]->status == "0") {
					//echo "$idmember sudah pernah melakukan Resignation pada tanggal $row[TGL]";
					$errMsg = "$data[dfno] sudah pernah melakukan Resignation";
				} else {
					//echo "$idmember masih dalam masa Active";
					$errMsg = "$data[dfno] masih dalam masa Active";
				}
				$arx = jsonFalseResponse($errMsg);
			} else {
				if ($data['act'] == "4") {
					$update = "update msmemb set idno='$updatektp',memberstatus='4',
			             updatenm='$user_name',updatedt='$tgl_updt',flag_syn='0',iplastupd='$ip' 
			           where dfno='$data[dfno]'";
					$qry_upd = $this->executeQuery($update, $this->db2);
					$arx = jsonTrueResponse(null, "Data berhasil diinput..");
				} else if($data['act'] == "2") {
					$update = "update msmemb set idno='$updatektp',status='0',
			             updatenm='$user_name',updatedt='$tgl_updt',flag_syn='0',iplastupd='$ip' 
			           where dfno='$data[dfno]'";
					$qry_upd = $this->executeQuery($update, $this->db2);
					$arx = jsonTrueResponse(null, "Data berhasil diinput..");
				}	
			}
		
		 
		//return array("response" => "false", "message" => $errMsg); 
				return $arx;

	}

	function updateStatResignReactive($id, $data) {
		$username = $this->username;
		if($data['act'] == "4") {
			$keterangan = "$data[dfno] : TERMINATION";
		} else if($data['act'] == "2") {
			$keterangan = "$data[dfno] : RESIGNATION";
		}
		
		//update dion @20/08/2018
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', memberstatus = '$data[act]',
		            dfno = '$data[dfno]', jointdt = '$data[jointdt]', idno = '$data[idno]', address = '$data[addr]', stockist = '$data[stockist]', fullnm='".$data['fullnm']."',
					updateby = '".$username."', updatedt = '".$this->dateTime."'
				WHERE followup_id = '$id'";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => $keterangan, "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function insertPengunduranDiri($id, $data) {
		$username = $this->username;
		if($data['act'] == "4") {
			$keterangan = "$data[dfno] : TERMINATION";
		} else if($data['act'] == "2") {
			$keterangan = "$data[dfno] : RESIGNATION";
		}
		$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, idno, 
									stockist, memberstatus, status_followup, createby, updateby, updatedt,
									jointdt, address)
							 VALUES ('$id', '1', '$data[dfno]', '$data[fullnm]', '$data[idno]', '$data[stockist]', 
							 		'$data[act]', '1', '$username', '$username', '".$this->dateTime."', '".$data['jointdt']."', '".$data['addr']."')";
		//echo $ins_followup;
		$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($exe_ins_followup > 0) {
			$hasil = array("response" => "true", "message" => $keterangan, "upd" => "false");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function updateStatExchangeAcc($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', trf_to_id = '$data[trf_to_id]', trf_to_nm = '$data[trf_to_nm]',
					updateby = '".$username."', updatedt = '".$this->dateTime."', keterangan = '$data[keterangan]'
				WHERE followup_id = '$id'";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Exchange Acc berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function insertExchangeAcc($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, trf_to_id, 
									trf_to_nm, status_followup, createby, updateby, updatedt, keterangan)
							 VALUES ('$id', '4', '$data[dfno]', '$data[fullnm]', '$data[trf_to_id]', '$data[trf_to_nm]', 
							 		 '1', '$username', '$username', '".$this->dateTime."', '$data[keterangan]')";
		//echo $ins_followup;
		$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($exe_ins_followup > 0) {
			$hasil = array("response" => "true", "message" => "Data Exchange Acc berhasil diinput..", "upd" => "false");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function updateStatTrfLine($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', dfno='$data[dfno]' , fullnm = '$data[fullnm]', trf_to_id = '$data[new_sponsorid]', trf_to_nm = '$data[new_sponsorname]',
					updateby = '".$username."', updatedt = '".$this->dateTime."', keterangan = '$data[keterangan]'
				WHERE followup_id = '$id'";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Transfer Line berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function updateStatTrfLine2($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', dfno='$data[dfno]' , fullnm = '$data[fullnm]', trf_to_id = '$data[new_sponsorid]', trf_to_nm = '$data[new_sponsorname]',
					updateby = '".$username."', updatedt = '".$this->dateTime."', keterangan = '$data[keterangan]'
				WHERE id = $id";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Transfer Line berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function insertTrfLine($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, trf_to_id, 
									trf_to_nm, status_followup, createby, updateby, updatedt, keterangan)
							 VALUES ('$id', '3', '$data[dfno]', '$data[fullnm]', '$data[new_sponsorid]', '$data[new_sponsorname]', 
							 		 '1', '$username', '$username', '".$this->dateTime."', '$data[keterangan]')";
		//echo $ins_followup;
		$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($exe_ins_followup > 0) {
			$hasil = array("response" => "true", "message" => "Data Transfer Line berhasil diinput..", "upd" => "false");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function updateStatTrfRecruiter($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', dfno='$data[dfno]' , fullnm = '$data[fullnm]', trf_to_id = '$data[new_sponsorid]', trf_to_nm = '$data[new_sponsorname]',
					updateby = '".$username."', updatedt = '".$this->dateTime."', keterangan = '$data[keterangan]'
				WHERE followup_id = '$id'";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Transfer Recruiter berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function updateStatTrfRecruiter2($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', dfno='$data[dfno]' , fullnm = '$data[fullnm]', trf_to_id = '$data[new_sponsorid]', trf_to_nm = '$data[new_sponsorname]',
					updateby = '".$username."', updatedt = '".$this->dateTime."', keterangan = '$data[keterangan]'
				WHERE id = $id";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Transfer Recruiter berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function insertTrfRecruiter($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, trf_to_id, 
									trf_to_nm, status_followup, createby, updateby, updatedt, keterangan)
							 VALUES ('$id', '8', '$data[dfno]', '$data[fullnm]', '$data[new_sponsorid]', '$data[new_sponsorname]', 
							 		 '1', '$username', '$username', '".$this->dateTime."', '$data[keterangan]')";
		//echo $ins_followup;
		$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($exe_ins_followup > 0) {
			$hasil = array("response" => "true", "message" => "Data Transfer Recruiter berhasil diinput..", "upd" => "false");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function updateStatTrfName($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$tgllahir = $data['dd']."/".$data['mm']."/".$data['yy'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', trf_to_nm = '$data[receiver_name]',
					idno = '$data[idno]', dob = '$tgllahir', address = '$data[address]', sex = '$data[sex]',
				    stockist = '$data[stockist]', bank_code = '$data[bank_code]', acc_no = '$data[acc_no]',  
				    keterangan = '$data[keterangan]', updateby = '".$username."', updatedt = '".$this->dateTime."'
				WHERE followup_id = '$id'";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Transfer Name berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	
	
	function updateStatCorrectionName($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		//$tgllahir = $data['dd']."/".$data['mm']."/".$data['yy'];
		$qry = "UPDATE DION_followup_trf_acc SET status_followup = '1', dfno = '$data[dfno]', fullnm = '$data[fullnm]', trf_to_nm = '$data[receiver_name]', 
				    keterangan = '$data[keterangan]', updateby = '".$username."', updatedt = '".$this->dateTime."'
				WHERE followup_id = '$id'";
		$qryUpd = $this->executeQuery($qry, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($qryUpd > 0) {
			$hasil = jsonTrueResponse(null, $keterangan);
			$hasil = array("response" => "true", "message" => "Data Name Correction berhasil diinput..", "upd" => "true");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function insertTrfName($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$tgllahir = $data['dd']."/".$data['mm']."/".$data['yy'];
		$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, trf_to_nm, 
									idno, dob, address, sex, stockist, bank_code, acc_no, keterangan,
								   status_followup, createby, updateby, updatedt)
							 VALUES ('$id', '2', '$data[dfno]', '$data[fullnm]', '$data[receiver_name]', '$data[idno]', '$tgllahir',
							        '$data[address]', '$data[sex]', '$data[stockist]','$data[bank_code]', '$data[acc_no]', '$data[keterangan]',
							 		 '1', '$username', '$username', '".$this->dateTime."')";
		//echo $ins_followup;
		$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($exe_ins_followup > 0) {
			$hasil = array("response" => "true", "message" => "Data Transfer Name berhasil diinput..", "upd" => "false");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}

	function insertCorrectionName($id, $data) {
		$username = $this->username;
		$keterangan = $data['keterangan'];
		$tgllahir = $data['dd']."/".$data['mm']."/".$data['yy'];
		$ins_followup = "INSERT INTO DION_followup_trf_acc (followup_id, followup_type, dfno, fullnm, trf_to_nm, 
									keterangan,
								   status_followup, createby, updateby, updatedt)
							 VALUES ('$id', '2', '$data[dfno]', '$data[fullnm]', '$data[receiver_name]', '$data[keterangan]',
							 		 '1', '$username', '$username', '".$this->dateTime."')";
		//echo $ins_followup;
		$exe_ins_followup = $this->executeQuery($ins_followup, $this->db2);
		
		$this->insertLogFollowup($id, "2", $keterangan, $username);
		if($exe_ins_followup > 0) {
			$hasil = array("response" => "true", "message" => "Data Transfer Name berhasil diinput..", "upd" => "false");
		} else {
			$hasil = jsonFalseResponse("Data gagal di update");
		}
		return $hasil;
	}
	
	function listPengunduranDiri($data) {
		$dtfrom = date("Y/m/d", strtotime($data['los_from']));
		$dtto = date("Y/m/d", strtotime($data['los_to']));
		$resignQry = "SELECT a.followup_id, a.dfno, 
					  	    CONVERT(VARCHAR(11), a.createdt, 113) as createdt, a.createby,
					  	    a.memberstatus,
					  	    a.followup_type,
					  	 CASE a.memberstatus 
					  	   WHEN '4' THEN 'TERMINATION'
					  	   WHEN '2' THEN 'RESIGNATION'
					  	 ELSE 
					  	  CASE a.followup_type 
					  	    WHEN '2' THEN 'TRANSFER NAME' 
					  	    WHEN '3' THEN 'TRANSFER LINE'
					  	    WHEN '4' THEN 'EXCHANGE ACCOUNT'
					  	    WHEN '5' THEN 'EXCHANGE STATEMENT BONUS'
					  	    WHEN '8' THEN 'UPDATE RECRUITER'
					  	    WHEN '9' THEN 'NAME CORRECTION'
					  	  ELSE  'REACTIVE ID'
					  	  END
						 END as keterangan ,
						 b.formno 
					  FROM DION_followup_trf_acc a
					  INNER JOIN DION_followup b ON (a.followup_id = b.id)
					  
					  WHERE CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto' 
					     AND b.batch_los_status = '0' AND a.status_followup = '1' AND a.followup_type != '5' AND a.followup_type != '7'
					  ORDER BY a.followup_type, a.memberstatus ASC";
			$resignReactive = $this->getRecordset($resignQry, NULL, $this->db2);
	    //echo $resignQry;
		return $resignReactive;
	}
	
	function generateLosNo() {
		$bid = "BID06";
		$date = date("my");
		$str = $bid."-".$date;
		$qry = "SELECT MAX(SUBSTRING(los_no, 12, 3)) as no_max 
				FROM 
				DION_followup a 
				where a.los_no LIKE '$str%'";
		$qryExe = $this->getRecordset($qry, NULL, $this->db2);
		if ($qryExe == null) {
			$next = "001";
		} else {
			$next = $qryExe[0]->no_max + 1;
			$nextTo = sprintf("%'.03d", $next);
		}
		return $str."-".$nextTo;
	}
	
	function generateBatchNo() {
		$bid = "BAT";
		$date = date("my");
		$str = $bid.$date;
		$qry = "SELECT MAX(SUBSTRING(batch_no, 9, 2)) as no_max 
				FROM 
				DION_followup a 
				where a.batch_no LIKE '$str%'";
		//echo $qry;
		$qryExe = $this->getRecordset($qry, NULL, $this->db2);
		if ($qryExe == null) {
			$next = "01";
		} else {
			$next = $qryExe[0]->no_max + 1;
			$nextTo = sprintf("%'.02d", $next);
		}
		return $str."-".$nextTo;
	}
	
	function getDetailExcAcc($id) {
		$qry = "SELECT dfno, fullnm, trf_to_id, trf_to_nm, keterangan FROM DION_followup_trf_acc WHERE id = $id";
		$res = $this->getRecordset($qry, NULL, $this->db2);
		return $res;
	}
	
	function saveLOS($form) {
		$arr = jsonFalseResponse("Generate LOS failed..");
		//$arr = set_list_array_to_string($form['followup_id']);
		$losno = $this->generateLosNo();
		$updateFollowUp = $this->updateLosStatus($form, $losno);
		
		if($updateFollowUp > 0) {
			$arr = jsonTrueResponse(null, "Generate LOS success, LOS No : $losno");
		}
		return $arr;
		
	}

     function updateBatchLOS($form, $value) {
     	//$arr = set_list_array_to_string($form['followup_id']);
     	$qryIns = "UPDATE DION_followup SET batch_los_status = '$value'
		          WHERE id IN ($form)";
		//echo $qryIns;
		$upd = $this->executeQuery($qryIns, $this->db2);
		return $upd;
     }
	 
	 function updateLosStatus($form, $losno) {
	 	$jum = count($form['followup_id']);
		for($i=0; $i<$jum; $i++) {
			$qryIns = "UPDATE DION_followup SET batch_los_status = '1', los_no = '$losno', 
	 	               los_createby = '".$this->username."', los_createdt = '".$this->dateTime."', signature_status = '1'
		          WHERE id = '".$form['followup_id'][$i]."'";
		     //echo $qryIns;
		    $upd = $this->executeQuery($qryIns, $this->db2);
			
			$this->insertLogFollowup($form['followup_id'][$i], "2", "LOS created : $losno", $this->username);
		}
	 	
		return $upd;
	 }
	 
	 function updateBatchStatus($form, $bacthno) {
	 	//$jum = count($form['followup_id']);
	 	$arr = set_list_array_to_string($form['followup_id']);
		
		$qryIns = "UPDATE DION_followup SET batch_los_status = '2', batch_no = '$bacthno', 
		 	               batch_createby = '".$this->username."', batch_createdt = '".$this->dateTime."', signature_status = '1'
			          WHERE id IN ($arr)";
		$qryx = $this->executeQuery($qryIns, $this->db2);		
					  
		$ins_log = "INSERT INTO DION_followup_log (followup_id, status, detail, createby) 
			           SELECT id, '2' as status, 'Batch created : $bacthno' as detail, '".$this->username."' FROM DION_followup WHERE id IN ($arr)";
		$qry = $this->executeQuery($ins_log, $this->db2);
		
		return $qryx;
	 }
	
	function getListLOS($data) {
		$str = "";
        
		if ($data['searchBy'] != "") {
			$str .= " AND a.$data[searchBy] LIKE '$data[paramValue]%' ";
		}

		if ($data['followup_type'] != "") {
			$str .= " AND a.followup_type = $data[followup_type] ";
		}
		
		$dtfrom = date("Y/m/d", strtotime($data['los_create_from']));
		$dtto = date("Y/m/d", strtotime($data['los_create_to']));
		
		if($data['followup_type'] == "1" || $data['followup_type'] == "6" || $data['followup_type'] == "3") {
			$qry = "SELECT  DISTINCT(b.los_no), 
					        CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt, 
					        b.los_createby,
					        b.followup_type
					FROM DION_followup b 
					WHERE CONVERT(VARCHAR(10), b.los_createdt, 111) 
					BETWEEN '$dtfrom' AND '$dtto' 
					AND (b.los_no is not NULL OR b.los_no != '') 
					AND b.followup_type = $data[followup_type] 
					ORDER BY b.los_no";
		   //echo $qry;
		} else {
		    $qry = "SELECT a.id, a.followup_id, b.los_no, a.dfno, a.fullnm, a.trf_to_id, a.trf_to_nm,
			           CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt, 
			           CONVERT(VARCHAR(10), a.createdt, 111) as createdt,
			           a.followup_type, a.memberstatus, a.keterangan, a.dfno_rank
			        FROM DION_followup_trf_acc a
			        INNER JOIN DION_followup b ON (a.followup_id = b.id)
			        WHERE CONVERT(VARCHAR(10), b.los_createdt, 111) 
			        BETWEEN '$dtfrom' AND '$dtto' AND (b.los_no is not NULL OR b.los_no != '') $str 
			        ORDER BY b.los_no, a.followup_type, a.memberstatus";
		}		
	    //echo $qry;
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDetailDataLos($id) {
		$qry = "SELECT a.keterangan, a.dfno, a.fullnm, a.trf_to_id, a.trf_to_nm, 
		               CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt,
		               b.los_no, a.idno, a.address, a.sex, a.dob, a.bank_code, a.acc_no,
		               a.stockist, a.memberstatus, a.dfno_rank, a.followup_id,
		               MONTH(b.los_createdt) as bulan, YEAR(b.los_createdt) as tahun	
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (a.followup_id = b.id) 
		        WHERE a.id = $id";
		
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDetailDataByLOSno($id) {
		$qry = "SELECT a.keterangan, a.dfno, a.fullnm, a.trf_to_id, a.trf_to_nm, 
		               CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt,
		               b.los_no, a.idno, a.address, a.sex, a.dob, a.bank_code, a.acc_no,
		               a.stockist, a.memberstatus, a.dfno_rank, a.followup_id, a.keterangan
		               MONTH(b.los_createdt) as bulan, YEAR(b.los_createdt) as tahun	
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (a.followup_id = b.id) 
		        WHERE a.id = $id";
		
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDataLOSbyLosNo($id) {
		$qry = "SELECT a.keterangan, a.dfno, a.fullnm, a.trf_to_id, a.trf_to_nm, 
		               CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt,
		               b.los_no, a.idno, a.address, a.sex, a.dob, a.bank_code, a.acc_no,
		               a.stockist, a.memberstatus, a.dfno_rank, a.followup_id,  a.keterangan,
		               MONTH(b.los_createdt) as bulan, YEAR(b.los_createdt) as tahun	
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (a.followup_id = b.id) 
		        WHERE b.los_no = '$id'
		        ORDER BY a.followup_type, a.memberstatus, a.dfno";
		//echo $qry;
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDataTrfLinebyLosNo($id) {
		$qry = "SELECT a.dfno, a.fullnm, a.trf_to_id, a.trf_to_nm, 
				       CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt,
				       b.los_no, a.idno, a.address, a.sex, a.dob, a.bank_code, a.acc_no,
				       a.stockist, a.memberstatus, a.dfno_rank, a.followup_id,  a.keterangan,
				       MONTH(b.los_createdt) as bulan, YEAR(b.los_createdt) as tahun,
				       c.sfno_reg as recruiterid, d.fullnm as recruitername	
				FROM DION_followup_trf_acc a
				INNER JOIN DION_followup b ON (a.followup_id = b.id)
				INNER JOIN msmemb c ON (a.dfno COLLATE SQL_Latin1_General_CP1_CS_AS = c.dfno) 
				INNER JOIN msmemb d ON (c.sfno_reg = d.dfno)
		        WHERE b.los_no = '$id'";
		//echo $qry;
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDetailDataLosByFid($id) {
		$qry = "SELECT a.keterangan, a.dfno, a.fullnm, a.trf_to_id, a.trf_to_nm, 
		               CONVERT(VARCHAR(10), b.los_createdt, 111) as los_createdt,
		               b.los_no, a.idno, a.address, a.sex, a.dob, a.bank_code, a.acc_no,
		               a.stockist, a.memberstatus, a.dfno_rank,
		               MONTH(b.los_createdt) as bulan, YEAR(b.los_createdt) as tahun	
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (a.followup_id = b.id) 
		        WHERE a.followup_id = '$id'";
		        echo $qry;
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	 
	
	function getDetailDataLosBySelectedID($id) {
		$qry = "SELECT a.*, b.los_no
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (a.followup_id = b.id) 
		        WHERE a.id IN ($id)";
		//echo $qry;
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDetailResignationBySelectedID($id) {
		$qry = "SELECT b.los_no, a.dfno, a.fullnm, a.memberstatus, MONTH(b.los_createdt) as bulan, YEAR(b.los_createdt) as tahun
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (a.followup_id = b.id)
		        WHERE a.id IN ($id)
		        ORDER BY memberstatus";
		//echo $qry;
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getListGeneratedLos($data) {
		$dtfrom = date("Y/m/d", strtotime($data['batch_from']));
		$dtto = date("Y/m/d", strtotime($data['batch_to']));
		$qry = "SELECT a.los_no, a.id as followup_id,  a.formno, c.dfno, c.fullnm, 
				      CONVERT(VARCHAR(11), a.los_createdt, 113) as los_createdt, a.los_createby,
				   CASE c.memberstatus 
				     WHEN '4' THEN 'TERMINATION'
				     WHEN '2' THEN 'RESIGNATION'
				   ELSE 
				    CASE c.followup_type 
				      WHEN '2' THEN 'TRANSFER NAME' 
				      WHEN '3' THEN 'TRANSFER LINE'
				      WHEN '4' THEN 'EXCHANGE ACCOUNT'
				      WHEN '5' THEN 'EXCHANGE STATEMENT BONUS'
				      WHEN '8' THEN 'UPDATE RECRUITER'
					  WHEN '9' THEN 'NAME CORRECTION'
				    ELSE  'REACTIVE ID'
				    END
				   END as keterangan  
				FROM DION_followup a
				INNER JOIN DION_followup_trf_acc c ON (a.id = c.followup_id)
				WHERE CONVERT(VARCHAR(10), a.los_createdt, 111) BETWEEN '$dtfrom' AND '$dtto'
				AND (a.batch_no is null OR a.batch_no = '') AND a.batch_los_status = '1'
				ORDER BY c.followup_type, c.memberstatus, a.los_no";
	    //echo $qry;
		$qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}

	function saveBatchLos($form) {
		$arr = jsonFalseResponse("Generate LOS failed..");
		//$arr = set_list_array_to_string($form['followup_id']);
		$batchno = $this->generateBatchNo();
		$updateFollowUp = $this->updateBatchStatus($form, $batchno);
		
		if($updateFollowUp > 0) {
			$arr = jsonTrueResponse(null, "Generate Batch LOS success, Batch No : $batchno");
		}
		return $arr;
		
	}
	
	function getListBatchLos($form) {
		$dtfrom = date("Y/m/d", strtotime($form['batch_list_from']));
		$dtto = date("Y/m/d", strtotime($form['batch_list_to']));
		$qry = "SELECT DISTINCT(a.batch_no) as batch_no, 
				    CONVERT(VARCHAR(11), a.batch_createdt, 113) as batch_createdt, 
				    a.batch_createby, b.acc_by, b.id as sign_id
				FROM DION_followup a
				INNER JOIN DION_followup_sign_acc b ON (a.signature_status = b.id) 
				WHERE (a.batch_no is not null OR a.batch_no != '') AND
				  (CONVERT(VARCHAR(20), a.los_createdt, 111) BETWEEN '$dtfrom' AND '$dtto')
				GROUP BY CONVERT(VARCHAR(11), a.batch_createdt, 113), a.batch_createby,  b.acc_by, a.batch_no, b.id
				";
		//echo $qry;
		$qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getListFollowupAcc() {
		$qry = "SELECT *
		        FROM DION_followup_sign_acc WHERE status = 1";
	    $qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	
	function getDetailBatchById($id) {
		$qry = "SELECT a.los_no, a.id as followup_id,  a.formno, c.dfno, c.fullnm, 
				      CONVERT(VARCHAR(11), a.los_createdt, 113) as los_createdt, a.los_createby,
				   CASE c.memberstatus 
				     WHEN '4' THEN 'TERMINATION'
				     WHEN '2' THEN 'RESIGNATION'
				   ELSE 
				    CASE c.followup_type 
				      WHEN '2' THEN 'TRANSFER NAME' 
				      WHEN '3' THEN 'TRANSFER LINE'
				      WHEN '4' THEN 'EXCHANGE ACCOUNT'
				      WHEN '5' THEN 'EXCHANGE STATEMENT BONUS'
				      WHEN '8' THEN 'UPDATE RECRUITER'
					  WHEN '9' THEN 'NAME CORRECTION'
				    ELSE  'REACTIVE ID'
				    END
				   END as keterangan  
				FROM DION_followup a
				INNER JOIN DION_followup_trf_acc c ON (a.id = c.followup_id)
				WHERE a.batch_no = '$id'
				ORDER BY c.followup_type, a.los_no";
		//echo $qry;
		$qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function updateReqAccBatch($form) {
		$qryx = null;
		//$status = $form['acc_id'] == "5" ? "3" : "2";
		$status = "2";
		$qryIns = "UPDATE DION_followup SET status_followup = '$status', signature_status = '$form[acc_id]' WHERE batch_no = '$form[batch_no]'";
			$qryx = $this->executeQuery($qryIns, $this->db2);
					  
		$ins_log = "INSERT INTO DION_followup_log (followup_id, status, detail, createby) 
			           SELECT id, '$status' as status, '$form[keterangan]' as detail, '".$this->username."' 
			           FROM DION_followup 
			           WHERE batch_no = '$form[batch_no]'";
		$qry = $this->executeQuery($ins_log, $this->db2);
		return $qryx;
	}
	
	function listFollowUpForUpload() {
		$str = "";
		$data = $this->input->post(NULL, TRUE);
		$dtfrom = date("Y/m/d", strtotime($data['cr_from']));
		$dtto = date("Y/m/d", strtotime($data['cr_to']));
		
		if ($data['formno'] != "") {
			$str = " b.formno = '$data[formno]'";
		} else {
			$str = " CONVERT(VARCHAR(10), a.createdt, 111) BETWEEN '$dtfrom' AND '$dtto'";
		}

		$qry = "SELECT a.id as idfo,b.id, b.formno, 
				       a.id as fid, a.dfno, 
				       a.keterangan, c.follow_type,
				       count(d.id) as jum
				FROM DION_followup_trf_acc a 
				INNER JOIN DION_followup b ON (b.id = a.followup_id) 
				INNER JOIN DION_followup_type c ON (a.followup_type = c.id) 
				LEFT JOIN DION_followup_los_image d ON (a.id = d.id)
				WHERE $str
				GROUP BY a.id,b.id, b.formno, 
				       a.id, a.dfno, 
				       a.keterangan, c.follow_type";
		//echo $qry;
		$qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function getDataFollowForUpload($id) {
		$qry = "SELECT b.id, b.formno, a.id as fid, a.dfno, a.keterangan, c.follow_type
		        FROM DION_followup_trf_acc a
		        INNER JOIN DION_followup b ON (b.id = a.followup_id)
				INNER JOIN DION_followup_type c ON (a.followup_type = c.id)
		        WHERE a.id = $id";
		$qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}
	
	function insertDataImage($path, $img, $followupid, $detail_id) {
		$ins_log = "INSERT INTO DION_followup_los_image(url, image_name, id, followupid) VALUES ('$path', '$img', $detail_id, '$followupid')";
		$qry = $this->executeQuery($ins_log, $this->db2);
		return $qry;
	}
	
	function getListPic($fid, $id) {
		$qry = "SELECT * FROM DION_followup_los_image
		        WHERE followupid = '$fid' AND id = '$id'";
		$qryRes = $this->getRecordset($qry, NULL, $this->db2);
		return $qryRes;
	}

}

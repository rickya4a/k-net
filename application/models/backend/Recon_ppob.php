<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Recon_ppob extends MY_Model{
	function __construct() {
		parent::__construct();
	}

	function reconPpob($trxno) {
		$query = "INSERT INTO ppob_trx_hdr (
			trx_id,
			memberid,
			membername,
			member_no_hp,
			trx_type,
			createdt,
			nominal,
			cashback_ori,
			cashback_reff,
			ppob_type,
			amount_sgo
		)
		SELECT
			trx_id,
			memberid,
			membername,
			member_no_hp,
			trx_type,
			createdt,
			nominal,
			cashback_ori,
			cashback_reff,
			ppob_type,
			amount_sgo
		FROM ppob_trx_hdr_tempsgo WHERE trx_id = '$trxno'

		INSERT INTO ppob_trx_det (
			trx_id,
				memberid_target,
				qty,
				nominal,
				no_hp_simcard,
				cust_no,
				reff_pay_id
		)
		SELECT
			trx_id,
				memberid_target,
				qty,
				nominal,
				no_hp_simcard,
				cust_no,
				'REC'
		FROM ppob_trx_det_tempsgo  WHERE trx_id = '$trxno'

		INSERT INTO ppob_trx_pay(
			trx_id,
				bank_code_payment,
				charge_admin,
				docno,
				payamt
		)
		SELECT
			trx_id,
				bank_code_payment,
				charge_admin,
				docno,
				payamt
		FROM ppob_trx_pay_tempsgo WHERE trx_id = '$trxno'";
		echo $query;
		// $result = $this->executeQuery($query);
		// return $result;
	}
}

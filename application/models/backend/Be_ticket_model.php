<?php
class Be_ticket_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getListTicketLocation() {
		$qry = "SELECT price_cat_id, price_cat_desc, eventdate, price_cat_status
		        FROM event_price_cat
		        WHERE price_cat_status = '1'
		        ORDER BY price_cat_id";
		return $this->getRecordset($qry);
	}
	
	function getListPendingPaymentTicket($data) {
		$qry = "SELECT trans_hdr_id, UPPER(trans_hdr_name) as trans_hdr_name, trans_qty_ticket, trans_hdr_dp, bookingNo,
		              CONVERT(VARCHAR(10), trans_hdr_date, 103) as trans_hdr_date
		        FROM event_trans_hdr_prepaid 
		        WHERE pay_type = '0' AND trans_hdr_status = '0'
		        AND trans_hdr_date BETWEEN '$data[pay_ticket_from]' AND '$data[pay_ticket_to]'";
		return $this->getRecordset($qry);
	}
	
	function getListApprovePaymentTicket($data) {
		$qry = "SELECT UPPER(trans_hdr_name) as trans_hdr_name, trans_qty_ticket, trans_hdr_dp, bookingNo,
		              CONVERT(VARCHAR(10), trans_hdr_date, 103) as trans_hdr_date, pay_type, trans_hdr_id
		        FROM event_trans_hdr
		        WHERE flag_paid = '1'
		        AND trans_hdr_date BETWEEN '$data[pay_ticket_from]' AND '$data[pay_ticket_to]'";
		return $this->getRecordset($qry);
	}
	
	function getListPaymentByBookingNo($bookingno) {
		$qry = "SELECT *
		        FROM event_trans_hdr_prepaid 
		        WHERE trans_hdr_id = $bookingno";
		return $this->getRecordset($qry);
	}
	
	function getHeaderEvent($trans_hdr_id) {
		$qry = "SELECT *
		        FROM event_trans_hdr_prepaid 
		        WHERE trans_hdr_id = '$trans_hdr_id'";
		return $this->getRecordset($qry);
	}
	
	function getDetailEvent($trans_hdr_id) {
		$qry = "SELECT c.event_desc, c.event_goals, CONVERT(VARCHAR(10), c.event_date, 103) as event_date,
					   c.event_email, c.event_fax, c.event_website,	 
				       c.event_address, a.bookingNo, a.flag_paid, 
				       a.trans_qty_ticket, a.trans_hdr_dp, UPPER(a.trans_hdr_name) as trans_hdr_name, a.trans_hdr_hp, a.trans_hdr_email,
				       b.trans_det_name, b.trans_det_hp, b.trans_tiket_no, b.trans_det_dp
				FROM event_trans_hdr_prepaid a
				INNER JOIN event_trans_det_prepaid b ON (a.trans_hdr_id = b.trans_hdr_id)
				INNER JOIN event_name c ON (b.event_id = c.event_id) 
				WHERE a.trans_hdr_id = $trans_hdr_id";
		return $this->getRecordset($qry);
	}
	
	function updatePaymentByBookingNo($bookingno) {
		$qry = "UPDATE event_trans_hdr_prepaid
		        SET trans_hdr_status = '1', flag_paid = '1' 
		        WHERE trans_hdr_id = $bookingno";
		return $this->executeQuery($qry);
	}
	
	function insertEventTransHDR($bookingno) {
		$this->db = $this->load->database("db_ecommerce", true);	
		$qry = "SELECT *
		        FROM event_trans_hdr_prepaid 
		        WHERE trans_hdr_id = $bookingno";
		$query = $this->db->query($qry); 
		foreach($query->result() as $data) {
			$qry = "INSERT INTO event_trans_hdr 
						(trans_hdr_id, trans_hdr_date, trans_hdr_name, trans_hdr_hp, trans_hdr_email,
						 trans_qty_ticket, trans_hdr_dp, trans_hdr_ip, trans_hdr_hn, flag_paid, flag_dist,
						 flag_post, post_date, bookingNo, trans_hdr_status, pay_type)
				    VALUES (".$data->trans_hdr_id.", '".$data->trans_hdr_date."', '".$data->trans_hdr_name."', '".$data->trans_hdr_hp."', '".$data->trans_hdr_email."',
					    ".$data->trans_qty_ticket.", ".$data->trans_hdr_dp.", '".$data->trans_hdr_ip."', '".$data->trans_hdr_hn."', '".$data->flag_paid."', '".$data->flag_dist."',
					     '".$data->flag_post."', '".$data->post_date."', '".$data->bookingNo."', '".$data->trans_hdr_status."', '".$data->pay_type."')";
			//echo $qry;
			$ins = $this->executeQuery($qry);
		}		
		return $ins;	 
	}
	
	function insertEventTransDetail($bookingno) {
		$this->db = $this->load->database("db_ecommerce", true);	
		$qry = "SELECT *
		        FROM event_trans_det_prepaid 
		        WHERE trans_hdr_id = $bookingno";
		$query = $this->db->query($qry); 
		foreach($query->result() as $data) {
			$qry = "INSERT INTO event_trans_det 
						(trans_hdr_id, trans_det_seq, event_id, price_tab_id, trans_det_qty,
						 trans_det_dp, trans_det_date, trans_det_name, trans_det_hp, trans_hdr_email, trans_tiket_no)
				    VALUES (".$data->trans_hdr_id.", ".$data->trans_det_seq.", ".$data->event_id.", ".$data->price_tab_id.", ".$data->trans_det_qty.",
					    ".$data->trans_det_dp.", '".$data->trans_det_date."', '".$data->trans_det_name."', '".$data->trans_det_hp."', 
					    '".$data->trans_hdr_email."', '".$data->trans_tiket_no."')";
			//echo $qry;
			$ins = $this->executeQuery($qry);
		}		
		return $ins;	 
	}
	
	function getListEvent() {
		$qry = "SELECT * FROM event_name";
		return $this->getRecordset($qry);
	}
	
	function getListParticipantByLocation($event_id, $pay_status) {
		if ($pay_status == 'all'){
			$qry = "SELECT b.trans_det_name, b.trans_det_hp, b.trans_tiket_no, b.trans_det_dp, a.bookingNo, 'A' as status_pay
					FROM event_trans_det b
						INNER JOIN event_trans_hdr a ON (a.trans_hdr_id = b.trans_hdr_id)
					WHERE b.event_id = 1
					UNION
					SELECT b.trans_det_name, b.trans_det_hp, b.trans_tiket_no, b.trans_det_dp, a.bookingNo, 'P' as status_pay
					FROM event_trans_det_prepaid b
						INNER JOIN event_trans_hdr_prepaid a ON (a.trans_hdr_id = b.trans_hdr_id)
					WHERE b.event_id = $event_id";
		}else{
			if ($pay_status == '0'){
				$pendingTable = '_prepaid';
				$flagPay	  = 'P';
			}elseif ($pay_status == '1'){
				$pendingTable = '';
				$flagPay	  = 'A';
			}
			
			$qry = "SELECT b.trans_det_name, b.trans_det_hp, b.trans_tiket_no, b.trans_det_dp, a.bookingNo, '$flagPay' as status_pay
					FROM event_trans_det".$pendingTable." b
					INNER JOIN event_trans_hdr".$pendingTable." a ON (a.trans_hdr_id = b.trans_hdr_id)
					WHERE b.event_id = $event_id";
		}
			
		return $this->getRecordset($qry);
	}
	
	function getTicketSMSList($data) {
		$tambahan = " ";	
		$tglx = " ";
		if($data['searchBy'] != "") {
			if($data['searchBy'] == "trans_det_name") {
				$tambahan = " $data[searchBy] LIKE '$data[paramValue]%'";
			} else {
				$tambahan = " $data[searchBy] = '$data[paramValue]%'";
			}
		}
		
		if($data['ticket_sms_from'] != "") {
			$tglx = "A.trans_det_date between '$data[ticket_sms_from]' and '$data[ticket_sms_to]' AND";
		}
		$qry = "SELECT A.trans_hdr_id, A.trans_det_name, A.trans_det_hp, A.trans_tiket_no,
		            CONVERT(VARCHAR(10), trans_det_date, 103) as trans_det_date
				FROM event_trans_Det A
				WHERE $tglx
				$tambahan
				ORDER BY $data[searchBy]";
        //echo $qry;				
		return $this->getRecordset($qry);
	}
	
	function getDtRegister($bookingNo){
        $slc = "select distinct a.bookingNo,a.trans_hdr_hp,a.trans_hdr_name,a.trans_qty_ticket,
                a.trans_hdr_email,c.event_desc,c.event_address,c.event_date as event_date, 
                CONVERT(VARCHAR(24),c.event_date,113) as event_date1,a.trans_hdr_dp,a.pay_type,a.flag_paid
                 from event_trans_hdr a
                inner join event_trans_det b on(a.trans_hdr_id = b.trans_hdr_id)
                inner join event_name c on (c.event_id = b.event_id)
                where a.flag_paid = '1' and a.trans_hdr_id = ".$bookingNo." and a.pay_type = '0'";
        //echo $slc;
        //echo "<br>";
        $result = $this->getRecordset($slc);
        return $result;
    }
    
    function getDtRegisterByTicketNo($ticketno) {
    	$slc = "select distinct a.bookingNo,a.trans_hdr_hp,a.trans_hdr_name,a.trans_qty_ticket,
					a.trans_hdr_email,c.event_desc,c.event_address,c.event_date as event_date, 
					CONVERT(VARCHAR(24),c.event_date,113) as event_date1,a.trans_hdr_dp,a.pay_type,a.flag_paid
				from event_trans_hdr a
				inner join event_trans_det b on(a.trans_hdr_id = b.trans_hdr_id)
				inner join event_name c on (c.event_id = b.event_id)
				where a.flag_paid = '1' and b.trans_tiket_no = '$ticketno'";
        //echo $slc;
        //echo "<br>";
        $result = $this->getRecordset($slc);
        return $result;	
    }	
    
    function getDtRegisterPre($bookingNo){
        $slc = "select distinct a.bookingNo,a.trans_hdr_hp,a.trans_hdr_name,a.trans_qty_ticket,
                a.trans_hdr_email,c.event_desc,c.event_address,c.event_date as event_date, 
                CONVERT(VARCHAR(24),c.event_date,113) as event_date1,a.trans_hdr_dp,a.pay_type,a.flag_paid
                 from event_trans_hdr_prepaid a
                inner join event_trans_det_prepaid b on(a.trans_hdr_id = b.trans_hdr_id)
                inner join event_name c on (c.event_id = b.event_id)
                where a.flag_paid = '0' and a.trans_hdr_id = ".$bookingNo." and a.pay_type = '0'";
        //echo $slc;
        // echo "<br>";
        $result = $this->getRecordset($slc);
        return $result;
    }
    
    function getDtDetail($randomid){
        
        $slcDet = "select * from event_trans_det where trans_hdr_id = ".$randomid."";
        //echo $slcDet;
        $result = $this->getRecordset($slcDet);
        return $result;
    }
	
	function getDtDetailByTicketNo($ticketno) {
		$slcDet = "select * from event_trans_det where trans_tiket_no = '".$ticketno."'";
        //echo $slcDet;
        $result = $this->getRecordset($slcDet);
        return $result;
	}
	
	function getDtDetailPre($randomid){
        
        $slcDet = "select * from event_trans_det_prepaid where trans_hdr_id = ".$randomid."";
        //echo $slcDet;
        $result = $this->getRecordset($slcDet);
        return $result;
    }
	
	function sendSmsTiket($det, $head){
        $alternate2 = $this->load->database('alternate2', true);
        foreach($head as $row){
            $bookNo = $row->bookingNo;
            $telhp = $row->trans_hdr_hp;
            $nmmember = $row->trans_hdr_name;
            $email = $row->trans_hdr_email;
            $eventPlace = $row->event_address;
            $eventDesc = $row->event_desc;
            $eventDate = date("d/m/Y H:i", strtotime($row->event_date));
			$eventPlace2 = $eventDesc.", date ".$eventDate; //->format('d/m/Y H:i');
        }
    
        foreach($det as $list){
            $tiket = $list->trans_tiket_no;
            $telhape = $list->trans_det_hp;
            $nmmember = $list->trans_det_name;
            
            $textDet = 'K-LINK EVENT YOGA: Ticket Number: '.$tiket.',Nama:'.$nmmember.',Tempat : '.$eventPlace2.'';
            
            $insDet = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID) 
	                    VALUES ('".$telhape."','".$textDet."','K-LINK Event Yoga')";
            //echo $insDet;
            $queryDet = $alternate2->query($insDet);
        }
    }

}
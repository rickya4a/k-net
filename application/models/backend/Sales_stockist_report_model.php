<?php
class Sales_stockist_report_model extends MY_Model {

	function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

	function getListGeneratedSales($x)
    {
        //$x['from'],$x['to'],$x['idstkk'],$x['bnsperiod'],$x['searchs']
        //$username = $this->session->userdata('stockist');
        $froms = date('Y/m/d', strtotime($x['from']));
        $tos = date('Y/m/d', strtotime($x['to']));

        /*if($x['searchs'] == "stock"){
            $usertype = "a.sc_dfno = '".$x['mainstk']."' and a.sctype = '".$x['sctype']."'";
        }*/

        $folderGets = explode('/', $x['bnsperiod']);
        $data['month'] = $folderGets[0];
        $data['year'] = $folderGets[1];
        $bonusperiod = $data['year']."-".$data['month']."-"."1";

        $slc = "select * from V_HILAL_SCO_SSR_LIST a
        		WHERE a.loccd = '$x[sc_dfno]'
        		AND a.sctype = '".$x['sctype']."'
        		AND a.bnsperiod='$bonusperiod'
        		AND a.flag_batch <> '0'
        		AND (a.batchno <> '' OR a.batchno is not null)
        		ORDER BY a.batchdt desc";
       //echo $slc;
       return $this->getRecordset($slc, null, $this->db2);
    }
	function getCurrentPeriod() // dipake
	{
		$qry = "SELECT a.currperiodSCO as lastperiod,
                DATEADD(month, 1, a.currperiodSCO) as nextperiod
                from klink_mlm2010.dbo.syspref a";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function getHeaderSsr($field, $value) {
		$qry = "SELECT top 1 a.trcd,
				       a.sc_dfno, b.fullnm as sc_dfno_name,
				       a.sc_co, c.fullnm as sc_co_name,
				       a.loccd, d.fullnm as loccd_name,
				       CONVERT(char(10), a.batchdt,126) as batchdt,
				       CONVERT(char(10), a.bnsperiod,126) as bnsperiod
				FROM sc_newtrh a
				LEFT OUTER JOIN mssc b ON (a.sc_dfno = b.loccd)
				LEFT OUTER JOIN mssc c ON (a.sc_co = c.loccd)
				LEFT OUTER JOIN mssc d ON (a.loccd = d.loccd)
				WHERE a.$field = '$value'";
		return $this->getRecordset($qry, null, $this->db2);
	}

	function getListSummaryTtp($field, $value) {
		$slc = "SELECT a.trcd, a.orderno, a.dfno, b.fullnm, a.tdp, a.tbv
				FROM sc_newtrh a
				LEFT OUTER JOIN msmemb b ON (a.dfno = b.dfno)
				WHERE a.$field = '$value'";
       //echo $slc;
       return $this->getRecordset($slc, null, $this->db2);
	}

	function getListSummaryProduct($field, $value) {
		$qry = "SELECT a.prdcd, b.prdnm, SUM(a.qtyord) as qtyord
				FROM sc_newtrd a
				INNER JOIN sc_newtrh c ON (a.trcd = c.trcd)
				LEFT OUTER JOIN msprd b ON (a.prdcd = b.prdcd)
				WHERE c.$field = '$value'
				GROUP BY a.prdcd, b.prdnm
				ORDER BY a.prdcd, b.prdnm";
		return $this->getRecordset($qry, null, $this->db2);
	}

	function getVoucherReportList($field, $value, $tipe_vch) {


		$nextS = "";
		if($tipe_vch == "P") {
			$nextS .= "SUBSTRING (a.VoucherNo,1,1) ='$tipe_vch' ";
		} else if($tipe_vch == "V") {
			$nextS .= "SUBSTRING (a.VoucherNo,1,1) = '$tipe_vch' ";
		} else if($tipe_vch == "VUMR") {
			$nextS .= "SUBSTRING (a.VoucherNo,1,4) = '$tipe_vch' ";
		} else {
			$nextS .= "(SUBSTRING (a.VoucherNo,1,3) = 'XPV' OR SUBSTRING (a.VoucherNo,1,3) = 'ZVO' OR SUBSTRING (a.VoucherNo,1,3) = 'XPP' OR SUBSTRING (a.VoucherNo,1,3) = 'XHD') ";
		}

		if($field == "VoucherNo") {
			if($tipe_vch == "P" || $tipe_vch == "XPV" || $tipe_vch == "ZVO" || $tipe_vch == "XHD") {
				$nextS .= "AND a.VoucherNo = '".$value."'";
			} else if($tipe_vch == "V") {
				$nextS .= "AND a.voucherkey = '".$value."'";
			}
		} else if($field == "DistributorCode") {
			$nextS .= "AND a.$field = '".$value."'";
			/*
			if($tipe_vch == "XPV") {
				$nextS .= "a.$field = '".$value."'";
			} else {
			  $nextS .= "AND a.$field = '".$value."'";
			}*/
		}

		$qry = "SELECT a.claimstatus, d.batchno, d.batchdt,
				a.DistributorCode, b.fullnm, a.voucherkey,
				a.VoucherNo as VoucherNo, a.vchtype,a.VoucherAmt,
				CONVERT(char(10), a.claim_date, 126) as tglklaim,
				CONVERT(char(10), a.ExpireDate,126) as ExpireDate,
				CONVERT(char(10), GETDATE(),126) as nowDate,
				CASE
					WHEN CONVERT(char(10), GETDATE(),126) > CONVERT(char(10), a.ExpireDate,126) THEN '1'
					ELSE '0' END AS status_expire,
				e.no_trx, e.createnm, CONVERT(char(10), e.createdt, 126) as tgldeposit,
				CASE
					WHEN a.loccd = 'K-NET' THEN 'K-NET MOBILE APPS' ELSE d.sc_dfno
				END AS loccd,
				CASE
					WHEN a.loccd = 'K-NET' THEN z.orderno ELSE c.trcd
				END AS trcd
			FROM tcvoucher a
			LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_paydet z ON (a.voucherkey = z.docno)
			LEFT OUTER JOIN msmemb b ON (a.DistributorCode = b.dfno)
			LEFT OUTER JOIN sc_newtrp c ON (a.VoucherNo = c.docno)
			LEFT OUTER JOIN sc_newtrh d ON (
				d.trcd = CASE WHEN c.trcd is null THEN '0'
				WHEN c.trcd is not null THEN c.trcd END
			)
			LEFT OUTER JOIN deposit_D e ON (a.voucherkey COLLATE SQL_Latin1_General_CP1_CI_AI = e.voucher_scan)
			WHERE $nextS";
		//echo $qry;
		/*$qry = "SELECT a.claimstatus, c.trcd, d.batchno, d.batchdt, a.loccd,
				       a.DistributorCode, b.fullnm, a.voucherkey, a.VoucherNo as VoucherNo,
				       a.vchtype,a.VoucherAmt,
				       CONVERT(char(10), a.claim_date, 126) as tglklaim,
				       CONVERT(char(10), a.ExpireDate,126) as ExpireDate,
				       CONVERT(char(10), GETDATE(),126) as nowDate,
				       CASE
				           WHEN CONVERT(char(10), GETDATE(),126) > CONVERT(char(10), a.ExpireDate,126) THEN '1'
				           ELSE '0'
				       END AS status_expire
				FROM tcvoucher a
				LEFT JOIN msmemb b ON (a.DistributorCode = b.dfno)
				LEFT JOIN sc_newtrp c ON (a.VoucherNo = c.docno)
				LEFT JOIN sc_newtrh d ON ( d.trcd =
					CASE
	                    WHEN c.trcd is null THEN '0'
	                    WHEN c.trcd is not null THEN c.trcd
	                END
				)
				WHERE $nextS";	*/
		//echo $qry;
		return $this->getRecordset($qry, null, $this->db2);
	}

	function extendExpiration($date, $vchno, $vchkey) {
		$query = "UPDATE klink_mlm2010.dbo.tcvoucher SET
							tcvoucher.ExpireDate = '$date'
						WHERE
							tcvoucher.VoucherNo = '$vchno'
						AND
							tcvoucher.voucherkey = '$vchkey'";
		$this->executeQuery($query, $this->setDB(2));
	}
}
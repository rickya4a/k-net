<?php
class Be_klaim_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
	}

    function getListingKlaim($idmember){
        $qry = "
                SELECT NoTrxV, VoucherNo, dfno, Fullnm, loccd, nmSTK, jmlPaket_Ladies, jmlPaket_Umum, ttbv, bnsperiod, Claim_Status
                FROM QTW_KLPromo_Oct17
                WHERE dfno = '".$idmember."'
                GROUP BY NoTrxV, VoucherNo, dfno, Fullnm, loccd, nmSTK, jmlPaket_Ladies, jmlPaket_Umum, ttbv,bnsperiod, Claim_Status";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }

    function getListingDetail($vc){
        $qry = "
                SELECT * FROM QTW_KLPromo_Oct17 where VoucherNo = '$vc';";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }
    function getFormDetail($vc){
        $qry = "
                SELECT * FROM TWA_KLPromo_Oct17_H where VoucherNo = '$vc';";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }
    function getProdukKlaim($bnsperiod,$idmembers){
        $qry = "
                SELECT *
                FROM TWA_KLPromo_Oct17_MH A
                LEFT JOIN TWA_KLPromo_Oct17_MD B
                ON A.bnsperiod=B.bnsperiod AND A.dfno=  B.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
                LEFT JOIN msprd c
                on b.prdcd=c.prdcd
                WHERE A.dfno = '$idmembers' AND a.bnsperiod ='$bnsperiod' ;

                ";
        //echo "query member ".$qry;
        $result = $this->getRecordset($qry,null,$this->db2);
        return $result;
    }
 }

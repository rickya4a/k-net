<?php
class Management_assetm extends MY_Model 
{
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
    
    function getCatInfo($param, $value) {
	 	$slc = "SELECT * FROM ma_cat_inv WHERE $param = '$value'";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
	 }
     
     function cekCatInAsset($id){
        $slc = "select count(a.id_cat) as isi 
                from ma_inventory a
                where a.status_inv = '0' and a.id_cat = '".$id."'
                group by a.id_cat";
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function getListCat(){
        $slc = "SELECT a.id_cat,a.cat_nm,a.remarks,a.status
                FROM ma_cat_inv a
                order by cat_nm";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function saveUpdateCatInv(){
        $data = $this->input->post(NULL, TRUE);
        $qry = "UPDATE ma_cat_inv SET cat_nm = '".$data['cat_nm']."', 
                remarks = '".$data['remarks']."', status = '".$data['status']."',
                updtnm = '".$this->username."'
		        WHERE id_cat = '".$data['id_cat']."'";
		//echo $qry;
        $query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function saveInputCatInv(){
        $y = $this->input->post(NULL, TRUE);
        $inp = "insert into ma_cat_inv(id_cat,cat_nm,status,remarks,createnm) 
                values('".$y['id_cat']."','".$y['cat_nm']."','".$y['status']."','".$y['remarks']."','".$this->username."')";
        
        $query = $this->db->query($inp);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function deleteInputCatInv($id){
        $qry = "DELETE FROM ma_cat_inv WHERE id_cat = '".$id."'";
		$query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function getListSupplier(){
        $slc = "SELECT * FROM ma_supplier order by nm_supplier";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function getSupplierInfo($param,$value){
        $slc = "SELECT * FROM ma_supplier WHERE $param = '$value'";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function deleteInputSupplier($id){
        $qry = "DELETE FROM ma_supplier WHERE id_supplier = '".$id."'";
		$query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
 
     function saveInputSupplier(){
        $y = $this->input->post(NULL, TRUE);
        $inp = "insert into ma_supplier(id_supplier,nm_supplier,company_nm,remarks,createnm,tel_hp_supplier) 
                values('".$y['id_supplier']."','".$y['nm_supplier']."','".$y['company']."','".$y['remarks']."','".$this->username."','".$y['tel_hp']."')";
        
        $query = $this->db->query($inp);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function saveUpdateSupplier(){
        $data = $this->input->post(NULL, TRUE);
        $qry = "UPDATE ma_supplier SET nm_supplier = '".$data['nm_supplier']."', 
                remarks = '".$data['remarks']."', company_nm = '".$data['company']."',
                updtnm = '".$this->username."'
		        WHERE id_supplier = '".$data['id_supplier']."'";
		//echo $qry;
        $query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function listSuppForInv(){
         $slc = "SELECT * FROM ma_supplier where status = '1' order by id_supplier";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function getInvInfo($param,$value){
        $slc = "SELECT a.id_inventory,a.nm_inventory,a.id_cat,a.value,a.qty,a.remarks,a.id_supplier,
                a.qty_redemp,a.qty_req,a.status_inv,b.id
                FROM ma_inventory a
                left join ma_inventory_history b on a.id_inventory = b.id_invent and a.status_inv = b.status
                WHERE $param = '$value'";
		//echo $slc;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function getListInv(){
       $slc = "SELECT a.id_inventory,a.nm_inventory,a.qty as qty_avail,a.value,a.remarks,a.status_inv,a.qty_req,a.qty_redemp,
                a.id_cat,a.id_supplier,b.cat_nm,c.nm_supplier
                    FROM ma_inventory a
                    inner join ma_cat_inv b on a.id_cat = b.id_cat COLLATE SQL_Latin1_General_CP1_CS_AS
                    inner join ma_supplier c on a.id_supplier = c.id_supplier COLLATE SQL_Latin1_General_CP1_CS_AS
                order by a.id_inventory";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function getListInv1(){
       $slc = "SELECT a.id_inventory,a.nm_inventory,a.qty as qty_avail,a.value,a.remarks,a.status_inv,a.qty_req,
                a.id_cat,a.id_supplier,b.cat_nm,c.nm_supplier
                    FROM ma_inventory a
                    inner join ma_cat_inv b on a.id_cat = b.id_cat COLLATE SQL_Latin1_General_CP1_CS_AS
                    inner join ma_supplier c on a.id_supplier = c.id_supplier COLLATE SQL_Latin1_General_CP1_CS_AS
                where a.status_inv = '0'
                order by a.id_inventory";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function saveInputInventory(){
       $y = $this->input->post(NULL, TRUE);
       if($y['status_invv'] == '0'){
            $qty = "qty";
       }else{
            $qty = "qty_req";
       }
        $insHistory = "insert into ma_inventory_history (id_invent,qty_req,createnm,values_inv) values('".$y['id_inventory']."',".$y['qtyReq'].",'".$this->username."',".$y['value'].")";
        $qry1 = $this->db->query($insHistory);
        
        //echo "<br>query ".$insHistory;
        
        $inp = "insert into ma_inventory(id_inventory,nm_inventory,id_cat,value,createnm,remarks,id_supplier,$qty,status_inv) 
                values('".$y['id_inventory']."','".$y['nm_inventory']."','".$y['cat_inv']."',".$y['value'].",'".$this->username."','".$y['remarks']."','".$y['list_supp']."',".$y['qtyReq'].",'".$y['status_invv']."')";
        //echo "<br>query ".$inp;
        $query = $this->db->query($inp);
        
        if(!$query && !$qry1){
            return 0;
        }else{
            return 1;
        }
     }
     
     function cekQtyAvail($idInventory){
        
        //print_r($data);
        $slc = "select a.qty,a.value,a.qty_redemp from ma_inventory a where a.id_inventory = '".$idInventory."'";
        $res = $this->getRecordset($slc, NULL, $this->db1);
	    //echo "qry ".$slc; 
		return $res;
     }
     
     function saveUpdateInventory($qtyAvail){
        $updtdt = date("Y-m-d  H:i:s");
        $data = $this->input->post(NULL, TRUE);
        
        if($data['status_invv'] == '0'){
            $qtyAvails = $qtyAvail + $data['qtyReq'];
            $qtyy = ",qty = ".$qtyAvails.",qty_req = 0";
        }else{
            $qtyy = ",qty_req = ".$data['qtyReq']."";
        }
        
        $updtHistory = "update ma_inventory_history set status = '".$data['status_invv']."',updtdt = '".$updtdt."',updtnm = '".$this->username."',updtstat = '".$data['status_invv']."' 
                        where id = '".$data['idToHistory']."'";
        $qryUpdateHistory = $this->db->query($updtHistory);
        
        $qry = "UPDATE ma_inventory SET nm_inventory = '".$data['nm_inventory']."', 
                id_cat = '".$data['cat_inv']."', id_supplier = '".$data['list_supp']."',
                value = ".$data['value'].",remarks = '".$data['remarks']."',
                updtnm = '".$this->username."',upd_dt = '".$updtdt."',status_inv = ".$data['status_invv']." $qtyy
		        WHERE id_inventory = '".$data['id_inventory']."'";
		//echo $qry;
        $query = $this->db->query($qry);
        
        if(!$query && !$qryUpdateHistory){
            return 0;
        }else{
            return 1;
        }
     }
     
     function deleteInventory($id){
        $qry = "DELETE FROM ma_inventory WHERE id_inventory = '".$id."'";
		$query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function getListDiv(){
        $slc = "select distinct department as divisi from klink_mlm2010.dbo.users order by department";
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function saveInputEmployee(){
        $y = $this->input->post(NULL, TRUE);
        $inp = "insert into ma_employee(id_employee,employee_nm,divisi,createnm,status) 
                values('".$y['id_employee']."','".$y['nm_employee']."','".$y['divisi']."','".$this->username."','".$y['status']."')";
        
        $query = $this->db->query($inp);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
     function getEmplInfo($param,$value){
        $slc = "SELECT * FROM ma_employee WHERE $param = '".$value."'";
        $res = $this->getRecordset($slc, NULL, $this->db1);
	    //echo  $slc;
		return $res;
     }
     
     function getListEmp(){
        $slc = "SELECT * from ma_employee";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
     }
     
     function saveUpdateEmployee(){
        $updtdt = date("Y-m-d  H:i:s");
        $data = $this->input->post(NULL, TRUE);
        $qry = "UPDATE ma_employee SET employee_nm = '".$data['nm_employee']."', 
                divisi = '".$data['divisi']."', status = '".$data['status']."',
                updatenm = '".$this->username."',updatedt = '".$updtdt."'
		        WHERE id_employee = '".$data['id_employee']."'";
		//echo $qry;
        $query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
     }
     
    function deleteEmployee($id){
        $qry = "DELETE FROM ma_employee WHERE id_employee = '".$id."'";
		$query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
    }
    
    function updtQtyAvails($qtyReq,$qtyAvail,$idInvent,$qtyredemp){
        $qtyRedempUdpt = $qtyReq + $qtyredemp;
        $qtyAvailUpdt = $qtyAvail - $qtyReq;
        if($qtyAvailUpdt == '0'){
            $status = ",status_inv = '4'";
        }else{
            $status ="";
        }
        $updt = "update ma_inventory set qty = ".$qtyAvailUpdt.",qty_redemp = ".$qtyRedempUdpt." $status
                where id_inventory = '".$idInvent."'";
        $query = $this->db->query($updt);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
    }
     
    function saveInputMutasi($qtyAvail,$amount,$qty_redemp){
       $y = $this->input->post(NULL, TRUE);
       $updtQtyAvail = $this->updtQtyAvails($y['qtyReq'],$qtyAvail,$y['id_inventory'],$qty_redemp);
        
        $inp = "insert into ma_mutasi(id_employee,id_inventory,qty,amount,createnm,remarks,status) 
                values('".$y['id_employee']."','".$y['id_inventory']."',".$y['qtyReq'].",".$amount.",'".$this->username."','".$y['remarks']."','".$y['status_invv']."')";
        //echo "query ".$inp;
        $query = $this->db->query($inp);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
    }
    
    function getListMutasi(){
        $slc = "SELECT a.id,a.id_employee,a.id_inventory,a.qty,a.amount,a.status,
                b.employee_nm,b.divisi,c.nm_inventory
                    FROM ma_mutasi a
                    inner join ma_employee b on a.id_employee = b.id_employee COLLATE SQL_Latin1_General_CP1_CS_AS
                    inner join ma_inventory c on a.id_inventory = c.id_inventory COLLATE SQL_Latin1_General_CP1_CS_AS
               WHERE a.flag_delete = '0'
               order by a.id_inventory";
		//echo $qry;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
    }
    
    function getMutasiInfo($param,$value){
        $slc = "SELECT a.id,a.id_employee,a.id_inventory,a.qty as qtyReq,a.amount,
                a.status,b.employee_nm,b.divisi,c.nm_inventory,c.qty as qtyAvails,a.remarks
                FROM ma_mutasi a
                    inner join ma_employee b on a.id_employee = b.id_employee COLLATE SQL_Latin1_General_CP1_CS_AS
                    inner join ma_inventory c on a.id_inventory = c.id_inventory COLLATE SQL_Latin1_General_CP1_CS_AS
                where $param = ".$value."";
		//echo $slc;
        $res = $this->getRecordset($slc, NULL, $this->db1);
	
		return $res;
    }
    
    function saveUpdateMutasi($qty,$value,$qtyRedem,$qtyPinjam){
        $updtdt = date("Y-m-d  H:i:s");
        $data = $this->input->post(NULL, TRUE);
        
        $qtyAvailss = $qty + $data['qtyReq'];
        $qtyRedemps = $qtyRedem - $data['qtyReq'];
        $qtyPinjams = $qtyPinjam - $data['qtyReq'];
        if($data['status_invv'] == '1'){
            
            $updInventory = "update ma_inventory set qty = ".$qtyAvailss.",
                            qty_redemp = ".$qtyRedemps." ,status_inv = '0'
                            where id_inventory = '".$data['id_invent']."'";
            $query1 = $this->db->query($updInventory);
            $tambahan = ",qty = ".$qtyPinjams."";
        }/*elseif($data['status_invv'] == '0'){
            $tambahan = ",qty = ".$data['qtyReq']."";
        }*/else{
            $tambahan = "";
        }
        $qry = "UPDATE ma_mutasi SET status = '".$data['status_invv']."', 
                updatenm = '".$this->username."',updatedt = '".$updtdt."',remarks = '".$data['remarks']."' $tambahan
		        WHERE id = '".$data['id_muts']."'";
		//echo $qry;
        $query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
    }
    
    function deleteMutasi($id){
        $qry = "update ma_mutasi set flag_delete = '1' WHERE id = '".$id."'";
		//echo $qry;
        $query = $this->db->query($qry);
        
        if(!$query){
            return 0;
        }else{
            return 1;
        }
    }
    
    function getListMutasiByDate($dt){
        if($dt['searchBy'] == ''){
            $wheree = "(CONVERT(VARCHAR(10), a.createdt, 111) 
                between '".$dt['mut_froms']."' and '".$dt['mut_tos']."')";
        }else{
            $wheree = "a.status = '".$dt['searchBy']."' and (CONVERT(VARCHAR(10), a.createdt, 111) 
                between '".$dt['mut_froms']."' and '".$dt['mut_tos']."')";
        }
        $slc = "select a.id_employee,a.id_inventory,a.qty,a.amount,a.status,
                a.remarks,b.employee_nm,b.divisi,c.nm_inventory
                from ma_mutasi a
                    inner join ma_employee b on (a.id_employee = b.id_employee COLLATE SQL_Latin1_General_CP1_CS_AS)
                    inner join ma_inventory c on (a.id_inventory = c.id_inventory COLLATE SQL_Latin1_General_CP1_CS_AS)
                where $wheree
                order by a.id_employee,b.divisi";
        //echo "ss ".$slc;
        $res = $this->getRecordset($slc, null, $this->db1);
		return $res;
    }
}
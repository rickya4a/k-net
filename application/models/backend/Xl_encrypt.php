<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Xl_encrypt extends MY_Model {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	public function getEncryptTime() {
		$qry = "SELECT MAX(a.encrypt_time) AS latest
				FROM db_ecommerce.dbo.ppob_xl_encrypt a";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}

	public function getRecordEncryption($data) {
		$qry = "SELECT COUNT(a.encrypt_time) AS jumlah_record
				FROM db_ecommerce.dbo.ppob_xl_encrypt a 
				WHERE a.encrypt_time LIKE '$data%'";
		$arr = $this->getRecordset($qry, NULL,$this->db1);
		return $arr;
	}

	public function chekLastEncrypt($from, $to) {
		$qry = "SELECT COUNT(a.encrypt_date) AS jumlah_record, a.encrypt_date
				FROM db_ecommerce.dbo.ppob_xl_encrypt a 
				WHERE a.encrypt_date BETWEEN '$from' AND '$to'
				GROUP BY a.encrypt_date";
		$arr = $this->getRecordset($qry, NULL, $this->db1);
		return $arr;
	}
}

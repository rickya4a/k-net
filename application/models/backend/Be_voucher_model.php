<?php
class Be_voucher_model extends MY_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();

	}
	function checkID1($data) {
		/*
		 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm ";
		 */
		$qry = "SELECT * FROM [dbo].[mssc] WHERE loccd = '$data' ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}
	function getVch($vch)
	{
		$kategori= strtolower($vch[0]);
		$by='a.VoucherKey';
		if($kategori=='p'){
			$by='a.VoucherNo';
		}

		$this->db->select('a.*, b.fullnm, b.dfno,DATEADD(MONTH, DATEDIFF(MONTH, 0,  GETDATE() ), 0)   as XD, a.loccd as stokis, a.updatenm as stokis2,DATEADD(MONTH, DATEDIFF(MONTH, 0, a.ExpireDate ), 0) as YEY ');
		$this->db->where($by,$vch);
		//$this->db->where($by, strtolower($vch));
		$this->db->join('klink_mlm2010.dbo.msmemb b', 'a.DistributorCode = b.dfno','left');

		return $this->db->get('klink_mlm2010.dbo.tcvoucher a');

		//echo $this->db->last_query();
		//echo $this->db->get_compiled_select();
	
	}
	function saveScan($id,$we)
	{
		$kategori= strtolower($id[0]);
		$by='(VoucherKey)';
		if($kategori=='p'){
			$by='(VoucherNo)';
		}

		$this->db->trans_start();
		$this->db->where($by,$id);
		$this->db->set('updatedt', date("Y-m-d h:i:s"));
		$this->db->set('updatenm', $this->username);
		$this->db->set('claim_date', date("Y-m-d h:i:s"));
		$this->db->set('loccd', 'BID06');
		$this->db->set('claimstatus', '1');
		$this->db->set('status', '1');
		$this->db->update('klink_mlm2010.dbo.tcvoucher');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			//if something went wrong, rollback everything
			$this->db->trans_rollback();
			return FALSE;
		} else {
			//if everything went right, delete the data from the database
			$this->db->trans_commit();
			return TRUE;
		}
	}
	/*------------------
	 * PRODUCT CATEGORY
	 --------------------*/
	function saveInputEtiket() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_etiket (
				nama,
				lokasi,
				max_online,
				max_offline,
				event_date,
				pembicara,
				exp_date_online,
				exp_date_offline,
				price_online,
				price_offline,
				status,
				remark,
				createdt,
				createnm, earlybird_date, price_earlybird_online

				)
		        VALUES ('$data[nm_event]', '$data[loc_event]', '$data[max_online]',
		              	'$data[max_offline]', '$data[event_date]',
					  	'$data[pembicara]','$data[exp_date_online]','$data[exp_date_offline]',
					  	'$data[price_online]','$data[price_offline]','0',
					  	'$data[remark]',GETDATE(),
		                '".$this->username."', '$data[bird_date]' ,'$data[price_bird]')";
		$query = $this->db->query($qry);
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $query;
	}
	
	function saveInputEtiketOLD20180130() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_etiket (
				nama,
				lokasi,
				max_online,
				act_online,
				max_offline,
				act_offline,
				total,
				act_total,
				event_date,
				pembicara,
				exp_date_online,
				exp_date_offline,
				price_online,
				price_offline,
				status,
				remark,
				createdt,
				createnm, earlybird_date, price_earlybird_online

				)
		        VALUES ('$data[nm_event]', '$data[loc_event]', '$data[max_online]',
		              	'$data[act_online]','$data[max_offline]','$data[act_offline]',
					  	'$data[total]','$data[act_total]','$data[event_date]',
					  	'$data[pembicara]','$data[exp_date_online]','$data[exp_date_offline]',
					  	'$data[price_online]','$data[price_offline]','0',
					  	'$data[remark]',GETDATE(),


		               '".$this->username."', '$data[bird_date]' ,'$data[price_bird]')";
		$query = $this->db->query($qry);
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $query;
	}

	function getlistAllEtiket() {
//		$qry = "SELECT
//				 *
//				FROM
//				  master_etiket
//				ORDER BY  createdt DESC
//				";
//		$res = $this->getRecordset($qry, NULL, $this->db);

		$result = array();
		$this->db->select('*')
			->from('master_etiket')
			->order_by('createdt','desc');

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;


//		return $res;
	}

	function getData($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('master_etiket');

		$q = $this->db->get();
		//echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}
	function getData2($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('trx_etiket');

		$q = $this->db->get();
//		echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;
	}

	function updateEtiket($id,$save){

		$this->db->trans_start();
		$this->db->where('id',$id);
		$this->db->update('master_etiket',$save);
		$this->db->trans_complete();
//		echo $this->db->last_query();
		return true;
	}


	function klaimEtiket($id,$save){

		$this->db->trans_start();
		$this->db->where('id',$id);
		$this->db->update('trx_etiket',$save);
		$this->db->trans_complete();
//		echo $this->db->last_query();
		return true;
	}





	function getListAllTRXTiket() {
//		$qry = "SELECT
//				 *
//				FROM
//				  master_etiket
//				ORDER BY  createdt DESC
//				";
//		$res = $this->getRecordset($qry, NULL, $this->db);

		$result = array();
		$this->db->select('*, a.status as stats,a.id as idtrx, a.createdt as tanggalbeli, a.id as idtiket')
			->from('trx_etiket a')
			->join('master_etiket b','a.id_etiket = b.id')
			->order_by('a.id','DESC')
			->where('a.status','0')
		;

		$q = $this->db->get();
//		echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;


//		return $res;
	}





	function getListProductCat($param, $value) {
		$qry = "SELECT * FROM master_prd_cat WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("No result", 1);
		}
		return $res;
	}

	function saveInputProductCat() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_prd_cat (cat_id, cat_desc, status, 
		               remarks, createnm)
		        VALUES ('$data[cat_id]', '$data[cat_desc]', '$data[status]', 
		              '$data[remarks]', '".$this->username."')";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Save Product Category failed..!!", 1);
		}
		return $query;
	}

	function getListAllProductCat() {
		$qry = "SELECT * FROM master_prd_cat";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data user group is empty..!", 1);
		}
		return $res;
	}

	function saveUpdateProductCat() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "UPDATE master_prd_cat SET cat_desc = '$data[cat_desc]', 
		               remarks = '$data[remarks]', status = '$data[status]'
		        WHERE cat_id = '$data[cat_id]'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Update Product Category failed..!!", 1);
		}
		return $query;
	}

	function deletetiket($id) {
		$qry = "UPDATE trx_etiket SET status = '1'
		        WHERE id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Product Category failed..!!", 1);
		}
		return $query;
	}

	/*------------------
	 * PRICE CODE
	 --------------------*/
	function getListAllPriceCode() {
		$qry = "SELECT * FROM master_prd_pricecode";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("Data price code is empty..!", 1);
		}
		return $res;
	}

	function getListPriceCode($param, $value) {
		$qry = "SELECT * FROM master_prd_pricecode WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("No result", 1);
		}
		return $res;
	}

	function savePriceCode($data) {
		//$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO master_prd_pricecode (pricecode, pricecode_desc, status, 
		               country_id, hq_id, branch_id, remarks, createnm)
		        VALUES ('$data[pricecode]', '$data[pricecode_desc]', '$data[status]',
		                '$data[country_id]', '$data[hq_id]', '$data[branch_id]',
		              '$data[remarks]', '".$this->username."')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Price Code DB Commerce failed..!!", 1);
		}
		return $query;
	}

	function savePriceCodeKlinkMlm($data) {
		$qry = "INSERT INTO pricecode (code, description, status,
		               PT_SVRID, notes, createnm, createdt)
		        VALUES ('$data[pricecode]', '$data[pricecode_desc]', '$data[status]',
		                '$data[country_id]', '$data[remarks]', '".$this->username."', '".$this->dateTime."')";

		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Price Code Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function updatePriceCode($data) {
		//$data = $this->input->post(NULL, TRUE);
		$qry = "update master_prd_pricecode SET pricecode_desc = '$data[pricecode_desc]', status = '$data[status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]', 
		             remarks = '$data[remarks]'
		        WHERE pricecode = '$data[pricecode]'";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Update Price Code DB Commerce failed..!!", 1);
		}
		return $query;
	}

	function updatePriceCodeKlinkMlm($data) {
		$qry = "update pricecode SET description = '$data[pricecode_desc]', status = '$data[status]',
		             PT_SVRID = '$data[country_id]', notes = '$data[remarks]'
		        WHERE code = '$data[pricecode]'";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Update Price Code KlinkMlm failed..!!", 1);
		}
		return $query;
	}

	function deletePriceCode($id) {
		$qry = "DELETE FROM master_prd_pricecode WHERE pricecode = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Price Code failed..!!", 1);
		}
		return $query;
	}

	/*------------------
     PRODUCT
    --------------------*/
	function getlistAllProduct($noexception = true) {
		$qry = "SELECT
				  dbo.master_prd_cat_inv.cat_inv_id,
				  dbo.master_prd_cat_inv.cat_id,
				  dbo.master_prd_cat.cat_desc,
				  dbo.master_prd_cat_inv.cat_inv_desc,
				  dbo.master_prd_cat_inv.is_discontinue
				FROM
				  dbo.master_prd_cat_inv
				INNER JOIN dbo.master_prd_cat ON (dbo.master_prd_cat_inv.cat_id = dbo.master_prd_cat.cat_id)";
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($noexception) {
			if($res == null) {
				throw new Exception("Data Product is empty..!", 1);
			}
		}
		return $res;
	}

	function getListProduct($param, $value) {
		$qry = "SELECT * FROM master_prd_cat_inv WHERE $param = '$value'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			throw new Exception("gA ADA ISIIII", 1);
		}
		return $res;
	}



	function inputProduct($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		if($parent_cat_inv_id == "") {
			$parent_cat_inv_id = $data['cat_inv_id'];
		}
		$qry = "INSERT INTO master_prd_cat_inv (cat_inv_id, cat_id, parent_cat_inv_id, 
		               cat_inv_desc, inv_type, status, bo_inv_status, bo_status,
					   sc_status, sub_status, ms_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, img_name, weight,
					   is_starterkit, is_discontinue, is_charge_ship, flag_is_do)
		        VALUES ('$data[cat_inv_id]', '$data[cat_id]', '$parent_cat_inv_id',
		                '$data[cat_inv_desc]', '$data[inv_type]', '$data[status]',
		              '$data[bo_inv_status]', '$data[bo_status]', '$data[sc_status]',
		              '$data[sub_status]', '$data[ms_status]', '$data[ecomm_status]',
					  '$data[web_status]', '$data[country_id]', '$data[hq_id]',
					  '$data[branch_id]', '".$this->username."', '".$_FILES["myfile"]["name"]."', ".$data['weight'].",
					  '$data[is_starterkit]', '$data[is_discontinue]', '$data[is_charge_ship]', '$data[flag_is_do]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function inputProductMapforMobile($data) {

		$parent_cat_inv_id = $data['parent_cat_inv_id'];
		if($parent_cat_inv_id == "") {
			$parent_cat_inv_id = $data['cat_inv_id'];
		}
		$qry = "INSERT INTO master_prd_cat_inv_mobile (cat_inv_id, cat_id, parent_cat_inv_id, 
		               cat_inv_desc, inv_type, status, bo_inv_status, bo_status,
					   sc_status, sub_status, ms_status, ecomm_status, web_status,
					   country_id, hq_id, branch_id, createnm, img_name, weight,
					   is_starterkit, is_discontinue, is_charge_ship, flag_is_do)
		        VALUES ('$data[cat_inv_id]', '$data[cat_id]', '$parent_cat_inv_id',
		                '$data[cat_inv_desc]', '$data[inv_type]', '$data[status]',
		              '$data[bo_inv_status]', '$data[bo_status]', '$data[sc_status]',
		              '$data[sub_status]', '$data[ms_status]', '$data[ecomm_status]',
					  '$data[web_status]', '$data[country_id]', '$data[hq_id]',
					  '$data[branch_id]', '".$this->username."', '".$_FILES["myfile"]["name"]."', ".$data['weight'].",
					  '$data[is_starterkit]', '$data[is_discontinue]', '$data[is_charge_ship]', '$data[flag_is_do]')";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			throw new Exception("Save Product DB Ecommerce failed..!!", 1);
		}
		return $query;
	}

	function inputProductKlinkMlm($data) {
		$qry = "INSERT INTO msprd (prdcd, prdnm, description, category, status, webstatus, scstatus, PT_SVRID, glposting_cd,
		               createnm, createdt)
	 	        VALUES ('$data[cat_inv_id]', '$data[cat_inv_desc]', '$data[cat_inv_desc]', '$data[cat_id]', '$data[status]',
				   '$data[web_status]', '$data[sc_status]', 'ID', 'GO', '".$this->username."', '".$this->dateTime."')";
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Product DB Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function updateProduct($data) {
		//$data = $this->input->post(NULL, TRUE);
		$ss = "";
		if($_FILES["myfile"]["name"] != "") {
			$ss .= ", img_name = '".$_FILES["myfile"]["name"]."' ";
		}
		$qry = "UPDATE master_prd_cat_inv SET cat_id = '$data[cat_id]', parent_cat_inv_id = '$data[parent_cat_inv_id]',
		             cat_inv_desc = '$data[cat_inv_desc]', inv_type = '$data[inv_type]', status = '$data[status]', bo_inv_status = '$data[bo_inv_status]',
		             bo_status = '$data[bo_status]', sc_status = '$data[sc_status]', sub_status = '$data[sub_status]', ms_status = '$data[ms_status]',
		             ecomm_status = '$data[ecomm_status]', web_status = '$data[web_status]',
		             country_id = '$data[country_id]', hq_id = '$data[hq_id]', branch_id = '$data[branch_id]', weight = ".$data['weight'].",
		             is_starterkit = '$data[is_starterkit]', is_discontinue = '$data[is_discontinue]', is_charge_ship = '$data[is_charge_ship]', flag_is_do = ".$data['flag_is_do']."
		             $ss
		        WHERE cat_inv_id = '$data[cat_inv_id]'";
		//echo $qry;
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Update Product failed..!!", 1);
		}
		return $query;
	}

	function updateProductKlinkMlm($data) {
		$qry = "UPDATE msprd SET prdnm = '$data[cat_inv_desc]', description = '$data[cat_inv_desc]',
	 		             category = '$data[cat_id]', status = '$data[status]', webstatus = '$data[web_status]', 
	 		             scstatus = '$data[sc_status]', PT_SVRID = 'ID', glposting_cd = 'GO',
		               updatenm = '".$this->username."', updatedt = '".$this->dateTime."'
		            WHERE prdcd = '$data[cat_inv_id]'";
		$query = $this->executeQuery($qry, $this->setDB(2));
		if(!$query) {
			throw new Exception("Save Product DB Klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function deleteProduct($id) {
		$qry = "DELETE FROM master_prd_cat_inv WHERE cat_inv_id = '$id'";
		$query = $this->db->query($qry);
		if(!$query) {
			throw new Exception("Delete Product failed..!!", 1);
		}
		return $query;
	}

	/*---------------------
     * PRODUCT PRICE
     -----------------------*/
	function showProductPriceCode($data) {
		$qry = "SELECT
				  dbo.master_prd_pricetab.pricecode,
				  dbo.master_prd_pricecode.pricecode_desc,
				  dbo.master_prd_pricetab.cat_inv_id,
				  dbo.master_prd_pricetab.cp,
				  dbo.master_prd_pricetab.dp,
				  dbo.master_prd_pricetab.bv,
				  dbo.master_prd_pricetab.tax
				FROM
				  dbo.master_prd_pricetab
				  INNER JOIN dbo.master_prd_pricecode ON (dbo.master_prd_pricetab.pricecode = dbo.master_prd_pricecode.pricecode)
				WHERE
				  (dbo.master_prd_pricetab.cat_inv_id = '$data[cat_inv_id]') AND 
				  (dbo.master_prd_pricetab.country_id = '$data[country_id]') AND 
				  (dbo.master_prd_pricetab.hq_id = '$data[hq_id]') AND 
				  (dbo.master_prd_pricetab.branch_id = '$data[branch_id]')";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);
		return $res;
	}

	function saveProductPrice($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		for($i=0; $i < $jum; $i++) {
			$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('$data[month]', '$data[year]', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$data[cat_inv_id]', '".$data['pricecode'][$i]."',
				              ".$data['cp'][$i].", ".$data['dp'][$i].", ".$data['bv'][$i].", '".$this->username."')";
			$query = $this->executeQuery($qry, $this->setDB(1));
			if(!$query) {
				$err++;
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB-ECOMMERCE failed..!!", 1);
		}
		return $query;
	}

	function saveproductPriceKlinkMLM($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;
		for($i=0; $i < $jum; $i++) {
			$check = "SELECT pricecode FROM pricetab WHERE pricecode = '".$data['pricecode'][$i]."' AND prdcd = '$data[cat_inv_id]'";
			$check2 = $this->getRecordset($check, NULL, $this->db2);

			if($check2 == null) {
				$qry = "INSERT INTO pricetab (prdcd, pricecode, cp, dp, bv, pv, createnm)
					               
					        VALUES ('$data[cat_inv_id]', '".$data['pricecode'][$i]."',
					              ".$data['cp'][$i].", ".$data['dp'][$i].", ".$data['bv'][$i].", ".$data['bv'][$i].", '".$this->username."')";
				$query = $this->executeQuery($qry, $this->setDB(2));
				if(!$query) {
					$err++;
				}
			} else {
				$redundant++;
				$prd = "Product $data[cat_inv_id] with pricecode ".$data['pricecode'][$i]." already exist in DB KlinkMLM";
				break;

			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB klinkmlm failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;
	}

	function deleteProductPrice($data) {
		//$jum = count($data['pricecode']);
		//for($i=0; $i < $jum; $i++) {
		$qry = "DELETE FROM master_prd_pricetab
		  	          WHERE cat_inv_id = '".$data['cat_inv_id']."' AND country_id = '".$data['country_id']."'
		  	          AND hq_id = '".$data['hq_id']."' AND branch_id = '".$data['branch_id']."'
		  	          AND period_month = ".$data['month']." AND period_year = ".$data['year']."";
		//echo $qry;
		$query = $this->executeQuery($qry, $this->setDB(1));
		//}
		if(!$query) {
			throw new Exception("Delete Price Code failed..!!", 1);
		}
		return $query;
	}

	function deleteProductPriceKlinkMlm($data) {
		$jum = count($data['pricecode']);
		$err = 0;
		for($i=0; $i < $jum; $i++) {
			$qry = "DELETE FROM pricetab WHERE prdcd = '$data[cat_inv_id]' AND pricecode = '".$data['pricecode'][$i]."'";
			$query = $this->executeQuery($qry, $this->setDB(2));
			if(!$query) {
				$err++;
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product DB klinkmlm failed..!!", 1);
		}
		return $query;
	}

	function getProductClaim($data) {
		$qry = "SELECT a.invoiceno, a.remark,
				CONVERT(VARCHAR(10), a.invoicedt, 103) AS invoicedt, 
				a.tdp, a.usernya
				FROM KL_TEMPTRANS a 
				where a.remark like '$data[name]%' and
				(a.registerdt>='$data[claim_from]' and a.registerdt <= '$data[claim_to]')
				order by a.invoicedt";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			throw new Exception(setErrorMessage(), 1);
		}
		return $res;
	}

	function getProductClaimDetail($id) {
		$qry = "SELECT b.prdcd, b.prdnm, b.qtyord,
			     b.tdp, b.qtyord * b.tdp as totaldp  
				FROM QKL_TRANS b 
				WHERE b.invoiceno = '$id'";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db2);
		if($res == null) {
			throw new Exception(setErrorMessage(), 1);
		}
		return $res;
	}

	/*-----------------------
     * PRODUCT BUNDLING
     * ---------------------*/
	function getListAllProductBundling() {
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Dion a
                WHERE a.prdcdcat = '13' order by prdnm";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListProductBundling($param, $id) {

	}

	function getListPrdBundlingHeader($id) {
		$qry = "select a.prdcd,
                    a.prdnm, 
                    a.prdcdcat, 
                    a.prdnmcatnm,
                    a.img_url, 
                    a.price_w, 
                    a.price_e,
                    a.price_cw, 
                    a.price_ce, 
                    a.bv,
                    a.weight
                from V_Ecomm_PriceList_Dion a
                WHERE a.prdcd = '$id'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListPrdBundlingDetail($id) {
		$qry = "select * from V_Ecomm_PriceList_Prd_Bundling a WHERE a.prdcd_parent = '$id'";
		$res = $this->getRecordset($qry, null, $this->db1);
		return $res;
	}

	function getListPrdBundlingDetailKlinkMlm($id) {
		$qry = "select * FROM DION_prdbundling_detail a  WHERE a.prdcd_parent = '$id'";
		$res = $this->getRecordset($qry, null, $this->db2);
		return $res;
	}

	function saveInputProductBundling($data) {

	}

	function saveInputProductBundlingDetail($data) {
		$jum = count($data['productcode']);
		$err = 0;
		$redundant = 0;
		$prd = null;
		$query = null;

		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}

		$del = "DELETE FROM master_prd_bundling WHERE cat_inv_id_parent = '".$prdcd."'";
		$delQuery = $this->executeQuery($del, $this->setDB(1));

		for($i=0; $i < $jum; $i++) {
			if($data['productcode'][$i] != "" || $data['productcode'][$i] != " ") {
				$qry = "INSERT INTO master_prd_bundling (cat_inv_id_child, cat_inv_id_parent, cat_desc, qty, status, createnm, createdt)
					    VALUES ('".$data['productcode'][$i]."', '".$prdcd."', '".$data['productname'][$i]."', ".$data['qty_real'][$i].",
					              '1', '".$this->username."', '".$this->dateTime."')";
				$query = $this->executeQuery($qry, $this->setDB(1));
				if(!$query) {
					$err++;

				}
			}
		}
		if($err > 0) {
			throw new Exception("Save Price Product Bundling failed..!!", 1);
		}

		if($redundant > 0) {
			throw new Exception($prd, 1);
		}
		return $query;


	}

	function saveInputProductBundlingPricetab($data) {
		$err = 0;
		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}
		$check = "SELECT pricecode FROM master_prd_pricetab WHERE cat_inv_id = '".$prdcd."'";
		$check2 = $this->getRecordset($check, NULL, $this->db1);
		if($check2 != null) {
			$delQry = "DELETE FROM master_prd_pricetab WHERE cat_inv_id = '".$prdcd."'";
			$del = $this->executeQuery($delQry, $this->setDB(1));
		}

		$qry = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$prdcd', '12W3',
				              ".$data['cust_west_price_real'].", ".$data['west_price_real'].", 
				              ".$data['bv_real'].", '".$this->username."')";
		$query = $this->executeQuery($qry, $this->setDB(1));
		if(!$query) {
			$err++;
		}

		$qry2 = "INSERT INTO master_prd_pricetab (period_month, period_year,
				               country_id, hq_id, branch_id, cat_inv_id, pricecode, cp, dp,
							   bv, createnm)
				        VALUES ('".date('m')."', '".date('Y')."', '$data[country_id]',
				                '$data[hq_id]', '$data[branch_id]', '$prdcd', '12E3',
				              ".$data['cust_east_price_real'].", ".$data['east_price_real'].", 
				              ".$data['bv_real'].", '".$this->username."')";
		$query2 = $this->executeQuery($qry2, $this->setDB(1));
		if(!$query2) {
			$err++;
		}

		if($err > 0) {
			throw new Exception("Input pricetab product $data[prdid] failed..", 1);
		}
		return $err;



	}

	function saveInputProductBundlingDetailKlink() {


	}

	function saveInputProductBundlingPricetabKlink($data) {

	}

	function inputProductBundlingKlinkMLM($data) {
		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}
		$SP_HILAL_insertGrpPrdHDR = "EXEC SP_HILAL_insertGrpPrdHDR
                '".$prdcd."',
                '".$data['cat_desc']."',
                '".$data['cat_desc']."',
                '".$this->username."',
                '".$this->username."',
                ".$data['west_price_real'].",
                ".$data['cust_west_price_real'].",
                ".$data['bv_real'].",
                '',
                '13'";
		//echo $qry;
		//echo "<br />";
		$query2 = $this->executeQuery($SP_HILAL_insertGrpPrdHDR, $this->setDB(2));


		$i = 0;
		for($i = 0; $i < count($data['productcode']); $i++)
		{
			if($data['productcode'][$i] != "")
			{
				$det = "EXEC SP_HILAL_insertGrpPrdDET
                            '".$this->username."',
                            '".$this->username."',
                            '',
                            '".$data['productcode'][$i]."',
                            '".$data['productname'][$i]."',
                            '".$prdcd."',
                            ".$data['west_real'][$i].",
                            ".$data['qty_real'][$i].",
                            ".$data['west_real'][$i].",
                            ".$data['tbv_real'][$i].",
                            '13'";
				//echo $det;
				//echo "<br />";
				$det2 = $this->executeQuery($det, $this->setDB(2));


			}

		}


		$price_tab = "EXEC SP_HILAL_insertPrdPricetab
                '".$prdcd."',
                '".$data['cat_desc']."',
                '".$data['cat_desc']."',
                '".$this->username."',
                '".$this->username."',
                ".$data['west_price_real'].",
                ".$data['cust_west_price_real'].", 
                ".$data['bv_real'].",
                '12W3',
                '13',
                ".$data['east_price_real'].",
                ".$data['cust_east_price_real'].",
                 
                '12E3'";
		//".$data['bv_real'].",
		//echo $price_tab;
		//echo "<br />";
		$price_tab2 = $this->executeQuery($price_tab, $this->setDB(2));
		//$detprice_tab2 = $this->executeQuery($price_tab2, $this->setDB(2));

	}

	function deleteProductBundlingKlinkMLM($data) {
		$srvReturn = false;
		//DELETE table newera_PRDDET
		/*$delnewera_PRDDET = "DELETE FROM newera_PRDDET WHERE prdcdCat = '".$data['prdid']."'";
     $stt_newera_PRDDET = $this->executeQuery($delnewera_PRDDET, $this->setDB(2));
     if($stt_newera_PRDDET > 0) {
           //DELETE table pricetab
           $delnewera_pricetab = "DELETE FROM pricetab WHERE prdcd = '".$data['prdid']."'";
           $stt_pricetab = $this->executeQuery($delnewera_pricetab, $this->setDB(2));
         if($stt_pricetab > 0) {
               //DELETE msprd
               $delnewera_msprd = "DELETE FROM msprd WHERE prdcd = '".$data['prdid']."'";
               $stt_msprd = $this->executeQuery($delnewera_msprd, $this->setDB(2));
             if($stt_msprd > 0) {
                   //DELETE newera_PRDCAT
                   $delnewera_PRDCAT = "DELETE FROM newera_PRDCAT WHERE prdcd = '".$data['prdid']."'";
                   $stt_newera_PRDCAT = $this->executeQuery($delnewera_PRDCAT, $this->setDB(2));
                 if($stt_newera_PRDCAT > 0) {
                      $srvReturn = true;
                 } else {
                      throw new Exception("(Step 4) : DELETE data newera_PRDCAT failed..", 1);
                 }
             } else {
                  throw new Exception("(Step 3) : DELETE data msprd failed..", 1);
             }
         } else {
              throw new Exception("(Step 2) : DELETE data pricetab failed..", 1);
         }
     } else {
          throw new Exception("(Step 1) : DELETE data newera_PRDDET failed..", 1);
     }*/
		$prdcd = $data['prdid'];
		if($prdcd == "") {
			$prdcd = $data['cat_inv_id_parent'];
		}

		$delnewera_PRDDET = "DELETE FROM newera_PRDDET WHERE prdcdCat = '".$prdcd."'";
		$stt_newera_PRDDET = $this->executeQuery($delnewera_PRDDET, $this->setDB(2));

		$delnewera_pricetab = "DELETE FROM pricetab WHERE prdcd = '".$prdcd."'";
		$stt_pricetab = $this->executeQuery($delnewera_pricetab, $this->setDB(2));

		$delnewera_msprd = "DELETE FROM msprd WHERE prdcd = '".$prdcd."'";
		$stt_msprd = $this->executeQuery($delnewera_msprd, $this->setDB(2));

		$delnewera_PRDCAT = "DELETE FROM newera_PRDCAT WHERE prdcd = '".$prdcd."'";
		$stt_newera_PRDCAT = $this->executeQuery($delnewera_PRDCAT, $this->setDB(2));
		return true;
	}










	public function getReportEtiket($tgl_awal,$tgl_akhir,$category,$search)
	{

		$result = array();
		$this->db->select('*, a.createdt as tglbeli');
		$this->db->from('trx_etiket a');
		$this->db->join('master_etiket b','a.id_etiket=b.id','left');
		$this->db->where('a.status','0');
		$this->db->where("  a.createdt BETWEEN '".$tgl_awal." 00:00:00.000' AND '".$tgl_akhir." 23:59:59.000'",null,false);

		if($category=='1'){

		}
		if($category=='2'){
			$this->db->like('a.dfno',$search);
		}
		if($category=='3'){
			$this->db->like('a.fullnm', $search);
		}
		if($category=='4'){
			$this->db->like('a.valid_dfno', $search);
		}

		if($category=='5'){
			$this->db->like('a.valid_fullnm', $search);
		}

		if($category=='6'){

//			$this->db->like('nama', $search);
			$this->db->like('lokasi', $search);
		}

		;


		$q = $this->db->get();
//	 echo $this->db->last_query();
		foreach($q->result() as $row)
		{
			$result[] = $row;
		}

		return $result;

	}


	public function getReportEtiket2($tgl_awal,$tgl_akhir,$category,$search)
	{

		$result = array();
		$qry = "
				SELECT *, a.tgltrf as tglbeli, c.fullnm as pembeli, d.fullnm as peserta, a.createdt as tanggal, a.dfno as dfno_pembeli, a.email as email_buyer
				FROM trx_etiket a
				LEFT JOIN master_etiket b
									on a.id_etiket=b.id
				LEFT JOIN klink_mlm2010.dbo.msmemb c
									on a.dfno = c.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				LEFT JOIN klink_mlm2010.dbo.msmemb d
									on a.valid_dfno=d.dfno COLLATE SQL_Latin1_General_CP1_CS_AS
				WHERE a.status = 0
				AND a. createdt BETWEEN '$tgl_awal 00:00:00.000' AND '$tgl_akhir 23:59:59.000'

				 ";

		if($category=='1'){

		}
		if($category=='2'){
			$qry.="AND a.dfno LIKE '%$search%' ";
		}
		if($category=='3'){
			$qry.="AND a.fullnm LIKE '%$search%' ";

		}
		if($category=='4'){
			$qry.="AND a.valid_dfno LIKE '%$search%' ";

		}

		if($category=='5'){
			$qry.="AND a.valid_fullnm LIKE '%$search%' ";

		}

		if($category=='6'){
			$qry.="AND b.lokasi LIKE '%$search%' ";

//			$this->db->like('nama', $search);
		}

		$qry.="order by notiket ";


		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}


	public function getReportFinance($tgl_awal,$tgl_akhir,$category,$search)
	{

		$result = array();
		$qry = "
				SELECT order_no, a.dfno, a.fullnm, b.datetrans, a.tgltrf,  a.flag_online, a.pembayaran,a.rekening, SUM(price) as nominal
				FROM [dbo].[trx_etiket] a
				LEFT JOIN ecomm_trans_hdr_sgo b
				ON a.order_no=b.token
				LEFT JOIN master_etiket c
				ON a.id_etiket=c.id

				WHERE a.status=0
				AND a. createdt BETWEEN '$tgl_awal 00:00:00.000' AND '$tgl_akhir 23:59:59.000'
				 ";

		if($category=='1'){

		}
		if($category=='2'){
			$qry.="AND a.dfno LIKE '%$search%' ";
		}
		if($category=='3'){
			$qry.="AND a.fullnm LIKE '%$search%' ";

		}
		if($category=='4'){
			$qry.="AND a.valid_dfno LIKE '%$search%' ";

		}

		if($category=='5'){
			$qry.="AND a.valid_fullnm LIKE '%$search%' ";

		}

		if($category=='6'){
			$qry.="AND c.lokasi LIKE '%$search%' ";

//			$this->db->like('nama', $search);
		}

		$qry.="GROUP BY order_no,a.dfno, a.fullnm,  b.datetrans, a.tgltrf,  a.flag_online, a.pembayaran,a.rekening
				order by datetrans ";


		$res = $this->getRecordset($qry, NULL, $this->db1);
		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}

	function validasitiket($data) {
		/*
		 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm ";
		 */
		$qry = "select a.valid_dfno, a.valid_fullnm, b.nama, b.lokasi, a.status, a.id_etiket
				from trx_etiket A
				LEFT JOIN master_etiket B
				ON A.id_etiket = B.id
				where notiket = '".$data."'  ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}
	function validasitiket2($data, $acara) {
		/*
		 $qry = "SELECT A.dfno, A.fullnm,max(B.AdjustedRank) as rank
		        FROM msmemb A inner join tbonus B
		        On A.dfno=B.distributorCode
		        where A.dfno= '$data'
		        AND A.DFNO NOT IN (SELECT noPeserta COLLATE SQL_Latin1_General_CP1_CS_AS  FROM NH_DTC)
		        Group by A.dfno, A.fullnm ";
		 */

		$qry = "select a.valid_dfno, a.valid_fullnm, b.nama, b.lokasi, a.status,a.id_etiket
				from trx_etiket A
				LEFT JOIN master_etiket B
				ON A.id_etiket = B.id
				where valid_dfno = '".$data."' and id_etiket = '".$acara."' and a.status=0 ";
		//echo $qry;
		$res = $this->getRecordset($qry, NULL, $this->db1);

		if($res == null) {
			//		throw new Exception("Data tidak ada..!!", 1);
			$res='Err';
		}
		return $res;
	}

	function submitAttendee() {
		$data = $this->input->post(NULL, TRUE);
		$qry = "INSERT INTO attendee (
				id,
				id_acara,
				no_tiket,
				peserta,
				waktu,
				status,
				createnm

				)
		        VALUES ('".$this->uuid->v4()."', '$data[loc_event]', '$data[no_tiket]',
		              	'$data[peserta]',GETDATE(),'0',



		               '".$this->username."')";
		$query = $this->db->query($qry);
		if(($this->db->affected_rows() != 1) ) {
			throw new Exception("Save failed..!!", 1);
		}
		return $query;
	}


}
<?php

    class Model_report_omset extends MY_Model{

        public function __construct(){

            parent::__construct();

        }

        public function listBnsperiod(){

            $sql = "select CONVERT(varchar, bnsperiod, 105) AS bnsperiod FROM [dbo].[V_HILAL_OMSET_DAILY]
                    where (bnsperiod > '01-01-2012')
                    GROUP BY bnsperiod";

            $result = $this->getRecordset($sql,null,$this->db2);
            //echo $this->db->last_query();
            return $result;
        }

        public function listReport($date){

            $sql = "select * from [dbo].[V_HILAL_OMSET_DAILY]
                    where bnsperiod = '$date' and TTP_TYPE = 'INVOICE'";

            $result = $this->getRecordset($sql,null,$this->db2);
            echo $this->db->last_query();
            return $result;
        }

        public function listReportByStockist($dt){

            $idstockist = $dt['idstockist'];
            $searchBy = $dt['searchBy'];

            if($searchBy == 'tgl'){

                $tgl_dari = $dt['tgl_dari'];
                $tgl_ke = $dt['tgl_ke'];

                $qry = "select * from V_HILAL_RPT_DRAH
                        where sc_dfno = '$idstockist' and trdt BETWEEN '$tgl_dari' and '$tgl_ke'";
            }

            if($searchBy == 'bnsperiod'){

                $month = $dt['bnsmonth'];
                $year = $dt['bnsyear'];
                $tgl = $year.'-'.$month.'-'.'01';

                $qry = "select * from V_HILAL_RPT_DRAH
                        where sc_dfno = '$idstockist' and bnsperiod = '$tgl'";
            }

            $result = $this->getRecordset($qry,null,$this->db2);
            //echo $this->db->last_query();
            return $result;
        }
    }
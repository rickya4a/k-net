<?php
class Model_memberLBC extends MY_Model{

    public function __construct(){
        // Call the Model constructor
        parent::__construct();

    }

    public function cekMemberLBC($dfno){

        $slc = "Select a.dfno,a.fullnm,a.addr1,a.addr2,a.addr3,a.register_dt,a.expired_dt,
                a.rank,a.rank_desc,a.STATUS,a.birthdt,a.reprint,a.email, b.tel_hp,b.tel_hm
                from ASH_LBC_MEMB a LEFT OUTER JOIN msmemb b
                ON (a.dfno collate SQL_Latin1_General_CP1_CS_AS=b.dfno collate SQL_Latin1_General_CP1_CS_AS)
                where a.dfno='$dfno'";
        //echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db2);
        return $result;
    }

    public function cekMember($dfno){

        $slc = "select dfno,fullnm from msmemb where  dfno = '$dfno'";
        //echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db2);
        return $result;
    }

    public function detailMemberLBC($dfno, $from, $to){

//        $slc = "select a.bnsperiod,YEAR(a.bnsperiod) as THN, MONTH(a.bnsperiod) as BLN,a.dfno,a.trcd,
//                a.xprdcd,b.prdnm,a.xqtyord,a.tdp,a.tbv
//                from QTW_NEWTRH_NEWTRD5 a LEFT OUTER JOIN msprd b ON (a.xprdcd=b.prdcd)
//                where (a.bnsperiod>='$from' and a.bnsperiod <= '$to') and a.dfno='$dfno'
//                order by a.bnsperiod,a.trcd,a.xprdcd";
        $slc = "select a.bnsperiod,YEAR(a.bnsperiod) as THN, MONTH(a.bnsperiod) as BLN,a.dfno,a.trcd,
                a.xprdcd,b.prdnm,a.xqtyord,a.tdp,a.tbv,a.xqtyord*a.tdp as xdp, a.xqtyord*a.tbv as xbv
                from QTW_NEWTRH_NEWTRD5 a LEFT OUTER JOIN msprd b ON (a.xprdcd=b.prdcd)
                where (a.bnsperiod>='$from' and a.bnsperiod <= '$to') and a.dfno='$dfno'
                order by a.bnsperiod,a.trcd,a.xprdcd";
        //echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db2);
        return $result;
    }

    public function salesLBChdr($bns_period){

        $sql = "select * from ASH_LBC_REKAPPRODUK where bnsperiod='$bns_period' and right(prdcd,2)<>'SP'";

        $result = $this->getRecordset($sql, null, $this->db2);

        return $result;
    }

    public function salesLBCdet($bns_period){

        $sql = "select bnsperiod,prdcd,sum(qtyord) as JML from ASH_LBC_REKAPPRODUK
                 where bnsperiod='$bns_period' and right(prdcd,2)<>'SP'
                group by bnsperiod,prdcd";

        $result = $this->getRecordset($sql, null, $this->db2);

        return $result;
    }


}
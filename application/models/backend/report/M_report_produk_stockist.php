<?php
class M_report_produk_stockist extends MY_Model {
	function __construct() {
		parent::__construct();
	}

	function rekapProduk($stockist, $status, $from, $to) {
		$sql = "SELECT c.prdcd, c.prdnm, SUM(c.qty) as jumlah
						FROM db_ecommerce.dbo.ecomm_trans_det_prd c
						LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_hdr a  ON (c.orderno = a.orderno)
						LEFT OUTER JOIN klink_mlm2010.dbo.webol_trans_ok b ON (a.orderno = b.orderno)
						WHERE a.sentTo = '1'
						AND a.idstk = '$stockist' and b.[status] = '$status'
						AND LEFT(a.orderno, 4) != 'IDTA'
						AND c.prdcd NOT IN ('SK50E')
						AND a.datetrans BETWEEN '$from' AND '$to'
						GROUP BY c.prdcd, c.prdnm";
		$res = $this->getRecordset($sql, null, $this->db1);
		return $res;
	}

	function rekapTransaksi($stockist, $status, $from, $to) {
		$sql = "SELECT a.orderno, a.datetrans, a.total_pay, a.total_bv, COUNT(c.prdcd) as jenis_produk
						FROM db_ecommerce.dbo.ecomm_trans_hdr a
						LEFT OUTER JOIN klink_mlm2010.dbo.webol_trans_ok b ON (a.orderno = b.orderno)
						LEFT OUTER JOIN db_ecommerce.dbo.ecomm_trans_det_prd c ON (a.orderno = c.orderno)
						WHERE a.sentTo = '1'
						AND a.idstk = '$stockist' AND b.[status] = '$status'
						AND LEFT(a.orderno, 4) != 'IDTA'
						AND c.prdcd NOT IN ('SK50E')
						AND a.datetrans BETWEEN '$from' AND '$to'
						GROUP BY a.orderno, a.datetrans, a.total_pay, a.total_bv";
		$res = $this->getRecordset($sql, null, $this->db1);
		return $res;
	}
}

?>
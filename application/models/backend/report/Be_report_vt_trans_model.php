<?php
class Be_report_vt_trans_model extends MY_Model {
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
		
    }
	
	function getListVT($dt){
    	
		$dtFrom = explode('/',$dt['date_from']);
		$dtTo 	= explode('/',$dt['date_to']);
		
		$dtFromX = $dtFrom[2]."/".$dtFrom[1]."/".$dtFrom[0];
		$dtToX 	= $dtTo[2]."/".$dtTo[1]."/".$dtTo[0];
		
		 $slc = "SELECT *
		 		 FROM V_ECOM_LIST_VT_RPT A
		 		 WHERE (CONVERT(VARCHAR(10), a.datetrans, 111) between '$dtFromX' and '$dtToX')
		 		 ORDER BY A.sett_Amount,A.sett_Req_Date,A.sett_Paid_Date";
		//echo  $slc;
        $result = $this->getRecordset($slc, null, $this->db1);
        return $result; 
    }
    	
}
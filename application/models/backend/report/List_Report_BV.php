<?php

    class List_Report_BV extends MY_Model{

        function __construct(){

            parent::__construct();
        }

        function get_detailreport0100($year){

            $sql = "SELECT DISTINCT bonusmonth, bonusyear,
										dist_0_to_100, bv_0_to_100, dp_0_to_100,
										dist_101_to_200, bv_101_to_200, dp_101_to_200,
										dist_201_to_300, bv_201_to_300, dp_201_to_300,
                                        dist_301_to_400, bv_301_to_400, dp_301_to_400,
                                        dist_401_to_500, bv_401_to_500, dp_401_to_500,
                                        dist_501_to_600, bv_501_to_600, dp_501_to_600,
                                        dist_601_above, bv_601_above, dp_601_above
                    FROM [dbo].[HILAL_RPT_SUMM_BV_2]
                    where bonusyear = '$year'
                    ORDER BY bonusmonth";
//            $sql = "SELECT * FROM [dbo].[HILAL_RPT_SUMM_BV_2] where bonusyear = '$year'
//                    ORDER BY bonusmonth";
            $result = $this->getRecordset($sql,null,$this->db2);
            //echo $this->db->last_query();
            return $result;
        }

        function get_detailreport0099($year){

            $sql = "SELECT DISTINCT bonusmonth, bonusyear,
										dist_0_to_99, bv_0_to_99, dp_0_to_99,
										dist_100_to_199, bv_100_to_199, dp_100_to_199,
										dist_200_to_299, bv_200_to_299, dp_200_to_299,
                                        dist_300_to_399, bv_300_to_399, dp_300_to_399,
                                        dist_400_to_499, bv_400_to_499, dp_400_to_499,
                                        dist_500_to_599, bv_500_to_599, dp_500_to_599,
                                        dist_600_above, bv_600_above, dp_600_above
                    FROM [dbo].[HILAL_RPT_SUMM_BV_2]
                    where bonusyear = '$year'
                    ORDER BY bonusmonth";
            $result = $this->getRecordset($sql, null, $this->db2);
            return $result;
        }

        //permintaan pak toto
        function cekTTBV($idmember){

            $sql = "select ttbv from TWA_KLPromo_Oct17_H where dfno = '$idmember'";
            $result = $this->getRecordset($sql, null, $this->db2);
            return $result;
        }
    }
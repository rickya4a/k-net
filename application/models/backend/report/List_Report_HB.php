<?php

    class List_Report_HB extends MY_Model{

        function __construct(){

            parent::__construct();
        }

        function getHdrHB($idmember){

            $sql = "SELECT b.fullnm, a.dfno, x.total
                FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                LEFT JOIN msmemb b
                on a.dfno = b.dfno
                LEFT JOIN
                (SELECT b.dfno, COUNT(*) as total
                 FROM [dbo].[TWA_NONSUPLEMEN_UNDIAN] a
                 LEFT JOIN msmemb b
                 on a.dfno = b.dfno
                 GROUP BY b.dfno) x
                ON x.dfno=b.dfno
								where a.dfno = '$idmember'
                GROUP BY b.fullnm, a.dfno, x.total";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getHdr($idmember){

            $sql = "select fullnm from msmemb where dfno='$idmember'";

            $result = $this->getRecordset($sql,null,$this->db2);
            return $result;
        }

        function getListHB($idmember, $bnsperiod){

            $sql = "select a.bnsperiod,a.dfno,a.rdfno, a.trcd, b.etdt, a.prdcd ,a.prdnm,a.prdcd_paket,a.qty,
                    (a.bv/a.qty) as sBV,a.bv from TWA_NONSUPLEMEN_SALES a LEFT OUTER JOIN NEWTRH b ON
                    (a.trcd=b.trcd) where a.bnsperiod='$bnsperiod' and a.dfno='$idmember' and a.dfno=a.rdfno
                    order by a.bnsperiod,a.dfno,a.trcd,b.etdt, a.prdcd";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getListRekrutanHB($idmember, $bnsperiod){

            $sql = "select a.bnsperiod,a.rdfno as dfno, b.fullnm,b.jointdt,
                    sum(a.bv) as tbvs,sum(a.bv)/50 as tkp from TWA_NONSUPLEMEN_SALES a LEFT OUTER JOIN MSMEMB b ON
                    (a.rdfno=b.dfno) where a.bnsperiod='$bnsperiod' and a.dfno='$idmember' and a.dfno<>a.rdfno
                    group by a.bnsperiod,a.rdfno,b.fullnm,b.jointdt
                    order by a.bnsperiod,a.rdfno";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getDetRekrutanHB($idmember, $bnsperiod){

            $sql = "select a.bnsperiod,a.dfno,a.rdfno, b.fullnm,b.jointdt,a.trcd, a.prdcd ,a.prdnm,a.prdcd_paket,a.qty,
                    (a.bv/a.qty) as sBV,a.bv from TWA_NONSUPLEMEN_SALES a LEFT OUTER JOIN MSMEMB b ON
                    (a.rdfno=b.dfno) where a.bnsperiod='$bnsperiod' and a.rdfno='$idmember' and a.dfno<>a.rdfno
                    order by a.bnsperiod,a.dfno,a.rdfno,a.trcd, a.prdcd";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getAllSales($idmember,$bnsperiod){

            $sql = "Select a.bnsperiod,a.dfno,a.etdt, a.trcd, a.xprdcd ,b.prdnm,a.xqtyord, a.tbv,
                    (a.xqtyord*a.tbv) as totBV
                    from QTW_NEWTRH_NEWTRD5 a LEFT OUTER JOIN msprd b ON (a.xprdcd=b.prdcd)
                    where bnsperiod='$bnsperiod' and a.dfno='$idmember'
                    order by a.bnsperiod,a.dfno,a.etdt,a.xprdcd";

            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getAllSalesNonSupl($idmember,$bnsperiod){

            $sql = "select a.bnsperiod,a.dfno,a.rdfno, a.trcd, b.etdt, a.prdcd ,a.prdnm,a.prdcd_paket,a.qty,
                    (a.bv/a.qty) as sBV,a.bv from TWA_NONSUPLEMEN_SALES a LEFT OUTER JOIN NEWTRH b ON
                    (a.trcd=b.trcd) where a.bnsperiod='$bnsperiod' and a.dfno='$idmember' and a.dfno=a.rdfno
                    order by a.bnsperiod,a.dfno,a.trcd,b.etdt, a.prdcd";

            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getAllSalesRekrutan($idmember,$bnsperiod){

            $sql = "SELECT  a.trcd,  a.etdt,  a.bnsperiod,  a.dfno,  b.fullnm, a.xdp,  a.tdp,  a.tbv,
                    a.xprdcd,  c.prdnm, a.xqtyord, b.sfno_reg,  b.jointdt, (a.xqtyord*a.tbv) as totBV
                    FROM QTW_NEWTRH_NEWTRD5 a    LEFT OUTER JOIN msmemb b ON (a.dfno = b.dfno)
                    LEFT OUTER JOIN msprd c ON (a.xprdcd = c.prdcd)
                    where  a.bnsperiod='$bnsperiod'  and b.jointdt>='$bnsperiod' and b.sfno_reg='$idmember'
                    order by b.jointdt, a.bnsperiod, b.sfno_reg,a.dfno, a.etdt,a.trcd, a.xprdcd";

            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getBVAllNonSupl($idmember,$bnsperiod){

            $sql = "select a.bnsperiod,a.rdfno as dfno, b.fullnm,b.jointdt,
                    sum(a.bv) as tbvs,sum(a.bv)/50 as tkp from TWA_NONSUPLEMEN_SALES a LEFT OUTER JOIN MSMEMB b ON
                    (a.rdfno=b.dfno) where a.bnsperiod='$bnsperiod' and a.dfno='$idmember' and a.dfno<>a.rdfno
                    group by a.bnsperiod,a.rdfno,b.fullnm,b.jointdt
                    order by a.bnsperiod,a.rdfno";

            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getAllSalesNonSuplRekrutan($idmember,$bnsperiod){

            $sql = "select a.bnsperiod,a.dfno,a.rdfno, b.fullnm,b.jointdt,a.trcd, a.prdcd ,a.prdnm,a.prdcd_paket,a.qty,
                    (a.bv/a.qty) as sBV,a.bv from TWA_NONSUPLEMEN_SALES a LEFT OUTER JOIN MSMEMB b ON
                    (a.rdfno=b.dfno) where a.bnsperiod='$bnsperiod' and a.rdfno='$idmember' and a.dfno<>a.rdfno
                    order by a.bnsperiod,a.dfno,a.rdfno,a.trcd, a.prdcd";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getBVAllSales($idmember,$bnsperiod){

            $sql = "Select bnsperiod,dfno, sum(xqtyord*tbv) as tBV from QTW_NEWTRH_NEWTRD5
                    where dfno='$idmember' and bnsperiod='$bnsperiod' GROUP BY bnsperiod,dfno";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

        function getBVAllSalesNonSupl($idmember,$bnsperiod){

            $sql = "Select bnsperiod,dfno, sum(bv) as tBV from TWA_NONSUPLEMEN_SALES
                    where dfno='$idmember' and bnsperiod='$bnsperiod'
                    and (dfno=rdfno) GROUP BY bnsperiod,dfno";

            //echo $sql;
            $result = $this->getRecordset($sql,null,$this->db2);

            return $result;
        }

    }
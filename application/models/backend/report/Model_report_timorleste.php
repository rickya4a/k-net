<?php

    class Model_report_timorleste extends MY_Model{

        function __construct(){

            parent::__construct();

        }

        function getReportTimorLeste($data){

            $stat = $data['StatTrans'];
            $date_from = $data['date_from'];
            $date_to = $data['date_to'];
            $amount = $data['amount'];
            $nett = $data['nett'];

            if($stat == 'all'){

                $sql = "select a.idnumber, a.fullnm, a.NETT, a.status, a.tgl_trf, b.accountno, b.bankid, b.accountname
                        from QTWA_TLBNSsum2 a
                        INNER join QTWA_TLBNSdet2 b
                        on a.idnumber = b.idnumber
                        where b.bnsperiod between '$date_from' and '$date_to' and a.NETT $amount '$nett'";

                $result = $this->getRecordset($sql,null,$this->db7);
                return $result;

            }

            elseif($stat == 'P'){

                $sql = "select a.idnumber, a.fullnm, a.NETT, a.status, a.tgl_trf, b.accountno, b.bankid, b.accountname
                    from QTWA_TLBNSsum2 a
                    INNER join QTWA_TLBNSdet2 b
                    on a.idnumber = b.idnumber
                    where a.status = '$stat' and b.bnsperiod between '$date_from' and '$date_to' and a.NETT $amount '$nett'";

                //echo $sql;
                $result = $this->getRecordset($sql,null,$this->db7);
                return $result;
            }

            else{

                $sql = "select a.idnumber, a.fullnm, a.NETT, a.status, a.tgl_trf, b.accountno, b.bankid, b.accountname
                    from QTWA_TLBNSsum2 a
                    INNER join QTWA_TLBNSdet2 b
                    on a.idnumber = b.idnumber
                    where a.status = '$stat' and b.bnsperiod between '$date_from' and '$date_to' and a.NETT $amount '$nett'";

                //echo $sql;
                $result = $this->getRecordset($sql,null,$this->db7);
                return $result;
            }
            
        }

        function insertStatTL($param) {

            $qry = "INSERT TL_update (idnumber, status) VALUES ('$param','T')";

            //echo $qry;

            $res = $this->executeQuery($qry, $this->db7);

            return $res;
        }

        function getDetailReport($id, $stat){

            $qry = "select bnsperiod, idnumber, fullnm, NETT, TRF, KET
                    from QTWA_TLBNSdet2
                    where idnumber = '$id' and status = '$stat'";

            //echo $qry;
            $result = $this->getRecordset($qry,null,$this->db7);

            return $result;
        }

        function getDetailReport2($id){

            $qry = "select a.idnumber, a.bnsperiod, a.NETT, b.NETT as total
                    from QTWA_TLBNSdet2 a
                    inner join QTWA_TLBNSsum2 b
                    on a.idnumber = b.idnumber
                    where a.idnumber = '$id' and a.status = 'P' and b.status = 'P'";

            //echo $qry;
            $result = $this->getRecordset($qry,null,$this->db7);

            return $result;
        }

        function prevSave($idmember){

                $qry = "select idnumber, fullnm, NETT
                    from QTWA_TLBNSSUM2
                    where idnumber in($idmember) and status = 'P'";
//            $qry = "select *
//                    from QTWA_TLBNSSUM2
//                    where idnumber in($idmember) and status = 'P'";

                //echo $qry;

                $result = $this->getRecordset($qry,null,$this->db7);

                return $result;

        }

        function updateStatusTrans($idmember, $potongan){

            $qry = "update TL_update_H set potongan = '$potongan' where idnumber = '$idmember'";
            //echo $qry;
            $result = $this->executeQuery($qry,$this->db7);

            return $result;
        }

        function insertStatusTrans($idmember, $bonus){

            $qry = "insert into TL_update_H (idnumber,status,date,potongan,bonus) VALUES ('$idmember','T',getdate(),'0','$bonus')";
            //echo $qry;
            $result = $this->executeQuery($qry,$this->db7);

            return $result;
        }

        function updateNettTrans($idmember, $nett){

            $qry = "update TL_update_H set total_trans = '$nett' where idnumber = '$idmember'";
            //echo $qry;
            $result = $this->executeQuery($qry,$this->db7);

            return $result;
        }

        function insertDetailTrans($idmember, $bnsmonth, $nett){

            $qry = "insert into TL_update_D (idnumber, date, bnsmonth, transfer) VALUES ('$idmember',getdate(),'$bnsmonth','$nett')";

            $result = $this->executeQuery($qry,$this->db7);

            return $result;
        }

        function updateStatus($idmember, $bnsmonth){

            $qry = "update tl_bns set status = 'T', tgl_trf = getdate()
                    where idnumber = '$idmember' and status = 'P' and bnsperiod = '$bnsmonth'";

            //echo $qry;
            $result = $this->executeQuery($qry,$this->db7);

            return $result;
        }

    }
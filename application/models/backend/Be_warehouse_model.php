<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Be_warehouse_model extends MY_Model {
  
    function __construct() {
        // Call the Model constructor
        parent::__construct();
       
    }
	
	function getShippingList($data) {
		$qry = "SELECT A.orderno, A.conoteJNE, 
				    A.idstk, A.scnm, A.pickup_number,
				    COUNT(A.prdcd) AS qty_prd, 
				    CONVERT(VARCHAR(10), A.datetrans, 103) as datetrans
				FROM V_ECOMM_LIST_FOR_DO_GEN A
				WHERE CONVERT(VARCHAR(10), A.pickup_date, 111) BETWEEN '$data[from]' AND '$data[to]'
				AND A.whcd = '$data[whcd]'
				GROUP BY A.orderno, A.conoteJNE, A.idstk ,A.scnm, A.pickup_number,
				CONVERT(VARCHAR(10), A.datetrans, 103)";
		 //echo $qry;
		 $result = $this->getRecordset($qry,null,$this->db1);
		 if($result == null) {
		 	throw new Exception(setErrorMessage(), 1);			 
		 }
		 return $result;		
	}
	
	function getDetailProductShipping($orderno) {
		$qry = "select A.prdcd, A.prdnm, A.qty
				from V_ECOMM_LIST_FOR_DO_GEN a
				where a.orderno = '$orderno'";
		 $result = $this->getRecordset($qry,null,$this->db1);
		 if($result == null) {
		 	throw new Exception(setErrorMessage(), 1);			 
		 }
		 return $result;
	}
	
	function getShippingSummary($orderno) {
		 $qry = "select STUFF((SELECT ', ' + (B.orderno)
				                FROM V_ECOMM_LIST_FOR_DO_GEN B
				                WHERE B.orderno IN ($orderno)
				                GROUP BY B.orderno
				                ORDER BY B.orderno
				                FOR XML PATH ('')),1,2,''
				                ) AS orderno,
				       STUFF((SELECT ', ' + (B.conoteJNE)
				                FROM V_ECOMM_LIST_FOR_DO_GEN B
				                WHERE B.orderno IN ($orderno)
				                GROUP BY B.orderno, B.conoteJNE
				                ORDER BY B.conoteJNE
				                FOR XML PATH ('')),1,2,''
				                ) AS conoteJNE,
				       A.prdcd, A.prdnm, SUM(A.qty) AS qty
				FROM V_ECOMM_LIST_FOR_DO_GEN a
				WHERE a.orderno in ($orderno)
				GROUP BY A.prdcd, A.prdnm";
		 $result = $this->getRecordset($qry,null,$this->db1);
		 if($result == null) {
		 	throw new Exception(setErrorMessage(), 1);			 
		 }
		 return $result;
	}
	
	function getListShippingLabel($dta) {
		if($dta['print_count'] == "0") {
			$add = " AND a.print_count = 0";
		} else {
			$add = " AND a.print_count > 0";
		}
		
		$whcd = " AND a.whcd = '$dta[sentToWH]' ";
		if($dta['sentToWH'] == ""){
			$whcd = "";
		}
//		$qry = "SELECT * FROM  V_ECOM_LIST_PICKUP_READY_HDR
//			    WHERE CONVERT(VARCHAR(10), datetrans, 111) BETWEEN '$dta[prt_from]' AND '$dta[prt_to]'
//			    $whcd
//			    $add";
		$qry = "SELECT a.*, b.shipper_name
				FROM V_ECOM_LIST_PICKUP_READY_HDR a
				LEFT JOIN master_logistic_shipper b
				ON a.cargo_id = b.shipper_id
				WHERE CONVERT(VARCHAR(10), datetrans, 111) BETWEEN '$dta[prt_from]' AND '$dta[prt_to]'
				$whcd $add
				AND a.cargo_id = $dta[list_shipping]";
        //echo $qry;
		$result = $this->getRecordset($qry,null,$this->db1);
		if($result == null) {
		 	throw new Exception(setErrorMessage(), 1);			 
		}
		return $result;
	}
	
	function getDetailVoucher($param) {
		$cekVoucherNum = "SELECT
		                      a.status, 
					          a.vchkey,
					          a.formno,
					          a.activate_by,
					          a.prdcd,
					          c.prdnm,
					          a.updatenm,
					          a.activate_dfno,
					          b.fullnm as nama_member_aktif,
					          CONVERT(VARCHAR(30), a.updatedt, 103) AS tgl2,
					          sold_trcd,
					          sold_trcdnewera, remarks
					      FROM
					          starterkit a
					          LEFT OUTER JOIN msmemb b ON (a.activate_dfno = b.dfno)
							  LEFT OUTER JOIN msprd c ON (a.prdcd = c.prdcd)
					      WHERE
					          (a.formno = '$param')";	
        $result = $this->getRecordset($cekVoucherNum,null,$this->db2);
		//var_dump($result);
		return $result;		
	}
	
	function getListVcrByParam($isdate, $param1, $param2, $param3, $searchby){
		$join = null;
		
		if($isdate == 0) {
			if($searchby == "trcd"){
				$param = " a.invoiceno ='$param1' ";
			}elseif($searchby == "receiptno"){
				$param = " b.trcd ='$param1' ";
			}elseif($searchby == "vcno"){
				$param = "S.formno ='$param1' ";
				$join = " INNER JOIN klink_mlm2010.dbo.starterkit S ON A.invoiceno=S.sold_trcd ";
			}
		} elseif($isdate == 1) {
			$param = " b.createdt BETWEEN '$param2' AND '$param3' ";
		}
		$qry = "SELECT a.category, a.ordtype, a.invoiceno, a.invoicedt, a.dfno, c.fullnm,
					   b.trcd, b.createdt, b.applyto
				FROM klink_mlm2010.dbo.ordivtrh a
					 INNER JOIN klink_mlm2010.dbo.billivhdr b on a.receiptno=b.trcd
				     INNER JOIN klink_mlm2010.dbo.mssc c ON a.dfno=c.loccd
				     $join
				WHERE $param
				ORDER BY a.invoiceno";
        // /echo $qry;				
		$result = $this->getRecordset($qry,null,$this->db1);
		//var_dump($result);
		return $result;
	}
	
	function getDetailProductByOrderNo($orderno){
		$qry = "SELECT A.invoiceno, a.prdcd, b.prdnm, b.kit, a.qtyord, a.dp, a.bv, b.kit
				FROM klink_mlm2010.dbo.ordivtrd A
					 INNER JOIN klink_mlm2010.dbo.msprd B ON A.prdcd=B.prdcd
				WHERE A.invoiceno='$orderno'
				ORDER BY b.kit desc";
				
		
        //echo $qry;				
		$result = $this->getRecordset($qry,null,$this->db1);
		//var_dump($result);
		return $result;
	}
	
	function getListProductForSK($orderno) {
		$qry = "SELECT b.invoiceno, b.prdcd, c.prdnm, c.kit,
					b.pricecode, b.dp, b.bv, b.qtyord,
				    COUNT(d.formno) as jml_active,
				    b.qtyord - COUNT(d.formno) as sisa_qty 
				FROM klink_mlm2010.dbo.ordivtrd b
				LEFT OUTER JOIN klink_mlm2010.dbo.msprd c ON (b.prdcd = c.prdcd)
				LEFT OUTER JOIN klink_mlm2010.dbo.starterkit d ON (b.invoiceno = d.sold_trcd) AND d.prdcd = b.prdcd
				WHERE b.invoiceno = '$orderno' AND c.kit = '1'
				GROUP BY b.invoiceno, b.prdcd, c.prdnm, 
					b.pricecode, b.dp, b.bv, b.qtyord, c.kit
				";	
	    $result = $this->getRecordset($qry,null,$this->db1);
		if($result == null) {
			$qry = "SELECT b.invoiceno, b.prdcd, c.prdnm, c.kit,
					b.pricecode, b.dp, b.bv, b.qtyord,
				    COUNT(d.formno) as jml_active,
				    b.qtyord - COUNT(d.formno) as sisa_qty 
				FROM klink_mlm2010.dbo.ordtrd b
				LEFT OUTER JOIN klink_mlm2010.dbo.msprd c ON (b.prdcd = c.prdcd)
				LEFT OUTER JOIN klink_mlm2010.dbo.starterkit d ON (b.invoiceno = d.sold_trcd) AND d.prdcd = b.prdcd
				WHERE b.invoiceno = '$orderno' AND c.kit = '1'
				GROUP BY b.invoiceno, b.prdcd, c.prdnm, 
					b.pricecode, b.dp, b.bv, b.qtyord, c.kit
				";	
	        $result = $this->getRecordset($qry,null,$this->db1);
		}	
		return $result;
	}
	
	function getStarterkitBrOrderno($orderno, $prdcd){
		/* $qry = "SELECT a.sold_trcd, MAX(a.formno) as max_formno, MIN(a.formno) as min_formno, a.updatenm, a.updatedt
				FROM klink_mlm2010.dbo.starterkit a
				WHERE A.sold_trcd='$orderno'
				GROUP BY a.sold_trcd, a.updatenm, a.updatedt";
		 * 
		 */
		 $qry = "SELECT a.sold_trcd, a.formno, a.updatenm, a.updatedt, a.activate_dfno, b.fullnm, a.status
				FROM klink_mlm2010.dbo.starterkit a
				     LEFT OUTER JOIN klink_mlm2010.dbo.msmemb b on a.activate_dfno=b.dfno
				WHERE A.sold_trcd='$orderno' AND a.prdcd = '$prdcd'
				ORDER BY a.formno";
        //echo $qry;				
		$result = $this->getRecordset($qry,null,$this->db1);
		//var_dump($result);
		return $result;
	}

	function getCheckvoucher($vchno){
		$qry = "SELECT top 1 * 
				FROM klink_mlm2010.dbo.starterkit a
				WHERE A.formno='$vchno' and a.status='0'
				ORDER BY a.formno";
        //echo $qry;				
		$result = $this->getRecordset($qry,null,$this->db1);
		//var_dump($result);
		return $result;
	}
	
	function getValidVoucher($vch_start, $qty) {
		  $bagian1 = substr($vch_start, 0, 6);
          $counter = substr($vch_start, 6, 6);
          $value = "";
          //echo "$voucher_from<br>";
          //echo "$bagian1<br>";
          //echo "$counter";
          $baru = $qty + $counter - 1;
          
          $data = array();
          $arrayKe = 0;
          for($i = $counter; $i <= $baru; $i++)
          {
            
            $next_id = sprintf("%06s",$i);
        	$y =  strval($bagian1.$next_id);
        	$value .= "'".$y."',";
            array_push($data,$y);
        	$arrayKe++;
          }
          $value=substr($value,0,-1);
          //echo "$value<br>";
          //$lastCounter = $baru - 1;
          $arrayKe--;
          
          $cekAvailableVoucher = "SELECT a.sold_trcd, a.formno, a.updatenm, CONVERT(VARCHAR(10), a.updatedt, 111) as updatedt, a.activate_dfno, b.fullnm, a.status
                                  FROM klink_mlm2010.dbo.starterkit a 
                                  LEFT OUTER JOIN klink_mlm2010.dbo.msmemb b on a.activate_dfno=b.dfno
                                  WHERE a.formno IN ($value) AND a.status != '0'";
		   //echo $cekAvailableVoucher;
		  $result = $this->getRecordset($cekAvailableVoucher,null,$this->db1);
		  if($result != null) {
		  	 $arr = array("response" => "false", "res" => $result);
		  } else {
		  	$arr = array("response" => "true", "res" => $data[$arrayKe]);
		  }
		  return $arr;
	}
	
	function getReleaseVcrSKupd($vchno, $usrname, $trcd, $prdcd){
		$qry = "UPDATE klink_mlm2010.dbo.starterkit 
				SET sold_trcd='$trcd', updatenm='$usrname', updatedt=GETDATE(), status='1', prdcd='$prdcd'
				WHERE formno='$vchno'";
       // $qry;				
		$result1 = $this->executeQuery($qry);
		
		$qry2 = "SELECT * FROM klink_mlm2010.dbo.starterkit WHERE formno='$vchno'";
		$result = $this->getRecordset($qry2,null,$this->db1);
		//var_dump($result);
		return $result;
	}
	
	function updateReleaseVoucher($form) {
		$value = "";
		for($i = $form['vch_start']; $i <= $form['vch_end']; $i++)
        {
           $value .= "'".$i."',";
        }
        $value=substr($value,0,-1);
        
        $tgl_skrg = date("Y-m-d");	 
                	
		$updStarterkit = "UPDATE starterkit SET sold_trcd = '$form[trxno]', status='1', updatenm = '$this->username', 
    	                         updatedt='$tgl_skrg', PT_SVRID = 'ID', prdcd='$form[productcode]', sold_trcdnewera = '$form[trxno]' 
    					  WHERE formno IN($value)";	 
		$res = $this->executeQuery($updStarterkit, $this->db2);
		return $res;	  
                	
	}
	
	function getListProvinceWithEmptyStockist() {
		$qry = "SELECT  DISTINCT(a.provinsiNama) as provinsiNama 
				FROM master_kgp_wil a 
				WHERE loccd is null OR loccd = ''
				GROUP BY a.provinsiNama
				ORDER BY provinsiNama";	
		$result = $this->getRecordset($qry,null,$this->db1);
		if($result != null) {
			$def = jsonTrueResponse($result);
		} else {
			$def = jsonFalseResponse("No province..");
		}
		return $def;
	}
	
	function getListProvinceKgp() {
		$qry = "SELECT  DISTINCT(a.provinsiNama) as provinsiNama 
				FROM master_kgp_wil a 
				GROUP BY a.provinsiNama
				ORDER BY provinsiNama";	
		$result = $this->getRecordset($qry,null,$this->db1);
		if($result != null) {
			$def = jsonTrueResponse($result);
		} else {
			$def = jsonFalseResponse("No province..");
		}
		return $def;
	}
	
	function getListEmptyStkByProvinceKgp($provinsiNama) {
		$qry = "SELECT * 
				FROM master_kgp_wil a 
				WHERE a.provinsiNama = '$provinsiNama' AND (loccd is null OR loccd = '')
				ORDER BY a.KotaKode";	
		$result = $this->getRecordset($qry,null,$this->db1);
		if($result != null) {
			$def = jsonTrueResponse($result);
		} else {
			$def = jsonFalseResponse("No province..");
		}
		return $def;
	}
	
	function getListStkByProvinceKgp($provinsiNama) {
		$qry = "SELECT * 
				FROM master_kgp_wil a 
				WHERE a.provinsiNama = '$provinsiNama'
				ORDER BY a.KotaKode";	
		$result = $this->getRecordset($qry,null,$this->db1);
		if($result != null) {
			$def = jsonTrueResponse($result);
		} else {
			$def = jsonFalseResponse("No province..");
		}
		return $def;
	}
	
	function updateStkByProvinceKgp($arr) {
		$db_qryx = $this->load->database('db_ecommerce', true);
		$db_qryx->trans_begin();
		foreach($arr as $data) {
			$qry = "UPDATE master_kgp_wil SET loccd = '$data[loccd]', fullnm = '$data[stkfullnm]', pricecode = '$data[pricecode]' WHERE KotaKode = '$data[KotaKode]'";
			//echo $qry."<br />";
			$db_qryx->query($qry);
		}
		if ($db_qryx->trans_status() === FALSE) {
            $db_qryx->trans_rollback();
		    $return = jsonFalseResponse("Update data stockist gagal..");
        } else {
        	$db_qryx->trans_commit();
			$return = jsonTrueResponse(null, "Update Data Stockist berhasil");
			
		}
		return $return;
	}
			
}
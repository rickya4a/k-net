<?php

    class Claim_voucher_model extends MY_Model{

        function __construct(){

            parent::__construct();

        }

        function sendTrxSMS($nama, $telp, $voucher) {

            $alternate2 = $this->load->database('alternate2', true);
            $text = 'Selamat, a/n '.$nama.' telah mendapat voucher belanja dengan nomor voucher '.$voucher;
            $insSms = "INSERT INTO outbox (DestinationNumber, TextDecoded, CreatorID, SenderID)
		                    VALUES ('".$telp."','".$text."','K-Link Transaction Status', 'MyPhone6')";
            //echo "Ins SMS : $insSms";
            $query = $alternate2->query($insSms);
        }

        function insertVoucherNo($voucher){

            //$db = $this->load->database('db_ecommerce' , true);
            $sql = "insert into db_ecommerce.dbo.fendi_tes_listvoucher (vch_no, status, nominal) values('".$voucher."','Released','100000')";

            $query = $this->db->query($sql);

//            $result = $this->getRecordset($sql,null,$this->db1);
            //echo $result;
            return true;
        }

        function insertStatus2($voucher, $nama, $email, $telp, $idmember){

            $sql = "update db_ecommerce.dbo.fendi_tes_listvoucher set
                    status = 'Identified', nama = '$nama', email = '$email', no_telp = '$telp', dfno = '$idmember'
                    where vch_no = '$voucher'";

            $query = $this->db->query($sql);

            return true;
        }

        function insertStatus3($voucher, $nama, $email, $telp){

            $sql = "update db_ecommerce.dbo.fendi_tes_listvoucher set status = 'Identified', nama = '$nama', email = '$email', no_telp = '$telp'
                    where vch_no = '$voucher'";

            $query = $this->db->query($sql);

            return true;
        }

        function getInfo(){

            $sql = "select nominal from db_ecommerce.dbo.fendi_tes_listvoucher";
            $result = $this->getRecordset($sql,null,$this->db1);
            return $result;

        }

        function getId($vch_no){

            $sql = "select id from db_ecommerce.dbo.fendi_tes_listvoucher where vch_no = '$vch_no'";
            $result = $this->getRecordset($sql,null,$this->db1);

            //echo $sql;
            return $result;
        }

        function getDetailVch($id_vch){

            $sql = "select * from db_ecommerce.dbo.fendi_tes_voucher where id = '$id_vch'";
            $result = $this->getRecordset($sql,null,$this->db1);

            //echo $sql;
            return $result;
        }

        function getBank(){
            $slc = "select * from ecomm_bank WHERE status = '1' order by bankDesc";

            $result = $this->getRecordset($slc,null,$this->db1);
            return $result;
        }

        function getListPrd25k(){

            $slc = "SELECT prdcd,
                    prdnm,
                    prdcdcat,
                    prdnmcatnm,
                    img_url,
                    price_w,
                    price_e,
                    price_cw,
                    price_ce,
                    weight
                    FROM V_Ecomm_PriceList
                    where
                    prdnm = 'GOLDEN VALLEY MINUMAN HERBAL SERBUK ROOIBOS' OR
                    prdnm = 'KOPI DURIAN'";

            $result = $this->getRecordset($slc,null,$this->db1);
            return $result;
        }

        function getListPrd50k(){

            $slc = "SELECT prdcd,
                    prdnm,
                    prdcdcat,
                    prdnmcatnm,
                    img_url,
                    price_w,
                    price_e,
                    price_cw,
                    price_ce,
                    weight
                    FROM V_Ecomm_PriceList
                    where
                    prdnm = 'K-AYUVERDA AYUARTIS 30 CAPS' or
                    prdnm = 'K-HONEY 5 IN 1'";

            $result = $this->getRecordset($slc,null,$this->db1);
            return $result;
        }

        function getListPrd(){

            $slc = "SELECT prdcd,
                    prdnm,
                    prdcdcat,
                    prdnmcatnm,
                    img_url,
                    price_w,
                    price_e,
                    price_cw,
                    price_ce,
                    weight
                    FROM V_Ecomm_PriceList
                    where
                    prdnm = 'GOLDEN VALLEY MINUMAN HERBAL SERBUK ROOIBOS' OR
                    prdnm = 'KOPI DURIAN' or
                    prdnm = 'K-AYUVERDA AYUARTIS 30 CAPS' or
                    prdnm = 'K-HONEY 5 IN 1'";

            $result = $this->getRecordset($slc,null,$this->db1);
            return $result;
        }


    }
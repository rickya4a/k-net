<?php

class CI_Klink_cart {
	public function __construct($params = array())
	{
		$this->CI =& get_instance();

		// Are any config settings being passed manually?  If so, set them
		$config = is_array($params) ? $params : array();

		// Load the Sessions class
		$this->CI->load->driver('session', $config);

		// Grab the shopping cart array from the session table
		$this->_cart_contents = $this->CI->session->userdata('cart_contents');
		if ($this->_cart_contents === NULL)
		{
			// No cart exists so we'll set some base values
			$this->_cart_contents = array('cart_total' => 0, 'total_west_price' => 0, 'total_east_price' => 0, 'total_bv' => 0, 'pricecode' => '12W3', 'total_items' => 0);
		}

		log_message('info', 'Cart Class Initialized');
	}
	
	
	
	
	protected function _update($items = array())
	{
		// Without these array indexes there is nothing we can do
		if ( ! isset($items['rowid'], $this->_cart_contents[$items['rowid']]))
		{
			return FALSE;
		}

		// Prep the quantity
		if (isset($items['qty']))
		{
			$items['qty'] = (float) $items['qty'];
			// Is the quantity zero?  If so we will remove the item from the cart.
			// If the quantity is greater than zero we are updating
			if ($items['qty'] == 0)
			{
				unset($this->_cart_contents[$items['rowid']]);
				return TRUE;
			}
		}

		// find updatable keys
		$keys = array_intersect(array_keys($this->_cart_contents[$items['rowid']]), array_keys($items));
		// if a price was passed, make sure it contains valid data
		if (isset($items['west_price']))
		{
			$items['west_price'] = (float) $items['west_price'];
		}
		
		if (isset($items['east_price']))
		{
			$items['east_price'] = (float) $items['east_price'];
		}

		// product id & name shouldn't be changed
		foreach (array_diff($keys, array('id', 'name')) as $key)
		{
			$this->_cart_contents[$items['rowid']][$key] = $items[$key];
		}

		return TRUE;
	}
	
	protected function _save_cart()
	{
		// Let's add up the individual prices and set the cart sub-total
		$this->_cart_contents['total_items'] = $this->_cart_contents['total_west_price'] = $this->_cart_contents['total_east_price'] = $this->_cart_contents['total_bv'];
		foreach ($this->_cart_contents as $key => $val)
		{
			// We make sure the array contains the proper indexes
			if ( ! is_array($val) OR ! isset($val['west_price'], $val['east_price'], $val['bv'], $val['qty']))
			{
				continue;
			}

			$this->_cart_contents['total_west_price'] += ($val['west_price'] * $val['qty']);
			$this->_cart_contents['total_east_price'] += ($val['east_price'] * $val['qty']);
			$this->_cart_contents['total_bv'] += ($val['bv'] * $val['qty']);
			$this->_cart_contents['total_items'] += $val['qty'];
			
			$this->_cart_contents[$key]['subtotal_west_price'] = ($this->_cart_contents[$key]['west_price'] * $this->_cart_contents[$key]['qty']);
			$this->_cart_contents[$key]['subtotal_east_price'] = ($this->_cart_contents[$key]['east_price'] * $this->_cart_contents[$key]['qty']);
			
		}

		// Is our cart empty? If so we delete it from the session
		if (count($this->_cart_contents) <= 2)
		{
			$this->CI->session->unset_userdata('cart_contents');

			// Nothing more to do... coffee time!
			return FALSE;
		}

		// If we made it this far it means that our cart has data.
		// Let's pass it to the Session class so it can be stored
		$this->CI->session->set_userdata(array('cart_contents' => $this->_cart_contents));

		// Woot!
		return TRUE;
	}
	public function total_bv()
	{
		return $this->_cart_contents['total_bv'];
	}
	
	public function total_west_price()
	{
		return $this->_cart_contents['total_west_price'];
	}
	
	public function total_east_price()
	{
		return $this->_cart_contents['total_east_price'];
	}
	
	public function pricecode()
	{
		return $this->_cart_contents['pricecode'];
	}
	
	public function contents($newest_first = FALSE)
	{
		// do we want the newest first?
		$cart = ($newest_first) ? array_reverse($this->_cart_contents) : $this->_cart_contents;

		// Remove these so they don't create a problem when showing the cart table
		unset($cart['total_items']);
		unset($cart['total_west_price']);
		unset($cart['total_east_price']);
		unset($cart['total_bv']);
		unset($cart['pricecode']);

		return $cart;
	}
	
	public function get_item($row_id)
	{
		return (in_array($row_id, array('total_items', 'total_west_price', 'total_east_price', 'total_bv', 'pricecode'), TRUE) OR ! isset($this->_cart_contents[$row_id]))
			? FALSE
			: $this->_cart_contents[$row_id];
	}
	
	public function destroy()
	{
		$this->_cart_contents = array('total_west_price' => 0, 'total_east_price' => 0, 'total_bv' => 0, 'total_items' => 0);
		$this->CI->session->unset_userdata('cart_contents');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function selectArray($arr) {
	$str = array();
	foreach($arr as $dta) {
		$ss = $dta->selectID;
		$dd = $dta->selectValue;
		$str[$ss] = $dd;
	}
	return $str;
}

function cleanAddress($address) {
	$alamat = str_replace(array("\n","\r","(", " ( ", "( " ,")"," ) "," )","'","*","&","â","â??", "Â"), "", $address);
	return $alamat;
}

function showListPromoAgustus2018() {
	$CI = & get_instance();
    $CI->load->model('login_model', 'login');
	$res = $CI->login->showListPromoAgustus2018();
	//echo "sd";
	//print_r($res);
    return $res;
}

function showListPromoNov2018() {

	$CI = & get_instance();
    $CI->load->model('login_model', 'login');
	$res = $CI->login->showListPromoNovember2018();
	//echo "sd";
	//print_r($res);
    return $res;
}

function sgo_iframe() {
	echo "<iframe id=\"sgoplus-iframe\" sandbox=\"allow-same-origin allow-scripts allow-top-navigation\" src=\"\" scrolling=\"no\" allowtransparency=\"true\" frameborder=\"0\" height=\"300\"></iframe>";
}



function showListFreePrdNM_Promo($listFreeProduct) {
	if($listFreeProduct != null) {
?>	
   
    <div class="col-md-6 col-xs-12">
				
		<span class="title_header">
	    	<i class="fa fa-check-square-o"></i> Pilih Salah Satu Free Product
	    </span>
	    <div class="register-top-grid">
	    	<label>Free Product :</label>
	    	<select id="free_product" name="free_product" onchange="Shopping.pilFreeProduct()">
	    		
	    			<?php
	    			 foreach($listFreeProduct as $freeprd) {
	    			 	echo "<option value=\"$freeprd->prdcd|$freeprd->prdnm|1|$freeprd->bv|$freeprd->westPrice|$freeprd->eastPrice|$freeprd->weight\">$freeprd->prdnm</option>";
	    			 }
	    			?>
	    		
	    	</select>
	    	<input type="hidden" id="free_prdcd" name="free_prdcd" value="<?php echo $listFreeProduct[0]->prdcd; ?>" />
	    	<input type="hidden" id="free_prdnm" name="free_prdnm" value="<?php echo $listFreeProduct[0]->prdnm; ?>" />
	    	<input type="hidden" id="free_qty" name="free_qty" value="1" />
	    	<input type="hidden" id="free_bv" name="free_bv" value="<?php echo $listFreeProduct[0]->bv; ?>" />
	    	<input type="hidden" id="free_westPrice" name="free_westPrice" value="<?php echo $listFreeProduct[0]->westPrice; ?>" />
	    	<input type="hidden" id="free_eastPrice" name="free_eastPrice" value="<?php echo $listFreeProduct[0]->eastPrice; ?>" />
	    	<input type="hidden" id="free_weight" name="free_weight" value="<?php echo $listFreeProduct[0]->weight; ?>" />
	    <div>
	</div>    	
<?php 
	}               	
}

function smsTemplate($no_hp, $message) {
	$url = "http://sms-api.jatismobile.com/index.ashx?";
	$urlx ="userid=klinknusan&";
	$urlx .="password=klinknusan790&";
	$urlx .="msisdn=$no_hp&";
	$clean_message = str_replace(" ", "%20", $message);
	$urlx .="message=$clean_message&";
	$urlx .="sender=K-LINK&";
	$urlx .="division=IT%20Dept&";
	$urlx .="batchname=knet&";
    $urlx .="uploadby=k-link&";
    $urlx .="channel=2";

	//create curl resource 
    $ch = curl_init();
    // set url
    $sx = $url.$urlx;
    curl_setopt($ch, CURLOPT_URL, $sx);
	//return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	// $output contains the output string 
	$output = curl_exec($ch);
	// close curl resource to free up system resources 
	$info = curl_getinfo($ch);
	$err = curl_error($ch);
	curl_close($ch);

	if ($err) {
		/*echo "<pre>";
		print_r($err);
		echo "<br />";
		print_r($info);
		echo "</pre>";*/

		return 0;
	} else {

		/*echo "<pre>";
		print_r($err);
		echo "<br />";
		print_r($info);
		echo "</pre>";*/
		return $output;
	}

}

function pricing_url($tipe_shipper) {
   if($tipe_shipper == "1") {
		$url = "http://apiv2.jne.co.id:10101/tracing/api/pricedev";
   } else if($tipe_shipper == "2") {
        $url = "202.152.20.135:8080/Service.svc/KlinkTarifResponse";
   } else if($tipe_shipper == "3") {
		$url = "";
   } else if($tipe_shipper == "4") {
		$url = "";
   }
   
   return $url;
}

function conot_url($tipe_shipper) {
   if($tipe_shipper == "1") {
		$url = "http://apiv2.jne.co.id:10101/tracing/api/generatecnote";
   } else if($tipe_shipper == "2") {
        $url = "";
   } else if($tipe_shipper == "3") {
		$url = "";
   } else if($tipe_shipper == "4") {
		$url = "";
   }
   
   return $url;	
}

function tracking_konot_url($tipe_shipper) {
   if($tipe_shipper == "1") {
		$url = "http://apiv2.jne.co.id:10101/tracing/api/list/cnote/";
   } else if($tipe_shipper == "2") {
        $url = "";
   } else if($tipe_shipper == "3") {
		$url = "";
   } else if($tipe_shipper == "4") {
		$url = "";
   }
   
   return $url;	
}

function sendSmsKnetSales($data) {
	if($data[0]->sentTo == "1") {
			$ship = 'Kode Ambil Brg: '.$data[0]->secno;
	} else {
		if($data[0]->cargo_id == "2") {
			$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/kgptracking';
		} else if($data[0]->cargo_id == "1") {
			$ship = 'Conote: '.$data[0]->conoteJNE. ', Tracking : http://bit.ly/jnetracking';
		}

	}

	//$text = 'K-LINK : Trx sukses,No Order: '.$data[0]->orderno.',Jumlah Trx: '.$data[0]->total_pay.','.$ship.'';
	$text = 'K-LINK : Trx sukses,No Order: '.$data[0]->orderno.', '.$ship.'';
	if($data[0]->log_usrlogin == $data[0]->log_dfno) {
		//$no_hp = $data[0]->tel_hp_login;
		$no_hp = "087780441874";
		smsTemplate($no_hp, $text);
	} else {
		if($data[0]->tel_hp_login != null || $data[0]->tel_hp_login != "") {
			//$no_hp = $data[0]->tel_hp_login;
			$no_hp = "087780441874";
			smsTemplate($no_hp, $text);
		}

		if($data[0]->tel_hp_as != null || $data[0]->tel_hp_as != "") {
			//$no_hp = $data[0]->tel_hp_as;
			$no_hp = "087780441874";
			smsTemplate($no_hp, $text);
		}
	}
}


function setFieldPost($fields) {
		//$data_string = json_encode($fields);
		//url-ify the data for the POST
		$fields_string = '';
		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		//rtrim($fields_string, '&');
		$fields_string = rtrim($fields_string, '&');
		return $fields_string;
	}

function ppob_generateSignature($arr) {
		$jum = count($arr);
		$str = "";
		for($i=0;$i<$jum;$i++) {
			$str .= "##".$arr[$i];
		}

		$str .= "##";
		$str = strtoupper($str);
		$str = preg_replace( "/\r|\n/", "", $str );
		//echo $str;
		return hash("sha256", $str);
	}

function ppob_generateSignatureBeforeHash($arr) {
		$jum = count($arr);
		$str = "";
		for($i=0;$i<$jum;$i++) {
			$str .= "##".$arr[$i];
		}

		$str .= "##";
		$str = strtoupper($str);
		$str = preg_replace( "/\r|\n/", "", $str );
		//echo $str;
		return $str;
	}

function ppob_set_nominal($arr) {
	$jum = preg_split('//', $arr, -1, PREG_SPLIT_NO_EMPTY);
		//print_r($jum);
	$x = count($jum);

	$tes = "";
	$akhir = $x - 2;
	for($i=0;$i<$akhir;$i++) {
		$tes .= $jum[$i];
	}
	return $tes.".".$jum[$akhir].$jum[$akhir-1];
}

function show_info_ppob_payresult($res, $result) {
	//echo "<pre>";
	//print_r($res);
	//echo "</pre>";
	$newRow = "";
	if($res[0]->ppob_type == "4") {
		$header = "Transaksi Paket Data XL Co Branded";
		$name = "XL-DATA";
	} else {
		
		$header = "Transaksi PPOB";
		$name = $res[0]->product_name;
		if($res[0]->ppob_type == '3') {
			$ref_pay_id = $res[0]->reff_pay_id;
			$newRow = "<tr><th>Reff Pay</th><th>".$ref_pay_id."&nbsp;&nbsp;<a id='$ref_pay_id' class='btn btn-mini btn-danger' onclick=javascript:checkPpobStatus(this)>Check Status</a></th></tr>";
		}
	}
	$str = "";
    $str .= "<table width='70%' class='table table-bordered table-striped table-bordered'>";
	$str .= "<tr><th colspan='2'>$header</th></tr>";
    $str .= "<tr width=30%><th>Trx ID</th><th>".$res[0]->trx_id."</th></tr>";
	$str .= "<tr><th>ID Member</th><th>".$res[0]->memberid." / ".$res[0]->membername."</th></tr>";
	$str .= "<tr><th>Nama Produk</th><th>".$name."</th></tr>";
	$str .= "<tr><th>Cust ID</th><th>".$res[0]->cust_no."</th></tr>";
	$str .= "<tr><th>Nominal</th><th>".$res[0]->nominal."</th></tr>";
	$str .= $newRow;
    $str .= "</table>";
  return $str;
}

function show_info_inquiry_ppob($result) {
  $str = "";

  if($result != null) {
	  if($result->error_code == "0000") {
	  	$str .= "<table width='70%' class='table table-bordered table-striped'>";
	  	$header = "Susah";

		if(property_exists($result->data, "card_no")) {
		//if(in_array("card_no", $result->data)) {
			$header = "Tagihan Kartu Kredit";
		} else if(property_exists($result->data, "phone_no")) {
			$header = "Tagihan Pulsa Pra/Pasca Bayar";
		} else if(property_exists($result->data, "meter_serial") && property_exists($result->data, "class")) {
			$dtax = $result->data;
			$header = "Tagihan PLN Prabayar";
			$str .= "<tr><th colspan=2>$header</th></tr>";
			$str .= "<tr><td width=30%>No Pelanggan</td><td>$dtax->customer_id</td></tr>";
			$str .= "<tr><td width=30%>Nama Customer</td><td>$dtax->customer_name</td></tr>";
			$str .= "<tr><td width=30%>Tipe VA</td><td>$dtax->class</td></tr>";

		} else if(property_exists($result->data, "fare_power") && property_exists($result->data, "bill_period")) {
			$dtax = $result->data;
			$header = "Tagihan PLN Pasca Bayar";
			$str .= "<tr><th colspan=2>$header</th></tr>";
			$str .= "<tr><td width=30%>No Pelanggan</td><td>$dtax->customer_id</td></tr>";
			$str .= "<tr><td width=30%>Nama Customer</td><td>$dtax->customer_name</td></tr>";
			$str .= "<tr><td width=30%>Tipe VA</td><td>$dtax->fare_power</td></tr>";
			$str .= "<tr><td width=30%>Periode</td><td>$dtax->bill_period</td></tr>";
		} else if(property_exists($result->data, "train_number")) {
			$dtax = $result->data;
			$header = "Tiket Kereta Api";
			$str .= "<tr><th colspan=2>$header</th></tr>";
			$str .= "<tr><td width=30%>No Tiket</td><td>$dtax->ticket_no</td></tr>";
			$str .= "<tr><td width=30%>Nama Penumpang</td><td>$dtax->passenger_name</td></tr>";
			$str .= "<tr><td width=30%>Kereta</td><td>$dtax->train_name / $dtax->train_number</td></tr>";
			$str .= "<tr><td width=30%>No Kursi</td><td>$dtax->train_seat</td></tr>";
		}
		$nominal = ppob_set_nominal($result->amount);

		$str .= "<tr><td width=30%>Nominal</td><td>$nominal</td></tr>";
		/*foreach($result->data as $key => $value) {
			$str .= "<tr><td width=30%>$key</td><td>$value</td></tr>";
		}*/

		 $str .= "</table>";
	  } else {
		$str .= "<div class='alert alert-error'>".$result->error_desc."<div>";
	  }

  } else {
  	$str .= "Zero response";
  }

  return $str;
}


function callApi($api_type='', $api_activity='', $api_input=''){
	$data = array();
	$result = http_post_form("https://api.falconide.com/falconapi/web.send.rest", $api_input);
	return $result;
}

function http_post_form($url,$data,$timeout=20){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);
	$result = curl_exec($ch);
	$result = curl_error($ch) ? curl_error($ch) : $result;
	curl_close($ch);
	return $result;
}


function addRangeOneMonth($tes, $i) {
	$date = new DateTime($tes);
    $date->modify("first day of +$i month");
    return $date->format('d/m/Y');
}

function reduceRangeOneMonth($tes, $i) {
	$date = new DateTime($tes);
    $date->modify("first day of -$i month");
    return $date->format('d/m/Y');
}

function addNewRangeOneMonth($tes, $i) {
	$date = new DateTime($tes);
    $date->modify("first day of $i month");
    return $date->format('d/m/Y');
}

function getTglBatasBnsSebelumnya() {
	return 4;
}

function getRangeBonusMonth() {
	$tgl = date("j");
	$batas = getTglBatasBnsSebelumnya();
	if($tgl > $batas) {
		$rangePeriodBns = 0;
	} else {
		$rangePeriodBns = 1;
	}
	return $rangePeriodBns;
}

function setDatatable() {
		echo "<script>
				$(document).ready(function() {
					$(All.get_active_tab() + \" .datatable\").dataTable({
						\"aLengthMenu\" : [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
						\"sPaginationType\" : \"bootstrap\",
						\"oLanguage\" : {
						},
						\"bDestroy\" : true
					});
					$(All.get_active_tab() + \" .datatable\").removeAttr('style');
				});
			</script>";
	}

function loopMenu($data, $parent) {
		if(isset($data[$parent]))  { // jika ada anak dari menu maka tampilkan
	 	    /* setiap menu ditampilkan dengan tag <ul> dan apabila nilai $parent bukan 0 maka sembunyikan element
	 	     * karena bukan merupakan menu utama melainkan sub menu */
	 	  	$str = '<ul parent="'.$parent.'" style="display:'.($parent>0?'none':'').'">';
	 	  	foreach($data[$parent] as $value)  {
	 	  	  /* variable $child akan bernilai sebuah string apabila ada sub menu dari masing-masing menu utama
	 	  	   * dan akan bernilai negatif apabila tidak ada sub menu */
	 	  	  $child = loopMenu($data,$value->id);
	 	  	  $str .= '<li>';
	 	  	  /* beri tanda sebuah folder dengan warna yang mencolok apabila terdapat sub menu di bawah menu utama
	 	  	   * dan beri juga event javascript untuk membuka sub menu di dalamnya */
	 	  	  $str .= ($child) ? '<a href="javascript:openTree('.$value->id.') "><img src="asset/image/folderclose2.jpg" id="img'.$value->id.'" border="0"></a>' : '<img src="asset/image/folderclose1.jpg">';
	 	  	  if($value->url_laravel == "#") {
			    $str .= '<a href="javascript:openTree('.$value->id.') ">'.$value->name.'</a></li>';
			  }
			  else {
			    $str .= '<a href="'.$value->url_laravel.'">'.$value->name.'</a></li>';
			  }
	 	  	  if($child) $str .= $child;
			}
			$str .= '</ul>';
			return $str;
		  }
		  else
	    return false;
	}

if ( ! function_exists('jsonTrueResponse'))
{
	function jsonTrueResponse($data = null, $message = "success") {
	 
     	$arr = array("response" => "true", "arrayData" => $data, "message" => $message);
		//$arr2 = array_map('utf8_encode', $arr);
		return $arr;
     }
}

if ( ! function_exists('randomNumber'))
{
    function randomNumber($length) {
	    $min = 1 . str_repeat(0, $length-1);
	    $max = str_repeat(9, $length);
	    $ss = mt_rand($min, $max);
		return date("y")."".date("m").$ss;
	}
}

if ( ! function_exists('createRandomNumber'))
{
    function createRandomNumber($prefix, $length) {
    	$lebar = strlen($prefix);
		$length = $length  - $lebar;
	    $min = 1 . str_repeat(0, $length-1);
	    $max = str_repeat(9, $length);
	    $ss = mt_rand($min, $max);
		return trim($prefix.$ss);
	}
}

if ( ! function_exists('substrwords'))
{
	function substrwords($text, $maxchar, $end=' ...') {
	    if (strlen($text) > $maxchar || $text == '') {
	        $words = preg_split('/\s/', $text);
	        $output = '';
	        $i      = 0;
	        while (1) {
	            $length = strlen($output)+strlen($words[$i]);
	            if ($length > $maxchar) {
	                break;
	            }
	            else {
	                $output .= " " . $words[$i];
	                ++$i;
	            }
	        }
	        $output .= $end;
	    }
	    else {
	        $output = $text;
	    }
	    return $output;
	}
}

if ( ! function_exists('jsonFalseResponse'))
{
     function jsonFalseResponse($message = "No result found..!!") {
	 	$arr = array("response" => "false", "message" => $message);
		return $arr;
	 }
}

if ( ! function_exists('sessionExpireMessage'))
{
     function sessionExpireMessage($arrs = true) {
     	$message = "Session Expired, please re-login";
	 	if($arrs) {
	    	$arr = array("response" => "false", "message" => $message);
		} else {
			$arr = "<div class=\"alert alert-error\"><p align=center>".$message."</p></div>";
		}
		return $arr;
	 }
}

if ( ! function_exists('setErrorMessage'))
{
    function setErrorMessage($message = "No result found")
    {
       echo "<div class=\"alert alert-error\"><p align=center>".$message."</p></div>";
    }
}

if ( ! function_exists('setSuccessMessage'))
{
    function setSuccessMessage($message)
    {
        echo "<div class=\"alert alert-success\"><p align=center>".$message."</p></div>";

    }
}


if ( ! function_exists('requiredFieldMessage'))
{
     function requiredFieldMessage($message = "Please fill the REQUIRED field") {
		return $message;
	 }
}

if ( ! function_exists('dataNotFoundMessage'))
{
     function dataNotFoundMessage($message = "Data yang dicari tidak ada..!!") {
		return $message;
	 }
}

if(! function_exists('placeholderCheck')) {
    function placeholderCheck() {
        $str = "required (press TAB after typing to check data)";
        return $str;
    }
}

if(! function_exists('inputText')) {
	function inputText($arr) {
		$setPlaceHolder = "";
		$setEvent = "";
		$maxlength = "";
		$setValue = "";
		$readOnly = "";
		$maxlength = "";
		if(isset($arr['readonly'])) {
			$readOnly = "readonly=readonly";
		}

		if(isset($arr['type'])) {
			$inpType = " type=\"$arr[type]\"";
		} else {
			$inpType = "type=\"text\"";
		}

		if(isset($arr['class'])) {
			$class = "";
			if($arr['class'] != "") {
				$class = " class=\"$arr[class]\"";
			}

		} else {
			$class = "class= \"span9 typeahead\"";
		}

		if(isset($arr['addClass'])) {
			$class .= $class." ".$arr['addClass'];
		}
		if(isset($arr['maxlength'])) {
			$maxlength  = " maxlength=\"$arr[maxlength]\"";
		}
		if(isset($arr['placeholder'])) {
			$setPlaceHolder = "placeholder=\"$arr[placeholder]\"";
		}
		if(isset($arr['event'])) {
			$setEvent = $arr['event'];
		}

		if(isset($arr['style'])) {
			$style = " style=\"$arr[style]\"";
		} else {
			$style = "";
		}

		if(isset($arr['value'])) {
			$setValue = $arr['value'];
		}

		if(isset($arr['maxlength'])) {
			$maxlength = "maxlength=\"$arr[maxlength]\"";
		}
		$str = "<label class=\"control-label\" for=\"typeahead\">$arr[labelname]</label>";
        $str .=  "<div class=\"controls\">";
        if(isset($arr['hiddentext'])) {
		  $str .=  "<input type=\"hidden\" id=\"$arr[hiddentext]\" name=\"$arr[hiddentext]\"  />";
		}
        $str .=  "<input $style $setPlaceHolder $maxlength $inpType $class id=\"$arr[fieldname]\"  name=\"$arr[fieldname]\" value=\"$setValue\" $readOnly $setEvent />";
		$str .=  "</div>";

		return $str;
	}
}

if(! function_exists('status_arr')) {
	function status_arr() {
		$arr = array(
		  "1" => "Yes",
		  "0" => "No"
		);
		return $arr;
	}
}

if(! function_exists('inputDoubleSelect')) {
	function inputDoubleSelect($arr, $arr2) {
		$setEvent = "";
		$setValue = "";
		$readOnly = "";
		$class = "";


		$setEvent2 = "";
		$setValue2 = "";
		$readOnly2 = "";
		$nextLabel = "";
		$class2 = "";
		//readonly
		if(isset($arr['readonly'])) {
			$readOnly = "readonly=readonly";
		}

		if(isset($arr2['readonly'])) {
			$readOnly2 = "readonly=readonly";
		}

		//class
		if(!isset($arr['class'])) {
			$class =  "";
		} else {
			$class =  " class=\"$arr[class]\"";
		}
		if(!isset($arr2['class'])) {
			$class2 =  "";
		} else {
			$class2 = " class=\"$arr2[class]\"";
		}

		//event
		if(isset($arr['event'])) {
			$setEvent = $arr['event'];
		}
		if(isset($arr2['event'])) {
			$setEvent2 = $arr2['event'];
		}
		//event

		//value
		if(isset($arr['value'])) {
			$setValue = $arr['value'];
		}
		if(isset($arr2['value'])) {
			$setValue2 = $arr2['value'];
		}
		//

		if(isset($arr2['labelname'])) {
			$nextLabel = "&nbsp;&nbsp;$arr2[labelname]&nbsp;&nbsp;";
		} else {
			$nextLabel = "";
		}

		$str = "<label class=\"control-label\" for=\"typeahead\">$arr[labelname]</label>";
        $str .=  "<div class=\"controls\">";
        //$str .=  "<input $style $setPlaceHolder $maxlength $inpType $class $width id=\"$arr[fieldname]\"  name=\"$arr[fieldname]\" value=\"$setValue\" $readOnly $setEvent />";
		$str .=  "<select id=\"$arr[fieldname]\" name=\"$arr[fieldname]\">";
		if(isset($arr['options'])) {
			foreach($arr['options'] as $key => $value) {
				$str .= "<option value=\"$key\">$value</option>";
			}
		}
		$str .=  "</select>";
		$str .= "<span style=\"width:200px;\">$nextLabel</span>";
		$str .=  "<select id=\"$arr2[fieldname]\" name=\"$arr2[fieldname]\">";
		if(isset($arr2['options'])) {
			foreach($arr2['options'] as $key => $value) {
				$str .= "<option value=\"$key\">$value</option>";
			}
		}
		$str .=  "</select>";

		// .=  "<input $style2 $setPlaceHolder2 $maxlength2 $inpType2 $class2 $width2 id=\"$arr2[fieldname]\"  name=\"$arr2[fieldname]\" value=\"$setValue2\" $readOnly2 $setEvent2 />";
		$str .=  "</div>";
		return $str;
	}
}

if(! function_exists('inputDoubleText')) {
	function inputDoubleText($arr, $arr2) {
		$setPlaceHolder = "";
		$setEvent = "";
		$maxlength = "";
		$setValue = "";
		$readOnly = "";
		$class = "";

		$setPlaceHolder2 = "";
		$setEvent2 = "";
		$maxlength2 = "";
		$setValue2 = "";
		$readOnly2 = "";
		$nextLabel = "";
		$class2 = "";
		//readonly
		if(isset($arr['readonly'])) {
			$readOnly = "readonly=readonly";
		}

		if(isset($arr2['readonly'])) {
			$readOnly2 = "readonly=readonly";
		}
		//
		//type
		if(isset($arr['type'])) {
			$inpType = " type=\"$arr[type]\"";
		} else {
			$inpType = " type=\"text\"";
		}
		if(isset($arr2['type'])) {
			$inpType2 = " type=\"$arr2[type]\"";
		} else {
			$inpType2 = " type=\"text\"";
		}
		//class
		if(!isset($arr['class'])) {
			$class =  "";
		} else {
			$class =  " class=\"$arr[class]\"";
		}
		if(!isset($arr2['class'])) {
			$class2 =  "";
		} else {
			$class2 = " class=\"$arr2[class]\"";
		}
		//
		//width
		if(!isset($arr['width'])) {
			$width = "";
		} else {
			$width = " width=\"$arr[width]\"";
		}
		if(!isset($arr2['width'])) {
			$width2 = "";
		} else {
			$width2 = " width=\"$arr2[width]\"";
		}
		//
		//placeholder
		if(isset($arr['placeholder'])) {
			$setPlaceHolder = "placeholder=\"$arr[placeholder]\"";
		}
		if(isset($arr2['placeholder'])) {
			$setPlaceHolder2 = "placeholder=\"$arr2[placeholder]\"";
		}
		//

		//event
		if(isset($arr['event'])) {
			$setEvent = $arr['event'];
		}
		if(isset($arr2['event'])) {
			$setEvent2 = $arr2['event'];
		}
		//event

		//value
		if(isset($arr['value'])) {
			$setValue = $arr['value'];
		}
		if(isset($arr2['value'])) {
			$setValue2 = $arr2['value'];
		}
		//
		//maxlength
		if(isset($arr['maxlength'])) {
			$maxlength = "maxlength=\"$arr[maxlength]\"";
		}
		if(isset($arr2['maxlength'])) {
			$maxlength2 = "maxlength=\"$arr2[maxlength]\"";
		}
		//
		//style
		if(isset($arr['style'])) {
			$style = " style=\"$arr[style]\"";
		} else {
			$style = "";
		}
		if(isset($arr2['style'])) {
			$style2 = " style=\"$arr2[style]\"";
		} else {
			$style2 = "";
		}
		//



		if(isset($arr2['labelname'])) {
			$nextLabel = "&nbsp;&nbsp;$arr2[labelname]&nbsp;&nbsp;";
		} else {
			$nextLabel = "";
		}

		$str = "<label class=\"control-label\" for=\"typeahead\">$arr[labelname]</label>";
        $str .=  "<div class=\"controls\">";
        $str .=  "<input $style $setPlaceHolder $maxlength $inpType $class $width id=\"$arr[fieldname]\"  name=\"$arr[fieldname]\" value=\"$setValue\" $readOnly $setEvent />";
		$str .= "$nextLabel";
		$str .=  "<input $style2 $setPlaceHolder2 $maxlength2 $inpType2 $class2 $width2 id=\"$arr2[fieldname]\"  name=\"$arr2[fieldname]\" value=\"$setValue2\" $readOnly2 $setEvent2 />";
		$str .=  "</div>";
		return $str;
	}
}

if(!function_exists('opening_form')) {
    function opening_form($arr) {
    	$id = "";
		$enctype = "";
		$class = "class=form-horizontal";
		if(isset($arr['id'])) {
			$id = "id=".$arr['id'];
		}

		if(isset($arr['class'])) {
			$class = "class=".$arr['class'];
		}

		if(isset($arr['enctype']) && $arr['enctype'] == true) {
			$enctype = "enctype=multipart/form-data";
		}
		$str =  "<div class=\"mainForm\"><form $id $class $enctype $id>";
        $str .= "<fieldset><div class=\"control-group\">";
		return $str;
	}

}

if(!function_exists('closing_form')) {
    function closing_form() {
    	$str =  "</div> <!-- end control-group --></fieldset></form><div class=\"result\"></div></div>";
        return $str;
	}

}



if(! function_exists('inputHidden')) {
    function inputHidden($field, $value = null) {
    	$str =  "<input type=\"hidden\" id=\"$field\" name=\"$field\" value=\"$value\" />";
		return $str;
	}
}

if(! function_exists('emptyResultDiv')) {
    function emptyResultDiv($message = "No result found..!") {
        $str = "<div class=\"alert\">$message</div>";
        return $str;
    }
}

if(! function_exists('text_cut')) {
    function text_cut($text, $length = 200, $dots = true) {
        $text = trim(preg_replace('#[\s\n\r\t]{2,}#', ' ', $text));
        $text_temp = $text;
        while (substr($text, $length, 1) != " ") { $length++; if ($length > strlen($text)) { break; } }
        $text = substr($text, 0, $length);
        return $text . ( ( $dots == true && $text != '' && strlen($text_temp) > $length ) ? '...' : '');
    }
}

if(! function_exists('selectFlagActive')) {
	function selectFlagActive($labelName = "Active", $fieldname = "act") {
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"$fieldname\" name=\"$fieldname\">";
        $str .= "<option value=\"1\">Yes</option>";
        $str .= "<option value=\"0\">No</option>";
        $str .= "</select>";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('selectTwoOption')) {
	function selectTwoOption($labelName = "Active", $fieldname = "act", $arr) {
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"$fieldname\" name=\"$fieldname\">";
		$str .= $arr;
        $str .= "</select>";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('inputSelectArray')) {
	function inputSelectArray($labelName = "Active", $fieldname = "act", $arr, $event = null) {
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"$fieldname\" name=\"$fieldname\" $event>";
		$str .= "<option value=\"\">--Select here--</option>";
		foreach($arr as $key => $value) {
		$str .= "<option value=\"$key\">$value</option>";
		}
        $str .= "</select>";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('setSingleUploadFile')) {
	function setSingleUploadFile($labelName, $fileTitle = FALSE) {
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .=  "<div class=\"controls\">";
        $str .= "<input id=\"myfile\" type=\"file\" name=\"myfile\" class=\"cfile span7 typeahead\" />";
             if($fileTitle) {
			 	$str .= "<input placeholder=\"Title of Image, max length is 50 characters\" id=\"imageTitle\" type=\"text\" name=\"imageTitle\" class = \"span9 typeahead\" maxlength=\"50\" />";
			 }
			 $str .= "<span id=\"spanPic\" class=\"fileExistingInfo\"></span>";
             $str .= "<input type=\"hidden\" class=\"fileHiddenExistingInfo\" id=\"filename\" name=\"filename\" />";

        $str .=  "</div>";

		return $str;
	}
}

if(! function_exists('setSingleUploadFile2')) {
	function setSingleUploadFile2($labelName, $fileTitle = FALSE) {
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .=  "<div class=\"controls\">";
        $str .= "<input id=\"myfile\" type=\"file\" name=\"myfile\" class=\"cfile span7 typeahead\" />";
             if($fileTitle) {
			 	$str .= "<input placeholder=\"Rename file name\" id=\"imageTitle\" type=\"text\" name=\"imageTitle\" class = \"span9 typeahead\" maxlength=\"50\" />";
			 }
			 $str .= "<span id=\"spanPic\" class=\"fileExistingInfo\"></span>";
             $str .= "<input type=\"hidden\" class=\"fileHiddenExistingInfo\" id=\"filename\" name=\"filename\" />";

        $str .=  "</div>";

		return $str;
	}
}

if(! function_exists('monthYearPeriod')) {
	function monthYearPeriod($labelName = "Period") {
		$date = date('Y');
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"month\" name=\"month\" style='width:100px'>";
		$str .= "<option value=\"1\">January</option>";
		$str .= "<option value=\"2\">February</option>";
		$str .= "<option value=\"3\">March</option>";
		$str .= "<option value=\"4\">April</option>";
		$str .= "<option value=\"5\">May</option>";
		$str .= "<option value=\"6\">June</option>";
		$str .= "<option value=\"7\">July</option>";
		$str .= "<option value=\"8\">August</option>";
		$str .= "<option value=\"9\">September</option>";
		$str .= "<option value=\"10\">October</option>";
		$str .= "<option value=\"11\">November</option>";
		$str .= "<option value=\"12\">December</option>";
        $str .= "</select>";
		 $str .= "&nbsp;<input type=text id=year name=year value=\"$date\" style='width:50px' />";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('monthYearPeriod2')) {
	function monthYearPeriod2($labelName = "Period") {
		$date = date('Y');
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"month\" name=\"month\" style='width:100px'>";
		$str .= "<option value=\"01\">January</option>";
		$str .= "<option value=\"02\">February</option>";
		$str .= "<option value=\"03\">March</option>";
		$str .= "<option value=\"04\">April</option>";
		$str .= "<option value=\"05\">May</option>";
		$str .= "<option value=\"06\">June</option>";
		$str .= "<option value=\"07\">July</option>";
		$str .= "<option value=\"08\">August</option>";
		$str .= "<option value=\"09\">September</option>";
		$str .= "<option value=\"10\">October</option>";
		$str .= "<option value=\"11\">November</option>";
		$str .= "<option value=\"12\">December</option>";
        $str .= "</select>";
		 $str .= "&nbsp;<input type=text id=year name=year value=\"$date\" style='width:50px' />";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('bonusPeriod')) {
	function bonusPeriod($labelName = "Bonus Period") {
		$date = date('Y');
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"bnsmonth\" name=\"bnsmonth\" style='width:120px'>";
		$str .= "<option value=\"\">--Select Here--</option>";
		$str .= "<option value=\"01\">January</option>";
		$str .= "<option value=\"02\">February</option>";
		$str .= "<option value=\"03\">March</option>";
		$str .= "<option value=\"04\">April</option>";
		$str .= "<option value=\"05\">May</option>";
		$str .= "<option value=\"06\">June</option>";
		$str .= "<option value=\"07\">July</option>";
		$str .= "<option value=\"08\">August</option>";
		$str .= "<option value=\"09\">September</option>";
		$str .= "<option value=\"10\">October</option>";
		$str .= "<option value=\"11\">November</option>";
		$str .= "<option value=\"12\">December</option>";
        $str .= "</select>";
		 $str .= "&nbsp;<input type=text id=bnsyear name=bnsyear value=\"$date\" style='width:50px' autocomplete=off/>";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('bonusPeriodAll')) {
	function bonusPeriodAll($labelName = "Bonus Period") {
		$date = date('Y');
		$str = "<label class=\"control-label\" for=\"typeahead\">$labelName</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select id=\"bnsmonth\" name=\"bnsmonth\" style='width:120px'>";
		$str .= "<option value=\"all\">All</option>";
		$str .= "<option value=\"01\">January</option>";
		$str .= "<option value=\"02\">February</option>";
		$str .= "<option value=\"03\">March</option>";
		$str .= "<option value=\"04\">April</option>";
		$str .= "<option value=\"05\">May</option>";
		$str .= "<option value=\"06\">June</option>";
		$str .= "<option value=\"07\">July</option>";
		$str .= "<option value=\"08\">August</option>";
		$str .= "<option value=\"09\">September</option>";
		$str .= "<option value=\"10\">October</option>";
		$str .= "<option value=\"11\">November</option>";
		$str .= "<option value=\"12\">December</option>";
        $str .= "</select>";
		 $str .= "&nbsp;<input type=text id=bnsyear name=bnsyear value=\"$date\" style='width:50px' />";
        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('inputCountryHQBranch')) {
	function inputCountryHQBranch() {
		 $str = "";
		 $country_id = null;
		 $hq_id = null;
		 $branch_id = null;
		 $country_id = array(
		 	"labelname" => "Country ID",
		 	"fieldname" => "country_id",
		 	"value" => "ID",
		 	"readonly" => "yes"
 		 );
         $str .= inputText($country_id);

		 //Headquarter ID
		 $hq_id = array(
		 	"labelname" => "Headquarter ID",
		 	"fieldname" => "hq_id",
		 	"value" => "BID06",
		 	"readonly" => "yes"

 		 );
          $str .= inputText($hq_id);

		  //Headquarter ID
		 $branch_id = array(
		 	"labelname" => "Branch ID",
		 	"fieldname" => "branch_id",
		 	"value" => "B001",
		 	"readonly" => "yes"
 		 );
          $str .= inputText($branch_id);

		  return $str;
	}
}

if(! function_exists('inputSelect')) {
	function inputSelect($arr) {
		$setClass = "";
		$setEvent = "";
		if(isset($arr['event'])) {
			$setEvent .= $arr['event'];
		}

		if(isset($arr['class'])) {
			$setClass .= $arr['class'];
		}

		if(isset($arr['addClass'])) {
			$setClass .= $setClass." ".$arr['addClass'];
		}

		$str = "<label class=\"control-label\" for=\"typeahead\">$arr[labelname]</label>";
        $str .= "<div class=\"controls\">";
        $str .= "<select class=\"$setClass\" id=\"$arr[fieldname]\" name=\"$arr[fieldname]\" $setEvent>";
        $str .= "<option value=\"\">--Select here--</option>";
        $str .= $arr['optionlist'];
        $str .= "</select>";
		if(isset($arr['refresh'])) {
			$str .= "&nbsp;<input class=\"btn btn-mini btn-primary\" type=\"button\" onclick=\"$arr[refresh]\" value=\"Refresh\">";
		}
		if(isset($arr['submit'])) {
			$str .= "&nbsp;<input class=\"btn btn-mini btn-primary\" type=\"button\" onclick=\"$arr[submit]\" value=\"Submit\">";
		}

        $str .= "</div>";

		return $str;
	}
}

if(! function_exists('btnUpdateDelete')) {
	function btnUpdateDelete($arr) {
		$html = "";
		$html .= "<td><div align=\"center\">";
		if(isset($arr['view'])) {
		    $html .= "<a class=\"btn btn-mini btn-success\" onclick=\"$arr[view]\"><i class=\"icon-search icon-white\"></i></a>&nbsp;";
		}
		if(isset($arr['update'])) {
        	$html .= "<a class=\"btn btn-mini btn-info\" onclick=\"$arr[update]\"><i class=\"icon-edit icon-white\"></i></a>&nbsp;";
		}
		if(isset($arr['delete'])) {
        	$html .= "<a class=\"btn btn-mini btn-danger\" onclick=\"$arr[delete]\"><i class=\"icon-trash icon-white\"></i></a>";
		}
        $html .=  "</div></td>";
		return $html;
	}
}



if(! function_exists('button_set')) {
    function button_set($save, $update, $view, $cancel = "All.cancelUpdateForm()") {
      $html = "";
      $html .= "<label class=\"control-label\" for=\"typeahead\">&nbsp</label> ";
      $html .= "<div class=\"controls\"  id=\"inp_btn\">";
      $html .= "<input type=\"button\" id=\"btn_input_user\" class=\"btn btn-primary .submit\" name=\"save\" value=\"Submit\" onclick=\"$save\" />&nbsp;";
      $html .= "<input type=\"reset\" class=\"btn btn-reset\" value=\"Reset\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-success\" value=\"View List\" onclick=\"$view\" />";
      $html .= "</div><div class=\"controls\" id=\"upd_btn\" style=\"display: none;\">";
      $html .= "<input type=\"button\" class=\"btn btn-primary\" id=\"updsave\" name=\"save\" value=\"Update\" onclick=\"$update\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-danger\" value=\"Cancel Update\" id=\"cancelupd\" onclick=\"$cancel\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-success\" value=\"View List\" onclick=\"$view\" /></div>";

	  return $html;
    }
}

if(! function_exists('button_set2')) {
    function button_set2($save) {
      $html = "";
      $html .= "<label class=\"control-label\" for=\"typeahead\">&nbsp</label> ";
      $html .= "<div class=\"controls\"  id=\"inp_btn\">";
      $html .= "<input type=\"button\" id=\"btn_input_user\" class=\"btn btn-primary .submit\" name=\"save\" value=\"Submit\" onclick=\"$save\" />&nbsp;";
	   $html .= "<input type=\"reset\" class=\"btn btn-reset\" name=\"reset\" value=\"Reset\" />&nbsp;";
      $html .= "</div>";

	  return $html;
    }
}

if(! function_exists('button_set_no_visible')) {
    function button_set_prdprice($save, $update, $view, $cancel = "Product.cancelUpdatePrdPrice()") {
      $html = "";
      $html .= "<label class=\"control-label\" for=\"typeahead\" \">&nbsp</label> ";
      $html .= "<div class=\"controls\"  id=\"inp_btn\" style=\"display: block;\">";
      $html .= "<input type=\"button\" id=\"btn_input_user\" class=\"btn btn-primary .submit\" name=\"save\" value=\"Save\" onclick=\"$save\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-reset\" value=\"Reset\" onclick=\"$cancel\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-success\" value=\"View List\" onclick=\"$view\" />";
      $html .= "</div><div class=\"controls\" id=\"upd_btn\" style=\"display: none;\">";
      $html .= "<input type=\"button\" class=\"btn btn-primary\" id=\"updsave\" name=\"save\" value=\"Update\" onclick=\"$update\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-danger\" value=\"Cancel Update\" id=\"cancelupd\" onclick=\"$cancel\" />&nbsp;";
      $html .= "<input type=\"button\" class=\"btn btn-success\" value=\"View List\" onclick=\"$view\" /></div>";

	  return $html;
    }
}

if(! function_exists('datepickerFromTo')) {
	function datepickerFromTo($label, $from = "from", $to = "to") {
	  $html = "";
	  $html .= "<label class=\"control-label\" for=\"typeahead\">$label</label>";
      $html .= "<div class=\"controls\">";
      $html .= "<input type=\"text\" class=\"dtpicker\" id=\"$from\" name=\"$from\" placeholder=\"From\" required=\"required\" />";
      $html .= "&nbsp;<input type=\"text\" class=\"dtpicker\" id=\"$to\" name=\"$to\" placeholder=\"To\" required=\"required\" />";
      $html .= "</div>";
	  return $html;
	}
}



if(! function_exists('set_list_array_to_string2')) {
    function set_list_array_to_string2($array, $fieldname, $roundby = "'" , $delimiter = ",")
    {
        $ss = '';
        //$jum = count($array);
        foreach($array as $dta)
        {
            $ss .= $roundby.$dta[$fieldname].$roundby.$delimiter." ";
        }
        $ss = substr($ss, 0, -2);
        return $ss;
    }
}

if(! function_exists('getUsername')) {
    function getUsername()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info !=  NULL) {
			$username = $store_info[0]->fullnm;
			return $username;
		} else {
			return NULL;
		}
	}
}

if(! function_exists('getUserID')) {
    function getUserID()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');

		if($store_info !=  NULL) {
			$userid = $store_info[0]->dfno;
			return $userid;
		} else {
			$non_member_info =   $CI->session->userdata('non_member_info');
			if($non_member_info !=  NULL) {
				$userid = $non_member_info['userlogin'];
			   return $userid;
			} else {
				return NULL;
			}
		}
	}
}

if(! function_exists('getUserAddress')) {
    function getUserAddress()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		$usraddress = $store_info[0]->addr1." ".$store_info[0]->addr2." ".$store_info[0]->addr3;
		return $usraddress;
	}
}

if(! function_exists('getUserEmail')) {
    function getUserEmail()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info !=  NULL) {
			$email = $store_info[0]->email;
			return $email;
		} else {
			$non_member_info =   $CI->session->userdata('non_member_info');
			if($non_member_info !=  NULL) {
				$email = $non_member_info['useremail'];
			   return $email;
			} else {
				return NULL;
			}
		}
	}
}

if(! function_exists('getUserIdno')) {
    function getUserIdno()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info !=  NULL) {
			$idno = $store_info[0]->idno;
			return $idno;
		} else {
			return NULL;
		}
	}
}

if(! function_exists('getUserPhone')) {
    function getUserPhone()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info !=  NULL) {
			if($store_info[0]->tel_hp != "") {
				$telp = $store_info[0]->tel_hp;
			} else {
				$telp = $store_info[0]->tel_hm;
			}
		} else {
			$non_member_info =   $CI->session->userdata('non_member_info');
			if($non_member_info !=  NULL) {
				$telp = $non_member_info['usertelp'];
			} else {
				$telp = NULL;
			}
		}
		return $telp;
	}
}

/*tambahan dari ana*/
if(! function_exists('getUserPhonehome')) {
    function getUserPhonehome()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info != NULL) {
			$telphm = $store_info[0]->tel_hm;
            return $telphm;
		}else{
		  return NULL;
		}
	}
}

if(! function_exists('getUserNovac')) {
    function getUserNovac()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info != NULL) {
			$novac = $store_info[0]->novac;
            return $novac;
		}else{
		  return NULL;
		}
	}
}

if(! function_exists('getSponsorID')) {
    function getSponsorID()
    {
    	$sponsorid = "";
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info[0]->sponsorid != "") {
			$sponsorid = $store_info[0]->sponsorid;
		}
		return $sponsorid;
	}
}

if(! function_exists('getSponsorName')) {
    function getSponsorName()
    {
    	$sponsorname = "";
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info[0]->sponsorname != "") {
			$sponsorname = $store_info[0]->sponsorname;
		}
		return $sponsorname;
	}
}

if(! function_exists('getBirthDate')) {
    function getBirthDate()
    {
    	$birthdt = "";
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info[0]->tel_hp != "") {

			$birthdt = date('d-m-Y', strtotime($store_info[0]->birthdt));
		}
		return $birthdt;
	}
}

if(! function_exists('getUserPassword')) {
    function getUserPassword()
    {
    	$CI = & get_instance();
    	$store_info =   $CI->session->userdata('store_info');
		if($store_info !=  NULL) {
			$email = $store_info[0]->password;
			return $email;
		} else {
			$non_member_info =   $CI->session->userdata('non_member_info');
			if($non_member_info !=  NULL) {
				$email = $non_member_info['password'];
			   return $email;
			} else {
				return NULL;
			}
		}
	}
}

if(! function_exists('getBeUsername')) {
    function getBeUsername()
    {
    	$beUsername = "";
    	$CI = & get_instance();
    	$beUsername =   $CI->session->userdata('ecom_user');
		/*if($ecomm_user[0]->ecom_user != "") {
				
			$beUsername = $ecom_user[0]->ecom_user;
		} */
		return $beUsername;
	}
}

if(! function_exists('getBeGroupname')) {
    function getBeGroupname()
    {
    	$groupName = "";
    	$CI = & get_instance();
    	$groupName =   $CI->session->userdata('ecom_groupname');
		/*if($ecomm_user->ecom_groupname != "") {
				
			$groupName = $ecom_user->ecom_groupname;
		} */
		return $groupName;
	}
}


if(! function_exists('getTotalPayNet')) {
    function getTotalPayNet()
    {

    	$CI = & get_instance();
    	$pricecode =   $CI->session->userdata('pricecode');
        $personal_info  = $CI->session->userdata('personal_info');
        $is_drop=$personal_info['id_lp'];
		if($pricecode == "12W3" || $pricecode == "12W4") {
			$totalPay = $CI->cart->total_west_price();
			if($is_drop!='' && $is_drop == "DROPSHIP"){
			$totalPay = $CI->cart->total_west_Cprice();
			}
		} else {
			$totalPay = $CI->cart->total_east_price();
			if($is_drop!='' && $is_drop == "DROPSHIP"){
			$totalPay = $CI->cart->total_east_Cprice();
			}
		}
		return $totalPay;
	}
}

if(! function_exists('getTotalPayNetBaru')) {
    function getTotalPayNetBaru()
    {
    	$freeship = "1";
    	$CI = & get_instance();
    	$pricecode =   $CI->session->userdata('pricecode');
		if($pricecode == "12W3" || $pricecode == "12W4") {
			$totalPay = $CI->cart->total_west_price();
		} else {
			$totalPay = $CI->cart->total_east_price();
		}
		//return $totalPay;
		return array("freeship" => $freeship, "total_pay" => $totalPay);
	}
}


if(! function_exists('getTotalPayNetAndShipCost')) {
    function getTotalPayNetAndShipCost()
    {

    	$CI = & get_instance();
    	$pricecode =   $CI->session->userdata('pricecode');
		$promo =   $CI->session->userdata('promo');
		$shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		  if($shipping_jne_info == null) {
		  	 $shipping_jne_info['price'] = 0;
		  }

		if($pricecode == "12W3" || $pricecode == "12W4") {
			$totalPay = $CI->cart->total_west_price() + $shipping_jne_info['price'];
		} else {
			$totalPay = $CI->cart->total_east_price() + $shipping_jne_info['price'];
		}
		return $totalPay;
	}
}

if(! function_exists('getTotalPayNetAndShipCost2')) {
    function getTotalPayNetAndShipCost2()
    {
    	$idmember = getUserID();
    	$freeship = "0";
		$totDiscount = 0;
    	$CI = & get_instance();
    	$pricecode =   $CI->session->userdata('pricecode');
		$promo =   $CI->session->userdata('promo');
		$shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		   if($shipping_jne_info == null || $shipping_jne_info['price'] == 0) {
		  	 $shipping_jne_info['price'] = 0;
			 $freeship = "1";
		  } else {
		  	 //if($idmember == "IDSPAAA66834" || $idmember == "IDSPAAA91431" || $idmember == "JOKO WIYOTO") {
		  	 /*if($promo == null) {

		  	 }	else {*/
		  	 if($promo != null) {
		  	 	//if(array_key_exists("full_discount", $promo)) {
		  	 		/*if($promo['full_discount'] > 0) {
		  	 			$totDiscount = $shipping_jne_info['price'];
					} else { */
						if(array_key_exists("FREESHIP", $promo)) {

							if($promo['FREESHIP'] > 0) {
								$shipping_jne_info['price'] = $shipping_jne_info['price'] - $promo['FREESHIP'];
								if($shipping_jne_info['price'] <= 0) {
									$shipping_jne_info['price'] = 0;
								}
								$totDiscount = $promo['FREESHIP'];
							} else {

								$totDiscount = $shipping_jne_info['price'];
								$shipping_jne_info['price'] = 0;
							}
							$freeship = "1";
						}
					//}
				//}	

		  	 	/*if(array_key_exists("FREESHIP", $promo)) {

					if($promo['FREESHIP'] > 0) {
						$shipping_jne_info['price'] = $shipping_jne_info['price'] - $promo['FREESHIP'];
						if($shipping_jne_info['price'] <= 0) {
							$shipping_jne_info['price'] = 0;
						}
						$totDiscount = $promo['FREESHIP'];
					} else {

						$totDiscount = $shipping_jne_info['price'];
						$shipping_jne_info['price'] = 0;
					}
					$freeship = "1";
				} */
		  	 }


			 //}
		  }

		if($pricecode == "12W3" || $pricecode == "12W4") {
			$totalPay = $CI->cart->total_west_price() + $shipping_jne_info['price'];
		} else {
			$totalPay = $CI->cart->total_east_price() + $shipping_jne_info['price'];
		}
		return array("freeship" => $freeship, "total_pay" => $totalPay, "tot_discount" => $totDiscount);
	}
}
/*
if(! function_exists('addSCartProduct')) {
	function addSCartProduct($arr) {
		$res = array("response" => "false", "message" => "Data product ".$arr['name']." gagal dimasukkan ke dalam Cart");
		$addCart = array(array(
			'id' => $arr['id'],
			'name' => $arr['name'],
			'qty' => $arr['qty'] ,
			'bv' => $arr['bv'],
			'west_price' => $arr['west_price'],
			'east_price' => $arr['east_price'],
			'weight' => $arr['weight'],

		));
		//print_r($addCart);
		$insCart = $CI->cart->insert($addCart);
		if($insCart) {
			$res = array("response" => "true",
				"total_item" => $this->cart->total_items(),
				"total_west_price" => $this->cart->total_west_price(),
				"total_east_price" => $this->cart->total_east_price(),
				"message" => "Terima Kasih, product ".$arr['name']." sudah dimasukkan ke dalam cart");
		}

		return $res;
	}
}*/

if(! function_exists('getTotalPayNetAndShipCostTerbaru')) {
    function getTotalPayNetAndShipCostTerbaru($totDiscount = 0)
    {

    	$end_promo="2019-01-04";
    	$today= date("Y-m-d");

    	$idmember = getUserID();
    	$freeship = "0";
		$CI = & get_instance();
    	$pricecode =   $CI->session->userdata('pricecode');
		$promo =   $CI->session->userdata('promo');
		$personal_info =   $CI->session->userdata('personal_info');
		$shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		$isDrop=$personal_info['id_lp'];
		  if($shipping_jne_info == null || $shipping_jne_info['price'] == 0) {
		  	 $biaya_kirim = 0;
			 $freeship = "1";
		  } else {
		  	if($promo != null) {
		  		if(array_key_exists("full_discount", $promo)) {
		  		  //cek apakah biaya kirim FULL discount
		  		  if($promo['full_discount'] > 0) {
		  	 			//$totDiscount = $shipping_jne_info['price'];

						//$totDiscount = $promo['full_discount'];//edited vera 04/12/2018
		  	 			if($today < $end_promo){//edited vera 05/12/2018
		  	 			$totDiscount = $promo['full_discount'];
		  	 			}else{
		  	 			$totDiscount = 0;
		  	 			}

				  } else {
				  	   if(array_key_exists("FREESHIP", $promo) && $promo['FREESHIP'] > 0) {
				  	  	   //$shipping_jne_info['price'] = $shipping_jne_info['price'] - $promo['FREESHIP'];
						   $totDiscount = $promo['FREESHIP'];
					   }

				  }
				}
				$freeship = "1";
			}
		  	$biaya_kirim = $shipping_jne_info['price'] - $totDiscount;
		  }
		if($pricecode == "12W3" || $pricecode == "12W4") {
			$totalPay = $CI->cart->total_west_price() + $biaya_kirim;
			if($isDrop=='DROPSHIP'){
			$totalPay = $CI->cart->total_west_Cprice() + $biaya_kirim;
			}
		} else {
			$totalPay = $CI->cart->total_east_price()+ $biaya_kirim;
			if($isDrop=='DROPSHIP'){
			$totalPay = $CI->cart->total_east_Cprice()+ $biaya_kirim;
			}
		}
		$shipping_jne_info['ship_discount'] = $totDiscount;
		$CI->session->set_userdata('shipping_jne_info', $shipping_jne_info);
		return array("freeship" => $freeship, "total_pay" => $totalPay, "ship_price" => $shipping_jne_info['price'], "tot_discount" => $totDiscount);
	}
}

if(! function_exists('getTotalSKandShipCost')) {
    function getTotalSKandShipCost() {
    	  $CI = & get_instance();
    	  $pricecode = $CI->session->userdata('pricecode');
		  $priceSK = $CI->session->userdata('starterkit_prd');
		  $shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		  if($shipping_jne_info == null) {
		  	 $shipping_jne_info['price'] = 0;
		  }

		  $total_payment = $priceSK['price'] + $shipping_jne_info['price'];
		  return $total_payment;
    }
}

if(! function_exists('getTotalSKandShipCost2')) {
    function getTotalSKandShipCost2() {
	      $idmember = getUserID();
    	  $freeship = "0";
		  $totDiscount = 0;
    	  $CI = & get_instance();
    	  $pricecode = $CI->session->userdata('pricecode');
		  $priceSK = $CI->session->userdata('starterkit_prd');
		  $promo =   $CI->session->userdata('promo');
		  $shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		  if($shipping_jne_info == null || $shipping_jne_info['price'] == 0) {
		  	 $shipping_jne_info['price'] = 0;
			 $freeship = "1";
		  } else {
		  	 //if($idmember == "IDSPAAA66834" || $idmember == "IDSPAAA91431") {
		  	  if($promo != null) {
				if(array_key_exists("FREESHIP", $promo)) {
					if($promo['FREESHIP'] > 0) {
						$shipping_jne_info['price'] = $shipping_jne_info['price'] - $promo['FREESHIP'];
						if($shipping_jne_info['price'] <= 0) {
							$shipping_jne_info['price'] = 0;
						}
						$totDiscount = $promo['FREESHIP'];
					} else {

						$totDiscount = $shipping_jne_info['price'];
						$shipping_jne_info['price'] = 0;
					}
					$freeship = "1";
				}
			  }
			 //}
		  }

		  $total_payment = $priceSK['price'] + $shipping_jne_info['price'];
		  //return $total_payment;
		  return array("freeship" => $freeship, "total_pay" => $total_payment, "tot_discount" => $totDiscount);
    }
}


if(! function_exists('getTotalSKandShipForLP')) {
    function getTotalSKandShipForLP() {

    	  $freeship = "0";
		  $totDiscount = 0;
    	  $CI = & get_instance();
		  $CI->load->model('webshop/member_model', 'member_model');
		  $member_info = $CI->session->userdata('member_info');
    	  $pricecode = $CI->session->userdata('pricecode');
		  $priceSK = $CI->session->userdata('starterkit_prd');
		  //$promo =   $CI->session->userdata('promo');
		  $shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		  if($shipping_jne_info == null || $shipping_jne_info['price'] == 0) {
		  	 $shipping_jne_info['price'] = 0;
			 $freeship = "1";
		  } else {
		  	 //if($member_info['recruiterid'] == "IDSPAAA91431") {
		  	    $res = $CI->member_model->checkListPromo();
			    $promo = array();
				if($res != null) {
					foreach($res as $dta) {
						$key = $dta->id;
						$promo[$key] = $dta->max_discount;
						$promo['full_discount'] = $dta->full_discount;
					}
				} else {
					$promo = null;
				}

		  	 if($promo != null) {
				if(array_key_exists("FREESHIP", $promo)) {
					if($promo['FREESHIP'] > 0) {
						$shipping_jne_info['price'] = $shipping_jne_info['price'] - $promo['FREESHIP'];
						if($shipping_jne_info['price'] <= 0) {
							$shipping_jne_info['price'] = 0;
						}
						$totDiscount = $promo['FREESHIP'];
					} else {

						$totDiscount = $shipping_jne_info['price'];
						$shipping_jne_info['price'] = 0;
					}
					$freeship = "1";
				}

			 }
		  }

		  $total_payment = $priceSK['price'] + $shipping_jne_info['price'];
		  //return $total_payment;
		  return array("freeship" => $freeship, "total_pay" => $total_payment, "tot_discount" => $totDiscount);
    }
}

if(! function_exists('getTotalBV')) {
    function getTotalBV()
    {
    	$totalBV = 0;
    	$CI = & get_instance();
    	$cart_contents =   $CI->session->userdata('cart_contents');
		$totalBV = $cart_contents['total_bv'];
		return $totalBV;
	}
}


if(! function_exists('setRpcResponse')) {
    function setRPCResponse($dta, $request)
    {
    	$returnArr = array();
    	$rpc = $dta->request($request);

        if (!$dta->send_request()) {
            echo $dta->display_error();
        } else {
        	$arr = $dta->display_response();
			if($arr['arrayData'] == null) {
				$returnArr = array("response" => "false");
			} else {
	            $valReturn = json_decode($arr['arrayData']);
				$returnArr = array("response" => "true", "arrayData" => $valReturn);
			}

		    return $returnArr;
        }
	}
}

if(! function_exists('getTotalPayment')) {
	function getTotalPayment($tipe)
    {
    	  $CI = & get_instance();
    	  $pricecode = $CI->session->userdata('pricecode');
		  $shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		  if($shipping_jne_info == null) {
		  	 $shipping_jne_info['price'] = 0;
		  }

          if($pricecode == "12W3" || $pricecode == "12W4") {
          	$total_payment = $CI->cart->total_west_price() + $shipping_jne_info['price'];
          } else {
          	$total_payment = $CI->cart->total_east_price() + $shipping_jne_info['price'];
          }

		  $total_pay = $total_payment;
		  if($tipe == "cc") {
		  	//$tot = $total_pay + ((3.2 / 100) * $total_pay) + 2500;
		  	$tot = $total_pay + 2500 + (2500 * 0.1);
		  } else {
		  	$tot = $total_pay + 2500;
		  }

		  return $tot;
	}
}


if(! function_exists('getTotalPaymentSK')) {
	function getTotalPaymentSK($tipe)
    {
    	  $CI = & get_instance();
    	  $pricecode = $CI->session->userdata('pricecode');
		  $priceSK = $CI->session->userdata('starterkit_prd');
		  $shipping_jne_info = $CI->session->userdata('shipping_jne_info');
		  if($shipping_jne_info == null) {
		  	 $shipping_jne_info['price'] = 0;
		  }
          /*if($pricecode == "12W3") {
          	$total_payment = $CI->cart->total_west_price() + $shipping_jne_info['price'];
          } else {
          	$total_payment = $CI->cart->total_east_price() + $shipping_jne_info['price'];
          }
		  */
		  $total_payment = $priceSK['price'] + $shipping_jne_info['price'];
		  $total_pay = $total_payment;
		  if($tipe == "cc") {
		  	//$tot = $total_pay + ((3.2 / 100) * $total_pay) + 2500;
		  	$tot = $total_pay + 2500 + (2500 * 0.1);
		  } else {
		  	$tot = $total_pay + 2500;
		  }

		  return $tot;
	}
}



if(! function_exists('getCustomerDetail')) {
	function getCustomerDetail($tipe = null)
    {
    	  $customer_details = array();
    	  if($tipe == "lp") {
	    	  $CI = & get_instance();
	    	  $member_info = $CI->session->userdata('member_info');
	    	  $customer_details = array(
	            'first_name'    => $member_info['membername'],
	            'last_name'     => "", // Optional
	            'email'         => $member_info['email_pendaftar'],
	            'phone'         => $member_info['tel_hp']
	            );
		  }	elseif($tipe == null) {
		  		$customer_details = array(
	            'first_name'    => $member_info['membername'],
	            'last_name'     => "", // Optional
	            'email'         => $member_info['email_pendaftar'],
	            'phone'         => $member_info['tel_hp']
	            );
		  }
		  return $customer_details;
	}
}

if(! function_exists('datebirth_combo')) {
	function datebirth_combo($minimum_age = 18, $maximum_age = 100, $class = '')
    {
    	$addClass = "";
		if($class != "") {
    		$addClass = "class='$class'";
    	}
    	$str = "<select $addClass  id=tgllhr name=tgllhr>";
		for($i = 1;$i <= 31; $i++) {
    		$val = sprintf("%02s", $i);
    		$str .= "<option value=\"$val\">$val</option>";
		}
    	$str .= "</select>&nbsp;/&nbsp;";

		$str .= "<select $addClass id=blnlhr name=blnlhr>";
		for($i = 1;$i <= 12; $i++) {
    		$val = sprintf("%02s", $i);
    		$str .= "<option value=\"$val\">$val</option>";
		}
    	$str .= "</select>&nbsp;/&nbsp;";

		$year = date("Y");
		$min = $year - $minimum_age;
		$max = $year - $maximum_age;
		$str .= "<select $addClass  id=thnlhr name=thnlhr>";
		for($i = $min; $i > $max; $i--) {
    		$str .= "<option value=\"$i\">$i</option>";
		}
    	$str .= "</select>";

		return $str;
	}

if(! function_exists('datebirth_comboTabIndex')) {
	function datebirth_comboTabIndex($tabtgl, $tabbln, $tabthn, $minimum_age = 18, $maximum_age = 100, $class = '')
    {
    	$addClass = "";
		if($class != "") {
    		$addClass = "class='$class'";
    	}
    	$str = "<select $addClass  id=tgllhr name=tgllhr tabindex=\"$tabtgl\">";
		for($i = 1;$i <= 31; $i++) {
    		$val = sprintf("%02s", $i);
    		$str .= "<option value=\"$val\">$val</option>";
		}
    	$str .= "</select>&nbsp;/&nbsp;";

		$str .= "<select $addClass id=blnlhr name=blnlhr tabindex=\"$tabbln\">";
		for($i = 1;$i <= 12; $i++) {
    		$val = sprintf("%02s", $i);
    		$str .= "<option value=\"$val\">$val</option>";
		}
    	$str .= "</select>&nbsp;/&nbsp;";

		$year = date("Y");
		$min = $year - $minimum_age;
		$max = $year - $maximum_age;
		$str .= "<select $addClass  id=thnlhr name=thnlhr tabindex=\"$tabthn\">";
		for($i = $min; $i > $max; $i--) {
    		$str .= "<option value=\"$i\">$i</option>";
		}
    	$str .= "</select>";

		return $str;
	}
}


if(! function_exists('datebirth_combo1')) {
	function datebirth_combo1($minimum_age = 18, $maximum_age = 100, $class = '',$tabtgl = '',$tabbln = '',$tabthn = '')
    {
    	$addClass = "";
        $addTabTgl = "";
        $addTabBln = "";
        $addTabThn = "";
		if($class != "") {
    		$addClass = "class='$class'";
    	}
        if($tabtgl != ""){
            $addTabTgl = "tabindex='$tabtgl'";
        }
        if($tabbln != ""){
            $addTabBln = "tabindex='$tabbln'";
        }
        if($tabthn != ""){
            $addTabThn = "tabindex='$tabthn'";
        }
    	$str = "<select $addClass  id=tgllhr name=tgllhr $addTabTgl>";
		for($i = 1;$i <= 31; $i++) {
    		$val = sprintf("%02s", $i);
    		$str .= "<option value=\"$val\">$val</option>";
		}
    	$str .= "</select>&nbsp;/&nbsp;";

		$str .= "<select $addClass id=blnlhr name=blnlhr $addTabBln>";
		for($i = 1;$i <= 12; $i++) {
    		$val = sprintf("%02s", $i);
    		$str .= "<option value=\"$val\">$val</option>";
		}
    	$str .= "</select>&nbsp;/&nbsp;";

		$year = date("Y");
		$min = $year - $minimum_age;
		$max = $year - $maximum_age;
		$str .= "<select $addClass  id=thnlhr name=thnlhr $addTabThn>";
		for($i = $min; $i > $max; $i--) {
    		$str .= "<option value=\"$i\">$i</option>";
		}
    	$str .= "</select>";

		return $str;
	}
 }

if(! function_exists('set_list_array_to_string')) {
	function set_list_array_to_string($array, $roundby = "'" , $delimiter = ",")
    {
        $ss = '';
        $jum = count($array);
        for($i = 0; $i < $jum; $i++)
        {
            $ss .= $roundby.$array[$i].$roundby.$delimiter." ";
        }
        $ss = substr($ss, 0, -2);
        return $ss;
    }
}

if(! function_exists('link_sk')) {
	function link_sk() {
		$str = "<a class=\"btn1 btn2 btn-primary1\" href=\"".base_url('member/file/sk')."\">";
	    $str .= "<i class=\"fa fa-arrow-left\"></i><span>Download Starterkit</span></a>";
	    return $str;

	}
}

if(! function_exists('set_list_array_to_stringCart')) {
    function set_list_array_to_stringCart($array, $fieldname, $roundby = "'" , $delimiter = ",")
    {
        $ss = '';
        //$jum = count($array);
        foreach($array as $dta)
        {
            $ss .= $roundby.$dta[$fieldname].$roundby.$delimiter." ";
        }
        $ss = substr($ss, 0, -2);
        return $ss;
    }
}
}

    function nma($ss,$length)
    {
        $fgh = strlen($ss);
		if($fgh > $length)
        {
            $tmp = explode(" ", $ss);
           $nma = $tmp[0]." ".$tmp[1];

            return $nma;
        }

		else
		{
		   return $ss;
		}
    }

    function nama($ss,$length)
    {
        $fgh = strlen($ss);

		 if($fgh > $length)
        {
            $tmp = explode(" ", $ss);
           $nma = $tmp[0]." ".$tmp[1];

            return $nma;
        }
		else
		{
		   return $ss;
		}
    }

    function garisSambung(){
        for($i = 1;$i<=11;$i++)
   	    {
            echo "_______";
        }
    }

    function garisStrip()
	{
	   for($i = 1;$i<=11;$i++)
   	    {
            echo "-------";
        }
	}
    function garisStripKW() {
		for($i = 1;$i<=8;$i++)
        	{
        	  echo "----------";
        	}
	}
    function garisStrip2()
	{
	   for($i = 1;$i<=2;$i++)
   	   {
            echo "----------";
       }
	}

    function garisStrip3()
	{
	   for($i = 1;$i<=8;$i++)
   	   {
            echo "--------";
       }
	}

    function TotQty($pengurang, $x)
	{
	    $kos = '';
    	$d = strlen($x);
    	$kiri = $pengurang - $d;
    	for($v = 1;$v <= $kiri;$v++)
    	{
    	  $kos .= " ";
    	}

    	echo $kos;
    	echo $x;
	}

    function tmbh_spaceHeader($value)
	{
	  $bts_kiri = 11;
      $data_kiri = 38;
      $bts_kanan = 13;
      $data_kanan = 18;

      $sisa_data_kiri = $data_kiri - $value;
    	if($sisa_data_kiri)
    	{
    	  $kosong = '';
    	  for($x = 1; $x <= $sisa_data_kiri; $x++)
    	  {
    	    $kosong .= " ";
    	  }
    	}
	  echo $kosong;
	}


    function tmbh_spaceHeaderProduct($value)
	{
    	  $kosong = '';
          for($x = 1; $x <= $value; $x++)
    	  {
    	    $kosong .= " ";
    	  }

	  echo $kosong;
	}

    function titleHeaderData()
	{
	    echo "Stock";
    	tmbh_spaceHeaderProduct(6);
    	echo "Description";
    	tmbh_spaceHeaderProduct(22);
    	echo "Qty";
    	tmbh_spaceHeaderProduct(10);
    	echo "DP";
        tmbh_spaceHeaderProduct(7);
        echo "Gross DP\n";
    	//tmbh_spaceHeaderProduct(5);

    	garisStrip();

	}

	function tmbh_spaceHeaderIP($value)
	{
    	  $kosong = '';
          for($x = 1; $x <= $value; $x++)
    	  {
    	    $kosong .= " ";
    	  }

	  echo $kosong;
	}

	function titleHeaderDataKW()
	{
	    echo "NO";
    	tmbh_spaceHeaderIP(1);
    	echo "KW NO";
    	tmbh_spaceHeaderIP(9);
    	echo "IP NO";
        tmbh_spaceHeaderIP(9);
        echo "CN NO";
    	tmbh_spaceHeaderIP(10);
        echo "ID MEMBER";
    	tmbh_spaceHeaderIP(5);
        echo "STOCKIST";
    	tmbh_spaceHeaderIP(5);
        echo "NOMINAL";
    	tmbh_spaceHeaderIP(1);
    	//tmbh_spaceHeaderIP(1);
        echo "\n";

    	garisStripKW();
	}

	function createEmptySpace($val) {
		$isi = "";
		for($i = 1;$i<=$val;$i++)
   	    {
            $isi .= " ";
        }
		return $isi;
	}

	function addSpacePersonal($batas, $value) {
		$pjg = strlen($value);
	    $kosong = '';
		//$batas = 11;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
		  {
    	     $kosong .= " ";
    	  }
		  return $kosong;
	}

	function addLine($jum, $char = "-") {
		$kosong = '';
		for($i = 1; $i <=$jum; $i++)
		  {
    	     $kosong .= $char;
    	  }
		  return $kosong;
	}

    function tmbh_spaceDetailPersonal($no,$value)
	{
	    $pjg = strlen($value);
	    $kosong = '';

	    //Utk STOCK
    	if($no == 1)
		{
		  $batas = 11;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
		  {
    	     $kosong .= " ";
    	  }
		}

        //Utk DESC
		elseif($no == 2)
		{
          $batas = 33;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}

         //Utk QTY
		elseif($no == 3)
		{
          $batas = 3;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}

         //Utk QTY
		elseif($no == 4)
		{
		  //$batas = 10;
          $batas = 6;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}

		//Utk DP
		elseif($no == 5)
		{
		  //$batas = 10;
          $batas = 12;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}
         elseif($no == 7)
		{
		  //$batas = 10;
          $batas = 2;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}

		//Utk GROSS DP
		elseif($no == 6)
		{
		  //$batas = 24;
          $batas = 14;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}
        echo $kosong;
	}

    function tmbh_spaceDetailPersonalKW($no,$value)
	{
	    $pjg = strlen($value);
	    $kosong = '';

	    //Utk NO
    	if($no == 1)
		{
		  if($pjg <= 1)
		  {
		    $kosong .= " ";
		  }

		}

        //Utk NO
		elseif($no == 2)
		{
		  //$batas = 10;
          $batas = 3;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
            //$kosong = " ";
		}

         //Utk KW NO
		elseif($no == 3)
		{
		  //$batas = 10;
          $batas = 14;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
            //$kosong = " ";
		}

		//Utk IP NO
		elseif($no == 4)
		{
		  //$batas = 10;
          $batas = 14;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
            //$kosong = " ";
		}

		//Utk ID MEMBER
		elseif($no == 5)
		{
		  //$batas = 24;
          $batas = 15;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }

		}

        //Utk NM MEMBER
		elseif($no == 6)
		{
		  //$batas = 24;
          $batas = 14;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }

		}

        //Utk ID STOKIST
		elseif($no == 7)
		{
		  //$batas = 24;
          $batas = 10;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }

		}

        //Utk NOMINAL
		elseif($no == 8)
		{
		  //$batas = 24;
          $batas = 10;

          $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
		}
		echo $kosong;
	}

    //tambahan ana
    function title_headerDO(){
       echo "No";
    	tmbh_spaceHeaderProduct(3);
    	echo "Product";
    	tmbh_spaceHeaderProduct(4);
    	echo "Product Name";
    	tmbh_spaceHeaderProduct(32);
    	echo "Qty";
    	tmbh_spaceHeaderProduct(4);
    	echo "Ship";
    	tmbh_spaceHeaderProduct(5);
    	echo "B/O\n";
    	garisStripDO(12);
    }

    function garisStripDO($x)
	{
	   for($i = 1;$i<=$x;$i++)
   	    {
            echo "-------";
        }
	}

    function tmbh_spaceDetailDO($no,$value,$batas)
	{
	    $pjg = strlen($value);
	    $kosong = '';

	    //Utk NO
    	if($no == 1)
		{
		  if($pjg <= 1)
		  {
		    $kosong .= " ";
		  }

		}

        //Utk NO
		elseif($no == 2)
		{
		  //$batas = 10;
          //$batas = 5;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
            //$kosong = " ";
		}

         //Utk Prdcd
		elseif($no == 3)
		{
		  //$batas = 10;
          //$batas = 11;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
            //$kosong = " ";
		}

		//Utk PRDNM
		elseif($no == 4)
		{
		  //$batas = 10;
          //$batas = 40;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <=$sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }
            //$kosong = " ";
		}

		//Utk QTY
		elseif($no == 5)
		{
		  //$batas = 24;
          //$batas = 7;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }

		}

        //Utk SHIP
		elseif($no == 6)
		{
		  //$batas = 24;
          //$batas = 6;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }

		}

        //Utk BO
		elseif($no == 7)
		{
		  //$batas = 24;
          //$batas = 3;
		  $sisa = $batas - $pjg;
		  for($i = 1; $i <= $sisa; $i++)
    		  {
    		    $kosong .= " ";
    		  }

		}

		echo $kosong;
	}

    function titleHeaderPromo(){
        echo "No";
    	tmbh_spaceHeaderProduct(3);
    	echo "Orderno";
    	tmbh_spaceHeaderProduct(7);
    	echo "ID Member";
    	tmbh_spaceHeaderProduct(5);
	    echo "Member Name";
    	tmbh_spaceHeaderProduct(11);
    	echo "Product";
    	tmbh_spaceHeaderProduct(4);
        echo "Prod. Name";
    	tmbh_spaceHeaderProduct(5);
        echo "Qty\n";
    	garisStripDO(12);
    }

	function settingLOSLayout($result, $sentCC = null) {
		$edp = "Nandang Hermansyah";
		$cc = $sentCC == null ? "Ms. Sharon" : $sentCC;
		$settingLayout = array(
			"border"              => 0,
			"contentFontSize"     => 11,
			"edpManager" 	      => $edp,
			"totalWidth"          => 190,
			"widthKopTitle" 	  => 40,
			"widthKopName"        => 70,
			"doubleDot"           => 2,
			"heightCell" 		  => 5,
			"heightCellSignature" => 25,
			"widthCellSignature"  => 63,
			"result"              => $result,
			"font"                => "Times",
			"sentTo"              => "Mr. Poa",
			"sentCC"              => $cc,
			"subject"             => "Member Account"
			//"sentFrom"            => $edp
		);

		return $settingLayout;
	}

	function LosPrintHeader($pdf, $settingLayout) {

		    $bidWidth = $settingLayout['totalWidth'] - $settingLayout['widthKopTitle'] - $settingLayout['doubleDot'] - $settingLayout['widthKopName'];
		    $result = $settingLayout['result'];
			$pdf->Ln();
	        $pdf->SetFillColor(255,255,255);
	        $pdf->SetFont($settingLayout['font'],'B',14);
	        $pdf->Cell($settingLayout['totalWidth'],$settingLayout['heightCell'], "LETTER OF STATEMENT",0,0,'C',true);
	        $pdf->Ln();
	        $pdf->Ln();

	        $pdf->SetFont($settingLayout['font'],'B',$settingLayout['contentFontSize']);
	        $pdf->SetFillColor(255,255,255);

			//To
	        $pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "To",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
	        $pdf->Cell($settingLayout['widthKopName'],$settingLayout['heightCell'], $settingLayout['sentTo'],$settingLayout['border'],0,'L',true);
	        $bid = "Reff :".$result[0] -> los_no;
			$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Reff",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$pdf->Cell("",$settingLayout['heightCell'], $result[0] -> los_no,$settingLayout['border'],0,'L',true);
	        //$pdf->Cell($bidWidth,$settingLayout['heightCell'], $bid,$settingLayout['border'],0,'R',true);
	        $pdf->Ln();

			//CC
			$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "CC",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$date = "Date :".date("d/m/Y");
	        $pdf->Cell($settingLayout['widthKopName'],$settingLayout['heightCell'], $settingLayout['sentCC'], $settingLayout['border'],0,'L',true);
	        $pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Date",$settingLayout['border'],0,'R',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$pdf->Cell("",$settingLayout['heightCell'], date("d/m/Y"),$settingLayout['border'],0,'L',true);
	        //$pdf->Cell($bidWidth,$settingLayout['heightCell'], $date,$settingLayout['border'],0,'R',true);
	        $pdf->Ln();

			//From
			$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "From",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$date = "";
	        $pdf->Cell($settingLayout['widthKopName'],$settingLayout['heightCell'], $settingLayout['edpManager'], $settingLayout['border'],0,'L',true);
	        $pdf->Cell($bidWidth,$settingLayout['heightCell'], $date,$settingLayout['border'],0,'R',true);
	        $pdf->Ln();

			//Subject
			$pdf->Cell($settingLayout['widthKopTitle'],$settingLayout['heightCell'], "Subject",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['doubleDot'],$settingLayout['heightCell'], ":",$settingLayout['border'],0,'C',true);
			$date = "";
	        $pdf->Cell($settingLayout['widthKopName'],$settingLayout['heightCell'], $settingLayout['subject'],$settingLayout['border'],0,'L',true);
	        $pdf->Cell($bidWidth,$settingLayout['heightCell'], $date,$settingLayout['border'],0,'R',true);
	        $pdf->Ln();
			 $pdf->Ln();
			//==
			$garis = "";
			//113
			for($i=0;$i<=84;$i++) {
				$garis .= "=";
			}
			$pdf->Cell($settingLayout['totalWidth'],$settingLayout['heightCell'], $garis,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Ln();
			 $pdf->Ln();
			$pdf->SetFont($settingLayout['font'],'',$settingLayout['contentFontSize']);
			$pembuka = "Dengan hormat,";
			$pdf->Cell($settingLayout['totalWidth'],$settingLayout['heightCell'], $pembuka,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT$pdf->Ln();
			$pdf->Ln();


	}

    function LosPrintFooter($pdf, $settingLayout) {
    	    $pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
    	    $pembuka = "Terima kasih";
			$pdf->Ln();
			$pdf->Ln();

			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], $pembuka,$settingLayout['border'],0,'C',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'C',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Approved By",$settingLayout['border'],0,'C',true);
			$pdf->Ln();

			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'L',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'L',true);
	        $pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], $settingLayout['edpManager'], $settingLayout['border'],0,'C',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Bayu Riono",$settingLayout['border'],0,'C',true);
			//$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Nandang Hermansyah",$settingLayout['border'],0,'C',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Dato' Dr. H. Md. Radzi Saleh",$settingLayout['border'],0,'C',true);
	        $pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(EDP Manager)",$settingLayout['border'],0,'C',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(Legal Manager)",$settingLayout['border'],0,'C',true);
			//$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(General Manager)",$settingLayout['border'],0,'C',true);
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(President Director K-Link)",$settingLayout['border'],0,'C',true);
	        $pdf->Ln();
			/*$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'L',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Approved by",$settingLayout['border'],0,'C',true);
	        $pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'C',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'C',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'C',true);
			$pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'C',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'C',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Dato' Dr. H. Md. Radzi Saleh",$settingLayout['border'],0,'C',true);
	        $pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'C',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'C',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(President Director K-Link)",$settingLayout['border'],0,'C',true);

			 */
	}

	function LosPrintFooter2sign($pdf, $settingLayout) {
		    //$pdf->Ln();
			//$pdf->Ln();
			//$pdf->Ln();
    	    $pembuka = "Terima kasih";
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell($settingLayout['totalWidth'],$settingLayout['heightCell'], $pembuka,$settingLayout['border'],0,'L',true); 	// its similar with TD in HT$pdf->Ln();
			$pdf->Ln();

			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'],"",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'],"",$settingLayout['border'],0,'L',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'],"Approved by",$settingLayout['border'],0,'L',true);
			$pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'L',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCellSignature'],"",$settingLayout['border'],0,'L',true);
	        $pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], $settingLayout['edpManager'], $settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'L',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "Dato' Dr. H. Md. Radzi Saleh",$settingLayout['border'],0,'L',true);
	        $pdf->Ln();
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(EDP Manager)",$settingLayout['border'],0,'L',true); 	// its similar with TD in HT
	        $pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "",$settingLayout['border'],0,'L',true);
			$pdf->Cell($settingLayout['widthCellSignature'],$settingLayout['heightCell'], "(President Director K-Link)",$settingLayout['border'],0,'L',true);

	}

	function convertMonthName($bln, $language = "in") {
		if($bln == "1") {
			$nama = "Januari";
		} else if($bln == "2") {
			$nama = "Februari";
		} else if($bln == "3") {
			$nama = "Maret";
		} else if($bln == "4") {
			$nama = "April";
		} else if($bln == "5") {
			$nama = "Mei";
		} else if($bln == "6") {
			$nama = "Juni";
		} else if($bln == "7") {
			$nama = "Juli";
		} else if($bln == "8") {
			$nama = "Agustus";
		} else if($bln == "9") {
			$nama = "September";
		} else if($bln == "10") {
			$nama = "Oktober";
		} else if($bln == "11") {
			$nama = "November";
		} else {
			$nama = "Desember";
		}
		return $nama;
	}

	function sendWelcomeEmail($resultInsMemb) {


		$content = welcomeEmail($resultInsMemb);

		if($resultInsMemb[0]->email != "" || $resultInsMemb[0]->email != null) {
			//emailSend($resultInsMemb[0]->email);
			$arr = emailSend("Selamat Bergabung dengan kami", $resultInsMemb[0]->email, $content);
			//echo $arr."<br />";
		}

		if($resultInsMemb[0]->email_pendaftar != "" || $resultInsMemb[0]->email_pendaftar != null) {
			//emailSend($resultInsMemb[0]->email_pendaftar);
			$arr = emailSend("Selamat Bergabung dengan kami", $resultInsMemb[0]->email_pendaftar, $content);
			//echo $arr."<br />";
		}
		//$content = "asdasdad";

		//rawurlencode

        //echo trim($apiresult);
	}

	function sendNotifSalesTransaction($resultInsMemb) {
		$content = notifSalesTransaction($resultInsMemb);
		if($resultInsMemb[0]->email != "" || $resultInsMemb[0]->email != null) {
			//emailSend($resultInsMemb[0]->email);
			$email = $resultInsMemb[0]->email;
			$arr = emailSend("Transaksi Sales K-NET", $email, $content);
			//echo $arr."<br />";
		}

		/*if($resultInsMemb[0]->email_pendaftar != "" || $resultInsMemb[0]->email_pendaftar != null) {
			//emailSend($resultInsMemb[0]->email_pendaftar);
			$arr = emailSend("dionrespati@gmail.com", $content);
			echo $arr."<br />";
		}*/
	}

	function emailSend($subject, $email_to, $content) {
		$from = "noreply@k-link.co.id";
        $fromname = "PT K-link Indonesia";

        $api_key = "87d9af6eab922de72f919312ce9d3e4a";
        //$subject = "Selamat Bergabung dengan kami";

		$data=array();
        $data['subject']= $subject;
        $data['fromname']= $fromname;
        $data['api_key'] = $api_key;
        $data['from'] = $from;
        $data['content']= $content;
        $data['recipients']= $email_to;
        $apiresult = callApi(@$api_type,@$action,$data);
		return $apiresult;
	}

	function notifSalesTransaction($param) {
		$CI = & get_instance();
		$CI->load->model("backend/be_transaction_model", "trans");
		$prd = $CI->trans->getTrxDetail($param[0]->orderno);

		$str = "<html>";
		$str .= "<head></head>";
		$str .= "<body>";
		$str .= "<img src=\"".base_url('assets/images/logo.jpg')."\"></img>";
		$str .= "<h2>Terima Kasih telah berbelanja melalui K-NET</h2>";
		$str .= "<p>Transaksi pembelanjaan produk anda sudah di proses, berikut adalah data transaksinya : </p>";
		$str .= "<p>";
		$str .= "<table width=80%>
				<tr>
				  <td width=25%>No Transaksi</td>
				  <td width=2%>:</td>
				  <td>".$param[0]->orderno."</td>
				</tr>
				<tr>
				  <td>ID Member</td>
				  <td>:</td>
				  <td>".$param[0]->id_memb." / ".$param[0]->nmmember."</td>
				</tr>
				<tr>
				  <td>Total Pembelanjaan</td>
				  <td>:</td>
				  <td>".$param[0]->total_pay."</td>
				</tr>
				<tr>
				  <td>Total BV</td>
				  <td>:</td>
				  <td>".$param[0]->total_bv."</td>
				</tr>
				<tr>
				  <td>Periode Bonus</td>
				  <td>:</td>
				  <td>".$param[0]->bonusmonth."</td>
				</tr>";

		if($param[0]->sentTo == "1") {
			$str .= "<tr>
					  <td>Stockist</td>
					  <td>:</td>
					  <td>".$param[0]->nmstkk."</td>
					</tr>
					<tr>
					  <td>Kode Pengambilan Barang</td>
					  <td>:</td>
					  <td>".$param[0]->secno."</td>
					</tr>
					<tr>
					  <td colspan=3><font color=red>*Barang di ambil di stokist</font></td>
					</tr>";
		} else if($param[0]->sentTo == "2") {
			$str .= "<tr>
					  <td>No Conote</td>
					  <td>:</td>
					  <td>".$param[0]->conoteJNE."</td>
					</tr>
					<tr>
					  <td valign=top>Alamat</td>
					  <td valign=top>:</td>
					  <td valign=top>".$param[0]->addr1."</td>
					</tr>
					<tr>
					  <td>Nama Penerima</td>
					  <td>:</td>
					  <td>".$param[0]->receiver_name." / ".$param[0]->tel_hp1."</td>
					</tr>";
		}
		$str .= "</table></p>";
		$str .= "<p>";
		$str .= "<table border=1 width=80%>";
		$str .= "<tr><th colspan=8>Detail Produk</th></tr>";
		$str .= "<tr><th>No</td><td>Kode Produk</th><th>Nama Produk</th><th>Qty</th><th>BV</th><th>Harga</th><th>Sub Total BV</th><th>Sub Total Harga</th></tr>";
		$i = 1;
		$tot_qty = 0;
		$tot_bv = 0;
		$tot_dp = 0;
		foreach($prd as $dtax) {
			$subbv = $dtax->qty*$dtax->bvr;
			$subdp = $dtax->qty*$dtax->dpr;

			$str .= "<tr>";
			$str .= "<td align=right>$i</td>";
			$str .= "<td>$dtax->prdcd</td>";
			$str .= "<td>$dtax->prdnm</td>";
			$str .= "<td align=right>".number_format($dtax->qty,0,".",",")."</td>";
			$str .= "<td align=right>".number_format($dtax->bvr,0,".",",")."</td>";
			$str .= "<td align=right>".number_format($dtax->dpr,0,".",",")."</td>";
			$str .= "<td align=right>".number_format($subbv,0,".",",")."</td>";
			$str .= "<td align=right>".number_format($subdp,0,".",",")."</td>";
			$str .= "</tr>";
			$i++;
			$tot_qty += $dtax->qty;
			$tot_bv += $subbv;
			$tot_dp += $subdp;
		}
		$str .= "<tr><td colspan=6>T O T A L</td><td align=right>".number_format($tot_bv,0,".",",")."</td><td align=right>".number_format($tot_dp,0,".",",")."</td></tr>";
		$str .= "</table></p>";
		$str .= "</body></html>";

		return $str;
	}
		function KatalogEmail($token,$param) {
		$str = "<html>";
		$str .= "<head></head>";
		$str .= "<body>";
		$str .= "<img src=\"".base_url('assets/images/logo.jpg')."\"></img>
					<h2>Selamat Bergabung dengan Kami</h2>
					<p>PT K-link Indonesia mengucapkan terima kasih atas kepercayaan Anda bertransaksi menggunakan aplikasi atau website online Kami.</p>
					<p>Berikut adalah data transaksi Anda :</p>
					<p>
					<table width=60%>";
			foreach ($param as $row) {
		$str.=      "<tr>
					<td width=10%>$row->cat_inv_desc</td>
					<td width=2%>:</td>
					<td>
					<a class='btn btn-success' href='".base_url('catalogue/download/'.$token.'/'.$row->cat_inv_id)."'>
					<i class='fa fa-arrow-left'></i>
					<span>Download</span>
					</a></td>
					</tr>";
			}

			$str .= "

					</table>
					</p>
					<p>Anda dapat login ke www.k-net.co.id untuk berbelanja secara online atau mendaftarkan distributor baru sebagai downline anda.
					Silahkan menggunakan ID Member dan password anda untuk login</p>
					<p>Salam Sukses..</p>";
		$str .= "<p></p>";
		$str .= "</body></html>";

		return $str;
		}
	function welcomeEmail($param) {
		$str = "<html>";
		$str .= "<head></head>";
		$str .= "<body>";
		$str .= "<img src=\"".base_url('assets/images/logo.jpg')."\"></img>
				<h2>Selamat Bergabung dengan Kami</h2>
				<p>PT K-link Indonesia mengucapkan selamat datang dan selamat bergabung bersama kami. K-link adalah 
				salah satu perusahaan Multi Level Marketing terbesar di Indonesia yang sudah berdiri sejak tahun 2002 dan telah menghasilkan ribuan leader-leader yang sukses dalam menjalankan bisnis ini. 
				K-link berkomitmen untuk terus maju dan mengembangkan diri bersama anda, 
				jadilah bagian dari kami, raihlah cita-cita dan impian anda, menuju kesuksesan yang sejati.</p>
				<p>Berikut adalah data keanggotaan anda :</p>
				<p>
				<table width=60%>
				<tr>
				  <td width=10%>ID Member</td>
				  <td width=2%>:</td>
				  <td>".$param[0]->memberid."</td>
				</tr>
				<tr>
				  <td width=40%>Nama</td>
				  <td width=5%>:</td>
				  <td>".$param[0]->membername."</td>
				</tr>
				<tr>
				  <td width=40%>ID Sponsor</td>
				  <td width=5%>:</td>
				  <td>".$param[0]->sponsorid." / ".$param[0]->sponsorname."</td>
				</tr>
				
				<tr>
				  <td width=40%>Password</td>
				  <td width=5%>:</td>
				  <td>".$param[0]->password."</td>
				</tr>
				</table>
				</p>
				<p>Anda dapat login ke www.k-net.co.id untuk berbelanja secara online atau mendaftarkan distributor baru sebagai downline anda.
				Silahkan menggunakan ID Member dan password anda untuk login</p>
				<p>Salam Sukses..</p>";
		$str .= "<p></p>";
		$str .= "</body></html>";

		return $str;
	}

	function link_js_sgo($tipe) {
		if($tipe == "dev") {
			$str = "<script type=\"text/javascript\" src=\"https://sandbox-kit.espay.id/public/signature/js\"></script>";
		} else if($tipe == "prod"){
			//<script type="text/javascript" src="https://secure.sgo.co.id/public/signature/js"></script>
			$str = "<script type=\"text/javascript\" src=\"https://kit.espay.id/public/signature/js\"></script>";
			//$str = "<script type=\"text/javascript\" src=\"https://secure.sgo.co.id/public/signature/js\"></script>";
		}
		return $str;
	}

	function backToMainForm() {

		echo "<div>
					 <input value=\"<< Kembali\" type=\"button\" class=\"btn btn-warning\" onclick=\"All.back_to_form(' .nextForm1',' .mainForm')\"/>
				</div>
				";
	}
	
	function backToNextForm() {

		echo "<div>
					 <input value=\"<< Kembali\" type=\"button\" class=\"btn btn-warning\" onclick=\"All.back_to_form(' .nextForm2',' .nextForm1')\"/>
				</div>
				";
	}

	function formatTglIndo($sds) {
		$arr = explode("/", $sds);
		$jum = count($arr);
//		echo $jum;
		$dateArr = array(
		  "01" => "Januari",
		  "1" => "Januari",
		  "02" => "Februari",
		  "2" => "Februari",
		  "03" => "Maret",
		  "3" => "Maret",
		  "04" => "April",
		  "4" => "April",
		  "05" => "Mei",
		  "5" => "Mei",
		  "06" => "Juni",
		  "6" => "Juni",
		  "07" => "Juli",
		  "7" => "Juli",
		  "08" => "Agustus",
		  "8" => "Agustus",
		  "09" => "September",
		  "9" => "September",
		  "10" => "Oktober",
		  "11" => "November",
		  "12" => "Desember"
		 );
		if($jum == 3) {
			$bln = $arr[1];
			return $arr[0]."/".$dateArr[$bln]."/".$arr[2];
		} else if($jum == 2) {
			$bln = $arr[0];
			return $dateArr[$bln]."/".$arr[1];
		}
	}

	function convertNumberToWord($num = false)
	{
	    $num = str_replace(array(',', ' '), '' , trim($num));
	    if(! $num) {
	        return false;
	    }
	    $num = (int) $num;
	    $words = array();
	    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
	        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
	    );
	    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
	    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
	        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
	        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
	    );
	    $num_length = strlen($num);
	    $levels = (int) (($num_length + 2) / 3);
	    $max_length = $levels * 3;
	    $num = substr('00' . $num, -$max_length);
	    $num_levels = str_split($num, 3);
	    for ($i = 0; $i < count($num_levels); $i++) {
	        $levels--;
	        $hundreds = (int) ($num_levels[$i] / 100);
	        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
	        $tens = (int) ($num_levels[$i] % 100);
	        $singles = '';
	        if ( $tens < 20 ) {
	            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
	        } else {
	            $tens = (int)($tens / 10);
	            $tens = ' ' . $list2[$tens] . ' ';
	            $singles = (int) ($num_levels[$i] % 10);
	            $singles = ' ' . $list1[$singles] . ' ';
	        }
	        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
	    } //end for loop
	    $commas = count($words);
	    if ($commas > 1) {
	        $commas = $commas - 1;
	    }
	    return implode(' ', $words);
	}

	//tambahan Vera
	function MinTrxPromoNonMember()
	{
		// Get a reference to the controller object
		$CI = get_instance();

		// You may need to load the model if it hasn't been pre-loaded
		$CI->load->model('nonmember_promo/M_nonmember_promo','M_nonmember_promo');

		// Call a function of the model
		//return $CI->M_nonmember_promo->getMinTrxPromo();
		$prd = $CI->M_nonmember_promo->getMinTrxPromo();
		return $prd[0]->min_trx_amount;


	}
	
	function menuWebPpobXL() {
		//$memberid = getUserID();
		//if($memberid == "IDSPAAA66834")
		//{
			?>
				 <li><a href="#">PPOB & XL Co Brand</a>
	              <ul>
	              	 <!--<li><a href="<?php echo "".site_url('sgo_ppob/trx/fe').""; ?>">Pembelian PPOB</a></li>-->
	            	 <li><a href="<?php echo "".site_url('xl/paketdata/fe').""; ?>">XL Co Branded</a></li>
	            	 <li><a href="<?php echo "".site_url('ppob/rekap/trx').""; ?>">Rekap Trx PPOB & XL Co Branded</a></li>
	            	 <li><a href="<?php echo "".site_url('va/invoice/send').""; ?>">Registrasi Virtual Account</a></li>
	            	 <li><a href="<?php echo "".site_url('va/history/trx/fe').""; ?>">History VA Trx</a></li>
	              </ul>
	            </li>
	        <?php 
	    //}
     }
	
	function menuWebDownload() {
		?>
		  <li>
            <a href="#">Download</a>
            <ul>
                <li>
                    <a href="#" onclick="Shopping.download_file('SADAForm.jpg')" >Form SADA</a>
                </li>
				<li>
                    <a href="#" onclick="Shopping.download_file('FormHebohBanget2018_2.pdf')" >Form Klaim Promo Heboh Banget</a>
                </li>
                <li>
                    <a href="#" onclick="Shopping.download_file('D042 Maret2019.pdf')">Daftar Harga</a>
                </li>
                <!--<li>
                    <a href="#" onclick="Shopping.download_file('FormIEC2017Claim.pdf')" >Form Claim IEC 2017</a>
                </li>
                <li>
                    <a href="#" onclick="Shopping.download_file('FormClaimIDC2017.pdf')" >Form Claim Initiative Dynamcic Challenge 2017</a>
                </li>
                <li>
                    <a href="#" onclick="Shopping.download_file('SURAT_PERNYATAAN_TESTIMONI_2017.pdf')" >Form Testimoni 2017</a>
                </li>
                <li>
                    <a href="<?php echo "".site_url('iac/cetakpolos').""; ?>" target="_blank" >Form IAC</a>
                </li>-->
                <!--<li>
                 <a href="#" onclick="Shopping.download_file('FORM_KLAIMBACKPAK.doc')" >Form Backpack</a>
             </li>-->
            </ul>
        </li>
		<?php
	}

	function menuWebPromoInfo() {
		?>
		<li>
            <a href="#">Promo Info</a>
            <ul>
<!--                <li><a href="--><?php //echo "".site_url('promo_rekrut').""; ?><!--">Cek Poin Recruit Rewards</a></li>-->
<!--                <li><a href="--><?php //echo "".site_url('promo/idc').""; ?><!--">Cek IDC (Initiative Dynamic Challenge)</a></li>-->
<!--                <li><a href="--><?php //echo "".site_url('promo/oliveKhoney').""; ?><!--">Olive Oil & K-honey Promo</a></li>-->
<!--                <li><a href="--><?php //echo "".site_url('iec').""; ?><!--">Listing IEC</a></li>-->
<!--                <li><a href="--><?php //echo "".site_url('iac').""; ?><!--">Listing IAC</a></li>-->
<!--                <li><a href="--><?php //echo "".site_url('klaim').""; ?><!--">Klaim Bonus</a></li>-->
<!--                <li><a href="--><?php //echo "".site_url('promo_cashback').""; ?><!--">Promo Cashback</a></li>-->
<!--				<li><a href="--><?php //echo "".site_url('promo_novapr').""; ?><!--">Promo November 2017 - April 2018</a></li>-->
<!--				<li><a href="--><?php //echo "".site_url('promo_novapr').""; ?><!--">Promo Spesial Ramadhan & Idul Fitri</a></li>-->
					<li>
					<a href="<?php echo "".site_url('kupon-hebohbanget').""; ?>">Promo Heboh Banget April 2018 - Maret 2019</a>
					</li>
				<li><a href="<?php echo "".site_url('promo-lbc').""; ?>">Promo LBC Trip to Bangkok</a></li>
			</ul>
        </li>
			<!--<li><a href="">Check Point IAC(Initiative Anniversary Challenge)</a></li>-->
                <!--	nandang 20161004
        	<li><a href="#">Promo Ayuverda</a>
                <ul>
                	<li><a href="<?php //echo "".site_url('promoayu/6').""; ?>">Juni 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promoayu/7').""; ?>">Juli 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promoayu/8').""; ?>">Agustus 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promoayu/9').""; ?>">September 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promoayu/10').""; ?>">Oktober 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promoayu/11').""; ?>">Nopember 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promoayu/12').""; ?>">Desember 2015</a></li>
                </ul>
            </li>
            -->

                <!--<li><a href="<?php //echo "".site_url('promo_kolagen').""; ?>">Cek Undian Kolagen Anniversary</a></li>-->
                <!--
            <li><a href="#">Promo K-Liquid Collagen</a>
            	<ul>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1512').""; ?>">Desember 2015</a></li>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1601').""; ?>">Januari 2016</a></li>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1602').""; ?>">Pebruari 2016</a></li>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1603').""; ?>">Maret 2016</a></li>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1604').""; ?>">April 2016</a></li>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1605').""; ?>">Mei 2016</a></li>
                	<li><a href="<?php //echo "".site_url('promo/collagen/1606').""; ?>">Juni 2016</a></li>
                </ul>
            </li>
            -->

                <!--<li><a href="#">Promo Calendar</a>
                  <ul><li><a href="<?php //echo "".site_url('promo/calendar').""; ?>">Claim Calendar</a></li></ul>
            </li>-->	
		<?php
	}

	function menuWebPerjalananRohani() {
		?>
		 <li><a href="#">Perjalanan Rohani</a>
            <ul>
                <li><a href="#">Pendaftaran</a>
                    <ul>
                        <li><a href="<?php echo "".site_url('umroh').""; ?>">Umroh</a></li>
                        <li><a href="<?php echo "".site_url('ziarah').""; ?>">Ziarah</a></li>
                    </ul>
                </li>

                <li><a href="<?php echo "".site_url('umroh/cek/mutasi/').""; ?>">Cek Mutasi</a></li>
                <li><a href="#">Setor Cicilan</a>
                    <ul>
                        <li><a href="<?php echo "".site_url('umroh/installments/').""; ?>">Cicilan Umroh</a></li>
                        <!--<li><a href="<?php echo "".site_url('ziarah/installments/').""; ?>">Cicilan Ziarah</a></li>-->
                    </ul>
                </li>
                <li><a href="<?php echo "".site_url('umroh/printVchNo/').""; ?>">Cetak Voucher</a></li>
                <li><a href="<?php echo "".site_url('umroh/reprint/').""; ?>">Reprint Bukti Pendaftaran</a></li>
            </ul>
        </li>
		<?php
	}

	function menuWebPendaftaranDist() {
		?>
		<li>
            <a href="#">Pendaftaran Distributor</a>
            <ul>
                <li><a href="<?php echo "".site_url('member/choosePay').""; ?>">Pendaftaran Distributor</a></li>
                <li><a href="<?php echo "".site_url('member/recruit').""; ?>">Daftar Rekrutan Member</a></li>
			    <?php
					//if(getUserID() == "ID9999A00251" || getUserID() == "IDSPAAA66834") {
				?>
                <li><a href="<?php echo "".site_url('dtc').""; ?>">Pembelian Tiket Acara</a></li>
				<?php
				//}
				?>
                <li><a href="<?php echo "".site_url('vtiket').""; ?>">Informasi Tiket </a></li>
            </ul>
        </li>
        
		<?php
	}

	function menuWeblandingPageRecruit() {
		?>
		<li><a href="#">Landing Page Recruit</a>
            <ul>
                <!--<li><a href="<?php echo "".site_url('member/reg/list/recruit').""; ?>">List Recruit Member</a></li>-->
                <li><a href="<?php echo "".site_url('member/reg/downline').""; ?>">Update Sponsor</a></li>
            </ul>
        </li>
		<?php
	}

	function menuWebSalesdanBonus() {
		?>
		<li><a href="#">Sales & Bonus</a>
            <ul>
                <li><a href="<?php echo "".site_url('sales/direct/downline').""; ?>">Info Jaringan</a></li>
                <li><a href="<?php echo "".site_url('bonus').""; ?>">Bonus Report</a></li>
                <li><a href="#">Cek Transaksi</a>
                    <ul>
                        <li><a href="<?php echo "".site_url('sales/online').""; ?>">Transaksi K-net</a></li>
                        <li><a href="<?php echo "".site_url('sales').""; ?>">Transaksi Global</a></li>
                        <li><a href="<?php echo "".site_url('sales/cek_payment').""; ?>">Transaksi Pembayaran</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo "".site_url('sales/recruiter/bonus').""; ?>">Bonus AyuArtis Oil</a></li>
                <li><a href="<?php echo "".site_url('sales/cashback_hydro').""; ?>">Cashback K-Hydrogenwater</a></li>
                <?php
				//if(getUserID() == "IDSPAAA66834") {
				?>
				<li><a href="<?php echo "".site_url('sales/pindahbv').""; ?>">Transfer BV Personal</a></li>
				<?php
				//}
				?>

            </ul>
        </li>
		<?php
	}
	
	function menuWebPromoCalendar() {
		?>
		<li><a href="#">Promo Calendar</a>
          <ul>
            
            <li><a href="<?php //echo "".site_url('promo/calendar').""; ?>">Claim Calendar</a></li>
          </ul>
        </li>
		<?php
	}
	
	function quiz2017() {
		?>
		<li><a href="#">Quiz</a>
            <ul>
                <!--<li><a href="<?php echo "".site_url('member/reg/list/recruit').""; ?>">List Recruit Member</a></li>-->
                <li><a href="https://goo.gl/forms/qOSVYePqMKKOqQZt1" target="_blank">Quiz 2017</a></li>
            </ul>
        </li>
		<?php
	}
	
	function blastNotifCurl($array) {
		
		$curl = curl_init();
		
		$arx = array(
		  CURLOPT_URL => $array['url'], 
		  CURLOPT_RETURNTRANSFER => true, 
		  CURLOPT_ENCODING => "", 
		  CURLOPT_MAXREDIRS => 10, 
		  CURLOPT_TIMEOUT => 30, 
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
		  CURLOPT_CUSTOMREQUEST => $array['method'], 
		  CURLOPT_POSTFIELDS => $array['postdata'], 
		  CURLOPT_HTTPHEADER => array(
		   // "Cache-Control: no-cache", 
		    "Content-Type: application/json",
			"Authorization: Basic ZDgwMTM2OTktNTljZC00OTg1LWEyZGUtZDQ4NThkZjI5NDk1"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
		} else {
			return $response;
			//print_r($response);
			//print_r($response);
		}
	}

	function convertAngkaKeHuruf($num = false)
	{
	    $num = str_replace(array(',', ' '), '' , trim($num));
	    if(! $num) {
	        return false;
	    }
	    $num = (int) $num;
	    $words = array();
	    $list1 = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas',
	        'dua belas', 'tiga belas', 'empat belas', 'lima belas', 'enam belas', 'tujuh belas', 'delapan belas', 'sembilan belas');
	    $list2 = array('', 'sepuluh', 'dua puluh', 'tiga puluh', 'empat puluh', 'lima puluh', 'enam puluh', 'tujuh puluh', 'delapan puluh', 'sembilan puluh', 'seratus');
	    $list3 = array('', 'ribu', 'juta', 'milyar', 'triliun', 'milion lipat empat', 'quintillion', 'sextillion', 'septillion',
	        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
	        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
	    );
	    $num_length = strlen($num);
	    $levels = (int) (($num_length + 2) / 3);
	    $max_length = $levels * 3;
	    $num = substr('00' . $num, -$max_length);
	    $num_levels = str_split($num, 3);
	    for ($i = 0; $i < count($num_levels); $i++) {
	        $levels--;
	        $hundreds = (int) ($num_levels[$i] / 100);
	        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' ratus' . ' ' : '');
	        $tens = (int) ($num_levels[$i] % 100);
	        $singles = '';
	        if ( $tens < 20 ) {
	            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
	        } else {
	            $tens = (int)($tens / 10);
	            $tens = ' ' . $list2[$tens] . ' ';
	            $singles = (int) ($num_levels[$i] % 10);
	            $singles = ' ' . $list1[$singles] . ' ';
	        }
	        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
	    } //end for loop
	    $commas = count($words);
	    if ($commas > 1) {
	        $commas = $commas - 1;
	    }
	    return implode(' ', $words);
	}


	//START API GOSEND
	function price_gsend($client_id,$pass_key,$x,$y){
		$curl = curl_init();

		$fields = array(
			'origin' => $x,
			'destination' => $y,
			'paymentType' => '3'
		);
		$postData = setFieldPost($fields); //create parameter

		//$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		//$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v3/calculate/price?",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/calculate/price",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}
	
	function tes_booking_gsend($client_id,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_add,$des_coord,$item,$price,$orderno){
		$body='{
		   "paymentType": 3,
		   "deviceToken": "",
		   "collection_location":"pickup",
		  "shipment_method":"'.$metode.'",
		   "routes":
		   [{
			   "originName": "'.$senderName.'",
			   "originNote": "'.$senderNote.'",
			   "originContactName": "'.$senderContact.'",
			   "originContactPhone": "'.$senderPhone.'",
			   "originLatLong": "'.$senderCoord.'",
			   "originAddress": "'.$senderAlamat.'",
			   "destinationName": "",
			   "destinationNote": "",
			   "destinationContactName": "'.$des_name.'",
			   "destinationContactPhone": "'.$des_phone.'",
			   "destinationLatLong": "'.$des_coord.'",
			   "destinationAddress": "'.$des_add.'",
			   "item": "'.$item.'",
			   "storeOrderId":"'.$orderno.'",
			   "insuranceDetails":
				{
				  "applied": "false",
				  "fee": "",
				  "product_description": "'.$item.'",
				  "product_price": "'.$price.'"
				}
		  }]}';
		  
		  $curl = curl_init();
		  $arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/booking/v3/makeBooking",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $body,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Content-Type: application/json",
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);
		  
		  $arr = array(
		    "body" => $body,
			"response" => $response,
			"err_curl" => $err,
			"info" => $info,
		  );
		  
		  return $arr;
	}

	function booking_gsend($client_id,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_add,$des_coord,$item,$price,$orderno){
		$body='{
		   "paymentType": 3,
		   "deviceToken": "",
		   "collection_location":"pickup",
		  "shipment_method":"'.$metode.'",
		   "routes":
		   [{
			   "originName": "'.$senderName.'",
			   "originNote": "'.$senderNote.'",
			   "originContactName": "'.$senderContact.'",
			   "originContactPhone": "'.$senderPhone.'",
			   "originLatLong": "'.$senderCoord.'",
			   "originAddress": "'.$senderAlamat.'",
			   "destinationName": "",
			   "destinationNote": "",
			   "destinationContactName": "'.$des_name.'",
			   "destinationContactPhone": "'.$des_phone.'",
			   "destinationLatLong": "'.$des_coord.'",
			   "destinationAddress": "'.$des_add.'",
			   "item": "'.$item.'",
			   "storeOrderId":"'.$orderno.'",
			   "insuranceDetails":
				{
				  "applied": "false",
				  "fee": "",
				  "product_description": "'.$item.'",
				  "product_price": "'.$price.'"
				}
		  }]}';

		  $curl = curl_init();
		  $arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/booking/v3/makeBooking",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $body,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Content-Type: application/json",
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function status_gsend($client_id,$pass_key,$storeid){
		$curl = curl_init();
		//$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		//$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v2/booking/orderno/$orderno",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking/storeOrderId/$storeid",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "",
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function status_conoteG($client_id,$pass_key,$orderid){
		$curl = curl_init();
		//$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		//$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v2/booking/orderno/$orderno",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking/orderno/$orderid",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "",
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function cek_distance($latitude1,$longitude1){

		if($latitude1 !="" && $longitude1 !=""){
			$r= "true";
		}
		else{
			$r= "false";
		}

		$this->load->model("webshop/Member_model", "member_model");
		$sql = $this->member_model->getConcat('vera_location', 'loccd');
		foreach($sql as $row) {
			//foreach ($sql->result() as $row) {
			$R = trim($row->R);
			$loccd= trim($row->loccd);
			$scnm = trim($row->fullnm);
			$alamat = trim($row->alamat);
			$latitude2 = trim($row->X);
			$longitude2 = trim($row->Y);
			$state = trim($row->state);

			$theta = $longitude1 - $longitude2;
			$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
			$distance = acos($distance);
			$distance = rad2deg($distance);
			$distance = $distance * 60 * 1.1515;
			$distance = ($distance * 1.609344);
			$round= round($distance,2);
			$penghubung="+";

			// $jarak[]= round($distance,2);
			$n2[]=$round.$penghubung.$scnm.$penghubung.$loccd.$penghubung.$state.$penghubung.$alamat;
			$list[]=array($round=>$round.'+'.$scnm.'+'.$loccd.'+'.$state.'+'.$alamat);

		}
		//echo "Tes Vera: ".$count;

		//menghapus data array yang kosong
		foreach ($n2 as $key => $value) {
			if($value >= 50){
				unset($n2[$key]);
			}
		}

		asort($n2); // sort descending preserving key
		$n2top = array_slice($n2, 0, 25); // get top 3

		foreach ($list as $key => $val) {
			foreach ($val as $k => $v) {
				//echo "$k = $v\n";
				$namelist1[$k]=$v;
			}
		}

		ksort($namelist1);
		foreach ($namelist1 as $m => $n) {
			$namelist2[]=$n;
		}

		$arr = array("response" => $r, "error"=> "Error","message" => "Berhasil!","jarak" => $namelist2);
		echo json_encode($arr);
	}

 	function setShippingInfoBaru(){

		$CI = & get_instance();
		$CI->load->service('webshop/cartshop_service', 'cartshop_service');
        $personal_info  = $CI->session->userdata('personal_info');
		if($personal_info['delivery'] == "1") {
			$arrx = array(
				"origin_name" => "",
				"destination_name" => "",
				"service_display" => "",
				"price" => 0,
				"etd_from" => "",
				"etd_thru" => "",
				"times" => "",
				"ship_discount" => 0,
				"lat_dest" => "", //tambahan Vera
				"long_dest" => ""
			);
			$CI->session->unset_userdata('shipping_jne_info');
			$CI->session->set_userdata('shipping_jne_info', $arrx);
		} else {
				 //tambahan Vera
				if($personal_info['shipper'] == "1") {
				$dtx = json_decode($CI->cartshop_service->showListPriceJne($CI->cart->total_weight()));
//				echo "isi dtc = ";
//				print_r($dtx);
				$arrx = array();
				if(array_key_exists("status", $dtx)) {
					$CI->session->unset_userdata('shipping_jne_info');
					$CI->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				} else {
					//if(getUserID() == "IDSPAAA66834") {
					$arrx = $CI->cartshop_service->setLowestJnePrice($dtx);
					$CI->session->unset_userdata('shipping_jne_info');
					$CI->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;

				}

			}else if($personal_info['shipper'] == "2") {
				$dtx = json_decode($CI->cartshop_service->showListPriceKgb($CI->cart->total_weight()));
				//print_r($dtx);
				$arrx = array();
				if($dtx->status == "000") {
					$harga = $dtx->Tarif + $dtx->PPN;
					$arrx = array(
						"origin_name" => $dtx->NamaOrigin,
						"destination_name" => $dtx->NamaDestinasi,
						"service_code" => "",
						"service_display" => "",
						"price" => $harga,
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$CI->session->unset_userdata('shipping_jne_info');
					$CI->session->set_userdata('shipping_jne_info', $arrx);
					return $arrx;
				} else {
					$arrx = array(
						"origin_name" => "",
						"destination_name" => "",
						"service_code" => "",
						"service_display" => "",
						"price" => "",
						"etd_from" => "",
						"etd_thru" => "",
						"times" => "",
						"ship_discount" => 0,
						"lat_dest" => "",
						"long_dest" => ""
					);
					$CI->session->unset_userdata('shipping_jne_info');
					$CI->session->set_userdata('shipping_jne_info', $arrx);
					return null;
				}
			}else if($personal_info['shipper'] == "4") {
				//$dtx = json_decode($CI->showListPriceGosend($CI->cart->total_weight()));
				//echo "tes11111";
				$metode=$CI->input->post("metode");
				$cmetode=substr($metode,0,1);

				if($cmetode == 1){
					$cmetode="Instant";
				}else{
					$cmetode="SameDay";
				}

				if( $personal_info['idmemberx'] == "IDSPAAA66834"){  //tambahan vera promo diskon gosend
					$arrx1 = array(
						"full_discount" => $CI->input->post("discount")
					);
					$CI->session->unset_userdata('promo');
					$CI->session->set_userdata('promo', $arrx1);
					//return $arrx1;
				}

				$harga=substr($metode,2);
				$charga=str_replace(".","",$harga);
				$charga= $charga * 1;

				$arrx = array(
					"origin_name" => "K-Link Tower",
					"destination_name" => $CI->input->post("alamat"),
					"service_code" => $CI->input->post("no_order"),
					"service_display" => $cmetode,
					"price" => $charga,
					"etd_from" => "",
					"etd_thru" => "",
					"times" => "",
					"ship_discount" => $CI->input->post("discount"),
					"lat_dest" => $CI->input->post("lat_dest"),
					"long_dest" => $CI->input->post("long_dest")
				);

				//print_r($arrx);
				$CI->session->unset_userdata('shipping_jne_info');
				$CI->session->set_userdata('shipping_jne_info', $arrx);
				
				//echo "tes : ";
				//print_r($arrx);
				return $arrx;

			}
		}

 }

	function cancel_gsend($client_id,$pass_key,$conote){
		$curl = curl_init();

		$fields = array(
			'orderNo' => $conote
		);
		$postData = setFieldPost($fields); //create parameter

		//$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		//$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v3/calculate/price?",
			CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking/cancel",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function booking_gsend_dev($client_id,$pass_key,$senderName,$senderNote,$senderContact,$senderPhone,$senderCoord,$senderAlamat,$metode,$des_name,$des_phone,$des_add,$des_coord,$item,$price){
		$curl = curl_init();
		$body='{
		   "paymentType": 3,
		   "deviceToken": "",
		   "collection_location":"pickup",
		  "shipment_method":"'.$metode.'",
		   "routes":
		   [{
			   "originName": "'.$senderName.'",
			   "originNote": "'.$senderNote.'",
			   "originContactName": "'.$senderContact.'",
			   "originContactPhone": "'.$senderPhone.'",
			   "originLatLong": "'.$senderCoord.'",
			   "originAddress": "'.$senderAlamat.'",
			   "destinationName": "",
			   "destinationNote": "",
			   "destinationContactName": "'.$des_name.'",
			   "destinationContactPhone": "'.$des_phone.'",
			   "destinationLatLong": "'.$des_coord.'",
			   "destinationAddress": "'.$des_add.'",
			   "item": "'.$item.'",
			   "storeOrderId":"",
			   "insuranceDetails":
				{
				  "applied": "false",
				  "fee": "",
				  "product_description": "'.$item.'",
				  "product_price": "'.$price.'"
				}
		  }]}';
			/* $body='
{
  "paymentType": 3,
  "deviceToken": "",
  "collection_location":"pickup",
 "shipment_method":"SameDay",
  "routes":
  [{
      "originName": "",
      "originNote": "",
      "originContactName": "The Kingdom Shop",
      "originContactPhone": "6285201311802",
      "originLatLong": "-6.1263348,106.7890888",
      "originAddress": "Jalan Pancoran Buntu I, Pancoran, 12780, Note origin example blablablaxhsdhs",
      "destinationName": "",
      "destinationNote": "",
      "destinationContactName": "Toko Jaya Agung",
      "destinationContactPhone": "6281254564161",
      "destinationLatLong": "-6.284508001748839,106.8295789",
      "destinationAddress": "Jalan Jatianom, Pasar Minggu, 12540, Note destination example",
      "item": "Shoes with size 27",
      "storeOrderId":"",
      "insuranceDetails":
       {
         "applied": "false",
       "fee": "2500",
         "product_description": "LED TV",
         "product_price": "3500000"
       }
 }]}
';*/
		//echo $body;
		$arx = array(
		   CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gokilat/v10/booking",
		   //CURLOPT_URL => "https://kilat-api.gojekapi.com/gokilat/v10/booking",
		   CURLOPT_RETURNTRANSFER => true,
		   CURLOPT_ENCODING => "",
		   CURLOPT_MAXREDIRS => 10,
		   CURLOPT_TIMEOUT => 30,
		   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		   CURLOPT_CUSTOMREQUEST => "POST",
		   CURLOPT_POSTFIELDS => $body,
		   CURLOPT_HTTPHEADER => array(
			   // masukkan data header disini
			   "Content-Type: application/json",
			   "Client-ID: $client_id",
			   "Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   return $err;
		   //echo($err);
		} else {
		   return $response;
		   //echo($response);
		   //print_r($response);
		}
}

	function price_gsend_dev($client_id,$pass_key,$x,$y){
		$curl = curl_init();

		$fields = array(
			'origin' => $x,
			'destination' => $y,
			'paymentType' => '3'
		);
		$postData = setFieldPost($fields); //create parameter

		//$client_id = "k-link-engine";
		//$pass_key = "bc9fcccb8c29faf50171fadfcad09069747785b81b53c8be03dc265e3186e907"; //Development
		//$pass_key = "93ba01448ac1ff0db93fb96bf04d093628544e3e6c68d66a441ba5be2d49a1b6";

		$arx = array(
			//CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gojek/v3/calculate/price?",
			CURLOPT_URL => "https://integration-kilat-api.gojekapi.com/gokilat/v10/calculate/price",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_HTTPHEADER => array(
				// masukkan data header disini
				"Client-ID: $client_id",
				"Pass-Key: $pass_key"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return $err;
			//print_r($err);
		} else {
			return $response;
			//echo($response);
			//print_r($response);
		}
	}

	function getUniqueCode($orderno,$nominal){

		$random=rand(100,1000);
		$nom= ($nominal * 1) + ($random * 1);

		if($orderno !="" && $nominal !=""){
			$r= "true";
			$arr = array("response" => $r, "error"=> "","last_nom" => $nom);
		}
		else{
			$r= "false";
			$arr = array("response" => $r, "error"=> "Error Get Kode!","last_nom" => $nominal);
		}

		echo json_encode($arr);
	}

	function get_api_payment(){
		$curl = curl_init();
		$body='{
				  "nominal": "100500",
				  "no_rek_tujuan": "94409546454",
				  "nama_rek_tujuan": "Vera",
				  "type": "Cr"
				}';

		//echo $body;
		$arx = array(
		   CURLOPT_URL => "http://service-mutasi.k-net.co.id/api/api_payment",
		   CURLOPT_RETURNTRANSFER => true,
		   CURLOPT_ENCODING => "",
		   CURLOPT_MAXREDIRS => 10,
		   CURLOPT_TIMEOUT => 30,
		   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		   CURLOPT_CUSTOMREQUEST => "POST",
		   CURLOPT_POSTFIELDS => $body,
		   CURLOPT_HTTPHEADER => array(
			   // masukkan data header disini
			   "Content-Type: application/json"));
		curl_setopt_array($curl, $arx);
		//ganti start dan end date sesuai data post yg diinginkan

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   return $err;
		   //echo($err);
		} else {
		   return $response;
		   //echo($response);
		  print_r($response);
		}
}




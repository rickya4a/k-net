var $ = jQuery;

var be_trans_klink = {
	getListTrx : function() {
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("transklink/searchTrx/list"), $(All.get_active_tab() + " #formTrxSearch").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);
		});
	},
	
	getDetailProduct : function(idx) {
		var id = idx.id;
		$.ajax({
			url : All.get_url('transklink/detTrxByID/') + id,
			type : 'GET',
			success : function(data) {
				if (data == "false") {
					alert("Can not found detail product..!");
				} else {
					$(All.get_active_tab() + " .mainForm").hide();
					All.clear_div_in_boxcontent(".nextForm1");
					$(All.get_active_tab() + " .nextForm1").html(data);
				}
			}
		});
	},
	
	getStockist : function(loccd) {
		All.set_disable_button();
		All.get_image_load2();

		$.post(All.get_url('trans/posting/list'), $(All.get_active_tab() + " #formSalesRecap").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='6' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='10%'>No</th>";
				rowshtml += "<th width='15%'>Order No</th>";
				rowshtml += "<th>Member Name</th>";
				rowshtml += "<th width='15%'>Total DP</th>";
				rowshtml += "<th width='10%'>Total BV</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " name=orderno[] value=" + value.orderno + " /></td>";
					rowshtml += "<td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.orderno + "</td>";
					rowshtml += "<td>" + value.nmmember + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td></tr>";

				});

				rowshtml += "</tbody>";

				rowshtml += "</table>";
				rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},
	
	getFullNameByID : function(nilai, urlX, setValue) {
		var xnil = nilai;
		//alert(xnil);
        All.set_disable_button();
		$.ajax({
            url: All.get_url(urlX) + "/" +nilai,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                All.set_enable_button();
                if(data.response == "true") {
                	$(All.get_active_tab() + " " + setValue).val(data.arrayData[0].fullnm);
				} else {
					alert("Data "+nilai+ " not found");
					$(All.get_active_tab() + " " + setValue).val(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    	
    getSalesRecap : function(is_xls) {
    	//alert(is_xls);
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("transklink/recapSales/list/" +is_xls), $(All.get_active_tab() + " #formSalesRecap").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);
		});
	},
	
	getBonusRecap : function(is_xls) {
    	//alert(is_xls);
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("transklink/recapBonus/list/" +is_xls), $(All.get_active_tab() + " #formBonusRecap").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);
		});
	},
	
	readFromFile : function() {
		var formData = new FormData($(All.get_active_tab() + " #formUploadGNV")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('transklink/gnvUploadPrev'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});

	},
	
	clear : function() {
		All.clear_div_in_boxcontent(".mainForm > .result");
	},

	saveGNVFileToDatabase : function() {
		var formData = new FormData($(All.get_active_tab() + " #formUploadGNV")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('transklink/gnvUploadAction'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
	},

	importPromoToDB : function() {
		var formData = new FormData($(All.get_active_tab() + " #formImportPromo")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('be/promo/importayu'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
	},
	
	setBonusPeriod: function(param) {
		var formData = new FormData($(All.get_active_tab() + " #formUpdatePeriod")[0]);
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		All.get_wait_message();
		$.ajax({
			url : All.get_url('transklink/setBnsPeriodAction/' +param),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				//$(All.get_box_content() + ".mainForm > .result").html(data);
				$(All.get_active_tab() + " .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
		/*
		All.set_disable_button();
		$.post(All.get_url("transklink/setBnsPeriodAction"), $(All.get_active_tab() + " #formUpdatePeriod").serialize(), function(result) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			var rowshtml = result.currperiod;
			$(All.get_active_tab() + " .result").append(rowshtml);
		}, "json");*/
		
	},
    
    listBVByBnsmonth : function(){
        All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("send/bv/act"), $(All.get_active_tab() + " #frmBV").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);
		});
    },
    
    listBnsRpt : function(){
        All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("send/bnsreport/act"), $(All.get_active_tab() + " #frmBV").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);
		});
    },
	
	
}
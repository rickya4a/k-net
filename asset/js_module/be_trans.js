var $ = jQuery;

var be_trans = {
	data : {
		totalbukanvoucher: 0,
		totalvoucher: 0,
		kembalian: 0,
		cashDouble: 0
	},

	reconcileXlPaketData: function(paramValue) {
		All.set_disable_button();
		//var trx_id = 
		//alert("param :" +paramValue);

		$.ajax({
			url: All.get_url("xl/redeem/stock"),
			type: 'POST',
			dataType: "json",
			data:{trx_id: paramValue},
			success:
				function(data){
					All.set_enable_button();
					var response = data.response;
					if(response == "true") {
						var arrayData = data.arrayData;
						alert(data.message + ", reff id : " +arrayData.reff_id);
						$(All.get_active_tab() + "td#" +paramValue).html(null);
						$(All.get_active_tab() + "td#" +paramValue).html("<font color='lightgreen'>Reconciled</font>");
					} else {
						alert(data.message);
					}
				},
			error: function(jqXHR, textStatus, errorThrown) {
				All.set_enable_button();
			}
		});
	},

	getListTrxToPost : function() {
		All.set_disable_button();
		All.get_image_load2();

		$.post(All.get_url('trans/posting/list'), $(All.get_active_tab() + " #formPostTrx").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='6' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='10%'>No</th>";
				rowshtml += "<th width='15%'>Order No</th>";
				rowshtml += "<th>Member Name</th>";
				rowshtml += "<th width='15%'>Total DP</th>";
				rowshtml += "<th width='10%'>Total BV</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " name=orderno[] value=" + value.orderno + " /></td>";
					rowshtml += "<td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.orderno + "</td>";
					rowshtml += "<td>" + value.nmmember + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td></tr>";

				});

				rowshtml += "</tbody>";

				rowshtml += "</table>";
				rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getListCN : function() {
		All.set_disable_button();
		All.get_image_load2();

		$.post(All.get_url('trans/print/cn/list'), $(All.get_active_tab() + " #formCN").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='8' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th width='5%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='5%'>No</th>";
				rowshtml += "<th width='14%'>CN No</th>";
				rowshtml += "<th width='14%'>KW No</th>";
				rowshtml += "<th>Member</th>";
				rowshtml += "<th width='10%'>ID Stockist</th>";
				rowshtml += "<th width='10%'>Total DP</th>";
				rowshtml += "<th width='5%'>Total BV</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " name=cnno[] value=" + value.CNno + " /></td>";
					rowshtml += "<td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.CNno + "</td>";
					rowshtml += "<td align=center>" + value.KWno + "</td>";
					rowshtml += "<td align=center>" + value.id_memb + " / " + value.nmmember + "</td>";
					rowshtml += "<td align=center>" + value.idstk + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td></tr>";

				});

				rowshtml += "</tbody>";

				rowshtml += "</table>";
				rowshtml += "<input type='submit' class='btn btn-small btn-success' value='Print CN' />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getListKW : function() {
		All.set_disable_button();
		All.get_image_load2();

		$.post(All.get_url('trans/print/kw/list'), $(All.get_active_tab() + " #formKW").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='6' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th width='5%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='8%'>No</th>";
				rowshtml += "<th width='14%'>KW No</th>";
				/*rowshtml += "<th>Stockist Name</th>";
				 rowshtml += "<th width='15%'>Total DP</th>";
				 rowshtml += "<th width='10%'>Total BV</th>";*/
				rowshtml += "<th>Member</th>";
				rowshtml += "<th width='10%'>ID Stockist</th>";
				rowshtml += "<th width='10%'>Total DP</th>";
				rowshtml += "<th width='5%'>Total BV</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " name=kwno[] value=" + value.KWno + " /></td>";
					rowshtml += "<td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.KWno + "</td>";
					rowshtml += "<td align=center>" + value.id_memb + " / " + value.nmmember + "</td>";
					rowshtml += "<td align=center>" + value.idstk + "</td>";
					//rowshtml += "<td>"+value.nmstkk+"</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td></tr>";

				});

				rowshtml += "</tbody>";

				rowshtml += "</table>";
				rowshtml += "<input type='submit' class='btn btn-small btn-success' value='Print KW' />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getListIP : function() {
		All.set_disable_button();
		All.get_image_load2();

		$.post(All.get_url('trans/print/ip/list'), $(All.get_active_tab() + " #formIP").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='6' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th width='5%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='8%'>No</th>";
				rowshtml += "<th width='14%'>IP No</th>";
				rowshtml += "<th>Member Name</th>";
				rowshtml += "<th width='15%'>Total DP</th>";

				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " name=ipno[] value=" + value.trcd + " /></td>";
					rowshtml += "<td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.trcd + "</td>";
					rowshtml += "<td>" + value.id_memb + " - " + value.nmmember + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.amount)) + "</td>";

				});

				rowshtml += "</tbody>";

				rowshtml += "</table>";
				rowshtml += "<input type='submit' class='btn btn-small btn-success' value='Print IP' />";
				rowshtml += "<input type='button' class='btn btn-small btn-success' value='Tes Get Param' onclick='be_trans.tesGetParam()' />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	tesGetParam : function() {
		var x  = $(All.get_active_tab() + " #formIP").serialize();
		console.log(x);
	},

	getListTrxReportBE : function() {
		All.set_disable_button();
		All.get_image_load();
		alert("sdsdsd");
		$.ajax({
			url: All.get_url("trans/report/list"),
			type: 'POST',
			dataType: "json",
			data: $(All.get_active_tab() + " #formTrxReport").serialize(),
			success:
				function(data){
					console.log(data);
				},
			error: function(jqXHR, textStatus, errorThrown) {
				All.set_enable_button();
			}
		});
	},

	getListTrxReport : function() {
		All.set_disable_button();
		All.clear_div_in_boxcontent(".mainForm .result");
		All.get_image_load2();
		$.post(All.get_url('trans/report/list'), $(All.get_active_tab() + " #formTrxReport").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			//console.log("ISI : " +data);
			if (data.response == "false") {
				All.set_error_message(" .mainForm .result");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				var trx_type = $(All.get_active_tab() + " #trx_type").val();
				if(trx_type == "TDKLENGKAP") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					rowshtml += "<th width='12%'>No Token</th>";
					rowshtml += "<th>Total Pay</th>";
					rowshtml += "<th>Total BV</th>";
					rowshtml += "<th>Pembayaran</th>";
					rowshtml += "<th>No HP Konfirmasi</th>";
					rowshtml += "<th>Pemilik No HP Konf</th>";
					rowshtml += "<th>Trx Date</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {
						var urxc = 'trans/id2/'+value.token;
						rowshtml += "<tr>";
						//rowshtml += "<td>" + value.orderno + "</td>";
						//rowshtml += "<td align=center><a href=# id='" + value.orderno + "' onclick=javascript:be_trans.getDetailTrx(this)>" + value.orderno + "</a></td>";
						//rowshtml += "<td align=center><a href=# id='" + value.token + "' onclick=javascript:be_voucher.getDetailTrx2(this)>" + value.token + "</a></td>";
						rowshtml += "<td align=center><a href=# onclick=javascript:All.ajaxShowDetailonNextForm('"+urxc+"')>" + value.token + "</a></td>";
						//rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=center>" + value.total_pay + "</td>";
						rowshtml += "<td align=right>" + value.total_bv + "</td>";
						rowshtml += "<td align=right>" + value.bankDisplayNm + "</td>";
						rowshtml += "<td align=right>" + value.no_hp_konfirmasi + "</td>";
						rowshtml += "<td align=right>" + value.pemilik_hp_konfirmasi + "</td>";
						rowshtml += "<td align=center>" + value.datetrans + "</td>";
						rowshtml += "</tr>";

					});

					rowshtml += "</tbody>";
					rowshtml += "</table>";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				} else if(trx_type == "failRegMampirkak") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					rowshtml += "<th width='12%'>Order No</th>";
					rowshtml += "<th width='12%'>Token No</th>";
					rowshtml += "<th>ID Member</th>";
					rowshtml += "<th>Member Name</th>";
					rowshtml += "<th>No HP Konfirmasi</th>";
					rowshtml += "<th>Pemilik No HP Konf</th>";
					rowshtml += "<th>Trx Date</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {
						rowshtml += "<tr>";
						//rowshtml += "<td>" + value.orderno + "</td>";
						rowshtml += "<td align=center><a href=# id='" + value.orderno + "' onclick=javascript:be_trans.getDetailTrx(this)>" + value.orderno + "</a></td>";
						rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=center>" + value.id_memb + "</td>";
						rowshtml += "<td align=right>" + value.nmmember + "</td>";
						rowshtml += "<td align=right>" + value.no_hp_konfirmasi + "</td>";
						rowshtml += "<td align=right>" + value.pemilik_hp_konfirmasi + "</td>";
						rowshtml += "<td align=center>" + value.datetrans + "</td>";
						rowshtml += "</tr>";

					});

					rowshtml += "</tbody>";
					rowshtml += "</table>";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				}
				else if(trx_type == "double") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					rowshtml += "<th width='12%'>Order No</th>";
					//rowshtml += "<th width='12%'>CN No</th>";
					rowshtml += "<th width='12%'>Token No</th>";
					rowshtml += "<th>ID Member</th>";
					//rowshtml += "<th>Member Name</th>";
					rowshtml += "<th>DP</th>";
					rowshtml += "<th>BV</th>";
					rowshtml += "<th>Trx Date</th>";
					rowshtml += "<th>Sent To</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {
						rowshtml += "<tr>";
						rowshtml += "<td>" + value.orderno + "</td>";
						rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=center>" + value.id_memb + "</td>";
						//rowshtml += "<td align=right>" + value.nmmember + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td>";
						rowshtml += "<td align=center>" + value.datetrans + "</td>";
						rowshtml += "<td align=center>" + value.sent + "</td>";
						rowshtml += "</tr>";

					});

					rowshtml += "</tbody>";
					rowshtml += "</table>";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				}
				else if(trx_type == "PRD") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='4' bgcolor='#lightgrey'>Product Summary</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					rowshtml += "<th width='10%'>No</th>";
					rowshtml += "<th width='20%'>Product Code</th>";
					rowshtml += "<th>Product Name</th>";
					rowshtml += "<th width='15%'>Qty</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totQty = 0;
					var qtyx;
					var qtyS = 0;
					$.each(arrayData, function(key, value) {
						qtyx = parseInt(value.total_qty);
						rowshtml += "<tr><td align=center>" + (key+1) + "</td>";
						rowshtml += "<td align=center>" + value.prdcd + "</td>";
						rowshtml += "<td align=left>" + value.prdnm + "</td>";
						rowshtml += "<td align=right>" + All.num(qtyx) + "</td></tr>";
						qtyS += qtyx;
					});
					rowshtml += "</tbody>";
					rowshtml += "<tr><td align=center colspan=3><b>TOTAL QTY</b></td><td align=right>"+qtyS+"</td></tr>";
					rowshtml += "</table>";

					//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					//All.set_datatable();
				} else if(trx_type == "TOP") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='5' bgcolor='#lightgrey'>Top Up Virtual Account</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					//rowshtml += "<th width='10%'>No</th>";
					rowshtml += "<th width='20%'>Trx ID</th>";
					rowshtml += "<th>ID Member</th>";
					rowshtml += "<th width='15%'>Date</th>";
					rowshtml += "<th width='18%'>Amount</th>";
					rowshtml += "<th width='15%'>Bank Desc</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totQty = 0;
					var qtyx;
					var qtyS = 0;
					$.each(arrayData, function(key, value) {
						qtyx = parseInt(value.total_qty);
						//rowshtml += "<tr><td align=center>" + (key+1) + "</td>";
						rowshtml += "<td align=center>" + value.trcd + "</td>";
						rowshtml += "<td align=left>" + value.dfno + " / " + value.fullnm +"</td>";
						rowshtml += "<td align=center>" + value.createdt + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.amount)) + "</td>";
						rowshtml += "<td align=center>" + value.bankDesc + "</td></tr>";
						qtyS += parseInt(value.amount);
					});
					rowshtml += "</tbody>";
					//rowshtml += "<tr><td align=center colspan=3><b>TOTAL QTY</b></td><td align=right>"+All.num(qtyS)+"</td><td>&nbsp;</td></tr>";
					rowshtml += "</table>";

					//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				} else {
					//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='12' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					rowshtml += "<th width='12%'>Order No</th>";
					rowshtml += "<th width='12%'>CN No</th>";
					rowshtml += "<th width='12%'>Token No</th>";
					rowshtml += "<th>Member Name</th>";
					rowshtml += "<th width='9%'>Stockist</th>";
					rowshtml += "<th width='9%'>Trx Date</th>";
					rowshtml += "<th width='4%'>Qty</th>";
					rowshtml += "<th width='8%'>DP</th>";
					rowshtml += "<th width='5%'>BV</th>";
					rowshtml += "<th width='4%'>CN Stat</th>";
					rowshtml += "<th width='4%'>BV Stat</th>";
					rowshtml += "<th width='4%'>Period</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {

						//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
						//rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
						if(value.sumofPrd == null || value.sumofPrd == "" || value.sumofPay == null || value.sumofPay == "") {
							var urxc = 'trans/reconcile/id/' + value.orderno;
							rowshtml += "<tr bgcolor='orange'><td align=center><a class='btn btn-mini btn-warning' href=# id='" + value.orderno + "' onclick=javascript:All.ajaxShowDetailonNextForm('"+urxc+"')>" + value.orderno + "</a></td>";
						} else {
							rowshtml += "<tr><td align=center><a href=# id='" + value.orderno + "' onclick=javascript:be_trans.getDetailTrx(this)>" + value.orderno + "</a></td>";
						}
						rowshtml += "<td align=center>" + value.CNno + "</td>";
						var notoken =  value.token;
						var kdk = notoken.substring(0, 2);

						if((kdk == "RM" || kdk == "RA" || kdk == "LP") && value.generate_idmemb === null) {
							rowshtml += "<td align=center><font color='red'><strong>MEMBER FAIL</strong></font></td>";
						} else {
							rowshtml += "<td align=center>" + value.token + "</td>";
						}
						rowshtml += "<td>" + value.nmmember + "</td>";
						rowshtml += "<td align=center>" + value.idstk + "</td>";
						rowshtml += "<td align=center>" + value.datetrans1 + "</td>";
						rowshtml += "<td align=right>" + value.sumofPrd + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td>";

						if(value.CN_STAT == '1'){
							//CNStat = 'Settled';
							CNStat = '1';
						}else{
							//CNStat = 'Unsettled';
							CNStat = '0';
						}
						if(value.BV_STAT == '1'){
							//BVStat = 'Settled';
							BVStat = '1'
						}else{
							//BVStat = 'Unsettled';
							BVStat = '0'
						}
						rowshtml += "<td align=center>" + CNStat + "</td>";
						rowshtml += "<td align=center>" + BVStat + "</td>";
						rowshtml += "<td align=right>" + value.bonusmonth + "</td>";
						totDP = totDP + parseInt(value.total_pay);
						totBV = totBV + parseInt(value.total_bv);

					});

					rowshtml += "</tbody></tr>";

					rowshtml += "</table>";
					rowshtml += "<table class='table table-striped table-bordered' width='100%'>";
					rowshtml += "<th><td align=center>T O T A L</th><th align=right width='8%'>" + All.num(totDP) + "</th><th align=right width='6%'>" + All.num(totBV) + "</th></tr>";
					rowshtml += "</table>";
					//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				}

			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	reconcileTrxData : function(orderno, prd_stt, ship_stt, pay_stt) {
		//var orderno = idx.id;
		$.ajax({
			url : All.get_url('trans/reconcile/save/') + orderno +"/"+ prd_stt +"/"+ ship_stt +"/"+pay_stt,
			type : 'GET',
			success : function(data) {
				$(All.get_active_tab() + " #recData").html(null);
				$(All.get_active_tab() + " #recData").html(data);
			}
		});
	},

	getDetailTrx : function(idx) {
		var orderno = idx.id;
		//alert('ORDERNO NYA ADALAH' + orderno);
		$.ajax({
			url : All.get_url('trans/id/') + orderno,
			dataType : 'json',
			type : 'GET',
			success : function(data) {
				if (data.response == "true") {
					$(All.get_active_tab() + " .mainForm").hide();
					All.clear_div_in_boxcontent(".nextForm1");
					var header = data.header;
					var detail = data.detail;
					var payment = data.payment;
					var shipaddr = data.shipaddr;
					var member = data.member;
					var payConnect = 0;
					var rowshtml = "<form>";
					rowshtml += "<table class='table table-striped table-bordered' width=100%>";
					rowshtml += "<tr><th colspan=4>TRANSACTION DATA</th></tr>";
					rowshtml += "<tr><td width=20%>";
					rowshtml += "<input type='hidden' id='notrx' name='notrx' value='"+header[0].orderno+"' />";
					rowshtml += "<input type='hidden' id='temptrx' name='temptrx' value='"+header[0].token+"' />";
					rowshtml += "Orderno / Token No</td><td>" + header[0].orderno + " / " + header[0].token + "</td><td>Total BV</td><td align=right>" + All.num(parseInt(header[0].total_bv)) + "</td></tr>";
					rowshtml += "<tr><td>CN No / Register No</td><td>" + header[0].CNno + " / " + header[0].REGISTERno + " </td><td width=20%>Total Pay</td><td align=right>" + All.num(parseInt(header[0].total_pay)) + "</td></tr>";
					rowshtml += "<tr><td>KW No</td><td>" + header[0].KWno + "</td><td>Shipping Cost</td><td align=right>" + All.num(parseInt(header[0].payShip)) + "</td></tr>";
					rowshtml += "<tr><td>IP No</td><td>" + header[0].IPno + "</td><td>Disc Shipping</td>";
					if(header[0].discount_shipping !== null || header[0].discount_shipping !== "") {
						rowshtml += "<td align=right> -" + All.num(parseInt(header[0].discount_shipping)) + "</td></tr>";
					} else {
						rowshtml += "<td align=right>0</td></tr>";
					}
					if (header[0].payConnectivity != null) {
						payConnect = header[0].payConnectivity;
					}
					if(header[0].id_memb == null || header[0].id_memb == "" || header[0].nmmember == null || header[0].nmmember == "") {
						//var tombol = 'All.getFullNameByID(this.form.upd_id.value,"trx/reconcile/helper/msmemb/dfno","#upd_name")';
						rowshtml += "<tr><td>ID Member</td><td>";
						rowshtml += "<input type='text' style='width: 120px' id='upd_id' name='upd_id' value='"+ header[0].id_memb +"' />";
						rowshtml += "<input type='text' style='width: 230px' id='upd_name_baru' name='upd_name_baru' value='"+ header[0].nmmember +"' />";
						rowshtml += "&nbsp;<input type='button' class='btn btn-mini btn-primary' value='Check ID' onclick='be_trans.getFullNameMember()' />";
						//+ header[0].id_memb + " / " + header[0].nmmember + 
						rowshtml += "</td>";
					} else {
						rowshtml += "<tr><td>ID Member</td><td>" + header[0].id_memb + " / " + header[0].nmmember + "</td>";
					}
					rowshtml += "<td>Admin Cost</td><td align=right>" + All.num(parseInt(header[0].payAdm)) + "</td></tr>";
					rowshtml += "<tr>";
					if(header[0].no_hp_konfirmasi != null && header[0].no_hp_konfirmasi != "") {
						rowshtml += "<td>No HP Konfirmasi</td><td>" + header[0].no_hp_konfirmasi + "</td>";
					} else {
						rowshtml += "<td>Input By/Phone No</td><td>" + header[0].log_usrlogin + " / " + header[0].tel_hp + "</td>";
					}
					rowshtml += "<td>Connectivity Cost</td><td align=right>" + All.num(parseInt(payConnect)) + "</td>";


					//rowshtml +="<td>"+header[0].BV_STAT+"</td></tr>";
					rowshtml += "<tr><td>Trx Date ## Trx Pay Date</td><td>" + header[0].datetrans_sgo + " " + header[0].trx_time_sgo + " ## " + header[0].datetrans1 + " " + header[0].trx_time + "</td><td>CN Status</td>";
					if (header[0].CN_STAT == "1") {
						rowshtml += "<td>Settled</td></tr>";
					} else {
						rowshtml += "<td>Suspended</td></tr>";
					}



					//rowshtml +="<td>BV Status</td><td>"+header[0].BV_STAT+"</td></tr>";
					if(header[0].idstk == null || header[0].idstk == "null" ||  header[0].idstk == "" || header[0].pricecode == null || header[0].pricecode == "null"|| header[0].pricecode == "") {
						rowshtml += "<tr><td>ID Stockist</td>";
						var nilai_stk = "";
						if(header[0].idstk != null || header[0].idstk != "") {
							nilai_stk = header[0].idstk;
						}
						rowshtml += "<td><input type=text value='"+nilai_stk+"' id='newidstk' name='newidstk' /><input type=hidden id='orderno' name='orderno' value='"+header[0].orderno+"' />&nbsp;";
						rowshtml += "<input type='button' class='btn btn-mini btn-primary' value='Set ID Stockist' onclick='be_trans.recoverStkCodeTrx()' /></td>";
					} else {
						rowshtml += "<tr><td>ID Stockist</td><td>" + header[0].nmstkk + "</td>";
					}
					rowshtml += "<td>BV Status</td>";
					if (header[0].BV_STAT == "1") {
						rowshtml += "<td>Settled</td></tr>";
					} else {
						rowshtml += "<td>Suspended</td></tr>";
					}
					//rowshtml += "";
					//rowshtml += "<tr><td>Total Pay</td><td>"+All.num(parseInt(header[0].total_pay))+"</td></tr>";
					//rowshtml += "<tr><td>Total BV</td><td>"+All.num(parseInt(header[0].total_bv))+"</td></tr>";
					//rowshtml += "<tr><td>Shipping Cost</td><td>"+All.num(parseInt(header[0].payShip))+"</td></tr>";
					//rowshtml += "<tr><td>Admin Cost</td><td>"+All.num(parseInt(header[0].payAdm))+"</td></tr>";

					rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type=button value='<< Back' onclick=be_trans.back_to_form() /></td><td><input class='btn btn-mini btn-primary' href='#'  onclick='be_trans.getTrxForResendSMS2()' value='Send SMS' /></td><td>Bonus Period</td><td>" + header[0].bonusmonth + "</td></tr>";
					rowshtml += "</table>";
					//<input class='btn btn-mini btn-primary' href='#' id='" + header[0].orderno + "' onclick='be_trans.getTrxForResendSMS(this)' value='Send SMS' />&nbsp;
					if(header[0].generate_idmemb != null && header[0].generate_nmmemb != null) {
						rowshtml += "<table class='table table-striped table-bordered' width=100%>";
						rowshtml += "<tr><th colspan=2>MEMBERSHIP REGISTRATION</th></tr>";
						rowshtml += "<tr><td width=20%>Generate ID Member</td><td>"+header[0].generate_idmemb+" / "+header[0].generate_nmmemb+"</td></tr>"
						rowshtml += "</table>";
					}

					rowshtml += "<table class='table table-striped table-bordered' width=100%>";
					rowshtml += "<tr><th colspan=8>DETAIL PRODUCT</th></tr>";
					rowshtml += "<tr><th width=5%>No</th><th width=15%>ID</th><th>Product Name</th><th width=8%>Qty</th><th width=10%>DP</th><th width=5%>BV</th><th width=10%>Tot DP</th><th width=5%>Tot BV</th></tr>";
					var tot_dp = 0;
					var tot_bv = 0;
					if(detail != null) {
						$.each(detail, function(key, value) {
							//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
							rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
							rowshtml += "<td align=center>" + value.prdcd + "</a></td>";
							rowshtml += "<td>" + value.prdnm + "</td>";
							rowshtml += "<td align=right>" + All.num(parseInt(value.qty)) + "</td>";

							rowshtml += "<td align=right>" + All.num(parseInt(value.dpr)) + "</td>";
							rowshtml += "<td align=right>" + All.num(parseInt(value.bvr)) + "</td>";
							rowshtml += "<td align=right>" + All.num(parseInt(value.qty * value.dpr)) + "</td>";
							rowshtml += "<td align=right>" + All.num(parseInt(value.qty * value.bvr)) + "</td></tr>";
							tot_dp += (value.qty * value.dpr);
							tot_bv += (value.qty * value.bvr);

						});
						var urlUpdPrd = 'trans/updPrd/' + header[0].orderno;
						rowshtml += "<tr><td colspan=6>&nbsp;<input type='button' value='Update Product (Belom Jadi)' class='btn btn-mini btn-warning' onclick=All.ajaxShowDetailonNextForm2('"+urlUpdPrd+"') /></td><td align=right>" + All.num(parseInt(tot_dp)) + "</td><td align=right>" + All.num(parseInt(tot_bv)) + "</td></tr>";
					} else {
						rowshtml += "<tr><td colspan=6><font color='red'>DATA PRODUCT IS NOT EXIST</font></td></tr>";
					}
					rowshtml += "</table>";
					var tot_pay = 0;
					var tot_charge = 0;
					rowshtml += "<table class='table table-striped table-bordered' width=100%>";
					rowshtml += "<tr><th colspan=6>DETAIL PAYMENT INFO</th></tr>";
					rowshtml += "<tr><th width=5%>No</th><th width=30%>payment Type</th><th width=20%>Doc No</th><th width=13%>Pay Amount</th><th width=13%>Charge</th><th>Status</th></tr>";
					if(payment != null) {
						$.each(payment, function(key, value) {
							//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
							//rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
							rowshtml += "<tr><td align=right>" + (key + 1) + "</a></td>";
							rowshtml += "<td align=center>" + value.bankDisplayNm + "</a></td>";
							rowshtml += "<td align=center>" + value.docno + "</a></td>";
							//rowshtml += "<td align=center>" + value.token + "</td>";
							rowshtml += "<td align=right>" + All.num(parseInt(value.payamt)) + "</td>";
							rowshtml += "<td align=right>" + All.num(parseInt(value.charge_admin)) + "</td>";
							rowshtml += "<td align=center>" + value.notes + "</td></tr>";
							tot_pay += parseInt(value.payamt);
							tot_charge += parseInt(value.charge_admin);

						});
						rowshtml += "<tr><td colspan=3 align=center><b>T O T A L</b></td><td align=right>" + All.num(parseInt(tot_pay)) + "</td><td align=right>" + All.num(parseInt(tot_charge)) + "</td><td>&nbsp;</td></tr>";
					} else {
						rowshtml += "<tr><td colspan=6><font color='red'>DATA PAYMENT IS NOT EXIST</font></td></tr>";
					}

					rowshtml += "</table>";
					if(shipaddr != null) {
						if (header[0].sentTo == "2") {
							rowshtml += "<table class='table table-striped table-bordered' width=100%>";
							rowshtml += "<tr><th colspan=2>SHIPPING INFO</th></tr>";
							rowshtml += "<tr><td width=20%>Shipper</td><td>" + shipaddr[0].shipper_name + "</td></tr>";
							rowshtml += "<tr><td width=20%>Conote</td><td>" + header[0].conoteJNE + "</td></tr>";
							//rowshtml += "<tr><td width=20%>Service Type</td><td>" + shipaddr[0].service_type_name + "</td></tr>";
							if(shipaddr[0].conote_new !== null) {
								rowshtml += "<tr><td width=20%>No Resi Fisik</td><td>" + shipaddr[0].conote_new + "  <a class='btn btn mini btn-primary' id="+shipaddr[0].conote_new+" onclick='be_trans.checkShippingStatus(this)'>Check Shipping Status</a></td></tr>";
							}
							if(shipaddr[0].cargo_id == "1" || shipaddr[0].cargo_id == "4") {
								rowshtml += "<tr><td>Service Type</td><td>" + header[0].service_type_name + "</td></tr>";
							}
							rowshtml += "<tr><td>Cell Phone</td><td>" + header[0].tel_hp1 + "</td></tr>";
							rowshtml += "<tr><td>Receiver Name</td><td>" + shipaddr[0].receiver_name + "</td></tr>";
							rowshtml += "<tr><td>Destination Address</td><td>" + header[0].addr1 + "</td></tr>";
							if(shipaddr[0].cargo_id == "1") {

								rowshtml += "<tr><td>Provinsi</td><td>" + shipaddr[0].PROPINSI + "</td></tr>";
								rowshtml += "<tr><td>Kecamatan</td><td>" + shipaddr[0].Kecamatan + "</td></tr>";
							} else if(shipaddr[0].cargo_id == "2") {
								rowshtml += "<tr><td>Area</td><td>" + shipaddr[0].kota_nama_kgp + "</td></tr>";
							}
							rowshtml += "<tr><td>Warehouse Code</td><td>" + shipaddr[0].whcd + " - " + shipaddr[0].wh_name + "</td></tr>";
							rowshtml += "<tr><td>Warehouse Address</td><td>" + shipaddr[0].whcd_addr1 +" "+ shipaddr[0].whcd_addr2 +"</td></tr>";
							//rowshtml += "<tr><td><input type=button class='btn btn-mini btn-primary' value='Check Shipping Status' onclick='be_trans.checkShippingStatus()' /></td><td>&nbsp;</td></tr>";
						} else {
							rowshtml += "<table class='table table-striped table-bordered' width=100%>";
							rowshtml += "<tr><th colspan=2>GOOD RECEIPT STATUS</th></tr>";
							rowshtml += "<tr><td width=20%>Security No</td><td>" + shipaddr[0].secno + "</td></tr>";
							if(shipaddr[0].status == "1" || shipaddr[0].tgl_ambil !== null) {

								rowshtml += "<tr><td>Receipt Date</td><td>" + shipaddr[0].tgl_ambil + " " + shipaddr[0].jam_ambil + "</td></tr>";
								rowshtml += "<tr><td>Stockist</td><td>" + shipaddr[0].userlogin + "</td></tr>";
							} else {
								rowshtml += "<tr><td>Status</td><td>Not picked</td></tr>";
							}
						}
					}
					rowshtml += "</form>";
					$(All.get_box_content() + ".nextForm1").append(rowshtml);
				} else {
					alert("No detail data found..!");
				}
			}
		});
	},

	getFullNameMember : function() {
		var idmember = $(All.get_active_tab() + " #upd_id").val();
		All.getFullNameByID(idmember,'trx/reconcile/helper/msmemb/dfno','#upd_name_baru');
	},

	recoverStkCodeTrx : function() {
		var newidstk = $(All.get_active_tab() + " #newidstk").val();
		var orderno = $(All.get_active_tab() + " #orderno").val();
		console.log("newidstk : " +newidstk+ ", orderno : " +orderno);
		All.set_disable_button();
		$.ajax({
			url : All.get_url('trans/upd/loccd'),
			type : 'POST',
			dataType: 'json',
			data : {newidstk : newidstk, orderno : orderno},
			success : function(data) {
				All.set_enable_button();
				alert(data.message);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},

	getInfoStockist: function(param) {
		var stk = $(All.get_active_tab() + " #loccd" +param).val();
		All.set_disable_button();
		$.ajax({
			url : All.get_url('api/stockist/info'),
			type : 'POST',
			dataType: 'json',
			data : {api_usr : All.api_usr, api_pwd : All.api_pwd, idstockist: stk},
			success : function(data) {
				All.set_enable_button();
				var arrayData = data.arrayData;
				if(data.response == "true") {
					$(All.get_active_tab() + " #pricecode" +param).val(arrayData[0].pricecode);
					$(All.get_active_tab() + " #stkfullnm" +param).val(arrayData[0].fullnm);
				} else {
					alert(data.message);
				}

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},

	/*===========start barcode_check=======================*/
	getDetailBarcode : function(idx) {
		var orderno = idx.id;
		var pSplit = orderno.split("**");
		var orderno1 = pSplit[0];
		var pType = pSplit[1];
		//alert('ORDERNO NYA ADALAH ' + orderno1);
		/*
		 $.ajax({
		 url : All.get_url('be/klink/stockist/barcode_check/det/') + orderno + '/' + ,
		 dataType : 'json',
		 type : 'GET',
		 success : function(data) {
		 if (data.response == "true") {
		 $(All.get_active_tab() + " .mainForm").hide();
		 All.clear_div_in_boxcontent(".nextForm1");
		 var header = data.header;
		 var detail = data.detail;
		 var rowshtml = "<form>";
		 rowshtml += "<table class='table table-striped table-bordered' width=100%>";
		 rowshtml += "<tr><th colspan=4>Transaction Detail</th></tr>";
		 rowshtml += "<tr><td width=20%>Orderno</td><td>" + header[0].orderno + "</td><td>Total BV</td><td align=right>" + All.num(parseInt(header[0].tbv)) + "</td></tr>";
		 rowshtml += "<tr><td>SSR/MSR No</td><td>" + header[0].batchno + " </td><td width=20%>Total DP</td><td align=right>" + All.num(parseInt(header[0].tdp)) + "</td></tr>";
		 rowshtml += "<tr><td>ID Member</td><td>" + header[0].dfno + " / " + header[0].fullnm + "</td></tr>";
		 rowshtml += "<tr><td>Trx Date</td><td>" + header[0].etdt + "</td></tr>";
		 rowshtml += "<tr><td>ID Stockist</td><td>" + header[0].loccd + " / " + header[0].sc_name + "</td><td>Bonus Period</td><td>" + header[0].bnsperiod + "</td></tr>";

		 rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type=button value='<< Back' onclick=be_trans.back_to_form() /></td><td colspan=3>&nbsp;</td>";
		 rowshtml += "</table>";

		 rowshtml += "<table class='table table-striped table-bordered' width=100%>";
		 rowshtml += "<tr><th colspan=8>DETAIL PRODUCT</th></tr>";
		 rowshtml += "<tr><th width=5%>No</th><th width=15%>ID</th><th>Product Name</th><th width=8%>Qty</th><th width=10%>DP</th><th width=5%>BV</th><th width=10%>Tot DP</th><th width=5%>Tot BV</th></tr>";
		 var tot_dp = 0;
		 var tot_bv = 0;
		 $.each(detail, function(key, value) {
		 //rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
		 rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
		 rowshtml += "<td align=center>" + value.prdcd + "</a></td>";
		 rowshtml += "<td>" + value.prdnm + "</td>";
		 rowshtml += "<td align=right>" + All.num(parseInt(value.qty)) + "</td>";

		 rowshtml += "<td align=right>" + All.num(parseInt(value.dpr)) + "</td>";
		 rowshtml += "<td align=right>" + All.num(parseInt(value.bvr)) + "</td>";
		 rowshtml += "<td align=right>" + All.num(parseInt(value.qty * value.dpr)) + "</td>";
		 rowshtml += "<td align=right>" + All.num(parseInt(value.qty * value.bvr)) + "</td></tr>";
		 tot_dp += (value.qty * value.dpr);
		 tot_bv += (value.qty * value.bvr);

		 });
		 rowshtml += "<tr><td colspan=6>&nbsp;</td><td align=right>" + All.num(parseInt(tot_dp)) + "</td><td align=right>" + All.num(parseInt(tot_bv)) + "</td></tr>";
		 rowshtml += "</table>";

		 rowshtml += "<table class='table table-striped table-bordered' width=100%>";
		 rowshtml += "<tr><th colspan=5>DETAIL PAYMENT INFO</th></tr>";
		 rowshtml += "<tr><th width=30%>payment Type</th><th width=20%>Token No</th><th width=13%>Pay Amount</th><th>Status</th></tr>";
		 $.each(payment, function(key, value) {
		 //rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
		 //rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";

		 rowshtml += "<td align=center>" + value.bankDisplayNm + "</a></td>";
		 rowshtml += "<td align=center>" + value.token + "</td>";
		 rowshtml += "<td align=right>" + All.num(parseInt(value.payamt)) + "</td>";
		 rowshtml += "<td align=center>" + value.notes + "</td></tr>";

		 });
		 rowshtml += "</table>";

		 if (header[0].sentTo == "2") {
		 rowshtml += "<table class='table table-striped table-bordered' width=100%>";
		 rowshtml += "<tr><th colspan=2>SHIPPING INFO</th></tr>";
		 rowshtml += "<tr><td width=20%>Conote JNE</td><td>" + header[0].conoteJNE + "</td></tr>";
		 if(shipaddr[0].conote_new !== null) {
		 rowshtml += "<tr><td width=20%>No Resi Fisik</td><td>" + shipaddr[0].conote_new + "  <a class='btn btn mini btn-primary' id="+shipaddr[0].conote_new+" onclick='be_trans.checkShippingStatus(this)'>Check Shipping Status</a></td></tr>";
		 }
		 rowshtml += "<tr><td>Service Type</td><td>" + header[0].service_type_name + "</td></tr>";
		 rowshtml += "<tr><td>Cell Phone</td><td>" + header[0].tel_hp1 + "</td></tr>";
		 rowshtml += "<tr><td>Receiver Name</td><td>" + shipaddr[0].receiver_name + "</td></tr>";
		 rowshtml += "<tr><td>Destination Address</td><td>" + header[0].addr1 + "</td></tr>";
		 rowshtml += "<tr><td>Provinsi</td><td>" + shipaddr[0].PROPINSI + "</td></tr>";
		 rowshtml += "<tr><td>Kecamatan</td><td>" + shipaddr[0].Kecamatan + "</td></tr>";
		 rowshtml += "<tr><td>Warehouse Code</td><td>" + shipaddr[0].whcd + "</td></tr>";
		 rowshtml += "<tr><td>Warehouse Address</td><td>" + shipaddr[0].whcd_addr1 +" "+ shipaddr[0].whcd_addr2 +"</td></tr>";
		 //rowshtml += "<tr><td><input type=button class='btn btn-mini btn-primary' value='Check Shipping Status' onclick='be_trans.checkShippingStatus()' /></td><td>&nbsp;</td></tr>";
		 } else {
		 rowshtml += "<table class='table table-striped table-bordered' width=100%>";
		 rowshtml += "<tr><th colspan=2>GOOD RECEIPT STATUS</th></tr>";
		 rowshtml += "<tr><td width=20%>Security No</td><td>" + shipaddr[0].secno + "</td></tr>";
		 if(shipaddr[0].tgl_ambil === null) {
		 rowshtml += "<tr><td>Status</td><td>Not picked</td></tr>";
		 } else {
		 rowshtml += "<tr><td>Receipt Date</td><td>" + shipaddr[0].tgl_ambil + " " + shipaddr[0].jam_ambil + "</td></tr>";
		 rowshtml += "<tr><td>Stockist</td><td>" + shipaddr[0].userlogin + "</td></tr>";
		 }
		 }
		 rowshtml += "</form>";
		 $(All.get_box_content() + ".nextForm1").append(rowshtml);
		 } else {
		 alert("No detail data found..!");
		 }
		 }
		 });
		 */
	},

	/*===========end barcode_check===================*/




	checkShippingStatus : function(nilai) {
		All.set_disable_button();
		var x = nilai.id;
		$.ajax({
			url : All.get_url('trans/conote/track/') + x,
			type : 'GET',
			success : function(data) {
				All.set_enable_button();
				$(All.get_active_tab() + " .nextForm1").hide();
				$(All.get_active_tab() + " .nextForm2").html(null);
				$(All.get_active_tab() + " .nextForm2").html(data);
			}
		});
	},

	back_to_form : function(clear_div, show_div) {
		//console.log("clear : " + clear_div + " show : " + show_div);
		$(All.get_active_tab() + " .nextForm1").html(null);
		$(All.get_active_tab() + " .mainForm").show();
	},

	savePostingTrx : function() {
		All.set_disable_button();
		All.get_image_load();
		console.log(All.get_active_tab() + " #formPostTrx");

		$.post(All.get_url('trans/posting/save'), $(All.get_active_tab() + " #formPostTrx").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "true") {
				All.set_success_message(".mainForm .result", data.message);
			} else {
				All.set_error_message(".mainForm .result", data.message);
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getListTrxSMS : function() {
		All.set_disable_button();
		All.get_image_load();

		$.post(All.get_url('trans/sms/list'), $(All.get_active_tab() + " #formSMSResend").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='8' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='7%'>No</th>";
				rowshtml += "<th width='20%'>Order No</th>";
				rowshtml += "<th>Member Name</th>";
				rowshtml += "<th>Phone No</th>";
				rowshtml += "<th width='10%'>Stockist</th>";
				rowshtml += "<th width='10%'>Trx Date</th>";
				rowshtml += "<th width='10%'>Total DP</th>";
				rowshtml += "<th>SMS</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
					rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.orderno + "</td>";
					rowshtml += "<td>" + value.nmmember + "</td>";
					if(value.no_hp_konfirmasi != null && value.no_hp_konfirmasi != "") {
						rowshtml += "<td>" + value.no_hp_konfirmasi + "</td>";
					} else {
						rowshtml += "<td>" + value.tel_hp + "</td>";
					}
					rowshtml += "<td align=center>" + value.idstk + "</td>";
					rowshtml += "<td align=center>" + value.datetrans1 + "</td>";
					rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
					rowshtml += "<td align=center><a class='btn btn-mini btn-primary' href=# id='" + value.token + "' onclick=javascript:be_trans.getTrxForResendSMSNotif('" + value.token + "')>Resend</a></td></tr>";

				});

				rowshtml += "</tbody></tr>";

				rowshtml += "</table>";
				//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getTrxForResendSMS : function(idx) {
		var orderno = idx.id;
		$.ajax({
			url : All.get_url('trans/sms/resend/') + orderno,
			dataType : 'json',
			type : 'GET',
			success : function(data) {
				if (data.response == "true") {
					alert(data.message);
				} else {
					alert("No detail data found..!");
				}
			}
		});

	},

	getTrxForResendSMS2 : function(idx) {
		var orderno = $(All.get_active_tab() + " #notrx").val();
		var temptrx = $(All.get_active_tab() + " #temptrx").val();
		$.ajax({
			url : All.get_url('trans/sms/resend2/') + orderno +"/"+ temptrx,
			dataType : 'json',
			type : 'GET',
			success : function(data) {
				if (data.response == "true") {
					alert(data.message);
				} else {
					alert("No detail data found..!");
				}
			}
		});

	},

	getTrxForResendSMSNotif: function(temp_orderno) {
		$.ajax({
			url : All.get_url('trans/sms/resend2/') + "XX" +"/"+ temp_orderno,
			dataType : 'json',
			type : 'GET',
			success : function(data) {
				if (data.response == "true") {
					alert(data.message);
				} else {
					alert("No detail data found..!");
				}
			}
		});
	},

	getListEmail : function() {
		All.set_disable_button();
		All.get_image_load();
		$.post(All.get_url('trans/email/list/send'), $(All.get_active_tab() + " #listSendConote").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");

			if (data.response == "true") {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Conote JNE</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
				rowshtml += "<th width='7%'>No</th>";
				rowshtml += "<th width='20%'>Order No</th>";
				rowshtml += "<th width='10%'>Trx Date</th>";
				rowshtml += "<th>Conote JNE</th>";
				rowshtml += "<th width='20%'>Service Code</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";
				$.each(arrayData, function(key, value) {
					rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " name=conoteJNE[] value=" + value.conoteJNE + "|" + value.service_type_id + " /></td>";
					rowshtml += "<td align=right>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.orderno + "</td>";
					rowshtml += "<td>" + value.datetrans + "</td>";
					rowshtml += "<td align=center>" + value.conoteJNE + "</td>";
					rowshtml += "<td align=center>" + value.service_type_name + "</td></tr>";

				});

				rowshtml += "</tbody></tr>";

				rowshtml += "</table>";
				rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Send to Email' onclick=be_trans.sendListConote() />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			} else {
				alert(data.message);
			}
		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	chooseDatePick : function() {
		All.clear_div_in_boxcontent(".mainForm .result");
		var data = "<form>";
		data += "Date Pick : <input type=text id=datepick name=datepick class='datepicker' />";
		data += "Time Pick : <input type=text id=timepick name=timepick />";
		data += "</form>";
		$(All.get_box_content() + ".mainForm .result").html(data);
	},

	sendListConote : function() {
		All.set_disable_button();
		All.get_image_load();
		$.post(All.get_url('trans/email/conote/send'), $(All.get_active_tab() + " #listSendConote").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			$(All.get_box_content() + ".mainForm .result").html(data);
		}).fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getListShippingCommerce : function() {
		All.set_disable_button();
		All.get_image_load();
		$.post(All.get_url('print/shipping/label/list'), $(All.get_active_tab() + " #formPrintShippingLabel").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			$(All.get_box_content() + ".mainForm .result").html(data);
		}).fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	getDetailShipProduct : function(idx) {
		var id = idx.id;
		$.ajax({
			url : All.get_url('print/shipping/orderno/') + id,
			type : 'GET',
			success : function(data) {
				if (data == "false") {
					alert("Can not found detail product..!");
				} else {
					$(All.get_active_tab() + " .mainForm").hide();
					All.clear_div_in_boxcontent(".nextForm1");
					$(All.get_active_tab() + " .nextForm1").html(data);
				}
			}
		});
	},

	//====================START HILAL BACKEND REPORT VT===============================
	getListVTrpt : function() {
		All.set_disable_button();
		$(All.get_active_tab() + " #listReportVT").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("trans/report/VT/act/") + "1", $(All.get_active_tab() + " #frmReportVT").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " #listReportVT").html(null);
			$(All.get_active_tab() + " #listReportVT").html(hasil);
		});
	},
	//====================END HILAL BACKEND REPORT VT===============================

	readFromFile : function() {
		var formData = new FormData($(All.get_active_tab() + " #formUploadSGO")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('trans/sgo/import/preview'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});

	},

	clear : function() {
		All.clear_div_in_boxcontent(".mainForm > .result");
	},

	saveSGOFileToDatabase : function() {
		var formData = new FormData($(All.get_active_tab() + " #formUploadSGO")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('trans/sgo/import/save'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
	},

	getUpdateChargeBank : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url: All.get_url("trans/bank/get/id/" +param) ,
			type: 'GET',
			dataType: 'json',
			success:
				function(data){
					All.set_enable_button();
					if(data.response == "true") {
						//All.clear_div_in_boxcontent(".mainForm > .result");
						//$(All.get_box_content() + ".mainForm > #formInputNews").show();
						All.formUpdateActivate();
						$(All.get_active_tab() + " #id").val(data.arrayData[0].id);
						$(All.get_active_tab() + " #bankCode").val(data.arrayData[0].bankCode);
						$(All.get_active_tab() + " #bankDesc").val(data.arrayData[0].bankDesc);
						$(All.get_active_tab() + " #bankDisplayNm").val(data.arrayData[0].bankDisplayNm);
						$(All.get_active_tab() + " #charge_connectivity").val(data.arrayData[0].charge_connectivity);
						$(All.get_active_tab() + " #charge_admin").val(data.arrayData[0].charge_admin);
						$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
						$(All.get_active_tab() + " #status_be").val(data.arrayData[0].status_be);
					} else {
						alert("Data not found, please click View button to make sure data is exist..");
					}
				},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},



	//====================START HILAL BACKEND REPORT VOUCHER===============================
	getUpdConote : function(){
		//var orderno = param;
		//alert(orderno);

		All.set_disable_button();
		//$(All.get_active_tab() + " .hasil").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("print/shipping/updcnote"), $(All.get_active_tab() + " #formUpdateConote").serialize(), function(hasil) {
			//All.clear_div_in_boxcontent(".formUpdateConote .hasil");
			All.set_enable_button();
			$(All.get_active_tab() + " .hasil").html(null);
			//var rowshtml = hasil.message;
			var rowshtml = hasil.conoteno;
			$(All.get_active_tab() + " .hasil").append(rowshtml);
			//$(All.get_box_content() + ".formUpdateConote .hasil").append(rowshtml);
		}, "json");
	},

	getListVcr : function() {
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("trans/searchVcr/list"), $(All.get_active_tab() + " #formVcrSearch").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);
		});
	},

	getListVcrSK : function() {
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("wh/releasevcr/act"), $(All.get_active_tab() + " #formVcrRelease").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);

			/*if (hasil.response == "false" || hasil.response == null) {
			 All.set_error_message(" .result", "No record found..!!");
			 } else {*/

			//}
		});
	},

	getVcrSKcheck : function() {
		All.set_disable_button();
		$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("wh/releasevcr/act"), $(All.get_active_tab() + " #formVcrRelease").serialize(), function(hasil) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").html(hasil);

			/*if (hasil.response == "false" || hasil.response == null) {
			 All.set_error_message(" .result", "No record found..!!");
			 } else {*/

			//}
		});
	},

	getVcrSKcheckByVcno : function() {
		All.set_disable_button();
		var vstart = $(All.get_active_tab() + " #start").val();
		var vend = $(All.get_active_tab() + " #end").val();
		var trcd = $(All.get_active_tab() + " #trcd").val();
		var prdcd = $(All.get_active_tab() + " #prdcd").val();

		var totVcr = $(All.get_active_tab() + " #qtyord2").val();
		var totQty = $(All.get_active_tab() + " #qtyord").val();
		var totQtyAll = $(All.get_active_tab() + " #qty").val();

		//var rowVcr = $(All.get_active_tab() + '#tblvchno td').children('tr').length;

		var totalRowCount = 0;
		var rowCount = 0;
		var table = document.getElementById("tblvchno");
		var rows = table.getElementsByTagName("tr")
		for (var i = 0; i < rows.length; i++) {
			totalRowCount++;
			if (rows[i].getElementsByTagName("td").length > 0) {
				rowCount++;
			}
		}
		var sisavcr = 0;
		sisavcr = totQtyAll - rowCount;
		//var message = "Total Row Count: " + totalRowCount;

		//alert(message);

		//$(All.get_active_tab() + '#tblvchno td').closest('tr').length;
		//var totVcr = $(All.get_active_tab() + "#tblvchno tr").length;
		//var rowVcr = $(All.get_active_tab() + "#tblvchno td").closest("tr").length

		//alert('total table row = ' +rowVcr+ ', total req released = ' +totVcr);

		//if(rowCount < totQty || rowCount == 0){
		if(totQty > 0  || rowCount == 0){
			var nox = 1;
			//alert('totQty = ' +totQty+ ' \ntotQtyAll = ' +totQtyAll+ ' \nsisavcr = ' +sisavcr);
			if(totQty <= totQtyAll && sisavcr > 0){
				if(rowCount == 0){
					rowCount = 1;
				} else {
					rowCount++;
				}

				for (var i=vstart; i <= vend; i++) {
					nox++;
					var prdcd = $(All.get_active_tab() + " #prdcd").val();

					var urlx = "wh/releasevcr/upd/" +i+'/' +trcd+ '/' +prdcd;
					$.ajax({
						url : All.get_url(urlx),
						dataType : 'json',
						type : 'GET',
						success : function(data) {
							if (data.response == "true") {
								$(All.get_active_tab() + " #tblvchno").append('<tr>' +
									'<td align=right>'+ rowCount++ +'</td>' +
									'<td align=left>'+ data.arrayData[0].formno +'</td>' +
									'<td align=left>'+ data.arrayData[0].updatenm +'</td>' +
									'<td align=left>'+ data.arrayData[0].updatedt +'</td>' +
									'<td align=left>&nbsp;</td>' +
									'<td align=left>&nbsp;</td>' +
									'</tr>');
							}
						}
					});
				};

				if(rowCount == 0){
					alert('No voucher released.');
				}

				var totalRowCount = 0;
				var rowCount = 0;
				var table = document.getElementById("tblvchno");
				var rows = table.getElementsByTagName("tr")
				for (var i = 0; i < rows.length; i++) {
					totalRowCount++;
					if (rows[i].getElementsByTagName("td").length > 0) {
						rowCount++;
					}
				}

				var sisavcr2 = 0;
				sisavcr2 = totQtyAll - rowCount;

				$(All.get_active_tab() + " #qtyord2").val(sisavcr2);
				$(All.get_active_tab() + " #qtyord").val(sisavcr2);
				//$(All.get_active_tab() + " #qtyord").val(totQtyAll - (totVcr + nox));
			}else{
				var bovcr = 0;
				//alert('totQty = ' +totVcr+ '\nrowCount = ' +rowCount);
				bovcr = totQtyAll - rowCount;
				//alert('totVcr = ' +totVcr);
				//alert('rowCount = ' +rowCount);
				//alert('rows = ' +rows);

				var message = "Now, You can only release  : " + (bovcr);
				alert(message);
				$(All.get_active_tab() + " #qtyord").val(bovcr);
			}
		}else{
			var bovcr = 0;
			//alert('totQty = ' +totVcr+ '\nrowCount = ' +rowCount);
			bovcr = totQtyAll - rowCount;
			//alert('totVcr = ' +totVcr);
			//alert('rowCount = ' +rowCount);
			//alert('rows = ' +rows);

			var message = "You can only release  : " + (bovcr);
			alert(message);
			$(All.get_active_tab() + " #qtyord").val(bovcr);
		}

		All.set_enable_button();
	},
	getDetailProduct : function(idx) {
		var id = idx.id;
		$.ajax({
			url : All.get_url('wh/releasevcr/det/') + id,
			type : 'GET',
			success : function(data) {
				if (data == "false") {
					alert("Can not found detail product..!");
				} else {
					$(All.get_active_tab() + " .mainForm").hide();
					All.clear_div_in_boxcontent(".nextForm1");
					$(All.get_active_tab() + " .nextForm1").html(data);
				}
			}
		});
	},


	//====================END HILAL BACKEND REPORT VOUCHER===============================




	//=========================TAMBAHAN ANANECH================================

	getGroupDO : function() {

		$(All.get_active_tab() + " .mainForm").hide();

		$.post(All.get_url("wh/do/group/"),$("#groupingDO").serialize(), function(hasil)
		{
			All.set_enable_button();
			$(All.get_active_tab() + " .nextForm1").html(null);
			$(All.get_active_tab() + " .nextForm1").html(hasil);

			/*}).fail(function() {
			 alert("Error requesting page");
			 $(All.get_active_tab() + " #groupDO").html(null);*/
		});
	},

	/*}
	 $(All.get_active_tab() + " #groupDO").html(null);
	 });
	 },*/

	generateDO : function(){

		$.post(All.get_url("wh/do/groupAct"),$("#groupPrd").serialize(), function(hasil)
		{
			All.set_enable_button();
			$(All.get_active_tab() + " .nextForm1").html(null);
			$(All.get_active_tab() + " .nextForm1").html(hasil);
		});
	},

	setUpdtEConot : function(orderno,type,token,cargo){
		//alert('TUST  ' + type + ' TEST2 ' +orderno);
		All.set_disable_button();
		$(All.get_active_tab() + " .mainForm").hide();

		$.ajax({
			url: All.get_url("trans/jne/updtEConotJne/")+orderno+"/"+type+"/"+token,
			type: 'GET',
			success:
				function(data){
					All.set_enable_button();
					$(All.get_active_tab() + " .nextForm1").html(null);
					$(All.get_active_tab() + " .nextForm1").html(data);

				}
		});

	},

	updateShipAddr : function(orderno, token) {
		All.set_disable_button();
		$(All.get_active_tab() + " .mainForm").hide();

		$.ajax({
			url: All.get_url("trans/shipaddr/ins/")+orderno+"/"+token,
			type: 'GET',
			dataType: 'json',
			success:
				function(data){
					All.set_enable_button();
					alert(data.message);
					if(data.response == "true") {
						All.back_to_form(' .nextForm1',' .mainForm');
						All.clear_div_in_boxcontent(".mainForm .result");
						All.ajaxFormPost('frmUpdt','trans/jne/getListNoConot');
					}

				}
		});
	},

	getDelChoice : function (nilai){
		//$(All.get_active_tab() + ' .receivername').attr('readonly', true);
		$(All.get_active_tab() + ' .shipto').attr('readonly', true);

		if(nilai == "1") {
			$(All.get_active_tab() + ' .shipto').attr('readonly', true);
		} else {
			$(All.get_active_tab() + ' .shipto').attr('readonly', false);
		}
	},

	add_new_row : function(){
		var amount =  parseInt($(All.get_active_tab() + " #amount").val());
		var tabidx = parseInt($(All.get_active_tab() + " #tabidx").val());
		var j = tabidx;
		var z = parseInt($(All.get_active_tab() + " #amt_record").val()) + 1;
		$(All.get_active_tab() + " #amt_record").val(z);

		var koma = ",";

		var rowshtml = "<tr id="+ amount +">";
		rowshtml +=  "<td><input onchange=be_trans.helper_show_prod_by_id("+ amount +") tabindex="+ j +" type=text class=span12 id=productcode"+ amount +" name=productcode[] /></td>";
		rowshtml +=  "<td><input readonly=yes type=text class=span12 id=productname"+ amount +"  name=productname[] /></td>";
		j++;
		rowshtml += "<td>";
		rowshtml += "<input onchange=be_trans.sales_qty_format("+ amount +") tabindex="+ j +" style=text-align:right type=text class=span12 id=qty"+ amount +" name=qty[] />";
		rowshtml += "<input style=text-align:right;  type=hidden class=span12 id=qty_real"+ amount +" name=qty_real[] /></td>";
		j++;
		rowshtml += "<td><input readonly=yes style=text-align:right; type=text class=kanan id=dp"+ amount +"  name=dp[] />";
		rowshtml += "<input style=text-align:right; type=hidden class=kanan id=dp_real"+ amount +" name=dp_real[] /></td>";
		rowshtml += "<td><input readonly=yes style=text-align:right; type=text class=kanan id=bv"+ amount +"  name=bv[] />";
		rowshtml += "<input style=text-align:right; type=hidden class=kanan id=bv_real"+ amount +" name=bv_real[] /></td>";
		rowshtml += "<td><input readonly=yes style=text-align:right; type=text class=kanan id=total_dp"+ amount +"  name=total_dp[] />";
		rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_dp_real"+ amount +"  name=total_dp_real[] />";
		rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_bv_real"+ amount +"  name=total_bv_real[] /></td>";
		rowshtml += "<td ><a class=tombol href=# id="+ amount +" onclick=be_trans.delete_row("+ amount +")><i class=ikon></i> </a>";

		rowshtml += "</td></tr>";
		var x = amount + 1;
		var y = tabidx + 2;

		$(All.get_active_tab() + " #amount").val(x);
		$(All.get_active_tab() + " #tabidx").val(y);


		$(All.get_active_tab() + " #dataPrd").append(rowshtml);
		$(All.get_active_tab() + " .kanan").removeClass().addClass("span12 text-right");
		$(All.get_active_tab() + " .tombol").removeClass().addClass("btn btn-mini btn-danger");
		$(All.get_active_tab() + " .ikon").removeClass().addClass("icon-trash icon-white");
		$(All.get_active_tab() + " #productcode" +amount).focus();

	},

	tabkeyhandler : function(e) {
		if(e.keyCode == 9) {

			e.preventDefault();
			document.getElementById("new_record").focus();
		}
	},

	helper_show_prod_by_id : function(frm){
		$.ajax({
			dataType: 'json',
			url: All.get_url("trans/helper_show_prod_by_id") ,
			type: 'POST',
			data: {productcode: $(All.get_active_tab() + " #productcode" +frm).val(), pricecode: $(All.get_active_tab() + " #pricecode").val()},
			success:
				function(data){
					if(data.response == 'true')
					{
						$(All.get_active_tab() + " #qty" +frm).focus();
						$(All.get_active_tab() + " #qty" +frm).val('1');

						//var arrayData = data.arrayData;
						$(All.get_active_tab() + " #productname" +frm).val(data.arrayData[0].prdnm);
						var dp = parseInt(data.arrayData[0].dp);
						var bv = parseInt(data.arrayData[0].bv);
						var qty = parseInt($(All.get_active_tab() + " #qty" +frm).val());
						if(isNaN(qty)) {
							qty = 1;
						}
						var tot_dp = dp * qty;
						var tot_BV = bv * qty;
						var tot_all_price = 0;
						var tot_all_bv = 0;

						$(All.get_active_tab() + " #qty" +frm).val('1');
						$(All.get_active_tab() + " #dp" +frm).val(All.num(dp));
						$(All.get_active_tab() + " #bv" +frm).val(All.num(bv));
						$(All.get_active_tab() + " #dp_real" +frm).val(parseInt(dp));
						$(All.get_active_tab() + " #bv_real" +frm).val(parseInt(bv));
						$(All.get_active_tab() + " #total_dp" +frm).val(All.num(tot_dp));
						$(All.get_active_tab() + " #total_dp_real" +frm).val(tot_dp);

						var jum_trx = parseInt($(All.get_active_tab() + " #amt_record").val());
						for(var i = 1; i <= jum_trx; i++)
						{
							var ss = $(All.get_active_tab() + " #qty" +i).val();
							if(ss === undefined)
							{
								tot_all_price = tot_all_price + 0;
								tot_all_bv = tot_all_bv + 0;
							}
							else
							{
								tot_all_price += parseInt($(All.get_active_tab() + " #total_dp_real" +i).val());
								tot_all_bv += parseInt($(All.get_active_tab() + " #total_bv_real" +i).val());
							}

						}
						$(All.get_active_tab() + " #total_all").val(All.num(tot_all_price));
						$(All.get_active_tab() + " #total_all_real").val(tot_all_price);
						$(All.get_active_tab() + " #total_all_bv").val(All.num(tot_all_bv));
						$(All.get_active_tab() + " #total_all_real_bv").val(tot_all_bv);

						//alert('total semua ' +tot_all_price);
					}
					else
					{
						alert('product does not exist, check if the product is a subscription type');
						$(All.get_active_tab() + " #productname" +frm).val(null);
						$(All.get_active_tab() + " #qty" +frm).val(null);
						$(All.get_active_tab() + " #qty_real" +frm).val(null);
						$(All.get_active_tab() + " #dp" +frm).val(null);
						$(All.get_active_tab() + " #dp_real" +frm).val(null);
						$(All.get_active_tab() + " #total_dp" +frm).val(null);
						$(All.get_active_tab() + " #total_dp_real" +frm).val(null);
						$(All.get_active_tab() + " #productcode" +frm).focus();
					}
				},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
			}
		});
	},

	sales_qty_format : function (frm){

		var qty = parseInt($(All.get_active_tab() + ' #qty' +frm).val());
		var nm = /^[0-9]+$/;
		if(qty == '0' || qty == '' )
		{
			$("#qty" +frm).focus();
			alert('qty tidak boleh kosong');

			$("#save").attr("disabled", "disabled");

		}
		else if(!qty.All.should_integer('qty','qty'))
		{
			$("#qty" +frm).focus();
			alert('qty tidak boleh negatif' +qty);

			$("#save").attr("disabled", "disabled");
		}
		else
		{
			var dp = parseInt($('#dp_real' +frm).val());

			var total = qty * dp;
			var shows = All.num(total);
			$('#total_dp_real' +frm).val(total);
			$('#total_dp' +frm).val(shows);

			$('#qty_real' +frm).val(qty);

			var tot_bv = parseInt(qty) * parseInt(bv);
			var show_tot_bv = All.num(tot_bv);
			$('#total_dp_real' +frm).val(tot_bv);
			$('#total_bv' +frm).val(show_tot_bv);

			var tot_all_price = 0;
			var jum_trx = parseInt($(All.get_active_tab() + " #amt_record").val());

			for(var i = 1; i <= jum_trx; i++)
			{
				var ss = $(All.get_active_tab() + " #qty" +i).val();
				if(ss === undefined)
				{
					tot_all_price = tot_all_price + 0;
				}
				else
				{
					tot_all_price += parseInt($(All.get_active_tab() + " #total_dp_real" +i).val());
				}

			}

		} /*else {
		 alert('Number should not negative or zero and can not contains character..!!');
		 $(All.get_active_tab() + "#qty" +frm).val(null);
		 $(All.get_active_tab() + "#qty" +frm).focus();
		 }  */
	},


	//=========================END TAMBAHAN ANANECH================================

	getListSP : function() {
		All.set_disable_button();
		All.get_image_load2();

		$.post(All.get_url('trans/report/product_sales/list'), $(All.get_active_tab() + " #formSP").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			if (data.response == "false") {
				All.set_error_message(" .result", "No record found..!!");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
				rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
				rowshtml += "<thead><tr><th colspan='8' bgcolor='#lightgrey'>List Transaction</th></tr>";
				rowshtml += "<tr bgcolor='#f4f4f4'>";
				rowshtml += "<th width='5%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' disabled /></th>";
				rowshtml += "<th width='10%'>No</th>";
				rowshtml += "<th width='20%'>Kode Produk</th>";
				rowshtml += "<th width='50%'>Nama Produk</th>";
				rowshtml += "<th width='10%'>Qty</th>";
				rowshtml += "</tr></thead>";
				rowshtml += "<tbody>";

				$.each(arrayData, function(key, value) {
					var key= key * 1;
					rowshtml += "<tr><td align=center><input type=checkbox disabled  id=pil" + (key + 1) + " name=cnno[] value=" + value.no_code + "/></td>";
					rowshtml += "<td align=center>" + (key + 1) + "&nbsp;</td>";
					rowshtml += "<td align=center>" + value.no_code + "</td>";
					rowshtml += "<td align=center>" + value.name_code + "</td>";
					rowshtml += "<td align=center>" + All.num(parseInt(value.tot_qty)) + "</td></tr>";

				});

				rowshtml += "</tbody>";

				rowshtml += "</table>";
				rowshtml += "<input type='submit' class='btn btn-small btn-success' value='Export XLS' />";
				$(All.get_box_content() + ".mainForm .result").append(rowshtml);
				All.set_datatable();
			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	setRebookEConotG : function(orderno,type,token,cargo){
		//alert('TUST  ' + type + ' TEST2 ' +orderno);
		All.set_disable_button();
		$(All.get_active_tab() + " .mainForm").hide();

		$.ajax({
			url: All.get_url("trans/gsend/RebookEConotG/")+orderno+"/"+type+"/"+token,
			type: 'GET',
			success:
				function(data){
					All.set_enable_button();
					$(All.get_active_tab() + " .nextForm1").html(null);
					$(All.get_active_tab() + " .nextForm1").html(data);

				}
		});

	},

	getListTrxRebook : function() {
		All.set_disable_button();
		All.clear_div_in_boxcontent(".mainForm .result");
		All.get_image_load2();
		$.post(All.get_url('trans/report/list'), $(All.get_active_tab() + " #formTrxReport").serialize(), function(data) {
			All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			//console.log("ISI : " +data);
			if (data.response == "false") {
				All.set_error_message(" .mainForm .result");
			} else {
				var arrayData = data.arrayData;
				var rowshtml = "";
				var trx_type = $(All.get_active_tab() + " #trx_type").val();
				if(trx_type == "TDKLENGKAP") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					rowshtml += "<th width='12%'>No Token</th>";
					rowshtml += "<th>Total Pay</th>";
					rowshtml += "<th>Total BV</th>";
					rowshtml += "<th>Pembayaran</th>";
					rowshtml += "<th>No HP Konfirmasi</th>";
					rowshtml += "<th>Pemilik No HP Konf</th>";
					rowshtml += "<th>Trx Date</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {
						var urxc = 'trans/id2/'+value.token;
						rowshtml += "<tr>";
						//rowshtml += "<td>" + value.orderno + "</td>";
						//rowshtml += "<td align=center><a href=# id='" + value.orderno + "' onclick=javascript:be_trans.getDetailTrx(this)>" + value.orderno + "</a></td>";
						//rowshtml += "<td align=center><a href=# id='" + value.token + "' onclick=javascript:be_voucher.getDetailTrx2(this)>" + value.token + "</a></td>";
						rowshtml += "<td align=center><a href=# onclick=javascript:All.ajaxShowDetailonNextForm('"+urxc+"')>" + value.token + "</a></td>";
						//rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=center>" + value.total_pay + "</td>";
						rowshtml += "<td align=right>" + value.total_bv + "</td>";
						rowshtml += "<td align=right>" + value.bankDisplayNm + "</td>";
						rowshtml += "<td align=right>" + value.no_hp_konfirmasi + "</td>";
						rowshtml += "<td align=right>" + value.pemilik_hp_konfirmasi + "</td>";
						rowshtml += "<td align=center>" + value.datetrans + "</td>";
						rowshtml += "</tr>";

					});

					rowshtml += "</tbody>";
					rowshtml += "</table>";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				} else if(trx_type == "failRegMampirkak") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					rowshtml += "<th width='12%'>Order No</th>";
					rowshtml += "<th width='12%'>Token No</th>";
					rowshtml += "<th>ID Member</th>";
					rowshtml += "<th>Member Name</th>";
					rowshtml += "<th>No HP Konfirmasi</th>";
					rowshtml += "<th>Pemilik No HP Konf</th>";
					rowshtml += "<th>Trx Date</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {
						rowshtml += "<tr>";
						//rowshtml += "<td>" + value.orderno + "</td>";
						rowshtml += "<td align=center><a href=# id='" + value.orderno + "' onclick=javascript:be_trans.getDetailTrx(this)>" + value.orderno + "</a></td>";
						rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=center>" + value.id_memb + "</td>";
						rowshtml += "<td align=right>" + value.nmmember + "</td>";
						rowshtml += "<td align=right>" + value.no_hp_konfirmasi + "</td>";
						rowshtml += "<td align=right>" + value.pemilik_hp_konfirmasi + "</td>";
						rowshtml += "<td align=center>" + value.datetrans + "</td>";
						rowshtml += "</tr>";

					});

					rowshtml += "</tbody>";
					rowshtml += "</table>";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				}
				else if(trx_type == "double") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='7' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					rowshtml += "<th width='12%'>Order No</th>";
					//rowshtml += "<th width='12%'>CN No</th>";
					rowshtml += "<th width='12%'>Token No</th>";
					rowshtml += "<th>ID Member</th>";
					//rowshtml += "<th>Member Name</th>";
					rowshtml += "<th>DP</th>";
					rowshtml += "<th>BV</th>";
					rowshtml += "<th>Trx Date</th>";
					rowshtml += "<th>Sent To</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {
						rowshtml += "<tr>";
						rowshtml += "<td>" + value.orderno + "</td>";
						rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=center>" + value.id_memb + "</td>";
						//rowshtml += "<td align=right>" + value.nmmember + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td>";
						rowshtml += "<td align=center>" + value.datetrans + "</td>";
						rowshtml += "<td align=center>" + value.sent + "</td>";
						rowshtml += "</tr>";

					});

					rowshtml += "</tbody>";
					rowshtml += "</table>";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				}
				else if(trx_type == "PRD") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='4' bgcolor='#lightgrey'>Product Summary</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					rowshtml += "<th width='10%'>No</th>";
					rowshtml += "<th width='20%'>Product Code</th>";
					rowshtml += "<th>Product Name</th>";
					rowshtml += "<th width='15%'>Qty</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totQty = 0;
					var qtyx;
					var qtyS = 0;
					$.each(arrayData, function(key, value) {
						qtyx = parseInt(value.total_qty);
						rowshtml += "<tr><td align=center>" + (key+1) + "</td>";
						rowshtml += "<td align=center>" + value.prdcd + "</td>";
						rowshtml += "<td align=left>" + value.prdnm + "</td>";
						rowshtml += "<td align=right>" + All.num(qtyx) + "</td></tr>";
						qtyS += qtyx;
					});
					rowshtml += "</tbody>";
					rowshtml += "<tr><td align=center colspan=3><b>TOTAL QTY</b></td><td align=right>"+qtyS+"</td></tr>";
					rowshtml += "</table>";

					//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					//All.set_datatable();
				} else if(trx_type == "TOP") {
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='5' bgcolor='#lightgrey'>Top Up Virtual Account</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					//rowshtml += "<th width='10%'>No</th>";
					rowshtml += "<th width='20%'>Trx ID</th>";
					rowshtml += "<th>ID Member</th>";
					rowshtml += "<th width='15%'>Date</th>";
					rowshtml += "<th width='18%'>Amount</th>";
					rowshtml += "<th width='15%'>Bank Desc</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totQty = 0;
					var qtyx;
					var qtyS = 0;
					$.each(arrayData, function(key, value) {
						qtyx = parseInt(value.total_qty);
						//rowshtml += "<tr><td align=center>" + (key+1) + "</td>";
						rowshtml += "<td align=center>" + value.trcd + "</td>";
						rowshtml += "<td align=left>" + value.dfno + " / " + value.fullnm +"</td>";
						rowshtml += "<td align=center>" + value.createdt + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.amount)) + "</td>";
						rowshtml += "<td align=center>" + value.bankDesc + "</td></tr>";
						qtyS += parseInt(value.amount);
					});
					rowshtml += "</tbody>";
					//rowshtml += "<tr><td align=center colspan=3><b>TOTAL QTY</b></td><td align=right>"+All.num(qtyS)+"</td><td>&nbsp;</td></tr>";
					rowshtml += "</table>";

					//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				} else {
					//rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
					rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
					rowshtml += "<thead><tr><th colspan='12' bgcolor='#lightgrey'>List Transaction</th></tr>";
					rowshtml += "<tr bgcolor='#f5f4f4'>";
					//rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
					//rowshtml += "<th width='6%'>No</th>";
					rowshtml += "<th width='12%'>Order No</th>";
					rowshtml += "<th width='12%'>CN No</th>";
					rowshtml += "<th width='12%'>Token No</th>";
					rowshtml += "<th>Member Name</th>";
					rowshtml += "<th width='9%'>Stockist</th>";
					rowshtml += "<th width='9%'>Trx Date</th>";
					rowshtml += "<th width='4%'>Qty</th>";
					rowshtml += "<th width='8%'>DP</th>";
					rowshtml += "<th width='5%'>BV</th>";
					rowshtml += "<th width='4%'>CN Stat</th>";
					rowshtml += "<th width='4%'>BV Stat</th>";
					rowshtml += "<th width='4%'>Period</th>";
					rowshtml += "</tr></thead>";
					rowshtml += "<tbody>";
					var totDP = 0;
					var totBV = 0;
					var CNStat = '';
					var BVStat = '';
					$.each(arrayData, function(key, value) {

						//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
						//rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
						if(value.sumofPrd == null || value.sumofPrd == "" || value.sumofPay == null || value.sumofPay == "") {
							var urxc = 'trans/reconcile/id/' + value.orderno;
							rowshtml += "<tr bgcolor='orange'><td align=center><a class='btn btn-mini btn-warning' href=# id='" + value.orderno + "' onclick=javascript:All.ajaxShowDetailonNextForm('"+urxc+"')>" + value.orderno + "</a></td>";
						} else {
							rowshtml += "<tr><td align=center><a href=# id='" + value.orderno + "' onclick=javascript:be_trans.getDetailTrx(this)>" + value.orderno + "</a></td>";
						}
						rowshtml += "<td align=center>" + value.CNno + "</td>";
						var notoken =  value.token;
						var kdk = notoken.substring(0, 2);

						if((kdk == "RM" || kdk == "RA" || kdk == "LP") && value.generate_idmemb === null) {
							rowshtml += "<td align=center><font color='red'><strong>MEMBER FAIL</strong></font></td>";
						} else {
							rowshtml += "<td align=center>" + value.token + "</td>";
						}
						rowshtml += "<td>" + value.nmmember + "</td>";
						rowshtml += "<td align=center>" + value.idstk + "</td>";
						rowshtml += "<td align=center>" + value.datetrans1 + "</td>";
						rowshtml += "<td align=right>" + value.sumofPrd + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_pay)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.total_bv)) + "</td>";

						if(value.CN_STAT == '1'){
							//CNStat = 'Settled';
							CNStat = '1';
						}else{
							//CNStat = 'Unsettled';
							CNStat = '0';
						}
						if(value.BV_STAT == '1'){
							//BVStat = 'Settled';
							BVStat = '1'
						}else{
							//BVStat = 'Unsettled';
							BVStat = '0'
						}
						rowshtml += "<td align=center>" + CNStat + "</td>";
						rowshtml += "<td align=center>" + BVStat + "</td>";
						rowshtml += "<td align=right>" + value.bonusmonth + "</td>";
						totDP = totDP + parseInt(value.total_pay);
						totBV = totBV + parseInt(value.total_bv);

					});

					rowshtml += "</tbody></tr>";

					rowshtml += "</table>";
					rowshtml += "<table class='table table-striped table-bordered' width='100%'>";
					rowshtml += "<th><td align=center>T O T A L</th><th align=right width='8%'>" + All.num(totDP) + "</th><th align=right width='6%'>" + All.num(totBV) + "</th></tr>";
					rowshtml += "</table>";
					//rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
					$(All.get_box_content() + ".mainForm .result").append(rowshtml);
					All.set_datatable();
				}

			}

		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

}

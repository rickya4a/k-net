var $ = jQuery;

var be_toto = {
	
    getPreviewInpToto : function (){
        All.set_disable_button();
        $.post(All.get_url("be_toto/preview") , $("#formToto").serialize(), function(hasil)
        {
            All.set_enable_button();
            //if(hasil == "Semua field harus diisi..!!" || hasil == "KTP atau Passport Sudah Terpakai") {
            //alert(hasil.message);
            All.clear_div_in_boxcontent(".mainForm .result");
            if(hasil.response == "false") {
                alert(hasil.message);
            } else if(hasil.response == "double") {
            	alert(hasil.message);
            	
            	var arrayData = hasil.arrayData;
            	var rowshtml = "";
		 	   //rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
		 	    rowshtml += "<table width='80%' align='center' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		        rowshtml += "<thead><tr><th colspan='2' bgcolor='#lightgrey'>List Input Produk</th></tr>";
		        
		        rowshtml += "<tr><td width='20%'>Kode Produk</td><td>"+arrayData[0].kodep+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>Jenis Produk</td><td>"+arrayData[0].jenisp+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>Type Produk</td><td>"+arrayData[0].typep+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>Warna Produk</td><td>"+arrayData[0].warnap+"</td></tr>";
			    $(All.get_box_content() + ".mainForm .result").append(rowshtml);
            } else {
                $(All.get_active_tab() + " .mainForm").hide();
                $(All.get_active_tab() + " .nextForm1").html(null);
                var data = hasil.arrayData[0];
                var rowhtml = "<form action='"+All.get_url("be_toto/print")+"' method='POST' target='_BLANK'><table class='table table-striped table-bordered' width=100%>";
                rowhtml += "<tr><td colspan=2 align=center><h3>Kode Produk : "+data.kodep+"</h3></td></tr>";
                rowhtml += "<tr><td width=25%>Jenis Produk</td><td>&nbsp;"+data.jenisp+"</td></tr>";
                rowhtml += "<tr><td>Type Produk</td><td>&nbsp;"+data.typep+"</td></tr>";
                rowhtml += "<tr><td>Warna </td><td>&nbsp;"+data.warnap+"</td></tr>";
                rowhtml += "<tr><td>";
                /*rowhtml += "<input type=hidden id='noreg' name='noreg' value='"+data.registerno+"' />";*/
                rowhtml += "<input type=button id='backToFRM' name='noreg' class='btn btn-small btn-warning' value='Input Baru' onclick='be_umroh.backToInputToto()' />&nbsp;&nbsp;";
                //rowhtml += "<input type=submit class='btn btn-small btn-success' value='Cetak' /></td></tr>";
                rowhtml += "</table></form>";
                $(All.get_active_tab() + " .nextForm1").append(rowhtml);
            }
        },"json");
    },
    
    backToInputToto : function() {
    	 $(All.get_active_tab() + " .nextForm1").html(null);
    	 $(All.get_active_tab() + " .mainForm").show();
    	 $(All.get_active_tab() + " input[type=text]").val(null);
    	 //$(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val('1');
    },
    
    getListRegUmroh : function() {
    	 All.set_disable_button();
         $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
         $.post(All.get_url("be_umroh/reg/list/get") , $(All.get_active_tab() + " #frmRegList").serialize(), function(data)
         {  
            All.set_enable_button();
            All.clear_div_in_boxcontent(".mainForm .result");
            if(data.response == "false") {
            	alert(data.message)
            } else {
            	var arrayData = data.arrayData;
				var rowshtml = "";
		 	   //rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
		 	    rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		        rowshtml += "<thead><tr><th colspan='6' bgcolor='#lightgrey'>List Registrasi Umroh</th></tr>";
		        rowshtml += "<tr bgcolor='#f4f4f4'>";
			    //rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		        rowshtml += "<th width='5%'>No</th>";
		        rowshtml += "<th width='10%'>Voucher No</th>";
		        rowshtml += "<th width='10%'>Reg No</th>";
		        rowshtml += "<th width='13%'>ID Member</th>";
		        rowshtml += "<th>Member Name</th>";
		        rowshtml += "<th width='10%'>Nominal</th>";
			    rowshtml += "<th width='10%'>CN No.</th>";
			     rowshtml += "<th width='10%'>Act</th>";
		        rowshtml += "</tr></thead>";
		        rowshtml += "<tbody>";
		        $.each(arrayData,function(key, value){
		                     //rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
                             rowshtml += "<td align=right>"+(key+1)+"&nbsp;</td>";
                             rowshtml += "<td align=center>"+value.voucherno+"</td>";
                             rowshtml += "<td align=center>"+value.registerno+"</td>";
                             rowshtml += "<td>"+value.dfno+"</td>";
                             rowshtml += "<td>"+value.fullnm+"</td>";
                             rowshtml += "<td align=right>"+All.num(parseInt(value.tot_fund))+"</td>";
                             rowshtml += "<td align=right>"+value.CNno+"</td>";
                             if(value.voucherno != null) {
	                             var sd = All.get_url('be_umroh/print/notaUmroh/')
	                             sd += value.registerno;
	                             rowshtml += "<td align=right><a class='btn btn-success' href='"+sd+"' target='_blank'>Print Nota</a></td></tr>";
                             } else {
                             	 rowshtml += "<td align=right>&nbsp;</td></tr>";
                             }
		                });
		        
		        rowshtml += "</tbody>";
				
			    rowshtml += "</table>";
			    //rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
			    $(All.get_box_content() + ".mainForm .result").append(rowshtml);
			    All.set_datatable();
            }
         },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
    },
    
   getListPayUmroh : function() {
        All.set_disable_button();
            $(All.get_active_tab() + " #listingPayUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/listPayment/app") , $(All.get_active_tab() + " #frmApp").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listingPayUmroh").html(null);
                $(All.get_active_tab() + " #listingPayUmroh").html(hasil);   
            });
    },
    
    backListPayUmroh : function(){
        $(All.get_active_tab() + " #frmApproval").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
        be_umroh.getListPayUmroh();
    },
    
    approvePayment : function(regno,from,to,tipe_perjalanan){
        //console.log("test "+regno);
        All.set_disable_button();
        $(All.get_active_tab() + " #frmApproval").hide();
        //$(All.get_active_tab() + " #listingPayUmroh").hide();
         $.ajax({
            url: All.get_url("be_umroh/listPayment/appRes/" +regno+"/" +from+"/" +to+"/" +tipe_perjalanan),
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
                $(All.get_active_tab() + " .nextForm1").html(null);
                $(All.get_active_tab() + " .nextForm1").html(data);
                
            }
        });
    },
    
    getVoucherInfo : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #dtVchUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/reprint/voucher/act") , $(All.get_active_tab() + " #frmVchPending").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #dtVchUmroh").html(null);
                $(All.get_active_tab() + " #dtVchUmroh").html(hasil);   
            });
    },
    
    resendSms : function(regno){
        //console.log("test "+regno);
        All.set_disable_button();
        $(All.get_active_tab() + " #formDtUmrohh").hide();
        //$(All.get_active_tab() + " #listingPayUmroh").hide();
         $.ajax({
            url: All.get_url("be_umroh/resendSms/act/" +regno),
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
                $(All.get_active_tab() + " .nextForm1").html(null);
                $(All.get_active_tab() + " .nextForm1").html(data);
                
            }
        });
    },
    
    back_to_list : function(){
        $(All.get_active_tab() + " #formDtUmrohh").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
    },
    
    get_registerno_info : function(regnos){
        //console.log("masuk kemari");
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            $.ajax({
             dataType: 'json',   
             type: "POST",
             url : All.get_url("be_umroh/helper/regnoInfo"),
             data : {regnos: regnos},
             success: function(data)
             {
                var arrayData = data.arrayData;
                if(data.response == "true") {
                    //console.log("masuk kemari " +arrayData[0].dfno);
                    $(All.get_active_tab() + " #idmember").val(arrayData[0].dfno);
                     $(All.get_active_tab() + " #nmmember").val(arrayData[0].fullnm);
                     $(All.get_active_tab() + " #pktUmroh").val(arrayData[0].departuredesc);
                     $(All.get_active_tab() + " #saldo").val(arrayData[0].saldo);
                     $(All.get_active_tab() + " #amtUmroh").focus();
                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
                } else {
                    $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
                     alert('RegisterNo Salah');
                     $(All.get_active_tab() + " #idmember").attr('readonly', true);
                     $(All.get_active_tab() + " #nmmember").attr('readonly', true);
                     $(All.get_active_tab() + " #pktUmroh").attr('readonly', true);
                     $(All.get_active_tab() + " #saldo").attr('readonly', true);
                    
                     $(All.get_active_tab() + " #regnos").focus();
                }
            } 
        });
    },
    
    saveInstallment : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/save/installTrf") , $(All.get_active_tab() + " #formCicilan").serialize(), function(hasil)
            {  
                
                All.set_enable_button();
                $(All.get_active_tab() + " input[type=text]").val(null);
                $(All.get_active_tab() + " #result").html(null);
                $(All.get_active_tab() + " #result").html("<div class='alert alert-error' align=center>"+hasil.message+"</div>");   
                //All.set_error_message(' #result',hasil.message);
            },"json");
    },
    
    getListInstall : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #listInstallUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/report/installment/act") , $(All.get_active_tab() + " #frmReportInstall").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listInstallUmroh").html(null);
                $(All.get_active_tab() + " #listInstallUmroh").html(hasil);   
            });
    },
	
	getListPost : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " .result").html(null);
        $(All.get_active_tab() + " .result").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
        $(All.get_active_tab() + " .nextForm1").hide();
        $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.post(All.get_url("be_umroh/post/act") , $(All.get_active_tab() + " #frmReportPost").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        });
    },
     
    getUpdatePostPrev : function(){
        All.set_disable_button();
        //$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		var atLeastOneIsChecked = $(All.get_active_tab() + ' input[name="registerno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			All.set_enable_button();
			alert("Please select at least one transaction..");
			return false;
		} 
		
        $(All.get_active_tab() + " .result").hide();
        $(All.get_active_tab() + " .nextForm1").show();
        $.post(All.get_url("be_umroh/post/prev") , $(All.get_active_tab() + " #frmReportPost").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .nextForm1").html(null);
            $(All.get_active_tab() + " .nextForm1").html(hasil);   
        });
    },
       
    getUpdatePost : function(){
        All.set_disable_button();
		$(All.get_active_tab() + " .nextForm1").hide();
		$.post(All.get_url("be_umroh/post/upd"), $(All.get_active_tab() + " #frmReportPostPrev").serialize(), function(hasil) {
			be_umroh.getListPost();
			//alert('masuk');
			//All.clear_div_in_boxcontent(".formUpdateConote .hasil");
			/*
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").append(rowshtml);*/
			//$(All.get_box_content() + ".formUpdateConote .hasil").append(rowshtml);
		});
    },
    
	getListMutasi : function(){
		All.set_disable_button();
            $(All.get_active_tab() + " #listMutasiUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/mutasi/action") , $(All.get_active_tab() + " #frmMut").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listMutasiUmroh").html(null);
                $(All.get_active_tab() + " #listMutasiUmroh").html(hasil);   
            });
	},
	
	getListReg : function() {
		All.set_disable_button();
        $(All.get_active_tab() + " #listInstallUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/report/installment/act") , $(All.get_active_tab() + " #frmReportInstall").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listInstallUmroh").html(null);
                $(All.get_active_tab() + " #listInstallUmroh").html(hasil);   
            });
	},
	
	getListUmrToWeb  : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #listUmrohToWeb").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/listUpload/journey/act") , $(All.get_active_tab() + " #frmReportToWeb").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listUmrohToWeb").html(null);
                $(All.get_active_tab() + " #listUmrohToWeb").html(hasil);   
            });
    },
	
	
	saveUploadMT  : function() {
	   var formData = new FormData($(All.get_active_tab() + " #formUploadMT940")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('be_umroh/save_upload'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
	},
	
    
}

jQuery(document).ready(function($){
    if($(All.get_active_tab() + " #formUmroh").length) {
        var thnlhrOri = $('<select class="hide"></select>');
        var thnlhrOpt = $(All.get_active_tab() + " thnlhr option").clone();
        thnlhrOri.append(thnlhrOpt).insertAfter(All.get_active_tab() + ' #thnlhr');
        console.log("dokumen load..");
        be_umroh.getTipeJamaah();
        $(All.get_active_tab() + ' #email').blur(function(){
            var strValid = be_umroh.isValidEmailAddress($(All.get_active_tab() + ' #email').val());
            if(strValid === false) {
                alert('Alamat email salah');
                $(All.get_active_tab() + ' #email').focus();
            }
        });
		
    }
});
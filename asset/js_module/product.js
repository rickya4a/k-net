var $ = jQuery;

var Product = {
	convertMoney : function(idx, set) {
		var x = idx.value;
		var formatValue = All.num(x);
		$(All.get_active_tab() + " #" +set).val(x);
		$(idx).val(formatValue);
	},
	
	getUpdateProductCat : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #cat_id" +param).val();
		$.ajax({
            url: All.get_url("product/cat/list/cat_id/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					//All.clear_div_in_boxcontent(".mainForm > .result");
  					//$(All.get_box_content() + ".mainForm > #formInputNews").show();
					All.formUpdateActivate();
					$(All.get_active_tab() + " #cat_id").val(data.arrayData[0].cat_id);
					$(All.get_active_tab() + " #cat_desc").val(data.arrayData[0].cat_desc);
					$(All.get_active_tab() + " #remarks").val(data.arrayData[0].remarks);
					$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
					
					//$(All.get_active_tab() + " #parentID").val(data.arraydata[0].orderlist);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	getUpdatePriceCode : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #pricecode" +param).val();
		$.ajax({
            url: All.get_url("pricecode/list/pricecode/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					//All.clear_div_in_boxcontent(".mainForm > .result");
  					//$(All.get_box_content() + ".mainForm > #formInputNews").show();
					All.formUpdateActivate();
					$(All.get_active_tab() + " #pricecode").val(data.arrayData[0].pricecode);
					$(All.get_active_tab() + " #pricecode_desc").val(data.arrayData[0].pricecode_desc);
					$(All.get_active_tab() + " #country_id").val(data.arrayData[0].country_id);
					$(All.get_active_tab() + " #hq_id").val(data.arrayData[0].hq_id);
					$(All.get_active_tab() + " #branch_id").val(data.arrayData[0].branch_id);
					$(All.get_active_tab() + " #remarks").val(data.arrayData[0].remarks);
					$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
					
					//$(All.get_active_tab() + " #parentID").val(data.arraydata[0].orderlist);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	refreshListProductCat : function(set_to) {
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
            url: All.get_url("product/cat/list/json"),
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                if(data.response == "true") {
					var arrayData = data.arrayData;
					$(All.get_active_tab() + set_to).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.cat_id+">"+value.cat_desc+"</option>";
	                });
                	$(All.get_active_tab() + set_to).append(rowhtml);
				} else {
					
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	refreshListProduct : function(set_to) {
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
            url: All.get_url("product/list/json"),
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                if(data.response == "true") {
					var arrayData = data.arrayData;
					$(All.get_active_tab() + set_to).html(null);
	                var rowhtml = "<option value=''>--Select here--</option>";
	                $.each(arrayData, function(key, value) {
	                    
	                    rowhtml += "<option  value="+value.cat_inv_id+">"+value.cat_inv_desc+"</option>";
	                });
                	$(All.get_active_tab() + set_to).append(rowhtml);
				} else {
					
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	saveInputProduct  : function(url, formid) {
		
		/*All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
        {  
            All.set_enable_button();
			if(data.response == "false") {
                All.set_error_message(".mainForm > .result", data.message);
            } 
            else {
                All.set_success_message(".mainForm > .result", data.message);
				All.reset_all_input();
            } 
            Product.refreshListProductCat(' #cat_id');
		    Product.refreshListProduct(' #parent_cat_inv_id');
            
        }, "json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  */
       
		All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);
    
        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {
                
                if(data.response == "false") {
                	All.set_error_message(".mainForm > .result", data.message);
	            } 
	            else {
	                All.set_success_message(".mainForm > .result", data.message);
					All.reset_all_input();
	            } 
	            
	            Product.refreshListProductCat(' #cat_id');
		    	Product.refreshListProduct(' #parent_cat_inv_id');
            },
            cache: false,
            contentType: false,
            processData: false
        });
	},
	saveInputBanner  : function(url, formid) {

		/*All.set_disable_button();
		 All.get_wait_message();
		 $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
		 {
		 All.set_enable_button();
		 if(data.response == "false") {
		 All.set_error_message(".mainForm > .result", data.message);
		 }
		 else {
		 All.set_success_message(".mainForm > .result", data.message);
		 All.reset_all_input();
		 }
		 Product.refreshListProductCat(' #cat_id');
		 Product.refreshListProduct(' #parent_cat_inv_id');

		 }, "json").fail(function() {
		 alert("Error requesting page");
		 All.set_enable_button();
		 });  */

		All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);

		$.ajax({
			url: All.get_url(url),
			type: 'POST',
			data: formData,
			dataType: 'json',
			async: false,
			success: function (data) {

				if(data.response == "false") {
					All.set_error_message(".mainForm > .result", data.message);
				}
				else {
					All.set_success_message(".mainForm > .result", data.message);
					All.reset_all_input();
				}

				Product.refreshListProductCat(' #cat_id');
				Product.refreshListProduct(' #parent_cat_inv_id');
			},
			cache: false,
			contentType: false,
			processData: false
		});
	},
	saveInputEtiket  : function(url, formid) {

		All.set_disable_button();
		All.get_wait_message();
		var kosong = 0;
		$('form#formInputProduct').find('input').each(function(){
		    if($(this).prop('required')) {
		        if($(this).val() == "" || $(this).val() === " ") {
		        	kosong++;
		        }
		    } 
		});
		
		if(kosong > 0) {
			alert("masih ada yg kosong");
			All.set_enable_button();

		}
		else{
			var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);

			$.ajax({
				url: All.get_url(url),
				type: 'POST',
				data: formData,
				dataType: 'json',
				async: false,
				success: function (data) {
					if(data.response == "false") {
						All.set_error_message(".mainForm > .result", data.message);
						All.set_enable_button();


					}
					else {
						All.set_success_message(".mainForm > .result", data.message);
						All.reset_all_input();
						All.set_enable_button();

					}
					All.set_enable_button();

				},
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            },
				cache: false,
				contentType: false,
				processData: false
			});

		}
		/*
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);

		$.ajax({
			url: All.get_url(url),
			type: 'POST',
			data: formData,
			dataType: 'json',
			async: false,
			success: function (data) {
				if(data.response == "false") {
					All.set_error_message(".mainForm > .result", data.message);
					All.set_enable_button();


				}
				else {
					All.set_success_message(".mainForm > .result", data.message);
					All.reset_all_input();
					All.set_enable_button();

				}
				All.set_enable_button();

			},
			cache: false,
			contentType: false,
			processData: false
		}); */
	},

	getUpdateEtiket : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		
		
		//var id = $(All.get_active_tab() + " #cat_inv_id" +param).val();
		$.ajax({
			url: All.get_url("etiket/getEditTiket/" +param) ,
			type: 'GET',
			dataType: 'json',
			success:
				function(data){
					All.set_enable_button();
					if(data.response == "true") {
						//All.clear_div_in_boxcontent(".mainForm > .result");
						//$(All.get_box_content() + ".mainForm > #formInputNews").show();
						All.formUpdateActivate();
						$(All.get_active_tab() + " .fileExistingInfo").html(null);
						$(All.get_active_tab() + " .fileHiddenExistingInfo").val(null);
						$(All.get_active_tab() + " #nm_event").val(data.arrayData[0].nama);
						$(All.get_active_tab() + " #loc_event").val(data.arrayData[0].lokasi);
						$(All.get_active_tab() + " #max_online").val(data.arrayData[0].max_online);
						$(All.get_active_tab() + " #act_online").val(data.arrayData[0].act_online);
						$(All.get_active_tab() + " #max_offline").val(data.arrayData[0].max_offline);
						$(All.get_active_tab() + " #act_offline").val(data.arrayData[0].act_offline);
						$(All.get_active_tab() + " #total").val(data.arrayData[0].total);
						$(All.get_active_tab() + " #act_total").val(data.arrayData[0].act_total);
						$(All.get_active_tab() + " #event_date").val(data.arrayData[0].event_date);
						$(All.get_active_tab() + " #pembicara").val(data.arrayData[0].pembicara);
						$(All.get_active_tab() + " #exp_date_online").val(data.arrayData[0].exp_date_online);
						$(All.get_active_tab() + " #exp_date_offline").val(data.arrayData[0].exp_date_offline);
						$(All.get_active_tab() + " #price_online").val(data.arrayData[0].price_online);
						$(All.get_active_tab() + " #price_offline").val(data.arrayData[0].price_offline);
						$(All.get_active_tab() + " #remark").val(data.arrayData[0].remark);
						$(All.get_active_tab() + " #id").val(data.arrayData[0].id);
						$(All.get_active_tab() + " #bird_date").val(data.arrayData[0].earlybird_date);
						$(All.get_active_tab() + " #price_bird").val(data.arrayData[0].price_earlybird_online);
						$(All.get_active_tab() + " #pil_event").val(data.arrayData[0].pilevent);
						$(All.get_active_tab() + " #event_cat_id").val(data.arrayData[0].cat_id);
						$(All.get_active_tab() + " #pil_loc").val(data.arrayData[0].pilloc);
						$(All.get_active_tab() + " #loc_id").val(data.arrayData[0].kode_lokasi);
						$(All.get_active_tab() + " #etiket_code_id").val(data.arrayData[0].etiket_code_id);
						
					}
				},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},
	getKlaimEtiket : function(param) {
		All.set_disable_button();
		All.get_wait_message();


		//var id = $(All.get_active_tab() + " #cat_inv_id" +param).val();
		$.ajax({
			url: All.get_url("etiket/klaimEtiket/" +param) ,
			type: 'GET',
			dataType: 'json',
			success:
				function(data){
					All.set_enable_button();
					if(data.response == "true") {
						//All.clear_div_in_boxcontent(".mainForm > .result");
						//$(All.get_box_content() + ".mainForm > #formInputNews").show();
						All.formUpdateActivate();
						$(All.get_active_tab() + " .fileExistingInfo").html(null);
						$(All.get_active_tab() + " .fileHiddenExistingInfo").val(null);
						$(All.get_active_tab() + " #id").val(data.arrayData[0].id);
						$(All.get_active_tab() + " #dfno").val(data.arrayData[0].dfno);
						$(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
						$(All.get_active_tab() + " #idmbr2").val(data.arrayData[0].valid_dfno);
						$(All.get_active_tab() + " #display_info2").val(data.arrayData[0].valid_fullnm);
						$(All.get_active_tab() + " #email").val(data.arrayData[0].email);
						$(All.get_active_tab() + " #notelp").val(data.arrayData[0].notelp);
                        $(All.get_active_tab() + " #remark").val(data.arrayData[0].remark);

					}
				},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},
	getUpdateProduct : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #cat_inv_id" +param).val();
		$.ajax({
            url: All.get_url("product/list/cat_inv_id/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					//All.clear_div_in_boxcontent(".mainForm > .result");
  					//$(All.get_box_content() + ".mainForm > #formInputNews").show();
					All.formUpdateActivate();
					$(All.get_active_tab() + " .fileExistingInfo").html(null);
	  				$(All.get_active_tab() + " .fileHiddenExistingInfo").val(null);
					$(All.get_active_tab() + " #cat_inv_id").val(data.arrayData[0].cat_inv_id);
					$(All.get_active_tab() + " #cat_inv_desc").val(data.arrayData[0].cat_inv_desc);
					$(All.get_active_tab() + " #country_id").val(data.arrayData[0].country_id);
					$(All.get_active_tab() + " #hq_id").val(data.arrayData[0].hq_id);
					$(All.get_active_tab() + " #branch_id").val(data.arrayData[0].branch_id);
					$(All.get_active_tab() + " #cat_id").val(data.arrayData[0].cat_id);
					$(All.get_active_tab() + " #parent_cat_inv_id").val(data.arrayData[0].parent_cat_inv_id);
					$(All.get_active_tab() + " #inv_type").val(data.arrayData[0].inv_type);
					$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
					$(All.get_active_tab() + " #bo_inv_status").val(data.arrayData[0].bo_inv_status);
					$(All.get_active_tab() + " #bo_status").val(data.arrayData[0].bo_status);
					$(All.get_active_tab() + " #sc_status").val(data.arrayData[0].sc_status);
					$(All.get_active_tab() + " #sub_status").val(data.arrayData[0].sub_status);
					$(All.get_active_tab() + " #ms_status").val(data.arrayData[0].ms_status);
					$(All.get_active_tab() + " #ecomm_status").val(data.arrayData[0].ecomm_status);
					$(All.get_active_tab() + " #web_status").val(data.arrayData[0].web_status);
					$(All.get_active_tab() + " #weight").val(data.arrayData[0].weight);
					
					//tambahan dion 8/6/2016
					$(All.get_active_tab() + " #is_starterkit").val(data.arrayData[0].is_starterkit);
					$(All.get_active_tab() + " #is_discontinue").val(data.arrayData[0].is_discontinue);
					$(All.get_active_tab() + " #is_charge_ship").val(data.arrayData[0].is_charge_ship);
					$(All.get_active_tab() + " #flag_is_do").val(data.arrayData[0].flag_is_do);
					if(data.arrayData[0].img_name !== null) {						
						
		                    $(All.get_active_tab() + " #spanPic").append("Existing File Image : " +data.arrayData[0].img_name);
							$(All.get_active_tab() + " #filename").val(data.arrayData[0].img_name);
							//$(All.get_active_tab() + " #imageTitle" +(key + 1)).val(value.title);
		               
					}
					//$(All.get_active_tab() + " #parentID").val(data.arraydata[0].orderlist);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	getUpdateBanner : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #cat_inv_id" +param).val();
		$.ajax({
			url: All.get_url("bannerANP/list/id/" +id) ,
			type: 'GET',
			dataType: 'json',
			success:
				function(data){
					All.set_enable_button();
					if(data.response == "true") {
						//All.clear_div_in_boxcontent(".mainForm > .result");
						//$(All.get_box_content() + ".mainForm > #formInputNews").show();
						All.formUpdateActivate();
						$(All.get_active_tab() + " .fileExistingInfo").html(null);
						$(All.get_active_tab() + " .fileHiddenExistingInfo").val(null);
						$(All.get_active_tab() + " #ixd").val(data.arrayData[0].id);
						$(All.get_active_tab() + " #hdr_desc").val(data.arrayData[0].hdr_desc);
						$(All.get_active_tab() + " #hdr_status").val(data.arrayData[0].hdr_status);
						if(data.arrayData[0].img_name !== null) {

							$(All.get_active_tab() + " #spanPic").append("Existinsg File Image : " +data.arrayData[0].img_url);
							$(All.get_active_tab() + " #filename").val(data.arrayData[0].img_url);
							//$(All.get_active_tab() + " #imageTitle" +(key + 1)).val(value.title);

						}
						//$(All.get_active_tab() + " #parentID").val(data.arraydata[0].orderlist);
					}
				},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},
	getPriceCodeByProdID : function(nilai) {
		var country_id = nilai.country_id.value;
		var hq_id = nilai.hq_id.value;
		var branch_id = nilai.branch_id.value;	
		var cat_inv_id = nilai.cat_inv_id.value;
		var form_id = nilai.id;
		
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
            url: All.get_url("product/price/check"),
            type: 'POST',
            data: {country_id : country_id, hq_id : hq_id, branch_id : branch_id, cat_inv_id : cat_inv_id},
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                $(All.get_active_tab() + " #listPricePrd").html(null);
                
                var arrayData = data.arrayData;
                var rowhtml = "<label class='control-label' for='typeahead'>Product Price</label>";
			        rowhtml +=  "<div class='controls'>";
			        rowhtml += "<table width='100%' class='table table-striped table-bordered'>";
			        rowhtml += "<tr><th width=20%>Price Code</th><th>Description</th>";
			        rowhtml += "<th width=20%>Dist Price</th><th width=20%>Cust Price</th>";
			        rowhtml += "<th width=15%>BV</th></tr>";
                All.clear_div_in_boxcontent(".mainForm > .result");
                if(data.response == "true") {
                	$(All.get_active_tab() + " #cat_inv_desc").val(data.prdnm);
                	//for update
                	if(data.message == "1") {
	                	$.each(arrayData, function(key, value) {
		                    rowhtml += "<tr>";
		                    rowhtml += "<td><input type=text class='span20' id=pricecode"+(key+1)+" name=pricecode[] value="+value.pricecode+" readonly=readonly /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=pricecode_desc"+(key+1)+" name=pricecode_desc[] value='"+value.pricecode_desc+"' readonly=readonly /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=dp"+(key+1)+" name=dp[] value="+value.dp+" /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=cp"+(key+1)+" name=cp[] value="+value.cp+" /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=bv"+(key+1)+" name=bv[] value="+value.bv+" /></td>";
		                    rowhtml += "</tr>";
		                });
	                	rowhtml += "</table>";
	                	$(All.get_active_tab() + " #listPricePrd").append(rowhtml);
	                	$(All.get_active_tab() + " #inp_btn").css('display', 'none');
					    $(All.get_active_tab() + " #upd_btn").css('display', 'block');
	                } else {
	                	//for input
	                	$.each(arrayData, function(key, value) {
		                    rowhtml += "<tr>";
		                    rowhtml += "<td><input type=text class='span20' id=pricecode"+(key+1)+" name=pricecode[] value="+value.pricecode+" readonly=readonly /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=pricecode_desc"+(key+1)+" name=pricecode_desc[] value='"+value.pricecode_desc+"' readonly=readonly /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=dp"+(key+1)+" name=dp[] /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=cp"+(key+1)+" name=cp[] /></td>";
		                    rowhtml += "<td><input type=text class='span20' id=bv"+(key+1)+" name=bv[] /></td>";
		                    rowhtml += "</tr>";
		                });
	                	rowhtml += "</table>";
	                	$(All.get_active_tab() + " #listPricePrd").append(rowhtml);	
	                	$(All.get_active_tab() + " #inp_btn").css('display', 'block');
					    $(All.get_active_tab() + " #upd_btn").css('display', 'none');
                    }
                } else {
                	$(All.get_active_tab() + " #cat_inv_desc").val(null);
                	alert("Invalid product code :" +cat_inv_id)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	cancelUpdatePrdPrice : function() {
		$(All.get_active_tab() + " #inp_btn").css('display', 'block');
		$(All.get_active_tab() + " #upd_btn").css('display', 'none');
		$(All.get_active_tab() + " #cat_inv_desc").val(null);
		$(All.get_active_tab() + " #cat_inv_id").val(null);
		$(All.get_active_tab() + " #listPricePrd").html(null);
	},
	
	saveProductPrice : function(url, formid) {
		All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
        {  
            All.set_enable_button();
			if(data.response == "false") {
                All.set_error_message(".mainForm > .result", data.message);
            } 
            else {
                All.set_success_message(".mainForm > .result", data.message);
				All.cancelUpdateForm();
				$(All.get_active_tab() + " #listPricePrd").html(null);
            } 
            
        }, "json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
	},
	
		getDetailPrdBundling : function(nilai) {
		if(nilai == null || nilai == "") {
			
		}
	    else { 
			All.clear_div_in_boxcontent(" .mainForm .result");
			All.set_disable_button();
			All.get_wait_message();
			$.ajax({
	            url: All.get_url("product/bundling/id") +"/" +nilai,
	            type: "GET",
				dataType: "json",
	            success:
	            function(data){
	                All.set_enable_button();
					if(data.response == "true") {
						var header = data.header;
						$(All.get_active_tab() + " #prdid option[value="+header[0].prdcd+"]").attr('selected','selected');
						$(All.get_active_tab() + " #cat_inv_id_parent").val(header[0].prdcd);
						//$(All.get_active_tab() + " #prdid").val(header[0].prdcd);
						$(All.get_active_tab() + " #cat_desc").val(header[0].prdnm);
						if(header[0].price_w !== null) {
							$(All.get_active_tab() + " #west_price").val(All.num(header[0].price_w));
						}
						if(header[0].price_w !== null) {
							$(All.get_active_tab() + " #west_price_real").val(header[0].price_w);
						}
						if(header[0].price_e !== null) {
							$(All.get_active_tab() + " #east_price").val(All.num(header[0].price_e));
						}
						if(header[0].price_e !== null) {
							$(All.get_active_tab() + " #east_price_real").val(header[0].price_e);
						}
						if(header[0].price_cw !== null) {
							$(All.get_active_tab() + " #cust_west_price").val(All.num(header[0].price_cw));
						}
						if(header[0].price_cw !== null) {
							$(All.get_active_tab() + " #cust_west_price_real").val(header[0].price_cw);
						}
						if(header[0].price_ce !== null) {
							$(All.get_active_tab() + " #cust_east_price").val(All.num(header[0].price_ce));
						}
						if(header[0].price_ce !== null) {
							$(All.get_active_tab() + " #cust_east_price_real").val(header[0].price_ce);
						}
						if(header[0].bv !== null) {
							$(All.get_active_tab() + " #bv").val(All.num(header[0].bv));
						}
						if(header[0].bv !== null) {
							$(All.get_active_tab() + " #bv_real").val(header[0].bv);
						}
						if(header[0].weight !== null) {
							$(All.get_active_tab() + " #tot_weight_real").val(header[0].weight);
							$(All.get_active_tab() + " #tot_all_weight_real").val(header[0].weight);
						}
						if(header[0].weight !== null) {
							$(All.get_active_tab() + " #tot_weight").val(header[0].weight);
							$(All.get_active_tab() + " #tot_all_weight").val(header[0].weight);
						}
						var detailKlink = data.detailKlink;
						var detail = data.detail;
						if(detail !== null) {
							    $(All.get_active_tab() + " #db_comm").val("1");
							    //$(All.get_active_tab() + " #save").removeAttr("disabled");
							    //$(All.get_active_tab() + " #save2").attr("disabled", "disabled");
							    if(detailKlink !== null) {
							    	$(All.get_active_tab() + " #db_klink").val("1");
							    	$(All.get_active_tab() + " #ket").html("<font color=red>&nbsp;&nbsp;Data sudah ada di KlinkMLM dan DB Ecommerce</font>");
							    	$(All.get_active_tab() + " #save").val("Update Data Bundling");
							    	$(All.get_active_tab() + " #save").attr("onclick", "Product.post_update_product_grouping()");
							    	//All.set_disable_button();
							    	Product.showProductKlinkMlm(detailKlink);
							    } else {
							    	
								    $(All.get_active_tab() + " #db_klink").val("0");
								    $(All.get_active_tab() + " #ket").html("<font color=red>&nbsp;&nbsp;Data akan diimport ke DB KlinkMLM</font>");
								    $(All.get_active_tab() + " #save").attr("onclick", "Product.post_input_product_grouping()");
							    	All.set_enable_button();
							    }
							    Product.addRowFromDetailBundling(detailKlink,1);
							    
							    
						} else {
							
							if(detailKlink !== null) {
								$(All.get_active_tab() + " #db_klink").val("1");
								$(All.get_active_tab() + " #db_comm").val("0");
								//$(All.get_active_tab() + " #save").removeAttr("disabled");
							    //$(All.get_active_tab() + " #save2").attr("disabled", "disabled");
							    $(All.get_active_tab() + " #ket").html("<font color=red>&nbsp;&nbsp;Data akan diimport ke DB Ecommerce</font>");
							    $(All.get_active_tab() + " #save").val("Save");
							    $(All.get_active_tab() + " #save").attr("onclick", "Product.post_input_product_grouping()");
								Product.addRowFromDetailBundling(detailKlink,2);
								console.log("Dadada");
							} else {
								$(All.get_active_tab() + " #addData").html(null);
								$(All.get_active_tab() + " #db_comm").val("0");
								$(All.get_active_tab() + " #db_klink").val("0");
								$(All.get_active_tab() + " #ket").html("<font color=red>&nbsp;&nbsp;Data belum ada di KlinkMLM dan DB Ecommerce</font>");
								$(All.get_active_tab() + " #save").val("Save");
							    $(All.get_active_tab() + " #save").attr("onclick", "Product.post_input_product_grouping()");
							}
						}
						
						console.log("db_klink : " +$(All.get_active_tab() + " #db_klink").val());
						console.log("db_comm : " +$(All.get_active_tab() + " #db_comm").val());
					} else {
						$(All.get_active_tab() + " #addData").html(null);
						$(All.get_active_tab() + " input[type=text]").val(null);
						$(All.get_active_tab() + " #ket").html(null);
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
       }
	},
	
	showProductKlinkMlm : function(datax) {
		All.clear_div_in_boxcontent(" .mainForm .result");
	},
	
	addRowFromDetailBundling: function(data, param) {
		$(All.get_active_tab() + " #addData").html(null);
		var z = data.length;
		var tabidx = parseInt($(All.get_active_tab() + " #tabidx").val());
		var j = tabidx;
        //var z = parseInt($(All.get_active_tab() + " #amt_record").val()) + 1;
        $(All.get_active_tab() + " #amt_record").val(z);
		//console.log("jumlah : " +jum);
		var total_all_west = 0;
		var total_all_east = 0;
		var total_all_cust_west = 0;
		var total_all_cust_east = 0;
		var total_all_bv = 0;
		var total_all_weight = 0;
		var rowshtml = "";
		$.each(data, function(key, value) {
			//console.log("prdnm : " +value.prdnm)
			rowshtml = "<tr id="+ (key + 1) +">";
	        rowshtml +=  "<td><input onchange=Product.helper_show_prod_by_id("+ (key + 1) +") tabindex="+ j +" type=text class=span12 id=productcode"+ (key + 1) +" name=productcode[] value="+value.prdcd+" /></td>";
	        rowshtml +=  "<td><input readonly=yes type=text class=span12 id=productname"+ (key + 1) +"  name=productname[] value='"+value.prdnm+"' /></td>";
	        j++;
	        rowshtml += "<td><input tabindex="+ j +" style=text-align:right type=text id=qty"+ (key + 1) +" name=qty[] class='span12 input-qty' value="+All.num(parseInt(value.qty))+" onkeypress='if(event.keyCode==9) {Product.sales_qty_format("+ (key + 1) +");}' />";
	        rowshtml += "<input style='text-align:right;'  type=hidden class=span3 id=qty_real"+ (key + 1) +" name=qty_real[] value="+value.qty+" /></td>";
	        j++;
	        
	        //BV
	        rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tbv"+ (key + 1) +"  name=tbv[] value="+All.num(parseInt(value.bv))+" />";
	        rowshtml += "<input type=hidden id=tbv_real"+ (key + 1) +" name=tbv_real[] value="+value.bv+" />";
	        rowshtml += "</td>";

			var tbbv = parseInt(value.qty) * parseInt(value.bv);

	        rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tot_tbv"+ (key + 1) +"  name=tot_tbv[] value="+All.num(parseInt(tbbv))+" />";
	        rowshtml += "<input type=hidden id=tot_tbv_real"+ (key + 1) +" name=tot_tbv_real[] value="+tbbv+" />";
	        rowshtml += "</td>";


	        rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=west"+ (key + 1) +"  name=west[] value="+All.num(parseInt(value.price_w))+"  />";
	        rowshtml += "<input type=hidden id=west_real"+ (key + 1) +" name=west_real[] value="+parseInt(value.price_w)+" />";
	        rowshtml += "<input type=hidden id=east_real"+ (key + 1) +" name=east_real[] value="+parseInt(value.price_e)+" />";
	        rowshtml += "<input type=hidden id=west_cust_real"+ (key + 1) +" name=west_cust_real[] value="+parseInt(value.price_cw)+" />";
	        rowshtml += "<input type=hidden id=east_cust_real"+ (key + 1) +" name=east_cust_real[] value="+parseInt(value.price_ce)+" />";
	        rowshtml += "</td>";

			//weight
			rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tweight"+ (key + 1) +"  name=tweight[] value="+All.num(parseFloat(value.weight))+" />";
			rowshtml += "<input type=hidden id=tweight_real"+ (key + 1) +" name=tweight_real[] value="+value.weight+" />";
			rowshtml += "</td>";
			//console.log('berat '+value.weight);

			var tot_weight = parseInt(value.qty) * parseFloat(value.weight);

			rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tot_weight"+ (key + 1) +"  name=tot_weight[] value="+parseFloat(tot_weight).toFixed(3)+" />";
			rowshtml += "<input type=hidden id=tot_weight_real"+ (key + 1) +" name=tot_weight_real[] value="+tot_weight+" />";
			rowshtml += "</td>";

	        rowshtml += "<td><input readonly=yes style=text-align:right; type=text class=kanan id=total_west"+ (key + 1) +"  name=total_west[] value="+All.num(parseInt(value.tot_price_w))+" />";
	        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_west_real"+ (key + 1) +"  name=total_west_real[] value="+parseInt(value.tot_price_w)+" />";
	        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_east_real"+ (key + 1) +"  name=total_east_real[] value="+parseInt(value.tot_price_e)+" />";
	        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_west_cust_real"+ (key + 1) +"  name=total_west_cust_real[] value="+parseInt(value.tot_price_cw)+" />";
	        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_east_cust_real"+ (key + 1) +"  name=total_east_cust_real[] value="+parseInt(value.tot_price_ce)+" />";
	        rowshtml += "</td>";
	        //rowshtml += "<td ><a class='btn btn-small btn-danger' href=# id="+ amount +" onclick=Product.delete_row("+ amount +")><i class=ikon></i> </a></td>";

	        rowshtml += "</tr>";

	        total_all_west += parseInt(value.tot_price_w);
		    total_all_east += parseInt(value.tot_price_e);
		    total_all_cust_west += parseInt(value.tot_price_cw);
		    total_all_cust_east += parseInt(value.tot_price_ce);
		    total_all_bv += tbbv;
			total_all_weight += tot_weight;

			//var hasil = total_all_weight.toFixed(2);

			console.log('beratnya : '+total_all_weight);
			//console.log('beratnya : '+hasil);

		    $(All.get_active_tab() + " #addData").append(rowshtml);
		    $(All.get_active_tab() + " .kanan").removeClass().addClass("span12 text-right");
		    $(All.get_active_tab() + " .tombol").removeClass().addClass("btn btn-small btn-danger");
		    $(All.get_active_tab() + " .ikon").removeClass().addClass("icon-trash icon-white");


		});
		
		var fix_total_all_weight = total_all_weight.toFixed(3);
		var nextrow = z + 1;
		$(All.get_active_tab() + " #amount").val(nextrow);
		$(All.get_active_tab() + " #tabidx").val(j);
		$(All.get_active_tab() + " #total_all_west").val(All.num(total_all_west));
		$(All.get_active_tab() + " #total_all_west_real").val(total_all_west);
		$(All.get_active_tab() + " #total_all_east_real").val(total_all_east);
		$(All.get_active_tab() + " #total_all_cust_west_real").val(total_all_cust_west);
		$(All.get_active_tab() + " #total_all_cust_east_real").val(total_all_cust_east);
		$(All.get_active_tab() + " #total_bv").val(All.num(total_all_bv));
		$(All.get_active_tab() + " #total_bv_real").val(total_all_bv);
		$(All.get_active_tab() + " #tot_weight").val(fix_total_all_weight);
		$(All.get_active_tab() + " #tot_weight_real").val(fix_total_all_weight);

		$(All.get_active_tab() + " #productcode" +nextrow).focus();
	},

	helper_add_new_row : function() {
		var amount =  parseInt($(All.get_active_tab() + " #amount").val());
        var tabidx = parseInt($(All.get_active_tab() + " #tabidx").val());
        var j = tabidx;
        var z = parseInt($(All.get_active_tab() + " #amt_record").val()) + 1;
        $(All.get_active_tab() + " #amt_record").val(z);
        
        var koma = ",";
            
        var rowshtml = "<tr id="+ amount +">";
        rowshtml +=  "<td><input onchange=Product.helper_show_prod_by_id("+ amount +") tabindex="+ j +" type=text class=span12 id=productcode"+ amount +" name=productcode[] /></td>";
        rowshtml +=  "<td><input readonly=yes type=text class=span12 id=productname"+ amount +"  name=productname[] /></td>";
        j++;
        rowshtml += "<td><input tabindex="+ j +" style=text-align:right type=text id=qty"+ amount +" name=qty[] class='span12 input-qty' onBlur='Product.sales_qty_format("+ amount +")' />";
        rowshtml += "<input style=text-align:right;  type=hidden class=span3 id=qty_real"+ amount +" name=qty_real[] /></td>";
        j++;
        //BV
        rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tbv"+ amount +"  name=tbv[] />";
        rowshtml += "<input type=hidden id=tbv_real"+ amount +" name=tbv_real[] />";
        rowshtml += "</td>";                      
        rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tot_tbv"+ amount +"  name=tot_tbv[] />";
        rowshtml += "<input type=hidden id=tot_tbv_real"+ amount +" name=tot_tbv_real[] />";
        rowshtml += "</td>";
        
        //West Price
        rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=west"+ amount +"  name=west[]/>";
        rowshtml += "<input type=hidden id=west_real"+ amount +" name=west_real[] />";
        rowshtml += "<input type=hidden id=east_real"+ amount +" name=east_real[] />";
        rowshtml += "<input type=hidden id=west_cust_real"+ amount +" name=west_cust_real[] />";
        rowshtml += "<input type=hidden id=east_cust_real"+ amount +" name=east_cust_real[] />";
        rowshtml += "</td>";

		//weight
		rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tweight"+ amount +"  name=tweight[] />";
		rowshtml += "<input type=hidden id=tweight_real"+ amount +" name=tweight_real[] />";
		rowshtml += "</td>";

		//total weight
		rowshtml += "<td><input style=text-align:right; readonly=yes type=text class=kanan id=tot_weight"+ amount +"  name=tot_weight[] />";
		rowshtml += "<input type=hidden id=tot_weight_real"+ amount +" name=tot_weight_real[] />";
		rowshtml += "</td>";

        rowshtml += "<td><input readonly=yes style=text-align:right; type=text class=kanan id=total_west"+ amount +"  name=total_west[] />";
        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_west_real"+ amount +"  name=total_west_real[] />";
        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_east_real"+ amount +"  name=total_east_real[] />";
        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_west_cust_real"+ amount +"  name=total_west_cust_real[] />";
        rowshtml += "<input style=text-align:right; type=hidden class=kanan id=total_east_cust_real"+ amount +"  name=total_east_cust_real[] />";
        rowshtml += "</td>";
        //rowshtml += "<td ><a class='btn btn-small btn-danger' href=# id="+ amount +" onclick=Product.delete_row("+ amount +")><i class=ikon></i> </a></td>";
        
        rowshtml += "</tr>";

        var x = amount + 1;
        var y = tabidx + 2;
        $(All.get_active_tab() + " #amount").val(x);
        $(All.get_active_tab() + " #tabidx").val(y); 
        $(All.get_active_tab() + " #addData").append(rowshtml);
        $(All.get_active_tab() + " .kanan").removeClass().addClass("span12 text-right");
        $(All.get_active_tab() + " .tombol").removeClass().addClass("btn btn-small btn-danger");
        $(All.get_active_tab() + " .ikon").removeClass().addClass("icon-trash icon-white");
        $(All.get_active_tab() + " #productcode" +amount).focus();
	},
	
	helper_show_prod_by_id : function(frm) {
    //var pay_type = $(All.get_active_tab() + " #payn_type").val();
       var prdcd = $(All.get_active_tab() + " #productcode" +frm).val();
       $.ajax({
                dataType: 'json',
                url: All.get_url("product/id/") +prdcd,
                type: 'GET',
                success:
                function(data){
                    if(data !== null)
                    {
                           $(All.get_active_tab() + " #productname" +frm).val(data[0].prdnm);
                           var dp = parseInt(data[0].price_w);
                           var dp_east = parseInt(data[0].price_e);
                           var west_cp = parseInt(data[0].price_cw);
                           var east_cp = parseInt(data[0].price_ce);
                           var bv = parseInt(data[0].bv);
						   var weight = parseFloat(data[0].weight);
                           var qty = parseInt($(All.get_active_tab() + " #qty" +frm).val());
                           if(isNaN(qty)) 
                           {
                                qty = 1;
                           }
                           var tot_dp = dp * qty;
                           var tot_dp_east = dp_east * qty;
                           var tot_west_cp = west_cp * qty;
                           var tot_east_cp = east_cp * qty;
                           var tot_bv = bv * qty;
						   var tot_weight = weight * qty;
                           
                           
                            $(All.get_active_tab() + " #qty" +frm).val('1');

                            $(All.get_active_tab() + " #west" +frm).val(All.num(dp));

                            $(All.get_active_tab() + " #tbv" +frm).val(All.num(bv));
                            $(All.get_active_tab() + " #tbv_real" +frm).val(bv);

                            $(All.get_active_tab() + " #tot_tbv" +frm).val(All.num(tot_bv));
                            $(All.get_active_tab() + " #tot_tbv_real" +frm).val(tot_bv);
                            
                             
                            $(All.get_active_tab() + " #west_real" +frm).val(parseInt(dp));
                            $(All.get_active_tab() + " #east_real" +frm).val(parseInt(dp_east));
                            $(All.get_active_tab() + " #west_cust_real" +frm).val(parseInt(west_cp));
                            $(All.get_active_tab() + " #east_cust_real" +frm).val(parseInt(east_cp));

							//weight
							$(All.get_active_tab() + " #tweight" +frm).val((weight));
							$(All.get_active_tab() + " #tweight_real" +frm).val(weight);

							//total weight
							$(All.get_active_tab() + " #tot_weight" +frm).val(tot_weight);
							$(All.get_active_tab() + " #tot_weight_real" +frm).val(tot_weight);

                            $(All.get_active_tab() + " #total_west" +frm).val(All.num((tot_dp)));
                            $(All.get_active_tab() + " #total_west_real" +frm).val(tot_dp);

                            $(All.get_active_tab() + " #total_east_real" +frm).val(tot_dp_east);

                            $(All.get_active_tab() + " #total_west_cust_real" +frm).val(tot_west_cp);
                            $(All.get_active_tab() + " #total_east_cust_real" +frm).val(tot_east_cp);
                            
                            
                            Product.hitungTotalPrd();    
                            $(All.get_active_tab() + " #qty" +frm).focus();
                    }
                    else
                    {
                        alert('product does not exist');
                        $(All.get_active_tab() + " #productname" +frm).val(null);
                        $(All.get_active_tab() + " #qty" +frm).val(null);
                        $(All.get_active_tab() + " #qty_real" +frm).val(null);
                        $(All.get_active_tab() + " #west" +frm).val(null);
                        $(All.get_active_tab() + " #west_real" +frm).val(null);
                        $(All.get_active_tab() + " #total_west" +frm).val(null);
                        $(All.get_active_tab() + " #total_west_real" +frm).val(null);
                        $(All.get_active_tab() + " #total_east_real" +frm).val(null);
                        $(All.get_active_tab() + " #total_west_cust_real" +frm).val(null);
                        $(All.get_active_tab() + " #total_east_cust_real" +frm).val(null);
                        $(All.get_active_tab() + " #productcode" +frm).focus();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                     alert(thrownError + ':' +xhr.status);
                }
            }); 
            //alert('isi : ' +data.dp);
    },
    
    hitungTotalPrd : function() {
    	var jum_trx = parseInt($(All.get_active_tab() + " #amt_record").val());
    	var tot_all_west_price = 0;
        var tot_all_east_price = 0;
        var tot_all_west_cust_price = 0;
        var tot_all_east_cust_price = 0;
        var tot_bv = 0;
		var tot_weight = 0;
		//var hasil = 0;
        console.log("isi : " +jum_trx); 
                         
        for(var i = 1; i <= jum_trx; i++)
        {
        	 console.log("isi urutan qty : " +i);
             var ss = $(All.get_active_tab() + " #qty" +i).val();
             if(ss === undefined)
             {
                tot_all_west_price = tot_all_west_price + 0;
                tot_all_east_price = tot_all_east_price + 0;
                tot_all_west_cust_price = tot_all_west_cust_price + 0;
                tot_all_east_cust_price = tot_all_east_cust_price + 0;
                tot_bv = tot_bv + 0;
				 tot_weight = tot_weight + 0;
                
             } 
             else   
             {
                tot_all_west_price += parseInt($(All.get_active_tab() + " #total_west_real" +i).val());
                tot_all_east_price += parseInt($(All.get_active_tab() + " #total_east_real" +i).val());
                tot_all_west_cust_price += parseInt($(All.get_active_tab() + " #total_west_cust_real" +i).val());
                tot_all_east_cust_price += parseInt($(All.get_active_tab() + " #total_east_cust_real" +i).val());
                tot_bv += parseInt($(All.get_active_tab() + " #tot_tbv_real" +i).val());
				tot_weight += parseFloat($(All.get_active_tab() + " #tot_weight_real" +i).val())
                 

				 //hasil = tot_weight.toFixed(2);

             }
             
             
        }
        var fix_tot_weight = tot_weight.toFixed(3);
        
        console.log("tot_all_west_price : " +tot_all_west_price);
             console.log("tot_all_east_price : " +tot_all_east_price);
             console.log("tot_all_west_cust_price : " +tot_all_west_cust_price);
             console.log("tot_all_east_cust_price : " +tot_all_east_cust_price);
            //$(All.get_active_tab() + " #new_record" +frm).focus();
            $(All.get_active_tab() + " #total_all_west").val(All.num(tot_all_west_price));
            $(All.get_active_tab() + " #total_all_west_real").val(tot_all_west_price);
            $(All.get_active_tab() + " #total_all_east_real").val(tot_all_east_price);
            $(All.get_active_tab() + " #total_all_west_cust_real").val(tot_all_west_cust_price);
            $(All.get_active_tab() + " #total_all_east_cust_real").val(tot_all_east_cust_price);
            $(All.get_active_tab() + " #total_bv").val(All.num(tot_bv));
            $(All.get_active_tab() + " #total_bv_real").val(tot_bv);
			$(All.get_active_tab() + " #tot_weight").val(fix_tot_weight);
			$(All.get_active_tab() + " #tot_weight_real").val(fix_tot_weight);
            
            //ubah data pada header
            $(All.get_active_tab() + " #west_price").val(All.num(tot_all_west_price));
            $(All.get_active_tab() + " #west_price_real").val(tot_all_west_price);
            $(All.get_active_tab() + " #east_price").val(All.num(tot_all_east_price));
            $(All.get_active_tab() + " #east_price_real").val(tot_all_east_price);
            $(All.get_active_tab() + " #cust_west_price").val(All.num(tot_all_west_cust_price));
            $(All.get_active_tab() + " #cust_west_price_real").val(tot_all_west_cust_price);
            $(All.get_active_tab() + " #cust_east_price").val(All.num(tot_all_east_cust_price));
            $(All.get_active_tab() + " #cust_east_price_real").val(tot_all_east_cust_price);
            $(All.get_active_tab() + " #bv").val(All.num(tot_bv));
            $(All.get_active_tab() + " #bv_real").val(tot_bv);
			$(All.get_active_tab() + " #tot_all_weight").val(fix_tot_weight);
			$(All.get_active_tab() + " #tot_all_weight_real").val(fix_tot_weight);
            
            
            //$(All.get_active_tab() + " #new_record" +frm).focus();
    },
    sales_qty_format : function(frm)
    {         
        var qty = $(All.get_active_tab() + " #qty" +frm).val();
        var dp = parseInt($(All.get_active_tab() + ' #west_real' +frm).val());
        var dp_east = parseInt($(All.get_active_tab() + ' #east_real' +frm).val());
        var west_cust = parseInt($(All.get_active_tab() + ' #west_cust_real' +frm).val());
        var east_cust = parseInt($(All.get_active_tab() + ' #east_cust_real' +frm).val());
        var bv = parseInt($(All.get_active_tab() + ' #tbv_real' +frm).val());
		var weight = parseFloat($(All.get_active_tab() + ' #tweight_real' +frm).val());
        
        var noo = 'Qty tidak boleh null';
        var nm =  /^[0-9]+$/;
       
       if(qty == 0 || qty == '' )
        {
            $(All.get_active_tab() + " #qty" +frm).val("1");
            /*alert('qty tidak boleh kosong');
            
            $(All.get_active_tab() + " #save").attr("disabled", "disabled");*/
           Product.sales_qty_format(frm);
            
        }
        else if(!qty.match(nm))
        {
            $(All.get_active_tab() + " #qty" +frm).focus();
            alert('qty tidak boleh negatif' +qty);
            
            $(All.get_active_tab() + " #save").attr("disabled", "disabled");
        }
        else
        {
            $(All.get_active_tab() + " #save").removeAttr("disabled");
            
            var tabdx =  parseInt($(All.get_active_tab() + " #tabidx").val());
            var saveBtn = tabdx + 1;
            $(All.get_active_tab() + " #new_record").attr('tabindex', tabdx);
            $(All.get_active_tab() + " #save").attr('tabindex', saveBtn);
            
            var qtyxx = parseInt(qty);
			var weightx = parseFloat(weight);
            var tot_west = qtyxx * dp;
            var tot_east = qtyxx * dp_east;
            var tot_west_cust = qtyxx * west_cust;
            var tot_east_cust = qtyxx * east_cust;
            var tot_bv = qtyxx * bv;
			var tot_weight = qtyxx * weightx;
			
			var fix_tot_weight = tot_weight.toFixed(3);

            
            $(All.get_active_tab() + ' #total_west' +frm).val(All.num(tot_west));
            $(All.get_active_tab() + ' #total_west_real' +frm).val(tot_west);
            $(All.get_active_tab() + ' #total_east_real' +frm).val(tot_east);
            $(All.get_active_tab() + ' #total_west_cust_real' +frm).val(tot_west_cust);
            $(All.get_active_tab() + ' #total_east_cust_real' +frm).val(tot_east_cust);
            $(All.get_active_tab() + ' #tot_tbv' +frm).val(All.num(tot_bv));
            $(All.get_active_tab() + ' #tot_tbv_real' +frm).val(tot_bv);
            $(All.get_active_tab() + ' #qty_real' +frm).val(qtyxx);
			$(All.get_active_tab() + ' #tot_weight' +frm).val(fix_tot_weight);
			$(All.get_active_tab() + ' #tot_weight_real' +frm).val(fix_tot_weight);
            Product.hitungTotalPrd();
            
                      
                
          }
    },
    
    delete_row : function(frm)
    {
        var dp = $(All.get_active_tab() + " #total_west_real" +frm).val();
        if(dp === undefined)
        {
            dp = 0;
        } 
        var tot_all_west_price = parseInt($(All.get_active_tab() + " #total_all_real").val()) - parseInt(dp);   
        
        var tot_all_west_price_shows = All.num(tot_all_west_price);
    
        $(All.get_active_tab() + " #total_all_real").val(tot_all_west_price); 
        $(All.get_active_tab() + " #total_all").val(tot_all_west_price_shows);
        
        $(All.get_active_tab() + " #total_west_pay").val(tot_all_west_price_shows);
        $(All.get_active_tab() + " #total_west_pay_real").val(tot_all_west_price);
        
        $(All.get_active_tab() + " #totalDp").val(tot_all_west_price_shows);
        $(All.get_active_tab() + " #totalDp_real").val(tot_all_west_price);
        
       
        $(All.get_active_tab() + " tr#" +frm).remove(); 
    },
    
    post_input_product_grouping : function() {
    	$.post(All.get_url('product/bundling/save') , $(All.get_active_tab() + " #formInputPrdBundling").serialize(), function(data)
        {              
            All.set_enable_button();
			alert(data.message);
			All.reload_page('product/bundling');
        },"json");
    },
    
    post_update_product_grouping : function() {
    	$.post(All.get_url('product/bundling/update') , $(All.get_active_tab() + " #formInputPrdBundling").serialize(), function(data)
        {              
            All.set_enable_button();
			alert(data.message);
			All.reload_page('product/bundling');
        },"json");
    },
    
    getDetailProductPine : function(nilai) {
       if(nilai !== null || nilai !== "" || nilai !== " ") {
       	   All.set_disable_button();
		   All.get_wait_message();
	       $.ajax({
	                url: All.get_url("product/pine/list/id"),
	                type: 'POST',
	                data: {prdcd: nilai},
	                success:
	                function(data){
	                	All.set_enable_button();
	                    $(All.get_active_tab() + " .mainForm > .result").html(null);
	                    $(All.get_active_tab() + " .mainForm > .result").html(data);
	                },
	                error: function (xhr, ajaxOptions, thrownError) {
	                     alert(thrownError + ':' +xhr.status);
	                }
	            });      	
	       } else {
	       		alert("Product code must be filled..");
	       }     
       
    },
    
    importPrdToEcommerce : function() {
    	All.set_disable_button();
    	$.post(All.get_url('product/pine/import/save') , $(All.get_active_tab() + " #formImportPrd").serialize(), function(data)
        {              
            All.set_enable_button();
	        if(data.response == "true") {
	        	$(All.get_box_content() + " .mainForm > .result").html(null);
	        	//All.set_success_message(All.get_active_tab() + " .mainForm > .result", "Import product to DB E-commerce success..")
	        	All.set_success_message(".mainForm .result", data.message);
	        } else {
	        	alert(data.message);
	        }
			//All.reload_page('product/bundling');
        },"json");
    },
    
    getPrdBundlingByID : function(prdcd) {
    	
    },
    
    updateStatusPrd : function(prdcd, param, value) {
    	//alert("param yg di update : " +param + " di set : " +value);
    	var r = confirm("Anda yakin akan mengubah status produk ini ?");
	    if (r == true) {
	       All.set_disable_button();
		   All.get_wait_message();
	       $.ajax({
	                url: All.get_url("product/status/update"),
	                type: 'POST',
	                dataType: 'json',
	                data: {prdcd: prdcd, param: param, value: value},
	                success:
	                function(data){
	                	All.set_enable_button();
	                    alert(data.message);
	                },
	                error: function (xhr, ajaxOptions, thrownError) {
	                     alert(thrownError + ':' +xhr.status);
	                }
	            });      	
	         
	    } else {
	        txt = "You pressed Cancel!";
	    }
    },
    
    sm_checkBv : function(nilai) {
    	var x = nilai.value;
    	
    	All.set_disable_button();
	    All.get_wait_message();
        $.ajax({
                url: All.get_url("lounge/check/") +x,
                type: 'GET',
                dataType: 'json',
                success:
                function(data){
                	All.set_enable_button();
                    if(data.response == 'false') {
                    	alert(data.message);
                    } else {
                    	
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                     alert(thrownError + ':' +xhr.status);
                }
         });
    }
    
}
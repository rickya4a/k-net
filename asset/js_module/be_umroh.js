var $ = jQuery;

var be_umroh = {
	getTipePerjalanan : function(nilai) {
		$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            
            $.ajax({
	            url: All.get_url("be_umroh/helper/journey/") +nilai,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data != null) {
	                	 $(All.get_active_tab() + " #jdwlbrkt").html(null);
	                	 var rowhtml = "";
	                	 $.each(data,function(key, value) {
	                	 	var nix = value.id+"-"+value.departuredesc;
	                	 	var nix2 = value.departuredt+" "+value.departuredesc;
	                	 	rowhtml += "<option value='"+nix+"'>"+nix2+"</option>";
	                	 });	
	                	 $(All.get_active_tab() + " #jdwlbrkt").append(rowhtml);
	                } else {
	                	 alert("Tidak ada jadwal keberangkatan..");
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      }); 
		
	},
    
	getTipeJamaah : function(){
        //console.log("masuk tipe jamaah");
        //console.log($(All.get_active_tab() + ' input[name^="tipeJamaah"]').length);
        if($(All.get_active_tab() + ' input[name^="tipeJamaah"]').length) {
            
            /*var arrThn = [];
            $(All.get_active_tab() + ' #thnlhr').next('select').find('option').each(function(){
              arrThn.push($(this).val());
            });*/
           $(All.get_active_tab() + ' #thnlhr').html(null);
           var x = be_umroh.datebirth_combo(18, 100);
           $(All.get_active_tab() + ' #thnlhr').append(x);
             
            /*$(All.get_active_tab() + ' #thnlhr').html('');
            for(var i = 18; i < arrThn.length; i++) {
                $(All.get_active_tab() + ' #thnlhr').append('<option value="'+arrThn[i]+'">'+arrThn[i]+'</option>');
            }*/
            
            $(All.get_active_tab() + ' #idno').removeAttr('disabled');
            //$(All.get_active_tab() + ' #tel_hp').removeAttr('disabled');
            $(All.get_active_tab() + ' #email').removeAttr('disabled');
            $(All.get_active_tab() + " #fullnm").attr('readonly', true);
            
            //$(All.get_active_tab() + " #idstk").attr('disabled', 'disabled');
            //$(All.get_active_tab() + ' #idsponsor').parent().addClass('hide');
            //$(All.get_active_tab() + ' #nmsponsor').parent().addClass('hide');
            $(All.get_active_tab() + ' #idsponsor').attr('readonly', true);
            $(All.get_active_tab() + ' #nmsponsor').attr('readonly', true);
            $(All.get_active_tab() + ' input[name^="tipeJamaah"]').change(function(e) {
                $(All.get_active_tab() + ' #thnlhr').html('');
                //console.log(arrThn);
                //JIKA MEMILIH BUKAN MEMBER < 17 TAHUN
                if($(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val() === '2') {
                    //console.log("choose : " +$(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val());
                    console.log("tipe jamaah : " + $(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val());
                    $(All.get_active_tab() + ' #lbl-idmemb').text("ID Member Reference");
                    $(All.get_active_tab() + ' #thnlhr').html(null);
		           var x = be_umroh.datebirth_combo(0, 17);
		           $(All.get_active_tab() + ' #thnlhr').append(x);
                    
                    
					$(All.get_active_tab() + ' #idmember').removeAttr('readonly');
                    
                    $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
                    $(All.get_active_tab() + " #addr1").val('');
                    $(All.get_active_tab() + " #fullnm").removeAttr('readonly');
                    $(All.get_active_tab() + " #addr1").removeAttr('readonly');
                    $(All.get_active_tab() + " #tel_hp").removeAttr('readonly');
                    $(All.get_active_tab() + " #idstk").val(0);
                    $(All.get_active_tab() + " #idstk").attr('readonly', true);
                    //$(All.get_active_tab() + ' #idsponsor').parent().addClass('hide');
                    //$(All.get_active_tab() + ' #nmsponsor').parent().addClass('hide');
                    $(All.get_active_tab() + ' #idno').attr('readonly', true);
                    $(All.get_active_tab() + ' #idsponsor').attr('readonly', true);
                    $(All.get_active_tab() + ' #nmsponsor').attr('readonly', true);
                    //$(All.get_active_tab() + " .spnsr").css('display', 'none');
                    $(All.get_active_tab() + " #fullnm").focus();
                   
                } 
                //JIKA MEMILIH MEMBER
                else if($(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val() === '1') {
                    console.log("tipe jamaah : " + $(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val());
                    //console.log("choose : " +$(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val());
                    $(All.get_active_tab() + ' #lbl-idmemb').text("ID Member");
                    $(All.get_active_tab() + ' #thnlhr').html(null);
		            var x = be_umroh.datebirth_combo(18, 100);
		            $(All.get_active_tab() + ' #thnlhr').append(x);
		            $(All.get_active_tab() + ' #idmember').val(null);
					 $(All.get_active_tab() + ' #idmember').removeAttr('readonly');
                   $(All.get_active_tab() + " #idstk").val(0);
                    //$(All.get_active_tab() + ' #tel_hp').attr('disabled');
                    $(All.get_active_tab() + ' #email').attr('disabled');
                    $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
                    $(All.get_active_tab() + " #fullnm").attr('readonly', true);
                    $(All.get_active_tab() + " #idno").attr('readonly', true);
                    $(All.get_active_tab() + " #addr1").attr('readonly', true);
                    //$(All.get_active_tab() + " #tel_hp").attr('readonly', true);
                    $(All.get_active_tab() + " #addr1").val('');
                    $(All.get_active_tab() + " #idstk").attr('readonly', true);
                    $(All.get_active_tab() + ' #idsponsor').attr('readonly', true);
                    $(All.get_active_tab() + ' #nmsponsor').attr('readonly', true);
                    //$(All.get_active_tab() + " .spnsr").css('display', 'none');
                    $(All.get_active_tab() + " #idmember").focus();
                }else{
                    console.log("tipe jamaah : " + $(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val());
                    $(All.get_active_tab() + ' #lbl-idmemb').text("ID Member");
                    //console.log("choose : " +$(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val());
                    $(All.get_active_tab() + ' #thnlhr').html(null);
		            var x = be_umroh.datebirth_combo(17, 100);
		            $(All.get_active_tab() + ' #thnlhr').append(x);
		            
                    $(All.get_active_tab() + ' #idno').removeAttr('disabled');
                    //$(All.get_active_tab() + ' #tel_hp').removeAttr('disabled');
                    $(All.get_active_tab() + ' #email').removeAttr('disabled');
                    //$(All.get_active_tab() + ' #idstk').removeAttr('readonly');
                    $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
                    $(All.get_active_tab() + " #fullnm").removeAttr('readonly');
                    $(All.get_active_tab() + " #idno").removeAttr('readonly');
                    $(All.get_active_tab() + " #addr1").removeAttr('readonly');
                    //$(All.get_active_tab() + " #tel_hp").removeAttr('readonly');
                    $(All.get_active_tab() + " #addr1").val('');
                     $(All.get_active_tab() + " #idstk").val(0);
                    //$("#idsponsor").removeAttr('readonly');
                    //$(All.get_active_tab() + ' #idsponsor').parent().removeClass('hide');
                    //$(All.get_active_tab() + ' #nmsponsor').parent().removeClass('hide');
                    $(All.get_active_tab() + ' #idsponsor').removeAttr('readonly');
                    $(All.get_active_tab() + ' #nmsponsor').removeAttr('readonly');
                    $(All.get_active_tab() + " #idmember").attr('readonly', true);
                    $(All.get_active_tab() + " #fullnm").focus();
                }
            }); 
        }
    },
    
    datebirth_combo : function(min, max) {
    	var currentYear = (new Date).getFullYear();
    	var min = currentYear - min;
		var max = currentYear - max;
		var str = "";
		for(i = min; i > max; i--) {
			str += "<option value="+i+">"+i+"</option>";
		}
		return str;
    },
    
    get_distributor_info : function (idmember){
        if(idmember == ""){
            //$("#fullnm").val(null);
            $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
            $(All.get_active_tab() + " #fullnm").attr('readonly', false);
        }
        //if(idmember.length == 9) {
            // asumsi panjang id member PASTI 10 digit
            var idxx = $(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val();
            if(idxx == "1") {
	            //$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	            All.set_disable_button();
	            $.ajax({
		            url: All.get_url("be_umroh/helper/distInfo/") +idmember,
		            type: 'GET',
		            dataType: "json",
		            success:
		            function(data){
		                if(data.response == "true") {
		                	 var tgl = data.arrayData[0].birthdt;
		                	 res = tgl.split("-"); 
		                	 $(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
		                     $(All.get_active_tab() + " #addr1").val(data.arrayData[0].addr1);
		                     $(All.get_active_tab() + " #tel_hp").val(data.arrayData[0].tel_hp);
		                     $(All.get_active_tab() + " #tel_hm").val(data.arrayData[0].tel_hm);
		                     $(All.get_active_tab() + " #idno").val(data.arrayData[0].idno);
		                     $(All.get_active_tab() + " #email").val(data.arrayData[0].email);
		                     $(All.get_active_tab() + " #sex").val(data.arrayData[0].sex);
		                     $(All.get_active_tab() + " #birthplace").val(data.arrayData[0].birthpl);
		                     
		                     $(All.get_active_tab() + " #idsponsor").val(data.arrayData[0].sponsorid);
		                     $(All.get_active_tab() + " #nmsponsor").val(data.arrayData[0].sponsorname);
		                     
		                     $(All.get_active_tab() + " #idrecruiter").val(data.arrayData[0].sfno_reg);
		                     $(All.get_active_tab() + " #nmrecruiter").val(data.arrayData[0].recruitername);
		                     
		                     $(All.get_active_tab() + " #idstk").val(data.arrayData[0].bnsstmsc);
		                     //$(All.get_active_tab() + " #idstk").attr('readonly', true);
		                     $(All.get_active_tab() + " #tgllhr").val(res[0]);
		                     $(All.get_active_tab() + " #blnlhr").val(res[1]);
		                     $(All.get_active_tab() + " #thnlhr").val(res[2]);
		                     $(All.get_active_tab() + " #addr1").attr('readonly', true);
		                     $(All.get_active_tab() + " #fullnm").attr('readonly', true);
		                     //$(All.get_active_tab() + " #tel_hp").attr('readonly', true);
		                     //$("#tel_hp").attr('readonly', true);
		                     //$("#idno").attr('readonly', true);
		                     $(All.get_active_tab() + " #birthplace").focus();
		                     //$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
		                     All.set_enable_button();
		                } else {
		                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
		                     alert('ID Sponsor salah atau sudah termination');
		                     $(All.get_active_tab() + " #fullnm").attr('readonly', true);
		                     //$("#fullnm").attr('readonly', false);
		                     $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
		                     $(All.get_active_tab() + " #idmember").focus();
		                }
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
		      }); 
           } 
            
        //}
    },
    
    get_sponsor_info: function (idsponsor) {
    	//if(idsponsor.length == 5) {
    		$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            
            $.ajax({
	            url: All.get_url("be_umroh/helper/distInfo/") +idsponsor,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #nmsponsor").val(data.arrayData[0].fullnm);
	                	 $(All.get_active_tab() + " #nmsponsor").attr('readonly', true);
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('ID Sponsor salah atau sudah termination')
	                     $(All.get_active_tab() + " #nmsponsor").attr('readonly', true);
	                     $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
	                     $(All.get_active_tab() + " #idsponsor").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      }); 
    	//}	
    },
    
    get_recruiter_info: function (idsponsor) {
    	//if(idsponsor.length == 5) {
    		$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            
            $.ajax({
	            url: All.get_url("be_umroh/helper/distInfo/") +idsponsor,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #nmrecruiter").val(data.arrayData[0].fullnm);
	                	 $(All.get_active_tab() + " #nmrecruiter").attr('readonly', true);
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('ID Recruiter salah atau sudah termination')
	                     $(All.get_active_tab() + " #nmrecruiter").attr('readonly', true);
	                     $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
	                     $(All.get_active_tab() + " #idrecruiter").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      }); 
    	//}	
    },
    
    get_recruiter_info: function (idsponsor) {
    	//if(idsponsor.length == 5) {
    		$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            
            $.ajax({
	            url: All.get_url("be_umroh/helper/distInfo/") +idsponsor,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #nmrecruiter").val(data.arrayData[0].fullnm);
	                	 $(All.get_active_tab() + " #nmrecruiter").attr('readonly', true);
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('ID Sponsor salah atau sudah termination')
	                     $(All.get_active_tab() + " #nmrecruiter").attr('readonly', true);
	                     $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
	                     $(All.get_active_tab() + " #idrecruiter").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      }); 
    	//}	
    },
    
    cekNoKtp : function(noktp){
        
		$.ajax({
	            url: All.get_url("be_umroh/helper/ktp/") +noktp,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "false") {
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('No KTP sudah terdaftar');
	                     $(All.get_active_tab() + " #idno").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
	
	cekNoHp : function(tel_hp){
        
		$.ajax({
	            url: All.get_url("be_umroh/helper/hp/") +tel_hp,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "false") {
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('No Hp Sudah Terdaftar atas nama ' +data.arrayData[0].fullnm);
	                     $(All.get_active_tab() + " #tel_hp").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
		
    },

	isValidEmailAddress: function(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    },	
    
    getPreviewInpUmroh : function (){
        All.set_disable_button();
        $.post(All.get_url("be_umroh/preview") , $("#formUmroh").serialize(), function(hasil)
        {
            All.set_enable_button();
            //if(hasil == "Semua field harus diisi..!!" || hasil == "KTP atau Passport Sudah Terpakai") {
            //alert(hasil.message);
            All.clear_div_in_boxcontent(".mainForm .result");
            if(hasil.response == "false") {
                alert(hasil.message);
            } else if(hasil.response == "double") {
            	alert(hasil.message);
            	
            	var arrayData = hasil.arrayData;
            	var rowshtml = "";
		 	   //rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
		 	    rowshtml += "<table width='80%' align='center' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		        rowshtml += "<thead><tr><th colspan='2' bgcolor='#lightgrey'>List Registrasi Umroh</th></tr>";
		        
		        rowshtml += "<tr><td width='20%'>ID Member</td><td>"+arrayData[0].dfno+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>Nama Member</td><td>"+arrayData[0].fullnm+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>No KTP</td><td>"+arrayData[0].idno+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>Tgl Input</td><td>"+arrayData[0].createdt+"</td></tr>";
		        rowshtml += "<tr><td width='20%'>Input By</td><td>"+arrayData[0].createnm+"</td></tr></table>";
			    $(All.get_box_content() + ".mainForm .result").append(rowshtml);
            } else {
                $(All.get_active_tab() + " .mainForm").hide();
                $(All.get_active_tab() + " .nextForm1").html(null);
                var data = hasil.arrayData[0];
                var rowhtml = "<form action='"+All.get_url("be_umroh/print")+"' method='POST' target='_BLANK'><table class='table table-striped table-bordered' width=100%>";
                rowhtml += "<tr><td colspan=2 align=center><h3>No Registrasi Umroh : "+data.registerno+"</h3></td></tr>";
                rowhtml += "<tr><td width=25%>ID Member</td><td>&nbsp;"+data.dfno+"</td></tr>";
                rowhtml += "<tr><td>Nama Lengkap</td><td>&nbsp;"+data.fullnm+"</td></tr>";
                rowhtml += "<tr><td>No HP</td><td>&nbsp;"+data.tel_hp+"</td></tr>";
                rowhtml += "<tr><td>Jenis Paket</td><td>&nbsp;"+data.departuredesc+"</td></tr>";
                rowhtml += "<tr><td>Tgl Keberangkatan</td><td>&nbsp;"+data.departuredt+"</td></tr>";
                rowhtml += "<tr><td>";
                rowhtml += "<input type=hidden id='noreg' name='noreg' value='"+data.registerno+"' />";
                rowhtml += "<input type=button id='backToFRM' name='noreg' class='btn btn-small btn-warning' value='Input Baru' onclick='be_umroh.backToInputUmroh()' />&nbsp;&nbsp;";
                //rowhtml += "<input type=submit class='btn btn-small btn-success' value='Cetak' /></td></tr>";
                rowhtml += "</table></form>";
                $(All.get_active_tab() + " .nextForm1").append(rowhtml);
            }
        },"json").fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
    },
    
    backToInputUmroh : function() {
    	 $(All.get_active_tab() + " .nextForm1").html(null);
    	 $(All.get_active_tab() + " .mainForm").show();
    	 $(All.get_active_tab() + " input[type=text]").val(null);
    	 //$(All.get_active_tab() + ' input[name^="tipeJamaah"]:checked').val('1');
    },
    
    getListRegUmroh : function() {
    	 All.set_disable_button();
         $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
         $.post(All.get_url("be_umroh/reg/list/get") , $(All.get_active_tab() + " #frmRegList").serialize(), function(data)
         {  
            All.set_enable_button();
            All.clear_div_in_boxcontent(".mainForm .result");
            if(data.response == "false") {
            	alert(data.message)
            } else {
            	var arrayData = data.arrayData;
				var rowshtml = "";
		 	   //rowshtml += "<input class='btn btn-small btn-primary' type=button value='Posting Data' onclick='All.postListData('listTrxToPost','trans/posting/save')' />";
		 	    rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
		        rowshtml += "<thead><tr><th colspan='6' bgcolor='#lightgrey'>List Registrasi Umroh</th></tr>";
		        rowshtml += "<tr bgcolor='#f4f4f4'>";
			    //rowshtml += "<th><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
		        rowshtml += "<th width='5%'>No</th>";
		        rowshtml += "<th width='10%'>Voucher No</th>";
		        rowshtml += "<th width='10%'>Reg No</th>";
		        rowshtml += "<th width='13%'>ID Member</th>";
		        rowshtml += "<th>Member Name</th>";
		        rowshtml += "<th width='10%'>Nominal</th>";
			    rowshtml += "<th width='10%'>CN No.</th>";
			     rowshtml += "<th width='10%'>Act</th>";
		        rowshtml += "</tr></thead>";
		        rowshtml += "<tbody>";
		        $.each(arrayData,function(key, value){
		                     //rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
                             rowshtml += "<td align=right>"+(key+1)+"&nbsp;</td>";
                             rowshtml += "<td align=center>"+value.voucherno+"</td>";
                             rowshtml += "<td align=center>"+value.registerno+"</td>";
                             rowshtml += "<td>"+value.dfno+"</td>";
                             rowshtml += "<td>"+value.fullnm+"</td>";
                             rowshtml += "<td align=right>"+All.num(parseInt(value.tot_fund))+"</td>";
                             rowshtml += "<td align=right>"+value.CNno+"</td>";
                             if(value.voucherno != null) {
	                             var sd = All.get_url('be_umroh/print/notaUmroh/')
	                             sd += value.registerno;
	                             rowshtml += "<td align=right><a class='btn btn-success' href='"+sd+"' target='_blank'>Print Nota</a></td></tr>";
                             } else {
                             	 rowshtml += "<td align=right>&nbsp;</td></tr>";
                             }
		                });
		        
		        rowshtml += "</tbody>";
				
			    rowshtml += "</table>";
			    //rowshtml += "<input type='button' class='btn btn-small btn-primary' value='Posting Data' onclick=be_trans.savePostingTrx() />";
			    $(All.get_box_content() + ".mainForm .result").append(rowshtml);
			    All.set_datatable();
            }
         },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
    },
    
   getListPayUmroh : function() {
        All.set_disable_button();
            $(All.get_active_tab() + " #listingPayUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/listPayment/app") , $(All.get_active_tab() + " #frmApp").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listingPayUmroh").html(null);
                $(All.get_active_tab() + " #listingPayUmroh").html(hasil);   
            });
    },
    
    backListPayUmroh : function(){
        $(All.get_active_tab() + " #frmApproval").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
        be_umroh.getListPayUmroh();
    },
    
    approvePayment : function(regno,from,to,tipe_perjalanan){
        //console.log("test "+regno);
        All.set_disable_button();
        $(All.get_active_tab() + " #frmApproval").hide();
        //$(All.get_active_tab() + " #listingPayUmroh").hide();
         $.ajax({
            url: All.get_url("be_umroh/listPayment/appRes/" +regno+"/" +from+"/" +to+"/" +tipe_perjalanan),
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
                $(All.get_active_tab() + " .nextForm1").html(null);
                $(All.get_active_tab() + " .nextForm1").html(data);
                
            }
        });
    },
    
    getVoucherInfo : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #dtVchUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/reprint/voucher/act") , $(All.get_active_tab() + " #frmVchPending").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #dtVchUmroh").html(null);
                $(All.get_active_tab() + " #dtVchUmroh").html(hasil);   
            });
    },
    
    resendSms : function(regno){
        //console.log("test "+regno);
        All.set_disable_button();
        $(All.get_active_tab() + " #formDtUmrohh").hide();
        //$(All.get_active_tab() + " #listingPayUmroh").hide();
         $.ajax({
            url: All.get_url("be_umroh/resendSms/act/" +regno),
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
                $(All.get_active_tab() + " .nextForm1").html(null);
                $(All.get_active_tab() + " .nextForm1").html(data);
                
            }
        });
    },
    
    back_to_list : function(){
        $(All.get_active_tab() + " #formDtUmrohh").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
    },
    
    get_registerno_info : function(regnos){
        //console.log("masuk kemari");
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            $.ajax({
             dataType: 'json',   
             type: "POST",
             url : All.get_url("be_umroh/helper/regnoInfo"),
             data : {regnos: regnos},
             success: function(data)
             {
                var arrayData = data.arrayData;
                if(data.response == "true") {
                    //console.log("masuk kemari " +arrayData[0].dfno);
                    $(All.get_active_tab() + " #idmember").val(arrayData[0].dfno);
                     $(All.get_active_tab() + " #nmmember").val(arrayData[0].fullnm);
                     $(All.get_active_tab() + " #pktUmroh").val(arrayData[0].departuredesc);
                     $(All.get_active_tab() + " #saldo").val(arrayData[0].saldo);
                     $(All.get_active_tab() + " #amtUmroh").focus();
                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
                } else {
                    $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
                     alert('RegisterNo Salah');
                     $(All.get_active_tab() + " #idmember").attr('readonly', true);
                     $(All.get_active_tab() + " #nmmember").attr('readonly', true);
                     $(All.get_active_tab() + " #pktUmroh").attr('readonly', true);
                     $(All.get_active_tab() + " #saldo").attr('readonly', true);
                    
                     $(All.get_active_tab() + " #regnos").focus();
                }
            } 
        });
    },
    
    saveInstallment : function(){
		document.getElementById("regnose").value = document.getElementById("regnos").value;
        All.set_disable_button();
        $(All.get_active_tab() + " #result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/save/installTrf") , $(All.get_active_tab() + " #formCicilan").serialize(), function(hasil)
            {  
                
                All.set_enable_button();
                $(All.get_active_tab() + " input[type=text]").val(null);
                $(All.get_active_tab() + " #result").html(null);
                $(All.get_active_tab() + " #result").html("<div class='alert alert-error' align=center>"+hasil.message+"</div>");   
				if(hasil.response=='true'){
				$('#kwprint').show();		
				}
				
                //All.set_error_message(' #result',hasil.message);
            }
			,"json"
			);
			

    },
    
    getListInstall : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #listInstallUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/report/installment/act") , $(All.get_active_tab() + " #frmReportInstall").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listInstallUmroh").html(null);
                $(All.get_active_tab() + " #listInstallUmroh").html(hasil);   
            });
    },

	getCancel : function(){
		All.set_disable_button();
		$(All.get_active_tab() + " #formListCancel").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		$.post(All.get_url("be_umroh/cancel/act") , $(All.get_active_tab() + " #formCancelKSmart").serialize(), function(hasil)
		{
			All.set_enable_button();
			$(All.get_active_tab() + " #formListCancel").html(null);
			$(All.get_active_tab() + " #formListCancel").html(hasil);
		});
	},
	
	getListPost : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " .result").html(null);
        $(All.get_active_tab() + " .result").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
        $(All.get_active_tab() + " .nextForm1").hide();
        $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.post(All.get_url("be_umroh/post/act") , $(All.get_active_tab() + " #frmReportPost").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        });
    },
     
    getUpdatePostPrev : function(){
        All.set_disable_button();
        //$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		var atLeastOneIsChecked = $(All.get_active_tab() + ' input[name="registerno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			All.set_enable_button();
			alert("Please select at least one transaction..");
			return false;
		} 
		
        $(All.get_active_tab() + " .result").hide();
        $(All.get_active_tab() + " .nextForm1").show();
        $.post(All.get_url("be_umroh/post/prev") , $(All.get_active_tab() + " #frmReportPost").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .nextForm1").html(null);
            $(All.get_active_tab() + " .nextForm1").html(hasil);   
        });
    },
       
    getUpdatePost : function(){
        All.set_disable_button();
		$(All.get_active_tab() + " .nextForm1").hide();
		$.post(All.get_url("be_umroh/post/upd"), $(All.get_active_tab() + " #frmReportPostPrev").serialize(), function(hasil) {
			be_umroh.getListPost();
			//alert('masuk');
			//All.clear_div_in_boxcontent(".formUpdateConote .hasil");
			/*
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").append(rowshtml);*/
			//$(All.get_box_content() + ".formUpdateConote .hasil").append(rowshtml);
		});
    },
    
    
     getSMSBCPostPrev : function(){
        All.set_disable_button();
        //$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
		var atLeastOneIsChecked = $(All.get_active_tab() + ' input[name="registerno[]"]:checkbox:checked').length;
		if(atLeastOneIsChecked  < 1) {
			All.set_enable_button();
			alert("Please select at least one transaction..");
			return false;
		} 
		
        $(All.get_active_tab() + " .result").hide();
        $(All.get_active_tab() + " .nextForm1").show();
        $.post(All.get_url("be_umroh/smsbcpost/prev") , $(All.get_active_tab() + " #formListSMSPost").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .nextForm1").html(null);
            $(All.get_active_tab() + " .nextForm1").html(hasil);   
        });
    },
       
    getSMSBCPost : function(){
        All.set_disable_button();
		$(All.get_active_tab() + " .nextForm1").hide();
		$.post(All.get_url("be_umroh/post/upd"), $(All.get_active_tab() + " #frmReportPostPrev").serialize(), function(hasil) {
			be_umroh.getListPost();
			//alert('masuk');
			//All.clear_div_in_boxcontent(".formUpdateConote .hasil");
			/*
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").append(rowshtml);*/
			//$(All.get_box_content() + ".formUpdateConote .hasil").append(rowshtml);
		});
    },
    
	getSMSBCListPost : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " .result").html(null);
        $(All.get_active_tab() + " .result").show();
        $(All.get_active_tab() + " .nextForm1").html(null);
        $(All.get_active_tab() + " .nextForm1").hide();
        $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.post(All.get_url("be_umroh/smsbcpost/act") , $(All.get_active_tab() + " #formListSMSPost").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        });
    },
     
    getSMSBCUpdatePost : function(){
        All.set_disable_button();
		$(All.get_active_tab() + " .nextForm1").hide();
		$.post(All.get_url("be_umroh/post/upd"), $(All.get_active_tab() + " #frmReportPostPrev").serialize(), function(hasil) {
			be_umroh.getListPost();
			//alert('masuk');
			//All.clear_div_in_boxcontent(".formUpdateConote .hasil");
			/*
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(null);
			$(All.get_active_tab() + " .result").append(rowshtml);*/
			//$(All.get_box_content() + ".formUpdateConote .hasil").append(rowshtml);
		});
    },
    
	getListMutasi : function(){
		All.set_disable_button();
            $(All.get_active_tab() + " #listMutasiUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/mutasi/action") , $(All.get_active_tab() + " #frmMut").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listMutasiUmroh").html(null);
                $(All.get_active_tab() + " #listMutasiUmroh").html(hasil);   
            });
	},
	
	getListReg : function() {
		All.set_disable_button();
        $(All.get_active_tab() + " #listInstallUmroh").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/report/installment/act") , $(All.get_active_tab() + " #frmReportInstall").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listInstallUmroh").html(null);
                $(All.get_active_tab() + " #listInstallUmroh").html(hasil);   
            });
	},
	
	getListUmrToWeb  : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #listUmrohToWeb").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_umroh/listUpload/journey/act") , $(All.get_active_tab() + " #frmReportToWeb").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listUmrohToWeb").html(null);
                $(All.get_active_tab() + " #listUmrohToWeb").html(hasil);   
            });
    },
	
	
	saveUploadMT  : function() {
	   var formData = new FormData($(All.get_active_tab() + " #formUploadMT940")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('be_umroh/save_upload'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
	},

	getProsesCancel : function(param) {
		//($(All.get_active_tab() + ' #qty' +frm).val());
		var dfno = $("#dfno" +param).val();
		//var date1 = $("#from" +param).val();
		//var date2 = $("#to" +param).val();
		//var date1 = $(All.get_active_tab() + " #from").val();
		//var date2 = $(All.get_active_tab() + " #to").val();
		//console.log("newidstk : " +newidstk+ ", orderno : " +orderno);
		All.set_disable_button();
		$.ajax({
			url : All.get_url('be_umroh/cancel/act2'),
			type : 'POST',
			data : {dfno : dfno},

			success : function(data) {

				All.set_enable_button();
				$(All.get_active_tab() + " .mainForm").hide();
				$(All.get_active_tab() + " .nextForm1").html(null);
				$(All.get_active_tab() + " .nextForm1").html(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	},

	sendSMSBC  : function() {
		var formData = new FormData($(All.get_active_tab() + " #frmReportPostPrev")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('be_umroh/smsbcpost/send'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				alert("SMS Dikirim!");
				All.set_enable_button();
				/*All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);*/

			},
			cache : false,
			contentType : false,
			processData : false
		});
	},
    
}

jQuery(document).ready(function($){
    if($(All.get_active_tab() + " #formUmroh").length) {
        var thnlhrOri = $('<select class="hide"></select>');
        var thnlhrOpt = $(All.get_active_tab() + " thnlhr option").clone();
        thnlhrOri.append(thnlhrOpt).insertAfter(All.get_active_tab() + ' #thnlhr');
        console.log("dokumen load..");
        be_umroh.getTipeJamaah();
        $(All.get_active_tab() + ' #email').blur(function(){
            var strValid = be_umroh.isValidEmailAddress($(All.get_active_tab() + ' #email').val());
            if(strValid === false) {
                alert('Alamat email salah');
                $(All.get_active_tab() + ' #email').focus();
            }
        });
		
    }
});
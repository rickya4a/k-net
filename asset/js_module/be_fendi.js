var $ = jQuery;

var be_fendi = {
    data: {
        totalbukanvoucher: 0,
        totalvoucher: 0,
        kembalian: 0,
        cashDouble: 0
    },

    ShowDetailNextForm : function(urlx) {
        All.set_disable_button();
        $.ajax({
            url: All.get_url(urlx),
            type: 'POST',
            success:
                function(data){
                    All.set_enable_button();
                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");
                    $(All.get_active_tab() + " .nextForm1").html(data);
                },
            error: function(jqXHR, textStatus, errorThrown) {
                All.set_enable_button();
            }
        });
    },

    ShowDetailNextForm2 : function(urlx) {
        All.set_disable_button();
        $.ajax({
            url: All.get_url(urlx),
            type: 'GET',
            success:
                function(data){
                    All.set_enable_button();
                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");
                    $(All.get_active_tab() + " .nextForm1").html(data);
                },
            error: function(jqXHR, textStatus, errorThrown) {
                All.set_enable_button();
            }
        });
    },

    ajaxShowDetailonNextForm4 : function() {

        //var dfno = $(All.get_active_tab() + " #idmember").val();
        var idnumber = $("#idnumber").serialize();

        All.set_disable_button();
        $.ajax({
            url : All.get_url('be/report/prevSave'),
            type : 'POST',
            data : {idnumber : idnumber},

            success : function(data) {

                All.set_enable_button();
                $(All.get_active_tab() + " .mainForm").hide();
                $(All.get_active_tab() + " .nextForm1").html(null);
                $(All.get_active_tab() + " .nextForm1").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }   
        });
    },

    formPost : function() {

        //$.post(All.get_url('');
        //var idnumber = $("#idmemb").serialize();
        var isi = $(All.get_active_tab() + " #formTL").serialize();


        //console.log("isinya : "+idnumber);
        All.set_disable_button();
        $.ajax({
            url : All.get_url('be/report/prevSave'),
            type : 'POST',
            data : isi,

            success : function(data) {

                All.set_enable_button();
                $(All.get_active_tab() + " .mainForm").hide();
                $(All.get_active_tab() + " .nextForm1").html(null);
                $(All.get_active_tab() + " .nextForm1").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    formPost2 : function() {

        var arrData=[];

        // loop over each table row (tr)
        $("#attrTable tr").each(function(){

            var currentRow=$(this);

            var col1_value=currentRow.find("td:eq(0)").text();
            var col2_value=currentRow.find("td:eq(1)").text();
            var col3_value=currentRow.find("td:eq(2)").text();
            var col4_value=currentRow.find("td:eq(3) input").val();
            var col5_value=currentRow.find("td:eq(4) input").val();
            var col6_value=currentRow.find("td:eq(5)").text();

            console.log(col6_value);
            //var obj={};
            //obj.col1=col1_value;
            //obj.col2=col2_value;
            //obj.col3=col3_value;
            //obj.col4=col4_value;
            //obj.col5=col5_value;
            //obj.col6=col6_value;
            //
            //arrData.push(obj);



                var obj = {

                    "col6" : col6_value

                };

                queryStr = { "obj" : obj };
                arrData.push(queryStr);





        });

        //console.log(arrData);

        //$.ajax({
        //
        //    url : All.get_url('be/report/insertDetail'),
        //    type : 'POST',
        //    data : arrData,
        //
        //    success : function(data) {
        //
        //        All.set_enable_button();
        //        $(All.get_active_tab() + " .nextForm1").hide();
        //        $(All.get_active_tab() + " .nextForm2").html(null);
        //        $(All.get_active_tab() + " .nextForm2").html(data);
        //    },
        //    error: function (xhr, ajaxOptions, thrownError) {
        //        alert(thrownError + ':' +xhr.status);
        //        All.set_enable_button();
        //    }
        //});

    },

    formPost3 : function() {

        //$.post(All.get_url('');
        //var idmember = $("#idmember").serialize();
        var isi = $(All.get_active_tab() + " #formPrevSave").serialize();


        console.log("isinya : "+isi);
        All.set_disable_button();
        $.ajax({
            url : All.get_url('be/report/insertDetail'),
            type : 'POST',
            data : isi,

            success : function(data) {

                All.set_enable_button();
                $(All.get_active_tab() + " .mainForm1").hide();
                $(All.get_active_tab() + " .nextForm2").html(null);
                $(All.get_active_tab() + " .nextForm2").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    getAllValueTable : function(){

        var ary = [];
        $(function () {
            $('.attrTable tr').each(function (a, b) {
                var name = $('.attrName', b).text();
                var value = $('.attrValue', b).text();
                ary.push({ Name: name, Value: value });

            });
            alert(JSON.stringify( ary));
        });
    },

    getListTimorLeste : function(){

        All.set_disable_button();
        All.get_image_load2();

        $.post(All.get_url('be/report/getDetailTimor'), $(All.get_active_tab() + " #formTL").serialize(), function(data) {

            All.set_enable_button();
            All.clear_div_in_boxcontent(".mainForm .result");

            if (data.response == "false") {

                All.set_error_message(" .result", "No record found..!!");

            } else {
                var arrayData = data.arrayData;

                var rowshtml = "";

                rowshtml += "<table width='100%' class='table table-striped table-bordered bootstrap-datatable datatable'>";
                rowshtml += "<thead><tr><th colspan='10' bgcolor='#lightgrey'>List Bonus TimorLeste </th></tr>";
                rowshtml += "<tr bgcolor='#f4f4f4'>";
                rowshtml += "<th width='5%'><input type='checkbox' onclick='All.checkUncheckAll(this)' name='checkall' /></th>";
                rowshtml += "<th width='5%'>No</th>";
                rowshtml += "<th>ID Member</th>";
                rowshtml += "<th>Nama Member</th>";
                rowshtml += "<th>Total Bonus</th>";
                rowshtml += "<th>Status</th>";
                rowshtml += "<th>Tanggal Transfer</th>";
                rowshtml += "<th>No. Akun</th>";
                rowshtml += "<th>Bank</th>";
                rowshtml += "<th>Nama Akun</th>";
                rowshtml += "</tr></thead>";
                rowshtml += "<tbody>";
                $.each(arrayData, function(key, value) {
                    rowshtml += "<tr><td align=center><input type=checkbox id=pil" + (key + 1) + " id=idmemb name=idmemb[] value=" + value.idnumber + " /></td>";
                    rowshtml += "<td align=center>" + (key + 1) + "&nbsp;</td>";
                    rowshtml += "<td align=center><a href = \"#\" id=" +value.idnumber+ " onclick=\"be_fendi.ShowDetailNextForm('be/report/timor_leste/detail/" +value.idnumber + "/p')\" >" +value.idnumber+ "</td>";
                    rowshtml += "<td align=center>" + value.fullnm + "</td>";
                    rowshtml += "<td align=center>" + value.NETT + "</td>";
                    rowshtml += "<td align=center>" + value.status + "</td>";
                    if(value.tgl_trf == null){
                        rowshtml += "<td align=center> - </td>";
                    }else{
                        rowshtml += "<td align=center>" + value.tgl_trf + "</td>";
                    }
                    rowshtml += "<td align=center>" + value.accountno + "</td>";
                    rowshtml += "<td align=center>" + value.bankid + "</td>";
                    rowshtml += "<td align=center>" + value.accountname + "</td></tr>";

                });

                rowshtml += "</tbody>";

                rowshtml += "</table>";
                //rowshtml += "<input type='submit' class='btn btn-small btn-success' value='UPDATE' />";
                rowshtml += "<input type=\"button\" id=\"btn_input_user\" class=\"btn btn-primary .submit\" name=\"save\" value=\"UPDATE\" onclick=\"be_fendi.formPost()\" />";
                $(All.get_box_content() + ".mainForm .result").append(rowshtml);
                All.set_datatable();
            }

        }, "json").fail(function() {

            alert("No Result Found!!!");
            All.reload_page('be/report/timor_leste');
            All.set_enable_button();
        });
    },

    formPostSiCepat : function() {

        //$.post(All.get_url('');
        //var idmember = $("#idmember").serialize();
        var isi = $(All.get_active_tab() + " #formNoResi").serialize();


        console.log("isinya : "+isi);

        //All.set_disable_button();
        //$.ajax({
        //    url : All.get_url('be/report/insertDetail'),
        //    type : 'POST',
        //    data : isi,
        //
        //    success : function(data) {
        //
        //        All.set_enable_button();
        //        $(All.get_active_tab() + " .mainForm1").hide();
        //        $(All.get_active_tab() + " .nextForm2").html(null);
        //        $(All.get_active_tab() + " .nextForm2").html(data);
        //    },
        //    error: function (xhr, ajaxOptions, thrownError) {
        //        alert(thrownError + ':' +xhr.status);
        //        All.set_enable_button();
        //    }
        //});
    },

    pilihHadiah : function() {
        var nilai = $("#free_product").val();
        var res = nilai.split("|");
        $("#free_prdcd").val(res[0]);
        $("#free_prdnm").val(res[1]);
        $("#free_qty").val(res[2]);
        $("#free_bv").val(res[3]);
        $("#free_westPrice").val(res[4]);
        $("#free_eastPrice").val(res[5]);
        $("#free_weight").val(res[6]);
    }

}

var be_voucher = {

    getDetailVch: function (param) {
        var vch_no = param.id;
        //console.log('Hai'+ vch_no);

        $.ajax({
            url: All.get_url('voucher_list/id/') + vch_no,
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                console.log('test');

                var header = data.header;
                var detail = data.detail;
                var payment = data.payment;
                var shipping = data.shipping;

                if (header != null && detail != null && payment != null && shipping != null) {

                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");

                    var rowshtml = "";
                    //var back = "All.back_to_form("'.nextForm1'","' .mainForm'")";

                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=4>TRANSACTION DATA</th></tr>";
                    rowshtml += "<tr><td width=20%>Voucher No</td><td>" + header[0].vch_no + "</td></tr>";
                    rowshtml += "<tr><td>ID Member</td><td>" + header[0].dfno + " </td></tr>";
                    rowshtml += "<tr><td>Nama</td><td>" + header[0].nama + " </td></tr>";
                    rowshtml += "<tr><td>Email</td><td>" + header[0].email + " </td></tr>";
                    rowshtml += "<tr><td>Telp</td><td>" + header[0].no_telp + " </td></tr>";
                    rowshtml += "<tr><td>Status</td><td>" + header[0].status + " </td></tr>";

                    rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type=button value='<< Back' onclick=be_trans.back_to_form() /></td><td>&nbsp;</td></tr>";
                    rowshtml += "</table>";


                    // $(All.get_box_content() + ".nextForm1").append(rowshtml);

                    //table detail product

                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=8>DETAIL PRODUCT</th></tr>";
                    rowshtml += "<tr><th width=5%>No</th><th width=15%>Product Code</th><th>Product Name</th><th width=8%>Harga</th><th width=10%>Qty</th></tr>";

                    $.each(detail, function (key, value) {
                        //rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
                        rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
                        rowshtml += "<td align=center>" + value.prdcd + "</a></td>";
                        rowshtml += "<td>" + value.prdnm + "</td>";
                        rowshtml += "<td align=right>" + All.num(parseInt(value.dpr)) + "</td>";
                        rowshtml += "<td align=right>" + All.num(parseInt(value.qty)) + "</td>";

                    });

                    //rowshtml += "<tr><td colspan=4>&nbsp;</td><td align=right>" + All.num(parseInt(value.tot_pay)) + "</td></tr>";
                    rowshtml += "</table>";

                    //table detail payment
                    var tot_pay = 0;
                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=8>DETAIL PAYMENT INFO</th></tr>";
                    rowshtml += "<tr><th width=5%>No</th><th width=15%>Payment Type</th><th>Date</th><th>Pay Amount</th></tr>";

                    $.each(payment, function (key, val) {
                        //rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
                        rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
                        rowshtml += "<td align=center>" + val.bankDesc + "</a></td>";
                        rowshtml += "<td align=center>" + val.datetrans + "</a></td>";
                        rowshtml += "<td align=right>" + All.num(parseInt(val.payamt)) + "</td>";
                        tot_pay += parseInt(val.payamt);
                    });
                    rowshtml += "<tr><td colspan=3 align=center><b>T O T A L</b></td><td align=right>" + All.num(parseInt(tot_pay)) + "</td></tr>";
                    rowshtml += "</table>";


                    //table detail shipping

                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=4>DETAIL SHIPPING</th></tr>";
                    rowshtml += "<tr><td width=20%>Jasa Pengiriman</td><td>JNE</td></tr>";
                    rowshtml += "<tr><td>Alamat </td><td>" + shipping[0].addr1 + " </td></tr>";
                    rowshtml += "<tr><td>Conote</td><td>" + shipping[0].conoteJNE + " </td></tr>";
                    if (shipping[0].delivery_status == 1) {
                        rowshtml += "<tr><td>Status</td><td>paid/processing</td></tr>";
                    }
                    else if (shipping[0].delivery_status == 2) {
                        rowshtml += "<tr><td>Status</td><td>packing</td></tr>";
                    }
                    else if (shipping[0].delivery_status == 3) {
                        rowshtml += "<tr><td>Status</td><td>delivered</td></tr>";
                    }
                    else if (shipping[0].delivery_status == 4) {
                        rowshtml += "<tr><td>Status</td><td>accepted</td></tr>";
                    }

                    //rowshtml += "<tr><td>Status</td><td>" + shipping[0].delivery_status + " </td></tr>";
                    rowshtml += "</table>";

                    rowshtml += "<br>";

                    $(All.get_box_content() + ".nextForm1").append(rowshtml);
                }

                else {
                    alert("No detail data found..!");
                }

            }
        });

    },


    //detail trx ppob (backend/reconcile)

    getDetailPPOB: function (param) {
        var orderid = param.id;

        $.ajax({
            url: All.get_url('backend/transaction/getDetTrxXL/') + orderid,
            dataType: 'json',
            type: 'GET',
            success: function (data) {

                var hdr = data.hdr;
                var det = data.det;
                var pay = data.pay;
                var result = data.result;
                console.log(data);
                if (hdr != null) {
                    //if(hdr[0].ppob_type=='4'){
                    //ppob_type = 4 => XL Co branded Paket Data

                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");

                    var rowshtml = "";

                    //ppob_trx_hdr
                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=4>TRANSACTION DATA</th></tr>";
                    rowshtml += "<tr><td width=20%>Order No</td><td>" + hdr[0].trx_id + "</td></tr>";
                    rowshtml += "<tr><td>ID Member</td><td>" + hdr[0].memberid + " </td></tr>";
                    rowshtml += "<tr><td>Nama</td><td>" + hdr[0].membername + " </td></tr>";


                    //if ppob_type = 4 => XL Co branded Paket Data
                    if (hdr[0].ppob_type == '4') {
                        rowshtml += "<tr><td>Tipe Transaksi</td><td>" + hdr[0].nama_paket + " " + "</td></tr>";
                    }

                    //if ppob_type = 3 => PPOB Sgo
                    else if (hdr[0].ppob_type == '3') {
                        rowshtml += "<tr><td>Tipe Transaksi</td><td>" + hdr[0].product_name + " " + "</td></tr>";
                    }

                    rowshtml += "<tr><td>Nominal</td><td>" + hdr[0].nominal + " </td></tr>";
                    rowshtml += "<tr><td>Cashback Ori</td><td>" + hdr[0].cashback_ori + " </td></tr>";

                    rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type=button value='<< Back' onclick=be_trans.back_to_form() /></td><td>&nbsp;</td></tr>";
                    rowshtml += "</table>";

                    //ppob_trx_det
                    if (det == '') {
                        rowshtml += "<form method='post' name='formSaveTrx' id='formSaveTrx'>";
                        rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                        rowshtml += "<tr><th colspan=4>DETAIL TRANSACTION</th></tr>";
                        rowshtml += "<tr><td width=20%>Customer No</td><td><input type='hidden' name='trx_id' id='trx_id' class='form-control' value=" + hdr[0].trx_id + "><input type='text' id='cust_no' name='cust_no' class='form-control span20'></td></tr>";
                        rowshtml += "<tr><td>Reff Payment</td><td><input type='text' name='reff_pay' class='form-control'></td></tr>";
                        rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type='button' value='Update' onclick=All.ajaxFormPost('formSaveTrx','backend/transaction/upDetTrans')></td><td>&nbsp;</td></tr>";

                        rowshtml += "</form>";

                        rowshtml += "</table>";
                    }
                    else {

                        rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                        rowshtml += "<tr><th colspan=4>DETAIL TRANSACTION</th></tr>";
                        rowshtml += "<tr><td width=20%>Customer No</td><td>" + det[0].cust_no + "</td></tr>";
                        rowshtml += "<tr><td>Reff Payment</td><td>" + det[0].reff_pay_id + " </td></tr>";

                        rowshtml += "</table>";
                    }


                    //ppob_trx_pay
                    if (pay == '') {
                        rowshtml += "<form method='post' name='formSavePay' id='formSavePay'>";
                        rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                        rowshtml += "<tr><th colspan=4>PAYMENT</th></tr>";
                        rowshtml += "<tr><td width=20%>Bank</td><td><input type='hidden' name='trx_id' id='trx_id' class='form-control' value=" + hdr[0].trx_id + ">";
                        rowshtml += "<select name='type_bank' id='type_bank' onchange=selectBank()>";
                        rowshtml += "<option value=''>--Pilih--</option>";
                        $.each(result, function (key, value) {
                            //get list bank for an option
                            rowshtml += "<option value='" + value.bankCode + "'>" + value.bankDisplayNm + "</option>";

                        });


                        rowshtml += "</select></td></tr>";
                        rowshtml += "<tr><td>Charge Admin</td><td><input type='text' name='charge_adm' id='charge_adm' class='form-control' readonly></td></tr>";
                        rowshtml += "<input type='hidden' name='bank_code' id='bank_code'>";
                        rowshtml += "<tr><td>Bank Display Name</td><td><input type='text' name='bank_name' id='bank_name' class='form-control' readonly></td></tr>";
                        rowshtml += "<tr><td>Pay Amount</td><td><input type='text' name='payamt' class='form-control' readonly value=" + hdr[0].nominal + " ></td></tr>";
                        rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type='button' value='Update' onclick=All.ajaxFormPost('formSavePay','backend/transaction/upDetPay')></td><td>&nbsp;</td></tr>";

                        rowshtml += "</form>";

                        rowshtml += "</table>";
                    }
                    else {

                        rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                        rowshtml += "<tr><th colspan=4>PAYMENT</th></tr>";
                        rowshtml += "<tr><td width=20%>Bank Code Payment</td><td>" + pay[0].bank_code_payment + "</td></tr>";
                        rowshtml += "<tr><td>Charge Admin</td><td>" + pay[0].charge_admin + " </td></tr>";
                        rowshtml += "<tr><td>Pay Amount</td><td>" + pay[0].payamt + " </td></tr>";
                        rowshtml += "<tr><td>Bank Description</td><td>" + pay[0].bankDesc + " </td></tr>";
                        rowshtml += "<tr><td>Bank Display Name</td><td>" + pay[0].bankDisplayNm + " </td></tr>";

                        rowshtml += "</table>";
                    }

                    rowshtml += "</table>";

                    rowshtml += "<br>";

                    $(All.get_box_content() + ".nextForm1").append(rowshtml);

                    //}


                }//closing tag main if

                else {
                    alert("No detail data found..!");
                }

            }
        });


    },

    getDetailHifi: function (param) {
        var trx_id = param.id;
        console.log('Hai'+ trx_id);
        //alert('test pay kmart');

        $.ajax({
            url: All.get_url('hifi/trx/id/') + trx_id,
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                console.log('test');

                var header = data.header;
                var det = data.det;
                var pay = data.payment;
                //var shipping = data.shipping;

                if (header != null || det !=null) {

                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");

                    var rowshtml = "";
                    //var back = "All.back_to_form("'.nextForm1'","' .mainForm'")";

                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=4>TRANSACTION DATA</th></tr>";
                    rowshtml += "<tr><td width=20%>Transaction ID</td><td>" + header[0].trx_id + "</td></tr>";
                    rowshtml += "<tr><td>ID Member</td><td>" + header[0].memberid + " </td></tr>";
                    rowshtml += "<tr><td>Nama Member</td><td>" + header[0].membername + " </td></tr>";
                    rowshtml += "<tr><td>No. Hp</td><td>" + header[0].member_no_hp + " </td></tr>";
                    rowshtml += "<tr><td>Tanggal</td><td>" + header[0].createdt + " </td></tr>";
                    //if ppob_type = 1 => Jatis and ppob_type = 2 => XL Co branded SIM Card
                    if (header[0].ppob_type == '1' || header[0].ppob_type == '2') {
                        rowshtml += "<tr><td>Tipe Transaksi</td><td>" + header[0].trx_type + " </td></tr>";
                    }
                    //if ppob_type = 3 => PPOB Sgo
                    else if (header[0].ppob_type == '3') {
                        rowshtml += "<tr><td>Tipe Transaksi</td><td>" + header[0].product_name + " " + "</td></tr>";
                    }
                    //if ppob_type = 4 => XL Co branded Paket Data
                    else if (header[0].ppob_type == '4') {
                        rowshtml += "<tr><td>Tipe Transaksi</td><td>" + header[0].nama_paket + " " + "</td></tr>";
                    }

                    rowshtml += "<tr><td>Nominal</td><td>" + header[0].nominal + " </td></tr>";
                    rowshtml += "<tr><td>Cashback Ori</td><td>" + header[0].cashback_ori + " </td></tr>";

                    rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type=button value='<< Back' onclick=be_trans.back_to_form() /></td><td>&nbsp;</td></tr>";
                    rowshtml += "</table>";


                    //table detail product

                    rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                    rowshtml += "<tr><th colspan=4>DETAIL TRANSACTION</th></tr>";
                    rowshtml += "<tr><td width=20%>Customer No</td><td>" + det[0].cust_no + "</td></tr>";
                    rowshtml += "<tr><td>Reff Payment</td><td>" + det[0].reff_pay_id + " </td></tr>";

                    rowshtml += "</table>";

                    //payment
					if (header[0].ppob_type != '1') {
                        //get prefix of trx_id first
						var trxid = header[0].trx_id;
                        var res = trxid.substring(0, 2);
                        //console.log(res);

						if(header[0].ppob_type == '4' && res == 'KX') {
                            //alert('payment k-mart');

                            rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                            rowshtml += "<tr><th colspan=4>PAYMENT ON K-MART</th></tr>";
                            rowshtml += "<tr><td width=20%>Kode Pembayaran</td><td>" + pay[0].cashtype + "</td></tr>";
                            rowshtml += "<tr><td>Tipe Pembayaran</td><td>" + pay[0].cashnm + " </td></tr>";
                            rowshtml += "<tr><td>Kasir</td><td>" + pay[0].createNM + " </td></tr>";

                            rowshtml += "</table>";
						}
                        else if(header[0].ppob_type == '4' && res != 'KX') {
							//alert('bukan');

                            rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                            rowshtml += "<tr><th colspan=4>PAYMENT</th></tr>";
                            rowshtml += "<tr><td width=20%>Kode Bank</td><td>" + pay[0].bank_code_payment + "</td></tr>";
                            rowshtml += "<tr><td>Charge Admin</td><td>" + pay[0].charge_admin + " </td></tr>";
                            rowshtml += "<tr><td>Pay Amount</td><td>" + pay[0].payamt + " </td></tr>";
                            rowshtml += "<tr><td>Bank Description</td><td>" + pay[0].bankDesc + " </td></tr>";
                            rowshtml += "<tr><td>Bank Display Name</td><td>" + pay[0].bankDisplayNm + " </td></tr>";

                            rowshtml += "</table>";

                        }
                        else if(header[0].ppob_type == '3'){
                            rowshtml += "<table class='table table-striped table-bordered' width=100%>";
                            rowshtml += "<tr><th colspan=4>PAYMENT</th></tr>";
                            rowshtml += "<tr><td width=20%>Kode Bank</td><td>" + pay[0].bank_code_payment + "</td></tr>";
                            rowshtml += "<tr><td>Charge Admin</td><td>" + pay[0].charge_admin + " </td></tr>";
                            rowshtml += "<tr><td>Pay Amount</td><td>" + pay[0].payamt + " </td></tr>";
                            rowshtml += "<tr><td>Bank Description</td><td>" + pay[0].bankDesc + " </td></tr>";
                            rowshtml += "<tr><td>Bank Display Name</td><td>" + pay[0].bankDisplayNm + " </td></tr>";

                            rowshtml += "</table>";

                        }
					}
                    rowshtml += "<br>";



                    $(All.get_box_content() + ".nextForm1").append(rowshtml);
                }

                else {
                    alert("No detail data found..!");
                }

            }
        });

    },

    getListVaTrx: function (param) {
        $.ajax({
            //url: All.get_url('sgo_ppob/saveTemp'),
            url: All.get_url('va/history/trx/list'),
            type: 'POST',
            data: $("#listVaTrx").serialize(),
            dataType: 'json',
            success:
                function(data){
                    $("input[type=button]").removeAttr('disabled');
                    if(data.response == 'true') {
                        var arrayData = data.arrayData;
                        var xhtml = "";
                        $.each(arrayData, function(key, value) {
                            var link = "javascipt:getDetailTrxPpobById('"+value.trcd+"')";
                            xhtml += "<tr><td>"+(key+1)+"</td>";
                            xhtml += "<td>"+value.trcd+"</td>";
                            xhtml += "<td>"+value.shortdescription+"</td>";
                            xhtml += "<td>"+value.tipe_dk+"</td>";
                            xhtml += "<td>"+All.num(parseInt(value.amount))+"</td>";
                            xhtml += "<td>"+value.createdt+"</td>";
                            xhtml += "<td><a class='btn btn-mini btn-success' id='"+value.trcd+"' onclick=getDetailTrxPpobById(this)>Detail</a></td></tr>";
                        });

                        $("#listTrx").html(null);
                        $("#listTrx").html(xhtml);
                    } else {
                        alert(data.message);
                        $("#listTrx").html(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                $("input[type=button]").removeAttr('disabled');
            }
        });//closing ajax
    },

    listProvinsi : function(idx) {
        All.set_disable_button();
        var txt = $("#" + idx.id + " :selected").text();
        //$(setToDiv).attr("disabled", "disabled");
        console.log(txt);
        if (idx.value == "") {
            alert("Silahkan pilih Ekspedisi/Cargo dahulu");
        } else {
            $.ajax({
                url: All.get_url("cargo/province/list/") + idx.value,
                type: 'GET',
                dataType: 'json',
                success: function (data) {


                    //$("#provDiv").html(null);
                    //$("#kabDiv").html(null);
                    //$("#kecDiv").html(null);
                    All.set_enable_button();
                    if (data.response == "true") {
                        var arrayData = data.arrayData;

                        if (idx.value == "1") {
                            $("#provinsi").html(null);
                            //$("#KecDiv").css("display", "block");
                            //$("#kota").html(null);
                            //$("#kecamatan").html(null);
                            //$("#stockistref").html(null);
                            var rowhtml = "<option value=''>--Select Here--</option>";
                            //rowhtml += "<span>Provinsi<label><font color=red>&nbsp;*</font></label></span>";
                            //rowhtml += "<select class='form-list' name='provinsi' id='provinsi' onchange=Shopping.show_kota(this,'#kota') >";
                            //rowhtml += "<option value=''>--Select Here--</option>";
                            $.each(arrayData, function (key, value) {
                                rowhtml += "<option value=" + value.kode_provinsi + ">" + value.provinsi + "</option>";

                            });
                            $("#provinsi").append(rowhtml);

                        } else if (idx.value == "2") {
                            var rowhtml = "<option value=''>--Select Here--</option>";
                            $("#provinsi").html(null);
                            //$("#kota").html(null);
                            //$("#kecamatan").html(null);
                            //$("#stockistref").html(null);
                            $.each(arrayData, function (key, value) {
                                rowhtml += "<option value='" + value.provinsi + "'>" + value.provinsi + "</option>";

                            });
                            $("#provinsi").append(rowhtml);

                        }

                    } else {
                        //alert(data.message)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' + xhr.status);
                    All.set_enable_button();
                }
            });
        }
    },

    getDetailTrx2 : function(idx) {
        var token = idx.id;
        console.log('2');
        $.ajax({
            url: All.get_url('trans/id2/') + token,
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                if (data.response == "true") {
                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");
                    console.log('true');
                    var detail = data.detail;
                    var idmemb = detail[0].id_memb;
                    var nmmemb = detail[0].nmmember;
                    var idstk = detail[0].idstk;
                    var nmstk = detail[0].nmstkk;
                    var disc = detail[0].discount_shipping;
                    var bonus = detail[0].bonusmonth;
                    console.log(nmmemb);

                    if(idmemb != null){
                        var idmemb2 = idmemb;
                    }
                    else{
                        var idmemb2 = '';
                    }

                    if(nmmemb != null){
                        var nmmemb2 = nmmemb;
                    }
                    else{
                        var nmmemb2 = '';
                    }

                    if(idstk != null){
                        var idstk2 = idstk;
                    }
                    else{
                        var idstk2 = '';
                    }

                    if(nmstk != null){
                        var nmstk3 = nmstk.split(" - ");
                        //console.log(nmstk3[1]);
                        var nmstk2 = nmstk3[1];
                    }
                    else if(nmstk == '-'){
                        var nmstk2 = '';
                    }
                    else{
                        var nmstk2 = '';
                    }
                    if(disc != null){
                        var disc2 = disc;
                    }
                    else{
                        var disc2 = '';
                    }
                    if(bonus != null){
                        var bonus2 = bonus;
                    }
                    else{
                        var bonus2 = '';
                    }

                    var rowshtml = "<form method='post' id='formSave'>";
                    rowshtml += "<table width=80% align='center'>";
                    rowshtml += "<tr><th colspan=3>TRANSACTION DATA</th></tr>";
                    rowshtml += "<tr><br></tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>No.Token</td>";
                    //rowshtml += "<input type='hidden' id='notrx' name='notrx' value='"+header[0].orderno+"' />";
                    rowshtml += "<td width='30%'><input type='text' class='span8' id='token' name='token' value='"+detail[0].token+"' readonly/></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>ID Member</td>";
                    rowshtml += "<td width='30%'><input type='text' class='span8' id='idmemb' name='idmemb' onchange=\"All.getFullNameByID(this.value,'be/memberinfo/msmemb/dfno','#nmmemb')\" value='"+idmemb2+"'/></td>";
                    rowshtml += "<td align='left' width='30%'><input type='text' id='nmmemb' class='span12' name='nmmemb' readonly value='"+nmmemb2+"'/></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>ID Stokist</td>";
                    rowshtml += "<td width='30%'><input type='text' id='idstk' class='span8' name='idstk' onchange=\"All.getFullNameByID(this.value,'be/memberinfo/mssc/loccd','#nmstk')\" value='"+idstk2+"'/></td>";
                    rowshtml += "<td align='left' width='30%'><input type='text' id='nmstk' name='nmstk' class='span12' readonly value='"+nmstk2+"'/></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>Send To</td>";
                    rowshtml += "<td width='30%'><select name='sent_to' id='sent_to' class='span8'><option value='1'>Stockist</option><option value='2'>Alamat</option></select></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>Discount Shipping</td>";
                    rowshtml += "<td width='30%'><input type='text' id='disc' class='span8' name='disc' value='"+disc2+"'/></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>Bonus Month</td>";
                    rowshtml += "<td width='30%'><input type='text' id='bonus' class='span8' name='bonus' value='"+bonus2+"'/></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%></td>";
                    rowshtml += "<td width='30%'><input type='button' class='btn btn-warning' value='BACK' onclick=\"be_trans.back_to_form()\"/>&nbsp;";
                    rowshtml += "<input type='button' class='btn btn-danger' value='SUBMIT' onclick=\"All.ajaxFormPost('formSave','backend/transaction/updateHdrSgo')\"/>&nbsp;";
                    rowshtml += "<input type='button' class='btn btn-info' value='RECONSILE' onclick=\"reconcileSgoTrx('"+detail[0].token+"')\"/></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "</form>";
                    $(All.get_box_content() + ".nextForm1").append(rowshtml);
                }
                else{
                    alert('Error');
                }
            }
        });
    },

    getDetailTrx2 : function(idx) {
        var token = idx.id;
        console.log('2');
        $.ajax({
            url: All.get_url('trans/id2/') + token,
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                if (data.response == "true") {
                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");
                    console.log('true');
                    var detail = data.detail;
                    var idmemb = detail[0].id_memb;
                    var nmmemb = detail[0].nmmember;
                    var idstk = detail[0].idstk;
                    var nmstk = detail[0].nmstkk;
                    var disc = detail[0].discount_shipping;
                    var bonus = detail[0].bonusmonth;
                    console.log(nmmemb);

                    if(idmemb != null){
                        var idmemb2 = idmemb;
                    }
                    else{
                        var idmemb2 = '';
                    }

                    if(nmmemb != null){
                        var nmmemb2 = nmmemb;
                    }
                    else{
                        var nmmemb2 = '';
                    }

                    if(idstk != null){
                        var idstk2 = idstk;
                    }
                    else{
                        var idstk2 = '';
                    }

                    if(nmstk != null){
                        var nmstk3 = nmstk.split(" - ");
                        //console.log(nmstk3[1]);
                        var nmstk2 = nmstk3[1];
                    }
                    else if(nmstk == '-'){
                        var nmstk2 = '';
                    }
                    else{
                        var nmstk2 = '';
                    }
                    if(disc != null){
                        var disc2 = disc;
                    }
                    else{
                        var disc2 = '';
                    }
                    if(bonus != null){
                        var bonus2 = bonus;
                    }
                    else{
                        var bonus2 = '';
                    }

                    var rowshtml = "<form method='post' id='formSave'>";
                    rowshtml += "<table width=80% align='center'>";
                    rowshtml += "<tr><th colspan=3>TRANSACTION DATA</th></tr>";
                    rowshtml += "<tr><br></tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>No.Token</td>";
                    //rowshtml += "<input type='hidden' id='notrx' name='notrx' value='"+header[0].orderno+"' />";
                    rowshtml += "<td width='30%'><input type='text' class='span8' id='token' name='token' value='"+detail[0].token+"' readonly/></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>ID Member</td>";
                    rowshtml += "<td width='30%'><input type='text' class='span8' id='idmemb' name='idmemb' onchange=\"All.getFullNameByID(this.value,'be/memberinfo/msmemb/dfno','#nmmemb')\" value='"+idmemb2+"'/></td>";
                    rowshtml += "<td align='left' width='30%'><input type='text' id='nmmemb' class='span12' name='nmmemb' readonly value='"+nmmemb2+"'/></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>ID Stokist</td>";
                    rowshtml += "<td width='30%'><input type='text' id='idstk' class='span8' name='idstk' onchange=\"All.getFullNameByID(this.value,'be/memberinfo/mssc/loccd','#nmstk')\" value='"+idstk2+"'/></td>";
                    rowshtml += "<td align='left' width='30%'><input type='text' id='nmstk' name='nmstk' class='span12' readonly value='"+nmstk2+"'/></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>Send To</td>";
                    rowshtml += "<td width='30%'><select name='sent_to' id='sent_to' class='span8'><option value='1'>Stockist</option><option value='2'>Alamat</option></select></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>Discount Shipping</td>";
                    rowshtml += "<td width='30%'><input type='text' id='disc' class='span8' name='disc' value='"+disc2+"'/></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%>Bonus Month</td>";
                    rowshtml += "<td width='30%'><input type='text' id='bonus' class='span8' name='bonus' value='"+bonus2+"'/></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "<tr>";
                    rowshtml += "<td width=20%></td>";
                    rowshtml += "<td width='30%'><input type='button' class='btn btn-warning' value='BACK' onclick=\"be_trans.back_to_form()\"/>&nbsp;";
                    rowshtml += "<input type='button' class='btn btn-danger' value='SUBMIT' onclick=\"All.ajaxFormPost('formSave','backend/transaction/updateHdrSgo')\"/>&nbsp;";
                    rowshtml += "<input type='button' class='btn btn-info' value='RECONSILE' onclick=\"reconcileSgoTrx('"+detail[0].token+"')\"/></td>";
                    rowshtml += "<td width='30%'></td>";
                    rowshtml += "</tr>";
                    rowshtml += "</form>";
                    $(All.get_box_content() + ".nextForm1").append(rowshtml);
                }
                else{
                    alert('Error');
                }
            }
        });
    },

    test2 : function(){
        alert('1');
    }


}//closing tag be_voucher

function selectBank(){
    alert('tes onchange5');

    if($('#type_bank').val() != '')
    {
        console.log($('#type_bank').val());

        $.ajax({
         type : "POST",
         url  : All.get_url('backend/transaction/getSelectBank'),
         dataType : 'json',
         data : {type_bank:$('#type_bank').val()},//what is this? ^-^
         success: function(data){
             $("#charge_adm").val(data.charge_adm);
             $("#bank_code").val(data.bank_code);
             $("#bank_name").val(data.bank_name);
         }
         });
    }

}

function test(){
    alert('test');
}

function reconcileSgoTrx(orderid) {
    //console.log(orderid);

    All.set_disable_button();
    $.ajax({
        url: All.get_url('api/sgo/renotif'),
        type: 'POST',
        data: {order_id: orderid},
        dataType: 'json',
        success:
            function(data){
                All.set_enable_button();
                alert(data.message);
            },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + ':' +xhr.status);
            All.set_enable_button();
        }
    });

}

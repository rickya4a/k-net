var $ = jQuery;

var be_promo = {
	checkIDC : function() {
		var memberid = $(All.get_active_tab() + " #idc_memb").val();
    	$.ajax({
            url: All.get_url("be/promo/idc/result/") +memberid,
            type: 'GET',
            success:
            function(data){
            	$(All.get_active_tab() + " .result").html(null);
            	$(All.get_active_tab() + " .result").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	showIDCDetail : function() {
		var memberid = $(All.get_active_tab() + " #idc_memb").val();
		var bnsperiod = $(All.get_active_tab() + " #bnsperiod").val();
    	$.ajax({
            url: All.get_url("be/promo/detail") ,
            type: 'POST',
            data : {memberid : memberid, bnsperiod : bnsperiod},
            success:
            function(data){
            	$(All.get_active_tab() + " .result2").html(null);
            	$(All.get_active_tab() + " .result2").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	checkPoinRewards : function() {
    	var memberid = $(All.get_active_tab() + " #memberid").val();
    	$.ajax({
            url: All.get_url("be/promo/rekrut/id/") +memberid,
            type: 'GET',
            success:
            function(data){
            	$(All.get_active_tab() + " .result").html(null);
            	$(All.get_active_tab() + " .result").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
   },
   
   checkPromoAnnivKolagen : function() {
   		var memberid = $(All.get_active_tab() + " #memberid").val();
    	$.ajax({
            url: All.get_url("be/promo/anniv_kolagen/id/") +memberid,
            type: 'GET',
            success:
            function(data){
            	$(All.get_active_tab() + " .result").html(null);
            	$(All.get_active_tab() + " .result").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
   },
   
   getUpdatePromoConfig : function(param) {
   		All.set_disable_button();
	   	$.ajax({
	            url: All.get_url("be/promo/config/id/") +param,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	            	All.set_enable_button();
	            	if(data.response == "true") {
	            		$(All.get_active_tab() + " #id").val(data.arrayData[0].id);
	            		$(All.get_active_tab() + " #name").val(data.arrayData[0].name);
	            		$(All.get_active_tab() + " #max_discount").val(data.arrayData[0].max_discount);
	            		$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
	            		$(All.get_active_tab() + " #period_start").val(data.arrayData[0].period_start);
	            		$(All.get_active_tab() + " #period_end").val(data.arrayData[0].period_end);
	  					All.formUpdateActivate();
	            	} else {
	            		alert(data.message);
	            		All.cancelUpdateForm();
	            	}
	            	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	    });
   },
   
   getUpdateBannerPromo : function(param) {
   		All.set_disable_button();
	   	$.ajax({
	            url: All.get_url("be/promo/banner/id/") +param,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	            	All.set_enable_button();
	            	if(data.response == "true") {
	            		$(All.get_active_tab() + " #id").val(data.arrayData[0].id);
	            		$(All.get_active_tab() + " #month").val(data.arrayData[0].month);
	            		$(All.get_active_tab() + " #year").val(data.arrayData[0].year);
	            		$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
	            		$(All.get_active_tab() + " #promo_name").val(data.arrayData[0].promo_name);
	            		//$(All.get_active_tab() + " #period_end").val(data.arrayData[0].period_end);
	  					All.formUpdateActivate();
	            	} else {
	            		alert(data.message);
	            		All.cancelUpdateForm();
	            	}
	            	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	    });
   },
   
   saveInputBannerPromo : function(url, formid, refreshList) {
   		All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);
    
        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {
                
                if(data.response == "false") {
                	All.set_error_message(" .mainForm .result", data.message);
	            } 
	            else {
	                All.set_success_message(" .mainForm .result", data.message);
					All.reset_all_input();
	            } 
	            
	            All.getListData(refreshList);
            },
            cache: false,
            contentType: false,
            processData: false
        });
   },
   /*
   checkStockist : function() {
   		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
            url: All.get_url(''),
            type: 'GET',
            data: formData,
            dataType: 'json',
            success: function (data) {
                All.set_enable_button();
	            	if(data.response == "true") {
	            		
                    } else {
	            		alert(data.message);
	            	}
            }
        });
   }*/
  
  getDataRedemp : function() {
  	    var param = $(All.get_active_tab() + " #idmember").val();
  	    var bnsperiod = $(All.get_active_tab() + " #bnsperiod").val();
  	    var pricecode = $(All.get_active_tab() + " #pricecode").val();
  	    All.set_disable_button();
	   	$.ajax({
	            url: All.get_url("be/promo/kolagen400bv/redemp/check"),
	            type: 'POST',
	            dataType: 'json',
	            data : {idmmember: param, bnsperiod : bnsperiod, pricecode : pricecode},
	            success:
	            function(data){
	            	
	            	if(data.response == "true" ) {
	            		/*var tbv = parseInt(data.member[0].tbv);
	            		if(tbv >= 450) {
	            			All.set_enable_button();
	            			var claim = Math.floor(tbv/450)*/
	            			$(All.get_active_tab() + " #total_remain").val(data.max_qty_redemp);
	            			$(All.get_active_tab() + " #membernm").val(data.member[0].fullnm);
	            			$(All.get_active_tab() + " #prdcd1").val(data.product[0].prdcd);
	            			$(All.get_active_tab() + " #prdnm1").val(data.product[0].prdnm);
	            			$(All.get_active_tab() + " #total_redemp").val(1);
	            			$(All.get_active_tab() + " #qty1").val(1);
	            			$(All.get_active_tab() + " #bv1").val(data.product[0].bv);
	            			$(All.get_active_tab() + " #dp1").val(data.product[0].dp);
	            			$(All.get_active_tab() + " #total_dp1").val(data.product[0].dp);
	            			$(All.get_active_tab() + " #tdp").val(data.product[0].dp);
	            			$(All.get_active_tab() + " #tbv").val(data.product[0].bv);
	            			$(All.get_active_tab() + " #nominal").val(data.product[0].dp);
	            			$(All.get_active_tab() + " #pbv").val(All.num(parseInt(data.member[0].tbv)));
	            			//$(All.get_active_tab() + " #prdcd1").removeAttr("readonly");
	            			
	            	    /*} else {
	            	   	   alert("BV member hanya " +tbv+ " kurang dari 400 BV..");
	            	   	   All.set_disable_button();
	            	   	   be_promo.setNull();
	            	    }*/
	            	} else {
	            		alert(data.message);
	            		All.set_disable_button();
	            		be_promo.setNull();
	            		
	            	}
	            	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	    });
  },
  
  setNull : function() {
  	$(All.get_active_tab() + " #total_remain").val(null);
	            		   $(All.get_active_tab() + " #membernm").val(null);
	            		   $(All.get_active_tab() + " #prdcd1").attr("readonly", "readonly");
	            		   $(All.get_active_tab() + " #prdcd1").val(null);
	            			$(All.get_active_tab() + " #prdnm1").val(null);
	            			$(All.get_active_tab() + " #qty1").val(null);
	            			$(All.get_active_tab() + " #bv1").val(null);
	            			$(All.get_active_tab() + " #dp1").val(null);
	            			$(All.get_active_tab() + " #total_dp1").val(null);
	            			$(All.get_active_tab() + " #tdp").val(null);
	            			$(All.get_active_tab() + " #tbv").val(null);
	            			$(All.get_active_tab() + " #nominal").val(null);
	            			$(All.get_active_tab() + " #pbv").val(null);
  },
  
  checkPrdMaxQty : function(param) {
  	var maxqty = parseInt($(All.get_active_tab() + " #total_remain").val());
  	var nilaiskr = parseInt($(All.get_active_tab() + " #qty" +param).val());
  	if(nilaiskr > maxqty) {
  		alert("Jumlah maksimum klaim produk adalah : "+maxqty);
  		$(All.get_active_tab() + " #total_dp" +param).val(null);
  		All.set_disable_button();
  	} else {
  		All.set_enable_button();
  		var dp = parseInt($(All.get_active_tab() + " #dp" +param).val());
  		var bv = parseInt($(All.get_active_tab() + " #bv" +param).val());
  		//alert("nilaiskr : " +nilaiskr);
  		var tot_dp = nilaiskr * dp;
  		var tot_bv = nilaiskr * bv;
  		$(All.get_active_tab() + " #total_redemp").val(nilaiskr);
  		$(All.get_active_tab() + " #total_dp" +param).val(tot_dp);
  		$(All.get_active_tab() + " #tdp").val(tot_dp);
  		$(All.get_active_tab() + " #tbv").val(tot_bv);
  		$(All.get_active_tab() + " #nominal").val(tot_dp);
  	}
  	//console.log("Jumlah maksimum klaim produk adalah : "+nilaiskr);
  },
  
  checkDataStockist : function(nilai) {
  	    All.set_disable_button();
	   	$.ajax({
	            url: All.get_url("be/promo/kolagen400bv/stockist/check/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	            	All.set_enable_button();
	            	if(data.response == "true") {
	            		$(All.get_active_tab() + " #pricecode").val(data.arrayData[0].pricecode);
	            		$(All.get_active_tab() + " #loccdnm").val(data.arrayData[0].fullnm);
	            		$(All.get_active_tab() + " #sc_co").val(data.arrayData[0].sfno);
	            		$(All.get_active_tab() + " #sc_dfno").val(data.arrayData[0].sfno);
	            		$(All.get_active_tab() + " #costkname").val(data.arrayData[0].costkname);
	            	} else {
	            		alert(data.message);
	            		All.set_disable_button();
	            		$(All.get_active_tab() + " #pricecode").val(null);
	            		$(All.get_active_tab() + " #loccdnm").val(null);
	            		$(All.get_active_tab() + " #sc_co").val(null);
	            		$(All.get_active_tab() + " #sc_dfno").val(null);
	            		$(All.get_active_tab() + " #costkname").val(null);
	            	}
	            	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	    });
  },
  
  checkDataProduct : function(param) {
  	   All.set_disable_button();
  	   var prdcd = $(All.get_active_tab() + " #prdcd" +param).val();
  	   var pricecode = $(All.get_active_tab() + " #pricecode").val();
	   	$.ajax({
	            url: All.get_url("be/promo/kolagen400bv/price/") +prdcd+ "/" +pricecode,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	            	
	            	if(data.response == "true") {
	            		All.set_enable_button();
	            		$(All.get_active_tab() + " #prdnm" +param).val(data.arrayData[0].prdnm);
	            		$(All.get_active_tab() + " #dp" +param).val(All.num_normal(data.arrayData[0].dp));
	            		$(All.get_active_tab() + " #bv" +param).val(All.num_normal(data.arrayData[0].bv));
	            	} else {
	            		
	            		alert("Invalid Kode Produk");
	            		$(All.get_active_tab() + " #prdnm" +param).val(null);
	            		$(All.get_active_tab() + " #dp" +param).val(null);
	            		$(All.get_active_tab() + " #bv" +param).val(null);
	            		All.set_disable_button();
	            	}
	            	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	    });
  },
  
  saveKolagenRedemp : function() {
  	All.set_disable_button();
	$.post(All.get_url('be/promo/kolagen400bv/redemp/save'), $(All.get_active_tab() + " #kolagen400bv").serialize(), function(data) {
		if(data.response == "true") {
			var str =  "No Trx       : "+data.arraydata.trcd+"\n";
			str +=     "ID Member    : "+data.arraydata.idmember+"\n";
			str +=     "Nama Member  : "+data.arraydata.membernm+"\n";
			str +=     "Total Klaim  : "+data.arraydata.total_redemp+"\n";
			alert(str);
			All.reload_page('be/promo/kolagen400bv')
		} else {
			
		}
	}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});	
 },
 
 checkDoubleOrderNo : function(nilai) {
 	
 	$.ajax({
	            url: All.get_url("db2/get/orderno/from/sc_newtrh/orderno/") +nilai,
	            type: 'GET',
	            dataType: 'json',
	            success:
	            function(data){
	            	
	            	if(data.response == "true") {
	            		alert("Orderno sudah ada..");
	            		$(All.get_active_tab() + " #orderno").focus();
	            		All.set_disable_button();
	            	} else {
	            		All.set_enable_button();
	            		
	            	}
	            	
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	    });
 },
 
 getListRedempToGenerate : function() {
 	All.set_disable_button();
 	$.post(All.get_url('be/promo/kolagen400bv/generate/list'), $(All.get_active_tab() + " #kolagen400Generate").serialize(), function(data) {
			All.set_enable_button();
			$(All.get_active_tab() + " .result").html(data);
		 }).fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
 },
 
 previewGenerate : function() {
 	All.set_disable_button();
 	$.post(All.get_url('be/promo/kolagen400bv/generate/save'), $(All.get_active_tab() + " #generatesub").serialize(), function(data) {
			All.set_enable_button();
			$(All.get_active_tab() + " .mainForm").hide();
			$(All.get_active_tab() + " .nextForm1").html(null);
			$(All.get_active_tab() + " .nextForm1").html(data);
		 }).fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
 },
 
 saveGenerate : function() {
 	
 	All.set_disable_button();
 	$.post(All.get_url('be/promo/kolagen400bv/generate/save2'), $(All.get_active_tab() + " #generatesubs22").serialize(), function(data) {
			All.set_enable_button();
			//$(All.get_active_tab() + " .mainForm").hide();
			$(All.get_active_tab() + " .nextForm1").html(null);
			$(All.get_active_tab() + " .nextForm1").html(data);
		 }).fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
 },
 
 checkIEC : function() {
		var memberid = $(All.get_active_tab() + " #iec_memb").val();
    	$.ajax({
            url: All.get_url("be/iec/result/") +memberid,
            type: 'GET',
            success:
            function(data){
            	$(All.get_active_tab() + " .result").html(null);
            	$(All.get_active_tab() + " .result").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	saveShoppingVoucher : function() {
	 	All.set_disable_button();
	 	$.post(All.get_url('shop/vch/create/save'), $(All.get_active_tab() + " #saveShopVch").serialize(), function(data) {
				All.set_enable_button();
	            $(All.get_active_tab() + " .mainForm").hide();
				All.clear_div_in_boxcontent(".nextForm1");
	            $(All.get_active_tab() + " .nextForm1").html(data);  
			 }).fail(function() {
				alert("Error requesting page");
				All.set_enable_button();
			});
	 },

	importPromoAyuartisToDB : function() {
		var formData = new FormData($(All.get_active_tab() + " #formImportPromo")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url('be/promo/importayu/action2'),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm > .result");
				$(All.get_box_content() + ".mainForm > .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});
	},

	getDetailBonus : function() {
		var dfno = $(All.get_active_tab() + " #idmember").val();
		var date1 = $(All.get_active_tab() + " #tgl1").val();
		var date2 = $(All.get_active_tab() + " #tgl2").val();
		//console.log("newidstk : " +newidstk+ ", orderno : " +orderno);
		All.set_disable_button();
		$.ajax({
			url : All.get_url('bonusayuartis/tampil/detail'),
			type : 'POST',
			dataType: 'json',
			data : {dfno : dfno, date1 : date1, date2 : date2},
			success : function(data) {
				All.set_enable_button();
				alert(data.message);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' +xhr.status);
				All.set_enable_button();
			}
		});
	}
  
  
}
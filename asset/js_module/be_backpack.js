var $ = jQuery;

var be_backpack = {
	checkIDStockist : function(nilai, sentTo) {
		All.set_disable_button();
		All.get_wait_message();
		
		$.ajax({
            url: All.get_url("be/backpack/stockist/") +nilai,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {					
					$(All.get_active_tab() + sentTo).val(data.arrayData[0].fullnm);
					$(All.get_active_tab() + " #idmember1").focus();
				} else {
					alert(data.message);
					All.set_disable_button();
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	},
	
	checkIDMember : function(nilai, param) {
		All.set_disable_button();
		All.get_wait_message();
		
		$.ajax({
            url: All.get_url("be/backpack/member/") +nilai,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {	
					var x = parseInt(param);
					var next = x + 1;				
					$(All.get_active_tab() + " #nmmember" +param).val(data.arrayData[0].fullnm);
					be_backpack.addNewTabMember(next);
					$(All.get_active_tab() + " #jum").val(x);
				} else {
					alert(data.message);
					$(All.get_active_tab() + " #idmember" +param).val(null);
					$(All.get_active_tab() + " #nmmember" +param).val(null);
					$(All.get_active_tab() + " #idmember" +param).focus();
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	},
	
	addNewTabMember : function(next2) {
		var next = parseInt(next2)
		var rowhtml = "";
		rowhtml += "<tr><td><input id='idmember"+next+"' name='idmember[]' type='text' class='span20' onchange='be_backpack.checkIDMember(this.value,"+next+")' /></td>";
	  	rowhtml += "<td><input id='nmmember"+next+"' name='nmmember[]' type='text' class='span20' readonly='readonly' /></td>";
	  	rowhtml += "<td><input style='text-align: right' type='text' id='trf"+next+"' name='trf[]'  class='span20' value='150000' readonly='readonly' /></td></tr>";
	  	$(All.get_active_tab() + " #tbl_member").append(rowhtml);
	  	var total_trf = parseInt($(All.get_active_tab() + " #total_trf_real").val());
	  	var tot = total_trf + 150000;
	  	$(All.get_active_tab() + " #total_trf_real").val(tot);
	  	$(All.get_active_tab() + " #total_trf").val(All.num(tot));
	  	$(All.get_active_tab() + " #idmember" +next).focus();
	},
	
	addNewTabMemberAfterInput : function(next2) {
		$(All.get_active_tab() + " #tbl_member").html(null);
		var next = parseInt(next2)
		var rowhtml = "";
		rowhtml += "<tr><td><input id='idmember"+next+"' name='idmember[]' type='text' class='span20' onchange='be_backpack.checkIDMember(this.value,"+next+")' /></td>";
	  	rowhtml += "<td><input id='nmmember"+next+"' name='nmmember[]' type='text' class='span20' readonly='readonly' /></td>";
	  	rowhtml += "<td><input style='text-align: right' type='text' id='trf"+next+"' name='trf[]'  class='span20' value='150000' readonly='readonly' /></td></tr>";
	  	$(All.get_active_tab() + " #tbl_member").append(rowhtml);
	  	$(All.get_active_tab() + " #total_trf").val("0");
    	$(All.get_active_tab() + " #total_trf_real").val("0");
    	$(All.get_active_tab() + " #jum").val("0");
    	$(All.get_active_tab() + " #id_stockist").focus();
	},
	
	saveTrfBackpak : function(formID) {
		All.set_disable_button();
        //All.get_image_load2();
        $.post(All.get_url('be/backpack/save') , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {  
            All.set_enable_button();
            if(data.response == "true") {
            	alert("ID : " +data.id);
            	$(All.get_active_tab() + " #id_stockist").val(null);
            	$(All.get_active_tab() + " #nm_stockist").val(null);
            	be_backpack.addNewTabMemberAfterInput("1");
            } else {
            	alert(data.message);
            }
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
	},
	
	getUpdateStatus : function(nilai) {
		All.set_disable_button();
		All.get_wait_message();
		var id_trf = $(All.get_active_tab() + " #idx" +nilai).val();
		var stt = $(All.get_active_tab() + " #updstt" +nilai).val();
		$.ajax({
            url: All.get_url("be/backpack/update/status"),
            type: 'post',
			dataType: 'json',
			data : {id: id_trf, status :stt},
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
				if(data.response == "true") {	
					All.ajaxFormPost('formReportTrcBackpack','be/backpack/report/list');
				} 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	}
	
}
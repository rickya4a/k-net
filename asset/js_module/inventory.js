var $ = jQuery;

var Asset = {
    getUpdateInvCat : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #id_cat" +param).val();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/catInv/check/id_cat/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					All.formUpdateActivate();
					$(All.get_active_tab() + " #id_cat").val(data.arrayData[0].id_cat);
                    $(All.get_active_tab() + " #id_cat").attr("readonly","readonly");
					$(All.get_active_tab() + " #cat_nm").val(data.arrayData[0].cat_nm);
					$(All.get_active_tab() + " #remarks").val(data.arrayData[0].remarks);
					$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
    
    getUpdateSupplier : function (param){
        All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #id_supplier" +param).val();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/supplier/check/id_supplier/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					All.formUpdateActivate();
					$(All.get_active_tab() + " #id_supplier").val(data.arrayData[0].id_supplier);
                    $(All.get_active_tab() + " #id_supplier").attr("readonly","readonly");
					$(All.get_active_tab() + " #nm_supplier").val(data.arrayData[0].nm_supplier);
					$(All.get_active_tab() + " #remarks").val(data.arrayData[0].remarks);
					$(All.get_active_tab() + " #company").val(data.arrayData[0].company_nm);
                    $(All.get_active_tab() + " #tel_hp").val(data.arrayData[0].tel_hp_supplier);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
                 $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
            }
        });
    },
    
    getUpdateInv : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #id_inventory" +param).val();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
        $(All.get_active_tab() + " .qtyReq").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/inv/check/id_inventory/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					All.formUpdateActivate();
                    $(All.get_active_tab() + " #id_inventory").attr("readonly","readonly");
					$(All.get_active_tab() + " #id_inventory").val(data.arrayData[0].id_inventory);
                    $(All.get_active_tab() + " #nm_inventory").val(data.arrayData[0].nm_inventory);
                    $(All.get_active_tab() + " #cat_inv").val(data.arrayData[0].id_cat);
					$(All.get_active_tab() + " #list_supp").val(data.arrayData[0].id_supplier);
                    $(All.get_active_tab() + " #status_invv").val(data.arrayData[0].status_inv);
                    
                    if(data.arrayData[0].qty == '0'){
                        $(All.get_active_tab() + " #qtyInv").attr("readonly","readonly");
                        $(All.get_active_tab() + " #qtyReq").removeAttr('readonly');
                        $(All.get_active_tab() + " #qtyReq").val(data.arrayData[0].qty_req);
                    }else{
                       $(All.get_active_tab() + " #qtyInv").attr("readonly","readonly");
                       $(All.get_active_tab() + " #qtyInv").val(data.arrayData[0].qty);
                       $(All.get_active_tab() + " #qtyReq").attr("readonly","readonly"); 
                       $(All.get_active_tab() + " #qtyReq").val(data.arrayData[0].qty_req);
                       //$(All.get_active_tab() + " #qtyReq").val();
                    }
                    
					$(All.get_active_tab() + " #value").val(parseInt(data.arrayData[0].value));
					$(All.get_active_tab() + " #remarks").val(data.arrayData[0].remarks);
                    $(All.get_active_tab() + " #idToHistory").val(data.arrayData[0].id);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
    
    getUpdateEmpl : function(param){
        
        //alert('isiny ' +param);
        All.set_disable_button();
		All.get_wait_message();
		//var id = $(All.get_active_tab() + " #id_employee" +param).val();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/employee/check/id_employee/" +param) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					All.formUpdateActivate();
                    $(All.get_active_tab() + " #id_employee").attr("readonly","readonly");
					$(All.get_active_tab() + " #id_employee").val(data.arrayData[0].id_employee);
                    $(All.get_active_tab() + " #nm_employee").val(data.arrayData[0].employee_nm);
                    $(All.get_active_tab() + " #divisi").val(data.arrayData[0].divisi);
					$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    getDetailEmployee : function(param) {
        //alert('isiny ' +param);
        All.set_disable_button();
		All.get_wait_message();
		//var id = $(All.get_active_tab() + " #id_employee" +param).val();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/employee/check/id_employee/" +param) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					//All.formUpdateActivate();
                    //$(All.get_active_tab() + " #id_employee").attr("readonly","readonly");
					$(All.get_active_tab() + " #id_employee").val(data.arrayData[0].id_employee);
                    $(All.get_active_tab() + " #nm_employee").val(data.arrayData[0].employee_nm);
                    $(All.get_active_tab() + " #divisi").val(data.arrayData[0].divisi);
					$(All.get_active_tab() + " #invent").focus();
                    //$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    getDetailInv : function(id){
        //alert('isiny ' +param);
        All.set_disable_button();
		All.get_wait_message();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/inv/check/id_inventory/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					//All.formUpdateActivate();
                    //$(All.get_active_tab() + " #id_employee").attr("readonly","readonly");
					$(All.get_active_tab() + " #id_inventory").val(data.arrayData[0].id_inventory);
                    $(All.get_active_tab() + " #qtyAvail").val(data.arrayData[0].qty);
                    //$(All.get_active_tab() + " #divisi").val(data.arrayData[0].divisi);
					$(All.get_active_tab() + " #qtyReq").focus();
                    //$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    getUpdateMutasi : function (param){
        All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #id_mut" +param).val();
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
        //$(All.get_active_tab() + " .qtyReq").removeAttr('readonly');
		$.ajax({
            url: All.get_url("ma/mutasi/check/id/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					
					All.formUpdateActivate();
                    $(All.get_active_tab() + " #id_employee").attr("readonly","readonly");
                    $(All.get_active_tab() + " #id_employee").val(data.arrayData[0].id_employee);
					$(All.get_active_tab() + " #nm_employee").val(data.arrayData[0].employee_nm);
                    $(All.get_active_tab() + " #divisi").val(data.arrayData[0].divisi);
                    $(All.get_active_tab() + " #id_inventory").val(data.arrayData[0].id_inventory);
                    $(All.get_active_tab() + " #qtyAvail").val(data.arrayData[0].qtyAvails);
                    $(All.get_active_tab() + " #id_muts").val(data.arrayData[0].id);
                    $(All.get_active_tab() + " #id_invss").val(data.arrayData[0].id_inventory);
                    $(All.get_active_tab() + " #id_inventory").attr("readonly","readonly");
                    if(data.arrayData[0].qtyAvails == '0'){
                        $(All.get_active_tab() + " #qtyReq").attr("readonly","readonly");
                        $(All.get_active_tab() + " #qtyReq").val(data.arrayData[0].qtyReq);
                    }else{
                        $(All.get_active_tab() + " #qtyReq").attr('readonly');
                        $(All.get_active_tab() + " #qtyReq").val(data.arrayData[0].qtyReq);
                    }
                    
                    $(All.get_active_tab() + " #status_invv").val(data.arrayData[0].status);
                    $(All.get_active_tab() + " #remarks").val(data.arrayData[0].remarks);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    inputFormData: function(url, formid) {
    	All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
        {  
            All.set_enable_button();
			if(data.response == "false") {
                All.set_error_message(".mainForm .result", data.message);
            } 
            else {
                All.set_success_message(".mainForm .result", data.message);
				All.reset_all_input();
                All.reload_page('ma/mutasi/inv');
            } 
            
        }, "json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
    },
    
    updateFormData: function(url, formid, nextToLoad) {
    	All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #" +formid).serialize(), function(data)
        {  
            All.set_enable_button();
			alert(data.message);
			if(data.response == "true") {
                	All.reset_all_input();
                	All.cancelUpdateForm();
                	All.getListData(nextToLoad);
                    All.reload_page('ma/mutasi/inv');
	            } 
	            
            
        }, "json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
    },
    
    getListMutasi : function(){
   	    All.set_disable_button();
		All.get_image_load();

		$.post(All.get_url('ma/mutasi/report/act/0'), $(All.get_active_tab() + " #formMutasiRpt").serialize(), function(data) {
			All.set_enable_button();
			//All.clear_div_in_boxcontent(".mainForm .result");
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(data);

		});
    },
}
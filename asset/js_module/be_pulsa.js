var $ = jQuery;

var be_pulsa = {
	payDebtClient : function(idx) {
		var client_name = idx.id;
		All.set_disable_button();
		All.get_image_load();
		$.ajax({
			url : All.get_url('be/pulsa/pay/') + client_name,
			type : 'GET',
			dataType : "json",
			success : function(data) {
				alert(data.message);
				if (data.response == "true") {
					All.ajaxFormPost('formPulsaSearch', 'be/pulsa/search/list');
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	addSelectedPrice : function(param) {
		var cur_tot_price = parseInt($(All.get_active_tab() + " #total_selected_price_real").val());
		var add = parseInt($(All.get_active_tab() + " #price" + param).val());
		var price_now = 0;
		if ($(All.get_active_tab() + " #pil" + param).is(':checked')) {
			price_now = cur_tot_price + add;
		} else {
			price_now = cur_tot_price - add;
		}
		$(All.get_active_tab() + " #total_selected_price_real").val(price_now);
		$(All.get_active_tab() + " #total_selected_price").val(All.num(price_now));
	},
	
	addSelectedPriceDetail : function(param) {
		var cur_tot_price = parseInt($(All.get_active_tab() + " #total_selected_price_realdet").val());
		var add = parseInt($(All.get_active_tab() + " #pricedet" + param).val());
		var price_now = 0;
		if ($(All.get_active_tab() + " #pilDet" + param).is(':checked')) {
			price_now = cur_tot_price + add;
		} else {
			price_now = cur_tot_price - add;
		}
		$(All.get_active_tab() + " #total_selected_price_realdet").val(price_now);
		$(All.get_active_tab() + " #total_selected_pricedet").val(All.num(price_now));
	},
	
	paySelectedDebt : function() {
		
		/*All.set_disable_button();
		All.get_image_load2();*/
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").length);
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").serialize());
		$.post(All.get_url('be/pulsa/pay'), $(All.get_active_tab() + " #formPaySelectedDebt").serialize(), function(data) {
			All.set_enable_button();
			if (data.response == "true") {
				alert("Trx successfully updated..!");
				All.ajaxFormPost("formPulsaSearch",'be/pulsa/search/list')
			} else {
				alert(data.message);
			}
		 }, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
		//console.log("ok");
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").length
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").serialize());
	},
	
	paySelectedDebtDet : function() {
		
		/*All.set_disable_button();
		All.get_image_load2();*/
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").length);
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").serialize());
		$.post(All.get_url('be/pulsa/pay/selected'), $(All.get_active_tab() + " #formPaySelectedDebtClient").serialize(), function(data) {
			All.set_enable_button();
			if (data.response == "true") {
				alert("Trx successfully updated..!");
				All.back_to_form(" .nextForm1", " .mainForm");
				All.ajaxFormPost("formPulsaSearch",'be/pulsa/search/list')
			} else {
				alert(data.message);
			}
		 }, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
		//console.log("ok");
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").length
		//console.log($(All.get_active_tab() + " #formPaySelectedDebt").serialize());
	},

	showDateonTrxSearch : function(nilai) {
		if (nilai == "cell_phone" || nilai == "trx_client") {
			$(All.get_active_tab() + " .showDt").css("display", "block");
		} else {
			$(All.get_active_tab() + " .showDt").css("display", "none");
		}
	},

	searchTrxByID : function(nilai) {
		$.ajax({
			url : All.get_url('be/pulsa/fullcode'),
			type : 'POST',
			dataType : "json",
			data : {
				fullcode : nilai
			},
			success : function(data) {
				if (data.response == "true") {
					$(All.get_active_tab() + " #no_tujuan").val(data.phone);
					$(All.get_active_tab() + " #client").val(data.client);
					$(All.get_active_tab() + " #harga_jual").val(data.price);
					$(All.get_active_tab() + " #code").val(data.kode);
					$(All.get_active_tab() + " #harga_beli").val(data.harga_beli);
					$(All.get_active_tab() + " #jam").val(data.jam);

				} else {
					alert(data.message);
					$(All.get_active_tab() + " #no_tujuan").val(null);
					$(All.get_active_tab() + " #client").val(null);
					$(All.get_active_tab() + " #harga_jual").val(null);
					$(All.get_active_tab() + " #code").val(null);
					$(All.get_active_tab() + " #harga_beli").val(null);
					$(All.get_active_tab() + " #jam").val(null);
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	savePulsaTrx : function(formID) {
		All.set_disable_button();
		All.get_image_load2();
		$.post(All.get_url('be/pulsa/save'), $(All.get_active_tab() + " #" + formID).serialize(), function(data) {
			All.set_enable_button();
			if (data.response == "true") {
				alert(data.message);
				$(All.get_active_tab() + " #trx_id").val(null);
				$(All.get_active_tab() + " #no_tujuan").val(null);
				$(All.get_active_tab() + " #client").val(null);
				$(All.get_active_tab() + " #harga_jual").val(null);
				$(All.get_active_tab() + " #code").val(null);
				$(All.get_active_tab() + " #harga_beli").val(null);
				$(All.get_active_tab() + " #jam").val(null);
				All.ajaxFormPost(formID, 'be/pulsa/date');
			} else {
				alert(data.message);
			}
		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	saveUpdatePulsaTrx : function(formID) {
		All.set_disable_button();
		All.get_image_load2();
		$.post(All.get_url('be/pulsa/update'), $(All.get_active_tab() + " #" + formID).serialize(), function(data) {
			All.set_enable_button();
			if (data.response == "true") {
				alert(data.message);
				$(All.get_active_tab() + " #trx_id").val(null);
				$(All.get_active_tab() + " #no_tujuan").val(null);
				$(All.get_active_tab() + " #client").val(null);
				$(All.get_active_tab() + " #harga_jual").val(null);
				$(All.get_active_tab() + " #code").val(null);
				$(All.get_active_tab() + " #harga_beli").val(null);
				$(All.get_active_tab() + " #jam").val(null);
				All.ajaxFormPost(formID, 'be/pulsa/date');
			} else {
				alert(data.message);
			}
		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	setOrderType : function(nilai) {
		if (nilai == "L") {
			$(All.get_active_tab() + " #lunas").val("Y");
		} else {
			$(All.get_active_tab() + " #lunas").val("");
		}
	},

	getUpdateTrx : function(nilai) {
		All.set_disable_button();
		All.get_image_load();
		$.ajax({
			url : All.get_url('be/pulsa/id/') + nilai,
			type : 'GET',
			dataType : "json",
			success : function(data) {
				if (data.response == "true") {
					All.set_enable_button();
					All.formUpdateActivate();
					$(All.get_active_tab() + " #trx_id").val(data.arrayData[0].full_code);
					$(All.get_active_tab() + " #no_tujuan").val(data.arrayData[0].no_tujuan);
					$(All.get_active_tab() + " #client").val(data.arrayData[0].client);
					$(All.get_active_tab() + " #harga_jual").val(data.arrayData[0].harga_jual);
					$(All.get_active_tab() + " #order_lgs").val(data.arrayData[0].order_lgs);
					$(All.get_active_tab() + " #lunas").val(data.arrayData[0].lunas);
					$(All.get_active_tab() + " #tgl_trans").val(data.arrayData[0].tgl_trans);
					$(All.get_active_tab() + " #idx").val(data.arrayData[0].id);
				} else {
					alert("Data not found..!!");
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	deleteTrx : function(nilai) {
		All.set_disable_button();
		All.get_image_load();
		$.ajax({
			url : All.get_url('be/pulsa/delete/') + nilai,
			type : 'GET',
			dataType : "json",
			success : function(data) {
				alert(data.message);
				if (data.response == "true") {
					All.ajaxFormPost("formPulsa", 'be/pulsa/date');
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});

	},

	checkDoubleNoClient : function(no) {
		All.set_disable_button();
		All.get_image_load();
		$.ajax({
			url : All.get_url('be/pulsa/client/no/') + no,
			type : 'GET',
			dataType : "json",
			success : function(data) {
				if (data.response == "true") {
					alert("Already exist phone number : " + no + ", Owner : " + data.arrayData[0].client);
					$(All.get_active_tab() + " #no_tujuan").val(null);
					$(All.get_active_tab() + " #client").val(null);
					All.set_enable_button();
				} else {
					All.set_enable_button();
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	getUpdateClient : function(thx) {
		var no = thx.id;
		All.set_disable_button();
		All.get_image_load();
		$.ajax({
			url : All.get_url('be/pulsa/client/no/') + no,
			type : 'GET',
			dataType : "json",
			success : function(data) {
				if (data.response == "true") {
					All.set_enable_button();
					All.formUpdateActivate();
					$(All.get_active_tab() + " #no_tujuan").val(data.arrayData[0].no_tujuan);
					$(All.get_active_tab() + " #client").val(data.arrayData[0].client);
					$(All.get_active_tab() + " #flag_upd").val(1);
				} else {
					alert("Data not found..!!");
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + ':' + xhr.status);
				All.set_enable_button();
			}
		});
	},

	saveInputClient : function(formID) {
		All.set_disable_button();
		All.get_image_load2();
		$.post(All.get_url('be/pulsa/client/save'), $(All.get_active_tab() + " #" + formID).serialize(), function(data) {
			All.set_enable_button();
			if (data.response == "true") {
				alert(data.message);
				$(All.get_active_tab() + " #flag_update").val("0");
				$(All.get_active_tab() + " #no_tujuan").val(null);
				$(All.get_active_tab() + " #client").val(null);
				All.ajaxFormPost(formID, 'be/pulsa/client/list');
			} else {
				alert(data.message);
			}
		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	saveUpdateClient : function(formID) {
		All.set_disable_button();
		All.get_image_load2();
		$.post(All.get_url('be/pulsa/client/update'), $(All.get_active_tab() + " #" + formID).serialize(), function(data) {
			All.set_enable_button();
			if (data.response == "true") {
				alert(data.message);
				$(All.get_active_tab() + " #no_tujuan").val(null);
				$(All.get_active_tab() + " #client").val(null);
				All.ajaxFormPost(formID, 'be/pulsa/client/list');
			} else {
				alert(data.message);
			}
		}, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
		});
	},

	deleteDataClient : function(client, no) {

	}
}
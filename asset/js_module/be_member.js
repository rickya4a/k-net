var $ = jQuery;

var be_member = {
    get_sponsor_info: function (idsponsor) {
    	
    		$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            
            $.ajax({
	            url: All.get_url("be_member/helper/distInfo/") +idsponsor,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #nmsponsor").val(data.arrayData[0].fullnm);
	                	 $(All.get_active_tab() + " #nmsponsor").attr('readonly', true);
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('ID Sponsor salah atau sudah termination')
	                     $(All.get_active_tab() + " #nmsponsor").attr('readonly', true);
	                     $(All.get_active_tab() + " #formUmroh input[type='text']").val('');
	                     $(All.get_active_tab() + " #idsponsor").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    cekKtp : function(ktpno){
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
		$.ajax({
	            url: All.get_url("be_member/helper/ktp/") +ktpno,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "false") {
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('No KTP sudah terdaftar');
	                     $(All.get_active_tab() + " #ktpno").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    cekTlp : function(tel_hp){
        
		$.ajax({
	            url: All.get_url("be_member/helper/hp/") +tel_hp,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "false") {
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('No Hp Sudah Terdaftar');
	                     $(All.get_active_tab() + " #tel_hp").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
		
    },
    
    get_id_info : function (dfno){
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            //alert('isi nya' +dfno);
           $.ajax({
	            url: All.get_url("be/member/helper/idInfo/") +dfno,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	               var arrayData = data.arrayData;
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #fullnm").val(arrayData[0].fullnm);
	                	 $(All.get_active_tab() + " #fullnm").attr('readonly', true);
	                     $(All.get_active_tab() + " #ranknm").val(arrayData[0].ranknm);
	                	 $(All.get_active_tab() + " #ranknm").attr('readonly', true);
	                     $(All.get_active_tab() + " #currentrank").val(arrayData[0].currentrank);
	                	 $(All.get_active_tab() + " #currentrank").attr('readonly', true);
	                     $(All.get_active_tab() + " #period").val(arrayData[0].bonusmonth);
	                	 $(All.get_active_tab() + " #period").attr('readonly', true);
	                     $(All.get_active_tab() + " #year1").val(arrayData[0].bonusyear);
	                	 $(All.get_active_tab() + " #year1").attr('readonly', true);
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('ID salah atau sudah termination');
	                     $(All.get_active_tab() + " #fullnm").attr('readonly', true);
	                     $(All.get_active_tab() + " #formRank input[type='text']").val('');
	                     $(All.get_active_tab() + " #dfno").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    get_level_info : function (){
           var nmrank = $(All.get_active_tab() + " #newrank option:selected").text();
           $(All.get_active_tab() + " #newranknm").val(nmrank);
           /*$.ajax({
	            url: All.get_url("be/member/helper/levelInfo/") +level,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	               var arrayData = data.arrayData;
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #newranknm").val(arrayData[0].level);
	                	 $(All.get_active_tab() + " #newranknm").attr('readonly', true);
	                     $(All.get_active_tab() + " #level").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });*/
    },
    
    saveRank : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be/member/save/") , $(All.get_active_tab() + " #formRank").serialize(), function(data)
            {  
                $(All.get_active_tab() + " input[type=text]").val(null);
                $(All.get_active_tab() + " select[class=form-list]").val(null);
                $(All.get_active_tab() + " .result").html(null);
                $(All.get_active_tab() + " .result").html("<div class='alert alert-error' align=center>"+data.message+"</div>");
                /*if(data.response == "true") {
                	
                } else {
                	
                }*/
            },"json");
    },
    
    saveInput : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be/member/place/save") , $(All.get_active_tab() + " #formPlaceInput").serialize(), function(data)
            {  
                $(All.get_active_tab() + " input[type=text]").val(null);
                $(All.get_active_tab() + " .result").html(null);
                $(All.get_active_tab() + " .result").html("<div class='alert alert-error' align=center>"+data.message+"</div>");
                /*if(data.response == "true") {
                	
                } else {
                	
                }*/
            },"json");
    },
    
    formPost : function() {
		All.set_disable_button();
        $.post(All.get_url("be/member/place") , $(All.get_active_tab() + " #"+ formPlace).serialize(), function(data)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .mainForm").hide();
	        All.clear_div_in_boxcontent(".nextForm1");
	        $(All.get_active_tab() + " .nextForm1").html(data); 
	        $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(data);  
               
        }).fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
	},
	
    getDetailMember : function(nilai) {
    	   All.set_disable_button();
           $.ajax({
	            url: All.get_url("be/member/id/") +nilai,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	            	All.set_enable_button();
	                var arrayData = data.arrayData;
	                if(data.response == "true") {
	                     $(All.get_active_tab() + " #memberid").val(arrayData[0].memberid);
	                	 $(All.get_active_tab() + " #membername").val(arrayData[0].membername);
	                	 $(All.get_active_tab() + " #sponsorid").val(arrayData[0].sponsorid);
	                	 $(All.get_active_tab() + " #sponsornm").val(arrayData[0].sponsorname);
	                	 $(All.get_active_tab() + " #rekruiterid").val(arrayData[0].recruiterid);
	                	 $(All.get_active_tab() + " #rekruiternm").val(arrayData[0].rekruitername);
	                     $(All.get_active_tab() + " #ktpno").val(arrayData[0].idno);
                         $(All.get_active_tab() + " #tgllhr").val(arrayData[0].tgllhr);
                         $(All.get_active_tab() + " #blnlhr").val(arrayData[0].blnlhr);
                         $(All.get_active_tab() + " #thnlhr").val(arrayData[0].thnlhr);
                         $(All.get_active_tab() + " #addr1").val(arrayData[0].addr1);
                         $(All.get_active_tab() + " #sex").val(arrayData[0].sex);
                         $(All.get_active_tab() + " #tel_hp").val(arrayData[0].tel_hp);
                         $(All.get_active_tab() + " #tel_hm").val(arrayData[0].tel_hm);
                         $(All.get_active_tab() + " #norek").val(arrayData[0].acc_no);
                         $(All.get_active_tab() + " #pilBank").val(arrayData[0].bankid);
                         $(All.get_active_tab() + " #loccd").val(arrayData[0].stk_code);
	                     $(All.get_active_tab() + " #loccd_name").val(arrayData[0].stkname);
                         $(All.get_active_tab() + " #userlogin").val(arrayData[0].userlogin);
                         $(All.get_active_tab() + " #token").val(arrayData[0].token);
	                     /*$(All.get_active_tab() + " #bnsstmt").val(arrayData[0].bnsstmsc);
	                     $(All.get_active_tab() + " #bnsstmt_name").val(arrayData[0].bnsstmt_name);*/
	                } else {
	                	 alert(data.message);
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    getDetailStockist : function(nilai, setTo) {
    	  All.set_disable_button();
           $.ajax({
	            url: All.get_url("be/member/stk/id/") +nilai,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	            	All.set_enable_button();
	                var arrayData = data.arrayData;
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #" +setTo).val(arrayData[0].fullnm);                	 
	                } else {
	                	 alert(data.message);
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    getDtByOrderid : function(orderid){
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            
            $.ajax({
	            url: All.get_url("be_member/helper/dtByOrderID/") +orderid,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                var arrayData = data.arrayData;
	                if(data.response == "true") {
	                   $(All.get_active_tab() + " #sponsorid").val(arrayData[0].sponsorid);
                       $(All.get_active_tab() + " #sponsornm").val(arrayData[0].sponsornm);
                       $(All.get_active_tab() + " #membername").val(arrayData[0].membername);
                       $(All.get_active_tab() + " #idstk").val(arrayData[0].stk_code);
                       $(All.get_active_tab() + " #ktpno").val(arrayData[0].idno);
                       $(All.get_active_tab() + " #addr1").val(arrayData[0].addr1);
                       $(All.get_active_tab() + " #tel_hp").val(arrayData[0].tel_hp);
                       $(All.get_active_tab() + " #tel_hm").val(arrayData[0].tel_hm);
                       $(All.get_active_tab() + " #norek").val(arrayData[0].acc_no);
                       $(All.get_active_tab() + " #pilBank").val(arrayData[0].bankid);
                       $(All.get_active_tab() + " #sex").val(arrayData[0].sex);
                       $(All.get_active_tab() + " #tgllhr").val(arrayData[0].tgllhr);
                       $(All.get_active_tab() + " #blnlhr").val(arrayData[0].blnlhr);
                       $(All.get_active_tab() + " #thnlhr").val(arrayData[0].thnlhr);
                       $(All.get_active_tab() + " #userlogin").val(arrayData[0].userlogin);
                       $(All.get_active_tab() + " #prdcd").val(arrayData[0].prdcd);
                       $(All.get_active_tab() + " #ordernumb").val(arrayData[0].ordernumb);
	                }else{
	                   
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    checkSponsor : function(sponsor,usrlogin){
    	//var usrlogin = 'ADMIN';
        All.set_disable_button();
		$.ajax({
            url: All.get_url("be/member/helper/checkSponsor") + "/" +sponsor+ "/" +usrlogin,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                All.set_enable_button();
                if(data.response == "true") {
                    if(data.message != null) {
                        alert(data.message);
                    }
                	$(All.get_active_tab() + " #sponsornm" ).val(data.arrayData[0].fullnm);
				} else {
					alert("Data "+nilai+ " not found");
					$(All.get_active_tab() + " #sponsornm" ).val(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
        
    },
    
    checkSponsor1 : function(rekruiter,usrlogin){
    	//var usrlogin = 'ADMIN';
        All.set_disable_button();
		$.ajax({
            url: All.get_url("be/member/helper/checkSponsor") + "/" +rekruiter+ "/" +usrlogin,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                All.set_enable_button();
                if(data.response == "true") {
                    if(data.message != null) {
                        alert(data.message);
                    }
                	$(All.get_active_tab() + " #rekruiternm" ).val(data.arrayData[0].fullnm);
				} else {
					alert("Data "+nilai+ " not found");
					$(All.get_active_tab() + " #rekruiternm" ).val(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
        
    },
    
    checkUserlogin : function(usrlogin){
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            $.ajax({
	            url: All.get_url("be_member/helper/distInfo/") +usrlogin,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #userloginnm").val(data.arrayData[0].fullnm);
	                	 $(All.get_active_tab() + " #userloginnm").attr('readonly', true);
	                     $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                	 $(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     alert('ID Sponsor salah atau sudah termination')
	                     $(All.get_active_tab() + " #userloginnm").attr('readonly', true);
	                     //$(All.get_active_tab() + " #formUmroh input[type='text']").val('');
	                     $(All.get_active_tab() + " #userlogin").focus();
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    getDataMemberInfo : function(nilai) {
    	All.set_disable_button();
           $.ajax({
	            url: All.get_url("be/member/info/id/") +nilai,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	            	All.set_enable_button();
	                var arrayData = data.arrayData;
	                var lbc = data.lbc;
	                if(data.response == "true") {
					   $(All.get_active_tab() + " #recruiterid").val(arrayData[0].idrecruiter);
                       $(All.get_active_tab() + " #recruitername").val(arrayData[0].recruitername);	
	                   $(All.get_active_tab() + " #sponsorid").val(arrayData[0].idsponsor);
                       $(All.get_active_tab() + " #sponsorname").val(arrayData[0].sponsorname);
                       $(All.get_active_tab() + " #membername").val(arrayData[0].fullnm);
                       $(All.get_active_tab() + " #ktpno").val(arrayData[0].idno);
                       $(All.get_active_tab() + " #addr1").val(arrayData[0].addr1);
                       $(All.get_active_tab() + " #addr2").val(arrayData[0].addr2);
                       $(All.get_active_tab() + " #addr3").val(arrayData[0].addr3);
                       $(All.get_active_tab() + " #sex").val(arrayData[0].sex);
                       $(All.get_active_tab() + " #tel_hp").val(arrayData[0].tel_hp);
                       $(All.get_active_tab() + " #tgllhr").val(arrayData[0].tgllhr);
                       $(All.get_active_tab() + " #blnlhr").val(arrayData[0].blnlhr);
                       $(All.get_active_tab() + " #thnlhr").val(arrayData[0].thnlhr);
					   $(All.get_active_tab() + " #bankid").val(arrayData[0].bankid);
					   $(All.get_active_tab() + " #bankaccno").val(arrayData[0].bankaccno);
					   $(All.get_active_tab() + " #email").val(arrayData[0].email);
                       $(All.get_active_tab() + " #loccd").val(arrayData[0].loccd);
                       $(All.get_active_tab() + " #loccd_name").val(arrayData[0].loccd_name);
                       $(All.get_active_tab() + " #bnsstmt").val(arrayData[0].bnsstmsc);
                       $(All.get_active_tab() + " #bnsstmt_name").val(arrayData[0].bnsstmsc_name);
                       if(lbc !== null) {     
                       	 $(All.get_active_tab() + " #lbc_area").css("display", "block");            	 
                       	 $(All.get_active_tab() + " #register_dt").val(lbc[0].register_dt);
                       	 $(All.get_active_tab() + " #expired_dt").val(lbc[0].expired_dt);
                       	 if(lbc[0].status_expire == "1") {
                       	 	$(All.get_active_tab() + " #status_expire").val(1);
                       	 	var exp = "Expired";
                       	 	$(All.get_active_tab() + " #del_exp_lbc").removeAttr("disabled");
                       	 } else {
                       	 	$(All.get_active_tab() + " #status_expire").val(0);
                       	 	$(All.get_active_tab() + " #del_exp_lbc").attr("disabled", "disabled");
                       	 	var exp = "Active";
                       	 }
                       	 $(All.get_active_tab() + " #status_expire").val(0);
                       	 $(All.get_active_tab() + " #status_lbc").val(exp);
                       } else {
                       	 $(All.get_active_tab() + " #lbc_area").css("display", "none");
                       	 $(All.get_active_tab() + " #lbcmember").val(0);
                       	 $(All.get_active_tab() + " #del_exp_lbc").attr("disabled", "disabled");
                       }
	                } else {
	                	 alert(data.message);
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    clear_expire_lbc : function() {
    	var idmemb = $(All.get_active_tab() + " #memberid").val();
    	$.ajax({
	            url: All.get_url("db2/delete/from/ASH_LBC_MEMB/dfno") + "/" +idmemb,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	                
	                alert(data.message);
	                if(data.response == "true") {
	                	 All.reload_page('be/member/info');
	                } 
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    checkExistIDmember : function(nilai){
        $(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            $.ajax({
	            url: All.get_url("be_member/helper/checkExistIDMember/") +nilai,
	            type: 'GET',
	            dataType: "json",
	            success:
	            function(data){
	               
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " #idmember").val(data.arrayData[0].id_memb);
	                     $(All.get_active_tab() + " #orderid").val(data.arrayData[0].orderno);
	                     $(All.get_active_tab() + " #tipe_upd").val(data.update);
	                     
                         //$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
	                } else {
	                   $(All.get_active_tab() + " #idmember").val("0");
	                	 //$(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
	                     $(All.get_active_tab() + " #membername").focus();
	                     $(All.get_active_tab() + " #tipe_upd").val(0);
	                }
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    isValidEmailAddress: function(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    },
    
    reconcileMemberRegVoucher : function(param) {
    	All.set_disable_button();
    	$.ajax({
	            url: All.get_url("be/member/voucher/reconcile/save"),
	            type: 'POST',
	            dataType: "json",
	            data: {idmember: param},
	            success:
	            function(data){
	                All.set_enable_button();
	                alert(data.message);
	                if(data.response == "true") {
	                	 $(All.get_active_tab() + " tr#"+param).remove();
	                } 
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	      });
    },
    
    readFromFile : function(url) {
		var formData = new FormData($(All.get_active_tab() + " #formUploadVchError")[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url(url),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false
		});

	},
}

jQuery(document).ready(function($){
    if($(All.get_active_tab() + " #formRegs").length) {
        $(All.get_active_tab() + ' #email').blur(function(){
            var strValid = be_member.isValidEmailAddress($(All.get_active_tab() + ' #email').val());
            if(strValid === false) {
                alert('Alamat email salah');
                $(All.get_active_tab() + ' #email').focus();
            }
        });
    }
});
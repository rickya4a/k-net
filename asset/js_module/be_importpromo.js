var $ = jQuery;

var be_importpromo = {

    importPromoToDB : function() {
        var formData = new FormData($(All.get_active_tab() + " #formImportPromo")[0]);
        All.set_disable_button();
        All.get_wait_message();
        $.ajax({
            url : All.get_url('be/promo/importayu'),
            type : 'POST',
            data : formData,
            async : false,
            success : function(data) {
                All.set_enable_button();
                All.clear_div_in_boxcontent(".mainForm > .result");
                $(All.get_box_content() + ".mainForm > .result").html(data);
            },
            cache : false,
            contentType : false,
            processData : false
        });
    },
}
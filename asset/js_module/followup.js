
var $ = jQuery;

var Followup = {
	getUpdateFollowUpType : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #id" +param).val();
		$.ajax({
            url: All.get_url("db2/get/DION_followup_type/where/id/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					All.formUpdateActivate();
					$(All.get_active_tab() + " #id").val(data.arrayData[0].id);
					$(All.get_active_tab() + " #followup_type").val(data.arrayData[0].follow_type);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	setCustType : function() {
		
	},
	
	checkIDCust : function() {
		
		var cust_type = $(All.get_active_tab() + " #cust_type").val();
		if(cust_type != "other") {
			var cust_id = $(All.get_active_tab() + " #cust_id").val();
			var url;
			if(cust_type == "1") {
				url = "followup/memb/dfno/" + cust_id;
				$.ajax({
	            	url: All.get_url(url) ,
		            type: 'GET',
					dataType: 'json',
		            success:
		            function(data){
		                All.set_enable_button();
						if(data.response == "true") {
							$(All.get_active_tab() + " #cust_name").val(data.arrayData[0].fullnm);
							$(All.get_active_tab() + " #tel_no").val(data.arrayData[0].tel_hm);
							$(All.get_active_tab() + " #tel_hp").val(data.arrayData[0].tel_hp);
							$(All.get_active_tab() + " #email").val(data.arrayData[0].email);
						} else {
							alert("ID Member tidak ada dalam database")
						}
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
	            }); 
			} else if(cust_type == "2") {
				url = "db2/get/mssc/where/loccd/" + cust_id;
				$.ajax({
	            	url: All.get_url(url) ,
		            type: 'GET',
					dataType: 'json',
		            success:
		            function(data){
		                All.set_enable_button();
						if(data.response == "true") {
							$(All.get_active_tab() + " #cust_name").val(data.arrayData[0].fullnm);
							$(All.get_active_tab() + " #tel_no").val(data.arrayData[0].tel_of);
							$(All.get_active_tab() + " #tel_hp").val(data.arrayData[0].tel_hp);
							$(All.get_active_tab() + " #email").val(data.arrayData[0].email);
						} else {
							alert("ID Member tidak ada dalam database")
						}
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
	            }); 
			} 
			
			
  
		} 
	},
	
	setFollowupType : function() {
		var followup_type = $(All.get_active_tab() + " #followup_type").val();
		var cust_id = $(All.get_active_tab() + " #cust_id").val();
		var cust_name = $(All.get_active_tab() + " #cust_name").val();
		var nextUrl = "";
		/*if(followup_type == "7"  || followup_type == "1" || followup_type == "6") {
			$(All.get_active_tab() + " #isBV").html(null);	
			$(All.get_active_tab() + " #isBV").show();*/
			if(followup_type == "7") {
				nextUrl = "followup/bv/form";
			} else {	
				nextUrl = "followup/resign-reactive/" + followup_type;
			}
			$.ajax({
            url: All.get_url(nextUrl) ,
	            type: 'GET',
	            success:
	            function(data){
	            	$(All.get_active_tab() + " #isBV").html(null);	
			        $(All.get_active_tab() + " #isBV").show()
	                $(All.get_active_tab() + " #isBV").append(data);
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
        	});		
		/*} else {
			$(All.get_active_tab() + " #isBV").html(null);
			$(All.get_active_tab() + " #isBV").hide();
		}*/
	},
	
	addTTP : function() {
		All.set_disable_button();
		var ttpAdd = $(All.get_active_tab() + " #ttpAdd").val().trim();
		$.ajax({
            url: All.get_url("followup/bv/ttp/add/" +ttpAdd) ,
            type: 'GET',
            dataType: 'json',
            success:
            function(data){
            	All.set_enable_button();
            	if(data.response == "false") {
            		var r = confirm("No TTP tidak ada di database, Tekan OK bila ingin tetap malanjutkan..");
					if (r == true) {
					    html += "<tr>";
	            		html += "<td align=center><input type=hidden name=ttp[] value="+ttpAdd+" />"+ttpAdd+"</td>";
	            		html += "<td align=center><input type=hidden name=trcd[] value="+ttpAdd+" />"+ttpAdd+"</td>";
	            		html += "<td align=right><input type=hidden name=ndp[] value=0 />0</td>";
	            		html += "<td align=right><input type=hidden name=nbv[] value=0 />0</td>";
	            		html += "<td align=center><input type=hidden name=dfno[] value=0 />0</td>";
	            		html += "<td align=center><a class='btn btn-mini btn-danger'><i class='icon-trash icon-white'></i></a></td>";
            			html += "</tr>"
                		$(All.get_active_tab() + " #listTTP").append(html);
					} 
            	} else {
            		$(All.get_active_tab() + " #ttpAdd").val(null);
            		$(All.get_active_tab() + " #ttpAdd").focus();
            		arrayData = data.arrayData;
            		var html = "";
            		var ndp = parseInt(arrayData[0].ndp);
            		var nbv = parseInt(arrayData[0].nbv);
            		html += "<tr>";
            		if(arrayData[0].orderno !== null) {
            			html += "<td align=center><input type=hidden name=ttp[] value="+arrayData[0].orderno+" />"+arrayData[0].orderno+"</td>";
            		} else {
            			html += "<td align=center><input type=hidden name=ttp[] value="+arrayData[0].trcd+" />"+arrayData[0].trcd+"</td>";
            		}
            		html += "<td align=center><input type=hidden name=trcd[] value="+arrayData[0].trcd+" />"+arrayData[0].trcd+"</td>";
            		html += "<td align=right><input type=hidden name=ndp[] value="+ndp+" />"+All.num(ndp)+"</td>";
            		html += "<td align=right><input type=hidden name=nbv[] value="+nbv+" />"+All.num(nbv)+"</td>";
            		html += "<td align=center><input type=hidden name=dfno[] value="+arrayData[0].dfno+" />"+arrayData[0].dfno+"</td>";
            		html += "<td align=center><a class='btn btn-mini btn-danger'><i class='icon-trash icon-white'></i></a></td>";
            		html += "</tr>"
                	$(All.get_active_tab() + " #listTTP").append(html);
            	}		 
            	
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
	   });
  },
  
  saveFollowUp : function() {
  	  All.set_disable_button();
  	  $.post(All.get_url('followup/save'), $(All.get_active_tab() + " #followUpInput").serialize(), function(data) {
			All.set_enable_button();
			alert(data.message);
			if (data.response == "true") {
				All.reload_page("followup");
				$(All.get_active_tab() + " #formno").focus();
			}
		 }, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
	  });
  },
  
  deleteFollowUp : function(param) {
  	if(confirm("Anda akan menghapus record ini..?"))
    {
  	  var id = $(All.get_active_tab() + " #id" +param).val();
		$.ajax({
            url: All.get_url("followup/delete/id/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                alert(data.message);
				if(data.response == "true") {
					All.set_enable_button();
					$(All.get_active_tab() + " table tr#" +param).animate({ opacity: "hide" }, "slow");
        			return false;	
				} 	
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
        
       //console.log("tr#id" +param);
        
     }    
  },
  
  updateFollowUp : function() {
  	  All.set_disable_button();
  	  $.post(All.get_url('followup/edit/save'), $(All.get_active_tab() + " #followUpEdit").serialize(), function(data) {
			All.set_enable_button();
			alert(data.message);
			if(data.response == "true") {
				All.back_to_form(' .nextForm1',' .mainForm');
			} 
		 }, "json").fail(function() {
			alert("Error requesting page");
			All.set_enable_button();
	  });
  },
  
  tabIndexEnter : function()  {
  	/*$(document).on("keypress", ".TabOnEnter" , function(e)
		  {
		  	var currIndex = $(this).attr('tabindex');
		    var arr = [];
		  	if(e.keyCode ==  13)
		    {	
		    	$('.TabOnEnter').each(function(){
		      	arr.push($(this).attr('tabindex'));
		      });
		      var currentIndex = arr.indexOf(currIndex);
		      if (currentIndex < arr.length) {
		      	$('.TabOnEnter[tabindex="' + arr[currentIndex+1] + '"]').focus();
		      }
		    }   
		}); */
  },
  
  searchCustID : function() {
  	    All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #cust_id").val();
		$.ajax({
            url: All.get_url("db2/get/msmemb/where/dfno/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                
				if(data.response == "true") {
					All.set_enable_button();
					$(All.get_active_tab() + " #cust_name").val(data.arrayData[0].fullnm);
				} else {
					$.ajax({
			            url: All.get_url("db2/get/mssc/where/loccd/" +id) ,
			            type: 'GET',
						dataType: 'json',
			            success:
			            function(data){
			            	if(data.response == "true") {
			            		All.set_enable_button();
								$(All.get_active_tab() + " #cust_name").val(data.arrayData[0].fullnm);
			            	}
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                 alert(thrownError + ':' +xhr.status);
							 All.set_enable_button();
			            }
			        });	
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
  },
  
  getIdFollowup : function(param) {
  	$(All.get_active_tab() + " #penjelasan").val("");
  	var status = $(All.get_active_tab() + " #status" +param).val();
  	var fid = $(All.get_active_tab() + " #id" +param).val();
  	$(All.get_active_tab() + " #fid").val(fid);
  	$(All.get_active_tab() + " #updStatus").val(status);
  },
  
  getDetailFollowUp : function(param) {
  	
  },
  
  updateStatusFollowUp : function(param) {
  	 var fid = $(All.get_active_tab() + " #fid").val();
  	 var status = $(All.get_active_tab() + " #updStatus").val();
  	 var penjelasan = $(All.get_active_tab() + " #penjelasan").val();
  	 
  	 $.ajax({
            url: All.get_url("followup/status/update") ,
            type: 'POST',
			dataType: 'json',
			data: {followup_id: fid, status_followup: status, alasan: penjelasan},
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
				/*if(data.response == "true") {	
					All.postListData('listFollowUp','followup/process/list');
				}*/
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
  },
  
  getTrxByTrxNo : function(value) {
  	  All.set_disable_button();
		All.get_wait_message();
		$.ajax({
            url: All.get_url("followup/trx_klink/" +value) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	All.set_enable_button();
            	if(data.response == "true") {
            		$(All.get_active_tab() + " #tipetrx").val(data.tipetrx);
            		$(All.get_active_tab() + " #orderno").val(data.arrayData[0].orderno);
            		$(All.get_active_tab() + " #nbv").val(data.arrayData[0].nbv);
            		$(All.get_active_tab() + " #trcd").val(data.arrayData[0].trcd);
            		$(All.get_active_tab() + " #trcdx").val(data.arrayData[0].trcd);
            		
            		$(All.get_active_tab() + " #ndp").val(data.arrayData[0].ndp);
            		$(All.get_active_tab() + " #bnsperiod").val(data.arrayData[0].tglbns);
            		$(All.get_active_tab() + " #curdate").val(data.arrayData[0].tglnow);
            		$(All.get_active_tab() + " #stk").val(data.arrayData[0].sc_dfno);
            		$(All.get_active_tab() + " #stkname").val(data.arrayData[0].stkname);
            		$(All.get_active_tab() + " #co_stk").val(data.arrayData[0].scco);
            		$(All.get_active_tab() + " #co_stk_name").val(data.arrayData[0].costkname);
            		$(All.get_active_tab() + " #main_stk").val(data.arrayData[0].loccd);
            		$(All.get_active_tab() + " #main_stk_name").val(data.arrayData[0].mainstkname);
            		$(All.get_active_tab() + " #createby").val(data.arrayData[0].createnm);
            		$(All.get_active_tab() + " #createdt").val(data.arrayData[0].tglinp);
            		$(All.get_active_tab() + " #memberid").val(data.arrayData[0].dfno);
            		$(All.get_active_tab() + " #membername").val(data.arrayData[0].nmmember);
            		
            		/*if(data.arrayData[0].diffdate > 1) {
            			$(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
            		} else {
            			$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
            		}
            		
            		disable = "disabled=disabled";
			        err = "<font color=red>*Please Check Bonus Period and Current Date</font>";*/
			        if(parseInt(data.arrayData[0].diffdate) > 0) {
			        	$(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
				    	$(All.get_active_tab() + " #errMsg").html("<font color=red>*Please Check Bonus Period and Current Date</font>");
			        } else {
			        	if(data.arrayData[0].diffdate == 0 || data.batas_update <= data.arrayData[0].tglnow) {
			        		$(All.get_active_tab() + " #btn_input_user").removeAttr("disabled");
					       	$(All.get_active_tab() + " #errMsg").html(null);
			        	} else {
			        		$(All.get_active_tab() + " #btn_input_user").attr("disabled", "disabled");
				    		$(All.get_active_tab() + " #errMsg").html("<font color=red>*Please Check Bonus Period and Current Date</font>");
			        	}
			        }    
			        var rowshtml = "";
			        $(All.get_active_tab() + " #detListPrd").html(null);
			        var prd = data.product;
			        var tot_bv = 0;
			        var tot_dp = 0;
			        $.each(prd, function(key, value) {
						
						rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
						rowshtml += "<td align=center>" + value.prdcd + "</td>";
						rowshtml += "<td>" + value.prdnm + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.qtyord)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.pv)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.dp)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.sumbv)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.sumharga)) + "</td></tr>";
						tot_bv += parseInt(value.sumbv);
						tot_dp += parseInt(value.sumharga);
					});
					rowshtml += "<tr><td colspan=6 align=center>TOTAL</td><td align=right>"+All.num(parseInt(tot_bv))+"</td><td align=right>"+All.num(parseInt(tot_dp))+"</td></tr>";
					$(All.get_active_tab() + " #detListPrd").append(rowshtml);
				    
            	} else {
            		alert(data.message);
            	}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });    	
        
  },
  
  updateBvTrx : function() {
    var formno = $(All.get_active_tab() + " #formnoXXX").val();	 	
  	var rec_id = $(All.get_active_tab() + " #rec_id").val();
  	var trxtype = $(All.get_active_tab() + " #tipetrx").val(); 
  	var trxno = $(All.get_active_tab() + " #trcdx").val();
  	var id_member = $(All.get_active_tab() + " #new_memberid").val();
  	var remarks = $(All.get_active_tab() + " #remarks").val();
  	All.set_disable_button();
  	$.ajax({
            url: All.get_url("followup/update/bv/save") ,
            type: 'POST',
            dataType: 'json',
            data: {rec_id: rec_id, tipetrx: trxtype, trcd: trxno, new_memberid: id_member, remarks: remarks},
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
                if(data.response == "true") {
                	$(All.get_box_content() + " .nextForm2").html(null);
                	$.ajax({
		        		url: All.get_url("followup/update/id/" +formno) ,
			            type: 'GET',
			            success:
			            function(data){
			                $(All.get_box_content() + " .nextForm1").html(null);
			                $(All.get_box_content() + " .nextForm1").html(data);
			                $(All.get_box_content() + " .nextForm1").show();	
			            }
		            });	
		        }    
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
          });  
  },
  
  updateResignTerminate : function() {
  		All.set_disable_button();
  		var followup_id = $(All.get_active_tab() + " #followup_id").val();
        $.post(All.get_url("followup/resign/save") , $(All.get_active_tab() + " #updateResignTerminate").serialize(), function(data)
        {  
            All.set_enable_button();
            alert(data.message);
            if(data.response == "true") {
            	$.ajax({
		            url: All.get_url("followup/update/id/" +followup_id) ,
		            type: 'GET',
					success:
		            function(data){
		            	if(data.upd == "true") {
		            		$(All.get_active_tab() + " .nextForm2").html(null);
			                $(All.get_active_tab() + " .nextForm1").show();
			                $(All.get_active_tab() + " .nextForm1").html(null);
			                $(All.get_active_tab() + " .nextForm1").html(data);
		            	} else {
		            		$(All.get_active_tab() + " .result").html(null);
		            		$(All.get_active_tab() + " .result").html(data.message);
		            	}
		                
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
		        });
            }   
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        })
  },
  
  updateExcAcc : function() {
  		All.set_disable_button();
  		var followup_id = $(All.get_active_tab() + " #followup_id").val();
        $.post(All.get_url("followup/exc/acc/save") , $(All.get_active_tab() + " #updateExcAcc").serialize(), function(data)
        {  
            All.set_enable_button();
            alert(data.message);
            if(data.response == "true") {
            	$.ajax({
		            url: All.get_url("followup/update/id/" +followup_id) ,
		            type: 'GET',
					success:
		            function(data){
		            	if(data.upd == "true") {
		            		$(All.get_active_tab() + " .nextForm2").html(null);
			                $(All.get_active_tab() + " .nextForm1").show();
			                $(All.get_active_tab() + " .nextForm1").html(null);
			                $(All.get_active_tab() + " .nextForm1").html(data);
		            	} else {
		            		$(All.get_active_tab() + " .result").html(null);
		            		$(All.get_active_tab() + " .result").html(data.message);
		            	}
		                
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
		        });
            }   
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        })
  },
  
  
  getDataMemberTermination : function() {
  	  var dfno = $(All.get_active_tab() + " #dfno").val();
  	  
  	  url = "followup/memb/dfno/" + dfno;
				$.ajax({
	            	url: All.get_url(url) ,
		            type: 'GET',
					dataType: 'json',
		            success:
		            function(data){
		                All.set_enable_button();
						if(data.response == "true") {
							var addrx = data.arrayData[0].addr1+""+data.arrayData[0].addr2+""+data.arrayData[0].addr3;
							$(All.get_active_tab() + " #dfno").val(data.arrayData[0].dfno);
							$(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
							$(All.get_active_tab() + " #idno").val(data.arrayData[0].idno);
							$(All.get_active_tab() + " #addr").val(addrx);
							$(All.get_active_tab() + " #jointdt").val(data.arrayData[0].jointdt);
							$(All.get_active_tab() + " #stockist").val(data.arrayData[0].loccd);
						} else {
							alert("ID Member tidak ada dalam database")
						}
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
	            });
  },
  
  setMemberInfoForResignation : function() {
  		var dfno = $(All.get_active_tab() + " #dfno").val();
  	    url = "followup/memb/dfno/" + dfno;
				$.ajax({
	            	url: All.get_url(url) ,
		            type: 'GET',
					dataType: 'json',
		            success:
		            function(data){
		                All.set_enable_button();
						if(data.response == "true") {
							$(All.get_active_tab() + " #dfno").val(data.arrayData[0].dfno);
							$(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
							$(All.get_active_tab() + " #idno").val(data.arrayData[0].idno);
							$(All.get_active_tab() + " #dfno_rank").val(data.arrayData[0].shortnm);
							$(All.get_active_tab() + " #memberstatus").val(data.arrayData[0].memberstatus);
						} else {
							alert("ID Member tidak ada dalam database")
						}
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
	            });
  },
  
  saveDataLOS : function(formID, url) {
  	All.set_disable_button();
  		var followup_id = $(All.get_active_tab() + " #followup_id").val();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #" +formID).serialize(), function(data)
        {  
            All.set_enable_button();
            alert(data.message);
            if(data.response == "true") {
            	if(data.upd == "true") {
	            	$.ajax({
			            url: All.get_url("followup/update/id/" +followup_id) ,
			            type: 'GET',
						success:
			            function(data){
			            	
			            	$(All.get_active_tab() + " .nextForm2").html(null);
				            $(All.get_active_tab() + " .nextForm1").show();
				            $(All.get_active_tab() + " .nextForm1").html(null);
				            $(All.get_active_tab() + " .nextForm1").html(data);
			            	
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			                 alert(thrownError + ':' +xhr.status);
							 All.set_enable_button();
			            }
			        });
			    } else {
			    	All.reset_all_input();
			    }    
            }   
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        })
  },
  
  getDataMemberExcUpline : function() {
  		var dfno = $(All.get_active_tab() + " #dfno").val();
  	    url = "followup/memb/dfno/" + dfno;
				$.ajax({
	            	url: All.get_url(url) ,
		            type: 'GET',
					dataType: 'json',
		            success:
		            function(data){
		                All.set_enable_button();
						if(data.response == "true") {
							$(All.get_active_tab() + " #dfno").val(data.arrayData[0].dfno);
							$(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
							$(All.get_active_tab() + " #sponsorid").val(data.arrayData[0].sponsorid);
							$(All.get_active_tab() + " #sponsorname").val(data.arrayData[0].sponsorname);
						} else {
							alert("ID Member tidak ada dalam database")
						}
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
	            });
  },
  
  previewGenerateLOS : function(formID, url){
   	    All.set_disable_button();
        //All.get_image_load2();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {  
            //All.reset_all_input();
            All.set_enable_button();
        	alert(data.message);
        	All.postListData(formID,'followup/los/type/list');
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
    },
    
  saveBatchLos : function(formID, url){
   	    All.set_disable_button();
        //All.get_image_load2();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {  
            //All.reset_all_input();
            All.set_enable_button();
        	alert(data.message);
        	All.postListData(formID,'followup/los/batch/list');
        },"json").fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
    },
    
    updateAccStatus : function(param) {
    	var batch_no = $(All.get_active_tab() + " #batchno" +param).val();
    	var acc_up = $(All.get_active_tab() + " #acc" +param).val();
    	var acc_text = $(All.get_active_tab() + " #acc" +param+ " option:selected").text();
    	$.ajax({
	            	url: All.get_url('followup/batch/acc/update') ,
		            type: 'POST',
					dataType: 'json',
					data: {batch_no: batch_no, acc_id: acc_up, keterangan: acc_text},
		            success:
		            function(data){
		                All.set_enable_button();
						alert(data.message);
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
		                 alert(thrownError + ':' +xhr.status);
						 All.set_enable_button();
		            }
	            });
	   //alert(acc_text);
    },
    
    saveUploadLOS : function() {
    	All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #uploadSaveImgLos")[0]);
    
        $.ajax({
            url: All.get_url('followup/los/upload/save'),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {
            	alert(data.message)
                All.set_enable_button();
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }  ,
    
    saveUploadLOS2 : function(param) {
    	All.set_disable_button();
		//All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #listFollowUpDet" +param)[0]);
        /*var idfo = $(All.get_active_tab() + " #idfo" +param).val();
        var id = $(All.get_active_tab() + " #id" +param).val();
        var file = $(All.get_active_tab() + " #file" +param).val();*/
        $.ajax({
            url: All.get_url('followup/los/upload/save'),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {
            	alert(data.message)
                All.set_enable_button();
            },
            cache: false,
            contentType: false,
            processData: false
        });
       
       //console.log("idfo : " + idfo + " - id :" +id + " - file : " +file);
       //console.log("formData : " + formData);
    }  
  	
}
$(document).keypress("ss",function(event) {
    /*if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19)) return true;
    $(All.get_active_tab() + " #btnSubmit").click();
    event.preventDefault();
    return false;*/
   if(event.ctrlKey && event.shiftKey) {
   	 $(All.get_active_tab() + " #btnSubmit").click();
   }
});



$(document).on("keypress", ".TabOnEnter" , function(e)
  {
    if( e.keyCode ==  13 )
    {
       var nextElement = $(All.get_active_tab() + ' [tabindex="' + (this.tabIndex+1)  + '"]');
       //console.log( this , nextElement ); 
       if(nextElement.length )
         nextElement.focus()
       else
         $('[tabindex="20"]').focus();  
    }   
});
 
var $ = jQuery;

var be_newera = {
	getDetailIncomingPayment : function(nilai) {
		All.set_disable_button();
		$.ajax({
            url: All.get_url("be/klink/incoming/detail"),
            type: 'post',
			dataType: 'json',
			data : {inc_pay: nilai},
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {	
					$(All.get_active_tab() + " #status").val(data.arrayData[0].status);
					$(All.get_active_tab() + " #cust_type").val(data.arrayData[0].kustomer);
					$(All.get_active_tab() + " #amount").val(data.arrayData[0].amount);
					$(All.get_active_tab() + " #value1").val(data.arrayData[0].dfno);
					$(All.get_active_tab() + " #value2").val(data.fullname);
					$(All.get_active_tab() + " #createnm").val(data.arrayData[0].createnm);
					$(All.get_active_tab() + " #updatenm").val(data.arrayData[0].updatenm);
					
				} else {
					alert(data.message);
					All.reset_all_input();
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
	},
	
	getFullNameByID : function() {
        All.set_disable_button();
        var cust_type = $(All.get_active_tab() + " #cust_type").val();
        var dfno = $(All.get_active_tab() + " #value1").val();
        if(cust_type != 'O') {
			$.ajax({
	            url: All.get_url("be/klink/incoming/fullname") + "/" +cust_type+ "/" +dfno,
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){
	            	
	                All.set_enable_button();
	                if(data.response == "true") {
	                	$(All.get_active_tab() + " #value2").val(data.arrayData[0].value2);
					} else {
						alert(data.message);
						$(All.get_active_tab() + " #value2").val(null);
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	    }    
    },
    
    getDetailTrxByID : function(nilai) {
    	All.set_disable_button();
		$.ajax({
            url: All.get_url("be/klink/shipping/kw/") +nilai,
            type: 'get',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {	
					$(All.get_active_tab() + " #registerno").val(data.arrayData[0].registerno);
					$(All.get_active_tab() + " #dfno").val(data.arrayData[0].dfno);
					$(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
					$(All.get_active_tab() + " #totdp").val(All.num(parseInt(data.arrayData[0].tdp)));
					$(All.get_active_tab() + " #totbv").val(All.num(parseInt(data.arrayData[0].tbv)));
					$(All.get_active_tab() + " #ship").val(data.arrayData[0].ship);
				} else {
					alert(data.message);
					//All.reset_all_input();
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      }); 
    }
}
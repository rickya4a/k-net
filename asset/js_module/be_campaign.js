var $ = jQuery;

var be_campaign = {

	getListAyuIndia  : function(param){
    All.set_disable_button();
    $(All.get_active_tab() + " #listAyuIndia").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.post(All.get_url("campaign/listUploadAyu/act/" +param) , $(All.get_active_tab() + " #frmAyuListIndia").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " #listAyuIndia").html(null);
            $(All.get_active_tab() + " #listAyuIndia").html(hasil);   
        });
    },
    
    saveInputCampaign  : function(url, formid) {

		All.set_disable_button();
		All.get_wait_message();

		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);
    			
        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            secureuri:false,
			fileElementId:'userfile',
            dataType: 'json',
            async: false,
            success: function (data) {
                
                if(data.response == "false") {
                	All.set_error_message(".mainForm > .result", data.message);
	            } 
	            else {
	                All.set_success_message(".mainForm > .result", data.message);
					All.reset_all_input();
	            } 
	            
	            //alert('selesai masuk');
	            All.set_enable_button();
	           	be_campaign.getListCampaignPic();
		    	//Product.refreshListProduct(' #parent_cat_inv_id');
            },
            cache: false,
            contentType: false,
            processData: false
        });
	},
	
    getListCampaignPic: function(url) {
    	All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url(url) ,
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
				//$(All.get_active_tab() + " #searchF").val(0);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    getListData  : function() {
        All.set_disable_button();
            $(All.get_active_tab() + " #result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("campaign/list") , $(All.get_active_tab() + " #frmInputCampaignPic").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #result").html(null);
                $(All.get_active_tab() + " #result").html(hasil);   
            });
    },
    
    getDetailDataX  : function(idx) {
        All.set_disable_button();
    	var id =  idx.id;
    	//alert('id = ' +id);
    	/*
        $(All.get_active_tab() + " #result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.post(All.get_url("campaign/det/" +id) , $(All.get_active_tab() + " #frmListCampaign" +id).serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " #result").html(null);
            $(All.get_active_tab() + " #result").html(hasil);   
        });
        */
        All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url("campaign/det/" +id) ,
            type: 'POST',
            success:
            function(data){
                All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
				//$(All.get_active_tab() + " #searchF").val(0);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    
    
    getDetailData : function(idx) {
    	var id =  idx.id;
		$.ajax({
            url: All.get_url("campaign/det/" +id) ,
			dataType : 'json',
			type : 'GET',
			success : function(data) {
				if (data.response == "true") {
					$(All.get_active_tab() + " .mainForm").hide();
					All.clear_div_in_boxcontent(".nextForm1");
					var header = data.header;
					var detail = data.detail;
					var payment = data.payment;
					var shipaddr = data.shipaddr;
					var payConnect = 0;
					var rowshtml = "<form>";
					rowshtml += "<table class='table table-striped table-bordered' width=100%>";
					rowshtml += "<tr><th colspan=4>TRANSACTION DATA</th></tr>";
					rowshtml += "<tr><td width=20%>Orderno</td><td>" + header[0].orderno + "</td><td>Total BV</td><td align=right>" + All.num(parseInt(header[0].total_bv)) + "</td></tr>";
					rowshtml += "<tr><td>CN No / Register No</td><td>" + header[0].CNno + " / " + header[0].REGISTERno + " </td><td width=20%>Total Pay</td><td align=right>" + All.num(parseInt(header[0].total_pay)) + "</td></tr>";
					rowshtml += "<tr><td>KW No</td><td>" + header[0].KWno + "</td><td>Shipping Cost</td><td align=right>" + All.num(parseInt(header[0].payShip)) + "</td></tr>";
					rowshtml += "<tr><td>IP No</td><td>" + header[0].IPno + "</td><td>Admin Cost</td><td align=right>" + All.num(parseInt(header[0].payAdm)) + "</td></tr>";
					if (header[0].payConnectivity != null) {
						payConnect = header[0].payConnectivity;
					}
					rowshtml += "<tr><td>ID Member</td><td>" + header[0].id_memb + " / " + header[0].nmmember + "</td><td>Connectivity Cost</td><td align=right>" + All.num(parseInt(payConnect)) + "</td></tr>";
					rowshtml += "<tr><td>Input By</td><td>" + header[0].log_usrlogin + "</td><td>CN Status</td>";
					if (header[0].CN_STAT == "1") {
						rowshtml += "<td>Settled</td></tr>";
					} else {
						rowshtml += "<td>Suspended</td></tr>";
					}

					//rowshtml +="<td>"+header[0].BV_STAT+"</td></tr>";
					rowshtml += "<tr><td>Trx Date</td><td>" + header[0].datetrans1 + " " + header[0].trx_time + "</td><td>BV Status</td>";
					if (header[0].BV_STAT == "1") {
						rowshtml += "<td>Settled</td></tr>";
					} else {
						rowshtml += "<td>Suspended</td></tr>";
					}
					//rowshtml +="<td>BV Status</td><td>"+header[0].BV_STAT+"</td></tr>";
					rowshtml += "<tr><td>ID Stockist</td><td>" + header[0].nmstkk + "</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
					//rowshtml += "<tr><td>Total Pay</td><td>"+All.num(parseInt(header[0].total_pay))+"</td></tr>";
					//rowshtml += "<tr><td>Total BV</td><td>"+All.num(parseInt(header[0].total_bv))+"</td></tr>";
					//rowshtml += "<tr><td>Shipping Cost</td><td>"+All.num(parseInt(header[0].payShip))+"</td></tr>";
					//rowshtml += "<tr><td>Admin Cost</td><td>"+All.num(parseInt(header[0].payAdm))+"</td></tr>";

					rowshtml += "<tr><td><input class='btn btn-mini btn-warning span20' type=button value='<< Back' onclick=be_trans.back_to_form() /></td><td colspan=3>&nbsp;</td>";
					rowshtml += "</table>";

					rowshtml += "<table class='table table-striped table-bordered' width=100%>";
					rowshtml += "<tr><th colspan=8>DETAIL PRODUCT</th></tr>";
					rowshtml += "<tr><th width=5%>No</th><th width=15%>ID</th><th>Product Name</th><th width=8%>Qty</th><th width=10%>DP</th><th width=5%>BV</th><th width=10%>Tot DP</th><th width=5%>Tot BV</th></tr>";
					var tot_dp = 0;
					var tot_bv = 0;
					$.each(detail, function(key, value) {
						//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
						rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";
						rowshtml += "<td align=center>" + value.prdcd + "</a></td>";
						rowshtml += "<td>" + value.prdnm + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.qty)) + "</td>";

						rowshtml += "<td align=right>" + All.num(parseInt(value.dpr)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.bvr)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.qty * value.dpr)) + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.qty * value.bvr)) + "</td></tr>";
						tot_dp += (value.qty * value.dpr);
						tot_bv += (value.qty * value.bvr);

					});
					rowshtml += "<tr><td colspan=6>&nbsp;</td><td align=right>" + All.num(parseInt(tot_dp)) + "</td><td align=right>" + All.num(parseInt(tot_bv)) + "</td></tr>";
					rowshtml += "</table>";

					rowshtml += "<table class='table table-striped table-bordered' width=100%>";
					rowshtml += "<tr><th colspan=5>DETAIL PAYMENT INFO</th></tr>";
					rowshtml += "<tr><th width=30%>payment Type</th><th width=20%>Token No</th><th width=13%>Pay Amount</th><th>Status</th></tr>";
					$.each(payment, function(key, value) {
						//rowshtml += "<tr><td align=center><input type=checkbox id=pil"+(key+1)+" name=orderno[] value="+value.orderno+" /></td>";
						//rowshtml += "<tr><td align=right>" + (key + 1) + "&nbsp;</td>";

						rowshtml += "<td align=center>" + value.bankDisplayNm + "</a></td>";
						rowshtml += "<td align=center>" + value.token + "</td>";
						rowshtml += "<td align=right>" + All.num(parseInt(value.payamt)) + "</td>";
						rowshtml += "<td align=center>" + value.notes + "</td></tr>";

					});
					rowshtml += "</table>";

					if (header[0].sentTo == "2") {
						rowshtml += "<table class='table table-striped table-bordered' width=100%>";
						rowshtml += "<tr><th colspan=2>SHIPPING INFO</th></tr>";
						rowshtml += "<tr><td width=20%>Conote JNE</td><td>" + header[0].conoteJNE + "</td></tr>";
						rowshtml += "<tr><td>Service Type</td><td>" + header[0].service_type_name + "</td></tr>";
						rowshtml += "<tr><td>Cell Phone</td><td>" + header[0].tel_hp1 + "</td></tr>";
						rowshtml += "<tr><td>Destination Address</td><td>" + header[0].addr1 + "</td></tr>";
						rowshtml += "<tr><td>Provinsi</td><td>" + shipaddr[0].PROPINSI + "</td></tr>";
						rowshtml += "<tr><td>Kecamatan</td><td>" + shipaddr[0].Kecamatan + "</td></tr>";
						rowshtml += "<tr><td>Warehouse Code</td><td>" + shipaddr[0].whcd + "</td></tr>";
						rowshtml += "<tr><td>Warehouse Address</td><td>" + shipaddr[0].whcd_addr1 +" "+ shipaddr[0].whcd_addr2 +"</td></tr>";
					}
					rowshtml += "</form>";
					$(All.get_box_content() + ".nextForm1").append(rowshtml);
				} else {
					alert("No detail data found..!");
				}
			}
		});
	},
    
    saveInputCampaignPic : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #listingCampaignPic").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("campaign/save") , $(All.get_active_tab() + " #frmInputCampaignPic").serialize(), function(hasil)
            {  
                All.set_enable_button();
                //be_stockist.getListStockistPromo();
               //alert('sukses simpan'); 
            });
    },

    deleteCampaignPic : function(param) {
        All.set_disable_button();
        
		var loccd =  $("#loccd-"+param ).val();
		var start_date =  $("#start_date-"+param ).val();
		var expired_date =  $("#expired_date-"+param ).val();
		//alert("hai..."+loccd+" "+start_date+" "+expired_date);
		
            $.post(All.get_url("be_stockist/deleteSCPromo/") +loccd +"/" +start_date +"/" +expired_date, $(All.get_active_tab() + " #listStockistPromo").serialize(), function(hasil)
            {  
                be_stockist.getListStockistPromo();    
            });
    },
    
	
}
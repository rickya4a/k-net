var $ = jQuery;

var be_ticket = {
	/*getListPaymentTicket : function() {
		All.set_disable_button();
		All.get_image_load2();
        //$(All.get_active_tab() + " .result").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
        $.post(All.get_url("be/ticket/payment/list") , $(All.get_active_tab() + " #listPaymentTicket").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        });
	}, */
	
	approvePaymentTicket : function(nilai) {
		All.set_disable_button();
         $.ajax({
            url: All.get_url("be/ticket/payment/approve/" +nilai),
            dataType: 'json',
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
                if(data.response === "true") {
                	be_ticket.getListPaymentTicket();
                } 
                
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    }
        });
	},
	
	/*getPaymentReport : function() {
		All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url("be/ticket/payment/report/list") , $(All.get_active_tab() + " #ticketPayReport").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        });
	},
	
	getDetailEvent : function(idx) {
		var hdr_id = idx.id;
		All.set_disable_button();
		$.ajax({
            url: All.get_url('be/ticket/event/detail/') +hdr_id,
            type: 'GET',
            success:
            function(data){
            	All.set_enable_button();
            	$(All.get_active_tab() + " .mainForm").hide();
                All.clear_div_in_boxcontent(".nextForm1");
                $(All.get_active_tab() + " .nextForm1").html(data);  
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    } 
        });	
	},
	
	getTicketLocationReport : function() {
		All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url("be/ticket/location/list") , $(All.get_active_tab() + " #locationReport").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        });
	},
	
	getTicketSMS : function() {
		All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url("be/ticket/sms/list") , $(All.get_active_tab() + " #ticketSMSResend").serialize(), function(hasil)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(hasil);   
        }).fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });  
	},*/
	
	resendTicketSMS : function(param) {
		All.set_disable_button();
		$.ajax({
            url: All.get_url('be/ticket/sms/send/') +param,
            type: 'GET',
            dataType: 'json',
            success:
            function(data){
            	All.set_enable_button();
            	alert(data.message);
            } ,
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    }
        });	
	}, 
	
	getUpdateEtiketCategory : function(param) {
		All.set_disable_button();
		All.get_image_load();
		var cat_id = $(All.get_active_tab() + " #cat_id").val();
		console.log(param);
		console.log(All.get_active_tab());
		$.ajax({
            url: All.get_url('etiket/cat/id') +"/" +cat_id ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	  }	
}
var $ = jQuery;

var be_stockist = {
    
   getListStockistPromo : function() {
        All.set_disable_button();
            $(All.get_active_tab() + " #listingStockistPromo").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_stockist/listPromo") , $(All.get_active_tab() + " #frmScPromo").serialize(), function(hasil)
            {  
                All.set_enable_button();
                $(All.get_active_tab() + " #listingStockistPromo").html(null);
                $(All.get_active_tab() + " #listingStockistPromo").html(hasil);   
            });
    },
    
    saveSCPromo : function(){
        All.set_disable_button();
        $(All.get_active_tab() + " #listingStockistPromo").html('<span style="display: block; text-align: center;">Silahkan tunggu...</span>');
            $.post(All.get_url("be_stockist/savePromo") , $(All.get_active_tab() + " #frmScPromo").serialize(), function(hasil)
            {  
                be_stockist.getListStockistPromo(); 
            });
    },

    
    deleteStockistPromo : function(param) {
        All.set_disable_button();
        
		var loccd =  $("#loccd-"+param ).val();
		var start_date =  $("#start_date-"+param ).val();
		var expired_date =  $("#expired_date-"+param ).val();
		//alert("hai..."+loccd+" "+start_date+" "+expired_date);
		
            $.post(All.get_url("be_stockist/deleteSCPromo/") +loccd +"/" +start_date +"/" +expired_date, $(All.get_active_tab() + " #listStockistPromo").serialize(), function(hasil)
            {  
                be_stockist.getListStockistPromo();    
            });
    },
    
    saveUploadMT  : function(url, formid) {
		All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);
    
        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {
                
                if(data.response == "false") {
                	All.set_error_message(".mainForm > .result", data.message);
	            } 
	            else {
	                All.set_success_message(".mainForm > .result", data.message);
					All.reset_all_input();
	            } 
	            
	            Product.refreshListProductCat(' #cat_id');
		    	Product.refreshListProduct(' #parent_cat_inv_id');
            },
            cache: false,
            contentType: false,
            processData: false
        });
	},
    
    
	show_kab : function(id,kab)
    {
        //console.log('fdfd ' +id);
      $(kab).attr("disabled", "disabled");
       if(id == "") { 
            alert ("Silahkan pilih provinsi terlebih dahulu");
       }
       else{
            $.ajax({
                url: All.get_url("be/stockist/listKab/") +id,
                type: 'GET',
                success:
                function(data){
                    $(kab).removeAttr("disabled", "disabled");
                    $(kab).html(null);
                    $(kab).html(data);
                    
                },
                    error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' +xhr.status);
				    All.set_enable_button();
                }
            });
       }
    }, 
    
    testSave : function(url, formid) {
        All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
        {  
            All.set_enable_button();
			if(data.response == "false") {
                All.set_error_message(".mainForm .result", data.message);
            } 
            else {
                All.set_success_message(".mainForm .result", data.message);
				//All.reset_all_input();
            } 
        },'json');
	},
	
	checkValidDO : function() {
		//var stockist = $("#idstk").val();
		var do_number = $("#do_number").val();
		All.set_disable_button();
		$.ajax({
            url: All.get_url('barcode/check/do') + "/" +do_number,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	
                //All.set_enable_button();
                if(data.response == "false") {
                	alert(data.message);
				} else {
					All.set_enable_button();
					if(data.ins_do == "1") {
						$(All.get_active_tab() + " #whcd").val(data.arrayData[0].whcd);
						$(All.get_active_tab() + " #idstk").val(data.arrayData[0].loccdTo);
						$(All.get_active_tab() + " #ins_do").val(data.ins_do);
					} else {
						$(All.get_active_tab() + " #ins_do").val(data.ins_do);
						$(All.get_active_tab() + " #whcd").val(data.arrayData[0].whcd);
						$(All.get_active_tab() + " #idstk").val(data.arrayData[0].shipto);
					}
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},
	
	getDataStockistBarcode  : function(param) {
		All.set_disable_button();
    	$.ajax({
            url: All.get_url('db2/get/fullnm/from/mssc/loccd') +"/"+param ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
				if(data.response == "true") {	
				    All.set_enable_button();
				    $(All.get_active_tab() + " #fullnm").val(data.arrayData[0].fullnm);
				    $(All.get_active_tab() + " #whcd").val(data.arrayData[0].whcd);	
				} else {
					alert(data.message);	
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	}
    
    /*testSave : function() {
        console.log();
        All.set_disable_button();
        All.get_image_load2();
        var arrStkMaintenance = $('input[name^="stkMaintenance"]:checked').map(function(){
                            return $(this).val();}).get();
        $.post(All.get_url('be/stockist/updateStkEcomm') , {dt: arrStkMaintenance}, function(data)
        {  
            All.set_enable_button();
            $(All.get_active_tab() + " .mainForm").hide();
            All.clear_div_in_boxcontent(".nextForm1");
            $(All.get_active_tab() + " .nextForm1").html(data);  
        }).fail(function() { 
            alert("Error requesting page"); 
            All.set_enable_button();
        });
     },*/
    
}

jQuery(document).ready(function($){
    if($(All.get_active_tab() + " #formUmroh").length) {
        var thnlhrOri = $('<select class="hide"></select>');
        var thnlhrOpt = $(All.get_active_tab() + " thnlhr option").clone();
        thnlhrOri.append(thnlhrOpt).insertAfter(All.get_active_tab() + ' #thnlhr');
        console.log("dokumen load..");
        be_umroh.getTipeJamaah();
        $(All.get_active_tab() + ' #email').blur(function(){
            var strValid = be_umroh.isValidEmailAddress($(All.get_active_tab() + ' #email').val());
            if(strValid === false) {
                alert('Alamat email salah');
                $(All.get_active_tab() + ' #email').focus();
            }
        });
		
    }
});
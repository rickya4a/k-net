var $ = jQuery;

var All = {
	api_usr : "k-net.apps",
	api_pwd : "apps@2017",

    get_active_tab : function() {
       var active_div =  $("#active_div").val();
       var x = "#tabs-" + active_div;
       return x;
    },

    get_image_load : function(set_to) {
        set_to = set_to || ".mainForm > .result";
        $(All.get_box_content() + set_to).html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
    },

    get_image_load2 : function() {
        //set_to = set_to || ".mainForm > .result";
        $(All.get_active_tab() + " .result").html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
    },

    set_wait_message: function() {
        var ht = "";
        ht += "<div class=wait_msg style='display: none;' align=center><font color=red>Please wait a moment...</font></div>";
        ht += "<div class=img_load></div>";
        return ht;
    },

    clear_wait_message: function() {
        $(All.get_active_tab() + " .wait_msg").css('display', 'none');
        $(All.get_active_tab() + " .img_load").html(null);
    },

    set_amount_record : function(count) {
        var ht = "";
        ht += "<input type=hidden id=cnt_rec value="+count+" />";
        ht += "<input type=hidden id=rack_cnt value="+count+" />";
        return ht;
    },

    get_wait_message:function() {
        $(All.get_active_tab() + " .wait_msg").css('display', 'block');
        //All.get_image_load(All.get_active_tab() + " .img_load");
        $(All.get_active_tab() + " .img_load").html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
    },

    clear_wait_message: function() {
        $(All.get_active_tab() + " .wait_msg").css('display', 'none');
        $(All.get_active_tab() + " .img_load").html(null);
    },

    set_datatable : function() {
        $(All.get_active_tab() + " .datatable").dataTable({

        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        "sPaginationType": "bootstrap",
		"oLanguage": {

		},
        "bDestroy": true
	  });
      $(All.get_active_tab() + " .datatable").removeAttr('style');
    },

    set_datatable2 : function() {
       /* $(All.get_active_tab() + " .datatable").dataTable({

        "aLengthMenu": [[100, 150, 200, 250 -1], [100, 150, 200, 250, 'All']],
        "sPaginationType": "bootstrap",
		"oLanguage": {

		},
        "bDestroy": true,
        "pageLength": 50
	  });
      $(All.get_active_tab() + " .datatable").removeAttr('style');*/
        $(All.get_active_tab() + " .datatable").dataTable({
            "pageLength": 50
        });
        $(All.get_active_tab() + " .datatable").removeAttr('style');
    },

    set_disable_button : function() {
    	$(All.get_active_tab() + " .btn").attr('disabled', 'disabled');
        $(" .btn1").attr('disabled', 'disabled');
        /*$(All.get_active_tab() + " input[type=button]").attr('disabled', 'disabled');
        $(All.get_active_tab() + " input[type=reset]").attr('disabled', 'disabled');
        $(All.get_active_tab() + " input[type=submit]").attr('disabled', 'disabled');*/
    },

    set_enable_button : function() {
        $(All.get_active_tab() + " .btn").removeAttr('disabled');
        $(" .btn1").removeAttr('disabled');
        /*$(All.get_active_tab() + " input[type=button]").removeAttr('disabled');
        $(All.get_active_tab() + " input[type=reset]").removeAttr('disabled');
        $(All.get_active_tab() + " input[type=submit]").removeAttr('disabled');*/
    },

    reset_all_input : function() {
        $(All.get_active_tab() + " input[type=file]").val(null);
        $(All.get_active_tab() + " input[type=text]").val(null);
        $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
		$(All.get_active_tab() + " input[type=password]").val(null);
        $(All.get_active_tab() + " textarea").val(null);
        $(All.get_active_tab() + " select").val("");
    },

    reset_all_input2 : function() {
        $(All.get_active_tab() + " input[type=file]").val(null);
        $(All.get_active_tab() + " input[type=text]").val(null);
        $(All.get_active_tab() + " input[type=hidden]").val(null);
		$(All.get_active_tab() + " input[type=password]").val(null);
        $(All.get_active_tab() + " textarea").val(null);
        $(All.get_active_tab() + " select").val("");
    },

    formUpdateActivate : function() {
    	$(All.get_active_tab() + " #inp_btn").css('display', 'none');
		$(All.get_active_tab() + " #upd_btn").css('display', 'block');
		$(All.get_active_tab() + " .setReadOnly").attr('readonly','readonly');
    },

    cancelUpdateForm : function() {
    	$(All.get_active_tab() + " #upd_btn").css('display', 'none');
	    $(All.get_active_tab() + " #inp_btn").css('display', 'block');
	    $(All.get_active_tab() + " #id").val(null);
	    $(All.get_active_tab() + " .setReadOnly").removeAttr('readonly');
        $(All.get_active_tab() + " #qtyReq").removeAttr('readonly');
	    All.reset_all_input();
	    $(All.get_active_tab() + " #country_id").val("ID");
	    $(All.get_active_tab() + " #hq_id").val("BID06");
	    $(All.get_active_tab() + " #branch_id").val("B001");
	    $(All.get_active_tab() + " #year").val((new Date).getFullYear());
	    $(All.get_active_tab() + " .fileExistingInfo").html(null);
	    $(All.get_active_tab() + " .fileHiddenExistingInfo").val(null);
    },

    check_completed_rack_to_select : function() {
          var cnt_rec = parseInt($(All.get_active_tab() + " #cnt_rec").val());
          var rckcnt = parseInt($(All.get_active_tab() + " #rack_cnt").val());
          var x;
          var err = 0;
          for(x = 1; x <= cnt_rec; x++)
          {
             if($(All.get_active_tab() + " #put_rack" +x).val() == "") {
                err++
             }
          }
        if(err == 0 && rckcnt <= 0) {
           return true;
        } else {
           return false;
        }
    },

    get_base : function() {
       //var url = "http://www.k-linkmember.co.id/ksystemy/";
       var url = "https://www.k-net.co.id/";
       //var url = "http://localhost/ksystemy/";
       return url;
    },

    get_url : function(urlx) {
       var url = All.get_base() + urlx;
       return url;
    },

    reload_page : function(urlx) {
    	var x = All.get_active_tab();
    	$.ajax({
            url: All.get_url(urlx) ,
            type: 'GET',
            success:
            function(data){
                $(All.get_active_tab()).html(null);
                //$(All.get_active_tab()).html("<center><img src="+ All.get_base() +"/asset/images/ajax-loader.gif ></center>");
                $(All.get_active_tab()).html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      });
     //alert(urlx);
    },

    back_to_form : function(clear_div, show_div) {
        //console.log("clear : " +clear_div+ " show : "+show_div);
        $(All.get_active_tab() + clear_div).html(null);
        $(All.get_active_tab() + show_div).show();
    },

    back_to_form2 : function(clear_div, show_div, set_header) {
        $(All.get_box_content() + clear_div).html(null);
        $(All.get_box_content() + show_div).show();
        $(All.get_header_form()).html(set_header);
    },

    get_header_form : function() {
        var getDiv = All.get_active_tab()+ " > .row-fluid > .block > .block-heading";
        return getDiv;
    },

    get_box_content : function() {
        var getDiv = All.get_active_tab()+ " > .row-fluid > .block > .block-body > .box-content > ";
        return getDiv;
    },

    clear_div_in_boxcontent : function(set_clear) {
      $(All.get_box_content() + set_clear).html(null);
    },

    set_error_message : function(set_to, param) {
        set_to = set_to || "hasil";
        param = param || "No result found";

        $(All.get_box_content() + set_to).html(null);
        $(All.get_box_content() + set_to).html("<div class='alert alert-error' align=center>"+param+"</div>");
    },

    show_mainForm_after_process : function() {
        All.clear_div_in_boxcontent(".nextForm1");
        All.clear_div_in_boxcontent(".nextForm2");
        All.clear_div_in_boxcontent(".nextForm3");
        All.clear_div_in_boxcontent(".nextForm4");

        All.set_all_to_display();

        $(All.get_box_content() + ".mainForm").show();
        All.clear_div_in_boxcontent(".mainForm > .result");
    },

    set_all_to_display : function() {
      $(All.get_box_content() + ".nextForm1").css('display', 'block');
      $(All.get_box_content() + ".nextForm2").css('display', 'block');
      $(All.get_box_content() + ".nextForm3").css('display', 'block');
      $(All.get_box_content() + ".nextForm4").css('display', 'block');
    },

    set_success_message : function(set_to, param) {
        set_to = set_to || "hasil";
        param = param || "Success";
        $(All.get_box_content() + set_to).html(null);
        $(All.get_box_content() + set_to).html("<div class='alert alert-success' align=center>"+param+"</div>");
    },
	num : function(val) {
		//console.log("isi val : " +val);
		if(isNaN(val)) {
			return val;
		} else {
			var result = val.toString().split('').reverse().join("").match(/[0-9]{1,3}/g).join(".").match(/./g).reverse().join("");
			return result;
		}
	},

	num_normal : function(val) {
         var result = val.toString().split('').reverse().join("")
                          .match(/[0-9]{1,3}/g).join("")
                          .match(/./g).reverse().join("");
		return result;
	},

    nextFocus : function(h, prev, next, evt)
      {
          evt = (evt) ? evt : event;
    	  var charCode = (evt.charCode) ? evt.charCode :
    	  ((evt.which) ? evt.which : evt.keyCode);
    	  if (charCode == 13 || charCode == 3 || charCode == 9)
    	   {
    	     h.elements[next].focus( );
    		 //h.elements[next].select( );
    		 //trans.h.value = trans.h.value.toUpperCase();
             return false;

    	   }
    	   else
    	   {
    	     if(charCode == 43)
    		 {
    		   h.elements[prev].focus( );
    		   //h.elements[next].select( );
    		   return false;
    		 }
    	   }
    	   return true
      },

    checkUncheckAll: function(theElement)
    {

    	var theForm = theElement.form;

    	for(z=0; z<theForm.length;z++)
    	{

    		if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkall')
    		{
    			theForm[z].checked = theElement.checked;
    		}

    	}
     },

     checkUncheckByClass : function(kelas, setto) {
     	var tes = $(All.get_active_tab() + " ." +kelas).is(':checked');
     	if(tes) {
     		if(kelas == "menux") {
     			$(All.get_active_tab() + " ." +setto).prop('checked', true);
     		} else {
     			$(All.get_active_tab() + " ." +setto).prop('checked', true);
     			$(All.get_active_tab() + " ." +setto).val(1);
     		}

     	} else {
     		if(kelas == "menux") {
     			$(All.get_active_tab() + " ." +setto).prop('checked', false);
     		} else {
     			$(All.get_active_tab() + " ." +setto).prop('checked', false);
     			$(All.get_active_tab() + " ." +setto).val(0);
     		}

     	}

     },

     setValCheck : function(tes) {
     	var x = tes.value;
     	if(x == "0") {
     		tes.value = "1";
     	} else {
     		tes.value = "0";
     	}
     },

     should_integer : function(checkField, focusIfValid) {
        var nm = /^[0-9]+$/;
        var amount = $(All.get_active_tab() + " #" +checkField).val();

        if(amount.match(nm))
        {
            $(All.get_active_tab() + " .to_submit").removeAttr('disabled', 'yes');
            $(All.get_active_tab() + " #" +focusIfValid).focus();
        }
        else if (amount == '') {
            $(All.get_active_tab() + " #" +checkField).focus();

        }
        else
        {
            alert('Number should not negative or should not contain character');
            $(All.get_active_tab() + " .to_submit").attr('disabled', 'yes');
            $(All.get_active_tab() + " #" +checkField).focus();
            //$("#amount").focus();
        }
    },

    checkDoubleInput: function(url, param, paramValue) {
    	if(paramValue !== "") {

	    	All.set_disable_button();
			All.get_wait_message();
			$.ajax({
	            url: All.get_url(url + param+"/" +paramValue),
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data){

	                if(data.response == "true") {
						alert("Double " +param+ " with value : " +paramValue);
					} else {
						All.set_enable_button();
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	    } else {
	    	alert("Please fill the field..");
	    }
    },

    postCheckDoubleInput: function(url, param, paramValue) {
    	if(paramValue !== "") {
	    	All.set_disable_button();
			All.get_wait_message();
			$.ajax({
	            url: All.get_url(url + param),
	            type: 'post',
	            data: {param: paramValue},
				dataType: 'json',
	            success:
	            function(data){
	                All.set_enable_button();
	                if(data.response == "true") {
						alert("Double " +param+ " with value : " +paramValue);
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	    } else {
	    	alert("Please fill the field..");
	    }
    },

    checkDoubleInputWithDisableButton: function(url, param, paramValue) {
    	if(paramValue !== "") {
	    	All.set_disable_button();
			All.get_wait_message();
			$.ajax({
	            url: All.get_url(url + param+"/" +paramValue),
	            type: 'GET',
				dataType: 'json',
	            success:
	            function(data) {
	                if(data.response == "true") {
						alert("Double " +param+ " with value : " +paramValue);
					} else {
						All.set_enable_button();
					}
	            },
	            error: function (xhr, ajaxOptions, thrownError) {
	                 alert(thrownError + ':' +xhr.status);
					 All.set_enable_button();
	            }
	        });
	    } else {
	    	alert("Please fill the field..");
	    }
    },


    getListData : function(url) {
    	All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url(url) ,
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
				//$(All.get_active_tab() + " #searchF").val(0);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },


//ningsih
    sanListVch : function(url,form){
        //All.set_disable_button();
        //$("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1xd").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url(url) , $('#'+form).serialize(), function(hasil)
        {
            //All.set_enable_button();
            $("#listClaimed").hide();
            $("#listClaimed").html(null);
            $("#listClaimed").html(null);
            $("#result").html(null);

            $("#hasilPencarian1xd").show();
            $("#hasilPencarian1xd").html(null);
            $("#hasilPencarian1xd").html(hasil);
        });
        //}
    },

    searchOfficeCahyono : function(url,from){
        //All.set_disable_button();
        //$("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1xd").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url(url) , $('#'+from).serialize(), function(hasil)
        {
            //All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#detail_downline").html(null);
            $("#result").html(null);

            $("#hasilPencarian1xd").show();
            $("#hasilPencarian1xd").html(null);
            $("#hasilPencarian1xd").html(hasil);
        });
        //}
    },
    searchOfficeCahyono2 : function(url,from){
        //All.set_disable_button();
        //$("input[type=button]").attr('disabled', 'disabled');
        $("#hasilPencarian1").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $("#hasilPencarian2").html('<span style="display: block; text-align: center;">Silakan tunggu...</span>');
        $.post(All.get_url(url) , $('#'+from).serialize(), function(hasil)
        {
            //All.set_enable_button();
            $("#detail_downline").hide();
            $("#detail_downline").html(null);
            $("#detail_downline").html(null);
            $("#result").html(null);

            $("#hasilPencarian1").show();
            $("#hasilPencarian1").html(null);
            $("#hasilPencarian1").html(hasil);

            $("#hasilPencarian2").show();
            $("#hasilPencarian2").html(null);
            $("#hasilPencarian2").html(hasil);
        });
        //}
    },
    getListDataCahyono : function(url) {
        //All.set_disable_button();
        All.get_image_load();
        $.ajax({
            url: All.get_url(url) ,
            type: 'GET',
            success:
                function(data){
                    All.set_enable_button();
                    $("#hasilPencarian1").hide();
                    All.clear_div_in_boxcontent(".mainForm .result");
                    $(All.get_box_content() + ".mainForm .result").html(data);
                    //$(All.get_active_tab() + " #searchF").val(0);
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    inputFormDataCahyono: function(url, formid) {
        //All.set_disable_button();
        All.get_wait_message();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
        {
            All.set_enable_button();
            if(data.response == "false") {
                All.set_error_message(".mainForm .result", data.message);
            }
            else {
                All.set_success_message(".mainForm .result", data.message);
                All.reset_all_input();
            }

        }, "json").fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
    },


    postListData: function(formID, url) {
    	All.set_disable_button();
		//All.get_image_load();
		var activeForm = $('#'+formID);
		console.log(All.get_url(url));
		$.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {
            All.set_enable_button();
			All.clear_div_in_boxcontent(".mainForm .result");
			$(All.get_box_content() + ".mainForm .result").html(data);

        }).fail(function() {
	            alert("Error requesting page");
	            All.set_enable_button();
	        });

    },

    ajaxFormGet : function(url) {
    	All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url(url) ,
            type: 'GET',
            success:
            function(data){
                All.set_enable_button();
				$(All.get_active_tab() + " .result").html(null);
                $(All.get_active_tab() + " .result").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },


	ajaxJsonGetResponse : function(url) {
    	All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url(url) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
				All.set_enable_button();
                alert(data.message);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },

    ajaxFormPost : function(formID, url) {
		All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(data);
        }).fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
	},

    ajaxFormPost2 : function(formID, url) {
        All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {
            All.set_enable_button();
            $(All.get_active_tab() + " .hasil").html(null);
            $(All.get_active_tab() + " .hasil").html(data);
        }).fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
    },

    ajaxPostResetField : function(formID, url){
   	    All.set_disable_button();
        All.get_image_load2();
        $.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formID).serialize(), function(data)
        {
            All.reset_all_input();
            All.set_enable_button();
            $(All.get_active_tab() + " .result").html(null);
            $(All.get_active_tab() + " .result").html(data);
        }).fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
    },

	ajaxShowDetailonNextForm : function(urlx) {
		All.set_disable_button();
		$.ajax({
            url: All.get_url(urlx),
            type: 'GET',
            success:
            function(data){
            	All.set_enable_button();
            	$(All.get_active_tab() + " .mainForm").hide();
                All.clear_div_in_boxcontent(".nextForm1");
                $(All.get_active_tab() + " .nextForm1").html(data);
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    }
        });
	},

	ajaxShowDetailonNextForm2 : function(urlx) {
		All.set_disable_button();
		$.ajax({
            url: All.get_url(urlx),
            type: 'GET',
            success:
            function(data){
            	All.set_enable_button();
                $(All.get_active_tab() + " .mainForm").hide();
            	$(All.get_active_tab() + " .nextForm1").hide();
                All.clear_div_in_boxcontent(".nextForm2");
                $(All.get_active_tab() + " .nextForm2").html(data);
            },
		    error: function(jqXHR, textStatus, errorThrown) {
		       All.set_enable_button();
		    }
        });
	},

    ajaxShowDetailonNextForm3 : function(urlx) {
        All.set_disable_button();
        $.ajax({
            url: All.get_url(urlx),
            type: 'GET',
            success:
                function(data){
                    All.set_enable_button();
                    $(All.get_active_tab() + " .mainForm").hide();
                    All.clear_div_in_boxcontent(".nextForm1");
                    $(All.get_active_tab() + " .nextForm1").html(data);
                },
            error: function(jqXHR, textStatus, errorThrown) {
                All.set_enable_button();
            }
        });
    },

    inputFormData: function(url, formid) {
    	All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #"+ formid).serialize(), function(data)
        {
            All.set_enable_button();
			if(data.response == "false") {
                All.set_error_message(".mainForm .result", data.message);
            }
            else {
                All.set_success_message(".mainForm .result", data.message);
				All.reset_all_input();
            }

        }, "json").fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
    },

    ajaxPostUpdate : function(formid, formUpdate, showFormAfterPost) {

		All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url(formUpdate),
            type: 'GET',
            dataType: "json",
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
                if(data.response == "true") {
                	All.ajaxFormPost(formid, showFormAfterPost);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},

    updateFormData: function(url, formid, nextToLoad) {
    	All.set_disable_button();
		All.get_wait_message();
		$.post(All.get_url(url) , $(All.get_active_tab() + " #" +formid).serialize(), function(data)
        {
            All.set_enable_button();
			alert(data.message);
			if(data.response == "true") {
                	All.reset_all_input();
                	All.cancelUpdateForm();
                	All.getListData(nextToLoad);
	            }


        }, "json").fail(function() {
            alert("Error requesting page");
            All.set_enable_button();
        });
    },

    updateFormDataEnctype: function(url, formid, nextToLoad) {

        All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);

        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {

                All.set_enable_button();
				alert(data.message);
				if(data.response == "true") {
                	All.reset_all_input();
                	All.cancelUpdateForm();
                	All.getListData(nextToLoad);
	            }
            },
            cache: false,
            contentType: false,
            processData: false,

            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },

    readFromFile : function(formid, url) {
		var formData = new FormData($(All.get_active_tab() + " #" +formid)[0]);
		All.set_disable_button();
		All.get_wait_message();
		$.ajax({
			url : All.get_url(url),
			type : 'POST',
			data : formData,
			async : false,
			success : function(data) {
				All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
			},
			cache : false,
			contentType : false,
			processData : false,

			error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
		});

	},

	updateDionDD: function(url, formid, nextToLoad) {
		All.set_disable_button();
		All.get_wait_message();

		var paramx = $(All.get_active_tab() + " #" +formid);
		var formData = new FormData(paramx[0]);

		//var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);

        console.log(formData);
	},

    updateFormDataEnctypeNoListReload: function(url, formid, nextToLoad) {

        All.set_disable_button();
		All.get_wait_message();
		var formData = new FormData($(All.get_active_tab() + " #"+ formid)[0]);

        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: false,
            success: function (data) {

                All.set_enable_button();
				alert(data.message);
				if(data.response == "true") {
                	All.reset_all_input();
                	All.cancelUpdateForm();
                	//All.getListData(nextToLoad);
	            }
            },
            cache: false,
            contentType: false,
            processData: false,

            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },
    deleteFormData: function(url, param, nextToLoad) {
    	All.set_disable_button();
		All.get_wait_message();

		$.ajax({
            url: All.get_url(url + param) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
				if(data.response == "true") {
					All.getListData(nextToLoad);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
      });

      //alert('url :' +url+ 'param : ' +param+ 'load :' +nextToLoad);
    },

    deleteData : function(url, formID, showFormAfterPost) {
    	All.set_disable_button();
    	$.ajax({
            url: All.get_url(url) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
				if(data.response == "true") {
					//All.getListData(nextToLoad);
					All.ajaxFormPost(formID, showFormAfterPost);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },

    reconcileData : function(url, formID, showFormAfterPost) {
    	All.set_disable_button();
    	$.ajax({
            url: All.get_url(url) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
				if(data.response == "true") {
					//alert('masuk sini');
					//All.getListData(nextToLoad);
					All.ajaxFormPost(formID, showFormAfterPost);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },

    postAjaxForm  : function(formID, url) {

		All.set_disable_button();
		All.get_wait_message();
		var paramx = $(All.get_active_tab() + " #" +formID);
		var formData = new FormData(paramx[0]);

        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                All.set_enable_button();
				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });
	},

	jsonPostRequest: function(formID, url) {
		All.set_disable_button();
		//All.get_wait_message();
		var paramx = $(All.get_active_tab() + " #" +formID);
		var formData = new FormData(paramx[0]);

		$.ajax({
            url: All.get_url(url),
            type: 'POST',
			dataType: 'json',
            data: formData,
            async: false,
            success: function (data) {
                All.set_enable_button();
                alert(data.message)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            },
            cache: false,
            contentType: false,
            processData: false
        });
	},

	postAjaxFormResetField : function(formID, url) {

		All.set_disable_button();
		All.get_wait_message();
		var paramx = $(All.get_active_tab() + " #" +formID);
		var formData = new FormData(paramx[0]);

        $.ajax({
            url: All.get_url(url),
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                All.set_enable_button();
                $(All.get_active_tab() + " input[type=file]").val(null);
        		$(All.get_active_tab() + " input[type=text]").val(null);
        		$(All.get_active_tab() + " input[type=hidden]").val(null);

				All.clear_div_in_boxcontent(".mainForm .result");
				$(All.get_box_content() + ".mainForm .result").html(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });

	},

	convertDateIndo: function(tgl, to, delimiter)
     {
        var date1 = tgl.split("/");
        var tanggal = "";
        if(to == "ymd") {
            //tanggal = date1[2] + delimiter."".date1[1]."".delimiter.date1[0];
            tanggal = tanggal.concat(date1[2], delimiter, date1[1], delimiter, date1[0]);
        } else if (to == "mdy") {
            //tanggal = date1[1]."".delimiter."".date1[0]."".delimiter.date1[2];
            tanggal = tanggal.concat(date1[1], delimiter, date1[0], delimiter, date1[2]);
        }
        return tanggal;
     },

     dinamicTabContentHeight: function(){
   		 $('#contentY > .ui-tabs-panel').css('max-height',$(window).height() - 205);
   	 },

   	 checkMultipleCheckbox : function(namex) {
   	 	var atLeastOneIsChecked = $(All.get_active_tab() + " input[name='"+namex+"']:checkbox:checked").length;
		if(atLeastOneIsChecked  < 1) {
			alert("Please select at least one checkbox..");
			return false;
		}
   	 },

     getFullNameByID : function(nilai, urlX, setValue) {
        All.set_disable_button();
		$.ajax({
            url: All.get_url(urlX) + "/" +nilai,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
            	console.log(setValue);
                All.set_enable_button();
                if(data.response == "true") {
                	$(All.get_active_tab() + " " + setValue).val(data.arrayData[0].fullnm);
				} else {
					alert("Data "+nilai+ " not found");
					$(All.get_active_tab() + " " + setValue).val(null);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },

    getSaldoBal : function(nilai, urlX, setValue1, setValue2) {
        All.set_disable_button();
        $.ajax({
            url: All.get_url(urlX) + "/" +nilai,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    //alert('4');

                    All.set_enable_button();
                    if(data.response == "true") {
						detmember = data.arrayData[0].dfno + " -- " + data.arrayData[0].fullnm + " -- " + data.arrayData[0].novac;

                        //$(All.get_active_tab() + " " + setValue1).val(data.arrayData[0].fullnm);
                        $(All.get_active_tab() + " " + setValue1).val(detmember);
                        if(data.arrayData[0].amount == 'null'){
                            $(All.get_active_tab() + " " + setValue2).val('Belum ada rekap top up saldo');
                        }
                        else{
                            var num = data.arrayData[0].amount;
                            //console.log(num);
                            saldo = addCommas(num);
                            //sisa_saldo = saldo.toLocaleString(0);

                            $(All.get_active_tab() + " " + setValue2).val(saldo);
                        }

                    } else {
                        alert("Data "+nilai+ " not found");
                        $(All.get_active_tab() + " " + setValue1).val(null);
                        $(All.get_active_tab() + " " + setValue2).val(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    helperDBKlinkMLM : function(table, param, value) {
    	All.set_disable_button();
		All.get_image_load();
		$.ajax({
            url: All.get_url('helper/klink_mlm2010') + "/" +table+ "/" +param+ "/" +value,
            type: 'GET',
            dataType: "json",
            success:
            function(data){
                All.set_enable_button();
                alert(data.message);
                if(data.response == "true") {
                	alert(data.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
    },


   	getUpdateOperator : function(param) {
		All.set_disable_button();
		All.get_wait_message();
		var id = $(All.get_active_tab() + " #id" +param).val();
		$.ajax({
            url: All.get_url("transklink/operatorSms/id/" +id) ,
            type: 'GET',
			dataType: 'json',
            success:
            function(data){
                All.set_enable_button();
				if(data.response == "true") {
					//All.clear_div_in_boxcontent(".mainForm > .result");
  					//$(All.get_box_content() + ".mainForm > #formInputNews").show();
					All.formUpdateActivate();
					$(All.get_active_tab() + " .op_name").html(null);
	  				$(All.get_active_tab() + " .op_id").val(null);
					$(All.get_active_tab() + " #cat_inv_id").val(data.arrayData[0].id);
					$(All.get_active_tab() + " #cat_inv_desc").val(data.arrayData[0].operator_desc);
				}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                 alert(thrownError + ':' +xhr.status);
				 All.set_enable_button();
            }
        });
	},


    listProvinsi : function(idx) {
        All.set_disable_button();
        var txt = $("#"+idx.id+" :selected").text();
        //$(setToDiv).attr("disabled", "disabled");
        console.log('2');
        if(idx.value == "") {
            alert("Silahkan pilih Ekspedisi/Cargo dahulu");
        } else {
            $.ajax({
                url: All.get_url("cargo/province/list/") +idx.value,
                type: 'GET',
                dataType: 'json',
                success:
                    function(data){


                        //$("#provDiv").html(null);
                        //$("#kabDiv").html(null);
                        //$("#kecDiv").html(null);
                        All.set_enable_button();
                        if(data.response == "true") {
                            var arrayData = data.arrayData;

                            if(idx.value == "1") {
                                $(All.get_active_tab() + "#provinsi").html(null);
                                //$("#provinsi").html(null);
                                $("#KecDiv").css("display", "block");
                                $(All.get_active_tab() + "#kota").html(null);
                                $(All.get_active_tab() + "#kecamatan").html(null);
                                $(All.get_active_tab() + "#stockistref").html(null);
                                var rowhtml = "<option value=''>--Select Here--</option>";
                                //rowhtml += "<span>Provinsi<label><font color=red>&nbsp;*</font></label></span>";
                                //rowhtml += "<select class='form-list' name='provinsi' id='provinsi' onchange=Shopping.show_kota(this,'#kota') >";
                                //rowhtml += "<option value=''>--Select Here--</option>";
                                $.each(arrayData, function(key, value) {
                                    rowhtml += "<option value="+value.kode_provinsi+">"+value.provinsi+"</option>";

                                });
                                $("#provinsi").append(rowhtml);

                            } else if(idx.value == "2") {
                                var rowhtml = "<option value=''>--Select Here--</option>";
                                $(All.get_active_tab() + "#provinsi").html(null);
                                $("#KecDiv").css("display", "none");
                                $(All.get_active_tab() + "#kota").html(null);
                                $(All.get_active_tab() + "#stockistref").html(null);
                                $.each(arrayData, function(key, value) {
                                    rowhtml += "<option value='"+value.provinsi+"'>"+value.provinsi+"</option>";

                                });
                                $("#provinsi").append(rowhtml);

                            }

                        } else {
                            //alert(data.message)
                        }
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + ':' +xhr.status);
                    All.set_enable_button();
                }
            });
        }
    },

    tampilkanKota : function(setToDiv) {
        All.set_disable_button();

        var shipper = $("#shipper").val();
        var provinsi = $("#provinsi").val();
        $(setToDiv).html(null);
        $(setToDiv).attr("disabled", "disabled");
        if(shipper == "") {
            alert("Silahkan pilih Provinsi dahulu");
        } else {
            var niss = $("#provinsi :selected").text();
            if(shipper == "1") {
                All.show_kotaJNE(provinsi, setToDiv);
            } else {
                All.show_kota_kgb(provinsi, setToDiv);
            }
            $("#nama_provinsi").val(niss);
        }
    },


    show_kotaJNE : function (nilai, setToDiv)
    {

        $.ajax({
            url: All.get_url("shipping/kota/") +nilai,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){


                    $(setToDiv).removeAttr("disabled", "disabled");
                    All.set_enable_button();
                    if(data.response == "true") {
                        var arrayData = data.arrayData;
                        //$("#nama_provinsi").val(txt);
                        $(setToDiv).html(null);
                        var rowhtml = "<option value=''>--Select here--</option>";
                        $.each(arrayData, function(key, value) {

                            rowhtml += "<option  value="+value.kode_kabupaten+">" +value.kabupaten.toUpperCase()+"</option>";
                            //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

                        });
                        $(setToDiv).append(rowhtml);
                        $("#kecamatan").val(null);
                        $("#stockistref").val(null);
                        $("#sender_address").val(null);
                        $("#destination_address").val(null);
                    } else {
                        $(setToDiv).append(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });

    },

    show_kota_kgb : function(provinsi, setDiv) {
        All.set_disable_button();
        //var txt = $("#"+idx.id+" :selected").text();
        $(setDiv).attr("disabled", "disabled");

        $.ajax({
            url: All.get_url("cargo/kota/list"),
            type: 'POST',
            dataType: 'json',
            data : {provinsiNama:provinsi},
            success:
                function(data){
                    var rowhtml = "<option value=''>--Select Here--</option>";
                    if(data.response == "true") {
                        $(setDiv).removeAttr("disabled");
                        $(setDiv).html(null);
                        var arrayData = data.arrayData;
                        $.each(arrayData, function(key, value) {
                            rowhtml += "<option value='"+value.KotaKode+"'>"+value.KotaNama+"</option>";

                        });
                        //rowhtml += "</select>";
                        //rowhtml += "<input type='hidden' id='nama_provinsi' name='nama_provinsi' value='' />";
                        $(setDiv).append(rowhtml);
                    } else {
                        $(setDiv).append(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });

    },

    show_stockistKGB : function(nilai, setToDiv) {
        $("#destination_address").val(nilai);

        $.ajax({
            url: All.get_url("cargo/stockist/list"),
            type: 'POST',
            dataType: 'json',
            data : {kotaKode:nilai},
            success:
                function(data){
                    var rowhtml = "<option value=''>--Select Here--</option>";
                    if(data.response == "true") {
                        $(setToDiv).removeAttr("disabled");
                        $(setToDiv).html(null);
                        var arrayData = data.arrayData;
                        $.each(arrayData, function(key, value) {
                            rowhtml += "<option value='"+value.loccd+"'>"+value.loccd+" - "+value.fullnm+"</option>";

                        });
                        //rowhtml += "</select>";
                        //rowhtml += "<input type='hidden' id='nama_provinsi' name='nama_provinsi' value='' />";
                        $(setToDiv).append(rowhtml);
                    } else {
                        $(setToDiv).append(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    show_kecamatan : function (idx,setToDiv)
    {
        All.set_disable_button();
        $(setToDiv).attr("disabled", "disabled");
        $.ajax({
            url: All.get_url("shipping/kecamatan/") +idx.value,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    var txt = $("#"+idx.id+" :selected").text();
                    $(setToDiv).removeAttr("disabled", "disabled");
                    All.set_enable_button();
                    if(data.response == "true") {
                        $("#nama_kota").val(txt);
                        var arrayData = data.arrayData;
                        $(setToDiv).html(null);
                        var rowhtml = "<option value=''>--Select here--</option>";
                        $.each(arrayData, function(key, value) {

                            rowhtml += "<option  value="+value.kode_kecamatan+">" +value.kecamatan.toUpperCase()+"</option>";
                            //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

                        });
                        $(setToDiv).append(rowhtml);
                        $("#kelurahan").html(null);

                    } else {
                        $(setToDiv).html(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

	listStockistByArea : function(idx) {
		 All.set_disable_button();

        $.ajax({
            url: All.get_url("shipping/kelurahan/") +idx.value,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    if(data !== null) {
						var arrData = data.arrayData;
						//$(All.get_active_tab() + " #idstockistreff").val(arrData[0].);
					}
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
	},

    show_kelurahan  : function (idx,setToDiv)
    {
        All.set_disable_button();
        $(setToDiv).attr("disabled", "disabled");
        $.ajax({
            url: All.get_url("shipping/kelurahan/") +idx.value,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    var txt = $("#"+idx.id+" :selected").text();
                    //$(setToDiv).removeAttr("disabled", "disabled");
                    All.set_enable_button();
                    if(data.listStk !== null) {
                        $("#nama_kecamatan").val(txt);
                        $("#destination_address").val(data.arrayData[0].kode_kec_JNE);
                        var arrayData = data.listStk;
                        $(setToDiv).html(null);
                        var rowhtml = "<option value=''>--Select here--</option>";
                        $.each(arrayData, function(key, value) {

                            rowhtml += "<option  value="+value.loccd+">"+value.loccd+" - " +value.fullnm.toUpperCase()+" - "+value.state.toUpperCase()+"</option>";
                            //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

                        });
                        $(setToDiv).append(rowhtml);
                        $(setToDiv).removeAttr("disabled");

                    } else {
                        //$(setToDiv).html(null);
                        $("#nama_kecamatan").val(txt);
                        $(setToDiv).html(null);
                        var rowhtml = "<option value=''>--Select here--</option>";

                        $(setToDiv).append(rowhtml);
                        $(setToDiv).removeAttr("disabled");
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },


    tampilkanPriceCode : function() {
        All.set_disable_button();

        var shipper = $("#shipper").val();
        var stockistref = $("#stockistref").val();
        var kota = $("#kota").val();

        if(shipper == "") {
            alert("Silahkan pilih Stockist dahulu");
        } else {
            //var niss = $("#kota :stockistref").text();
            if(shipper == "1") {
                var kec = $("#kecamatan").val();
                All.set_disable_button();
                $("#pricecode").attr("disabled", "disabled");
                $.ajax({
                    url: All.get_url("shipping/getPricecodeX/") +stockistref +"/" +kec,
                    type: 'GET',
                    dataType: 'json',
                    success:
                        function(data){
                            var stkname = $("#stockistref :selected").text();
                            $(All.get_active_tab() + "#pricecode").attr("disabled", "disabled");
                            All.set_enable_button();
                            if(data.response == "true") {
                                $("#nama_stockist").val(stkname);
                                $("#nama_stockistr1ref").val(stkname);
                                $("#state").val(data.arrayData[0].state);
                                $("#sender_address").val(data.arrayData[0].kode_kec_JNE_Origin);
                                $("#jne_branch").val(data.arrayData[0].jne_branchcd);
                                //console.log(data.arrayData[0].kode_kec_JNE_Origin);
                                $("#pricecode").val(data.arrayData[0].pricecode);

                                console.log(data.arrayData[0].kode_kec_JNE_Origin);
                                console.log(data.arrayData[0].state);

                                if(data.arrayData[0].pricecode == "12E3") {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "block");

                                } else {
                                    $(".westP").css("display", "block");
                                    $(".eastP").css("display", "none");
                                }
                                //$("#state").val(data.arrayData[0].pricecode);
                                All.updatePriceAtHeader(data.arrayData[0].pricecode);
                            } else {
                                alert("Price code tidak ada");
                            }
                        },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + ':' +xhr.status);
                        All.set_enable_button();
                    }
                });
            } else {
                $.ajax({
                    url: All.get_url("cargo/pricecode/") +kota,
                    type: 'GET',
                    dataType: 'json',
                    success:
                        function(data){

                            var stkname = $("#stockistref :selected").text();
                            $("#pricecode").removeAttr("disabled", "disabled");
                            All.set_enable_button();
                            if(data.response == "true") {
                                //var arrayData = data.arrayData;
                                $("#nama_stockist").val(stkname);
                                $("#nama_stockistr1ref").val(stkname);
                                $("#pricecode").val(data.arrayData[0].pricecode);
                                $("#sender_address").val(null);
                                $("#jne_branch").val(null);
                                if(data.arrayData[0].pricecode == "12E3") {
                                    $(".westP").css("display", "none");
                                    $(".eastP").css("display", "block");

                                } else {
                                    $(".westP").css("display", "block");
                                    $(".eastP").css("display", "none");
                                }
                                All.updatePriceAtHeader(data.arrayData[0].pricecode);
                            } else {
                                alert("Price code tidak ada..")
                                //$(setToDiv).append(null);
                            }
                        },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + ':' +xhr.status);
                        All.set_enable_button();
                    }
                });
            }
            //$("#nama_kota").val(niss);
        }
    },

    updatePriceAtHeader : function(pricecode) {
        var totQtyWest = $("#totQtyWest").val();
        var totalAmountEast = $("#totalAmountEast").val();
        var totalAmountWest = $("#totalAmountWest").val();
        $("#showInfoSumCart").html(null);
        var row = "";
        if(pricecode == "12W3") {
            row += "&nbsp;&nbsp;&nbsp;&nbsp;"+ totQtyWest + "&nbsp;&nbsp; items | Rp. " + All.num(totalAmountWest);
        } else {
            row += "&nbsp;&nbsp;&nbsp;&nbsp;"+ totQtyWest + "&nbsp;&nbsp; items | Rp. " + All.num(totalAmountEast);
        }
        $("#showInfoSumCart").append(row);

    },

    tampilkanKecamatan : function() {
        All.set_disable_button();

        var shipper = $("#shipper").val();
        var kota = $("#kota").val();

        if(shipper == "") {
            alert("Silahkan pilih Kota/Kabupaten dahulu");
        } else {
            var niss = $("#kota :selected").text();
            if(shipper == "1") {
                $("#kecamatan").html(null);
                $(All.get_active_tab() + " #kecamatan").attr("disabled", "disabled");
                All.show_kecamatanJNE(kota, '#kecamatan');
            } else {
                //Shopping.show_kota_kgb(kota, setToDiv);
                $("#stockistref").html(null);
                $(All.get_active_tab() + " #stockistref").attr("disabled", "disabled");
                All.show_stockistKGB(kota, '#stockistref');
            }
            $("#nama_kota").val(niss);
        }
    },

    show_kecamatanJNE : function(nilai, setToDiv) {
        $.ajax({
            url: All.get_url("shipping/kecamatan/") +nilai,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){
                    //var txt = $("#"+idx.id+" :selected").text();
                    $(setToDiv).removeAttr("disabled", "disabled");
                    All.set_enable_button();
                    if(data.response == "true") {
                        //$("#nama_kota").val(txt);
                        var arrayData = data.arrayData;
                        $(setToDiv).html(null);
                        var rowhtml = "<option value=''>--Select here--</option>";
                        $.each(arrayData, function(key, value) {

                            rowhtml += "<option  value="+value.kode_kecamatan+">" +value.kecamatan.toUpperCase()+"</option>";
                            //rowhtml = "<input type="hidden" value="+value.pricecode+" name="pricecode">";

                        });
                        $(setToDiv).append(rowhtml);
                        $("#kelurahan").html(null);

                    } else {
                        $(setToDiv).html(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    cekOrderNo : function (){
        console.log('2');
        var orderno = $("#orderno").val();
        console.log(orderno);

        $.ajax({
            url: All.get_url("trans/null_shipping/cekOrderNo/") +orderno,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){

                    All.set_enable_button();
                    if(data.response == "true" && data.arrayData != null) {
                        //var arrayData = data.arrayData;
                        document.getElementById("btn_input_user").disabled = false;

                        console.log('berhasil');

                    }
                    else {
                        alert("Order No yang diinput salah");

                        document.getElementById("btn_input_user").disabled = true;
                        //$(setToDiv).append(null);
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    },

    //$route['trans/pending/check/(:any)'] = 'backend/transaction/checkNmStockist/$1';
    getNmStockist : function (){
        //console.log('berhasil');
        var id = $("#idstk").val();
        console.log('2');

        $.ajax({
            url: All.get_url("trans/pending/check/") +id,
            type: 'GET',
            dataType: 'json',
            success:
                function(data){

                    All.set_enable_button();
                    if(data != null) {
                        //var arrayData = data.arrayData;
                        var fullnm = data.fullnm;
                        $(All.get_active_tab() + " #nmstk").val(data.fullnm[0].fullnm);

                        console.log('berhasil');

                    }
                    else {
                        alert("Kode Stockist yang diinput salah");
                    }
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + ':' +xhr.status);
                All.set_enable_button();
            }
        });
    }






}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1;
}

function test(){
    alert('test');
}

